'************************************************************************************************************************************
'Class Name : clsCompanyTotalsByAnalysisReport.vb
'Purpose    :
'Date       :10/8/2010
'Written By :Vimal M. Gohil
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

''' <summary>
''' Purpose: Report Generation Class 
''' Developer: Vimal M. Gohil
''' </summary>
Public Class clsPayrollSummary
    Inherits IReportData
    Private Shared ReadOnly mstrModuleName As String = "clsPayrollSummary"
    Private mstrReportId As String = enArutiReport.PayrollSummary
    Dim objDataOperation As clsDataOperation

#Region " Constructor "
    Public Sub New(Byval intLangId As Integer,Byval intCompanyId AS Integer) 'S.SANDEEP |24-Sep-2020| -- START --END
        Me.setReportData(CInt(mstrReportId),intLangId,intCompanyId) 'S.SANDEEP |24-Sep-2020| -- START --END
        Call Create_OnDetailReport()
    End Sub
#End Region

#Region " Private Variables "
    Private mintReportId As Integer = 0
    Private mstrReportTypeName As String = ""

    Private mintPeriodId As Integer = 0
    Private mstrPeriodName As String = ""

    Private mstrOrderByQuery As String = ""

    Private mintTotalIds As Integer = 0


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Private mblnIsActive As Boolean = True
    'Pinkal (24-Jun-2011) -- End



    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes
    Private mblnIgnorezeroHeads As Boolean = False
    'Pinkal (22-Mar-2012) -- End

    'Sohail (16 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Sohail (16 Apr 2012) -- End

    'Sohail (12 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_TableName As String = ""
    'Sohail (12 May 2012) -- End

    'S.SANDEEP [ 11 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrCurrency_Sign As String = String.Empty
    Private mdecEx_Rate As Decimal = 0
    'S.SANDEEP [ 11 SEP 2012 ] -- END



    'Pinkal (21-Jan-2013) -- Start
    'Enhancement : TRA Changes
    Private menExportAction As enExportAction
    Private mdtTableExcel As DataTable
    'Pinkal (21-Jan-2013) -- End


    'Pinkal (24-May-2013) -- Start
    'Enhancement : TRA Changes
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrUserAccessFilter As String = String.Empty
    Private mintBaseCurrencyId As Integer = 0
    'Pinkal (24-May-2013) -- End


    'Anjan [16 November 2015] -- Start
    'ENHANCEMENT : Include setting for Analysis by to be shown on each page. Requested by Rutta for VFT.
    Private mblnAnalysisOnNewPage As Boolean = False
    'Anjan [16 November 2015] -- End

    'Sohail (21 Jan 2016) -- Start
    'Enhancement - 3 Logo options on Payroll Summary Report for KBC.
    Private mblnIsLogo As Boolean = True
    Private mblnIsCompanyInfo As Boolean = False
    Private mblnIsLogoCompanyInfo As Boolean = False
    Private mintPayslipTemplate As Integer = 1
    'Sohail (21 Jan 2016) -- End
    'Sohail (11 Mar 2016) -- Start
    'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
    Private mblnSetPayslipPaymentApproval As Boolean = ConfigParameter._Object._SetPayslipPaymentApproval
    Private mblnShowPaymentApprovalDetailsOnStatutoryReport As Boolean = ConfigParameter._Object._ShowPaymentApprovalDetailsOnStatutoryReport
    Private mstrPreparedByDetails As String = ""
    Private mstrApprovedByDetails As String = ""
    Private mstrAuthorizedByDetails As String = ""
    'Sohail (11 Mar 2016) -- End

    'Sohail (23 Sep 2016) -- Start
    'MWAUWSA Enhancement -  63.1 - Show Employer Contribution and Show Informational Heads options on Payroll Summary.
    Private mblnShowEmployerContribution As Boolean
    Private mblnShowInformational As Boolean
    'Sohail (23 Sep 2016) -- End
    'Sohail (30 Dec 2019) -- Start
    'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
    Private mstrInfoHeadIDs As String = String.Empty
    'Sohail (30 Dec 2019) -- End

#End Region

#Region " Properties "
    Public WriteOnly Property _ReportId() As Integer
        Set(ByVal value As Integer)
            mintReportId = value
        End Set
    End Property

    Public WriteOnly Property _ReportTypeName() As String
        Set(ByVal value As String)
            mstrReportTypeName = value
        End Set
    End Property

    Public WriteOnly Property _PeriodId() As Integer
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    Public WriteOnly Property _PeriodName() As String
        Set(ByVal value As String)
            mstrPeriodName = value
        End Set
    End Property

    Public WriteOnly Property _OrderByQuery() As String
        Set(ByVal value As String)
            mstrOrderByQuery = value
        End Set
    End Property


    'Pinkal (24-Jun-2011) -- Start
    'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    Public WriteOnly Property _IsActive() As Boolean
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property
    'Pinkal (24-Jun-2011) -- End

    'Pinkal (05-Mar-2012) -- Start
    'Enhancement : TRA Changes
    Public WriteOnly Property _IgnoreZeroHeads() As Boolean
        Set(ByVal value As Boolean)
            mblnIgnorezeroHeads = value
        End Set
    End Property
    'Pinkal (05-Mar-2012) -- End

    'Sohail (16 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _PeriodStartDate() As Date
        Set(ByVal value As Date)
            mdtPeriodStartDate = value
        End Set
    End Property

    Public WriteOnly Property _PeriodEndDate() As Date
        Set(ByVal value As Date)
            mdtPeriodEndDate = value
        End Set
    End Property
    'Sohail (16 Apr 2012) -- End

    'Sohail (12 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_TableName() As String
        Set(ByVal value As String)
            mstrAnalysis_TableName = value
        End Set
    End Property
    'Sohail (12 May 2012) -- End

    'S.SANDEEP [ 11 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _Currency_Sign() As String
        Set(ByVal value As String)
            mstrCurrency_Sign = value
        End Set
    End Property

    Public WriteOnly Property _Ex_Rate() As Decimal
        Set(ByVal value As Decimal)
            mdecEx_Rate = value
        End Set
    End Property
    'S.SANDEEP [ 11 SEP 2012 ] -- END


    'Pinkal (24-May-2013) -- Start
    'Enhancement : TRA Changes

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _BaseCurrencyId() As Integer
        Set(ByVal value As Integer)
            mintBaseCurrencyId = value
        End Set
    End Property

    'Pinkal (24-May-2013) -- End

    'Anjan [16 November 2015] -- Start
    'ENHANCEMENT : Include setting for Analysis by to be shown on each page. Requested by Rutta for VFT.
    Public WriteOnly Property _ShowAnalysisOnNewPage() As Boolean
        Set(ByVal value As Boolean)
            mblnAnalysisOnNewPage = value
        End Set
    End Property
    'Anjan [16 November 2015] -- End

    'Sohail (21 Jan 2016) -- Start
    'Enhancement - 3 Logo options on Payroll Summary Report for KBC.
    Public WriteOnly Property _IsOnlyLogo() As Boolean
        Set(ByVal value As Boolean)
            mblnIsLogo = value
        End Set
    End Property
    Public WriteOnly Property _IsOnlyCompanyInfo() As Boolean
        Set(ByVal value As Boolean)
            mblnIsCompanyInfo = value
        End Set
    End Property
    Public WriteOnly Property _IsLogoCompanyInfo() As Boolean
        Set(ByVal value As Boolean)
            mblnIsLogoCompanyInfo = value
        End Set
    End Property

    Public WriteOnly Property _PayslipTemplate() As Integer
        Set(ByVal value As Integer)
            mintPayslipTemplate = value
        End Set
    End Property
    'Sohail (21 Jan 2016) -- End

    'Sohail (23 Sep 2016) -- Start
    'MWAUWSA Enhancement -  63.1 - Show Employer Contribution and Show Informational Heads options on Payroll Summary.
    Public WriteOnly Property _ShowEmployerContribution() As Boolean
        Set(ByVal value As Boolean)
            mblnShowEmployerContribution = value
        End Set
    End Property

    Public WriteOnly Property _ShowInformational() As Boolean
        Set(ByVal value As Boolean)
            mblnShowInformational = value
        End Set
    End Property
    'Sohail (23 Sep 2016) -- End

    'Sohail (30 Dec 2019) -- Start
    'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
    Public WriteOnly Property _InfoHeadIDs() As String
        Set(ByVal value As String)
            mstrInfoHeadIDs = value
        End Set
    End Property
    'Sohail (30 Dec 2019) -- End

#End Region

#Region "Public Function & Procedures "

    Public Sub SetDefaultValue()
        Try
            mintReportId = 0
            mstrReportTypeName = ""

            mintPeriodId = 0
            mstrPeriodName = ""

            mstrOrderByQuery = ""


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            mblnIsActive = True
            'Pinkal (24-Jun-2011) -- End


            'Pinkal (22-Mar-2012) -- Start
            'Enhancement : TRA Changes
            mblnIgnorezeroHeads = False
            'Pinkal (22-Mar-2012) -- End
            'Sohail (30 Dec 2019) -- Start
            'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
            mstrInfoHeadIDs = ""
            'Sohail (30 Dec 2019) -- End

            'Sohail (12 May 2012) -- Start
            'TRA - ENHANCEMENT
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAnalysis_TableName = ""
            'Sohail (12 May 2012) -- End

            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstrCurrency_Sign = ""
            mdecEx_Rate = 0
            'S.SANDEEP [ 11 SEP 2012 ] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetDefaultValue; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Sub FilterTitleAndFilterQuery()
        Me._FilterTitle = ""
        Me._FilterQuery = ""

        Try
            If mintPeriodId > 0 Then
                'Vimal (27 Nov 2010) -- Start 
                objDataOperation.AddParameter("@Period", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
                'Vimal (27 Nov 2010) -- End
            End If

            'Sohail (16 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            If mblnIsActive = False Then
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodStartDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPeriodEndDate))
            End If
            'Sohail (16 Apr 2012) -- End


            'S.SANDEEP [ 11 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mstrCurrency_Sign.Trim.Length > 0 Then
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 33, "Currency :") & " " & mstrCurrency_Sign & " "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 34, "Exchange Rate:") & " " & CDbl(mdecEx_Rate) & " "
            End If
            'S.SANDEEP [ 11 SEP 2012 ] -- END

            If Me.OrderByQuery <> "" Then
                'Sohail (12 May 2012) -- Start
                'TRA - ENHANCEMENT
                'mstrOrderByQuery &= " ORDER BY " & Me.OrderByQuery
                If mintViewIndex > 0 Then
                    'Sohail (20 Feb 2013) -- Start
                    'TRA - ENHANCEMENT
                    'Me._FilterQuery &= " ORDER BY Id, HeadTypeId, " & Me.OrderByQuery
                    Me._FilterQuery &= " ORDER BY GName, HeadTypeId, " & Me.OrderByQuery
                    'Sohail (20 Feb 2013) -- End
                Else
                    'Sohail (20 Feb 2013) -- Start
                    'TRA - ENHANCEMENT
                    'Me._FilterQuery &= "ORDER BY Id, HeadTypeId, " & Me.OrderByQuery
                    Me._FilterQuery &= "ORDER BY GName, HeadTypeId, " & Me.OrderByQuery
                    'Sohail (20 Feb 2013) -- End
                End If
                'Sohail (12 May 2012) -- End
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 24, " Order By : ") & " " & Me.OrderByDisplay
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: FilterTitleAndFilterQuery; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As Aruti.Data.enPrintAction = Aruti.Data.enPrintAction.Preview, Optional ByVal ExportAction As Aruti.Data.enExportAction = Aruti.Data.enExportAction.None)
        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try


        '    'Pinkal (21-Jan-2013) -- Start
        '    'Enhancement : TRA Changes

        '    menExportAction = ExportAction
        '    mdtTableExcel = Nothing

        '    'Pinkal (21-Jan-2013) -- End



        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    objRpt = Generate_DetailReport()
        '    Rpt = objRpt

        '    'objRpt = Generate_DetailReport()

        '    If Not IsNothing(objRpt) Then


        '        'Pinkal (21-Jan-2013) -- Start
        '        'Enhancement : TRA Changes

        '        Dim intArrayColumnWidth As Integer() = Nothing
        '        Dim rowsArrayHeader As New ArrayList
        '        Dim rowsArrayFooter As New ArrayList
        '        Dim blnShowGrandTotal As Boolean = True
        '        Dim objDic As New Dictionary(Of Integer, Object)
        '        Dim strarrGroupColumns As String() = Nothing
        '        Dim strGTotal As String = Language.getMessage(mstrModuleName, 10, "Grand Total :")
        '        Dim strSubTotal As String = Language.getMessage(mstrModuleName, 11, "Sub Total")
        '        Dim row As WorksheetRow
        '        Dim wcell As WorksheetCell

        '        If mdtTableExcel IsNot Nothing Then

        '            Dim intCurrencyColumn As Integer = mdtTableExcel.Columns.Count - 1

        '            If mintViewIndex > 0 Then
        '                Dim strGrpCols As String() = {"column12", "column6"}
        '                strarrGroupColumns = strGrpCols
        '                intCurrencyColumn -= 1
        '            Else
        '                Dim strGrpCols As String() = {"column6"}
        '                strarrGroupColumns = strGrpCols
        '                intCurrencyColumn -= 1
        '            End If


        '            ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

        '            For i As Integer = 0 To intArrayColumnWidth.Length - 1
        '                intArrayColumnWidth(i) = 125
        '            Next

        '            '*******   REPORT FOOTER   ******
        '            row = New WorksheetRow()
        '            wcell = New WorksheetCell("", "s8w")
        '            row.Cells.Add(wcell)
        '            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '            rowsArrayFooter.Add(row)
        '            '----------------------

        '            row = New WorksheetRow()
        '            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 22, "Messages"), "s8bw")
        '            row.Cells.Add(wcell)
        '            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '            rowsArrayFooter.Add(row)


        '            Dim dsListMsg As New DataSet
        '            Dim objMessage As New clsPayslipMessages_master

        '            'S.SANDEEP [04 JUN 2015] -- START
        '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        '            'dsListMsg = objMessage.GetList("Messages", True)
        '            dsListMsg = objMessage.GetList(FinancialYear._Object._DatabaseName, _
        '                                           User._Object._Userunkid, _
        '                                           FinancialYear._Object._YearUnkid, _
        '                                           Company._Object._Companyunkid, _
        '                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
        '                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
        '                                           ConfigParameter._Object._UserAccessModeSetting, True, _
        '                                           ConfigParameter._Object._IsIncludeInactiveEmp, "Messages", True)
        '            'S.SANDEEP [04 JUN 2015] -- END


        '            If dsListMsg.Tables("Messages").Rows.Count > 0 Then


        '                If dsListMsg.Tables("Messages").Rows(0).Item("msg1").ToString().Trim.Length > 0 Then
        '                    row = New WorksheetRow()
        '                    wcell = New WorksheetCell(dsListMsg.Tables("Messages").Rows(0).Item("msg1").ToString, "s8w")
        '                    row.Cells.Add(wcell)
        '                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '                    rowsArrayFooter.Add(row)
        '                End If

        '                If dsListMsg.Tables("Messages").Rows(0).Item("msg2").ToString().Trim.Length > 0 Then
        '                    row = New WorksheetRow()
        '                    wcell = New WorksheetCell(dsListMsg.Tables("Messages").Rows(0).Item("msg2").ToString, "s8w")
        '                    row.Cells.Add(wcell)
        '                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '                    rowsArrayFooter.Add(row)
        '                End If

        '                If dsListMsg.Tables("Messages").Rows(0).Item("msg3").ToString().Trim.Length > 0 Then
        '                    row = New WorksheetRow()
        '                    wcell = New WorksheetCell(dsListMsg.Tables("Messages").Rows(0).Item("msg3").ToString, "s8w")
        '                    row.Cells.Add(wcell)
        '                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '                    rowsArrayFooter.Add(row)
        '                End If

        '                If dsListMsg.Tables("Messages").Rows(0).Item("msg4").ToString().Trim.Length > 0 Then
        '                    row = New WorksheetRow()
        '                    wcell = New WorksheetCell(dsListMsg.Tables("Messages").Rows(0).Item("msg4").ToString, "s8w")
        '                    row.Cells.Add(wcell)
        '                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '                    rowsArrayFooter.Add(row)
        '                End If

        '            End If

        '            row = New WorksheetRow()
        '            wcell = New WorksheetCell("", "s8w")
        '            row.Cells.Add(wcell)
        '            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
        '            rowsArrayFooter.Add(row)


        '            If ConfigParameter._Object._IsShowCheckedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Checked By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowApprovedBy = True Then

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 13, "Approved By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)

        '            End If

        '            If ConfigParameter._Object._IsShowReceivedBy = True Then
        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 14, "Received By :"), "s8bw")
        '                row.Cells.Add(wcell)

        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                wcell.MergeAcross = mdtTableExcel.Columns.Count - 2

        '                rowsArrayFooter.Add(row)

        '                row = New WorksheetRow()
        '                wcell = New WorksheetCell("", "s8w")
        '                row.Cells.Add(wcell)
        '                rowsArrayFooter.Add(row)
        '            End If

        '        End If

        '        'Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, "", "", "", Nothing, "", False, rowsArrayHeader, rowsArrayFooter, objDic, Nothing, False)

        '        'Pinkal (21-Jan-2013) -- End
        '    End If

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: generateReport; Module Name: " & mstrModuleName)
        'End Try
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try
            menExportAction = ExportAction
            mdtTableExcel = Nothing

            'Sohail (21 Jan 2016) -- Start
            'Enhancement - 3 Logo options on Payroll Summary Report for KBC.
            Dim objConfig As New clsConfigOptions
            Dim objCompany As New clsCompany_Master
            Dim objUser As New clsUserAddEdit

            mintCompanyUnkid = xCompanyUnkid
            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid
            objConfig._Companyunkid = xCompanyUnkid
            objCompany._Companyunkid = xCompanyUnkid
            objUser._Userunkid = xUserUnkid
            mintUserUnkid = xUserUnkid
            User._Object._Userunkid = mintUserUnkid
            'Sohail (21 Jan 2016) -- End

            'Sohail (11 Mar 2016) -- Start
            'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
            'objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId, objCompany._Name, objUser._Username)
            mstrPreparedByDetails = ""
            mstrApprovedByDetails = ""
            mstrAuthorizedByDetails = ""

            objRpt = Generate_DetailReport(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xBaseCurrencyId, objCompany._Name, objUser._Username, objConfig._SetPayslipPaymentApproval, objConfig._ShowPaymentApprovalDetailsOnStatutoryReport)
            'Sohail (11 Mar 2016) -- End

            Rpt = objRpt

            If Not IsNothing(objRpt) Then
                Dim intArrayColumnWidth As Integer() = Nothing
                Dim rowsArrayHeader As New ArrayList
                Dim rowsArrayFooter As New ArrayList
                Dim blnShowGrandTotal As Boolean = True
                Dim objDic As New Dictionary(Of Integer, Object)
                Dim strarrGroupColumns As String() = Nothing
                Dim strGTotal As String = Language.getMessage(mstrModuleName, 10, "Grand Total :")
                Dim strSubTotal As String = Language.getMessage(mstrModuleName, 11, "Sub Total")
                Dim row As WorksheetRow
                Dim wcell As WorksheetCell

                If mdtTableExcel IsNot Nothing Then
                    Dim intCurrencyColumn As Integer = mdtTableExcel.Columns.Count - 1

                    If mintViewIndex > 0 Then
                        Dim strGrpCols As String() = {"column12", "column6"}
                        strarrGroupColumns = strGrpCols
                        intCurrencyColumn -= 1
                    Else
                        Dim strGrpCols As String() = {"column6"}
                        strarrGroupColumns = strGrpCols
                        intCurrencyColumn -= 1
                    End If

                    ReDim intArrayColumnWidth(mdtTableExcel.Columns.Count - 1)

                    For i As Integer = 0 To intArrayColumnWidth.Length - 1
                        intArrayColumnWidth(i) = 125
                    Next

                    '*******   REPORT FOOTER   ******
                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayFooter.Add(row)
                    '----------------------

                    row = New WorksheetRow()
                    wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 22, "Messages"), "s8bw")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayFooter.Add(row)


                    Dim dsListMsg As New DataSet
                    Dim objMessage As New clsPayslipMessages_master

                    dsListMsg = objMessage.GetList(xDatabaseName, _
                                                   xUserUnkid, _
                                                   xYearUnkid, _
                                                   xCompanyUnkid, _
                                                   mdtPeriodStartDate, _
                                                   mdtPeriodEndDate, _
                                                   xUserModeSetting, True, _
                                                   mblnIsActive, "Messages", True)
                    objMessage = Nothing

                    If dsListMsg.Tables("Messages").Rows.Count > 0 Then
                        If dsListMsg.Tables("Messages").Rows(0).Item("msg1").ToString().Trim.Length > 0 Then
                            row = New WorksheetRow()
                            wcell = New WorksheetCell(dsListMsg.Tables("Messages").Rows(0).Item("msg1").ToString, "s8w")
                            row.Cells.Add(wcell)
                            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                            rowsArrayFooter.Add(row)
                        End If

                        If dsListMsg.Tables("Messages").Rows(0).Item("msg2").ToString().Trim.Length > 0 Then
                            row = New WorksheetRow()
                            wcell = New WorksheetCell(dsListMsg.Tables("Messages").Rows(0).Item("msg2").ToString, "s8w")
                            row.Cells.Add(wcell)
                            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                            rowsArrayFooter.Add(row)
                        End If

                        If dsListMsg.Tables("Messages").Rows(0).Item("msg3").ToString().Trim.Length > 0 Then
                            row = New WorksheetRow()
                            wcell = New WorksheetCell(dsListMsg.Tables("Messages").Rows(0).Item("msg3").ToString, "s8w")
                            row.Cells.Add(wcell)
                            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                            rowsArrayFooter.Add(row)
                        End If

                        If dsListMsg.Tables("Messages").Rows(0).Item("msg4").ToString().Trim.Length > 0 Then
                            row = New WorksheetRow()
                            wcell = New WorksheetCell(dsListMsg.Tables("Messages").Rows(0).Item("msg4").ToString, "s8w")
                            row.Cells.Add(wcell)
                            wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                            rowsArrayFooter.Add(row)
                        End If

                    End If

                    'Sohail (11 Mar 2016) -- Start
                    'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
                    If mblnSetPayslipPaymentApproval = True AndAlso mblnShowPaymentApprovalDetailsOnStatutoryReport = True Then

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                        rowsArrayFooter.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 12, "Prepared By"), "s8bw")
                        wcell.MergeAcross = 1
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 43, "Approved By"), "s8bw")
                        wcell.MergeAcross = 1
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 44, "Authorized By"), "s8bw")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)
                        '--------------------

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(mstrPreparedByDetails.Replace(vbCrLf, "#10;"), "s8w")
                        wcell.MergeAcross = 1
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(mstrApprovedByDetails.Replace(vbCrLf, "#10;"), "s8w")
                        wcell.MergeAcross = 1
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(mstrAuthorizedByDetails.Replace(vbCrLf, "#10;"), "s8w")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)
                        '--------------------
                    End If
                    'Sohail (11 Mar 2016) -- End

                    row = New WorksheetRow()
                    wcell = New WorksheetCell("", "s8w")
                    row.Cells.Add(wcell)
                    wcell.MergeAcross = mdtTableExcel.Columns.Count - 1
                    rowsArrayFooter.Add(row)

                    'Sohail (11 Mar 2016) -- Start
                    'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
                    If ConfigParameter._Object._IsShowPreparedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 25, "Prepared By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(objUser._Username, "s8w")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 45, "Date :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8BottomLine")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 49, "Sign :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8BottomLine")
                        row.Cells.Add(wcell)

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If
                    'Sohail (11 Mar 2016) -- End

                    If ConfigParameter._Object._IsShowCheckedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 26, "Checked By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        'Sohail (11 Mar 2016) -- Start
                        'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
                        'wcell.MergeAcross = mdtTableExcel.Columns.Count - 2
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 46, "Date :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8BottomLine")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 50, "Sign :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8BottomLine")
                        row.Cells.Add(wcell)
                        'Sohail (11 Mar 2016) -- End

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowApprovedBy = True Then

                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 27, "Approved By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        'Sohail (11 Mar 2016) -- Start
                        'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
                        'wcell.MergeAcross = mdtTableExcel.Columns.Count - 2
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 47, "Date :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8BottomLine")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 51, "Sign :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8BottomLine")
                        row.Cells.Add(wcell)
                        'Sohail (11 Mar 2016) -- End

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)

                    End If

                    If ConfigParameter._Object._IsShowReceivedBy = True Then
                        row = New WorksheetRow()
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 28, "Received By :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        'Sohail (11 Mar 2016) -- Start
                        'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
                        'wcell.MergeAcross = mdtTableExcel.Columns.Count - 2
                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 48, "Date :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8BottomLine")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 52, "Sign :"), "s8bw")
                        row.Cells.Add(wcell)

                        wcell = New WorksheetCell("", "s8BottomLine")
                        row.Cells.Add(wcell)
                        'Sohail (11 Mar 2016) -- End

                        rowsArrayFooter.Add(row)

                        row = New WorksheetRow()
                        wcell = New WorksheetCell("", "s8w")
                        row.Cells.Add(wcell)
                        rowsArrayFooter.Add(row)
                    End If

                End If

                Call ReportExecute(objRpt, PrintAction, ExportAction, xExportReportPath, xOpenReportAfterExport, mdtTableExcel, intArrayColumnWidth, True, True, False, strarrGroupColumns, "", "", "", Nothing, "", False, rowsArrayHeader, rowsArrayFooter, objDic, Nothing, False)

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: generateReportNew; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04 JUN 2015] -- END


    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        OrderByDisplay = ""
        OrderByQuery = ""
        Try
            OrderByDisplay = iColumn_DetailReport.ColumnItem(0).DisplayName
            OrderByQuery = iColumn_DetailReport.ColumnItem(0).Name
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setDefaultOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)
        Try
            Call OrderByExecute(iColumn_DetailReport)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: setOrderBy; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Report Generation "

    Dim iColumn_DetailReport As New IColumnCollection

    Public Property Field_OnDetailReport() As IColumnCollection
        Get
            Return iColumn_DetailReport
        End Get
        Set(ByVal value As IColumnCollection)
            iColumn_DetailReport = value
        End Set
    End Property

    Private Sub Create_OnDetailReport()
        Try
            iColumn_DetailReport.Clear()
            iColumn_DetailReport.Add(New IColumn("TranCode", Language.getMessage(mstrModuleName, 1, "Tran Code")))
            iColumn_DetailReport.Add(New IColumn("TranName", Language.getMessage(mstrModuleName, 2, "Tran Name")))
            iColumn_DetailReport.Add(New IColumn("Amount", Language.getMessage(mstrModuleName, 3, "Amount")))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Create_OnDetailReport; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS



    '    Private Function Generate_DetailReport() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '        Dim StrQ As String = ""
    '        Dim dsList As New DataSet
    '        Dim rpt_Data As ArutiReport.Designer.dsArutiReport

    '        'Vimal (30 Nov 2010) -- Start  
    '        Dim dsListSummary As New DataSet
    '        'Vimal (30 Nov 2010) -- End


    '        'Pinkal (21-Jan-2013) -- Start
    '        'Enhancement : TRA Changes
    '        Dim dtDetailsExport As DataTable = Nothing
    '        'Pinkal (21-Jan-2013) -- End



    '        Try
    '            objDataOperation = New clsDataOperation

    '            'Sohail (14 Aug 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            Dim objExchangeRate As New clsExchangeRate
    '            Dim decDecimalPlaces As Decimal = 0


    '            'Pinkal (24-May-2013) -- Start
    '            'Enhancement : TRA Changes
    '            'objExchangeRate._ExchangeRateunkid = ConfigParameter._Object._Base_CurrencyId

    '            If mintBaseCurrencyId <= 0 Then
    '                mintBaseCurrencyId = ConfigParameter._Object._Base_CurrencyId
    '            End If
    '            objExchangeRate._ExchangeRateunkid = mintBaseCurrencyId

    '            'Pinkal (24-May-2013) -- End


    '            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
    '            'Sohail (14 Aug 2012) -- End

    '            'Sohail (31 Jan 2014) -- Start
    '            If mstrUserAccessFilter.Trim = "" Then
    '                mstrUserAccessFilter = UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Sohail (31 Jan 2014) -- End

    '            'Original TypeId /*------prtranhead_master.trnheadtype_id = 1 *Earning*, prtranhead_master.trnheadtype_id = 2 *Deduction*,
    '            '------prtranhead_master.trnheadtype_id = 3 *Employee St Deduction*, prtranhead_master.trnheadtype_id = 4 *Employer St Contribution
    '            '------prtranhead_master.trnheadtype_id = 5 *Informational */


    '            'Vimal (15 Dec 2010) -- Start 
    '            'ISSUE: Change HeadTypeId
    '            'StrQ = "SELECT  PeriodId " & _
    '            '              ", TranCode " & _
    '            '              ", TranName " & _
    '            '              ", SUM(Amount) AS Amount " & _
    '            '              ", HeadTypeId " & _
    '            '              ", HeadTypeName " & _
    '            '              ", TypeOfId " & _
    '            '        "FROM    ( SELECT    TableHead.periodid AS PeriodId " & _
    '            '                          ", ISNULL(TableHead.TranCode, ' ') AS TranCode " & _
    '            '                          ", ISNULL(TableHead.TranName, ' ') AS TranName " & _
    '            '                          ", ISNULL(TableHead.Amount, 0) AS Amount " & _
    '            '                          ", TableHead.headtypeid AS HeadTypeId " & _
    '            '                          ", TableHead.HeadTypeName AS HeadTypeName " & _
    '            '                          ", TableHead.typeofid AS TypeOfId " & _
    '            '                  "FROM      ( SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '            '                                      ", prtranhead_master.tranheadunkid AS tranunkid " & _
    '            '                                      ", ISNULL(prtranhead_master.trnheadcode, ' ') AS TranCode " & _
    '            '                                      ", ISNULL(prtranhead_master.trnheadname, ' ') AS TranName " & _
    '            '                                      ", ISNULL(prpayrollprocess_tran.amount, 0) AS Amount " & _
    '            '                                      ", prtranhead_master.trnheadtype_id AS headtypeid " & _
    '            '                                      ", prtranhead_master.typeof_id AS typeofid " & _
    '            '                                      ", @Earnings AS HeadTypeName " & _
    '            '                              "FROM      prpayrollprocess_tran " & _
    '            '                                        "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '            '                                        "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '            '                                        "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '            '                              "WHERE     prtranhead_master.trnheadtype_id = 1 " & _
    '            '                                        "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '            '                                        "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '            '                                        "AND hremployee_master.isactive = 1 " & _
    '            '                                        "AND prtnaleave_tran.payperiodunkid = @Period " & _
    '            '                              "UNION ALL " & _
    '            '                              "SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '            '                                      ", prtranhead_master.tranheadunkid AS tranunkid " & _
    '            '                                      ", ISNULL(prtranhead_master.trnheadcode, ' ') AS TranCode " & _
    '            '                                      ", ISNULL(prtranhead_master.trnheadname, ' ') AS TranName " & _
    '            '                                      ", ISNULL(prpayrollprocess_tran.amount, 0) AS Amount " & _
    '            '                                      ", CASE WHEN prtranhead_master.trnheadtype_id = 3 " & _
    '            '                                             "THEN 2 " & _
    '            '                                             "ELSE prtranhead_master.trnheadtype_id " & _
    '            '                                        "END AS headtypeid " & _
    '            '                                      ", prtranhead_master.typeof_id AS typeofid " & _
    '            '                                      ", @Deductions AS HeadTypeName " & _
    '            '                              "FROM      prpayrollprocess_tran " & _
    '            '                                        "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '            '                                        "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '            '                                        "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '            '                              "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '            '                                        "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '            '                                        "AND ( prtranhead_master.trnheadtype_id = 2 " & _
    '            '                                              "OR prtranhead_master.trnheadtype_id = 3 " & _
    '            '                                            ") " & _
    '            '                                        "AND hremployee_master.isactive = 1 " & _
    '            '                                        "AND prtnaleave_tran.payperiodunkid = @Period " & _
    '            '                              "UNION ALL " & _
    '            '                              "SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '            '                                      ", prtranhead_master.tranheadunkid AS tranunkid " & _
    '            '                                      ", ISNULL(prtranhead_master.trnheadcode, ' ') AS TranCode " & _
    '            '                                      ", ISNULL(prtranhead_master.trnheadname, ' ') AS TranName " & _
    '            '                                      ", ISNULL(prpayrollprocess_tran.amount, 0) AS Amount " & _
    '            '                                      ", CASE WHEN prtranhead_master.trnheadtype_id = 4 " & _
    '            '                                             "THEN 6 " & _
    '            '                                             "ELSE prtranhead_master.trnheadtype_id " & _
    '            '                                        "END AS headtypeid " & _
    '            '                                      ", prtranhead_master.typeof_id AS typeofid " & _
    '            '                                      ", @EmployersContributions AS HeadTypeName " & _
    '            '                              "FROM      prpayrollprocess_tran " & _
    '            '                                        "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '            '                                        "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '            '                                        "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '            '                              "WHERE     prtranhead_master.trnheadtype_id = 4 " & _
    '            '                                        "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '            '                                        "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '            '                                        "AND hremployee_master.isactive = 1 " & _
    '            '                                        "AND prtnaleave_tran.payperiodunkid = @Period " & _
    '            '                              "UNION ALL " & _
    '            '                              "SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '            '                                      ", prtranhead_master.tranheadunkid AS tranunkid " & _
    '            '                                      ", ISNULL(prtranhead_master.trnheadcode, ' ') AS TranCode " & _
    '            '                                      ", ISNULL(prtranhead_master.trnheadname, ' ') AS TranName " & _
    '            '                                      ", ISNULL(prpayrollprocess_tran.amount, 0) AS Amount " & _
    '            '                                      ", CASE WHEN prtranhead_master.trnheadtype_id = 5 " & _
    '            '                                             "THEN 7 " & _
    '            '                                             "ELSE prtranhead_master.trnheadtype_id " & _
    '            '                                        "END AS headtypeid " & _
    '            '                                      ", prtranhead_master.typeof_id AS typeofid " & _
    '            '                                      ", @Informational AS HeadTypeName " & _
    '            '                              "FROM      prpayrollprocess_tran " & _
    '            '                                        "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '            '                                        "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '            '                                        "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '            '                              "WHERE     prtranhead_master.trnheadtype_id = 5 " & _
    '            '                                        "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '            '                                        "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '            '                                        "AND prtranhead_master.isappearonpayslip = 1 " & _
    '            '                                        "AND hremployee_master.isactive = 1 " & _
    '            '                                        "AND prtnaleave_tran.payperiodunkid = @Period " & _
    '            '                              "UNION ALL " & _
    '            '                              "SELECT    periodid " & _
    '            '                                      ", tranunkid " & _
    '            '                                      ", TranCode " & _
    '            '                                      ", TranName " & _
    '            '                                      ", SUM(amount) AS Amount " & _
    '            '                                      ", headtypeid " & _
    '            '                                      ", typeofid " & _
    '            '                                      ", HeadTypeName " & _
    '            '                              "FROM      ( SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '            '                                                  ", CASE WHEN ISNULL(lnloan_advance_tran.isloan, " & _
    '            '                                                                     "0) = 1 THEN '3' " & _
    '            '                                                         "WHEN lnloan_advance_tran.isloan = 0 " & _
    '            '                                                         "THEN '4' " & _
    '            '                                                    "END AS tranunkid " & _
    '            '                                                  ", CASE WHEN ISNULL(lnloan_advance_tran.isloan, " & _
    '            '                                                                     "0) = 1 " & _
    '            '                                                         "THEN @Loan " & _
    '            '                                                         "WHEN lnloan_advance_tran.isloan = 0 " & _
    '            '                                                         "THEN @Advance " & _
    '            '                                                    "END AS TranCode " & _
    '            '                                                  ", CASE WHEN ISNULL(lnloan_advance_tran.isloan, " & _
    '            '                                                                     "0) = 1 " & _
    '            '                                                         "THEN @Loan " & _
    '            '                                                         "WHEN lnloan_advance_tran.isloan = 0 " & _
    '            '                                                         "THEN @Advance " & _
    '            '                                                    "END AS TranName " & _
    '            '                                                  ", ISNULL(prpayrollprocess_tran.amount, " & _
    '            '                                                           "0) AS Amount " & _
    '            '                                                  ", CASE WHEN ISNULL(lnloan_advance_tran.isloan, " & _
    '            '                                                                     "0) = 1 THEN '3' " & _
    '            '                                                         "WHEN lnloan_advance_tran.isloan = 0 " & _
    '            '                                                         "THEN '4' " & _
    '            '                                                    "END AS headtypeid " & _
    '            '                                                  ", CASE WHEN ISNULL(lnloan_advance_tran.isloan, " & _
    '            '                                                                     "0) = 1 THEN '3' " & _
    '            '                                                         "WHEN lnloan_advance_tran.isloan = 0 " & _
    '            '                                                         "THEN '4' " & _
    '            '                                                    "END AS typeofid " & _
    '            '                                                  ", CASE WHEN ISNULL(lnloan_advance_tran.isloan, " & _
    '            '                                                                     "0) = 1 " & _
    '            '                                                         "THEN @Loan " & _
    '            '                                                         "WHEN lnloan_advance_tran.isloan = 0 " & _
    '            '                                                         "THEN @Advance " & _
    '            '                                                    "END AS HeadTypeName " & _
    '            '                                          "FROM      prpayrollprocess_tran " & _
    '            '                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '            '                                                    "JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
    '            '                                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '            '                                          "WHERE     prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
    '            '                                                    "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '            '                                                    "AND ISNULL(lnloan_advance_tran.isvoid, 0) = 0 " & _
    '            '                                                    "AND hremployee_master.isactive = 1 " & _
    '            '                                                    "AND prtnaleave_tran.payperiodunkid = @Period " & _
    '            '                                        ") AS SumLoan " & _
    '            '                              "GROUP BY  periodid " & _
    '            '                                      ", tranunkid " & _
    '            '                                      ", TranCode " & _
    '            '                                      ", TranName " & _
    '            '                                      ", headtypeid " & _
    '            '                                      ", typeofid " & _
    '            '                                      ", HeadTypeName " & _
    '            '                              "UNION ALL " & _
    '            '                              "SELECT    periodid " & _
    '            '                                      ", tranunkid " & _
    '            '                                      ", TranCode " & _
    '            '                                      ", TranName " & _
    '            '                                      ", SUM(amount) AS Amount " & _
    '            '                                      ", headtypeid " & _
    '            '                                      ", typeofid " & _
    '            '                                      ", HeadTypeName " & _
    '            '                              "FROM      ( SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '            '                                                  ", '5' AS tranunkid " & _
    '            '                                                  ", @Saving AS TranCode " & _
    '            '                                                  ", @Saving AS TranName " & _
    '            '                                                  ", ISNULL(prpayrollprocess_tran.amount, " & _
    '            '                                                           "0) AS Amount " & _
    '            '                                                  ", '5' AS headtypeid " & _
    '            '                                                  ", '5' AS typeofid " & _
    '            '                                                  ", @Saving AS HeadTypeName " & _
    '            '                                          "FROM      prpayrollprocess_tran " & _
    '            '                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '            '                                                    "JOIN svsaving_tran ON svsaving_tran.savingtranunkid = prpayrollprocess_tran.savingtranunkid " & _
    '            '                                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '            '                                          "WHERE     prpayrollprocess_tran.savingtranunkid > 0 " & _
    '            '                                                    "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '            '                                                    "AND ISNULL(svsaving_tran.isvoid, 0) = 0 " & _
    '            '                                                     "AND hremployee_master.isactive = 1 " & _
    '            '                                                    "AND prtnaleave_tran.payperiodunkid = @Period " & _
    '            '                                        ") AS SumSaving " & _
    '            '                              "GROUP BY  periodid " & _
    '            '                                      ", tranunkid " & _
    '            '                                      ", TranCode " & _
    '            '                                      ", TranName " & _
    '            '                                      ", headtypeid " & _
    '            '                                      ", typeofid " & _
    '            '                                      ", HeadTypeName " & _
    '            '                            ") AS TableHead " & _
    '            '                ") AS CTA " & _
    '            '        "GROUP BY PeriodId " & _
    '            '              ", TranCode " & _
    '            '              ", TranName " & _
    '            '              ", HeadTypeId " & _
    '            '              ", HeadTypeName " & _
    '            '              ", TypeOfId "

    '            StrQ = "SELECT  PeriodId " & _
    '                          ", TranCode "

    '            'Sohail (04 Feb 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= ", TranName "
    '            'Sohail (12 May 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= ", TranName  + ' (' + CAST(COUNT(TypeOfId) AS VARCHAR(max)) + ')' AS TranName  "
    '            StrQ &= ", TranName "
    '            'Sohail (29 Jan 2014) -- Start
    '            'Enhancement - Oman
    '            'StrQ &= ", COUNT(TypeOfId) AS HeadCount "
    '            StrQ &= ", COUNT(DISTINCT employeeunkid) AS HeadCount "
    '            'Sohail (29 Jan 2014) -- End
    '            'Sohail (12 May 2012) -- End
    '            'Sohail (04 Feb 2012) -- End

    '            'S.SANDEEP [ 11 SEP 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            'StrQ &= ", SUM(Amount) AS Amount " & _
    '            '              ", HeadTypeId " & _
    '            '              ", HeadTypeName " & _
    '            '              ", TypeOfId " & _
    '            '              ", Id " & _
    '            '              ", GName " & _
    '            '        "FROM    ( SELECT    TableHead.periodid AS PeriodId " & _
    '            '                          ", ISNULL(TableHead.TranCode, ' ') AS TranCode " & _
    '            '                          ", ISNULL(TableHead.TranName, ' ') AS TranName " & _
    '            '                          ", CAST(ISNULL(TableHead.Amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '            '                          ", TableHead.headtypeid AS HeadTypeId " & _
    '            '                          ", TableHead.HeadTypeName AS HeadTypeName " & _
    '            '                          ", TableHead.typeofid AS TypeOfId " & _
    '            '                          ", TableHead.Id " & _
    '            '                          ", TableHead.GName " & _
    '            '                  "FROM      ( SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '            '                                      ", prtranhead_master.tranheadunkid AS tranunkid " & _
    '            '                                      ", ISNULL(prtranhead_master.trnheadcode, ' ') AS TranCode " & _
    '            '                                      ", ISNULL(prtranhead_master.trnheadname, ' ') AS TranName " & _
    '            '                                      ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '            '                                      ", prtranhead_master.trnheadtype_id AS headtypeid " & _
    '            '                                      ", prtranhead_master.typeof_id AS typeofid " & _
    '            '                                      ", @Earnings AS HeadTypeName "
    '            'Sohail (21 Nov 2013) -- Start
    '            'Issue - TWAWEZA ROUNDING ISSUE - PAYROLL REPORT AND PAYROLL SUMMARY $ Currency report DOES NOT MATCH
    '            'StrQ &= "      , CASE HeadTypeId " & _
    '            '                  "WHEN 3 THEN CASE ismonetary " & _
    '            '                                "WHEN 1 THEN ( SUM(Amount) * " & mdecEx_Rate & " ) " & _
    '            '                                "ELSE SUM(Amount) * 1.0000 " & _
    '            '                              "END " & _
    '            '                  "ELSE ( SUM(Amount) * " & mdecEx_Rate & " ) " & _
    '            '                "END AS Amount " & _
    '            '              ", HeadTypeId " & _
    '            '              ", HeadTypeName " & _
    '            '              ", TypeOfId " & _
    '            '              ", Id " & _
    '            '              ", GName " & _
    '            '        "FROM    ( SELECT    TableHead.periodid AS PeriodId " & _
    '            '                          ", ISNULL(TableHead.TranCode, ' ') AS TranCode " & _
    '            '                          ", ISNULL(TableHead.TranName, ' ') AS TranName " & _
    '            '                          ", CAST(ISNULL(TableHead.Amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '            '                          ", TableHead.headtypeid AS HeadTypeId " & _
    '            '                          ", TableHead.HeadTypeName AS HeadTypeName " & _
    '            '                          ", TableHead.typeofid AS TypeOfId " & _
    '            '                          ", TableHead.ismonetary " & _
    '            '                          ", TableHead.Id " & _
    '            '                          ", TableHead.GName " & _
    '            '                  "FROM      ( SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '            '                                      ", prtranhead_master.tranheadunkid AS tranunkid " & _
    '            '                                      ", ISNULL(prtranhead_master.trnheadcode, ' ') AS TranCode " & _
    '            '                                      ", ISNULL(prtranhead_master.trnheadname, ' ') AS TranName " & _
    '            '                                      ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '            '                                      ", prtranhead_master.trnheadtype_id AS headtypeid " & _
    '            '                                      ", prtranhead_master.typeof_id AS typeofid " & _
    '            '                                      ", @Earnings AS HeadTypeName " & _
    '            '                                      ", ismonetary "
    '            StrQ &= "      , CASE HeadTypeId " & _
    '                              "WHEN 3 THEN CASE ismonetary " & _
    '                                            "WHEN 1 THEN ( SUM(CAST(ISNULL(Amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) ) ) " & _
    '                                            "ELSE SUM(Amount) * 1.0000 " & _
    '                                          "END " & _
    '                              "ELSE ( SUM(CAST(ISNULL(Amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) ) ) " & _
    '                            "END AS Amount " & _
    '                          ", HeadTypeId " & _
    '                          ", HeadTypeName " & _
    '                          ", TypeOfId " & _
    '                          ", Id " & _
    '                          ", GName " & _
    '                    "FROM    ( SELECT    TableHead.periodid AS PeriodId " & _
    '                                      ", ISNULL(TableHead.TranCode, ' ') AS TranCode " & _
    '                                      ", ISNULL(TableHead.TranName, ' ') AS TranName " & _
    '                                      ", CAST(ISNULL(TableHead.Amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) * " & mdecEx_Rate & " AS Amount " & _
    '                                      ", TableHead.headtypeid AS HeadTypeId " & _
    '                                      ", TableHead.HeadTypeName AS HeadTypeName " & _
    '                                      ", TableHead.typeofid AS TypeOfId " & _
    '                                      ", TableHead.ismonetary " & _
    '                                      ", TableHead.employeeunkid " & _
    '                                      ", TableHead.Id " & _
    '                                      ", TableHead.GName " & _
    '                              "FROM      ( SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '                                                  ", prtranhead_master.tranheadunkid AS tranunkid " & _
    '                                                  ", ISNULL(prtranhead_master.trnheadcode, ' ') AS TranCode " & _
    '                                                  ", ISNULL(prtranhead_master.trnheadname, ' ') AS TranName " & _
    '                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                                                  ", prtranhead_master.trnheadtype_id AS headtypeid " & _
    '                                                  ", prtranhead_master.typeof_id AS typeofid " & _
    '                                                  ", @Earnings AS HeadTypeName " & _
    '                                                  ", ismonetary " & _
    '                                                  ", prpayrollprocess_tran.employeeunkid "
    '            'Sohail (29 Jan 2014) - [employeeunkid]
    '            'Sohail (21 Nov 2013) -- End
    '            'Sohail (25 Jun 2013) - [", (SUM(Amount) * " & mdecEx_Rate & ") AS Amount "] [", TableHead.ismonetary "] [", ismonetary "]
    '            'S.SANDEEP [ 11 SEP 2012 ] -- END



    '            'Sohail (12 May 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ",0 AS Id, '' AS GName "
    '            End If
    '            'Sohail (12 May 2012) -- End

    '            StrQ &= "FROM      prpayrollprocess_tran " & _
    '                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                    "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid "

    '            'Sohail (12 May 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Join
    '            End If
    '            'Sohail (12 May 2012) -- End

    '            StrQ &= "WHERE     prtranhead_master.trnheadtype_id = 1 " & _
    '                                                    "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                    "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                                                   "AND prtnaleave_tran.payperiodunkid = @Period "

    '            'Pinkal (24-Jun-2011) -- Start
    '            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '            If mblnIsActive = False Then
    '                'Sohail (16 Apr 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                'StrQ &= " AND hremployee_master.isactive = 1 "
    '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '                'Sohail (16 Apr 2012) -- End
    '            End If
    '            'Pinkal (24-Jun-2011) -- End


    '            'Pinkal (22-Mar-2012) -- Start
    '            'Enhancement : TRA Changes
    '            If mblnIgnorezeroHeads Then
    '                StrQ &= " AND Amount  <> 0 "
    '            End If
    '            'Pinkal (22-Mar-2012) -- End



    '            'Pinkal (24-May-2013) -- Start
    '            'Enhancement : TRA Changes

    '            'Sohail (28 Jun 2011) -- Start
    '            'Issue : According to prvilege that lower level user should not see superior level employees.
    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    'Sohail (17 Apr 2012) -- Start
    '            '    'TRA - ENHANCEMENT
    '            '    'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            '    'Sohail (17 Apr 2012) -- End
    '            'End If
    '            'Sohail (28 Jun 2011) -- End

    '            If mstrUserAccessFilter = "" Then
    '                If UserAccessLevel._AccessLevel.Length > 0 Then
    '                    StrQ &= UserAccessLevel._AccessLevelFilterString
    '                End If
    '            Else
    '                StrQ &= mstrUserAccessFilter
    '            End If

    '            'Pinkal (24-May-2013) -- End



    '            StrQ &= "UNION ALL " & _
    '                                          "SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '                                                  ", prtranhead_master.tranheadunkid AS tranunkid " & _
    '                                                  ", ISNULL(prtranhead_master.trnheadcode, ' ') AS TranCode " & _
    '                                                  ", ISNULL(prtranhead_master.trnheadname, ' ') AS TranName " & _
    '                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                                                  ", CASE WHEN prtranhead_master.trnheadtype_id = 3 " & _
    '                                                         "THEN 2 " & _
    '                                                         "ELSE prtranhead_master.trnheadtype_id " & _
    '                                                    "END AS headtypeid " & _
    '                                                  ", prtranhead_master.typeof_id AS typeofid " & _
    '                                                  ", @Deductions AS HeadTypeName " & _
    '                                                  ", ismonetary " & _
    '                                                  ", prpayrollprocess_tran.employeeunkid "
    '            'Sohail (29 Jan 2014) - [employeeunkid]
    '            'Sohail (25 Jun 2013) - [", ismonetary "]

    '            'Sohail (12 May 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ",0 AS Id, '' AS GName "
    '            End If
    '            'Sohail (12 May 2012) -- End

    '            StrQ &= "FROM      prpayrollprocess_tran " & _
    '                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                    "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid "

    '            'Sohail (12 May 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Join
    '            End If
    '            'Sohail (12 May 2012) -- End

    '            StrQ &= "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                    "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                                                    "AND ( prtranhead_master.trnheadtype_id = 2 " & _
    '                                                          "OR prtranhead_master.trnheadtype_id = 3 " & _
    '                                                        ") " & _
    '                                                  "AND prtnaleave_tran.payperiodunkid = @Period "


    '            'Pinkal (24-Jun-2011) -- Start
    '            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '            If mblnIsActive = False Then
    '                'Sohail (16 Apr 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                'StrQ &= " AND hremployee_master.isactive = 1 "
    '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                     " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                     " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                     " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '                'Sohail (16 Apr 2012) -- End
    '            End If
    '            'Pinkal (24-Jun-2011) -- End


    '            'Pinkal (22-Mar-2012) -- Start
    '            'Enhancement : TRA Changes
    '            If mblnIgnorezeroHeads Then
    '                StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
    '            End If
    '            'Pinkal (22-Mar-2012) -- End



    '            'Pinkal (24-May-2013) -- Start
    '            'Enhancement : TRA Changes

    '            'Sohail (28 Jun 2011) -- Start
    '            'Issue : According to prvilege that lower level user should not see superior level employees.
    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    'Sohail (17 Apr 2012) -- Start
    '            '    'TRA - ENHANCEMENT
    '            '    'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            '    'Sohail (17 Apr 2012) -- End
    '            'End If
    '            'Sohail (28 Jun 2011) -- End

    '            If mstrUserAccessFilter = "" Then
    '                If UserAccessLevel._AccessLevel.Length > 0 Then
    '                    StrQ &= UserAccessLevel._AccessLevelFilterString
    '                End If
    '            Else
    '                StrQ &= mstrUserAccessFilter
    '            End If

    '            'Pinkal (24-May-2013) -- End

    '            'Sohail (21 Jun 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            StrQ &= "UNION ALL " & _
    '                                         "SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '                                                 ", practivity_master.activityunkid AS tranunkid " & _
    '                                                 ", ISNULL(practivity_master.code, ' ') AS TranCode " & _
    '                                                 ", ISNULL(practivity_master.name, ' ') AS TranName " & _
    '                                                 ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                                                 ", practivity_master.trnheadtype_id AS headtypeid " & _
    '                                                 ", CASE practivity_master.trnheadtype_id WHEN " & enTranHeadType.EarningForEmployees & " THEN " & enTypeOf.Other_Earnings & " WHEN " & enTranHeadType.DeductionForEmployee & " THEN " & enTypeOf.Other_Deductions_Emp & " END AS typeofid " & _
    '                                                 ", CASE practivity_master.trnheadtype_id WHEN " & enTranHeadType.EarningForEmployees & " THEN @Earnings  WHEN " & enTranHeadType.DeductionForEmployee & " THEN @Deductions END AS HeadTypeName " & _
    '                                                 ", 1 AS ismonetary " & _
    '                                                 ", prpayrollprocess_tran.employeeunkid "
    '            'Sohail (29 Jan 2014) - [employeeunkid]

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ",0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= "FROM      prpayrollprocess_tran " & _
    '                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                                                    "JOIN practivity_master ON prpayrollprocess_tran.activityunkid = practivity_master.activityunkid "

    '            'Sohail (12 May 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Join
    '            End If
    '            'Sohail (12 May 2012) -- End

    '            StrQ &= "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                    "AND practivity_master.isactive = 1 " & _
    '                                                    "AND practivity_master.activityunkid > 0 " & _
    '                                                    "AND practivity_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ") " & _
    '                                                    "AND prtnaleave_tran.payperiodunkid = @Period "
    '            'Sohail (19 Aug 2014) - [AND practivity_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ") ]


    '            'Pinkal (24-Jun-2011) -- Start
    '            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '            If mblnIsActive = False Then
    '                'Sohail (16 Apr 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                'StrQ &= " AND hremployee_master.isactive = 1 "
    '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                     " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                     " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                     " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '                'Sohail (16 Apr 2012) -- End
    '            End If
    '            'Pinkal (24-Jun-2011) -- End


    '            'Pinkal (22-Mar-2012) -- Start
    '            'Enhancement : TRA Changes
    '            If mblnIgnorezeroHeads Then
    '                StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
    '            End If
    '            'Pinkal (22-Mar-2012) -- End



    '            'Pinkal (24-May-2013) -- Start
    '            'Enhancement : TRA Changes

    '            'Sohail (28 Jun 2011) -- Start
    '            'Issue : According to prvilege that lower level user should not see superior level employees.
    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    'Sohail (17 Apr 2012) -- Start
    '            '    'TRA - ENHANCEMENT
    '            '    'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            '    'Sohail (17 Apr 2012) -- End
    '            'End If
    '            'Sohail (28 Jun 2011) -- End

    '            If mstrUserAccessFilter = "" Then
    '                If UserAccessLevel._AccessLevel.Length > 0 Then
    '                    StrQ &= UserAccessLevel._AccessLevelFilterString
    '                End If
    '            Else
    '                StrQ &= mstrUserAccessFilter
    '            End If
    '            'Sohail (21 Jun 2013) -- End

    '            'Sohail (12 Nov 2014) -- Start
    '            'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
    '            StrQ &= "UNION ALL " & _
    '                                        "SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '                                                ", cmexpense_master.expenseunkid AS tranunkid " & _
    '                                                ", ISNULL(cmexpense_master.code, ' ') AS TranCode " & _
    '                                                ", ISNULL(cmexpense_master.name, ' ') AS TranName " & _
    '                                                ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                                                ", CASE cmexpense_master.trnheadtype_id WHEN 5 THEN 3 ELSE cmexpense_master.trnheadtype_id END AS headtypeid " & _
    '                                                ", CASE cmexpense_master.trnheadtype_id WHEN " & enTranHeadType.EarningForEmployees & " THEN " & enTypeOf.Other_Earnings & " WHEN " & enTranHeadType.DeductionForEmployee & " THEN " & enTypeOf.Other_Deductions_Emp & " WHEN " & enTranHeadType.Informational & " THEN " & enTypeOf.Informational & " END AS typeofid " & _
    '                                                ", CASE cmexpense_master.trnheadtype_id WHEN " & enTranHeadType.EarningForEmployees & " THEN @Earnings  WHEN " & enTranHeadType.DeductionForEmployee & " THEN @Deductions WHEN " & enTranHeadType.Informational & " THEN @Informational END AS HeadTypeName " & _
    '                                                ", 1 AS ismonetary " & _
    '                                                ", prpayrollprocess_tran.employeeunkid "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ",0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= "FROM      prpayrollprocess_tran " & _
    '                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '                                                    "LEFT JOIN cmclaim_process_tran ON cmclaim_process_tran.crprocesstranunkid = prpayrollprocess_tran.crprocesstranunkid " & _
    '                                                    "LEFT JOIN cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
    '                                                    "LEFT JOIN cmclaim_request_master ON cmclaim_process_tran.crmasterunkid = cmclaim_request_master.crmasterunkid "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Join
    '            End If

    '            StrQ &= "WHERE     ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                    "AND ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                                                    "AND cmclaim_process_tran.isvoid = 0 " & _
    '                                                    "AND cmclaim_request_master.isvoid = 0 " & _
    '                                                    "AND cmexpense_master.isactive = 1 " & _
    '                                                    "AND prpayrollprocess_tran.crprocesstranunkid > 0 " & _
    '                                                    "AND prtnaleave_tran.payperiodunkid = @Period "

    '            If mblnIsActive = False Then
    '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                     " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                     " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                     " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '            End If

    '            If mblnIgnorezeroHeads Then
    '                StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
    '            End If

    '            If mstrUserAccessFilter = "" Then
    '                If UserAccessLevel._AccessLevel.Length > 0 Then
    '                    StrQ &= UserAccessLevel._AccessLevelFilterString
    '                End If
    '            Else
    '                StrQ &= mstrUserAccessFilter
    '            End If
    '            'Sohail (12 Nov 2014) -- End

    '            StrQ &= "UNION ALL " & _
    '                                          "SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '                                                  ", prtranhead_master.tranheadunkid AS tranunkid " & _
    '                                                  ", ISNULL(prtranhead_master.trnheadcode, ' ') AS TranCode " & _
    '                                                  ", ISNULL(prtranhead_master.trnheadname, ' ') AS TranName " & _
    '                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                                                  ", CASE WHEN prtranhead_master.trnheadtype_id = 5 " & _
    '                                                         "THEN 3 " & _
    '                                                         "ELSE prtranhead_master.trnheadtype_id " & _
    '                                                    "END AS headtypeid " & _
    '                                                  ", prtranhead_master.typeof_id AS typeofid " & _
    '                                                  ", @Informational AS HeadTypeName " & _
    '                                                  ", ismonetary " & _
    '                                                  ", prpayrollprocess_tran.employeeunkid "
    '            'Sohail (29 Jan 2014) - [employeeunkid]
    '            'Sohail (25 Jun 2013) - [", ismonetary "]

    '            'Sohail (12 May 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ",0 AS Id, '' AS GName "
    '            End If
    '            'Sohail (12 May 2012) -- End

    '            StrQ &= "FROM      prpayrollprocess_tran " & _
    '                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                    "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid "

    '            'Sohail (12 May 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Join
    '            End If
    '            'Sohail (12 May 2012) -- End

    '            StrQ &= "WHERE     prtranhead_master.trnheadtype_id = 5 " & _
    '                                                    "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                    "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                                                    "AND prtranhead_master.isappearonpayslip = 1 " & _
    '                                                    "AND prtnaleave_tran.payperiodunkid = @Period "

    '            'Pinkal (24-Jun-2011) -- Start
    '            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '            If mblnIsActive = False Then
    '                'Sohail (16 Apr 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                'StrQ &= "  AND hremployee_master.isactive = 1 "
    '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '                'Sohail (16 Apr 2012) -- End
    '            End If
    '            'Pinkal (24-Jun-2011) -- End


    '            'Pinkal (22-Mar-2012) -- Start
    '            'Enhancement : TRA Changes
    '            If mblnIgnorezeroHeads Then
    '                StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
    '            End If
    '            'Pinkal (22-Mar-2012) -- End



    '            'Pinkal (24-May-2013) -- Start
    '            'Enhancement : TRA Changes

    '            'Sohail (28 Jun 2011) -- Start
    '            'Issue : According to prvilege that lower level user should not see superior level employees.
    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    'Sohail (17 Apr 2012) -- Start
    '            '    'TRA - ENHANCEMENT
    '            '    'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            '    'Sohail (17 Apr 2012) -- End
    '            'End If
    '            'Sohail (28 Jun 2011) -- End

    '            If mstrUserAccessFilter = "" Then
    '                If UserAccessLevel._AccessLevel.Length > 0 Then
    '                    StrQ &= UserAccessLevel._AccessLevelFilterString
    '                End If
    '            Else
    '                StrQ &= mstrUserAccessFilter
    '            End If

    '            'Pinkal (24-May-2013) -- End

    '            'Sohail (19 Aug 2014) -- Start
    '            'Enhancement - Include Informational heads on Pay Per Activity.
    '            StrQ &= "                     UNION ALL " & _
    '                                         "SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '                                                 ", practivity_master.activityunkid AS tranunkid " & _
    '                                                 ", ISNULL(practivity_master.code, ' ') AS TranCode " & _
    '                                                 ", ISNULL(practivity_master.name, ' ') AS TranName " & _
    '                                                 ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                                                 ", CASE WHEN practivity_master.trnheadtype_id = 5 " & _
    '                                                        "THEN 3 " & _
    '                                                        "ELSE practivity_master.trnheadtype_id " & _
    '                                                   "END AS headtypeid " & _
    '                                                 ", " & enTypeOf.Informational & " AS typeofid " & _
    '                                                 ", @Informational AS HeadTypeName " & _
    '                                                 ", 1 AS ismonetary " & _
    '                                                 ", prpayrollprocess_tran.employeeunkid "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ",0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= "FROM      prpayrollprocess_tran " & _
    '                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                    "LEFT JOIN practivity_master ON practivity_master.activityunkid =  prpayrollprocess_tran.activityunkid " & _
    '                                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Join
    '            End If

    '            StrQ &= "WHERE     prpayrollprocess_tran.activityunkid > 0 " & _
    '                                                    "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                    "AND practivity_master.trnheadtype_id IN (" & enTranHeadType.Informational & ") " & _
    '                                                    "AND prtnaleave_tran.payperiodunkid = @Period "

    '            If mblnIsActive = False Then
    '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '            End If


    '            If mblnIgnorezeroHeads Then
    '                StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
    '            End If

    '            If mstrUserAccessFilter = "" Then
    '                If UserAccessLevel._AccessLevel.Length > 0 Then
    '                    StrQ &= UserAccessLevel._AccessLevelFilterString
    '                End If
    '            Else
    '                StrQ &= mstrUserAccessFilter
    '            End If
    '            'Sohail (19 Aug 2014) -- End

    '            StrQ &= "UNION ALL " & _
    '                                          "SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '                                                  ", prtranhead_master.tranheadunkid AS tranunkid " & _
    '                                                  ", ISNULL(prtranhead_master.trnheadcode, ' ') AS TranCode " & _
    '                                                  ", ISNULL(prtranhead_master.trnheadname, ' ') AS TranName " & _
    '                                                  ", CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                                                  ", prtranhead_master.trnheadtype_id AS headtypeid " & _
    '                                                  ", prtranhead_master.typeof_id AS typeofid " & _
    '                                                  ", @EmployersContributions AS HeadTypeName " & _
    '                                                  ", ismonetary " & _
    '                                                  ", prpayrollprocess_tran.employeeunkid "
    '            'Sohail (29 Jan 2014) - [employeeunkid]
    '            'Sohail (25 Jun 2013) - [", ismonetary "]

    '            'Sohail (12 May 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ",0 AS Id, '' AS GName "
    '            End If
    '            'Sohail (12 May 2012) -- End

    '            StrQ &= "FROM      prpayrollprocess_tran " & _
    '                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                                                    "JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
    '                                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid "

    '            'Sohail (12 May 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Join
    '            End If
    '            'Sohail (12 May 2012) -- End

    '            StrQ &= "WHERE     prtranhead_master.trnheadtype_id = 4 " & _
    '                                                    "AND ISNULL(prpayrollprocess_tran.isvoid, 0) = 0 " & _
    '                                                    "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
    '                                                    "AND prtnaleave_tran.payperiodunkid = @Period "

    '            'Pinkal (24-Jun-2011) -- Start
    '            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '            If mblnIsActive = False Then
    '                'Sohail (16 Apr 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                'StrQ &= " AND hremployee_master.isactive = 1 "
    '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '                'Sohail (16 Apr 2012) -- End
    '            End If
    '            'Pinkal (24-Jun-2011) -- End


    '            'Pinkal (22-Mar-2012) -- Start
    '            'Enhancement : TRA Changes
    '            If mblnIgnorezeroHeads Then
    '                StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
    '            End If
    '            'Pinkal (22-Mar-2012) -- End



    '            'Pinkal (24-May-2013) -- Start
    '            'Enhancement : TRA Changes

    '            'Sohail (28 Jun 2011) -- Start
    '            'Issue : According to prvilege that lower level user should not see superior level employees.
    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    'Sohail (17 Apr 2012) -- Start
    '            '    'TRA - ENHANCEMENT
    '            '    'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            '    'Sohail (17 Apr 2012) -- End
    '            'End If
    '            'Sohail (28 Jun 2011) -- End

    '            If mstrUserAccessFilter = "" Then
    '                If UserAccessLevel._AccessLevel.Length > 0 Then
    '                    StrQ &= UserAccessLevel._AccessLevelFilterString
    '                End If
    '            Else
    '                StrQ &= mstrUserAccessFilter
    '            End If

    '            'Pinkal (24-May-2013) -- End

    '            'Sohail (20 Feb 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "UNION ALL " & _
    '            '                              "SELECT    periodid " & _
    '            '                                      ", tranunkid " & _
    '            '                                      ", TranCode " & _
    '            '                                      ", TranName " & _
    '            '                                      ", SUM(amount) AS Amount " & _
    '            '                                      ", headtypeid " & _
    '            '                                      ", typeofid " & _
    '            '                                      ", HeadTypeName " & _
    '            '                              "FROM      ( SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '            '                                                  ", '2' AS tranunkid " & _
    '            '                                                  ", CASE WHEN ISNULL(lnloan_advance_tran.isloan, " & _
    '            '                                                                     "0) = 1 THEN @Loan " & _
    '            '                                                         "WHEN lnloan_advance_tran.isloan = 0 " & _
    '            '                                                         "THEN @Advance " & _
    '            '                                                    "END AS TranCode " & _
    '            '                                                  ", CASE WHEN ISNULL(lnloan_advance_tran.isloan, " & _
    '            '                                                                     "0) = 1 THEN @Loan " & _
    '            '                                                         "WHEN lnloan_advance_tran.isloan = 0 " & _
    '            '                                                         "THEN @Advance " & _
    '            '                                                    "END AS TranName " & _
    '            '                                                  ", ISNULL(prpayrollprocess_tran.amount, " & _
    '            '                                                           "0) AS Amount " & _
    '            '                                                  ", '2' AS headtypeid " & _
    '            '                                                  ", '14' AS typeofid " & _
    '            '                                                  ", @Deductions AS HeadTypeName " & _
    '            '                                          "FROM      prpayrollprocess_tran " & _
    '            '                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '            '                                                    "JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid " & _
    '            '                                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '            '                                          "WHERE     prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
    '            '                                                    "AND ISNULL(prpayrollprocess_tran.isvoid, " & _
    '            '                                                               "0) = 0 " & _
    '            '                                                    "AND ISNULL(lnloan_advance_tran.isvoid, " & _
    '            '                                                               "0) = 0 " & _
    '            '                                                    "AND prtnaleave_tran.payperiodunkid = @Period "
    '            StrQ &= "UNION ALL " & _
    '                                          " SELECT  payperiodunkid AS periodid " & _
    '                                                              ", '2' AS tranunkid " & _
    '                                                              ", tranheadcode AS TranCode " & _
    '                                                              ", trnheadname AS TranName " & _
    '                                                              ", CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                                                              ", '2' AS headtypeid " & _
    '                                                              ", CASE WHEN GroupID = " & enViewPayroll_Group.Loan & " THEN '14' " & _
    '                                                                     "WHEN GroupID = " & enViewPayroll_Group.Advance & " THEN '14' " & _
    '                                                                     "WHEN GroupID = " & enViewPayroll_Group.Saving & " THEN '15' " & _
    '                                                                "END AS typeofid " & _
    '                                                              ", @Deductions AS HeadTypeName " & _
    '                                                              ", 1 ismonetary " & _
    '                                                              ", vwPayroll.employeeunkid "
    '            'Sohail (29 Jan 2014) - [employeeunkid]
    '            'Sohail (25 Jun 2013) - [", 1 ismonetary "]

    '            'Sohail (12 May 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ",0 AS Id, '' AS GName "
    '            End If
    '            'Sohail (12 May 2012) -- End

    '            StrQ &= "FROM    vwPayroll " & _
    '                                                        "JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "

    '            'Sohail (12 May 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Join
    '            End If
    '            'Sohail (12 May 2012) -- End

    '            StrQ &= "WHERE   payperiodunkid = @Period " & _
    '                                                                "AND GroupID IN ( " & enViewPayroll_Group.Loan & ", " & enViewPayroll_Group.Advance & ", " & enViewPayroll_Group.Saving & " ) "

    '            'Sohail (20 Feb 2012) -- End
    '            'Pinkal (24-Jun-2011) -- Start
    '            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '            If mblnIsActive = False Then
    '                'Sohail (20 Feb 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                'StrQ &= "  AND hremployee_master.isactive = 1 "
    '                'Sohail (16 Apr 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                'StrQ &= "  AND vwPayroll.isactive = 1 "
    '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '                'Sohail (16 Apr 2012) -- End
    '                'Sohail (20 Feb 2012) -- End
    '            End If
    '            'Pinkal (24-Jun-2011) -- End

    '            'Pinkal (22-Mar-2012) -- Start
    '            'Enhancement : TRA Changes
    '            If mblnIgnorezeroHeads Then
    '                StrQ &= " AND Amount  <> 0 "
    '            End If
    '            'Pinkal (22-Mar-2012) -- End




    '            'Pinkal (24-May-2013) -- Start
    '            'Enhancement : TRA Changes

    '            'Sohail (28 Jun 2011) -- Start
    '            'Issue : According to prvilege that lower level user should not see superior level employees.
    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    'Sohail (20 Feb 2012) -- Start
    '            '    'TRA - ENHANCEMENT
    '            '    'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '    'Sohail (17 Apr 2012) -- Start
    '            '    'TRA - ENHANCEMENT
    '            '    'StrQ &= " AND vwPayroll.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            '    'Sohail (17 Apr 2012) -- End
    '            '    'Sohail (20 Feb 2012) -- End
    '            'End If
    '            'Sohail (28 Jun 2011) -- End

    '            If mstrUserAccessFilter = "" Then
    '                If UserAccessLevel._AccessLevel.Length > 0 Then
    '                    StrQ &= UserAccessLevel._AccessLevelFilterString
    '                End If
    '            Else
    '                StrQ &= mstrUserAccessFilter
    '            End If

    '            'Pinkal (24-May-2013) -- End


    '            'Sohail (20 Feb 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            StrQ &= "UNION ALL " & _
    '                                       "SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '                                                 ", 0 AS tranunkid " & _
    '                                                 ", ' ' AS TranCode " & _
    '                                                 ", @OpeningBalance AS TranName " & _
    '                                                 ", CAST(ISNULL(prtnaleave_tran.openingbalance, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
    '                                                 ", 1 AS headtypeid " & _
    '                                                 ", -99 AS typeofid " & _
    '                                                 ", @OpeningBalance AS HeadTypeName " & _
    '                                                 ", 1 ismonetary " & _
    '                                                 ", prtnaleave_tran.employeeunkid "
    '            'Sohail (29 Jan 2014) - [employeeunkid]
    '            'Sohail (25 Jun 2013) - [", 1 ismonetary "]

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ",0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= "FROM      prtnaleave_tran " & _
    '                                "JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Join
    '            End If

    '            StrQ &= "WHERE     ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                               "AND prtnaleave_tran.payperiodunkid = @Period "

    '            If mblnIsActive = False Then
    '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '            End If

    '            If mblnIgnorezeroHeads Then
    '                StrQ &= " AND openingbalance  <> 0 "
    '            End If


    '            'Pinkal (24-May-2013) -- Start
    '            'Enhancement : TRA Changes

    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'End If

    '            If mstrUserAccessFilter = "" Then
    '                If UserAccessLevel._AccessLevel.Length > 0 Then
    '                    StrQ &= UserAccessLevel._AccessLevelFilterString
    '                End If
    '            Else
    '                StrQ &= mstrUserAccessFilter
    '            End If

    '            'Pinkal (24-May-2013) -- End


    '            'Sohail (20 Feb 2013) -- End



    '            'Sohail (20 Feb 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= ") AS SumLoan " & _
    '            '                              "GROUP BY  periodid " & _
    '            '                                      ", tranunkid " & _
    '            '                                      ", TranCode " & _
    '            '                                      ", TranName " & _
    '            '                                      ", headtypeid " & _
    '            '                                      ", typeofid " & _
    '            '                                      ", HeadTypeName " & _
    '            '                              "UNION ALL " & _
    '            '                              "SELECT    periodid " & _
    '            '                                      ", tranunkid " & _
    '            '                                      ", TranCode " & _
    '            '                                      ", TranName " & _
    '            '                                      ", SUM(amount) AS Amount " & _
    '            '                                      ", headtypeid " & _
    '            '                                      ", typeofid " & _
    '            '                                      ", HeadTypeName " & _
    '            '                              "FROM      ( SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
    '            '                                                  ", '2' AS tranunkid " & _
    '            '                                                  ", @Saving AS TranCode " & _
    '            '                                                  ", @Saving AS TranName " & _
    '            '                                                  ", ISNULL(prpayrollprocess_tran.amount, " & _
    '            '                                                           "0) AS Amount " & _
    '            '                                                  ", '2' AS headtypeid " & _
    '            '                                                  ", '15' AS typeofid " & _
    '            '                                                  ", @Deductions AS HeadTypeName " & _
    '            '                                          "FROM      prpayrollprocess_tran " & _
    '            '                                                    "JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '            '                                                    "JOIN svsaving_tran ON svsaving_tran.savingtranunkid = prpayrollprocess_tran.savingtranunkid " & _
    '            '                                                    "JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
    '            '                                          "WHERE     prpayrollprocess_tran.savingtranunkid > 0 " & _
    '            '                                                    "AND ISNULL(prpayrollprocess_tran.isvoid, " & _
    '            '                                                               "0) = 0 " & _
    '            '                                                    "AND ISNULL(svsaving_tran.isvoid, 0) = 0 " & _
    '            '                                                    "AND prtnaleave_tran.payperiodunkid = @Period "

    '            ''Pinkal (24-Jun-2011) -- Start
    '            ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE

    '            'If mblnIsActive = False Then
    '            '    StrQ &= "  AND hremployee_master.isactive = 1 "
    '            'End If
    '            ''Pinkal (24-Jun-2011) -- End

    '            ''Sohail (28 Jun 2011) -- Start
    '            ''Issue : According to prvilege that lower level user should not see superior level employees.
    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            'End If
    '            ''Sohail (28 Jun 2011) -- End
    '            'Sohail (20 Feb 2012) -- End

    '            'Sohail (20 Feb 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= ") AS SumSaving " & _
    '            '                              "GROUP BY  periodid " & _
    '            '                                      ", tranunkid " & _
    '            '                                      ", TranCode " & _
    '            '                                      ", TranName " & _
    '            '                                      ", headtypeid " & _
    '            '                                      ", typeofid " & _
    '            '                                      ", HeadTypeName " & _
    '            '                            ") AS TableHead " & _
    '            '                ") AS CTA " & _
    '            '        "GROUP BY PeriodId " & _
    '            '              ", TranCode " & _
    '            '              ", TranName " & _
    '            '              ", HeadTypeId " & _
    '            '              ", HeadTypeName " & _
    '            '              ", TypeOfId "

    '            'Sohail (20 Feb 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= " ) AS TableHead " & _
    '            '                ") AS CTA " & _
    '            '        "GROUP BY PeriodId " & _
    '            '              ", TranCode " & _
    '            '              ", TranName " & _
    '            '              ", HeadTypeId " & _
    '            '              ", HeadTypeName " & _
    '            '              ", TypeOfId " & _
    '            '              ", Id " & _
    '            '              ", GName " 'Sohail (12 May 2012) - [Id,GName]
    '            StrQ &= " ) AS TableHead " & _
    '                            ") AS CTA " & _
    '                    "GROUP BY PeriodId " & _
    '                          ", GName " & _
    '                          ", Id " & _
    '                          ", TranCode " & _
    '                          ", TranName " & _
    '                          ", HeadTypeId " & _
    '                          ", HeadTypeName " & _
    '                          ", TypeOfId " & _
    '                          ", ismonetary "
    '            'Sohail (25 Jun 2013) - [", ismonetary "]

    '            'Sohail (20 Feb 2013) -- End
    '            'Sohail (20 Feb 2012) -- End
    '            'Vimal (15 Dec 2010) -- End


    '            Call FilterTitleAndFilterQuery()

    '            StrQ &= Me._FilterQuery

    '            StrQ &= mstrOrderByQuery

    '            objDataOperation.AddParameter("@Earnings", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Earnings"))
    '            objDataOperation.AddParameter("@Deductions", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Deductions"))
    '            objDataOperation.AddParameter("@EmployersContributions", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Employers Contributions"))
    '            objDataOperation.AddParameter("@Informational", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Informational"))
    '            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Loan"))
    '            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Advance"))
    '            objDataOperation.AddParameter("@Saving", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 39, "Saving"))
    '            objDataOperation.AddParameter("@OpeningBalance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 40, "OpeningBalance"))

    '            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '            End If

    '            'Sohail (05 Jul 2011) -- Start
    '            Dim decEarnTotal As Decimal = 0
    '            Dim decDeducTotal As Decimal = 0
    '            Dim objDic As New Dictionary(Of Integer, Decimal)

    '            If dsList.Tables("DataTable").Select("HeadTypeId = " & enTranHeadType.EarningForEmployees & "").Length > 0 Then
    '                decEarnTotal = dsList.Tables("DataTable").Compute("SUM(Amount)", "HeadTypeId = " & enTranHeadType.EarningForEmployees & "")
    '            End If

    '            If dsList.Tables("DataTable").Select("HeadTypeId = " & enTranHeadType.DeductionForEmployee & "").Length > 0 Then
    '                decDeducTotal = dsList.Tables("DataTable").Compute("SUM(Amount)", "HeadTypeId = " & enTranHeadType.DeductionForEmployee & "")
    '            End If
    '            'Sohail (05 Jul 2011) -- End

    '            rpt_Data = New ArutiReport.Designer.dsArutiReport

    '            'Sohail (12 May 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            Dim decEarnSubTotal As Decimal = 0
    '            Dim decDeducSubTotal As Decimal = 0
    '            Dim strPrevGrpId As String = ""
    '            Dim decOpeningBalance As Decimal = 0
    '            'Sohail (12 May 2012) -- End




    '            'Pinkal (24-Jan-2013) -- Start
    '            'Enhancement : TRA Changes
    '            Dim mstrHeadTypeId As String = ""
    '            'Pinkal (24-Jan-2013) -- End


    '            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

    '                'Sohail (20 Feb 2013) -- Start
    '                'TRA - ENHANCEMENT
    '                If strPrevGrpId <> dtRow.Item("Id").ToString Then
    '                    decEarnSubTotal = 0
    '                    decDeducSubTotal = 0
    '                    decOpeningBalance = 0
    '                End If
    '                If CInt(dtRow.Item("TypeOfId")) = -99 Then 'Opening Balance
    '                    decOpeningBalance = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
    '                    strPrevGrpId = dtRow.Item("Id").ToString
    '                    Continue For
    '                End If
    '                'Sohail (20 Feb 2013) -- End

    '                Dim rpt_Rows As DataRow
    '                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

    '                rpt_Rows.Item("Column1") = dtRow.Item("PeriodId")
    '                rpt_Rows.Item("Column2") = dtRow.Item("TranCode")
    '                rpt_Rows.Item("Column3") = dtRow.Item("TranName")
    '                rpt_Rows.Item("Column4") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
    '                rpt_Rows.Item("Column5") = dtRow.Item("HeadTypeId")
    '                rpt_Rows.Item("Column6") = dtRow.Item("HeadTypeName")
    '                rpt_Rows.Item("Column7") = dtRow.Item("TypeOfId")

    '                'Sohail (05 Jul 2011) -- Start
    '                If CInt(dtRow.Item("HeadTypeId")) = enTranHeadType.EarningForEmployees Then
    '                    'Sohail (12 May 2012) -- Start
    '                    'TRA - ENHANCEMENT
    '                    'rpt_Rows.Item("Column8") = Format(decEarnTotal, GUI.fmtCurrency)
    '                    If strPrevGrpId <> dtRow.Item("Id").ToString Then
    '                        decEarnSubTotal = CDec(dtRow.Item("Amount"))
    '                    Else
    '                        decEarnSubTotal += CDec(dtRow.Item("Amount"))
    '                    End If
    '                    rpt_Rows.Item("Column8") = Format(decEarnSubTotal, GUI.fmtCurrency)



    '                    'Pinkal (24-Jan-2013) -- Start
    '                    'Enhancement : TRA Changes
    '                    rpt_Rows.Item("Column83") = decEarnSubTotal
    '                    'Pinkal (24-Jan-2013) -- End

    '                    'Sohail (12 May 2012) -- End
    '                ElseIf CInt(dtRow.Item("HeadTypeId")) = enTranHeadType.DeductionForEmployee Then
    '                    'Sohail (12 May 2012) -- Start
    '                    'TRA - ENHANCEMENT
    '                    'rpt_Rows.Item("Column8") = Format(decDeducTotal, GUI.fmtCurrency)
    '                    If strPrevGrpId <> dtRow.Item("Id").ToString Then
    '                        decDeducSubTotal = CDec(dtRow.Item("Amount"))
    '                    Else
    '                        decDeducSubTotal += CDec(dtRow.Item("Amount"))
    '                    End If
    '                    rpt_Rows.Item("Column8") = Format(decDeducSubTotal, GUI.fmtCurrency)
    '                    'Sohail (12 May 2012) -- End



    '                    'Pinkal (24-Jan-2013) -- Start
    '                    'Enhancement : TRA Changes
    '                    rpt_Rows.Item("Column83") = decDeducSubTotal
    '                    'Pinkal (24-Jan-2013) -- End
    '                Else
    '                    rpt_Rows.Item("Column8") = "0"

    '                    'Pinkal (24-Jan-2013) -- Start
    '                    'Enhancement : TRA Changes
    '                    rpt_Rows.Item("Column83") = 0
    '                    'Pinkal (24-Jan-2013) -- End


    '                End If
    '                'Sohail (12 May 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                'rpt_Rows.Item("Column9") = Format(decEarnTotal - decDeducTotal, GUI.fmtCurrency)
    '                rpt_Rows.Item("Column9") = Format(decEarnSubTotal - decDeducSubTotal, GUI.fmtCurrency)
    '                'Sohail (12 May 2012) -- End
    '                'Sohail (05 Jul 2011) -- End

    '                'Sohail (20 Feb 2013) -- Start
    '                'TRA - ENHANCEMENT
    '                rpt_Rows.Item("Column13") = Format(decEarnSubTotal - decDeducSubTotal + decOpeningBalance, GUI.fmtCurrency)
    '                rpt_Rows.Item("Column84") = decEarnSubTotal - decDeducSubTotal + decOpeningBalance
    '                rpt_Rows.Item("Column14") = Format(decOpeningBalance, GUI.fmtCurrency)
    '                'Sohail (20 Feb 2013) -- End

    '                'Sohail (12 May 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                rpt_Rows.Item("Column81") = dtRow.Item("HeadCount")
    '                rpt_Rows.Item("Column11") = dtRow.Item("Id")
    '                rpt_Rows.Item("Column12") = dtRow.Item("GName")
    '                strPrevGrpId = dtRow.Item("Id").ToString
    '                'Sohail (12 May 2012) -- End



    '                'Pinkal (24-Jan-2013) -- Start
    '                'Enhancement : TRA Changes

    '                rpt_Rows.Item("Column10") = dtRow.Item("HeadCount")
    '                rpt_Rows.Item("Column82") = CDec(dtRow.Item("Amount"))

    '                If menExportAction = enExportAction.ExcelExtra Then

    '                    If mstrHeadTypeId <> dtRow.Item("HeadTypeId").ToString AndAlso mstrHeadTypeId <> "" Then
    '                        If mstrHeadTypeId = enTranHeadType.EarningForEmployees Or mstrHeadTypeId = enTranHeadType.DeductionForEmployee Then
    '                            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

    '                            Dim J As Integer = 0

    '                            If mstrHeadTypeId = enTranHeadType.DeductionForEmployee Then
    '                                'Sohail (20 Feb 2013) -- Start
    '                                'TRA - ENHANCEMENT
    '                                'J = 1
    '                                J = 3
    '                                'Sohail (20 Feb 2013) -- End
    '                            End If


    '                            For i As Integer = 0 To J

    '                                'Sohail (20 Feb 2013) -- Start
    '                                'TRA - ENHANCEMENT
    '                                If i = 2 Then 'Net B/F
    '                                    If decEarnSubTotal - decDeducSubTotal = decEarnSubTotal - decDeducSubTotal + decOpeningBalance Then
    '                                        Continue For
    '                                    End If
    '                                End If
    '                                'Sohail (20 Feb 2013) -- End

    '                                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
    '                                rpt_Rows.Item("Column1") = dtRow.Item("PeriodId")

    '                                If mstrHeadTypeId = enTranHeadType.EarningForEmployees Then
    '                                    rpt_Rows.Item("Column2") = Language.getMessage(mstrModuleName, 19, "Gross Pay")
    '                                ElseIf mstrHeadTypeId = enTranHeadType.DeductionForEmployee And i = 0 Then
    '                                    rpt_Rows.Item("Column2") = Language.getMessage(mstrModuleName, 20, "Total Deduction")
    '                                ElseIf mstrHeadTypeId = enTranHeadType.DeductionForEmployee And i = 1 Then
    '                                    rpt_Rows.Item("Column2") = Language.getMessage(mstrModuleName, 21, "Net Pay")
    '                                    'Sohail (20 Feb 2013) -- Start
    '                                    'TRA - ENHANCEMENT
    '                                ElseIf mstrHeadTypeId = enTranHeadType.DeductionForEmployee And i = 2 Then
    '                                    rpt_Rows.Item("Column2") = Language.getMessage(mstrModuleName, 41, "Net B/F")
    '                                ElseIf mstrHeadTypeId = enTranHeadType.DeductionForEmployee And i = 3 Then
    '                                    rpt_Rows.Item("Column2") = Language.getMessage(mstrModuleName, 42, "Total Net Pay")
    '                                    'Sohail (20 Feb 2013) -- End
    '                                End If

    '                                rpt_Rows.Item("Column3") = ""
    '                                rpt_Rows.Item("Column4") = 0
    '                                rpt_Rows.Item("Column5") = mstrHeadTypeId

    '                                If mstrHeadTypeId = enTranHeadType.EarningForEmployees Then
    '                                    rpt_Rows.Item("Column6") = Language.getMessage(mstrModuleName, 4, "Earnings")
    '                                Else
    '                                    rpt_Rows.Item("Column6") = Language.getMessage(mstrModuleName, 5, "Deductions")
    '                                End If

    '                                rpt_Rows.Item("Column7") = ""

    '                                rpt_Rows.Item("Column11") = dtRow.Item("Id")
    '                                rpt_Rows.Item("Column12") = dtRow.Item("GName")

    '                                If mstrHeadTypeId = enTranHeadType.EarningForEmployees Then
    '                                    rpt_Rows.Item("Column82") = decEarnSubTotal
    '                                ElseIf mstrHeadTypeId = enTranHeadType.DeductionForEmployee And i = 0 Then
    '                                    rpt_Rows.Item("Column82") = decDeducSubTotal
    '                                ElseIf mstrHeadTypeId = enTranHeadType.DeductionForEmployee And i = 1 Then
    '                                    rpt_Rows.Item("Column82") = decEarnSubTotal - decDeducSubTotal
    '                                    'Sohail (20 Feb 2013) -- Start
    '                                    'TRA - ENHANCEMENT
    '                                ElseIf mstrHeadTypeId = enTranHeadType.DeductionForEmployee And i = 2 Then
    '                                    rpt_Rows.Item("Column82") = decOpeningBalance
    '                                ElseIf mstrHeadTypeId = enTranHeadType.DeductionForEmployee And i = 3 Then
    '                                    rpt_Rows.Item("Column82") = decEarnSubTotal - decDeducSubTotal + decOpeningBalance
    '                                    'Sohail (20 Feb 2013) -- End
    '                                End If

    '                                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

    '                            Next

    '                        Else
    '                            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '                        End If

    '                    Else
    '                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '                    End If


    '                    mstrHeadTypeId = dtRow.Item("HeadTypeId")

    '                Else
    '                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '                End If

    '            Next

    '            If menExportAction = enExportAction.ExcelExtra Then
    '                dtDetailsExport = rpt_Data.Tables("ArutiTable")
    '            End If

    '            'Pinkal (24-Jan-2013) -- End


    '            objRpt = New ArutiReport.Designer.rptPayrollSummary

    '            'Sandeep [ 10 FEB 2011 ] -- Start
    '            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '            Dim arrImageRow As DataRow = Nothing
    '            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '            ReportFunction.Logo_Display(objRpt, _
    '                                        ConfigParameter._Object._IsDisplayLogo, _
    '                                        ConfigParameter._Object._ShowLogoRightSide, _
    '                                        "arutiLogo1", _
    '                                        "arutiLogo2", _
    '                                        arrImageRow, _
    '                                        "txtCompanyName", _
    '                                        "txtReportName", _
    '                                        "txtFilterDescription", _
    '                                        ConfigParameter._Object._GetLeftMargin, _
    '                                        ConfigParameter._Object._GetRightMargin)

    '            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '                rpt_Data.Tables("ArutiTable").Rows.Add("")
    '            End If

    '            If ConfigParameter._Object._IsShowPreparedBy = True Then
    '                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 25, "Prepared By :"))
    '                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '            Else
    '                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '            End If

    '            If ConfigParameter._Object._IsShowCheckedBy = True Then
    '                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 26, "Checked By :"))
    '            Else
    '                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection6", True)
    '            End If

    '            If ConfigParameter._Object._IsShowApprovedBy = True Then
    '                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 27, "Approved By :"))
    '            Else
    '                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection7", True)
    '            End If

    '            If ConfigParameter._Object._IsShowReceivedBy = True Then
    '                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 28, "Received By :"))
    '            Else
    '                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection8", True)
    '            End If
    '            'Sandeep [ 10 FEB 2011 ] -- End

    '            objRpt.SetDataSource(rpt_Data)



    '            'Vimal (30 Nov 2010) -- Start 



    '            '/*------prpayment_tran.referenceid = 3 *PaySlip*, prpayment_tran.paymentmode = 1 *Cash*,

    '            'Vimal (10 Dec 2010) -- Start 
    '            'Issue: Return Multiple Rows when any detail is not contained. Ex. When Bank Payment is not done and Cash Payment or Salary on Hold are done then return multiple row.
    '            'StrQ = "SELECT ISNULL(TotalBankPayment.EmpId,0) AS TotalEmpBank " & _
    '            '              ", ISNULL(TotalBankPayment.Amount,0) AS TotalAmountBank " & _
    '            '              ", ISNULL(TotalCashPayment.EmpId,0) AS TotalEmpCash " & _
    '            '              ", ISNULL(TotalCashPayment.Amount,0) AS TotalAmountCash " & _
    '            '              ", ISNULL(TotalSalaryonHold.EmpId,0) AS TotalEmpHold " & _
    '            '              ", ISNULL(TotalSalaryonHold.Amount,0) AS TotalAmountHold " & _
    '            '              ", ISNULL(TotalEmployee.EmpId, 0) AS TotalEmployees " & _
    '            '        "FROM " & _
    '            '       "(SELECT  COUNT(DISTINCT prempsalary_tran.employeeunkid) AS EmpId " & _
    '            '             ", SUM(prempsalary_tran.amount) AS Amount, prpayment_tran.periodunkid AS PeriodId " & _
    '            '       "FROM    prempsalary_tran " & _
    '            '               "JOIN prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
    '            '       "WHERE   ISNULL(prempsalary_tran.isvoid,0) = 0 " & _
    '            '               "AND ISNULL(prpayment_tran.isvoid,0) = 0 " & _
    '            '               "AND prpayment_tran.periodunkid = @Period " & _
    '            '               "GROUP BY prpayment_tran.periodunkid " & _
    '            '               ") AS TotalBankPayment " & _
    '            '             "FULL JOIN " & _
    '            '       "(SELECT  COUNT(prpayment_tran.employeeunkid) AS EmpId " & _
    '            '             ", SUM(prpayment_tran.amount) AS Amount " & _
    '            '             ", prpayment_tran.periodunkid AS PeriodId " & _
    '            '       "FROM    prpayment_tran " & _
    '            '       "WHERE   ISNULL(prpayment_tran.isvoid,0) = 0 " & _
    '            '               "AND prpayment_tran.referenceid = 3 " & _
    '            '               "AND prpayment_tran.paymentmode = 1 " & _
    '            '               "AND prpayment_tran.periodunkid = @Period " & _
    '            '               "GROUP BY prpayment_tran.periodunkid " & _
    '            '              ") AS TotalCashPayment ON TotalBankPayment.PeriodId = TotalCashPayment.PeriodId " & _
    '            '             "FULL JOIN " & _
    '            '        "(SELECT COUNT(prtnaleave_tran.employeeunkid) AS EmpId " & _
    '            '             ", SUM(prtnaleave_tran.balanceamount) AS Amount " & _
    '            '             ", prtnaleave_tran.payperiodunkid AS PeriodId " & _
    '            '             "FROM prtnaleave_tran " & _
    '            '             "WHERE ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
    '            '             "AND prtnaleave_tran.payperiodunkid = @Period " & _
    '            '             "AND prtnaleave_tran.balanceamount <> 0 " & _
    '            '             "GROUP BY prtnaleave_tran.payperiodunkid " & _
    '            '          ") AS TotalSalaryonHold ON TotalBankPayment.PeriodId = TotalSalaryonHold.PeriodId " & _
    '            '          "FULL JOIN " & _
    '            '          "(SELECT COUNT(prtnaleave_tran.employeeunkid) AS EmpId " & _
    '            '                     ", prtnaleave_tran.payperiodunkid AS PeriodId " & _
    '            '                     "FROM prtnaleave_tran " & _
    '            '                     "WHERE ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '            '                     "AND prtnaleave_tran.payperiodunkid = @Period " & _
    '            '                     "AND prtnaleave_tran.total_amount <> 0 " & _
    '            '                     "GROUP BY prtnaleave_tran.payperiodunkid " & _
    '            '             ") AS TotalEmployee ON TotalBankPayment.PeriodId = TotalEmployee.PeriodId "

    '            'Sohail (12 May 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ = "SELECT ISNULL(TotalBankPayment.EmpId,0) AS TotalEmpBank " & _
    '            '               ", ISNULL(TotalBankPayment.Amount,0) AS TotalAmountBank " & _
    '            '               ", ISNULL(TotalCashPayment.EmpId,0) AS TotalEmpCash " & _
    '            '               ", ISNULL(TotalCashPayment.Amount,0) AS TotalAmountCash " & _
    '            '               ", ISNULL(TotalSalaryonHold.EmpId,0) AS TotalEmpHold " & _
    '            '               ", ISNULL(TotalSalaryonHold.Amount,0) AS TotalAmountHold " & _
    '            '               ", ISNULL(TotalEmployee.EmpId, 0) AS TotalEmployees " & _
    '            '        "FROM    ( SELECT    cfcommon_period_tran.periodunkid AS Periodid " & _
    '            '                  "FROM      cfcommon_period_tran " & _
    '            '                  "WHERE    cfcommon_period_tran.isactive = 1 " & _
    '            '                            "AND cfcommon_period_tran.periodunkid = @Period " & _
    '            '                ") AS TablePeriod " & _
    '            '                "LEFT JOIN ( SELECT  COUNT(DISTINCT prempsalary_tran.employeeunkid) AS EmpId " & _
    '            '                                  ", SUM(prempsalary_tran.amount) AS Amount " & _
    '            '                                  ", prpayment_tran.periodunkid AS PeriodId " & _
    '            '        "FROM    prempsalary_tran " & _
    '            '                "JOIN prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
    '            '                                    "JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '            '        "WHERE   ISNULL(prempsalary_tran.isvoid,0) = 0 " & _
    '            '                "AND ISNULL(prpayment_tran.isvoid,0) = 0 " & _
    '            '                                    "AND prpayment_tran.periodunkid = @Period "

    '            ''Sohail (28 Jun 2011) -- Start
    '            ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '            'If mblnIsActive = False Then
    '            '    'Sohail (17 Apr 2012) -- Start
    '            '    'TRA - ENHANCEMENT
    '            '    'StrQ &= "  AND hremployee_master.isactive = 1 "
    '            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "
    '            '    'Sohail (17 Apr 2012) -- End
    '            'End If

    '            ''Issue : According to prvilege that lower level user should not see superior level employees.
    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    'Sohail (17 Apr 2012) -- Start
    '            '    'TRA - ENHANCEMENT
    '            '    'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            '    'Sohail (17 Apr 2012) -- End
    '            'End If
    '            ''Sohail (28 Jun 2011) -- End

    '            'StrQ &= "                   GROUP BY prpayment_tran.periodunkid " & _
    '            '                          ") AS TotalBankPayment ON TablePeriod.Periodid = TotalBankPayment.PeriodId " & _
    '            '                "LEFT JOIN ( SELECT  COUNT(DISTINCT prpayment_tran.employeeunkid) AS EmpId " & _
    '            '              ", SUM(prpayment_tran.amount) AS Amount " & _
    '            '              ", prpayment_tran.periodunkid AS PeriodId " & _
    '            '        "FROM    prpayment_tran " & _
    '            '                                     "JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '            '        "WHERE   ISNULL(prpayment_tran.isvoid,0) = 0 " & _
    '            '                "AND prpayment_tran.referenceid = 3 " & _
    '            '                "AND prpayment_tran.paymentmode = 1 " & _
    '            '                                    "AND prpayment_tran.periodunkid = @Period "

    '            ''Sohail (28 Jun 2011) -- Start
    '            ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '            'If mblnIsActive = False Then
    '            '    'Sohail (17 Apr 2012) -- Start
    '            '    'TRA - ENHANCEMENT
    '            '    'StrQ &= "  AND hremployee_master.isactive = 1 "
    '            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "
    '            '    'Sohail (17 Apr 2012) -- End
    '            'End If

    '            ''Issue : According to prvilege that lower level user should not see superior level employees.
    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    'Sohail (17 Apr 2012) -- Start
    '            '    'TRA - ENHANCEMENT
    '            '    'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            '    'Sohail (17 Apr 2012) -- End
    '            'End If
    '            ''Sohail (28 Jun 2011) -- End

    '            'StrQ &= "                   GROUP BY prpayment_tran.periodunkid " & _
    '            '                          ") AS TotalCashPayment ON TablePeriod.Periodid = TotalCashPayment.PeriodId " & _
    '            '                "LEFT JOIN ( SELECT  COUNT(prtnaleave_tran.employeeunkid) AS EmpId " & _
    '            '              ", SUM(prtnaleave_tran.balanceamount) AS Amount " & _
    '            '              ", prtnaleave_tran.payperiodunkid AS PeriodId " & _
    '            '              "FROM prtnaleave_tran " & _
    '            '                                           "JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '            '              "WHERE ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
    '            '              "AND prtnaleave_tran.payperiodunkid = @Period " & _
    '            '                                    "AND prtnaleave_tran.balanceamount <> 0 "

    '            ''Sohail (28 Jun 2011) -- Start
    '            ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '            'If mblnIsActive = False Then
    '            '    'Sohail (17 Apr 2012) -- Start
    '            '    'TRA - ENHANCEMENT
    '            '    'StrQ &= "  AND hremployee_master.isactive = 1 "
    '            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "
    '            '    'Sohail (17 Apr 2012) -- End
    '            'End If

    '            ''Issue : According to prvilege that lower level user should not see superior level employees.
    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    'Sohail (17 Apr 2012) -- Start
    '            '    'TRA - ENHANCEMENT
    '            '    'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            '    'Sohail (17 Apr 2012) -- End
    '            'End If
    '            ''Sohail (28 Jun 2011) -- End

    '            'StrQ &= "                   GROUP BY prtnaleave_tran.payperiodunkid " & _
    '            '                          ") AS TotalSalaryonHold ON TablePeriod.Periodid = TotalSalaryonHold.PeriodId " & _
    '            '                "LEFT JOIN ( SELECT  COUNT(prtnaleave_tran.employeeunkid) AS EmpId " & _
    '            '                      ", prtnaleave_tran.payperiodunkid AS PeriodId " & _
    '            '                      "FROM prtnaleave_tran " & _
    '            '                                           "JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '            '                      "WHERE ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '            '                      "AND prtnaleave_tran.payperiodunkid = @Period " & _
    '            '                                    "AND prtnaleave_tran.total_amount <> 0 "

    '            ''Sohail (28 Jun 2011) -- Start
    '            ''ISSUE : INCLUSION OF INACTIVE EMPLOYEE
    '            'If mblnIsActive = False Then
    '            '    'Sohail (17 Apr 2012) -- Start
    '            '    'TRA - ENHANCEMENT
    '            '    'StrQ &= "  AND hremployee_master.isactive = 1 "
    '            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '            '       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "
    '            '    'Sohail (17 Apr 2012) -- End
    '            'End If

    '            ''Issue : According to prvilege that lower level user should not see superior level employees.
    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    'Sohail (17 Apr 2012) -- Start
    '            '    'TRA - ENHANCEMENT
    '            '    'StrQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            '    'Sohail (17 Apr 2012) -- End
    '            'End If
    '            ''Sohail (28 Jun 2011) -- End

    '            'StrQ &= "                   GROUP BY prtnaleave_tran.payperiodunkid " & _
    '            '                          ") AS TotalEmployee ON TablePeriod.Periodid = TotalEmployee.PeriodId "

    '            ''Vimal (10 Dec 2010) -- End
    '            StrQ = "SELECT  b.HeadTypeId " & _
    '                          ", c.Id " & _
    '                          ", c.GName " & _
    '                          ", ISNULL(a.EmpId, 0) AS EmpId " & _
    '                          ", ISNULL(a.Amount, 0) AS Amount " & _
    '                          ", b.HeadTypeName " & _
    '                    "FROM    ( SELECT    1 AS HeadTypeId " & _
    '                                      ", @BankPayment AS HeadTypeName " & _
    '                              "UNION " & _
    '                              "SELECT    2 AS HeadTypeId " & _
    '                                      ", @CashPayment AS HeadTypeName " & _
    '                              "UNION " & _
    '                              "SELECT    3 AS HeadTypeId " & _
    '                                      ", @SalaryOnHold AS HeadTypeName " & _
    '                            ") AS b "

    '            If mintViewIndex > 0 Then
    '                StrQ &= "LEFT JOIN ( SELECT  " & Mid(mstrAnalysis_Fields, 2) & " " & _
    '                                     "FROM    " & mstrAnalysis_TableName & " " & _
    '                                   ") AS c ON 1 = 1 "
    '            Else
    '                StrQ &= "LEFT JOIN ( SELECT  0 AS Id, '' AS GName " & _
    '                                   ") AS c ON 1 = 1 "
    '            End If

    '            'Sohail (14 Aug 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "LEFT JOIN ( SELECT  COUNT(DISTINCT prempsalary_tran.employeeunkid) AS EmpId " & _
    '            '                                  ", SUM(prempsalary_tran.amount) AS Amount " & _
    '            '                                  ", prpayment_tran.periodunkid AS PeriodId " & _
    '            '                                  ", 1 AS HeadTypeId "
    '            'StrQ &= "LEFT JOIN ( SELECT  COUNT(DISTINCT prempsalary_tran.employeeunkid) AS EmpId " & _
    '            '                                  ", SUM(CAST(prempsalary_tran.amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
    '            '                                  ", prpayment_tran.periodunkid AS PeriodId " & _
    '            '                                  ", 1 AS HeadTypeId "
    '            StrQ &= "LEFT JOIN ( SELECT  COUNT(DISTINCT prempsalary_tran.employeeunkid) AS EmpId " & _
    '                                             ", SUM(CAST((prempsalary_tran.amount *  " & mdecEx_Rate & ") AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
    '                                             ", prpayment_tran.periodunkid AS PeriodId " & _
    '                                             ", 1 AS HeadTypeId "
    '            'Sohail (14 Aug 2012) -- End

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ",0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= "FROM    prempsalary_tran " & _
    '                            "JOIN prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
    '                                                "JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid "
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Join
    '            End If

    '            StrQ &= "WHERE   ISNULL(prempsalary_tran.isvoid, 0) = 0 " & _
    '                            "AND ISNULL(prpayment_tran.isvoid,0) = 0 " & _
    '                            "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
    '                            "AND prpayment_tran.paymentmode IN ( " & enPaymentMode.CHEQUE & ", " & enPaymentMode.TRANSFER & " ) " & _
    '                                                "AND prpayment_tran.periodunkid = @Period "
    '            'Sohail (25 Mar 2015) - ["AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _]

    '            If mblnIsActive = False Then
    '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '            End If

    '            'Sohail (31 Jan 2014) -- Start
    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'End If
    '            If mstrUserAccessFilter.Length > 0 Then
    '                StrQ &= mstrUserAccessFilter
    '            End If
    '            'Sohail (31 Jan 2014) -- End

    '            StrQ &= "GROUP BY prpayment_tran.periodunkid "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
    '            End If

    '            'Sohail (14 Aug 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "UNION ALL " & _
    '            '                            "SELECT  COUNT(DISTINCT prpayment_tran.employeeunkid) AS EmpId " & _
    '            '              ", SUM(prpayment_tran.amount) AS Amount " & _
    '            '              ", prpayment_tran.periodunkid AS PeriodId " & _
    '            '                                  ", 2 AS HeadTypeId "
    '            'StrQ &= "UNION ALL " & _
    '            '                            "SELECT  COUNT(DISTINCT prpayment_tran.employeeunkid) AS EmpId " & _
    '            '              ", SUM(CAST(prpayment_tran.amount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
    '            '              ", prpayment_tran.periodunkid AS PeriodId " & _
    '            '                                  ", 2 AS HeadTypeId "
    '            StrQ &= "UNION ALL " & _
    '                                        "SELECT  COUNT(DISTINCT prpayment_tran.employeeunkid) AS EmpId " & _
    '                          ", SUM(CAST((prpayment_tran.amount *  " & mdecEx_Rate & ") AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
    '                          ", prpayment_tran.periodunkid AS PeriodId " & _
    '                                              ", 2 AS HeadTypeId "
    '            'Sohail (14 Aug 2012) -- End

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ",0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= "FROM    prpayment_tran " & _
    '                                                "JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid "
    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Join
    '            End If

    '            StrQ &= "WHERE   ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
    '                            "AND prpayment_tran.referenceid = 3 " & _
    '                                                "AND prpayment_tran.paymentmode IN ( " & enPaymentMode.CASH & ", " & enPaymentMode.CASH_AND_CHEQUE & " ) " & _
    '                                                "AND prpayment_tran.periodunkid = @Period "

    '            If mblnIsActive = False Then
    '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '            End If

    '            'Sohail (31 Jan 2014) -- Start
    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'End If
    '            If mstrUserAccessFilter.Length > 0 Then
    '                StrQ &= mstrUserAccessFilter
    '            End If
    '            'Sohail (31 Jan 2014) -- End

    '            StrQ &= "GROUP BY prpayment_tran.periodunkid "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
    '            End If

    '            'Sohail (14 Aug 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'StrQ &= "UNION ALL " & _
    '            '                            "SELECT  COUNT(prtnaleave_tran.employeeunkid) AS EmpId " & _
    '            '              ", SUM(prtnaleave_tran.balanceamount) AS Amount " & _
    '            '              ", prtnaleave_tran.payperiodunkid AS PeriodId " & _
    '            '                                  ", 3 AS HeadTypeId "
    '            'StrQ &= "UNION ALL " & _
    '            '                            "SELECT  COUNT(prtnaleave_tran.employeeunkid) AS EmpId " & _
    '            '              ", SUM(CAST(prtnaleave_tran.balanceamount AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
    '            '              ", prtnaleave_tran.payperiodunkid AS PeriodId " & _
    '            '                                  ", 3 AS HeadTypeId "

    '            StrQ &= "UNION ALL " & _
    '                                        "SELECT  COUNT(prtnaleave_tran.employeeunkid) AS EmpId " & _
    '                          ", SUM(CAST((prtnaleave_tran.balanceamount *  " & mdecEx_Rate & ") AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
    '                          ", prtnaleave_tran.payperiodunkid AS PeriodId " & _
    '                                              ", 3 AS HeadTypeId "
    '            'Sohail (14 Aug 2012) -- End

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields
    '            Else
    '                StrQ &= ",0 AS Id, '' AS GName "
    '            End If

    '            StrQ &= "FROM    prtnaleave_tran " & _
    '                                                "JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Join
    '            End If

    '            StrQ &= "WHERE   ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
    '                          "AND prtnaleave_tran.payperiodunkid = @Period " & _
    '                                                "AND prtnaleave_tran.balanceamount <> 0 "

    '            If mblnIsActive = False Then
    '                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                   " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (16 May 2012) - [empl_enddate]
    '            End If

    '            'Sohail (31 Jan 2014) -- Start
    '            'If UserAccessLevel._AccessLevel.Length > 0 Then
    '            '    StrQ &= UserAccessLevel._AccessLevelFilterString
    '            'End If
    '            If mstrUserAccessFilter.Length > 0 Then
    '                StrQ &= mstrUserAccessFilter
    '            End If
    '            'Sohail (31 Jan 2014) -- End

    '            StrQ &= "GROUP BY prtnaleave_tran.payperiodunkid "

    '            If mintViewIndex > 0 Then
    '                StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
    '            End If

    '            StrQ &= ") AS a ON a.HeadTypeId = b.HeadTypeId " & _
    '                                                "AND a.Id = c.Id " & _
    '                    "ORDER BY c.Id " & _
    '                          ", b.HeadTypeId "

    '            objDataOperation.AddParameter("@BankPayment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Bank Payment")
    '            objDataOperation.AddParameter("@CashPayment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Cash Payment")
    '            objDataOperation.AddParameter("@SalaryOnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Salary On Hold")
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptSubSummary"), "txtBankPayment", Language.getMessage(mstrModuleName, 35, "Bank Payment"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptSubSummary"), "txtCashPayment", Language.getMessage(mstrModuleName, 36, "Cash Payment"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptSubSummary"), "txtSalaryonHold", Language.getMessage(mstrModuleName, 37, "Salary On Hold"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptSubSummary"), "txtNoOfEmployees", Language.getMessage(mstrModuleName, 38, "No of Employees"))
    '            'Sohail (12 May 2012) -- End


    '            dsListSummary = objDataOperation.ExecQuery(StrQ, "Summary")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '            End If

    '            rpt_Data = New ArutiReport.Designer.dsArutiReport

    '            'Sohail (12 May 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            Dim decTotal As Decimal = 0
    '            strPrevGrpId = ""
    '            'Sohail (12 May 2012) -- End



    '            'Pinkal (24-Jan-2013) -- Start
    '            'Enhancement : TRA Changes
    '            Dim mstrGroupId As String = ""
    '            Dim mstrGroupName As String = ""
    '            Dim idx As Integer = 0
    '            'Pinkal (24-Jan-2013) -- End


    '            If dsListSummary.Tables("Summary").Rows.Count > 0 Then
    '                For Each dtSummary As DataRow In dsListSummary.Tables("Summary").Rows
    '                    Dim rpt_SummaryRow As DataRow
    '                    rpt_SummaryRow = rpt_Data.Tables("ArutiTable").NewRow

    '                    'Sohail (12 May 2012) -- Start
    '                    'TRA - ENHANCEMENT
    '                    'rpt_SummaryRow.Item("Column1") = dtSummary.Item("TotalEmpBank")
    '                    'rpt_SummaryRow.Item("Column2") = Format(CDec(dtSummary.Item("TotalAmountBank")), GUI.fmtCurrency)
    '                    'rpt_SummaryRow.Item("Column3") = dtSummary.Item("TotalEmpCash")
    '                    'rpt_SummaryRow.Item("Column4") = Format(CDec(dtSummary.Item("TotalAmountCash")), GUI.fmtCurrency)
    '                    'rpt_SummaryRow.Item("Column5") = dtSummary.Item("TotalEmpHold")
    '                    'rpt_SummaryRow.Item("Column6") = Format(CDec(dtSummary.Item("TotalAmountHold")), GUI.fmtCurrency)
    '                    'rpt_SummaryRow.Item("Column7") = dtSummary.Item("TotalEmployees")

    '                    ''Sohail (05 Jul 2011) -- Start
    '                    'rpt_SummaryRow.Item("Column8") = Format(CDec(dtSummary.Item("TotalAmountBank")) + CDec(dtSummary.Item("TotalAmountCash")) + CDec(dtSummary.Item("TotalAmountHold")), GUI.fmtCurrency)
    '                    ''Sohail (05 Jul 2011) -- End
    '                    rpt_SummaryRow.Item("Column1") = dtSummary.Item("Id")
    '                    rpt_SummaryRow.Item("Column2") = dtSummary.Item("GName")
    '                    rpt_SummaryRow.Item("Column3") = dtSummary.Item("HeadTypeId")
    '                    rpt_SummaryRow.Item("Column81") = dtSummary.Item("EmpId")
    '                    rpt_SummaryRow.Item("Column5") = Format(dtSummary.Item("Amount"), GUI.fmtCurrency)
    '                    rpt_SummaryRow.Item("Column6") = dtSummary.Item("HeadTypeName")

    '                    If strPrevGrpId <> dtSummary.Item("Id").ToString Then
    '                        decTotal = CDec(dtSummary.Item("Amount"))
    '                    Else
    '                        decTotal += CDec(dtSummary.Item("Amount"))
    '                    End If
    '                    rpt_SummaryRow.Item("Column7") = Format(decTotal, GUI.fmtCurrency)


    '                    'Sohail (12 May 2012) -- End


    '                    'Pinkal (24-Jan-2013) -- Start
    '                    'Enhancement : TRA Changes

    '                    If menExportAction = enExportAction.ExcelExtra Then

    '                        If mintViewIndex > 0 Then
    '                            Dim Row() As DataRow = dtDetailsExport.Select("Column11=" & dtSummary.Item("Id"))
    '                            If Row.Length <= 0 Then GoTo ForAnalysisAddRow
    '                        End If

    '                        If dtDetailsExport.Rows.Count > 0 AndAlso dtDetailsExport.Rows(0).Item("Column5").ToString() <> "" Then
    '                            Dim drRow As DataRow = dtDetailsExport.NewRow
    '                            drRow("Column1") = mintPeriodId
    '                            drRow("Column2") = dtSummary.Item("HeadTypeName")
    '                            drRow("Column5") = 99
    '                            drRow("Column6") = Language.getMessage(mstrModuleName, 23, "Payment Details")
    '                            drRow("Column8") = CDec(dtSummary.Item("Amount"))
    '                            drRow("Column10") = dtSummary.Item("EmpId")
    '                            drRow("Column11") = dtSummary.Item("Id")
    '                            drRow("Column12") = dtSummary.Item("GName")
    '                            drRow("Column81") = dtSummary.Item("EmpId")
    '                            drRow("Column82") = CDec(dtSummary.Item("Amount"))
    '                            dtDetailsExport.Rows.Add(drRow)
    '                        End If

    'ForAnalysisAddRow:
    '                        If strPrevGrpId <> "" And strPrevGrpId <> dtSummary.Item("Id").ToString Then
    'ForAddRow:
    '                            If dtDetailsExport.Rows.Count > 0 AndAlso dtDetailsExport.Rows(0).Item("Column5").ToString() <> "" Then
    '                                Dim dNewRow As DataRow = dtDetailsExport.NewRow
    '                                dNewRow("Column1") = mintPeriodId
    '                                dNewRow("Column2") = Language.getMessage(mstrModuleName, 38, "No of Employees")
    '                                dNewRow("Column5") = 99
    '                                dNewRow("Column6") = Language.getMessage(mstrModuleName, 23, "Payment Details")

    '                                Dim mdclTot As Decimal = 0
    '                                Dim mintTot As Integer = 0
    '                                Dim dcRow() As DataRow = dtDetailsExport.Select("Column5 =99 AND Column11=" & mstrGroupId)

    '                                If dcRow.Length > 0 Then

    '                                    For i As Integer = 0 To dcRow.Length - 1
    '                                        mdclTot += CDec(dcRow(i)("Column82"))
    '                                        mintTot += CInt(dcRow(i)("Column81"))
    '                                    Next

    '                                Else
    '                                    Continue For
    '                                End If

    '                                dNewRow("Column8") = mdclTot
    '                                dNewRow("Column82") = mdclTot

    '                                dNewRow("Column10") = mintTot
    '                                dNewRow("Column81") = mintTot


    '                                dNewRow("Column11") = mstrGroupId
    '                                dNewRow("Column12") = mstrGroupName
    '                                dtDetailsExport.Rows.Add(dNewRow)
    '                            End If

    '                        ElseIf mintViewIndex <= 0 And idx = dsListSummary.Tables(0).Rows.Count - 1 Then
    '                            GoTo ForAddRow
    '                        End If

    '                    End If
    '                    mstrGroupId = dtSummary.Item("Id")
    '                    mstrGroupName = dtSummary.Item("GName")
    '                    strPrevGrpId = dtSummary.Item("Id").ToString
    '                    idx += 1
    '                    'Pinkal (24-Jan-2013) -- End


    '                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_SummaryRow)
    '                Next
    '            End If


    '            objRpt.Subreports("rptSubSummary").SetDataSource(rpt_Data)

    '            Call ReportFunction.TextChange(objRpt.Subreports("rptSubSummary"), "txtBankPayment", Language.getMessage(mstrModuleName, 35, "Bank Payment"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptSubSummary"), "txtCashPayment", Language.getMessage(mstrModuleName, 36, "Cash Payment"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptSubSummary"), "txtSalaryonHold", Language.getMessage(mstrModuleName, 37, "Salary On Hold"))
    '            Call ReportFunction.TextChange(objRpt.Subreports("rptSubSummary"), "txtNoOfEmployees", Language.getMessage(mstrModuleName, 38, "No of Employees"))

    '            'Vimal (30 Nov 2010) -- End


    '            Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 15, "Period Name"))
    '            Call ReportFunction.TextChange(objRpt, "txtPeriodName", mstrPeriodName)

    '            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 16, "Print Date :"))
    '            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 17, "Printed By :"))
    '            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 18, "Page :"))


    '            'Vimal (15 Dec 2010) -- Start 
    '            'Call ReportFunction.TextChange(objRpt, "txtGrossPay", Language.getMessage(mstrModuleName, 19, "Gross Pay"))
    '            'Call ReportFunction.TextChange(objRpt, "txtTotalDeduction", Language.getMessage(mstrModuleName, 20, "Total Deduction"))

    '            'Sohail (05 Jul 2011) -- Start
    '            'objRpt.DataDefinition.FormulaFields("frmGrossCaption").Text = """" & Language.getMessage(mstrModuleName, 19, "Gross Pay") & """"
    '            Dim dRow() As DataRow = dsList.Tables("DataTable").Select("HeadTypeId = 2")
    '            If dRow.Length > 0 Then
    '                objRpt.DataDefinition.FormulaFields("frmGrossCaption").Text = """" & Language.getMessage(mstrModuleName, 19, "Gross Pay") & """"
    '            Else
    '                objRpt.DataDefinition.FormulaFields("frmGrossCaption").Text = """" & Language.getMessage(mstrModuleName, 21, "Net Pay") & """"
    '            End If
    '            'Sohail (05 Jul 2011) -- End
    '            objRpt.DataDefinition.FormulaFields("frmDeductionCaption").Text = """" & Language.getMessage(mstrModuleName, 20, "Total Deduction") & """"
    '            'Vimal (15 Dec 2010) -- End

    '            Call ReportFunction.TextChange(objRpt, "txtHeadCode", Language.getMessage(mstrModuleName, 29, "Head Code"))
    '            Call ReportFunction.TextChange(objRpt, "txtDescription", Language.getMessage(mstrModuleName, 30, "Description"))
    '            Call ReportFunction.TextChange(objRpt, "txtEmpCount", Language.getMessage(mstrModuleName, 31, "Emp. Count"))
    '            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 32, "Amount"))

    '            Call ReportFunction.TextChange(objRpt, "txtNetPay", Language.getMessage(mstrModuleName, 21, "Net Pay"))
    '            'Sohail (20 Feb 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            Call ReportFunction.TextChange(objRpt, "txtNetBF", Language.getMessage(mstrModuleName, 41, "Net B/F"))
    '            Call ReportFunction.TextChange(objRpt, "txtTotalNetPay", Language.getMessage(mstrModuleName, 42, "Total Net Pay"))
    '            'Sohail (20 Feb 2013) -- End

    '            Call ReportFunction.TextChange(objRpt, "txtMessages", Language.getMessage(mstrModuleName, 22, "Messages"))
    '            Call ReportFunction.TextChange(objRpt, "txtPaymentDetails", Language.getMessage(mstrModuleName, 23, "Payment Details"))

    '            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

    '            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
    '            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '            Dim dsListMsg As New DataSet
    '            Dim objMessage As New clsPayslipMessages_master

    '            'S.SANDEEP [04 JUN 2015] -- START
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'dsListMsg = objMessage.GetList("Messages", True)
    '            dsListMsg = objMessage.GetList(FinancialYear._Object._DatabaseName, _
    '                                           User._Object._Userunkid, _
    '                                           FinancialYear._Object._YearUnkid, _
    '                                           Company._Object._Companyunkid, _
    '                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                           eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
    '                                           ConfigParameter._Object._UserAccessModeSetting, True, _
    '                                           ConfigParameter._Object._IsIncludeInactiveEmp, "Messages", True)
    '            'S.SANDEEP [04 JUN 2015] -- END


    '            If dsListMsg.Tables("Messages").Rows.Count > 0 Then
    '                Call ReportFunction.TextChange(objRpt, "txtMsg1", dsListMsg.Tables("Messages").Rows(0).Item("msg1").ToString)
    '                Call ReportFunction.TextChange(objRpt, "txtMsg2", dsListMsg.Tables("Messages").Rows(0).Item("msg2").ToString)
    '                Call ReportFunction.TextChange(objRpt, "txtMsg3", dsListMsg.Tables("Messages").Rows(0).Item("msg3").ToString)
    '                Call ReportFunction.TextChange(objRpt, "txtMsg4", dsListMsg.Tables("Messages").Rows(0).Item("msg4").ToString)

    '            Else
    '                Call ReportFunction.TextChange(objRpt, "txtMsg1", "")
    '                Call ReportFunction.TextChange(objRpt, "txtMsg2", "")
    '                Call ReportFunction.TextChange(objRpt, "txtMsg3", "")
    '                Call ReportFunction.TextChange(objRpt, "txtMsg4", "")

    '            End If


    '            'Vimal (30 Nov 2010) -- Start 
    '            Call ReportFunction.SetRptDecimal(objRpt, "frmlCol41")
    '            'Vimal (15 Dec 2010) -- Start 
    '            'Call ReportFunction.SetRptDecimal(objRpt, "SumofEAR1")
    '            'Call ReportFunction.SetRptDecimal(objRpt, "totDeduction1")
    '            Call ReportFunction.SetRptDecimal(objRpt, "SumoffrmlCol41")
    '            'Vimal (15 Dec 2010) -- End
    '            Call ReportFunction.SetRptDecimal(objRpt, "totNetPay1")

    '            Call ReportFunction.SetRptDecimal(objRpt.Subreports("rptSubSummary"), "totAmount1")
    '            'Vimal (30 Nov 2010) -- End

    '            'Sohail (12 May 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            If mintViewIndex > 0 Then
    '                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", False)
    '                'Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", False)
    '            Else
    '                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
    '                'Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection2", True)
    '            End If
    '            'Sohail (12 May 2012) -- End




    '            'Pinkal (21-Jan-2013) -- Start
    '            'Enhancement : TRA Changes

    '            If menExportAction = enExportAction.ExcelExtra Then

    '                mdtTableExcel = New DataView(dtDetailsExport, "", "Column12,Column5", DataViewRowState.CurrentRows).ToTable

    '                mdtTableExcel.Columns.RemoveAt(0)

    '                Dim mintColumn As Integer = 0

    '                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 29, "Head Code")
    '                mdtTableExcel.Columns("Column2").SetOrdinal(0)
    '                mintColumn += 1

    '                mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 30, "Description")
    '                mdtTableExcel.Columns("Column3").SetOrdinal(1)
    '                mintColumn += 1

    '                mdtTableExcel.Columns("Column10").Caption = Language.getMessage(mstrModuleName, 31, "Emp. Count")
    '                mdtTableExcel.Columns("Column10").SetOrdinal(2)
    '                mintColumn += 1

    '                mdtTableExcel.Columns("Column82").Caption = Language.getMessage(mstrModuleName, 32, "Amount")
    '                mdtTableExcel.Columns("Column82").SetOrdinal(3)
    '                mintColumn += 1

    '                mdtTableExcel.Columns("Column6").Caption = ""
    '                mdtTableExcel.Columns("Column6").SetOrdinal(4)
    '                mintColumn += 1

    '                If mintViewIndex > 0 Then
    '                    mdtTableExcel.Columns("Column12").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Replace(":", "").Trim, mstrReport_GroupName)
    '                    mdtTableExcel.Columns("Column12").SetOrdinal(5)
    '                    mintColumn += 1
    '                End If

    '                For i = mintColumn To mdtTableExcel.Columns.Count - 1
    '                    mdtTableExcel.Columns.RemoveAt(mintColumn)
    '                Next

    '            End If


    '            'Pinkal (21-Jan-2013) -- End


    '            Return objRpt
    '        Catch ex As Exception
    '            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
    '            Return Nothing
    '        End Try
    '    End Function

    Private Function Generate_DetailReport(ByVal strDatabaseName As String, _
                                           ByVal intUserUnkid As Integer, _
                                           ByVal intYearUnkid As Integer, _
                                           ByVal intCompanyUnkid As Integer, _
                                           ByVal dtPeriodStart As Date, _
                                           ByVal dtPeriodEnd As Date, _
                                           ByVal strUserModeSetting As String, _
                                           ByVal blnOnlyApproved As Boolean, _
                                           ByVal intBaseCurrencyId As Integer, _
                                           ByVal strCompanyName As String, _
                                           ByVal strUserName As String, _
                                           ByVal blnSetPayslipPaymentApproval As Boolean, _
                                           ByVal blnShowPaymentApprovalDetailsOnStatutoryReport As Boolean _
                                           ) As CrystalDecisions.CrystalReports.Engine.ReportClass
        'Sohail (11 Mar 2016) - [blnSetPayslipPaymentApproval, blnShowPaymentApprovalDetailsOnStatutoryReport]
        'Sohail (21 Jan 2016) - [strCompanyName, strUserName]

        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Dim dsListSummary As New DataSet
        Dim dtDetailsExport As DataTable = Nothing
        Try
            objDataOperation = New clsDataOperation
            Dim objExchangeRate As New clsExchangeRate
            Dim decDecimalPlaces As Decimal = 0
            objExchangeRate._ExchangeRateunkid = intBaseCurrencyId
            decDecimalPlaces = objExchangeRate._Digits_After_Decimal
            'Sohail (20 Apr 2017) -- Start
            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
            Dim intBaseCountryUnkId As Integer = objExchangeRate._Countryunkid
            'Sohail (20 Apr 2017) -- End

            'Sohail (01 Feb 2017) -- Start
            'TRA Issue - 64.1 - JV Dr and Cr balance were not matching due to round off function used with default 2 decimals and jv total was not matching with payroll summary gross pay.
            'Sohail (29 Nov 2017) -- Start
            'SUMATRA – issue # 0001666: Discrepancy in various reports in 70.1.
            'decDecimalPlaces = 6
            'Sohail (29 Nov 2017) -- End
            'Sohail (01 Feb 2017) -- End
            'Sohail (03 Nov 2021) -- Start
            'HJIF Issue : HJIF-AH-4234 : TRANSACTION HEADS PROBLEM NSSF (Bring difference of 0.003 when you compare with 10% of gross and total of NSSF).
            decDecimalPlaces = 6
            'Sohail (03 Nov 2021) -- End

            If mdtPeriodStartDate <> Nothing Then dtPeriodStart = mdtPeriodStartDate
            If mdtPeriodEndDate <> Nothing Then dtPeriodEnd = mdtPeriodEndDate
            'Sohail (11 Mar 2016) -- Start
            'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
            mblnSetPayslipPaymentApproval = blnSetPayslipPaymentApproval
            mblnShowPaymentApprovalDetailsOnStatutoryReport = blnShowPaymentApprovalDetailsOnStatutoryReport
            'Sohail (11 Mar 2016) -- End

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            'Sohail (18 Feb 2016) -- Start
            'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
            StrQ = "CREATE TABLE #cteEmp " & _
                    "( " & _
                      "employeeunkid INT , " & _
                      "Id INT , " & _
                      "GName VARCHAR(MAX) " & _
                    ") " & _
                    "INSERT INTO #cteEmp " & _
                    "SELECT hremployee_master.employeeunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM hremployee_master "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE 1 = 1 "

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If
            'Sohail (18 Feb 2016) -- End


            StrQ &= "SELECT " & _
                   "     PeriodId " & _
                   "    ,TranCode " & _
                   "    ,TranName " & _
                   "    ,COUNT(DISTINCT employeeunkid) AS HeadCount " & _
                   "    ,CASE HeadTypeId WHEN 3 THEN CASE ismonetary " & _
                   "                     WHEN 1 THEN ( SUM(CAST(ISNULL(Amount, 0) * " & mdecEx_Rate & " AS DECIMAL(36, " & decDecimalPlaces & ")) ) ) " & _
                   "        ELSE SUM(Amount) * 1.0000 END ELSE ( SUM(CAST(ISNULL(Amount, 0) * " & mdecEx_Rate & " AS DECIMAL(36, " & decDecimalPlaces & ")) ) ) " & _
                   "        END AS Amount " & _
                   "    ,HeadTypeId " & _
                   "    ,HeadTypeName " & _
                   "    ,TypeOfId " & _
                   "    ,Id " & _
                   "    ,GName " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         TableHead.periodid AS PeriodId " & _
                   "        ,ISNULL(TableHead.TranCode, ' ') AS TranCode " & _
                   "        ,ISNULL(TableHead.TranName, ' ') AS TranName " & _
                   "        ,CAST(ISNULL(TableHead.Amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                   "        ,TableHead.headtypeid AS HeadTypeId " & _
                   "        ,TableHead.HeadTypeName AS HeadTypeName " & _
                   "        ,TableHead.typeofid AS TypeOfId " & _
                   "        ,TableHead.ismonetary " & _
                   "        ,TableHead.employeeunkid " & _
                   "        ,TableHead.Id " & _
                   "        ,TableHead.GName " & _
                   "    FROM " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             prtnaleave_tran.payperiodunkid AS periodid " & _
                   "            ,prtranhead_master.tranheadunkid AS tranunkid " & _
                   "            ,ISNULL(prtranhead_master.trnheadcode, ' ') AS TranCode " & _
                   "            ,ISNULL(prtranhead_master.trnheadname, ' ') AS TranName " & _
                   "            ,CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                   "            ,prtranhead_master.trnheadtype_id AS headtypeid " & _
                   "            ,prtranhead_master.typeof_id AS typeofid " & _
                   "            ,@Earnings AS HeadTypeName " & _
                   "            ,ismonetary " & _
                   "            ,prpayrollprocess_tran.employeeunkid "
            'Sohail (27 Apr 2021) - [CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))] = [ISNULL(prpayrollprocess_tran.amount, 0)]
            'Sohail (30 Oct 2018) - [,CASE HeadTypeId WHEN 3 THEN CASE ismonetary WHEN 1 THEN ( SUM(CAST(ISNULL(Amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) ) ) ELSE SUM(Amount) * 1.0000 END ELSE ( SUM(CAST(ISNULL(Amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) ) ) END AS Amount] = 
            '                       [,CAST(ISNULL(TableHead.Amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) * " & mdecEx_Rate & " AS Amount] = []

            If mintViewIndex > 0 Then
                'Sohail (18 Feb 2016) -- Start
                'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
                'StrQ &= mstrAnalysis_Fields
                StrQ &= ", #cteEmp.Id, #cteEmp.GName "
                'Sohail (18 Feb 2016) -- End
            Else
                StrQ &= ",0 AS Id, '' AS GName "
            End If


            StrQ &= "FROM  prpayrollprocess_tran " & _
                    "   LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                    "   LEFT JOIN #cteEmp ON prpayrollprocess_tran.employeeunkid = #cteEmp.employeeunkid " & _
                    "   LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid "
            'Sohail (18 Feb 2016) - [JOIN #cteEmp ON hremployee_master.employeeunkid = #cteEmp.employeeunkid]
            'Removed : "   LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _

            'Sohail (18 Feb 2016) -- Start
            'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Join
            'End If

            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Sohail (18 Feb 2016) -- End

            StrQ &= "WHERE prtranhead_master.trnheadtype_id = " & enTranHeadType.EarningForEmployees & " AND prpayrollprocess_tran.isvoid = 0 " & _
                    "AND prtnaleave_tran.isvoid = 0 AND prtranhead_master.isvoid = 0 AND prtnaleave_tran.payperiodunkid = @Period " & _
                    "AND #cteEmp.employeeunkid IS NOT NULL "
            'Sohail (04 Mar 2021) - [AND prtnaleave_tran.isvoid = 0]
            'Sohail (18 Feb 2016) - [AND #cteEmp.employeeunkid IS NOT NULL ]

            'Sohail (18 Feb 2016) -- Start
            'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
            'If mblnIsActive = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry
            '    End If
            'End If

            'If mblnIgnorezeroHeads Then
            '    StrQ &= " AND Amount  <> 0 "
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            If mblnIgnorezeroHeads Then
                StrQ &= " AND Amount  <> 0 "
            End If
            'Sohail (18 Feb 2016) -- End

            StrQ &= "UNION ALL " & _
                    "   SELECT " & _
                    "        prtnaleave_tran.payperiodunkid AS periodid " & _
                    "       ,prtranhead_master.tranheadunkid AS tranunkid " & _
                    "       ,ISNULL(prtranhead_master.trnheadcode, ' ') AS TranCode " & _
                    "       ,ISNULL(prtranhead_master.trnheadname, ' ') AS TranName " & _
                    "       ,CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                    "       ,CASE WHEN prtranhead_master.trnheadtype_id = " & enTranHeadType.EmployeesStatutoryDeductions & " THEN 2 ELSE prtranhead_master.trnheadtype_id END AS headtypeid " & _
                    "       ,prtranhead_master.typeof_id AS typeofid " & _
                    "       ,@Deductions AS HeadTypeName " & _
                    "       ,ismonetary " & _
                    "       ,prpayrollprocess_tran.employeeunkid "
            'Sohail (27 Apr 2021) - [CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))] = [ISNULL(prpayrollprocess_tran.amount, 0)]

            If mintViewIndex > 0 Then
                'Sohail (18 Feb 2016) -- Start
                'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
                'StrQ &= mstrAnalysis_Fields
                StrQ &= ", #cteEmp.Id, #cteEmp.GName "
                'Sohail (18 Feb 2016) -- End
            Else
                StrQ &= ",0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM prpayrollprocess_tran " & _
                    "   LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                    "   LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                    "   LEFT JOIN #cteEmp ON prpayrollprocess_tran.employeeunkid = #cteEmp.employeeunkid "
            'Sohail (18 Feb 2016) - [JOIN #cteEmp ON hremployee_master.employeeunkid = #cteEmp.employeeunkid]
            'Sohail (18 Feb 2016) - Removed :  "   LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _

            'Sohail (18 Feb 2016) -- Start
            'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Join
            'End If

            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Sohail (18 Feb 2016) -- End

            StrQ &= "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                    "   AND prtnaleave_tran.isvoid = 0 " & _
                    "   AND prtranhead_master.isvoid = 0 " & _
                    "   AND ( prtranhead_master.trnheadtype_id = " & enTranHeadType.DeductionForEmployee & " OR prtranhead_master.trnheadtype_id = " & enTranHeadType.EmployeesStatutoryDeductions & ") " & _
                    "AND prtnaleave_tran.payperiodunkid = @Period " & _
                    "AND #cteEmp.employeeunkid IS NOT NULL "
            'Sohail (04 Mar 2021) - [AND prtnaleave_tran.isvoid = 0]
            'Sohail (18 Feb 2016) - [AND #cteEmp.employeeunkid IS NOT NULL ]

            'Sohail (18 Feb 2016) -- Start
            'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
            'If mblnIsActive = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry
            '    End If
            'End If

            'If mblnIgnorezeroHeads Then
            '    StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            If mblnIgnorezeroHeads Then
                StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
            End If
            'Sohail (18 Feb 2016) -- End

            StrQ &= "UNION ALL " & _
                    "   SELECT " & _
                    "        prtnaleave_tran.payperiodunkid AS periodid " & _
                    "       ,practivity_master.activityunkid AS tranunkid " & _
                    "       ,ISNULL(practivity_master.code, ' ') AS TranCode " & _
                    "       ,ISNULL(practivity_master.name, ' ') AS TranName " & _
                    "       ,CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                    "       ,practivity_master.trnheadtype_id AS headtypeid " & _
                    "       ,CASE practivity_master.trnheadtype_id WHEN " & enTranHeadType.EarningForEmployees & " THEN " & enTypeOf.Other_Earnings & " WHEN " & enTranHeadType.DeductionForEmployee & " THEN " & enTypeOf.Other_Deductions_Emp & " END AS typeofid " & _
                    "       ,CASE practivity_master.trnheadtype_id WHEN " & enTranHeadType.EarningForEmployees & " THEN @Earnings  WHEN " & enTranHeadType.DeductionForEmployee & " THEN @Deductions END AS HeadTypeName " & _
                    "       ,1 AS ismonetary " & _
                    "       ,prpayrollprocess_tran.employeeunkid "
            'Sohail (27 Apr 2021) - [CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))] = [ISNULL(prpayrollprocess_tran.amount, 0)]

            If mintViewIndex > 0 Then
                'Sohail (18 Feb 2016) -- Start
                'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
                'StrQ &= mstrAnalysis_Fields
                StrQ &= ", #cteEmp.Id, #cteEmp.GName "
                'Sohail (18 Feb 2016) -- End
            Else
                StrQ &= ",0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM prpayrollprocess_tran " & _
                    "   LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                    "   LEFT JOIN practivity_master ON prpayrollprocess_tran.activityunkid = practivity_master.activityunkid " & _
                    "   LEFT JOIN #cteEmp ON prpayrollprocess_tran.employeeunkid = #cteEmp.employeeunkid "
            'Sohail (18 Feb 2016) - [JOIN #cteEmp ON hremployee_master.employeeunkid = #cteEmp.employeeunkid]
            'Sohail (18 Feb 2016) - Removed : "   LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _

            'Sohail (18 Feb 2016) -- Start
            'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Join
            'End If

            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Sohail (18 Feb 2016) -- End

            StrQ &= "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                    "   AND prtnaleave_tran.isvoid = 0 " & _
                    "   AND practivity_master.isactive = 1 " & _
                    "   AND practivity_master.activityunkid > 0 " & _
                    "   AND practivity_master.trnheadtype_id IN (" & enTranHeadType.EarningForEmployees & ", " & enTranHeadType.DeductionForEmployee & ") " & _
                    "   AND prtnaleave_tran.payperiodunkid = @Period " & _
                    "AND #cteEmp.employeeunkid IS NOT NULL "
            'Sohail (18 Feb 2016) - [AND #cteEmp.employeeunkid IS NOT NULL ]

            'Sohail (18 Feb 2016) -- Start
            'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
            'If mblnIsActive = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry
            '    End If
            'End If

            'If mblnIgnorezeroHeads Then
            '    StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            If mblnIgnorezeroHeads Then
                StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
            End If
            'Sohail (18 Feb 2016) -- End

            StrQ &= "UNION ALL " & _
                    "SELECT " & _
                    "    prtnaleave_tran.payperiodunkid AS periodid " & _
                    "   ,ISNULL(cmexpense_master.expenseunkid, crretireexpense.expenseunkid) AS tranunkid " & _
                    "   ,ISNULL(cmexpense_master.code, ISNULL(crretireexpense.code, ' ')) AS TranCode " & _
                    "   ,ISNULL(cmexpense_master.name, ISNULL(crretireexpense.name, ' ')) AS TranName " & _
                    "   ,CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                    "   ,CASE WHEN cmexpense_master.expenseunkid > 0 THEN CASE cmexpense_master.trnheadtype_id WHEN 5 THEN 3 ELSE cmexpense_master.trnheadtype_id END ELSE CASE WHEN prpayrollprocess_tran.add_deduct = 1 THEN 1 ELSE 2 END END AS headtypeid " & _
                    "   ,CASE WHEN cmexpense_master.expenseunkid > 0 THEN CASE cmexpense_master.trnheadtype_id WHEN " & enTranHeadType.EarningForEmployees & " THEN " & enTypeOf.Other_Earnings & " WHEN " & enTranHeadType.DeductionForEmployee & " THEN " & enTypeOf.Other_Deductions_Emp & " WHEN " & enTranHeadType.Informational & " THEN " & enTypeOf.Informational & " END ELSE CASE WHEN prpayrollprocess_tran.add_deduct = 1 THEN " & enTypeOf.Other_Earnings & " ELSE " & enTypeOf.Other_Deductions_Emp & " END END AS typeofid " & _
                    "   ,CASE WHEN cmexpense_master.expenseunkid > 0 THEN CASE cmexpense_master.trnheadtype_id WHEN " & enTranHeadType.EarningForEmployees & " THEN @Earnings  WHEN " & enTranHeadType.DeductionForEmployee & " THEN @Deductions WHEN " & enTranHeadType.Informational & " THEN @Informational END ELSE CASE WHEN prpayrollprocess_tran.add_deduct = 1 THEN @Earnings ELSE @Deductions END END AS HeadTypeName " & _
                    "   ,1 AS ismonetary " & _
                    "   ,prpayrollprocess_tran.employeeunkid "
            'Sohail (09 Jun 2021) - [crretirementprocessunkid]
            'Sohail (27 Apr 2021) - [CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))] = [ISNULL(prpayrollprocess_tran.amount, 0)]

            If mintViewIndex > 0 Then
                'Sohail (18 Feb 2016) -- Start
                'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
                'StrQ &= mstrAnalysis_Fields
                StrQ &= ", #cteEmp.Id, #cteEmp.GName "
                'Sohail (18 Feb 2016) -- End
            Else
                StrQ &= ",0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM prpayrollprocess_tran " & _
                    "   LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                    "   LEFT JOIN cmclaim_process_tran ON cmclaim_process_tran.crprocesstranunkid = prpayrollprocess_tran.crprocesstranunkid " & _
                    "   LEFT JOIN cmexpense_master ON cmclaim_process_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                    "   LEFT JOIN cmclaim_request_master ON cmclaim_process_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                    "   LEFT JOIN cmretire_process_tran ON cmretire_process_tran.crretirementprocessunkid = prpayrollprocess_tran.crretirementprocessunkid " & _
                    "   LEFT JOIN cmexpense_master AS crretireexpense ON cmretire_process_tran.expenseunkid = crretireexpense.expenseunkid " & _
                    "   LEFT JOIN cmclaim_retirement_master ON cmretire_process_tran.claimretirementunkid = cmclaim_retirement_master.claimretirementunkid " & _
                    "   LEFT JOIN #cteEmp ON prpayrollprocess_tran.employeeunkid = #cteEmp.employeeunkid "
            'Sohail (09 Jun 2021) - [crretirementprocessunkid]
            'Sohail (18 Feb 2016) - [JOIN #cteEmp ON hremployee_master.employeeunkid = #cteEmp.employeeunkid]
            'Removed : "   LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _

            'Sohail (18 Feb 2016) -- Start
            'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Join
            'End If

            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Sohail (18 Feb 2016) -- End

            StrQ &= "WHERE prpayrollprocess_tran.isvoid = 0 " & _
                    "   AND prtnaleave_tran.isvoid = 0 " & _
                    "   AND ISNULL(cmclaim_process_tran.isvoid, 0) = 0 " & _
                    "   AND ISNULL(cmclaim_request_master.isvoid, 0) = 0 " & _
                    "   AND ISNULL(cmexpense_master.isactive, 1) = 1 " & _
                    "   AND ISNULL(cmretire_process_tran.isvoid, 0) = 0 " & _
                    "   AND ISNULL(cmclaim_retirement_master.isvoid, 0) = 0 " & _
                    "   AND ISNULL(crretireexpense.isactive, 1) = 1 " & _
                    "   AND (prpayrollprocess_tran.crprocesstranunkid > 0 OR prpayrollprocess_tran.crretirementprocessunkid > 0) " & _
                    "   AND prtnaleave_tran.payperiodunkid = @Period " & _
                    "AND #cteEmp.employeeunkid IS NOT NULL "
            'Sohail (18 Feb 2016) - [AND #cteEmp.employeeunkid IS NOT NULL ]

            'Sohail (23 Sep 2016) -- Start
            'MWAUWSA Enhancement -  63.1 - Show Employer Contribution and Show Informational Heads options on Payroll Summary.
            If mblnShowInformational = False Then
                StrQ &= " AND ISNULL(cmexpense_master.trnheadtype_id, 0) <> " & enTranHeadType.Informational & " "
            End If
            'Sohail (23 Sep 2016) -- End

            'Sohail (18 Feb 2016) -- Start
            'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
            'If mblnIsActive = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry
            '    End If
            'End If

            'If mblnIgnorezeroHeads Then
            '    StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            If mblnIgnorezeroHeads Then
                StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
            End If
            'Sohail (18 Feb 2016) -- End

            If mblnShowInformational = True Then 'Sohail (23 Sep 2016)

                StrQ &= "UNION ALL " & _
                        "   SELECT " & _
                        "        prtnaleave_tran.payperiodunkid AS periodid " & _
                        "       ,prtranhead_master.tranheadunkid AS tranunkid " & _
                        "       ,ISNULL(prtranhead_master.trnheadcode, ' ') AS TranCode " & _
                        "       ,ISNULL(prtranhead_master.trnheadname, ' ') AS TranName " & _
                        "       ,CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                            "       ,CASE WHEN prtranhead_master.trnheadtype_id = " & enTranHeadType.Informational & " THEN 3 ELSE prtranhead_master.trnheadtype_id END AS headtypeid " & _
                        "       ,prtranhead_master.typeof_id AS typeofid " & _
                        "       ,@Informational AS HeadTypeName " & _
                        "       ,ismonetary " & _
                        "       ,prpayrollprocess_tran.employeeunkid "
                'Sohail (27 Apr 2021) - [CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))] = [ISNULL(prpayrollprocess_tran.amount, 0)]

                If mintViewIndex > 0 Then
                    'Sohail (18 Feb 2016) -- Start
                    'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
                    'StrQ &= mstrAnalysis_Fields
                    StrQ &= ", #cteEmp.Id, #cteEmp.GName "
                    'Sohail (18 Feb 2016) -- End
                Else
                    StrQ &= ",0 AS Id, '' AS GName "
                End If

                StrQ &= "FROM prpayrollprocess_tran " & _
                        "   LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "   LEFT JOIN #cteEmp ON prpayrollprocess_tran.employeeunkid = #cteEmp.employeeunkid "
                'Sohail (18 Feb 2016) - [JOIN #cteEmp ON hremployee_master.employeeunkid = #cteEmp.employeeunkid]
                'Removed : "   LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _

                'Sohail (18 Feb 2016) -- Start
                'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
                'If mintViewIndex > 0 Then
                '    StrQ &= mstrAnalysis_Join
                'End If

                'If xDateJoinQry.Trim.Length > 0 Then
                '    StrQ &= xDateJoinQry
                'End If

                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If

                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If
                'Sohail (18 Feb 2016) -- End

                StrQ &= "WHERE prtranhead_master.trnheadtype_id = " & enTranHeadType.Informational & " " & _
                    "AND prpayrollprocess_tran.isvoid = 0 " & _
                    "AND prtnaleave_tran.isvoid = 0 " & _
                    "AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                    "AND prtranhead_master.isappearonpayslip = 1 " & _
                    "AND prtnaleave_tran.payperiodunkid = @Period " & _
                    "AND #cteEmp.employeeunkid IS NOT NULL "
                'Sohail (18 Feb 2016) - [AND #cteEmp.employeeunkid IS NOT NULL ]

                'Sohail (18 Feb 2016) -- Start
                'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
                'If mblnIsActive = False Then
                '    If xDateFilterQry.Trim.Length > 0 Then
                '        StrQ &= xDateFilterQry
                '    End If
                'End If

                'If mblnIgnorezeroHeads Then
                '    StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
                'End If

                'If xUACFiltrQry.Trim.Length > 0 Then
                '    StrQ &= " AND " & xUACFiltrQry
                'End If
                If mblnIgnorezeroHeads Then
                    StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
                End If
                'Sohail (18 Feb 2016) -- End

                'Sohail (30 Dec 2019) -- Start
                'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
                If mstrInfoHeadIDs.Trim <> "" Then
                    StrQ &= " AND prtranhead_master.tranheadunkid IN (" & mstrInfoHeadIDs & ") "
                End If
                'Sohail (30 Dec 2019) -- End

                StrQ &= " UNION ALL " & _
                        "       SELECT " & _
                        "            prtnaleave_tran.payperiodunkid AS periodid " & _
                        "           ,practivity_master.activityunkid AS tranunkid " & _
                        "           ,ISNULL(practivity_master.code, ' ') AS TranCode " & _
                        "           ,ISNULL(practivity_master.name, ' ') AS TranName " & _
                        "           ,CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                        "           ,CASE WHEN practivity_master.trnheadtype_id = 5 THEN 3 ELSE practivity_master.trnheadtype_id END AS headtypeid " & _
                        "           ," & enTypeOf.Informational & " AS typeofid " & _
                        "           ,@Informational AS HeadTypeName " & _
                        "           ,1 AS ismonetary " & _
                        "           ,prpayrollprocess_tran.employeeunkid "
                'Sohail (27 Apr 2021) - [CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))] = [ISNULL(prpayrollprocess_tran.amount, 0)]

                If mintViewIndex > 0 Then
                    'Sohail (18 Feb 2016) -- Start
                    'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
                    'StrQ &= mstrAnalysis_Fields
                    StrQ &= ", #cteEmp.Id, #cteEmp.GName "
                    'Sohail (18 Feb 2016) -- End
                Else
                    StrQ &= ",0 AS Id, '' AS GName "
                End If

                StrQ &= "FROM prpayrollprocess_tran " & _
                        "   LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   LEFT JOIN practivity_master ON practivity_master.activityunkid =  prpayrollprocess_tran.activityunkid " & _
                        "   LEFT JOIN #cteEmp ON prpayrollprocess_tran.employeeunkid = #cteEmp.employeeunkid "
                'Sohail (18 Feb 2016) - [JOIN #cteEmp ON hremployee_master.employeeunkid = #cteEmp.employeeunkid]
                'Removed : "   LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _

                'Sohail (18 Feb 2016) -- Start
                'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
                'If mintViewIndex > 0 Then
                '    StrQ &= mstrAnalysis_Join
                'End If

                'If xDateJoinQry.Trim.Length > 0 Then
                '    StrQ &= xDateJoinQry
                'End If

                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If

                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If
                'Sohail (18 Feb 2016) -- End

                StrQ &= "WHERE prpayrollprocess_tran.activityunkid > 0 " & _
                        "   AND prpayrollprocess_tran.isvoid = 0 " & _
                        "   AND prtnaleave_tran.isvoid = 0 " & _
                        "   AND practivity_master.trnheadtype_id IN (" & enTranHeadType.Informational & ") " & _
                        "   AND prtnaleave_tran.payperiodunkid = @Period " & _
                        "AND #cteEmp.employeeunkid IS NOT NULL "
                'Sohail (18 Feb 2016) - [AND #cteEmp.employeeunkid IS NOT NULL ]

                'Sohail (18 Feb 2016) -- Start
                'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
                'If mblnIsActive = False Then
                '    If xDateFilterQry.Trim.Length > 0 Then
                '        StrQ &= xDateFilterQry
                '    End If
                'End If

                'If mblnIgnorezeroHeads Then
                '    StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
                'End If

                'If xUACFiltrQry.Trim.Length > 0 Then
                '    StrQ &= " AND " & xUACFiltrQry
                'End If
                If mblnIgnorezeroHeads Then
                    StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
                End If
                'Sohail (18 Feb 2016) -- End
            End If 'Sohail (23 Sep 2016)

            If mblnShowEmployerContribution = True Then 'Sohail (23 Sep 2016)

                StrQ &= "UNION ALL " & _
                        "   SELECT " & _
                        "        prtnaleave_tran.payperiodunkid AS periodid " & _
                        "       ,prtranhead_master.tranheadunkid AS tranunkid " & _
                        "       ,ISNULL(prtranhead_master.trnheadcode, ' ') AS TranCode " & _
                        "       ,ISNULL(prtranhead_master.trnheadname, ' ') AS TranName " & _
                        "       ,CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                        "       ,prtranhead_master.trnheadtype_id AS headtypeid " & _
                        "       ,prtranhead_master.typeof_id AS typeofid " & _
                        "       ,@EmployersContributions AS HeadTypeName " & _
                        "       ,ismonetary " & _
                        "       ,prpayrollprocess_tran.employeeunkid "
                'Sohail (27 Apr 2021) - [CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))] = [ISNULL(prpayrollprocess_tran.amount, 0)]

                If mintViewIndex > 0 Then
                    'Sohail (18 Feb 2016) -- Start
                    'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
                    'StrQ &= mstrAnalysis_Fields
                    StrQ &= ", #cteEmp.Id, #cteEmp.GName "
                    'Sohail (18 Feb 2016) -- End
                Else
                    StrQ &= ",0 AS Id, '' AS GName "
                End If

                StrQ &= "FROM prpayrollprocess_tran " & _
                        "   LEFT JOIN prtnaleave_tran ON prtnaleave_tran.tnaleavetranunkid = prpayrollprocess_tran.tnaleavetranunkid " & _
                        "   LEFT JOIN prtranhead_master ON prpayrollprocess_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                        "   LEFT JOIN #cteEmp ON prpayrollprocess_tran.employeeunkid = #cteEmp.employeeunkid "
                'Sohail (18 Feb 2016) - [JOIN #cteEmp ON hremployee_master.employeeunkid = #cteEmp.employeeunkid]
                'Removed : "   LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _

                'Sohail (18 Feb 2016) -- Start
                'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
                'If mintViewIndex > 0 Then
                '    StrQ &= mstrAnalysis_Join
                'End If

                'If xDateJoinQry.Trim.Length > 0 Then
                '    StrQ &= xDateJoinQry
                'End If

                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If

                'If xAdvanceJoinQry.Trim.Length > 0 Then
                '    StrQ &= xAdvanceJoinQry
                'End If
                'Sohail (18 Feb 2016) -- End

                StrQ &= "WHERE prtranhead_master.trnheadtype_id = " & enTranHeadType.EmployersStatutoryContributions & " " & _
                    "   AND prpayrollprocess_tran.isvoid = 0 " & _
                    "   AND prtnaleave_tran.isvoid = 0 " & _
                    "   AND ISNULL(prtranhead_master.isvoid, 0) = 0 " & _
                    "   AND prtnaleave_tran.payperiodunkid = @Period " & _
                    "AND #cteEmp.employeeunkid IS NOT NULL "
                'Sohail (18 Feb 2016) - [AND #cteEmp.employeeunkid IS NOT NULL ]

                'Sohail (18 Feb 2016) -- Start
                'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
                'If mblnIsActive = False Then
                '    If xDateFilterQry.Trim.Length > 0 Then
                '        StrQ &= xDateFilterQry
                '    End If
                'End If

                'If mblnIgnorezeroHeads Then
                '    StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
                'End If

                'If xUACFiltrQry.Trim.Length > 0 Then
                '    StrQ &= " AND " & xUACFiltrQry
                'End If
                If mblnIgnorezeroHeads Then
                    StrQ &= " AND prpayrollprocess_tran.Amount  <> 0 "
                End If
                'Sohail (18 Feb 2016) -- End
            End If 'Sohail (23 Sep 2016)

            'Sohail (22 Jan 2016) -- Start
            'Enhancement - Performance issue on payroll summary report.
            'StrQ &= "UNION ALL " & _
            '        "   SELECT " & _
            '        "        payperiodunkid AS periodid " & _
            '        "       ,'2' AS tranunkid " & _
            '        "       ,tranheadcode AS TranCode " & _
            '        "       ,trnheadname AS TranName " & _
            '        "       ,CAST(amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
            '        "       ,'2' AS headtypeid " & _
            '        "       ,CASE WHEN GroupID = " & enViewPayroll_Group.Loan & " THEN '14' " & _
            '        "             WHEN GroupID = " & enViewPayroll_Group.Advance & " THEN '14' " & _
            '        "             WHEN GroupID = " & enViewPayroll_Group.Saving & " THEN '15' " & _
            '        "        END AS typeofid " & _
            '        "       ,@Deductions AS HeadTypeName " & _
            '        "       ,1 ismonetary " & _
            '        "       ,vwPayroll.employeeunkid "
            StrQ &= "UNION ALL " & _
                    "   SELECT " & _
                    "        prtnaleave_tran.payperiodunkid AS periodid " & _
                    "       ,'2' AS tranunkid " & _
                    "       ,CASE WHEN prpayrollprocess_tran.loanadvancetranunkid > 0 THEN CASE WHEN lnloan_advance_tran.isloan = 1 THEN lnloan_scheme_master.code ELSE 'Advance' END WHEN prpayrollprocess_tran.savingtranunkid > 0 THEN svsavingscheme_master.savingschemecode END AS TranCode " & _
                    "       ,CASE WHEN prpayrollprocess_tran.loanadvancetranunkid > 0 THEN CASE WHEN lnloan_advance_tran.isloan = 1 THEN lnloan_scheme_master.name ELSE 'Advance' END WHEN prpayrollprocess_tran.savingtranunkid > 0 THEN svsavingscheme_master.savingschemename END AS TranName " & _
                    "       ,CAST(prpayrollprocess_tran.amount AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                    "       ,'2' AS headtypeid " & _
                    "       ,CASE WHEN prpayrollprocess_tran.loanadvancetranunkid > 0 THEN '14' WHEN prpayrollprocess_tran.savingtranunkid > 0 THEN '15' END AS typeofid " & _
                    "       ,@Deductions AS HeadTypeName " & _
                    "       ,1 ismonetary " & _
                    "       ,prpayrollprocess_tran.employeeunkid "
            'Sohail (27 Apr 2021) - [CAST(ISNULL(prpayrollprocess_tran.amount, 0) AS DECIMAL(36, " & decDecimalPlaces & "))] = [ISNULL(prpayrollprocess_tran.amount, 0)]
            'Sohail (22 Jan 2016) -- End

            If mintViewIndex > 0 Then
                'Sohail (18 Feb 2016) -- Start
                'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
                'StrQ &= mstrAnalysis_Fields
                StrQ &= ", #cteEmp.Id, #cteEmp.GName "
                'Sohail (18 Feb 2016) -- End
            Else
                StrQ &= ",0 AS Id, '' AS GName "
            End If

            'Sohail (22 Jan 2016) -- Start
            'Enhancement - Performance issue on payroll summary report.
            'StrQ &= "FROM vwPayroll " & _
            '        "   JOIN hremployee_master ON vwPayroll.employeeunkid = hremployee_master.employeeunkid "
            StrQ &= "FROM prpayrollprocess_tran " & _
                    "   LEFT JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                    "   LEFT JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = prpayrollprocess_tran.loanadvancetranunkid AND prpayrollprocess_tran.loanadvancetranunkid > 0 " & _
                    "   LEFT JOIN lnloan_scheme_master ON lnloan_advance_tran.loanschemeunkid = lnloan_scheme_master.loanschemeunkid " & _
                    "   LEFT JOIN svsaving_tran ON prpayrollprocess_tran.savingtranunkid = svsaving_tran.savingtranunkid AND prpayrollprocess_tran.savingtranunkid > 0	" & _
                    "   LEFT JOIN svsavingscheme_master ON svsaving_tran.savingschemeunkid = svsavingscheme_master.savingschemeunkid " & _
                    "   LEFT JOIN #cteEmp ON prpayrollprocess_tran.employeeunkid = #cteEmp.employeeunkid "
            'Sohail (18 Feb 2016) - [JOIN #cteEmp ON hremployee_master.employeeunkid = #cteEmp.employeeunkid]
            'Removed : "   LEFT JOIN hremployee_master ON prpayrollprocess_tran.employeeunkid = hremployee_master.employeeunkid " & _
            'Sohail (22 Jan 2016) -- End

            'Sohail (18 Feb 2016) -- Start
            'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Join
            'End If

            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Sohail (18 Feb 2016) -- End

            'Sohail (22 Jan 2016) -- Start
            'Enhancement - Performance issue on payroll summary report.
            StrQ &= "WHERE   prpayrollprocess_tran.isvoid = 0 " & _
                    "   AND prtnaleave_tran.isvoid = 0 " & _
                    "   AND (lnloan_advance_tran.isvoid = 0 OR svsaving_tran.isvoid = 0) " & _
                    "   AND prtnaleave_tran.payperiodunkid = @Period " & _
                    "AND #cteEmp.employeeunkid IS NOT NULL "
            'Sohail (18 Feb 2016) - [AND #cteEmp.employeeunkid IS NOT NULL ]
            'Sohail (22 Jan 2016) -- End

            'Sohail (18 Feb 2016) -- Start
            'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
            'If mblnIsActive = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry
            '    End If
            'End If

            'If mblnIgnorezeroHeads Then
            '    StrQ &= " AND Amount  <> 0 "
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            If mblnIgnorezeroHeads Then
                StrQ &= " AND Amount  <> 0 "
            End If
            'Sohail (18 Feb 2016) -- End

            StrQ &= "UNION ALL " & _
                    "SELECT    prtnaleave_tran.payperiodunkid AS periodid " & _
                    ", 0 AS tranunkid " & _
                    ", ' ' AS TranCode " & _
                    ", @OpeningBalance AS TranName " & _
                    ", CAST(ISNULL(prtnaleave_tran.openingbalance, 0) AS DECIMAL(36, " & decDecimalPlaces & ")) AS Amount " & _
                    ", 1 AS headtypeid " & _
                    ", -99 AS typeofid " & _
                    ", @OpeningBalance AS HeadTypeName " & _
                    ", 1 ismonetary " & _
                    ", prtnaleave_tran.employeeunkid "

            If mintViewIndex > 0 Then
                'Sohail (18 Feb 2016) -- Start
                'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
                'StrQ &= mstrAnalysis_Fields
                StrQ &= ", #cteEmp.Id, #cteEmp.GName "
                'Sohail (18 Feb 2016) -- End
            Else
                StrQ &= ",0 AS Id, '' AS GName "
            End If

            StrQ &= "FROM      prtnaleave_tran " & _
                                "LEFT JOIN #cteEmp ON prtnaleave_tran.employeeunkid = #cteEmp.employeeunkid "
            'Sohail (18 Feb 2016) - [JOIN #cteEmp ON hremployee_master.employeeunkid = #cteEmp.employeeunkid]
            'Removed : "JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _

            'Sohail (18 Feb 2016) -- Start
            'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Join
            'End If

            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Sohail (18 Feb 2016) -- End

            StrQ &= "WHERE     ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                               "AND prtnaleave_tran.payperiodunkid = @Period " & _
                    "AND #cteEmp.employeeunkid IS NOT NULL "
            'Sohail (18 Feb 2016) - [AND #cteEmp.employeeunkid IS NOT NULL ]

            'Sohail (18 Feb 2016) -- Start
            'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
            'If mblnIsActive = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry
            '    End If
            'End If

            'If mblnIgnorezeroHeads Then
            '    StrQ &= " AND openingbalance  <> 0 "
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            If mblnIgnorezeroHeads Then
                StrQ &= " AND openingbalance  <> 0 "
            End If
            'Sohail (18 Feb 2016) -- End

            StrQ &= "   ) AS TableHead " & _
                    ") AS CTA " & _
                    "GROUP BY PeriodId , GName , Id , TranCode , TranName " & _
                          ", HeadTypeId, HeadTypeName, TypeOfId, ismonetary "

            Call FilterTitleAndFilterQuery()

            StrQ &= Me._FilterQuery

            StrQ &= mstrOrderByQuery

            'Sohail (18 Feb 2016) -- Start
            'Enhancement - Performance Enhancement for Payroll Summary Report in 58.1 SP.
            StrQ &= " DROP TABLE #cteEmp "
            'Sohail (18 Feb 2016) -- End

            objDataOperation.AddParameter("@Earnings", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Earnings"))
            objDataOperation.AddParameter("@Deductions", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Deductions"))
            objDataOperation.AddParameter("@EmployersContributions", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Employers Contributions"))
            objDataOperation.AddParameter("@Informational", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Informational"))
            objDataOperation.AddParameter("@Loan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Loan"))
            objDataOperation.AddParameter("@Advance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Advance"))
            objDataOperation.AddParameter("@Saving", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 39, "Saving"))
            objDataOperation.AddParameter("@OpeningBalance", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 40, "OpeningBalance"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            Dim decEarnTotal As Decimal = 0
            Dim decDeducTotal As Decimal = 0
            Dim objDic As New Dictionary(Of Integer, Decimal)

            If dsList.Tables("DataTable").Select("HeadTypeId = " & enTranHeadType.EarningForEmployees & "").Length > 0 Then
                decEarnTotal = dsList.Tables("DataTable").Compute("SUM(Amount)", "HeadTypeId = " & enTranHeadType.EarningForEmployees & "")
            End If

            If dsList.Tables("DataTable").Select("HeadTypeId = " & enTranHeadType.DeductionForEmployee & "").Length > 0 Then
                decDeducTotal = dsList.Tables("DataTable").Compute("SUM(Amount)", "HeadTypeId = " & enTranHeadType.DeductionForEmployee & "")
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim decEarnSubTotal As Decimal = 0
            Dim decDeducSubTotal As Decimal = 0
            Dim strPrevGrpId As String = ""
            Dim decOpeningBalance As Decimal = 0

            Dim mstrHeadTypeId As String = ""

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows

                If strPrevGrpId <> dtRow.Item("Id").ToString Then
                    decEarnSubTotal = 0
                    decDeducSubTotal = 0
                    decOpeningBalance = 0
                End If

                If CInt(dtRow.Item("TypeOfId")) = -99 Then 'Opening Balance
                    decOpeningBalance = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
                    strPrevGrpId = dtRow.Item("Id").ToString
                    Continue For
                End If

                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow

                rpt_Rows.Item("Column1") = dtRow.Item("PeriodId")
                rpt_Rows.Item("Column2") = dtRow.Item("TranCode")
                rpt_Rows.Item("Column3") = dtRow.Item("TranName")
                rpt_Rows.Item("Column4") = Format(CDec(dtRow.Item("Amount")), GUI.fmtCurrency)
                rpt_Rows.Item("Column5") = dtRow.Item("HeadTypeId")
                rpt_Rows.Item("Column6") = dtRow.Item("HeadTypeName")
                rpt_Rows.Item("Column7") = dtRow.Item("TypeOfId")

                If CInt(dtRow.Item("HeadTypeId")) = enTranHeadType.EarningForEmployees Then
                    If strPrevGrpId <> dtRow.Item("Id").ToString Then
                        decEarnSubTotal = CDec(dtRow.Item("Amount"))
                    Else
                        decEarnSubTotal += CDec(dtRow.Item("Amount"))
                    End If
                    rpt_Rows.Item("Column8") = Format(decEarnSubTotal, GUI.fmtCurrency)

                    rpt_Rows.Item("Column83") = decEarnSubTotal

                ElseIf CInt(dtRow.Item("HeadTypeId")) = enTranHeadType.DeductionForEmployee Then
                    If strPrevGrpId <> dtRow.Item("Id").ToString Then
                        decDeducSubTotal = CDec(dtRow.Item("Amount"))
                    Else
                        decDeducSubTotal += CDec(dtRow.Item("Amount"))
                    End If
                    rpt_Rows.Item("Column8") = Format(decDeducSubTotal, GUI.fmtCurrency)
                    rpt_Rows.Item("Column83") = decDeducSubTotal

                Else
                    rpt_Rows.Item("Column8") = "0"
                    rpt_Rows.Item("Column83") = 0

                End If

                rpt_Rows.Item("Column9") = Format(decEarnSubTotal - decDeducSubTotal, GUI.fmtCurrency)
                rpt_Rows.Item("Column13") = Format(decEarnSubTotal - decDeducSubTotal + decOpeningBalance, GUI.fmtCurrency)
                rpt_Rows.Item("Column84") = decEarnSubTotal - decDeducSubTotal + decOpeningBalance
                rpt_Rows.Item("Column14") = Format(decOpeningBalance, GUI.fmtCurrency)
                rpt_Rows.Item("Column81") = dtRow.Item("HeadCount")
                rpt_Rows.Item("Column11") = dtRow.Item("Id")
                rpt_Rows.Item("Column12") = dtRow.Item("GName")
                strPrevGrpId = dtRow.Item("Id").ToString
                rpt_Rows.Item("Column10") = dtRow.Item("HeadCount")
                rpt_Rows.Item("Column82") = CDec(dtRow.Item("Amount"))

                If menExportAction = enExportAction.ExcelExtra Then

                    If mstrHeadTypeId <> dtRow.Item("HeadTypeId").ToString AndAlso mstrHeadTypeId <> "" Then
                        If mstrHeadTypeId = enTranHeadType.EarningForEmployees Or mstrHeadTypeId = enTranHeadType.DeductionForEmployee Then
                            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

                            Dim J As Integer = 0

                            If mstrHeadTypeId = enTranHeadType.DeductionForEmployee Then
                                'Sohail (20 Feb 2013) -- Start
                                'TRA - ENHANCEMENT
                                'J = 1
                                J = 3
                                'Sohail (20 Feb 2013) -- End
                            End If


                            For i As Integer = 0 To J

                                'Sohail (20 Feb 2013) -- Start
                                'TRA - ENHANCEMENT
                                If i = 2 Then 'Net B/F
                                    If decEarnSubTotal - decDeducSubTotal = decEarnSubTotal - decDeducSubTotal + decOpeningBalance Then
                                        Continue For
                                    End If
                                End If
                                'Sohail (20 Feb 2013) -- End

                                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                                rpt_Rows.Item("Column1") = dtRow.Item("PeriodId")

                                If mstrHeadTypeId = enTranHeadType.EarningForEmployees Then
                                    rpt_Rows.Item("Column2") = Language.getMessage(mstrModuleName, 19, "Gross Pay")
                                ElseIf mstrHeadTypeId = enTranHeadType.DeductionForEmployee And i = 0 Then
                                    rpt_Rows.Item("Column2") = Language.getMessage(mstrModuleName, 20, "Total Deduction")
                                ElseIf mstrHeadTypeId = enTranHeadType.DeductionForEmployee And i = 1 Then
                                    rpt_Rows.Item("Column2") = Language.getMessage(mstrModuleName, 21, "Net Pay")
                                    'Sohail (20 Feb 2013) -- Start
                                    'TRA - ENHANCEMENT
                                ElseIf mstrHeadTypeId = enTranHeadType.DeductionForEmployee And i = 2 Then
                                    rpt_Rows.Item("Column2") = Language.getMessage(mstrModuleName, 41, "Net B/F")
                                ElseIf mstrHeadTypeId = enTranHeadType.DeductionForEmployee And i = 3 Then
                                    rpt_Rows.Item("Column2") = Language.getMessage(mstrModuleName, 42, "Total Net Pay")
                                    'Sohail (20 Feb 2013) -- End
                                End If

                                rpt_Rows.Item("Column3") = ""
                                rpt_Rows.Item("Column4") = 0
                                rpt_Rows.Item("Column5") = mstrHeadTypeId

                                If mstrHeadTypeId = enTranHeadType.EarningForEmployees Then
                                    rpt_Rows.Item("Column6") = Language.getMessage(mstrModuleName, 4, "Earnings")
                                Else
                                    rpt_Rows.Item("Column6") = Language.getMessage(mstrModuleName, 5, "Deductions")
                                End If

                                rpt_Rows.Item("Column7") = ""

                                rpt_Rows.Item("Column11") = dtRow.Item("Id")
                                rpt_Rows.Item("Column12") = dtRow.Item("GName")

                                If mstrHeadTypeId = enTranHeadType.EarningForEmployees Then
                                    rpt_Rows.Item("Column82") = decEarnSubTotal
                                ElseIf mstrHeadTypeId = enTranHeadType.DeductionForEmployee And i = 0 Then
                                    rpt_Rows.Item("Column82") = decDeducSubTotal
                                ElseIf mstrHeadTypeId = enTranHeadType.DeductionForEmployee And i = 1 Then
                                    rpt_Rows.Item("Column82") = decEarnSubTotal - decDeducSubTotal
                                    'Sohail (20 Feb 2013) -- Start
                                    'TRA - ENHANCEMENT
                                ElseIf mstrHeadTypeId = enTranHeadType.DeductionForEmployee And i = 2 Then
                                    rpt_Rows.Item("Column82") = decOpeningBalance
                                ElseIf mstrHeadTypeId = enTranHeadType.DeductionForEmployee And i = 3 Then
                                    rpt_Rows.Item("Column82") = decEarnSubTotal - decDeducSubTotal + decOpeningBalance
                                    'Sohail (20 Feb 2013) -- End
                                End If

                                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)

                            Next

                        Else
                            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                        End If

                    Else
                        rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                    End If


                    mstrHeadTypeId = dtRow.Item("HeadTypeId")

                Else
                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
                End If

            Next

            If menExportAction = enExportAction.ExcelExtra Then
                dtDetailsExport = rpt_Data.Tables("ArutiTable")
            End If

            'Pinkal (24-Jan-2013) -- End


            objRpt = New ArutiReport.Designer.rptPayrollSummary

            'Sandeep [ 10 FEB 2011 ] -- Start
            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            'Sohail (21 Jan 2016) -- Start
            'Enhancement - 3 Logo options on Payroll Summary Report for KBC.
            'ReportFunction.Logo_Display(objRpt, _
            '                           ConfigParameter._Object._IsDisplayLogo, _
            '                           ConfigParameter._Object._ShowLogoRightSide, _
            '                           "arutiLogo1", _
            '                           "arutiLogo2", _
            '                           arrImageRow, _
            '                           "txtCompanyName", _
            '                           "txtReportName", _
            '                           "txtFilterDescription", _
            '                           ConfigParameter._Object._GetLeftMargin, _
            '                           ConfigParameter._Object._GetRightMargin)
            If mblnIsLogoCompanyInfo = True Then

                ReportFunction.Logo_Display(objRpt, _
                                           ConfigParameter._Object._IsDisplayLogo, _
                                           ConfigParameter._Object._ShowLogoRightSide, _
                                           "arutiLogo1", _
                                           "arutiLogo2", _
                                           arrImageRow, _
                                           "txtCompanyName", _
                                           "txtReportName", _
                                           "txtFilterDescription", _
                                           ConfigParameter._Object._GetLeftMargin, _
                                           ConfigParameter._Object._GetRightMargin)

            ElseIf mblnIsLogo = True Then

                ReportFunction.Logo_Display(objRpt, _
                                           ConfigParameter._Object._IsDisplayLogo, _
                                           False, _
                                           "arutiLogo1", _
                                           "arutiLogo2", _
                                           arrImageRow, _
                                           "txtCompanyName", _
                                           "txtReportName", _
                                           "txtFilterDescription", _
                                           ConfigParameter._Object._GetLeftMargin, _
                                           ConfigParameter._Object._GetRightMargin)

            End If
            'Sohail (21 Jan 2016) -- End

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            'Sohail (21 Jan 2016) -- Start
            'Enhancement - 3 Logo options on Payroll Summary Report for KBC.
            If mblnIsCompanyInfo = True Then
                objRpt.ReportDefinition.Sections("CompanyInfoSection").SectionFormat.EnableSuppress = False
                objRpt.ReportDefinition.Sections("LogoSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("Section2").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("CompanyNameSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("FilterSection").SectionFormat.EnableSuppress = False
                Call ReportFunction.TextChange(objRpt, "txtCompanyName1", strCompanyName)
                Call ReportFunction.TextChange(objRpt, "txtReportName1", Me._ReportName)
                objRpt.ReportDefinition.Sections("ReportNameSection").SectionFormat.EnableSuppress = True 'Sohail (07 Mar 2016)
            ElseIf mblnIsLogo = True Then
                objRpt.ReportDefinition.Sections("LogoSection").SectionFormat.EnableSuppress = False
                objRpt.ReportDefinition.Sections("CompanyInfoSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("Section2").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("CompanyNameSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("FilterSection").SectionFormat.EnableSuppress = False
                objRpt.ReportDefinition.Sections("ReportNameSection").SectionFormat.EnableSuppress = False 'Sohail (07 Mar 2016)
                Call ReportFunction.TextChange(objRpt, "txtReportName2", mstrReportTypeName) 'Sohail (07 Mar 2016)
                If mintPayslipTemplate = enPayslipTemplate.ONE_SIDED_KBC_13 Then
                    objRpt.ReportDefinition.Sections("CompanyNameSection").SectionFormat.EnableSuppress = False
                    Call ReportFunction.TextChange(objRpt, "txtCompanyName2", strCompanyName)
                End If

            ElseIf mblnIsLogoCompanyInfo = True Then
                objRpt.ReportDefinition.Sections("Section2").SectionFormat.EnableSuppress = False
                objRpt.ReportDefinition.Sections("CompanyInfoSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("LogoSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("CompanyNameSection").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("FilterSection").SectionFormat.EnableSuppress = True
                Call ReportFunction.TextChange(objRpt, "txtCompanyName1", strCompanyName)
                Call ReportFunction.TextChange(objRpt, "txtReportName1", Me._ReportName)
                objRpt.ReportDefinition.Sections("ReportNameSection").SectionFormat.EnableSuppress = True 'Sohail (07 Mar 2016)
                Call ReportFunction.TextChange(objRpt, "txtCompanyName", strCompanyName) 'Sohail (07 Mar 2016)
                Call ReportFunction.TextChange(objRpt, "txtReportName", mstrReportTypeName) 'Sohail (07 Mar 2016)
            End If
            'Sohail (21 Jan 2016) -- End

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 25, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
                'Sohail (11 Mar 2016) -- Start
                'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
                Call ReportFunction.TextChange(objRpt, "lblPreDate", Language.getMessage(mstrModuleName, 45, "Date :"))
                Call ReportFunction.TextChange(objRpt, "lblPreSign", Language.getMessage(mstrModuleName, 49, "Sign :"))
                'Sohail (11 Mar 2016) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 26, "Checked By :"))
                'Sohail (11 Mar 2016) -- Start
                'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
                Call ReportFunction.TextChange(objRpt, "lblChkDate", Language.getMessage(mstrModuleName, 46, "Date :"))
                Call ReportFunction.TextChange(objRpt, "lblChkSign", Language.getMessage(mstrModuleName, 50, "Sign :"))
                'Sohail (11 Mar 2016) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection6", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 27, "Approved By :"))
                'Sohail (11 Mar 2016) -- Start
                'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
                Call ReportFunction.TextChange(objRpt, "lblAppDate", Language.getMessage(mstrModuleName, 47, "Date :"))
                Call ReportFunction.TextChange(objRpt, "lblAppSign", Language.getMessage(mstrModuleName, 51, "Sign :"))
                'Sohail (11 Mar 2016) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection7", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 28, "Received By :"))
                'Sohail (11 Mar 2016) -- Start
                'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
                Call ReportFunction.TextChange(objRpt, "lblRecDate", Language.getMessage(mstrModuleName, 48, "Date :"))
                Call ReportFunction.TextChange(objRpt, "lblRecSign", Language.getMessage(mstrModuleName, 52, "Sign :"))
                'Sohail (11 Mar 2016) -- End
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection8", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            'Sohail (20 Apr 2017) -- Start
            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
            StrQ = "SELECT  hremployee_master.employeeunkid "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ",0 AS Id, '' AS GName "
            End If

            StrQ &= "INTO    #tmpEmp " & _
                    "FROM    hremployee_master "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Join
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= "WHERE 1 = 1 "

            If mblnIsActive = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            StrQ &= "SELECT DISTINCT " & _
                            "cfexchange_rate.countryunkid  " & _
                          ", cfexchange_rate.currency_name " & _
                          ", cfexchange_rate.currency_sign " & _
                    "INTO    #tmpCurr " & _
                    "FROM    cfexchange_rate " & _
                    "WHERE   cfexchange_rate.isactive = 1 "
            'Sohail (20 Apr 2017) -- End

            StrQ &= "SELECT " & _
                   "     b.HeadTypeId " & _
                   "    ,c.Id " & _
                   "    ,c.GName " & _
                   "    ,ISNULL(a.EmpId, 0) AS EmpId " & _
                   "    ,ISNULL(a.Amount, 0) AS Amount " & _
                   "    ,b.HeadTypeName " & _
                   ", #tmpCurr.currency_name " & _
                   ", #tmpCurr.countryunkid " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         1 AS HeadTypeId " & _
                   "        ,@BankPayment AS HeadTypeName " & _
                   "UNION " & _
                   "    SELECT " & _
                   "         2 AS HeadTypeId " & _
                   "        ,@CashPayment AS HeadTypeName " & _
                   "UNION " & _
                   "    SELECT " & _
                   "         3 AS HeadTypeId " & _
                   "        ,@SalaryOnHold AS HeadTypeName " & _
                   ") AS b "
            'Sohail (22 Apr 2017) - [countryunkid]
            'Sohail (20 Apr 2017) - [currency_name]

            If mintViewIndex > 0 Then
                StrQ &= "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "       " & Mid(mstrAnalysis_Fields, 2) & " " & _
                        "   FROM    " & mstrAnalysis_TableName & " " & _
                        ") AS c ON 1 = 1 "
            Else
                StrQ &= "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        0 AS Id " & _
                        "       ,'' AS GName " & _
                        ") AS c ON 1 = 1 "
            End If

            StrQ &= "LEFT JOIN ( SELECT  COUNT(DISTINCT prempsalary_tran.employeeunkid) AS EmpId " & _
                    ", SUM(CAST((prempsalary_tran.expaidamt) AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                    ", prpayment_tran.periodunkid AS PeriodId " & _
                    ", 1 AS HeadTypeId " & _
                    ", prpayment_tran.countryunkid "
            'Sohail (20 Apr 2017) - [SUM(CAST((prempsalary_tran.amount *  " & mdecEx_Rate & ") AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount]=[SUM(CAST((prempsalary_tran.expaidamt) AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount], [countryunkid]

            'Sohail (20 Apr 2017) -- Start
            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ",0 AS Id, '' AS GName "
            'End If
            StrQ &= ", #tmpEmp.Id, #tmpEmp.GName "
            'Sohail (20 Apr 2017) -- End

            StrQ &= "FROM prempsalary_tran " & _
                    "   JOIN prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                    "   AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                    "   JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN #tmpEmp ON #tmpEmp.employeeunkid = prempsalary_tran.employeeunkid "
            'Sohail (21 Apr 2017) - [AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112)] - issue : previous year carry forwarded paymenttranunkids were comin in join.

            'Sohail (20 Apr 2017) -- Start
            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Join
            'End If

            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Sohail (20 Apr 2017) -- End

            StrQ &= "WHERE prempsalary_tran.isvoid = 0 " & _
                    "   AND prpayment_tran.isvoid = 0 " & _
                    "AND #tmpEmp.employeeunkid IS NOT NULL " & _
                    "   AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                    "   AND prpayment_tran.paymentmode IN ( " & enPaymentMode.CHEQUE & ", " & enPaymentMode.TRANSFER & " ) " & _
                    "   AND prpayment_tran.periodunkid = @Period "
            'Sohail (20 Apr 2017) - [AND #tmpEmp.employeeunkid IS NOT NULL]

            'Sohail (20 Apr 2017) -- Start
            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
            'If mblnIsActive = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry
            '    End If
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'Sohail (20 Apr 2017) -- End

            StrQ &= "GROUP BY prpayment_tran.periodunkid " & _
                    ", prpayment_tran.countryunkid "
            'Sohail (20 Apr 2017) - [countryunkid]

            'Sohail (20 Apr 2017) -- Start
            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
            'End If
            StrQ &= ", #tmpEmp.Id, #tmpEmp.GName "
            'Sohail (20 Apr 2017) -- End

            StrQ &= "UNION ALL " & _
                    "   SELECT " & _
                    "        COUNT(DISTINCT prpayment_tran.employeeunkid) AS EmpId " & _
                    "       ,SUM(CAST((prpayment_tran.expaidamt) AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                    "       ,prpayment_tran.periodunkid AS PeriodId " & _
                    "       ,2 AS HeadTypeId " & _
                    ", prpayment_tran.countryunkid "
            'Sohail (20 Apr 2017) - [SUM(CAST((prpayment_tran.amount *  " & mdecEx_Rate & ") AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount]=[SUM(CAST((prpayment_tran.expaidamt) AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount], [countryunkid]

            'Sohail (20 Apr 2017) -- Start
            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ",0 AS Id, '' AS GName "
            'End If
            StrQ &= ", #tmpEmp.Id, #tmpEmp.GName "
            'Sohail (20 Apr 2017) -- End

            StrQ &= "FROM    prpayment_tran " & _
                    "   JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN #tmpEmp ON #tmpEmp.employeeunkid = prpayment_tran.employeeunkid "

            'Sohail (20 Apr 2017) -- Start
            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Join
            'End If

            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Sohail (20 Apr 2017) -- End

            StrQ &= "WHERE ISNULL(prpayment_tran.isvoid, 0) = 0 " & _
                    "   AND #tmpEmp.employeeunkid IS NOT NULL " & _
                    "   AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                    "   AND prpayment_tran.paymentmode IN ( " & enPaymentMode.CASH & ", " & enPaymentMode.CASH_AND_CHEQUE & " ) " & _
                    "   AND prpayment_tran.periodunkid = @Period "
            'Sohail (20 Apr 2017) - [AND #tmpEmp.employeeunkid IS NOT NULL]

            'Sohail (20 Apr 2017) -- Start
            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
            'If mblnIsActive = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry
            '    End If
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'Sohail (20 Apr 2017) -- End

            StrQ &= "GROUP BY prpayment_tran.periodunkid " & _
                    ", prpayment_tran.countryunkid "
            'Sohail (20 Apr 2017) - [countryunkid]

            'Sohail (20 Apr 2017) -- Start
            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
            'End If
            StrQ &= ", #tmpEmp.Id, #tmpEmp.GName "
            'Sohail (20 Apr 2017) -- End

            StrQ &= "UNION ALL " & _
                    "   SELECT " & _
                    "        COUNT(prtnaleave_tran.employeeunkid) AS EmpId " & _
                    "       ,SUM(CAST((prtnaleave_tran.balanceamount *  " & mdecEx_Rate & ") AS DECIMAL(36, " & decDecimalPlaces & "))) AS Amount " & _
                    "       ,prtnaleave_tran.payperiodunkid AS PeriodId " & _
                    "       ,3 AS HeadTypeId " & _
                    ", " & intBaseCountryUnkId & " AS countryunkid "

            'Sohail (20 Apr 2017) -- Start
            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields
            'Else
            '    StrQ &= ",0 AS Id, '' AS GName "
            'End If
            StrQ &= ", #tmpEmp.Id, #tmpEmp.GName "
            'Sohail (20 Apr 2017) -- End

            StrQ &= "FROM prtnaleave_tran " & _
                    "   JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN #tmpEmp ON #tmpEmp.employeeunkid = prtnaleave_tran.employeeunkid "

            'Sohail (20 Apr 2017) -- Start
            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Join
            'End If

            'If xDateJoinQry.Trim.Length > 0 Then
            '    StrQ &= xDateJoinQry
            'End If

            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If
            'Sohail (20 Apr 2017) -- End

            StrQ &= "WHERE ISNULL(prtnaleave_tran.isvoid, 0) = 0 " & _
                    "AND #tmpEmp.employeeunkid IS NOT NULL " & _
                    "   AND prtnaleave_tran.payperiodunkid = @Period " & _
                    "   AND prtnaleave_tran.balanceamount <> 0 "
            'Sohail (20 Apr 2017) - [AND #tmpEmp.employeeunkid IS NOT NULL]

            'Sohail (20 Apr 2017) -- Start
            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
            'If mblnIsActive = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        StrQ &= xDateFilterQry
            '    End If
            'End If

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'Sohail (20 Apr 2017) -- End

            StrQ &= "GROUP BY prtnaleave_tran.payperiodunkid "

            'Sohail (20 Apr 2017) -- Start
            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
            'If mintViewIndex > 0 Then
            '    StrQ &= mstrAnalysis_Fields.Replace("AS Id", "").Replace("AS GName", "")
            'End If
            StrQ &= ", #tmpEmp.Id, #tmpEmp.GName "
            'Sohail (20 Apr 2017) -- End

            StrQ &= ") AS a ON a.HeadTypeId = b.HeadTypeId " & _
                    "   AND a.Id = c.Id " & _
                    "LEFT JOIN #tmpCurr ON #tmpCurr.countryunkid = a.countryunkid " & _
                    "WHERE #tmpCurr.countryunkid IS NOT NULL " & _
                    "ORDER BY c.Id, #tmpCurr.currency_name, #tmpCurr.countryunkid, b.HeadTypeId "
            'Sohail (20 Apr 2017) - [LEFT JOIN #tmpCurr ON #tmpCurr.countryunkid = a.countryunkid], [ORDER BY c.Id, b.HeadTypeId]=[]

            'Sohail (20 Apr 2017) -- Start
            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
            StrQ &= "DROP TABLE #tmpEmp " & _
                    "DROP TABLE #tmpCurr "
            'Sohail (20 Apr 2017) -- End


            objDataOperation.AddParameter("@BankPayment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Bank Payment")
            objDataOperation.AddParameter("@CashPayment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Cash Payment")
            objDataOperation.AddParameter("@SalaryOnHold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Salary On Hold")
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubSummary"), "txtBankPayment", Language.getMessage(mstrModuleName, 35, "Bank Payment"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubSummary"), "txtCashPayment", Language.getMessage(mstrModuleName, 36, "Cash Payment"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubSummary"), "txtSalaryonHold", Language.getMessage(mstrModuleName, 37, "Salary On Hold"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubSummary"), "txtNoOfEmployees", Language.getMessage(mstrModuleName, 38, "No of Employees"))

            dsListSummary = objDataOperation.ExecQuery(StrQ, "Summary")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If

            rpt_Data = New ArutiReport.Designer.dsArutiReport

            Dim decTotal As Decimal = 0
            'Sohail (22 Apr 2017) -- Start
            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
            Dim decCurrTot As Decimal = 0
            Dim intPrevCountryId As Integer = 0
            'Sohail (22 Apr 2017) -- End
            strPrevGrpId = ""
            Dim mstrGroupId As String = ""
            Dim mstrGroupName As String = ""
            Dim idx As Integer = 0

            If dsListSummary.Tables("Summary").Rows.Count > 0 Then
                For Each dtSummary As DataRow In dsListSummary.Tables("Summary").Rows
                    Dim rpt_SummaryRow As DataRow
                    rpt_SummaryRow = rpt_Data.Tables("ArutiTable").NewRow
                    rpt_SummaryRow.Item("Column1") = dtSummary.Item("Id")
                    rpt_SummaryRow.Item("Column2") = dtSummary.Item("GName")
                    rpt_SummaryRow.Item("Column3") = dtSummary.Item("HeadTypeId")
                    rpt_SummaryRow.Item("Column81") = dtSummary.Item("EmpId")
                    rpt_SummaryRow.Item("Column5") = Format(dtSummary.Item("Amount"), GUI.fmtCurrency)
                    rpt_SummaryRow.Item("Column6") = dtSummary.Item("HeadTypeName")
                    'Sohail (20 Apr 2017) -- Start
                    'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
                    rpt_SummaryRow.Item("Column9") = dtSummary.Item("currency_name")
                    'Sohail (20 Apr 2017) -- End

                    If strPrevGrpId <> dtSummary.Item("Id").ToString Then
                        decTotal = CDec(dtSummary.Item("Amount"))
                    Else
                        decTotal += CDec(dtSummary.Item("Amount"))
                    End If
                    rpt_SummaryRow.Item("Column7") = Format(decTotal, GUI.fmtCurrency)

                    'Sohail (22 Apr 2017) -- Start
                    'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
                    If strPrevGrpId <> dtSummary.Item("Id").ToString OrElse intPrevCountryId <> CInt(dtSummary.Item("countryunkid")) Then
                        decCurrTot = CDec(dtSummary.Item("Amount"))
                    Else
                        decCurrTot += CDec(dtSummary.Item("Amount"))
                    End If
                    rpt_SummaryRow.Item("Column11") = Format(decCurrTot, GUI.fmtCurrency)
                    'Sohail (22 Apr 2017) -- End

                    If menExportAction = enExportAction.ExcelExtra Then

                        If mintViewIndex > 0 Then
                            'Sohail (20 Apr 2017) -- Start
                            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
                            'Dim Row() As DataRow = dtDetailsExport.Select("Column11=" & dtSummary.Item("Id"))
                            Dim Row() As DataRow = dtDetailsExport.Select("Column11= '" & dtSummary.Item("Id") & "' ")
                            'Sohail (20 Apr 2017) -- End
                            If Row.Length <= 0 Then GoTo ForAnalysisAddRow
                        End If

                        If dtDetailsExport.Rows.Count > 0 AndAlso dtDetailsExport.Rows(0).Item("Column5").ToString() <> "" Then
                            Dim drRow As DataRow = dtDetailsExport.NewRow
                            drRow("Column1") = mintPeriodId
                            drRow("Column2") = dtSummary.Item("HeadTypeName")
                            drRow("Column5") = 99
                            drRow("Column6") = Language.getMessage(mstrModuleName, 23, "Payment Details")
                            drRow("Column8") = CDec(dtSummary.Item("Amount"))
                            drRow("Column10") = dtSummary.Item("EmpId")
                            drRow("Column11") = dtSummary.Item("Id")
                            drRow("Column12") = dtSummary.Item("GName")
                            drRow("Column81") = dtSummary.Item("EmpId")
                            drRow("Column82") = CDec(dtSummary.Item("Amount"))
                            'Sohail (20 Apr 2017) -- Start
                            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
                            drRow("Column3") = dtSummary.Item("currency_name").ToString
                            'Sohail (20 Apr 2017) -- End
                            dtDetailsExport.Rows.Add(drRow)
                        End If

ForAnalysisAddRow:
                        If strPrevGrpId <> "" And strPrevGrpId <> dtSummary.Item("Id").ToString Then
ForAddRow:
                            If dtDetailsExport.Rows.Count > 0 AndAlso dtDetailsExport.Rows(0).Item("Column5").ToString() <> "" Then
                                Dim dNewRow As DataRow = dtDetailsExport.NewRow
                                dNewRow("Column1") = mintPeriodId
                                dNewRow("Column2") = Language.getMessage(mstrModuleName, 38, "No of Employees")
                                dNewRow("Column5") = 99
                                dNewRow("Column6") = Language.getMessage(mstrModuleName, 23, "Payment Details")

                                Dim mdclTot As Decimal = 0
                                Dim mintTot As Integer = 0
                                'Sohail (20 Apr 2017) -- Start
                                'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
                                'Dim dcRow() As DataRow = dtDetailsExport.Select("Column5 =99 AND Column11=" & mstrGroupId)
                                Dim dcRow() As DataRow = dtDetailsExport.Select("Column5 = '99' AND Column11='" & mstrGroupId & "' ")
                                'Sohail (20 Apr 2017) -- End

                                If dcRow.Length > 0 Then

                                    For i As Integer = 0 To dcRow.Length - 1
                                        mdclTot += CDec(dcRow(i)("Column82"))
                                        mintTot += CInt(dcRow(i)("Column81"))
                                    Next

                                Else
                                    Continue For
                                End If

                                dNewRow("Column8") = mdclTot
                                dNewRow("Column82") = mdclTot

                                dNewRow("Column10") = mintTot
                                dNewRow("Column81") = mintTot


                                dNewRow("Column11") = mstrGroupId
                                dNewRow("Column12") = mstrGroupName
                                dtDetailsExport.Rows.Add(dNewRow)
                            End If

                        ElseIf mintViewIndex <= 0 And idx = dsListSummary.Tables(0).Rows.Count - 1 Then
                            GoTo ForAddRow
                        End If

                    End If
                    mstrGroupId = dtSummary.Item("Id")
                    mstrGroupName = dtSummary.Item("GName")
                    strPrevGrpId = dtSummary.Item("Id").ToString
                    'Sohail (22 Apr 2017) -- Start
                    'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
                    intPrevCountryId = CInt(dtSummary.Item("countryunkid"))
                    'Sohail (22 Apr 2017) -- End
                    idx += 1
                    'Pinkal (24-Jan-2013) -- End


                    rpt_Data.Tables("ArutiTable").Rows.Add(rpt_SummaryRow)
                Next
            End If


            objRpt.Subreports("rptSubSummary").SetDataSource(rpt_Data)

            Call ReportFunction.TextChange(objRpt.Subreports("rptSubSummary"), "txtBankPayment", Language.getMessage(mstrModuleName, 35, "Bank Payment"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubSummary"), "txtCashPayment", Language.getMessage(mstrModuleName, 36, "Cash Payment"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubSummary"), "txtSalaryonHold", Language.getMessage(mstrModuleName, 37, "Salary On Hold"))
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubSummary"), "txtNoOfEmployees", Language.getMessage(mstrModuleName, 38, "No of Employees"))
            'Sohail (20 Apr 2017) -- Start
            'CCBRT Enhancement - 66.1 - Show Currency wise Payment detail on Payroll Summary Report.
            Call ReportFunction.TextChange(objRpt.Subreports("rptSubSummary"), "txtPaidCurrency", Language.getMessage(mstrModuleName, 53, "Paid Currency :"))
            'Sohail (20 Apr 2017) -- End

            Call ReportFunction.TextChange(objRpt, "txtPeriod", Language.getMessage(mstrModuleName, 15, "Period Name"))
            Call ReportFunction.TextChange(objRpt, "txtPeriodName", mstrPeriodName)

            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 16, "Print Date :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 17, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPageNumber", Language.getMessage(mstrModuleName, 18, "Page :"))

            Dim dRow() As DataRow = dsList.Tables("DataTable").Select("HeadTypeId = 2")
            If dRow.Length > 0 Then
                objRpt.DataDefinition.FormulaFields("frmGrossCaption").Text = """" & Language.getMessage(mstrModuleName, 19, "Gross Pay") & """"
            Else
                objRpt.DataDefinition.FormulaFields("frmGrossCaption").Text = """" & Language.getMessage(mstrModuleName, 21, "Net Pay") & """"
            End If
            objRpt.DataDefinition.FormulaFields("frmDeductionCaption").Text = """" & Language.getMessage(mstrModuleName, 20, "Total Deduction") & """"

            Call ReportFunction.TextChange(objRpt, "txtHeadCode", Language.getMessage(mstrModuleName, 29, "Head Code"))
            Call ReportFunction.TextChange(objRpt, "txtDescription", Language.getMessage(mstrModuleName, 30, "Description"))
            Call ReportFunction.TextChange(objRpt, "txtEmpCount", Language.getMessage(mstrModuleName, 31, "Emp. Count"))
            Call ReportFunction.TextChange(objRpt, "txtAmount", Language.getMessage(mstrModuleName, 32, "Amount"))

            Call ReportFunction.TextChange(objRpt, "txtNetPay", Language.getMessage(mstrModuleName, 21, "Net Pay"))
            Call ReportFunction.TextChange(objRpt, "txtNetBF", Language.getMessage(mstrModuleName, 41, "Net B/F"))
            Call ReportFunction.TextChange(objRpt, "txtTotalNetPay", Language.getMessage(mstrModuleName, 42, "Total Net Pay"))
            Call ReportFunction.TextChange(objRpt, "txtMessages", Language.getMessage(mstrModuleName, 22, "Messages"))
            Call ReportFunction.TextChange(objRpt, "txtPaymentDetails", Language.getMessage(mstrModuleName, 23, "Payment Details"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtCompanyName", Me._CompanyName)
            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription1", Me._FilterTitle) 'Sohail (21 Jan 2016)

            Dim dsListMsg As New DataSet
            Dim objMessage As New clsPayslipMessages_master

            dsListMsg = objMessage.GetList(strDatabaseName, _
                                           intUserUnkid, _
                                           intYearUnkid, _
                                           intCompanyUnkid, _
                                           mdtPeriodStartDate, _
                                           mdtPeriodEndDate, _
                                           strUserModeSetting, True, _
                                           mblnIsActive, "Messages", True)

            If dsListMsg.Tables("Messages").Rows.Count > 0 Then
                Call ReportFunction.TextChange(objRpt, "txtMsg1", dsListMsg.Tables("Messages").Rows(0).Item("msg1").ToString)
                Call ReportFunction.TextChange(objRpt, "txtMsg2", dsListMsg.Tables("Messages").Rows(0).Item("msg2").ToString)
                Call ReportFunction.TextChange(objRpt, "txtMsg3", dsListMsg.Tables("Messages").Rows(0).Item("msg3").ToString)
                Call ReportFunction.TextChange(objRpt, "txtMsg4", dsListMsg.Tables("Messages").Rows(0).Item("msg4").ToString)
                'Sohail (11 Mar 2016) -- Start
                'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
                If dsListMsg.Tables("Messages").Rows(0).Item("msg1").ToString.Trim = "" Then objRpt.ReportDefinition.Sections("Message1").SectionFormat.EnableSuppress = True
                If dsListMsg.Tables("Messages").Rows(0).Item("msg2").ToString.Trim = "" Then objRpt.ReportDefinition.Sections("Message2").SectionFormat.EnableSuppress = True
                If dsListMsg.Tables("Messages").Rows(0).Item("msg3").ToString.Trim = "" Then objRpt.ReportDefinition.Sections("Message3").SectionFormat.EnableSuppress = True
                If dsListMsg.Tables("Messages").Rows(0).Item("msg4").ToString.Trim = "" Then objRpt.ReportDefinition.Sections("Message4").SectionFormat.EnableSuppress = True
                'Sohail (11 Mar 2016) -- End
            Else
                Call ReportFunction.TextChange(objRpt, "txtMsg1", "")
                Call ReportFunction.TextChange(objRpt, "txtMsg2", "")
                Call ReportFunction.TextChange(objRpt, "txtMsg3", "")
                Call ReportFunction.TextChange(objRpt, "txtMsg4", "")
                'Sohail (11 Mar 2016) -- Start
                'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
                objRpt.ReportDefinition.Sections("Message1").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("Message2").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("Message3").SectionFormat.EnableSuppress = True
                objRpt.ReportDefinition.Sections("Message4").SectionFormat.EnableSuppress = True
                'Sohail (11 Mar 2016) -- End
            End If


            Call ReportFunction.SetRptDecimal(objRpt, "frmlCol41")
            Call ReportFunction.SetRptDecimal(objRpt, "SumoffrmlCol41")
            Call ReportFunction.SetRptDecimal(objRpt, "totNetPay1")
            Call ReportFunction.SetRptDecimal(objRpt.Subreports("rptSubSummary"), "totAmount1")
            If mintViewIndex > 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", False)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection2", True)
            End If

            'Anjan [16 November 2015] -- Start
            'ENHANCEMENT : Include setting for Analysis by to be shown on each page. Requested by Rutta for VFT.
            If mblnAnalysisOnNewPage = True Then
                'ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection1", True)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "1"
            Else
                'ReportFunction.EnableNewPageBefore(objRpt, "GroupHeaderSection1", False)
                objRpt.DataDefinition.FormulaFields("frmlNewPage").Text = "0"
            End If
            'Anjan [16 November 2015] -- End

            'Sohail (11 Mar 2016) -- Start
            'Enhancement - Show Payment Approver details (Approved By, Authorized By etc.) on Payroll Summary Report in 58.1 SP.
            If mblnSetPayslipPaymentApproval = True AndAlso mblnShowPaymentApprovalDetailsOnStatutoryReport = True Then
                Dim objTnALeave As New clsTnALeaveTran
                Dim dtTable As DataTable

                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(strDatabaseName) = mintPeriodId
                dsList = objTnALeave.Get_Balance_List(strDatabaseName, intUserUnkid, objPeriod._Yearunkid, intCompanyUnkid, dtPeriodStart, dtPeriodEnd, strUserModeSetting, blnOnlyApproved, False, True, "", "Balance", , mintPeriodId, "")
                dtTable = New DataView(dsList.Tables("Balance"), "balanceamount > 0", "", DataViewRowState.CurrentRows).ToTable

                If dtTable.Rows.Count = 0 Then 'If payment is done for all employees
                    Dim objPayment As New clsPayment_tran
                    Dim objLevel As New clsPaymentApproverlevel_master
                    Dim objPaymentApproval As New clsPayment_approval_tran
                    Dim objPayAuthorize As New clsPayment_authorize_tran
                    Dim dsLevel As DataSet
                    Dim dsAuthorizeUser As DataSet
                    Dim intTotalPaymentCount As Integer
                    Dim intTotalApprovedtCount As Integer
                    Dim intTotalAuthorizedtCount As Integer
                    Dim intMaxRowCount As Integer
                    Dim intCount As Integer = 0

                    'Sohail (24 Jun 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
                    'intTotalPaymentCount = objPayment.GetTotalPaymentCount(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                    intTotalPaymentCount = objPayment.GetTotalPaymentCount(strDatabaseName, intUserUnkid, objPeriod._Yearunkid, intCompanyUnkid, dtPeriodStart, dtPeriodEnd, strUserModeSetting, blnOnlyApproved, False, False, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                    'Sohail (24 Jun 2019) -- End

                    intMaxRowCount = objLevel.GetLevelListCount()
                    dsLevel = objLevel.GetList("Level", True, True)
                    intCount = objPayAuthorize.GetDISTINCTauthorizedUsersCount(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                    If intCount > intMaxRowCount Then
                        intMaxRowCount = intCount
                    End If
                    dsAuthorizeUser = objPayAuthorize.GetDISTINCTauthorizedUsers("Users", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)


                    For i = 0 To intMaxRowCount - 1

                        '*** Prepared By
                        If i = 0 Then

                            objPeriod = New clscommom_period_Tran
                            objPeriod._Periodunkid(strDatabaseName) = mintPeriodId
                            dsList = objPayment.GetDistinctPaymentUsers(strDatabaseName, intUserUnkid, objPeriod._Yearunkid, intCompanyUnkid, dtPeriodStart, dtPeriodEnd, strUserModeSetting, blnOnlyApproved, False, "LastPayment", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId, True)
                            If dsList.Tables("LastPayment").Rows.Count > 0 Then
                                Dim strPrepared As String = ""
                                For Each dsRow As DataRow In dsList.Tables("LastPayment").Rows
                                    If strPrepared.Trim = "" Then
                                        strPrepared = dsRow("userfullname").ToString & ", " & CDate(dsRow("paymentdate"))
                                    Else
                                        strPrepared &= "; " & dsRow("userfullname").ToString & ", " & CDate(dsRow("paymentdate"))
                                    End If
                                Next
                                If mstrPreparedByDetails.Trim = "" Then
                                    mstrPreparedByDetails &= strPrepared
                                Else
                                    mstrPreparedByDetails &= vbCrLf & strPrepared
                                End If
                            End If
                        End If


                        '*** Approved By
                        If i <= dsLevel.Tables("Level").Rows.Count - 1 Then
                            intTotalApprovedtCount = objPaymentApproval.GetTotalApprovedCount(mintPeriodId, CInt(dsLevel.Tables("Level").Rows(i)("levelunkid")))

                            If intTotalApprovedtCount = intTotalPaymentCount Then
                                dsList = objPaymentApproval.GetLastApprovedDetail("List", mintPeriodId, CInt(dsLevel.Tables("Level").Rows(i)("levelunkid")))
                                If dsList.Tables("List").Rows.Count > 0 Then
                                    Dim strApproved As String = ""
                                    For Each dsRow As DataRow In dsList.Tables("List").Rows
                                        If strApproved.Trim = "" Then
                                            strApproved = dsRow.Item("levelname").ToString & " : " & dsRow.Item("approverfullname").ToString & "," & CDate(dsRow.Item("approval_date"))
                                        Else
                                            strApproved &= "; " & dsRow.Item("approverfullname").ToString & "," & CDate(dsRow.Item("approval_date"))
                                        End If
                                    Next
                                    If mstrApprovedByDetails.Trim = "" Then
                                        mstrApprovedByDetails &= strApproved
                                    Else
                                        mstrApprovedByDetails &= vbCrLf & strApproved
                                    End If
                                End If
                            End If
                        End If


                        '*** Authorized By
                        If i <= dsAuthorizeUser.Tables("Users").Rows.Count - 1 Then
                            'Sohail (24 Jun 2019) -- Start
                            'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
                            'intTotalAuthorizedtCount = objPayAuthorize.GetAuthorizedTotalCount(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                            intTotalAuthorizedtCount = objPayAuthorize.GetAuthorizedTotalCount(strDatabaseName, intUserUnkid, objPeriod._Yearunkid, intCompanyUnkid, dtPeriodStart, dtPeriodEnd, strUserModeSetting, blnOnlyApproved, False, False, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId)
                            'Sohail (24 Jun 2019) -- End

                            If intTotalAuthorizedtCount = intTotalPaymentCount Then
                                dsList = objPayAuthorize.GetLastAuthorizationDetail("List", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, mintPeriodId, CInt(dsAuthorizeUser.Tables("Users").Rows(i).Item("userunkid")))
                                If dsList.Tables("List").Rows.Count > 0 Then
                                    Dim strAuthorized As String = ""
                                    For Each dsRow As DataRow In dsList.Tables("List").Rows
                                        If strAuthorized.Trim = "" Then
                                            strAuthorized = dsRow.Item("userfullname").ToString & "," & CDate(dsRow.Item("authorize_date"))
                                        Else
                                            strAuthorized &= "; " & dsRow.Item("userfullname").ToString & "," & CDate(dsRow.Item("authorize_date"))
                                        End If
                                    Next
                                    If mstrAuthorizedByDetails.Trim = "" Then
                                        mstrAuthorizedByDetails &= strAuthorized
                                    Else
                                        mstrAuthorizedByDetails &= vbCrLf & strAuthorized
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If
            Else
                objRpt.ReportDefinition.Sections("ApproverDetails").SectionFormat.EnableSuppress = True
            End If

            Call ReportFunction.TextChange(objRpt, "lblAprPreparedBy", Language.getMessage(mstrModuleName, 12, "Prepared By"))
            objRpt.DataDefinition.FormulaFields("frmlAprPreparedBy").Text = "" & Chr(34) & mstrPreparedByDetails.Replace(vbCrLf, Chr(34) & " + Chr(10) + " & Chr(34)) & Chr(34) & ""
            Call ReportFunction.TextChange(objRpt, "lblAprApprovedBy", Language.getMessage(mstrModuleName, 43, "Approved By"))
            objRpt.DataDefinition.FormulaFields("frmlAprApprovedBy").Text = "" & Chr(34) & mstrApprovedByDetails.Replace(vbCrLf, Chr(34) & " + Chr(10) + " & Chr(34)) & Chr(34) & ""
            Call ReportFunction.TextChange(objRpt, "lblAprAuthorizedBy", Language.getMessage(mstrModuleName, 44, "Authorized By"))
            objRpt.DataDefinition.FormulaFields("frmlAprAuthorizedBy").Text = "" & Chr(34) & mstrAuthorizedByDetails.Replace(vbCrLf, Chr(34) & " + Chr(10) + " & Chr(34)) & Chr(34) & ""
            'Sohail (11 Mar 2016) -- End

            If menExportAction = enExportAction.ExcelExtra Then
                mdtTableExcel = New DataView(dtDetailsExport, "", "Column12,Column5", DataViewRowState.CurrentRows).ToTable
                mdtTableExcel.Columns.RemoveAt(0)
                Dim mintColumn As Integer = 0
                mdtTableExcel.Columns("Column2").Caption = Language.getMessage(mstrModuleName, 29, "Head Code")
                mdtTableExcel.Columns("Column2").SetOrdinal(0)
                mintColumn += 1

                mdtTableExcel.Columns("Column3").Caption = Language.getMessage(mstrModuleName, 30, "Description")
                mdtTableExcel.Columns("Column3").SetOrdinal(1)
                mintColumn += 1

                mdtTableExcel.Columns("Column10").Caption = Language.getMessage(mstrModuleName, 31, "Emp. Count")
                mdtTableExcel.Columns("Column10").SetOrdinal(2)
                mintColumn += 1

                mdtTableExcel.Columns("Column82").Caption = Language.getMessage(mstrModuleName, 32, "Amount")
                mdtTableExcel.Columns("Column82").SetOrdinal(3)
                mintColumn += 1

                mdtTableExcel.Columns("Column6").Caption = ""
                mdtTableExcel.Columns("Column6").SetOrdinal(4)
                mintColumn += 1

                If mintViewIndex > 0 Then
                    mdtTableExcel.Columns("Column12").Caption = IIf(mstrReport_GroupName.Trim.Contains(":"), mstrReport_GroupName.Replace(":", "").Trim, mstrReport_GroupName)
                    mdtTableExcel.Columns("Column12").SetOrdinal(5)
                    mintColumn += 1
                End If

                For i = mintColumn To mdtTableExcel.Columns.Count - 1
                    mdtTableExcel.Columns.RemoveAt(mintColumn)
                Next

            End If

            Return objRpt
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Generate_DetailReport; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
    'S.SANDEEP [04 JUN 2015] -- END


#End Region

    'Last Message No 24
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Tran Code")
            Language.setMessage(mstrModuleName, 2, "Tran Name")
            Language.setMessage(mstrModuleName, 3, "Amount")
            Language.setMessage(mstrModuleName, 4, "Earnings")
            Language.setMessage(mstrModuleName, 5, "Deductions")
            Language.setMessage(mstrModuleName, 6, "Employers Contributions")
            Language.setMessage(mstrModuleName, 7, "Informational")
            Language.setMessage(mstrModuleName, 8, "Loan")
            Language.setMessage(mstrModuleName, 9, "Advance")
            Language.setMessage(mstrModuleName, 10, "Grand Total :")
            Language.setMessage(mstrModuleName, 11, "Sub Total")
            Language.setMessage(mstrModuleName, 12, "Prepared By")
            Language.setMessage(mstrModuleName, 15, "Period Name")
            Language.setMessage(mstrModuleName, 16, "Print Date :")
            Language.setMessage(mstrModuleName, 17, "Printed By :")
            Language.setMessage(mstrModuleName, 18, "Page :")
            Language.setMessage(mstrModuleName, 19, "Gross Pay")
            Language.setMessage(mstrModuleName, 20, "Total Deduction")
            Language.setMessage(mstrModuleName, 21, "Net Pay")
            Language.setMessage(mstrModuleName, 22, "Messages")
            Language.setMessage(mstrModuleName, 23, "Payment Details")
            Language.setMessage(mstrModuleName, 24, " Order By :")
            Language.setMessage(mstrModuleName, 25, "Prepared By :")
            Language.setMessage(mstrModuleName, 26, "Checked By :")
            Language.setMessage(mstrModuleName, 27, "Approved By :")
            Language.setMessage(mstrModuleName, 28, "Received By :")
            Language.setMessage(mstrModuleName, 29, "Head Code")
            Language.setMessage(mstrModuleName, 30, "Description")
            Language.setMessage(mstrModuleName, 31, "Emp. Count")
            Language.setMessage(mstrModuleName, 32, "Amount")
            Language.setMessage(mstrModuleName, 33, "Currency :")
            Language.setMessage(mstrModuleName, 34, "Exchange Rate:")
            Language.setMessage(mstrModuleName, 35, "Bank Payment")
            Language.setMessage(mstrModuleName, 36, "Cash Payment")
            Language.setMessage(mstrModuleName, 37, "Salary On Hold")
            Language.setMessage(mstrModuleName, 38, "No of Employees")
            Language.setMessage(mstrModuleName, 39, "Saving")
            Language.setMessage(mstrModuleName, 40, "OpeningBalance")
            Language.setMessage(mstrModuleName, 41, "Net B/F")
            Language.setMessage(mstrModuleName, 42, "Total Net Pay")
            Language.setMessage(mstrModuleName, 43, "Approved By")
            Language.setMessage(mstrModuleName, 44, "Authorized By")
            Language.setMessage(mstrModuleName, 45, "Date :")
            Language.setMessage(mstrModuleName, 46, "Date :")
            Language.setMessage(mstrModuleName, 47, "Date :")
            Language.setMessage(mstrModuleName, 48, "Date :")
            Language.setMessage(mstrModuleName, 49, "Sign :")
            Language.setMessage(mstrModuleName, 50, "Sign :")
            Language.setMessage(mstrModuleName, 51, "Sign :")
            Language.setMessage(mstrModuleName, 52, "Sign :")
            Language.setMessage(mstrModuleName, 53, "Paid Currency :")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
