#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmOrbitReport

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmOrbitReport"
    Private objBankPaymentList As clsOrbitReport
    
    Private mintFirstOpenPeriod As Integer = 0
    Private mstrCompanyBankSwiftCode As String = String.Empty
#End Region

#Region " Constructor "

    Public Sub New()
        objBankPaymentList = New clsOrbitReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objBankPaymentList.SetDefaultValue()
        InitializeComponent()
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim objBank As New clspayrollgroup_master
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Try
            'Anjan [10 June 2015] -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, True)
            dsList = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                          User._Object._Userunkid, _
                                          FinancialYear._Object._YearUnkid, _
                                          Company._Object._Companyunkid, _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                          ConfigParameter._Object._UserAccessModeSetting, _
                                          True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)

            'Anjan [10 June 2015] -- End

            With cboEmployeeName
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
            End With

            dsList = objBank.getListForCombo(enPayrollGroupType.Bank, "List", True)
            With cboBankName
                .ValueMember = "groupmasterunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objCompanyBank.GetComboList(Company._Object._Companyunkid, 1, "Bank")
            With cboCompanyBankName
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("Bank")
                .SelectedValue = 0
            End With

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)
            'Sohail (21 Aug 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = mintFirstOpenPeriod
            End With

            dsList = objExRate.getComboList("Currency", True)
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsList.Tables("Currency")
                .SelectedValue = 0
            End With

            'Sohail (12 Aug 2014) -- Start
            'Enhancement - New Orbit2 Report.
            cboReportType.Items.Clear()
            cboReportType.Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 8, "Orbit"), 1))
            cboReportType.Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 9, "Orbit2"), 2))

            'S.SANDEEP [ 03 OCT 2014 ] -- START
            cboReportType.Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 10, "Orbit3 - Loan"), 3))
            cboReportType.Items.Add(New ComboBoxValue(Language.getMessage(mstrModuleName, 11, "Orbit4 - Advance"), 4))
            'S.SANDEEP [ 03 OCT 2014 ] -- END

            cboReportType.SelectedIndex = 0
            'Sohail (12 Aug 2014) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmp = Nothing
            objBank = Nothing
            objPeriod = Nothing
            objMaster = Nothing
            objExRate = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            'S.SANDEEP [ 03 OCT 2014 ] -- START
            'objBankPaymentList.setDefaultOrderBy(0)
            'txtOrderBy.Text = objBankPaymentList.OrderByDisplay
            'S.SANDEEP [ 03 OCT 2014 ] -- END

            cboReportType.SelectedIndex = 0 'Sohail (12 Aug 2014)
            cboBankName.SelectedValue = 0
            cboBranchName.SelectedValue = 0
            cboCountry.SelectedValue = 0
            cboEmployeeName.SelectedValue = 0
            cboPeriod.SelectedValue = mintFirstOpenPeriod
            txtEmpcode.Text = ""
            txtAmount.Text = ""
            txtAmountTo.Text = ""

            'S.SANDEEP [ 03 OCT 2014 ] -- START
            'objBankPaymentList.setDefaultOrderBy(0)
            objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value))
            txtOrderBy.Text = objBankPaymentList.OrderByDisplay
            'S.SANDEEP [ 03 OCT 2014 ] -- END


            chkIncludeInactiveEmp.Checked = True
            chkIncludeReportColumnHeader.Checked = True

            cboCurrency.SelectedValue = 0

            cboCompanyBankName.SelectedValue = 0
            cboCompanyBranchName.SelectedValue = 0
            chkIncludeInactiveEmp.Checked = True
            chkIncludeInactiveEmp.Visible = False
            cboCompanyAccountNo.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
        End Try
    End Sub

    Private Function SetFilter() As Boolean

        Try
            If CInt(cboCompanyBankName.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Company Bank."), enMsgBoxStyle.Information)
                cboCompanyBankName.Focus()
                Return False
            ElseIf CInt(cboCompanyBranchName.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please select Company Bank Branch."), enMsgBoxStyle.Information)
                cboCompanyBranchName.Focus()
                Return False
            ElseIf CInt(cboCompanyAccountNo.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select Company Bank Account."), enMsgBoxStyle.Information)
                cboCompanyAccountNo.Focus()
                Return False
            ElseIf CInt(cboCompanyAccountType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please select Company Bank Account Type."), enMsgBoxStyle.Information)
                cboCompanyAccountType.Focus()
                Return False
            ElseIf cboPeriod.SelectedValue <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Period is mandatory information.Please select Period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
                'S.SANDEEP [ 03 OCT 2014 ] -- START
                'ElseIf CInt(cboCurrency.SelectedValue) <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Currency is mandatory information. Please select Currency."), enMsgBoxStyle.Information)
                '    cboCurrency.Focus()
                '    Return False
            ElseIf CInt(cboCurrency.SelectedValue) <= 0 AndAlso cboCurrency.Enabled = True Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Currency is mandatory information. Please select Currency."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Return False
                'S.SANDEEP [ 03 OCT 2014 ] -- END
            ElseIf txtBatchName.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Please enter Batch Name. Batch Name is mandatory information."), enMsgBoxStyle.Information)
                txtBatchName.Focus()
                Return False
            End If

            objBankPaymentList.SetDefaultValue()

            'Sohail (12 Aug 2014) -- Start
            'Enhancement - New Orbit2 Report.
            objBankPaymentList._ReportTypeId = CType(cboReportType.SelectedItem, ComboBoxValue).Value
            objBankPaymentList._ReportTypeName = cboReportType.Text
            'Sohail (12 Aug 2014) -- End

            If CInt(cboPeriod.SelectedValue) > 0 Then
                objBankPaymentList._PeriodId = CInt(cboPeriod.SelectedValue)
            End If

            If CInt(cboBankName.SelectedValue) > 0 Then
                objBankPaymentList._BankId = CInt(cboBankName.SelectedValue)
                objBankPaymentList._BankName = cboBankName.Text
            End If

            If CInt(cboBranchName.SelectedValue) > 0 Then
                objBankPaymentList._BranchId = CInt(cboBranchName.SelectedValue)
                objBankPaymentList._BankBranchName = cboBranchName.Text
            End If

            If CInt(cboCountry.SelectedValue) > 0 Then
                objBankPaymentList._CountryId = CInt(cboCountry.SelectedValue)
                objBankPaymentList._CountryName = cboCountry.Text
            End If

            If CInt(cboEmployeeName.SelectedValue) > 0 Then
                objBankPaymentList._EmployeeUnkId = CInt(cboEmployeeName.SelectedValue)
                objBankPaymentList._EmpName = cboEmployeeName.Text
            End If

            If Trim(txtEmpcode.Text) <> "" Then
                objBankPaymentList._EmpCode = txtEmpcode.Text
            End If

            If Trim(txtAmount.Text) <> "" And Trim(txtAmountTo.Text) <> "" Then
                objBankPaymentList._Amount = txtAmount.Text
                objBankPaymentList._AmountTo = txtAmountTo.Text
            End If


            objBankPaymentList._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked
            objBankPaymentList._IncludeReportColumnHeader = chkIncludeReportColumnHeader.Checked

            objBankPaymentList._CurrencyId = CInt(cboCurrency.SelectedValue)
            objBankPaymentList._CurrencySign = cboCurrency.Text
            objBankPaymentList._PeriodName = cboPeriod.Text
            objBankPaymentList._BatchName = txtBatchName.Text.Trim

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End
            objBankPaymentList._PeriodStartDate = objPeriod._Start_Date
            objBankPaymentList._PeriodEndDate = objPeriod._End_Date
            objPeriod = Nothing


            objBankPaymentList._CompanyBranchId = CInt(cboCompanyBranchName.SelectedValue)
            objBankPaymentList._CompanyBankSwiftCode = mstrCompanyBankSwiftCode
            objBankPaymentList._CompanyBankBranchName = cboCompanyBranchName.SelectedText
            objBankPaymentList._CompanyBankAccountNo = cboCompanyAccountNo.Text
            objBankPaymentList._CompanyBankAccountTypeId = CInt(cboCompanyAccountType.SelectedValue)
            objBankPaymentList._CompanyBankAccountTypeName = cboCompanyAccountType.Text
            Dim objAccType As New clsBankAccType
            objAccType._Accounttypeunkid = CInt(cboCompanyAccountType.SelectedValue)
            objBankPaymentList._CompanyBankAccountTypeCode = objAccType._Accounttype_Code
            objAccType = Nothing

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try

    End Function

#End Region

#Region " Form's Events "

    Private Sub frmOrbitReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBankPaymentList = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOrbitReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmOrbitReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Return Then
                Windows.Forms.SendKeys.Send("{TAB}")
            ElseIf e.Control AndAlso e.KeyCode = Windows.Forms.Keys.E Then
                btnExport.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "frmOrbitReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmOrbitReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            OtherSettings()

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOrbitReport_Load", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End

            'Sohail (12 Aug 2014) -- Start
            'Enhancement - New Orbit2 Report.
            'objBankPaymentList.Generate_DetailReport()
            If CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = 1 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objBankPaymentList.Generate_DetailReport()
                objBankPaymentList.Generate_DetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
                'Sohail (21 Aug 2015) -- End
            ElseIf CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = 2 Then
                objBankPaymentList.Generate_DetailReport2(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
                'S.SANDEEP [ 03 OCT 2014 ] -- START
            ElseIf CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = 3 Or CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = 4 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objBankPaymentList.Generate_Orbit3_DetailReport()
                objBankPaymentList.Generate_Orbit3_DetailReport(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, objPeriod._Start_Date, objPeriod._End_Date, ConfigParameter._Object._UserAccessModeSetting, True, chkIncludeInactiveEmp.Checked, True, ConfigParameter._Object._CurrencyFormat, ConfigParameter._Object._Base_CurrencyId, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport)
                'Sohail (21 Aug 2015) -- End
                'S.SANDEEP [ 03 OCT 2014 ] -- END
            End If
            'Sohail (12 Aug 2014) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim frm As New frmCommonSearch
        Try
            frm.DataSource = cboEmployeeName.DataSource
            frm.SelectedValue = cboEmployeeName.SelectedValue
            frm.ValueMember = cboEmployeeName.ValueMember
            frm.DisplayMember = cboEmployeeName.DisplayMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog Then
                cboEmployeeName.SelectedValue = frm.SelectedValue
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnLanguage.Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBankPaymentList.SetMessages()
            objfrm._Other_ModuleNames = "clsBankPaymentList"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "Form_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "ComboBox Event"
    Private Sub cboBankName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankName.SelectedIndexChanged
        Dim objBankBranch As New clsbankbranch_master
        Dim dsList As New DataSet
        Try
            dsList = objBankBranch.getListForCombo("List", True, CInt(cboBankName.SelectedValue))
            With cboBranchName
                .ValueMember = "branchunkid"
                .DisplayMember = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0

            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBankName_SelectedIndexChanged", mstrModuleName)
        Finally
            objBankBranch = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Sub

    Private Sub cboCompanyBankName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCompanyBankName.SelectedIndexChanged
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim dsList As New DataSet
        Try

            dsList = objCompanyBank.GetComboList(Company._Object._Companyunkid, 2, "List", " cfbankbranch_master.bankgroupunkid = " & CInt(cboCompanyBankName.SelectedValue) & " ")
            With cboCompanyBranchName
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            Dim objPayrollGrp As New clspayrollgroup_master
            objPayrollGrp._Groupmasterunkid = CInt(cboCompanyBankName.SelectedValue)
            mstrCompanyBankSwiftCode = objPayrollGrp._Swiftcode
            objPayrollGrp = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCompanyBankName_SelectedIndexChanged", mstrModuleName)
        Finally
            dsList.Dispose()
            dsList = Nothing
        End Try
    End Sub

    Private Sub cboCompanyBranchName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCompanyBranchName.SelectedIndexChanged
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim dsCombos As DataSet
        Try
            dsCombos = objCompanyBank.GetComboListBankAccount("Account", Company._Object._Companyunkid, True, CInt(cboCompanyBankName.SelectedValue), CInt(cboCompanyBranchName.SelectedValue))
            With cboCompanyAccountNo
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Account")
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCompanyBranchName_SelectedIndexChanged", mstrModuleName)
        Finally
            objCompanyBank = Nothing
        End Try
    End Sub

    Private Sub cboCompanyAccountNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCompanyAccountNo.SelectedIndexChanged
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim dsCombos As DataSet
        Dim strFilter As String = ""
        Try
            
            If CInt(cboCompanyAccountNo.SelectedValue) > 0 Then
                strFilter &= " companybanktranunkid = " & CInt(cboCompanyAccountNo.SelectedValue) & " "
            End If
            dsCombos = objCompanyBank.GetComboList(Company._Object._Companyunkid, 3, "AccountType", strFilter)

            With cboCompanyAccountType
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("AccountType")
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCompanyAccountNo_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged _
                                                                                                           , cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            If cboCurrency.Visible = True Then
                If CInt(cboCurrency.SelectedValue) > 0 Then
                    Dim objExRate As New clsExchangeRate
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                    'Sohail (21 Aug 2015) -- End
                    dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                    If dsList.Tables("ExRate").Rows.Count > 0 Then
                    Else
                    End If
                Else
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        Finally
            objPeriod = Nothing
            objCompany = Nothing
        End Try

    End Sub

    'S.SANDEEP [ 03 OCT 2014 ] -- START
    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) > 0 Then
                Select Case CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value)
                    Case 3, 4   'LOAN = 3 , ADVANCE = 4
                        cboCurrency.Enabled = False
                    Case Else
                        cboCurrency.Enabled = True
                End Select
                objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value))
                txtOrderBy.Text = objBankPaymentList.OrderByDisplay
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboReportType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 03 OCT 2014 ] -- END

#End Region

#Region " Other Control's Events "
    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click

        Try
            'S.SANDEEP [ 03 OCT 2014 ] -- START
            'objBankPaymentList.setOrderBy(0)
            objBankPaymentList.setOrderBy(CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value))
            'S.SANDEEP [ 03 OCT 2014 ] -- END

            txtOrderBy.Text = objBankPaymentList.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try

    End Sub

   
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
		
			Call SetLanguage()
			
			Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnReset.GradientBackColor = GUI._ButttonBackColor 
			Me.btnReset.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.objbtnLanguage.GradientBackColor = GUI._ButttonBackColor 
			Me.objbtnLanguage.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
			Me.lblCompanyAccountNo.Text = Language._Object.getCaption(Me.lblCompanyAccountNo.Name, Me.lblCompanyAccountNo.Text)
			Me.lblCompanyBranchName.Text = Language._Object.getCaption(Me.lblCompanyBranchName.Name, Me.lblCompanyBranchName.Text)
			Me.lblCompanyBankName.Text = Language._Object.getCaption(Me.lblCompanyBankName.Name, Me.lblCompanyBankName.Text)
			Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
			Me.chkIncludeInactiveEmp.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmp.Name, Me.chkIncludeInactiveEmp.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblAmountTo.Text = Language._Object.getCaption(Me.lblAmountTo.Name, Me.lblAmountTo.Text)
			Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
			Me.lblBranchName.Text = Language._Object.getCaption(Me.lblBranchName.Name, Me.lblBranchName.Text)
			Me.lblBankName.Text = Language._Object.getCaption(Me.lblBankName.Name, Me.lblBankName.Text)
			Me.lblCountryName.Text = Language._Object.getCaption(Me.lblCountryName.Name, Me.lblCountryName.Text)
			Me.lblEmpCode.Text = Language._Object.getCaption(Me.lblEmpCode.Name, Me.lblEmpCode.Text)
			Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.Name, Me.lblEmployeeName.Text)
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.lblCompanyAccountType.Text = Language._Object.getCaption(Me.lblCompanyAccountType.Name, Me.lblCompanyAccountType.Text)
			Me.lblBatchName.Text = Language._Object.getCaption(Me.lblBatchName.Name, Me.lblBatchName.Text)
			Me.chkIncludeReportColumnHeader.Text = Language._Object.getCaption(Me.chkIncludeReportColumnHeader.Name, Me.chkIncludeReportColumnHeader.Text)
			Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.Name, Me.lblReportType.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Company Bank.")
			Language.setMessage(mstrModuleName, 2, "Please select Company Bank Branch.")
			Language.setMessage(mstrModuleName, 3, "Please select Company Bank Account.")
			Language.setMessage(mstrModuleName, 4, "Please select Company Bank Account Type.")
			Language.setMessage(mstrModuleName, 5, "Period is mandatory information.Please select Period.")
			Language.setMessage(mstrModuleName, 6, "Currency is mandatory information. Please select Currency.")
			Language.setMessage(mstrModuleName, 7, "Please enter Batch Name. Batch Name is mandatory information.")
			Language.setMessage(mstrModuleName, 8, "Orbit")
			Language.setMessage(mstrModuleName, 9, "Orbit2")
			Language.setMessage(mstrModuleName, 10, "Orbit3 - Loan")
			Language.setMessage(mstrModuleName, 11, "Orbit4 - Advance")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
