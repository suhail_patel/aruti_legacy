﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJVLedgerReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbSortBy = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSort = New eZee.Common.eZeeGradientButton
        Me.lblOrderBy = New System.Windows.Forms.Label
        Me.txtOrderBy = New System.Windows.Forms.TextBox
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkSAGEEvolutionJVExport = New System.Windows.Forms.LinkLabel
        Me.lnkSAPStandardJVExport = New System.Windows.Forms.LinkLabel
        Me.objlblToEmpID = New System.Windows.Forms.Label
        Me.objlblFromEmpID = New System.Windows.Forms.Label
        Me.txtToEmpID = New eZee.TextBox.NumericTextBox
        Me.txtFromEmpID = New eZee.TextBox.NumericTextBox
        Me.lnkCargoWiseJVExport = New System.Windows.Forms.LinkLabel
        Me.lnkCoralSunJVExport = New System.Windows.Forms.LinkLabel
        Me.lnkSAGE300ERPImport = New System.Windows.Forms.LinkLabel
        Me.lnkAdvanceFilter = New System.Windows.Forms.LinkLabel
        Me.lnkSAGE300ERPJVExport = New System.Windows.Forms.LinkLabel
        Me.lnkPayrollJournalExport = New System.Windows.Forms.LinkLabel
        Me.lnkPayrollJournalReport = New System.Windows.Forms.LinkLabel
        Me.pnlLoanSchemeList = New System.Windows.Forms.Panel
        Me.objchkSelectAllLoanScheme = New System.Windows.Forms.CheckBox
        Me.dgScheme = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhSchemeCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhScheme = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtSearchScheme = New System.Windows.Forms.TextBox
        Me.lnkFlexCubeLoanBatchExport = New System.Windows.Forms.LinkLabel
        Me.lnkPASTELV2JVExport = New System.Windows.Forms.LinkLabel
        Me.lnkSAGE300JVExport = New System.Windows.Forms.LinkLabel
        Me.lnkSAPECC6_0JVExport = New System.Windows.Forms.LinkLabel
        Me.lnkBRJVPostSQL = New System.Windows.Forms.LinkLabel
        Me.lnkBRJVExport = New System.Windows.Forms.LinkLabel
        Me.lnkFlexCubeJVPostOracle = New System.Windows.Forms.LinkLabel
        Me.lnkShowJVBatchPosting = New System.Windows.Forms.LinkLabel
        Me.lnkFlexCubeJVExport = New System.Windows.Forms.LinkLabel
        Me.chkIncludeEmpCodeNameOnDebit = New System.Windows.Forms.CheckBox
        Me.lnkSAPJVBSOneExport = New System.Windows.Forms.LinkLabel
        Me.lnkFlexCubeUPLDJVExport = New System.Windows.Forms.LinkLabel
        Me.lblPostingDate = New System.Windows.Forms.Label
        Me.dtpPostingDate = New System.Windows.Forms.DateTimePicker
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.cboCostCenter = New System.Windows.Forms.ComboBox
        Me.lnkDynamicsNavJVExport = New System.Windows.Forms.LinkLabel
        Me.lblTranHead = New System.Windows.Forms.Label
        Me.cboTranHead = New System.Windows.Forms.ComboBox
        Me.lnkSunAccountProjectJVExport = New System.Windows.Forms.LinkLabel
        Me.lnkFlexCubeRetailJVExport = New System.Windows.Forms.LinkLabel
        Me.lnkNetSuiteERPJVExport = New System.Windows.Forms.LinkLabel
        Me.lnkXEROJVExport = New System.Windows.Forms.LinkLabel
        Me.txtInvoiceRef = New System.Windows.Forms.TextBox
        Me.lblInvoiceRef = New System.Windows.Forms.Label
        Me.chkShowColumnHeader = New System.Windows.Forms.CheckBox
        Me.lblCustomCCenter = New System.Windows.Forms.Label
        Me.cboCustomCCenter = New System.Windows.Forms.ComboBox
        Me.lnkiScala2JVExport = New System.Windows.Forms.LinkLabel
        Me.lnkSunJV5Export = New System.Windows.Forms.LinkLabel
        Me.gbFilterTBCJV = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.lvTranHead = New eZee.Common.eZeeListView(Me.components)
        Me.colhCheck = New System.Windows.Forms.ColumnHeader
        Me.colhEContribHeadCode = New System.Windows.Forms.ColumnHeader
        Me.colhEContribHead = New System.Windows.Forms.ColumnHeader
        Me.chkIgnoreZero = New System.Windows.Forms.CheckBox
        Me.lnkTBCJVExport = New System.Windows.Forms.LinkLabel
        Me.lnkiScalaJVExport = New System.Windows.Forms.LinkLabel
        Me.lblExRate = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.lblCurrency = New System.Windows.Forms.Label
        Me.lnkSunJVExport = New System.Windows.Forms.LinkLabel
        Me.chkIncludeEmployerContribution = New System.Windows.Forms.CheckBox
        Me.lblCCGroup = New System.Windows.Forms.Label
        Me.cboCCGroup = New System.Windows.Forms.ComboBox
        Me.chkShowGroupByCCGroup = New System.Windows.Forms.CheckBox
        Me.lblBranch = New System.Windows.Forms.Label
        Me.cboBranch = New System.Windows.Forms.ComboBox
        Me.chkShowSummaryNewPage = New System.Windows.Forms.CheckBox
        Me.chkShowSummaryBottom = New System.Windows.Forms.CheckBox
        Me.chkInactiveemp = New System.Windows.Forms.CheckBox
        Me.cboReportType = New System.Windows.Forms.ComboBox
        Me.lblReportType = New System.Windows.Forms.Label
        Me.lblCreditAMountTo = New System.Windows.Forms.Label
        Me.txtCreditAMountTo = New eZee.TextBox.NumericTextBox
        Me.lblCreditAMountFrom = New System.Windows.Forms.Label
        Me.txtCreditAMountFrom = New eZee.TextBox.NumericTextBox
        Me.lblDebitAmountTo = New System.Windows.Forms.Label
        Me.txtDebitAmountTo = New eZee.TextBox.NumericTextBox
        Me.lblDebitAmountFrom = New System.Windows.Forms.Label
        Me.txtDebitAmountFrom = New eZee.TextBox.NumericTextBox
        Me.lblPeriod = New System.Windows.Forms.Label
        Me.cboPeriod = New System.Windows.Forms.ComboBox
        Me.SaveDialog = New System.Windows.Forms.SaveFileDialog
        Me.lnkHakikaBankJVExport = New System.Windows.Forms.LinkLabel
        Me.gbSortBy.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.pnlLoanSchemeList.SuspendLayout()
        CType(Me.dgScheme, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFilterTBCJV.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NavPanel
        '
        Me.NavPanel.Location = New System.Drawing.Point(0, 590)
        Me.NavPanel.Size = New System.Drawing.Size(712, 55)
        Me.NavPanel.TabIndex = 2
        '
        'gbSortBy
        '
        Me.gbSortBy.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gbSortBy.BorderColor = System.Drawing.Color.Black
        Me.gbSortBy.Checked = False
        Me.gbSortBy.CollapseAllExceptThis = False
        Me.gbSortBy.CollapsedHoverImage = Nothing
        Me.gbSortBy.CollapsedNormalImage = Nothing
        Me.gbSortBy.CollapsedPressedImage = Nothing
        Me.gbSortBy.CollapseOnLoad = False
        Me.gbSortBy.Controls.Add(Me.objbtnSort)
        Me.gbSortBy.Controls.Add(Me.lblOrderBy)
        Me.gbSortBy.Controls.Add(Me.txtOrderBy)
        Me.gbSortBy.ExpandedHoverImage = Nothing
        Me.gbSortBy.ExpandedNormalImage = Nothing
        Me.gbSortBy.ExpandedPressedImage = Nothing
        Me.gbSortBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSortBy.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSortBy.HeaderHeight = 25
        Me.gbSortBy.HeaderMessage = ""
        Me.gbSortBy.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbSortBy.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSortBy.HeightOnCollapse = 0
        Me.gbSortBy.LeftTextSpace = 0
        Me.gbSortBy.Location = New System.Drawing.Point(12, 508)
        Me.gbSortBy.Name = "gbSortBy"
        Me.gbSortBy.OpenHeight = 300
        Me.gbSortBy.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSortBy.ShowBorder = True
        Me.gbSortBy.ShowCheckBox = False
        Me.gbSortBy.ShowCollapseButton = False
        Me.gbSortBy.ShowDefaultBorderColor = True
        Me.gbSortBy.ShowDownButton = False
        Me.gbSortBy.ShowHeader = True
        Me.gbSortBy.Size = New System.Drawing.Size(646, 61)
        Me.gbSortBy.TabIndex = 1
        Me.gbSortBy.Temp = 0
        Me.gbSortBy.Text = "Sorting"
        Me.gbSortBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSort
        '
        Me.objbtnSort.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSort.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSort.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSort.BorderSelected = False
        Me.objbtnSort.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objbtnSort.Image = Global.ArutiReports.My.Resources.Resources.sort
        Me.objbtnSort.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSort.Location = New System.Drawing.Point(384, 32)
        Me.objbtnSort.Name = "objbtnSort"
        Me.objbtnSort.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSort.TabIndex = 1
        '
        'lblOrderBy
        '
        Me.lblOrderBy.BackColor = System.Drawing.Color.Transparent
        Me.lblOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrderBy.Location = New System.Drawing.Point(8, 36)
        Me.lblOrderBy.Name = "lblOrderBy"
        Me.lblOrderBy.Size = New System.Drawing.Size(82, 13)
        Me.lblOrderBy.TabIndex = 0
        Me.lblOrderBy.Text = "Sort By"
        '
        'txtOrderBy
        '
        Me.txtOrderBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBy.Location = New System.Drawing.Point(96, 32)
        Me.txtOrderBy.Name = "txtOrderBy"
        Me.txtOrderBy.ReadOnly = True
        Me.txtOrderBy.Size = New System.Drawing.Size(282, 21)
        Me.txtOrderBy.TabIndex = 0
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.AllowDrop = True
        Me.gbFilterCriteria.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gbFilterCriteria.AutoScroll = True
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkHakikaBankJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSAGEEvolutionJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSAPStandardJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.objlblToEmpID)
        Me.gbFilterCriteria.Controls.Add(Me.objlblFromEmpID)
        Me.gbFilterCriteria.Controls.Add(Me.txtToEmpID)
        Me.gbFilterCriteria.Controls.Add(Me.txtFromEmpID)
        Me.gbFilterCriteria.Controls.Add(Me.lnkCargoWiseJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkCoralSunJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSAGE300ERPImport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAdvanceFilter)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSAGE300ERPJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkPayrollJournalExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkPayrollJournalReport)
        Me.gbFilterCriteria.Controls.Add(Me.pnlLoanSchemeList)
        Me.gbFilterCriteria.Controls.Add(Me.lnkFlexCubeLoanBatchExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkPASTELV2JVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSAGE300JVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSAPECC6_0JVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkBRJVPostSQL)
        Me.gbFilterCriteria.Controls.Add(Me.lnkBRJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkFlexCubeJVPostOracle)
        Me.gbFilterCriteria.Controls.Add(Me.lnkShowJVBatchPosting)
        Me.gbFilterCriteria.Controls.Add(Me.lnkFlexCubeJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.chkIncludeEmpCodeNameOnDebit)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSAPJVBSOneExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkFlexCubeUPLDJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lblPostingDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpPostingDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblCostCenter)
        Me.gbFilterCriteria.Controls.Add(Me.cboCostCenter)
        Me.gbFilterCriteria.Controls.Add(Me.lnkDynamicsNavJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lblTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboTranHead)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSunAccountProjectJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkFlexCubeRetailJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkNetSuiteERPJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkXEROJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.txtInvoiceRef)
        Me.gbFilterCriteria.Controls.Add(Me.lblInvoiceRef)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowColumnHeader)
        Me.gbFilterCriteria.Controls.Add(Me.lblCustomCCenter)
        Me.gbFilterCriteria.Controls.Add(Me.cboCustomCCenter)
        Me.gbFilterCriteria.Controls.Add(Me.lnkiScala2JVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSunJV5Export)
        Me.gbFilterCriteria.Controls.Add(Me.gbFilterTBCJV)
        Me.gbFilterCriteria.Controls.Add(Me.chkIgnoreZero)
        Me.gbFilterCriteria.Controls.Add(Me.lnkTBCJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lnkiScalaJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.lblExRate)
        Me.gbFilterCriteria.Controls.Add(Me.cboCurrency)
        Me.gbFilterCriteria.Controls.Add(Me.lblCurrency)
        Me.gbFilterCriteria.Controls.Add(Me.lnkSunJVExport)
        Me.gbFilterCriteria.Controls.Add(Me.chkIncludeEmployerContribution)
        Me.gbFilterCriteria.Controls.Add(Me.lblCCGroup)
        Me.gbFilterCriteria.Controls.Add(Me.cboCCGroup)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowGroupByCCGroup)
        Me.gbFilterCriteria.Controls.Add(Me.lblBranch)
        Me.gbFilterCriteria.Controls.Add(Me.cboBranch)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowSummaryNewPage)
        Me.gbFilterCriteria.Controls.Add(Me.chkShowSummaryBottom)
        Me.gbFilterCriteria.Controls.Add(Me.chkInactiveemp)
        Me.gbFilterCriteria.Controls.Add(Me.cboReportType)
        Me.gbFilterCriteria.Controls.Add(Me.lblReportType)
        Me.gbFilterCriteria.Controls.Add(Me.lblCreditAMountTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtCreditAMountTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblCreditAMountFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtCreditAMountFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblDebitAmountTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtDebitAmountTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblDebitAmountFrom)
        Me.gbFilterCriteria.Controls.Add(Me.txtDebitAmountFrom)
        Me.gbFilterCriteria.Controls.Add(Me.lblPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.cboPeriod)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(677, 436)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSAGEEvolutionJVExport
        '
        Me.lnkSAGEEvolutionJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkSAGEEvolutionJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSAGEEvolutionJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkSAGEEvolutionJVExport.Location = New System.Drawing.Point(40, 363)
        Me.lnkSAGEEvolutionJVExport.Name = "lnkSAGEEvolutionJVExport"
        Me.lnkSAGEEvolutionJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkSAGEEvolutionJVExport.TabIndex = 326
        Me.lnkSAGEEvolutionJVExport.TabStop = True
        Me.lnkSAGEEvolutionJVExport.Text = "&SAGE Evolution JV Export..."
        Me.lnkSAGEEvolutionJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSAPStandardJVExport
        '
        Me.lnkSAPStandardJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkSAPStandardJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSAPStandardJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkSAPStandardJVExport.Location = New System.Drawing.Point(57, 386)
        Me.lnkSAPStandardJVExport.Name = "lnkSAPStandardJVExport"
        Me.lnkSAPStandardJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkSAPStandardJVExport.TabIndex = 324
        Me.lnkSAPStandardJVExport.TabStop = True
        Me.lnkSAPStandardJVExport.Text = "&SAP Standard JV Export..."
        Me.lnkSAPStandardJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objlblToEmpID
        '
        Me.objlblToEmpID.BackColor = System.Drawing.Color.Transparent
        Me.objlblToEmpID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblToEmpID.Location = New System.Drawing.Point(298, 7)
        Me.objlblToEmpID.Name = "objlblToEmpID"
        Me.objlblToEmpID.Size = New System.Drawing.Size(77, 16)
        Me.objlblToEmpID.TabIndex = 322
        Me.objlblToEmpID.Text = "To Emp ID"
        Me.objlblToEmpID.Visible = False
        '
        'objlblFromEmpID
        '
        Me.objlblFromEmpID.BackColor = System.Drawing.Color.Transparent
        Me.objlblFromEmpID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblFromEmpID.Location = New System.Drawing.Point(93, 6)
        Me.objlblFromEmpID.Name = "objlblFromEmpID"
        Me.objlblFromEmpID.Size = New System.Drawing.Size(77, 16)
        Me.objlblFromEmpID.TabIndex = 321
        Me.objlblFromEmpID.Text = "From Emp ID"
        Me.objlblFromEmpID.Visible = False
        '
        'txtToEmpID
        '
        Me.txtToEmpID.AllowNegative = True
        Me.txtToEmpID.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtToEmpID.DigitsInGroup = 0
        Me.txtToEmpID.Flags = 0
        Me.txtToEmpID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtToEmpID.Location = New System.Drawing.Point(379, 4)
        Me.txtToEmpID.MaxDecimalPlaces = 4
        Me.txtToEmpID.MaxWholeDigits = 9
        Me.txtToEmpID.Name = "txtToEmpID"
        Me.txtToEmpID.Prefix = ""
        Me.txtToEmpID.RangeMax = 1.7976931348623157E+308
        Me.txtToEmpID.RangeMin = -1.7976931348623157E+308
        Me.txtToEmpID.Size = New System.Drawing.Size(95, 21)
        Me.txtToEmpID.TabIndex = 320
        Me.txtToEmpID.Text = "0"
        Me.txtToEmpID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtToEmpID.Visible = False
        '
        'txtFromEmpID
        '
        Me.txtFromEmpID.AllowNegative = True
        Me.txtFromEmpID.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtFromEmpID.DigitsInGroup = 0
        Me.txtFromEmpID.Flags = 0
        Me.txtFromEmpID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFromEmpID.Location = New System.Drawing.Point(176, 4)
        Me.txtFromEmpID.MaxDecimalPlaces = 4
        Me.txtFromEmpID.MaxWholeDigits = 9
        Me.txtFromEmpID.Name = "txtFromEmpID"
        Me.txtFromEmpID.Prefix = ""
        Me.txtFromEmpID.RangeMax = 1.7976931348623157E+308
        Me.txtFromEmpID.RangeMin = -1.7976931348623157E+308
        Me.txtFromEmpID.Size = New System.Drawing.Size(116, 21)
        Me.txtFromEmpID.TabIndex = 319
        Me.txtFromEmpID.Text = "0"
        Me.txtFromEmpID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtFromEmpID.Visible = False
        '
        'lnkCargoWiseJVExport
        '
        Me.lnkCargoWiseJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkCargoWiseJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkCargoWiseJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkCargoWiseJVExport.Location = New System.Drawing.Point(18, 388)
        Me.lnkCargoWiseJVExport.Name = "lnkCargoWiseJVExport"
        Me.lnkCargoWiseJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkCargoWiseJVExport.TabIndex = 317
        Me.lnkCargoWiseJVExport.TabStop = True
        Me.lnkCargoWiseJVExport.Text = "&Cargo Wise JV Export..."
        Me.lnkCargoWiseJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkCoralSunJVExport
        '
        Me.lnkCoralSunJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkCoralSunJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkCoralSunJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkCoralSunJVExport.Location = New System.Drawing.Point(29, 370)
        Me.lnkCoralSunJVExport.Name = "lnkCoralSunJVExport"
        Me.lnkCoralSunJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkCoralSunJVExport.TabIndex = 315
        Me.lnkCoralSunJVExport.TabStop = True
        Me.lnkCoralSunJVExport.Text = "&Coral Sun JV Export..."
        Me.lnkCoralSunJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSAGE300ERPImport
        '
        Me.lnkSAGE300ERPImport.BackColor = System.Drawing.Color.Transparent
        Me.lnkSAGE300ERPImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSAGE300ERPImport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkSAGE300ERPImport.Location = New System.Drawing.Point(231, 308)
        Me.lnkSAGE300ERPImport.Name = "lnkSAGE300ERPImport"
        Me.lnkSAGE300ERPImport.Size = New System.Drawing.Size(61, 16)
        Me.lnkSAGE300ERPImport.TabIndex = 313
        Me.lnkSAGE300ERPImport.TabStop = True
        Me.lnkSAGE300ERPImport.Text = "&SAGE 300 ERP Importation Template..."
        Me.lnkSAGE300ERPImport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAdvanceFilter
        '
        Me.lnkAdvanceFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lnkAdvanceFilter.BackColor = System.Drawing.Color.Transparent
        Me.lnkAdvanceFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAdvanceFilter.Location = New System.Drawing.Point(460, 4)
        Me.lnkAdvanceFilter.Name = "lnkAdvanceFilter"
        Me.lnkAdvanceFilter.Size = New System.Drawing.Size(173, 17)
        Me.lnkAdvanceFilter.TabIndex = 311
        Me.lnkAdvanceFilter.TabStop = True
        Me.lnkAdvanceFilter.Text = "Employee Allocation Filter..."
        Me.lnkAdvanceFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lnkSAGE300ERPJVExport
        '
        Me.lnkSAGE300ERPJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkSAGE300ERPJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSAGE300ERPJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkSAGE300ERPJVExport.Location = New System.Drawing.Point(76, 308)
        Me.lnkSAGE300ERPJVExport.Name = "lnkSAGE300ERPJVExport"
        Me.lnkSAGE300ERPJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkSAGE300ERPJVExport.TabIndex = 309
        Me.lnkSAGE300ERPJVExport.TabStop = True
        Me.lnkSAGE300ERPJVExport.Text = "&SAGE 300 ERP JV Export..."
        Me.lnkSAGE300ERPJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkPayrollJournalExport
        '
        Me.lnkPayrollJournalExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkPayrollJournalExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkPayrollJournalExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkPayrollJournalExport.Location = New System.Drawing.Point(428, 36)
        Me.lnkPayrollJournalExport.Name = "lnkPayrollJournalExport"
        Me.lnkPayrollJournalExport.Size = New System.Drawing.Size(142, 16)
        Me.lnkPayrollJournalExport.TabIndex = 307
        Me.lnkPayrollJournalExport.TabStop = True
        Me.lnkPayrollJournalExport.Text = "&Payroll Journal Export..."
        Me.lnkPayrollJournalExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkPayrollJournalReport
        '
        Me.lnkPayrollJournalReport.BackColor = System.Drawing.Color.Transparent
        Me.lnkPayrollJournalReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkPayrollJournalReport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkPayrollJournalReport.Location = New System.Drawing.Point(280, 36)
        Me.lnkPayrollJournalReport.Name = "lnkPayrollJournalReport"
        Me.lnkPayrollJournalReport.Size = New System.Drawing.Size(142, 16)
        Me.lnkPayrollJournalReport.TabIndex = 306
        Me.lnkPayrollJournalReport.TabStop = True
        Me.lnkPayrollJournalReport.Text = "&Payroll Journal Report..."
        Me.lnkPayrollJournalReport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLoanSchemeList
        '
        Me.pnlLoanSchemeList.Controls.Add(Me.objchkSelectAllLoanScheme)
        Me.pnlLoanSchemeList.Controls.Add(Me.dgScheme)
        Me.pnlLoanSchemeList.Controls.Add(Me.txtSearchScheme)
        Me.pnlLoanSchemeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlLoanSchemeList.Location = New System.Drawing.Point(371, 321)
        Me.pnlLoanSchemeList.Name = "pnlLoanSchemeList"
        Me.pnlLoanSchemeList.Size = New System.Drawing.Size(227, 177)
        Me.pnlLoanSchemeList.TabIndex = 304
        '
        'objchkSelectAllLoanScheme
        '
        Me.objchkSelectAllLoanScheme.AutoSize = True
        Me.objchkSelectAllLoanScheme.Location = New System.Drawing.Point(8, 33)
        Me.objchkSelectAllLoanScheme.Name = "objchkSelectAllLoanScheme"
        Me.objchkSelectAllLoanScheme.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAllLoanScheme.TabIndex = 18
        Me.objchkSelectAllLoanScheme.UseVisualStyleBackColor = True
        '
        'dgScheme
        '
        Me.dgScheme.AllowUserToAddRows = False
        Me.dgScheme.AllowUserToDeleteRows = False
        Me.dgScheme.AllowUserToResizeRows = False
        Me.dgScheme.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgScheme.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgScheme.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgScheme.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgScheme.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgScheme.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgcolhunkid, Me.dgColhSchemeCode, Me.dgColhScheme})
        Me.dgScheme.Location = New System.Drawing.Point(1, 28)
        Me.dgScheme.Name = "dgScheme"
        Me.dgScheme.RowHeadersVisible = False
        Me.dgScheme.RowHeadersWidth = 5
        Me.dgScheme.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgScheme.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgScheme.Size = New System.Drawing.Size(223, 146)
        Me.dgScheme.TabIndex = 286
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'objdgcolhunkid
        '
        Me.objdgcolhunkid.HeaderText = "unkid"
        Me.objdgcolhunkid.Name = "objdgcolhunkid"
        Me.objdgcolhunkid.ReadOnly = True
        Me.objdgcolhunkid.Visible = False
        '
        'dgColhSchemeCode
        '
        Me.dgColhSchemeCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.dgColhSchemeCode.HeaderText = "Code"
        Me.dgColhSchemeCode.Name = "dgColhSchemeCode"
        Me.dgColhSchemeCode.ReadOnly = True
        Me.dgColhSchemeCode.Width = 70
        '
        'dgColhScheme
        '
        Me.dgColhScheme.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.dgColhScheme.HeaderText = "Scheme Name"
        Me.dgColhScheme.Name = "dgColhScheme"
        Me.dgColhScheme.ReadOnly = True
        Me.dgColhScheme.Width = 99
        '
        'txtSearchScheme
        '
        Me.txtSearchScheme.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchScheme.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchScheme.Location = New System.Drawing.Point(1, 1)
        Me.txtSearchScheme.Name = "txtSearchScheme"
        Me.txtSearchScheme.Size = New System.Drawing.Size(223, 21)
        Me.txtSearchScheme.TabIndex = 12
        '
        'lnkFlexCubeLoanBatchExport
        '
        Me.lnkFlexCubeLoanBatchExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkFlexCubeLoanBatchExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkFlexCubeLoanBatchExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkFlexCubeLoanBatchExport.Location = New System.Drawing.Point(75, 416)
        Me.lnkFlexCubeLoanBatchExport.Name = "lnkFlexCubeLoanBatchExport"
        Me.lnkFlexCubeLoanBatchExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkFlexCubeLoanBatchExport.TabIndex = 302
        Me.lnkFlexCubeLoanBatchExport.TabStop = True
        Me.lnkFlexCubeLoanBatchExport.Text = "Flex Cube Loan Batch Export..."
        Me.lnkFlexCubeLoanBatchExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkPASTELV2JVExport
        '
        Me.lnkPASTELV2JVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkPASTELV2JVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkPASTELV2JVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkPASTELV2JVExport.Location = New System.Drawing.Point(76, 319)
        Me.lnkPASTELV2JVExport.Name = "lnkPASTELV2JVExport"
        Me.lnkPASTELV2JVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkPASTELV2JVExport.TabIndex = 300
        Me.lnkPASTELV2JVExport.TabStop = True
        Me.lnkPASTELV2JVExport.Text = "&PASTEL V2-JV Export..."
        Me.lnkPASTELV2JVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSAGE300JVExport
        '
        Me.lnkSAGE300JVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkSAGE300JVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSAGE300JVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkSAGE300JVExport.Location = New System.Drawing.Point(75, 339)
        Me.lnkSAGE300JVExport.Name = "lnkSAGE300JVExport"
        Me.lnkSAGE300JVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkSAGE300JVExport.TabIndex = 298
        Me.lnkSAGE300JVExport.TabStop = True
        Me.lnkSAGE300JVExport.Text = "&SAGE 300-JV Export..."
        Me.lnkSAGE300JVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSAPECC6_0JVExport
        '
        Me.lnkSAPECC6_0JVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkSAPECC6_0JVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSAPECC6_0JVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkSAPECC6_0JVExport.Location = New System.Drawing.Point(75, 358)
        Me.lnkSAPECC6_0JVExport.Name = "lnkSAPECC6_0JVExport"
        Me.lnkSAPECC6_0JVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkSAPECC6_0JVExport.TabIndex = 296
        Me.lnkSAPECC6_0JVExport.TabStop = True
        Me.lnkSAPECC6_0JVExport.Text = "&SAP ECC 6.0-JV Export..."
        Me.lnkSAPECC6_0JVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkBRJVPostSQL
        '
        Me.lnkBRJVPostSQL.BackColor = System.Drawing.Color.Transparent
        Me.lnkBRJVPostSQL.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkBRJVPostSQL.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkBRJVPostSQL.Location = New System.Drawing.Point(75, 379)
        Me.lnkBRJVPostSQL.Name = "lnkBRJVPostSQL"
        Me.lnkBRJVPostSQL.Size = New System.Drawing.Size(61, 16)
        Me.lnkBRJVPostSQL.TabIndex = 294
        Me.lnkBRJVPostSQL.TabStop = True
        Me.lnkBRJVPostSQL.Text = "BR-JV Post to S&QL..."
        Me.lnkBRJVPostSQL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkBRJVExport
        '
        Me.lnkBRJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkBRJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkBRJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkBRJVExport.Location = New System.Drawing.Point(75, 396)
        Me.lnkBRJVExport.Name = "lnkBRJVExport"
        Me.lnkBRJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkBRJVExport.TabIndex = 293
        Me.lnkBRJVExport.TabStop = True
        Me.lnkBRJVExport.Text = "&BR-JV Export..."
        Me.lnkBRJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkFlexCubeJVPostOracle
        '
        Me.lnkFlexCubeJVPostOracle.BackColor = System.Drawing.Color.Transparent
        Me.lnkFlexCubeJVPostOracle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkFlexCubeJVPostOracle.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkFlexCubeJVPostOracle.Location = New System.Drawing.Point(75, 413)
        Me.lnkFlexCubeJVPostOracle.Name = "lnkFlexCubeJVPostOracle"
        Me.lnkFlexCubeJVPostOracle.Size = New System.Drawing.Size(61, 16)
        Me.lnkFlexCubeJVPostOracle.TabIndex = 291
        Me.lnkFlexCubeJVPostOracle.TabStop = True
        Me.lnkFlexCubeJVPostOracle.Text = "Flex Cube JV Post to O&racle..."
        Me.lnkFlexCubeJVPostOracle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkShowJVBatchPosting
        '
        Me.lnkShowJVBatchPosting.BackColor = System.Drawing.Color.Transparent
        Me.lnkShowJVBatchPosting.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkShowJVBatchPosting.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkShowJVBatchPosting.Location = New System.Drawing.Point(29, 420)
        Me.lnkShowJVBatchPosting.Name = "lnkShowJVBatchPosting"
        Me.lnkShowJVBatchPosting.Size = New System.Drawing.Size(61, 16)
        Me.lnkShowJVBatchPosting.TabIndex = 292
        Me.lnkShowJVBatchPosting.TabStop = True
        Me.lnkShowJVBatchPosting.Text = "Show JV &Batch Posting..."
        Me.lnkShowJVBatchPosting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkFlexCubeJVExport
        '
        Me.lnkFlexCubeJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkFlexCubeJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkFlexCubeJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkFlexCubeJVExport.Location = New System.Drawing.Point(121, 329)
        Me.lnkFlexCubeJVExport.Name = "lnkFlexCubeJVExport"
        Me.lnkFlexCubeJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkFlexCubeJVExport.TabIndex = 289
        Me.lnkFlexCubeJVExport.TabStop = True
        Me.lnkFlexCubeJVExport.Text = "&Flex Cube JV Export..."
        Me.lnkFlexCubeJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIncludeEmpCodeNameOnDebit
        '
        Me.chkIncludeEmpCodeNameOnDebit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeEmpCodeNameOnDebit.Location = New System.Drawing.Point(438, 321)
        Me.chkIncludeEmpCodeNameOnDebit.Name = "chkIncludeEmpCodeNameOnDebit"
        Me.chkIncludeEmpCodeNameOnDebit.Size = New System.Drawing.Size(220, 16)
        Me.chkIncludeEmpCodeNameOnDebit.TabIndex = 287
        Me.chkIncludeEmpCodeNameOnDebit.Text = "Include Emp. code and Name on Debit"
        Me.chkIncludeEmpCodeNameOnDebit.UseVisualStyleBackColor = True
        Me.chkIncludeEmpCodeNameOnDebit.Visible = False
        '
        'lnkSAPJVBSOneExport
        '
        Me.lnkSAPJVBSOneExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkSAPJVBSOneExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSAPJVBSOneExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkSAPJVBSOneExport.Location = New System.Drawing.Point(8, 404)
        Me.lnkSAPJVBSOneExport.Name = "lnkSAPJVBSOneExport"
        Me.lnkSAPJVBSOneExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkSAPJVBSOneExport.TabIndex = 285
        Me.lnkSAPJVBSOneExport.TabStop = True
        Me.lnkSAPJVBSOneExport.Text = "&SAP JV BS One Export..."
        Me.lnkSAPJVBSOneExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkFlexCubeUPLDJVExport
        '
        Me.lnkFlexCubeUPLDJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkFlexCubeUPLDJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkFlexCubeUPLDJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkFlexCubeUPLDJVExport.Location = New System.Drawing.Point(121, 308)
        Me.lnkFlexCubeUPLDJVExport.Name = "lnkFlexCubeUPLDJVExport"
        Me.lnkFlexCubeUPLDJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkFlexCubeUPLDJVExport.TabIndex = 283
        Me.lnkFlexCubeUPLDJVExport.TabStop = True
        Me.lnkFlexCubeUPLDJVExport.Text = "&Flex Cube UPLD JV Export..."
        Me.lnkFlexCubeUPLDJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPostingDate
        '
        Me.lblPostingDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPostingDate.Location = New System.Drawing.Point(318, 65)
        Me.lblPostingDate.Name = "lblPostingDate"
        Me.lblPostingDate.Size = New System.Drawing.Size(104, 13)
        Me.lblPostingDate.TabIndex = 281
        Me.lblPostingDate.Text = "Posting Date"
        '
        'dtpPostingDate
        '
        Me.dtpPostingDate.Checked = False
        Me.dtpPostingDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPostingDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPostingDate.Location = New System.Drawing.Point(428, 61)
        Me.dtpPostingDate.Name = "dtpPostingDate"
        Me.dtpPostingDate.ShowCheckBox = True
        Me.dtpPostingDate.Size = New System.Drawing.Size(126, 21)
        Me.dtpPostingDate.TabIndex = 280
        '
        'lblCostCenter
        '
        Me.lblCostCenter.BackColor = System.Drawing.Color.Transparent
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(441, 372)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(85, 16)
        Me.lblCostCenter.TabIndex = 262
        Me.lblCostCenter.Text = "Default C. Center"
        '
        'cboCostCenter
        '
        Me.cboCostCenter.DropDownWidth = 230
        Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Location = New System.Drawing.Point(444, 391)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(82, 21)
        Me.cboCostCenter.TabIndex = 261
        '
        'lnkDynamicsNavJVExport
        '
        Me.lnkDynamicsNavJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkDynamicsNavJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkDynamicsNavJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkDynamicsNavJVExport.Location = New System.Drawing.Point(8, 372)
        Me.lnkDynamicsNavJVExport.Name = "lnkDynamicsNavJVExport"
        Me.lnkDynamicsNavJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkDynamicsNavJVExport.TabIndex = 259
        Me.lnkDynamicsNavJVExport.TabStop = True
        Me.lnkDynamicsNavJVExport.Text = "Dynamics Nav JV Export..."
        Me.lnkDynamicsNavJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblTranHead
        '
        Me.lblTranHead.BackColor = System.Drawing.Color.Transparent
        Me.lblTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTranHead.Location = New System.Drawing.Point(441, 324)
        Me.lblTranHead.Name = "lblTranHead"
        Me.lblTranHead.Size = New System.Drawing.Size(85, 16)
        Me.lblTranHead.TabIndex = 257
        Me.lblTranHead.Text = "Transaction Head"
        '
        'cboTranHead
        '
        Me.cboTranHead.DropDownWidth = 230
        Me.cboTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTranHead.FormattingEnabled = True
        Me.cboTranHead.Location = New System.Drawing.Point(444, 343)
        Me.cboTranHead.Name = "cboTranHead"
        Me.cboTranHead.Size = New System.Drawing.Size(82, 21)
        Me.cboTranHead.TabIndex = 256
        '
        'lnkSunAccountProjectJVExport
        '
        Me.lnkSunAccountProjectJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkSunAccountProjectJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSunAccountProjectJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkSunAccountProjectJVExport.Location = New System.Drawing.Point(121, 351)
        Me.lnkSunAccountProjectJVExport.Name = "lnkSunAccountProjectJVExport"
        Me.lnkSunAccountProjectJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkSunAccountProjectJVExport.TabIndex = 254
        Me.lnkSunAccountProjectJVExport.TabStop = True
        Me.lnkSunAccountProjectJVExport.Text = "Sun Account (&Project JV) Export..."
        Me.lnkSunAccountProjectJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkFlexCubeRetailJVExport
        '
        Me.lnkFlexCubeRetailJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkFlexCubeRetailJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkFlexCubeRetailJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkFlexCubeRetailJVExport.Location = New System.Drawing.Point(8, 340)
        Me.lnkFlexCubeRetailJVExport.Name = "lnkFlexCubeRetailJVExport"
        Me.lnkFlexCubeRetailJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkFlexCubeRetailJVExport.TabIndex = 252
        Me.lnkFlexCubeRetailJVExport.TabStop = True
        Me.lnkFlexCubeRetailJVExport.Text = "&Flex Cube Retail JV Export..."
        Me.lnkFlexCubeRetailJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkNetSuiteERPJVExport
        '
        Me.lnkNetSuiteERPJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkNetSuiteERPJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkNetSuiteERPJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkNetSuiteERPJVExport.Location = New System.Drawing.Point(8, 324)
        Me.lnkNetSuiteERPJVExport.Name = "lnkNetSuiteERPJVExport"
        Me.lnkNetSuiteERPJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkNetSuiteERPJVExport.TabIndex = 250
        Me.lnkNetSuiteERPJVExport.TabStop = True
        Me.lnkNetSuiteERPJVExport.Text = "&NetSuite ERP JV Export..."
        Me.lnkNetSuiteERPJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkXEROJVExport
        '
        Me.lnkXEROJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkXEROJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkXEROJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkXEROJVExport.Location = New System.Drawing.Point(8, 308)
        Me.lnkXEROJVExport.Name = "lnkXEROJVExport"
        Me.lnkXEROJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkXEROJVExport.TabIndex = 247
        Me.lnkXEROJVExport.TabStop = True
        Me.lnkXEROJVExport.Text = "&XERO JV Export..."
        Me.lnkXEROJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInvoiceRef
        '
        Me.txtInvoiceRef.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInvoiceRef.Location = New System.Drawing.Point(428, 195)
        Me.txtInvoiceRef.Name = "txtInvoiceRef"
        Me.txtInvoiceRef.Size = New System.Drawing.Size(153, 21)
        Me.txtInvoiceRef.TabIndex = 10
        '
        'lblInvoiceRef
        '
        Me.lblInvoiceRef.BackColor = System.Drawing.Color.Transparent
        Me.lblInvoiceRef.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInvoiceRef.Location = New System.Drawing.Point(315, 197)
        Me.lblInvoiceRef.Name = "lblInvoiceRef"
        Me.lblInvoiceRef.Size = New System.Drawing.Size(107, 16)
        Me.lblInvoiceRef.TabIndex = 245
        Me.lblInvoiceRef.Text = "Invoice Reference"
        '
        'chkShowColumnHeader
        '
        Me.chkShowColumnHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowColumnHeader.Location = New System.Drawing.Point(428, 224)
        Me.chkShowColumnHeader.Name = "chkShowColumnHeader"
        Me.chkShowColumnHeader.Size = New System.Drawing.Size(223, 16)
        Me.chkShowColumnHeader.TabIndex = 12
        Me.chkShowColumnHeader.Text = "Show Column Header"
        Me.chkShowColumnHeader.UseVisualStyleBackColor = True
        '
        'lblCustomCCenter
        '
        Me.lblCustomCCenter.BackColor = System.Drawing.Color.Transparent
        Me.lblCustomCCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCustomCCenter.Location = New System.Drawing.Point(8, 198)
        Me.lblCustomCCenter.Name = "lblCustomCCenter"
        Me.lblCustomCCenter.Size = New System.Drawing.Size(107, 16)
        Me.lblCustomCCenter.TabIndex = 244
        Me.lblCustomCCenter.Text = "Custom C.Center"
        '
        'cboCustomCCenter
        '
        Me.cboCustomCCenter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCustomCCenter.DropDownWidth = 120
        Me.cboCustomCCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCustomCCenter.FormattingEnabled = True
        Me.cboCustomCCenter.Location = New System.Drawing.Point(121, 196)
        Me.cboCustomCCenter.Name = "cboCustomCCenter"
        Me.cboCustomCCenter.Size = New System.Drawing.Size(153, 21)
        Me.cboCustomCCenter.TabIndex = 9
        '
        'lnkiScala2JVExport
        '
        Me.lnkiScala2JVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkiScala2JVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkiScala2JVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkiScala2JVExport.Location = New System.Drawing.Point(8, 288)
        Me.lnkiScala2JVExport.Name = "lnkiScala2JVExport"
        Me.lnkiScala2JVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkiScala2JVExport.TabIndex = 241
        Me.lnkiScala2JVExport.TabStop = True
        Me.lnkiScala2JVExport.Text = "&iScala2 JV Export..."
        Me.lnkiScala2JVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSunJV5Export
        '
        Me.lnkSunJV5Export.BackColor = System.Drawing.Color.Transparent
        Me.lnkSunJV5Export.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSunJV5Export.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkSunJV5Export.Location = New System.Drawing.Point(8, 267)
        Me.lnkSunJV5Export.Name = "lnkSunJV5Export"
        Me.lnkSunJV5Export.Size = New System.Drawing.Size(61, 16)
        Me.lnkSunJV5Export.TabIndex = 239
        Me.lnkSunJV5Export.TabStop = True
        Me.lnkSunJV5Export.Text = "&Sun JV 5 Export..."
        Me.lnkSunJV5Export.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbFilterTBCJV
        '
        Me.gbFilterTBCJV.BorderColor = System.Drawing.Color.Black
        Me.gbFilterTBCJV.Checked = False
        Me.gbFilterTBCJV.CollapseAllExceptThis = False
        Me.gbFilterTBCJV.CollapsedHoverImage = Nothing
        Me.gbFilterTBCJV.CollapsedNormalImage = Nothing
        Me.gbFilterTBCJV.CollapsedPressedImage = Nothing
        Me.gbFilterTBCJV.CollapseOnLoad = False
        Me.gbFilterTBCJV.Controls.Add(Me.Panel1)
        Me.gbFilterTBCJV.ExpandedHoverImage = Nothing
        Me.gbFilterTBCJV.ExpandedNormalImage = Nothing
        Me.gbFilterTBCJV.ExpandedPressedImage = Nothing
        Me.gbFilterTBCJV.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterTBCJV.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterTBCJV.HeaderHeight = 25
        Me.gbFilterTBCJV.HeaderMessage = ""
        Me.gbFilterTBCJV.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterTBCJV.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterTBCJV.HeightOnCollapse = 0
        Me.gbFilterTBCJV.LeftTextSpace = 0
        Me.gbFilterTBCJV.Location = New System.Drawing.Point(121, 374)
        Me.gbFilterTBCJV.Name = "gbFilterTBCJV"
        Me.gbFilterTBCJV.OpenHeight = 300
        Me.gbFilterTBCJV.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterTBCJV.ShowBorder = True
        Me.gbFilterTBCJV.ShowCheckBox = False
        Me.gbFilterTBCJV.ShowCollapseButton = False
        Me.gbFilterTBCJV.ShowDefaultBorderColor = True
        Me.gbFilterTBCJV.ShowDownButton = False
        Me.gbFilterTBCJV.ShowHeader = True
        Me.gbFilterTBCJV.Size = New System.Drawing.Size(314, 121)
        Me.gbFilterTBCJV.TabIndex = 237
        Me.gbFilterTBCJV.Temp = 0
        Me.gbFilterTBCJV.Text = "SAP JV Export Filter"
        Me.gbFilterTBCJV.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.gbFilterTBCJV.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.objchkSelectAll)
        Me.Panel1.Controls.Add(Me.lvTranHead)
        Me.Panel1.Location = New System.Drawing.Point(3, 26)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(308, 94)
        Me.Panel1.TabIndex = 3
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(8, 4)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 19
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'lvTranHead
        '
        Me.lvTranHead.BackColorOnChecked = True
        Me.lvTranHead.CheckBoxes = True
        Me.lvTranHead.ColumnHeaders = Nothing
        Me.lvTranHead.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhCheck, Me.colhEContribHeadCode, Me.colhEContribHead})
        Me.lvTranHead.CompulsoryColumns = ""
        Me.lvTranHead.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvTranHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvTranHead.FullRowSelect = True
        Me.lvTranHead.GridLines = True
        Me.lvTranHead.GroupingColumn = Nothing
        Me.lvTranHead.HideSelection = False
        Me.lvTranHead.Location = New System.Drawing.Point(0, 0)
        Me.lvTranHead.MinColumnWidth = 50
        Me.lvTranHead.MultiSelect = False
        Me.lvTranHead.Name = "lvTranHead"
        Me.lvTranHead.OptionalColumns = ""
        Me.lvTranHead.ShowMoreItem = False
        Me.lvTranHead.ShowSaveItem = False
        Me.lvTranHead.ShowSelectAll = True
        Me.lvTranHead.ShowSizeAllColumnsToFit = True
        Me.lvTranHead.Size = New System.Drawing.Size(308, 94)
        Me.lvTranHead.Sortable = True
        Me.lvTranHead.TabIndex = 0
        Me.lvTranHead.UseCompatibleStateImageBehavior = False
        Me.lvTranHead.View = System.Windows.Forms.View.Details
        '
        'colhCheck
        '
        Me.colhCheck.Tag = "colhCheck"
        Me.colhCheck.Text = ""
        Me.colhCheck.Width = 30
        '
        'colhEContribHeadCode
        '
        Me.colhEContribHeadCode.Tag = "colhEContribHeadCode"
        Me.colhEContribHeadCode.Text = "Code"
        Me.colhEContribHeadCode.Width = 100
        '
        'colhEContribHead
        '
        Me.colhEContribHead.Tag = "colhEContribHead"
        Me.colhEContribHead.Text = "Employer Contribution Head"
        Me.colhEContribHead.Width = 170
        '
        'chkIgnoreZero
        '
        Me.chkIgnoreZero.Checked = True
        Me.chkIgnoreZero.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIgnoreZero.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIgnoreZero.Location = New System.Drawing.Point(428, 246)
        Me.chkIgnoreZero.Name = "chkIgnoreZero"
        Me.chkIgnoreZero.Size = New System.Drawing.Size(223, 17)
        Me.chkIgnoreZero.TabIndex = 14
        Me.chkIgnoreZero.Text = "Ignore Zero"
        Me.chkIgnoreZero.UseVisualStyleBackColor = True
        '
        'lnkTBCJVExport
        '
        Me.lnkTBCJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkTBCJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkTBCJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkTBCJVExport.Location = New System.Drawing.Point(8, 245)
        Me.lnkTBCJVExport.Name = "lnkTBCJVExport"
        Me.lnkTBCJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkTBCJVExport.TabIndex = 233
        Me.lnkTBCJVExport.TabStop = True
        Me.lnkTBCJVExport.Text = "&SAP JV Export..."
        Me.lnkTBCJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkiScalaJVExport
        '
        Me.lnkiScalaJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkiScalaJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkiScalaJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkiScalaJVExport.Location = New System.Drawing.Point(8, 226)
        Me.lnkiScalaJVExport.Name = "lnkiScalaJVExport"
        Me.lnkiScalaJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkiScalaJVExport.TabIndex = 231
        Me.lnkiScalaJVExport.TabStop = True
        Me.lnkiScalaJVExport.Text = "&iScala JV Export..."
        Me.lnkiScalaJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblExRate
        '
        Me.lblExRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExRate.Location = New System.Drawing.Point(362, 91)
        Me.lblExRate.Name = "lblExRate"
        Me.lblExRate.Size = New System.Drawing.Size(184, 14)
        Me.lblExRate.TabIndex = 229
        Me.lblExRate.Text = "#Value"
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.DropDownWidth = 230
        Me.cboCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(121, 88)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(153, 21)
        Me.cboCurrency.TabIndex = 2
        '
        'lblCurrency
        '
        Me.lblCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrency.Location = New System.Drawing.Point(8, 92)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(107, 16)
        Me.lblCurrency.TabIndex = 228
        Me.lblCurrency.Text = "Currency"
        '
        'lnkSunJVExport
        '
        Me.lnkSunJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkSunJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSunJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkSunJVExport.Location = New System.Drawing.Point(121, 288)
        Me.lnkSunJVExport.Name = "lnkSunJVExport"
        Me.lnkSunJVExport.Size = New System.Drawing.Size(190, 16)
        Me.lnkSunJVExport.TabIndex = 223
        Me.lnkSunJVExport.TabStop = True
        Me.lnkSunJVExport.Text = "&Sun JV Export..."
        Me.lnkSunJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkIncludeEmployerContribution
        '
        Me.chkIncludeEmployerContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncludeEmployerContribution.Location = New System.Drawing.Point(121, 245)
        Me.chkIncludeEmployerContribution.Name = "chkIncludeEmployerContribution"
        Me.chkIncludeEmployerContribution.Size = New System.Drawing.Size(259, 16)
        Me.chkIncludeEmployerContribution.TabIndex = 13
        Me.chkIncludeEmployerContribution.Text = "Include Employer Contribution"
        Me.chkIncludeEmployerContribution.UseVisualStyleBackColor = True
        '
        'lblCCGroup
        '
        Me.lblCCGroup.BackColor = System.Drawing.Color.Transparent
        Me.lblCCGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCCGroup.Location = New System.Drawing.Point(315, 170)
        Me.lblCCGroup.Name = "lblCCGroup"
        Me.lblCCGroup.Size = New System.Drawing.Size(107, 16)
        Me.lblCCGroup.TabIndex = 219
        Me.lblCCGroup.Text = "C.Center Group"
        '
        'cboCCGroup
        '
        Me.cboCCGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCCGroup.DropDownWidth = 120
        Me.cboCCGroup.Enabled = False
        Me.cboCCGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCCGroup.FormattingEnabled = True
        Me.cboCCGroup.Location = New System.Drawing.Point(428, 168)
        Me.cboCCGroup.Name = "cboCCGroup"
        Me.cboCCGroup.Size = New System.Drawing.Size(153, 21)
        Me.cboCCGroup.TabIndex = 8
        '
        'chkShowGroupByCCGroup
        '
        Me.chkShowGroupByCCGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowGroupByCCGroup.Location = New System.Drawing.Point(121, 223)
        Me.chkShowGroupByCCGroup.Name = "chkShowGroupByCCGroup"
        Me.chkShowGroupByCCGroup.Size = New System.Drawing.Size(257, 16)
        Me.chkShowGroupByCCGroup.TabIndex = 11
        Me.chkShowGroupByCCGroup.Text = "Show Group By Cost Center Group"
        Me.chkShowGroupByCCGroup.UseVisualStyleBackColor = True
        '
        'lblBranch
        '
        Me.lblBranch.BackColor = System.Drawing.Color.Transparent
        Me.lblBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBranch.Location = New System.Drawing.Point(8, 171)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(107, 16)
        Me.lblBranch.TabIndex = 214
        Me.lblBranch.Text = "Branch"
        '
        'cboBranch
        '
        Me.cboBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBranch.DropDownWidth = 150
        Me.cboBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBranch.FormattingEnabled = True
        Me.cboBranch.Location = New System.Drawing.Point(121, 169)
        Me.cboBranch.Name = "cboBranch"
        Me.cboBranch.Size = New System.Drawing.Size(153, 21)
        Me.cboBranch.TabIndex = 7
        '
        'chkShowSummaryNewPage
        '
        Me.chkShowSummaryNewPage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowSummaryNewPage.Location = New System.Drawing.Point(428, 269)
        Me.chkShowSummaryNewPage.Name = "chkShowSummaryNewPage"
        Me.chkShowSummaryNewPage.Size = New System.Drawing.Size(223, 16)
        Me.chkShowSummaryNewPage.TabIndex = 16
        Me.chkShowSummaryNewPage.Text = "Show Summary Report on New Page"
        Me.chkShowSummaryNewPage.UseVisualStyleBackColor = True
        '
        'chkShowSummaryBottom
        '
        Me.chkShowSummaryBottom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowSummaryBottom.Location = New System.Drawing.Point(121, 267)
        Me.chkShowSummaryBottom.Name = "chkShowSummaryBottom"
        Me.chkShowSummaryBottom.Size = New System.Drawing.Size(259, 16)
        Me.chkShowSummaryBottom.TabIndex = 15
        Me.chkShowSummaryBottom.Text = "Show Summary Report at the Bottom"
        Me.chkShowSummaryBottom.UseVisualStyleBackColor = True
        '
        'chkInactiveemp
        '
        Me.chkInactiveemp.Checked = True
        Me.chkInactiveemp.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkInactiveemp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkInactiveemp.Location = New System.Drawing.Point(428, 291)
        Me.chkInactiveemp.Name = "chkInactiveemp"
        Me.chkInactiveemp.Size = New System.Drawing.Size(223, 16)
        Me.chkInactiveemp.TabIndex = 17
        Me.chkInactiveemp.Text = "Include Inactive Employee"
        Me.chkInactiveemp.UseVisualStyleBackColor = True
        Me.chkInactiveemp.Visible = False
        '
        'cboReportType
        '
        Me.cboReportType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboReportType.DropDownWidth = 120
        Me.cboReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboReportType.FormattingEnabled = True
        Me.cboReportType.Location = New System.Drawing.Point(121, 34)
        Me.cboReportType.Name = "cboReportType"
        Me.cboReportType.Size = New System.Drawing.Size(153, 21)
        Me.cboReportType.TabIndex = 0
        '
        'lblReportType
        '
        Me.lblReportType.BackColor = System.Drawing.Color.Transparent
        Me.lblReportType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReportType.Location = New System.Drawing.Point(8, 36)
        Me.lblReportType.Name = "lblReportType"
        Me.lblReportType.Size = New System.Drawing.Size(107, 16)
        Me.lblReportType.TabIndex = 63
        Me.lblReportType.Text = "Report Type"
        '
        'lblCreditAMountTo
        '
        Me.lblCreditAMountTo.BackColor = System.Drawing.Color.Transparent
        Me.lblCreditAMountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCreditAMountTo.Location = New System.Drawing.Point(362, 143)
        Me.lblCreditAMountTo.Name = "lblCreditAMountTo"
        Me.lblCreditAMountTo.Size = New System.Drawing.Size(60, 15)
        Me.lblCreditAMountTo.TabIndex = 61
        Me.lblCreditAMountTo.Text = "To"
        '
        'txtCreditAMountTo
        '
        Me.txtCreditAMountTo.AllowNegative = True
        Me.txtCreditAMountTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCreditAMountTo.DigitsInGroup = 0
        Me.txtCreditAMountTo.Flags = 0
        Me.txtCreditAMountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCreditAMountTo.Location = New System.Drawing.Point(428, 141)
        Me.txtCreditAMountTo.MaxDecimalPlaces = 4
        Me.txtCreditAMountTo.MaxWholeDigits = 9
        Me.txtCreditAMountTo.Name = "txtCreditAMountTo"
        Me.txtCreditAMountTo.Prefix = ""
        Me.txtCreditAMountTo.RangeMax = 1.7976931348623157E+308
        Me.txtCreditAMountTo.RangeMin = -1.7976931348623157E+308
        Me.txtCreditAMountTo.Size = New System.Drawing.Size(153, 21)
        Me.txtCreditAMountTo.TabIndex = 6
        Me.txtCreditAMountTo.Text = "0"
        Me.txtCreditAMountTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCreditAMountFrom
        '
        Me.lblCreditAMountFrom.BackColor = System.Drawing.Color.Transparent
        Me.lblCreditAMountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCreditAMountFrom.Location = New System.Drawing.Point(8, 144)
        Me.lblCreditAMountFrom.Name = "lblCreditAMountFrom"
        Me.lblCreditAMountFrom.Size = New System.Drawing.Size(107, 16)
        Me.lblCreditAMountFrom.TabIndex = 59
        Me.lblCreditAMountFrom.Text = "Credit Amount From"
        '
        'txtCreditAMountFrom
        '
        Me.txtCreditAMountFrom.AllowNegative = True
        Me.txtCreditAMountFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCreditAMountFrom.DigitsInGroup = 0
        Me.txtCreditAMountFrom.Flags = 0
        Me.txtCreditAMountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCreditAMountFrom.Location = New System.Drawing.Point(121, 142)
        Me.txtCreditAMountFrom.MaxDecimalPlaces = 4
        Me.txtCreditAMountFrom.MaxWholeDigits = 9
        Me.txtCreditAMountFrom.Name = "txtCreditAMountFrom"
        Me.txtCreditAMountFrom.Prefix = ""
        Me.txtCreditAMountFrom.RangeMax = 1.7976931348623157E+308
        Me.txtCreditAMountFrom.RangeMin = -1.7976931348623157E+308
        Me.txtCreditAMountFrom.Size = New System.Drawing.Size(153, 21)
        Me.txtCreditAMountFrom.TabIndex = 5
        Me.txtCreditAMountFrom.Text = "0"
        Me.txtCreditAMountFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDebitAmountTo
        '
        Me.lblDebitAmountTo.BackColor = System.Drawing.Color.Transparent
        Me.lblDebitAmountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDebitAmountTo.Location = New System.Drawing.Point(362, 117)
        Me.lblDebitAmountTo.Name = "lblDebitAmountTo"
        Me.lblDebitAmountTo.Size = New System.Drawing.Size(60, 15)
        Me.lblDebitAmountTo.TabIndex = 56
        Me.lblDebitAmountTo.Text = "To"
        '
        'txtDebitAmountTo
        '
        Me.txtDebitAmountTo.AllowNegative = True
        Me.txtDebitAmountTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDebitAmountTo.DigitsInGroup = 0
        Me.txtDebitAmountTo.Flags = 0
        Me.txtDebitAmountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDebitAmountTo.Location = New System.Drawing.Point(428, 114)
        Me.txtDebitAmountTo.MaxDecimalPlaces = 4
        Me.txtDebitAmountTo.MaxWholeDigits = 9
        Me.txtDebitAmountTo.Name = "txtDebitAmountTo"
        Me.txtDebitAmountTo.Prefix = ""
        Me.txtDebitAmountTo.RangeMax = 1.7976931348623157E+308
        Me.txtDebitAmountTo.RangeMin = -1.7976931348623157E+308
        Me.txtDebitAmountTo.Size = New System.Drawing.Size(153, 21)
        Me.txtDebitAmountTo.TabIndex = 4
        Me.txtDebitAmountTo.Text = "0"
        Me.txtDebitAmountTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDebitAmountFrom
        '
        Me.lblDebitAmountFrom.BackColor = System.Drawing.Color.Transparent
        Me.lblDebitAmountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDebitAmountFrom.Location = New System.Drawing.Point(8, 117)
        Me.lblDebitAmountFrom.Name = "lblDebitAmountFrom"
        Me.lblDebitAmountFrom.Size = New System.Drawing.Size(107, 16)
        Me.lblDebitAmountFrom.TabIndex = 54
        Me.lblDebitAmountFrom.Text = "Debit Amount From"
        '
        'txtDebitAmountFrom
        '
        Me.txtDebitAmountFrom.AllowNegative = True
        Me.txtDebitAmountFrom.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtDebitAmountFrom.DigitsInGroup = 0
        Me.txtDebitAmountFrom.Flags = 0
        Me.txtDebitAmountFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDebitAmountFrom.Location = New System.Drawing.Point(121, 115)
        Me.txtDebitAmountFrom.MaxDecimalPlaces = 4
        Me.txtDebitAmountFrom.MaxWholeDigits = 9
        Me.txtDebitAmountFrom.Name = "txtDebitAmountFrom"
        Me.txtDebitAmountFrom.Prefix = ""
        Me.txtDebitAmountFrom.RangeMax = 1.7976931348623157E+308
        Me.txtDebitAmountFrom.RangeMin = -1.7976931348623157E+308
        Me.txtDebitAmountFrom.Size = New System.Drawing.Size(153, 21)
        Me.txtDebitAmountFrom.TabIndex = 3
        Me.txtDebitAmountFrom.Text = "0"
        Me.txtDebitAmountFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPeriod
        '
        Me.lblPeriod.BackColor = System.Drawing.Color.Transparent
        Me.lblPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriod.Location = New System.Drawing.Point(8, 63)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(107, 16)
        Me.lblPeriod.TabIndex = 27
        Me.lblPeriod.Text = "Period"
        '
        'cboPeriod
        '
        Me.cboPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriod.DropDownWidth = 120
        Me.cboPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(121, 61)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(153, 21)
        Me.cboPeriod.TabIndex = 1
        '
        'lnkHakikaBankJVExport
        '
        Me.lnkHakikaBankJVExport.BackColor = System.Drawing.Color.Transparent
        Me.lnkHakikaBankJVExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkHakikaBankJVExport.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.lnkHakikaBankJVExport.Location = New System.Drawing.Point(231, 329)
        Me.lnkHakikaBankJVExport.Name = "lnkHakikaBankJVExport"
        Me.lnkHakikaBankJVExport.Size = New System.Drawing.Size(61, 16)
        Me.lnkHakikaBankJVExport.TabIndex = 328
        Me.lnkHakikaBankJVExport.TabStop = True
        Me.lnkHakikaBankJVExport.Text = "&Hakika Bank JV Export..."
        Me.lnkHakikaBankJVExport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmJVLedgerReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(712, 645)
        Me.Controls.Add(Me.gbSortBy)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmJVLedgerReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Journal Voucher Ledger"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.Controls.SetChildIndex(Me.gbSortBy, 0)
        Me.gbSortBy.ResumeLayout(False)
        Me.gbSortBy.PerformLayout()
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.pnlLoanSchemeList.ResumeLayout(False)
        Me.pnlLoanSchemeList.PerformLayout()
        CType(Me.dgScheme, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFilterTBCJV.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbSortBy As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSort As eZee.Common.eZeeGradientButton
    Private WithEvents lblOrderBy As System.Windows.Forms.Label
    Private WithEvents txtOrderBy As System.Windows.Forms.TextBox
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Private WithEvents lblDebitAmountTo As System.Windows.Forms.Label
    Public WithEvents txtDebitAmountTo As eZee.TextBox.NumericTextBox
    Private WithEvents lblDebitAmountFrom As System.Windows.Forms.Label
    Public WithEvents txtDebitAmountFrom As eZee.TextBox.NumericTextBox
    Private WithEvents lblPeriod As System.Windows.Forms.Label
    Public WithEvents cboPeriod As System.Windows.Forms.ComboBox
    Private WithEvents lblCreditAMountTo As System.Windows.Forms.Label
    Public WithEvents txtCreditAMountTo As eZee.TextBox.NumericTextBox
    Private WithEvents lblCreditAMountFrom As System.Windows.Forms.Label
    Public WithEvents txtCreditAMountFrom As eZee.TextBox.NumericTextBox
    Public WithEvents cboReportType As System.Windows.Forms.ComboBox
    Private WithEvents lblReportType As System.Windows.Forms.Label
    Friend WithEvents chkInactiveemp As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowSummaryBottom As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowSummaryNewPage As System.Windows.Forms.CheckBox
    Private WithEvents lblBranch As System.Windows.Forms.Label
    Public WithEvents cboBranch As System.Windows.Forms.ComboBox
    Friend WithEvents chkShowGroupByCCGroup As System.Windows.Forms.CheckBox
    Private WithEvents lblCCGroup As System.Windows.Forms.Label
    Public WithEvents cboCCGroup As System.Windows.Forms.ComboBox
    Friend WithEvents chkIncludeEmployerContribution As System.Windows.Forms.CheckBox
    Friend WithEvents lnkSunJVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents SaveDialog As System.Windows.Forms.SaveFileDialog
    Friend WithEvents lblExRate As System.Windows.Forms.Label
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblCurrency As System.Windows.Forms.Label
    Friend WithEvents lnkiScalaJVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkTBCJVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents chkIgnoreZero As System.Windows.Forms.CheckBox
    Friend WithEvents gbFilterTBCJV As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lvTranHead As eZee.Common.eZeeListView
    Friend WithEvents colhCheck As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhEContribHead As System.Windows.Forms.ColumnHeader
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents colhEContribHeadCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents lnkSunJV5Export As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkiScala2JVExport As System.Windows.Forms.LinkLabel
    Private WithEvents lblCustomCCenter As System.Windows.Forms.Label
    Public WithEvents cboCustomCCenter As System.Windows.Forms.ComboBox
    Friend WithEvents chkShowColumnHeader As System.Windows.Forms.CheckBox
    Private WithEvents lblInvoiceRef As System.Windows.Forms.Label
    Friend WithEvents txtInvoiceRef As System.Windows.Forms.TextBox
    Friend WithEvents lnkXEROJVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkNetSuiteERPJVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkFlexCubeRetailJVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkSunAccountProjectJVExport As System.Windows.Forms.LinkLabel
    Private WithEvents lblTranHead As System.Windows.Forms.Label
    Public WithEvents cboTranHead As System.Windows.Forms.ComboBox
    Friend WithEvents lnkDynamicsNavJVExport As System.Windows.Forms.LinkLabel
    Private WithEvents lblCostCenter As System.Windows.Forms.Label
    Public WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents lblPostingDate As System.Windows.Forms.Label
    Friend WithEvents dtpPostingDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lnkFlexCubeUPLDJVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkSAPJVBSOneExport As System.Windows.Forms.LinkLabel
    Friend WithEvents chkIncludeEmpCodeNameOnDebit As System.Windows.Forms.CheckBox
    Friend WithEvents lnkFlexCubeJVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkFlexCubeJVPostOracle As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkBRJVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkBRJVPostSQL As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkSAPECC6_0JVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkSAGE300JVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkPASTELV2JVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkShowJVBatchPosting As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkFlexCubeLoanBatchExport As System.Windows.Forms.LinkLabel
    Friend WithEvents pnlLoanSchemeList As System.Windows.Forms.Panel
    Friend WithEvents objchkSelectAllLoanScheme As System.Windows.Forms.CheckBox
    Friend WithEvents dgScheme As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhSchemeCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhScheme As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents txtSearchScheme As System.Windows.Forms.TextBox
    Friend WithEvents lnkPayrollJournalExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkPayrollJournalReport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkSAGE300ERPJVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkAdvanceFilter As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkSAGE300ERPImport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkCoralSunJVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkCargoWiseJVExport As System.Windows.Forms.LinkLabel
    Private WithEvents objlblToEmpID As System.Windows.Forms.Label
    Private WithEvents objlblFromEmpID As System.Windows.Forms.Label
    Public WithEvents txtToEmpID As eZee.TextBox.NumericTextBox
    Public WithEvents txtFromEmpID As eZee.TextBox.NumericTextBox
    Friend WithEvents lnkSAPStandardJVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkSAGEEvolutionJVExport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnkHakikaBankJVExport As System.Windows.Forms.LinkLabel
End Class
