#Region " Imports "
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports
Imports Aruti.Data
Imports ArutiReport
#End Region


Public Class frmBankSummaryReport

#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmBankSummaryReport"
    Private objBankSummary As clsBankSummaryReport
#End Region

#Region "Constructor"
    Public Sub New()
        objBankSummary = New clsBankSummaryReport(User._Object._Languageunkid,Company._Object._Companyunkid)
        objBankSummary.SetDefaultValue()
        InitializeComponent()
    End Sub
#End Region

#Region "Public Functions"
    Public Sub FillCombo()
        Try
            'Vimal (27 Nov 2010) -- Start 
            cboBank.DataSource = (New clspayrollgroup_master).getListForCombo(enPayrollGroupType.Bank, , True).Tables(0)
            'Vimal (27 Nov 2010) -- End
            cboBank.ValueMember = "groupmasterunkid"
            cboBank.DisplayMember = "name"

            cboBranch.DataSource = (New clsbankbranch_master).getListForCombo(, True).Tables(0)
            cboBranch.ValueMember = "branchunkid"
            cboBranch.DisplayMember = "name"

            'Vimal (30 Nov 2010) -- Start 
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'cboPeriod.DataSource = (New clscommom_period_Tran).getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True).Tables(0)
            cboPeriod.DataSource = (New clscommom_period_Tran).getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True).Tables(0)
            'Sohail (21 Aug 2015) -- End
            cboPeriod.ValueMember = "periodunkid"
            cboPeriod.DisplayMember = "name"
            'Vimal (30 Nov 2010) -- End

            'Sohail (16 Dec 2011) -- Start
            With cboCurrency
                .DataSource = (New clsExchangeRate).getComboList("Currency", True).Tables(0)
                'Sohail (03 Sep 2012) -- Start
                'TRA - ENHANCEMENT
                '.ValueMember = "exchangerateunkid"
                .ValueMember = "countryunkid"
                'Sohail (03 Sep 2012) -- End
                .DisplayMember = "currency_name"
                .SelectedValue = 0
            End With
            'Sohail (16 Dec 2011) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            'Vimal (30 Nov 2010) -- Start 
            cboPeriod.SelectedValue = 0
            'Vimal (30 Nov 2010) -- End
            cboBank.SelectedValue = 0
            cboBranch.SelectedValue = 0
            txtAmountPaidFrom.Text = ""
            txtAmountPaidTo.Text = ""

            'Vimal (10 Dec 2010) -- Start 
            'objBankSummary.setDefaultOrderBy(cboBank.SelectedIndex)
            objBankSummary.setDefaultOrderBy(0)
            'Vimal (10 Dec 2010) -- End

            'Anjan [14 Mar 2014] -- Start
            'ENHANCEMENT : Requested by Rutta
            chkShowCountry.Checked = False
            'Anjan [14 Mar 2014 ] -- End
            chkShowAddress.Checked = False 'Sohail (24 Mar 2014)

            txtOrderBy.Text = objBankSummary.OrderByDisplay

            cboCurrency.SelectedValue = 0 'Sohail (16 Dec 2011)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetValues", mstrModuleName)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            'Sohail (16 Dec 2011) -- Start
            'TRA - Multi currency Payment List, for Bank Payment and Cash Payment List.
            If CInt(cboCurrency.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Currency is mandatory information. Please select Currency."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Exit Function
            End If
            'Sohail (16 Dec 2011) -- End

            objBankSummary.SetDefaultValue()

            objBankSummary._BankId = cboBank.SelectedValue
            objBankSummary._BankName = cboBank.Text

            objBankSummary._BranchId = cboBranch.SelectedValue
            objBankSummary._BranchName = cboBranch.Text

            objBankSummary._AmountFrom = txtAmountPaidFrom.Text
            objBankSummary._AmountTo = txtAmountPaidTo.Text

            'Vimal (30 Nov 2010) -- Start 
            objBankSummary._PeriodId = cboPeriod.SelectedValue
            objBankSummary._PeriodName = cboPeriod.Text
            'Vimal (30 Nov 2010) -- End

            'Sohail (16 Dec 2011) -- Start
            objBankSummary._CurrencyId = CInt(cboCurrency.SelectedValue)
            objBankSummary._CurrencyName = cboCurrency.Text
            'Sohail (16 Dec 2011) -- End


            'Anjan [14 Mar 2014] -- Start
            'ENHANCEMENT : Requested by Rutta
            objBankSummary._ShowCountry = chkShowCountry.Checked
            'Anjan [14 Mar 2014 ] -- End

            objBankSummary._ShowAddress = chkShowAddress.Checked 'Sohail (24 Mar 2014)

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFilter", mstrModuleName)
        End Try
    End Function
#End Region


#Region "Form's Events"

   
    Private Sub frmBankSummaryReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objBankSummary = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankSummaryReport_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBankSummaryReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Language.setLanguage(Me.Name)

            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes
            OtherSettings()
            'Pinkal (03-Sep-2012) -- End

            Me._Message = objBankSummary._ReportDesc
            Me._Title = objBankSummary._ReportName

            Call FillCombo()
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankSummaryReport_Load", mstrModuleName)
        End Try
    End Sub


    Private Sub frmBankSummaryReport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control Then
                If e.KeyCode = Windows.Forms.Keys.R Then
                    Call frmBankSummaryReport_Report_Click(Nothing, Nothing)
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankSummaryReport_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBankSummaryReport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            Select Case e.KeyChar
                Case CChar(ChrW(CInt(Windows.Forms.Keys.Enter)))
                    Windows.Forms.SendKeys.Send("{TAB}")
                    e.Handled = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankSummaryReport_KeyPress", mstrModuleName)
        End Try
    End Sub

   
#End Region

#Region "Buttons"

    Private Sub frmBankSummaryReport_Report_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Report_Click
        Try
            If SetFilter() = False Then Exit Sub

            'Vimal (27 Nov 2010) -- Start 
            'objBankSummary.generateReport(Aruti.Data.enReportType.Standard, enPrintAction.Preview, enExportAction.None)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objBankSummary.generateReport(0, e.Type, enExportAction.None)
            objBankSummary.generateReportNew(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._ExportReportPath, ConfigParameter._Object._OpenAfterExport, 0, e.Type, enExportAction.None, ConfigParameter._Object._Base_CurrencyId)
            'Sohail (21 Aug 2015) -- End
            'Vimal (27 Nov 2010) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankSummaryReport_Report_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBankSummaryReport_Export_Click(ByVal sender As Object, ByVal e As Aruti.Data.PrintButtonEventArgs) Handles Me.Export_Click
        Try
            If SetFilter() = False Then Exit Sub

            'Vimal (27 Nov 2010) -- Start 
            'objBankSummary.generateReport(Aruti.Data.enReportType.Standard, enPrintAction.None, e.Type)
            objBankSummary.generateReport(0, enPrintAction.None, e.Type)
            'Vimal (27 Nov 2010) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankSummaryReport_Export_Click", mstrModuleName)
        End Try


    End Sub

    Private Sub frmBankSummaryReport_Reset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Reset_Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankSummaryReport_Reset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub frmBankSummaryReport_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Cancel_Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmBankSummaryReport_Cancel_Click", mstrModuleName)
        End Try
    End Sub



    'Pinkal (03-Sep-2012) -- Start
    'Enhancement : TRA Changes

    Private Sub frmBankSummaryReport_Language_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Language_Click
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsBankSummaryReport.SetMessages()
            objfrm._Other_ModuleNames = "clsBankSummaryReport"
            objfrm.displayDialog(Me)

            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "frmBankSummaryReport_Language_Click", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub


    'Pinkal (03-Sep-2012) -- End


#End Region


#Region " Controls "
    Private Sub objbtnSort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSort.Click
        Try
            'Vimal (10 Dec 2010) -- Start 
            'objBankSummary.setOrderBy(cboBank.SelectedIndex)
            objBankSummary.setOrderBy(0)
            'Vimal (10 Dec 2010) -- End
            txtOrderBy.Text = objBankSummary.OrderByDisplay
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "objbtnSort_Click", mstrModuleName)
        End Try
    End Sub
#End Region
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbSortBy.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSortBy.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbSortBy.Text = Language._Object.getCaption(Me.gbSortBy.Name, Me.gbSortBy.Text)
			Me.lblOrderBy.Text = Language._Object.getCaption(Me.lblOrderBy.Name, Me.lblOrderBy.Text)
			Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
			Me.lblBankName.Text = Language._Object.getCaption(Me.lblBankName.Name, Me.lblBankName.Text)
			Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
			Me.lblAmountPaidTo.Text = Language._Object.getCaption(Me.lblAmountPaidTo.Name, Me.lblAmountPaidTo.Text)
			Me.lblAmountPaidFrom.Text = Language._Object.getCaption(Me.lblAmountPaidFrom.Name, Me.lblAmountPaidFrom.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Currency is mandatory information. Please select Currency.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
