﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGrievanceDetailReport
    Inherits Aruti.Data.frmBaseReportForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkSetAnalysis = New System.Windows.Forms.LinkLabel
        Me.chkshowcommiteemem = New System.Windows.Forms.CheckBox
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblstatus = New System.Windows.Forms.Label
        Me.txtRefno = New System.Windows.Forms.TextBox
        Me.lblrefno = New System.Windows.Forms.Label
        Me.objbtnSearchagainst = New eZee.Common.eZeeGradientButton
        Me.cboAgainstEmployee = New System.Windows.Forms.ComboBox
        Me.lblagainstemployee = New System.Windows.Forms.Label
        Me.objbtnSearchfrmemp = New eZee.Common.eZeeGradientButton
        Me.cboFromEmployee = New System.Windows.Forms.ComboBox
        Me.lblfromemployee = New System.Windows.Forms.Label
        Me.dttodate = New System.Windows.Forms.DateTimePicker
        Me.dtfromdate = New System.Windows.Forms.DateTimePicker
        Me.lbltoDate = New System.Windows.Forms.Label
        Me.lblfromdate = New System.Windows.Forms.Label
        Me.gbFilterCriteria.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkSetAnalysis)
        Me.gbFilterCriteria.Controls.Add(Me.chkshowcommiteemem)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblstatus)
        Me.gbFilterCriteria.Controls.Add(Me.txtRefno)
        Me.gbFilterCriteria.Controls.Add(Me.lblrefno)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchagainst)
        Me.gbFilterCriteria.Controls.Add(Me.cboAgainstEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblagainstemployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchfrmemp)
        Me.gbFilterCriteria.Controls.Add(Me.cboFromEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblfromemployee)
        Me.gbFilterCriteria.Controls.Add(Me.dttodate)
        Me.gbFilterCriteria.Controls.Add(Me.dtfromdate)
        Me.gbFilterCriteria.Controls.Add(Me.lbltoDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblfromdate)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(8, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(444, 179)
        Me.gbFilterCriteria.TabIndex = 18
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkSetAnalysis
        '
        Me.lnkSetAnalysis.BackColor = System.Drawing.Color.Transparent
        Me.lnkSetAnalysis.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkSetAnalysis.Location = New System.Drawing.Point(347, 4)
        Me.lnkSetAnalysis.Name = "lnkSetAnalysis"
        Me.lnkSetAnalysis.Size = New System.Drawing.Size(94, 17)
        Me.lnkSetAnalysis.TabIndex = 19
        Me.lnkSetAnalysis.TabStop = True
        Me.lnkSetAnalysis.Text = "Analysis By"
        Me.lnkSetAnalysis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkshowcommiteemem
        '
        Me.chkshowcommiteemem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkshowcommiteemem.Location = New System.Drawing.Point(239, 119)
        Me.chkshowcommiteemem.Name = "chkshowcommiteemem"
        Me.chkshowcommiteemem.Size = New System.Drawing.Size(160, 17)
        Me.chkshowcommiteemem.TabIndex = 111
        Me.chkshowcommiteemem.Text = "Show Commitee Members"
        Me.chkshowcommiteemem.UseVisualStyleBackColor = True
        Me.chkshowcommiteemem.Visible = False
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(124, 144)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(275, 21)
        Me.cboStatus.TabIndex = 110
        '
        'lblstatus
        '
        Me.lblstatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblstatus.Location = New System.Drawing.Point(8, 146)
        Me.lblstatus.Name = "lblstatus"
        Me.lblstatus.Size = New System.Drawing.Size(110, 17)
        Me.lblstatus.TabIndex = 109
        Me.lblstatus.Text = "Status"
        Me.lblstatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRefno
        '
        Me.txtRefno.Location = New System.Drawing.Point(124, 117)
        Me.txtRefno.Name = "txtRefno"
        Me.txtRefno.Size = New System.Drawing.Size(106, 21)
        Me.txtRefno.TabIndex = 108
        '
        'lblrefno
        '
        Me.lblrefno.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblrefno.Location = New System.Drawing.Point(8, 119)
        Me.lblrefno.Name = "lblrefno"
        Me.lblrefno.Size = New System.Drawing.Size(110, 17)
        Me.lblrefno.TabIndex = 107
        Me.lblrefno.Text = "Ref No."
        Me.lblrefno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchagainst
        '
        Me.objbtnSearchagainst.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchagainst.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchagainst.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchagainst.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchagainst.BorderSelected = False
        Me.objbtnSearchagainst.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchagainst.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchagainst.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchagainst.Location = New System.Drawing.Point(405, 89)
        Me.objbtnSearchagainst.Name = "objbtnSearchagainst"
        Me.objbtnSearchagainst.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchagainst.TabIndex = 106
        '
        'cboAgainstEmployee
        '
        Me.cboAgainstEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAgainstEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAgainstEmployee.FormattingEnabled = True
        Me.cboAgainstEmployee.Location = New System.Drawing.Point(124, 89)
        Me.cboAgainstEmployee.Name = "cboAgainstEmployee"
        Me.cboAgainstEmployee.Size = New System.Drawing.Size(275, 21)
        Me.cboAgainstEmployee.TabIndex = 105
        '
        'lblagainstemployee
        '
        Me.lblagainstemployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblagainstemployee.Location = New System.Drawing.Point(8, 91)
        Me.lblagainstemployee.Name = "lblagainstemployee"
        Me.lblagainstemployee.Size = New System.Drawing.Size(110, 17)
        Me.lblagainstemployee.TabIndex = 104
        Me.lblagainstemployee.Text = "Against Employee"
        Me.lblagainstemployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchfrmemp
        '
        Me.objbtnSearchfrmemp.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchfrmemp.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchfrmemp.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchfrmemp.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchfrmemp.BorderSelected = False
        Me.objbtnSearchfrmemp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchfrmemp.Image = Global.ArutiReports.My.Resources.Resources.Mini_Search
        Me.objbtnSearchfrmemp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchfrmemp.Location = New System.Drawing.Point(405, 62)
        Me.objbtnSearchfrmemp.Name = "objbtnSearchfrmemp"
        Me.objbtnSearchfrmemp.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchfrmemp.TabIndex = 102
        '
        'cboFromEmployee
        '
        Me.cboFromEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFromEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFromEmployee.FormattingEnabled = True
        Me.cboFromEmployee.Location = New System.Drawing.Point(124, 62)
        Me.cboFromEmployee.Name = "cboFromEmployee"
        Me.cboFromEmployee.Size = New System.Drawing.Size(275, 21)
        Me.cboFromEmployee.TabIndex = 101
        '
        'lblfromemployee
        '
        Me.lblfromemployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfromemployee.Location = New System.Drawing.Point(8, 64)
        Me.lblfromemployee.Name = "lblfromemployee"
        Me.lblfromemployee.Size = New System.Drawing.Size(110, 17)
        Me.lblfromemployee.TabIndex = 100
        Me.lblfromemployee.Text = "From Employee"
        Me.lblfromemployee.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dttodate
        '
        Me.dttodate.Checked = False
        Me.dttodate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dttodate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dttodate.Location = New System.Drawing.Point(293, 34)
        Me.dttodate.Name = "dttodate"
        Me.dttodate.ShowCheckBox = True
        Me.dttodate.Size = New System.Drawing.Size(106, 21)
        Me.dttodate.TabIndex = 99
        '
        'dtfromdate
        '
        Me.dtfromdate.Checked = False
        Me.dtfromdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtfromdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtfromdate.Location = New System.Drawing.Point(124, 34)
        Me.dtfromdate.Name = "dtfromdate"
        Me.dtfromdate.ShowCheckBox = True
        Me.dtfromdate.Size = New System.Drawing.Size(106, 21)
        Me.dtfromdate.TabIndex = 98
        '
        'lbltoDate
        '
        Me.lbltoDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltoDate.Location = New System.Drawing.Point(236, 36)
        Me.lbltoDate.Name = "lbltoDate"
        Me.lbltoDate.Size = New System.Drawing.Size(51, 17)
        Me.lbltoDate.TabIndex = 97
        Me.lbltoDate.Text = "To"
        Me.lbltoDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblfromdate
        '
        Me.lblfromdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfromdate.Location = New System.Drawing.Point(8, 36)
        Me.lblfromdate.Name = "lblfromdate"
        Me.lblfromdate.Size = New System.Drawing.Size(110, 17)
        Me.lblfromdate.TabIndex = 57
        Me.lblfromdate.Text = "From Date"
        Me.lblfromdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmGrievanceDetailReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(655, 452)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Name = "frmGrievanceDetailReport"
        Me.Text = "frmGrievanceDetailReport"
        Me.Controls.SetChildIndex(Me.NavPanel, 0)
        Me.Controls.SetChildIndex(Me.gbFilterCriteria, 0)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblfromdate As System.Windows.Forms.Label
    Friend WithEvents lbltoDate As System.Windows.Forms.Label
    Friend WithEvents dtfromdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dttodate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblfromemployee As System.Windows.Forms.Label
    Friend WithEvents cboFromEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchfrmemp As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchagainst As eZee.Common.eZeeGradientButton
    Friend WithEvents cboAgainstEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblagainstemployee As System.Windows.Forms.Label
    Friend WithEvents lblrefno As System.Windows.Forms.Label
    Friend WithEvents txtRefno As System.Windows.Forms.TextBox
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblstatus As System.Windows.Forms.Label
    Friend WithEvents chkshowcommiteemem As System.Windows.Forms.CheckBox
    Friend WithEvents lnkSetAnalysis As System.Windows.Forms.LinkLabel
End Class
