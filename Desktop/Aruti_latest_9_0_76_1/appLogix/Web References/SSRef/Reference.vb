﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.9151
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System
Imports System.ComponentModel
Imports System.Diagnostics
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Xml.Serialization

'
'This source code was auto-generated by Microsoft.VSDesigner, Version 2.0.50727.9151.
'
Namespace SSRef
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.9149"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Web.Services.WebServiceBindingAttribute(Name:="ArutiFileTransferServiceSoap", [Namespace]:="http://www.arutihr.com/")>  _
    Partial Public Class ArutiFileTransferService
        Inherits System.Web.Services.Protocols.SoapHttpClientProtocol
        
        Private userCredentialsValueField As UserCredentials
        
        Private DownloadFileOperationCompleted As System.Threading.SendOrPostCallback
        
        Private UploadFileOperationCompleted As System.Threading.SendOrPostCallback
        
        Private UpdateFileOperationCompleted As System.Threading.SendOrPostCallback
        
        Private DeleteFileOperationCompleted As System.Threading.SendOrPostCallback
        
        Private useDefaultCredentialsSetExplicitly As Boolean
        
        '''<remarks/>
        Public Sub New()
            MyBase.New
            Me.Url = Global.Aruti.Data.My.MySettings.Default.appLogix_SSRef_ArutiFileTransferService
            If (Me.IsLocalFileSystemWebService(Me.Url) = true) Then
                Me.UseDefaultCredentials = true
                Me.useDefaultCredentialsSetExplicitly = false
            Else
                Me.useDefaultCredentialsSetExplicitly = true
            End If
        End Sub
        
        Public Property UserCredentialsValue() As UserCredentials
            Get
                Return Me.userCredentialsValueField
            End Get
            Set
                Me.userCredentialsValueField = value
            End Set
        End Property
        
        Public Shadows Property Url() As String
            Get
                Return MyBase.Url
            End Get
            Set
                If (((Me.IsLocalFileSystemWebService(MyBase.Url) = true)  _
                            AndAlso (Me.useDefaultCredentialsSetExplicitly = false))  _
                            AndAlso (Me.IsLocalFileSystemWebService(value) = false)) Then
                    MyBase.UseDefaultCredentials = false
                End If
                MyBase.Url = value
            End Set
        End Property
        
        Public Shadows Property UseDefaultCredentials() As Boolean
            Get
                Return MyBase.UseDefaultCredentials
            End Get
            Set
                MyBase.UseDefaultCredentials = value
                Me.useDefaultCredentialsSetExplicitly = true
            End Set
        End Property
        
        '''<remarks/>
        Public Event DownloadFileCompleted As DownloadFileCompletedEventHandler
        
        '''<remarks/>
        Public Event UploadFileCompleted As UploadFileCompletedEventHandler
        
        '''<remarks/>
        Public Event UpdateFileCompleted As UpdateFileCompletedEventHandler
        
        '''<remarks/>
        Public Event DeleteFileCompleted As DeleteFileCompletedEventHandler
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapHeaderAttribute("UserCredentialsValue"),  _
         System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.arutihr.com/DownloadFile", RequestNamespace:="http://www.arutihr.com/", ResponseNamespace:="http://www.arutihr.com/", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function DownloadFile(ByVal strFolderName As String, ByVal strFileName As String, ByRef strErrorMessage As String) As <System.Xml.Serialization.XmlElementAttribute(DataType:="base64Binary")> Byte()
            Dim results() As Object = Me.Invoke("DownloadFile", New Object() {strFolderName, strFileName, strErrorMessage})
            strErrorMessage = CType(results(1),String)
            Return CType(results(0),Byte())
        End Function
        
        '''<remarks/>
        Public Overloads Sub DownloadFileAsync(ByVal strFolderName As String, ByVal strFileName As String, ByVal strErrorMessage As String)
            Me.DownloadFileAsync(strFolderName, strFileName, strErrorMessage, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub DownloadFileAsync(ByVal strFolderName As String, ByVal strFileName As String, ByVal strErrorMessage As String, ByVal userState As Object)
            If (Me.DownloadFileOperationCompleted Is Nothing) Then
                Me.DownloadFileOperationCompleted = AddressOf Me.OnDownloadFileOperationCompleted
            End If
            Me.InvokeAsync("DownloadFile", New Object() {strFolderName, strFileName, strErrorMessage}, Me.DownloadFileOperationCompleted, userState)
        End Sub
        
        Private Sub OnDownloadFileOperationCompleted(ByVal arg As Object)
            If (Not (Me.DownloadFileCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent DownloadFileCompleted(Me, New DownloadFileCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapHeaderAttribute("UserCredentialsValue"),  _
         System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.arutihr.com/UploadFile", RequestNamespace:="http://www.arutihr.com/", ResponseNamespace:="http://www.arutihr.com/", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function UploadFile(<System.Xml.Serialization.XmlElementAttribute(DataType:="base64Binary")> ByVal filebytes() As Byte, ByVal strFolderName As String, ByVal strFileName As String, ByRef strErrorMessage As String) As Boolean
            Dim results() As Object = Me.Invoke("UploadFile", New Object() {filebytes, strFolderName, strFileName, strErrorMessage})
            strErrorMessage = CType(results(1),String)
            Return CType(results(0),Boolean)
        End Function
        
        '''<remarks/>
        Public Overloads Sub UploadFileAsync(ByVal filebytes() As Byte, ByVal strFolderName As String, ByVal strFileName As String, ByVal strErrorMessage As String)
            Me.UploadFileAsync(filebytes, strFolderName, strFileName, strErrorMessage, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub UploadFileAsync(ByVal filebytes() As Byte, ByVal strFolderName As String, ByVal strFileName As String, ByVal strErrorMessage As String, ByVal userState As Object)
            If (Me.UploadFileOperationCompleted Is Nothing) Then
                Me.UploadFileOperationCompleted = AddressOf Me.OnUploadFileOperationCompleted
            End If
            Me.InvokeAsync("UploadFile", New Object() {filebytes, strFolderName, strFileName, strErrorMessage}, Me.UploadFileOperationCompleted, userState)
        End Sub
        
        Private Sub OnUploadFileOperationCompleted(ByVal arg As Object)
            If (Not (Me.UploadFileCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent UploadFileCompleted(Me, New UploadFileCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapHeaderAttribute("UserCredentialsValue"),  _
         System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.arutihr.com/UpdateFile", RequestNamespace:="http://www.arutihr.com/", ResponseNamespace:="http://www.arutihr.com/", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function UpdateFile(ByVal strOldPath As String, ByVal strNewpath As String, ByVal strFolderName As String, ByRef strErrorMessage As String) As Boolean
            Dim results() As Object = Me.Invoke("UpdateFile", New Object() {strOldPath, strNewpath, strFolderName, strErrorMessage})
            strErrorMessage = CType(results(1),String)
            Return CType(results(0),Boolean)
        End Function
        
        '''<remarks/>
        Public Overloads Sub UpdateFileAsync(ByVal strOldPath As String, ByVal strNewpath As String, ByVal strFolderName As String, ByVal strErrorMessage As String)
            Me.UpdateFileAsync(strOldPath, strNewpath, strFolderName, strErrorMessage, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub UpdateFileAsync(ByVal strOldPath As String, ByVal strNewpath As String, ByVal strFolderName As String, ByVal strErrorMessage As String, ByVal userState As Object)
            If (Me.UpdateFileOperationCompleted Is Nothing) Then
                Me.UpdateFileOperationCompleted = AddressOf Me.OnUpdateFileOperationCompleted
            End If
            Me.InvokeAsync("UpdateFile", New Object() {strOldPath, strNewpath, strFolderName, strErrorMessage}, Me.UpdateFileOperationCompleted, userState)
        End Sub
        
        Private Sub OnUpdateFileOperationCompleted(ByVal arg As Object)
            If (Not (Me.UpdateFileCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent UpdateFileCompleted(Me, New UpdateFileCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        <System.Web.Services.Protocols.SoapHeaderAttribute("UserCredentialsValue"),  _
         System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.arutihr.com/DeleteFile", RequestNamespace:="http://www.arutihr.com/", ResponseNamespace:="http://www.arutihr.com/", Use:=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle:=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)>  _
        Public Function DeleteFile(ByVal strFolderName As String, ByVal strFileName As String, ByRef strErrorMessage As String) As Boolean
            Dim results() As Object = Me.Invoke("DeleteFile", New Object() {strFolderName, strFileName, strErrorMessage})
            strErrorMessage = CType(results(1),String)
            Return CType(results(0),Boolean)
        End Function
        
        '''<remarks/>
        Public Overloads Sub DeleteFileAsync(ByVal strFolderName As String, ByVal strFileName As String, ByVal strErrorMessage As String)
            Me.DeleteFileAsync(strFolderName, strFileName, strErrorMessage, Nothing)
        End Sub
        
        '''<remarks/>
        Public Overloads Sub DeleteFileAsync(ByVal strFolderName As String, ByVal strFileName As String, ByVal strErrorMessage As String, ByVal userState As Object)
            If (Me.DeleteFileOperationCompleted Is Nothing) Then
                Me.DeleteFileOperationCompleted = AddressOf Me.OnDeleteFileOperationCompleted
            End If
            Me.InvokeAsync("DeleteFile", New Object() {strFolderName, strFileName, strErrorMessage}, Me.DeleteFileOperationCompleted, userState)
        End Sub
        
        Private Sub OnDeleteFileOperationCompleted(ByVal arg As Object)
            If (Not (Me.DeleteFileCompletedEvent) Is Nothing) Then
                Dim invokeArgs As System.Web.Services.Protocols.InvokeCompletedEventArgs = CType(arg,System.Web.Services.Protocols.InvokeCompletedEventArgs)
                RaiseEvent DeleteFileCompleted(Me, New DeleteFileCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState))
            End If
        End Sub
        
        '''<remarks/>
        Public Shadows Sub CancelAsync(ByVal userState As Object)
            MyBase.CancelAsync(userState)
        End Sub
        
        Private Function IsLocalFileSystemWebService(ByVal url As String) As Boolean
            If ((url Is Nothing)  _
                        OrElse (url Is String.Empty)) Then
                Return false
            End If
            Dim wsUri As System.Uri = New System.Uri(url)
            If ((wsUri.Port >= 1024)  _
                        AndAlso (String.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) = 0)) Then
                Return true
            End If
            Return false
        End Function
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "2.0.50727.9149"),  _
     System.SerializableAttribute(),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code"),  _
     System.Xml.Serialization.XmlTypeAttribute([Namespace]:="http://www.arutihr.com/"),  _
     System.Xml.Serialization.XmlRootAttribute([Namespace]:="http://www.arutihr.com/", IsNullable:=false)>  _
    Partial Public Class UserCredentials
        Inherits System.Web.Services.Protocols.SoapHeader
        
        Private passwordField As String
        
        Private webClientIDField As Integer
        
        Private compCodeField As String
        
        Private authenticationCodeField As String
        
        Private anyAttrField() As System.Xml.XmlAttribute
        
        '''<remarks/>
        Public Property Password() As String
            Get
                Return Me.passwordField
            End Get
            Set
                Me.passwordField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property WebClientID() As Integer
            Get
                Return Me.webClientIDField
            End Get
            Set
                Me.webClientIDField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property CompCode() As String
            Get
                Return Me.compCodeField
            End Get
            Set
                Me.compCodeField = value
            End Set
        End Property
        
        '''<remarks/>
        Public Property AuthenticationCode() As String
            Get
                Return Me.authenticationCodeField
            End Get
            Set
                Me.authenticationCodeField = value
            End Set
        End Property
        
        '''<remarks/>
        <System.Xml.Serialization.XmlAnyAttributeAttribute()>  _
        Public Property AnyAttr() As System.Xml.XmlAttribute()
            Get
                Return Me.anyAttrField
            End Get
            Set
                Me.anyAttrField = value
            End Set
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.9149")>  _
    Public Delegate Sub DownloadFileCompletedEventHandler(ByVal sender As Object, ByVal e As DownloadFileCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.9149"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class DownloadFileCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As Byte()
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),Byte())
            End Get
        End Property
        
        '''<remarks/>
        Public ReadOnly Property strErrorMessage() As String
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(1),String)
            End Get
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.9149")>  _
    Public Delegate Sub UploadFileCompletedEventHandler(ByVal sender As Object, ByVal e As UploadFileCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.9149"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class UploadFileCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As Boolean
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),Boolean)
            End Get
        End Property
        
        '''<remarks/>
        Public ReadOnly Property strErrorMessage() As String
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(1),String)
            End Get
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.9149")>  _
    Public Delegate Sub UpdateFileCompletedEventHandler(ByVal sender As Object, ByVal e As UpdateFileCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.9149"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class UpdateFileCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As Boolean
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),Boolean)
            End Get
        End Property
        
        '''<remarks/>
        Public ReadOnly Property strErrorMessage() As String
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(1),String)
            End Get
        End Property
    End Class
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.9149")>  _
    Public Delegate Sub DeleteFileCompletedEventHandler(ByVal sender As Object, ByVal e As DeleteFileCompletedEventArgs)
    
    '''<remarks/>
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "2.0.50727.9149"),  _
     System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.ComponentModel.DesignerCategoryAttribute("code")>  _
    Partial Public Class DeleteFileCompletedEventArgs
        Inherits System.ComponentModel.AsyncCompletedEventArgs
        
        Private results() As Object
        
        Friend Sub New(ByVal results() As Object, ByVal exception As System.Exception, ByVal cancelled As Boolean, ByVal userState As Object)
            MyBase.New(exception, cancelled, userState)
            Me.results = results
        End Sub
        
        '''<remarks/>
        Public ReadOnly Property Result() As Boolean
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(0),Boolean)
            End Get
        End Property
        
        '''<remarks/>
        Public ReadOnly Property strErrorMessage() As String
            Get
                Me.RaiseExceptionIfNecessary
                Return CType(Me.results(1),String)
            End Get
        End Property
    End Class
End Namespace
