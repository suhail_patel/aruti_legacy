﻿'************************************************************************************************************************************
'Class Name : clsCfcommon_master.vb
'Purpose    :
'Date       :13/10/2017
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports System.Data

#End Region

Public Class clsSQLFilterCollection
    Private Shared ReadOnly mstrModuleName As String = "clsSQLFilterCollection"

#Region " Private Variables "

    Private mstrQuery As String = String.Empty
    Private mstrParameterName As String = String.Empty
    Private mSQLDbType As SqlDbType
    Private minteZeeDataType As Integer = 0
    Private mobjParameterValue As Object = Nothing

#End Region

#Region " Constructor "

    Public Sub New()

    End Sub

    Public Sub New(ByVal strQuery As String, ByVal strParameterName As String, ByVal oSQLDbType As SqlDbType, ByVal inteZeeDataType As Integer, ByVal objParameterValue As Object)
        Try
            mstrQuery = strQuery
            mstrParameterName = strParameterName
            mSQLDbType = oSQLDbType
            minteZeeDataType = inteZeeDataType
            mobjParameterValue = objParameterValue

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Properties "

    Public Property _Query() As String
        Get
            Return mstrQuery
        End Get
        Set(ByVal value As String)
            mstrQuery = value
        End Set
    End Property

    Public Property _ParameterName() As String
        Get
            Return mstrParameterName
        End Get
        Set(ByVal value As String)
            mstrParameterName = value
        End Set
    End Property

    Public Property _SQLDbType() As SqlDbType
        Get
            Return mSQLDbType
        End Get
        Set(ByVal value As SqlDbType)
            mSQLDbType = value
        End Set
    End Property

    Public Property _eZeeDataType() As Integer
        Get
            Return minteZeeDataType
        End Get
        Set(ByVal value As Integer)
            minteZeeDataType = value
        End Set
    End Property

    Public Property _ParameterValue() As Object
        Get
            Return mobjParameterValue
        End Get
        Set(ByVal value As Object)
            mobjParameterValue = value
        End Set
    End Property

#End Region

End Class
