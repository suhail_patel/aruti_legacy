﻿'************************************************************************************************************************************
'Class Name : clshearing_schedule_Tran.vb
'Purpose    :
'Date       : 13/06/2016
'Written By : Nilay
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System.Text

''' <summary>
''' Purpose: 
''' Developer: Nilay
''' </summary>
Public Class clshearing_schedule_Tran
    Private Shared ReadOnly mstrModuleName As String = "clshearing_schedule_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintHearningscheduletranunkid As Integer
    Private mintHearingschedulemasterunkid As Integer
    Private mdtTran As DataTable
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private mstrWebFormName As String = String.Empty
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Nilay
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _HeatingScheduleTranTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hearningscheduletranunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Hearningscheduletranunkid() As Integer
        Get
            Return mintHearningscheduletranunkid
        End Get
        Set(ByVal value As Integer)
            mintHearningscheduletranunkid = value
            'Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hearingschedulemasterunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Hearingschedulemasterunkid(ByVal mdtEffectiveDate As Date) As Integer
        Get
            Return mintHearingschedulemasterunkid
        End Get
        Set(ByVal value As Integer)
            mintHearingschedulemasterunkid = value
            Call GetData(mdtEffectiveDate)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webclientip
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webhostname
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webformname
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New()
        mdtTran = New DataTable("Tran")
        Dim dCol As DataColumn
        Try
            With mdtTran
                dCol = New DataColumn
                With dCol
                    .ColumnName = "hearningscheduletranunkid"
                    .DataType = GetType(System.Int32)
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "hearingschedulemasterunkid"
                    .DataType = GetType(System.Int32)
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "committeetranunkid"
                    .DataType = GetType(System.Int32)
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "isnotified"
                    .DataType = GetType(System.Boolean)
                    .DefaultValue = False
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "notify_date"
                    .DataType = GetType(System.DateTime)
                    .DefaultValue = Nothing
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "committeemasterunkid"
                    .DataType = GetType(System.Int32)
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "employeeunkid"
                    .DataType = GetType(System.Int32)
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "isvoid"
                    .DataType = GetType(System.Boolean)
                    .DefaultValue = False
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "voiduserunkid"
                    .DataType = GetType(System.Int32)
                    .DefaultValue = 0
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "voiddatetime"
                    .DataType = GetType(System.DateTime)
                    .DefaultValue = Nothing
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "voidreason"
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "AUD"
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "GUID"
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                'S.SANDEEP [24 MAY 2016] -- Start
                'Email Notification
                dCol = New DataColumn
                With dCol
                    .ColumnName = "email"
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "member_name"
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                .Columns.Add(dCol)
                'S.SANDEEP [24 MAY 2016] -- End

            End With

            mdtTran.Columns.Add("ex_name", GetType(String)).DefaultValue = ""
            mdtTran.Columns.Add("ex_company", GetType(String)).DefaultValue = ""
            mdtTran.Columns.Add("ex_department", GetType(String)).DefaultValue = ""
            mdtTran.Columns.Add("ex_contactno", GetType(String)).DefaultValue = ""
            mdtTran.Columns.Add("ex_email", GetType(String)).DefaultValue = ""

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Private Sub GetData(ByVal mdtEffectiveDate As Date)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'strQ = "SELECT " & _
            '          "  hrhearing_schedule_tran.hearningscheduletranunkid " & _
            '          ", hrhearing_schedule_tran.hearingschedulemasterunkid " & _
            '          ", hrhearing_schedule_tran.committeetranunkid " & _
            '          ", hrhearing_schedule_tran.isnotified " & _
            '          ", hrhearing_schedule_tran.notify_date " & _
            '          ", hrdiscipline_committee.committeemasterunkid " & _
            '          ", hrdiscipline_committee.employeeunkid " & _
            '          ", hrhearing_schedule_tran.isvoid " & _
            '          ", hrhearing_schedule_tran.voiduserunkid " & _
            '          ", hrhearing_schedule_tran.voiddatetime " & _
            '          ", hrhearing_schedule_tran.voidreason " & _
            '          ", '' AS AUD " & _
            '          ", CASE WHEN hrdiscipline_committee.employeeunkid <=0 THEN hrdiscipline_committee.ex_email " & _
            '          "       WHEN hrdiscipline_committee.employeeunkid > 0 THEN hremployee_master.email " & _
            '          "  END AS email " & _
            '          ", CASE WHEN hrdiscipline_committee.employeeunkid <=0 THEN hrdiscipline_committee.ex_name " & _
            '          "       WHEN hrdiscipline_committee.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') " & _
            '          "  END AS member_name " & _
            '       "FROM hrhearing_schedule_tran " & _
            '          " LEFT JOIN hrdiscipline_committee ON hrdiscipline_committee.committeetranunkid = hrhearing_schedule_tran.committeetranunkid " & _
            '          " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrdiscipline_committee.employeeunkid " & _
            '       "WHERE hearingschedulemasterunkid = @hearingschedulemasterunkid " & _
            '          " AND isvoid = 0 "

            strQ = "SELECT " & _
                      "hearningscheduletranunkid " & _
                     ",hrhearing_schedule_master.hearingschedulemasterunkid " & _
                     ",ISNULL(hrdiscipline_committee.committeetranunkid,0) AS committeetranunkid " & _
                     ",CASE WHEN ISNULL(committeemasterunkid,0) <= 0 THEN  ISNULL(hrhearing_schedule_tran.committeemstunkid,0) ELSE ISNULL(committeemasterunkid,0) END committeemasterunkid " & _
                     ",ISNULL(cfcommon_master.name,'') AS committee " & _
                     ",'' AS csv_investigator " & _
                     ",ISNULL(cm.masterunkid,0) AS memcategoryunkid " & _
                     ",ISNULL(cm.name,'') AS members_category " & _
                     ",hrhearing_schedule_tran.isvoid " & _
                     ",hrhearing_schedule_tran.voiduserunkid " & _
                     ",hrhearing_schedule_tran.voiddatetime " & _
                     ",hrhearing_schedule_tran.voidreason " & _
                     ",'' AS AUD " & _
                     ",'' AS GUID " & _
                ",ISNULL(CASE WHEN hrhearing_schedule_tran.committeetranunkid > 0 THEN " & _
                                "CASE WHEN hrdiscipline_committee.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') " & _
                                        "WHEN hrdiscipline_committee.employeeunkid <= 0 THEN hrdiscipline_committee.ex_name " & _
                                "END " & _
                                "WHEN hrhearing_schedule_tran.committeetranunkid <= 0 THEN " & _
                                        "CASE WHEN hrhearing_schedule_tran.employeeunkid > 0 THEN ISNULL(ExEmp.firstname,'')+' '+ISNULL(ExEmp.surname,'') " & _
                                        "ELSE hrhearing_schedule_tran.ex_name END " & _
                    "END,'') AS members " & _
                ",ISNULL(CASE WHEN hrhearing_schedule_tran.committeetranunkid > 0 THEN " & _
                                "CASE WHEN hrdiscipline_committee.employeeunkid > 0 THEN ISNULL(hrdepartment_master.name,'') " & _
                                        "WHEN hrdiscipline_committee.employeeunkid <= 0 THEN hrdiscipline_committee.ex_department " & _
                                "END " & _
                                "WHEN hrhearing_schedule_tran.committeetranunkid <= 0 THEN " & _
                                        "CASE WHEN hrhearing_schedule_tran.employeeunkid > 0 THEN ISNULL(ExEmpDt.name,'') " & _
                                        "ELSE hrhearing_schedule_tran.ex_department END " & _
                " " & _
                    "END,'') AS department " & _
                ",ISNULL(CASE WHEN hrhearing_schedule_tran.committeetranunkid > 0 THEN " & _
                                "CASE WHEN hrdiscipline_committee.employeeunkid > 0 THEN cfcompany_master.name " & _
                                        "WHEN hrdiscipline_committee.employeeunkid <= 0 THEN hrdiscipline_committee.ex_company " & _
                                "END " & _
                                "WHEN hrhearing_schedule_tran.committeetranunkid <= 0 THEN " & _
                                        "CASE WHEN hrhearing_schedule_tran.employeeunkid > 0 THEN ExEmpCo.name " & _
                                        "ELSE hrhearing_schedule_tran.ex_company END " & _
                " " & _
                    "END,'') AS company " & _
                ",ISNULL(CASE WHEN hrhearing_schedule_tran.committeetranunkid > 0 THEN " & _
                                "CASE WHEN hrdiscipline_committee.employeeunkid > 0 THEN hremployee_master.present_tel_no " & _
                                        "WHEN hrdiscipline_committee.employeeunkid <= 0 THEN hrdiscipline_committee.ex_contactno " & _
                                "END " & _
                                "WHEN hrhearing_schedule_tran.committeetranunkid <= 0 THEN " & _
                                        "CASE WHEN hrhearing_schedule_tran.employeeunkid > 0 THEN ExEmp.present_tel_no " & _
                                        "ELSE hrhearing_schedule_tran.ex_contactno END " & _
                    "END,'') AS contactno " & _
                ",ISNULL(CASE WHEN hrhearing_schedule_tran.committeetranunkid > 0 THEN " & _
                                "CASE WHEN hrdiscipline_committee.employeeunkid > 0 THEN hremployee_master.email " & _
                                        "WHEN hrdiscipline_committee.employeeunkid <= 0 THEN hrdiscipline_committee.ex_email " & _
                                "END " & _
                                "WHEN hrhearing_schedule_tran.committeetranunkid <= 0 THEN " & _
                                        "CASE WHEN hrhearing_schedule_tran.employeeunkid > 0 THEN ExEmp.email " & _
                                        "ELSE hrhearing_schedule_tran.ex_email END " & _
                    "END,'') AS email " & _
                ",CASE WHEN hrhearing_schedule_tran.committeetranunkid > 0 THEN hrdiscipline_committee.employeeunkid " & _
                    "ELSE ISNULL(hrhearing_schedule_tran.employeeunkid,0) END AS employeeunkid " & _
                ",hrhearing_schedule_tran.ex_name " & _
                ",hrhearing_schedule_tran.ex_company " & _
                ",hrhearing_schedule_tran.ex_department " & _
                ",hrhearing_schedule_tran.ex_contactno " & _
                ",hrhearing_schedule_tran.ex_email " & _
                ",hrhearing_schedule_tran.isnotified " & _
                ",hrhearing_schedule_tran.notify_date " & _
                ",'' AS member_name " & _
                   "FROM hrhearing_schedule_tran " & _
                "LEFT JOIN hrhearing_schedule_master ON hrhearing_schedule_master.hearingschedulemasterunkid = hrhearing_schedule_tran.hearingschedulemasterunkid " & _
                "LEFT JOIN hrdiscipline_committee ON hrdiscipline_committee.committeetranunkid = hrhearing_schedule_tran.committeetranunkid " & _
                "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdiscipline_committee.committeemasterunkid " & _
                "LEFT JOIN cfcommon_master AS cm ON cm.masterunkid = hrdiscipline_committee.mcategoryunkid " & _
                "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrdiscipline_committee.employeeunkid " & _
                "LEFT JOIN hrmsConfiguration..cfcompany_master ON cfcompany_master.companyunkid = hremployee_master.companyunkid " & _
                "LEFT JOIN hremployee_master AS ExEmp ON ExEmp.employeeunkid = hrhearing_schedule_tran.employeeunkid " & _
                "LEFT JOIN hrmsConfiguration..cfcompany_master AS ExEmpCo ON ExEmpCo.companyunkid = ExEmp.companyunkid " & _
                "LEFT JOIN " & _
                "( " & _
                    "SELECT " & _
                         "hremployee_transfer_tran.employeeunkid " & _
                        ",hremployee_transfer_tran.departmentunkid " & _
                        ",ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                    "FROM hremployee_transfer_tran " & _
                    "WHERE hremployee_transfer_tran.isvoid = 0 AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(mdtEffectiveDate) & "' " & _
                ") AS A ON A.employeeunkid = hremployee_master.employeeunkid AND A.rno = 1 " & _
                "LEFT JOIN hrdepartment_master ON A.departmentunkid = hrdepartment_master.departmentunkid " & _
                "LEFT JOIN " & _
                "( " & _
                    "SELECT " & _
                            "hremployee_transfer_tran.employeeunkid " & _
                        ",hremployee_transfer_tran.departmentunkid " & _
                        ",ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                    "FROM hremployee_transfer_tran " & _
                    "WHERE hremployee_transfer_tran.isvoid = 0 AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(mdtEffectiveDate) & "' " & _
                ") AS B ON B.employeeunkid = ExEmp.employeeunkid AND B.rno = 1 " & _
                "LEFT JOIN hrdepartment_master As ExEmpDt ON B.departmentunkid = ExEmpDt.departmentunkid " & _
                "WHERE hrhearing_schedule_tran.hearingschedulemasterunkid = @hearingschedulemasterunkid AND hrhearing_schedule_tran.isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@hearingschedulemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHearingschedulemasterunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()
            If dsList.Tables(0).Rows.Count > 0 Then
                mdtTran = dsList.Tables(0).Copy
                'For Each dtRow As DataRow In dsList.Tables(0).Rows
                '    mdtTran.ImportRow(dtRow)
                'Next
            Else
                mdtTran = dsList.Tables(0).Clone
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getCommitteeMasterId(ByVal inthearingschedulemasterid As Integer) As Integer
        Dim strQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            Dim intCommitteeMasterId As Integer = 0

            strQ = "SELECT DISTINCT " & _
                   "  CASE WHEN ISNULL(hrdiscipline_committee.committeemasterunkid,0) <= 0 THEN ISNULL(hrhearing_schedule_tran.committeemstunkid,0) ELSE ISNULL(hrdiscipline_committee.committeemasterunkid,0) END AS committeemasterunkid " & _
                   "FROM hrhearing_schedule_tran " & _
                        " LEFT JOIN hrhearing_schedule_master ON hrhearing_schedule_master.hearingschedulemasterunkid = hrhearing_schedule_tran.hearingschedulemasterunkid " & _
                        " LEFT JOIN hrdiscipline_committee ON hrdiscipline_committee.committeetranunkid = hrhearing_schedule_tran.committeetranunkid " & _
                   "WHERE hrhearing_schedule_master.hearingschedulemasterunkid = @hearingschedulemasterunkid " & _
                        " AND hrhearing_schedule_tran.isvoid = 0 AND hrhearing_schedule_master.isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@hearingschedulemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, inthearingschedulemasterid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "Id")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("Id").Rows.Count > 0 Then
                intCommitteeMasterId = CInt(dsList.Tables("Id").Rows(0).Item("committeemasterunkid"))
            End If

            Return intCommitteeMasterId
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getCommitteeMasterId", mstrModuleName)
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrhearing_schedule_tran) </purpose>
    Public Function InsertUpdateDeleteHearingTran(ByVal objDataOp As clsDataOperation, _
                                                  ByVal intUserId As Integer, _
                                                  ByVal dtCurrentDateTime As DateTime, _
                                                  ByVal intDisciplineFileUnkid As Integer, _
                                                  ByVal intCompanyUnkId As Integer, _
                                                  ByVal dtAttachment As DataTable, _
                                                  ByVal strTransScreenName As String) As Boolean 'S.SANDEEP |11-NOV-2019| -- START {dtAttachment,strTransactionName} -- END
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        Try
            For Each drow As DataRow In mdtTran.Rows
                objDataOperation.ClearParameters()
                With drow
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"

                                If CInt(.Item("committeetranunkid")) > 0 Then
                                If isExist(objDataOperation, mintHearingschedulemasterunkid, CInt(.Item("committeetranunkid"))) Then
                                    mstrMessage = Language.getMessage(mstrModuleName, 2, "This hearing schedule is already exists.")
                                    Return False
                                End If
                                End If

                                strQ = "INSERT INTO hrhearing_schedule_tran ( " & _
                                             "  hearingschedulemasterunkid " & _
                                             ", committeetranunkid " & _
                                             ", isnotified " & _
                                             ", notify_date " & _
                                             ", isvoid " & _
                                             ", voiduserunkid " & _
                                             ", voiddatetime " & _
                                             ", voidreason" & _
                                             ", ex_name " & _
                                             ", ex_company " & _
                                             ", ex_department " & _
                                             ", ex_contactno " & _
                                             ", ex_email " & _
                                             ", employeeunkid " & _
                                             ", committeemstunkid " & _
                                       ") VALUES (" & _
                                             "  @hearingschedulemasterunkid " & _
                                             ", @committeetranunkid " & _
                                             ", @isnotified " & _
                                             ", @notify_date " & _
                                             ", @isvoid " & _
                                             ", @voiduserunkid " & _
                                             ", @voiddatetime " & _
                                             ", @voidreason" & _
                                             ", @ex_name " & _
                                             ", @ex_company " & _
                                             ", @ex_department " & _
                                             ", @ex_contactno " & _
                                             ", @ex_email " & _
                                             ", @employeeunkid " & _
                                             ", @committeemstunkid " & _
                                       "); SELECT @@identity"

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@hearingschedulemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHearingschedulemasterunkid.ToString)
                                objDataOperation.AddParameter("@committeetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeetranunkid"))
                                If IsDBNull(.Item("isnotified")) = False Then
                                objDataOperation.AddParameter("@isnotified", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isnotified"))
                                Else
                                    objDataOperation.AddParameter("@isnotified", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                End If
                                If .Item("notify_date").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@notify_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("notify_date"))
                                Else
                                    objDataOperation.AddParameter("@notify_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If .Item("voiddatetime").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                'Pinkal (19-Dec-2020) -- Start
                                'Enhancement  -  Working on Discipline module for NMB.
                                objDataOperation.AddParameter("@ex_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_name"))
                                objDataOperation.AddParameter("@ex_company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_company"))
                                objDataOperation.AddParameter("@ex_department", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_department"))
                                objDataOperation.AddParameter("@ex_contactno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_contactno"))
                                objDataOperation.AddParameter("@ex_email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_email"))
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))
                                objDataOperation.AddParameter("@committeemstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeemasterunkid"))
                                'Pinkal (19-Dec-2020) -- End

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintHearningscheduletranunkid = dsList.Tables(0).Rows(0).Item(0)

                                If InsertATHearingScheduleTran(objDataOperation, enAuditType.ADD, dtCurrentDateTime, intUserId, drow) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Sohail (30 Nov 2017) -- Start
                                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                'Call SendMailToMembers(mintHearingschedulemasterunkid, intDisciplineFileUnkid, CStr(.Item("member_name")), CStr(.Item("email")))


                                'S.SANDEEP |11-NOV-2019| -- START
                                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                                'Call SendMailToMembers(mintHearingschedulemasterunkid, intDisciplineFileUnkid, CStr(.Item("member_name")), CStr(.Item("email")), intCompanyUnkId)
                                Dim strAttachedFiles As String = String.Empty
                                If dtAttachment IsNot Nothing AndAlso dtAttachment.Rows.Count > 0 Then
                                    strAttachedFiles = String.Join(",", dtAttachment.AsEnumerable().Select(Function(x) x.Field(Of String)("filepath")).ToArray())
                                End If
                                Call SendMailToMembers(mintHearingschedulemasterunkid, intDisciplineFileUnkid, CStr(.Item("members")), CStr(.Item("email")), intCompanyUnkId, strAttachedFiles)
                                'S.SANDEEP |11-NOV-2019| -- END

                                'Sohail (30 Nov 2017) -- End

                            Case "U"
                                'S.SANDEEP |11-NOV-2019| -- START
                                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                                strQ = "UPDATE hrhearing_schedule_tran SET " & _
                                       "     committeetranunkid = @committeetranunkid " & _
                                       "    ,isnotified = @isnotified " & _
                                       "    ,notify_date = @notify_date " & _
                                       "    ,isvoid = @isvoid " & _
                                       "    ,voiduserunkid = @voiduserunkid " & _
                                       "    ,voiddatetime = @voiddatetime " & _
                                       "    ,voidreason = @voidreason " & _
                                       "    ,ex_name  = @ex_name " & _
                                       "    ,ex_company = @ex_company " & _
                                       "    ,ex_department = @ex_department " & _
                                       "    ,ex_contactno = @ex_contactno " & _
                                       "    ,ex_email  = @ex_email " & _
                                       "    ,employeeunkid  = @employeeunkid " & _
                                       "    ,committeemstunkid = @committeemstunkid " & _
                                       "WHERE hearningscheduletranunkid = @hearningscheduletranunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@hearingschedulemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("hearingschedulemasterunkid").ToString)
                                objDataOperation.AddParameter("@committeetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeetranunkid"))
                                objDataOperation.AddParameter("@isnotified", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isnotified"))
                                If .Item("notify_date").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@notify_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("notify_date"))
                                Else
                                    objDataOperation.AddParameter("@notify_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If .Item("voiddatetime").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))


                                'Pinkal (19-Dec-2020) -- Start
                                'Enhancement  -  Working on Discipline module for NMB.
                                objDataOperation.AddParameter("@ex_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_name"))
                                objDataOperation.AddParameter("@ex_company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_company"))
                                objDataOperation.AddParameter("@ex_department", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_department"))
                                objDataOperation.AddParameter("@ex_contactno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_contactno"))
                                objDataOperation.AddParameter("@ex_email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_email"))
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))
                                objDataOperation.AddParameter("@committeemstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeemasterunkid"))
                                'Pinkal (19-Dec-2020) -- End


                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If InsertATHearingScheduleTran(objDataOperation, enAuditType.ADD, dtCurrentDateTime, intUserId, drow) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                Dim strAttachedFiles As String = String.Empty
                                If dtAttachment IsNot Nothing AndAlso dtAttachment.Rows.Count > 0 Then
                                    strAttachedFiles = String.Join(",", dtAttachment.AsEnumerable().Select(Function(x) x.Field(Of String)("filepath")).ToArray())
                                End If
                                Call SendMailToMembers(mintHearingschedulemasterunkid, intDisciplineFileUnkid, CStr(.Item("member_name")), CStr(.Item("email")), intCompanyUnkId, strAttachedFiles)
                                'S.SANDEEP |11-NOV-2019| -- END
                            Case "D"

                                strQ = "UPDATE hrhearing_schedule_tran SET " & _
                                            "  isvoid = @isvoid " & _
                                            ", voiduserunkid = @voiduserunkid " & _
                                            ", voiddatetime = @voiddatetime " & _
                                            ", voidreason = @voidreason " & _
                                       "WHERE hearningscheduletranunkid = @hearningscheduletranunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@hearningscheduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("hearningscheduletranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If .Item("voiddatetime").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If InsertATHearingScheduleTran(objDataOperation, enAuditType.DELETE, dtCurrentDateTime, intUserId, drow) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If
                End With
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDeleteHearingTran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal objDataOp As clsDataOperation, ByVal inthearingschedulemasterunkid As Integer, ByVal intcommitteetranunkid As Integer, _
                            Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        Try
            strQ = "SELECT " & _
                      "  hearningscheduletranunkid " & _
                      ", hearingschedulemasterunkid " & _
                      ", committeetranunkid " & _
                      ", isnotified " & _
                      ", notify_date " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                   "FROM hrhearing_schedule_tran " & _
                   "WHERE hearingschedulemasterunkid = @hearingschedulemasterunkid " & _
                   "AND committeetranunkid = @committeetranunkid "

            'If intUnkid > 0 Then
            '    strQ &= " AND hearningscheduletranunkid <> @hearningscheduletranunkid"
            'End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@hearingschedulemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, inthearingschedulemasterunkid.ToString)
            objDataOperation.AddParameter("@committeetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intcommitteetranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Private Function InsertATHearingScheduleTran(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As enAuditType, _
                                                 ByVal dtCurrentDateTime As DateTime, ByVal intUserId As Integer, ByVal dtRow As DataRow) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            With dtRow
                strQ = "INSERT INTO athrhearing_schedule_tran ( " & _
                      "  hearningscheduletranunkid " & _
                      ", hearingschedulemasterunkid " & _
                      ", committeetranunkid " & _
                      ", isnotified " & _
                      ", notify_date " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", module_name1 " & _
                      ", module_name2 " & _
                      ", module_name3 " & _
                      ", module_name4 " & _
                      ", module_name5 " & _
                      ", isweb " & _
                      ", ex_name " & _
                      ", ex_company " & _
                      ", ex_department " & _
                      ", ex_contactno " & _
                      ", ex_email " & _
                      ", employeeunkid " & _
                      ", committeemstunkid " & _
                   ") VALUES (" & _
                      "  @hearningscheduletranunkid " & _
                      ", @hearingschedulemasterunkid " & _
                      ", @committeetranunkid " & _
                      ", @isnotified " & _
                      ", @notify_date " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @auditdatetime " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      ", @module_name1 " & _
                      ", @module_name2 " & _
                      ", @module_name3 " & _
                      ", @module_name4 " & _
                      ", @module_name5 " & _
                      ", @isweb " & _
                      ", @ex_name " & _
                      ", @ex_company " & _
                      ", @ex_department " & _
                      ", @ex_contactno " & _
                      ", @ex_email " & _
                      ", @employeeunkid " & _
                      ", @committeemstunkid " & _
                   ") "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@hearningscheduletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(mintHearningscheduletranunkid <= 0, .Item("hearningscheduletranunkid"), mintHearningscheduletranunkid))
                objDataOperation.AddParameter("@hearingschedulemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(mintHearingschedulemasterunkid <= 0, .Item("hearingschedulemasterunkid"), mintHearingschedulemasterunkid))
                objDataOperation.AddParameter("@committeetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeetranunkid"))
                objDataOperation.AddParameter("@isnotified", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isnotified"))
                objDataOperation.AddParameter("@notify_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(.Item("notify_date").ToString <> Nothing, .Item("notify_date"), DBNull.Value))

                'Pinkal (19-Dec-2020) -- Start
                'Enhancement  -  Working on Discipline module for NMB.
                objDataOperation.AddParameter("@ex_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_name"))
                objDataOperation.AddParameter("@ex_company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_company"))
                objDataOperation.AddParameter("@ex_department", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_department"))
                objDataOperation.AddParameter("@ex_contactno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_contactno"))
                objDataOperation.AddParameter("@ex_email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_email"))
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))
                objDataOperation.AddParameter("@committeemstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeemasterunkid"))
                'Pinkal (19-Dec-2020) -- End

            End With

            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATHearingScheduleTran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

    'S.SANDEEP |01-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    ''S.SANDEEP [24 MAY 2016] -- Start
    ''Email Notification
    'Public Function SendMailToMembers(ByVal intHearingScheduleMasterUnkid As Integer, ByVal intDisciplineFileUnkid As Integer, ByVal strMembers As String, ByVal strEmail As String, ByVal intCompanyUnkId As Integer) As Boolean
    '    'Sohail (30 Nov 2017) - [intCompanyUnkId]
    '    Try
    '        Dim strMessage As String = String.Empty
    '        Dim strBuilder As New StringBuilder
    '        Dim objNetConn As New clsNetConnectivity
    '        Dim objMail As New clsSendMail
    '        Dim objEmployee As New clsEmployee_Master
    '        Dim objUser As New clsUserAddEdit
    '        Dim objDisciplineFileMaster As New clsDiscipline_file_master
    '        Dim objHearingScheduleMaster As New clshearing_schedule_master

    '        If objNetConn._Conected = False Then Exit Function

    '        objHearingScheduleMaster._Hearingschedulemasterunkid = intHearingScheduleMasterUnkid
    '        objDisciplineFileMaster._Disciplinefileunkid = intDisciplineFileUnkid

    '        objMail._Subject = Language.getMessage(mstrModuleName, 3, "Hearing Schedule Notification") & " " & Language.getMessage(mstrModuleName, 4, " :  Reference No - ") & objDisciplineFileMaster._Reference_No

    '        strMessage = "<HTML><BODY>"

    '        'Gajanan [27-Mar-2019] -- Start
    '        'Enhancement - Change Email Language

    '        'strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & "&nbsp;<b>" & strMembers & "</b>" & " ,<BR></BR>"
    '        strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & "&nbsp;<b>" & getTitleCase(strMembers) & "</b>" & " ,<BR></BR>"
    '        'Gajanan [27-Mar-2019] -- End

    '        'Gajanan [27-Mar-2019] -- Start
    '        'Enhancement - Change Email Language

    '        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 6, "This is to inform you that, Hearing is scheduled for mentioned reference no in the subject. Please refer following details.") & "<BR></BR><BR></BR>"
    '        strMessage &= Language.getMessage(mstrModuleName, 6, "This is to inform you that, Hearing is scheduled for mentioned reference no in the subject. Please refer following details.") & "<BR></BR><BR></BR>"
    '        'Gajanan [27-Mar-2019] -- End

    '        strBuilder.Append("<B>" & Language.getMessage(mstrModuleName, 7, "Hearing Schedule :") & "</B><BR></BR>" & vbCrLf)
    '        strBuilder.Append("<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=3 WIDTH='100%'>" & vbCrLf)
    '        strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
    '        strBuilder.Append("<TD WIDTH='40%' BGCOLOR='#4682b4' ALIGN='LEFT'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 8, "Particular") & "</B></FONT></TD>" & vbCrLf)
    '        strBuilder.Append("<TD WIDTH='60%' BGCOLOR='#4682b4' ALIGN='LEFT'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 9, "Details") & "</B></FONT></TD>" & vbCrLf)
    '        strBuilder.Append("</TR>" & vbCrLf)
    '        strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
    '        strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 10, "Hearing Date") & "</B></FONT></TD>" & vbCrLf)
    '        strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & objHearingScheduleMaster._HearingDate.Date & "</FONT></TD>" & vbCrLf)
    '        strBuilder.Append("</TR>" & vbCrLf)
    '        strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
    '        strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 11, "Hearing Time") & "</B></FONT></TD>" & vbCrLf)
    '        strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & objHearingScheduleMaster._HearningTime.ToShortTimeString & "</FONT></TD>" & vbCrLf)
    '        strBuilder.Append("</TR>" & vbCrLf)
    '        strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
    '        strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 12, "Hearing Vanue") & "</B></FONT></TD>" & vbCrLf)
    '        strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & objHearingScheduleMaster._HearningVenue & "</FONT></TD>" & vbCrLf)
    '        strBuilder.Append("</TR>" & vbCrLf)
    '        strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
    '        strBuilder.Append("<TD WIDTH='40%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 13, "Remark") & "</B></FONT></TD>" & vbCrLf)
    '        strBuilder.Append("<TD WIDTH='60%' ALIGN='LEFT'><FONT SIZE=2>" & objHearingScheduleMaster._Remark & "</FONT></TD>" & vbCrLf)
    '        strBuilder.Append("</TR>" & vbCrLf)
    '        strBuilder.Append("</TABLE><BR></BR>" & vbCrLf)

    '        'strMessage &= strBuilder.ToString
    '        'strMessage &= objDisciplineFileMaster.getChargeDetailForEmail(intDisciplineFileUnkid)
    '        'strMessage &= objDisciplineFileMaster.getCountForEmail(intDisciplineFileUnkid)


    '        'Gajanan [27-Mar-2019] -- Start
    '        'Enhancement - Change Email Language
    '        'strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
    '        strBuilder.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
    '        'Gajanan [27-Mar-2019] -- End

    '        strMessage &= "</BODY></HTML>"

    '        Dim strSuccess As String = ""
    '        Dim mintLoginTypeId As Integer = CInt(objHearingScheduleMaster._LoginTypeId)
    '        objUser._Userunkid = objHearingScheduleMaster._Userunkid

    '        objMail._Message = strMessage
    '        objMail._ToEmail = strEmail
    '        If mintLoginTypeId <= 0 Then mintLoginTypeId = enLogin_Mode.DESKTOP
    '        If mstrWebFormName.Trim.Length > 0 Then
    '            objMail._Form_Name = mstrWebFormName
    '            objMail._WebClientIP = mstrWebClientIP
    '            objMail._WebHostName = mstrWebHostName
    '        End If
    '        objMail._LogEmployeeUnkid = objHearingScheduleMaster._LoginEmployeeunkid
    '        objMail._OperationModeId = mintLoginTypeId
    '        objMail._UserUnkid = objHearingScheduleMaster._Userunkid
    '        objMail._SenderAddress = IIf(objUser._Email = "", objUser._Username, objUser._Email)
    '        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.DISCIPLINE_MGT
    '        'Sohail (30 Nov 2017) -- Start
    '        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '        'strSuccess = objMail.SendMail()
    '        strSuccess = objMail.SendMail(intCompanyUnkId)
    '        'Sohail (30 Nov 2017) -- End

    '        If strSuccess.Trim.Length <= 0 Then
    '            Return True
    '        Else
    '            Return False
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: SendMailToMembers; Module Name: " & mstrModuleName)
    '    End Try
    'End Function
    ''S.SANDEEP [24 MAY 2016] -- End

    'S.SANDEEP |01-OCT-2021| -- START
    Private Function UpdateNotificationStatus(ByVal strCheckedIds As String, ByVal objData As clsDataOperation, ByVal intUserId As Integer) As Boolean
        Dim StrQ As String = ""
        Try
            If strCheckedIds.Trim.Length > 0 Then                
                StrQ = "UPDATE hrhearing_schedule_tran SET isnotified = 1,notify_date = GETDATE() WHERE hearningscheduletranunkid IN (" & strCheckedIds & ") "

                objData.ExecNonQuery(StrQ)

                If objData.ErrorMessage <> "" Then
                    Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                End If

                StrQ = "INSERT INTO athrhearing_schedule_tran " & _
                        "( " & _
                        " hearningscheduletranunkid " & _
                        ",hearingschedulemasterunkid " & _
                        ",committeetranunkid " & _
                        ",isnotified " & _
                        ",notify_date " & _
                        ",audittype " & _
                        ",audituserunkid " & _
                        ",auditdatetime " & _
                        ",ip " & _
                        ",machine_name " & _
                        ",form_name " & _
                        ",module_name1 " & _
                        ",module_name2 " & _
                        ",module_name3 " & _
                        ",module_name4 " & _
                        ",module_name5 " & _
                        ",isweb " & _
                        ",ex_name " & _
                        ",ex_company " & _
                        ",ex_department " & _
                        ",ex_contactno " & _
                        ",ex_email " & _
                        ",employeeunkid " & _
                        ",committeemstunkid) " & _
                        "SELECT " & _
                        " hearningscheduletranunkid " & _
                        ",hearingschedulemasterunkid " & _
                        ",committeetranunkid " & _
                        ",isnotified " & _
                        ",notify_date " & _
                        ",2 " & _
                        "," & intUserId & " " & _
                        ",GETDATE() " & _
                        ",'" & IIf(mstrWebClientIP.Trim.Length > 0, mstrWebClientIP, getIP()) & "' " & _
                        ",'" & IIf(mstrWebHostName.Trim.Length > 0, mstrWebFormName, getHostName()) & "' " & _
                        ",'" & IIf(mstrWebFormName.Trim.Length > 0, mstrWebFormName, mstrForm_Name) & "' " & _
                        ",'' " & _
                        ",'' " & _
                        ",'' " & _
                        ",'' " & _
                        ",'' " & _
                        "," & IIf(mstrWebFormName.Trim.Length > 0, 1, 0) & " " & _
                        ",ex_name " & _
                        ",ex_company " & _
                        ",ex_department " & _
                        ",ex_contactno " & _
                        ",ex_email " & _
                        ",employeeunkid " & _
                        ",committeemstunkid " & _
                        "FROM hrhearing_schedule_tran " & _
                        "WHERE hearningscheduletranunkid IN (" & strCheckedIds & ") "

                objData.ExecNonQuery(StrQ)

                If objData.ErrorMessage <> "" Then
                    Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateNotificationStatus; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |01-OCT-2021| -- END

    Public Function NotifyMembers(ByVal intDefaultNotifyType As Integer, _
                                  ByVal strHMastrId As String, _
                                  ByVal intCompanyUnkId As Integer, _
                                  ByVal mintLoginTypeId As Integer, _
                                  ByVal intUserId As Integer, _
                                  ByVal strSender As String, _
                                  ByRef blnIsNoDatatoSend As Boolean, _
                                  Optional ByVal blnIncludeCaseInfoHearingNotification As Boolean = False, _
                                  Optional ByVal blnNotifyChargedEmployeeOfHearing As Boolean = False _
                                  ) As Boolean
        'Hemant (24 Sep 2021) -- [blnIncludeCaseInfoHearingNotification,blnNotifyChargedEmployeeOfHearing]
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        'Hemant (24 Sep 2021) -- Start             
        Dim lstSuccessDisciplinefileIdList As List(Of String) = New List(Of String)
        'Hemant (24 Sep 2021) -- End
        Try
            Using objData As New clsDataOperation
                StrQ &= "DECLARE @words VARCHAR (MAX) " & _
                        "   SET @words = '" & strHMastrId & "' " & _
                        "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
                        "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
                        "SELECT @words = @words + ',', @start = 1, @stop = len(@words)+1 " & _
                        "WHILE @start < @stop begin " & _
                        "    SELECT " & _
                        "       @end   = CHARINDEX(',',@words,@start) " & _
                        "      ,@word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
                        "      ,@start = @end+1 " & _
                        "    INSERT @split VALUES (@word) " & _
                        "END "
                StrQ &= "SELECT " & _
                       "     CASE WHEN EM.employeeunkid > 0 THEN EM.firstname+' '+EM.surname ELSE HT.ex_name END AS mName " & _
                       "    ,ISNULL(EM.email,ISNULL(HT.ex_email,'')) AS mEmail " & _
                       "    ,CONVERT(NVARCHAR(8),HM.hearing_date,112) AS hDate " & _
                       "    ,CONVERT(NVARCHAR(5),HM.hearning_time,108) AS hTime " & _
                       "    ,HM.hearning_venue AS hVenue " & _
                       "    ,HM.remark AS hRemark " & _
                       "    ,DM.reference_no AS hRefNo " & _
                       "    ,IE.firstname+' '+IE.surname AS hInvEmp " & _
                       "    ,IE.email AS hEmail " & _
                       "    ,HM.hearingschedulemasterunkid AS hMstId " & _
                       "    ,ISNULL(DT.filepath,'') AS hfilepath " & _
                       "    ,HT.hearningscheduletranunkid AS htranid " & _
                       "    ,DM.disciplinefileunkid " & _
                       "    ,involved_employeeunkid " & _
                       "FROM hrhearing_schedule_master AS HM " & _
                        "    JOIN @split ON [@split].word = HM.hearingschedulemasterunkid " & _
                       "    JOIN hrdiscipline_file_master AS DM ON HM.disciplinefileunkid = DM.disciplinefileunkid " & _
                       "    JOIN hrhearing_schedule_tran AS HT ON HM.hearingschedulemasterunkid = HT.hearingschedulemasterunkid " & _
                       "    LEFT JOIN hremployee_master AS EM ON HT.employeeunkid = EM.employeeunkid " & _
                       "    LEFT JOIN hremployee_master AS IE ON DM.involved_employeeunkid = IE.employeeunkid " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "       SELECT " & _
                        "            DT2.transactionunkid " & _
                        "           ,STUFF((SELECT ',' + CAST(DT1.filepath AS VARCHAR(MAX)) " & _
                       "                FROM hrdocuments_tran AS DT1 " & _
                       "                WHERE DT1.transactionunkid = DT2.transactionunkid " & _
                       "                    AND  DT1.form_name = 'frmDisciplineHearingAddEdit' AND DT1.isactive = 1 " & _
                       "                FOR XML PATH('')), 1, 1, '') AS filepath " & _
                        "       FROM hrdocuments_tran AS DT2 " & _
                        "       WHERE DT2.form_name = 'frmDisciplineHearingAddEdit' AND DT2.isactive = 1 " & _
                        "       GROUP BY DT2.transactionunkid " & _
                        "   ) AS DT ON HM.hearingschedulemasterunkid = DT.transactionunkid " & _
                       "WHERE HM.isvoid = 0 AND HM.status = 1 AND HT.isvoid = 0 AND HT.isnotified = 0 " & _
                       "    AND ISNULL(EM.email,ISNULL(HT.ex_email,'')) <> '' " & _
                       "ORDER BY HT.employeeunkid, HM.hearing_date "

                'Hemant (24 Sep 2021) -- [DM.disciplinefileunkid,involved_employeeunkid]
                dsList = objData.ExecQuery(StrQ, "List")

                If objData.ErrorMessage <> "" Then
                    Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                End If

                If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    'Dim iEmlst As New List(Of String)
                    'iEmlst = dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of String)("mEmail")).Distinct().ToList()
                    'If iEmlst.Count > 0 Then
                    '    Dim objMail As New clsSendMail
                    '    For Each eMl As String In iEmlst
                    '        Dim strSuccess As String = ""
                    '        Dim strCheckedIds As String = ""
                    '        Dim xRows() As DataRow = dsList.Tables(0).Select("mEmail = '" & eMl & "'")
                    '        Dim strDisciplinefileUnkidIds As String = String.Join(",", dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("mEmail") = eMl.ToString).Select(Function(x) x.Field(Of Integer)("disciplinefileunkid").ToString).Distinct().ToArray())
                    '        If xRows.Length > 0 Then
                    '            Dim strFilesAttached As String = ""

                    '            strFilesAttached = xRows(0)("hfilepath").ToString()
                    '            Dim strMessage As String = "" : Dim intSrNo As Integer = 1 : Dim strBuilder As New StringBuilder
                    '            objMail._Subject = Language.getMessage(mstrModuleName, 3, "Hearing Schedule Notification")
                    '            strBuilder.Append("<HTML><BODY>" & vbCrLf)
                    '            strBuilder.Append(Language.getMessage(mstrModuleName, 5, "Dear") & " " & "&nbsp;<b>" & getTitleCase(xRows(0)("mName").ToString()) & "</b>" & ",<BR></BR>" & vbCrLf)
                    '            strBuilder.Append(Language.getMessage(mstrModuleName, 100, "This is to inform you that a disciplinary hearing(s) has been scheduled as detailed below;") & "<BR></BR>" & vbCrLf)
                    '            strBuilder.Append("<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=3 WIDTH='60%'>" & vbCrLf)
                    '            strBuilder.Append("<TR WIDTH='60%'>" & vbCrLf)
                    '            strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 101, "Sr.No") & "</B></FONT></TD>" & vbCrLf)
                    '            strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 102, "Ref.No") & "</B></FONT></TD>" & vbCrLf)
                    '            strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 103, "Hearing Date") & "</B></FONT></TD>" & vbCrLf)
                    '            strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 104, "Hearing Time") & "</B></FONT></TD>" & vbCrLf)
                    '            strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 105, "Venue") & "</B></FONT></TD>" & vbCrLf)
                    '            strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 106, "Remark") & "</B></FONT></TD>" & vbCrLf)
                    '            strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 107, "Posted Against") & "</B></FONT></TD>" & vbCrLf)
                    '            strBuilder.Append("</TR>" & vbCrLf)
                    '            For Each iR As DataRow In xRows
                    '                strBuilder.Append("<TR WIDTH='60%'>" & vbCrLf)
                    '                strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & intSrNo.ToString & "</FONT></TD>" & vbCrLf)
                    '                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & iR("hRefNo").ToString() & "</FONT></TD>" & vbCrLf)
                    '                strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & eZeeDate.convertDate(iR("hDate").ToString()).ToShortDateString & "</FONT></TD>" & vbCrLf)
                    '                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & iR("hTime").ToString() & "</FONT></TD>" & vbCrLf)
                    '                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & iR("hVenue").ToString() & "</FONT></TD>" & vbCrLf)
                    '                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & iR("hRemark").ToString() & "</FONT></TD>" & vbCrLf)
                    '                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & getTitleCase(iR("hInvEmp").ToString()) & "</FONT></TD>" & vbCrLf)
                    '                strBuilder.Append("</TR>" & vbCrLf)
                    '                intSrNo += 1
                    '                strCheckedIds &= "," & iR("htranid").ToString()
                    '            Next
                    '            strBuilder.Append("</TABLE><BR><BR>" & vbCrLf)
                    '            strBuilder.Append(Language.getMessage(mstrModuleName, 17, "Regards,") & vbCrLf)
                    '            strBuilder.Append("</BODY></HTML>" & vbCrLf)

                    '            strMessage = strBuilder.ToString()

                    '            objMail._Message = strMessage
                    '            objMail._ToEmail = eMl
                    '            If mintLoginTypeId <= 0 Then mintLoginTypeId = enLogin_Mode.DESKTOP
                    '            If mstrWebFormName.Trim.Length > 0 Then
                    '                objMail._Form_Name = mstrWebFormName
                    '                objMail._WebClientIP = mstrWebClientIP
                    '                objMail._WebHostName = mstrWebHostName
                    '            End If
                    '            objMail._LogEmployeeUnkid = 0
                    '            objMail._OperationModeId = mintLoginTypeId
                    '            objMail._UserUnkid = intUserId
                    '            objMail._SenderAddress = strSender
                    '            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.DISCIPLINE_MGT
                    '            objMail._AttachedFiles = strFilesAttached
                    '            strSuccess = objMail.SendMail(intCompanyUnkId)
                    '            'Hemant (24 Sep 2021) -- Start             
                    '            'ISSUE/ENHANCEMENT : OLD-481(ZRA) - Give Configurable Option to include Case Information on Hearing Schedule Notification
                    '            If strSuccess.Trim.Length <= 0 Then
                    '                Dim arrSuccessDisciplinefileUnkidIds() As String = strDisciplinefileUnkidIds.Split(",")
                    '                lstSuccessDisciplinefileIdList.AddRange(arrSuccessDisciplinefileUnkidIds)
                    '                lstSuccessDisciplinefileIdList = lstSuccessDisciplinefileIdList.Distinct().ToList
                    '            End If
                    '            'Hemant (24 Sep 2021) -- End
                    '            If strSuccess.Trim.Length <= 0 Then
                    '                If strCheckedIds.Trim.Length > 0 Then
                    '                    strCheckedIds = Mid(strCheckedIds, 2)
                    '                    StrQ = "UPDATE hrhearing_schedule_tran SET isnotified = 1,notify_date = GETDATE() WHERE hearningscheduletranunkid IN (" & strCheckedIds & ") "

                    '                    objData.ExecNonQuery(StrQ)

                    '                    If objData.ErrorMessage <> "" Then
                    '                        Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                    '                    End If

                    '                    StrQ = "INSERT INTO athrhearing_schedule_tran " & _
                    '                            "( " & _
                    '                            " hearningscheduletranunkid " & _
                    '                            ",hearingschedulemasterunkid " & _
                    '                            ",committeetranunkid " & _
                    '                            ",isnotified " & _
                    '                            ",notify_date " & _
                    '                            ",audittype " & _
                    '                            ",audituserunkid " & _
                    '                            ",auditdatetime " & _
                    '                            ",ip " & _
                    '                            ",machine_name " & _
                    '                            ",form_name " & _
                    '                            ",module_name1 " & _
                    '                            ",module_name2 " & _
                    '                            ",module_name3 " & _
                    '                            ",module_name4 " & _
                    '                            ",module_name5 " & _
                    '                            ",isweb " & _
                    '                            ",ex_name " & _
                    '                            ",ex_company " & _
                    '                            ",ex_department " & _
                    '                            ",ex_contactno " & _
                    '                            ",ex_email " & _
                    '                            ",employeeunkid " & _
                    '                            ",committeemstunkid) " & _
                    '                            "SELECT " & _
                    '                            " hearningscheduletranunkid " & _
                    '                            ",hearingschedulemasterunkid " & _
                    '                            ",committeetranunkid " & _
                    '                            ",isnotified " & _
                    '                            ",notify_date " & _
                    '                            ",2 " & _
                    '                            "," & intUserId & " " & _
                    '                            ",GETDATE() " & _
                    '                            ",'" & IIf(mstrWebClientIP.Trim.Length > 0, mstrWebClientIP, getIP()) & "' " & _
                    '                            ",'" & IIf(mstrWebHostName.Trim.Length > 0, mstrWebFormName, getHostName()) & "' " & _
                    '                            ",'" & IIf(mstrWebFormName.Trim.Length > 0, mstrWebFormName, mstrForm_Name) & "' " & _
                    '                            ",'' " & _
                    '                            ",'' " & _
                    '                            ",'' " & _
                    '                            ",'' " & _
                    '                            ",'' " & _
                    '                            "," & IIf(mstrWebFormName.Trim.Length > 0, 1, 0) & " " & _
                    '                            ",ex_name " & _
                    '                            ",ex_company " & _
                    '                            ",ex_department " & _
                    '                            ",ex_contactno " & _
                    '                            ",ex_email " & _
                    '                            ",employeeunkid " & _
                    '                            ",committeemstunkid " & _
                    '                            "FROM hrhearing_schedule_tran " & _
                    '                            "WHERE hearningscheduletranunkid IN (" & strCheckedIds & ") "

                    '                    objData.ExecNonQuery(StrQ)

                    '                    If objData.ErrorMessage <> "" Then
                    '                        Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                    '                    End If
                    '                End If
                    '            End If
                    '        End If
                    '    Next
                    'End If

                    Select Case intDefaultNotifyType
                        Case 1  'Consolidate all hearing in single mail to each member(s)
                    Dim iEmlst As New List(Of String)
                    iEmlst = dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of String)("mEmail")).Distinct().ToList()
                    If iEmlst.Count > 0 Then
                        Dim objMail As New clsSendMail
                        For Each eMl As String In iEmlst
                            Dim strSuccess As String = ""
                            Dim strCheckedIds As String = ""
                            Dim xRows() As DataRow = dsList.Tables(0).Select("mEmail = '" & eMl & "'")
                            Dim strDisciplinefileUnkidIds As String = String.Join(",", dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of String)("mEmail") = eMl.ToString).Select(Function(x) x.Field(Of Integer)("disciplinefileunkid").ToString).Distinct().ToArray())
                            If xRows.Length > 0 Then
                                Dim strFilesAttached As String = ""
                                strFilesAttached = xRows(0)("hfilepath").ToString()
                                Dim strMessage As String = "" : Dim intSrNo As Integer = 1 : Dim strBuilder As New StringBuilder
                                objMail._Subject = Language.getMessage(mstrModuleName, 3, "Hearing Schedule Notification")
                                strBuilder.Append("<HTML><BODY>" & vbCrLf)
                                strBuilder.Append(Language.getMessage(mstrModuleName, 5, "Dear") & " " & "&nbsp;<b>" & getTitleCase(xRows(0)("mName").ToString()) & "</b>" & ",<BR></BR>" & vbCrLf)
                                strBuilder.Append(Language.getMessage(mstrModuleName, 100, "This is to inform you that a disciplinary hearing(s) has been scheduled as detailed below;") & "<BR></BR>" & vbCrLf)
                                strBuilder.Append("<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=3 WIDTH='60%'>" & vbCrLf)
            strBuilder.Append("<TR WIDTH='60%'>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 101, "Sr.No") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 102, "Ref.No") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 103, "Hearing Date") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 104, "Hearing Time") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 105, "Venue") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 106, "Remark") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 107, "Posted Against") & "</B></FONT></TD>" & vbCrLf)
            strBuilder.Append("</TR>" & vbCrLf)
                                For Each iR As DataRow In xRows
            strBuilder.Append("<TR WIDTH='60%'>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & intSrNo.ToString & "</FONT></TD>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & iR("hRefNo").ToString() & "</FONT></TD>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & eZeeDate.convertDate(iR("hDate").ToString()).ToShortDateString & "</FONT></TD>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & iR("hTime").ToString() & "</FONT></TD>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & iR("hVenue").ToString() & "</FONT></TD>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & iR("hRemark").ToString() & "</FONT></TD>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & getTitleCase(iR("hInvEmp").ToString()) & "</FONT></TD>" & vbCrLf)
            strBuilder.Append("</TR>" & vbCrLf)
                                    intSrNo += 1
                                    strCheckedIds &= "," & iR("htranid").ToString()
                                Next
                                strBuilder.Append("</TABLE><BR><BR>" & vbCrLf)
                                strBuilder.Append(Language.getMessage(mstrModuleName, 17, "Regards,") & vbCrLf)
                                strBuilder.Append("</BODY></HTML>" & vbCrLf)
                                strMessage = strBuilder.ToString()
            objMail._Message = strMessage
                                objMail._ToEmail = eMl
            If mintLoginTypeId <= 0 Then mintLoginTypeId = enLogin_Mode.DESKTOP
            If mstrWebFormName.Trim.Length > 0 Then
                objMail._Form_Name = mstrWebFormName
                objMail._WebClientIP = mstrWebClientIP
                objMail._WebHostName = mstrWebHostName
            End If
                                objMail._LogEmployeeUnkid = 0
            objMail._OperationModeId = mintLoginTypeId
                                objMail._UserUnkid = intUserId
                                objMail._SenderAddress = strSender
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.DISCIPLINE_MGT
            objMail._AttachedFiles = strFilesAttached
            strSuccess = objMail.SendMail(intCompanyUnkId)
                                If strSuccess.Trim.Length <= 0 Then
                                    Dim arrSuccessDisciplinefileUnkidIds() As String = strDisciplinefileUnkidIds.Split(",")
                                    lstSuccessDisciplinefileIdList.AddRange(arrSuccessDisciplinefileUnkidIds)
                                    lstSuccessDisciplinefileIdList = lstSuccessDisciplinefileIdList.Distinct().ToList
                                End If
                                If strSuccess.Trim.Length <= 0 Then
                                    If strCheckedIds.Trim.Length > 0 Then
                                        strCheckedIds = Mid(strCheckedIds, 2)
                                                Call UpdateNotificationStatus(strCheckedIds, objData, intUserId)
                                            End If                                            
                                        End If
                                    End If
                                Next
                                        End If
                        Case 2  'Each hearing in diffrent with charges details to each member(s)
                            Dim objMail As New clsSendMail
                            Dim strSuccess As String = ""
                            Dim strCheckedIds As String = ""
                            Dim objDisciplineFile As New clsDiscipline_file_master
                            For Each xRows As DataRow In dsList.Tables(0).Rows
                                Dim strFilesAttached As String = "" : strFilesAttached = xRows("hfilepath").ToString()
                                Dim strMessage As String = "" : Dim intSrNo As Integer = 1 : Dim strBuilder As New StringBuilder
                                objMail._Subject = Language.getMessage(mstrModuleName, 3, "Hearing Schedule Notification")
                                strBuilder.Append("<HTML><BODY>" & vbCrLf)
                                strBuilder.Append(Language.getMessage(mstrModuleName, 5, "Dear") & " " & "&nbsp;<b>" & getTitleCase(xRows("mName").ToString()) & "</b>" & ",<BR></BR>" & vbCrLf)
                                strBuilder.Append(Language.getMessage(mstrModuleName, 100, "This is to inform you that a disciplinary hearing(s) has been scheduled as detailed below;") & "<BR></BR>" & vbCrLf)
                                strBuilder.Append("<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=3 WIDTH='60%'>" & vbCrLf)
                                strBuilder.Append("<TR WIDTH='60%'>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 101, "Sr.No") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 102, "Ref.No") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 103, "Hearing Date") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 104, "Hearing Time") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 105, "Venue") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 106, "Remark") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 107, "Posted Against") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("</TR>" & vbCrLf)
                                strBuilder.Append("<TR WIDTH='60%'>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & intSrNo.ToString & "</FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & xRows("hRefNo").ToString() & "</FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & eZeeDate.convertDate(xRows("hDate").ToString()).ToShortDateString & "</FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & xRows("hTime").ToString() & "</FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & xRows("hVenue").ToString() & "</FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & xRows("hRemark").ToString() & "</FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & getTitleCase(xRows("hInvEmp").ToString()) & "</FONT></TD>" & vbCrLf)
                                strBuilder.Append("</TR>" & vbCrLf)
                                strBuilder.Append("</TABLE><BR><BR>" & vbCrLf)

                                strBuilder.Append(Language.getMessage(mstrModuleName, 9, "Please refer following Disciplinary Charges details posted against you.") & vbCrLf)

                                strBuilder.Append(objDisciplineFile.getChargeDetailForEmail(CInt(xRows.Item("disciplinefileunkid"))) & vbCrLf)
                                strBuilder.Append(objDisciplineFile.getCountForEmail(CInt(xRows.Item("disciplinefileunkid"))) & vbCrLf)

                                strBuilder.Append(Language.getMessage(mstrModuleName, 17, "Regards,") & vbCrLf)
                                strBuilder.Append("</BODY></HTML>" & vbCrLf)
                                strMessage = strBuilder.ToString()
                                objMail._Message = strMessage
                                objMail._ToEmail = xRows("mEmail").ToString()
                                If mintLoginTypeId <= 0 Then mintLoginTypeId = enLogin_Mode.DESKTOP
                                If mstrWebFormName.Trim.Length > 0 Then
                                    objMail._Form_Name = mstrWebFormName
                                    objMail._WebClientIP = mstrWebClientIP
                                    objMail._WebHostName = mstrWebHostName
                                End If
                                objMail._LogEmployeeUnkid = 0
                                objMail._OperationModeId = mintLoginTypeId
                                objMail._UserUnkid = intUserId
                                objMail._SenderAddress = strSender
                                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.DISCIPLINE_MGT
                                objMail._AttachedFiles = strFilesAttached
                                strSuccess = objMail.SendMail(intCompanyUnkId)
                                If strSuccess.Trim.Length <= 0 Then
                                    lstSuccessDisciplinefileIdList.Add(xRows("disciplinefileunkid").ToString())
                                End If
                                If strSuccess.Trim.Length <= 0 Then
                                    Call UpdateNotificationStatus(xRows("htranid").ToString(), objData, intUserId)
                                        End If
                            Next
                        Case 3  'Each hearing in diffrent with out charges details to each member(s)
                            Dim objMail As New clsSendMail
                            Dim strSuccess As String = ""
                            Dim strCheckedIds As String = ""
                            For Each xRows As DataRow In dsList.Tables(0).Rows
                                Dim strFilesAttached As String = "" : strFilesAttached = xRows("hfilepath").ToString()
                                Dim strMessage As String = "" : Dim intSrNo As Integer = 1 : Dim strBuilder As New StringBuilder
                                objMail._Subject = Language.getMessage(mstrModuleName, 3, "Hearing Schedule Notification")
                                strBuilder.Append("<HTML><BODY>" & vbCrLf)
                                strBuilder.Append(Language.getMessage(mstrModuleName, 5, "Dear") & " " & "&nbsp;<b>" & getTitleCase(xRows("mName").ToString()) & "</b>" & ",<BR></BR>" & vbCrLf)
                                strBuilder.Append(Language.getMessage(mstrModuleName, 100, "This is to inform you that a disciplinary hearing(s) has been scheduled as detailed below;") & "<BR></BR>" & vbCrLf)
                                strBuilder.Append("<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=3 WIDTH='60%'>" & vbCrLf)
                                strBuilder.Append("<TR WIDTH='60%'>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 101, "Sr.No") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 102, "Ref.No") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 103, "Hearing Date") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 104, "Hearing Time") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 105, "Venue") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 106, "Remark") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 107, "Posted Against") & "</B></FONT></TD>" & vbCrLf)
                                strBuilder.Append("</TR>" & vbCrLf)
                                strBuilder.Append("<TR WIDTH='60%'>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & intSrNo.ToString & "</FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & xRows("hRefNo").ToString() & "</FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & eZeeDate.convertDate(xRows("hDate").ToString()).ToShortDateString & "</FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & xRows("hTime").ToString() & "</FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & xRows("hVenue").ToString() & "</FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & xRows("hRemark").ToString() & "</FONT></TD>" & vbCrLf)
                                strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & getTitleCase(xRows("hInvEmp").ToString()) & "</FONT></TD>" & vbCrLf)
                                strBuilder.Append("</TR>" & vbCrLf)
                                strBuilder.Append("</TABLE><BR><BR>" & vbCrLf)
                                strBuilder.Append(Language.getMessage(mstrModuleName, 17, "Regards,") & vbCrLf)
                                strBuilder.Append("</BODY></HTML>" & vbCrLf)
                                strMessage = strBuilder.ToString()
                                objMail._Message = strMessage
                                objMail._ToEmail = xRows("mEmail").ToString()
                                If mintLoginTypeId <= 0 Then mintLoginTypeId = enLogin_Mode.DESKTOP
                                If mstrWebFormName.Trim.Length > 0 Then
                                    objMail._Form_Name = mstrWebFormName
                                    objMail._WebClientIP = mstrWebClientIP
                                    objMail._WebHostName = mstrWebHostName
                                    End If
                                objMail._LogEmployeeUnkid = 0
                                objMail._OperationModeId = mintLoginTypeId
                                objMail._UserUnkid = intUserId
                                objMail._SenderAddress = strSender
                                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.DISCIPLINE_MGT
                                objMail._AttachedFiles = strFilesAttached
                                strSuccess = objMail.SendMail(intCompanyUnkId)
                                If strSuccess.Trim.Length <= 0 Then
                                    lstSuccessDisciplinefileIdList.Add(xRows("disciplinefileunkid").ToString())
                                End If
                                If strSuccess.Trim.Length <= 0 Then
                                    Call UpdateNotificationStatus(xRows("htranid").ToString(), objData, intUserId)
                            End If
                        Next
                    End Select

                    

                    'Hemant (24 Sep 2021) -- Start             
                    'ISSUE/ENHANCEMENT : OLD-482(ZRA) - Give Option to Notify The Accused/Charged Employee of Hearing Schedule
                    If blnNotifyChargedEmployeeOfHearing = True Then
                        If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 AndAlso dsList.Tables(0).Rows.Count > 0 Then
                            Dim objDisciplineFile As New clsDiscipline_file_master
                            Dim dtMain As New DataTable
                            Dim strcolname As String() = New String() {"hDate", "hTime", "hVenue", "hRemark", "hRefNo", "hInvEmp", "hEmail", "hMstId", "disciplinefileunkid", "involved_employeeunkid"}
                            dtMain = dsList.Tables(0).DefaultView.ToTable(True, strcolname)
                            'Dim lstEmpId As New List(Of Integer)
                            'lstEmpId = dtMain.AsEnumerable().Select(Function(x) x.Field(Of Integer)("involved_employeeunkid")).Distinct().ToList()
                            'Dim objMail As New clsSendMail
                            'For Each eEmpId As Integer In lstEmpId
                            '    Dim strSuccess As String = ""
                            '    Dim xRows() As DataRow = dtMain.Select("involved_employeeunkid = " & eEmpId & "")
                            '    If xRows.Length > 0 Then
                            '        Dim strInvEmpEmail As String = ""
                            '        strInvEmpEmail = xRows(0)("hEmail").ToString()
                            '        If strInvEmpEmail.Trim.Length <= 0 Then Exit For
                            '        Dim strMessage As String = "" : Dim intSrNo As Integer = 1 : Dim strBuilder As New StringBuilder
                            '        objMail._Subject = Language.getMessage(mstrModuleName, 3, "Hearing Schedule Notification")
                            '        strBuilder.Append("<HTML><BODY>" & vbCrLf)
                            '        strBuilder.Append(Language.getMessage(mstrModuleName, 5, "Dear") & " " & "&nbsp;<b>" & getTitleCase(xRows(0)("hInvEmp").ToString()) & "</b>" & ",<BR></BR>" & vbCrLf)
                            '        strBuilder.Append(Language.getMessage(mstrModuleName, 100, "This is to inform you that a disciplinary hearing(s) has been scheduled as detailed below;") & "<BR></BR>" & vbCrLf)
                            '        strBuilder.Append("<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=3 WIDTH='100%'>" & vbCrLf)
                            '        strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                            '        strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 101, "Sr.No") & "</B></FONT></TD>" & vbCrLf)
                            '        strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 102, "Ref.No") & "</B></FONT></TD>" & vbCrLf)
                            '        strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 103, "Hearing Date") & "</B></FONT></TD>" & vbCrLf)
                            '        strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 104, "Hearing Time") & "</B></FONT></TD>" & vbCrLf)
                            '        strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 105, "Venue") & "</B></FONT></TD>" & vbCrLf)
                            '        strBuilder.Append("<TD WIDTH='15%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 106, "Remark") & "</B></FONT></TD>" & vbCrLf)

                            '        strBuilder.Append("</TR>" & vbCrLf)
                            '        For Each iR As DataRow In xRows
                            '            strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                            '            strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & intSrNo.ToString & "</FONT></TD>" & vbCrLf)
                            '            strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & iR("hRefNo").ToString() & "</FONT></TD>" & vbCrLf)
                            '            strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & eZeeDate.convertDate(iR("hDate").ToString()).ToShortDateString & "</FONT></TD>" & vbCrLf)
                            '            strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & iR("hTime").ToString() & "</FONT></TD>" & vbCrLf)
                            '            strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & iR("hVenue").ToString() & "</FONT></TD>" & vbCrLf)
                            '            strBuilder.Append("<TD WIDTH='15%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & iR("hRemark").ToString() & "</FONT></TD>" & vbCrLf)
                            '            strBuilder.Append("</TR>" & vbCrLf)
                            '            intSrNo += 1
                            '        Next
                            '        strBuilder.Append("</TABLE><BR><BR>" & vbCrLf)
                            '        strBuilder.Append(Language.getMessage(mstrModuleName, 17, "Regards,") & vbCrLf)
                            '        strBuilder.Append("</BODY></HTML>" & vbCrLf)

                            '        strMessage = strBuilder.ToString()

                            '        objMail._Message = strMessage
                            '        objMail._ToEmail = strInvEmpEmail
                            '        If mintLoginTypeId <= 0 Then mintLoginTypeId = enLogin_Mode.DESKTOP
                            '        If mstrWebFormName.Trim.Length > 0 Then
                            '            objMail._Form_Name = mstrWebFormName
                            '            objMail._WebClientIP = mstrWebClientIP
                            '            objMail._WebHostName = mstrWebHostName
                            '        End If
                            '        objMail._LogEmployeeUnkid = 0
                            '        objMail._OperationModeId = mintLoginTypeId
                            '        objMail._UserUnkid = intUserId
                            '        objMail._SenderAddress = strSender
                            '        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.DISCIPLINE_MGT
                            '        strSuccess = objMail.SendMail(intCompanyUnkId)
                            '    End If
                            'Next

                            Dim objMail As New clsSendMail
                            For Each drRow As DataRow In dtMain.Rows
                                If lstSuccessDisciplinefileIdList.Contains(CInt(drRow.Item("disciplinefileunkid"))) = True Then
                                    Dim strSuccess As String = ""
                                    Dim strInvEmpEmail As String = ""
                                    strInvEmpEmail = drRow.Item("hEmail").ToString()
                                    If strInvEmpEmail.Trim.Length <= 0 Then Exit For
                                    Dim strMessage As String = "" : Dim intSrNo As Integer = 1 : Dim strBuilder As New StringBuilder
                                    objMail._Subject = Language.getMessage(mstrModuleName, 3, "Hearing Schedule Notification")
                                    strBuilder.Append("<HTML><BODY>" & vbCrLf)
                                    strBuilder.Append(Language.getMessage(mstrModuleName, 5, "Dear") & " " & "&nbsp;<b>" & getTitleCase(drRow.Item("hInvEmp").ToString()) & "</b>" & ",<BR></BR>" & vbCrLf)
                                    strBuilder.Append(Language.getMessage(mstrModuleName, 100, "This is to inform you that a disciplinary hearing(s) has been scheduled as detailed below;") & "<BR></BR>" & vbCrLf)
                                    strBuilder.Append("<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=3 WIDTH='100%'>" & vbCrLf)
                                    strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='5%' BGCOLOR='#4682b4' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 101, "Sr.No") & "</B></FONT></TD>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='10%' BGCOLOR='#4682b4' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 102, "Ref.No") & "</B></FONT></TD>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='5%' BGCOLOR='#4682b4' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 103, "Hearing Date") & "</B></FONT></TD>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='5%' BGCOLOR='#4682b4' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 104, "Hearing Time") & "</B></FONT></TD>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='10%' BGCOLOR='#4682b4' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 105, "Venue") & "</B></FONT></TD>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='15%' BGCOLOR='#4682b4' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 106, "Remark") & "</B></FONT></TD>" & vbCrLf)

                                    strBuilder.Append("</TR>" & vbCrLf)

                                    strBuilder.Append("<TR WIDTH='100%'>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & intSrNo.ToString & "</FONT></TD>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & drRow.Item("hRefNo").ToString() & "</FONT></TD>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & eZeeDate.convertDate(drRow.Item("hDate").ToString()).ToShortDateString & "</FONT></TD>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='5%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & drRow.Item("hTime").ToString() & "</FONT></TD>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & drRow.Item("hVenue").ToString() & "</FONT></TD>" & vbCrLf)
                                    strBuilder.Append("<TD WIDTH='15%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2>" & drRow.Item("hRemark").ToString() & "</FONT></TD>" & vbCrLf)
                                    strBuilder.Append("</TR>" & vbCrLf)

                                    strBuilder.Append("</TABLE><BR><BR>" & vbCrLf)

                                    strBuilder.Append(objDisciplineFile.getCountForEmail(CInt(drRow.Item("disciplinefileunkid"))))
                                    strBuilder.Append(Language.getMessage(mstrModuleName, 17, "Regards,") & vbCrLf)
                                    strBuilder.Append("</BODY></HTML>" & vbCrLf)

                                    strMessage = strBuilder.ToString()

                                    objMail._Message = strMessage
                                    objMail._ToEmail = strInvEmpEmail
                                    If mintLoginTypeId <= 0 Then mintLoginTypeId = enLogin_Mode.DESKTOP
                                    If mstrWebFormName.Trim.Length > 0 Then
                                        objMail._Form_Name = mstrWebFormName
                                        objMail._WebClientIP = mstrWebClientIP
                                        objMail._WebHostName = mstrWebHostName
                                    End If
                                    objMail._LogEmployeeUnkid = 0
                                    objMail._OperationModeId = mintLoginTypeId
                                    objMail._UserUnkid = intUserId
                                    objMail._SenderAddress = strSender
                                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.DISCIPLINE_MGT
                                    strSuccess = objMail.SendMail(intCompanyUnkId)
                                End If
                            Next
                            objDisciplineFile = Nothing
                                    End If
                                End If
                    'Hemant (24 Sep 2021) -- End

                    'Hemant (24 Sep 2021) -- Start             
                    'ISSUE/ENHANCEMENT : OLD-481(ZRA) - Give Configurable Option to include Case Information on Hearing Schedule Notification
                    If blnIncludeCaseInfoHearingNotification = True Then
                        If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 AndAlso dsList.Tables(0).Rows.Count > 0 Then
                            Dim objDisciplineFile As New clsDiscipline_file_master
                            Dim dtMain As New DataTable
                            Dim strcolname As String() = New String() {"hDate", "hTime", "hVenue", "hRemark", "hRefNo", "hInvEmp", "hEmail", "hMstId", "disciplinefileunkid", "involved_employeeunkid"}
                            dtMain = dsList.Tables(0).DefaultView.ToTable(True, strcolname)
                            For Each drRow As DataRow In dtMain.Rows
                                If lstSuccessDisciplinefileIdList.Contains(CInt(drRow.Item("disciplinefileunkid"))) = True Then
                                    Dim strSuccess As String = ""
                                    Dim strInvEmpEmail As String = ""
                                    strInvEmpEmail = drRow.Item("hEmail").ToString()
                                    If strInvEmpEmail.Trim.Length <= 0 Then Exit For
                                    Dim objMail As New clsSendMail
                                    Dim strMessage As String = "" : Dim strBuilder As New StringBuilder
                                    objMail._Subject = Language.getMessage(mstrModuleName, 108, "Discipline Charge Notification") & " " & Language.getMessage(mstrModuleName, 4, " :  Reference No - ") & drRow.Item("hRefNo").ToString()
                                    strMessage = "<HTML><BODY>"
                                    strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " <b>" & getTitleCase(drRow.Item("hInvEmp").ToString()) & "</b>" & " ,<BR></BR>"
                                    strMessage &= Language.getMessage(mstrModuleName, 9, "Please refer following Disciplinary Charges details posted against you.")

                                    strMessage &= objDisciplineFile.getChargeDetailForEmail(CInt(drRow.Item("disciplinefileunkid")))
                                    strMessage &= objDisciplineFile.getCountForEmail(CInt(drRow.Item("disciplinefileunkid")))

                                    strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"

                                    strMessage &= "</BODY></HTML>"

                                    objMail._Message = strMessage
                                    objMail._ToEmail = strInvEmpEmail
                                    If mintLoginTypeId <= 0 Then mintLoginTypeId = enLogin_Mode.DESKTOP
                                    If mstrWebFormName.Trim.Length > 0 Then
                                        objMail._Form_Name = mstrWebFormName
                                        objMail._WebClientIP = mstrWebClientIP
                                        objMail._WebHostName = mstrWebHostName
                                    End If
                                    objMail._LogEmployeeUnkid = 0
                                    objMail._OperationModeId = mintLoginTypeId
                                    objMail._UserUnkid = intUserId
                                    objMail._SenderAddress = strSender
                                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.DISCIPLINE_MGT
                                    strSuccess = objMail.SendMail(intCompanyUnkId)
                            End If
                        Next
                            objDisciplineFile = Nothing
                        End If
                    End If
                    'Hemant (24 Sep 2021) -- End
                    blnFlag = True
            Else
                    blnIsNoDatatoSend = True
                    blnFlag = False
            End If
            End Using
            Return blnFlag
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: NotifyMembers; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function SendMailToMembers(ByVal intHearingScheduleMasterUnkid As Integer, _
                                      ByVal intDisciplineFileUnkid As Integer, _
                                      ByVal strMembers As String, _
                                      ByVal strEmail As String, _
                                      ByVal intCompanyUnkId As Integer, _
                                      ByVal strFilesAttached As String) As Boolean 'S.SANDEEP |11-NOV-2019| -- START {strFilesAttached} -- END
        'Try
        '    Dim strMessage As String = String.Empty
        '    Dim strBuilder As New StringBuilder
        '    Dim objNetConn As New clsNetConnectivity
        '    Dim objMail As New clsSendMail
        '    Dim objEmployee As New clsEmployee_Master
        '    Dim objUser As New clsUserAddEdit
        '    Dim objDisciplineFileMaster As New clsDiscipline_file_master
        '    Dim objHearingScheduleMaster As New clshearing_schedule_master

        '    If objNetConn._Conected = False Then Exit Function

        '    objHearingScheduleMaster._Hearingschedulemasterunkid = intHearingScheduleMasterUnkid
        '    objDisciplineFileMaster._Disciplinefileunkid = intDisciplineFileUnkid
        '    Dim strValue As String = ""
        '    Dim objConfig As New clsConfigOptions
        '    strValue = objConfig.GetKeyValue(intCompanyUnkId, "EmployeeAsOnDate")
        '    objConfig = Nothing
        '    If strValue.Trim.Length <= 0 Then strValue = eZeeDate.convertDate(Now.Date)
        '    objEmployee._Employeeunkid(eZeeDate.convertDate(strValue.ToString()), Nothing) = objDisciplineFileMaster._Involved_Employeeunkid

        '    objMail._Subject = Language.getMessage(mstrModuleName, 3, "Hearing Schedule Notification") & " " & Language.getMessage(mstrModuleName, 4, " :  Reference No - ") & objDisciplineFileMaster._Reference_No
        '    strMessage = "<HTML><BODY>"
        '    strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & "&nbsp;<b>" & getTitleCase(strMembers) & "</b>" & ",<BR></BR>"
        '    strMessage &= Language.getMessage(mstrModuleName, 14, "This is to inform you that a disciplinary hearing against") & " " & getTitleCase(objEmployee._Firstname & " " & objEmployee._Surname) & " " & Language.getMessage(mstrModuleName, 15, "with reference NO.") & " " & objDisciplineFileMaster._Reference_No & " " & _
        '                  Language.getMessage(mstrModuleName, 16, "has been scheduled as detailed below;") & "<BR></BR><BR></BR>"

        '    strBuilder.Append("<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=3 WIDTH='60%'>" & vbCrLf)
        '    strBuilder.Append("<TR WIDTH='60%'>" & vbCrLf)
        '    strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 10, "Hearing Date") & "</B></FONT></TD>" & vbCrLf)
        '    strBuilder.Append("<TD WIDTH='50%' ALIGN='LEFT'><FONT SIZE=2>" & objHearingScheduleMaster._HearingDate.Date & "</FONT></TD>" & vbCrLf)
        '    strBuilder.Append("</TR>" & vbCrLf)
        '    strBuilder.Append("<TR WIDTH='60%'>" & vbCrLf)
        '    strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 11, "Hearing Time") & "</B></FONT></TD>" & vbCrLf)
        '    strBuilder.Append("<TD WIDTH='50%' ALIGN='LEFT'><FONT SIZE=2>" & objHearingScheduleMaster._HearningTime.ToShortTimeString & "</FONT></TD>" & vbCrLf)
        '    strBuilder.Append("</TR>" & vbCrLf)
        '    strBuilder.Append("<TR WIDTH='60%'>" & vbCrLf)
        '    strBuilder.Append("<TD WIDTH='10%' ALIGN='LEFT' VALIGN = 'TOP'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 12, "Hearing Venue") & "</B></FONT></TD>" & vbCrLf)
        '    strBuilder.Append("<TD WIDTH='50%' ALIGN='LEFT'><FONT SIZE=2>" & objHearingScheduleMaster._HearningVenue & "</FONT></TD>" & vbCrLf)
        '    strBuilder.Append("</TR>" & vbCrLf)
        '    strBuilder.Append("</TABLE><BR></BR>" & vbCrLf)
        '    strBuilder.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
        '    strMessage &= strBuilder.ToString()

        '    strMessage &= Language.getMessage(mstrModuleName, 17, "Regards,")
        '    strMessage &= "</BODY></HTML>"

        '    Dim strSuccess As String = ""
        '    Dim mintLoginTypeId As Integer = CInt(objHearingScheduleMaster._LoginTypeId)
        '    objUser._Userunkid = objHearingScheduleMaster._Userunkid

        '    objMail._Message = strMessage
        '    objMail._ToEmail = strEmail
        '    If mintLoginTypeId <= 0 Then mintLoginTypeId = enLogin_Mode.DESKTOP
        '    If mstrWebFormName.Trim.Length > 0 Then
        '        objMail._Form_Name = mstrWebFormName
        '        objMail._WebClientIP = mstrWebClientIP
        '        objMail._WebHostName = mstrWebHostName
        '    End If
        '    objMail._LogEmployeeUnkid = objHearingScheduleMaster._LoginEmployeeunkid
        '    objMail._OperationModeId = mintLoginTypeId
        '    objMail._UserUnkid = objHearingScheduleMaster._Userunkid
        '    objMail._SenderAddress = IIf(objUser._Email = "", objUser._Username, objUser._Email)
        '    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.DISCIPLINE_MGT
        '    'S.SANDEEP |11-NOV-2019| -- START
        '    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
        '    objMail._AttachedFiles = strFilesAttached
        '    'S.SANDEEP |11-NOV-2019| -- END
        '    strSuccess = objMail.SendMail(intCompanyUnkId)

        '    If strSuccess.Trim.Length <= 0 Then
        '        Return True
        '    Else
        '        Return False
        '    End If

        'Catch ex As Exception
        '    Throw New Exception(ex.Message & "; Procedure Name: SendMailToMembers; Module Name: " & mstrModuleName)
        'End Try
    End Function
    'S.SANDEEP |01-OCT-2019| -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "WEB")
            Language.setMessage(mstrModuleName, 2, "This hearing schedule is already exists.")
            Language.setMessage(mstrModuleName, 3, "Hearing Schedule Notification")
			Language.setMessage(mstrModuleName, 4, " :  Reference No -")
            Language.setMessage(mstrModuleName, 5, "Dear")
			Language.setMessage(mstrModuleName, 9, "Please refer following Disciplinary Charges details posted against you.")
			Language.setMessage(mstrModuleName, 17, "Regards,")
			Language.setMessage(mstrModuleName, 100, "This is to inform you that a disciplinary hearing(s) has been scheduled as detailed below;")
			Language.setMessage(mstrModuleName, 101, "Sr.No")
			Language.setMessage(mstrModuleName, 102, "Ref.No")
			Language.setMessage(mstrModuleName, 103, "Hearing Date")
			Language.setMessage(mstrModuleName, 104, "Hearing Time")
			Language.setMessage(mstrModuleName, 105, "Venue")
			Language.setMessage(mstrModuleName, 106, "Remark")
			Language.setMessage(mstrModuleName, 107, "Posted Against")
			Language.setMessage(mstrModuleName, 108, "Discipline Charge Notification")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class