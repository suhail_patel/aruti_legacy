﻿'************************************************************************************************************************************
'Class Name : clsDiscipline_Committe.vb
'Purpose    :
'Date       :20/03/2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsDiscipline_Committee
    Private Shared ReadOnly mstrModuleName As String = "clsDiscipline_Committee"
    Private mstrMessage As String = ""
    Private mdtTran As DataTable
    Private mintCommitteeTranId As Integer
    Private mintCommitteeMasterId As Integer

#Region " Properties "


    'Pinkal (22-Jan-2021) -- Start
    'Enhancement NMB - Working Discipline changes required by NMB.
    'Public Property _CommitteeMasterId(ByVal mdtEffectiveDate As Date) As Integer
    Public Property _CommitteeMasterId(ByVal mdtEffectiveDate As Date, ByVal xDatabaseName As String, ByVal xIncludeIn_ActiveEmployee As Boolean) As Integer
        'Pinkal (22-Jan-2021) -- End

        Get
            Return mintCommitteeMasterId
        End Get
        Set(ByVal value As Integer)
            mintCommitteeMasterId = value
            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            'Call Get_Members()

            'Pinkal (22-Jan-2021) -- Start
            'Enhancement NMB - Working Discipline changes required by NMB.
            'Call Get_Members(mdtEffectiveDate)
            Call Get_Members(mdtEffectiveDate, xDatabaseName, xIncludeIn_ActiveEmployee)
            'Pinkal (22-Jan-2021) -- End

            'S.SANDEEP [24 MAY 2016] -- END
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("Committee")

            mdtTran.Columns.Add("committeetranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("committeemasterunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("ex_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("ex_company", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("ex_department", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("ex_contactno", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("isactive", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("employee_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("company", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("department", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("contact", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("Ids", System.Type.GetType("System.String")).DefaultValue = ""
            'S.SANDEEP [ 19 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mdtTran.Columns.Add("mcategoryunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("mcategoryname", System.Type.GetType("System.String")).DefaultValue = ""
            'S.SANDEEP [ 19 DEC 2012 ] -- END

            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            mdtTran.Columns.Add("emailid", GetType(String)).DefaultValue = ""
            mdtTran.Columns.Add("ex_email", System.Type.GetType("System.String")).DefaultValue = ""
            'S.SANDEEP [24 MAY 2016] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private & Public Funtions "


    'S.SANDEEP [24 MAY 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}

    'Pinkal (22-Jan-2021) -- Start
    'Enhancement NMB - Working Discipline changes required by NMB.
    'Private Sub Get_Members(ByVal mdtEffectiveDate As Date)
    Private Sub Get_Members(ByVal mdtEffectiveDate As Date, ByVal xDatabaseName As String, ByVal xIncludeIn_ActiveEmployee As Boolean)
        'Pinkal (22-Jan-2021) -- End

        'Private Sub Get_Members()
        'S.SANDEEP [24 MAY 2016] -- END
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim objDataOp As New clsDataOperation
        Try
            'S.SANDEEP [ 19 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT " & _
            '       " hrdiscipline_committee.committeetranunkid " & _
            '       ",hrdiscipline_committee.committeemasterunkid " & _
            '       ",hrdiscipline_committee.employeeunkid " & _
            '       ",hrdiscipline_committee.ex_name " & _
            '       ",hrdiscipline_committee.ex_company " & _
            '       ",hrdiscipline_committee.ex_department " & _
            '       ",hrdiscipline_committee.ex_contactno " & _
            '       ",hrdiscipline_committee.isactive " & _
            '       ",'' AS AUD " & _
            '       ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS employee_name " & _
            '       ",'" & Company._Object._Name & "' AS company " & _
            '       ",ISNULL(hrdepartment_master.name,'') AS department " & _
            '       ",ISNULL(present_tel_no,'') AS contact " & _
            '       ",(SELECT ISNULL(STUFF((SELECT ',' + CAST(s.committeetranunkid AS NVARCHAR(50)) FROM hrdiscipline_committee s  WHERE s.committeemasterunkid = '" & mintCommitteeMasterId & "' AND isactive = 1 ORDER BY s.committeetranunkid FOR XML PATH('')),1,1,''),'')) AS Ids " & _
            '       "FROM hrdiscipline_committee " & _
            '       "LEFT JOIN hremployee_master ON hrdiscipline_committee.employeeunkid = hremployee_master.employeeunkid " & _
            '       "LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '       "WHERE hrdiscipline_committee.isactive = 1 AND hrdiscipline_committee.committeemasterunkid = '" & mintCommitteeMasterId & "' "

            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'StrQ = "SELECT " & _
            '           " hrdiscipline_committee.committeetranunkid " & _
            '           ",hrdiscipline_committee.committeemasterunkid " & _
            '           ",hrdiscipline_committee.employeeunkid " & _
            '           ",hrdiscipline_committee.ex_name " & _
            '           ",hrdiscipline_committee.ex_company " & _
            '           ",hrdiscipline_committee.ex_department " & _
            '           ",hrdiscipline_committee.ex_contactno " & _
            '           ",hrdiscipline_committee.isactive " & _
            '           ",'' AS AUD " & _
            '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS employee_name " & _
            '           ",cfcompany_master.name AS company " & _
            '           ",ISNULL(hrdepartment_master.name,'') AS department " & _
            '           ",ISNULL(present_tel_no,'') AS contact " & _
            '           ",ISNULL(hremployee_master.email,'') AS emailid " & _
            '           ",(SELECT ISNULL(STUFF((SELECT ',' + CAST(s.committeetranunkid AS NVARCHAR(50)) FROM hrdiscipline_committee s  WHERE s.committeemasterunkid = '" & mintCommitteeMasterId & "' AND isactive = 1 ORDER BY s.committeetranunkid FOR XML PATH('')),1,1,''),'')) AS Ids " & _
            '           ",ISNULL(MCat.name,'') AS mcategoryname " & _
            '           ",mcategoryunkid AS mcategoryunkid " & _
            '           ",hrdiscipline_committee.ex_email " & _
            '       "FROM hrdiscipline_committee " & _
            '           "LEFT JOIN cfcommon_master AS MCat ON MCat.masterunkid = hrdiscipline_committee.mcategoryunkid " & _
            '           "LEFT JOIN hremployee_master ON hrdiscipline_committee.employeeunkid = hremployee_master.employeeunkid " & _
            '           "LEFT JOIN hrmsConfiguration..cfcompany_master ON cfcompany_master.companyunkid = hremployee_master.companyunkid " & _
            '           "LEFT JOIN " & _
            '           "( " & _
            '           "  SELECT " & _
            '           "     hremployee_transfer_tran.employeeunkid " & _
            '           "    ,hremployee_transfer_tran.departmentunkid " & _
            '           "    ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
            '           "  FROM hremployee_transfer_tran " & _
            '           "  WHERE hremployee_transfer_tran.isvoid = 0 AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(mdtEffectiveDate) & "' " & _
            '           " ) AS A ON A.employeeunkid = hremployee_master.employeeunkid AND A.rno = 1 " & _
            '           "LEFT JOIN hrdepartment_master ON A.departmentunkid = hrdepartment_master.departmentunkid " & _
            '       "WHERE hrdiscipline_committee.isactive = 1 AND hrdiscipline_committee.committeemasterunkid = '" & mintCommitteeMasterId & "' "



            'Pinkal (22-Jan-2021) -- Start
            'Enhancement NMB - Working Discipline changes required by NMB.
            Dim xDateJoinQry, xDataFilterQry As String
            xDateJoinQry = "" : xDataFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, mdtEffectiveDate, mdtEffectiveDate, True, False, xDatabaseName)
            'Pinkal (22-Jan-2021) -- End


            StrQ = "SELECT " & _
                       " hrdiscipline_committee.committeetranunkid " & _
                       ",hrdiscipline_committee.committeemasterunkid " & _
                       ",hrdiscipline_committee.employeeunkid " & _
                       ",hrdiscipline_committee.ex_name " & _
                       ",hrdiscipline_committee.ex_company " & _
                       ",hrdiscipline_committee.ex_department " & _
                       ",hrdiscipline_committee.ex_contactno " & _
                       ",hrdiscipline_committee.isactive " & _
                       ",'' AS AUD " & _
                       ",ISNULL(firstname,'')+' '+ISNULL(surname,'') AS employee_name " & _
                       ",cfcompany_master.name AS company " & _
                       ",ISNULL(hrdepartment_master.name,'') AS department " & _
                       ",ISNULL(present_tel_no,'') AS contact " & _
                       ",ISNULL(hremployee_master.email,'') AS emailid " & _
                       ",(SELECT ISNULL(STUFF((SELECT ',' + CAST(s.committeetranunkid AS NVARCHAR(50)) FROM hrdiscipline_committee s  WHERE s.committeemasterunkid = '" & mintCommitteeMasterId & "' AND isactive = 1 ORDER BY s.committeetranunkid FOR XML PATH('')),1,1,''),'')) AS Ids " & _
                       ",ISNULL(MCat.name,'') AS mcategoryname " & _
                       ",mcategoryunkid AS mcategoryunkid " & _
                       ",hrdiscipline_committee.ex_email " & _
                   "FROM hrdiscipline_committee " & _
                       "LEFT JOIN cfcommon_master AS MCat ON MCat.masterunkid = hrdiscipline_committee.mcategoryunkid " & _
                       "LEFT JOIN hremployee_master ON hrdiscipline_committee.employeeunkid = hremployee_master.employeeunkid " & _
                       "LEFT JOIN hrmsConfiguration..cfcompany_master ON cfcompany_master.companyunkid = hremployee_master.companyunkid " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "  SELECT " & _
                       "     hremployee_transfer_tran.employeeunkid " & _
                       "    ,hremployee_transfer_tran.departmentunkid " & _
                       "    ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                       "  FROM hremployee_transfer_tran " & _
                       "  WHERE hremployee_transfer_tran.isvoid = 0 AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(mdtEffectiveDate) & "' " & _
                       " ) AS A ON A.employeeunkid = hremployee_master.employeeunkid AND A.rno = 1 " & _
                       "LEFT JOIN hrdepartment_master ON A.departmentunkid = hrdepartment_master.departmentunkid "


            'Pinkal (22-Jan-2021) -- Start
            'Enhancement NMB - Working Discipline changes required by NMB.
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            StrQ &= "WHERE hrdiscipline_committee.isactive = 1 AND hrdiscipline_committee.committeemasterunkid = '" & mintCommitteeMasterId & "' "


            If xIncludeIn_ActiveEmployee = False Then
                If xDataFilterQry.Trim.Length > 0 Then
                    StrQ &= xDataFilterQry
                End If
            End If
            'Pinkal (22-Jan-2021) -- End


            'S.SANDEEP |01-OCT-2019| -- END


            'S.SANDEEP [24 MAY 2016] -- [ex_email]
            'S.SANDEEP [ 19 DEC 2012 ] -- END

            dsList = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                Throw New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
            End If

            mdtTran.Rows.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(dtRow)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Members; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function InsertUpdateDelete_Members() As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOpr As New clsDataOperation
        objDataOpr.BindTransaction()
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOpr.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO hrdiscipline_committee ( " & _
                                        "  committeemasterunkid " & _
                                        ", employeeunkid " & _
                                        ", ex_name " & _
                                        ", ex_company " & _
                                        ", ex_department " & _
                                        ", ex_contactno " & _
                                        ", isactive" & _
                                        ", mcategoryunkid " & _
                                        ", ex_email " & _
                                       ") VALUES (" & _
                                        "  @committeemasterunkid " & _
                                        ", @employeeunkid " & _
                                        ", @ex_name " & _
                                        ", @ex_company " & _
                                        ", @ex_department " & _
                                        ", @ex_contactno " & _
                                        ", @isactive" & _
                                        ", @mcategoryunkid " & _
                                        ", @ex_email " & _
                                      "); SELECT @@identity" 'S.SANDEEP [ 19 DEC 2012 ] -- START [mcategoryunkid] -- END
                                'S.SANDEEP [24 MAY 2016] -- [ex_email]

                                objDataOpr.AddParameter("@committeemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeemasterunkid").ToString)
                                objDataOpr.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOpr.AddParameter("@ex_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_name").ToString)
                                objDataOpr.AddParameter("@ex_company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_company").ToString)
                                objDataOpr.AddParameter("@ex_department", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_department").ToString)
                                objDataOpr.AddParameter("@ex_contactno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_contactno").ToString)
                                objDataOpr.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isactive").ToString)
                                'S.SANDEEP [ 19 DEC 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                objDataOpr.AddParameter("@mcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("mcategoryunkid").ToString)
                                'S.SANDEEP [ 19 DEC 2012 ] -- END

                                'S.SANDEEP [24 MAY 2016] -- START
                                'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                                objDataOpr.AddParameter("@ex_email", SqlDbType.NVarChar, eZeeDataType.EMAIL_SIZE, .Item("ex_email").ToString)
                                'S.SANDEEP [24 MAY 2016] -- END

                                dsList = objDataOpr.ExecQuery(StrQ, "List")

                                If objDataOpr.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                                mintCommitteeTranId = dsList.Tables(0).Rows(0).Item(0)

                                If clsCommonATLog.Insert_AtLog(objDataOpr, 1, "hrdiscipline_committee", "committeetranunkid", mintCommitteeTranId) = False Then
                                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "U"
                                StrQ = "UPDATE hrdiscipline_committee SET " & _
                                       "  committeemasterunkid = @committeemasterunkid" & _
                                       ", employeeunkid = @employeeunkid" & _
                                       ", ex_name = @ex_name" & _
                                       ", ex_company = @ex_company" & _
                                       ", ex_department = @ex_department" & _
                                       ", ex_contactno = @ex_contactno" & _
                                       ", isactive = @isactive " & _
                                       ", mcategoryunkid = @mcategoryunkid " & _
                                       ", ex_email = @ex_email " & _
                                       "WHERE committeetranunkid = @committeetranunkid " 'S.SANDEEP [ 19 DEC 2012 ] -- START [mcategoryunkid] -- END
                                'S.SANDEEP [24 MAY 2016] -- [ex_email]

                                objDataOpr.AddParameter("@committeetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeetranunkid").ToString)
                                objDataOpr.AddParameter("@committeemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeemasterunkid").ToString)
                                objDataOpr.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOpr.AddParameter("@ex_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_name").ToString)
                                objDataOpr.AddParameter("@ex_company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_company").ToString)
                                objDataOpr.AddParameter("@ex_department", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_department").ToString)
                                objDataOpr.AddParameter("@ex_contactno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ex_contactno").ToString)
                                objDataOpr.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isactive").ToString)
                                'S.SANDEEP [ 19 DEC 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                objDataOpr.AddParameter("@mcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("mcategoryunkid").ToString)
                                'S.SANDEEP [ 19 DEC 2012 ] -- END

                                'S.SANDEEP [24 MAY 2016] -- START
                                'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                                objDataOpr.AddParameter("@ex_email", SqlDbType.NVarChar, eZeeDataType.EMAIL_SIZE, .Item("ex_email").ToString)
                                'S.SANDEEP [24 MAY 2016] -- END

                                objDataOpr.ExecNonQuery(StrQ)

                                If clsCommonATLog.Insert_AtLog(objDataOpr, 2, "hrdiscipline_committee", "committeetranunkid", .Item("committeetranunkid")) = False Then
                                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                                    Throw exForce
                                End If
                            Case "D"

                                StrQ = "UPDATE hrdiscipline_committee SET " & _
                                       " isactive = 0 " & _
                                       "WHERE committeetranunkid = @committeetranunkid "

                                objDataOpr.AddParameter("@committeetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeetranunkid").ToString)
                                objDataOpr.ExecNonQuery(StrQ)

                                If clsCommonATLog.Insert_AtLog(objDataOpr, 3, "hrdiscipline_committee", "committeetranunkid", .Item("committeetranunkid")) = False Then
                                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next

            objDataOpr.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOpr.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_Members; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function GetList() As DataSet
    Public Function GetList(ByVal strCompanyName As String, Optional ByVal intCommitteeMasterId As Integer = 0) As DataSet
        'Shani(24-Aug-2015) -- Endt

        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim objDataOpr As New clsDataOperation
        Try
            StrQ = "SELECT " & _
                    "	 hrdiscipline_committee.committeetranunkid " & _
                    "	,hrdiscipline_committee.committeemasterunkid " & _
                    "	,hrdiscipline_committee.employeeunkid " & _
                    "	,hrdiscipline_committee.ex_name " & _
                    "	,hrdiscipline_committee.ex_company " & _
                    "	,hrdiscipline_committee.ex_department " & _
                    "	,hrdiscipline_committee.ex_contactno " & _
                    "	,hrdiscipline_committee.isactive " & _
                    "	,CASE WHEN hrdiscipline_committee.employeeunkid <= 0 THEN @External " & _
                    "		  WHEN hrdiscipline_committee.employeeunkid > 0 THEN  @Internal " & _
                    "	 END AS MType " & _
                     "	,CASE WHEN hrdiscipline_committee.employeeunkid <= 0 THEN 1 " & _
                    "		  WHEN hrdiscipline_committee.employeeunkid > 0 THEN  2 " & _
                    "	 END AS MTypeId " & _
                    "	,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS employee_name " & _
                    "	,'" & strCompanyName & "' AS company " & _
                    "	,ISNULL(hrdepartment_master.name,'') AS department " & _
                    "	,ISNULL(present_tel_no,'') AS contact " & _
                    "	,ISNULL(cfcommon_master.name,'') AS committee " & _
                    "   ,ISNULL(MCat.name,'') AS mcategoryname " & _
                    "   ,mcategoryunkid AS mcategoryunkid " & _
                    "   ,hrdiscipline_committee.ex_email " & _
                    "	,CASE WHEN hrdiscipline_committee.employeeunkid <= 0 THEN hrdiscipline_committee.ex_name " & _
                    "		  WHEN hrdiscipline_committee.employeeunkid > 0 THEN  ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') " & _
                    "	 END AS investigator_name " & _
                    "FROM hrdiscipline_committee " & _
                    "   LEFT JOIN cfcommon_master AS MCat ON MCat.masterunkid = hrdiscipline_committee.mcategoryunkid AND MCat.mastertype ='" & clsCommon_Master.enCommonMaster.COMMITTEE_MEMBERS_CATEGORY & "' " & _
                    "	JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdiscipline_committee.committeemasterunkid AND cfcommon_master.mastertype ='" & clsCommon_Master.enCommonMaster.DISCIPLINE_COMMITTEE & "'  " & _
                    "	LEFT JOIN hremployee_master ON hrdiscipline_committee.employeeunkid = hremployee_master.employeeunkid " & _
                    "	LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                    "WHERE hrdiscipline_committee.isactive = 1 "

            If intCommitteeMasterId > 0 Then
                StrQ &= " AND hrdiscipline_committee.committeemasterunkid = " & intCommitteeMasterId & " "
            End If
            'S.SANDEEP [24 MAY 2016] -- [ex_email]
            'Shani(24-Aug-2015) -- [Company._Object._Name-->strCompanyName]
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            objDataOpr.AddParameter("@External", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "External"))
            objDataOpr.AddParameter("@Internal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Internal"))
            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim blnFlag As Boolean = False
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT * FROM hrdiscipline_investigators WHERE committeetranunkid = '" & intUnkid & "' "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If


            If dsList.Tables(0).Rows.Count > 0 Then
                blnFlag = True
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "External")
			Language.setMessage(mstrModuleName, 2, "Internal")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
