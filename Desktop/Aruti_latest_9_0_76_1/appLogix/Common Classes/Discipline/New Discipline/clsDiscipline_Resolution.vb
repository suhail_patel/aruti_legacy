﻿'************************************************************************************************************************************
'Class Name : clsDiscipline_Resolution.vb
'Purpose    :
'Date       :28/11/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsDiscipline_Resolution
    Private Shared ReadOnly mstrModuleName As String = "clsDiscipline_Resolution"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Dim objInvestigator As New clsDiscipline_Investigators

#Region " Private variables "

    Private mintResolutionunkid As Integer
    Private mintDisciplinefileunkid As Integer
    Private mdtTrandate As Date
    Private mintDisciplinaryactionunkid As Integer
    Private mdtAction_Start_Date As Date
    Private mdtAction_End_Date As Date
    Private mintStatusunkid As Integer
    Private mstrResolutionstep As String = String.Empty
    Private mstrRemark As String = String.Empty
    Private mblnIsapproved As Boolean
    Private mintApproveduserunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintInvolvedEmpId As Integer = 0
    'S.SANDEEP [ 20 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
    Private mintCommitteemasterunkid As Integer
    'S.SANDEEP [ 20 MARCH 2012 ] -- END

    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIsExReOpen As Boolean = False
    Private mstrCaseno As String = String.Empty
    Private mstrCourtname As String = String.Empty
    Private mdtApprovaldate As Date
    'S.SANDEEP [ 20 APRIL 2012 ] -- END

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set resolutionunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Resolutionunkid() As Integer
        Get
            Return mintResolutionunkid
        End Get
        Set(ByVal value As Integer)
            mintResolutionunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disciplinefileunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Disciplinefileunkid() As Integer
        Get
            Return mintDisciplinefileunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplinefileunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trandate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Trandate() As Date
        Get
            Return mdtTrandate
        End Get
        Set(ByVal value As Date)
            mdtTrandate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disciplinaryactionunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Disciplinaryactionunkid() As Integer
        Get
            Return mintDisciplinaryactionunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplinaryactionunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set action_start_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Action_Start_Date() As Date
        Get
            Return mdtAction_Start_Date
        End Get
        Set(ByVal value As Date)
            mdtAction_Start_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set action_end_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Action_End_Date() As Date
        Get
            Return mdtAction_End_Date
        End Get
        Set(ByVal value As Date)
            mdtAction_End_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disciplinestatusunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set resolutionstep
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Resolutionstep() As String
        Get
            Return mstrResolutionstep
        End Get
        Set(ByVal value As String)
            mstrResolutionstep = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isapproved
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isapproved() As Boolean
        Get
            Return mblnIsapproved
        End Get
        Set(ByVal value As Boolean)
            mblnIsapproved = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approveduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Approveduserunkid() As Integer
        Get
            Return mintApproveduserunkid
        End Get
        Set(ByVal value As Integer)
            mintApproveduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _InvolvedEmpId() As Integer
        Set(ByVal value As Integer)
            mintInvolvedEmpId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set committeemasterunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Committeemasterunkid() As Integer
        Get
            Return mintCommitteemasterunkid
        End Get
        Set(ByVal value As Integer)
            mintCommitteemasterunkid = value
        End Set
    End Property

    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _IsExReOpen() As Boolean
        Set(ByVal value As Boolean)
            mblnIsExReOpen = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set caseno
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Caseno() As String
        Get
            Return mstrCaseno
        End Get
        Set(ByVal value As String)
            mstrCaseno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set courtname
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Courtname() As String
        Get
            Return mstrCourtname
        End Get
        Set(ByVal value As String)
            mstrCourtname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvaldate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Approvaldate() As Date
        Get
            Return mdtApprovaldate
        End Get
        Set(ByVal value As Date)
            mdtApprovaldate = value
        End Set
    End Property
    'S.SANDEEP [ 20 APRIL 2012 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOp As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  resolutionunkid " & _
              ", disciplinefileunkid " & _
              ", trandate " & _
              ", disciplinaryactionunkid " & _
              ", action_start_date " & _
              ", action_end_date " & _
              ", disciplinestatusunkid " & _
              ", resolutionstep " & _
              ", remark " & _
              ", ISNULL(hrdiscipline_resolutions.isapproved,0) as isapproved " & _
              ", approveduserunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", committeemasterunkid " & _
                   ", caseno " & _
                   ", courtname " & _
                   ", approvaldate " & _
             "FROM hrdiscipline_resolutions " & _
             "WHERE resolutionunkid = @resolutionunkid "
            'S.SANDEEP [ 20 MARCH 2012 committeemasterunkid ] -- START -- END

            objDataOp.AddParameter("@resolutionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResolutionunkid.ToString)

            dsList = objDataOp.ExecQuery(strQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintResolutionunkid = CInt(dtRow.Item("resolutionunkid"))
                mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
                mdtTrandate = dtRow.Item("trandate")
                mintDisciplinaryactionunkid = CInt(dtRow.Item("disciplinaryactionunkid"))
                mdtAction_Start_Date = dtRow.Item("action_start_date")
                If IsDBNull(dtRow.Item("action_end_date")) Then
                    mdtAction_End_Date = Nothing
                Else
                    mdtAction_End_Date = dtRow.Item("action_end_date")
                End If
                mintStatusunkid = CInt(dtRow.Item("disciplinestatusunkid"))
                mstrResolutionstep = dtRow.Item("resolutionstep").ToString
                mstrRemark = dtRow.Item("remark").ToString
                mblnIsapproved = CBool(dtRow.Item("isapproved"))
                mintApproveduserunkid = CInt(dtRow.Item("approveduserunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'S.SANDEEP [ 20 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
                mintCommitteemasterunkid = CInt(dtRow.Item("committeemasterunkid"))
                'S.SANDEEP [ 20 MARCH 2012 ] -- END

                'S.SANDEEP [ 20 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mstrCaseno = dtRow.Item("caseno").ToString
                mstrCourtname = dtRow.Item("courtname").ToString
                If IsDBNull(dtRow.Item("approvaldate")) Then
                    mdtApprovaldate = Nothing
                Else
                    mdtApprovaldate = dtRow.Item("approvaldate")
                End If
                'S.SANDEEP [ 20 APRIL 2012 ] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnOnlyActive As Boolean = True, _
                            Optional ByVal xFilterString As String = "") As DataSet 'S.SANDEEP [04 JUN 2015] -- START -- END
        'Public Function GetList(ByVal strTableName As String,Optional ByVal blnOnlyActive As Boolean = True) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

        Try
            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            'strQ = "SELECT " & _
            '         " hrdiscipline_resolutions.resolutionunkid " & _
            '         ",hrdiscipline_resolutions.disciplinefileunkid " & _
            '         ",hrdiscipline_resolutions.trandate " & _
            '         ",hrdiscipline_resolutions.disciplinaryactionunkid " & _
            '         ",hrdiscipline_resolutions.action_start_date " & _
            '         ",hrdiscipline_resolutions.action_end_date " & _
            '         ",hrdiscipline_resolutions.disciplinestatusunkid " & _
            '         ",hrdiscipline_resolutions.resolutionstep " & _
            '         ",hrdiscipline_resolutions.remark " & _
            '         ",ISNULL(hrdiscipline_resolutions.isapproved,0) as isapproved  " & _
            '         ",hrdiscipline_resolutions.approveduserunkid " & _
            '         ",hrdiscipline_resolutions.userunkid " & _
            '         ",hrdiscipline_resolutions.isvoid " & _
            '         ",hrdiscipline_resolutions.voiduserunkid " & _
            '         ",hrdiscipline_resolutions.voiddatetime " & _
            '         ",hrdiscipline_resolutions.voidreason " & _
            '         ",ISNULL(hrdisciplinestatus_master.name,'') AS DStatus " & _
            '         ",CASE WHEN ISNULL(hrdiscipline_resolutions.isapproved,0) = 1 THEN @Approved " & _
            '         "      WHEN ISNULL(hrdiscipline_resolutions.isapproved,0) = 0 THEN @Pending " & _
            '         " END AS AStatus " & _
            '         ",ISNULL(Involved.firstname,'')+' '+ISNULL(Involved.othername,'')+' '+ISNULL(Involved.surname,'') AS InvolvedEmployee " & _
            '         ",ISNULL(Against.firstname,'')+' '+ISNULL(Against.othername,'')+' '+ISNULL(Against.surname,'') AS AgainstEmployee " & _
            '         ",hrdiscipline_file.disciplinetypeunkid " & _
            '         ",hrdiscipline_file.against_employeeunkid " & _
            '         ",hrdiscipline_file.involved_employeeunkid " & _
            '         ",ISNULL(hraction_reason_master.reason_action,'') AS DAction " & _
            '         ",CONVERT(CHAR(8),hrdiscipline_resolutions.trandate,112) AS RDate " & _
            '         ",CONVERT(CHAR(8),hrdiscipline_resolutions.action_start_date,112) AS A_F_Date " & _
            '         ",CONVERT(CHAR(8),hrdiscipline_resolutions.action_end_date,112) AS A_T_DATE " & _
            '         ",ISNULL(hrdiscipline_file.incident,'') AS incident " & _
            '         ",ISNULL(Involved.employeecode,'') AS InvolvedEmployeecode " & _
            '         ",ISNULL(hrdiscipline_file.isapproved,0) AS isfinalapproved " & _
            '         ",ISNULL(Involved.email,'') AS Email " & _
            '         ", committeemasterunkid " & _
            '        "FROM hrdiscipline_resolutions " & _
            '         " LEFT JOIN hrdisciplinestatus_master ON hrdisciplinestatus_master.disciplinestatusunkid = hrdiscipline_resolutions.disciplinestatusunkid " & _
            '         " JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = hrdiscipline_resolutions.disciplinaryactionunkid AND hraction_reason_master.isreason = 0 " & _
            '         " JOIN hrdiscipline_file ON hrdiscipline_resolutions.disciplinefileunkid = hrdiscipline_file.disciplinefileunkid " & _
            '         " JOIN hremployee_master AS Involved ON hrdiscipline_file.involved_employeeunkid = Involved.employeeunkid " & _
            '         " LEFT JOIN hremployee_master AS Against ON hrdiscipline_file.against_employeeunkid = Against.employeeunkid " & _
            '         " WHERE 1 = 1 "
            strQ = "SELECT " & _
                     " hrdiscipline_resolutions.resolutionunkid " & _
                     ",hrdiscipline_resolutions.disciplinefileunkid " & _
                     ",hrdiscipline_resolutions.trandate " & _
                     ",hrdiscipline_resolutions.disciplinaryactionunkid " & _
                     ",hrdiscipline_resolutions.action_start_date " & _
                     ",hrdiscipline_resolutions.action_end_date " & _
                     ",hrdiscipline_resolutions.disciplinestatusunkid " & _
                     ",hrdiscipline_resolutions.resolutionstep " & _
                     ",hrdiscipline_resolutions.remark " & _
                     ",ISNULL(hrdiscipline_resolutions.isapproved,0) as isapproved  " & _
                     ",hrdiscipline_resolutions.approveduserunkid " & _
                     ",hrdiscipline_resolutions.userunkid " & _
                     ",hrdiscipline_resolutions.isvoid " & _
                     ",hrdiscipline_resolutions.voiduserunkid " & _
                     ",hrdiscipline_resolutions.voiddatetime " & _
                     ",hrdiscipline_resolutions.voidreason " & _
                     ",ISNULL(hrdisciplinestatus_master.name,'') AS DStatus " & _
                     ",CASE WHEN ISNULL(hrdiscipline_resolutions.isapproved,0) = 1 THEN @Approved " & _
                     "      WHEN ISNULL(hrdiscipline_resolutions.isapproved,0) = 0 THEN @Pending " & _
                     " END AS AStatus " & _
                     ",ISNULL(Involved.firstname,'')+' '+ISNULL(Involved.othername,'')+' '+ISNULL(Involved.surname,'') AS InvolvedEmployee " & _
                     ",ISNULL(Against.firstname,'')+' '+ISNULL(Against.othername,'')+' '+ISNULL(Against.surname,'') AS AgainstEmployee " & _
                     ",hrdiscipline_file.disciplinetypeunkid " & _
                     ",hrdiscipline_file.against_employeeunkid " & _
                     ",hrdiscipline_file.involved_employeeunkid " & _
                     ",ISNULL(hraction_reason_master.reason_action,'') AS DAction " & _
                     ",CONVERT(CHAR(8),hrdiscipline_resolutions.trandate,112) AS RDate " & _
                     ",CONVERT(CHAR(8),hrdiscipline_resolutions.action_start_date,112) AS A_F_Date " & _
                     ",CONVERT(CHAR(8),hrdiscipline_resolutions.action_end_date,112) AS A_T_DATE " & _
                     ",ISNULL(hrdiscipline_file.incident,'') AS incident " & _
                     ",ISNULL(Involved.employeecode,'') AS InvolvedEmployeecode " & _
                     ",ISNULL(Involved.email,'') AS Email " & _
                     ",ISNULL(hrdiscipline_file.disciplinestatusunkid,0) AS final_statusId " & _
                     ",hrdiscipline_resolutions.caseno AS caseno " & _
                     ",hrdiscipline_resolutions.courtname AS courtname " & _
                     ",ISNULL(CONVERT(CHAR(8),hrdiscipline_resolutions.approvaldate,112),'') AS approvaldate " & _
                     ",committeemasterunkid " & _
                    "FROM hrdiscipline_resolutions " & _
                     " LEFT JOIN hrdisciplinestatus_master ON hrdisciplinestatus_master.disciplinestatusunkid = hrdiscipline_resolutions.disciplinestatusunkid " & _
                     " JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = hrdiscipline_resolutions.disciplinaryactionunkid AND hraction_reason_master.isreason = 0 " & _
                     " JOIN hrdiscipline_file ON hrdiscipline_resolutions.disciplinefileunkid = hrdiscipline_file.disciplinefileunkid " & _
                     " JOIN hremployee_master AS Involved ON hrdiscipline_file.involved_employeeunkid = Involved.employeeunkid " & _
                     " LEFT JOIN hremployee_master AS Against ON hrdiscipline_file.against_employeeunkid = Against.employeeunkid "

            'S.SANDEEP [15 NOV 2016] -- START
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry.Replace("hremployee_master", "Involved")
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry.Replace("hremployee_master", "Involved")
                strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bTRM\b", "ITRM")
                strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bRET\b", "IRET")
                strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bHIRE\b", "IHIRE")
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry.Replace("hremployee_master", "Involved")
                    strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bTRM\b", "ITRM")
                    strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bRET\b", "IRET")
                    strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bHIRE\b", "IHIRE")
                End If
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & System.Text.RegularExpressions.Regex.Replace(xUACFiltrQry, "\bhremployee_master\b", "Involved") & " "
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry.Replace("hremployee_master", "Against")
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry.Replace("hremployee_master", "Against")
                strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bTRM\b", "ATRM")
                strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bRET\b", "ARET")
                strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bHIRE\b", "AHIRE")
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry.Replace("hremployee_master", "Against")
                    strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bTRM\b", "ATRM")
                    strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bRET\b", "ARET")
                    strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bHIRE\b", "AHIRE")
                End If
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & System.Text.RegularExpressions.Regex.Replace(xUACFiltrQry, "\bhremployee_master\b", "Against") & " "
            End If


            strQ &= " WHERE 1 = 1 "


            'S.SANDEEP [ 20 MARCH 2012 ] -- END
            If blnOnlyActive Then
                strQ &= " AND hrdiscipline_resolutions.isvoid = 0 "
            End If

            
            
            

            

            

            If xFilterString.Trim.Length > 0 Then
                strQ &= " AND " & xFilterString
            End If
            'S.SANDEEP [04 JUN 2015] -- END

            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Pending"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrdiscipline_resolutions) </purpose>
    Public Function Insert(ByVal strDatabaseName As String, _
                           ByVal intYearId As Integer, _
                           ByVal intCompanyId As Integer, _
                           ByVal dtTotal_Active_Employee_AsOnDate As Date, _
                           ByVal blnIsArutiDemo As String, _
                           ByVal intTotal_Active_Employee_ForAllCompany As Integer, _
                           ByVal intNoOfEmployees As Integer, _
                           ByVal intUserUnkId As Integer, _
                           ByVal blnDonotAttendanceinSeconds As Boolean, _
                           ByVal strUserAccessMode As String, _
                           ByVal xPeriodStart As DateTime, _
                           ByVal xPeriodEnd As DateTime, _
                           ByVal xOnlyApproved As Boolean, _
                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                           ByVal blnAllowToApproveEarningDeduction As Boolean, _
                           ByVal dtCurrentDateTime As DateTime, _
                           ByVal mblnCreateADUserFromEmp As Boolean, _
                           ByVal mblnUserMustChangePwdOnNextLogOn As Boolean, _
                           ByVal blnAllowToChangeEOCLeavingDateOnClosedPeriod As Boolean, _
                           Optional ByVal dtTable As DataTable = Nothing, _
                           Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                           Optional ByVal strFilerString As String = "", _
                           Optional ByVal xHostName As String = "", _
                           Optional ByVal xIPAddr As String = "", _
                           Optional ByVal xLoggedUserName As String = "", _
                           Optional ByVal xLoginMod As enLogin_Mode = enLogin_Mode.DESKTOP _
                           ) As Boolean 'S.SANDEEP [04 JUN 2015] -- START {intInvolved_Personunkid,blnDonotAttendanceinSeconds _
        'Sohail (09 Oct 2019) - [mblnCreateADUserFromEmp, mblnUserMustChangePwdOnNextLogOn, blnAllowToChangeEOCLeavingDateOnClosedPeriod]
        'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{xHostName,xIPAddr,xLoggedUserName,xLoginMod})} -- END

        'Sohail (21 Aug 2015) - [strDatabaseName]
        'dtTotal_Active_Employee_AsOnDate,blnIsArutiDemo,intTotal_Active_Employee_ForAllCompany,intNoOfEmployees,intNoOfEmployees} -- END

        If isExist(mdtTrandate, mintDisciplinaryactionunkid, mstrResolutionstep, mintInvolvedEmpId) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, you cannot add same resolution step on the same date for particular employee. Please define new resolution step.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.BindTransaction()

            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid.ToString)
            objDataOperation.AddParameter("@trandate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTrandate)
            objDataOperation.AddParameter("@disciplinaryactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinaryactionunkid.ToString)
            objDataOperation.AddParameter("@action_start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAction_Start_Date)
            If mdtAction_End_Date <> Nothing Then
                objDataOperation.AddParameter("@action_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAction_End_Date)
            Else
                objDataOperation.AddParameter("@action_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@disciplinestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@resolutionstep", SqlDbType.VarChar, 8000, mstrResolutionstep.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.VarChar, 8000, mstrRemark.ToString)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@approveduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveduserunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            objDataOperation.AddParameter("@committeemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCommitteemasterunkid.ToString)
            'S.SANDEEP [ 20 MARCH 2012 ] -- END

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@caseno", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrCaseno.ToString)
            objDataOperation.AddParameter("@courtname", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrCourtname.ToString)
            If mdtApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            Else
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP [ 20 APRIL 2012 ] -- END

            strQ = "INSERT INTO hrdiscipline_resolutions ( " & _
              "  disciplinefileunkid " & _
              ", trandate " & _
              ", disciplinaryactionunkid " & _
              ", action_start_date " & _
              ", action_end_date " & _
              ", disciplinestatusunkid " & _
              ", resolutionstep " & _
              ", remark " & _
              ", isapproved " & _
              ", approveduserunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", committeemasterunkid" & _
              ", caseno " & _
              ", courtname " & _
              ", approvaldate " & _
            ") VALUES (" & _
              "  @disciplinefileunkid " & _
              ", @trandate " & _
              ", @disciplinaryactionunkid " & _
              ", @action_start_date " & _
              ", @action_end_date " & _
              ", @disciplinestatusunkid " & _
              ", @resolutionstep " & _
              ", @remark " & _
              ", @isapproved " & _
              ", @approveduserunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @committeemasterunkid" & _
              ", @caseno " & _
              ", @courtname " & _
              ", @approvaldate " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintResolutionunkid = dsList.Tables(0).Rows(0).Item(0)

            If dtTable IsNot Nothing Then
                objInvestigator._ResolutionUnkid = mintResolutionunkid
                objInvestigator._DataTable = dtTable
                If objInvestigator.InsertUpdateDelete_Investigators(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            If (mintStatusunkid = 1 Or mintStatusunkid = 3) AndAlso mblnIsapproved = True Then
                Dim objDisciplineFile As New clsDiscipline_File
                objDisciplineFile._Disciplinefileunkid = mintDisciplinefileunkid
                objDisciplineFile._Disciplinestatusunkid = mintStatusunkid
                If mintStatusunkid = 3 AndAlso mblnIsExReOpen = True Then
                    objDisciplineFile._IsFromResolution = True
                    objDisciplineFile._IsExternal = True
                End If
                If objDisciplineFile.Update(True) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If mblnIsapproved = True AndAlso mintStatusunkid <> 3 Then
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Call IsEmployeeUpdated(mintInvolvedEmpId, dtTotal_Active_Employee_AsOnDate, blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, intUserUnkId)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call IsEmployeeUpdated(mintInvolvedEmpId, dtTotal_Active_Employee_AsOnDate, blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, intUserUnkId, objDataOperation)
                Call IsEmployeeUpdated(strDatabaseName, intCompanyId, intYearId, mintInvolvedEmpId, dtTotal_Active_Employee_AsOnDate, blnIsArutiDemo, _
                                       intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, intUserUnkId, objDataOperation, blnDonotAttendanceinSeconds, _
                                       strUserAccessMode, xPeriodStart, xPeriodEnd, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, _
                                       dtCurrentDateTime, mblnCreateADUserFromEmp, mblnUserMustChangePwdOnNextLogOn, blnAllowToChangeEOCLeavingDateOnClosedPeriod, blnApplyUserAccessFilter, strFilerString, xHostName, xIPAddr, xLoggedUserName, xLoginMod)
                'Sohail (09 Oct 2019) - [mblnCreateADUserFromEmp, mblnUserMustChangePwdOnNextLogOn, blnAllowToApproveEarningDeduction]
                'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{xHostName,xIPAddr,xLoggedUserName,xLoginMod})} -- END

                'Sohail (21 Aug 2015) -- End
                'S.SANDEEP [04 JUN 2015] -- END
            End If
            'S.SANDEEP [ 20 MARCH 2012 ] -- END



            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrdiscipline_resolutions) </purpose>
    Public Function Update(ByVal strDatabaseName As String, _
                           ByVal intYearId As Integer, _
                           ByVal intCompanyId As Integer, _
                           ByVal dtTotal_Active_Employee_AsOnDate As Date, _
                           ByVal blnIsArutiDemo As String, _
                           ByVal intTotal_Active_Employee_ForAllCompany As Integer, _
                           ByVal intNoOfEmployees As Integer, _
                           ByVal intUserUnkId As Integer, _
                           ByVal blnDonotAttendanceinSeconds As Boolean, _
                           ByVal strUserAccessMode As String, _
                           ByVal xPeriodStart As DateTime, _
                           ByVal xPeriodEnd As DateTime, _
                           ByVal xOnlyApproved As Boolean, _
                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                           ByVal blnAllowToApproveEarningDeduction As Boolean, _
                           ByVal dtCurrentDateTime As DateTime, _
                           ByVal mblnCreateADUserFromEmp As Boolean, _
                           ByVal mblnUserMustChangePwdOnNextLogOn As Boolean, _
                           ByVal blnAllowToChangeEOCLeavingDateOnClosedPeriod As Boolean, _
                           Optional ByVal dtTable As DataTable = Nothing, _
                           Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                           Optional ByVal strFilerString As String = "", _
                           Optional ByVal xHostName As String = "", _
                           Optional ByVal xIPAddr As String = "", _
                           Optional ByVal xLoggedUserName As String = "", _
                           Optional ByVal xLoginMod As enLogin_Mode = enLogin_Mode.DESKTOP) As Boolean
        'Sohail (09 Oct 2019) - [mblnCreateADUserFromEmp, mblnUserMustChangePwdOnNextLogOn, blnAllowToChangeEOCLeavingDateOnClosedPeriod]
        'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{xHostName,xIPAddr,xLoggedUserName,xLoginMod})} -- END

        'Sohail (21 Aug 2015) - [strDatabaseName]
        If isExist(mdtTrandate, mintDisciplinaryactionunkid, mstrResolutionstep, mintInvolvedEmpId, mintResolutionunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, you cannot add same resolution step on the same date for particular employee. Please define new resolution step.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.BindTransaction()

            objDataOperation.AddParameter("@resolutionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResolutionunkid.ToString)
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid.ToString)
            objDataOperation.AddParameter("@trandate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTrandate)
            objDataOperation.AddParameter("@disciplinaryactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinaryactionunkid.ToString)
            objDataOperation.AddParameter("@action_start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAction_Start_Date)
            If mdtAction_End_Date <> Nothing Then
                objDataOperation.AddParameter("@action_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAction_End_Date)
            Else
                objDataOperation.AddParameter("@action_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@disciplinestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@resolutionstep", SqlDbType.VarChar, 8000, mstrResolutionstep.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.VarChar, 8000, mstrRemark.ToString)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@approveduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveduserunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            objDataOperation.AddParameter("@committeemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCommitteemasterunkid.ToString)
            'S.SANDEEP [ 20 MARCH 2012 ] -- END

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@caseno", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrCaseno.ToString)
            objDataOperation.AddParameter("@courtname", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrCourtname.ToString)
            If mdtApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            Else
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP [ 20 APRIL 2012 ] -- END

            strQ = "UPDATE hrdiscipline_resolutions SET " & _
              "  disciplinefileunkid = @disciplinefileunkid" & _
              ", trandate = @trandate" & _
              ", disciplinaryactionunkid = @disciplinaryactionunkid" & _
              ", action_start_date = @action_start_date" & _
              ", action_end_date = @action_end_date" & _
              ", disciplinestatusunkid = @disciplinestatusunkid" & _
              ", resolutionstep = @resolutionstep" & _
              ", remark = @remark" & _
              ", isapproved = @isapproved" & _
              ", approveduserunkid = @approveduserunkid" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
                   ", committeemasterunkid = @committeemasterunkid " & _
                   ", caseno = @caseno " & _
                   ", courtname = @courtname " & _
                   ", approvaldate = @approvaldate " & _
            "WHERE resolutionunkid = @resolutionunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dtTable IsNot Nothing Then
                objInvestigator._ResolutionUnkid = mintResolutionunkid
                objInvestigator._DataTable = dtTable
                If objInvestigator.InsertUpdateDelete_Investigators(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            If (mintStatusunkid = 1 Or mintStatusunkid = 3) AndAlso mblnIsapproved = True Then
                Dim objDisciplineFile As New clsDiscipline_File
                objDisciplineFile._Disciplinefileunkid = mintDisciplinefileunkid
                objDisciplineFile._Disciplinestatusunkid = mintStatusunkid

                If mintStatusunkid = 3 AndAlso mblnIsExReOpen = True Then
                    objDisciplineFile._IsFromResolution = True
                    objDisciplineFile._IsExternal = True
                End If

                If objDisciplineFile.Update(True) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If mblnIsapproved = True AndAlso mintStatusunkid <> 3 Then
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Call IsEmployeeUpdated(mintInvolvedEmpId, dtTotal_Active_Employee_AsOnDate, blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, intUserUnkId)
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'Call IsEmployeeUpdated(mintInvolvedEmpId, dtTotal_Active_Employee_AsOnDate, blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, intUserUnkId, objDataOperation)
                Call IsEmployeeUpdated(strDatabaseName, intCompanyId, intYearId, mintInvolvedEmpId, dtTotal_Active_Employee_AsOnDate, blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, intUserUnkId, objDataOperation, blnDonotAttendanceinSeconds, _
                                       strUserAccessMode, xPeriodStart, xPeriodEnd, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, _
                                       dtCurrentDateTime, mblnCreateADUserFromEmp, mblnUserMustChangePwdOnNextLogOn, blnAllowToChangeEOCLeavingDateOnClosedPeriod, blnApplyUserAccessFilter, strFilerString, xHostName, xIPAddr, xLoggedUserName, xLoginMod)
                'Sohail (09 Oct 2019) - [mblnCreateADUserFromEmp, mblnUserMustChangePwdOnNextLogOn, blnAllowToApproveEarningDeduction]
                'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{xHostName,xIPAddr,xLoggedUserName,xLoginMod})} -- END
                'Sohail (21 Aug 2015) -- End
                'S.SANDEEP [04 JUN 2015] -- END
            End If
            'S.SANDEEP [ 20 MARCH 2012 ] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrdiscipline_resolutions) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.BindTransaction()

            strQ = "UPDATE hrdiscipline_resolutions SET " & _
                    "  isvoid = 1 " & _
                    ", voiduserunkid = @voiduserunkid " & _
                    ", voiddatetime = @voiddatetime " & _
                    ", voidreason = @voidreason " & _
                   "WHERE resolutionunkid = @resolutionunkid; "

            strQ &= "UPDATE hrdiscipline_investigators SET " & _
                    "  isvoid = 1 " & _
                    ", voiduserunkid = @voiduserunkid " & _
                    ", voiddatetime = @voiddatetime " & _
                    ", voidreason = @voidreason " & _
                   "WHERE resolutionunkid = @resolutionunkid "

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@resolutionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList = clsCommonATLog.GetChildList(objDataOperation, "hrdiscipline_investigators", "resolutionunkid", intUnkid)

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdiscipline_investigators", "resolutionunkid", intUnkid, "hrdiscipline_investigators", "investigatorunkid", dtRow.Item("investigatorunkid"), 3, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            Else
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdiscipline_investigators", "resolutionunkid", intUnkid, "hrdiscipline_investigators", "investigatorunkid", -1, 3, 0) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal dtTrandate As DateTime, ByVal intDisciplinaryactionunkid As Integer, ByVal strResolutionstep As String, ByVal intInvolvedEmpId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  hrdiscipline_resolutions.resolutionunkid " & _
              ", hrdiscipline_resolutions.disciplinefileunkid " & _
              ", hrdiscipline_resolutions.trandate " & _
              ", hrdiscipline_resolutions.disciplinaryactionunkid " & _
              ", hrdiscipline_resolutions.action_start_date " & _
              ", hrdiscipline_resolutions.action_end_date " & _
              ", hrdiscipline_resolutions.disciplinestatusunkid " & _
              ", hrdiscipline_resolutions.resolutionstep " & _
              ", hrdiscipline_resolutions.remark " & _
              ", hrdiscipline_resolutions.isapproved " & _
              ", hrdiscipline_resolutions.approveduserunkid " & _
              ", hrdiscipline_resolutions.userunkid " & _
              ", hrdiscipline_resolutions.isvoid " & _
              ", hrdiscipline_resolutions.voiduserunkid " & _
              ", hrdiscipline_resolutions.voiddatetime " & _
              ", hrdiscipline_resolutions.voidreason " & _
             "FROM hrdiscipline_resolutions " & _
             " JOIN hrdiscipline_file ON hrdiscipline_resolutions.disciplinefileunkid = hrdiscipline_file.disciplinefileunkid " & _
             "WHERE hrdiscipline_resolutions.disciplinaryactionunkid = @disciplinaryactionunkid " & _
             "AND hrdiscipline_resolutions.resolutionstep = @resolutionstep " & _
             "AND hrdiscipline_resolutions.trandate = @trandate AND hrdiscipline_resolutions.isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND resolutionunkid <> @resolutionunkid"
                objDataOperation.AddParameter("@resolutionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            If intInvolvedEmpId > 0 Then
                strQ &= " AND hrdiscipline_file.involved_employeeunkid = @InvolvedEmpId"
                objDataOperation.AddParameter("@InvolvedEmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intInvolvedEmpId)
            End If

            objDataOperation.AddParameter("@disciplinaryactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDisciplinaryactionunkid)
            objDataOperation.AddParameter("@resolutionstep", SqlDbType.VarChar, 8000, strResolutionstep)
            objDataOperation.AddParameter("@trandate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtTrandate)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Is_LastResolutionSelected(ByVal intDisciplinefileId As Integer) As Integer
        Dim intLastResolutionId As Integer = -1
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            dsList = objDataOperation.ExecQuery("SELECT TOP 1 resolutionunkid FROM hrdiscipline_resolutions WHERE disciplinefileunkid = '" & intDisciplinefileId & "' AND isapproved = 0 ORDER BY resolutionunkid ASC", "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intLastResolutionId = dsList.Tables(0).Rows(0)("resolutionunkid")
            End If

            Return intLastResolutionId

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Is_LastResolutionSelected; Module Name: " & mstrModuleName)
        Finally
            dsList.Dispose() : exForce = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function ApproveResolutionStep(ByVal strDatabaseName As String, _
                                          ByVal intYearId As Integer, _
                                          ByVal intCompanyId As Integer, _
                                          ByVal dtTotal_Active_Employee_AsOnDate As Date, _
                                          ByVal blnIsArutiDemo As String, _
                                          ByVal intTotal_Active_Employee_ForAllCompany As Integer, _
                                          ByVal intNoOfEmployees As Integer, _
                                          ByVal intUserUnkId As Integer, _
                                          ByVal intResolutionId As Integer, _
                                          ByVal intDisciplineFileId As Integer, _
                                          ByVal intInvolvedEmpId As Integer, _
                                          ByVal dtApprovalDate As Date, _
                                          ByVal blnDonotAttendanceinSeconds As Boolean, _
                                          ByVal strUserAccessMode As String, _
                                          ByVal xPeriodStart As DateTime, _
                                          ByVal xPeriodEnd As DateTime, _
                                          ByVal xOnlyApproved As Boolean, _
                                          ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                          ByVal blnAllowToApproveEarningDeduction As Boolean, _
                                          ByVal dtCurrentDateTime As DateTime, _
                                          ByVal mblnCreateADUserFromEmp As Boolean, _
                                          ByVal mblnUserMustChangePwdOnNextLogOn As Boolean, _
                                          ByVal blnAllowToChangeEOCLeavingDateOnClosedPeriod As Boolean, _
                                          Optional ByVal intStatusId As Integer = -1, _
                                          Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                                          Optional ByVal strFilerString As String = "", _
                                          Optional ByVal xHostName As String = "", _
                                          Optional ByVal xIPAddr As String = "", _
                                          Optional ByVal xLoggedUserName As String = "", _
                                          Optional ByVal xLoginMod As enLogin_Mode = enLogin_Mode.DESKTOP) As Boolean
        'Sohail (09 Oct 2019) - [mblnCreateADUserFromEmp, mblnUserMustChangePwdOnNextLogOn, blnAllowToChangeEOCLeavingDateOnClosedPeriod]
        'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{xHostName,xIPAddr,xLoggedUserName,xLoginMod})} -- END
        'Sohail (21 Aug 2015) - [strDatabaseName]
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            '**************** UPDATING RESOLUTION STEPS ****************' START
            StrQ = "UPDATE hrdiscipline_resolutions " & _
                   "SET isapproved = 1 " & _
                   "   ,approveduserunkid = '" & User._Object._Userunkid & "' " & _
                   "   ,approvaldate = @Date " & _
                   "WHERE resolutionunkid = '" & intResolutionId & "'"

            objDataOperation.AddParameter("@Date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtApprovalDate)
            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            '**************** UPDATING RESOLUTION STEPS ****************' END

            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            ''**************** UPDATING DISCIPLINE FILE ****************' START
            'StrQ = "UPDATE hrdiscipline_file " & _
            '       "SET isapproved = 1 " & _
            '       "WHERE disciplinefileunkid = '" & intDisciplineFileId & "'"

            'objDataOperation.ExecNonQuery(StrQ)

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            ''**************** UPDATING DISCIPLINE FILE ****************' END

            '**************** UPDATING DISCIPLINE FILE ****************' START
            If (intStatusId = 1 Or intStatusId = 3) Then
                If (intStatusId = 1 Or intStatusId = 3) Then
                    Dim objDisciplineFile As New clsDiscipline_File
                    objDisciplineFile._Disciplinefileunkid = intDisciplineFileId
                    objDisciplineFile._Disciplinestatusunkid = intStatusId

                    If intStatusId = 3 AndAlso mblnIsExReOpen = True Then
                        objDisciplineFile._IsFromResolution = True
                        objDisciplineFile._IsExternal = True
                    End If

                    If objDisciplineFile.Update(True) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            '**************** UPDATING DISCIPLINE FILE ****************' END
            'S.SANDEEP [ 20 MARCH 2012 ] -- END



            '**************** UPDATING EMPLOYEE MASTER ****************' START

            _Resolutionunkid = intResolutionId

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If RevertAction(intDisciplineFileId, intInvolvedEmpId) = False Then

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If RevertAction(strDatabaseName, intDisciplineFileId, intInvolvedEmpId) = False Then
            If RevertAction(strDatabaseName, intYearId, intCompanyId, intDisciplineFileId, intInvolvedEmpId, dtTotal_Active_Employee_AsOnDate, _
                            blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, blnDonotAttendanceinSeconds, strUserAccessMode, _
                            xPeriodStart, xPeriodEnd, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, _
                            dtCurrentDateTime, blnApplyUserAccessFilter, strFilerString, xHostName, xIPAddr, xLoggedUserName, xLoginMod) = False Then
                'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{xHostName,xIPAddr,xLoggedUserName,xLoginMod})} -- END

                'Shani(24-Aug-2015) -- End

                'Sohail (21 Aug 2015) -- End
                exForce = New Exception(mstrMessage)
                Throw exForce
            End If

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If IsEmployeeUpdated(intInvolvedEmpId) = False Then
            '    exForce = New Exception(mstrMessage)
            '    Throw exForce
            'End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If IsEmployeeUpdated(intInvolvedEmpId, dtTotal_Active_Employee_AsOnDate, blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, intUserUnkId, objDataOperation) = False Then
            If IsEmployeeUpdated(strDatabaseName, intCompanyId, intYearId, intInvolvedEmpId, dtTotal_Active_Employee_AsOnDate, blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, intUserUnkId, objDataOperation, blnDonotAttendanceinSeconds, _
                                 strUserAccessMode, xPeriodStart, xPeriodEnd, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, _
                                 dtCurrentDateTime, mblnCreateADUserFromEmp, mblnUserMustChangePwdOnNextLogOn, blnAllowToChangeEOCLeavingDateOnClosedPeriod, blnApplyUserAccessFilter, strFilerString, xHostName, xIPAddr, xLoggedUserName, xLoginMod) = False Then
                'Sohail (09 Oct 2019) - [mblnCreateADUserFromEmp, mblnUserMustChangePwdOnNextLogOn, blnAllowToApproveEarningDeduction]
                'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{xHostName,xIPAddr,xLoggedUserName,xLoginMod})} -- END
                'Sohail (21 Aug 2015) -- End
                exForce = New Exception(mstrMessage)
                Throw exForce
            End If
            'S.SANDEEP [04 JUN 2015] -- END


            '**************** UPDATING EMPLOYEE MASTER ****************' END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: ApproveResolutionStep; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function IsEmployeeUpdated(ByVal strDatabaseName As String, _
                                       ByVal intCompanyId As Integer, _
                                       ByVal intYearId As Integer, _
                                       ByVal intInvolved_Personunkid As Integer, _
                                       ByVal dtTotal_Active_Employee_AsOnDate As Date, _
                                       ByVal blnIsArutiDemo As String, _
                                       ByVal intTotal_Active_Employee_ForAllCompany As Integer, _
                                       ByVal intNoOfEmployees As Integer, _
                                       ByVal intUserUnkId As Integer, _
                                       ByVal xDataOperation As clsDataOperation, _
                                       ByVal blnDonotAttendanceinSeconds As Boolean, _
                                       ByVal strUserAccessMode As String, _
                                       ByVal xPeriodStart As DateTime, _
                                       ByVal xPeriodEnd As DateTime, _
                                       ByVal xOnlyApproved As Boolean, _
                                       ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                       ByVal blnAllowToApproveEarningDeduction As Boolean, _
                                       ByVal dtCurrentDateTime As DateTime, _
                                       ByVal mblnCreateADUserFromEmp As Boolean, _
                                       ByVal mblnUserMustChangePwdOnNextLogOn As Boolean, _
                                       ByVal blnAllowToChangeEOCLeavingDateOnClosedPeriod As Boolean, _
                                       Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                                       Optional ByVal strFilerString As String = "", _
                                       Optional ByVal xHostName As String = "", _
                                       Optional ByVal xIPAddr As String = "", _
                                       Optional ByVal xLoggedUserName As String = "", _
                                       Optional ByVal xLoginMod As enLogin_Mode = enLogin_Mode.DESKTOP) As Boolean
        'Sohail (09 Oct 2019) - [blnAllowToChangeEOCLeavingDateOnClosedPeriod]


        'Pinkal (18-Aug-2018) --         'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[ ByVal mblnCreateADUserFromEmp As Boolean]


        'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{xHostName,xIPAddr,xLoggedUserName,xLoginMod})} -- END

        'S.SANDEEP [04 JUN 2015] -- START {blnDonotAttendanceinSeconds} -- END
        'Sohail (21 Aug 2015) - [strDatabaseName]

        Try
            mstrMessage = ""
            Dim objEmp As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = intInvolved_Personunkid
            objEmp._Employeeunkid(xPeriodEnd) = intInvolved_Personunkid
            'S.SANDEEP [04 JUN 2015] -- END

            Select Case _Disciplinaryactionunkid
                Case 1 'SUSPENSION
                    objEmp._Suspende_From_Date = _Action_Start_Date
                    objEmp._Suspende_To_Date = _Action_End_Date
                Case 2 'TERMINATED
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objEmp.IsEmpTerminated(mdtAction_Start_Date, intInvolved_Personunkid) = True Then

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Sohail (09 Oct 2019) -- Start
                    'NMB Enhancement # : allow to change eoc / retirement date in current open or last closed period even after salary is paid or period is closed.
                    'If objEmp.IsEmpTerminated(mdtAction_Start_Date, intInvolved_Personunkid, strDatabaseName, intYearId) = True Then
                    If objEmp.IsEmpTerminated(mdtAction_Start_Date, intInvolved_Personunkid, strDatabaseName, intYearId, blnSkipForLastClosedPeriod:=True) = True Then
                        'Sohail (09 Oct 2019) -- End
                        'If objEmp.IsEmpTerminated(mdtAction_Start_Date, intInvolved_Personunkid, strDatabaseName) = True Then
                        'S.SANDEEP [04 JUN 2015] -- END

                        'Sohail (21 Aug 2015) -- End
                        objEmp._Termination_From_Date = _Action_Start_Date
                    Else
                        mstrMessage = objEmp._Message
                        Return False
                    End If
            End Select

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objEmp.Update() = False Then
            '    Return False
            'End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objEmp.Update(dtTotal_Active_Employee_AsOnDate, _
            '                 blnIsArutiDemo, _
            '                 intTotal_Active_Employee_ForAllCompany, _
            '                 intNoOfEmployees, _
            '                 intUserUnkId, , , , , , , , , , , xDataOperation) = False Then
            'S.SANDEEP [08 DEC 2016] -- START
            'ENHANCEMENT : Issue for Wrong User Name Sent in Email (Edit By User : <UserName>)
            'If objEmp.Update(strDatabaseName, intYearId, intCompanyId, dtTotal_Active_Employee_AsOnDate, _
            '                 blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, _
            '                 intUserUnkId, blnDonotAttendanceinSeconds, strUserAccessMode, xPeriodStart, xPeriodEnd, xOnlyApproved, _
            '                 xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateTime, , , , , , , , , xDataOperation, blnApplyUserAccessFilter, strFilerString) = False Then


            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

            'If objEmp.Update(strDatabaseName, intYearId, intCompanyId, dtTotal_Active_Employee_AsOnDate, _
            '                 blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, _
            '                 intUserUnkId, blnDonotAttendanceinSeconds, strUserAccessMode, xPeriodStart, xPeriodEnd, xOnlyApproved, _
            '             xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateTime, , , , , , , , , xDataOperation, blnApplyUserAccessFilter, strFilerString, , xHostName, xIPAddr, xLoggedUserName, xLoginMod) = False Then

            If objEmp.Update(strDatabaseName, intYearId, intCompanyId, dtTotal_Active_Employee_AsOnDate, _
                             blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, _
                             intUserUnkId, blnDonotAttendanceinSeconds, strUserAccessMode, xPeriodStart, xPeriodEnd, xOnlyApproved, _
                           xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateTime, mblnCreateADUserFromEmp, mblnUserMustChangePwdOnNextLogOn, , , , , , , , , xDataOperation, blnApplyUserAccessFilter, strFilerString, , xHostName, xIPAddr, xLoggedUserName, xLoginMod) = False Then

                'Pinkal (18-Aug-2018) -- End

                'S.SANDEEP [08 DEC 2016] -- END

                'Sohail (21 Aug 2015) -- End
                Return False
            End If

            'S.SANDEEP [04 JUN 2015] -- END


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsEmployeeUpdated; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP [ 20 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
    Public Function IsCase_Can_Close(ByVal intDisciplineFileId As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim intRetrurnValue As String = -1
        Dim intTotalCnt As Integer = -1
        Dim objDataOpr As New clsDataOperation
        Try
            StrQ = "SELECT * FROM hrdiscipline_resolutions WHERE disciplinefileunkid = '" & intDisciplineFileId & "' AND isvoid = 0 "

            intTotalCnt = objDataOpr.RecordCount(StrQ)

            If objDataOpr.ErrorMessage <> "" Then
                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
            End If

            If intTotalCnt > 0 Then

                StrQ = "SELECT * FROM hrdiscipline_resolutions WHERE disciplinefileunkid = '" & intDisciplineFileId & "' AND isapproved = 0 AND isvoid = 0 "

                intTotalCnt = -1
                intTotalCnt = objDataOpr.RecordCount(StrQ)

                If objDataOpr.ErrorMessage <> "" Then
                    Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                End If

                If intTotalCnt > 0 Then
                    intRetrurnValue = 2
                Else
                    intRetrurnValue = 3
                End If
            Else
                intRetrurnValue = 1
            End If

            Return intRetrurnValue

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsCase_Can_Close; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'Public Function IfCaseClosed(ByVal intDisciplineFileId As Integer) As Boolean
    '    Dim StrQ As String = String.Empty
    '    Dim blnFlag As Boolean = False
    '    Dim objDataOpr As New clsDataOperation
    '    Dim iTotalCnt As Integer = -1
    '    Dim iUnApprovedCnt As Integer = -1
    '    Try
    '        StrQ = "SELECT * FROM hrdiscipline_resolutions WHERE disciplinefileunkid = '" & intDisciplineFileId & "' "

    '        iTotalCnt = objDataOpr.RecordCount(StrQ)

    '        If objDataOpr.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
    '        End If

    '        If iTotalCnt > 0 Then

    '            StrQ = "SELECT * FROM hrdiscipline_resolutions WHERE disciplinefileunkid = '" & intDisciplineFileId & "' AND isapproved = 0 "

    '            iUnApprovedCnt = objDataOpr.RecordCount(StrQ)

    '            If objDataOpr.ErrorMessage <> "" Then
    '                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
    '            End If

    '            If (iTotalCnt = iUnApprovedCnt) Or iUnApprovedCnt <= 1 Then
    '                blnFlag = True
    '            End If
    '        Else
    '            blnFlag = True
    '        End If

    '        Return blnFlag
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: IfCaseClosed; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Function
    'Public Function IsClosedStatusAdded(ByVal intDisciplineUnkid As Integer) As Boolean
    '    Dim StrQ As String = String.Empty
    '    Dim blnFlag As Boolean = False
    '    Dim objDataOpr As New clsDataOperation
    '    Dim iTotalCnt As Integer = -1
    '    Try
    '        StrQ = "SELECT * FROM hrdiscipline_resolutions WHERE disciplinefileunkid = '" & intDisciplineUnkid & "' AND disciplinestatusunkid = 1 "

    '        iTotalCnt = objDataOpr.RecordCount(StrQ)

    '        If objDataOpr.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
    '        End If

    '        If iTotalCnt > 0 Then
    '            blnFlag = True
    '        End If

    '        Return blnFlag

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: IsClosedStausAdded; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Function

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function RevertAction(ByVal strDatabaseName As String, ByVal intFileUnkid As Integer, ByVal intPersonId As Integer) As Boolean
    Public Function RevertAction(ByVal strDatabaseName As String, _
                                 ByVal intYearId As Integer, _
                                 ByVal intCompanyId As Integer, _
                                 ByVal intFileUnkid As Integer, _
                                 ByVal intPersonId As Integer, _
                                 ByVal dtTotal_Active_Employee_AsOnDate As Date, _
                                 ByVal blnIsArutiDemo As String, _
                                 ByVal intTotal_Active_Employee_ForAllCompany As Integer, _
                                 ByVal intNoOfEmployees As Integer, ByVal blnDonotAttendanceinSeconds As Boolean, ByVal strUserAccessMode As String, _
                                 ByVal xPeriodStart As DateTime, _
                                 ByVal xPeriodEnd As DateTime, _
                                 ByVal xOnlyApproved As Boolean, _
                                 ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                 ByVal blnAllowToApproveEarningDeduction As Boolean, _
                                 ByVal dtCurrentDateTime As DateTime, _
                                 ByVal mblnCreateADUserFromEmp As Boolean, _
                                 ByVal mblnUserMustChangePwdOnNextLogOn As Boolean, _
                                 Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                                 Optional ByVal strFilerString As String = "", _
                                 Optional ByVal xHostName As String = "", _
                                 Optional ByVal xIPAddr As String = "", _
                                 Optional ByVal xLoggedUserName As String = "", _
                                 Optional ByVal xLoginMod As enLogin_Mode = enLogin_Mode.DESKTOP) As Boolean

        'Pinkal (18-Aug-2018) -- 'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273]. [ByVal mblnCreateADUserFromEmp As Boolean]

        'S.SANDEEP [08 DEC 2016] -- START {Issue for Wrong User Name Sent in Email (Edit By User : <UserName>{xHostName,xIPAddr,xLoggedUserName,xLoginMod})} -- END
        'Shani(24-Aug-2015) -- End

        'Sohail (21 Aug 2015) - [strDatabaseName]
        Dim StrQ As String = String.Empty
        Dim blnFlag As Boolean = False
        Dim objDataOpr As New clsDataOperation
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT TOP 1 disciplinaryactionunkid,disciplinefileunkid,resolutionunkid FROM hrdiscipline_resolutions WHERE isapproved = 1 AND disciplinefileunkid = '" & intFileUnkid & "' AND isvoid = 0 ORDER BY resolutionunkid DESC "

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Dim objEmp As New clsEmployee_Master

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmp._Employeeunkid = intPersonId
                objEmp._Employeeunkid(xPeriodEnd) = intPersonId
                'S.SANDEEP [04 JUN 2015] -- END

                Select Case CInt(dsList.Tables("List").Rows(0)("disciplinaryactionunkid"))
                    Case 1
                        objEmp._Suspende_From_Date = Nothing
                        objEmp._Suspende_To_Date = Nothing
                    Case 2
                        objEmp._Termination_From_Date = Nothing
                End Select
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objEmp.Update() = False Then
                '    mstrMessage = objEmp._Message
                '    Return False
                'End If
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objEmp.Update(ConfigParameter._Object._CurrentDateAndTime.Date, _
                '                           ConfigParameter._Object._IsArutiDemo, _
                '                           Company._Object._Total_Active_Employee_ForAllCompany, _
                '                           ConfigParameter._Object._NoOfEmployees, _
                '                           mintUserunkid) = False Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objEmp.Update(strDatabaseName, ConfigParameter._Object._CurrentDateAndTime.Date, _
                '                           ConfigParameter._Object._IsArutiDemo, _
                '                           Company._Object._Total_Active_Employee_ForAllCompany, _
                '                           ConfigParameter._Object._NoOfEmployees, _
                '                           mintUserunkid) = False Then
                'S.SANDEEP [08 DEC 2016] -- START
                'ENHANCEMENT : Issue for Wrong User Name Sent in Email (Edit By User : <UserName>)
                'If objEmp.Update(strDatabaseName, intYearId, intCompanyId, _
                '                 dtTotal_Active_Employee_AsOnDate, _
                '                 blnIsArutiDemo, _
                '                 intTotal_Active_Employee_ForAllCompany, _
                '                 intNoOfEmployees, _
                '                 mintUserunkid, blnDonotAttendanceinSeconds, strUserAccessMode, _
                '                 xPeriodStart, xPeriodEnd, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateTime, , , , , , , , , objDataOpr, _
                '                 blnApplyUserAccessFilter, strFilerString) = False Then


                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

                'If objEmp.Update(strDatabaseName, intYearId, intCompanyId, _
                '                 dtTotal_Active_Employee_AsOnDate, _
                '                 blnIsArutiDemo, _
                '                 intTotal_Active_Employee_ForAllCompany, _
                '                 intNoOfEmployees, _
                '                 mintUserunkid, blnDonotAttendanceinSeconds, strUserAccessMode, _
                '                 xPeriodStart, xPeriodEnd, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateTime, , , , , , , , , objDataOpr, _
                '             blnApplyUserAccessFilter, strFilerString, , xHostName, xIPAddr, xLoggedUserName, xLoginMod) = False Then

                If objEmp.Update(strDatabaseName, intYearId, intCompanyId, _
                                 dtTotal_Active_Employee_AsOnDate, _
                                 blnIsArutiDemo, _
                                 intTotal_Active_Employee_ForAllCompany, _
                                 intNoOfEmployees, _
                                 mintUserunkid, blnDonotAttendanceinSeconds, strUserAccessMode, _
                                xPeriodStart, xPeriodEnd, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateTime, mblnCreateADUserFromEmp, mblnUserMustChangePwdOnNextLogOn, , , , , , , , , objDataOpr, _
                             blnApplyUserAccessFilter, strFilerString, , xHostName, xIPAddr, xLoggedUserName, xLoginMod) = False Then

                    'Pinkal (18-Aug-2018) -- End

                    'S.SANDEEP [08 DEC 2016] -- END


                    'Shani(24-Aug-2015) -- End

                    'Sohail (21 Aug 2015) -- End
                    mstrMessage = objEmp._Message
                    Return False
                End If
                'S.SANDEEP [04 JUN 2015] -- END
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: RevertAction; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Is_Add_Step_Allowed(ByVal intDisciplineId As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim intRetValue As Integer = -1
        Dim objDataOpr As New clsDataOperation
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT TOP 1 " & _
                   " resolutionunkid " & _
                   ",disciplinestatusunkid " & _
                   ",isapproved " & _
                   "FROM hrdiscipline_resolutions " & _
                   "WHERE disciplinefileunkid = '" & intDisciplineId & "' AND isvoid = 0 " & _
                   " ORDER BY resolutionunkid DESC "

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Select Case CInt(dsList.Tables("List").Rows(0)("disciplinestatusunkid"))
                    Case 1  'CLOSED
                        If CBool(dsList.Tables("List").Rows(0)("isapproved")) = True Then
                            intRetValue = 2
                        Else
                            intRetValue = 4
                        End If
                    Case 3  'RE-OPENED
                        If CBool(dsList.Tables("List").Rows(0)("isapproved")) = True Then
                            intRetValue = 3
                        Else
                            intRetValue = 5
                        End If
                End Select
            Else
                intRetValue = 1
            End If

            Return intRetValue

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Add_Step_Allowed", mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 20 MARCH 2012 ] -- END

    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Sub Is_ExternallyReopen(ByVal intDisciplineFileId As Integer)
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Try
            StrQ = "SELECT " & _
                   "     resolutionunkid " & _
                   "    ,disciplinefileunkid " & _
                   "    ,disciplinestatusunkid " & _
                   "    ,caseno " & _
                   "    ,courtname " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         resolutionunkid " & _
                   "        ,disciplinefileunkid " & _
                   "        ,disciplinestatusunkid " & _
                   "        ,caseno " & _
                   "        ,courtname " & _
                   "        ,ROW_NUMBER() OVER(PARTITION BY disciplinefileunkid ORDER BY resolutionunkid DESC) AS Nos " & _
                   "    FROM hrdiscipline_resolutions " & _
                   "    WHERE isvoid = 0 " & _
                   ")AS A " & _
                   "WHERE A.Nos = 1 AND disciplinefileunkid = '" & intDisciplineFileId & "' "

            dsList = objDataOperation.ExecQuery(StrQ, "List")


            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mstrCaseno = dsList.Tables(0).Rows(0)("caseno").ToString
                mstrCourtname = dsList.Tables(0).Rows(0)("courtname").ToString
            Else
                mstrCaseno = "" : mstrCourtname = ""
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_ExternallyReopen", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function GetMinClosingDate(ByVal intDisciplineUnkid) As Date
        Dim dtDate As Date = Nothing
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Try
            StrQ = "SELECT TOP 1 " & _
                   " resolutionunkid " & _
                   ",CONVERT(CHAR(8),approvaldate,112) AS ClosingDate " & _
                   "FROM hrdiscipline_resolutions " & _
                   "WHERE disciplinefileunkid = '" & intDisciplineUnkid & "' " & _
                   " AND isapproved = 1 " & _
                   " AND isvoid = 0 " & _
                   "ORDER BY resolutionunkid DESC "

            Dim dsList As New DataSet

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                dtDate = eZeeDate.convertDate(dsList.Tables(0).Rows(0)("ClosingDate").ToString).ToShortDateString
            End If

            Return dtDate

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetMinClosingDate", mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 20 APRIL 2012 ] -- END


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Approved")
            Language.setMessage(mstrModuleName, 2, "Pending")
            Language.setMessage(mstrModuleName, 3, "Sorry, you cannot add same resolution step on the same date for particular employee. Please define new resolution step.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
