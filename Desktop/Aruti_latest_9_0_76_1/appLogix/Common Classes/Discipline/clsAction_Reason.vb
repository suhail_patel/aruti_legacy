﻿'************************************************************************************************************************************
'Class Name : clsAction_Reason.vb
'Purpose    :
'Date       :29/06/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsAction_Reason
    Private Shared ReadOnly mstrModuleName As String = "clsAction_Reason"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintActionreasonunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrReason_Action As String = String.Empty
    Private mblnIsreason As Boolean
    Private mblnIsactive As Boolean = True
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actionreasonunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Actionreasonunkid() As Integer
        Get
            Return mintActionreasonunkid
        End Get
        Set(ByVal value As Integer)
            mintActionreasonunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reason_action
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reason_Action() As String
        Get
            Return mstrReason_Action
        End Get
        Set(ByVal value As String)
            mstrReason_Action = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isreason() As Boolean
        Get
            Return mblnIsreason
        End Get
        Set(ByVal value As Boolean)
            mblnIsreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  actionreasonunkid " & _
              ", code " & _
              ", reason_action " & _
              ", isreason " & _
              ", isactive " & _
             "FROM hraction_reason_master " & _
             "WHERE actionreasonunkid = @actionreasonunkid "

            objDataOperation.AddParameter("@actionreasonunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintActionreasonUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintactionreasonunkid = CInt(dtRow.Item("actionreasonunkid"))
                mstrcode = dtRow.Item("code").ToString
                mstrreason_action = dtRow.Item("reason_action").ToString
                mblnisreason = CBool(dtRow.Item("isreason"))
                mblnisactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal blnOnlyAction As Boolean, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                        "  actionreasonunkid " & _
                        ", code " & _
                        ", reason_action " & _
                        ", isreason " & _
                        ", isactive " & _
                    "FROM hraction_reason_master "

            If blnOnlyAction Then
                strQ &= "WHERE isreason = 0 "
            Else
                strQ &= "WHERE isreason = 1 "
            End If


            If blnOnlyActive Then
                strQ &= " And isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hraction_reason_master) </purpose>
    Public Function Insert() As Boolean

        'S.SANDEEP [ 12 MAY 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(mstrCode) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
        '    Return False
        'End If

        'If isExist(, mstrReason_Action) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Action/Reason is already defined. Please define new Action/Reason.")
        '    Return False
        'End If

        If isExist(mstrCode, , , mblnIsreason) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
            Return False
        End If

        If isExist(, mstrReason_Action, , mblnIsreason) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Action/Reason is already defined. Please define new Action/Reason.")
            Return False
        End If
        'S.SANDEEP [ 12 MAY 2012 ] -- END

        

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@reason_action", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrreason_action.ToString)
            objDataOperation.AddParameter("@isreason", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisreason.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

            strQ = "INSERT INTO hraction_reason_master ( " & _
                            "  code " & _
                            ", reason_action " & _
                            ", isreason " & _
                            ", isactive" & _
                        ") VALUES (" & _
                            "  @code " & _
                            ", @reason_action " & _
                            ", @isreason " & _
                            ", @isactive" & _
                        "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintActionreasonunkid = dsList.Tables(0).Rows(0).Item(0)


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hraction_reason_master", "actionreasonunkid", mintActionreasonunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hraction_reason_master) </purpose>
    Public Function Update() As Boolean

        'S.SANDEEP [ 12 MAY 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(mstrCode, , mintActionreasonunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
        '    Return False
        'End If

        'If isExist(, mstrReason_Action, mintActionreasonunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Action/Reason is already defined. Please define new Action/Reason.")
        '    Return False
        'End If

        If isExist(mstrCode, , mintActionreasonunkid, mblnIsreason) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
            Return False
        End If

        If isExist(, mstrReason_Action, mintActionreasonunkid, mblnIsreason) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Action/Reason is already defined. Please define new Action/Reason.")
            Return False
        End If
        'S.SANDEEP [ 12 MAY 2012 ] -- END

        

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@actionreasonunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintactionreasonunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@reason_action", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrreason_action.ToString)
            objDataOperation.AddParameter("@isreason", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisreason.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

            StrQ = "UPDATE hraction_reason_master SET " & _
              "  code = @code" & _
              ", reason_action = @reason_action" & _
              ", isreason = @isreason" & _
              ", isactive = @isactive " & _
            "WHERE actionreasonunkid = @actionreasonunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hraction_reason_master", mintActionreasonunkid, "actionreasonunkid", 2) Then

                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hraction_reason_master", "actionreasonunkid", mintActionreasonunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hraction_reason_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try

            'Anjan (31 Dec 2010)-Start
            'strQ = "DELETE FROM hraction_reason_master " & _
            '"WHERE actionreasonunkid = @actionreasonunkid "

            strQ = " UPDATE hraction_reason_master " & _
                       " SET isactive = 0 " & _
                    "WHERE actionreasonunkid = @actionreasonunkid "
            'Anjan (31 Dec 2010)-End

            objDataOperation.AddParameter("@actionreasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hraction_reason_master", "actionreasonunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT " & _
                    "TABLE_NAME AS TableName  " & _
                    ",COLUMN_NAME " & _
                    "FROM INFORMATION_SCHEMA.COLUMNS " & _
                    "WHERE COLUMN_NAME IN ('actionreasonunkid','disciplinaryactionunkid')"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            strQ = ""
            objDataOperation.AddParameter("@actionreasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("TableName") = "hraction_reason_master" Then Continue For
                'strQ = "SELECT actionreasonunkid FROM " & dtRow.Item("TableName").ToString & " WHERE actionreasonunkid = @actionreasonunkid "
                strQ = "SELECT " & dtRow.Item("COLUMN_NAME") & " FROM " & dtRow.Item("TableName") & " WHERE " & dtRow.Item("COLUMN_NAME") & " = @actionreasonunkid "
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", _
                            Optional ByVal strName As String = "", _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal blnIsReason As Boolean = False) As Boolean 'S.SANDEEP [ 12 MAY 2012 ] -- START -- END
        'Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  actionreasonunkid " & _
              ", code " & _
              ", reason_action " & _
              ", isreason " & _
              ", isactive " & _
             "FROM hraction_reason_master " & _
             "WHERE 1 = 1 "

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strName.Length > 0 Then
                strQ &= "AND reason_action = @reason_action "
            End If

            If strCode.Length > 0 Then
                strQ &= "AND code = @code "
            End If

            'S.SANDEEP [ 12 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If blnIsReason Then
                strQ &= "AND isreason = 1 "
            Else
                strQ &= "AND isreason = 0 "
            End If
            'S.SANDEEP [ 12 MAY 2012 ] -- END



            If intUnkid > 0 Then
                strQ &= " AND actionreasonunkid <> @actionreasonunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@reason_action", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@actionreasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal mblnFlag As Boolean = False, Optional ByVal isAction As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If mblnFlag = True Then
                strQ = "SELECT 0 As actionreasonunkid ,@code as code, @ItemName As  name  UNION "
            End If
            strQ &= "SELECT actionreasonunkid,code,reason_action FROM hraction_reason_master WHERE isactive =1 "
            If isAction = False Then
                strQ &= " AND isreason = 1 "
            Else
                strQ &= " AND isreason = 0 "
            End If

            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            'objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Code"))
            'S.SANDEEP [24 MAY 2016] -- END



            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        End Try
    End Function


    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function GetReasonUnkid(ByVal IsReasion As Boolean, ByVal StrReason As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                   "  actionreasonunkid " & _
                   "FROM hraction_reason_master " & _
                   "WHERE isactive = 1 AND isreason = @IsReasion " & _
                   "AND LTRIM(RTRIM(reason_action)) = LTRIM(RTRIM(@Reason)) "

            objDataOperation.AddParameter("@Reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrReason)
            objDataOperation.AddParameter("@IsReasion", SqlDbType.Bit, eZeeDataType.BIT_SIZE, IsReasion)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("actionreasonunkid")
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetReasonUnkid; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 07 NOV 2011 ] -- END


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
			Language.setMessage(mstrModuleName, 2, "This Action/Reason is already defined. Please define new Action/Reason.")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
