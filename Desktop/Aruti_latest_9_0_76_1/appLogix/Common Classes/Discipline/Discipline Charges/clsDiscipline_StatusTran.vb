﻿'************************************************************************************************************************************
'Class Name : clsDiscipline_StatusTran.vb
'Purpose    :
'Date       :20/03/2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsDiscipline_StatusTran
    Private Const mstrModuleName = "clsDiscipline_StatusTran"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintStatustranunkid As Integer
    Private mintDisciplinefileunkid As Integer
    Private mdtStatusdate As Date = Nothing
    Private mintDisciplinestatusunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mintUserunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    Private mintVoiduserunkid As Integer = 0
    Private mblnIsexternal As Boolean = False
    Private mobjDataOperation As clsDataOperation
    'S.SANDEEP [24 MAY 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    Private mblnSetCaseOpenModeAutomatic As Boolean = False
    'S.SANDEEP [24 MAY 2016] -- END

    'S.SANDEEP |01-MAY-2020| -- START
    'ISSUE/ENHANCEMENT : MODIFICATION ON REPORT (ADDTIONAL COLUMNS)
    Private mintDisciplineOpeningReasonId As Integer = 0
    'S.SANDEEP |01-MAY-2020| -- END

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statustranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Statustranunkid() As Integer
        Get
            Return mintStatustranunkid
        End Get
        Set(ByVal value As Integer)
            mintStatustranunkid = value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disciplinefileunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Disciplinefileunkid() As Integer
        Get
            Return mintDisciplinefileunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplinefileunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusdate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Statusdate() As Date
        Get
            Return mdtStatusdate
        End Get
        Set(ByVal value As Date)
            mdtStatusdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disciplinestatusunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Disciplinestatusunkid() As Integer
        Get
            Return mintDisciplinestatusunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplinestatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isexternal
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isexternal() As Boolean
        Get
            Return mblnIsexternal
        End Get
        Set(ByVal value As Boolean)
            mblnIsexternal = Value
        End Set
    End Property

    Public WriteOnly Property _objDataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            mobjDataOperation = value
        End Set
    End Property

    'S.SANDEEP [24 MAY 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    Public WriteOnly Property _SetCaseOpenModeAutomatic() As Boolean
        Set(ByVal value As Boolean)
            mblnSetCaseOpenModeAutomatic = value
        End Set
    End Property
    'S.SANDEEP [24 MAY 2016] -- END

    'S.SANDEEP |01-MAY-2020| -- START
    'ISSUE/ENHANCEMENT : MODIFICATION ON REPORT (ADDTIONAL COLUMNS)
    Public Property _DisciplineOpeningReasonId() As Integer
        Get
            Return mintDisciplineOpeningReasonId
        End Get
        Set(ByVal value As Integer)
            mintDisciplineOpeningReasonId = value
        End Set
    End Property
    'S.SANDEEP |01-MAY-2020| -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  statustranunkid " & _
              ", disciplinefileunkid " & _
              ", statusdate " & _
              ", disciplinestatusunkid " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
                   ", isexternal " & _
                   ", openreasonunkid " & _
             "FROM hrdiscipline_status_tran " & _
             "WHERE statustranunkid = @statustranunkid "
            'S.SANDEEP |01-MAY-2020| -- START {openreasonunkid} -- END

            objDataOperation.AddParameter("@statustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatustranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintstatustranunkid = CInt(dtRow.Item("statustranunkid"))
                mintdisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
                mdtstatusdate = dtRow.Item("statusdate")
                mintdisciplinestatusunkid = CInt(dtRow.Item("disciplinestatusunkid"))
                mstrremark = dtRow.Item("remark").ToString
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtvoiddatetime = dtRow.Item("voiddatetime")
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mblnIsexternal = CBool(dtRow.Item("isexternal"))
                'S.SANDEEP |01-MAY-2020| -- START
                'ISSUE/ENHANCEMENT : MODIFICATION ON REPORT (ADDTIONAL COLUMNS)
                mintDisciplineOpeningReasonId = CInt(dtRow.Item("openreasonunkid"))
                'S.SANDEEP |01-MAY-2020| -- END
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intDisciplineId As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                   " hrdiscipline_status_tran.statustranunkid " & _
                   ",hrdiscipline_status_tran.disciplinefileunkid " & _
                   ",hrdiscipline_status_tran.statusdate " & _
                   ",hrdiscipline_status_tran.disciplinestatusunkid " & _
                   ",hrdiscipline_status_tran.remark " & _
                   ",hrdiscipline_status_tran.userunkid " & _
                   ",hrdiscipline_status_tran.isvoid " & _
                   ",hrdiscipline_status_tran.voiduserunkid " & _
                   ",hrdiscipline_status_tran.voiddatetime " & _
                   ",hrdiscipline_status_tran.voidreason " & _
                   ",hrdisciplinestatus_master.name AS status " & _
                       ",hrdiscipline_status_tran.isexternal " & _
                   ",hrdiscipline_status_tran.openreasonunkid " & _
                   "FROM hrdiscipline_status_tran " & _
                   " JOIN hrdisciplinestatus_master ON hrdiscipline_status_tran.disciplinestatusunkid = hrdisciplinestatus_master.disciplinestatusunkid " & _
                   "WHERE isvoid = 0 "
            'Hemant (22 Jan 2021) -- [hrdisciplinestatus_master.isexternal --> hrdiscipline_status_tran.isexternal, hrdisciplinestatus_master.openreasonunkid --> hrdiscipline_status_tran.isexternal]
            'S.SANDEEP |01-MAY-2020| -- START {openreasonunkid} -- END

            If intDisciplineId > 0 Then
                strQ &= " AND disciplinefileunkid = '" & intDisciplineId & "'"
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    '' <summary>
    '' Modify By: Sandeep J. Sharma
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> INSERT INTO Database Table (hrdiscipline_status_tran) </purpose>
    Private Sub SetCaseOpenMode(ByRef mblnExMode As Boolean, ByVal objDataOperation As clsDataOperation, ByVal intDisciplineFileId As Integer)
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "SELECT TOP 1 " & _
                   "  @external = hrdiscipline_status_tran.isexternal " & _
                   "FROM hrdiscipline_status_tran " & _
                   "WHERE hrdiscipline_status_tran.isvoid = 0 AND hrdiscipline_status_tran.disciplinefileunkid = '" & intDisciplineFileId & "' " & _
                   "ORDER BY hrdiscipline_status_tran.statusdate DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@external", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnExMode, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mblnExMode = objDataOperation.GetParameterValue("@external")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetCaseOpenMode; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrdiscipline_status_tran) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            If mblnSetCaseOpenModeAutomatic Then
                Call SetCaseOpenMode(mblnIsexternal, objDataOperation, mintDisciplinefileunkid)
                objDataOperation.ClearParameters()
            End If
            'S.SANDEEP [24 MAY 2016] -- END

            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid.ToString)
            objDataOperation.AddParameter("@statusdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStatusdate.ToString)
            objDataOperation.AddParameter("@disciplinestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinestatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@isexternal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternal.ToString)
            'S.SANDEEP |01-MAY-2020| -- START
            'ISSUE/ENHANCEMENT : MODIFICATION ON REPORT (ADDTIONAL COLUMNS)
            objDataOperation.AddParameter("@openreasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplineOpeningReasonId.ToString)
            'S.SANDEEP |01-MAY-2020| -- END

            strQ = "INSERT INTO hrdiscipline_status_tran ( " & _
              "  disciplinefileunkid " & _
              ", statusdate " & _
              ", disciplinestatusunkid " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", isexternal " & _
              ", openreasonunkid " & _
            ") VALUES (" & _
              "  @disciplinefileunkid " & _
              ", @statusdate " & _
              ", @disciplinestatusunkid " & _
              ", @remark " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @isexternal " & _
              ", @openreasonunkid " & _
            "); SELECT @@identity"
            'S.SANDEEP |01-MAY-2020| -- START {openreasonunkid} -- END
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintStatustranunkid = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrdiscipline_status_tran) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@statustranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstatustranunkid.ToString)
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintdisciplinefileunkid.ToString)
            objDataOperation.AddParameter("@statusdate", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtstatusdate.ToString)
            objDataOperation.AddParameter("@disciplinestatusunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintdisciplinestatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrremark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@isexternal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternal.ToString)
            'S.SANDEEP |01-MAY-2020| -- START
            'ISSUE/ENHANCEMENT : MODIFICATION ON REPORT (ADDTIONAL COLUMNS)
            objDataOperation.AddParameter("@openreasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplineOpeningReasonId.ToString)
            'S.SANDEEP |01-MAY-2020| -- END

            strQ = "UPDATE hrdiscipline_status_tran SET " & _
              "  disciplinefileunkid = @disciplinefileunkid" & _
              ", statusdate = @statusdate" & _
              ", disciplinestatusunkid = @disciplinestatusunkid" & _
              ", remark = @remark" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", isexternal = @isexternal " & _
              ", openreasonunkid = @openreasonunkid " & _
            "WHERE statustranunkid = @statustranunkid "
            'S.SANDEEP |01-MAY-2020| -- START {openreasonunkid} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrdiscipline_file) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()

        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE hrdiscipline_status_tran SET " & _
                     " isvoid = 1 " & _
                     ",voiddatetime = @voiddatetime " & _
                     ",voidreason = @voidreason " & _
                     ",voiduserunkid = @voiduserunkid " & _
                   "WHERE disciplinefileunkid = @disciplinefileunkid "

            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrdiscipline_file", "disciplinefileunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If mobjDataOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If mobjDataOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

End Class


'***************************** 'S.SANDEEP [24 MAY 2016] DISCIPLINE MAKE OVER *********************************************
'
'
'
'
'
'Public Class clsDiscipline_StatusTran
'    Private Const mstrModuleName = "clsDiscipline_StatusTran"
'    Dim mstrMessage As String = ""

'#Region " Private variables "

'    Private mintStatustranunkid As Integer
'    Private mintDisciplinefileunkid As Integer
'    Private mdtStatusdate As Date
'    Private mintDisciplinestatusunkid As Integer
'    Private mstrRemark As String = String.Empty
'    Private mintUserunkid As Integer
'    Private mblnIsvoid As Boolean
'    Private mdtVoiddatetime As Date
'    Private mstrVoidreason As String = String.Empty
'    Private mintVoiduserunkid As Integer
'    'S.SANDEEP [ 20 APRIL 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mblnIsexternal As Boolean
'    'S.SANDEEP [ 20 APRIL 2012 ] -- END

'#End Region

'#Region " Properties "

'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set statustranunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Statustranunkid() As Integer
'        Get
'            Return mintStatustranunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintStatustranunkid = value
'            Call getData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set disciplinefileunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Disciplinefileunkid() As Integer
'        Get
'            Return mintDisciplinefileunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintDisciplinefileunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set statusdate
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Statusdate() As Date
'        Get
'            Return mdtStatusdate
'        End Get
'        Set(ByVal value As Date)
'            mdtStatusdate = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set disciplinestatusunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Disciplinestatusunkid() As Integer
'        Get
'            Return mintDisciplinestatusunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintDisciplinestatusunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set remark
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Remark() As String
'        Get
'            Return mstrRemark
'        End Get
'        Set(ByVal value As String)
'            mstrRemark = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set userunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Userunkid() As Integer
'        Get
'            Return mintUserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintUserunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isvoid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Isvoid() As Boolean
'        Get
'            Return mblnIsvoid
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsvoid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiddatetime
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voiddatetime() As Date
'        Get
'            Return mdtVoiddatetime
'        End Get
'        Set(ByVal value As Date)
'            mdtVoiddatetime = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voidreason
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voidreason() As String
'        Get
'            Return mstrVoidreason
'        End Get
'        Set(ByVal value As String)
'            mstrVoidreason = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiduserunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voiduserunkid() As Integer
'        Get
'            Return mintVoiduserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintVoiduserunkid = Value
'        End Set
'    End Property

'    'S.SANDEEP [ 20 APRIL 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    ''' <summary>
'    ''' Purpose: Get or Set isexternal
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Isexternal() As Boolean
'        Get
'            Return mblnIsexternal
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsexternal = Value
'        End Set
'    End Property
'    'S.SANDEEP [ 20 APRIL 2012 ] -- END

'#End Region

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  statustranunkid " & _
'              ", disciplinefileunkid " & _
'              ", statusdate " & _
'              ", disciplinestatusunkid " & _
'              ", remark " & _
'              ", userunkid " & _
'              ", isvoid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime " & _
'              ", voidreason " & _
'                   ", isexternal " & _
'             "FROM hrdiscipline_status_tran " & _
'             "WHERE statustranunkid = @statustranunkid "

'            'S.SANDEEP [ 20 APRIL 2012 isexternal ] -- START -- END

'            objDataOperation.AddParameter("@statustranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintStatusTranUnkId.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintstatustranunkid = CInt(dtRow.Item("statustranunkid"))
'                mintdisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
'                mdtstatusdate = dtRow.Item("statusdate")
'                mintdisciplinestatusunkid = CInt(dtRow.Item("disciplinestatusunkid"))
'                mstrremark = dtRow.Item("remark").ToString
'                mintuserunkid = CInt(dtRow.Item("userunkid"))
'                mblnIsvoid = CBool(dtRow.Item("isvoid"))
'                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
'                mdtvoiddatetime = dtRow.Item("voiddatetime")
'                mstrVoidreason = dtRow.Item("voidreason").ToString
'                'S.SANDEEP [ 20 APRIL 2012 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                mblnIsexternal = CBool(dtRow.Item("isexternal"))
'                'S.SANDEEP [ 20 APRIL 2012 ] -- END
'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'        End Try
'    End Sub

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, Optional ByVal intDisciplineId As Integer = -1) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                   " hrdiscipline_status_tran.statustranunkid " & _
'                   ",hrdiscipline_status_tran.disciplinefileunkid " & _
'                   ",hrdiscipline_status_tran.statusdate " & _
'                   ",hrdiscipline_status_tran.disciplinestatusunkid " & _
'                   ",hrdiscipline_status_tran.remark " & _
'                   ",hrdiscipline_status_tran.userunkid " & _
'                   ",hrdiscipline_status_tran.isvoid " & _
'                   ",hrdiscipline_status_tran.voiduserunkid " & _
'                   ",hrdiscipline_status_tran.voiddatetime " & _
'                   ",hrdiscipline_status_tran.voidreason " & _
'                   ",hrdisciplinestatus_master.name AS status " & _
'                       ",hrdisciplinestatus_master.isexternal " & _
'                   "FROM hrdiscipline_status_tran " & _
'                   " JOIN hrdisciplinestatus_master ON hrdiscipline_status_tran.disciplinestatusunkid = hrdisciplinestatus_master.disciplinestatusunkid " & _
'                   "WHERE isvoid = 0 "

'            'S.SANDEEP [ 20 APRIL 2012 isexternal ] -- START -- END

'            If intDisciplineId > 0 Then
'                strQ &= " AND disciplinefileunkid = '" & intDisciplineId & "'"
'            End If

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'        End Try
'        Return dsList
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (hrdiscipline_status_tran) </purpose>
'    Public Function Insert() As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Dim objDataOperation As New clsDataOperation
'        Try
'            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid.ToString)
'            objDataOperation.AddParameter("@statusdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStatusdate.ToString)
'            objDataOperation.AddParameter("@disciplinestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinestatusunkid.ToString)
'            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            If mdtVoiddatetime <> Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
'            'S.SANDEEP [ 20 APRIL 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objDataOperation.AddParameter("@isexternal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternal.ToString)
'            'S.SANDEEP [ 20 APRIL 2012 ] -- END

'            strQ = "INSERT INTO hrdiscipline_status_tran ( " & _
'              "  disciplinefileunkid " & _
'              ", statusdate " & _
'              ", disciplinestatusunkid " & _
'              ", remark " & _
'              ", userunkid " & _
'              ", isvoid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime " & _
'              ", voidreason" & _
'              ", isexternal " & _
'            ") VALUES (" & _
'              "  @disciplinefileunkid " & _
'              ", @statusdate " & _
'              ", @disciplinestatusunkid " & _
'              ", @remark " & _
'              ", @userunkid " & _
'              ", @isvoid " & _
'              ", @voiduserunkid " & _
'              ", @voiddatetime " & _
'              ", @voidreason" & _
'              ", @isexternal " & _
'            "); SELECT @@identity"

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintStatustranunkid = dsList.Tables(0).Rows(0).Item(0)

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (hrdiscipline_status_tran) </purpose>
'    Public Function Update() As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@statustranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstatustranunkid.ToString)
'            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintdisciplinefileunkid.ToString)
'            objDataOperation.AddParameter("@statusdate", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtstatusdate.ToString)
'            objDataOperation.AddParameter("@disciplinestatusunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintdisciplinestatusunkid.ToString)
'            objDataOperation.AddParameter("@remark", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrremark.ToString)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            If mdtVoiddatetime <> Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            End If
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'            'S.SANDEEP [ 20 APRIL 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            objDataOperation.AddParameter("@isexternal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternal.ToString)
'            'S.SANDEEP [ 20 APRIL 2012 ] -- END



'            strQ = "UPDATE hrdiscipline_status_tran SET " & _
'              "  disciplinefileunkid = @disciplinefileunkid" & _
'              ", statusdate = @statusdate" & _
'              ", disciplinestatusunkid = @disciplinestatusunkid" & _
'              ", remark = @remark" & _
'              ", userunkid = @userunkid" & _
'              ", isvoid = @isvoid" & _
'              ", voiduserunkid = @voiduserunkid" & _
'              ", voiddatetime = @voiddatetime" & _
'              ", voidreason = @voidreason " & _
'              ", isexternal = @isexternal " & _
'            "WHERE statustranunkid = @statustranunkid "

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (hrdiscipline_file) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation

'        objDataOperation.BindTransaction()

'        Try
'            strQ = "UPDATE hrdiscipline_status_tran SET " & _
'                     " isvoid = 1 " & _
'                     ",voiddatetime = @voiddatetime " & _
'                     ",voidreason = @voidreason " & _
'                     ",voiduserunkid = @voiduserunkid " & _
'                   "WHERE disciplinefileunkid = @disciplinefileunkid "

'            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
'            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrdiscipline_file", "disciplinefileunkid", intUnkid) = False Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If
'            objDataOperation.ReleaseTransaction(True)

'            Return True
'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'End Class
