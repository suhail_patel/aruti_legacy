﻿'************************************************************************************************************************************
'Class Name : clsDiscipline_file_tran.vb
'Purpose    :
'Date       :24-JUN-2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports System.Windows.Forms
Imports eZee.Common.eZeeForm

#End Region

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsDiscipline_chargestatus_tran

#Region " Private Variables "

    Private mstrModuleName As String = "clsDiscipline_chargestatus_tran"
    Private mstrMessage As String = ""
    Private mintChargestatusunkid As Integer = 0
    Private mintDisciplinefiletranunkid As Integer = 0
    Private mintDisciplinefileunkid As Integer = 0
    Private mdtStatusdate As DateTime = Nothing
    Private mintStatusunkid As Integer = 0
    Private mstrRemark As String = ""
    Private mintUserunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mdtVoidatetime As DateTime = Nothing
    Private mstrVoidreason As String = ""
    Private mblnIsexternal As Boolean = False
    Private mobjDataOperation As clsDataOperation
    Private mblnSetCaseOpenModeAutomatic As Boolean = False
    Private mblnIsApproved As Boolean = False
    Private mdtApprovalDate As DateTime = Nothing
    Private mstrApproval_Remark As String = String.Empty
    Private mblnIssubmitforApproval As Boolean = False

#End Region

#Region " Properties "

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _Chargestatusunkid() As Integer
        Get
            Return mintChargestatusunkid
        End Get
        Set(ByVal value As Integer)
            mintChargestatusunkid = value
            Call GetData()
        End Set
    End Property

    Public Property _Disciplinefiletranunkid() As Integer
        Get
            Return mintDisciplinefiletranunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplinefiletranunkid = value
        End Set
    End Property

    Public Property _Disciplinefileunkid() As Integer
        Get
            Return mintDisciplinefileunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplinefileunkid = value
        End Set
    End Property

    Public Property _Statusdate() As DateTime
        Get
            Return mdtStatusdate
        End Get
        Set(ByVal value As DateTime)
            mdtStatusdate = value
        End Set
    End Property

    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    Public Property _Voidatetime() As DateTime
        Get
            Return mdtVoidatetime
        End Get
        Set(ByVal value As DateTime)
            mdtVoidatetime = value
        End Set
    End Property

    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public Property _Isexternal() As Boolean
        Get
            Return mblnIsexternal
        End Get
        Set(ByVal value As Boolean)
            mblnIsexternal = value
        End Set
    End Property

    Public Property _mDataOperation() As clsDataOperation
        Get
            Return mobjDataOperation
        End Get
        Set(ByVal value As clsDataOperation)
            mobjDataOperation = value
        End Set
    End Property

    Public WriteOnly Property _SetCaseOpenModeAutomatic() As Boolean
        Set(ByVal value As Boolean)
            mblnSetCaseOpenModeAutomatic = value
        End Set
    End Property

    Public Property _IsApproved() As Boolean
        Get
            Return mblnIsApproved
        End Get
        Set(ByVal value As Boolean)
            mblnIsApproved = value
        End Set
    End Property

    Public Property _ApprovalDate() As DateTime
        Get
            Return mdtApprovalDate
        End Get
        Set(ByVal value As DateTime)
            mdtApprovalDate = value
        End Set
    End Property

    Public Property _Approval_Remark() As String
        Get
            Return mstrApproval_Remark
        End Get
        Set(ByVal value As String)
            mstrApproval_Remark = value
        End Set
    End Property

    Public Property _IssubmitforApproval() As Boolean
        Get
            Return mblnIssubmitforApproval
        End Get
        Set(ByVal value As Boolean)
            mblnIssubmitforApproval = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                   "     chargestatusunkid " & _
                   "    ,disciplinefileunkid " & _
                   "    ,disciplinefiletranunkid " & _
                   "    ,statusdate " & _
                   "    ,statusunkid " & _
                   "    ,remark " & _
                   "    ,userunkid " & _
                   "    ,isvoid " & _
                   "    ,voiduserunkid " & _
                   "    ,voidatetime " & _
                   "    ,voidreason " & _
                   "    ,isexternal " & _
                   "    ,isapproved " & _
                   "    ,approval_date " & _
                   "    ,approval_remark " & _
                   "    ,issubmitforapproval " & _
                   "FROM hrdiscipline_chargestatus_tran " & _
                   "WHERE hrdiscipline_chargestatus_tran.isvoid = 0 " & _
                   "AND chargestatusunkid = @chargestatusunkid "

            objDataOperation.AddParameter("@chargestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChargestatusunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintChargestatusunkid = CInt(dtRow.Item("chargestatusunkid"))
                mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
                mintDisciplinefiletranunkid = CInt(dtRow.Item("disciplinefiletranunkid"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mdtStatusdate = CDate(dtRow.Item("statusdate"))
                mstrRemark = CStr(dtRow.Item("remark"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voidatetime")) = False Then
                    mdtVoidatetime = CDate(dtRow.Item("voidatetime"))
                End If
                mstrVoidreason = CStr(dtRow.Item("voidreason"))
                mblnIsexternal = CBool(dtRow.Item("isexternal"))
                mblnIsApproved = CBool(dtRow.Item("isapproved"))
                If IsDBNull(dtRow.Item("approval_date")) = False Then
                    mdtApprovalDate = dtRow.Item("approval_date")
                Else
                    mdtApprovalDate = Nothing
                End If
                mstrApproval_Remark = CStr(dtRow.Item("approval_remark"))
                mblnIssubmitforApproval = CBool(dtRow.Item("issubmitforapproval"))
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intDisciplineId As Integer = -1, Optional ByVal intDisciplineFiletranId As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                   "     chargestatusunkid " & _
                   "    ,disciplinefileunkid " & _
                   "    ,disciplinefiletranunkid " & _
                   "    ,statusdate " & _
                   "    ,statusunkid " & _
                   "    ,remark " & _
                   "    ,userunkid " & _
                   "    ,isvoid " & _
                   "    ,voiduserunkid " & _
                   "    ,voidatetime " & _
                   "    ,voidreason " & _
                   "    ,isexternal " & _
                   "    ,isapproved " & _
                   "    ,approval_date " & _
                   "    ,approval_remark " & _
                   "    ,issubmitforapproval " & _
                   "FROM hrdiscipline_chargestatus_tran " & _
                   "WHERE hrdiscipline_chargestatus_tran.isvoid = 0 "

            If intDisciplineId > 0 Then
                strQ &= " AND disciplinefileunkid = '" & intDisciplineId & "'"
            End If

            If intDisciplineFiletranId > 0 Then
                strQ &= " AND disciplinefiletranunkid = '" & intDisciplineFiletranId & "'"
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrdiscipline_status_tran) </purpose>
    Public Function Insert(ByVal mDicVlaues As Dictionary(Of Integer, String), ByVal intDisciplineStatusId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        mobjDataOperation = objDataOperation
        Dim blnFlag As Boolean = False
        Dim objDiscFileStatus As New clsDiscipline_StatusTran
        Try
            For Each iKey As Integer In mDicVlaues.Keys
                Dim TagIds As List(Of Integer) = mDicVlaues(iKey).Split(","c).[Select](AddressOf Integer.Parse).ToList()
                For Each i As Integer In TagIds
                    mintDisciplinefileunkid = iKey
                    mintDisciplinefiletranunkid = i
                    If Insert() = False Then
                        blnFlag = False
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    Else
                        blnFlag = True
                    End If
                Next
            Next

            If blnFlag Then
                If intDisciplineStatusId <> 2 Then
                    objDiscFileStatus._objDataOperation = mobjDataOperation
                    objDiscFileStatus._Disciplinefileunkid = mintDisciplinefileunkid
                    objDiscFileStatus._Disciplinestatusunkid = 2
                    objDiscFileStatus._Isvoid = False
                    objDiscFileStatus._Remark = ""
                    objDiscFileStatus._Statusdate = ConfigParameter._Object._CurrentDateAndTime
                    objDiscFileStatus._Userunkid = User._Object._Userunkid
                    objDiscFileStatus._Voiddatetime = Nothing
                    objDiscFileStatus._Voidreason = ""
                    objDiscFileStatus._Voiduserunkid = -1
                    objDiscFileStatus._Isexternal = mblnIsexternal
                    If objDiscFileStatus.Insert() = False Then
                        mstrMessage = objDiscFileStatus._Message
                        Return False
                    End If
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    '' <summary>
    '' Modify By: Sandeep J. Sharma
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> INSERT INTO Database Table (hrdiscipline_status_tran) </purpose>
    Private Sub SetCaseOpenMode(ByRef mblnExMode As Boolean, ByVal objDataOperation As clsDataOperation, ByVal intDisciplineFileTranId As Integer)
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "SELECT TOP 1 " & _
                   "   @external = hrdiscipline_chargestatus_tran.isexternal " & _
                   "  ,@issubmitforapproval = hrdiscipline_chargestatus_tran.issubmitforapproval " & _
                   "  ,@isapproved = hrdiscipline_chargestatus_tran.isapproved " & _
                   "  ,@approval_remark = hrdiscipline_chargestatus_tran.approval_remark "
            If mdtApprovalDate <> Nothing Then
                strQ &= "  ,@approval_date = hrdiscipline_chargestatus_tran.approval_date "
            End If
            strQ &= "FROM hrdiscipline_chargestatus_tran " & _
                    "WHERE hrdiscipline_chargestatus_tran.isvoid = 0 AND hrdiscipline_chargestatus_tran.disciplinefiletranunkid = '" & intDisciplineFileTranId & "' " & _
                    "ORDER BY hrdiscipline_chargestatus_tran.statusdate DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@external", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnExMode, ParameterDirection.InputOutput)
            objDataOperation.AddParameter("@issubmitforapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmitforApproval, ParameterDirection.InputOutput)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsApproved, ParameterDirection.InputOutput)
            If mdtApprovalDate <> Nothing Then
                objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovalDate, ParameterDirection.InputOutput)
            End If
            objDataOperation.AddParameter("@approval_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrApproval_Remark, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mblnExMode = objDataOperation.GetParameterValue("@external")
            mblnIssubmitforApproval = objDataOperation.GetParameterValue("@issubmitforapproval")
            mblnIsApproved = objDataOperation.GetParameterValue("@isapproved")
            mdtApprovalDate = objDataOperation.GetParameterValue("@approval_date")
            mstrApproval_Remark = objDataOperation.GetParameterValue("@approval_remark")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetCaseOpenMode; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrdiscipline_status_tran) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            If mblnSetCaseOpenModeAutomatic Then
                Call SetCaseOpenMode(mblnIsexternal, objDataOperation, mintDisciplinefiletranunkid)
                objDataOperation.ClearParameters()
            End If

            strQ = "INSERT INTO hrdiscipline_chargestatus_tran " & _
                    "( " & _
                          "disciplinefileunkid " & _
                         ",disciplinefiletranunkid " & _
                         ",statusdate " & _
                         ",statusunkid " & _
                         ",remark " & _
                         ",userunkid " & _
                         ",isvoid " & _
                         ",voiduserunkid " & _
                         ",voidatetime " & _
                         ",voidreason " & _
                         ",isexternal " & _
                         ",isapproved " & _
                         ",approval_date " & _
                         ",approval_remark " & _
                         ",issubmitforapproval " & _
                    ") " & _
                    "VALUES " & _
                    "( " & _
                          "@disciplinefileunkid " & _
                         ",@disciplinefiletranunkid " & _
                         ",@statusdate " & _
                         ",@statusunkid " & _
                         ",@remark " & _
                         ",@userunkid " & _
                         ",@isvoid " & _
                         ",@voiduserunkid " & _
                         ",@voidatetime " & _
                         ",@voidreason " & _
                         ",@isexternal " & _
                         ",@isapproved " & _
                         ",@approval_date " & _
                         ",@approval_remark " & _
                         ",@issubmitforapproval " & _
                    "); SELECT @@identity "

            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid)
            objDataOperation.AddParameter("@disciplinefiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefiletranunkid)
            objDataOperation.AddParameter("@statusdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStatusdate)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            If mdtVoidatetime <> Nothing Then
                objDataOperation.AddParameter("@voidatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidatetime)
            Else
                objDataOperation.AddParameter("@voidatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@isexternal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternal)

            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsApproved)
            If mdtApprovalDate <> Nothing Then
                objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovalDate)
            Else
                objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@approval_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrApproval_Remark)
            objDataOperation.AddParameter("@issubmitforapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmitforApproval)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrdiscipline_status_tran) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@chargestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChargestatusunkid)
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid)
            objDataOperation.AddParameter("@disciplinefiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefiletranunkid)
            objDataOperation.AddParameter("@statusdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStatusdate)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            If mdtVoidatetime <> Nothing Then
                objDataOperation.AddParameter("@voidatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidatetime)
            Else
                objDataOperation.AddParameter("@voidatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@isexternal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexternal)

            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsApproved)
            If mdtApprovalDate <> Nothing Then
                objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovalDate)
            Else
                objDataOperation.AddParameter("@approval_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@approval_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrApproval_Remark)
            objDataOperation.AddParameter("@issubmitforapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmitforApproval)

            strQ = "UPDATE hrdiscipline_chargestatus_tran SET " & _
                   "     disciplinefileunkid = @disciplinefileunkid " & _
                   "    ,disciplinefiletranunkid = @disciplinefiletranunkid " & _
                   "    ,statusdate = @statusdate " & _
                   "    ,statusunkid = @statusunkid " & _
                   "    ,remark = @remark " & _
                   "    ,userunkid = @userunkid " & _
                   "    ,isvoid = @isvoid " & _
                   "    ,voiduserunkid = @voiduserunkid " & _
                   "    ,voidatetime = @voidatetime " & _
                   "    ,voidreason = @voidreason " & _
                   "    ,isexternal = @isexternal " & _
                   "    ,isapproved = @isapproved " & _
                   "    ,approval_date = @approval_date " & _
                   "    ,approval_remark = @approval_remark " & _
                   "    ,issubmitforapproval = @issubmitforapproval " & _
                   "WHERE chargestatusunkid = @chargestatusunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Integer</returns>
    ''' <purpose></purpose>
    Public Function GetLastStatus(ByVal intDisciplineFileTranId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim intStatusId As Integer = 0
        Try
            StrQ = "SELECT TOP 1 " & _
                   "    @statusunkid = hrdiscipline_chargestatus_tran.statusunkid " & _
                   "FROM hrdiscipline_chargestatus_tran " & _
                   "WHERE hrdiscipline_chargestatus_tran.isvoid = 0 " & _
                   "  AND hrdiscipline_chargestatus_tran.disciplinefiletranunkid = @disciplinefiletranunkid " & _
                   "ORDER BY hrdiscipline_chargestatus_tran.statusdate DESC "

            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusId, ParameterDirection.InputOutput)
            objDataOperation.AddParameter("@disciplinefiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDisciplineFileTranId)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            intStatusId = objDataOperation.GetParameterValue("@statusunkid")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLastStatus; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return intStatusId
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Integer</returns>
    ''' <purpose></purpose>
    Public Function GetLastChargeStatusUnkid(ByVal intDisciplineFileTranId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim intchargestatusunkid As Integer = 0
        Try
            StrQ = "SELECT TOP 1 " & _
                   "    @chargestatusunkid = hrdiscipline_chargestatus_tran.chargestatusunkid " & _
                   "FROM hrdiscipline_chargestatus_tran " & _
                   "WHERE hrdiscipline_chargestatus_tran.isvoid = 0 " & _
                   "  AND hrdiscipline_chargestatus_tran.disciplinefiletranunkid = @disciplinefiletranunkid " & _
                   "ORDER BY hrdiscipline_chargestatus_tran.statusdate DESC "

            objDataOperation.AddParameter("@chargestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intchargestatusunkid, ParameterDirection.InputOutput)
            objDataOperation.AddParameter("@disciplinefiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDisciplineFileTranId)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            intchargestatusunkid = objDataOperation.GetParameterValue("@chargestatusunkid")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLastChargeStatusUnkid; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return intchargestatusunkid
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrdiscipline_file) </purpose>
    Public Function Delete(ByVal intDisciplineFileId As Integer, ByVal intDisciplineFileTranId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Try

            strQ = "UPDATE hrdiscipline_chargestatus_tran SET " & _
                   "     isvoid = @isvoid " & _
                   "    ,voiduserunkid = @voiduserunkid " & _
                   "    ,voidatetime = @voidatetime " & _
                   "    ,voidreason = @voidreason " & _
                   "WHERE disciplinefileunkid = @disciplinefileunkid AND disciplinefiletranunkid = @disciplinefiletranunkid "

            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDisciplineFileId)
            objDataOperation.AddParameter("@disciplinefiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDisciplineFileTranId)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

End Class
