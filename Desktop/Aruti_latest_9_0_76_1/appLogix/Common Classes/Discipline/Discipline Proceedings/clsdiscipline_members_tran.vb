﻿'************************************************************************************************************************************
'Class Name : clsdiscipline_members_tran.vb
'Purpose    :
'Date       : 24/06/2016
'Written By : Nilay
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System.Text

Public Class clsdiscipline_members_tran

    Private Shared ReadOnly mstrModuleName As String = "clsdiscipline_members_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintDisciplineMembersTranunkid As Integer = -1
    Private mintDisciplineProceedingMasterunkid As Integer = -1
    Private mintDisciplineFileTranunkid As Integer = -1
    Private mdtMemTran As DataTable
    Private mobjDataOp As clsDataOperation
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    'S.SANDEEP [24 MAY 2016] -- Start
    'Email Notification
    Private mintDisciplineFileUnkid As Integer = 0
    Private mintLoginEmployeeunkid As Integer = 0
    Private mintLoginTypeId As Integer = 0
    'S.SANDEEP [24 MAY 2016] -- End

#End Region

#Region " Properties "

    Public Property _DisciplineProceedingMasterunkid(ByVal mdtEffectiveDate As Date) As Integer
        Get
            Return mintDisciplineProceedingMasterunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplineProceedingMasterunkid = value
            Call GetMemTran(mdtEffectiveDate)
        End Set
    End Property

    Public Property _DisciplineFileTranunkid() As Integer
        Get
            Return mintDisciplineFileTranunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplineFileTranunkid = value
        End Set
    End Property

    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            mobjDataOp = value
        End Set
    End Property

    Public Property _MemTranTable() As DataTable
        Get
            Return mdtMemTran
        End Get
        Set(ByVal value As DataTable)
            mdtMemTran = value
        End Set
    End Property

    Public WriteOnly Property _WebFormName() As String
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    'S.SANDEEP [24 MAY 2016] -- Start
    'Email Notification
    Public Property _DisciplineFileUnkid() As Integer
        Get
            Return mintDisciplineFileUnkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplineFileUnkid = value
        End Set
    End Property

    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    Public Property _LoginTypeId() As Integer
        Get
            Return mintLoginTypeId
        End Get
        Set(ByVal value As Integer)
            mintLoginTypeId = value
        End Set
    End Property
    'S.SANDEEP [24 MAY 2016] -- End



#End Region

#Region " Constructor "

    Public Sub New()
        Try
            mdtMemTran = New DataTable("Proceeding")

            mdtMemTran.Columns.Add("disciplinememberstranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtMemTran.Columns.Add("disciplineproceedingmasterunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtMemTran.Columns.Add("disciplinefiletranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtMemTran.Columns.Add("committeetranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            'DISPLAY
            mdtMemTran.Columns.Add("committeemasterunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtMemTran.Columns.Add("committee", System.Type.GetType("System.String")).DefaultValue = ""
            mdtMemTran.Columns.Add("csv_investigator", System.Type.GetType("System.String")).DefaultValue = ""
            mdtMemTran.Columns.Add("memcategoryunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtMemTran.Columns.Add("members_category", System.Type.GetType("System.String")).DefaultValue = ""
            mdtMemTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtMemTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtMemTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtMemTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtMemTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtMemTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""

            mdtMemTran.Columns.Add("members", GetType(String)).DefaultValue = ""
            mdtMemTran.Columns.Add("department", GetType(String)).DefaultValue = ""
            mdtMemTran.Columns.Add("company", GetType(String)).DefaultValue = ""
            mdtMemTran.Columns.Add("contactno", GetType(String)).DefaultValue = ""
            mdtMemTran.Columns.Add("emailid", GetType(String)).DefaultValue = ""

            mdtMemTran.Columns.Add("ex_name", GetType(String)).DefaultValue = ""
            mdtMemTran.Columns.Add("ex_company", GetType(String)).DefaultValue = ""
            mdtMemTran.Columns.Add("ex_department", GetType(String)).DefaultValue = ""
            mdtMemTran.Columns.Add("ex_contactno", GetType(String)).DefaultValue = ""
            mdtMemTran.Columns.Add("ex_email", GetType(String)).DefaultValue = ""
            mdtMemTran.Columns.Add("employeeunkid", GetType(Int32)).DefaultValue = 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Constructor; Module Name: " & mstrModuleName)
        End Try
    End Sub

#End Region

    Private Sub GetMemTran(ByVal mdtEffectiveDate As Date)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If mobjDataOp IsNot Nothing Then
            objDataOperation = mobjDataOp
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            'strQ = "SELECT " & _
            '            "   hrdiscipline_members_tran.disciplinememberstranunkid " & _
            '            ",  hrdiscipline_members_tran.disciplineproceedingmasterunkid " & _
            '            ",  hrdiscipline_proceeding_master.disciplinefiletranunkid " & _
            '            ",  hrdiscipline_members_tran.committeetranunkid" & _
            '            ",  hrdiscipline_committee.committeemasterunkid " & _
            '            ",  cfcommon_master.name AS committee " & _
            '            ",  ISNULL(MemberName.csv_investigator, '') AS csv_investigator " & _
            '            ",  hrdiscipline_committee.mcategoryunkid AS memcategoryunkid " & _
            '            ",  cm.name AS members_category " & _
            '            ",  hrdiscipline_members_tran.isvoid " & _
            '            ",  hrdiscipline_members_tran.voiduserunkid " & _
            '            ",  hrdiscipline_members_tran.voiddatetime " & _
            '            ",  hrdiscipline_members_tran.voidreason " & _
            '            ",  '' AS AUD " & _
            '            ",  '' AS GUID " & _
            '       "FROM hrdiscipline_members_tran " & _
            '            "   LEFT JOIN hrdiscipline_proceeding_master ON hrdiscipline_proceeding_master.disciplineproceedingmasterunkid = hrdiscipline_members_tran.disciplineproceedingmasterunkid " & _
            '            "   LEFT JOIN hrdiscipline_committee ON hrdiscipline_committee.committeetranunkid = hrdiscipline_members_tran.committeetranunkid " & _
            '            "   LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdiscipline_committee.committeemasterunkid " & _
            '            "   LEFT JOIN cfcommon_master AS cm ON cm.masterunkid = hrdiscipline_committee.mcategoryunkid " & _
            '            "   LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrdiscipline_committee.employeeunkid " & _
            '            "   LEFT JOIN ( " & _
            '            "               SELECT DISTINCT " & _
            '            "                   A.committeemasterunkid " & _
            '            "                   ,( SELECT " & _
            '            "                        STUFF(( SELECT " & _
            '            "                                   ', ' + " & _
            '            "                                   CASE WHEN hrdiscipline_committee.employeeunkid <= 0 THEN hrdiscipline_committee.ex_name " & _
            '            "                                        WHEN hrdiscipline_committee.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') " & _
            '            "                                   END " & _
            '            "                                FROM hrdiscipline_committee " & _
            '            "                                LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrdiscipline_committee.employeeunkid " & _
            '            "                                WHERE A.committeemasterunkid = hrdiscipline_committee.committeemasterunkid " & _
            '            "                                FOR XML PATH ('') " & _
            '            "                               ), 1, 1, '' " & _
            '            "                              ) " & _
            '            "                    ) AS csv_investigator " & _
            '            "               FROM hrdiscipline_committee AS A " & _
            '            "             ) AS MemberName ON MemberName.committeemasterunkid = hrdiscipline_committee.committeemasterunkid " & _
            '       "WHERE hrdiscipline_members_tran.disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid "

            strQ = "SELECT " & _
                     "disciplinememberstranunkid " & _
                    ",hrdiscipline_proceeding_master.disciplineproceedingmasterunkid " & _
                    ",disciplinefiletranunkid " & _
                    ",ISNULL(hrdiscipline_committee.committeetranunkid,0) AS committeetranunkid " & _
                    ", CASE WHEN ISNULL(committeemasterunkid,0) <= 0 THEN  ISNULL(hrdiscipline_members_tran.committeemstunkid,0) ELSE ISNULL(committeemasterunkid,0) END committeemasterunkid " & _
                    ",ISNULL(cfcommon_master.name,'') AS committee " & _
                    ",'' AS csv_investigator " & _
                    ",ISNULL(cm.masterunkid,0) AS memcategoryunkid " & _
                    ",ISNULL(cm.name,'') AS members_category " & _
                    ",hrdiscipline_members_tran.isvoid " & _
                    ",hrdiscipline_members_tran.voiduserunkid " & _
                    ",hrdiscipline_members_tran.voiddatetime " & _
                    ",hrdiscipline_members_tran.voidreason " & _
                    ",'' AS AUD " & _
                    ",'' AS GUID " & _
                    ",ISNULL(CASE WHEN hrdiscipline_members_tran.committeetranunkid > 0 THEN " & _
                                    "CASE WHEN hrdiscipline_committee.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') " & _
                                         "WHEN hrdiscipline_committee.employeeunkid <= 0 THEN hrdiscipline_committee.ex_name " & _
                                    "END " & _
                                 "WHEN hrdiscipline_members_tran.committeetranunkid <= 0 THEN " & _
                                         "CASE WHEN hrdiscipline_members_tran.employeeunkid > 0 THEN ISNULL(ExEmp.firstname,'')+' '+ISNULL(ExEmp.surname,'') " & _
                                         "ELSE hrdiscipline_members_tran.ex_name END " & _
                     "END,'') AS members " & _
                     ",ISNULL(CASE WHEN hrdiscipline_members_tran.committeetranunkid > 0 THEN " & _
                                    "CASE WHEN hrdiscipline_committee.employeeunkid > 0 THEN ISNULL(hrdepartment_master.name,'') " & _
                                         "WHEN hrdiscipline_committee.employeeunkid <= 0 THEN hrdiscipline_committee.ex_department " & _
                                    "END " & _
                                 "WHEN hrdiscipline_members_tran.committeetranunkid <= 0 THEN " & _
                                         "CASE WHEN hrdiscipline_members_tran.employeeunkid > 0 THEN ISNULL(ExEmpDt.name,'') " & _
                                         "ELSE hrdiscipline_members_tran.ex_department END " & _
                " " & _
                     "END,'') AS department " & _
                    ",ISNULL(CASE WHEN hrdiscipline_members_tran.committeetranunkid > 0 THEN " & _
                                    "CASE WHEN hrdiscipline_committee.employeeunkid > 0 THEN cfcompany_master.name " & _
                                         "WHEN hrdiscipline_committee.employeeunkid <= 0 THEN hrdiscipline_committee.ex_company " & _
                                    "END " & _
                                 "WHEN hrdiscipline_members_tran.committeetranunkid <= 0 THEN " & _
                                         "CASE WHEN hrdiscipline_members_tran.employeeunkid > 0 THEN ExEmpCo.name " & _
                                         "ELSE hrdiscipline_members_tran.ex_company END " & _
                " " & _
                     "END,'') AS company " & _
                    ",ISNULL(CASE WHEN hrdiscipline_members_tran.committeetranunkid > 0 THEN " & _
                                    "CASE WHEN hrdiscipline_committee.employeeunkid > 0 THEN hremployee_master.present_tel_no " & _
                                         "WHEN hrdiscipline_committee.employeeunkid <= 0 THEN hrdiscipline_committee.ex_contactno " & _
                                    "END " & _
                                 "WHEN hrdiscipline_members_tran.committeetranunkid <= 0 THEN " & _
                                         "CASE WHEN hrdiscipline_members_tran.employeeunkid > 0 THEN ExEmp.present_tel_no " & _
                                         "ELSE hrdiscipline_members_tran.ex_contactno END " & _
                     "END,'') AS contactno " & _
                    ",ISNULL(CASE WHEN hrdiscipline_members_tran.committeetranunkid > 0 THEN " & _
                                    "CASE WHEN hrdiscipline_committee.employeeunkid > 0 THEN hremployee_master.email " & _
                                         "WHEN hrdiscipline_committee.employeeunkid <= 0 THEN hrdiscipline_committee.ex_email " & _
                                    "END " & _
                                 "WHEN hrdiscipline_members_tran.committeetranunkid <= 0 THEN " & _
                                         "CASE WHEN hrdiscipline_members_tran.employeeunkid > 0 THEN ExEmp.email " & _
                                         "ELSE hrdiscipline_members_tran.ex_email END " & _
                     "END,'') AS emailid " & _
                    ",CASE WHEN hrdiscipline_members_tran.committeetranunkid > 0 THEN hrdiscipline_committee.employeeunkid " & _
                      "ELSE ISNULL(hrdiscipline_members_tran.employeeunkid,0) END AS employeeunkid " & _
                    " FROM hrdiscipline_members_tran " & _
                    "LEFT JOIN hrdiscipline_proceeding_master ON hrdiscipline_proceeding_master.disciplineproceedingmasterunkid = hrdiscipline_members_tran.disciplineproceedingmasterunkid " & _
                    "LEFT JOIN hrdiscipline_committee ON hrdiscipline_committee.committeetranunkid = hrdiscipline_members_tran.committeetranunkid " & _
                    "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdiscipline_committee.committeemasterunkid " & _
                    "LEFT JOIN cfcommon_master AS cm ON cm.masterunkid = hrdiscipline_committee.mcategoryunkid " & _
                    "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrdiscipline_committee.employeeunkid " & _
                    "LEFT JOIN hrmsConfiguration..cfcompany_master ON cfcompany_master.companyunkid = hremployee_master.companyunkid " & _
                    "LEFT JOIN hremployee_master AS ExEmp ON ExEmp.employeeunkid = hrdiscipline_members_tran.employeeunkid " & _
                    "LEFT JOIN hrmsConfiguration..cfcompany_master AS ExEmpCo ON ExEmpCo.companyunkid = ExEmp.companyunkid " & _
                    "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                             "hremployee_transfer_tran.employeeunkid " & _
                            ",hremployee_transfer_tran.departmentunkid " & _
                            ",ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                        "FROM hremployee_transfer_tran " & _
                        "WHERE hremployee_transfer_tran.isvoid = 0 AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(mdtEffectiveDate) & "' " & _
                    ") AS A ON A.employeeunkid = hremployee_master.employeeunkid AND A.rno = 1 " & _
                    "LEFT JOIN hrdepartment_master ON A.departmentunkid = hrdepartment_master.departmentunkid " & _
                     "LEFT JOIN " & _
                    "( " & _
                        "SELECT " & _
                             "hremployee_transfer_tran.employeeunkid " & _
                            ",hremployee_transfer_tran.departmentunkid " & _
                            ",ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                        "FROM hremployee_transfer_tran " & _
                        "WHERE hremployee_transfer_tran.isvoid = 0 AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) <= '" & eZeeDate.convertDate(mdtEffectiveDate) & "' " & _
                    ") AS B ON B.employeeunkid = ExEmp.employeeunkid AND B.rno = 1 " & _
                    "LEFT JOIN hrdepartment_master As ExEmpDt ON B.departmentunkid = ExEmpDt.departmentunkid " & _
                    " WHERE hrdiscipline_members_tran.disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid AND hrdiscipline_members_tran.isvoid = 0  "


            'Pinkal (19-Dec-2020) -- Enhancement  -  Working on Discipline module for NMB.[ ", CASE WHEN ISNULL(committeemasterunkid,0) <= 0 THEN  ISNULL(hrdiscipline_members_tran.committeemstunkid,0) ELSE ISNULL(committeemasterunkid,0) END AS committeemasterunkid " & _]


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@disciplineproceedingmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplineProceedingMasterunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Dim csv_investigator = From row In dsList.Tables("List").AsEnumerable() Group row By id = row.Field(Of Integer)("committeemasterunkid") Into Group Select csv = String.Join(", ", From p In Group Select p.Field(Of String)("investigator"))
            '",  CASE WHEN hrdiscipline_committee.employeeunkid <=0 THEN hrdiscipline_committee.ex_name " & _
            '            "        WHEN hrdiscipline_committee.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') " & _
            '            "   END AS investigator " & _

            mdtMemTran.Rows.Clear()
            For Each drow As DataRow In dsList.Tables(0).Rows
                mdtMemTran.ImportRow(drow)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetMemTran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    Public Function InsertUpdateDeleteMembersTran(ByVal objDataOperation As clsDataOperation, ByVal intUserId As Integer, _
                                                  ByVal dtCurrentDate As DateTime, ByVal blnIsFromOpenCharge_ApproveDisApprove_Screen As Boolean, _
                                                  ByVal intCompanyUnkId As Integer, _
                                                  ByVal dtAttachment As DataTable, _
                                                  Optional ByVal strNotificationContent As String = "") As Boolean
        'Hemant (22 Jan 2021) -- [dtAttachment]
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            For Each dRow As DataRow In mdtMemTran.Rows
                objDataOperation.ClearParameters()
                With dRow
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                If CInt(.Item("disciplinefiletranunkid")) = mintDisciplineFileTranunkid Then

                                    strQ = "INSERT INTO hrdiscipline_members_tran ( " & _
                                                "   disciplineproceedingmasterunkid " & _
                                                ",  committeetranunkid " & _
                                                ",  ex_name " & _
                                                ",  ex_company " & _
                                                ",  ex_department " & _
                                                ",  ex_contactno " & _
                                                ",  ex_email " & _
                                                ",  employeeunkid " & _
                                                ",  isvoid " & _
                                                ",  voiduserunkid " & _
                                                ",  voiddatetime " & _
                                                ",  voidreason " & _
                                                ", committeemstunkid " & _
                                           ") VALUES ( " & _
                                                "   @disciplineproceedingmasterunkid " & _
                                                ",  @committeetranunkid " & _
                                                ",  @ex_name " & _
                                                ",  @ex_company " & _
                                                ",  @ex_department " & _
                                                ",  @ex_contactno " & _
                                                ",  @ex_email " & _
                                                ",  @employeeunkid " & _
                                                ",  @isvoid " & _
                                                ",  @voiduserunkid " & _
                                                ",  @voiddatetime " & _
                                                ",  @voidreason " & _
                                                ",  @committeemstunkid " & _
                                           "); SELECT @@identity "


                                    'Pinkal (19-Dec-2020) --Enhancement  -  Working on Discipline module for NMB.[", committeemstunkid " & _]

                                    objDataOperation.ClearParameters()
                                    objDataOperation.AddParameter("@disciplineproceedingmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(mintDisciplineProceedingMasterunkid > 0, mintDisciplineProceedingMasterunkid, .Item("disciplineproceedingmasterunkid")))
                                    objDataOperation.AddParameter("@committeetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeetranunkid"))
                                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                    If .Item("voiddatetime").ToString <> Nothing Then
                                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                    Else
                                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                    End If
                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                    objDataOperation.AddParameter("@ex_name", SqlDbType.NVarChar, 500, .Item("ex_name"))
                                    objDataOperation.AddParameter("@ex_company", SqlDbType.NVarChar, 500, .Item("ex_company"))
                                    objDataOperation.AddParameter("@ex_department", SqlDbType.NVarChar, 500, .Item("ex_department"))
                                    objDataOperation.AddParameter("@ex_contactno", SqlDbType.NVarChar, 500, .Item("ex_contactno"))
                                    objDataOperation.AddParameter("@ex_email", SqlDbType.NVarChar, 500, .Item("ex_email"))
                                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))


                                    'Pinkal (19-Dec-2020) -- Start
                                    'Enhancement  -  Working on Discipline module for NMB.
                                    objDataOperation.AddParameter("@committeemstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeemasterunkid"))
                                    'Pinkal (19-Dec-2020) -- End


                                    dsList = objDataOperation.ExecQuery(strQ, "List")

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    mintDisciplineMembersTranunkid = dsList.Tables(0).Rows(0).Item(0)

                                    If InsertATMembersTran(objDataOperation, intUserId, dtCurrentDate, dRow, enAuditType.ADD) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    If blnIsFromOpenCharge_ApproveDisApprove_Screen = True Then
                                        'Sohail (30 Nov 2017) -- Start
                                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                        'Call SendMailToMembers(mintDisciplineFileUnkid, CStr(.Item("members")), CStr(.Item("emailid")), strNotificationContent, intUserId)
                                        'Hemant (22 Jan 2021) -- Start
                                        'Enhancement #OLD-275 - NMB Discipline Open Case Notification
                                        'Call SendMailToMembers(mintDisciplineFileUnkid, CStr(.Item("members")), CStr(.Item("emailid")), strNotificationContent, intUserId, intCompanyUnkId)
                                        Dim strAttachedFiles As String = String.Empty
                                        If dtAttachment IsNot Nothing AndAlso dtAttachment.Rows.Count > 0 Then
                                            strAttachedFiles = String.Join(",", dtAttachment.AsEnumerable().Select(Function(x) x.Field(Of String)("orgfilepath")).ToArray())
                                        End If
                                        Call SendMailToMembers(mintDisciplineFileUnkid, CStr(.Item("members")), CStr(.Item("emailid")), strNotificationContent, intUserId, intCompanyUnkId, strAttachedFiles)
                                        'Hemant (22 Jan 2021) -- End

                                        'Sohail (30 Nov 2017) -- End
                                    End If

                                End If

                            Case "U"

                                strQ = "UPDATE hrdiscipline_members_tran SET " & _
                                           " disciplineproceedingmasterunkid = @disciplineproceedingmasterunkid " & _
                                           ",committeetranunkid = @committeetranunkid " & _
                                           ",isvoid = @isvoid " & _
                                           ",voiduserunkid = @voiduserunkid " & _
                                           ",voiddatetime = @voiddatetime " & _
                                           ",voidreason = @voidreason " & _
                                           ",ex_name = @ex_name " & _
                                           ",ex_company = @ex_company " & _
                                           ",ex_department = @ex_department " & _
                                           ",ex_contactno = @ex_contactno " & _
                                           ",ex_email = @ex_email " & _
                                           ",employeeunkid = @employeeunkid " & _
                                           ",committeemstunkid = @committeemstunkid " & _
                                           " WHERE disciplinememberstranunkid = @disciplinememberstranunkid "


                                'Pinkal (19-Dec-2020) -- Enhancement  -  Working on Discipline module for NMB.[",committeemstunkid = @committeemstunkid " & _]

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@disciplinememberstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("disciplinememberstranunkid"))
                                objDataOperation.AddParameter("@disciplineproceedingmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("disciplineproceedingmasterunkid"))
                                objDataOperation.AddParameter("@committeetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeetranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If .Item("voiddatetime").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                objDataOperation.AddParameter("@ex_name", SqlDbType.NVarChar, 500, .Item("ex_name"))
                                objDataOperation.AddParameter("@ex_company", SqlDbType.NVarChar, 500, .Item("ex_company"))
                                objDataOperation.AddParameter("@ex_department", SqlDbType.NVarChar, 500, .Item("ex_department"))
                                objDataOperation.AddParameter("@ex_contactno", SqlDbType.NVarChar, 500, .Item("ex_contactno"))
                                objDataOperation.AddParameter("@ex_email", SqlDbType.NVarChar, 500, .Item("ex_email"))
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))

                                'Pinkal (19-Dec-2020) -- Start
                                'Enhancement  -  Working on Discipline module for NMB.
                                objDataOperation.AddParameter("@committeemstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeemasterunkid"))
                                'Pinkal (19-Dec-2020) -- End


                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If InsertATMembersTran(objDataOperation, intUserId, dtCurrentDate, dRow, enAuditType.EDIT) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"
                                strQ = "UPDATE hrdiscipline_members_tran SET " & _
                                            "   isvoid = @isvoid " & _
                                            ",  voiduserunkid = @voiduserunkid " & _
                                            ",  voiddatetime = @voiddatetime " & _
                                            ",  voidreason = @voidreason " & _
                                       "WHERE disciplinememberstranunkid =@disciplinememberstranunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@disciplinememberstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("disciplinememberstranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                If .Item("voiddatetime").ToString <> Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If InsertATMembersTran(objDataOperation, intUserId, dtCurrentDate, dRow, enAuditType.DELETE) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If
                End With
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDeleteMembersTran; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    Private Function InsertATMembersTran(ByVal objDataOperation As clsDataOperation, ByVal intUserId As Integer, ByVal dtCurrentDate As DateTime, _
                                            ByVal dtRow As DataRow, ByVal eAudit As enAuditType) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Try
            With dtRow
                StrQ = "INSERT INTO athrdiscipline_members_tran ( " & _
                            "   disciplinememberstranunkid " & _
                            ",  disciplineproceedingmasterunkid " & _
                            ",  committeetranunkid " & _
                            ",  ex_name " & _
                            ",  ex_company " & _
                            ",  ex_department " & _
                            ",  ex_contactno " & _
                            ",  ex_email " & _
                            ",  employeeunkid " & _
                            ",  audittype " & _
                            ",  audituserunkid " & _
                            ",  auditdatetime " & _
                            ",  ip " & _
                            ",  machine_name " & _
                            ",  form_name " & _
                            ",  module_name1 " & _
                            ",  module_name2 " & _
                            ",  module_name3 " & _
                            ",  module_name4 " & _
                            ",  module_name5 " & _
                            ",  isweb " & _
                            ",  committeemstunkid " & _
                       ") VALUES ( " & _
                            "   @disciplinememberstranunkid " & _
                            ",  @disciplineproceedingmasterunkid " & _
                            ",  @committeetranunkid " & _
                            ",  @ex_name " & _
                            ",  @ex_company " & _
                            ",  @ex_department " & _
                            ",  @ex_contactno " & _
                            ",  @ex_email " & _
                            ",  @employeeunkid " & _
                            ",  @audittype " & _
                            ",  @audituserunkid " & _
                            ",  @auditdatetime " & _
                            ",  @ip " & _
                            ",  @machine_name " & _
                            ",  @form_name " & _
                            ",  @module_name1 " & _
                            ",  @module_name2 " & _
                            ",  @module_name3 " & _
                            ",  @module_name4 " & _
                            ",  @module_name5 " & _
                            ",  @isweb " & _
                            ",  @committeemstunkid " & _
                       ") "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@disciplinememberstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(mintDisciplineMembersTranunkid <= 0, .Item("disciplinememberstranunkid"), mintDisciplineMembersTranunkid))
                objDataOperation.AddParameter("@disciplineproceedingmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(mintDisciplineProceedingMasterunkid <= 0, .Item("disciplineproceedingmasterunkid"), mintDisciplineProceedingMasterunkid))
                objDataOperation.AddParameter("@committeetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeetranunkid"))

                objDataOperation.AddParameter("@ex_name", SqlDbType.NVarChar, 500, .Item("ex_name"))
                objDataOperation.AddParameter("@ex_company", SqlDbType.NVarChar, 500, .Item("ex_company"))
                objDataOperation.AddParameter("@ex_department", SqlDbType.NVarChar, 500, .Item("ex_department"))
                objDataOperation.AddParameter("@ex_contactno", SqlDbType.NVarChar, 500, .Item("ex_contactno"))
                objDataOperation.AddParameter("@ex_email", SqlDbType.NVarChar, 500, .Item("ex_email"))
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))

                'Pinkal (19-Dec-2020) -- Start
                'Enhancement  -  Working on Discipline module for NMB.
                objDataOperation.AddParameter("@committeemstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("committeemasterunkid"))
                'Pinkal (19-Dec-2020) -- End

            End With
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAudit)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDate)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATMembersTran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
        Return True
    End Function

    'S.SANDEEP [24 MAY 2016] -- Start
    'Email Notification
    Public Function SendMailToMembers(ByVal intDisciplineFileUnkid As Integer, ByVal strMember As String, ByVal strEmail As String, _
                                      ByVal strNotificationContent As String, ByVal intUserId As Integer, ByVal intCompanyUnkId As Integer, _
                                      ByVal strFilesAttached As String) As Boolean
        'Hemant (22 Jan 2021) -- [strFilesAttached]
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Try
            Dim strMessage As String = String.Empty
            Dim strBuilder As New StringBuilder
            Dim objNetConn As New clsNetConnectivity
            Dim objMail As New clsSendMail
            Dim objUser As New clsUserAddEdit
            Dim objDiscFileMaster As New clsDiscipline_file_master
            'Hemant (22 Jan 2021) -- Start
            'Enhancement #OLD-275 - NMB Discipline Open Case Notification
            Dim objGrp As New clsGroup_Master
            objGrp._Groupunkid = 1
            'Hemant (22 Jan 2021) -- End

            If objNetConn._Conected = False Then Exit Function

            objUser._Userunkid = intUserId
            objDiscFileMaster._Disciplinefileunkid = intDisciplineFileUnkid

            objMail._Subject = Language.getMessage(mstrModuleName, 2, "Charge Opening Notification") & " " & Language.getMessage(mstrModuleName, 3, " :  Reference No - ") & objDiscFileMaster._Reference_No

            strMessage = "<HTML><BODY>"


            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'strMessage &= Language.getMessage(mstrModuleName, 4, "Dear") & " " & "&nbsp;<b>" & getTitleCase(strMember) & "</b>" & " ,<BR></BR>"
            strMessage &= Language.getMessage(mstrModuleName, 4, "Dear") & " " & "&nbsp;<b>" & getTitleCase(strMember) & "</b>" & " ,<BR></BR>"
            'Gajanan [27-Mar-2019] -- End



            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 5, "This is to infom you that, Following charge count opening details.") & "<BR></BR><BR></BR>"
            strMessage &= Language.getMessage(mstrModuleName, 5, "This is to infom you that, Following charge count opening details.") & "<BR></BR><BR></BR>"
            'Gajanan [27-Mar-2019] -- End


            strMessage &= objDiscFileMaster.getChargeDetailForEmail(intDisciplineFileUnkid)
            'Hemant (22 Jan 2021) -- Start
            'Enhancement #OLD-275 - NMB Discipline Open Case Notification
            If objGrp._Groupname.ToUpper <> "NMB PLC" Then
                'Hemant (22 Jan 2021) -- End
            strMessage &= "<b>" & Language.getMessage(mstrModuleName, 6, "Charges Count :") & "</b><BR></BR>"

            strMessage &= strNotificationContent
            End If 'Hemant (22 Jan 2021)

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
            'Gajanan [27-Mar-2019] -- End


            strMessage &= "</BODY></HTML>"

            Dim strSuccess As String = ""
            objMail._Message = strMessage
            objMail._ToEmail = strEmail
            If mintLoginTypeId <= 0 Then mintLoginTypeId = enLogin_Mode.DESKTOP
            If mstrWebFormName.Trim.Length > 0 Then
                objMail._Form_Name = mstrWebFormName
                objMail._WebClientIP = mstrWebClientIP
                objMail._WebHostName = mstrWebHostName
            End If
            objMail._LogEmployeeUnkid = mintLoginEmployeeunkid
            objMail._OperationModeId = mintLoginTypeId
            objMail._UserUnkid = intUserId
            objMail._SenderAddress = IIf(objUser._Email = "", objUser._Username, objUser._Email)
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.DISCIPLINE_MGT
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'strSuccess = objMail.SendMail()
            'Hemant (22 Jan 2021) -- Start
            'Enhancement #OLD-275 - NMB Discipline Open Case Notification
            objMail._AttachedFiles = strFilesAttached
            'Hemant (22 Jan 2021) -- End
            strSuccess = objMail.SendMail(intCompanyUnkId)
            'Sohail (30 Nov 2017) -- End

            If strSuccess.Trim.Length <= 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendMailToMembers; Module Name: " & mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [24 MAY 2016] -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "WEB")
            Language.setMessage(mstrModuleName, 2, "Charge Opening Notification")
            Language.setMessage(mstrModuleName, 3, " :  Reference No -")
            Language.setMessage(mstrModuleName, 4, "Dear")
            Language.setMessage(mstrModuleName, 5, "This is to infom you that, Following charge count opening details.")
            Language.setMessage(mstrModuleName, 6, "Charges Count :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
