﻿'************************************************************************************************************************************
'Class Name : clsDiscipline_Status.vb
'Purpose    :
'Date       :14/09/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsDiscipline_Status
    Private Shared ReadOnly mstrModuleName As String = "clsDiscipline_Status"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " ENUM "

    Public Enum enDisciplineStatusType
        EXTERNAL = 1
        INTERNAL = 2
        BOTH = 3
        ALL = 4
    End Enum

#End Region

#Region " Private variables "
    Private mintDisciplinestatusunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty

    'Anjan (20 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private mintLevel As Integer = 0
    'Anjan (20 Mar 2012)-End 

    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintStatusmodeid As Integer
    'S.SANDEEP [ 20 APRIL 2012 ] -- END

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disciplinestatusunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Disciplinestatusunkid() As Integer
        Get
            Return mintDisciplinestatusunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplinestatusunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = value
        End Set
    End Property

    Public Property _Level() As Integer
        Get
            Return mintLevel
        End Get
        Set(ByVal value As Integer)
            mintLevel = value
        End Set
    End Property

    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set statusmodeid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Statusmodeid() As Integer
        Get
            Return mintStatusmodeid
        End Get
        Set(ByVal value As Integer)
            mintStatusmodeid = Value
        End Set
    End Property

    'S.SANDEEP [ 20 APRIL 2012 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Anjan (20 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'StrQ = "SELECT " & _
            '  "  disciplinestatusunkid " & _
            '  ", code " & _
            '  ", name " & _
            '  ", description " & _
            '  ", isactive " & _
            '  ", name1 " & _
            '  ", name2 " & _
            ' "FROM hrdisciplinestatus_master " & _
            ' "WHERE disciplinestatusunkid = @disciplinestatusunkid "
            strQ = "SELECT " & _
              "  disciplinestatusunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2 " & _
             ", statuslevel " & _
              ", statusmodeid  " & _
             "FROM hrdisciplinestatus_master " & _
             "WHERE disciplinestatusunkid = @disciplinestatusunkid "
            'Anjan (20 Mar 2012)-End 

            'S.SANDEEP [ 20 APRIL 2012 statusmodeid  ] -- START -- END




            objDataOperation.AddParameter("@disciplinestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinestatusunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintDisciplinestatusunkid = CInt(dtRow.Item("disciplinestatusunkid"))
                mstrCode = dtRow.Item("code").ToString
                mstrName = dtRow.Item("name").ToString
                mstrDescription = dtRow.Item("description").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrName1 = dtRow.Item("name1").ToString
                mstrName2 = dtRow.Item("name2").ToString
                'Anjan (20 Mar 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                mintLevel = dtRow.Item("statuslevel")
                'Anjan (20 Mar 2012)-End 

                'S.SANDEEP [ 20 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mintStatusmodeid = CInt(dtRow.Item("statusmodeid"))
                'S.SANDEEP [ 20 APRIL 2012 ] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Anjan (20 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'StrQ = "SELECT " & _
            '  "  disciplinestatusunkid " & _
            '  ", code " & _
            '  ", name " & _
            '  ", description " & _
            '  ", isactive " & _
            '  ", name1 " & _
            '  ", name2 " & _
            ' "FROM hrdisciplinestatus_master "
            strQ = "SELECT " & _
              "  disciplinestatusunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2 " & _
              ", statuslevel " & _
              ", statusmodeid " & _
              ", CASE WHEN statusmodeid = 1 THEN @EXTERNAL " & _
              "       WHEN statusmodeid = 2 THEN @INTERNAL " & _
              "       WHEN statusmodeid = 3 THEN @BOTH " & _
              "  END AS StatusType " & _
             "FROM hrdisciplinestatus_master "
            'Anjan (20 Mar 2012)-End 

            'S.SANDEEP [ 20 APRIL 2012 isexternal ] -- START -- END

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            objDataOperation.AddParameter("@EXTERNAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "External"))
            objDataOperation.AddParameter("@INTERNAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Internal"))
            objDataOperation.AddParameter("@BOTH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Both"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrdisciplinestatus_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCode, , , , mintStatusmodeid) Then   'S.SANDEEP [ 20 APRIL 2012 ]
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
            Return False
        End If

        If isExist(, mstrName, , , mintStatusmodeid) Then   'S.SANDEEP [ 20 APRIL 2012 ]
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Status is already defined. Please define new Status.")
            Return False
        End If


        'Anjan (20 Mar 2012)-Start
        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
        ' Andrew and Rutta wants to allow same level for different status
        'If isExist(, , , mintLevel) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 4, "This Level of Status is already defined. Please define new Level.")
        '    Return False
        'End If
        'Anjan (20 Mar 2012)-End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)

            'Anjan (20 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            objDataOperation.AddParameter("@statuslevel", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevel)
            'Anjan (20 Mar 2012)-End 



            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@statusmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusmodeid.ToString)
            'S.SANDEEP [ 20 APRIL 2012 ] -- END



            strQ = "INSERT INTO hrdisciplinestatus_master ( " & _
                              "  code " & _
                              ", name " & _
                              ", description " & _
                              ", isactive " & _
                              ", name1 " & _
                              ", name2" & _
                              ", statuslevel " & _
                              ", statusmodeid " & _
                      ") VALUES (" & _
                              "  @code " & _
                              ", @name " & _
                              ", @description " & _
                              ", @isactive " & _
                              ", @name1 " & _
                              ", @name2" & _
                              ", @statuslevel " & _
                              ", @statusmodeid" & _
                      "); SELECT @@identity"


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintDisciplinestatusunkid = dsList.Tables(0).Rows(0).Item(0)

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrdisciplinestatus_master", "disciplinestatusunkid", mintDisciplinestatusunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrdisciplinestatus_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrCode, , mintDisciplinestatusunkid, , mintStatusmodeid) Then   'S.SANDEEP [ 20 APRIL 2012 ]
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
            Return False
        End If

        If isExist(, mstrName, mintDisciplinestatusunkid, , mintStatusmodeid) Then   'S.SANDEEP [ 20 APRIL 2012 ]
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Status is already defined. Please define new Status.")
            Return False
        End If


        'Anjan (20 Mar 2012)-Start
        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
        'Andrew and Rutta wants to allow same level for different status
        'If isExist(, , mintDisciplinestatusunkid, mintLevel) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 5, "This Level for Status is already defined. Please define new Level.")
        '    Return False
        'End If
        'Anjan (20 Mar 2012)-End 


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@disciplinestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinestatusunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)

            'Anjan (20 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            objDataOperation.AddParameter("@statuslevel", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevel)
            'Anjan (20 Mar 2012)-End 

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@statusmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusmodeid.ToString)
            'S.SANDEEP [ 20 APRIL 2012 ] -- END



            'Anjan (20 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'strQ = "UPDATE hrdisciplinestatus_master SET " & _
            '              "  code = @code" & _
            '              ", name = @name" & _
            '              ", description = @description" & _
            '              ", isactive = @isactive" & _
            '              ", name1 = @name1" & _
            '              ", name2 = @name2 " & _
            '            "WHERE disciplinestatusunkid = @disciplinestatusunkid "
            strQ = "UPDATE hrdisciplinestatus_master SET " & _
                          "  code = @code" & _
                          ", name = @name" & _
                          ", description = @description" & _
                          ", isactive = @isactive" & _
                          ", name1 = @name1" & _
                          ", name2 = @name2 " & _
                         ", statuslevel = @statuslevel " & _
                   ", statusmodeid = @statusmodeid " & _
                        "WHERE disciplinestatusunkid = @disciplinestatusunkid "
            'Anjan (20 Mar 2012)-End 



            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrdisciplinestatus_master", mintDisciplinestatusunkid, "disciplinestatusunkid", 2) Then

                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrdisciplinestatus_master", "disciplinestatusunkid", mintDisciplinestatusunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrdisciplinestatus_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'strQ = " UPDATE hrdisciplinestatus_master " & _
            '           " SET isactive = 1 " & _
            '"WHERE disciplinestatusunkid = @disciplinestatusunkid "

            strQ = " UPDATE hrdisciplinestatus_master " & _
                       " SET isactive = 0 " & _
                    "WHERE disciplinestatusunkid = @disciplinestatusunkid "

            'Pinkal (12-Oct-2011) -- End

            objDataOperation.AddParameter("@disciplinestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrdisciplinestatus_master", "disciplinestatusunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   " TABLE_NAME AS TableName  " & _
                   "FROM INFORMATION_SCHEMA.COLUMNS " & _
                   "WHERE COLUMN_NAME IN ('disciplinestatusunkid')"

            objDataOperation.AddParameter("@disciplinestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            strQ = ""
            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("TableName") = "hrdisciplinestatus_master" Then Continue For

                strQ = "SELECT disciplinestatusunkid FROM " & dtRow.Item("TableName").ToString & " WHERE disciplinestatusunkid = @disciplinestatusunkid "
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            If blnIsUsed = False Then
                '****CHECKING FOR THE STATUS AS EMPLOYEEUNKID FOR HRDISCIPLINE_ANALYSIS****
                strQ = "SELECT status FROM hrdiscipline_analysis WHERE status = @disciplinestatusunkid "
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                End If
            End If


            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1, Optional ByVal intLevel As Integer = 0, Optional ByVal intStatusModeId As Integer = 0) As Boolean 'Anjan (20 Mar 2012) 'S.SANDEEP [ 20 APRIL 2012 intStatusModeId ]        
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Anjan (20 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'strQ = "SELECT " & _
            '              "  disciplinestatusunkid " & _
            '              ", code " & _
            '              ", name " & _
            '              ", description " & _
            '              ", isactive " & _
            '              ", name1 " & _
            '              ", name2 " & _
            '           "FROM hrdisciplinestatus_master " & _
            '           "WHERE isactive = 1 "
            strQ = "SELECT " & _
                          "  disciplinestatusunkid " & _
                          ", code " & _
                          ", name " & _
                          ", description " & _
                          ", isactive " & _
                          ", name1 " & _
                          ", name2 " & _
                          ", statuslevel  " & _
                       "FROM hrdisciplinestatus_master " & _
                       "WHERE isactive = 1 "

            'Anjan (20 Mar 2012)-End 


            If strName.Length > 0 Then
                strQ &= " AND name = @name "
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If strCode.Length > 0 Then
                strQ &= "AND code = @code "
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If


            'Anjan (20 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            If intLevel > 0 Then
                strQ &= "AND statuslevel = @statuslevel "
                objDataOperation.AddParameter("@statuslevel", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevel)
            End If
            'Anjan (20 Mar 2012)-End

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If intStatusModeId > 0 Then
                strQ &= "AND statusmodeid = @statusmodeid "
                objDataOperation.AddParameter("@statusmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusModeId)
            End If
            'S.SANDEEP [ 20 APRIL 2012 ] -- END

            If intUnkid > 0 Then
                strQ &= " AND disciplinestatusunkid <> @disciplinestatusunkid"
                objDataOperation.AddParameter("@disciplinestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Get The List for Combo </purpose>
    Public Function getComboList(Optional ByVal strList As String = "List", _
                                 Optional ByVal blnFlag As Boolean = False, _
                                 Optional ByVal eStatusMode As enDisciplineStatusType = enDisciplineStatusType.ALL) As DataSet
        'Public Function getComboList(Optional ByVal strList As String = "List", _
        '                         Optional ByVal blnFlag As Boolean = False, _
        '                         Optional ByVal eStatusMode As enDisciplineStatusType = enDisciplineStatusType.BOTH) As DataSet
        'S.SANDEEP [ 15 MAY 2012 ] -- START  -- END

        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If blnFlag = True Then
            '    StrQ = " SELECT 0 As Id,@Select As name UNION "
            'End If
            'StrQ &= "SELECT disciplinestatusunkid As Id, name As name FROM hrdisciplinestatus_master WHERE isactive = 1 "

            If blnFlag = True Then
                StrQ = " SELECT 0 As Id,@Select As name, 0 AS statusmodeid UNION "
            End If
            StrQ &= "SELECT disciplinestatusunkid As Id, name As name, statusmodeid AS statusmodeid FROM hrdisciplinestatus_master WHERE isactive = 1 "
            'S.SANDEEP [ 20 APRIL 2012 ] -- END

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'S.SANDEEP [ 15 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ &= " AND statusmodeid IN (" & enDisciplineStatusType.BOTH & "," & eStatusMode & ") "
            If eStatusMode <> enDisciplineStatusType.ALL Then
            StrQ &= " AND statusmodeid IN (" & enDisciplineStatusType.BOTH & "," & eStatusMode & ") "
            End If
            'S.SANDEEP [ 15 MAY 2012 ] -- END

            'S.SANDEEP [ 20 APRIL 2012 ] -- END

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        End Try
    End Function

    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function getStatusTypeComboList(Optional ByVal StrList As String = "List", Optional ByVal blnFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            If blnFlag = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If

            StrQ &= "SELECT " & enDisciplineStatusType.EXTERNAL & " AS Id, @EXTERNAL AS Name UNION " & _
                    "SELECT " & enDisciplineStatusType.INTERNAL & " AS Id, @INTERNAL AS Name UNION " & _
                    "SELECT " & enDisciplineStatusType.BOTH & " AS Id, @BOTH AS Name "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            objDataOperation.AddParameter("@EXTERNAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "External"))
            objDataOperation.AddParameter("@INTERNAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Internal"))
            objDataOperation.AddParameter("@BOTH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Both"))


            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        End Try
    End Function
    'S.SANDEEP [ 20 APRIL 2012 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
			Language.setMessage(mstrModuleName, 2, "This Status is already defined. Please define new Status.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 5, "External")
			Language.setMessage(mstrModuleName, 6, "Internal")
			Language.setMessage(mstrModuleName, 7, "Both")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class