﻿'************************************************************************************************************************************
'Class Name : clsdiscipline_analysis.vb
'Purpose    :
'Date       :31/07/2010
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsdiscipline_analysis
    Private Shared ReadOnly mstrModuleName As String = "clsdiscipline_analysis"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintDisciplineanalysisunkid As Integer
    Private mdtDiscipline_Tran_Date As Date
    Private mintDisciplinetypeunkid As Integer
    Private mintDisciplinaryactionunkid As Integer
    Private mdtAction_Start_Date As Date
    Private mdtAction_End_Date As Date
    Private mintInvestigatorunkid As Integer
    Private mintInvolved_Personunkid As Integer
    Private mintAgainst_Personunkid As Integer
    Private mstrIncident_Caused As String = String.Empty
    Private mintStatus As Integer
    Private mstrRemark As String = String.Empty
    Private mstrResolution_Steps As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disciplineanalysisunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Disciplineanalysisunkid() As Integer
        Get
            Return mintDisciplineanalysisunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplineanalysisunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set discipline_tran_date
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Discipline_Tran_Date() As Date
        Get
            Return mdtDiscipline_Tran_Date
        End Get
        Set(ByVal value As Date)
            mdtDiscipline_Tran_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disciplinetypeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Disciplinetypeunkid() As Integer
        Get
            Return mintDisciplinetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplinetypeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disciplinaryactionunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Disciplinaryactionunkid() As Integer
        Get
            Return mintDisciplinaryactionunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplinaryactionunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set action_start_date
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Action_Start_Date() As Date
        Get
            Return mdtAction_Start_Date
        End Get
        Set(ByVal value As Date)
            mdtAction_Start_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set action_end_date
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Action_End_Date() As Date
        Get
            Return mdtAction_End_Date
        End Get
        Set(ByVal value As Date)
            mdtAction_End_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set investigatorunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Investigatorunkid() As Integer
        Get
            Return mintInvestigatorunkid
        End Get
        Set(ByVal value As Integer)
            mintInvestigatorunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set involved_personunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Involved_Personunkid() As Integer
        Get
            Return mintInvolved_Personunkid
        End Get
        Set(ByVal value As Integer)
            mintInvolved_Personunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set against_personunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Against_Personunkid() As Integer
        Get
            Return mintAgainst_Personunkid
        End Get
        Set(ByVal value As Integer)
            mintAgainst_Personunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set incident_caused
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Incident_Caused() As String
        Get
            Return mstrIncident_Caused
        End Get
        Set(ByVal value As String)
            mstrIncident_Caused = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set status
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Status() As Integer
        Get
            Return mintStatus
        End Get
        Set(ByVal value As Integer)
            mintStatus = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set resolution_steps
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Resolution_Steps() As String
        Get
            Return mstrResolution_Steps
        End Get
        Set(ByVal value As String)
            mstrResolution_Steps = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isVoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  disciplineanalysisunkid " & _
              ", discipline_tran_date " & _
              ", disciplinetypeunkid " & _
              ", disciplinaryactionunkid " & _
              ", action_start_date " & _
              ", action_end_date " & _
              ", investigatorunkid " & _
              ", involved_personunkid " & _
              ", against_personunkid " & _
              ", incident_caused " & _
              ", status " & _
              ", remark " & _
              ", resolution_steps " & _
              ", Userunkid " & _
              ", isVoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrdiscipline_analysis " & _
             "WHERE disciplineanalysisunkid = @disciplineanalysisunkid "

            objDataOperation.AddParameter("@disciplineanalysisunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintDisciplineanalysisUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintdisciplineanalysisunkid = CInt(dtRow.Item("disciplineanalysisunkid"))
                If dtRow.Item("discipline_tran_date") <> Nothing Then mdtDiscipline_Tran_Date = dtRow.Item("discipline_tran_date")
                mintDisciplinetypeunkid = CInt(dtRow.Item("disciplinetypeunkid"))
                mintDisciplinaryactionunkid = CInt(dtRow.Item("disciplinaryactionunkid"))
                If dtRow.Item("action_start_date") <> Nothing Then mdtAction_Start_Date = dtRow.Item("action_start_date")
                'If dtRow.Item("action_end_date") <> Nothing Then mdtAction_End_Date = dtRow.Item("action_end_date")
                If IsDBNull(dtRow.Item("action_end_date")) Then
                    mdtAction_End_Date = Nothing
                Else
                    mdtAction_End_Date = dtRow.Item("action_end_date")
                End If
                mintInvestigatorunkid = CInt(dtRow.Item("investigatorunkid"))
                mintInvolved_Personunkid = CInt(dtRow.Item("involved_personunkid"))
                mintAgainst_Personunkid = CInt(dtRow.Item("against_personunkid"))
                mstrIncident_Caused = dtRow.Item("incident_caused").ToString
                mintStatus = CInt(dtRow.Item("status"))
                mstrRemark = dtRow.Item("remark").ToString
                mstrResolution_Steps = dtRow.Item("resolution_steps").ToString
                mintUserunkid = CInt(dtRow.Item("Userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isVoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If dtRow.Item("voiddatetime").ToString <> Nothing Then mdtVoiddatetime = dtRow.Item("voiddatetime")
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'S.SANDEEP [ 29 JUNE 2011 ] -- START
            'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
            'strQ = "SELECT " & _
            '       "  disciplineanalysisunkid " & _
            '       ", ISNULL(invest.firstname+' '+invest.surname+' '+invest.othername,'')AS Investigator " & _
            '       ", ISNULL(involved.firstname+' '+involved.surname+' '+involved.othername,'')AS InvolvedPerson " & _
            '       ", convert(char(8),discipline_tran_date,112) as  discipline_tran_date" & _
            '       ", hrdiscipline_analysis.disciplinetypeunkid " & _
            '       ", hrdisciplinetype_master.name " & _
            '       ", hrdisciplinetype_master.severity " & _
            '       ", hrdiscipline_analysis.disciplinaryactionunkid " & _
            '       ", hraction_reason_master.code " & _
            '       ", action_start_date " & _
            '       ", action_end_date " & _
            '       ", investigatorunkid " & _
            '       ", involved_personunkid " & _
            '       ", against_personunkid " & _
            '       ", incident_caused " & _
            '       ", hrdiscipline_analysis.remark " & _
            '       ", resolution_steps " & _
            '       ", hrdiscipline_analysis.Userunkid " & _
            '       ", hrdiscipline_analysis.isVoid " & _
            '       ", hrdiscipline_analysis.voiduserunkid " & _
            '       ", hrdiscipline_analysis.voiddatetime " & _
            '       ", hrdiscipline_analysis.voidreason " & _
            '       ", CASE WHEN status = 1 then @Complete when status = 2 then @Incomplete END AS statusname " & _
            '       ", status " & _
            '       " FROM hrdiscipline_analysis " & _
            '       "  LEFT JOIN hrdisciplinetype_master on hrdisciplinetype_master.disciplinetypeunkid = hrdiscipline_analysis.disciplinetypeunkid " & _
            '       "  LEFT JOIN hraction_reason_master on hraction_reason_master.actionreasonunkid = hrdiscipline_analysis.disciplinaryactionunkid AND isreason = 0 " & _
            '       "  LEFT JOIN hremployee_master AS involved ON hrdiscipline_analysis.involved_personunkid = involved.employeeunkid " & _
            '       "  LEFT JOIN hremployee_master AS invest ON hrdiscipline_analysis.investigatorunkid = invest.employeeunkid "

            'If blnOnlyActive Then
            '    strQ &= " WHERE hrdiscipline_analysis.isvoid = 0 "
            'End If

            
            strQ = "SELECT " & _
              "  disciplineanalysisunkid " & _
                    "	,ISNULL(invest.firstname+' '+invest.surname+' '+invest.othername,'')AS Investigator " & _
                    "	,ISNULL(involved.firstname+' '+involved.surname+' '+involved.othername,'')AS InvolvedPerson " & _
              ", convert(char(8),discipline_tran_date,112) as  discipline_tran_date" & _
              ", hrdiscipline_analysis.disciplinetypeunkid " & _
              ", hrdisciplinetype_master.name " & _
              ", hrdisciplinetype_master.severity " & _
              ", hrdiscipline_analysis.disciplinaryactionunkid " & _
              ", hraction_reason_master.code " & _
              ", action_start_date " & _
              ", action_end_date " & _
              ", investigatorunkid " & _
              ", involved_personunkid " & _
              ", against_personunkid " & _
              ", incident_caused " & _
              "	,hrdiscipline_analysis.remark " & _
              ", resolution_steps " & _
              ", hrdiscipline_analysis.Userunkid " & _
              ", hrdiscipline_analysis.isVoid " & _
              ", hrdiscipline_analysis.voiduserunkid " & _
              ", hrdiscipline_analysis.voiddatetime " & _
              ", hrdiscipline_analysis.voidreason " & _
                    "   ,CASE WHEN status = 1 then @Complete when status = 2 then @Incomplete END AS statusname " & _
                    "   ,status " & _
                   " ,hraction_reason_master.reason_action " & _
              " FROM hrdiscipline_analysis " & _
              " LEFT JOIN hrdisciplinetype_master on hrdisciplinetype_master.disciplinetypeunkid = hrdiscipline_analysis.disciplinetypeunkid " & _
                    "	LEFT JOIN hraction_reason_master on hraction_reason_master.actionreasonunkid = hrdiscipline_analysis.disciplinaryactionunkid AND isreason = 0 " & _
                    "	LEFT JOIN hremployee_master AS involved ON hrdiscipline_analysis.involved_personunkid = involved.employeeunkid " & _
                   "  LEFT JOIN hremployee_master AS invest ON hrdiscipline_analysis.investigatorunkid = invest.employeeunkid " & _
                   "WHERE 1 = 1 "

            
            If blnOnlyActive Then
                strQ &= " AND hrdiscipline_analysis.isvoid = 0 "
            End If

            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                'Sohail (06 Jan 2012) -- Start
                'TRA - ENHANCEMENT
                'strQ &= " AND ISNULL(involved.isactive,0) = 1 AND ISNULL(invest.isactive,0) = 1 "
                strQ &= " AND CONVERT(CHAR(8),involved.appointeddate,112) <= @enddate " & _
                           " AND ISNULL(CONVERT(CHAR(8),involved.termination_from_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),involved.termination_to_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),involved.empl_enddate,112), @startdate) >= @startdate " 'Sohail (12 May 2012) - [empl_enddate]

               strQ &= " AND CONVERT(CHAR(8),invest.appointeddate,112) <= @enddate " & _
                           " AND ISNULL(CONVERT(CHAR(8),invest.termination_from_date,112),@startdate) >= @startdate " & _
                            " AND ISNULL(CONVERT(CHAR(8),invest.termination_to_date,112),@startdate) >= @startdate " & _
                            " AND ISNULL(CONVERT(CHAR(8),invest.empl_enddate,112), @startdate) >= @startdate " 'Sohail (12 May 2012) - [empl_enddate]

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                'Sohail (06 Jan 2012) -- End

            End If
            'S.SANDEEP [ 29 JUNE 2011 ] -- END 

            




            If UserAccessLevel._AccessLevel.Length > 0 Then
                If blnOnlyActive Then
                    strQ &= " AND involved.jobunkid IN (" & UserAccessLevel._AccessLevel & ") AND invest.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
                Else
                    strQ &= " WHERE involved.jobunkid IN (" & UserAccessLevel._AccessLevel & ") AND invest.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
                End If
            End If



            objDataOperation.AddParameter("@Complete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Complete"))
            objDataOperation.AddParameter("@Incomplete", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Incomplete"))


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrdiscipline_analysis) </purpose>
    '''Shani(24-Aug-2015) -- Start
    '''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function Insert(ByVal strDatabaseName) As Boolean
    Public Function Insert(ByVal strDatabaseName As String, _
                           ByVal intYearId As Integer, _
                           ByVal intCompanyId As Integer, _
                           ByVal dtTotal_Active_Employee_AsOnDate As Date, _
                           ByVal blnIsArutiDemo As String, _
                           ByVal intTotal_Active_Employee_ForAllCompany As Integer, _
                           ByVal intNoOfEmployees As Integer, ByVal blnDonotAttendanceinSeconds As Boolean, _
                           ByVal strUserAccessMode As String, _
                           ByVal xPeriodStart As DateTime, _
                           ByVal xPeriodEnd As DateTime, _
                           ByVal xOnlyApproved As Boolean, _
                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                           ByVal blnAllowToApproveEarningDeduction As Boolean, _
                           ByVal dtCurrentDateTime As DateTime, _
                           Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                           Optional ByVal strFilerString As String = "" _
                           ) As Boolean
        'Shani(24-Aug-2015) -- End
        'Sohail (21 Aug 2015) - [strDatabaseName]
        If isExist(mdtDiscipline_Tran_Date, mintDisciplinetypeunkid, mintInvestigatorunkid, mintInvolved_Personunkid, mintAgainst_Personunkid, mstrIncident_Caused) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Discipline Analysis is already defined. Please define new Discipline Analysis.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@discipline_tran_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDiscipline_Tran_Date)
            objDataOperation.AddParameter("@disciplinetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinetypeunkid.ToString)
            objDataOperation.AddParameter("@disciplinaryactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinaryactionunkid.ToString)
            objDataOperation.AddParameter("@action_start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAction_Start_Date)
            If mdtAction_End_Date = Nothing Then
                objDataOperation.AddParameter("@action_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@action_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAction_End_Date)
            End If
            objDataOperation.AddParameter("@investigatorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInvestigatorunkid.ToString)
            objDataOperation.AddParameter("@involved_personunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInvolved_Personunkid.ToString)
            objDataOperation.AddParameter("@against_personunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAgainst_Personunkid.ToString)
            objDataOperation.AddParameter("@incident_caused", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIncident_Caused.ToString)
            objDataOperation.AddParameter("@status", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatus.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@resolution_steps", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrResolution_Steps.ToString)
            objDataOperation.AddParameter("@Userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isVoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO hrdiscipline_analysis ( " & _
              "  discipline_tran_date " & _
              ", disciplinetypeunkid " & _
              ", disciplinaryactionunkid " & _
              ", action_start_date " & _
              ", action_end_date " & _
              ", investigatorunkid " & _
              ", involved_personunkid " & _
              ", against_personunkid " & _
              ", incident_caused " & _
              ", status " & _
              ", remark " & _
              ", resolution_steps " & _
              ", Userunkid " & _
              ", isVoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @discipline_tran_date " & _
              ", @disciplinetypeunkid " & _
              ", @disciplinaryactionunkid " & _
              ", @action_start_date " & _
              ", @action_end_date " & _
              ", @investigatorunkid " & _
              ", @involved_personunkid " & _
              ", @against_personunkid " & _
              ", @incident_caused " & _
              ", @status " & _
              ", @remark " & _
              ", @resolution_steps " & _
              ", @Userunkid " & _
              ", @isVoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintDisciplineanalysisunkid = dsList.Tables(0).Rows(0).Item(0)


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrdiscipline_analysis", "disciplineanalysisunkid", mintDisciplineanalysisunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If SetEmployeeData() = False Then

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If SetEmployeeData(strDatabaseName) = False Then
            If SetEmployeeData(strDatabaseName, intYearId, intCompanyId, dtTotal_Active_Employee_AsOnDate, _
                               blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, _
                               intNoOfEmployees, blnDonotAttendanceinSeconds, strUserAccessMode, _
                               xPeriodStart, xPeriodEnd, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, _
                               dtCurrentDateTime, blnApplyUserAccessFilter, strFilerString) = False Then
                'Shani(24-Aug-2015) -- End

                'Sohail (21 Aug 2015) -- End
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'If SetEmployeeData() = False Then
            '    objDataOperation.ReleaseTransaction(False)
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'Else
            '    objDataOperation.ReleaseTransaction(True)
            'End If
            objDataOperation.ReleaseTransaction(True)
            Return True

            'Pinkal (12-Oct-2011) -- End

        Catch ex As Exception

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrdiscipline_analysis) </purpose>
    '''Shani(24-Aug-2015) -- Start
    '''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '''Public Function Update(ByVal strDatabaseName As String) As Boolean
    Public Function Update(ByVal strDatabaseName As String, _
                           ByVal intYearId As Integer, _
                           ByVal intCompanyId As Integer, _
                           ByVal dtTotal_Active_Employee_AsOnDate As Date, _
                           ByVal blnIsArutiDemo As String, _
                           ByVal intTotal_Active_Employee_ForAllCompany As Integer, _
                           ByVal intNoOfEmployees As Integer, _
                           ByVal blnDonotAttendanceinSeconds As Boolean, _
                           ByVal strUserAccessMode As String, _
                           ByVal xPeriodStart As DateTime, _
                           ByVal xPeriodEnd As DateTime, _
                           ByVal xOnlyApproved As Boolean, _
                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                           ByVal blnAllowToApproveEarningDeduction As Boolean, _
                           ByVal dtCurrentDateTime As DateTime, _
                           Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                           Optional ByVal strFilerString As String = "" _
                           ) As Boolean
        'Shani(24-Aug-2015) -- End

        'Sohail (21 Aug 2015) - [strDatabaseName]
        If isExist(mdtDiscipline_Tran_Date, mintDisciplinetypeunkid, mintInvestigatorunkid, mintInvolved_Personunkid, mintAgainst_Personunkid, mstrIncident_Caused, mintDisciplineanalysisunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Discipline Analysis is already defined. Please define new Discipline Analysis.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@disciplineanalysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplineanalysisunkid.ToString)
            objDataOperation.AddParameter("@discipline_tran_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDiscipline_Tran_Date.ToString)
            objDataOperation.AddParameter("@disciplinetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinetypeunkid.ToString)
            objDataOperation.AddParameter("@disciplinaryactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinaryactionunkid.ToString)
            objDataOperation.AddParameter("@action_start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAction_Start_Date)
            If mdtAction_End_Date = Nothing Then
                objDataOperation.AddParameter("@action_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@action_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAction_End_Date)
            End If
            objDataOperation.AddParameter("@investigatorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInvestigatorunkid.ToString)
            objDataOperation.AddParameter("@involved_personunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInvolved_Personunkid.ToString)
            objDataOperation.AddParameter("@against_personunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAgainst_Personunkid.ToString)
            objDataOperation.AddParameter("@incident_caused", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIncident_Caused.ToString)
            objDataOperation.AddParameter("@status", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatus.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@resolution_steps", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrResolution_Steps.ToString)
            objDataOperation.AddParameter("@Userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isVoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE hrdiscipline_analysis SET " & _
              "  discipline_tran_date = @discipline_tran_date" & _
              ", disciplinetypeunkid = @disciplinetypeunkid" & _
              ", disciplinaryactionunkid = @disciplinaryactionunkid" & _
              ", action_start_date = @action_start_date" & _
              ", action_end_date = @action_end_date" & _
              ", investigatorunkid = @investigatorunkid" & _
              ", involved_personunkid = @involved_personunkid" & _
              ", against_personunkid = @against_personunkid" & _
              ", incident_caused = @incident_caused" & _
              ", status = @status" & _
              ", remark = @remark" & _
              ", resolution_steps = @resolution_steps" & _
              ", Userunkid = @Userunkid" & _
              ", isVoid = @isVoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE disciplineanalysisunkid = @disciplineanalysisunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrdiscipline_analysis", mintDisciplineanalysisunkid, "disciplineanalysisunkid", 2) Then

                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrdiscipline_analysis", "disciplineanalysisunkid", mintDisciplineanalysisunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If


            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If SetEmployeeData() = False Then

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If SetEmployeeData(strDatabaseName) = False Then
            If SetEmployeeData(strDatabaseName, intYearId, intCompanyId, dtTotal_Active_Employee_AsOnDate, _
                               blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, blnDonotAttendanceinSeconds, _
                               strUserAccessMode, xPeriodStart, xPeriodEnd, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, _
                               dtCurrentDateTime, blnApplyUserAccessFilter, strFilerString) = False Then
                'Shani(24-Aug-2015) -- End

                'Sohail (21 Aug 2015) -- End
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'If SetEmployeeData() = False Then
            '    objDataOperation.ReleaseTransaction(False)
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'Else
            '    objDataOperation.ReleaseTransaction(True)
            'End If
            objDataOperation.ReleaseTransaction(True)

            'Pinkal (12-Oct-2011) -- End

            Return True

        Catch ex As Exception

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End

            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrdiscipline_analysis) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception



        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE hrdiscipline_analysis Set" & _
                        " isvoid = 1 " & _
                        ",voiddatetime = @voiddate " & _
                        ",voidreason = @voidreason " & _
                        ",voiduserunkid = @voiduserunkid " & _
                       " WHERE disciplineanalysisunkid = @disciplineanalysisunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voiddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@disciplineanalysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrdiscipline_analysis", "disciplineanalysisunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End

            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@disciplineanalysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal dtDisciplineDate As DateTime, ByVal intDisciplineTypeunkid As Integer, ByVal intInvenstigatorunkid As Integer, ByVal intPersonInvolvedunkid As Integer, _
                            ByVal intAgainstPersonunkid As Integer, ByVal strIncident As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  disciplineanalysisunkid " & _
              ", discipline_tran_date " & _
              ", disciplinetypeunkid " & _
              ", disciplinaryactionunkid " & _
              ", action_start_date " & _
              ", action_end_date " & _
              ", investigatorunkid " & _
              ", involved_personunkid " & _
              ", against_personunkid " & _
              ", incident_caused " & _
              ", status " & _
              ", remark " & _
              ", resolution_steps " & _
              ", Userunkid " & _
              ", isVoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrdiscipline_analysis " & _
             " WHERE discipline_tran_date = @discipline_tran_date " & _
             " AND disciplinetypeunkid = @disciplinetypeunkid " & _
             " AND investigatorunkid = @investigatorunkid " & _
             " AND involved_personunkid = @involved_personunkid " & _
             " AND against_personunkid = @against_personunkid " & _
             " AND incident_caused = @incident_caused and isvoid = 0"

            If intUnkid > 0 Then
                strQ &= " AND disciplineanalysisunkid <> @disciplineanalysisunkid"
            End If

            objDataOperation.AddParameter("@discipline_tran_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtDisciplineDate)
            objDataOperation.AddParameter("@disciplinetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDisciplineTypeunkid)
            objDataOperation.AddParameter("@investigatorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intInvenstigatorunkid)
            objDataOperation.AddParameter("@involved_personunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPersonInvolvedunkid)
            objDataOperation.AddParameter("@against_personunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAgainstPersonunkid)
            objDataOperation.AddParameter("@incident_caused", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strIncident)
            objDataOperation.AddParameter("@disciplineanalysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function SetEmployeeData(ByVal strDatabaseName As String) As Boolean
    Private Function SetEmployeeData(ByVal strDatabaseName As String, _
                                     ByVal intYearId As Integer, _
                                     ByVal intCompanyId As Integer, _
                                     ByVal dtTotal_Active_Employee_AsOnDate As Date, _
                                     ByVal blnIsArutiDemo As String, _
                                     ByVal intTotal_Active_Employee_ForAllCompany As Integer, _
                                     ByVal intNoOfEmployees As Integer, _
                                     ByVal blnDonotAttendanceinSeconds As Boolean, _
                                     ByVal strUserAccessMode As String, _
                                     ByVal xPeriodStart As DateTime, _
                                     ByVal xPeriodEnd As DateTime, _
                                     ByVal xOnlyApproved As Boolean, _
                                     ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                     ByVal blnAllowToApproveEarningDeduction As Boolean, _
                                     ByVal dtCurrentDateTime As DateTime, _
                                     Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                                     Optional ByVal strFilerString As String = "" _
                                     ) As Boolean
        'Shani(24-Aug-2015) -- End

        'Sohail (21 Aug 2015) - [strDatabaseName]
        Dim blnEmpFlag As Boolean = False
        Try
            mstrMessage = ""
            Dim objEmp As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = mintInvolved_Personunkid
            objEmp._Employeeunkid(xPeriodEnd) = mintInvolved_Personunkid
            'S.SANDEEP [04 JUN 2015] -- END

            Select Case mintDisciplinaryactionunkid
                Case 1     'Suspension
                    objEmp._Suspende_From_Date = mdtAction_Start_Date
                    objEmp._Suspende_To_Date = mdtAction_End_Date
                Case 2     'Terminated
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objEmp.IsEmpTerminated(mdtAction_Start_Date, mintInvolved_Personunkid) = True Then
                    If objEmp.IsEmpTerminated(mdtAction_Start_Date, mintInvolved_Personunkid, strDatabaseName) = True Then
                        'Sohail (21 Aug 2015) -- End
                        objEmp._Termination_From_Date = mdtAction_Start_Date
                    Else
                        blnEmpFlag = False
                        mstrMessage = objEmp._Message
                        Return blnEmpFlag
                    End If

            End Select

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'blnEmpFlag = objEmp.Update()
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'blnEmpFlag = objEmp.Update(ConfigParameter._Object._CurrentDateAndTime.Date, _
            '                           ConfigParameter._Object._IsArutiDemo, _
            '                           Company._Object._Total_Active_Employee_ForAllCompany, _
            '                           ConfigParameter._Object._NoOfEmployees, _
            '                           mintUserunkid)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'blnEmpFlag = objEmp.Update(strDatabaseName, _
            '                           ConfigParameter._Object._CurrentDateAndTime.Date, _
            '                           ConfigParameter._Object._IsArutiDemo, _
            '                           Company._Object._Total_Active_Employee_ForAllCompany, _
            '                           ConfigParameter._Object._NoOfEmployees, _
            '                           mintUserunkid)
            blnEmpFlag = objEmp.Update(strDatabaseName, intYearId, intCompanyId, dtTotal_Active_Employee_AsOnDate, _
                                       blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, _
                                       intNoOfEmployees, mintUserunkid, blnDonotAttendanceinSeconds, strUserAccessMode, xPeriodStart, xPeriodEnd, _
                                       xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateTime, , , , , , , , , , blnApplyUserAccessFilter, strFilerString)
            'Shani(24-Aug-2015) -- End

            'Sohail (21 Aug 2015) -- End
            'S.SANDEEP [04 JUN 2015] -- END

            Return blnEmpFlag

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetEmployeeData", mstrModuleName)
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Discipline Analysis is already defined. Please define new Discipline Analysis.")
            Language.setMessage(mstrModuleName, 2, "Complete")
            Language.setMessage(mstrModuleName, 3, "Incomplete")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class