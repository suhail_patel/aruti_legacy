﻿'************************************************************************************************************************************
'Class Name : clsemployee_dayoff_Tran.vb
'Purpose    :
'Date       :31/10/2013
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsemployee_dayoff_Tran
    Private Const mstrModuleName = "clsemployee_dayoff_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintEmpdayofftranunkid As Integer
    Private mdtDayoffdate As Date
    Private mintEmployeeunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empdayofftranunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Empdayofftranunkid() As Integer
        Get
            Return mintEmpdayofftranunkid
        End Get
        Set(ByVal value As Integer)
            mintEmpdayofftranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dayoffdate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Dayoffdate() As Date
        Get
            Return mdtDayoffdate
        End Get
        Set(ByVal value As Date)
            mdtDayoffdate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  empdayofftranunkid " & _
              ", dayoffdate " & _
              ", employeeunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hremployee_dayoff_tran " & _
             "WHERE empdayofftranunkid = @empdayofftranunkid "

            objDataOperation.AddParameter("@empdayofftranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintEmpdayoffTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintempdayofftranunkid = CInt(dtRow.Item("empdayofftranunkid"))
                mdtdayoffdate = dtRow.Item("dayoffdate")
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtvoiddatetime = dtRow.Item("voiddatetime")
                mstrvoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'Pinkal (17-Jan-2017) -- Start
    'Enhancement - Working on DAYOFF Error given by AMOS. 

    '''' <summary>
    '''' Modify By: Pinkal Jariwala
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, _
    '                         Optional ByVal mstrAdvanceFilter As String = "", _
    '                         Optional ByVal iEmployeeId As Integer = 0, _
    '                         Optional ByVal mDate1 As DateTime = Nothing, _
    '                         Optional ByVal mDate2 As DateTime = Nothing _
    '                         ) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation


    '    Try
    '        strQ = "SELECT " & _
    '               "     hremployee_dayoff_tran.empdayofftranunkid " & _
    '               "    ,CONVERT(CHAR(8),hremployee_dayoff_tran.dayoffdate,112) AS dayoffdate " & _
    '               "    ,hremployee_dayoff_tran.employeeunkid " & _
    '               "    ,hremployee_dayoff_tran.userunkid " & _
    '               "    ,hremployee_dayoff_tran.isvoid " & _
    '               "    ,hremployee_dayoff_tran.voiduserunkid " & _
    '               "    ,hremployee_dayoff_tran.voiddatetime " & _
    '               "    ,hremployee_dayoff_tran.voidreason " & _
    '               "    ,employeecode " & _
    '               "    ,firstname+' '+surname AS employeename " & _
    '               "FROM hremployee_dayoff_tran " & _
    '               "    JOIN hremployee_master ON hremployee_dayoff_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '               "WHERE isvoid = 0 "

    '        If mstrAdvanceFilter.Trim.Length > 0 Then
    '            strQ &= " AND " & mstrAdvanceFilter & " "
    '        End If

    '        If iEmployeeId > 0 Then
    '            strQ &= " AND hremployee_dayoff_tran.employeeunkid = '" & iEmployeeId & "' "
    '        End If

    '        If mDate1 <> Nothing AndAlso mDate2 <> Nothing Then
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_dayoff_tran.dayoffdate,112) BETWEEN '" & eZeeDate.convertDate(mDate1) & "' AND '" & eZeeDate.convertDate(mDate2) & "' "
    '        End If

    '        strQ &= "ORDER BY firstname+' '+surname, hremployee_dayoff_tran.dayoffdate "

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function


    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, _
                                       ByVal xDatabaseName As String, _
                                       ByVal xUserUnkid As Integer, _
                                       ByVal xYearUnkid As Integer, _
                                       ByVal xCompanyUnkid As Integer, _
                                       ByVal xUserModeSetting As String, _
                                       ByVal xEmployeeAsOnDate As Date, _
                             Optional ByVal mstrAdvanceFilter As String = "", _
                             Optional ByVal iEmployeeId As Integer = 0, _
                             Optional ByVal mDate1 As DateTime = Nothing, _
                             Optional ByVal mDate2 As DateTime = Nothing _
                             ) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation


        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xEmployeeAsOnDate.Date, xEmployeeAsOnDate.Date, , , xDatabaseName)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xEmployeeAsOnDate.Date, xDatabaseName)

        Try
            strQ = "SELECT " & _
                   "     hremployee_dayoff_tran.empdayofftranunkid " & _
                   "    ,CONVERT(CHAR(8),hremployee_dayoff_tran.dayoffdate,112) AS dayoffdate " & _
                   "    ,hremployee_dayoff_tran.employeeunkid " & _
                   "    ,hremployee_dayoff_tran.userunkid " & _
                   "    ,hremployee_dayoff_tran.isvoid " & _
                   "    ,hremployee_dayoff_tran.voiduserunkid " & _
                   "    ,hremployee_dayoff_tran.voiddatetime " & _
                   "    ,hremployee_dayoff_tran.voidreason " & _
                   "    ,ISNULL(hremployee_master.employeecode,'') AS  employeecode " & _
                   "    ,ISNULL(firstname,'') +' '+ ISNULL(surname,'')  AS employeename " & _
                   "FROM hremployee_dayoff_tran " & _
                   "    JOIN hremployee_master ON hremployee_dayoff_tran.employeeunkid = hremployee_master.employeeunkid "

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            strQ &= " WHERE isvoid = 0 "


            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry & " "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvanceFilter & " "
            End If

            If iEmployeeId > 0 Then
                strQ &= " AND hremployee_dayoff_tran.employeeunkid = '" & iEmployeeId & "' "
            End If

            If mDate1 <> Nothing AndAlso mDate2 <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),hremployee_dayoff_tran.dayoffdate,112) BETWEEN '" & eZeeDate.convertDate(mDate1) & "' AND '" & eZeeDate.convertDate(mDate2) & "' "
            End If

            strQ &= "ORDER BY ISNULL(firstname,'') +' '+ ISNULL(surname,''), hremployee_dayoff_tran.dayoffdate "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Pinkal (17-Jan-2017) -- End

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_dayoff_tran) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.BindTransaction()
            objDataOperation.AddParameter("@dayoffdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDayoffdate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO hremployee_dayoff_tran ( " & _
                      "  dayoffdate " & _
                      ", employeeunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @dayoffdate " & _
                      ", @employeeunkid " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintEmpdayofftranunkid = dsList.Tables(0).Rows(0).Item(0)


            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", mintEmployeeunkid, "hremployee_dayoff_tran", "empdayofftranunkid", mintEmpdayofftranunkid, 2, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_dayoff_tran) </purpose>
    Public Function Update() As Boolean

        If isExist(mdtDayoffdate, mintEmployeeunkid, mintEmpdayofftranunkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.BindTransaction()
            objDataOperation.AddParameter("@empdayofftranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpdayofftranunkid.ToString)
            objDataOperation.AddParameter("@dayoffdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDayoffdate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE hremployee_dayoff_tran SET " & _
                      "  dayoffdate = @dayoffdate" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                    "WHERE empdayofftranunkid = @empdayofftranunkid AND isvoid = 0 "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", mintEmployeeunkid, "hremployee_dayoff_tran", "empdayofftranunkid", mintEmpdayofftranunkid, 2, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployee_dayoff_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal intEmpId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.BindTransaction()

            strQ = "UPDATE hremployee_dayoff_tran SET " & _
                      " isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                    "WHERE empdayofftranunkid = @empdayofftranunkid "

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@empdayofftranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", intEmpId, "hremployee_dayoff_tran", "empdayofftranunkid", intUnkid, 2, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal iEmployeeId As Integer, ByVal iDate As DateTime) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT 1 FROM tnalogin_summary WHERE employeeunkid = '" & iEmployeeId & "' AND CONVERT(CHAR(8),login_date,112) = '" & eZeeDate.convertDate(iDate).ToString & "' AND isdayoff = 1 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal mdtDate As Date, ByVal intEmpId As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal objDataOperation As clsDataOperation = Nothing) As Boolean
        'Pinkal (25-Jan-2018) -- Bug 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.[, Optional ByVal objDataOperation As clsDataOperation = Nothing]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOperation Is Nothing Then
        objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  empdayofftranunkid " & _
                      ", dayoffdate " & _
                      ", employeeunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     "FROM hremployee_dayoff_tran " & _
                     "WHERE CONVERT(CHAR(8),dayoffdate,112) = @dayoffdate " & _
                     "AND employeeunkid = @employeeunkid AND isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND empdayofftranunkid <> @empdayofftranunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@dayoffdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDate))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@empdayofftranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Pinkal (25-Jan-2018) -- Start
            'Bug - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.
            'objDataOperation = Nothing
            'Pinkal (25-Jan-2018) -- End
        End Try
    End Function

End Class