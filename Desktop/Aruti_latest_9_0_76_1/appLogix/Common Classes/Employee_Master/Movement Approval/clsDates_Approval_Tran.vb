﻿'************************************************************************************************************************************
'Class Name : clsDates_Approval_Tran.vb
'Purpose    :
'Date       :31-Jul-2018
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clsDates_Approval_Tran
    Private Const mstrModuleName = "clsDates_Approval_Tran"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mstrTranguid As String = String.Empty
    Private mdtTransactiondate As Date
    Private mintMappingunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mblnIsfinal As Boolean
    Private mintStatusunkid As Integer
    Private mintDatestranunkid As Integer
    Private mdtEffectivedate As Date
    Private mintEmployeeunkid As Integer
    Private mintRehiretranunkid As Integer
    Private mdtDate1 As Date
    Private mdtDate2 As Date
    Private mintChangereasonunkid As Integer
    Private mintActionreasonunkid As Integer
    Private mintDatetypeunkid As Integer
    Private mblnIsexclude_Payroll As Boolean
    Private mblnIsconfirmed As Boolean
    Private mstrOtherreason As String = String.Empty
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintVoiduserunkid As Integer
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer
    Private mstrIp As String = String.Empty
    Private mstrHostname As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean
    Private mblnInsertNullTerminationDate As Boolean = False
    Private mblnIsProcessed As Boolean = False
    'S.SANDEEP |17-JAN-2019| -- START
    Private mintOperationTypeId As Integer = 0
    'S.SANDEEP |17-JAN-2019| -- END
    'Sohail (21 Oct 2019) -- Start
    'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
    Private mintLeaveissueunkid As Integer = 0
    'Sohail (21 Oct 2019) -- End

    'Pinkal (07-Mar-2020) -- Start
    'Enhancement - Changes Related to Payroll UAT for NMB.
    Private mdtActualDate As DateTime = Nothing
    'Pinkal (07-Mar-2020) -- End

    'Gajanan [18-May-2020] -- Start
    'Enhancement:Discipline Module Enhancement NMB
    Private mintDisciplineunkid As Integer = -1
    'Gajanan [18-May-2020] -- End
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranguid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Tranguid() As String
        Get
            Return mstrTranguid
        End Get
        Set(ByVal value As String)
            mstrTranguid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set datestranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Datestranunkid() As Integer
        Get
            Return mintDatestranunkid
        End Get
        Set(ByVal value As Integer)
            mintDatestranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set rehiretranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Rehiretranunkid() As Integer
        Get
            Return mintRehiretranunkid
        End Get
        Set(ByVal value As Integer)
            mintRehiretranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set date1
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Date1() As Date
        Get
            Return mdtDate1
        End Get
        Set(ByVal value As Date)
            mdtDate1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set date2
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Date2() As Date
        Get
            Return mdtDate2
        End Get
        Set(ByVal value As Date)
            mdtDate2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set changereasonunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Changereasonunkid() As Integer
        Get
            Return mintChangereasonunkid
        End Get
        Set(ByVal value As Integer)
            mintChangereasonunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actionreasonunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Actionreasonunkid() As Integer
        Get
            Return mintActionreasonunkid
        End Get
        Set(ByVal value As Integer)
            mintActionreasonunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set datetypeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Datetypeunkid() As Integer
        Get
            Return mintDatetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintDatetypeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isexclude_payroll
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isexclude_Payroll() As Boolean
        Get
            Return mblnIsexclude_Payroll
        End Get
        Set(ByVal value As Boolean)
            mblnIsexclude_Payroll = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isconfirmed
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isconfirmed() As Boolean
        Get
            Return mblnIsconfirmed
        End Get
        Set(ByVal value As Boolean)
            mblnIsconfirmed = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set otherreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Otherreason() As String
        Get
            Return mstrOtherreason
        End Get
        Set(ByVal value As String)
            mstrOtherreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hostname
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Hostname() As String
        Get
            Return mstrHostname
        End Get
        Set(ByVal value As String)
            mstrHostname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    Public WriteOnly Property _InsertNullTerminationDate() As Boolean
        Set(ByVal value As Boolean)
            mblnInsertNullTerminationDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mblnIsProcessed
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _IsProcessed() As Boolean
        Get
            Return mblnIsProcessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsProcessed = value
        End Set
    End Property


    'S.SANDEEP |17-JAN-2019| -- START
    Public Property _OperationTypeId() As Integer
        Get
            Return mintOperationTypeId
        End Get
        Set(ByVal value As Integer)
            mintOperationTypeId = value
        End Set
    End Property
    'S.SANDEEP |17-JAN-2019| -- END

    'Sohail (21 Oct 2019) -- Start
    'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
    Public Property _Leaveissueunkid() As Integer
        Get
            Return mintLeaveissueunkid
        End Get
        Set(ByVal value As Integer)
            mintLeaveissueunkid = value
        End Set
    End Property
    'Sohail (21 Oct 2019) -- End

    'Pinkal (07-Mar-2020) -- Start
    'Enhancement - Changes Related to Payroll UAT for NMB.

    ''' <summary>
    ''' Purpose: Get or Set ActualDate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ActualDate() As Date
        Get
            Return mdtActualDate
        End Get
        Set(ByVal value As Date)
            mdtActualDate = value
        End Set
    End Property

    'Pinkal (07-Mar-2020) -- End

    'Gajanan [18-May-2020] -- Start
    'Enhancement:Discipline Module Enhancement NMB
    Public Property _Disciplinefileunkid() As Integer
        Get
            Return mintDisciplineunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplineunkid = value
        End Set
    End Property
    'Gajanan [18-May-2020] -- End

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, _
                            ByVal eDateTran As enEmp_Dates_Transaction, _
                            ByVal xFinYearStartDate As Date, _
                            Optional ByVal xEmployeeUnkid As Integer = 0, Optional ByVal blnOnlyPending As Boolean = True) As DataSet 'S.SANDEEP [14-AUG-2018] -- START {Ref#244} [blnOnlyPending] -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mdtAppDate As String = ""
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                   "FROM hremployee_master " & _
                   "WHERE employeeunkid = '" & xEmployeeUnkid & "' "

            objDataOperation.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtAppDate = objDataOperation.GetParameterValue("@ADate")

            Dim xMasterType As Integer = 0
            Select Case eDateTran
                Case enEmp_Dates_Transaction.DT_PROBATION
                    xMasterType = clsCommon_Master.enCommonMaster.PROBATION
                Case enEmp_Dates_Transaction.DT_CONFIRMATION
                    xMasterType = clsCommon_Master.enCommonMaster.CONFIRMATION
                Case enEmp_Dates_Transaction.DT_SUSPENSION
                    xMasterType = clsCommon_Master.enCommonMaster.SUSPENSION
                Case enEmp_Dates_Transaction.DT_TERMINATION
                    xMasterType = clsCommon_Master.enCommonMaster.TERMINATION
                Case enEmp_Dates_Transaction.DT_REHIRE
                    xMasterType = clsCommon_Master.enCommonMaster.RE_HIRE
                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    xMasterType = clsCommon_Master.enCommonMaster.RETIREMENTS
                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enEmp_Dates_Transaction.DT_EXEMPTION
                    xMasterType = clsCommon_Master.enCommonMaster.EXEMPTION
                    'Sohail (21 Oct 2019) -- End
            End Select


            strQ = "SELECT " & _
            "  hremployee_dates_approval_tran.tranguid " & _
                   ", hremployee_dates_approval_tran.transactiondate " & _
                   ", hremployee_dates_approval_tran.mappingunkid " & _
                   ", hremployee_dates_approval_tran.remark " & _
                   ", hremployee_dates_approval_tran.isfinal " & _
                   ", hremployee_dates_approval_tran.statusunkid " & _
                   ", hremployee_dates_approval_tran.datestranunkid " & _
                   ", hremployee_dates_approval_tran.effectivedate " & _
                   ", hremployee_dates_approval_tran.employeeunkid " & _
                   ", hremployee_dates_approval_tran.rehiretranunkid " & _
                   ", hremployee_dates_approval_tran.date1 " & _
                   ", hremployee_dates_approval_tran.date2 " & _
                   ", hremployee_dates_approval_tran.changereasonunkid " & _
                   ", hremployee_dates_approval_tran.datetypeunkid " & _
                   ", hremployee_dates_approval_tran.statusunkid " & _
                   ", hremployee_dates_approval_tran.actionreasonunkid " & _
                   ", hremployee_dates_approval_tran.isexclude_payroll " & _
                   ", hremployee_dates_approval_tran.isconfirmed " & _
                   ", hremployee_dates_approval_tran.otherreason " & _
                   ", hremployee_dates_approval_tran.isvoid " & _
                   ", hremployee_dates_approval_tran.voiduserunkid " & _
                   ", hremployee_dates_approval_tran.voiddatetime " & _
                   ", hremployee_dates_approval_tran.voidreason " & _
                   ", hremployee_master.employeecode as ecode " & _
                   ", hremployee_dates_approval_tran.isvoid " & _
                   ", hremployee_dates_approval_tran.voiddatetime " & _
                   ", hremployee_dates_approval_tran.voidreason " & _
                   ", hremployee_dates_approval_tran.voiduserunkid " & _
                   ", hremployee_dates_approval_tran.audittype " & _
                   ", hremployee_dates_approval_tran.audituserunkid " & _
                   ", hremployee_dates_approval_tran.ip " & _
                   ", hremployee_dates_approval_tran.hostname " & _
                   ", hremployee_dates_approval_tran.form_name " & _
                   ", hremployee_dates_approval_tran.isweb " & _
                   ", hremployee_master.firstname+' '+ISNULL(hremployee_master.othername,'')+' '+hremployee_master.surname AS ename " & _
                   ", CONVERT(CHAR(8),hremployee_dates_approval_tran.effectivedate,112) AS edate " & _
                   ", CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS adate " & _
                   ", CONVERT(CHAR(8),hremployee_dates_approval_tran.date1,112) AS edate1 " & _
                   ", CONVERT(CHAR(8),hremployee_dates_approval_tran.date2,112) AS edate2 " & _
                   ", '' AS EffDate " & _
                   ", '' AS dDate1 " & _
                   ", '' AS dDate2 " & _
                   ", CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') WHEN hremployee_dates_approval_tran.leaveissueunkid > 0 THEN ISNULL(lvleavetype_master.leavename, '') ELSE ISNULL(cfcommon_master.name,'') END AS CReason " & _
                   ", CASE WHEN CONVERT(CHAR(8),hremployee_dates_approval_tran.date1,112) < '" & eZeeDate.convertDate(xFinYearStartDate).ToString() & "' THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS allowopr " & _
                   ", ISNULL(hremployee_dates_approval_tran.operationtypeid,0) AS operationtypeid " & _
                   ", CASE WHEN ISNULL(hremployee_dates_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeMovmentApproval.enOperationType.ADDED) & "' THEN @ADDED " & _
                   "       WHEN ISNULL(hremployee_dates_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeMovmentApproval.enOperationType.EDITED) & "' THEN @EDITED " & _
                   "       WHEN ISNULL(hremployee_dates_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeMovmentApproval.enOperationType.DELETED) & "' THEN @DELETED " & _
                   "  END AS OperationType " & _
                   ", CAST(0 AS BIT) AS isfromemployee " & _
                   ", ISNULL(hremployee_dates_approval_tran.leaveissueunkid, 0) AS leaveissueunkid " & _
                   ", CONVERT(CHAR(8),hremployee_dates_approval_tran.actualdate,112) AS acdate " & _
                   ", hremployee_dates_approval_tran.actualdate " & _
                   ", hremployee_dates_approval_tran.disciplinefileunkid " & _
             "FROM hremployee_dates_approval_tran " & _
                   "    JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_dates_approval_tran.employeeunkid " & _
                   "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_dates_approval_tran.changereasonunkid AND cfcommon_master.mastertype = " & xMasterType & _
                   "    LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = hremployee_dates_approval_tran.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                   "    LEFT JOIN lvleaveIssue_master ON lvleaveIssue_master.leaveissueunkid = hremployee_dates_approval_tran.leaveissueunkid " & _
                   "    LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
                   " WHERE hremployee_dates_approval_tran.datetypeunkid = " & eDateTran & " and hremployee_dates_approval_tran.isvoid = 0 AND hremployee_dates_approval_tran.datestranunkid >= 0 AND ISNULL(hremployee_dates_approval_tran.isprocessed,0) = 0 "


            'Pinkal (07-Mar-2020) -- Enhancement - Changes Related to Payroll UAT for NMB.[CONVERT(CHAR(8),hremployee_dates_approval_tran.actualdate,112) AS acdate , hremployee_dates_approval_tran.actualdate "]

            'Sohail (21 Oct 2019) - [leaveissueunkid, LEFT JOIN lvleaveIssue_master, LEFT JOIN lvleavetype_master]
            'S.SANDEEP [16 Jan 2016] -- START
            '", CASE WHEN CONVERT(CHAR(8),hremployee_dates_approval_tran.date1,112) < '" & eZeeDate.convertDate(xFinYearStartDate).ToString() & "' THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS allowopr " & _
            '------------ > ADDED
            'S.SANDEEP [16 Jan 2016] -- END


            If eDateTran = enEmp_Dates_Transaction.DT_TERMINATION Then
                strQ &= " AND hremployee_dates_approval_tran.date1 IS NOT NULL "
            End If

            If xEmployeeUnkid > 0 Then
            strQ &= " AND hremployee_dates_approval_tran.employeeunkid = '" & xEmployeeUnkid & "' "
            End If

            If mdtAppDate.Trim.Length > 0 Then
                strQ &= " AND CONVERT(CHAR(8),hremployee_dates_approval_tran.effectivedate,112) >= '" & mdtAppDate & "' "
            End If

            strQ &= " ORDER BY effectivedate DESC "
            'S.SANDEEP |17-JAN-2019| -- START


            objDataOperation.AddParameter("@ADDED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeMovmentApproval", 62, "Newly Added Information"))
            objDataOperation.AddParameter("@EDITED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeMovmentApproval", 63, "Information Edited"))
            objDataOperation.AddParameter("@DELETED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeMovmentApproval", 64, "Information Deleted"))
            'S.SANDEEP |17-JAN-2019| -- END
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [14-AUG-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            Dim strValue As String = String.Join(",", dsList.Tables(0).AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("employeeunkid")).[Select](Function(x) New With {Key .empid = x.Key, Key .empstat = x.[Select](Function(z) z.Field(Of Integer)("statusunkid")).Max}).Where(Function(x) x.empstat = 2 Or x.empstat = 3).Select(Function(x) x.empstat.ToString()).ToArray())
            'S.SANDEEP [09-AUG-2018] -- START {Added : x.empstat | Removed : x.empid} -- END
            If strValue.Trim.Length > 0 Then
                'S.SANDEEP [09-AUG-2018] -- START
                'Dim dtTbl As DataTable = New DataView(dsList.Tables(0), "employeeunkid NOT IN (" & strValue & ")", "", DataViewRowState.CurrentRows).ToTable().Copy()
                Dim dtTbl As DataTable = Nothing
                If strValue = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                    strValue = String.Join(",", dsList.Tables(0).AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("employeeunkid")).[Select](Function(x) New With {Key .empid = x.Key, Key .empstat = x.[Select](Function(z) z.Field(Of Integer)("statusunkid")).Max}).Where(Function(x) x.empstat = 2 Or x.empstat = 3).Select(Function(x) x.empid.ToString()).ToArray())
                    If strValue.Trim.Length > 0 Then
                        dtTbl = New DataView(dsList.Tables(0), "employeeunkid NOT IN (" & strValue & ")", "", DataViewRowState.CurrentRows).ToTable().Copy()
                    End If
                Else
                    dtTbl = New DataView(dsList.Tables(0), "statusunkid NOT IN (" & strValue & ")", "", DataViewRowState.CurrentRows).ToTable().Copy()
                End If
                'S.SANDEEP [09-AUG-2018] -- END
                dsList.Tables.Remove(strTableName)
                dsList.Tables.Add(dtTbl)
            End If
            'S.SANDEEP [14-AUG-2018] -- END

            For Each xRow As DataRow In dsList.Tables(0).Rows
                xRow("EffDate") = eZeeDate.convertDate(xRow.Item("edate").ToString).ToShortDateString
                'S.SANDEEP [29 APR 2015] -- START
                'xRow("dDate1") = eZeeDate.convertDate(xRow.Item("edate1").ToString).ToShortDateString
                If xRow.Item("edate1").ToString.Trim.Length > 0 Then
                    xRow("dDate1") = eZeeDate.convertDate(xRow.Item("edate1").ToString).ToShortDateString
                End If
                'S.SANDEEP [29 APR 2015] -- END
                If xRow.Item("edate2").ToString.Trim.Length > 0 Then
                    xRow("dDate2") = eZeeDate.convertDate(xRow.Item("edate2").ToString).ToShortDateString
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_dates_approval_tran) </purpose>
    Public Function Insert(ByVal intCompanyId As Integer, ByVal eOprType As clsEmployeeMovmentApproval.enOperationType, Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal blnFromApproval As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
            'S.SANDEEP |17-JAN-2019| -- START

        'Dim objDataOperation As New clsDataOperation

        'If xDataOpr IsNot Nothing Then
        '    objDataOperation = xDataOpr
        '    objDataOperation.ClearParameters()
        'Else
        '    objDataOperation = New clsDataOperation
        'End If

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()


        If blnFromApproval = False Then

            If isExist(Nothing, Nothing, Nothing, mintDatetypeunkid, mintEmployeeunkid, eOprType, , objDataOperation, True, mintDatestranunkid) Then
                Select Case mintDatetypeunkid
                    Case enEmp_Dates_Transaction.DT_PROBATION
                        mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, porbation information is already present for the selected effective date.")
                    Case enEmp_Dates_Transaction.DT_CONFIRMATION
                        mstrMessage = Language.getMessage(mstrModuleName, 16, "Sorry, confirmation information is already present for the selected effective date.")
                    Case enEmp_Dates_Transaction.DT_SUSPENSION
                        mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, suspension information is already present for the selected effective date.")
                    Case enEmp_Dates_Transaction.DT_TERMINATION
                        mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, termination information is already present for the selected effective date.")
                    Case enEmp_Dates_Transaction.DT_REHIRE
                        mstrMessage = Language.getMessage(mstrModuleName, 17, "Sorry, re-hire information is already present for the selected effective date.")
                    Case enEmp_Dates_Transaction.DT_RETIREMENT
                        mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, retirement information is already present for the selected effective date.")
                    Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                        mstrMessage = Language.getMessage(mstrModuleName, 19, "Sorry, appointment information is already present for the selected effective date.")
                    Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                        mstrMessage = Language.getMessage(mstrModuleName, 20, "Sorry, birthdate information is already present for the selected effective date.")
                    Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                        mstrMessage = Language.getMessage(mstrModuleName, 21, "Sorry, first appointment date information is already present for the selected effective date.")
                    Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                        mstrMessage = Language.getMessage(mstrModuleName, 22, "Sorry, anniversary information is already present for the selected effective date.")
                        'Sohail (21 Oct 2019) -- Start
                        'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                    Case enEmp_Dates_Transaction.DT_EXEMPTION
                        mstrMessage = Language.getMessage(mstrModuleName, 23, "Sorry, exemption information is already present for the selected effective date.")
                        'Sohail (21 Oct 2019) -- End
                End Select
                Return False
            End If



            'If isExist(Nothing, Nothing, Nothing, mintDatetypeunkid, mintEmployeeunkid, , objDataOperation, True) Then
            '    Select Case mintDatetypeunkid
            '        Case enEmp_Dates_Transaction.DT_PROBATION
            '            mstrMessage = Language.getMessage(mstrModuleName, 60, "Sorry, Probation information is already present in pending state for the selected employee.")
            '        Case enEmp_Dates_Transaction.DT_CONFIRMATION
            '            mstrMessage = Language.getMessage(mstrModuleName, 61, "Sorry, Confirmation information is already present in pending state for the selected employee.")
            '        Case enEmp_Dates_Transaction.DT_SUSPENSION
            '            mstrMessage = Language.getMessage(mstrModuleName, 62, "Sorry, Suspension information is already present in pending state for the selected employee.")
            '        Case enEmp_Dates_Transaction.DT_TERMINATION
            '            mstrMessage = Language.getMessage(mstrModuleName, 63, "Sorry, Termination information is already present in pending state for the selected employee.")
            '        Case enEmp_Dates_Transaction.DT_RETIREMENT
            '            mstrMessage = Language.getMessage(mstrModuleName, 64, "Sorry, Retirement information is already present in pending state for the selected employee.")
            '    End Select
            '    Return False
            'End If

            'If isExist(mdtEffectivedate, Nothing, Nothing, mintDatetypeunkid, mintEmployeeunkid, , objDataOperation) Then
            '    Select Case mintDatetypeunkid
            '        Case enEmp_Dates_Transaction.DT_PROBATION
            '            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, porbation information is already present for the selected effective date.")
            '        Case enEmp_Dates_Transaction.DT_CONFIRMATION
            '            mstrMessage = Language.getMessage(mstrModuleName, 16, "Sorry, confirmation information is already present for the selected effective date.")
            '        Case enEmp_Dates_Transaction.DT_SUSPENSION
            '            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, suspension information is already present for the selected effective date.")
            '        Case enEmp_Dates_Transaction.DT_TERMINATION
            '            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, termination information is already present for the selected effective date.")
            '        Case enEmp_Dates_Transaction.DT_REHIRE
            '            mstrMessage = Language.getMessage(mstrModuleName, 17, "Sorry, re-hire information is already present for the selected effective date.")
            '        Case enEmp_Dates_Transaction.DT_RETIREMENT
            '            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, retirement information is already present for the selected effective date.")
            '        Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
            '            mstrMessage = Language.getMessage(mstrModuleName, 19, "Sorry, appointment information is already present for the selected effective date.")
            '        Case enEmp_Dates_Transaction.DT_BIRTH_DATE
            '            mstrMessage = Language.getMessage(mstrModuleName, 20, "Sorry, birthdate information is already present for the selected effective date.")
            '        Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
            '            mstrMessage = Language.getMessage(mstrModuleName, 21, "Sorry, first appointment date information is already present for the selected effective date.")
            '        Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
            '            mstrMessage = Language.getMessage(mstrModuleName, 22, "Sorry, anniversary information is already present for the selected effective date.")
            '    End Select
            '    Return False
            'End If

            'If isExist(Nothing, mdtDate1, mdtDate2, mintDatetypeunkid, mintEmployeeunkid, , objDataOperation) Then
            '    Select Case mintDatetypeunkid
            '        Case enEmp_Dates_Transaction.DT_PROBATION
            '            mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, This probation date range is already defined. Please define new date range for this probation.")
            '        Case enEmp_Dates_Transaction.DT_CONFIRMATION
            '            mstrMessage = Language.getMessage(mstrModuleName, 15, "Sorry, This confirmation information is already present.")
            '        Case enEmp_Dates_Transaction.DT_SUSPENSION
            '            mstrMessage = Language.getMessage(mstrModuleName, 6, "Sorry, This suspension date range is already defined. Please define new date range for this suspension.")
            '        Case enEmp_Dates_Transaction.DT_TERMINATION
            '            If mblnInsertNullTerminationDate = True Then
            '                GoTo lbl
            '            Else
            '                mstrMessage = Language.getMessage(mstrModuleName, 7, "Sorry, This termination information is already present.")
            '            End If
            '        Case enEmp_Dates_Transaction.DT_REHIRE
            '            mstrMessage = Language.getMessage(mstrModuleName, 18, "Sorry, This re-hire information is already present.")
            '        Case enEmp_Dates_Transaction.DT_RETIREMENT
            '            mstrMessage = Language.getMessage(mstrModuleName, 8, "Sorry, This retirement information is already present.")
            '        Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
            '            mstrMessage = Language.getMessage(mstrModuleName, 23, "Sorry, This appointment date information is already present.")
            '        Case enEmp_Dates_Transaction.DT_BIRTH_DATE
            '            mstrMessage = Language.getMessage(mstrModuleName, 24, "Sorry, This birthdate date information is already present.")
            '        Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
            '            mstrMessage = Language.getMessage(mstrModuleName, 25, "Sorry, This first appointment date information is already present.")
            '        Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
            '            mstrMessage = Language.getMessage(mstrModuleName, 26, "Sorry, This anniversary information is already present.")
            '    End Select
            '    Return False
            'End If

            'S.SANDEEP |17-JAN-2019| -- END
        End If
        
lbl:
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@datestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDatestranunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            If mdtDate1 <> Nothing Then
                objDataOperation.AddParameter("@date1", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDate1)
            Else
                objDataOperation.AddParameter("@date1", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtDate2 <> Nothing Then
                objDataOperation.AddParameter("@date2", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDate2)
            Else
                objDataOperation.AddParameter("@date2", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@actionreasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActionreasonunkid.ToString)
            objDataOperation.AddParameter("@datetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDatetypeunkid.ToString)
            objDataOperation.AddParameter("@isexclude_payroll", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexclude_Payroll.ToString)
            objDataOperation.AddParameter("@isconfirmed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsconfirmed.ToString)
            objDataOperation.AddParameter("@otherreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherreason.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsProcessed)
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationTypeId)
            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid)
            'Sohail (21 Oct 2019) -- End

            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            If mdtActualDate <> Nothing Then
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualDate)
            Else
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Pinkal (07-Mar-2020) -- End


            'Gajanan [18-May-2020] -- Start
            'Enhancement:Discipline Module Enhancement NMB
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplineunkid)
            'Gajanan [18-May-2020] -- End


            strQ = "INSERT INTO hremployee_dates_approval_tran ( " & _
              "  tranguid " & _
              ", transactiondate " & _
              ", mappingunkid " & _
              ", remark " & _
              ", isfinal " & _
              ", statusunkid " & _
              ", datestranunkid " & _
              ", effectivedate " & _
              ", employeeunkid " & _
              ", rehiretranunkid " & _
              ", date1 " & _
              ", date2 " & _
              ", changereasonunkid " & _
              ", actionreasonunkid " & _
              ", datetypeunkid " & _
              ", isexclude_payroll " & _
              ", isconfirmed " & _
              ", otherreason " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", voiduserunkid " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", ip " & _
              ", hostname " & _
              ", form_name " & _
              ", isweb" & _
              ", isprocessed " & _
              ", operationtypeid " & _
              ", leaveissueunkid " & _
              ", actualdate " & _
              ", disciplinefileunkid " & _
            ") VALUES (" & _
              "  @tranguid " & _
              ", @transactiondate " & _
              ", @mappingunkid " & _
              ", @remark " & _
              ", @isfinal " & _
              ", @statusunkid " & _
              ", @datestranunkid " & _
              ", @effectivedate " & _
              ", @employeeunkid " & _
              ", @rehiretranunkid " & _
              ", @date1 " & _
              ", @date2 " & _
              ", @changereasonunkid " & _
              ", @actionreasonunkid " & _
              ", @datetypeunkid " & _
              ", @isexclude_payroll " & _
              ", @isconfirmed " & _
              ", @otherreason " & _
              ", @isvoid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @voiduserunkid " & _
              ", @audittype " & _
              ", @audituserunkid " & _
              ", @ip " & _
              ", @hostname " & _
              ", @form_name " & _
              ", @isweb" & _
              ", @isprocessed " & _
              ", @operationtypeid " & _
              ", @leaveissueunkid " & _
              ", @actualdate " & _
              ", @disciplinefileunkid " & _
            "); SELECT @@identity"

            'Pinkal (07-Mar-2020) -- Enhancement - Changes Related to Payroll UAT for NMB.[@actualdate]

            'Sohail (21 Oct 2019) - [leaveissueunkid]
            'S.SANDEEP |17-JAN-2019| -- Add Opration Type
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'mstrTranguid = dsList.Tables(0).Rows(0).Item(0)
            'S.SANDEEP |17-JAN-2019| -- START

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            'S.SANDEEP |17-JAN-2019| -- END
            Return True
        Catch ex As Exception
            'S.SANDEEP |17-JAN-2019| -- START
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        'S.SANDEEP |17-JAN-2019| -- END
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Update(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal intTranUnkid As Integer, ByVal dtEffDate As Date, ByVal intDateTypeId As Integer, ByVal strColName As String) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
            'S.SANDEEP |17-JAN-2019| -- START

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
            'S.SANDEEP |17-JAN-2019| -- END
        Try
            'S.SANDEEP [15-NOV-2018] -- Start

            strQ = "UPDATE hremployee_dates_approval_tran SET " & _
                   " " & strColName & " = '" & intTranUnkid & "' " & _
                   "WHERE isprocessed= 0 and CONVERT(CHAR(8),effectivedate,112) = '" & eZeeDate.convertDate(dtEffDate).ToString & "' AND employeeunkid = '" & intEmployeeId & "' " & _
                   " AND datetypeunkid = '" & intDateTypeId & "' "
            '----REMOVED (effectivedate)
            '----ADDED (CONVERT(CHAR(8),effectivedate,112))
            'S.SANDEEP [15-NOV-2018] -- End
            Call xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP |17-JAN-2019| -- START
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)

            'S.SANDEEP |17-JAN-2019| -- END
            End If
            Return True
        Catch ex As Exception
            'S.SANDEEP |17-JAN-2019| -- START
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP |17-JAN-2019| -- END
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
        Finally
            'S.SANDEEP |17-JAN-2019| -- START
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP |17-JAN-2019| -- END
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_dates_approval_tran) </purpose>
    Public Function Update(ByVal intCompanyId As Integer, ByVal eOprType As clsEmployeeMovmentApproval.enOperationType, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

            'S.SANDEEP |17-JAN-2019| -- START


        'Dim objDataOperation As New clsDataOperation

'        If xDataOpr IsNot Nothing Then
'            objDataOperation = xDataOpr
'            objDataOperation.ClearParameters()
'        Else
'            objDataOperation = New clsDataOperation
'        End If

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
            'S.SANDEEP |17-JAN-2019| -- END

        If isExist(mdtEffectivedate, Nothing, Nothing, mintDatetypeunkid, mintEmployeeunkid, mstrTranguid, "", objDataOperation) Then
            Select Case mintDatetypeunkid
                Case enEmp_Dates_Transaction.DT_PROBATION
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, porbation information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_CONFIRMATION
                    mstrMessage = Language.getMessage(mstrModuleName, 16, "Sorry, confirmation information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_SUSPENSION
                    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, suspension information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_TERMINATION
                    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, termination information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_REHIRE
                    mstrMessage = Language.getMessage(mstrModuleName, 17, "Sorry, re-hire information is already present for the selected effective date.")
                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, retirement information is already present for the selected effective date.")
                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enEmp_Dates_Transaction.DT_EXEMPTION
                    mstrMessage = Language.getMessage(mstrModuleName, 23, "Sorry, exemption information is already present for the selected effective date.")
                    'Sohail (21 Oct 2019) -- End
            End Select
            Return False
        End If
        If isExist(Nothing, mdtDate1, mdtDate2, mintDatetypeunkid, mintEmployeeunkid, mstrTranguid, "", objDataOperation) Then
            Select Case mintDatetypeunkid
                Case enEmp_Dates_Transaction.DT_PROBATION
                    mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, This probation date range is already defined. Please define new date range for this probation.")
                Case enEmp_Dates_Transaction.DT_CONFIRMATION
                    mstrMessage = Language.getMessage(mstrModuleName, 15, "Sorry, This confirmation information is already present.")
                Case enEmp_Dates_Transaction.DT_SUSPENSION
                    mstrMessage = Language.getMessage(mstrModuleName, 6, "Sorry, This suspension date range is already defined. Please define new date range for this suspension.")
                Case enEmp_Dates_Transaction.DT_TERMINATION
                    mstrMessage = Language.getMessage(mstrModuleName, 7, "Sorry, This termination information is already present.")
                Case enEmp_Dates_Transaction.DT_REHIRE
                    mstrMessage = Language.getMessage(mstrModuleName, 18, "Sorry, This re-hire information is already present.")
                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    mstrMessage = Language.getMessage(mstrModuleName, 8, "Sorry, This retirement information is already present.")
                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enEmp_Dates_Transaction.DT_EXEMPTION
                    mstrMessage = Language.getMessage(mstrModuleName, 24, "Sorry, This exemption information is already present.")
                    'Sohail (21 Oct 2019) -- End
            End Select
            Return False
        End If

        Try
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@datestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDatestranunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            If mdtDate1 <> Nothing Then
                objDataOperation.AddParameter("@date1", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDate1)
            Else
                objDataOperation.AddParameter("@date1", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtDate2 <> Nothing Then
                objDataOperation.AddParameter("@date2", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDate2)
            Else
                objDataOperation.AddParameter("@date2", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@actionreasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActionreasonunkid.ToString)
            objDataOperation.AddParameter("@datetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDatetypeunkid.ToString)
            objDataOperation.AddParameter("@isexclude_payroll", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexclude_Payroll.ToString)
            objDataOperation.AddParameter("@isconfirmed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsconfirmed.ToString)
            objDataOperation.AddParameter("@otherreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherreason.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid)
            'Sohail (21 Oct 2019) -- End

            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            If mdtActualDate <> Nothing Then
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualDate)
            Else
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Pinkal (07-Mar-2020) -- End


            'Gajanan [18-May-2020] -- Start
            'Enhancement:Discipline Module Enhancement NMB
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplineunkid)
            'Gajanan [18-May-2020] -- End


            strQ = "UPDATE hremployee_dates_approval_tran SET " & _
              "  transactiondate = @transactiondate" & _
              ", mappingunkid = @mappingunkid" & _
              ", remark = @remark" & _
              ", isfinal = @isfinal" & _
              ", statusunkid = @statusunkid" & _
              ", datestranunkid = @datestranunkid" & _
              ", effectivedate = @effectivedate" & _
              ", employeeunkid = @employeeunkid" & _
              ", rehiretranunkid = @rehiretranunkid" & _
              ", date1 = @date1" & _
              ", date2 = @date2" & _
              ", changereasonunkid = @changereasonunkid" & _
              ", actionreasonunkid = @actionreasonunkid" & _
              ", datetypeunkid = @datetypeunkid" & _
              ", isexclude_payroll = @isexclude_payroll" & _
              ", isconfirmed = @isconfirmed" & _
              ", otherreason = @otherreason" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", audittype = @audittype" & _
              ", audituserunkid = @audituserunkid" & _
              ", ip = @ip" & _
              ", hostname = @hostname" & _
              ", form_name = @form_name" & _
              ", isweb = @isweb " & _
              ", leaveissueunkid = @leaveissueunkid " & _
              ", actualdate = @actualdate " & _
              ", disciplinefileunkid = @disciplinefileunkid " & _
            "WHERE tranguid = @tranguid "

            'Pinkal (07-Mar-2020) -- Enhancement - Changes Related to Payroll UAT for NMB.[", actualdate = @actualdate " & _]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP |17-JAN-2019| -- START


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP |17-JAN-2019| -- END
            Return True
        Catch ex As Exception
            'S.SANDEEP |17-JAN-2019| -- START
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
                        'S.SANDEEP |17-JAN-2019| -- END
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployee_dates_approval_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal eOprType As clsEmployeeMovmentApproval.enOperationType, ByVal intCompanyId As Integer, ByVal mblnCreateADUserFromEmpMst As Boolean, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        mstrMessage = ""
        Try
            Dim objemployee_dates_tran As clsemployee_dates_tran
            objemployee_dates_tran = New clsemployee_dates_tran

            objemployee_dates_tran._Datestranunkid(objDataOperation) = intUnkid


            Me._Audittype = enAuditType.ADD
            Me._Effectivedate = objemployee_dates_tran._Effectivedate
            Me._Employeeunkid = objemployee_dates_tran._Employeeunkid
            Me._Date1 = objemployee_dates_tran._Date1
            Me._Date2 = objemployee_dates_tran._Date2
            Me._Isconfirmed = objemployee_dates_tran._Isconfirmed
            Me._Datetypeunkid = objemployee_dates_tran._Datetypeunkid
            Me._Changereasonunkid = objemployee_dates_tran._Changereasonunkid
            Me._Voiddatetime = Nothing
            'Gajanan [9-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
            'Me._Voidreason = ""
            'Gajanan [9-July-2019] -- End
            Me._Voiduserunkid = -1
            Me._Actionreasonunkid = objemployee_dates_tran._Actionreasonunkid
            Me._Isexclude_Payroll = objemployee_dates_tran._Actionreasonunkid
            Me._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
            Me._Tranguid = Guid.NewGuid.ToString()
            Me._Transactiondate = Now
            Me._Remark = ""
            Me._Rehiretranunkid = 0
            Me._Mappingunkid = 0
            Me._Datestranunkid = intUnkid
            Me._IsProcessed = False
            Me._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.DELETED
            Me._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            Me._Leaveissueunkid = objemployee_dates_tran._Leaveissueunkid
            'Sohail (21 Oct 2019) -- End

            'Gajanan [9-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
            Me._Isvoid = False
            'Gajanan [9-July-2019] -- End

            objemployee_dates_tran._Voidreason = Me._Voidreason
            objemployee_dates_tran._Voiduserunkid = Me._Audituserunkid
            objemployee_dates_tran._Voiddatetime = Now
            objemployee_dates_tran._Isvoid = True


            If mblnIsweb Then
            objemployee_dates_tran._WebClientIP = Me._Ip
            objemployee_dates_tran._WebFormName = Me._Form_Name
            objemployee_dates_tran._WebHostName = Me._Hostname
            End If
            objemployee_dates_tran._Userunkid = Me._Audituserunkid


            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            Me._ActualDate = objemployee_dates_tran._ActualDate
            'Pinkal (07-Mar-2020) -- End


            'Gajanan [9-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
            'Gajanan [9-July-2019] -- End


            'Gajanan [18-May-2020] -- Start
            'Enhancement:Discipline Module Enhancement NMB
            Me._Disciplinefileunkid = objemployee_dates_tran._Disciplinefileunkid
            'Gajanan [18-May-2020] -- End


            blnFlag = Insert(intCompanyId, clsEmployeeMovmentApproval.enOperationType.DELETED, objDataOperation)

            If blnFlag = True AndAlso mstrMessage.Trim.Length <= 0 Then
                If objemployee_dates_tran.Delete(mblnCreateADUserFromEmpMst, intUnkid, intCompanyId, objDataOperation) = False Then
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
                End If
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return blnFlag
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@tranguid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xEffDate As DateTime, _
                            ByVal xDate1 As DateTime, _
                            ByVal xDate2 As DateTime, _
                            ByVal xDateTran As Integer, _
                            ByVal xEmployeeId As Integer, _
                            ByVal xeOprType As clsEmployeeMovmentApproval.enOperationType, _
                            Optional ByVal strUnkid As String = "", _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                            Optional ByVal blnPreventNewEntry As Boolean = False, _
                            Optional ByVal xTranferUnkid As Integer = 0, _
                            Optional ByVal xDisciplineunkid As Integer = -1 _
                            ) As Boolean 'S.SANDEEP |17-JAN-2019| -- START {xTranferUnkid,xeOprType} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
            'S.SANDEEP |17-JAN-2019| -- START

        'Dim objDataOperation As New clsDataOperation

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
            'S.SANDEEP |17-JAN-2019| -- END
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  tranguid " & _
              ", transactiondate " & _
              ", mappingunkid " & _
              ", remark " & _
              ", isfinal " & _
              ", statusunkid " & _
              ", datestranunkid " & _
              ", effectivedate " & _
              ", employeeunkid " & _
              ", rehiretranunkid " & _
              ", date1 " & _
              ", date2 " & _
              ", changereasonunkid " & _
              ", actionreasonunkid " & _
              ", datetypeunkid " & _
              ", isexclude_payroll " & _
              ", isconfirmed " & _
              ", otherreason " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", voiduserunkid " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", ip " & _
              ", hostname " & _
              ", form_name " & _
              ", isweb " & _
              ", actualdate " & _
              ", disciplinefileunkid " & _
             "FROM hremployee_dates_approval_tran " & _
             "WHERE isvoid = 0 AND datetypeunkid = " & xDateTran & " and operationtypeid = @operationtypeid " & _
             "AND statusunkid <> 3 "
            'Pinkal (07-Mar-2020) -- Enhancement - Changes Related to Payroll UAT for NMB.[", actualdate " & _]

            If blnPreventNewEntry Then
                'S.SANDEEP [09-AUG-2018] -- START
                'strQ &= " AND hremployee_dates_approval_tran.isfinal = 0 "
                strQ &= " AND hremployee_dates_approval_tran.isfinal = 0  AND ISNULL(isprocessed,0) = 0 "
                'S.SANDEEP [09-AUG-2018] -- END
            End If

            If xEffDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),effectivedate,112) = @effectivedate "
            End If

            If xEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            If strUnkid.Trim.Length > 0 Then
                strQ &= " AND tranguid <> @tranguid"
            End If

            Select Case xDateTran
                Case enEmp_Dates_Transaction.DT_PROBATION
                    If xDate1 <> Nothing AndAlso xDate2 <> Nothing Then
                        strQ &= "  AND " & _
                                "( " & _
                                "  (@xDate1 BETWEEN CONVERT(CHAR(8),date1,112) AND CONVERT(CHAR(8),date2,112) " & _
                                "       OR @xDate2 BETWEEN CONVERT(CHAR(8),date1,112) AND CONVERT(CHAR(8),date2,112)) " & _
                                "  OR " & _
                                "  (CONVERT(CHAR(8),date1,112) BETWEEN @xDate1 and @xDate2 " & _
                                "   AND CONVERT(CHAR(8),date2,112) BETWEEN @xDate1 and @xDate2) " & _
                                ") "
                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                        objDataOperation.AddParameter("xDate2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate2))
                    End If
                Case enEmp_Dates_Transaction.DT_CONFIRMATION
                    If xDate1 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8),date1,112) = @xDate1 "
                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                    End If
                Case enEmp_Dates_Transaction.DT_SUSPENSION

                    If xDate1 <> Nothing AndAlso xDate2 <> Nothing Then
                        strQ &= "  AND " & _
                                "( " & _
                                "  (@xDate1 BETWEEN CONVERT(CHAR(8),date1,112) AND CONVERT(CHAR(8),date2,112) " & _
                                "       OR @xDate2 BETWEEN CONVERT(CHAR(8),date1,112) AND CONVERT(CHAR(8),date2,112)) " & _
                                "  OR " & _
                                "  (CONVERT(CHAR(8),date1,112) BETWEEN @xDate1 and @xDate2 " & _
                                "   AND CONVERT(CHAR(8),date2,112) BETWEEN @xDate1 and @xDate2) " & _
                                "   OR date2 IS NULL " & _
                                ") "

                        'S.SANDEEP |02-MAR-2020| -- START
                        'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
                        '--------------------- ADDED {OR date2 IS NULL} 
                        'S.SANDEEP |02-MAR-2020| -- END


                        'Gajanan [18-May-2020] -- Start
                        'Enhancement:Discipline Module Enhancement NMB

                        If xDisciplineunkid > 0 Then
                            strQ &= " AND disciplinefileunkid  = @disciplinefileunkid "
                            objDataOperation.AddParameter("disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xDisciplineunkid)
                        End If

                        'Gajanan [18-May-2020] -- End


                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                        objDataOperation.AddParameter("xDate2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate2))
                    End If

                Case enEmp_Dates_Transaction.DT_TERMINATION
                    If xDate1 <> Nothing AndAlso xDate2 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8),date1,112) = @xDate1  AND  CONVERT(CHAR(8),date2,112) = @xDate2 "
                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                        objDataOperation.AddParameter("xDate2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate2))
                    End If
                Case enEmp_Dates_Transaction.DT_REHIRE
                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    If xDate1 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8),date1,112) = @xDate1 "
                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                    End If
                Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                    If xDate1 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8),date1,112) = @xDate1 "
                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                    End If
                Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                    If xDate1 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8),date1,112) = @xDate1 "
                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                    End If
                Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                    If xDate1 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8),date1,112) = @xDate1 "
                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                    End If
                Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                    If xDate1 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8),date1,112) = @xDate1 "
                        objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                    End If
                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enEmp_Dates_Transaction.DT_EXEMPTION
                    If xDate1 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8),date1,112) >= @xDate1 "
                        objDataOperation.AddParameter("@xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                    End If
                    If xDate2 <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8),date1,112) >= @xDate2 "
                        objDataOperation.AddParameter("@xDate2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate2))
                    End If
                    'Sohail (21 Oct 2019) -- End
            End Select

            'S.SANDEEP |17-JAN-2019| -- START
            If xeOprType = clsEmployeeMovmentApproval.enOperationType.EDITED Or mintOperationTypeId = clsEmployeeMovmentApproval.enOperationType.DELETED Then
                If xTranferUnkid > 0 Then
                    strQ &= " AND datestranunkid = @datestranunkid "
                    objDataOperation.AddParameter("@datestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTranferUnkid)
                End If
            End If
            'S.SANDEEP |17-JAN-2019| -- END

            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xEffDate))
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strUnkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xeOprType)

            'S.SANDEEP |17-JAN-2019| -- Add Opration Type
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Sohail (21 Oct 2019) -- Start
    'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
    Public Function GetUnkIdByLeaveIssueUnkId(ByVal xDataOp As clsDataOperation _
                                            , ByVal intLeaveIssueUnkId As Integer _
                                            , Optional ByVal strFilter As String = "" _
                                            ) As Integer
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim intID As Integer = 0

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            StrQ = "SELECT DISTINCT " & _
                        "hremployee_dates_approval_tran.datestranunkid " & _
                    "FROM hremployee_dates_approval_tran " & _
                    "WHERE hremployee_dates_approval_tran.isvoid = 0 "

            If intLeaveIssueUnkId > 0 Then
                StrQ &= "AND hremployee_dates_approval_tran.leaveissueunkid = @leaveissueunkid "
                objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveIssueUnkId)
            End If

            If strFilter.Trim <> "" Then
                StrQ &= strFilter & " "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intID = CInt(dsList.Tables(0).Rows(0).Item("datestranunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUnkIdLeaveIssueUnkId; Module Name: " & mstrModuleName)
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return intID
    End Function
    'Sohail (21 Oct 2019) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, porbation information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 2, "Sorry, suspension information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 3, "Sorry, termination information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 4, "Sorry, retirement information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 5, "Sorry, This probation date range is already defined. Please define new date range for this probation.")
			Language.setMessage(mstrModuleName, 6, "Sorry, This suspension date range is already defined. Please define new date range for this suspension.")
			Language.setMessage(mstrModuleName, 7, "Sorry, This termination information is already present.")
			Language.setMessage(mstrModuleName, 8, "Sorry, This retirement information is already present.")
			Language.setMessage(mstrModuleName, 15, "Sorry, This confirmation information is already present.")
			Language.setMessage(mstrModuleName, 16, "Sorry, confirmation information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 17, "Sorry, re-hire information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 18, "Sorry, This re-hire information is already present.")
			Language.setMessage(mstrModuleName, 19, "Sorry, appointment information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 20, "Sorry, birthdate information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 21, "Sorry, first appointment date information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 22, "Sorry, anniversary information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 23, "Sorry, exemption information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 24, "Sorry, This exemption information is already present.")
			Language.setMessage("clsEmployeeMovmentApproval", 62, "Newly Added Information")
			Language.setMessage("clsEmployeeMovmentApproval", 63, "Information Edited")
			Language.setMessage("clsEmployeeMovmentApproval", 64, "Information Deleted")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class