﻿'************************************************************************************************************************************
'Class Name :clsemployee_transfer_tran.vb
'Purpose    :
'Date       :14-Mar-2015
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System.Threading
Imports System.DirectoryServices

''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsemployee_transfer_tran
    Private Shared ReadOnly mstrModuleName As String = "clsemployee_transfer_tran"
    Private trd As Thread
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintTransferunkid As Integer = 0
    Private mdtEffectivedate As Date = Nothing
    Private mintEmployeeunkid As Integer = 0
    Private mintRehiretranunkid As Integer = 0
    Private mintStationunkid As Integer = 0
    Private mintDeptgroupunkid As Integer = 0
    Private mintDepartmentunkid As Integer = 0
    Private mintSectiongroupunkid As Integer = 0
    Private mintSectionunkid As Integer = 0
    Private mintUnitgroupunkid As Integer = 0
    Private mintUnitunkid As Integer = 0
    Private mintTeamunkid As Integer = 0
    Private mintClassgroupunkid As Integer = 0
    Private mintClassunkid As Integer = 0
    Private mintChangereasonunkid As Integer = 0
    Private mblnIsFromemployee As Boolean = False
    Private mintUserunkid As Integer = 0
    Private mintStatusunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transferunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Transferunkid(Optional ByVal objDataOper As clsDataOperation = Nothing) As Integer 'S.SANDEEP [25 OCT 2016] -- START {objDataOperation} -- END
        Get
            Return mintTransferunkid
        End Get
        Set(ByVal value As Integer)
            mintTransferunkid = value
            'S.SANDEEP [25 OCT 2016] -- START
            'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
            'Call GetData()
            Call GetData(objDataOper)
            'S.SANDEEP [25 OCT 2016] -- END
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set rehiretranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Rehiretranunkid() As Integer
        Get
            Return mintRehiretranunkid
        End Get
        Set(ByVal value As Integer)
            mintRehiretranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stationunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Stationunkid() As Integer
        Get
            Return mintStationunkid
        End Get
        Set(ByVal value As Integer)
            mintStationunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set deptgroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Deptgroupunkid() As Integer
        Get
            Return mintDeptgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintDeptgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set departmentunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Departmentunkid() As Integer
        Get
            Return mintDepartmentunkid
        End Get
        Set(ByVal value As Integer)
            mintDepartmentunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sectiongroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Sectiongroupunkid() As Integer
        Get
            Return mintSectiongroupunkid
        End Get
        Set(ByVal value As Integer)
            mintSectiongroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sectionunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Sectionunkid() As Integer
        Get
            Return mintSectionunkid
        End Get
        Set(ByVal value As Integer)
            mintSectionunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unitgroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Unitgroupunkid() As Integer
        Get
            Return mintUnitgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintUnitgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unitunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Unitunkid() As Integer
        Get
            Return mintUnitunkid
        End Get
        Set(ByVal value As Integer)
            mintUnitunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set teamunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Teamunkid() As Integer
        Get
            Return mintTeamunkid
        End Get
        Set(ByVal value As Integer)
            mintTeamunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set classgroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Classgroupunkid() As Integer
        Get
            Return mintClassgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintClassgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set classunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Classunkid() As Integer
        Get
            Return mintClassunkid
        End Get
        Set(ByVal value As Integer)
            mintClassunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set changereasonunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Changereasonunkid() As Integer
        Get
            Return mintChangereasonunkid
        End Get
        Set(ByVal value As Integer)
            mintChangereasonunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfromemployee
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _IsFromEmployee() As Integer
        Get
            Return mblnIsFromemployee
        End Get
        Set(ByVal value As Integer)
            mblnIsFromemployee = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webformname
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webclientip
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webhostname
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
              "  transferunkid " & _
              ", effectivedate " & _
              ", employeeunkid " & _
              ", rehiretranunkid " & _
              ", stationunkid " & _
              ", deptgroupunkid " & _
              ", departmentunkid " & _
              ", sectiongroupunkid " & _
              ", sectionunkid " & _
              ", unitgroupunkid " & _
              ", unitunkid " & _
              ", teamunkid " & _
              ", classgroupunkid " & _
              ", classunkid " & _
              ", changereasonunkid " & _
              ", isfromemployee " & _
              ", userunkid " & _
              ", statusunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hremployee_transfer_tran " & _
             "WHERE transferunkid = @transferunkid "

            objDataOperation.AddParameter("@transferunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTransferunkid = CInt(dtRow.Item("transferunkid"))
                mdtEffectivedate = dtRow.Item("effectivedate")
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintRehiretranunkid = CInt(dtRow.Item("rehiretranunkid"))
                mintStationunkid = CInt(dtRow.Item("stationunkid"))
                mintDeptgroupunkid = CInt(dtRow.Item("deptgroupunkid"))
                mintDepartmentunkid = CInt(dtRow.Item("departmentunkid"))
                mintSectiongroupunkid = CInt(dtRow.Item("sectiongroupunkid"))
                mintSectionunkid = CInt(dtRow.Item("sectionunkid"))
                mintUnitgroupunkid = CInt(dtRow.Item("unitgroupunkid"))
                mintUnitunkid = CInt(dtRow.Item("unitunkid"))
                mintTeamunkid = CInt(dtRow.Item("teamunkid"))
                mintClassgroupunkid = CInt(dtRow.Item("classgroupunkid"))
                mintClassunkid = CInt(dtRow.Item("classunkid"))
                mintChangereasonunkid = CInt(dtRow.Item("changereasonunkid"))
                mblnIsFromemployee = CBool(dtRow.Item("isfromemployee"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal xEmployeeUnkid As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mdtAppDate As String = ""
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                   "FROM hremployee_master " & _
                   "WHERE employeeunkid = '" & xEmployeeUnkid & "' "

            objDataOperation.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtAppDate = objDataOperation.GetParameterValue("@ADate")


            strQ = "SELECT " & _
                   "  hremployee_transfer_tran.transferunkid " & _
                   ", hremployee_transfer_tran.effectivedate " & _
                   ", hremployee_transfer_tran.employeeunkid " & _
                   ", hremployee_transfer_tran.rehiretranunkid " & _
                   ", hremployee_transfer_tran.stationunkid " & _
                   ", hremployee_transfer_tran.deptgroupunkid " & _
                   ", hremployee_transfer_tran.departmentunkid " & _
                   ", hremployee_transfer_tran.sectiongroupunkid " & _
                   ", hremployee_transfer_tran.sectionunkid " & _
                   ", hremployee_transfer_tran.unitgroupunkid " & _
                   ", hremployee_transfer_tran.unitunkid " & _
                   ", hremployee_transfer_tran.teamunkid " & _
                   ", hremployee_transfer_tran.classgroupunkid " & _
                   ", hremployee_transfer_tran.classunkid " & _
                   ", hremployee_transfer_tran.changereasonunkid " & _
                   ", hremployee_transfer_tran.isfromemployee " & _
                   ", hremployee_transfer_tran.userunkid " & _
                   ", hremployee_transfer_tran.statusunkid " & _
                   ", hremployee_transfer_tran.isvoid " & _
                   ", hremployee_transfer_tran.voiduserunkid " & _
                   ", hremployee_transfer_tran.voiddatetime " & _
                   ", hremployee_transfer_tran.voidreason " & _
                   ", hremployee_master.employeecode as ecode " & _
                   ", hremployee_master.firstname+' '+ISNULL(hremployee_master.othername,'')+' '+hremployee_master.surname AS ename " & _
                   ", ISNULL(hrstation_master.name,'') AS branch " & _
                   ", ISNULL(hrdepartment_group_master.name,'') AS deptgroup " & _
                   ", ISNULL(hrdepartment_master.name,'') AS dept " & _
                   ", ISNULL(hrsectiongroup_master.name,'') AS secgroup " & _
                   ", ISNULL(hrsection_master.name,'') AS section " & _
                   ", ISNULL(hrunitgroup_master.name,'') AS unitname " & _
                   ", ISNULL(hrunit_master.name,'') AS unit " & _
                   ", ISNULL(hrteam_master.name,'') AS team " & _
                   ", ISNULL(hrclassgroup_master.name,'') AS classgrp " & _
                   ", ISNULL(hrclasses_master.name,'') AS class " & _
                   ", CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) AS edate " & _
                   ", CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS adate " & _
                   ", '' AS EffDate " & _
                   ", CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name, '') END AS CReason " & _
                   "FROM hremployee_transfer_tran " & _
                   "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_transfer_tran.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.TRANSFERS & _
                   "    LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = hremployee_transfer_tran.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                   "    LEFT JOIN hrstation_master on hrstation_master.stationunkid = hremployee_transfer_tran.stationunkid " & _
                   "    LEFT JOIN hrdepartment_group_master on hrdepartment_group_master.deptgroupunkid = hremployee_transfer_tran.deptgroupunkid " & _
                   "    LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_transfer_tran.departmentunkid " & _
                   "    LEFT JOIN hrsectiongroup_master ON hremployee_transfer_tran.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
                   "    LEFT JOIN hrsection_master on hrsection_master.sectionunkid = hremployee_transfer_tran.sectionunkid " & _
                   "    LEFT JOIN hrunitgroup_master ON hremployee_transfer_tran.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
                   "    LEFT JOIN hrunit_master on hrunit_master.unitunkid = hremployee_transfer_tran.unitunkid " & _
                   "    LEFT JOIN hrteam_master ON hremployee_transfer_tran.teamunkid = hrteam_master.teamunkid " & _
                   "    LEFT JOIN hrclassgroup_master on hrclassgroup_master.classgroupunkid = hremployee_transfer_tran.classgroupunkid " & _
                   "    LEFT JOIN hrclasses_master on hrclasses_master.classesunkid = hremployee_transfer_tran.classunkid " & _
                   "    JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_transfer_tran.employeeunkid " & _
                   "WHERE isvoid = 0 "

            If xEmployeeUnkid Then
                strQ &= " AND hremployee_transfer_tran.employeeunkid = '" & xEmployeeUnkid & "' "
            End If

            If mdtAppDate.Trim.Length > 0 Then
                strQ &= " AND CONVERT(CHAR(8),hremployee_transfer_tran.effectivedate,112) >= '" & mdtAppDate & "' "
            End If

            strQ &= " ORDER BY effectivedate DESC "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each xRow As DataRow In dsList.Tables(0).Rows
                xRow("EffDate") = eZeeDate.convertDate(xRow.Item("edate").ToString).ToShortDateString
            Next


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_transfer_tran) </purpose>
    Public Function Insert(ByVal xDatabaseName As String, ByVal mblnCreateADUserFromEmpMst As Boolean, ByVal intCompanyId As Integer, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal xMdtMigration As DataTable = Nothing, _
                           Optional ByVal xblnFromApproval As Boolean = False, _
                           Optional ByVal xblnFromApprovalScreen As Boolean = False) As Boolean 'S.SANDEEP [10-MAY-2017] -- START {intCompanyId} -- END


        'Pinkal (12-Oct-2020) -- Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.[ByVal xDatabaseName As String, ]

        'Pinkal (18-Aug-2018) -- 'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[ByVal mblnCreateADUserFromEmpMst As Boolean]

        'Gajanan [23-SEP-2019] -- Add {xMdtMigration,xblnFromApproval,xblnFromApprovalScreen}

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        Dim mdtCurrentDate As Date = Nothing
        'Pinkal (18-Aug-2018) -- End


        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If

        If isExist(mdtEffectivedate, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, mintEmployeeunkid, , objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, transfer information is already present for the selected effective date.")
            Return False
        End If


        'Pinkal (09-Apr-2015) -- Start
        'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA

        'S.SANDEEP [29 APR 2015] -- START
        'ALLOW SAME ALLOCATION IF EMPLOYEE IS REHIRED AND USER WANT PUT IN SAME

        'dsList = Get_Current_Allocation(mdtEffectivedate, mintEmployeeunkid)
        'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
        '    If CInt(dsList.Tables(0).Rows(0)("stationunkid")) = mintStationunkid AndAlso CInt(dsList.Tables(0).Rows(0)("deptgroupunkid")) = mintDeptgroupunkid _
        '      AndAlso CInt(dsList.Tables(0).Rows(0)("departmentunkid")) = mintDepartmentunkid AndAlso CInt(dsList.Tables(0).Rows(0)("sectiongroupunkid")) = mintSectiongroupunkid _
        '      AndAlso CInt(dsList.Tables(0).Rows(0)("sectionunkid")) = mintSectionunkid AndAlso CInt(dsList.Tables(0).Rows(0)("unitgroupunkid")) = mintUnitgroupunkid _
        '      AndAlso CInt(dsList.Tables(0).Rows(0)("unitunkid")) = mintUnitunkid AndAlso CInt(dsList.Tables(0).Rows(0)("teamunkid")) = mintTeamunkid _
        '      AndAlso CInt(dsList.Tables(0).Rows(0)("classgroupunkid")) = mintClassgroupunkid AndAlso CInt(dsList.Tables(0).Rows(0)("classunkid")) = mintClassunkid Then
        '        mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, transfer information is already present for the selected combination for the selected effective date.")
        '        Return False
        '    End If
        'End If
        'dsList = Nothing
        If mintRehiretranunkid <= 0 Then
        dsList = Get_Current_Allocation(mdtEffectivedate, mintEmployeeunkid)
        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
            If CInt(dsList.Tables(0).Rows(0)("stationunkid")) = mintStationunkid AndAlso CInt(dsList.Tables(0).Rows(0)("deptgroupunkid")) = mintDeptgroupunkid _
              AndAlso CInt(dsList.Tables(0).Rows(0)("departmentunkid")) = mintDepartmentunkid AndAlso CInt(dsList.Tables(0).Rows(0)("sectiongroupunkid")) = mintSectiongroupunkid _
              AndAlso CInt(dsList.Tables(0).Rows(0)("sectionunkid")) = mintSectionunkid AndAlso CInt(dsList.Tables(0).Rows(0)("unitgroupunkid")) = mintUnitgroupunkid _
              AndAlso CInt(dsList.Tables(0).Rows(0)("unitunkid")) = mintUnitunkid AndAlso CInt(dsList.Tables(0).Rows(0)("teamunkid")) = mintTeamunkid _
              AndAlso CInt(dsList.Tables(0).Rows(0)("classgroupunkid")) = mintClassgroupunkid AndAlso CInt(dsList.Tables(0).Rows(0)("classunkid")) = mintClassunkid Then
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, transfer information is already present for the selected combination for the selected effective date.")
                Return False
            End If
        End If
        dsList = Nothing
        End If
        'S.SANDEEP [29 APR 2015] -- END

        'Pinkal (09-Apr-2015) -- End


        If isExist(mdtEffectivedate, mintStationunkid, mintDeptgroupunkid, _
                   mintDepartmentunkid, mintSectiongroupunkid, mintSectionunkid, _
                   mintUnitgroupunkid, mintUnitunkid, mintTeamunkid, mintClassgroupunkid, mintClassunkid, mintEmployeeunkid, , objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, transfer information is already present for the selected combination for the selected effective date.")
            Return False
        End If

        If xDataOpr Is Nothing Then
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeptgroupunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectiongroupunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitgroupunkid.ToString)
            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitunkid.ToString)
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassgroupunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromemployee.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO hremployee_transfer_tran ( " & _
                       "  effectivedate " & _
                       ", employeeunkid " & _
                       ", rehiretranunkid " & _
                       ", stationunkid " & _
                       ", deptgroupunkid " & _
                       ", departmentunkid " & _
                       ", sectiongroupunkid " & _
                       ", sectionunkid " & _
                       ", unitgroupunkid " & _
                       ", unitunkid " & _
                       ", teamunkid " & _
                       ", classgroupunkid " & _
                       ", classunkid " & _
                       ", changereasonunkid " & _
                       ", isfromemployee " & _
                       ", userunkid " & _
                       ", statusunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                   ") VALUES (" & _
                       "  @effectivedate " & _
                       ", @employeeunkid " & _
                       ", @rehiretranunkid " & _
                       ", @stationunkid " & _
                       ", @deptgroupunkid " & _
                       ", @departmentunkid " & _
                       ", @sectiongroupunkid " & _
                       ", @sectionunkid " & _
                       ", @unitgroupunkid " & _
                       ", @unitunkid " & _
                       ", @teamunkid " & _
                       ", @classgroupunkid " & _
                       ", @classunkid " & _
                       ", @changereasonunkid " & _
                       ", @isfromemployee " & _
                       ", @userunkid " & _
                       ", @statusunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason " & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTransferunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailForTransfers(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [10-MAY-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

            'S.SANDEEP [19-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : STOP SYMMETRY REAL TIME (RUTTA'S REQUEST)
            'If Integrate_Symmetry(objDataOperation, intCompanyId, mintEmployeeunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [19-JUL-2017] -- END

            'S.SANDEEP [10-MAY-2017] -- END

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.


            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
            'If MigrationInsert(intCompanyId, clsEmployeeMovmentApproval.enOperationType.ADDED, mblnCreateADUserFromEmpMst, objDataOperation, xMdtMigration, xblnFromApproval, xblnFromApprovalScreen) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            If MigrationInsert(intCompanyId, clsEmployeeMovmentApproval.enOperationType.ADDED, mblnCreateADUserFromEmpMst, xDatabaseName _
                                   , objDataOperation, xMdtMigration, xblnFromApproval, xblnFromApprovalScreen) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Pinkal (12-Oct-2020) -- End



            'If IsNothing(xMdtMigration) = False AndAlso xMdtMigration.Rows.Count > 0 Then
            '    Dim objMovementMigration As New clsMovementMigration

            '    Dim objConfig As New clsConfigOptions
            '    Dim strParamKeys() As String = { _
            '                               "EMPLOYEEASONDATE", _
            '                               "CLAIMREQUEST_PAYMENTAPPROVALWITHLEAVEAPPROVAL" _
            '                               }
            '    Dim MigrationDicKeyValues As New Dictionary(Of String, String)
            '    MigrationDicKeyValues = objConfig.GetKeyValue(intCompanyId, strParamKeys, objDataOperation)

            '    If IsNothing(MigrationDicKeyValues) = False _
            '    AndAlso MigrationDicKeyValues.ContainsKey("EmployeeAsOnDate") _
            '    AndAlso MigrationDicKeyValues.ContainsKey("ClaimRequest_PaymentApprovalwithLeaveApproval") Then
            '        Dim Formname As String = String.Empty
            '        If mstrWebFormName.Trim.Length <= 0 Then
            '            Formname = mstrForm_Name
            '        Else
            '            Formname = mstrWebFormName
            '        End If


            '        If IsNothing(xMdtMigration) = True Then
            '            objMovementMigration.isMovementInApprovalFlow(objMovementMigration._DataList, mintEmployeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, _
            '                                                          enApprovalMigrationScreenType.TRANSFER, 0, objDataOperation)

            '            If IsNothing(objMovementMigration._DataList) = False Then
            '                xMdtMigration = objMovementMigration._DataList
            '            End If

            '        End If


            '        If objMovementMigration.Insert(xMdtMigration, mintTransferunkid, mintEmployeeunkid, CInt(enApprovalMigrationScreenType.TRANSFER), _
            '                                 False, MigrationDicKeyValues("EmployeeAsOnDate").ToString(), _
            '                                CBool(MigrationDicKeyValues("ClaimRequest_PaymentApprovalwithLeaveApproval").ToString()), mintUserunkid, Formname, _
            '                                CInt(clsEmployeeMovmentApproval.enOperationType.ADDED), xblnFromApproval, objDataOperation) Then
            '        End If
            '    End If
            'End If

            'Gajanan [23-SEP-2019] -- End



            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If



            'Pinkal (07-Dec-2019) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst Then
                strQ = "SELECT @Date = GETDATE()"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@Date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCurrentDate, ParameterDirection.Output)
                objDataOperation.ExecNonQuery(strQ)
                mdtCurrentDate = CDate(objDataOperation.GetParameterValue("@Date"))

                If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
                    'Pinkal (04-Apr-2020) -- Start
                    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                    'If SetADEmployeeDepartment(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
                    If SetADEmployeeDepartment(objDataOperation, intCompanyId, mintEmployeeunkid, mdtCurrentDate, False) = False Then
                        'Pinkal (04-Apr-2020) -- End
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Else
                    If SetADEmployeeDepartment(objDataOperation, intCompanyId, mintEmployeeunkid, mdtEffectivedate, True) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (07-Dec-2019) -- End

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If

            'Pinkal (04-Apr-2020) -- Start
                    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

            'If mblnCreateADUserFromEmpMst Then
            '    If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
            '        'If SetADEmployeeDepartment(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
            '        If SetADEmployeeDepartment(objDataOperation, intCompanyId, mintEmployeeunkid, mdtCurrentDate) = False Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    End If
            'End If

            If ex.Message.ToString().Contains("Sorry, record cannot be saved. Some allocation does not exist in active directory.") Then
                mstrMessage = ex.Message
                Throw New Exception(ex.Message)
            ElseIf ex.Message.ToString().Contains("System cannot connect with Active Directory at the moment. Please check Active Directory connection.  Also username or password (Aruti to AD Server) may be incorrect.") Then
                mstrMessage = ex.Message
                Throw New Exception(ex.Message)
            Else
                Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
                    End If
            'Pinkal (04-Apr-2020) -- End
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_transfer_tran) </purpose>
    Public Function Update(ByVal mblnCreateADUserFromEmpMst As Boolean, ByVal intCompanyId As Integer, _
                           ByVal xDataBaseName As String, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal xMdtMigration As DataTable = Nothing, _
                           Optional ByVal xblnFromApproval As Boolean = False, _
                           Optional ByVal xblnFromApprovalScreen As Boolean = False) As Boolean 'S.SANDEEP [10-MAY-2017] -- START {intCompanyId} -- END


        'Pinkal (12-Oct-2020) -- Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.[ ByVal xDataBaseName As String]

        'Pinkal (12-Oct-2020) -- End

            'Pinkal (18-Aug-2018) -- Start/End
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

        'Gajanan [23-SEP-2019] -- Add {xMdtMigration,xblnFromApproval,xblnFromApprovalScreen}

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If


        If isExist(mdtEffectivedate, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, mintEmployeeunkid, mintTransferunkid, objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, transfer information is already present for the selected effective date.")
            Return False
        End If

        If isExist(mdtEffectivedate, mintStationunkid, mintDeptgroupunkid, _
                   mintDepartmentunkid, mintSectiongroupunkid, mintSectionunkid, _
                   mintUnitgroupunkid, mintUnitunkid, mintTeamunkid, mintClassgroupunkid, mintClassunkid, mintEmployeeunkid, mintTransferunkid, objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, transfer information is already present for the selected combination for the selected effective date.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        Dim mdtCurrentDate As Date = Nothing
        'Pinkal (18-Aug-2018) -- End

        If xDataOpr Is Nothing Then
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@transferunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeptgroupunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectiongroupunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitgroupunkid.ToString)
            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitunkid.ToString)
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassgroupunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromemployee.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE hremployee_transfer_tran SET " & _
                   "  effectivedate = @effectivedate" & _
                   ", employeeunkid = @employeeunkid" & _
                   ", rehiretranunkid = @rehiretranunkid " & _
                   ", stationunkid = @stationunkid" & _
                   ", deptgroupunkid = @deptgroupunkid" & _
                   ", departmentunkid = @departmentunkid" & _
                   ", sectiongroupunkid = @sectiongroupunkid" & _
                   ", sectionunkid = @sectionunkid" & _
                   ", unitgroupunkid = @unitgroupunkid" & _
                   ", unitunkid = @unitunkid" & _
                   ", teamunkid = @teamunkid" & _
                   ", classgroupunkid = @classgroupunkid" & _
                   ", classunkid = @classunkid" & _
                   ", changereasonunkid = @changereasonunkid" & _
                   ", isfromemployee = @isfromemployee" & _
                   ", userunkid = @userunkid" & _
                   ", statusunkid = @statusunkid" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE transferunkid = @transferunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForTransfers(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [10-MAY-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

            'S.SANDEEP [19-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : STOP SYMMETRY REAL TIME (RUTTA'S REQUEST)
            'If Integrate_Symmetry(objDataOperation, intCompanyId, mintEmployeeunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [19-JUL-2017] -- END

            'S.SANDEEP [10-MAY-2017] -- END


            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.


            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

            'If MigrationInsert(intCompanyId, clsEmployeeMovmentApproval.enOperationType.EDITED, mblnCreateADUserFromEmpMst, _
            '                        objDataOperation, xMdtMigration, xblnFromApproval, xblnFromApprovalScreen) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            If MigrationInsert(intCompanyId, clsEmployeeMovmentApproval.enOperationType.EDITED, mblnCreateADUserFromEmpMst _
                                   , xDataBaseName, objDataOperation, xMdtMigration, xblnFromApproval, xblnFromApprovalScreen) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (12-Oct-2020) -- End

            'Gajanan [23-SEP-2019] -- End

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If


            'Pinkal (07-Dec-2019) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst Then
                strQ = "SELECT @Date = GETDATE()"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@Date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCurrentDate, ParameterDirection.Output)
                objDataOperation.ExecNonQuery(strQ)
                mdtCurrentDate = CDate(objDataOperation.GetParameterValue("@Date"))

                If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
                    'Pinkal (04-Apr-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                'If SetADEmployeeDepartment(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
                    If SetADEmployeeDepartment(objDataOperation, intCompanyId, mintEmployeeunkid, mdtCurrentDate, False) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Else
                    If SetADEmployeeDepartment(objDataOperation, intCompanyId, mintEmployeeunkid, mdtEffectivedate, True) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                End If
                'Pinkal (04-Apr-2020) -- End
            End If
           'Pinkal (07-Dec-2019) -- End

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If

            'Pinkal (04-Apr-2020) -- Start
                    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

            'If mblnCreateADUserFromEmpMst Then
            '    If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
            '        'If SetADEmployeeDepartment(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
            '        If SetADEmployeeDepartment(objDataOperation, intCompanyId, mintEmployeeunkid, mdtCurrentDate) = False Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    End If
            'End If

            If ex.Message.ToString().Contains("Sorry, record cannot be saved. Some allocation does not exist in active directory.") Then
                mstrMessage = ex.Message
                Throw New Exception(ex.Message)
            ElseIf ex.Message.ToString().Contains("System cannot connect with Active Directory at the moment. Please check Active Directory connection.  Also username or password (Aruti to AD Server) may be incorrect.") Then
                mstrMessage = ex.Message
                Throw New Exception(ex.Message)
            Else
                Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
                    End If
            'Pinkal (04-Apr-2020) -- End

            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployee_transfer_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal intCompanyId As Integer, ByVal mblnCreateADUserFromEmpMst As Boolean, _
                           ByVal xDataBaseName As String, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal xMdtMigration As DataTable = Nothing, _
                           Optional ByVal xblnFromApproval As Boolean = False, _
                           Optional ByVal xblnFromApprovalScreen As Boolean = False) As Boolean 'S.SANDEEP [01 JUN 2016] -- START -- END


        'Pinkal (12-Oct-2020) -- Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.[  ByVal xDataBaseName As String]

        'Pinkal (18-Aug-2018) -- 'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[ByVal mblnCreateADUserFromEmpMst As Boolean]

        'Gajanan [23-SEP-2019] -- Add {xMdtMigration,xblnFromApproval,xblnFromApprovalScreen}

        'S.SANDEEP [10-MAY-2017] -- START {intCompanyId} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        Dim mdtCurrentDate As Date = Nothing
        'Pinkal (18-Aug-2018) -- End

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "UPDATE hremployee_transfer_tran SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE transferunkid = @transferunkid "

            objDataOperation.AddParameter("@transferunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ClearParameters()
            mintTransferunkid = intUnkid
            Call GetData(objDataOperation)

            If InsertAuditTrailForTransfers(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [10-MAY-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

            'S.SANDEEP [19-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : STOP SYMMETRY REAL TIME (RUTTA'S REQUEST)
            'If Integrate_Symmetry(objDataOperation, intCompanyId, mintEmployeeunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [19-JUL-2017] -- END

            'S.SANDEEP [10-MAY-2017] -- END


            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.


            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
            'If MigrationInsert(intCompanyId, clsEmployeeMovmentApproval.enOperationType.DELETED, ConfigParameter._Object._CreateADUserFromEmpMst _
            '                     , objDataOperation, xMdtMigration, xblnFromApproval, xblnFromApprovalScreen) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            If MigrationInsert(intCompanyId, clsEmployeeMovmentApproval.enOperationType.DELETED, ConfigParameter._Object._CreateADUserFromEmpMst _
                              , xDataBaseName, objDataOperation, xMdtMigration, xblnFromApproval, xblnFromApprovalScreen) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Pinkal (12-Oct-2020) -- End



            'Gajanan [23-SEP-2019] -- End


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If


            'Pinkal (07-Dec-2019) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst Then
                strQ = "SELECT @Date = GETDATE()"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@Date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCurrentDate, ParameterDirection.Output)
                objDataOperation.ExecNonQuery(strQ)
                mdtCurrentDate = CDate(objDataOperation.GetParameterValue("@Date"))

                'Pinkal (04-Apr-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
                    'If SetADEmployeeDepartment(objDataOperation, intCompanyId, mintEmployeeunkid, mdtCurrentDate) = False Then
                    If SetADEmployeeDepartment(objDataOperation, intCompanyId, mintEmployeeunkid, mdtCurrentDate, False) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Else
                    If SetADEmployeeDepartment(objDataOperation, intCompanyId, mintEmployeeunkid, mdtEffectivedate, True) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                End If
                'Pinkal (04-Apr-2020) -- End
            End If
            'Pinkal (07-Dec-2019) -- End

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)

            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

            'If mblnCreateADUserFromEmpMst Then
            '    'If SetADEmployeeDepartment(objDataOperation, intCompanyId, mintEmployeeunkid, mdtCurrentDate) = False Then
            '    If SetADEmployeeDepartment(objDataOperation, intCompanyId, mintEmployeeunkid, mdtCurrentDate) = False Then
            '        If ex.Message.ToString().Contains("Sorry, record cannot be saved. Some allocation does not exist in active directory.") Then
            '            mstrMessage = ex.Message
            '        Else
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    End If
            'End If

            If ex.Message.ToString().Contains("Sorry, record cannot be saved. Some allocation does not exist in active directory.") Then
                mstrMessage = ex.Message
                Throw New Exception(ex.Message)
            ElseIf ex.Message.ToString().Contains("System cannot connect with Active Directory at the moment. Please check Active Directory connection.  Also username or password (Aruti to AD Server) may be incorrect.") Then
                mstrMessage = ex.Message
                Throw New Exception(ex.Message)
            Else
                Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            End If


            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            'If mblnCreateADUserFromEmpMst Then
            '    If mdtEffectivedate.Date <= mdtCurrentDate.Date Then

            '        'Pinkal (09-Mar-2020) -- Start
            '        'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            '        'If SetADEmployeeDepartment(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
            '        If SetADEmployeeDepartment(objDataOperation, intCompanyId, mintEmployeeunkid, mdtCurrentDate) = False Then
            '            'Pinkal (09-Mar-2020) -- End
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    End If
            'End If
            'Pinkal (18-Aug-2018) -- End


            'Pinkal (04-Apr-2020) -- End
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xEffDate As DateTime, _
                            ByVal xStationId As Integer, _
                            ByVal xDeptGrpId As Integer, _
                            ByVal xDeptUnkid As Integer, _
                            ByVal xSecGrpId As Integer, _
                            ByVal xSecUnkid As Integer, _
                            ByVal xUnitGrpId As Integer, _
                            ByVal xUnitUnkid As Integer, _
                            ByVal xTeamUnkid As Integer, _
                            ByVal xClassGrpId As Integer, _
                            ByVal xClassUnkid As Integer, _
                            ByVal xEmployeeId As Integer, _
                            Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If
        Try
            strQ = "SELECT " & _
              "  transferunkid " & _
              ", effectivedate " & _
              ", employeeunkid " & _
              ", rehiretranunkid " & _
              ", stationunkid " & _
              ", deptgroupunkid " & _
              ", departmentunkid " & _
              ", sectiongroupunkid " & _
              ", sectionunkid " & _
              ", unitgroupunkid " & _
              ", unitunkid " & _
              ", teamunkid " & _
              ", classgroupunkid " & _
              ", classunkid " & _
              ", changereasonunkid " & _
              ", userunkid " & _
              ", statusunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hremployee_transfer_tran " & _
             "WHERE CONVERT(CHAR(8),effectivedate,112) = @effectivedate AND isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND transferunkid <> @transferunkid "
            End If

            If xEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            If xStationId > 0 Then
                strQ &= " AND stationunkid = @stationunkid "
                objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xStationId)
            End If

            If xDeptGrpId > 0 Then
                strQ &= " AND deptgroupunkid = @deptgroupunkid "
                objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xDeptGrpId)
            End If

            If xDeptUnkid > 0 Then
                strQ &= " AND departmentunkid = @departmentunkid "
                objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xDeptUnkid)
            End If

            If xSecGrpId > 0 Then
                strQ &= " AND sectiongroupunkid = @sectiongroupunkid "
                objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xSecGrpId)
            End If

            If xSecUnkid > 0 Then
                strQ &= " AND sectionunkid = @sectionunkid "
                objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xSecUnkid)
            End If

            If xUnitGrpId > 0 Then
                strQ &= " AND unitgroupunkid = @unitgroupunkid "
                objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUnitGrpId)
            End If

            If xUnitUnkid > 0 Then
                strQ &= " AND unitunkid = @unitunkid "
                objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUnitUnkid)
            End If

            If xTeamUnkid > 0 Then
                strQ &= " AND teamunkid = @teamunkid "
                objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTeamUnkid)
            End If

            If xClassGrpId > 0 Then
                strQ &= " AND classgroupunkid = @classgroupunkid "
                objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xClassGrpId)
            End If

            If xClassUnkid > 0 Then
                strQ &= " AND classunkid = @classunkid "
                objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xClassUnkid)
            End If

            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xEffDate))
            objDataOperation.AddParameter("@transferunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Private Function InsertAuditTrailForTransfers(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As enAuditType) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            StrQ = "INSERT INTO athremployee_transfer_tran ( " & _
                              "  transferunkid " & _
                              ", effectivedate " & _
                              ", employeeunkid " & _
                              ", rehiretranunkid " & _
                              ", stationunkid " & _
                              ", deptgroupunkid " & _
                              ", departmentunkid " & _
                              ", sectiongroupunkid " & _
                              ", sectionunkid " & _
                              ", unitgroupunkid " & _
                              ", unitunkid " & _
                              ", teamunkid " & _
                              ", classgroupunkid " & _
                              ", classunkid " & _
                              ", changereasonunkid " & _
                              ", isfromemployee " & _
                              ", statusunkid " & _
                              ", audittype " & _
                              ", audituserunkid " & _
                              ", auditdatetime " & _
                              ", ip " & _
                              ", machine_name " & _
                              ", form_name " & _
                              ", module_name1 " & _
                              ", module_name2 " & _
                              ", module_name3 " & _
                              ", module_name4 " & _
                              ", module_name5 " & _
                              ", isweb" & _
                          ") VALUES (" & _
                              "  @transferunkid " & _
                              ", @effectivedate " & _
                              ", @employeeunkid " & _
                              ", @rehiretranunkid " & _
                              ", @stationunkid " & _
                              ", @deptgroupunkid " & _
                              ", @departmentunkid " & _
                              ", @sectiongroupunkid " & _
                              ", @sectionunkid " & _
                              ", @unitgroupunkid " & _
                              ", @unitunkid " & _
                              ", @teamunkid " & _
                              ", @classgroupunkid " & _
                              ", @classunkid " & _
                              ", @changereasonunkid " & _
                              ", @isfromemployee " & _
                              ", @statusunkid " & _
                              ", @audittype " & _
                              ", @audituserunkid " & _
                              ", @auditdatetime " & _
                              ", @ip " & _
                              ", @machine_name " & _
                              ", @form_name " & _
                              ", @module_name1 " & _
                              ", @module_name2 " & _
                              ", @module_name3 " & _
                              ", @module_name4 " & _
                              ", @module_name5 " & _
                              ", @isweb" & _
                          ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@transferunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeptgroupunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectiongroupunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitgroupunkid.ToString)
            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitunkid.ToString)
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassgroupunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromemployee.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebClientIP.Trim = "", getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim = "", getHostName, mstrWebHostName))
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 3, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailsForTransfers; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Get_Current_Allocation(ByVal xDate As Date, Optional ByVal xEmployeeId As Integer = 0, Optional ByVal objDoOperation As clsDataOperation = Nothing) As DataSet

        'Pinkal (18-Aug-2018) --  'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[Optional ByVal objDoOperation As clsDataOperation = Nothing]

        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim mdtAppDate As String = ""
        Try

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            'Using objDo As New clsDataOperation
            Dim objDo As clsDataOperation
            If objDoOperation Is Nothing Then
                objDo = New clsDataOperation
            Else
                objDo = objDoOperation
            End If
            objDo.ClearParameters()
            'Pinkal (18-Aug-2018) -- End


                If xEmployeeId > 0 Then
                    StrQ = "SELECT " & _
                           "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                           "FROM hremployee_master " & _
                           "WHERE employeeunkid = '" & xEmployeeId & "' "

                    objDo.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

                    objDo.ExecNonQuery(StrQ)

                    If objDo.ErrorMessage <> "" Then
                        Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    End If

                    mdtAppDate = objDo.GetParameterValue("@ADate")
                End If
                'S.SANDEEP [07 APR 2015] -- END

                StrQ = "SELECT " & _
                       "     effectivedate " & _
                       "    ,employeeunkid " & _
                       "    ,stationunkid " & _
                       "    ,deptgroupunkid " & _
                       "    ,departmentunkid " & _
                       "    ,sectiongroupunkid " & _
                       "    ,sectionunkid " & _
                       "    ,unitgroupunkid " & _
                       "    ,unitunkid " & _
                       "    ,teamunkid " & _
                       "    ,classgroupunkid " & _
                       "    ,classunkid " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT " & _
                       "         effectivedate " & _
                       "        ,employeeunkid " & _
                       "        ,stationunkid " & _
                       "        ,deptgroupunkid " & _
                       "        ,departmentunkid " & _
                       "        ,sectiongroupunkid " & _
                       "        ,sectionunkid " & _
                       "        ,unitgroupunkid " & _
                       "        ,unitunkid " & _
                       "        ,teamunkid " & _
                       "        ,classgroupunkid " & _
                       "        ,classunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                       "    FROM hremployee_transfer_tran " & _
                       "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effdate "
                If xEmployeeId > 0 Then
                    StrQ &= " AND employeeunkid = '" & xEmployeeId & "' "
                End If
                StrQ &= ") AS CA WHERE CA.xNo = 1 "

                'S.SANDEEP [07 APR 2015] -- START
                If mdtAppDate.Trim.Length > 0 Then
                    StrQ &= " AND CONVERT(CHAR(8),effectivedate,112) >= '" & mdtAppDate & "' "
                End If
                'S.SANDEEP [07 APR 2015] -- END

                objDo.AddParameter("@effdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate))

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If
            'End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Current_Allocation; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Private Function Set_Notification_Allocation(ByVal StrUserName As String, _
                                                 ByVal xEmpId As Integer, _
                                                 ByVal xEcode As String, _
                                                 ByVal xEmployeeName As String, _
                                                 ByVal xCurrentAlloc As Dictionary(Of Integer, String), _
                                                 ByVal xNotifAlloc As String, _
                                                 ByVal xEffectiveDate As Date, _
                                                 Optional ByVal xHost As String = "", _
                                                 Optional ByVal xIP As String = "", Optional ByVal xCurr_User As String = "") As String

        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Try

            If xCurrentAlloc.Keys.Count <= 0 Then Return ""

            If xNotifAlloc.Trim.Length <= 0 Then
                xNotifAlloc = ConfigParameter._Object._Notify_Allocation
            End If

            Dim xOldAlloc As New DataSet
            Dim xOldDate As Date = DateAdd(DateInterval.Day, -1, xEffectiveDate)
            xOldAlloc = Get_Current_Allocation(xOldDate, xEmpId)

            If xOldAlloc.Tables(0).Rows.Count <= 0 Then
                Dim xRow As DataRow = xOldAlloc.Tables(0).NewRow

                xRow.Item("effectivedate") = xOldDate
                xRow.Item("employeeunkid") = xEmpId
                xRow.Item("stationunkid") = 0
                xRow.Item("deptgroupunkid") = 0
                xRow.Item("departmentunkid") = 0
                xRow.Item("sectiongroupunkid") = 0
                xRow.Item("sectionunkid") = 0
                xRow.Item("unitgroupunkid") = 0
                xRow.Item("unitunkid") = 0
                xRow.Item("teamunkid") = 0
                xRow.Item("classgroupunkid") = 0
                xRow.Item("classunkid") = 0

                xOldAlloc.Tables(0).Rows.Add(xRow)
            End If


            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            If xNotifAlloc.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
                StrMessage.Append(Language.getMessage("frmEmployeeMaster", 147, "Dear") & " " & "<b>" & getTitleCase(StrUserName) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that changes have been made for employee : <b>" & xEmployeeName & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 142, "This is to inform you that changes have been made for employee") & " <b>" & getTitleCase(xEmployeeName) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & xEcode & "</b>. Following information has been changed by user : <b>" & IIf(xCurr_User.Trim = "", User._Object._Firstname & " " & User._Object._Lastname, xCurr_User) & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 143, "with employeecode") & " <b>" & xEcode & "</b>." & " " & Language.getMessage("frmEmployeeMaster", 144, "Following information has been changed by user") & " " & "<b>" & " " & getTitleCase(IIf(xCurr_User.Trim = "", User._Object._Firstname & " " & User._Object._Lastname, xCurr_User)) & "</b></span></p>")

                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & IIf(xHost.Trim = "", getHostName.ToString, xHost) & "</b> and IPAddress : <b>" & IIf(xIP.Trim = "", getIP.ToString, xIP) & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 145, "from Machine") & " " & "<b>" & getHostName.ToString & "</b>" & " " & Language.getMessage("frmEmployeeMaster", 146, "and IPAddress") & " " & "<b>" & getIP.ToString & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append(vbCrLf)

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'Gajanan (21 Nov 2018) -- Start
                'StrMessage.Append("<TABLE border = '1' WIDTH = '50%' style='margin-left: 25px'>")
                StrMessage.Append("<TABLE border = '1' WIDTH = '50%'>")
                'Gajanan (21 Nov 2018) -- End
                'Gajanan [27-Mar-2019] -- End
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 5, "Effective Date") & "</span></b></TD>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & xEffectiveDate.ToShortDateString & "</span></b></TD>")
                StrMessage.Append("</TR>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("</TABLE>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<BR>")
                'Gajanan (21 Nov 2018) -- Start
                StrMessage.Append("<TABLE border = '1' WIDTH = '90%' style='margin-left: 25px'>")
                'Gajanan (21 Nov 2018) -- End
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TR WIDTH = '90%' bgcolor= 'SteelBlue'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 6, "Allocations") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:35%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 7, "Old Allocation") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:35%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 8, "New Allocation") & "</span></b></TD>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("</TR>")
                For Each sId As String In xNotifAlloc.Split(CChar("||"))(0).Split(CChar(","))
                    Select Case CInt(sId)
                        Case enAllocation.BRANCH
                            Dim ObjStation As New clsStation
                            If xCurrentAlloc.ContainsKey(CInt(sId)) = True Then
                                ObjStation._Stationunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("stationunkid"))
                                StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 430, "Branch") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjStation._Name.Trim = "", "&nbsp;", ObjStation._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentAlloc(CInt(sId)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.DEPARTMENT_GROUP
                            Dim ObjDG As New clsDepartmentGroup
                            If xCurrentAlloc.ContainsKey(CInt(sId)) = True Then
                                ObjDG._Deptgroupunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("deptgroupunkid"))
                                StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 429, "Department Group") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjDG._Name.Trim = "", "&nbsp;", ObjDG._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentAlloc(CInt(sId)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.DEPARTMENT
                            Dim ObjDept As New clsDepartment
                            If xCurrentAlloc.ContainsKey(CInt(sId)) = True Then
                                ObjDept._Departmentunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("departmentunkid"))
                                StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 428, "Department") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjDept._Name.Trim = "", "&nbsp;", ObjDept._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentAlloc(CInt(sId)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.SECTION_GROUP
                            Dim ObjSecGrp As New clsSectionGroup
                            If xCurrentAlloc.ContainsKey(CInt(sId)) = True Then
                                ObjSecGrp._Sectiongroupunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("sectiongroupunkid"))
                                StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 427, "Section Group") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjSecGrp._Name.Trim = "", "&nbsp;", ObjSecGrp._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentAlloc(CInt(sId)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.SECTION
                            Dim ObjSec As New clsSections
                            If xCurrentAlloc.ContainsKey(CInt(sId)) = True Then
                                ObjSec._Sectionunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("sectionunkid"))
                                StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 426, "Section") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjSec._Name.Trim = "", "&nbsp;", ObjSec._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentAlloc(CInt(sId)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.UNIT_GROUP
                            Dim ObjUG As New clsUnitGroup
                            If xCurrentAlloc.ContainsKey(CInt(sId)) = True Then
                                ObjUG._Unitgroupunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("unitgroupunkid"))
                                StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 425, "Unit Group") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjUG._Name.Trim = "", "&nbsp;", ObjUG._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentAlloc(CInt(sId)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.UNIT
                            Dim ObjUnit As New clsUnits
                            If xCurrentAlloc.ContainsKey(CInt(sId)) = True Then
                                ObjUnit._Unitunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("unitunkid"))
                                StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 424, "Unit") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjUnit._Name.Trim = "", "&nbsp;", ObjUnit._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentAlloc(CInt(sId)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.TEAM
                            Dim ObjTeam As New clsTeams
                            If xCurrentAlloc.ContainsKey(CInt(sId)) = True Then
                                ObjTeam._Teamunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("teamunkid"))
                                StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 423, "Team") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjTeam._Name.Trim = "", "&nbsp;", ObjTeam._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentAlloc(CInt(sId)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.CLASS_GROUP
                            Dim ObjCG As New clsClassGroup
                            If xCurrentAlloc.ContainsKey(CInt(sId)) = True Then
                                ObjCG._Classgroupunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("classgroupunkid"))
                                StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 420, "Class Group") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjCG._Name.Trim = "", "&nbsp;", ObjCG._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentAlloc(CInt(sId)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.CLASSES
                            Dim ObjClass As New clsClass
                            If xCurrentAlloc.ContainsKey(CInt(sId)) = True Then
                                ObjClass._Classesunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("classunkid"))
                                StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 419, "Classes") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjClass._Name.Trim = "", "&nbsp;", ObjClass._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentAlloc(CInt(sId)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                    End Select
                Next
            End If
            StrMessage.Append("</TABLE>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language

                'Gajanan (21 Nov 2018) -- Start
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")

            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")

                'Gajanan (21 Nov 2018) -- End
            'Gajanan [27-Mar-2019] -- End
            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            If blnFlag = False Then
                StrMessage = StrMessage.Remove(0, StrMessage.Length)
            End If

            Return StrMessage.ToString

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Set_Notification_Allocation; Module Name: " & mstrModuleName)
            Return ""
        End Try
    End Function

    Public Sub SendEmails(ByVal xEmployeeId As Integer, _
                          ByVal xCode As String, _
                          ByVal xEName As String, _
                          ByVal xDict_CurrentAlloc As Dictionary(Of Integer, String), _
                          ByVal xConfigAllocNotif As String, _
                          ByVal xEffDate As Date, _
                          ByVal intCompanyUnkId As Integer, _
                          Optional ByVal xHostName As String = "", _
                          Optional ByVal xIPAddr As String = "", _
                          Optional ByVal xLoggedUserName As String = "", _
                          Optional ByVal xUserId As Integer = 0, _
                          Optional ByVal xLoginMod As enLogin_Mode = enLogin_Mode.DESKTOP)
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Try
            If xConfigAllocNotif.Trim.Length <= 0 Then xConfigAllocNotif = ConfigParameter._Object._Notify_Allocation
            If xConfigAllocNotif.Trim.Length > 0 Then
                Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                For Each sId As String In xConfigAllocNotif.Split(CChar("||"))(2).Split(CChar(","))
                    objUsr._Userunkid = CInt(sId)
                    StrMessage = Set_Notification_Allocation(objUsr._Firstname & " " & objUsr._Lastname, xEmployeeId, xCode, xEName, xDict_CurrentAlloc, xConfigAllocNotif, xEffDate, xHostName, xIPAddr, xLoggedUserName)
                    If StrMessage <> "" Then
                        Dim objSendMail As New clsSendMail

                        objSendMail._ToEmail = objUsr._Email
                        objSendMail._Subject = Language.getMessage(mstrModuleName, 4, "Notification of Changes in Employee Master file")
                        objSendMail._Message = StrMessage
                        objSendMail._Form_Name = ""
                        objSendMail._LogEmployeeUnkid = 0
                        objSendMail._OperationModeId = xLoginMod
                        objSendMail._UserUnkid = xUserId
                        objSendMail._SenderAddress = objUsr._Firstname & " " & objUsr._Lastname
                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                        Try
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(intCompanyUnkId)
                            'Sohail (30 Nov 2017) -- End
                        Catch ex As Exception
                        End Try
                        objSendMail = Nothing
                        'gobjEmailList.Add(New clsEmailCollection(objUsr._Email, _
                        '                                         Language.getMessage(mstrModuleName, 4, "Notification of Changes in Employee Master file"), _
                        '                                         StrMessage, _
                        '                                         "", _
                        '                                         0, _
                        '                                         "", _
                        '                                         "", _
                        '                                         xUserId, _
                        '                                         xLoginMod, _
                        '                                         clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT, _
                        '                                         objUsr._Firstname & " " & objUsr._Lastname))
                    End If
                Next
                objUsr = Nothing
                'trd = New Thread(AddressOf Send_Notification)
                'trd.IsBackground = True
                'trd.Start()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendEmails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Send_Notification(ByVal intCompanyUnkId As Integer)
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Try
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId
                    Try
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        objSendMail.SendMail(intCompanyUnkId)
                        'Sohail (30 Nov 2017) -- End
                    Catch ex As Exception
                    End Try
                Next
                gobjEmailList.Clear()
            End If
        Catch ex As Exception
            Throw New Exception(mstrModuleName & ":Send_Notification:- " & ex.Message)
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub

    Public Function IsTransferPresent(ByVal xEmployeeId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim blnFlag As Boolean = False
        Dim StrQ As String = String.Empty
        Dim iCnt As Integer = 0
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            StrQ = "SELECT 1 FROM hremployee_transfer_tran WHERE employeeunkid = '" & xEmployeeId & "'  AND isvoid = 0 "

            iCnt = objDataOperation.RecordCount(StrQ)

            If iCnt > 0 Then
                blnFlag = True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTransferPresent; Module Name: " & mstrModuleName)
        Finally
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return blnFlag
    End Function

    'S.SANDEEP [10-MAY-2017] -- START
    'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION
    Private Function Integrate_Symmetry(ByVal xDataOpr As clsDataOperation, ByVal intCompanyId As Integer, ByVal intEmpId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsDetails As New DataSet
        Dim exForce As Exception = Nothing
        Dim dtEmpAsOnDate As String = ""
        Dim dtServerDate As Date
        Dim blnIsSymmetryIntegrated As Boolean = False
        Try
            If intCompanyId > 0 Then
                StrQ = "SELECT " & _
                       "  @issymmetryintegrated = cfconfiguration.key_value " & _
                       "FROM hrmsConfiguration..cfconfiguration " & _
                       "WHERE UPPER(cfconfiguration.key_name) = 'ISSYMMETRYINTEGRATED' " & _
                       "AND cfconfiguration.companyunkid = @companyunkid "
                With xDataOpr
                    .ClearParameters()
                    .AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
                    .AddParameter("@issymmetryintegrated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsSymmetryIntegrated, ParameterDirection.Output)

                    .ExecNonQuery(StrQ)
                    If .ErrorMessage <> "" Then
                        exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                        Throw exForce
                    End If
                    'S.SANDEEP [12-JUN-2017] -- START
                    'ISSUE/ENHANCEMENT : SYMMETRY DBNULL TO BOOLEAN ERROR
                    'blnIsSymmetryIntegrated = .GetParameterValue("@issymmetryintegrated")
                    If IsDBNull(.GetParameterValue("@issymmetryintegrated")) = False Then
                    blnIsSymmetryIntegrated = .GetParameterValue("@issymmetryintegrated")
                    End If
                    'S.SANDEEP [12-JUN-2017] -- END
                End With

                If blnIsSymmetryIntegrated Then
                    Dim mstrEmpCode As String = String.Empty
                    StrQ = "SELECT @employeecode = employeecode,@serverdate = GETDATE() FROM hremployee_master WHERE employeeunkid = @employeeunkid "
                    With xDataOpr
                        .ClearParameters()
                        .AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
                        .AddParameter("@employeecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpCode, ParameterDirection.InputOutput)
                        .AddParameter("@ServerDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtServerDate, ParameterDirection.Output)
                        .ExecNonQuery(StrQ)
                        If .ErrorMessage <> "" Then
                            exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                        mstrEmpCode = .GetParameterValue("@employeecode")
                        dtServerDate = .GetParameterValue("@serverdate")
                    End With

                    StrQ = "SELECT " & _
                           "  @empasondate = cfconfiguration.key_value " & _
                           "FROM hrmsConfiguration..cfconfiguration " & _
                           "WHERE UPPER(cfconfiguration.key_name) = 'EMPLOYEEASONDATE' " & _
                           "AND cfconfiguration.companyunkid = @companyunkid "
                    With xDataOpr
                        .ClearParameters()
                        .AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
                        .AddParameter("@empasondate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, dtEmpAsOnDate, ParameterDirection.Output)

                        .ExecNonQuery(StrQ)
                        If .ErrorMessage <> "" Then
                            exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                        dtEmpAsOnDate = .GetParameterValue("@empasondate")
                    End With

                    Dim strPersonalData3, strPersonalData5, strPersonalData6 As String
                    strPersonalData3 = "" : strPersonalData5 = "" : strPersonalData6 = ""

                    StrQ = "SELECT TOP 1 " & _
                           "     @PersonalData3 = ISNULL(hrdepartment_master.name,'') " & _
                           "    ,@PersonalData5 = ISNULL(hrclassgroup_master.name,'') " & _
                           "    ,@PersonalData6 = ISNULL(hrclasses_master.name,'') " & _
                           "FROM hremployee_transfer_tran " & _
                           "    LEFT JOIN hrdepartment_master ON hremployee_transfer_tran.departmentunkid = hrdepartment_master.departmentunkid " & _
                           "    LEFT JOIN hrclassgroup_master ON hremployee_transfer_tran.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
                           "    LEFT JOIN hrclasses_master ON hremployee_transfer_tran.classunkid = hrclasses_master.classesunkid " & _
                           "WHERE hremployee_transfer_tran.isvoid = 0 " & _
                           "    AND hremployee_transfer_tran.employeeunkid = @employeeunkid " & _
                           "    AND CONVERT(NVARCHAR (8),hremployee_transfer_tran.effectivedate,112) <= @xDate " & _
                           "ORDER BY CONVERT(NVARCHAR (8),hremployee_transfer_tran.effectivedate,112) DESC "
                    With xDataOpr
                        .ClearParameters()
                        .AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
                        .AddParameter("@xDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtEmpAsOnDate)
                        .AddParameter("@PersonalData3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strPersonalData3, ParameterDirection.InputOutput)
                        .AddParameter("@PersonalData5", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strPersonalData5, ParameterDirection.InputOutput)
                        .AddParameter("@PersonalData6", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strPersonalData6, ParameterDirection.InputOutput)

                        .ExecNonQuery(StrQ)
                        If .ErrorMessage <> "" Then
                            exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                        strPersonalData3 = .GetParameterValue("@PersonalData3")
                        strPersonalData5 = .GetParameterValue("@PersonalData5")
                        strPersonalData6 = .GetParameterValue("@PersonalData6")
                    End With

                    If mstrEmpCode.Trim.Length > 0 Then
                        StrQ = "SELECT " & _
                               "     cfconfiguration.key_name " & _
                               "    ,cfconfiguration.key_value " & _
                               "FROM hrmsConfiguration..cfconfiguration " & _
                               "WHERE cfconfiguration.companyunkid = @companyunkid " & _
                               "AND UPPER(cfconfiguration.key_name) LIKE '%SYMMETRY%' "

                        xDataOpr.ClearParameters()
                        xDataOpr.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)

                        dsDetails = xDataOpr.ExecQuery(StrQ, "List")

                        If xDataOpr.ErrorMessage <> "" Then
                            exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                            Throw exForce
                        End If

                        If dsDetails.Tables("List").Rows.Count > 0 Then
                            Dim strConn As String = String.Empty
                            Using oSQL As SqlClient.SqlConnection = New SqlClient.SqlConnection
                                strConn = "Data Source="
                                Dim tmp As DataRow() = Nothing
                                tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYDATASERVERADDRESS'")
                                If tmp.Length > 0 Then strConn &= tmp(0)("key_value") & ";"

                                tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYDATABASENAME'")
                                If tmp.Length > 0 Then strConn &= "Initial Catalog=" & tmp(0)("key_value") & ";"

                                tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYAUTHENTICATIONMODEID'")
                                If tmp.Length > 0 Then
                                    If CInt(tmp(0)("key_value")) = 0 Then   'WINDOWS
                                        strConn &= "Integrated Security=True "
                                    ElseIf CInt(tmp(0)("key_value")) = 1 Then   'USER
                                        tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYDATABASEUSERNAME'")
                                        If tmp.Length > 0 Then strConn &= "User ID=" & tmp(0)("key_value") & ";"
                                        tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYDATABASEPASSWORD'")
                                        If tmp.Length > 0 Then strConn &= "Password=" & clsSecurity.Decrypt(tmp(0)("key_value"), "ezee") & ";"
                                    End If
                                End If
                                oSQL.ConnectionString = strConn
                                oSQL.Open()
                                StrQ = "" : Dim intRecCount As Integer = 0
                                StrQ = "SELECT * FROM DataImportTable WHERE DataImportTable.EmployeeReference = @EmployeeReference "
                                Using oCmd As SqlClient.SqlCommand = New SqlClient.SqlCommand
                                    oCmd.Connection = oSQL
                                    oCmd.CommandText = StrQ
                                    oCmd.Parameters.Clear()
                                    oCmd.Parameters.AddWithValue("@EmployeeReference", mstrEmpCode)
                                    Dim dsInfo As New DataSet
                                    Dim oDa As New SqlClient.SqlDataAdapter(oCmd)
                                    oDa.Fill(dsInfo)

                                    oCmd.Parameters.Clear()
                                    If dsInfo.Tables(0).Rows.Count > 0 Then 'UPDATE
                                        StrQ = "UPDATE DataImportTable SET " & _
                                               " PersonalData3 = @PersonalData3 " & _
                                               ",PersonalData5 = @PersonalData5 " & _
                                               ",PersonalData6 = @PersonalData6 " & _
                                               ",RecordRequest = @RecordRequest " & _
                                               ",ImportNow = @ImportNow " & _
                                               " WHERE RecordCount = @RecordCount "

                                        oCmd.Parameters.AddWithValue("@RecordCount", dsInfo.Tables(0).Rows(0).Item("RecordCount"))
                                        oCmd.Parameters.AddWithValue("@PersonalData3", strPersonalData3.ToString.Substring(0, IIf(Len(strPersonalData3) < 40, Len(strPersonalData3), 40)))
                                        oCmd.Parameters.AddWithValue("@PersonalData5", strPersonalData5.ToString.Substring(0, IIf(Len(strPersonalData5) < 40, Len(strPersonalData5), 40)))
                                        oCmd.Parameters.AddWithValue("@PersonalData6", strPersonalData6.ToString.Substring(0, IIf(Len(strPersonalData6) < 40, Len(strPersonalData6), 40)))
                                        oCmd.Parameters.AddWithValue("@RecordRequest", IIf(dsInfo.Tables(0).Rows(0).Item("RecordRequest") <> 0, dsInfo.Tables(0).Rows(0).Item("RecordRequest"), 0))
                                        oCmd.Parameters.AddWithValue("@ImportNow", 1)

                                        oCmd.CommandText = StrQ
                                        oCmd.ExecuteNonQuery()
                                    End If
                                End Using
                            End Using
                        End If
                    End If
                End If
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Integrate_Symmetry; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [10-MAY-2017] -- END


    'Pinkal (04-Apr-2020) -- Start
    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

    Private Function SetADEmployeeDepartment(ByVal objDataOperation As clsDataOperation, ByVal xCompanyId As Integer _
                                                                  , ByVal xEmployeeID As Integer, ByVal xCurrentDate As Date, ByVal mblnFutureDate As Boolean) As Boolean
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim mdtDate As DateTime = Nothing
        Dim mstrDisplayName As String = ""
        Dim mstrADProperty As String = ""
        Dim exForce As Exception = Nothing
        Try

            strQ = "SELECT  @DisplayName = ISNULL(displayname,'') FROM hremployee_master WHERE employeeunkid =  @employeeunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeID, ParameterDirection.Input)
            objDataOperation.AddParameter("@DisplayName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDisplayName, ParameterDirection.Output)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrDisplayName = objDataOperation.GetParameterValue("@DisplayName").ToString()

            dsList = Get_Current_Allocation(xCurrentDate.Date, mintEmployeeunkid, objDataOperation)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then


                'Pinkal (09-Mar-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                Dim mintParentAllocationId As Integer = 0
                Dim mintMaxHierarchyLevel As Integer = 0
                Dim mintParentAllocationTranId As Integer = 0


                Dim objADAttribute As New clsADAttribute_mapping
                Dim dtAttributes As DataTable = objADAttribute.GetList("List", xCompanyId, True, objDataOperation)
                If dtAttributes IsNot Nothing AndAlso dtAttributes.Rows.Count > 0 Then

                    '/* START TO GET PARENET ALLOCATION ID,HIERARCHY LEVEL ,PARENT ALLOCATION TRAN ID AND LAST LEVEL ALLOCATION NAME 
                    Dim drParent = dtAttributes.AsEnumerable().Where(Function(x) x.Field(Of String)("attributename") = clsADAttribute_mapping.enAttributes.EntryPoint.ToString()).Select(Function(x) x.Field(Of Integer)("mappingunkid"))
                    If drParent IsNot Nothing AndAlso drParent.Count > 0 Then
                        mintParentAllocationId = CInt(drParent(0))
                    End If

                    Dim drHierarchy = dtAttributes.AsEnumerable().Where(Function(x) x.Field(Of String)("attributename") = clsADAttribute_mapping.enAttributes.HierarchyLevel.ToString()).Select(Function(x) x.Field(Of Integer)("mappingunkid"))
                    If drHierarchy IsNot Nothing AndAlso drHierarchy.Count > 0 Then
                        mintMaxHierarchyLevel = CInt(drHierarchy(0))
                    End If

                
                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(xCurrentDate, objDataOperation) = xEmployeeID

                    Select Case mintParentAllocationId

                        Case enAllocation.BRANCH
                            mintParentAllocationTranId = objEmployee._Stationunkid

                        Case enAllocation.DEPARTMENT_GROUP
                            mintParentAllocationTranId = objEmployee._Deptgroupunkid

                        Case enAllocation.DEPARTMENT
                            mintParentAllocationTranId = objEmployee._Departmentunkid

                        Case enAllocation.SECTION_GROUP
                            mintParentAllocationTranId = objEmployee._Sectiongroupunkid

                        Case enAllocation.SECTION
                            mintParentAllocationTranId = objEmployee._Sectionunkid

                        Case enAllocation.UNIT_GROUP
                            mintParentAllocationTranId = objEmployee._Unitgroupunkid

                        Case enAllocation.UNIT
                            mintParentAllocationTranId = objEmployee._Unitunkid

                        Case enAllocation.TEAM
                            mintParentAllocationTranId = objEmployee._Teamunkid

                        Case enAllocation.JOB_GROUP
                            mintParentAllocationTranId = objEmployee._Jobgroupunkid

                        Case enAllocation.JOBS
                            mintParentAllocationTranId = objEmployee._Jobunkid

                        Case enAllocation.CLASS_GROUP
                            mintParentAllocationTranId = objEmployee._Classgroupunkid

                        Case enAllocation.CLASSES
                            mintParentAllocationTranId = objEmployee._Classunkid

                        Case enAllocation.COST_CENTER
                            mintParentAllocationTranId = objEmployee._Costcenterunkid

                    End Select

                    objEmployee = Nothing

                    Dim objHierarchyLevel As New clsadentry_hierarchy
                    Dim mintMaxAllocationId As Integer = 0
                    Dim dsHierarchyLevel As DataSet = objHierarchyLevel.GetList("List", xCompanyId, True, mintParentAllocationId, mintParentAllocationTranId, objDataOperation)
                    If dsHierarchyLevel IsNot Nothing AndAlso dsHierarchyLevel.Tables(0).Rows.Count > 0 Then

                        'Pinkal (04-Apr-2020) -- Start
                        'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                        'mintMaxAllocationId = CInt(dsHierarchyLevel.Tables(0).Rows(0)("allocationrefid" & mintMaxHierarchyLevel - 1))
                        For j As Integer = 1 To mintMaxHierarchyLevel
                            Dim xAllocationIds As String = IIf((mintMaxHierarchyLevel - j) > 0, mintMaxHierarchyLevel - j, "").ToString()
                            If CInt(dsHierarchyLevel.Tables(0).Rows(0)("allocationrefid" & xAllocationIds)) > 0 Then
                                'mintMaxAllocationId = CInt(dsHierarchyLevel.Tables(0).Rows(0)("allocationrefid" & xAllocationIds))
                                If mintMaxAllocationId <= 0 Then mintMaxAllocationId = CInt(dsHierarchyLevel.Tables(0).Rows(0)("allocationrefid" & xAllocationIds))
                                Dim xAllocationName As String = GetADMappingAllocationName(CInt(dsHierarchyLevel.Tables(0).Rows(0)("allocationrefid" & xAllocationIds)), objDataOperation, dsList)
                                If xAllocationName.Trim.Length > 0 Then
                                    Dim mstrMissingAllocation As String = ""
                                    Dim dtOUTable As IList(Of String) = dtAttributes.AsEnumerable().Where(Function(x) x.Field(Of String)("oupath") <> "" And x.Field(Of Integer)("mappingunkid") = CInt(dsHierarchyLevel.Tables(0).Rows(0)("allocationrefid" & xAllocationIds)) And x.Field(Of String)("attributename") = "OU=" & xAllocationName).Select(Function(x) x.Field(Of String)("oupath")).ToList()
                                    If dtOUTable IsNot Nothing AndAlso dtOUTable.Count > 0 Then
                                        If DirectoryEntry.Exists(dtOUTable(0).ToString()) = False Then
                                            Dim objMaster As New clsMasterData
                                            Dim dsAllocation As DataSet = objMaster.GetEAllocation_Notification("List", dsHierarchyLevel.Tables(0).Rows(0)("allocationrefid" & xAllocationIds).ToString(), False, False)
                                            If dsAllocation IsNot Nothing AndAlso dsAllocation.Tables(0).Rows.Count > 0 Then
                                                mstrMissingAllocation = dsAllocation.Tables(0).Rows(0)("Name").ToString()
                                            End If
                                            dsAllocation = Nothing
                                            objMaster = Nothing

                                            'Pinkal (20-Aug-2020) -- Start
                                            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                                            If mstrMissingAllocation.Trim > 0 OrElse xAllocationName.Trim.Length > 0 Then
                                            SendADFailedToSaveNotificationToADUser(xCompanyId, mstrMissingAllocation, xAllocationName, mintUserunkid)
                                            End If
                                            'Pinkal (20-Aug-2020) -- End
                                            Throw New Exception("Sorry, record cannot be saved. Some allocation does not exist in active directory.")
                                        End If
                                    Else
                                        Dim objMaster As New clsMasterData
                                        Dim dsAllocation As DataSet = objMaster.GetEAllocation_Notification("List", dsHierarchyLevel.Tables(0).Rows(0)("allocationrefid" & xAllocationIds).ToString(), False, False)
                                        If dsAllocation IsNot Nothing AndAlso dsAllocation.Tables(0).Rows.Count > 0 Then
                                            mstrMissingAllocation = dsAllocation.Tables(0).Rows(0)("Name").ToString()
                                        End If
                                        dsAllocation = Nothing
                                        objMaster = Nothing
                                        'Pinkal (20-Aug-2020) -- Start
                                        'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                                        If mstrMissingAllocation.Trim.Length > 0 OrElse xAllocationName.Trim.Length > 0 Then
                                        SendADFailedToSaveNotificationToADUser(xCompanyId, mstrMissingAllocation, xAllocationName, mintUserunkid)
                                        End If
                                        'Pinkal (20-Aug-2020) -- End
                                        Throw New Exception("Sorry, record cannot be saved. Some allocation does not exist in active directory.")
                                    End If
                                End If
                                'Exit For
                            End If
                        Next
                        'Pinkal (04-Apr-2020) -- End

                        If mintMaxAllocationId = 0 Then mintMaxAllocationId = mintParentAllocationId
                    End If


                    'Pinkal (04-Apr-2020) -- Start
                    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

                    Dim mstrAllocationName As String = GetADMappingAllocationName(mintMaxAllocationId, objDataOperation, dsList)

                    '/* END TO GET PARENET ALLOCATION ID,HIERARCHY LEVEL ,PARENT ALLOCATION TRAN ID AND LAST LEVEL ALLOCATION NAME 

                    '/*  START TO GET OFFICE ALLOCATION NAME
                    Dim mintOfficeAllocationId As Integer = 0
                    Dim drOfficeAllocation = dtAttributes.AsEnumerable().Where(Function(x) x.Field(Of String)("attributename") = clsADAttribute_mapping.enAttributes.Office.ToString()).Select(Function(x) x.Field(Of Integer)("mappingunkid"))
                    If drOfficeAllocation IsNot Nothing AndAlso drOfficeAllocation.Count > 0 Then
                        mintOfficeAllocationId = CInt(drOfficeAllocation(0))
                    End If

                    Dim mstrOfficeAllocationName As String = GetADMappingAllocationName(mintOfficeAllocationId, objDataOperation, dsList)
                    '/*  END TO GET OFFICE ALLOCATION NAME


                    '/*  START TO GET DESCRIPTION ALLOCATION NAME
                    Dim mintDescriptionAllocationId As Integer = 0
                    Dim drDescriptionAllocation = dtAttributes.AsEnumerable().Where(Function(x) x.Field(Of String)("attributename") = clsADAttribute_mapping.enAttributes.Description.ToString()).Select(Function(x) x.Field(Of Integer)("mappingunkid"))
                    If drDescriptionAllocation IsNot Nothing AndAlso drDescriptionAllocation.Count > 0 Then
                        mintDescriptionAllocationId = CInt(drDescriptionAllocation(0))
                    End If

                    Dim mstrDescriptionAllocationName As String = GetADMappingAllocationName(mintDescriptionAllocationId, objDataOperation, dsList)
                    '/*  END TO GET DESCRIPTION ALLOCATION NAME



                    'Pinkal (18-Mar-2021) -- Start 
                    'NMB Enhancmenet : AD Enhancement for NMB.
                    '/*  START TO GET TRANSFER ALLOCATION NAME
                    Dim mintTransferAllocationId As Integer = 0
                    Dim drTransaferAllocation = dtAttributes.AsEnumerable().Where(Function(x) x.Field(Of String)("attributename") = clsADAttribute_mapping.enAttributes.Transfer_Allocation.ToString().Replace("_", " ")).Select(Function(x) x.Field(Of Integer)("mappingunkid"))
                    If drTransaferAllocation IsNot Nothing AndAlso drTransaferAllocation.Count > 0 Then
                        mintTransferAllocationId = CInt(drTransaferAllocation(0))
                    End If

                    Dim mstrTransferAllocationName As String = GetADMappingAllocationName(mintTransferAllocationId, objDataOperation, dsList)


                    '/*  END TO GET TRANSFER ALLOCATION NAME
                    'UpdateEmpDepartmentInAD(mstrDisplayName, mintMaxAllocationId, mstrAllocationName, xCompanyId, mstrOfficeAllocationName)
                    'UpdateEmpDepartmentInAD(objDataOperation, mstrDisplayName, mintMaxAllocationId, mstrAllocationName, xCompanyId, mblnFutureDate, mstrOfficeAllocationName, mstrDescriptionAllocationName)
                    UpdateEmpDepartmentInAD(objDataOperation, mstrDisplayName, mintMaxAllocationId, mstrAllocationName, xCompanyId, mblnFutureDate, mstrOfficeAllocationName, mstrDescriptionAllocationName, mstrTransferAllocationName)

                    'Pinkal (18-Mar-2021) -- End


                    'Pinkal (04-Apr-2020) -- End

                End If

                'Pinkal (09-Mar-2020) -- End

            End If
        Catch ex As Exception
            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If ex.Message.ToString().Contains("Sorry, record cannot be saved. Some allocation does not exist in active directory.") Then
                Throw New Exception(ex.Message)
            ElseIf ex.Message.ToString().Contains("System cannot connect with Active Directory at the moment. Please check Active Directory connection.  Also username or password (Aruti to AD Server) may be incorrect.") Then
                Throw New Exception(ex.Message)
            Else
                Throw New Exception(mstrModuleName & " SetADEmployeeDepartment:- " & ex.Message & " [" & mstrADProperty & "]")
            End If
            'Pinkal (04-Apr-2020) -- End
        End Try
        Return True
    End Function

    'Pinkal (04-Apr-2020) -- End



    'Pinkal (04-Apr-2020) -- Start
    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
    'Private Sub UpdateEmpDepartmentInAD(ByVal mstrDisplayName As String, ByVal mstrDepartmentName As String,  Optional ByVal mstrClassName As String = "")
    Private Sub UpdateEmpDepartmentInAD(ByVal objDataOperation As clsDataOperation, ByVal mstrDisplayName As String, ByVal mintAllocationId As Integer, ByVal mstrAllocationName As String _
                                                            , ByVal xCompanyId As Integer, ByVal mblnFutureDate As Boolean, Optional ByVal mstrOfficeName As String = "" _
                                                            , Optional ByVal mstrDescriptionAllocationName As String = "", Optional ByVal mstrTransferAllocationName As String = "")

        'Pinkal (18-Mar-2021) -- NMB Enhancmenet : AD Enhancement for NMB.[Optional ByVal mstrTransferAllocationName As String = ""]

        'Pinkal (09-Mar-2020) -- End
        Dim mstrADProperty As String = ""
        Dim mstrADIPAddress As String = ""
        Dim mstrADDomain As String = ""
        Dim mstrADUserName As String = ""
        Dim mstrADUserPwd As String = ""
        Dim entry As DirectoryEntry = Nothing
        Dim result As SearchResult = Nothing
        'Pinkal (04-Apr-2020) -- Start
        'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
        Dim mstrAllocation As String = ""
        'Pinkal (04-Apr-2020) -- End

        Try

            GetADConnection(mstrADIPAddress, mstrADDomain, mstrADUserName, mstrADUserPwd, objDataOperation)

            If IsADUserExist(mstrDisplayName, mstrADIPAddress, mstrADDomain, mstrADUserName, mstrADUserPwd, result) Then

                'Pinkal (15-Feb-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                'entry = New DirectoryEntry(result.Path)
                entry = New DirectoryEntry(result.Path, mstrADUserName, mstrADUserPwd)
                'Pinkal (15-Feb-2020) -- End

                If IsDBNull(entry.Properties("department").Value) = False AndAlso entry.Properties("department").Value <> Nothing Then

                    'Pinkal (18-Mar-2021) -- Start 
                    'NMB Enhancmenet : AD Enhancement for NMB.

                                        'Pinkal (05-Sep-2020) -- Start
                                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

                                        If IsDBNull(entry.Properties("physicalDeliveryOfficeName").Value) = False AndAlso entry.Properties("physicalDeliveryOfficeName").Value <> Nothing Then
                                    If mstrOfficeName.Trim.Length > 0 AndAlso entry.Properties("physicalDeliveryOfficeName").Value.ToString() <> mstrOfficeName Then
                                        SetADProperty(entry, "physicalDeliveryOfficeName", mstrOfficeName, mstrADProperty) 'Office
                                        entry.CommitChanges()
                                        entry.RefreshCache()
                                    End If
                                        Else
                                            If mstrOfficeName.Trim.Length > 0 Then
                                                SetADProperty(entry, "physicalDeliveryOfficeName", mstrOfficeName, mstrADProperty) 'Office
                                                entry.CommitChanges()
                                                entry.RefreshCache()
                                            End If
                                        End If

                                        If IsDBNull(entry.Properties("Description").Value) = False AndAlso entry.Properties("Description").Value <> Nothing Then
                                    If mstrDescriptionAllocationName.Trim.Length > 0 AndAlso entry.Properties("Description").Value.ToString() <> mstrDescriptionAllocationName.Trim Then
                                        SetADProperty(entry, "Description", mstrDescriptionAllocationName, mstrADProperty) 'Description
                                        entry.CommitChanges()
                                        entry.RefreshCache()
                                    End If
                                        Else
                                            If mstrDescriptionAllocationName.Trim.Length > 0 Then
                                                SetADProperty(entry, "Description", mstrDescriptionAllocationName, mstrADProperty) 'Description
                                                entry.CommitChanges()
                                                entry.RefreshCache()
                                            End If
                                        End If

                                        'Pinkal (05-Sep-2020) -- End


                    If IsDBNull(entry.Properties("l").Value) = False AndAlso entry.Properties("l").Value <> Nothing Then
                        If mstrTransferAllocationName.Trim.Length > 0 AndAlso entry.Properties("l").Value.ToString() <> mstrTransferAllocationName.Trim Then
                            SetADProperty(entry, "l", mstrTransferAllocationName, mstrADProperty) 'Description
                            entry.CommitChanges()
                            entry.RefreshCache()
                        End If
                    Else
                        If mstrTransferAllocationName.Trim.Length > 0 Then
                            SetADProperty(entry, "l", mstrTransferAllocationName, mstrADProperty) 'City
                            entry.CommitChanges()
                            entry.RefreshCache()
                        End If
                    End If
                    'Pinkal (18-Mar-2021) -- End



                    If entry.Properties("department").Value.ToString() <> mstrAllocationName Then

ADDeptAssignment:

                        '/ START TO MOVE USER FROM EXISTING OU TO ANOTHER OU
                        Dim objADAttribute As New clsADAttribute_mapping
                        'Pinkal (09-Mar-2020) -- Start
                        'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                        'Dim dtAttributes As DataTable = objADAttribute.GetList("List", True, objDataOperation)
                        Dim dtAttributes As DataTable = objADAttribute.GetList("List", xCompanyId, True, objDataOperation)
                        'Pinkal (09-Mar-2020) -- End


                        'Pinkal (04-Apr-2020) -- Start
                        'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

                        If dtAttributes IsNot Nothing AndAlso dtAttributes.Rows.Count > 0 Then
                            Dim dtOUTable As IList(Of String) = dtAttributes.AsEnumerable().Where(Function(x) x.Field(Of String)("oupath") <> "" And x.Field(Of Integer)("mappingunkid") = mintAllocationId And x.Field(Of String)("attributename") = "OU=" & mstrAllocationName).Select(Function(x) x.Field(Of String)("oupath")).ToList()
                            If dtOUTable IsNot Nothing AndAlso dtOUTable.Count > 0 Then
                                Dim mstrOUPath As String = dtOUTable(0).ToString()

                                If DirectoryEntry.Exists(mstrOUPath) Then

                                    If mblnFutureDate = False Then

                                        SetADProperty(entry, "department", mstrAllocationName, mstrADProperty)  'Department
                                        entry.CommitChanges()
                                        entry.RefreshCache()


                                    Dim newEntry As New DirectoryEntry(mstrOUPath, mstrADUserName, mstrADUserPwd)
                                    entry.MoveTo(newEntry)
                                    newEntry.Close()

                                End If

                            Else
                                'Pinkal (04-Apr-2020) -- Start
                                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                                Dim objMaster As New clsMasterData
                                Dim dsAllocation As DataSet = objMaster.GetEAllocation_Notification("List", mintAllocationId.ToString(), False, False)
                                If dsAllocation IsNot Nothing AndAlso dsAllocation.Tables(0).Rows.Count > 0 Then
                                    mstrAllocation = dsAllocation.Tables(0).Rows(0)("Name").ToString()
                                End If
                                dsAllocation = Nothing
                                objMaster = Nothing
                                Throw New Exception("Sorry, record cannot be saved. Some allocation does not exist in active directory.")
                                'Pinkal (04-Apr-2020) -- End
                            End If   'If DirectoryEntry.Exists(mstrOUPath) Then
                        Else
                            'Pinkal (04-Apr-2020) -- Start
                            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                            Dim objMaster As New clsMasterData
                            Dim dsAllocation As DataSet = objMaster.GetEAllocation_Notification("List", mintAllocationId.ToString(), False, False)
                            If dsAllocation IsNot Nothing AndAlso dsAllocation.Tables(0).Rows.Count > 0 Then
                                mstrAllocation = dsAllocation.Tables(0).Rows(0)("Name").ToString()
                            End If
                            dsAllocation = Nothing
                            objMaster = Nothing
                            Throw New Exception("Sorry, record cannot be saved. Some allocation does not exist in active directory.")
                            'Pinkal (04-Apr-2020) -- End

                        End If  'If dtOUTable IsNot Nothing AndAlso dtOUTable.Count > 0 Then

                    End If  'If dtAttributes IsNot Nothing AndAlso dtAttributes.Rows.Count > 0 Then

                    'Pinkal (04-Apr-2020) -- End

                    objADAttribute = Nothing

                    '/ END TO MOVE USER FROM EXISTING OU TO ANOTHER OU

                End If 'if entry.Properties("department").Value.ToString() <> mstrDepartmentName Then

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                Else
                    GoTo ADDeptAssignment
                    'Pinkal (05-Sep-2020) -- End
                End If

                entry.Close()
                entry = Nothing


            End If   'If IsADUserExist(mstrDisplayName, mstrADIPAddress, mstrADDomain, mstrADUserName, mstrADUserPwd, result) Then

        Catch ex As Exception

            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If ex.Message.ToString().Contains("Sorry, record cannot be saved. Some allocation does not exist in active directory.") Then

                'Pinkal (20-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                If mstrAllocation.Trim.Length > 0 OrElse mstrAllocationName.Trim.Length > 0 Then
                SendADFailedToSaveNotificationToADUser(xCompanyId, mstrAllocation, mstrAllocationName, mintUserunkid)
                End If
                'Pinkal (20-Aug-2020) -- End

                mstrMessage = ex.Message
                Throw New Exception(ex.Message)
            ElseIf ex.Message.ToString().Contains("System cannot connect with Active Directory at the moment. Please check Active Directory connection.  Also username or password (Aruti to AD Server) may be incorrect.") Then
                Throw New Exception(ex.Message)
            Else
                Throw New Exception(mstrModuleName & " UpdateEmpDepartmentInAD:- " & ex.Message & " [" & mstrADProperty & "]")
                    End If
            'Pinkal (04-Apr-2020) -- End
        End Try
    End Sub

    'Pinkal (04-Apr-2020) -- End

    'Pinkal (04-Apr-2020) -- Start
    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

    Private Function GetADMappingAllocationName(ByVal xAllocationId As Integer, ByVal objDataOperation As clsDataOperation, ByVal dsList As DataSet) As String
        Dim mstrAllocationName As String = ""
        Dim strQ As String = ""
        Try
            Select Case CType(xAllocationId, enAllocation)

                        Case enAllocation.BRANCH

                            strQ = "SELECT  @Station = ISNULL(name,'') FROM hrstation_master WHERE stationunkid =  @stationunkid"
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables(0).Rows(0)("stationunkid")), ParameterDirection.Input)
                    objDataOperation.AddParameter("@Station", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAllocationName, ParameterDirection.Output)
                            objDataOperation.ExecNonQuery(strQ)

                    mstrAllocationName = objDataOperation.GetParameterValue("@Station").ToString()

                        Case enAllocation.DEPARTMENT_GROUP

                            strQ = "SELECT  @DeptGrp = ISNULL(name,'') FROM hrdepartment_group_master WHERE deptgroupunkid =  @deptgroupunkid"
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables(0).Rows(0)("deptgroupunkid")), ParameterDirection.Input)
                    objDataOperation.AddParameter("@DeptGrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAllocationName, ParameterDirection.Output)
                            objDataOperation.ExecNonQuery(strQ)

                    mstrAllocationName = objDataOperation.GetParameterValue("@DeptGrp").ToString()

                        Case enAllocation.DEPARTMENT

                            strQ = "SELECT  @Department = ISNULL(name,'') FROM hrdepartment_master WHERE departmentunkid =  @departmentunkid"
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables(0).Rows(0)("departmentunkid")), ParameterDirection.Input)
                    objDataOperation.AddParameter("@Department", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAllocationName, ParameterDirection.Output)
                            objDataOperation.ExecNonQuery(strQ)

                    mstrAllocationName = objDataOperation.GetParameterValue("@Department").ToString()

                        Case enAllocation.SECTION_GROUP

                            strQ = "SELECT  @SecGrp = ISNULL(name,'') FROM hrsectiongroup_master WHERE sectiongroupunkid =  @sectiongroupunkid"
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables(0).Rows(0)("sectiongroupunkid")), ParameterDirection.Input)
                    objDataOperation.AddParameter("@SecGrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAllocationName, ParameterDirection.Output)
                            objDataOperation.ExecNonQuery(strQ)

                    mstrAllocationName = objDataOperation.GetParameterValue("@SecGrp").ToString()

                        Case enAllocation.SECTION

                            strQ = "SELECT  @Section = ISNULL(name,'') FROM hrsection_master WHERE sectionunkid =  @sectionunkid"
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables(0).Rows(0)("sectionunkid")), ParameterDirection.Input)
                    objDataOperation.AddParameter("@Section", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAllocationName, ParameterDirection.Output)
                            objDataOperation.ExecNonQuery(strQ)

                    mstrAllocationName = objDataOperation.GetParameterValue("@Section").ToString()

                        Case enAllocation.UNIT_GROUP

                            strQ = "SELECT  @UnitGrp = ISNULL(name,'') FROM hrunitgroup_master WHERE unitgroupunkid =  @unitgroupunkid"
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables(0).Rows(0)("unitgroupunkid")), ParameterDirection.Input)
                    objDataOperation.AddParameter("@UnitGrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAllocationName, ParameterDirection.Output)
                            objDataOperation.ExecNonQuery(strQ)

                    mstrAllocationName = objDataOperation.GetParameterValue("@UnitGrp").ToString()

                        Case enAllocation.UNIT

                            strQ = "SELECT  @Unit = ISNULL(name,'') FROM hrunit_master WHERE unitunkid =  @unitunkid"
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables(0).Rows(0)("unitunkid")), ParameterDirection.Input)
                    objDataOperation.AddParameter("@Unit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAllocationName, ParameterDirection.Output)
                            objDataOperation.ExecNonQuery(strQ)

                    mstrAllocationName = objDataOperation.GetParameterValue("@Unit").ToString()

                        Case enAllocation.TEAM

                            strQ = "SELECT  @Team = ISNULL(name,'') FROM hrteam_master WHERE teamunkid =  @teamunkid"
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables(0).Rows(0)("teamunkid")), ParameterDirection.Input)
                    objDataOperation.AddParameter("@Team", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAllocationName, ParameterDirection.Output)
                            objDataOperation.ExecNonQuery(strQ)

                    mstrAllocationName = objDataOperation.GetParameterValue("@Team").ToString()

                        Case enAllocation.CLASS_GROUP

                            strQ = "SELECT  @ClassGrp = ISNULL(name,'') FROM hrclassgroup_master WHERE classgroupunkid =  @classgroupunkid"
                            objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables(0).Rows(0)("classgroupunkid")), ParameterDirection.Input)
                    objDataOperation.AddParameter("@ClassGrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAllocationName, ParameterDirection.Output)
                            objDataOperation.ExecNonQuery(strQ)

                    mstrAllocationName = objDataOperation.GetParameterValue("@ClassGrp").ToString()

                        Case enAllocation.CLASSES

                strQ = "SELECT  @Class = ISNULL(name,'') FROM hrclasses_master WHERE classesunkid =  @classesunkid"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@classesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables(0).Rows(0)("classunkid")), ParameterDirection.Input)
                    objDataOperation.AddParameter("@Class", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAllocationName, ParameterDirection.Output)
                objDataOperation.ExecNonQuery(strQ)

                    mstrAllocationName = objDataOperation.GetParameterValue("@Class").ToString()

                    End Select


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetADMappingAllocationName; Module Name: " & mstrModuleName)
        End Try
        Return mstrAllocationName
    End Function

    Private Sub SendADFailedToSaveNotificationToADUser(ByVal xCompanyId As Integer, ByVal mstrAllocation As String, ByVal mstrAllocationName As String, ByVal mintUserunkid As Integer)
        Try
            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = xCompanyId

            Dim xLoginMode As enLogin_Mode
            If mstrWebFormName.Trim.Length <= 0 Then
                xLoginMode = enLogin_Mode.DESKTOP
            Else
                xLoginMode = enLogin_Mode.MGR_SELF_SERVICE
                End If
            Dim objEmp As New clsEmployee_Master

            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            If mstrAllocation.Trim.Length > 0 OrElse mstrAllocationName.Trim.Length > 0 Then
            objEmp.SendADFailedToSaveNotificationToADUser(xCompanyId, mstrAllocation, mstrAllocationName, mintUserunkid, xLoginMode, objCompany._Senderaddress)
            End If
            'Pinkal (20-Aug-2020) -- End
            objEmp = Nothing
            objCompany = Nothing
        Catch ex As Exception
            Throw New Exception(mstrModuleName & " SendADFailedToSaveNotificationToADUser:- " & ex.Message)
        End Try
    End Sub

    'Pinkal (04-Apr-2020) -- End


    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
    Public Function MigrationInsert(ByVal intCompanyId As Integer, _
                                    ByVal xOprationType As clsEmployeeMovmentApproval.enOperationType, _
                                    ByVal mblnCreateADUserFromEmpMst As Boolean, _
                                    ByVal xDatabaseName As String, _
                                    Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                                    Optional ByVal xMdtMigration As DataTable = Nothing, _
                                    Optional ByVal xblnFromApproval As Boolean = False, _
                                    Optional ByVal xblnFromApprovalScreen As Boolean = False, _
                                    Optional ByVal eStatusId As clsEmployee_Master.EmpApprovalStatus = clsEmployee_Master.EmpApprovalStatus.Approved _
                                    ) As Boolean


        'Pinkal (12-Oct-2020) -- 'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.[ByVal xDatabaseName As String]

        'Pinkal (07-Dec-2019) -- Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[ByVal mblnCreateADUserFromEmpMst As Boolean]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        objDataOperation.ClearParameters()
        Try
            If (IsNothing(xMdtMigration) = False AndAlso xMdtMigration.Rows.Count > 0) Or _
                (xblnFromApprovalScreen = True AndAlso IsNothing(xMdtMigration)) Then

                Dim objMovementMigration As New clsMovementMigration

                Dim objConfig As New clsConfigOptions
                Dim strParamKeys() As String = { _
                                           "EMPLOYEEASONDATE", _
                                           "CLAIMREQUEST_PAYMENTAPPROVALWITHLEAVEAPPROVAL" _
                                           }
                Dim MigrationDicKeyValues As New Dictionary(Of String, String)
                MigrationDicKeyValues = objConfig.GetKeyValue(intCompanyId, strParamKeys, objDataOperation)

                If IsNothing(MigrationDicKeyValues) = False _
                AndAlso MigrationDicKeyValues.ContainsKey("EmployeeAsOnDate") _
                AndAlso MigrationDicKeyValues.ContainsKey("ClaimRequest_PaymentApprovalwithLeaveApproval") Then
                    Dim Formname As String = String.Empty
                    If mstrWebFormName.Trim.Length <= 0 Then
                        Formname = mstrForm_Name
                    Else
                        Formname = mstrWebFormName
                    End If

                    If IsNothing(xMdtMigration) = True Then
                        objMovementMigration.isMovementInApprovalFlow(objMovementMigration._DataList, mintEmployeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, _
                                                                      enApprovalMigrationScreenType.TRANSFER, 0, objDataOperation)

                        If IsNothing(objMovementMigration._DataList) = False Then
                            xMdtMigration = objMovementMigration._DataList
                        End If

                    End If


                    objMovementMigration._Ip = IIf(mstrWebClientIP.Trim = "", getIP, mstrWebClientIP)
                    objMovementMigration._Hostname = IIf(mstrWebHostName.Trim = "", getHostName, mstrWebHostName)
                    If mstrWebFormName.Trim.Length <= 0 Then
                        objMovementMigration._Form_Name = mstrForm_Name
                        objMovementMigration._Isweb = False
                    Else
                        objMovementMigration._Form_Name = mstrWebFormName
                        objMovementMigration._Isweb = True
                    End If


                    'Pinkal (12-Oct-2020) -- Start
                    'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

                    'If objMovementMigration.Insert(xMdtMigration, mintTransferunkid, mintEmployeeunkid, CInt(enApprovalMigrationScreenType.TRANSFER), _
                    '                         xblnFromApproval, MigrationDicKeyValues("EmployeeAsOnDate").ToString(), _
                    '                        CBool(MigrationDicKeyValues("ClaimRequest_PaymentApprovalwithLeaveApproval").ToString()), mintUserunkid, Formname, _
                    '                         CInt(xOprationType), mblnCreateADUserFromEmpMst, xblnFromApprovalScreen, objDataOperation, eStatusId) = False Then

                    If objMovementMigration.Insert(xDatabaseName, xMdtMigration, mintTransferunkid, mintEmployeeunkid, CInt(enApprovalMigrationScreenType.TRANSFER), _
                                             xblnFromApproval, MigrationDicKeyValues("EmployeeAsOnDate").ToString(), _
                                            CBool(MigrationDicKeyValues("ClaimRequest_PaymentApprovalwithLeaveApproval").ToString()), mintUserunkid, Formname, _
                                             CInt(xOprationType), mblnCreateADUserFromEmpMst, xblnFromApprovalScreen, objDataOperation, eStatusId) = False Then

                        'Pinkal (12-Oct-2020) -- End

                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce

                    End If
                End If
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If

        End Try
    End Function
    'Gajanan [23-SEP-2019] -- End  


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, transfer information is already present for the selected effective date.")
            Language.setMessage(mstrModuleName, 2, "Sorry, transfer information is already present for the selected combination for the selected effective date.")
            Language.setMessage(mstrModuleName, 3, "WEB")
            Language.setMessage(mstrModuleName, 4, "Notification of Changes in Employee Master file")
            Language.setMessage(mstrModuleName, 5, "Effective Date")
            Language.setMessage(mstrModuleName, 6, "Allocations")
            Language.setMessage(mstrModuleName, 7, "Old Allocation")
            Language.setMessage(mstrModuleName, 8, "New Allocation")
			Language.setMessage("frmEmployeeMaster", 142, "This is to inform you that changes have been made for employee")
			Language.setMessage("frmEmployeeMaster", 143, "with employeecode")
			Language.setMessage("frmEmployeeMaster", 144, "Following information has been changed by user")
			Language.setMessage("frmEmployeeMaster", 145, "from Machine")
			Language.setMessage("frmEmployeeMaster", 146, "and IPAddress")
			Language.setMessage("frmEmployeeMaster", 147, "Dear")
            Language.setMessage("clsMasterData", 419, "Classes")
            Language.setMessage("clsMasterData", 420, "Class Group")
            Language.setMessage("clsMasterData", 423, "Team")
            Language.setMessage("clsMasterData", 424, "Unit")
            Language.setMessage("clsMasterData", 425, "Unit Group")
            Language.setMessage("clsMasterData", 426, "Section")
            Language.setMessage("clsMasterData", 427, "Section Group")
            Language.setMessage("clsMasterData", 428, "Department")
            Language.setMessage("clsMasterData", 429, "Department Group")
            Language.setMessage("clsMasterData", 430, "Branch")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
