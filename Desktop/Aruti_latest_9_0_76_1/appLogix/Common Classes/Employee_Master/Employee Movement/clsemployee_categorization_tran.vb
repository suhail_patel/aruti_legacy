﻿'************************************************************************************************************************************
'Class Name : clsemployee_categorization_Tran.vb
'Purpose    :
'Date       :01-Apr-2015
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System.Threading
Imports System.DirectoryServices

''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsemployee_categorization_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsemployee_categorization_Tran"
    Private trd As Thread
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintCategorizationtranunkid As Integer
    Private mdtEffectivedate As Date
    Private mintEmployeeunkid As Integer
    Private mintJobGroupunkid As Integer
    Private mintJobunkid As Integer
    Private mintGradeunkid As Integer
    Private mintGradelevelunkid As Integer
    Private mintChangereasonunkid As Integer
    Private mblnIsfromemployee As Boolean
    Private mintStatusunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    'S.SANDEEP [14 APR 2015] -- START
    Private mintRehiretranunkid As Integer = 0
    'S.SANDEEP [14 APR 2015] -- END

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categorizationtranunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Categorizationtranunkid(Optional ByVal objDataOper As clsDataOperation = Nothing) As Integer 'S.SANDEEP [25 OCT 2016] -- START {objDataOperation} -- END
        Get
            Return mintCategorizationtranunkid
        End Get
        Set(ByVal value As Integer)
            mintCategorizationtranunkid = value
            'S.SANDEEP [25 OCT 2016] -- START
            'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
            'Call GetData()
            Call GetData(objDataOper)
            'S.SANDEEP [25 OCT 2016] -- END
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobgroupunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _JobGroupunkid() As Integer
        Get
            Return mintJobGroupunkid
        End Get
        Set(ByVal value As Integer)
            mintJobGroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Gradeunkid() As Integer
        Get
            Return mintGradeunkid
        End Get
        Set(ByVal value As Integer)
            mintGradeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradelevelunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Gradelevelunkid() As Integer
        Get
            Return mintGradelevelunkid
        End Get
        Set(ByVal value As Integer)
            mintGradelevelunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set changereasonunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Changereasonunkid() As Integer
        Get
            Return mintChangereasonunkid
        End Get
        Set(ByVal value As Integer)
            mintChangereasonunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfromemployee
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isfromemployee() As Boolean
        Get
            Return mblnIsfromemployee
        End Get
        Set(ByVal value As Boolean)
            mblnIsfromemployee = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webformname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webclientip
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webhostname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    'S.SANDEEP [14 APR 2015] -- START
    ''' <summary>
    ''' Purpose: Get or Set rehiretranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Rehiretranunkid() As Integer
        Get
            Return mintRehiretranunkid
        End Get
        Set(ByVal value As Integer)
            mintRehiretranunkid = value
        End Set
    End Property
    'S.SANDEEP [14 APR 2015] -- END

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing) 'S.SANDEEP [01 JUN 2016] -- START -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        Try
            strQ = "SELECT " & _
              "  categorizationtranunkid " & _
              ", effectivedate " & _
              ", employeeunkid " & _
              ", ISNULL(rehiretranunkid,0) AS rehiretranunkid " & _
              ", jobgroupunkid " & _
              ", jobunkid " & _
              ", gradeunkid " & _
              ", gradelevelunkid " & _
              ", changereasonunkid " & _
              ", isfromemployee " & _
              ", statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hremployee_categorization_tran " & _
             "WHERE categorizationtranunkid = @categorizationtranunkid " 'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END

            'Shani(11-APR-2016) -- [rehiretranunkid]-->[ISNULL(rehiretranunkid,0) AS rehiretranunkid]


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@categorizationtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintCategorizationTranUnkId.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintcategorizationtranunkid = CInt(dtRow.Item("categorizationtranunkid"))
                mdteffectivedate = dtRow.Item("effectivedate")
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintJobGroupunkid = CInt(dtRow.Item("jobgroupunkid"))
                mintjobunkid = CInt(dtRow.Item("jobunkid"))
                mintgradeunkid = CInt(dtRow.Item("gradeunkid"))
                mintgradelevelunkid = CInt(dtRow.Item("gradelevelunkid"))
                mintchangereasonunkid = CInt(dtRow.Item("changereasonunkid"))
                mblnisfromemployee = CBool(dtRow.Item("isfromemployee"))
                mintstatusunkid = CInt(dtRow.Item("statusunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then mdtVoiddatetime = dtRow.Item("voiddatetime")
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'S.SANDEEP [14 APR 2015] -- START
                mintRehiretranunkid = CInt(dtRow.Item("rehiretranunkid"))
                'S.SANDEEP [14 APR 2015] -- END
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intEmployeeId As Integer = 0, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mdtAppDate As String = ""

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()
        Try

            strQ = "SELECT " & _
               "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
               "FROM hremployee_master " & _
               "WHERE employeeunkid = '" & intEmployeeId & "' "

            objDataOperation.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtAppDate = objDataOperation.GetParameterValue("@ADate")


            strQ = "SELECT " & _
                      "  categorizationtranunkid " & _
                      ", CONVERT(CHAR(8),hremployee_categorization_tran.effectivedate,112) AS effectivedate " & _
                      ", CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS adate " & _
                      ", hremployee_categorization_tran.employeeunkid " & _
                      ", hremployee_categorization_tran.jobgroupunkid " & _
                      ", hremployee_categorization_tran.jobunkid " & _
                      ", hremployee_categorization_tran.gradeunkid " & _
                      ", hremployee_categorization_tran.gradelevelunkid " & _
                      ", hremployee_categorization_tran.changereasonunkid " & _
                      ", hremployee_categorization_tran.isfromemployee " & _
                      ", hremployee_categorization_tran.statusunkid " & _
                      ", hremployee_categorization_tran.userunkid " & _
                      ", hremployee_categorization_tran.isvoid " & _
                      ", hremployee_categorization_tran.voiduserunkid " & _
                      ", hremployee_categorization_tran.voiddatetime " & _
                      ", hremployee_categorization_tran.voidreason " & _
                      ", ISNULL(hrjobgroup_master.name,'') AS JobGroup " & _
                      ", ISNULL(hrjob_master.job_name,'') AS Job " & _
                      ", ISNULL(hrgrade_master.name,'') AS Grade " & _
                      ", ISNULL(hrgradelevel_master.name,'') AS GradeLevel " & _
                   ", CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name,'') END AS Reason " & _
                      ", ISNULL(hremployee_categorization_tran.rehiretranunkid,0) AS rehiretranunkid " & _
                     " FROM hremployee_categorization_tran " & _
                     " JOIN hremployee_master on hremployee_master.employeeunkid = hremployee_categorization_tran.employeeunkid  " & _
                     " LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hremployee_categorization_tran.jobgroupunkid " & _
                     " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_categorization_tran.jobunkid " & _
                     " LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = hremployee_categorization_tran.gradeunkid " & _
                     " LEFT JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = hremployee_categorization_tran.gradelevelunkid " & _
                     " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_categorization_tran.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RECATEGORIZE & _
                   " LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = hremployee_categorization_tran.changereasonunkid AND RH.mastertype =  " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                   "WHERE 1 = 1 " 'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END
            'S.SANDEEP [17 NOV 2015] -- START ISNULL(hremployee_categorization_tran.rehiretranunkid,0) -- END

            If intEmployeeId > 0 Then
                strQ &= " AND hremployee_categorization_tran.employeeunkid = '" & intEmployeeId & "' "
            End If

            If mdtAppDate.Trim.Length > 0 Then
                strQ &= " AND CONVERT(CHAR(8),hremployee_categorization_tran.effectivedate,112) >= '" & mdtAppDate & "' "
            End If

            If blnOnlyActive Then
                strQ &= " AND hremployee_categorization_tran.isvoid = 0 "
            End If

            strQ &= " ORDER BY effectivedate DESC "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each xRow As DataRow In dsList.Tables(0).Rows
                xRow("effectivedate") = eZeeDate.convertDate(xRow.Item("effectivedate").ToString).ToShortDateString
            Next


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_categorization_tran) </purpose>
    Public Function Insert(ByVal mblnCreateADUserFromEmpMst As Boolean, ByVal intCompanyId As Integer, _
                           ByVal xDatabaseName As String, _
                           Optional ByVal objDOperation As clsDataOperation = Nothing, _
                           Optional ByVal xMdtMigration As DataTable = Nothing, _
                           Optional ByVal xblnFromApproval As Boolean = False, _
                           Optional ByVal xblnFromApprovalScreen As Boolean = False) As Boolean 'S.SANDEEP [10-MAY-2017] -- START {intCompanyId} -- END


        'Pinkal (12-Oct-2020) -- Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens. [ByVal xDatabaseName As String]

        'Pinkal (18-Aug-2018) -- 'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[ByVal mblnCreateADUserFromEmpMst As Boolean]

        'Gajanan [23-SEP-2019] -- Add {xMdtMigration,xblnFromApproval,xblnFromApprovalScreen}

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        Dim mdtCurrentDate As Date = Nothing
        'Pinkal (18-Aug-2018) -- End


        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If



        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        If mblnCreateADUserFromEmpMst Then
            strQ = "SELECT @Date = GETDATE()"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCurrentDate, ParameterDirection.Output)
            objDataOperation.ExecNonQuery(strQ)
            mdtCurrentDate = CDate(objDataOperation.GetParameterValue("@Date"))
        End If
        'Pinkal (18-Aug-2018) -- End


        If isExist(mdtEffectivedate.Date, mintEmployeeunkid, -1, -1, -1, -1, -1, objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Re-Categorize information is already present for the selected effective date.")
            Return False
        End If

        'Pinkal (09-Apr-2015) -- Start
        'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA


        'S.SANDEEP [29 APR 2015] -- START
        'ALLOW SAME RECATEGORIZATION IF EMPLOYEE IS REHIRED AND USER WANT PUT IN SAME

        'dsList = Get_Current_Job(mdtEffectivedate.Date, mintEmployeeunkid)
        'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
        '    If CInt(dsList.Tables(0).Rows(0)("jobunkid")) = mintJobunkid AndAlso CInt(dsList.Tables(0).Rows(0)("jobgroupunkid")) = mintJobGroupunkid Then
        '        mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date.")
        '        Return False
        '    End If
        'End If
        'dsList = Nothing

        If mintRehiretranunkid <= 0 Then
        dsList = Get_Current_Job(mdtEffectivedate.Date, mintEmployeeunkid)
        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
            If CInt(dsList.Tables(0).Rows(0)("jobunkid")) = mintJobunkid AndAlso CInt(dsList.Tables(0).Rows(0)("jobgroupunkid")) = mintJobGroupunkid Then
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date.")
                Return False
            End If
        End If
        dsList = Nothing
        End If
        'S.SANDEEP [29 APR 2015] -- END


        'Pinkal (09-Apr-2015) -- End

        'S.SANDEEP [18 NOV 2015] -- START
        'If isExist(mdtEffectivedate.Date, mintEmployeeunkid, mintJobGroupunkid, mintJobunkid, mintGradeunkid, mintGradelevelunkid, , objDOperation) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date.")
        '    Return False
        'End If

        If isExist(mdtEffectivedate.Date, mintEmployeeunkid, mintJobGroupunkid, mintJobunkid, mintGradeunkid, mintGradelevelunkid, , objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date.")
            Return False
        End If
        'S.SANDEEP [18 NOV 2015] -- END


        If objDOperation Is Nothing Then
            objDataOperation.BindTransaction()
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobGroupunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'S.SANDEEP [14 APR 2015] -- START
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            'S.SANDEEP [14 APR 2015] -- END


            strQ = "INSERT INTO hremployee_categorization_tran ( " & _
                      "  effectivedate " & _
                      ", employeeunkid " & _
                      ", rehiretranunkid " & _
                      ", jobgroupunkid " & _
                      ", jobunkid " & _
                      ", gradeunkid " & _
                      ", gradelevelunkid " & _
                      ", changereasonunkid " & _
                      ", isfromemployee " & _
                      ", statusunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @effectivedate " & _
                      ", @employeeunkid " & _
                      ", @rehiretranunkid " & _
                      ", @jobgroupunkid " & _
                      ", @jobunkid " & _
                      ", @gradeunkid " & _
                      ", @gradelevelunkid " & _
                      ", @changereasonunkid " & _
                      ", @isfromemployee " & _
                      ", @statusunkid " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                    "); SELECT @@identity" 'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCategorizationtranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailForReCategorization(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            'S.SANDEEP [10-MAY-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

            'S.SANDEEP [19-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : STOP SYMMETRY REAL TIME (RUTTA'S REQUEST)
            'If Integrate_Symmetry(objDataOperation, intCompanyId, mintEmployeeunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [19-JUL-2017] -- END

            'S.SANDEEP [10-MAY-2017] -- END



            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.



            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

            'If MigrationInsert(intCompanyId, clsEmployeeMovmentApproval.enOperationType.ADDED, mblnCreateADUserFromEmpMst, objDataOperation, xMdtMigration, xblnFromApproval, xblnFromApprovalScreen) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            If MigrationInsert(xDatabaseName, intCompanyId, clsEmployeeMovmentApproval.enOperationType.ADDED, mblnCreateADUserFromEmpMst, objDataOperation, xMdtMigration, xblnFromApproval, xblnFromApprovalScreen) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (12-Oct-2020) -- End



            'Gajanan [23-SEP-2019] -- End



            'S.SANDEEP [14 APR 2015] -- START
            If objDOperation Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP [14 APR 2015] -- END



            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst Then
                If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
                    If SetADEmployeeJob(objDataOperation, mintEmployeeunkid, mdtCurrentDate, intCompanyId) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (04-Apr-2020) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)

            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

            'If mblnCreateADUserFromEmpMst Then
            '    If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
            '        If SetADEmployeeJob(objDataOperation, mintEmployeeunkid, mdtCurrentDate, intCompanyId) = False Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    End If
            'End If
            If ex.Message.ToString().Contains("System cannot connect with Active Directory at the moment. Please check Active Directory connection.  Also username or password (Aruti to AD Server) may be incorrect.") Then
                Throw New Exception(ex.Message)
            Else
                Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
                    End If
            'Pinkal (04-Apr-2020) -- End

            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_categorization_tran) </purpose>
    Public Function Update(ByVal intCompanyId As Integer, ByVal mblnCreateADUserFromEmpMst As Boolean, _
                           ByVal xDatabaseName As String, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal xMdtMigration As DataTable = Nothing, _
                           Optional ByVal xblnFromApproval As Boolean = False, _
                           Optional ByVal xblnFromApprovalScreen As Boolean = False) As Boolean 'S.SANDEEP [10-MAY-2017] -- START {intCompanyId} -- END

        'Pinkal (12-Oct-2020) -- Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.[ ByVal xDatabaseName As String, _]

        'Pinkal (18-Aug-2018) -- 'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[ ByVal mblnCreateADUserFromEmpMst As Boolean]

        'Gajanan [23-SEP-2019] -- Add {xMdtMigration,xblnFromApproval,xblnFromApprovalScreen}


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        Dim mdtCurrentDate As Date = Nothing
        'Pinkal (18-Aug-2018) -- End

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If


        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        If mblnCreateADUserFromEmpMst Then
            strQ = "SELECT @Date = GETDATE()"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCurrentDate, ParameterDirection.Output)
            objDataOperation.ExecNonQuery(strQ)
            mdtCurrentDate = CDate(objDataOperation.GetParameterValue("@Date"))
        End If
        'Pinkal (18-Aug-2018) -- End

        If isExist(mdtEffectivedate.Date, mintEmployeeunkid, -1, -1, -1, -1, mintCategorizationtranunkid, objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Re-Categorize information is already present for the selected effective date.")
            Return False
        End If

        If isExist(mdtEffectivedate.Date, mintEmployeeunkid, mintJobGroupunkid, mintJobunkid, mintGradeunkid, mintGradelevelunkid, mintCategorizationtranunkid, objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date.")
            Return False
        End If

        If xDataOpr Is Nothing Then
        objDataOperation.BindTransaction()
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@categorizationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategorizationtranunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobGroupunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'S.SANDEEP [14 APR 2015] -- START
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            'S.SANDEEP [14 APR 2015] -- END

            strQ = "UPDATE hremployee_categorization_tran SET " & _
                      "  effectivedate = @effectivedate" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", rehiretranunkid = @rehiretranunkid" & _
                      ", jobgroupunkid = @jobgroupunkid" & _
                      ", jobunkid = @jobunkid" & _
                      ", gradeunkid = @gradeunkid" & _
                      ", gradelevelunkid = @gradelevelunkid" & _
                      ", changereasonunkid = @changereasonunkid" & _
                      ", isfromemployee = @isfromemployee" & _
                      ", statusunkid = @statusunkid" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                    "WHERE categorizationtranunkid = @categorizationtranunkid " 'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForReCategorization(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [10-MAY-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

            'S.SANDEEP [19-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : STOP SYMMETRY REAL TIME (RUTTA'S REQUEST)
            'If Integrate_Symmetry(objDataOperation, intCompanyId, mintEmployeeunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [19-JUL-2017] -- END
            
            'S.SANDEEP [10-MAY-2017] -- END

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.


            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

            'If MigrationInsert(intCompanyId, clsEmployeeMovmentApproval.enOperationType.EDITED, mblnCreateADUserFromEmpMst, _
            '                   objDataOperation, xMdtMigration, xblnFromApproval, xblnFromApprovalScreen) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
                'End If

            If MigrationInsert(xDatabaseName, intCompanyId, clsEmployeeMovmentApproval.enOperationType.EDITED, mblnCreateADUserFromEmpMst, _
                               objDataOperation, xMdtMigration, xblnFromApproval, xblnFromApprovalScreen) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2020) -- End



            'Gajanan [23-SEP-2019] -- End

            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If


            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst Then
                If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
                If SetADEmployeeJob(objDataOperation, mintEmployeeunkid, mdtCurrentDate, intCompanyId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            End If
            'Pinkal (04-Apr-2020) -- End


            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If


            'Pinkal (04-Apr-2020) -- Start
                    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            'If mblnCreateADUserFromEmpMst Then
            '    If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
            '        If SetADEmployeeJob(objDataOperation, mintEmployeeunkid, mdtCurrentDate, intCompanyId) = False Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    End If
            'End If
            If ex.Message.ToString().Contains("System cannot connect with Active Directory at the moment. Please check Active Directory connection.  Also username or password (Aruti to AD Server) may be incorrect.") Then
                Throw New Exception(ex.Message)
            Else
                Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
                    End If
            'Pinkal (04-Apr-2020) -- End
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployee_categorization_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal intCompanyId As Integer, ByVal mblnCreateADUserFromEmpMst As Boolean _
                           , ByVal xDatabaseName As String, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal xMdtMigration As DataTable = Nothing, _
                           Optional ByVal xblnFromApproval As Boolean = False, _
                           Optional ByVal xblnFromApprovalScreen As Boolean = False) As Boolean 'S.SANDEEP [01 JUN 2016] -- START -- END


        'Pinkal (12-Oct-2020) -- Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.[ ByVal xDatabaseName As String]

        'Pinkal (18-Aug-2018) -- Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[ByVal mblnCreateADUserFromEmpMst As Boolean]

        'Gajanan [23-SEP-2019] -- Add {xMdtMigration,xblnFromApproval,xblnFromApprovalScreen}

        'S.SANDEEP [10-MAY-2017] -- START {intCompanyId} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        Dim mdtCurrentDate As Date = Nothing
        'Pinkal (18-Aug-2018) -- End

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If

        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        If mblnCreateADUserFromEmpMst Then
            strQ = "SELECT @Date = GETDATE()"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCurrentDate, ParameterDirection.Output)
            objDataOperation.ExecNonQuery(strQ)
            mdtCurrentDate = CDate(objDataOperation.GetParameterValue("@Date"))
        End If
        'Pinkal (18-Aug-2018) -- End


        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE hremployee_categorization_tran SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE categorizationtranunkid = @categorizationtranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@categorizationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ClearParameters()
            mintCategorizationtranunkid = intUnkid
            Call GetData(objDataOperation)

            If InsertAuditTrailForReCategorization(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [10-MAY-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

            'S.SANDEEP [19-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : STOP SYMMETRY REAL TIME (RUTTA'S REQUEST)
            'If Integrate_Symmetry(objDataOperation, intCompanyId, mintEmployeeunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [19-JUL-2017] -- END

            'S.SANDEEP [10-MAY-2017] -- END


            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.


            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

            'If MigrationInsert(intCompanyId, clsEmployeeMovmentApproval.enOperationType.DELETED, mblnCreateADUserFromEmpMst, objDataOperation, xMdtMigration, xblnFromApproval, xblnFromApprovalScreen) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            If MigrationInsert(xDatabaseName, intCompanyId, clsEmployeeMovmentApproval.enOperationType.DELETED, mblnCreateADUserFromEmpMst _
                                    , objDataOperation, xMdtMigration, xblnFromApproval, xblnFromApprovalScreen) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2020) -- End


            'Gajanan [23-SEP-2019] -- End

            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If



            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst Then
                If SetADEmployeeJob(objDataOperation, mintEmployeeunkid, mdtCurrentDate, intCompanyId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'Pinkal (04-Apr-2020) -- End

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)


            'Pinkal (04-Apr-2020) -- Start
                    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            'If mblnCreateADUserFromEmpMst Then
            '    If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
            '        If SetADEmployeeJob(objDataOperation, mintEmployeeunkid, mdtCurrentDate, intCompanyId) = False Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    End If
            'End If

            If ex.Message.ToString().Contains("System cannot connect with Active Directory at the moment. Please check Active Directory connection.  Also username or password (Aruti to AD Server) may be incorrect.") Then
                Throw New Exception(ex.Message)
            Else
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            End If
            'Pinkal (04-Apr-2020) -- End
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
    Public Function MigrationInsert(ByVal xDatabaseName As String, ByVal intCompanyId As Integer, _
                                    ByVal xOprationType As clsEmployeeMovmentApproval.enOperationType, _
                                    ByVal mblnCreateADUserFromEmpMst As Boolean, _
                                    Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                                    Optional ByVal xMdtMigration As DataTable = Nothing, _
                                    Optional ByVal xblnFromApproval As Boolean = False, _
                                    Optional ByVal xblnFromApprovalScreen As Boolean = False, _
                                    Optional ByVal eStatusId As clsEmployee_Master.EmpApprovalStatus = clsEmployee_Master.EmpApprovalStatus.Approved _
                                    ) As Boolean



        'Pinkal (12-Oct-2020) -- Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.[ByVal xDatabaseName As String]

        'Pinkal (07-Dec-2019) -- Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[ByVal mblnCreateADUserFromEmpMst As Boolean]

        'Pinkal (07-Dec-2019) -- End


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        objDataOperation.ClearParameters()
        Try
            If (IsNothing(xMdtMigration) = False AndAlso xMdtMigration.Rows.Count > 0) Or _
                (xblnFromApprovalScreen = True AndAlso IsNothing(xMdtMigration)) Then

                Dim objMovementMigration As New clsMovementMigration

                Dim objConfig As New clsConfigOptions
                Dim strParamKeys() As String = { _
                                           "EMPLOYEEASONDATE", _
                                           "CLAIMREQUEST_PAYMENTAPPROVALWITHLEAVEAPPROVAL" _
                                           }
                Dim MigrationDicKeyValues As New Dictionary(Of String, String)
                MigrationDicKeyValues = objConfig.GetKeyValue(intCompanyId, strParamKeys, objDataOperation)

                If IsNothing(MigrationDicKeyValues) = False _
                AndAlso MigrationDicKeyValues.ContainsKey("EmployeeAsOnDate") _
                AndAlso MigrationDicKeyValues.ContainsKey("ClaimRequest_PaymentApprovalwithLeaveApproval") Then
                    Dim Formname As String = String.Empty
                    If mstrWebFormName.Trim.Length <= 0 Then
                        Formname = mstrForm_Name
                    Else
                        Formname = mstrWebFormName
                    End If

                    If IsNothing(xMdtMigration) = True Then
                        objMovementMigration.isMovementInApprovalFlow(objMovementMigration._DataList, mintEmployeeunkid, clsEmployeeMovmentApproval.enOperationType.ADDED, _
                                                                      enApprovalMigrationScreenType.RECATEGORIZATION, 0, objDataOperation)

                        If IsNothing(objMovementMigration._DataList) = False Then
                            xMdtMigration = objMovementMigration._DataList
                        End If

                    End If


                    objMovementMigration._Ip = IIf(mstrWebClientIP.Trim = "", getIP, mstrWebClientIP)
                    objMovementMigration._Hostname = IIf(mstrWebHostName.Trim = "", getHostName, mstrWebHostName)
                    If mstrWebFormName.Trim.Length <= 0 Then
                        objMovementMigration._Form_Name = mstrForm_Name
                        objMovementMigration._Isweb = False
                    Else
                        objMovementMigration._Form_Name = mstrWebFormName
                        objMovementMigration._Isweb = True
                    End If



                    'Pinkal (12-Oct-2020) -- Start
                    'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

                    'If objMovementMigration.Insert(xMdtMigration, mintCategorizationtranunkid, mintEmployeeunkid, CInt(enApprovalMigrationScreenType.RECATEGORIZATION), _
                    '                         xblnFromApproval, MigrationDicKeyValues("EmployeeAsOnDate").ToString(), _
                    '                        CBool(MigrationDicKeyValues("ClaimRequest_PaymentApprovalwithLeaveApproval").ToString()), mintUserunkid, Formname, _
                    '                       CInt(xOprationType), mblnCreateADUserFromEmpMst, xblnFromApprovalScreen, objDataOperation, eStatusId) = False Then

                    If objMovementMigration.Insert(xDatabaseName, xMdtMigration, mintCategorizationtranunkid, mintEmployeeunkid, CInt(enApprovalMigrationScreenType.RECATEGORIZATION), _
                                             xblnFromApproval, MigrationDicKeyValues("EmployeeAsOnDate").ToString(), _
                                            CBool(MigrationDicKeyValues("ClaimRequest_PaymentApprovalwithLeaveApproval").ToString()), mintUserunkid, Formname, _
                                           CInt(xOprationType), mblnCreateADUserFromEmpMst, xblnFromApprovalScreen, objDataOperation, eStatusId) = False Then

                        'Pinkal (12-Oct-2020) -- End


                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce

                    End If
                End If
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If

        End Try
    End Function
    'Gajanan [23-SEP-2019] -- End  

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@categorizationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal dtEffectiveDate As Date, ByVal intEmployeeId As Integer, ByVal intJobGroupId As Integer, ByVal intJobId As Integer, ByVal intGradeId As Integer, _
                                      ByVal intGradeLevelId As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If

        Try

            objDataOperation.ClearParameters()

            strQ = "SELECT " & _
                      "  categorizationtranunkid " & _
                      ", effectivedate " & _
                      ", employeeunkid " & _
                      ", jobgroupunkid " & _
                      ", jobunkid " & _
                      ", gradeunkid " & _
                      ", gradelevelunkid " & _
                      ", changereasonunkid " & _
                      ", isfromemployee " & _
                      ", statusunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     "FROM hremployee_categorization_tran " & _
                      " WHERE  isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND categorizationtranunkid <> @categorizationtranunkid"
            End If

            If intEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            If intJobGroupId > 0 Then
                strQ &= " AND jobgroupunkid = @jobgroupunkid "
                objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobGroupId)
            End If

            If intJobId > 0 Then
                strQ &= " AND jobunkid = @jobunkid "
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobId)
            End If

            If intGradeId > 0 Then
                strQ &= " AND gradeunkid = @gradeunkid "
                objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeId)
            End If

            If intGradeLevelId > 0 Then
                strQ &= " AND gradelevelunkid = @gradelevelunkid "
                objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeLevelId)
            End If

            If dtEffectiveDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),effectivedate,112) = @effectivedate"
                'Gajanan [11-Dec-2019] -- Start   
                'Enhancement:Worked On December Cut Over Enhancement For NMB
                'objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEffectivedate))
                objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtEffectiveDate))
                'Gajanan [11-Dec-2019] -- End
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@categorizationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Get_Current_Job(ByVal xDate As Date, Optional ByVal xEmployeeId As Integer = 0, Optional ByVal objDataOperation As clsDataOperation = Nothing) As DataSet
        'Pinkal (18-Aug-2018) -- 'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[Optional ByVal objDataOperation As clsDataOperation = Nothing]

        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim mdtAppDate As String = ""

        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        Dim objDo As clsDataOperation
        'Pinkal (18-Aug-2018) -- End
        Try

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            'Using objDo As New clsDataOperation

            If objDataOperation Is Nothing Then
                objDo = New clsDataOperation
            Else
                objDo = objDataOperation
            End If
            objDo.ClearParameters()

            'Pinkal (18-Aug-2018) -- End


                If xEmployeeId > 0 Then
                    StrQ = "SELECT " & _
                           "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                           "FROM hremployee_master " & _
                           "WHERE employeeunkid = '" & xEmployeeId & "' "

                    objDo.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

                    objDo.ExecNonQuery(StrQ)

                    If objDo.ErrorMessage <> "" Then
                        Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    End If

                    mdtAppDate = objDo.GetParameterValue("@ADate")
                End If

                StrQ = "SELECT " & _
                       "     effectivedate " & _
                       "    ,employeeunkid " & _
                       "    ,jobgroupunkid  " & _
                       "    ,jobunkid " & _
                       "    ,gradeunkid " & _
                       "    ,gradelevelunkid " & _
                   "    , Job " & _
                  "     , JobGrp " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT " & _
                       "         ERC.effectivedate " & _
                       "        ,ERC.employeeunkid " & _
                       "        ,ERC.jobgroupunkid  " & _
                       "        ,ERC.jobunkid " & _
                       "        ,jobgradeunkid AS gradeunkid " & _
                       "        ,ERC.gradelevelunkid " & _
                       "        ,hrjob_master.job_name As Job " & _
                   "        ,hrjobgroup_master.name  As JobGrp " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                       "    FROM hremployee_categorization_tran AS ERC " & _
                       "        JOIN hrjob_master ON ERC.jobunkid = hrjob_master.jobunkid " & _
                   "        LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = ERC.jobgroupunkid " & _
                       "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effdate "


            'Pinkal (04-Apr-2020) --            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = ERC.jobgroupunkid]


            'S.SANDEEP |18-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'REMOVED : CASE WHEN ERC.gradeunkid <=0 THEN jobgradeunkid ELSE ERC.gradeunkid END AS gradeunkid
            'ADDED : jobgradeunkid AS gradeunkid
            'S.SANDEEP |18-FEB-2019| -- END

            'Pinkal (18-Aug-2018) --  'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[ "        ,ERC.job_name As Job " & _]

                If xEmployeeId > 0 Then
                    StrQ &= " AND employeeunkid = '" & xEmployeeId & "' "
                End If
                StrQ &= ") AS CA WHERE CA.xNo = 1 "

                'S.SANDEEP [07 APR 2015] -- START
                If mdtAppDate.Trim.Length > 0 Then
                    StrQ &= " AND CONVERT(CHAR(8),effectivedate,112) >= '" & mdtAppDate & "' "
                End If
                'S.SANDEEP [07 APR 2015] -- END


                objDo.AddParameter("@effdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate))

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            'End Using
            'Pinkal (18-Aug-2018) -- End
        Catch ex As Exception
            If objDataOperation Is Nothing Then objDo = Nothing
            Throw New Exception(ex.Message & "; Procedure Name: Get_Current_Job; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function IsReCategorizeForEmpExist(ByVal xEmployeeId As Integer, Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If
        Try
            StrQ = "SELECT * From hremployee_categorization_tran WHERE isvoid = 0  AND employeeunkid = " & xEmployeeId
            objDataOperation.ClearParameters()
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If
            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsReCategorizeForEmpExist; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Private Function InsertAuditTrailForReCategorization(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As enAuditType) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            StrQ = "INSERT INTO athremployee_categorization_tran ( " & _
                        "categorizationtranunkid " & _
                        ",effectivedate " & _
                        ",employeeunkid " & _
                        ",rehiretranunkid " & _
                        ",jobgroupunkid  " & _
                        ",jobunkid " & _
                        ",gradeunkid " & _
                        ",gradelevelunkid " & _
                        ",changereasonunkid " & _
                        ",isfromemployee " & _
                        ",statusunkid " & _
                        ",audittype " & _
                        ",audituserunkid " & _
                        ",auditdatetime " & _
                        ",ip " & _
                        ",machine_name " & _
                        ",form_name " & _
                        ",module_name1 " & _
                        ",module_name2 " & _
                        ",module_name3 " & _
                        ",module_name4 " & _
                        ",module_name5 " & _
                        ",isweb " & _
                    ") VALUES (" & _
                        " @categorizationtranunkid " & _
                        ",@effectivedate " & _
                        ",@employeeunkid " & _
                        ",@rehiretranunkid " & _
                        ",@jobgroupunkid  " & _
                        ",@jobunkid " & _
                        ",@gradeunkid " & _
                        ",@gradelevelunkid " & _
                        ",@changereasonunkid " & _
                        ",@isfromemployee " & _
                        ",@statusunkid " & _
                        ",@audittype " & _
                        ",@audituserunkid " & _
                        ",@auditdatetime " & _
                        ",@ip " & _
                        ",@machine_name " & _
                        ",@form_name " & _
                        ",@module_name1 " & _
                        ",@module_name2 " & _
                        ",@module_name3 " & _
                        ",@module_name4 " & _
                        ",@module_name5 " & _
                        ",@isweb " & _
                        ") " 'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@categorizationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategorizationtranunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobGroupunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP.Trim))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName.Trim))
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 3, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            'S.SANDEEP [14 APR 2015] -- START
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            'S.SANDEEP [14 APR 2015] -- END


            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForReCategorization; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Private Function Set_Notification_Allocation(ByVal StrUserName As String, _
                                               ByVal xEmpId As Integer, _
                                               ByVal xEcode As String, _
                                               ByVal xEmployeeName As String, _
                                               ByVal xCurrentAlloc As Dictionary(Of Integer, String), _
                                               ByVal xNotifAlloc As String, _
                                               ByVal xEffectiveDate As Date, _
                                               Optional ByVal xHost As String = "", _
                                               Optional ByVal xIP As String = "", Optional ByVal xCurr_User As String = "") As String

        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Try

            If xCurrentAlloc.Keys.Count <= 0 Then Return ""

            If xNotifAlloc.Trim.Length <= 0 Then
                xNotifAlloc = ConfigParameter._Object._Notify_Allocation
            End If

            Dim xOldAlloc As New DataSet
            Dim xOldDate As Date = DateAdd(DateInterval.Day, -1, xEffectiveDate)
            xOldAlloc = Get_Current_Job(xOldDate, xEmpId)

            If xOldAlloc.Tables(0).Rows.Count <= 0 Then
                Dim xRow As DataRow = xOldAlloc.Tables(0).NewRow

                xRow.Item("effectivedate") = xOldDate
                xRow.Item("employeeunkid") = xEmpId
                xRow.Item("jobgroupunkid") = 0
                xRow.Item("jobunkid") = 0
                xRow.Item("gradeunkid") = 0
                xRow.Item("gradelevelunkid") = 0

                xOldAlloc.Tables(0).Rows.Add(xRow)
            End If


            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            If xNotifAlloc.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
                StrMessage.Append(Language.getMessage("frmEmployeeMaster", 147, "Dear") & " " & "<b>" & getTitleCase(StrUserName) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End


                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that changes have been made for employee : <b>" & xEmployeeName & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 142, "This is to inform you that changes have been made for employee") & " " & "<b>" & " " & getTitleCase(xEmployeeName) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & xEcode & "</b>. Following information has been changed by user : <b>" & IIf(xCurr_User.Trim = "", User._Object._Firstname & " " & User._Object._Lastname, xCurr_User) & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 143, "with employeecode") & " " & "<b>" & " " & xEcode & "</b>." & " " & Language.getMessage("frmEmployeeMaster", 144, "Following information has been changed by user") & " " & "<b>" & " " & getTitleCase(IIf(xCurr_User.Trim = "", User._Object._Firstname & " " & User._Object._Lastname, xCurr_User)) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End


                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; '>")


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" &  & "</b> and IPAddress : <b>" & IIf(xIP.Trim = "", getIP.ToString, xIP) & "</b></span></p>")
                StrMessage.Append(" " & Language.getMessage("frmEmployeeMaster", 145, "from Machine") & " " & "<b>" & IIf(xHost.Trim = "", getHostName.ToString, xHost) & "</b>" & " " & Language.getMessage("frmEmployeeMaster", 146, "and IPAddress") & " " & "<b>" & IIf(xIP.Trim = "", getIP.ToString, xIP) & "</b></span></p>")
                'Gajanan [27-Mar-2019] -- End


                StrMessage.Append(vbCrLf)

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'Gajanan (21 Nov 2018) -- Start
                'StrMessage.Append("<TABLE border = '1' WIDTH = '50%'>")
                'StrMessage.Append("<TABLE border = '1' WIDTH = '50%' style='margin-left: 25px'>")
                StrMessage.Append("<TABLE border = '1' WIDTH = '50%' >")
                'Gajanan (21 Nov 2018) -- End
                'Gajanan [27-Mar-2019] -- End
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:30%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 5, "Effective Date") & "</span></b></TD>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & xEffectiveDate.ToShortDateString & "</span></b></TD>")
                StrMessage.Append("</TR>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("</TABLE>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<BR>")
                'Gajanan (21 Nov 2018) -- Start
                StrMessage.Append("<TABLE border = '1' WIDTH = '90%' style='margin-left: 25px'>")
                'Gajanan (21 Nov 2018) -- End
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TR WIDTH = '90%' bgcolor= 'SteelBlue'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 6, "Job") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:35%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 7, "Old Job") & "</span></b></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:35%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 8, "New Job") & "</span></b></TD>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("</TR>")
                For Each sId As String In xNotifAlloc.Split(CChar("||"))(0).Split(CChar(","))
                    Select Case CInt(sId)
                        Case enAllocation.JOB_GROUP
                            Dim ObjJobGrp As New clsJobGroup
                            If xCurrentAlloc.ContainsKey(CInt(sId)) = True Then
                                ObjJobGrp._Jobgroupunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("jobgroupunkid"))
                                StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 422, "Job Group") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjJobGrp._Name.Trim = "", "&nbsp;", ObjJobGrp._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentAlloc(CInt(sId)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.JOBS
                            Dim ObjJob As New clsJobs
                            If xCurrentAlloc.ContainsKey(CInt(sId)) = True Then
                                ObjJob._Jobunkid = CInt(xOldAlloc.Tables(0).Rows(0).Item("jobunkid"))
                                StrMessage.Append("<TR WIDTH = '90%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & Language.getMessage("clsMasterData", 421, "Jobs") & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & IIf(ObjJob._Job_Name.Trim = "", "&nbsp;", ObjJob._Job_Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '35%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & xCurrentAlloc(CInt(sId)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                    End Select
                Next
            End If
            StrMessage.Append("</TABLE>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language

            'Gajanan (21 Nov 2018) -- Start 
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
           'Gajanan (21 Nov 2018) -- End
            'Gajanan [27-Mar-2019] -- End
            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            If blnFlag = False Then
                StrMessage = StrMessage.Remove(0, StrMessage.Length)
            End If

            Return StrMessage.ToString

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Set_Notification_Allocation; Module Name: " & mstrModuleName)
            Return ""
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Sub SendEmails(ByVal xEmployeeId As Integer, _
                          ByVal xCode As String, _
                          ByVal xEName As String, _
                          ByVal xDict_CurrentAlloc As Dictionary(Of Integer, String), _
                          ByVal xConfigAllocNotif As String, _
                          ByVal xEffDate As Date, _
                          ByVal intCompanyUnkId As Integer, _
                          Optional ByVal xHostName As String = "", _
                          Optional ByVal xIPAddr As String = "", _
                          Optional ByVal xLoggedUserName As String = "", _
                          Optional ByVal xUserId As Integer = 0, _
                          Optional ByVal xLoginMod As enLogin_Mode = enLogin_Mode.DESKTOP)
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Try
            If xConfigAllocNotif.Trim.Length <= 0 Then xConfigAllocNotif = ConfigParameter._Object._Notify_Allocation
            If xConfigAllocNotif.Trim.Length > 0 Then
                Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                Dim objSendMail As New clsSendMail
                For Each sId As String In xConfigAllocNotif.Split(CChar("||"))(2).Split(CChar(","))
                    objUsr._Userunkid = CInt(sId)
                    StrMessage = Set_Notification_Allocation(objUsr._Firstname & " " & objUsr._Lastname, xEmployeeId, xCode, xEName, xDict_CurrentAlloc, xConfigAllocNotif, xEffDate, xHostName, xIPAddr, xLoggedUserName)
                    If StrMessage <> "" Then
                        'gobjEmailList.Add(New clsEmailCollection(objUsr._Email, _
                        '                                         Language.getMessage(mstrModuleName, 4, "Notification of Changes in Employee Master file"), _
                        '                                         StrMessage, _
                        '                                         "", _
                        '                                         0, _
                        '                                         "", _
                        '                                         "", _
                        '                                         xUserId, _
                        '                                         xLoginMod, _
                        '                                         clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT, _
                        '                                         objUsr._Firstname & " " & objUsr._Lastname))

                        objSendMail._ToEmail = objUsr._Email
                        objSendMail._Subject = Language.getMessage(mstrModuleName, 4, "Notification of Changes in Employee Master file")
                        objSendMail._Message = StrMessage
                        objSendMail._Form_Name = ""
                        objSendMail._LogEmployeeUnkid = 0
                        objSendMail._OperationModeId = xLoginMod
                        objSendMail._UserUnkid = xUserId
                        objSendMail._SenderAddress = objUsr._Firstname & " " & objUsr._Lastname
                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                        Try
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(intCompanyUnkId)
                            'Sohail (30 Nov 2017) -- End
                        Catch ex As Exception
                        End Try

                    End If
                Next
                objUsr = Nothing
                'trd = New Thread(AddressOf Send_Notification)
                'trd.IsBackground = True
                'trd.Start()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendEmails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Private Sub Send_Notification(ByVal intCompanyUnkId As Integer)
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        Try
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId
                    Try
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        objSendMail.SendMail(intCompanyUnkId)
                        'Sohail (30 Nov 2017) -- End
                    Catch ex As Exception
                    End Try
                Next
                gobjEmailList.Clear()
            End If
        Catch ex As Exception
            Throw New Exception(mstrModuleName & ":Send_Notification:- " & ex.Message)
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub

    'Shani(18-JUN-2016) -- Start
    'Enhancement : Add Allocation Column in Job master[Rutta]
    Public Function getJobComboList(Optional ByVal strListName As String = "List", _
                                    Optional ByVal mblnFlag As Boolean = False, _
                                    Optional ByVal intBrachUnkID As Integer = -1, _
                                    Optional ByVal intDepartmentGrpUnkID As Integer = -1, _
                                    Optional ByVal intDepartmentUnkid As Integer = -1, _
                                    Optional ByVal intSectionGrpUnkID As Integer = -1, _
                                    Optional ByVal intSectionId As Integer = -1, _
                                    Optional ByVal intUnitGrpUnkID As Integer = -1, _
                                    Optional ByVal intUnitId As Integer = -1, _
                                    Optional ByVal intTeamUnkid As Integer = -1, _
                                    Optional ByVal intClassGroupUnkid As Integer = -1, _
                                    Optional ByVal intClassUnkID As Integer = -1, _
                                    Optional ByVal intGradeId As Integer = -1, _
                                    Optional ByVal intGradeLevelUnkID As Integer = -1, _
                                    Optional ByVal intJobgroupid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                   " jobunkid As jobunkid " & _
                   ",name As name " & _
                   ",job_level AS job_level " & _
                   " FROM ( "
            If mblnFlag = True Then
                strQ &= "SELECT 0 As jobunkid , @ItemName As  name , -1 As job_level UNION "
            End If

            strQ &= "SELECT jobunkid, job_name, job_level As  name FROM hrjob_master WHERE isactive =1 "

            If intBrachUnkID > 0 Then
                If JobRecordCount("jobbranchunkid", objDataOperation) > 0 Then strQ &= "AND jobbranchunkid = '" & intBrachUnkID & "' "
            End If
            If intDepartmentGrpUnkID > 0 Then
                If JobRecordCount("jobdepartmentgrpunkid", objDataOperation) > 0 Then strQ &= "AND jobdepartmentgrpunkid = '" & intDepartmentGrpUnkID & "' "
            End If
            If intDepartmentUnkid > 0 Then
                If JobRecordCount("jobdepartmentunkid", objDataOperation) > 0 Then strQ &= "AND jobdepartmentunkid = '" & intDepartmentUnkid & "' "
            End If
            If intSectionGrpUnkID > 0 Then
                If JobRecordCount("jobsectiongrpunkid", objDataOperation) > 0 Then strQ &= "AND jobsectiongrpunkid = '" & intSectionGrpUnkID & "' "
            End If
            If intSectionId > 0 Then
                If JobRecordCount("jobsectionunkid", objDataOperation) > 0 Then strQ &= "AND jobsectionunkid = '" & intSectionId & "' "
            End If
            If intUnitGrpUnkID > 0 Then
                If JobRecordCount("jobunitgrpunkid", objDataOperation) > 0 Then strQ &= "AND jobunitgrpunkid = '" & intUnitGrpUnkID & "' "
            End If
            If intUnitId > 0 Then
                If JobRecordCount("jobunitunkid", objDataOperation) > 0 Then strQ &= "AND jobunitunkid = '" & intUnitId & "' "
            End If
            If intTeamUnkid > 0 Then
                If JobRecordCount("teamunkid", objDataOperation) > 0 Then strQ &= "AND teamunkid = '" & intTeamUnkid & "' "
            End If
            If intClassGroupUnkid > 0 Then
                If JobRecordCount("jobclassgroupunkid", objDataOperation) > 0 Then strQ &= "AND jobclassgroupunkid = '" & intClassGroupUnkid & "' "
            End If
            If intClassUnkID > 0 Then
                If JobRecordCount("jobclassunkid", objDataOperation) > 0 Then strQ &= "AND jobclassunkid = '" & intClassUnkID & "' "
            End If
            If intGradeId > 0 Then
                If JobRecordCount("jobgradeunkid", objDataOperation) > 0 Then strQ &= "AND jobgradeunkid = '" & intGradeId & "' "
            End If
            If intGradeLevelUnkID > 0 Then
                If JobRecordCount("jobgradelevelunkid", objDataOperation) > 0 Then strQ &= "AND jobgradelevelunkid = '" & intGradeLevelUnkID & "' "
            End If
            If intJobgroupid > 0 Then
                If JobRecordCount("jobgroupunkid", objDataOperation) > 0 Then strQ &= "AND jobgroupunkid = '" & intJobgroupid & "' "
            End If

            strQ &= ") As Job Where 1 = 1 "

            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Shani (15-JUL-2016) -- Start
            'Issue : job are not coming to employee master to adding empoyee (reason : filter allocation wise job)
            If dsList.Tables(0).Select("jobunkid <> 0").Count <= 0 Then
                objDataOperation.ClearParameters()
                strQ = "SELECT " & _
                       " jobunkid As jobunkid " & _
                       ",name As name " & _
                       ",job_level AS job_level " & _
                       " FROM ( "
                If mblnFlag = True Then
                    strQ &= "SELECT 0 As jobunkid , @ItemName As  name , -1 As job_level UNION "
                End If

                strQ &= "SELECT jobunkid, job_name, job_level As  name FROM hrjob_master WHERE isactive =1 AND jobbranchunkid = 0 " & _
                        "       AND jobdepartmentgrpunkid <= 0 " & _
                        "       AND jobdepartmentunkid <= 0 " & _
                        "       AND jobsectiongrpunkid <= 0 " & _
                        "       AND jobsectionunkid <= 0 " & _
                        "       AND jobunitgrpunkid <= 0 " & _
                        "       AND jobunitunkid <= 0 " & _
                        "       AND teamunkid <= 0 " & _
                        "       AND jobclassgroupunkid <= 0 " & _
                        "       AND jobclassunkid <= 0 " & _
                        "       AND jobgradeunkid <= 0 " & _
                        "       AND jobgradelevelunkid <= 0 " & _
                        "       AND jobgroupunkid <= 0 "
                strQ &= ") As Job Where 1 = 1 "

                objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Select"))

                dsList = objDataOperation.ExecQuery(strQ, strListName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'Shani (15-JUL-2016) -- End

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        End Try
    End Function

    Private Function JobRecordCount(ByVal strColumn As String, ByVal objDataOperation As clsDataOperation) As Integer
        Dim intRcord As Integer = 0
        Dim strQ As String = ""
        Try
            strQ = "SELECT * FROM hrjob_master WHERE isactive = 1 AND " & strColumn & " > 0 "
            intRcord = objDataOperation.RecordCount(strQ)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "JobRecordCount", mstrModuleName)
            intRcord = 0
        End Try
        Return intRcord
    End Function
    'Shani(18-JUN-2016) -- End


    'S.SANDEEP [10-MAY-2017] -- START
    'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION
    Private Function Integrate_Symmetry(ByVal xDataOpr As clsDataOperation, _
                                        ByVal intCompanyId As Integer, _
                                        ByVal intEmpId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsDetails As New DataSet
        Dim exForce As Exception = Nothing
        Dim dtEmpAsOnDate As String = ""
        Dim dtServerDate As Date
        Dim blnIsSymmetryIntegrated As Boolean = False
        Try
            If intCompanyId > 0 Then
                StrQ = "SELECT " & _
                       "  @issymmetryintegrated = cfconfiguration.key_value " & _
                       "FROM hrmsConfiguration..cfconfiguration " & _
                       "WHERE UPPER(cfconfiguration.key_name) = 'ISSYMMETRYINTEGRATED' " & _
                       "AND cfconfiguration.companyunkid = @companyunkid "
                With xDataOpr
                    .ClearParameters()
                    .AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
                    .AddParameter("@issymmetryintegrated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsSymmetryIntegrated, ParameterDirection.Output)

                    .ExecNonQuery(StrQ)
                    If .ErrorMessage <> "" Then
                        exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                        Throw exForce
                    End If
                    'S.SANDEEP [12-JUN-2017] -- START
                    'ISSUE/ENHANCEMENT : SYMMETRY DBNULL TO BOOLEAN ERROR
                    'blnIsSymmetryIntegrated = .GetParameterValue("@issymmetryintegrated")
                    If IsDBNull(.GetParameterValue("@issymmetryintegrated")) = False Then
                    blnIsSymmetryIntegrated = .GetParameterValue("@issymmetryintegrated")
                    End If
                    'S.SANDEEP [12-JUN-2017] -- END
                End With

                If blnIsSymmetryIntegrated Then
                    Dim mstrEmpCode As String = String.Empty
                    StrQ = "SELECT @employeecode = employeecode,@serverdate = GETDATE() FROM hremployee_master WHERE employeeunkid = @employeeunkid "
                    With xDataOpr
                        .ClearParameters()
                        .AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
                        .AddParameter("@employeecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpCode, ParameterDirection.InputOutput)
                        .AddParameter("@ServerDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtServerDate, ParameterDirection.Output)
                        .ExecNonQuery(StrQ)
                        If .ErrorMessage <> "" Then
                            exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                        mstrEmpCode = .GetParameterValue("@employeecode")
                        dtServerDate = .GetParameterValue("@serverdate")
                    End With

                    StrQ = "SELECT " & _
                           "  @empasondate = cfconfiguration.key_value " & _
                           "FROM hrmsConfiguration..cfconfiguration " & _
                           "WHERE UPPER(cfconfiguration.key_name) = 'EMPLOYEEASONDATE' " & _
                           "AND cfconfiguration.companyunkid = @companyunkid "
                    With xDataOpr
                        .ClearParameters()
                        .AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
                        .AddParameter("@empasondate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, dtEmpAsOnDate, ParameterDirection.Output)

                        .ExecNonQuery(StrQ)
                        If .ErrorMessage <> "" Then
                            exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                        dtEmpAsOnDate = .GetParameterValue("@empasondate")
                    End With

                    Dim strPersonalData4 As String : strPersonalData4 = ""

                    StrQ = "SELECT  TOP 1 " & _
                           "    @PersonalData4 = ISNULL(hrjob_master.job_name,'') " & _
                           "FROM hremployee_categorization_tran " & _
                           "    LEFT JOIN hrjob_master ON hremployee_categorization_tran.jobunkid = hrjob_master.jobunkid " & _
                           "WHERE hremployee_categorization_tran.isvoid = 0 " & _
                           "    AND hremployee_categorization_tran.employeeunkid = @employeeunkid " & _
                           "    AND CONVERT(NVARCHAR (8),hremployee_categorization_tran.effectivedate,112) <= @xDate " & _
                           "ORDER BY CONVERT(NVARCHAR (8),hremployee_categorization_tran.effectivedate,112) DESC "

                    With xDataOpr
                        .ClearParameters()
                        .AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
                        .AddParameter("@xDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtEmpAsOnDate)
                        .AddParameter("@PersonalData4", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strPersonalData4, ParameterDirection.InputOutput)

                        .ExecNonQuery(StrQ)
                        If .ErrorMessage <> "" Then
                            exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                        strPersonalData4 = .GetParameterValue("@PersonalData4")
                    End With

                    If mstrEmpCode.Trim.Length > 0 Then
                        StrQ = "SELECT " & _
                               "     cfconfiguration.key_name " & _
                               "    ,cfconfiguration.key_value " & _
                               "FROM hrmsConfiguration..cfconfiguration " & _
                               "WHERE cfconfiguration.companyunkid = @companyunkid " & _
                               "AND UPPER(cfconfiguration.key_name) LIKE '%SYMMETRY%' "

                        xDataOpr.ClearParameters()
                        xDataOpr.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)

                        dsDetails = xDataOpr.ExecQuery(StrQ, "List")

                        If xDataOpr.ErrorMessage <> "" Then
                            exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                            Throw exForce
                        End If

                        If dsDetails.Tables("List").Rows.Count > 0 Then
                            Dim strConn As String = String.Empty
                            Using oSQL As SqlClient.SqlConnection = New SqlClient.SqlConnection
                                strConn = "Data Source="
                                Dim tmp As DataRow() = Nothing
                                tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYDATASERVERADDRESS'")
                                If tmp.Length > 0 Then strConn &= tmp(0)("key_value") & ";"

                                tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYDATABASENAME'")
                                If tmp.Length > 0 Then strConn &= "Initial Catalog=" & tmp(0)("key_value") & ";"

                                tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYAUTHENTICATIONMODEID'")
                                If tmp.Length > 0 Then
                                    If CInt(tmp(0)("key_value")) = 0 Then   'WINDOWS
                                        strConn &= "Integrated Security=True "
                                    ElseIf CInt(tmp(0)("key_value")) = 1 Then   'USER
                                        tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYDATABASEUSERNAME'")
                                        If tmp.Length > 0 Then strConn &= "User ID=" & tmp(0)("key_value") & ";"
                                        tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYDATABASEPASSWORD'")
                                        If tmp.Length > 0 Then strConn &= "Password=" & clsSecurity.Decrypt(tmp(0)("key_value"), "ezee") & ";"
                                    End If
                                End If
                                oSQL.ConnectionString = strConn
                                oSQL.Open()
                                StrQ = "" : Dim intRecCount As Integer = 0
                                StrQ = "SELECT * FROM DataImportTable WHERE DataImportTable.EmployeeReference = @EmployeeReference "
                                Using oCmd As SqlClient.SqlCommand = New SqlClient.SqlCommand
                                    oCmd.Connection = oSQL
                                    oCmd.CommandText = StrQ
                                    oCmd.Parameters.Clear()
                                    oCmd.Parameters.AddWithValue("@EmployeeReference", mstrEmpCode)
                                    Dim dsInfo As New DataSet
                                    Dim oDa As New SqlClient.SqlDataAdapter(oCmd)
                                    oDa.Fill(dsInfo)

                                    oCmd.Parameters.Clear()
                                    If dsInfo.Tables(0).Rows.Count > 0 Then 'UPDATE
                                        StrQ = "UPDATE DataImportTable SET " & _
                                               " PersonalData4 = @PersonalData4 " & _
                                               ",RecordRequest = @RecordRequest " & _
                                               ",ImportNow = @ImportNow " & _
                                               " WHERE RecordCount = @RecordCount "

                                        oCmd.Parameters.AddWithValue("@RecordCount", dsInfo.Tables(0).Rows(0).Item("RecordCount"))
                                        oCmd.Parameters.AddWithValue("@PersonalData4", strPersonalData4.ToString.Substring(0, IIf(Len(strPersonalData4) < 40, Len(strPersonalData4), 40)))
                                        oCmd.Parameters.AddWithValue("@RecordRequest", IIf(dsInfo.Tables(0).Rows(0).Item("RecordRequest") <> 0, dsInfo.Tables(0).Rows(0).Item("RecordRequest"), 0))
                                        oCmd.Parameters.AddWithValue("@ImportNow", 1)

                                        oCmd.CommandText = StrQ
                                        oCmd.ExecuteNonQuery()
                                    End If
                                End Using
                            End Using
                        End If
                    End If
                End If
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Integrate_Symmetry; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [10-MAY-2017] -- END




    'Pinkal (04-Apr-2020) -- Start
    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

    Private Function SetADEmployeeJob(ByVal objDataOperation As clsDataOperation, ByVal xEmployeeID As Integer, ByVal xCurrentDate As Date, ByVal xCompanyId As Integer) As Boolean
        'Pinkal (09-Mar-2020) --  'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[ByVal xCompanyId As Integer]

        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim mdtDate As DateTime = Nothing
        Dim mstrDisplayName As String = ""
        Dim mstrADProperty As String = ""
        Dim exForce As Exception = Nothing
        Try

            strQ = "SELECT  @DisplayName = ISNULL(displayname,'') FROM hremployee_master WHERE employeeunkid =  @employeeunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeID, ParameterDirection.Input)
            objDataOperation.AddParameter("@DisplayName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDisplayName, ParameterDirection.Output)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrDisplayName = objDataOperation.GetParameterValue("@DisplayName").ToString()

            dsList = Get_Current_Job(xCurrentDate.Date, mintEmployeeunkid, objDataOperation)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then



                'Pinkal (04-Apr-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                Dim mintDescriptionAllocationID As Integer = 0
                Dim mstrDescriptionAllocationName As String = ""
                Dim objADAttribute As New clsADAttribute_mapping
                Dim dtAttributes As DataTable = objADAttribute.GetList("List", xCompanyId, True, objDataOperation)
                If dtAttributes IsNot Nothing AndAlso dtAttributes.Rows.Count > 0 Then

                    '/* START TO GET DESCRIPTION ALLOCATION NAME 
                    Dim drDescriptionAllocation = dtAttributes.AsEnumerable().Where(Function(x) x.Field(Of String)("attributename") = clsADAttribute_mapping.enAttributes.Description.ToString()).Select(Function(x) x.Field(Of Integer)("mappingunkid"))
                    If drDescriptionAllocation IsNot Nothing AndAlso drDescriptionAllocation.Count > 0 Then
                        mintDescriptionAllocationID = CInt(drDescriptionAllocation(0))
                    End If

                    If mintDescriptionAllocationID > 0 Then

                        Select Case CType(mintDescriptionAllocationID, enAllocation)

                            Case enAllocation.JOB_GROUP
                                mstrDescriptionAllocationName = dsList.Tables(0).Rows(0)("JobGrp").ToString()
                            Case enAllocation.JOBS
                                mstrDescriptionAllocationName = dsList.Tables(0).Rows(0)("Job").ToString()
                        End Select

                    End If
                    '/* END TO GET DESCRIPTION ALLOCATION NAME 
                End If

                'UpdateEmpJobInAD(xCompanyId, mstrDisplayName, dsList.Tables(0).Rows(0)("Job").ToString())
                UpdateEmpJobInAD(xCompanyId, mstrDisplayName, dsList.Tables(0).Rows(0)("Job").ToString(), mstrDescriptionAllocationName)
                'Pinkal (04-Apr-2020) -- End
            End If
        Catch ex As Exception

            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If ex.Message.ToString().Contains("System cannot connect with Active Directory at the moment. Please check Active Directory connection.  Also username or password (Aruti to AD Server) may be incorrect.") Then
                Throw New Exception(ex.Message)
            Else
            Throw New Exception(mstrModuleName & "SetADEmployeeJob:- " & ex.Message & " [" & mstrADProperty & "]")
            End If
            'Pinkal (04-Apr-2020) -- End
        End Try
        Return True
    End Function

    'Pinkal (04-Apr-2020) -- End

    'Pinkal (04-Apr-2020) -- Start
    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
    Private Sub UpdateEmpJobInAD(ByVal xCompanyId As Integer, ByVal mstrDisplayName As String, ByVal mstrJobName As String, Optional ByVal mstrDescriptionAllocationName As String = "")
        Dim mstrADProperty As String = ""
        Dim mstrADIPAddress As String = ""
        Dim mstrADDomain As String = ""
        Dim mstrADUserName As String = ""
        Dim mstrADUserPwd As String = ""
        Dim entry As DirectoryEntry = Nothing
        Dim result As SearchResult = Nothing
        Try
            
            GetADConnection(mstrADIPAddress, mstrADDomain, mstrADUserName, mstrADUserPwd, objDataOperation)

            If IsADUserExist(mstrDisplayName, mstrADIPAddress, mstrADDomain, mstrADUserName, mstrADUserPwd, result) Then

                'Pinkal (15-Feb-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                'entry = New DirectoryEntry(result.Path)
                entry = New DirectoryEntry(result.Path, mstrADUserName, mstrADUserPwd)
                'Pinkal (15-Feb-2020) -- End


                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

                If IsDBNull(entry.Properties("Description").Value) = False AndAlso entry.Properties("Description").Value <> Nothing Then
                If mstrDescriptionAllocationName.Trim.Length > 0 AndAlso entry.Properties("Description").Value.ToString() <> mstrDescriptionAllocationName.Trim Then
                    SetADProperty(entry, "Description", mstrDescriptionAllocationName, mstrADProperty) 'Description
                    entry.CommitChanges()
                    entry.RefreshCache()
                End If
                Else
                    If mstrDescriptionAllocationName.Trim.Length > 0 Then
                        SetADProperty(entry, "Description", mstrDescriptionAllocationName, mstrADProperty) 'Description
                        entry.CommitChanges()
                        entry.RefreshCache()
                    End If
                End If

                If IsDBNull(entry.Properties("title").Value) = False AndAlso entry.Properties("title").Value <> Nothing Then

                If entry.Properties("title").Value.ToString() <> mstrJobName Then

ADJobAssignment:

                        If mstrJobName.Trim.Length > 0 Then
                    SetADProperty(entry, "title", mstrJobName, mstrADProperty)  'Job
                    entry.CommitChanges()
                    entry.RefreshCache()
                        End If

                    'Pinkal (07-Dec-2019) -- Start
                    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

                    '/ START TO MOVE USER FROM EXISTING OU TO ANOTHER OU
                    Dim objADAttribute As New clsADAttribute_mapping

                    'Pinkal (09-Mar-2020) -- Start
                    'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                    'Dim dtAttributes As DataTable = objADAttribute.GetList("List", True, objDataOperation)
                    Dim dtAttributes As DataTable = objADAttribute.GetList("List", xCompanyId, True, objDataOperation)
                    'Pinkal (09-Mar-2020) -- End


                    If dtAttributes IsNot Nothing AndAlso dtAttributes.Rows.Count > 0 Then
                        Dim dtOUTable As IList(Of String) = dtAttributes.AsEnumerable().Where(Function(x) x.Field(Of String)("oupath") <> "" And x.Field(Of Integer)("mappingunkid") = enAllocation.JOBS And x.Field(Of String)("attributename") = "OU=" & mstrJobName).Select(Function(x) x.Field(Of String)("oupath")).ToList()

                        If dtOUTable IsNot Nothing AndAlso dtOUTable.Count > 0 Then
                            Dim mstrOUPath As String = dtOUTable(0).ToString()

                            If DirectoryEntry.Exists(mstrOUPath) Then
                                Dim newEntry As New DirectoryEntry(mstrOUPath, mstrADUserName, mstrADUserPwd)
                                entry.MoveTo(newEntry)
                                newEntry.Close()
                            End If   'If DirectoryEntry.Exists(mstrOUPath) Then

                        End If  'If dtOUTable IsNot Nothing AndAlso dtOUTable.Count > 0 Then

                    End If  'If dtAttributes IsNot Nothing AndAlso dtAttributes.Rows.Count > 0 Then

                    objADAttribute = Nothing

                    '/ END TO MOVE USER FROM EXISTING OU TO ANOTHER OU

                End If 'if entry.Properties("department").Value.ToString() <> mstrDepartmentName Then

                Else
                    GoTo ADJobAssignment
                End If

                'Pinkal (05-Sep-2020) -- End

                entry.Close()
                entry = Nothing

            End If   'If IsADUserExist(mstrDisplayName, mstrADIPAddress, mstrADDomain, mstrADUserName, mstrADUserPwd, result) Then

        Catch ex As Exception

            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If ex.Message.ToString().Contains("System cannot connect with Active Directory at the moment. Please check Active Directory connection.  Also username or password (Aruti to AD Server) may be incorrect.") Then
                mstrMessage = ex.Message
                Throw New Exception(ex.Message)
            Else
            Throw New Exception(mstrModuleName & "UpdateEmpJobInAD:- " & ex.Message & " [" & mstrADProperty & "]")
            End If
            'Pinkal (04-Apr-2020) -- End
        End Try
    End Sub

    'Pinkal (04-Apr-2020) -- End

    'Pinkal (18-Aug-2018) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Re-Categorize information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 2, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date.")
			Language.setMessage(mstrModuleName, 3, "WEB")
			Language.setMessage(mstrModuleName, 4, "Notification of Changes in Employee Master file")
			Language.setMessage(mstrModuleName, 5, "Effective Date")
			Language.setMessage(mstrModuleName, 6, "Job")
			Language.setMessage(mstrModuleName, 7, "Old Job")
			Language.setMessage(mstrModuleName, 8, "New Job")
			Language.setMessage(mstrModuleName, 9, "Select")
			Language.setMessage("frmEmployeeMaster", 142, "This is to inform you that changes have been made for employee")
			Language.setMessage("frmEmployeeMaster", 143, "with employeecode")
			Language.setMessage("frmEmployeeMaster", 144, "Following information has been changed by user")
			Language.setMessage("frmEmployeeMaster", 145, "from Machine")
			Language.setMessage("frmEmployeeMaster", 146, "and IPAddress")
			Language.setMessage("frmEmployeeMaster", 147, "Dear")
			Language.setMessage("clsMasterData", 421, "Jobs")
			Language.setMessage("clsMasterData", 422, "Job Group")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class