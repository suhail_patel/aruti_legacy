﻿'************************************************************************************************************************************
'Class Name : clsemployee_rehire_tran.vb
'Purpose    :
'Date       :14-Apr-2015
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsemployee_rehire_tran
    Private Shared ReadOnly mstrModuleName As String = "clsemployee_rehire_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintRehiretranunkid As Integer = 0
    Private mdtEffectivedate As Date = Nothing
    Private mintEmployeeunkid As Integer = 0
    Private mdtReinstatment_Date As Date = Nothing
    Private mintChangereasonunkid As Integer = 0
    Private mblnIsfromemployee As Boolean = False
    Private mintUserunkid As Integer = 0
    Private mintStatusunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty

    'Pinkal (07-Mar-2020) -- Start
    'Enhancement - Changes Related to Payroll UAT for NMB.
    Private mdtActualDate As Date = Nothing
    'Pinkal (07-Mar-2020) -- End


    '******************************** EMPLOYEE TRANSFERS VARIABLES *********> START
    Private mintStationunkid As Integer = 0
    Private mintDeptgroupunkid As Integer = 0
    Private mintDepartmentunkid As Integer = 0
    Private mintSectiongroupunkid As Integer = 0
    Private mintSectionunkid As Integer = 0
    Private mintUnitgroupunkid As Integer = 0
    Private mintUnitunkid As Integer = 0
    Private mintTeamunkid As Integer = 0
    Private mintClassgroupunkid As Integer = 0
    Private mintClassunkid As Integer = 0
    '******************************** EMPLOYEE TRANSFERS VARIABLES *********> START

    '******************************** EMPLOYEE WORK PERMIT **********> START
    Private mstrWork_Permit_No As String = String.Empty
    Private mintWorkcountryunkid As Integer = 0
    Private mstrIssue_Place As String = String.Empty
    Private mdtIssue_Date As Date = Nothing
    Private mdtExpiry_Date As Date = Nothing
    '******************************** EMPLOYEE WORK PERMIT **********> END

    '******************************** EMPLOYEE RE-CATEGORIZATION **********> START
    Private mintJobGroupunkid As Integer = 0
    Private mintJobunkid As Integer = 0
    Private mintJGradeUnkid As Integer = 0
    Private mintJGradeLevelUnkid As Integer = 0
    '******************************** EMPLOYEE RE-CATEGORIZATION **********> END

    '******************************** EMPLOYEE DATES **********> START
    Private mdtConfirmationDate As Date = Nothing
    Private mdtProbationFromDate As Date = Nothing
    Private mdtProbationToDate As Date = Nothing
    Private mdtEOCDate As Date = Nothing
    Private mdtLeavingDate As Date = Nothing
    Private mblnExcludeFromPayroll As Boolean = False
    Private mdtRetirementDate As Date = Nothing
    Private mdtAppointmentDate As Date = Nothing
    Private mdtSuspensionFromDate As Date = Nothing
    Private mdtSuspensionToDate As Date = Nothing
    '******************************** EMPLOYEE DATES **********> END

    '******************************** EMPLOYEE SALARY *********> START
    Private mintSGradeGrpId As Integer = 0
    Private mintSGradeunkid As Integer = 0
    Private mintSGradelevelunkid As Integer = 0
    Private mdecEmployeeScale As Decimal = 0
    Private mintPeriodUnkid As Integer = 0
    Private mintInfoSalHead As Integer = 0
    Private mblnIsCopyPrevoiusSLAB As Boolean = False
    Private mblnIsOverwritePrevoiusSLAB As Boolean = False
    Private mintInfoSalHeadUnkid As Integer = -1
    'Sohail (18 Feb 2019) -- Start
    'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
    Private mblnAssignDefaultTransactionHeads As Boolean = False
    'Sohail (18 Feb 2019) -- End
    '******************************** EMPLOYEE SALARY *********> END

    '******************************** EMPLOYEE COSTCENTER & TRANSACTION HEAD **********> START
    Private mintCostCenterUnkid As Integer = 0
    Private mintTransactionHeadUnkid As Integer = 0
    '******************************** EMPLOYEE COSTCENTER & TRANSACTION HEAD **********> END

    'S.SANDEEP [04-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 120
    '******************************** EMPLOYEE RESIDENT PERMIT **********> START
    Private mstrResident_Permit_No As String = String.Empty
    Private mintResidentcountryunkid As Integer = 0
    Private mstrResidentIssue_Place As String = String.Empty
    Private mdtResidentIssue_Date As Date = Nothing
    Private mdtResidentExpiry_Date As Date = Nothing
    '******************************** EMPLOYEE RESIDENT PERMIT **********> END
    'S.SANDEEP [04-Jan-2018] -- END

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set rehiretranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Rehiretranunkid() As Integer
        Get
            Return mintRehiretranunkid
        End Get
        Set(ByVal value As Integer)
            mintRehiretranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reinstatment_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Reinstatment_Date() As Date
        Get
            Return mdtReinstatment_Date
        End Get
        Set(ByVal value As Date)
            mdtReinstatment_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set changereasonunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Changereasonunkid() As Integer
        Get
            Return mintChangereasonunkid
        End Get
        Set(ByVal value As Integer)
            mintChangereasonunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfromemployee
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isfromemployee() As Boolean
        Get
            Return mblnIsfromemployee
        End Get
        Set(ByVal value As Boolean)
            mblnIsfromemployee = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webformname
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webclientip
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webhostname
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property


    'Pinkal (07-Mar-2020) -- Start
    'Enhancement - Changes Related to Payroll UAT for NMB.

    ''' <summary>
    ''' Purpose: Get or Set ActualDate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ActualDate() As Date
        Get
            Return mdtActualDate
        End Get
        Set(ByVal value As Date)
            mdtActualDate = value
        End Set
    End Property

    'Pinkal (07-Mar-2020) -- End


    '******************************** EMPLOYEE TRANSFERS VARIABLES *********> START
    ''' <summary>
    ''' Purpose: Set stationunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Stationunkid() As Integer
        Set(ByVal value As Integer)
            mintStationunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set deptgroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Deptgroupunkid() As Integer
        Set(ByVal value As Integer)
            mintDeptgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set departmentunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Departmentunkid() As Integer
        Set(ByVal value As Integer)
            mintDepartmentunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set sectiongroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Sectiongroupunkid() As Integer
        Set(ByVal value As Integer)
            mintSectiongroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set sectionunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Sectionunkid() As Integer
        Set(ByVal value As Integer)
            mintSectionunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set unitgroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Unitgroupunkid() As Integer
        Set(ByVal value As Integer)
            mintUnitgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set unitunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Unitunkid() As Integer
        Set(ByVal value As Integer)
            mintUnitunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set teamunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Teamunkid() As Integer
        Set(ByVal value As Integer)
            mintTeamunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set classgroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Classgroupunkid() As Integer
        Set(ByVal value As Integer)
            mintClassgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set classunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Classunkid() As Integer
        Set(ByVal value As Integer)
            mintClassunkid = value
        End Set
    End Property
    '******************************** EMPLOYEE TRANSFERS VARIABLES *********> END

    '******************************** EMPLOYEE WORK PERMIT **********> START
    ''' <summary>
    ''' Purpose: Set work_permit_no
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Work_Permit_No() As String
        Set(ByVal value As String)
            mstrWork_Permit_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set workcountryunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Workcountryunkid() As Integer
        Set(ByVal value As Integer)
            mintWorkcountryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set issue_place
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Issue_Place() As String
        Set(ByVal value As String)
            mstrIssue_Place = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set issue_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Issue_Date() As Date
        Set(ByVal value As Date)
            mdtIssue_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set expiry_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Expiry_Date() As Date
        Set(ByVal value As Date)
            mdtExpiry_Date = value
        End Set
    End Property
    '******************************** EMPLOYEE WORK PERMIT **********> END

    '******************************** EMPLOYEE RE-CATEGORIZATION **********> START
    ''' <summary>
    ''' Purpose: Set jobgroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _JobGroupunkid() As Integer
        Set(ByVal value As Integer)
            mintJobGroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set jobunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Jobunkid() As Integer
        Set(ByVal value As Integer)
            mintJobunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set jobgradeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _JGradeunkid() As Integer
        Set(ByVal value As Integer)
            mintJGradeUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set jobgradelevelunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _JGradelevelunkid() As Integer
        Set(ByVal value As Integer)
            mintJGradeLevelUnkid = value
        End Set
    End Property
    '******************************** EMPLOYEE RE-CATEGORIZATION **********> END

    '******************************** EMPLOYEE DATES **********> START
    ''' <summary>
    ''' Purpose: Set confirmation Date
    ''' Modified By: Sandeep Sharma
    ''' </summary>
    ''' <remarks></remarks>
    Public WriteOnly Property _ConfirmationDate()
        Set(ByVal value)
            mdtConfirmationDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set probation from Date
    ''' Modified By: Sandeep Sharma
    ''' </summary>
    ''' <remarks></remarks>
    Public WriteOnly Property _ProbationFromDate() As Date
        Set(ByVal value As Date)
            mdtProbationFromDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set probation to Date
    ''' Modified By: Sandeep Sharma
    ''' </summary>
    ''' <remarks></remarks>
    Public WriteOnly Property _ProbationToDate() As Date
        Set(ByVal value As Date)
            mdtProbationToDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set eoc Date
    ''' Modified By: Sandeep Sharma
    ''' </summary>
    ''' <remarks></remarks>
    Public WriteOnly Property _EOCDate() As Date
        Set(ByVal value As Date)
            mdtEOCDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set leaving Date
    ''' Modified By: Sandeep Sharma
    ''' </summary>
    ''' <remarks></remarks>
    Public WriteOnly Property _LeavingDate() As Date
        Set(ByVal value As Date)
            mdtLeavingDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set excludefrompayroll
    ''' Modified By: Sandeep Sharma
    ''' </summary>
    ''' <remarks></remarks>
    Public WriteOnly Property _ExcludeFromPayroll() As Boolean
        Set(ByVal value As Boolean)
            mblnExcludeFromPayroll = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set retirement date
    ''' Modified By: Sandeep Sharma
    ''' </summary>
    ''' <remarks></remarks>
    Public WriteOnly Property _RetirementDate() As Date
        Set(ByVal value As Date)
            mdtRetirementDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set appointment date
    ''' Modified By: Sandeep Sharma
    ''' </summary>
    ''' <remarks></remarks>
    Public WriteOnly Property _AppointmentDate() As Date
        Set(ByVal value As Date)
            mdtAppointmentDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set suspension from Date
    ''' Modified By: Sandeep Sharma
    ''' </summary>
    ''' <remarks></remarks>
    Public WriteOnly Property _SuspensionFromDate() As Date
        Set(ByVal value As Date)
            mdtSuspensionFromDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set suspension to Date
    ''' Modified By: Sandeep Sharma
    ''' </summary>
    ''' <remarks></remarks>
    Public WriteOnly Property _SuspensionToDate() As Date
        Set(ByVal value As Date)
            mdtSuspensionToDate = value
        End Set
    End Property
    '******************************** EMPLOYEE DATES **********> END

    '******************************** EMPLOYEE COSTCENTER & TRANSACTION HEAD **********> START
    ''' <summary>
    ''' Purpose: Set costcenterunkid
    ''' Modified By: Sandeep Sharma
    ''' </summary>
    ''' <remarks></remarks>
    Public WriteOnly Property _CostCenterUnkid() As Integer
        Set(ByVal value As Integer)
            mintCostCenterUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set transactionheadeunkid
    ''' Modified By: Sandeep Sharma
    ''' </summary>
    ''' <remarks></remarks>
    Public WriteOnly Property _TransactionHeadUnkid() As Integer
        Set(ByVal value As Integer)
            mintTransactionHeadUnkid = value
        End Set
    End Property
    '******************************** EMPLOYEE COSTCENTER & TRANSACTION HEAD **********> END

    '******************************** EMPLOYEE SALARY *********> START
    ''' <summary>
    ''' Purpose: Set gradegroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _SGradeGrpId() As Integer
        Set(ByVal value As Integer)
            mintSGradeGrpId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set gradeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _SGradeunkid() As Integer
        Set(ByVal value As Integer)
            mintSGradeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set gradelevelunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _SGradelevelunkid() As Integer
        Set(ByVal value As Integer)
            mintSGradelevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set gradelevelunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _EmployeeScale() As Decimal
        Set(ByVal value As Decimal)
            mdecEmployeeScale = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _PeriodUnkid() As Integer
        Set(ByVal value As Integer)
            mintPeriodUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set copypreviousslab
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _IsCopyPrevoiusSLAB() As Boolean
        Set(ByVal value As Boolean)
            mblnIsCopyPrevoiusSLAB = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set orverritepreviousslab
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _IsOverwritePrevoiusSLAB() As Boolean
        Set(ByVal value As Boolean)
            mblnIsOverwritePrevoiusSLAB = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set infosalhead
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _InfoSalHeadUnkid() As Integer
        Set(ByVal value As Integer)
            mintInfoSalHeadUnkid = value
        End Set
    End Property

    'Sohail (18 Feb 2019) -- Start
    'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
    Public WriteOnly Property _AssignDefaultTransactionHeads() As Boolean
        Set(ByVal value As Boolean)
            mblnAssignDefaultTransactionHeads = value
        End Set
    End Property
    'Sohail (18 Feb 2019) -- End
    '******************************** EMPLOYEE SALARY *********> END

    'S.SANDEEP [04-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 120
    '******************************** EMPLOYEE RESIDENT PERMIT **********> START
    Public WriteOnly Property _Resident_Permit_No() As String
        Set(ByVal value As String)
            mstrResident_Permit_No = value
        End Set
    End Property
    Public WriteOnly Property _Residentcountryunkid() As Integer
        Set(ByVal value As Integer)
            mintResidentcountryunkid = value
        End Set
    End Property
    Public WriteOnly Property _ResidentIssue_Place() As String
        Set(ByVal value As String)
            mstrResidentIssue_Place = value
        End Set
    End Property
    Public WriteOnly Property _ResidentIssue_Date() As Date
        Set(ByVal value As Date)
            mdtResidentIssue_Date = value
        End Set
    End Property
    Public WriteOnly Property _ResidentExpiry_Date() As Date
        Set(ByVal value As Date)
            mdtResidentExpiry_Date = value
        End Set
    End Property
    '******************************** EMPLOYEE RESIDENT PERMIT **********> END
    'S.SANDEEP [04-Jan-2018] -- END

    'Sohail (02 Mar 2020) -- Start
    'Ifakara Issue # : Message "Actual date should not be greater than selected period end date"  on editing first salary change.
    Private mintBaseCountryunkid As Integer = 0
    Public WriteOnly Property _BaseCountryunkid() As Integer
        Set(ByVal value As Integer)
            mintBaseCountryunkid = value
        End Set
    End Property
    'Sohail (02 Mar 2020) -- End
#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
                   "  rehiretranunkid " & _
                   ", effectivedate " & _
                   ", employeeunkid " & _
                   ", reinstatment_date " & _
                   ", changereasonunkid " & _
                   ", isfromemployee " & _
                       ", actualdate " & _
                   ", userunkid " & _
                   ", statusunkid " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", voidreason " & _
                   "FROM hremployee_rehire_tran " & _
                   "WHERE rehiretranunkid = @rehiretranunkid "


            'Pinkal (07-Mar-2020) -- Enhancement - Changes Related to Payroll UAT for NMB.[actualdate]


            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintRehiretranunkid = CInt(dtRow.Item("rehiretranunkid"))
                mdtEffectivedate = dtRow.Item("effectivedate")
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtReinstatment_Date = dtRow.Item("reinstatment_date")
                mintChangereasonunkid = CInt(dtRow.Item("changereasonunkid"))
                mblnIsfromemployee = CBool(dtRow.Item("isfromemployee"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString

                'Pinkal (07-Mar-2020) -- Start
                'Enhancement - Changes Related to Payroll UAT for NMB.
                If IsDBNull(dtRow.Item("actualdate")) = False Then
                    mdtActualDate = dtRow.Item("actualdate")
                Else
                    mdtActualDate = Nothing
                End If
                'Pinkal (07-Mar-2020) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xFinYearStartDate As Date, Optional ByVal xEmployeeUnkid As Integer = 0) As DataSet 'S.SANDEEP [07-Feb-2018] -- START {xFinYearStartDate} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mdtAppDate As String = ""
        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
                   "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                   "FROM hremployee_master " & _
                   "WHERE employeeunkid = '" & xEmployeeUnkid & "' "

            objDataOperation.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtAppDate = objDataOperation.GetParameterValue("@ADate")

            strQ = "SELECT " & _
                   "  hremployee_rehire_tran.rehiretranunkid " & _
                   ", hremployee_rehire_tran.effectivedate " & _
                   ", hremployee_rehire_tran.employeeunkid " & _
                   ", hremployee_rehire_tran.reinstatment_date " & _
                   ", hremployee_rehire_tran.changereasonunkid " & _
                   ", hremployee_rehire_tran.isfromemployee " & _
                   ", hremployee_rehire_tran.userunkid " & _
                   ", hremployee_rehire_tran.statusunkid " & _
                   ", hremployee_rehire_tran.isvoid " & _
                   ", hremployee_rehire_tran.voiduserunkid " & _
                   ", hremployee_rehire_tran.voiddatetime " & _
                   ", hremployee_rehire_tran.voidreason " & _
                   ", hremployee_master.employeecode as ecode " & _
                   ", hremployee_master.firstname+' '+ISNULL(hremployee_master.othername,'')+' '+hremployee_master.surname AS ename " & _
                   ", CONVERT(CHAR(8),hremployee_rehire_tran.effectivedate,112) AS edate " & _
                   ", CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS adate " & _
                   ", CONVERT(CHAR(8),hremployee_rehire_tran.reinstatment_date,112) AS redate " & _
                   ", '' AS EffDate " & _
                   ", '' AS rdate " & _
                   ", ISNULL(cfcommon_master.name,'') AS CReason " & _
                   ", CASE WHEN CONVERT(CHAR(8),hremployee_rehire_tran.reinstatment_date,112) < '" & eZeeDate.convertDate(xFinYearStartDate).ToString() & "' THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS allowopr " & _
                   ", hremployee_rehire_tran.actualdate " & _
                   "FROM hremployee_rehire_tran " & _
                   "  JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_rehire_tran.employeeunkid " & _
                   "  LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_rehire_tran.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                   "WHERE hremployee_rehire_tran.isvoid = 0 "

            'Pinkal (07-Mar-2020) -- Enhancement - Changes Related to Payroll UAT for NMB.[", hremployee_rehire_tran.actualdate " & _]

            'S.SANDEEP [07-Feb-2018] -- START {allowopr} -- END

            If xEmployeeUnkid Then
                strQ &= " AND hremployee_rehire_tran.employeeunkid = '" & xEmployeeUnkid & "' "
            End If

            If mdtAppDate.Trim.Length > 0 Then
                strQ &= " AND CONVERT(CHAR(8),hremployee_rehire_tran.effectivedate,112) >= '" & mdtAppDate & "' "
            End If

            strQ &= " ORDER BY effectivedate DESC "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)
            dsList.Tables(0).AsEnumerable()
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each xRow As DataRow In dsList.Tables(0).Rows
                xRow("EffDate") = eZeeDate.convertDate(xRow.Item("edate").ToString).ToShortDateString
                xRow("rdate") = eZeeDate.convertDate(xRow.Item("redate").ToString).ToShortDateString
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_rehire_tran) </purpose>
    Public Function Insert(ByVal strDatabaseName As String _
                           , ByVal xYearUnkid As Integer _
                           , ByVal xCompanyUnkid As Integer _
                           , ByVal xPeriodStart As DateTime _
                           , ByVal xPeriodEnd As DateTime _
                           , ByVal xUserModeSetting As String _
                           , ByVal xOnlyApproved As Boolean _
                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                           , ByVal xCurrScale As DataSet _
                           , ByVal blnAllowToApproveEarningDeduction As Boolean _
                           , ByVal dtCurrentDateAndTime As Date _
                           , ByVal mblnCreateADUserFromEmp As Boolean _
                           , Optional ByVal xMemTran As DataTable = Nothing _
                           , Optional ByVal xPrivilegeAllowToApproveSalaryChange As String = "" _
                           , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                           , Optional ByVal strFilerString As String = "" _
                           , Optional ByVal dtTotal_Active_Employee_AsOnDate As Date = Nothing _
                           , Optional ByVal blnIsArutiDemo As String = "" _
                           , Optional ByVal intTotal_Active_Employee_ForAllCompany As Integer = 0 _
                           , Optional ByVal intNoOfEmployees As Integer = 0 _
                           , Optional ByVal blnDonotAttendanceinSeconds As Boolean = False _
                           , Optional ByVal blnUserMustChangePwdOnNextLogOn As Boolean = False _
                           ) As Boolean
        'Sohail (18 Feb 2019) - [dtTotal_Active_Employee_AsOnDate, blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, blnDonotAttendanceinSeconds, blnUserMustChangePwdOnNextLogOn]
        'Pinkal (18-Aug-2018) -- 'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[, ByVal mblnCreateADUserFromEmp As Boolean, _]

        'Sohail (21 Aug 2015) - [strDatabaseName, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString]

        Dim objETransfer As New clsemployee_transfer_tran
        Dim objERecategorie As New clsemployee_categorization_Tran
        Dim objEDates As New clsemployee_dates_tran
        Dim objWPermit As New clsemployee_workpermit_tran
        Dim objECCT As New clsemployee_cctranhead_tran
        Dim objSalInc As New clsSalaryIncrement
        Dim objMemTran As New clsMembershipTran
        'Sohail (18 Feb 2019) -- Start
        'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
        Dim objED As New clsEarningDeduction
        Dim objTranHead As New clsTransactionHead
        'Sohail (18 Feb 2019) -- End

        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        Dim mdtCurrentDate As Date = Nothing
        'Pinkal (18-Aug-2018) -- End

        'S.SANDEEP |06-JUL-2020| -- START
        'ISSUE/ENHANCEMENT : NMB ENHANCEMENT (REHIRE ON CLOSE PERIOD) 
        Dim blnAllowRehireClosedPeriod As Boolean = False
        Dim intStatusId As Integer = 1
        Dim objConfig As New clsConfigOptions
        Dim strkeyvalue As String = ""
        strkeyvalue = objConfig.GetKeyValue(xCompanyUnkid, "ALLOWREHIREONCLOSEDPERIOD", Nothing)
        If strkeyvalue.Trim.Length > 0 Then
            blnAllowRehireClosedPeriod = CBool(strkeyvalue)
        End If
        objConfig = Nothing
        Dim objPrd As New clscommom_period_Tran
        objPrd._Periodunkid(strDatabaseName) = mintPeriodUnkid
        intStatusId = objPrd._Statusid
        objPrd = Nothing
        'S.SANDEEP |06-JUL-2020| -- END

        objDataOperation = New clsDataOperation

        If isExist(mdtEffectivedate, Nothing, mintEmployeeunkid, , objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, transfer information is already present for the selected effective date.")
            Return False
        End If

        If isExist(Nothing, mdtReinstatment_Date, mintEmployeeunkid, , objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, transfer information is already present for the selected reinstatement date.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        If mblnCreateADUserFromEmp Then
            strQ = "SELECT @Date = GETDATE()"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCurrentDate, ParameterDirection.Output)
            objDataOperation.ExecNonQuery(strQ)
            mdtCurrentDate = CDate(objDataOperation.GetParameterValue("@Date"))
        End If
        'Pinkal (18-Aug-2018) -- End


        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@reinstatment_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReinstatment_Date)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)


            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            If mdtActualDate <> Nothing Then
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualDate)
            Else
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Pinkal (07-Mar-2020) -- End


            strQ = "INSERT INTO hremployee_rehire_tran ( " & _
                       "  effectivedate " & _
                       ", employeeunkid " & _
                       ", reinstatment_date " & _
                       ", changereasonunkid " & _
                       ", isfromemployee " & _
                       ", actualdate" & _
                       ", userunkid " & _
                       ", statusunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason" & _
                   ") VALUES (" & _
                       "  @effectivedate " & _
                       ", @employeeunkid " & _
                       ", @reinstatment_date " & _
                       ", @changereasonunkid " & _
                       ", @isfromemployee " & _
                       ", @actualdate" & _
                       ", @userunkid " & _
                       ", @statusunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason" & _
                   "); SELECT @@identity"


            'Pinkal (07-Mar-2020) -- Enhancement - Changes Related to Payroll UAT for NMB.[@actualdate]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintRehiretranunkid = dsList.Tables(0).Rows(0).Item(0)

            mstrMessage = ""



            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmp Then
                If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
                    If SetEnableDisableUserInAD(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (18-Aug-2018) -- End



            'S.SANDEEP [10-MAY-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

            'S.SANDEEP [19-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : STOP SYMMETRY REAL TIME (RUTTA'S REQUEST)
            'If Integrate_Symmetry(objDataOperation, xCompanyUnkid, mintEmployeeunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [19-JUL-2017] -- END

            'S.SANDEEP [10-MAY-2017] -- END

            '========================== EMPLOYEE TRANSFERS ================================================== > START
            objETransfer._Changereasonunkid = mintChangereasonunkid
            objETransfer._Classgroupunkid = mintClassgroupunkid
            objETransfer._Classunkid = mintClassunkid
            objETransfer._Departmentunkid = mintDepartmentunkid
            objETransfer._Deptgroupunkid = mintDeptgroupunkid
            objETransfer._Effectivedate = mdtEffectivedate
            objETransfer._Employeeunkid = mintEmployeeunkid
            objETransfer._IsFromEmployee = False
            objETransfer._Isvoid = mblnIsvoid
            objETransfer._Rehiretranunkid = mintRehiretranunkid
            objETransfer._Sectiongroupunkid = mintSectiongroupunkid
            objETransfer._Sectionunkid = mintSectionunkid
            objETransfer._Stationunkid = mintStationunkid
            objETransfer._Statusunkid = mintStatusunkid
            objETransfer._Teamunkid = mintTeamunkid
            objETransfer._Unitgroupunkid = mintUnitgroupunkid
            objETransfer._Unitunkid = mintUnitunkid
            objETransfer._Userunkid = mintUserunkid
            objETransfer._Voiddatetime = mdtVoiddatetime
            objETransfer._Voidreason = mstrVoidreason
            objETransfer._Voiduserunkid = mintVoiduserunkid

            'S.SANDEEP [10-MAY-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

            
            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
            'If objETransfer.Insert(mblnCreateADUserFromEmp, xCompanyUnkid, objDataOperation) = False Then
            If objETransfer.Insert(strDatabaseName, mblnCreateADUserFromEmp, xCompanyUnkid, objDataOperation) = False Then
                'Pinkal (12-Oct-2020) -- End
                'If objETransfer.Insert(objDataOperation) = False Then
                'S.SANDEEP [10-MAY-2017] -- END
                If objETransfer._Message <> "" Then
                    mstrMessage = objETransfer._Message
                Else
                    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                End If
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objETransfer = Nothing
            '========================== EMPLOYEE TRANSFERS ================================================== > END

            '========================== EMPLOYEE RECATEGORIZE ================================================== > START
            objERecategorie._Changereasonunkid = mintChangereasonunkid
            objERecategorie._Effectivedate = mdtEffectivedate
            objERecategorie._Employeeunkid = mintEmployeeunkid
            objERecategorie._Gradelevelunkid = 0
            objERecategorie._Gradeunkid = 0
            objERecategorie._Isfromemployee = False
            objERecategorie._Isvoid = mblnIsvoid
            objERecategorie._JobGroupunkid = mintJobGroupunkid
            objERecategorie._Jobunkid = mintJobunkid
            objERecategorie._Rehiretranunkid = mintRehiretranunkid
            objERecategorie._Statusunkid = mintStatusunkid
            objERecategorie._Userunkid = mintUserunkid
            objERecategorie._Voiddatetime = mdtVoiddatetime
            objERecategorie._Voidreason = mstrVoidreason
            objERecategorie._Voiduserunkid = mintVoiduserunkid

            'S.SANDEEP [10-MAY-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
            '  If objERecategorie.Insert(mblnCreateADUserFromEmp, xCompanyUnkid, objDataOperation) = False Then
            If objERecategorie.Insert(mblnCreateADUserFromEmp, xCompanyUnkid, strDatabaseName, objDataOperation) = False Then
                'Pinkal (12-Oct-2020) -- End

                'Pinkal (18-Aug-2018) -- End
                'If objERecategorie.Insert(objDataOperation) = False Then
                'S.SANDEEP [10-MAY-2017] -- END
                If objERecategorie._Message <> "" Then
                    mstrMessage = objERecategorie._Message
                Else
                    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                End If
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objERecategorie = Nothing
            '========================== EMPLOYEE RECATEGORIZE ================================================== > END

            '========================== EMPLOYEE DATES ================================================== > START

            '--------------> Confirmation Date <----------------- START

            If objEDates.isExist(Nothing, mdtConfirmationDate, Nothing, enEmp_Dates_Transaction.DT_CONFIRMATION, mintEmployeeunkid, , objDataOperation) = False Then 'S.SANDEEP [20 JUL 2016] -- START -- END
            objEDates._Changereasonunkid = mintChangereasonunkid
            objEDates._Date1 = mdtConfirmationDate
            objEDates._Date2 = Nothing
            objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_CONFIRMATION
            objEDates._Effectivedate = mdtEffectivedate
            objEDates._Employeeunkid = mintEmployeeunkid
            objEDates._Isconfirmed = True
            objEDates._Isfromemployee = False
            objEDates._Isvoid = mblnIsvoid
            objEDates._Rehiretranunkid = mintRehiretranunkid
            objEDates._Statusunkid = mintStatusunkid
            objEDates._Userunkid = mintUserunkid
            objEDates._Voiddatetime = mdtVoiddatetime
            objEDates._Voidreason = mstrVoidreason
            objEDates._Voiduserunkid = mintVoiduserunkid

                'S.SANDEEP [10-MAY-2017] -- START
                'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'If objEDates.Insert(xCompanyUnkid, objDataOperation) = False Then
                If objEDates.Insert(mblnCreateADUserFromEmp, xCompanyUnkid, objDataOperation) = False Then
                    'Pinkal (18-Aug-2018) -- End
                    'If objEDates.Insert(objDataOperation) = False Then
                    'S.SANDEEP [10-MAY-2017] -- END
                If objEDates._Message <> "" Then
                    mstrMessage = objEDates._Message
                Else
                    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                End If
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            End If 'S.SANDEEP [20 JUL 2016] -- START -- END
            '--------------> Confirmation Date <----------------- END


            '--------------> Probation Date <----------------- START
            If mdtProbationFromDate <> Nothing AndAlso mdtProbationToDate <> Nothing Then
                If objEDates.isExist(Nothing, mdtProbationFromDate, mdtProbationToDate, enEmp_Dates_Transaction.DT_PROBATION, mintEmployeeunkid, , objDataOperation) = False Then 'S.SANDEEP [20 JUL 2016] -- START -- END
                objEDates._Changereasonunkid = mintChangereasonunkid
                objEDates._Date1 = mdtProbationFromDate
                objEDates._Date2 = mdtProbationToDate
                objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_PROBATION
                objEDates._Effectivedate = mdtEffectivedate
                objEDates._Employeeunkid = mintEmployeeunkid
                objEDates._Isvoid = mblnIsvoid
                objEDates._Rehiretranunkid = mintRehiretranunkid
                objEDates._Statusunkid = mintStatusunkid
                objEDates._Userunkid = mintUserunkid
                objEDates._Voiddatetime = mdtVoiddatetime
                objEDates._Voidreason = mstrVoidreason
                objEDates._Voiduserunkid = mintVoiduserunkid

                    'S.SANDEEP [10-MAY-2017] -- START
                    'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    'If objEDates.Insert(xCompanyUnkid, objDataOperation) = False Then
                    If objEDates.Insert(mblnCreateADUserFromEmp, xCompanyUnkid, objDataOperation) = False Then
                        'Pinkal (18-Aug-2018) -- End
                        'If objEDates.Insert(objDataOperation) = False Then
                        'S.SANDEEP [10-MAY-2017] -- END
                    If objEDates._Message <> "" Then
                        mstrMessage = objEDates._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
                End If 'S.SANDEEP [20 JUL 2016] -- START -- END
            End If
            '--------------> Probation Date <----------------- END


            '--------------> Termination Date <----------------- START
            If mdtEOCDate <> Nothing AndAlso mdtLeavingDate <> Nothing Then
                If objEDates.isExist(Nothing, mdtEOCDate, mdtLeavingDate, enEmp_Dates_Transaction.DT_TERMINATION, mintEmployeeunkid, , objDataOperation) = False Then 'S.SANDEEP [20 JUL 2016] -- START -- END
                objEDates._Actionreasonunkid = mintChangereasonunkid
                objEDates._Changereasonunkid = mintChangereasonunkid
                objEDates._Date1 = mdtEOCDate
                objEDates._Date2 = mdtLeavingDate
                objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_TERMINATION
                objEDates._Effectivedate = mdtEffectivedate
                objEDates._Employeeunkid = mintEmployeeunkid
                objEDates._Isvoid = mblnIsvoid
                objEDates._Isexclude_payroll = mblnExcludeFromPayroll
                objEDates._Rehiretranunkid = mintRehiretranunkid
                objEDates._Statusunkid = mintStatusunkid
                objEDates._Userunkid = mintUserunkid
                objEDates._Voiddatetime = mdtVoiddatetime
                objEDates._Voidreason = mstrVoidreason
                objEDates._Voiduserunkid = mintVoiduserunkid

                    'Pinkal (07-Mar-2020) -- Start
                    'Enhancement - Changes Related to Payroll UAT for NMB.
                    objEDates._ActualDate = mdtActualDate
                    'Pinkal (07-Mar-2020) -- End


                    'S.SANDEEP [10-MAY-2017] -- START
                    'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    'If objEDates.Insert(xCompanyUnkid, objDataOperation) = False Then
                    If objEDates.Insert(mblnCreateADUserFromEmp, xCompanyUnkid, objDataOperation) = False Then
                        'Pinkal (18-Aug-2018) -- End

                        'If objEDates.Insert(objDataOperation) = False Then
                        'S.SANDEEP [10-MAY-2017] -- END
                    If objEDates._Message <> "" Then
                        mstrMessage = objEDates._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
                End If 'S.SANDEEP [20 JUL 2016] -- START -- END
                'S.SANDEEP [29 APR 2015] -- START
            Else
                objEDates._Actionreasonunkid = mintChangereasonunkid
                objEDates._Changereasonunkid = mintChangereasonunkid
                objEDates._Date1 = Nothing
                objEDates._Date2 = Nothing
                objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_TERMINATION
                objEDates._Effectivedate = mdtEffectivedate
                objEDates._Employeeunkid = mintEmployeeunkid
                objEDates._Isvoid = mblnIsvoid
                objEDates._Isexclude_payroll = mblnExcludeFromPayroll
                objEDates._Rehiretranunkid = mintRehiretranunkid
                objEDates._Statusunkid = mintStatusunkid
                objEDates._Userunkid = mintUserunkid
                objEDates._Voiddatetime = mdtVoiddatetime
                objEDates._Voidreason = mstrVoidreason
                objEDates._Voiduserunkid = mintVoiduserunkid
                objEDates._InsertNullTerminationDate = True

                'S.SANDEEP [10-MAY-2017] -- START
                'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'If objEDates.Insert(xCompanyUnkid, objDataOperation) = False Then
                If objEDates.Insert(mblnCreateADUserFromEmp, xCompanyUnkid, objDataOperation) = False Then
                    'Pinkal (18-Aug-2018) -- End
                    'If objEDates.Insert(objDataOperation) = False Then
                    'S.SANDEEP [10-MAY-2017] -- END
                    If objEDates._Message <> "" Then
                        mstrMessage = objEDates._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
                'S.SANDEEP [29 APR 2015] -- END
            End If
            '--------------> Termination Date <----------------- END


            '--------------> Retirement Date <----------------- START
            If objEDates.isExist(Nothing, mdtRetirementDate, Nothing, enEmp_Dates_Transaction.DT_RETIREMENT, mintEmployeeunkid, , objDataOperation) = False Then 'S.SANDEEP [20 JUL 2016] -- START -- END
            objEDates._Changereasonunkid = mintChangereasonunkid
            objEDates._Date1 = mdtRetirementDate
            objEDates._Date2 = Nothing
            objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_RETIREMENT
            objEDates._Effectivedate = mdtEffectivedate
            objEDates._Employeeunkid = mintEmployeeunkid
            objEDates._Isvoid = mblnIsvoid
            objEDates._Rehiretranunkid = mintRehiretranunkid
            objEDates._Statusunkid = mintStatusunkid
            objEDates._Userunkid = mintUserunkid
            objEDates._Voiddatetime = mdtVoiddatetime
            objEDates._Voidreason = mstrVoidreason
            objEDates._Voiduserunkid = mintVoiduserunkid

                'S.SANDEEP [10-MAY-2017] -- START
                'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'If objEDates.Insert(xCompanyUnkid, objDataOperation) = False Then
                If objEDates.Insert(mblnCreateADUserFromEmp, xCompanyUnkid, objDataOperation) = False Then
                    'Pinkal (18-Aug-2018) -- End

                    'If objEDates.Insert(objDataOperation) = False Then
                    'S.SANDEEP [10-MAY-2017] -- END
                If objEDates._Message <> "" Then
                    mstrMessage = objEDates._Message
                Else
                    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                End If
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            End If 'S.SANDEEP [20 JUL 2016] -- START -- END
            
            '--------------> Retirement Date <----------------- END

            objEDates = Nothing
            '========================== EMPLOYEE DATES ================================================== > END


            '========================== EMPLOYEE WORK PERMIT ================================================== > START
            'S.SANDEEP [23-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002339|ARUTI-214}
            'objWPermit._Changereasonunkid = mintChangereasonunkid
            'objWPermit._Effectivedate = mdtEffectivedate
            'objWPermit._Employeeunkid = mintEmployeeunkid
            'objWPermit._Expiry_Date = mdtExpiry_Date
            'objWPermit._Issue_Date = mdtIssue_Date
            'objWPermit._Isvoid = mblnIsvoid
            'objWPermit._Statusunkid = mintStatusunkid
            'objWPermit._Userunkid = mintUserunkid
            'objWPermit._Voiddatetime = mdtVoiddatetime
            'objWPermit._Voidreason = mstrVoidreason
            'objWPermit._Voiduserunkid = mintVoiduserunkid
            'objWPermit._Work_Permit_No = mstrWork_Permit_No
            'objWPermit._Workcountryunkid = mintWorkcountryunkid
            'objWPermit._Rehiretranunkid = mintRehiretranunkid
            ''S.SANDEEP [04-Jan-2018] -- START
            ''ISSUE/ENHANCEMENT : REF-ID # 120
            'objWPermit._Issue_Place = mstrIssue_Place
            'objWPermit._IsResidentPermit = False
            ''S.SANDEEP [04-Jan-2018] -- END

            'If objWPermit.Insert(objDataOperation) = False Then
            '    If objWPermit._Message <> "" Then
            '        mstrMessage = objWPermit._Message
            '    Else
            '        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
            '    End If
            '    objDataOperation.ReleaseTransaction(False)
            '    Return False
            'End If
            If mstrWork_Permit_No.Trim.Length > 0 Then
                Dim iTranId As Integer = 0
                iTranId = objWPermit.GetPermitTranId(mstrWork_Permit_No, mintEmployeeunkid, False, mdtEffectivedate, objDataOperation)
                If iTranId > 0 Then
                    objWPermit._Workpermittranunkid(objDataOperation) = iTranId
                End If
            objWPermit._Changereasonunkid = mintChangereasonunkid
            objWPermit._Effectivedate = mdtEffectivedate
            objWPermit._Employeeunkid = mintEmployeeunkid
            objWPermit._Expiry_Date = mdtExpiry_Date
            objWPermit._Issue_Date = mdtIssue_Date
            objWPermit._Isvoid = mblnIsvoid
            objWPermit._Statusunkid = mintStatusunkid
            objWPermit._Userunkid = mintUserunkid
            objWPermit._Voiddatetime = mdtVoiddatetime
            objWPermit._Voidreason = mstrVoidreason
            objWPermit._Voiduserunkid = mintVoiduserunkid
            objWPermit._Work_Permit_No = mstrWork_Permit_No
            objWPermit._Workcountryunkid = mintWorkcountryunkid
            objWPermit._Rehiretranunkid = mintRehiretranunkid
            objWPermit._Issue_Place = mstrIssue_Place
            objWPermit._IsResidentPermit = False

                If iTranId > 0 Then
                    If objWPermit.Update(objDataOperation) = False Then
                        If objWPermit._Message <> "" Then
                            mstrMessage = objWPermit._Message
                        Else
                            mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                        End If
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                Else
            If objWPermit.Insert(objDataOperation) = False Then
                If objWPermit._Message <> "" Then
                    mstrMessage = objWPermit._Message
                Else
                    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                End If
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
                End If
            End If
            'S.SANDEEP [23-JUN-2018] -- END
            

            'objWPermit = Nothing 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 -- END
            '========================== EMPLOYEE WORK PERMIT ================================================== > END

            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            '========================== EMPLOYEE RESIDENT PERMIT ================================================== > START
            If mstrResident_Permit_No.Trim.Length > 0 Then
                'S.SANDEEP [23-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {#0002339|ARUTI-214}
                Dim iTranId As Integer = 0
                iTranId = objWPermit.GetPermitTranId(mstrResident_Permit_No, mintEmployeeunkid, True, mdtEffectivedate, objDataOperation)
                If iTranId > 0 Then
                    objWPermit._Workpermittranunkid(objDataOperation) = iTranId
                End If
                'S.SANDEEP [23-JUN-2018] -- END
                objWPermit._Changereasonunkid = mintChangereasonunkid
                objWPermit._Effectivedate = mdtEffectivedate
                objWPermit._Employeeunkid = mintEmployeeunkid
                objWPermit._Expiry_Date = mdtResidentExpiry_Date
                objWPermit._Issue_Date = mdtResidentExpiry_Date
                objWPermit._Issue_Place = mstrResidentIssue_Place
                objWPermit._Isvoid = mblnIsvoid
                objWPermit._Statusunkid = mintStatusunkid
                objWPermit._Userunkid = mintUserunkid
                objWPermit._Voiddatetime = mdtVoiddatetime
                objWPermit._Voidreason = mstrVoidreason
                objWPermit._Voiduserunkid = mintVoiduserunkid
                objWPermit._Work_Permit_No = mstrResident_Permit_No
                objWPermit._Workcountryunkid = mintResidentcountryunkid
                objWPermit._Rehiretranunkid = mintRehiretranunkid
                objWPermit._IsResidentPermit = True

                'S.SANDEEP [23-JUN-2018] -- START
                'ISSUE/ENHANCEMENT : {#0002339|ARUTI-214}
                'If objWPermit.Insert(objDataOperation) = False Then
                '    If objWPermit._Message <> "" Then
                '        mstrMessage = objWPermit._Message
                '    Else
                '        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                '    End If
                '    objDataOperation.ReleaseTransaction(False)
                '    Return False
                'End If

                If iTranId > 0 Then
                    If objWPermit.Update(objDataOperation) = False Then
                        If objWPermit._Message <> "" Then
                            mstrMessage = objWPermit._Message
                        Else
                            mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                        End If
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                Else
                If objWPermit.Insert(objDataOperation) = False Then
                    If objWPermit._Message <> "" Then
                        mstrMessage = objWPermit._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
                End If
                'S.SANDEEP [23-JUN-2018] -- END
                
                

                objWPermit = Nothing
            End If
            '========================== EMPLOYEE RESIDENT PERMIT ================================================== > END
            'S.SANDEEP [04-Jan-2018] -- END

            '========================== EMPLOYEE COST-CENTER & TRANSACTION HEAD ==================================== > START
            objECCT._Cctranheadvalueid = mintCostCenterUnkid
            objECCT._Changereasonunkid = mintChangereasonunkid
            objECCT._Effectivedate = mdtEffectivedate
            objECCT._Employeeunkid = mintEmployeeunkid
            objECCT._Istransactionhead = False
            objECCT._Isvoid = mblnIsvoid
            objECCT._Rehiretranunkid = mintRehiretranunkid
            objECCT._Statusunkid = mintStatusunkid
            objECCT._Userunkid = mintUserunkid
            objECCT._Voiddatetime = mdtVoiddatetime
            objECCT._Voidreason = mstrVoidreason
            objECCT._Voiduserunkid = mintVoiduserunkid

            If objECCT.Insert(objDataOperation) = False Then
                If objECCT._Message <> "" Then
                    mstrMessage = objECCT._Message
                Else
                    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                End If
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objECCT._Cctranheadvalueid = mintTransactionHeadUnkid
            objECCT._Changereasonunkid = mintChangereasonunkid
            objECCT._Effectivedate = mdtEffectivedate
            objECCT._Employeeunkid = mintEmployeeunkid
            objECCT._Istransactionhead = True
            objECCT._Isvoid = mblnIsvoid
            objECCT._Rehiretranunkid = mintRehiretranunkid
            objECCT._Statusunkid = mintStatusunkid
            objECCT._Userunkid = mintUserunkid
            objECCT._Voiddatetime = mdtVoiddatetime
            objECCT._Voidreason = mstrVoidreason
            objECCT._Voiduserunkid = mintVoiduserunkid

            If objECCT.Insert(objDataOperation) = False Then
                If objECCT._Message <> "" Then
                    mstrMessage = objECCT._Message
                Else
                    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                End If
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objECCT = Nothing
            '========================== EMPLOYEE COST-CENTER & TRANSACTION HEAD ==================================== > END

            'Sohail (18 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.

            If blnAllowRehireClosedPeriod = False Then 'S.SANDEEP |06-JUL-2020| -- START -- NMB ENHANCEMENT (REHIRE ON CLOSE PERIOD) -- END
            Dim objEmp As New clsEmployee_Master
            objEmp._DataOperation = objDataOperation
            objEmp._Employeeunkid(mdtReinstatment_Date) = mintEmployeeunkid
            If mintTransactionHeadUnkid > 0 Then
                If objEmp._Tranhedunkid <> mintTransactionHeadUnkid Then
                    objEmp._Tranhedunkid = mintTransactionHeadUnkid
                    If objEmp.Update(strDatabaseName, xYearUnkid, xCompanyUnkid, dtTotal_Active_Employee_AsOnDate, blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, mintUserunkid, blnDonotAttendanceinSeconds, xUserModeSetting, xPeriodStart, xPeriodEnd, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, mblnCreateADUserFromEmp, blnUserMustChangePwdOnNextLogOn, , , , , , , , , objDataOperation) = False Then
                        If objEmp._Message <> "" Then
                            mstrMessage = objEmp._Message
                        Else
                            mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                        End If
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            End If

            objTranHead._xDataOperation = objDataOperation
            objTranHead._Tranheadunkid(strDatabaseName) = objEmp._Tranhedunkid
            Dim dsSalary As DataSet = objED.GetSalaryHeadPeriodList("List", strDatabaseName, mintEmployeeunkid, enModuleReference.Payroll, True, objDataOperation, mintPeriodUnkid)
            If dsSalary.Tables(0).Rows.Count > 0 Then

                objED = New clsEarningDeduction

                objED._Edunkid(objDataOperation, strDatabaseName) = CInt(dsSalary.Tables(0).Rows(0).Item("edunkid"))
                objED._Periodunkid = mintPeriodUnkid
                objED._Tranheadunkid = objEmp._Tranhedunkid
                    objED._DataOperation = objDataOperation 'Sohail (15 Jan 2022)
                objED._Employeeunkid(strDatabaseName) = mintEmployeeunkid
                objED._Trnheadtype_Id = objTranHead._Trnheadtype_Id
                objED._Typeof_Id = objTranHead._Typeof_id
                objED._Calctype_Id = objTranHead._Calctype_Id
                objED._Formula = objTranHead._Formula
                objED._FormulaId = objTranHead._Formulaid
                objED._Userunkid = mintUserunkid
                'Sohail (03 Sep 2019) -- Start
                '0004125: NIBS : Assist to delete pending Salary Change from back end
                'objED._Isapproved = blnAllowToApproveEarningDeduction
                objED._Isapproved = True
                'Sohail (03 Sep 2019) -- End
                objED._Approveruserunkid = mintUserunkid
                If objTranHead._Calctype_Id = enCalcType.FlatRate_Others Then
                    objED._Amount = mdecEmployeeScale
                End If

                If objED.Update(strDatabaseName, False, objDataOperation, dtCurrentDateAndTime) = False Then
                    If objED._Message.Trim.Length > 0 Then
                        exForce = New Exception(objED._Message & " : [ED Update]")
                    Else
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage & " : [ED Insert]")
                    End If
                    Throw exForce
                End If
            Else
                objED = New clsEarningDeduction

                objED._Periodunkid = mintPeriodUnkid
                objED._Tranheadunkid = objEmp._Tranhedunkid
                    objED._DataOperation = objDataOperation 'Sohail (15 Jan 2022)
                objED._Employeeunkid(strDatabaseName) = mintEmployeeunkid
                objED._Trnheadtype_Id = objTranHead._Trnheadtype_Id
                objED._Typeof_Id = objTranHead._Typeof_id
                objED._Calctype_Id = objTranHead._Calctype_Id
                objED._Formula = objTranHead._Formula
                objED._FormulaId = objTranHead._Formulaid
                objED._Userunkid = mintUserunkid
                'Sohail (03 Sep 2019) -- Start
                '0004125: NIBS : Assist to delete pending Salary Change from back end
                'objED._Isapproved = blnAllowToApproveEarningDeduction
                objED._Isapproved = True
                'Sohail (03 Sep 2019) -- End
                objED._Approveruserunkid = mintUserunkid
                If objTranHead._Calctype_Id = enCalcType.FlatRate_Others Then
                    objED._Amount = mdecEmployeeScale
                End If

                If objED.Insert(strDatabaseName, mintUserunkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, False, blnAllowToApproveEarningDeduction) = False Then
                    If objED._Message.Trim.Length > 0 Then
                        exForce = New Exception(objED._Message & " : [ED Insert]")
                    Else
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage & " : [ED Insert]")
                    End If
                    Throw exForce
                End If
            End If
            End If
            'Sohail (18 Feb 2019) -- End

            '========================== EMPLOYEE SALARY CHANGES ================================================== > START
            If xCurrScale.Tables(0).Rows.Count > 0 Then
                If mintSGradeGrpId <> xCurrScale.Tables(0).Rows(0).Item("gradegroupunkid") Or _
                   mintSGradeunkid <> xCurrScale.Tables(0).Rows(0).Item("gradeunkid") Or _
                   mintSGradelevelunkid <> xCurrScale.Tables(0).Rows(0).Item("gradelevelunkid") Then
                    objSalInc._Isgradechange = True
                    objSalInc._Increment = mdecEmployeeScale - xCurrScale.Tables(0).Rows(0).Item("currentscale")
                    objSalInc._Increment_Mode = enSalaryIncrementBy.Grade
                Else
                    objSalInc._Increment_Mode = enSalaryIncrementBy.Amount
                End If
            End If
            objSalInc._Reason_Id = mintChangereasonunkid
            objSalInc._Gradegroupunkid = mintSGradeGrpId
            objSalInc._Gradeunkid = mintSGradeunkid
            objSalInc._Gradelevelunkid = mintSGradelevelunkid
            objSalInc._Rehiretranunkid = mintRehiretranunkid
            objSalInc._Periodunkid = mintPeriodUnkid
            objSalInc._Employeeunkid = mintEmployeeunkid
            'S.SANDEEP [27-Mar-2018] -- START
            'ISSUE/ENHANCEMENT : {Internal_Finding}
            'objSalInc._Incrementdate = mdtEffectivedate
            objSalInc._Incrementdate = mdtReinstatment_Date
            'S.SANDEEP [27-Mar-2018] -- END
            'Sohail (02 Mar 2020) -- Start
            'Ifakara Issue # : Message "Actual date should not be greater than selected period end date"  on editing first salary change.
            objSalInc._ActualDate = mdtReinstatment_Date
            objSalInc._Arrears_Countryid = mintBaseCountryunkid
            'Sohail (02 Mar 2020) -- End
            objSalInc._Currentscale = mdecEmployeeScale
            objSalInc._Newscale = mdecEmployeeScale
            objSalInc._Gradegroupunkid = mintSGradeGrpId
            objSalInc._Gradeunkid = mintSGradeunkid
            objSalInc._Gradelevelunkid = mintSGradelevelunkid
            objSalInc._Isfromemployee = False
            objSalInc._Userunkid = User._Object._Userunkid
            objSalInc._Isvoid = False

            If xPrivilegeAllowToApproveSalaryChange.Trim.Length <= 0 Then
                xPrivilegeAllowToApproveSalaryChange = User._Object.Privilege._AllowToApproveSalaryChange.ToString
            End If

            'Sohail (03 Sep 2019) -- Start
            '0004125: NIBS : Assist to delete pending Salary Change from back end
            'If CBool(xPrivilegeAllowToApproveSalaryChange) = True Then
            '    objSalInc._Isapproved = True
            '    objSalInc._Approveruserunkid = mintUserunkid
            'End If
                objSalInc._Isapproved = True
                objSalInc._Approveruserunkid = mintUserunkid
            'Sohail (03 Sep 2019) -- End
            objSalInc._Userunkid = mintUserunkid
            objSalInc._Voiddatetime = mdtVoiddatetime
            objSalInc._Voidreason = mstrVoidreason
            objSalInc._Voiduserunkid = mintVoiduserunkid

            If mintInfoSalHeadUnkid > 0 Then
                objSalInc._InfoSalHeadUnkid = mintInfoSalHeadUnkid
                objSalInc._IsCopyPrevoiusSLAB = mblnIsCopyPrevoiusSLAB
                objSalInc._IsOverwritePrevoiusSLAB = mblnIsOverwritePrevoiusSLAB
            End If
            objSalInc._DataOperation = objDataOperation
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objSalInc.Insert(False) = False Then
            'Sohail (18 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
            'If objSalInc.Insert(strDatabaseName, mintUserunkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, False, , , blnApplyUserAccessFilter, strFilerString) = False Then
            'Sohail (24 Feb 2022) -- Start
            'Issue :  : TWC - Not able to approve Pending Salary heads on Earning and Deduction screen.
            'If objSalInc.Insert(strDatabaseName, mintUserunkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, True, , , blnApplyUserAccessFilter, strFilerString) = False Then
            If objSalInc.Insert(strDatabaseName, mintUserunkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, True, , , blnApplyUserAccessFilter, strFilerString) = False Then
                'Sohail ((24 Feb 2022) -- End
                'Sohail (18 Feb 2019) -- End
                'Sohail (21 Aug 2015) -- End
                If objSalInc._Message <> "" Then
                    mstrMessage = objSalInc._Message
                Else
                    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                End If
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objSalInc = Nothing

            'Sohail (18 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
            If mblnAssignDefaultTransactionHeads = True Then
                If objED.AssignDefaultTransactionHeads(objDataOperation, strDatabaseName, xYearUnkid, xCompanyUnkid, mintPeriodUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, mintUserunkid, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, mintEmployeeunkid.ToString, False, False, False, blnApplyUserAccessFilter, "", False) = False Then
                    If objED._Message.Trim.Length > 0 Then
                        exForce = New Exception(objED._Message & " : [ED AssignDefaultTransactionHeads]")
                    Else
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage & " : [ED AssignDefaultTransactionHeads]")
                    End If
                    Throw exForce
                End If
                objED = Nothing
            End If
            'Sohail (18 Feb 2019) -- End
            '========================== EMPLOYEE SALARY CHANGES ================================================== > END


            '========================== EMPLOYEE MEMBERSHIP ================================================== > START
            If xMemTran IsNot Nothing Then
                objMemTran._Rehiretranunkid = mintRehiretranunkid
                objMemTran._EmployeeUnkid = mintEmployeeunkid
                objMemTran._DataList = xMemTran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objMemTran.InsertUpdateDelete_MembershipTran(mintUserunkid, objDataOperation) = False Then
                If objMemTran.InsertUpdateDelete_MembershipTran(strDatabaseName, mintUserunkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, objDataOperation, blnApplyUserAccessFilter, strFilerString) = False Then
                    'Sohail (21 Aug 2015) -- End
                    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            objMemTran = Nothing
            '========================== EMPLOYEE MEMBERSHIP ================================================== > END

            If InsertAuditTrailForRehire(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            'If mblnCreateADUserFromEmp Then
            '    If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
            '        If SetEnableDisableUserInAD(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    End If
            'End If
            'Pinkal (18-Aug-2018) -- End
            'Pinkal (11-Sep-2020) -- End


            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objETransfer = Nothing : objERecategorie = Nothing
            objEDates = Nothing : objWPermit = Nothing
            objECCT = Nothing : objSalInc = Nothing : objMemTran = Nothing
            'Sohail (18 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
            objTranHead = Nothing
            objED = Nothing
            'Sohail (18 Feb 2019) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_rehire_tran) </purpose>
    Public Function Update(ByVal strDatabaseName As String _
                           , ByVal xYearUnkid As Integer _
                           , ByVal xCompanyUnkid As Integer _
                           , ByVal xPeriodStart As DateTime _
                           , ByVal xPeriodEnd As DateTime _
                           , ByVal xUserModeSetting As String _
                           , ByVal xOnlyApproved As Boolean _
                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                           , ByVal xCurrScale As DataSet _
                           , ByVal blnAllowToApproveEarningDeduction As Boolean _
                           , ByVal dtCurrentDateAndTime As Date _
                           , ByVal mblnCreateADUserFromEmpMst As Boolean _
                           , Optional ByVal xMemTran As DataTable = Nothing _
                           , Optional ByVal xPrivilegeAllowToApproveSalaryChange As String = "" _
                           , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                           , Optional ByVal strFilerString As String = "" _
                           , Optional ByVal dtTotal_Active_Employee_AsOnDate As Date = Nothing _
                           , Optional ByVal blnIsArutiDemo As String = "" _
                           , Optional ByVal intTotal_Active_Employee_ForAllCompany As Integer = 0 _
                           , Optional ByVal intNoOfEmployees As Integer = 0 _
                           , Optional ByVal blnDonotAttendanceinSeconds As Boolean = False _
                           , Optional ByVal blnUserMustChangePwdOnNextLogOn As Boolean = False _
                           , Optional ByVal mblnCreateADUserFromEmp As Boolean = False _
                           ) As Boolean
        'Sohail (18 Feb 2019) - [dtTotal_Active_Employee_AsOnDate, blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, blnDonotAttendanceinSeconds, blnUserMustChangePwdOnNextLogOn, mblnCreateADUserFromEmp]
        'Pinkal (18-Aug-2018) -- 'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[, ByVal mblnCreateADUserFromEmpMst As Boolean _]


        'Sohail (21 Aug 2015) - [strDatabaseName, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString]

        Dim objETransfer As New clsemployee_transfer_tran
        Dim objERecategorie As New clsemployee_categorization_Tran
        Dim objEDates As New clsemployee_dates_tran
        Dim objWPermit As New clsemployee_workpermit_tran
        Dim objECCT As New clsemployee_cctranhead_tran
        Dim objSalInc As New clsSalaryIncrement
        Dim objMemTran As New clsMembershipTran
        'Sohail (18 Feb 2019) -- Start
        'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
        Dim objED As New clsEarningDeduction
        Dim objTranHead As New clsTransactionHead
        'Sohail (18 Feb 2019) -- End

        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        Dim mdtCurrentDate As DateTime = Nothing
        'Pinkal (18-Aug-2018) -- End

        'S.SANDEEP |06-JUL-2020| -- START
        'ISSUE/ENHANCEMENT : NMB ENHANCEMENT (REHIRE ON CLOSE PERIOD) 
        Dim blnAllowRehireClosedPeriod As Boolean = False
        Dim intStatusId As Integer = 1
        Dim objConfig As New clsConfigOptions
        Dim strkeyvalue As String = ""
        strkeyvalue = objConfig.GetKeyValue(xCompanyUnkid, "ALLOWREHIREONCLOSEDPERIOD", Nothing)
        If strkeyvalue.Trim.Length > 0 Then
            blnAllowRehireClosedPeriod = CBool(strkeyvalue)
        End If
        objConfig = Nothing
        Dim objPrd As New clscommom_period_Tran
        objPrd._Periodunkid(strDatabaseName) = mintPeriodUnkid
        intStatusId = objPrd._Statusid
        objPrd = Nothing
        'S.SANDEEP |06-JUL-2020| -- END

        If isExist(mdtEffectivedate, Nothing, mintEmployeeunkid, mintRehiretranunkid, objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, transfer information is already present for the selected effective date.")
            Return False
        End If

        If isExist(Nothing, mdtReinstatment_Date, mintEmployeeunkid, mintRehiretranunkid, objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, transfer information is already present for the selected reinstatement date.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation


        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        If mblnCreateADUserFromEmpMst Then
            strQ = "SELECT @Date = GETDATE()"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCurrentDate, ParameterDirection.Output)
            objDataOperation.ExecNonQuery(strQ)
            mdtCurrentDate = CDate(objDataOperation.GetParameterValue("@Date"))
        End If
        'Pinkal (18-Aug-2018) -- End


        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@reinstatment_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReinstatment_Date)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)


            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            If mdtActualDate <> Nothing Then
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualDate)
            Else
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Pinkal (07-Mar-2020) -- End

            strQ = "UPDATE hremployee_rehire_tran SET " & _
                   "  effectivedate = @effectivedate" & _
                   ", employeeunkid = @employeeunkid" & _
                   ", reinstatment_date = @reinstatment_date" & _
                   ", changereasonunkid = @changereasonunkid" & _
                   ", isfromemployee = @isfromemployee" & _
                   ", actualdate = @actualdate" & _
                   ", userunkid = @userunkid" & _
                   ", statusunkid = @statusunkid" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE rehiretranunkid = @rehiretranunkid "

            'Pinkal (07-Mar-2020) -- Enhancement - Changes Related to Payroll UAT for NMB.[actualdate = @actualdate]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrMessage = ""

            '========================== EMPLOYEE TRANSFERS ================================================== > START
            Dim xTransferUnkid As Integer = 0

            objDataOperation.ClearParameters()

            strQ = "SELECT @transferunkid = transferunkid FROM hremployee_transfer_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 "

            objDataOperation.AddParameter("@transferunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTransferUnkid, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ) 'S.SANDEEP [01 JUN 2016] -- START -- END

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            xTransferUnkid = objDataOperation.GetParameterValue("@transferunkid")

            If xTransferUnkid > 0 Then
                objETransfer._Transferunkid = xTransferUnkid
                objETransfer._Changereasonunkid = mintChangereasonunkid
                objETransfer._Classgroupunkid = mintClassgroupunkid
                objETransfer._Classunkid = mintClassunkid
                objETransfer._Departmentunkid = mintDepartmentunkid
                objETransfer._Deptgroupunkid = mintDeptgroupunkid
                objETransfer._Effectivedate = mdtEffectivedate
                objETransfer._Employeeunkid = mintEmployeeunkid
                objETransfer._IsFromEmployee = False
                objETransfer._Isvoid = mblnIsvoid
                objETransfer._Rehiretranunkid = mintRehiretranunkid
                objETransfer._Sectiongroupunkid = mintSectiongroupunkid
                objETransfer._Sectionunkid = mintSectionunkid
                objETransfer._Stationunkid = mintStationunkid
                objETransfer._Statusunkid = mintStatusunkid
                objETransfer._Teamunkid = mintTeamunkid
                objETransfer._Unitgroupunkid = mintUnitgroupunkid
                objETransfer._Unitunkid = mintUnitunkid
                objETransfer._Userunkid = mintUserunkid
                objETransfer._Voiddatetime = mdtVoiddatetime
                objETransfer._Voidreason = mstrVoidreason
                objETransfer._Voiduserunkid = mintVoiduserunkid

                'S.SANDEEP [10-MAY-2017] -- START
                'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION


                'Pinkal (12-Oct-2020) -- Start
                'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                'If objETransfer.Update(mblnCreateADUserFromEmpMst, xCompanyUnkid, objDataOperation) = False Then
                If objETransfer.Update(mblnCreateADUserFromEmpMst, xCompanyUnkid, strDatabaseName, objDataOperation) = False Then
                    'Pinkal (12-Oct-2020) -- End

                    'If objETransfer.Update(objDataOperation) = False Then
                    'S.SANDEEP [10-MAY-2017] -- END
                    If objETransfer._Message <> "" Then
                        mstrMessage = objETransfer._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                objETransfer = Nothing
            End If
            '========================== EMPLOYEE TRANSFERS ================================================== > END


            '========================== EMPLOYEE RECATEGORIZE ================================================== > START
            Dim xReCategorizeUnkid As Integer = 0

            objDataOperation.ClearParameters()

            strQ = "SELECT @categorizationtranunkid = categorizationtranunkid FROM hremployee_categorization_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 "

            objDataOperation.AddParameter("@categorizationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xReCategorizeUnkid, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ) 'S.SANDEEP [01 JUN 2016] -- START -- END

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            xReCategorizeUnkid = objDataOperation.GetParameterValue("@categorizationtranunkid")

            If xReCategorizeUnkid > 0 Then
                objERecategorie._Categorizationtranunkid = xReCategorizeUnkid
                objERecategorie._Changereasonunkid = mintChangereasonunkid
                objERecategorie._Effectivedate = mdtEffectivedate
                objERecategorie._Employeeunkid = mintEmployeeunkid
                objERecategorie._Gradelevelunkid = 0
                objERecategorie._Gradeunkid = 0
                objERecategorie._Isfromemployee = False
                objERecategorie._Isvoid = mblnIsvoid
                objERecategorie._JobGroupunkid = mintJobGroupunkid
                objERecategorie._Jobunkid = mintJobunkid
                objERecategorie._Rehiretranunkid = mintRehiretranunkid
                objERecategorie._Statusunkid = mintStatusunkid
                objERecategorie._Userunkid = mintUserunkid
                objERecategorie._Voiddatetime = mdtVoiddatetime
                objERecategorie._Voidreason = mstrVoidreason
                objERecategorie._Voiduserunkid = mintVoiduserunkid


                'S.SANDEEP [10-MAY-2017] -- START
                'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION


                'Pinkal (12-Oct-2020) -- Start
                'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                'If objERecategorie.Update(xCompanyUnkid, mblnCreateADUserFromEmpMst, objDataOperation) = False Then
                If objERecategorie.Update(xCompanyUnkid, mblnCreateADUserFromEmpMst, strDatabaseName, objDataOperation) = False Then
                    'Pinkal (12-Oct-2020) -- End

                    'If objERecategorie.Update(objDataOperation) = False Then
                    'S.SANDEEP [10-MAY-2017] -- END
                    If objERecategorie._Message <> "" Then
                        mstrMessage = objERecategorie._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                objERecategorie = Nothing
            End If
            '========================== EMPLOYEE RECATEGORIZE ================================================== > END


            '========================== EMPLOYEE DATES ================================================== > START
            Dim xDatesUnkid As Integer = 0
            '--------------> Confirmation Date <----------------- START
            objDataOperation.ClearParameters()

            strQ = "SELECT @datestranunkid = datestranunkid FROM hremployee_dates_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_CONFIRMATION

            objDataOperation.AddParameter("@datestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xDatesUnkid, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ) 'S.SANDEEP [01 JUN 2016] -- START -- END

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            xDatesUnkid = objDataOperation.GetParameterValue("@datestranunkid")

            If xDatesUnkid > 0 Then
                objEDates._Datestranunkid = xDatesUnkid
                objEDates._Changereasonunkid = mintChangereasonunkid
                objEDates._Date1 = mdtConfirmationDate
                objEDates._Date2 = Nothing
                objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_CONFIRMATION
                objEDates._Effectivedate = mdtEffectivedate
                objEDates._Employeeunkid = mintEmployeeunkid
                objEDates._Isconfirmed = True
                objEDates._Isfromemployee = False
                objEDates._Isvoid = mblnIsvoid
                objEDates._Rehiretranunkid = mintRehiretranunkid
                objEDates._Statusunkid = mintStatusunkid
                objEDates._Userunkid = mintUserunkid
                objEDates._Voiddatetime = mdtVoiddatetime
                objEDates._Voidreason = mstrVoidreason
                objEDates._Voiduserunkid = mintVoiduserunkid

                'S.SANDEEP [10-MAY-2017] -- START
                'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'If objEDates.Update(xCompanyUnkid, objDataOperation) = False Then
                If objEDates.Update(mblnCreateADUserFromEmpMst, xCompanyUnkid, objDataOperation) = False Then
                    'Pinkal (18-Aug-2018) -- End
                    'If objEDates.Update(objDataOperation) = False Then
                    'S.SANDEEP [10-MAY-2017] -- END
                    If objEDates._Message <> "" Then
                        mstrMessage = objEDates._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            '--------------> Confirmation Date <----------------- END

            '--------------> Probation Date <----------------- START
            xDatesUnkid = 0 : objDataOperation.ClearParameters()

            strQ = "SELECT @datestranunkid = datestranunkid FROM hremployee_dates_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_PROBATION

            objDataOperation.AddParameter("@datestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xDatesUnkid, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ) 'S.SANDEEP [01 JUN 2016] -- START -- END

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            xDatesUnkid = objDataOperation.GetParameterValue("@datestranunkid")

            If xDatesUnkid > 0 Then
                If mdtProbationFromDate <> Nothing AndAlso mdtProbationToDate <> Nothing Then 'S.SANDEEP [04 JUL 2016] -- START -- END
                objEDates._Datestranunkid = xDatesUnkid
                objEDates._Changereasonunkid = mintChangereasonunkid
                objEDates._Date1 = mdtProbationFromDate
                objEDates._Date2 = mdtProbationToDate
                objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_PROBATION
                objEDates._Effectivedate = mdtEffectivedate
                objEDates._Employeeunkid = mintEmployeeunkid
                objEDates._Isvoid = mblnIsvoid
                objEDates._Rehiretranunkid = mintRehiretranunkid
                objEDates._Statusunkid = mintStatusunkid
                objEDates._Userunkid = mintUserunkid
                objEDates._Voiddatetime = mdtVoiddatetime
                objEDates._Voidreason = mstrVoidreason
                objEDates._Voiduserunkid = mintVoiduserunkid

                    'S.SANDEEP [10-MAY-2017] -- START
                    'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    'If objEDates.Update(xCompanyUnkid, objDataOperation) = False Then
                    If objEDates.Update(mblnCreateADUserFromEmpMst, xCompanyUnkid, objDataOperation) = False Then
                        'Pinkal (18-Aug-2018) -- End
                        'If objEDates.Update(objDataOperation) = False Then
                        'S.SANDEEP [10-MAY-2017] -- END
                    If objEDates._Message <> "" Then
                        mstrMessage = objEDates._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            End If
            '--------------> Probation Date <----------------- END

            '--------------> Termination Date <----------------- START
            xDatesUnkid = 0 : objDataOperation.ClearParameters()

            strQ = "SELECT @datestranunkid = datestranunkid FROM hremployee_dates_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION

            objDataOperation.AddParameter("@datestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xDatesUnkid, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ) 'S.SANDEEP [01 JUN 2016] -- START -- END

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            xDatesUnkid = objDataOperation.GetParameterValue("@datestranunkid")

            If xDatesUnkid > 0 Then
                If mdtEOCDate <> Nothing AndAlso mdtLeavingDate <> Nothing Then 'S.SANDEEP [04 JUL 2016] -- START -- END
                objEDates._Datestranunkid = xDatesUnkid
                objEDates._Actionreasonunkid = mintChangereasonunkid
                objEDates._Changereasonunkid = mintChangereasonunkid
                objEDates._Date1 = mdtEOCDate
                objEDates._Date2 = mdtLeavingDate
                objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_TERMINATION
                objEDates._Effectivedate = mdtEffectivedate
                objEDates._Employeeunkid = mintEmployeeunkid
                objEDates._Isvoid = mblnIsvoid
                objEDates._Isexclude_payroll = mblnExcludeFromPayroll
                objEDates._Rehiretranunkid = mintRehiretranunkid
                objEDates._Statusunkid = mintStatusunkid
                objEDates._Userunkid = mintUserunkid
                objEDates._Voiddatetime = mdtVoiddatetime
                objEDates._Voidreason = mstrVoidreason
                objEDates._Voiduserunkid = mintVoiduserunkid


                    'Pinkal (07-Mar-2020) -- Start
                    'Enhancement - Changes Related to Payroll UAT for NMB.
                    objEDates._ActualDate = mdtActualDate
                    'Pinkal (07-Mar-2020) -- End

                    'S.SANDEEP [10-MAY-2017] -- START
                    'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    'If objEDates.Update(xCompanyUnkid, objDataOperation) = False Then
                    If objEDates.Update(mblnCreateADUserFromEmpMst, xCompanyUnkid, objDataOperation) = False Then
                        'Pinkal (18-Aug-2018) -- End

                        'If objEDates.Update(objDataOperation) = False Then
                        'S.SANDEEP [10-MAY-2017] -- END
                    If objEDates._Message <> "" Then
                        mstrMessage = objEDates._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            End If
            '--------------> Termination Date <----------------- END

            '--------------> Retirement Date <----------------- START
            xDatesUnkid = 0 : objDataOperation.ClearParameters()

            strQ = "SELECT @datestranunkid = datestranunkid FROM hremployee_dates_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_RETIREMENT

            objDataOperation.AddParameter("@datestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xDatesUnkid, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ) 'S.SANDEEP [01 JUN 2016] -- START -- END

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            xDatesUnkid = objDataOperation.GetParameterValue("@datestranunkid")

            If xDatesUnkid > 0 Then
                If mdtRetirementDate <> Nothing Then 'S.SANDEEP [04 JUL 2016] -- START -- END
                objEDates._Datestranunkid = xDatesUnkid
                objEDates._Changereasonunkid = mintChangereasonunkid
                objEDates._Date1 = mdtRetirementDate
                objEDates._Date2 = Nothing
                objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_RETIREMENT
                objEDates._Effectivedate = mdtEffectivedate
                objEDates._Employeeunkid = mintEmployeeunkid
                objEDates._Isvoid = mblnIsvoid
                objEDates._Rehiretranunkid = mintRehiretranunkid
                objEDates._Statusunkid = mintStatusunkid
                objEDates._Userunkid = mintUserunkid
                objEDates._Voiddatetime = mdtVoiddatetime
                objEDates._Voidreason = mstrVoidreason
                objEDates._Voiduserunkid = mintVoiduserunkid

                    'S.SANDEEP [10-MAY-2017] -- START
                    'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

                    'Pinkal (18-Aug-2018) -- Start
                    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                    'If objEDates.Update(xCompanyUnkid, objDataOperation) = False Then
                    If objEDates.Update(mblnCreateADUserFromEmpMst, xCompanyUnkid, objDataOperation) = False Then
                        'Pinkal (18-Aug-2018) -- End
                        'If objEDates.Update(objDataOperation) = False Then
                        'S.SANDEEP [10-MAY-2017] -- END
                    If objEDates._Message <> "" Then
                        mstrMessage = objEDates._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            End If
            '--------------> Retirement Date <----------------- END

            objEDates = Nothing
            '========================== EMPLOYEE DATES ================================================== > END


            '========================== EMPLOYEE WORK PERMIT ================================================== > START
            Dim xWorkPermitUnkid As Integer = 0

            objDataOperation.ClearParameters()

            strQ = "SELECT @workpermittranunkid = workpermittranunkid FROM hremployee_work_permit_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 AND isresidentpermit = 0 "
            'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {} -- END

            objDataOperation.AddParameter("@workpermittranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xWorkPermitUnkid, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ) 'S.SANDEEP [01 JUN 2016] -- START -- END

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            xWorkPermitUnkid = objDataOperation.GetParameterValue("@workpermittranunkid")

            If xWorkPermitUnkid > 0 Then
                objWPermit._Workpermittranunkid = xWorkPermitUnkid
                objWPermit._Changereasonunkid = mintChangereasonunkid
                objWPermit._Effectivedate = mdtEffectivedate
                objWPermit._Employeeunkid = mintEmployeeunkid
                objWPermit._Expiry_Date = mdtExpiry_Date
                objWPermit._Issue_Date = mdtIssue_Date
                objWPermit._Isvoid = mblnIsvoid
                objWPermit._Statusunkid = mintStatusunkid
                objWPermit._Userunkid = mintUserunkid
                objWPermit._Voiddatetime = mdtVoiddatetime
                objWPermit._Voidreason = mstrVoidreason
                objWPermit._Voiduserunkid = mintVoiduserunkid
                objWPermit._Work_Permit_No = mstrWork_Permit_No
                objWPermit._Workcountryunkid = mintWorkcountryunkid
                objWPermit._Rehiretranunkid = mintRehiretranunkid
                'S.SANDEEP [04-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 120
                objWPermit._Issue_Place = mstrIssue_Place
                objWPermit._IsResidentPermit = False
                'S.SANDEEP [04-Jan-2018] -- END
                If objWPermit.Update(objDataOperation) = False Then
                    If objWPermit._Message <> "" Then
                        mstrMessage = objWPermit._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                'objWPermit = Nothing 'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 -- END
            End If
            '========================== EMPLOYEE WORK PERMIT ================================================== > END

            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            '========================== EMPLOYEE RESIDENT PERMIT ================================================== > START
            xWorkPermitUnkid = 0

            objDataOperation.ClearParameters()

            strQ = "SELECT @workpermittranunkid = workpermittranunkid FROM hremployee_work_permit_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 AND isresidentpermit = 1 "

            objDataOperation.AddParameter("@workpermittranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xWorkPermitUnkid, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ) 'S.SANDEEP [01 JUN 2016] -- START -- END

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            xWorkPermitUnkid = objDataOperation.GetParameterValue("@workpermittranunkid")

            If xWorkPermitUnkid > 0 Then
                objWPermit._Workpermittranunkid = xWorkPermitUnkid
                objWPermit._Changereasonunkid = mintChangereasonunkid
                objWPermit._Effectivedate = mdtEffectivedate
                objWPermit._Employeeunkid = mintEmployeeunkid
                objWPermit._Expiry_Date = mdtResidentExpiry_Date
                objWPermit._Issue_Date = mdtResidentIssue_Date
                objWPermit._Isvoid = mblnIsvoid
                objWPermit._Statusunkid = mintStatusunkid
                objWPermit._Userunkid = mintUserunkid
                objWPermit._Voiddatetime = mdtVoiddatetime
                objWPermit._Voidreason = mstrVoidreason
                objWPermit._Voiduserunkid = mintVoiduserunkid
                objWPermit._Work_Permit_No = mstrResident_Permit_No
                objWPermit._Workcountryunkid = mintResidentcountryunkid
                objWPermit._Rehiretranunkid = mintRehiretranunkid
                objWPermit._IsResidentPermit = True

                If objWPermit.Update(objDataOperation) = False Then
                    If objWPermit._Message <> "" Then
                        mstrMessage = objWPermit._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                objWPermit = Nothing
            End If
            '========================== EMPLOYEE WORK PERMIT ================================================== > END
            'S.SANDEEP [04-Jan-2018] -- END

            '========================== EMPLOYEE COST-CENTER & TRANSACTION HEAD ==================================== > START
            Dim xCCTHeadUnkid As Integer = 0

            objDataOperation.ClearParameters()

            strQ = "SELECT @cctranheadunkid = cctranheadunkid FROM hremployee_cctranhead_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 AND istransactionhead = 0 "

            objDataOperation.AddParameter("@cctranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCCTHeadUnkid, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ) 'S.SANDEEP [01 JUN 2016] -- START -- END

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            xCCTHeadUnkid = objDataOperation.GetParameterValue("@cctranheadunkid")

            If xCCTHeadUnkid > 0 Then
                objECCT._Cctranheadunkid = xCCTHeadUnkid
                objECCT._Cctranheadvalueid = mintCostCenterUnkid
                objECCT._Changereasonunkid = mintChangereasonunkid
                objECCT._Effectivedate = mdtEffectivedate
                objECCT._Employeeunkid = mintEmployeeunkid
                objECCT._Istransactionhead = False
                objECCT._Isvoid = mblnIsvoid
                objECCT._Rehiretranunkid = mintRehiretranunkid
                objECCT._Statusunkid = mintStatusunkid
                objECCT._Userunkid = mintUserunkid
                objECCT._Voiddatetime = mdtVoiddatetime
                objECCT._Voidreason = mstrVoidreason
                objECCT._Voiduserunkid = mintVoiduserunkid

                If objECCT.Update(objDataOperation) = False Then
                    If objECCT._Message <> "" Then
                        mstrMessage = objECCT._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            xCCTHeadUnkid = 0

            objDataOperation.ClearParameters()

            strQ = "SELECT @cctranheadunkid = cctranheadunkid FROM hremployee_cctranhead_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 AND istransactionhead = 1 "

            objDataOperation.AddParameter("@cctranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCCTHeadUnkid, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ) 'S.SANDEEP [01 JUN 2016] -- START -- END

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            xCCTHeadUnkid = objDataOperation.GetParameterValue("@cctranheadunkid")

            If xCCTHeadUnkid > 0 Then
                objECCT._Cctranheadunkid = xCCTHeadUnkid
                objECCT._Cctranheadvalueid = mintTransactionHeadUnkid
                objECCT._Changereasonunkid = mintChangereasonunkid
                objECCT._Effectivedate = mdtEffectivedate
                objECCT._Employeeunkid = mintEmployeeunkid
                objECCT._Istransactionhead = True
                objECCT._Isvoid = mblnIsvoid
                objECCT._Rehiretranunkid = mintRehiretranunkid
                objECCT._Statusunkid = mintStatusunkid
                objECCT._Userunkid = mintUserunkid
                objECCT._Voiddatetime = mdtVoiddatetime
                objECCT._Voidreason = mstrVoidreason
                objECCT._Voiduserunkid = mintVoiduserunkid

                If objECCT.Update(objDataOperation) = False Then
                    If objECCT._Message <> "" Then
                        mstrMessage = objECCT._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            objECCT = Nothing
            '========================== EMPLOYEE COST-CENTER & TRANSACTION HEAD ==================================== > END

            'Sohail (18 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
            If blnAllowRehireClosedPeriod = False Then 'S.SANDEEP |06-JUL-2020| -- START -- NMB ENHANCEMENT (REHIRE ON CLOSE PERIOD) -- END
            Dim objEmp As New clsEmployee_Master
            objEmp._DataOperation = objDataOperation
            objEmp._Employeeunkid(mdtReinstatment_Date) = mintEmployeeunkid
            If mintTransactionHeadUnkid > 0 Then
                If objEmp._Tranhedunkid <> mintTransactionHeadUnkid Then
                    objEmp._Tranhedunkid = mintTransactionHeadUnkid
                    If objEmp.Update(strDatabaseName, xYearUnkid, xCompanyUnkid, dtTotal_Active_Employee_AsOnDate, blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, mintUserunkid, blnDonotAttendanceinSeconds, xUserModeSetting, xPeriodStart, xPeriodEnd, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, mblnCreateADUserFromEmp, blnUserMustChangePwdOnNextLogOn, , , , , , , , , objDataOperation) = False Then
                        If objEmp._Message <> "" Then
                            mstrMessage = objEmp._Message
                        Else
                            mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                        End If
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            End If

            objTranHead._xDataOperation = objDataOperation
            objTranHead._Tranheadunkid(strDatabaseName) = mintTransactionHeadUnkid
            Dim dsSalary As DataSet = objED.GetSalaryHeadPeriodList("List", strDatabaseName, mintEmployeeunkid, enModuleReference.Payroll, True, objDataOperation, mintPeriodUnkid)
            If dsSalary.Tables(0).Rows.Count > 0 Then

                objED = New clsEarningDeduction

                objED._Edunkid(objDataOperation, strDatabaseName) = CInt(dsSalary.Tables(0).Rows(0).Item("edunkid"))
                objED._Periodunkid = mintPeriodUnkid
                objED._Tranheadunkid = objEmp._Tranhedunkid
                    objED._DataOperation = objDataOperation 'Sohail (15 Jan 2022)
                objED._Employeeunkid(strDatabaseName) = mintEmployeeunkid
                objED._Trnheadtype_Id = objTranHead._Trnheadtype_Id
                objED._Typeof_Id = objTranHead._Typeof_id
                objED._Calctype_Id = objTranHead._Calctype_Id
                objED._Formula = objTranHead._Formula
                objED._FormulaId = objTranHead._Formulaid
                objED._Userunkid = mintUserunkid
                'Sohail (03 Sep 2019) -- Start
                '0004125: NIBS : Assist to delete pending Salary Change from back end
                'objED._Isapproved = blnAllowToApproveEarningDeduction
                objED._Isapproved = True
                'Sohail (03 Sep 2019) -- End
                objED._Approveruserunkid = mintUserunkid
                If objTranHead._Calctype_Id = enCalcType.FlatRate_Others Then
                    objED._Amount = mdecEmployeeScale
                End If

                If objED.Update(strDatabaseName, False, objDataOperation, dtCurrentDateAndTime) = False Then
                    If objED._Message.Trim.Length > 0 Then
                        exForce = New Exception(objED._Message & " : [ED Update]")
                    Else
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage & " : [ED Insert]")
                    End If
                    Throw exForce
                End If
            Else
                objED = New clsEarningDeduction

                objED._Periodunkid = mintPeriodUnkid
                objED._Tranheadunkid = objEmp._Tranhedunkid
                    objED._DataOperation = objDataOperation 'Sohail (15 Jan 2022)
                objED._Employeeunkid(strDatabaseName) = mintEmployeeunkid
                objED._Trnheadtype_Id = objTranHead._Trnheadtype_Id
                objED._Typeof_Id = objTranHead._Typeof_id
                objED._Calctype_Id = objTranHead._Calctype_Id
                objED._Formula = objTranHead._Formula
                objED._FormulaId = objTranHead._Formulaid
                objED._Userunkid = mintUserunkid
                'Sohail (03 Sep 2019) -- Start
                '0004125: NIBS : Assist to delete pending Salary Change from back end
                'objED._Isapproved = blnAllowToApproveEarningDeduction
                objED._Isapproved = True
                'Sohail (03 Sep 2019) -- End
                objED._Approveruserunkid = mintUserunkid
                If objTranHead._Calctype_Id = enCalcType.FlatRate_Others Then
                    objED._Amount = mdecEmployeeScale
                End If

                If objED.Insert(strDatabaseName, mintUserunkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, False, dtCurrentDateAndTime, , objDataOperation, blnApplyUserAccessFilter, strFilerString, False, blnAllowToApproveEarningDeduction) = False Then
                    If objED._Message.Trim.Length > 0 Then
                        exForce = New Exception(objED._Message & " : [ED Insert]")
                    Else
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage & " : [ED Insert]")
                    End If
                    Throw exForce
                End If
            End If
            End If
            'Sohail (18 Feb 2019) -- End


            '========================== EMPLOYEE SALARY CHANGES ================================================== > START
            Dim xSalaryTranUnkid As Integer = 0

            objDataOperation.ClearParameters()

            strQ = "SELECT @salaryincrementtranunkid = salaryincrementtranunkid FROM prsalaryincrement_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 "

            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xSalaryTranUnkid, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ) 'S.SANDEEP [01 JUN 2016] -- START -- END

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            xSalaryTranUnkid = objDataOperation.GetParameterValue("@salaryincrementtranunkid")
            objDataOperation.ClearParameters()
            If xSalaryTranUnkid > 0 Then
                objSalInc._Salaryincrementtranunkid = xSalaryTranUnkid
                If xCurrScale.Tables(0).Rows.Count > 0 Then
                    If mintSGradeGrpId <> xCurrScale.Tables(0).Rows(0).Item("gradegroupunkid") Or _
                       mintSGradeunkid <> xCurrScale.Tables(0).Rows(0).Item("gradeunkid") Or _
                       mintSGradelevelunkid <> xCurrScale.Tables(0).Rows(0).Item("gradelevelunkid") Then
                        objSalInc._Isgradechange = True
                        objSalInc._Increment = mdecEmployeeScale - xCurrScale.Tables(0).Rows(0).Item("currentscale")
                        objSalInc._Increment_Mode = enSalaryIncrementBy.Grade
                    Else
                        objSalInc._Increment_Mode = enSalaryIncrementBy.Amount
                    End If
                End If
                objSalInc._Reason_Id = mintChangereasonunkid
                objSalInc._Gradegroupunkid = mintSGradeGrpId
                objSalInc._Gradeunkid = mintSGradeunkid
                objSalInc._Gradelevelunkid = mintSGradelevelunkid
                objSalInc._Rehiretranunkid = mintRehiretranunkid
                objSalInc._Periodunkid = mintPeriodUnkid
                objSalInc._Employeeunkid = mintEmployeeunkid
                'Sohail (02 Mar 2020) -- Start
                'Ifakara Issue # : Message "Actual date should not be greater than selected period end date"  on editing first salary change.
                If objSalInc._Incrementdate = objSalInc._ActualDate Then
                    objSalInc._ActualDate = mdtReinstatment_Date
                End If
                'Sohail (02 Mar 2020) -- End
                'S.SANDEEP [27-Mar-2018] -- START
                'ISSUE/ENHANCEMENT : {Internal_Finding}
                'objSalInc._Incrementdate = mdtEffectivedate
                objSalInc._Incrementdate = mdtReinstatment_Date
                'S.SANDEEP [27-Mar-2018] -- END
                objSalInc._Currentscale = mdecEmployeeScale
                objSalInc._Newscale = mdecEmployeeScale
                objSalInc._Gradegroupunkid = mintSGradeGrpId
                objSalInc._Gradeunkid = mintSGradeunkid
                objSalInc._Gradelevelunkid = mintSGradelevelunkid
                objSalInc._Isfromemployee = False
                objSalInc._Userunkid = User._Object._Userunkid
                objSalInc._Isvoid = False

                If xPrivilegeAllowToApproveSalaryChange.Trim.Length <= 0 Then
                    xPrivilegeAllowToApproveSalaryChange = User._Object.Privilege._AllowToApproveSalaryChange.ToString
                End If

                'Sohail (03 Sep 2019) -- Start
                '0004125: NIBS : Assist to delete pending Salary Change from back end
                'If CBool(xPrivilegeAllowToApproveSalaryChange) = True Then
                '    objSalInc._Isapproved = True
                '    objSalInc._Approveruserunkid = mintUserunkid
                'End If
                    objSalInc._Isapproved = True
                    objSalInc._Approveruserunkid = mintUserunkid
                'Sohail (03 Sep 2019) -- End
                objSalInc._Userunkid = mintUserunkid
                objSalInc._Voiddatetime = mdtVoiddatetime
                objSalInc._Voidreason = mstrVoidreason
                objSalInc._Voiduserunkid = mintVoiduserunkid

                If mintInfoSalHeadUnkid > 0 Then
                    objSalInc._InfoSalHeadUnkid = mintInfoSalHeadUnkid
                    objSalInc._IsCopyPrevoiusSLAB = mblnIsCopyPrevoiusSLAB
                    objSalInc._IsOverwritePrevoiusSLAB = mblnIsOverwritePrevoiusSLAB
                End If
                objDataOperation.ClearParameters()
                objSalInc._DataOperation = objDataOperation
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objSalInc.Update(False) = False Then
                'Sohail (24 Feb 2022) -- Start
                'Issue :  : TWC - Not able to approve Pending Salary heads on Earning and Deduction screen.
                'If objSalInc.Update(strDatabaseName, mintUserunkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, False, , blnApplyUserAccessFilter, strFilerString) = False Then
                If objSalInc.Update(strDatabaseName, mintUserunkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateAndTime, False, , blnApplyUserAccessFilter, strFilerString) = False Then
                    'Sohail ((24 Feb 2022) -- End
                    'Sohail (21 Aug 2015) -- End
                    If objSalInc._Message <> "" Then
                        mstrMessage = objSalInc._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                objSalInc = Nothing
            End If
            '========================== EMPLOYEE SALARY CHANGES ================================================== > END


            '========================== EMPLOYEE MEMBERSHIP ================================================== > START
            If xMemTran IsNot Nothing Then
                objMemTran._Rehiretranunkid = mintRehiretranunkid
                objMemTran._EmployeeUnkid = mintEmployeeunkid
                objMemTran._DataList = xMemTran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objMemTran.InsertUpdateDelete_MembershipTran(mintUserunkid, objDataOperation) = False Then
                If objMemTran.InsertUpdateDelete_MembershipTran(strDatabaseName, mintUserunkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, objDataOperation, blnApplyUserAccessFilter, strFilerString) = False Then
                    'Sohail (21 Aug 2015) -- End
                    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            objMemTran = Nothing
            '========================== EMPLOYEE MEMBERSHIP ================================================== > END

            If InsertAuditTrailForRehire(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [10-MAY-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

            'S.SANDEEP [19-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : STOP SYMMETRY REAL TIME (RUTTA'S REQUEST)
            'If Integrate_Symmetry(objDataOperation, xCompanyUnkid, mintEmployeeunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [19-JUL-2017] -- END

            'S.SANDEEP [10-MAY-2017] -- END

            objDataOperation.ReleaseTransaction(True)



            'Pinkal (07-Dec-2019) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst Then
                If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
                    If SetEnableDisableUserInAD(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (07-Dec-2019) -- End



            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            'If mblnCreateADUserFromEmpMst Then
            '    If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
            '        If SetEnableDisableUserInAD(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    End If
            'End If
            'Pinkal (18-Aug-2018) -- End
            'Pinkal (11-Sep-2020) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
            'Sohail (18 Feb 2019) -- Start
            'NMB Enhancement - 76.1 - Provide setting on transaction head to mark as default heads.
            objTranHead = Nothing
            objED = Nothing
            'Sohail (18 Feb 2019) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployee_rehire_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, _
                           ByVal strDatabaseName As String, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           ByVal xPeriodStart As DateTime, _
                           ByVal xPeriodEnd As DateTime, _
                           ByVal xUserModeSetting As String, _
                           ByVal xOnlyApproved As Boolean, _
                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                           ByVal blnAllowToApproveEarningDeduction As Boolean, _
                           ByVal dtCurrentDateAndTime As Date, _
                           ByVal mblnCreateADUserFromEmpMst As Boolean) As Boolean

        'Pinkal (18-Aug-2018) -- 'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[ByVal mblnCreateADUserFromEmpMst As Boolean]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'S.SANDEEP [01 JUN 2016] -- START
        Dim objETransfer As New clsemployee_transfer_tran
        Dim objERecategorie As New clsemployee_categorization_Tran
        Dim objEDates As New clsemployee_dates_tran
        Dim objWPermit As New clsemployee_workpermit_tran
        Dim objECCT As New clsemployee_cctranhead_tran
        Dim objSalInc As New clsSalaryIncrement
        Dim objMemTran As New clsMembershipTran
        'S.SANDEEP [01 JUN 2016] -- END

        'S.SANDEEP [17 SEP 2016] -- START
        Dim objHSalInc As New clsempsalary_history_tran
        'S.SANDEEP [17 SEP 2016] -- END

        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        Dim mdtCurrentDate As Date = Nothing
        'Pinkal (18-Aug-2018) -- End


        objDataOperation = New clsDataOperation

        'Pinkal (18-Aug-2018) -- Start
        'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
        If mblnCreateADUserFromEmpMst Then
            strQ = "SELECT @Date = GETDATE()"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCurrentDate, ParameterDirection.Output)
            objDataOperation.ExecNonQuery(strQ)
            mdtCurrentDate = CDate(objDataOperation.GetParameterValue("@Date"))
        End If
        'Pinkal (18-Aug-2018) -- End


        objDataOperation.BindTransaction() 'S.SANDEEP [01 JUN 2016] -- START -- END
        Try
            strQ = "UPDATE hremployee_rehire_tran SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE rehiretranunkid = @rehiretranunkid "

            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ClearParameters()
            mintRehiretranunkid = intUnkid
            Call GetData(objDataOperation)


            'S.SANDEEP [10-MAY-2017] -- START
            'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

            'S.SANDEEP [19-JUL-2017] -- START
            'ISSUE/ENHANCEMENT : STOP SYMMETRY REAL TIME (RUTTA'S REQUEST)
            'If Integrate_Symmetry(objDataOperation, xCompanyUnkid, mintEmployeeunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'S.SANDEEP [19-JUL-2017] -- END

            'S.SANDEEP [10-MAY-2017] -- END


            '========================== EMPLOYEE TRANSFERS ================================================== > START
            Dim xTransferUnkid As Integer = 0
            objDataOperation.ClearParameters()
            strQ = "SELECT @transferunkid = transferunkid FROM hremployee_transfer_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 "
            objDataOperation.AddParameter("@transferunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTransferUnkid, ParameterDirection.InputOutput)
            objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            xTransferUnkid = objDataOperation.GetParameterValue("@transferunkid")
            If xTransferUnkid > 0 Then
                objETransfer._Isvoid = mblnIsvoid
                objETransfer._Voiddatetime = mdtVoiddatetime
                objETransfer._Voidreason = mstrVoidreason
                objETransfer._Voiduserunkid = mintVoiduserunkid

                'S.SANDEEP [10-MAY-2017] -- START
                'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION


                'Pinkal (12-Oct-2020) -- Start
                'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                'If objETransfer.Delete(xTransferUnkid, xCompanyUnkid, mblnCreateADUserFromEmpMst, objDataOperation) = False Then
                If objETransfer.Delete(xTransferUnkid, xCompanyUnkid, mblnCreateADUserFromEmpMst, strDatabaseName, objDataOperation) = False Then
                    'Pinkal (12-Oct-2020) -- End

                    'If objETransfer.Delete(xTransferUnkid, objDataOperation) = False Then
                    'S.SANDEEP [10-MAY-2017] -- END
                    If objETransfer._Message <> "" Then
                        mstrMessage = objETransfer._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            '========================== EMPLOYEE TRANSFERS ================================================== > END

            '========================== EMPLOYEE RECATEGORIZE ================================================== > START
            Dim xReCategorizeUnkid As Integer = 0 : objDataOperation.ClearParameters()
            strQ = "SELECT @categorizationtranunkid = categorizationtranunkid FROM hremployee_categorization_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 "
            objDataOperation.AddParameter("@categorizationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xReCategorizeUnkid, ParameterDirection.InputOutput)
            objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            xReCategorizeUnkid = objDataOperation.GetParameterValue("@categorizationtranunkid")
            If xReCategorizeUnkid > 0 Then
                objERecategorie._Isvoid = mblnIsvoid
                objERecategorie._Voiddatetime = mdtVoiddatetime
                objERecategorie._Voidreason = mstrVoidreason
                objERecategorie._Voiduserunkid = mintVoiduserunkid
                'S.SANDEEP [10-MAY-2017] -- START
                'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION


                'Pinkal (12-Oct-2020) -- Start
                'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                'If objERecategorie.Delete(xReCategorizeUnkid, xCompanyUnkid, mblnCreateADUserFromEmpMst, objDataOperation) = False Then
                If objERecategorie.Delete(xReCategorizeUnkid, xCompanyUnkid, mblnCreateADUserFromEmpMst, strDatabaseName, objDataOperation) = False Then
                    'Pinkal (12-Oct-2020) -- End

                    'If objERecategorie.Delete(xReCategorizeUnkid, objDataOperation) = False Then
                    'S.SANDEEP [10-MAY-2017] -- END
                    If objERecategorie._Message <> "" Then
                        mstrMessage = objERecategorie._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            '========================== EMPLOYEE RECATEGORIZE ================================================== > END

            '========================== EMPLOYEE DATES ================================================== > START
            Dim xDatesUnkid As Integer = 0
            '--------------> Confirmation Date <----------------- START
            objDataOperation.ClearParameters()
            strQ = "SELECT @datestranunkid = datestranunkid FROM hremployee_dates_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_CONFIRMATION
            objDataOperation.AddParameter("@datestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xDatesUnkid, ParameterDirection.InputOutput)
            objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            xDatesUnkid = objDataOperation.GetParameterValue("@datestranunkid")
            If xDatesUnkid > 0 Then
                objEDates._Isvoid = mblnIsvoid
                objEDates._Voiddatetime = mdtVoiddatetime
                objEDates._Voidreason = mstrVoidreason
                objEDates._Voiduserunkid = mintVoiduserunkid
                'S.SANDEEP [10-MAY-2017] -- START
                'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'If objEDates.Delete(xDatesUnkid, xCompanyUnkid, objDataOperation) = False Then
                If objEDates.Delete(mblnCreateADUserFromEmpMst, xDatesUnkid, xCompanyUnkid, objDataOperation) = False Then
                    'Pinkal (18-Aug-2018) -- End
                    'If objEDates.Delete(xDatesUnkid, objDataOperation) = False Then
                    'S.SANDEEP [10-MAY-2017] -- END
                    If objEDates._Message <> "" Then
                        mstrMessage = objEDates._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            '--------------> Confirmation Date <----------------- END

            '--------------> Probation Date <----------------- START
            xDatesUnkid = 0 : objDataOperation.ClearParameters()
            strQ = "SELECT @datestranunkid = datestranunkid FROM hremployee_dates_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_PROBATION
            objDataOperation.AddParameter("@datestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xDatesUnkid, ParameterDirection.InputOutput)
            objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            xDatesUnkid = objDataOperation.GetParameterValue("@datestranunkid")
            If xDatesUnkid > 0 Then
                objEDates._Isvoid = mblnIsvoid
                objEDates._Voiddatetime = mdtVoiddatetime
                objEDates._Voidreason = mstrVoidreason
                objEDates._Voiduserunkid = mintVoiduserunkid
                'S.SANDEEP [10-MAY-2017] -- START
                'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'If objEDates.Delete(xDatesUnkid, xCompanyUnkid, objDataOperation) = False Then
                If objEDates.Delete(mblnCreateADUserFromEmpMst, xDatesUnkid, xCompanyUnkid, objDataOperation) = False Then
                    'Pinkal (18-Aug-2018) -- End
                    'If objEDates.Delete(xDatesUnkid, objDataOperation) = False Then
                    'S.SANDEEP [10-MAY-2017] -- END
                    If objEDates._Message <> "" Then
                        mstrMessage = objEDates._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            '--------------> Probation Date <----------------- END

            '--------------> Termination Date <----------------- START
            xDatesUnkid = 0 : objDataOperation.ClearParameters()
            strQ = "SELECT @datestranunkid = datestranunkid FROM hremployee_dates_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION
            objDataOperation.AddParameter("@datestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xDatesUnkid, ParameterDirection.InputOutput)
            objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            xDatesUnkid = objDataOperation.GetParameterValue("@datestranunkid")
            If xDatesUnkid > 0 Then
                objEDates._Isvoid = mblnIsvoid
                objEDates._Voiddatetime = mdtVoiddatetime
                objEDates._Voidreason = mstrVoidreason
                objEDates._Voiduserunkid = mintVoiduserunkid
                'S.SANDEEP [10-MAY-2017] -- START
                'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'If objEDates.Delete(xDatesUnkid, xCompanyUnkid, objDataOperation) = False Then
                If objEDates.Delete(mblnCreateADUserFromEmpMst, xDatesUnkid, xCompanyUnkid, objDataOperation) = False Then
                    'Pinkal (18-Aug-2018) -- End
                    'If objEDates.Delete(xDatesUnkid, objDataOperation) = False Then
                    'S.SANDEEP [10-MAY-2017] -- END
                    If objEDates._Message <> "" Then
                        mstrMessage = objEDates._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            '--------------> Termination Date <----------------- END

            '--------------> Retirement Date <----------------- START
            xDatesUnkid = 0 : objDataOperation.ClearParameters()
            strQ = "SELECT @datestranunkid = datestranunkid FROM hremployee_dates_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_RETIREMENT
            objDataOperation.AddParameter("@datestranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xDatesUnkid, ParameterDirection.InputOutput)
            objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            xDatesUnkid = objDataOperation.GetParameterValue("@datestranunkid")
            If xDatesUnkid > 0 Then
                objEDates._Isvoid = mblnIsvoid
                objEDates._Voiddatetime = mdtVoiddatetime
                objEDates._Voidreason = mstrVoidreason
                objEDates._Voiduserunkid = mintVoiduserunkid
                'S.SANDEEP [10-MAY-2017] -- START
                'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'If objEDates.Delete(xDatesUnkid, xCompanyUnkid, objDataOperation) = False Then
                If objEDates.Delete(mblnCreateADUserFromEmpMst, xDatesUnkid, xCompanyUnkid, objDataOperation) = False Then
                    'Pinkal (18-Aug-2018) -- End
                    'If objEDates.Delete(xDatesUnkid, objDataOperation) = False Then
                    'S.SANDEEP [10-MAY-2017] -- END
                    If objEDates._Message <> "" Then
                        mstrMessage = objEDates._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            '--------------> Retirement Date <----------------- END
            '========================== EMPLOYEE DATES ================================================== > END

            '========================== EMPLOYEE WORK PERMIT ================================================== > START
            Dim xWorkPermitUnkid As Integer = 0 : objDataOperation.ClearParameters()
            strQ = "SELECT @workpermittranunkid = workpermittranunkid FROM hremployee_work_permit_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 "
            objDataOperation.AddParameter("@workpermittranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xWorkPermitUnkid, ParameterDirection.InputOutput)
            objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            xWorkPermitUnkid = objDataOperation.GetParameterValue("@workpermittranunkid")
            If xWorkPermitUnkid > 0 Then
                objWPermit._Isvoid = mblnIsvoid
                objWPermit._Voiddatetime = mdtVoiddatetime
                objWPermit._Voidreason = mstrVoidreason
                objWPermit._Voiduserunkid = mintVoiduserunkid

                If objWPermit.Delete(xWorkPermitUnkid, objDataOperation) = False Then
                    If objWPermit._Message <> "" Then
                        mstrMessage = objWPermit._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            '========================== EMPLOYEE WORK PERMIT ================================================== > END

            '========================== EMPLOYEE COST-CENTER & TRANSACTION HEAD ==================================== > START
            Dim xCCTHeadUnkid As Integer = 0 : objDataOperation.ClearParameters()
            strQ = "SELECT @cctranheadunkid = cctranheadunkid FROM hremployee_cctranhead_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 AND istransactionhead = 0 "
            objDataOperation.AddParameter("@cctranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCCTHeadUnkid, ParameterDirection.InputOutput)
            objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            xCCTHeadUnkid = objDataOperation.GetParameterValue("@cctranheadunkid")
            If xCCTHeadUnkid > 0 Then
                objECCT._Isvoid = mblnIsvoid
                objECCT._Voiddatetime = mdtVoiddatetime
                objECCT._Voidreason = mstrVoidreason
                objECCT._Voiduserunkid = mintVoiduserunkid

                If objECCT.Delete(xCCTHeadUnkid, objDataOperation) = False Then
                    If objECCT._Message <> "" Then
                        mstrMessage = objECCT._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            xCCTHeadUnkid = 0 : objDataOperation.ClearParameters()
            strQ = "SELECT @cctranheadunkid = cctranheadunkid FROM hremployee_cctranhead_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 AND istransactionhead = 1 "
            objDataOperation.AddParameter("@cctranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCCTHeadUnkid, ParameterDirection.InputOutput)
            objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            xCCTHeadUnkid = objDataOperation.GetParameterValue("@cctranheadunkid")
            If xCCTHeadUnkid > 0 Then
                objECCT._Isvoid = mblnIsvoid
                objECCT._Voiddatetime = mdtVoiddatetime
                objECCT._Voidreason = mstrVoidreason
                objECCT._Voiduserunkid = mintVoiduserunkid

                If objECCT.Delete(xCCTHeadUnkid, objDataOperation) = False Then
                    If objECCT._Message <> "" Then
                        mstrMessage = objECCT._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            '========================== EMPLOYEE COST-CENTER & TRANSACTION HEAD ==================================== > END

            '========================== EMPLOYEE SALARY CHANGES ================================================== > START
            Dim xSalaryTranUnkid As Integer = 0 : objDataOperation.ClearParameters()
            strQ = "SELECT @salaryincrementtranunkid = salaryincrementtranunkid FROM prsalaryincrement_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 "
            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xSalaryTranUnkid, ParameterDirection.InputOutput)
            objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            xSalaryTranUnkid = objDataOperation.GetParameterValue("@salaryincrementtranunkid")
            If xSalaryTranUnkid > 0 Then
                objSalInc._Isvoid = mblnIsvoid
                objSalInc._Voiddatetime = mdtVoiddatetime
                objSalInc._Voidreason = mstrVoidreason
                objSalInc._Voiduserunkid = mintVoiduserunkid

                'Sohail (24 Feb 2022) -- Start
                'Issue :  : TWC - Not able to approve Pending Salary heads on Earning and Deduction screen.
                'If objSalInc.Void(strDatabaseName, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, xSalaryTranUnkid, mintVoiduserunkid, mdtVoiddatetime, mstrVoidreason, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, , , objDataOperation) = False Then
                If objSalInc.Void(strDatabaseName, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, xSalaryTranUnkid, mintVoiduserunkid, mdtVoiddatetime, mstrVoidreason, True, dtCurrentDateAndTime, , , objDataOperation) = False Then
                    'Sohail ((24 Feb 2022) -- End
                    If objSalInc._Message <> "" Then
                        mstrMessage = objSalInc._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            End If

            'S.SANDEEP [17 SEP 2016] -- START
            xSalaryTranUnkid = 0 : objDataOperation.ClearParameters()
            strQ = "SELECT @empsalaryhistorytranunkid = empsalaryhistorytranunkid FROM prempsalary_history_tran WHERE rehiretranunkid = '" & mintRehiretranunkid & "' AND isvoid = 0 "
            objDataOperation.AddParameter("@empsalaryhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xSalaryTranUnkid, ParameterDirection.InputOutput)
            objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            xSalaryTranUnkid = objDataOperation.GetParameterValue("@empsalaryhistorytranunkid")

            If xSalaryTranUnkid > 0 Then
                objHSalInc._Isvoid = mblnIsvoid
                objHSalInc._Voiddatetime = mdtVoiddatetime
                objHSalInc._Voidreason = mstrVoidreason
                objHSalInc._Voiduserunkid = mintVoiduserunkid
                'S.SANDEEP [21-JUN-2017] -- START
                objHSalInc._objDataOperation = objDataOperation
                'S.SANDEEP [21-JUN-2017] -- END

                If objHSalInc.Delete(xSalaryTranUnkid, dtCurrentDateAndTime) = False Then
                    If objHSalInc._Message <> "" Then
                        mstrMessage = objHSalInc._Message
                    Else
                        mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    End If
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            End If
            'S.SANDEEP [17 SEP 2016] -- START

            '========================== EMPLOYEE SALARY CHANGES ================================================== > END

            '========================== EMPLOYEE MEMBERSHIP ================================================== > START
            objMemTran._Rehiretranunkid = mintRehiretranunkid
            objMemTran._EmployeeUnkid = mintEmployeeunkid
            Dim xMemTran As DataTable = New DataView(objMemTran._DataList, "rehiretranunkid > 0", "", DataViewRowState.CurrentRows).ToTable()
            If xMemTran IsNot Nothing Then
                For Each xrow As DataRow In xMemTran.Rows
                    xrow.Item("AUD") = "D"
                Next
                If objMemTran.InsertUpdateDelete_MembershipTran(strDatabaseName, mintVoiduserunkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, objDataOperation) = False Then
                    mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            '========================== EMPLOYEE MEMBERSHIP ================================================== > END

            If InsertAuditTrailForRehire(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst Then
                If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
                    If SetEnableDisableUserInAD(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Pinkal (18-Aug-2018) -- End


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            'If mblnCreateADUserFromEmpMst Then
            '    If mdtEffectivedate.Date <= mdtCurrentDate.Date Then
            '        If SetEnableDisableUserInAD(objDataOperation, mintEmployeeunkid, mdtCurrentDate) = False Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    End If
            'End If
            'Pinkal (18-Aug-2018) -- End
            'Pinkal (11-Sep-2020) -- End

            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isExist(ByVal xEffDate As DateTime, ByVal xRehireDate As DateTime, ByVal xEmployeeId As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
                   "  rehiretranunkid " & _
                   ", effectivedate " & _
                   ", employeeunkid " & _
                   ", reinstatment_date " & _
                   ", changereasonunkid " & _
                   ", isfromemployee " & _
                   ", actualdate " & _
                   ", userunkid " & _
                   ", statusunkid " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", voidreason " & _
                   "FROM hremployee_rehire_tran " & _
                   "WHERE isvoid = 0 "


            'Pinkal (07-Mar-2020) -- Enhancement - Changes Related to Payroll UAT for NMB.[actualdate]

            If xEffDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),effectivedate,112) = @effectivedate "
            End If

            If intUnkid > 0 Then
                strQ &= " AND rehiretranunkid <> @rehiretranunkid "
            End If

            If xEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            If xRehireDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),reinstatment_date,112) = @reinstatment_date "
            End If

            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xEffDate))
            objDataOperation.AddParameter("@reinstatment_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xRehireDate))
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Private Function InsertAuditTrailForRehire(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As enAuditType) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            StrQ = "INSERT INTO athremployee_rehire_tran ( " & _
                              "  rehiretranunkid " & _
                              ", effectivedate " & _
                              ", employeeunkid " & _
                              ", reinstatment_date " & _
                              ", changereasonunkid " & _
                              ", isfromemployee " & _
                              ", actualdate " & _
                              ", statusunkid " & _
                              ", audittype " & _
                              ", audituserunkid " & _
                              ", auditdatetime " & _
                              ", ip " & _
                              ", machine_name " & _
                              ", form_name " & _
                              ", module_name1 " & _
                              ", module_name2 " & _
                              ", module_name3 " & _
                              ", module_name4 " & _
                              ", module_name5 " & _
                              ", isweb" & _
                          ") VALUES (" & _
                              "  @rehiretranunkid " & _
                              ", @effectivedate " & _
                              ", @employeeunkid " & _
                              ", @reinstatment_date " & _
                              ", @changereasonunkid " & _
                              ", @isfromemployee " & _
                              ", @actualdate " & _
                              ", @statusunkid " & _
                              ", @audittype " & _
                              ", @audituserunkid " & _
                              ", @auditdatetime " & _
                              ", @ip " & _
                              ", @machine_name " & _
                              ", @form_name " & _
                              ", @module_name1 " & _
                              ", @module_name2 " & _
                              ", @module_name3 " & _
                              ", @module_name4 " & _
                              ", @module_name5 " & _
                              ", @isweb" & _
                          ") "


            'Pinkal (07-Mar-2020) -- Enhancement - Changes Related to Payroll UAT for NMB.[@actualdate ]

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@reinstatment_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReinstatment_Date)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)

            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            If mdtActualDate <> Nothing Then
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualDate)
            Else
                objDataOperation.AddParameter("@actualdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Pinkal (07-Mar-2020) -- End

            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebClientIP.Trim = "", getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim = "", getHostName, mstrWebHostName))
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 3, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailsForDates; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Get_Current_Rehire(ByVal xDate As Date, Optional ByVal xEmployeeId As Integer = 0) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim mdtAppDate As String = ""
        Try
            Using objDo As New clsDataOperation
                If xEmployeeId > 0 Then
                    StrQ = "SELECT " & _
                           "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                           "FROM hremployee_master " & _
                           "WHERE employeeunkid = '" & xEmployeeId & "' "

                    objDo.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

                    objDo.ExecNonQuery(StrQ)

                    If objDo.ErrorMessage <> "" Then
                        Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    End If

                    mdtAppDate = objDo.GetParameterValue("@ADate")
                End If
                StrQ = "SELECT " & _
                       "     effectivedate " & _
                       "    ,employeeunkid " & _
                       "    ,reinstatment_date " & _
                       "    ,actualdate " & _
                       "    ,rdate " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT " & _
                       "         effectivedate " & _
                       "        ,employeeunkid " & _
                       "        ,reinstatment_date " & _
                       "        ,actualdate " & _
                       "        ,CONVERT(CHAR(8),reinstatment_date,112) AS rdate " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                       "    FROM hremployee_rehire_tran " & _
                       "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effdate "


                'Pinkal (07-Mar-2020) -- Enhancement - Changes Related to Payroll UAT for NMB.[actualdate]

                If xEmployeeId > 0 Then
                    StrQ &= " AND employeeunkid = '" & xEmployeeId & "' "
                End If

                StrQ &= ") AS CD WHERE 1 = 1 AND CD.xNo = 1 "

                If mdtAppDate.Trim.Length > 0 Then
                    StrQ &= " AND CONVERT(CHAR(8),effectivedate,112) >= '" & mdtAppDate & "' "
                End If

                objDo.AddParameter("@effdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate))

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Current_Rehire; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    'S.SANDEEP [02-NOV-2017] -- START
    Public Function IsValidRehire(ByVal intEmployeeId As Integer, ByVal dthiredate As Date, ByVal dtasondate As Date, Optional ByVal objDo As clsDataOperation = Nothing) As Boolean
        Dim StrQ As String = String.Empty
        Dim objDataOperation As clsDataOperation
        If objDo IsNot Nothing Then
            objDataOperation = objDo
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim xCheckdate As String = String.Empty
        Dim blnFlag As Boolean = True
        Try
            StrQ = "select " & _
                   "    @ckdate = convert(nvarchar(8),min(a.date1),112) " & _
                   "from " & _
                   "( " & _
                   "    select " & _
                   "         date1 " & _
                   "        ,row_number()over(partition by employeeunkid,datetypeunkid order by effectivedate desc) as rno " & _
                   "    from hremployee_dates_tran " & _
                   "    where employeeunkid = @employeeunkid and isvoid = 0 and datetypeunkid in (4,6) " & _
                   "        and convert(nvarchar(8),effectivedate,112) <= @effectivedate " & _
                   ")as a where a.rno = 1 "

            objDataOperation.AddParameter("@ckdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, xCheckdate, ParameterDirection.InputOutput)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtasondate).ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            xCheckdate = objDataOperation.GetParameterValue("@ckdate")

            If eZeeDate.convertDate(dthiredate).ToString <= xCheckdate Then
                blnFlag = False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValidRehire; Module Name: " & mstrModuleName)
        Finally
            If objDo Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        'S.SANDEEP [21-NOV-2017] -- START
        Return blnFlag
        'S.SANDEEP [21-NOV-2017] -- END
    End Function
    'S.SANDEEP [02-NOV-2017] -- END

    'S.SANDEEP [10-MAY-2017] -- START
    'ISSUE/ENHANCEMENT : ARUTI-SYMMETRY INTEGRATION
    Private Function Integrate_Symmetry(ByVal xDataOpr As clsDataOperation, _
                                        ByVal intCompanyId As Integer, _
                                        ByVal intEmpId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsDetails As New DataSet
        Dim exForce As Exception = Nothing
        Dim dtEmpAsOnDate As String = ""
        Dim dtServerDate As Date
        Dim dtAppointDate As Date
        Dim blnIsSymmetryIntegrated As Boolean = False
        Try
            If intCompanyId > 0 Then
                StrQ = "SELECT " & _
                       "  @issymmetryintegrated = cfconfiguration.key_value " & _
                       "FROM hrmsConfiguration..cfconfiguration " & _
                       "WHERE UPPER(cfconfiguration.key_name) = 'ISSYMMETRYINTEGRATED' " & _
                       "AND cfconfiguration.companyunkid = @companyunkid "
                With xDataOpr
                    .ClearParameters()
                    .AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
                    .AddParameter("@issymmetryintegrated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsSymmetryIntegrated, ParameterDirection.Output)

                    .ExecNonQuery(StrQ)
                    If .ErrorMessage <> "" Then
                        exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                        Throw exForce
                    End If
                    'S.SANDEEP [12-JUN-2017] -- START
                    'ISSUE/ENHANCEMENT : SYMMETRY DBNULL TO BOOLEAN ERROR
                    'blnIsSymmetryIntegrated = .GetParameterValue("@issymmetryintegrated")
                    If IsDBNull(.GetParameterValue("@issymmetryintegrated")) = False Then
                    blnIsSymmetryIntegrated = .GetParameterValue("@issymmetryintegrated")
                    End If
                    'S.SANDEEP [12-JUN-2017] -- END
                End With

                If blnIsSymmetryIntegrated Then
                    Dim mstrEmpCode As String = String.Empty
                    StrQ = "SELECT @employeecode = employeecode,@serverdate = GETDATE(), @dtAppointDate = appointeddate FROM hremployee_master WHERE employeeunkid = @employeeunkid "
                    With xDataOpr
                        .ClearParameters()
                        .AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
                        .AddParameter("@employeecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpCode, ParameterDirection.InputOutput)
                        .AddParameter("@ServerDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtServerDate, ParameterDirection.Output)
                        .ExecNonQuery(StrQ)
                        If .ErrorMessage <> "" Then
                            exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                        mstrEmpCode = .GetParameterValue("@employeecode")
                        dtServerDate = .GetParameterValue("@serverdate")
                        dtAppointDate = .GetParameterValue("@dtAppointDate")
                    End With

                    StrQ = "SELECT " & _
                           "  @empasondate = cfconfiguration.key_value " & _
                           "FROM hrmsConfiguration..cfconfiguration " & _
                           "WHERE UPPER(cfconfiguration.key_name) = 'EMPLOYEEASONDATE' " & _
                           "AND cfconfiguration.companyunkid = @companyunkid "

                    With xDataOpr
                        .ClearParameters()
                        .AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
                        .AddParameter("@empasondate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, dtEmpAsOnDate, ParameterDirection.Output)

                        .ExecNonQuery(StrQ)
                        If .ErrorMessage <> "" Then
                            exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                        dtEmpAsOnDate = .GetParameterValue("@empasondate")
                    End With

                    Dim dtActiveDate As Date = Nothing

                    StrQ = "SELECT TOP 1 " & _
                           "  @ActiveDate = hremployee_rehire_tran.reinstatment_date " & _
                           "FROM hremployee_rehire_tran " & _
                           "WHERE hremployee_rehire_tran.isvoid = 0 " & _
                           "    AND hremployee_rehire_tran.employeeunkid = @employeeunkid " & _
                           "    AND CONVERT(NVARCHAR(8), hremployee_rehire_tran.effectivedate, 112) <= @xDate " & _
                           "ORDER BY CONVERT(NVARCHAR(8), hremployee_rehire_tran.effectivedate, 112) DESC "

                    With xDataOpr
                        .ClearParameters()
                        .AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
                        .AddParameter("@xDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtEmpAsOnDate)
                        .AddParameter("@ActiveDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtActiveDate, ParameterDirection.Output)

                        .ExecNonQuery(StrQ)
                        If .ErrorMessage <> "" Then
                            exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                        If IsDBNull(.GetParameterValue("@ActiveDate")) = False Then
                            dtActiveDate = .GetParameterValue("@ActiveDate")
                        Else
                            dtActiveDate = dtAppointDate
                        End If
                    End With

                    If mstrEmpCode.Trim.Length > 0 AndAlso dtActiveDate <> Nothing Then
                        StrQ = "SELECT " & _
                               "     cfconfiguration.key_name " & _
                               "    ,cfconfiguration.key_value " & _
                               "FROM hrmsConfiguration..cfconfiguration " & _
                               "WHERE cfconfiguration.companyunkid = @companyunkid " & _
                               "AND UPPER(cfconfiguration.key_name) LIKE '%SYMMETRY%' "

                        xDataOpr.ClearParameters()
                        xDataOpr.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)

                        dsDetails = xDataOpr.ExecQuery(StrQ, "List")

                        If xDataOpr.ErrorMessage <> "" Then
                            exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                            Throw exForce
                        End If

                        If dsDetails.Tables("List").Rows.Count > 0 Then
                            Dim strConn As String = String.Empty
                            Using oSQL As SqlClient.SqlConnection = New SqlClient.SqlConnection
                                strConn = "Data Source="
                                Dim tmp As DataRow() = Nothing
                                tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYDATASERVERADDRESS'")
                                If tmp.Length > 0 Then strConn &= tmp(0)("key_value") & ";"

                                tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYDATABASENAME'")
                                If tmp.Length > 0 Then strConn &= "Initial Catalog=" & tmp(0)("key_value") & ";"

                                tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYAUTHENTICATIONMODEID'")
                                If tmp.Length > 0 Then
                                    If CInt(tmp(0)("key_value")) = 0 Then   'WINDOWS
                                        strConn &= "Integrated Security=True "
                                    ElseIf CInt(tmp(0)("key_value")) = 1 Then   'USER
                                        tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYDATABASEUSERNAME'")
                                        If tmp.Length > 0 Then strConn &= "User ID=" & tmp(0)("key_value") & ";"
                                        tmp = dsDetails.Tables("List").Select("key_name = 'SYMMETRYDATABASEPASSWORD'")
                                        If tmp.Length > 0 Then strConn &= "Password=" & clsSecurity.Decrypt(tmp(0)("key_value"), "ezee") & ";"
                                    End If
                                End If
                                oSQL.ConnectionString = strConn
                                oSQL.Open()
                                StrQ = "" : Dim intRecCount As Integer = 0
                                StrQ = "SELECT * FROM DataImportTable WHERE DataImportTable.EmployeeReference = @EmployeeReference "
                                Using oCmd As SqlClient.SqlCommand = New SqlClient.SqlCommand
                                    oCmd.Connection = oSQL
                                    oCmd.CommandText = StrQ
                                    oCmd.Parameters.Clear()
                                    oCmd.Parameters.AddWithValue("@EmployeeReference", mstrEmpCode)
                                    Dim dsInfo As New DataSet
                                    Dim oDa As New SqlClient.SqlDataAdapter(oCmd)
                                    oDa.Fill(dsInfo)

                                    oCmd.Parameters.Clear()
                                    If dsInfo.Tables(0).Rows.Count > 0 Then 'UPDATE
                                        StrQ = "UPDATE DataImportTable SET " & _
                                               " ActiveDate = @ActiveDate " & _
                                               ",RecordRequest = @RecordRequest " & _
                                               ",ImportNow = @ImportNow " & _
                                               " WHERE RecordCount = @RecordCount "

                                        oCmd.Parameters.AddWithValue("@RecordCount", dsInfo.Tables(0).Rows(0).Item("RecordCount"))
                                        oCmd.Parameters.AddWithValue("@ActiveDate", dtActiveDate)
                                        oCmd.Parameters.AddWithValue("@RecordRequest", IIf(dsInfo.Tables(0).Rows(0).Item("RecordRequest") <> 0, dsInfo.Tables(0).Rows(0).Item("RecordRequest"), 0))
                                        oCmd.Parameters.AddWithValue("@ImportNow", 1)

                                        oCmd.CommandText = StrQ
                                        oCmd.ExecuteNonQuery()

                                    ElseIf dsInfo.Tables(0).Rows.Count <= 0 Then 'INSERT

                                        StrQ = "SELECT " & _
                                               "     hremployee_master.employeecode AS EmployeeReference " & _
                                               "    ,hremployee_master.firstname AS Firstname " & _
                                               "    ,hremployee_master.othername AS InitLet " & _
                                               "    ,hremployee_master.surname AS LastName " & _
                                               "    ,CASE WHEN hremployee_master.accessunkid < = 0 THEN 0 ELSE hremployee_master.accessunkid END AS CardNumber " & _
                                               "    ,CASE WHEN CONVERT(NVARCHAR(8),RH.reinstatment_date,112) > CONVERT(NVARCHAR(8),hremployee_master.appointeddate,112) THEN RH.reinstatment_date ELSE hremployee_master.appointeddate END AS ActiveDate " & _
                                               "    ,ISNULL(tt.name,'') AS PersonalData1 " & _
                                               "    ,hremployee_master.birthdate AS PersonalData2 " & _
                                               "    ,ISNULL(hrdepartment_master.name,'') AS PersonalData3 " & _
                                               "    ,ISNULL(hrjob_master.job_name,'') AS PersonalData4 " & _
                                               "    ,ISNULL(hrclassgroup_master.name,'') AS PersonalData5 " & _
                                               "    ,ISNULL(hrclasses_master.name,'') AS PersonalData6 " & _
                                               "    ,ISNULL(treason,'') AS PersonalData7 " & _
                                               "    ,ED.date1 AS PersonalData8 " & _
                                               "    ,ED.date2 AS PersonalData9 " & _
                                               "    ,RT.date1 AS PersonalData10 " & _
                                               "    ,SP.date1 AS PersonalData11 " & _
                                               "    ,SP.date2 AS PersonalData12 " & _
                                               "    ,ldate AS ldate " & _
                                               "FROM hremployee_master " & _
                                               "LEFT JOIN " & _
                                               "( " & _
                                               "    SELECT " & _
                                               "         hremployee_dates_tran.employeeunkid " & _
                                               "        ,hremployee_dates_tran.date1 " & _
                                               "        ,hremployee_dates_tran.date2 " & _
                                               "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC) AS rno " & _
                                               "        ,CASE WHEN hremployee_dates_tran.rehiretranunkid > 0 THEN RTE.name ELSE TEC.name END AS treason " & _
                                               "    FROM hremployee_dates_tran " & _
                                               "        LEFT JOIN cfcommon_master AS RTE ON RTE.masterunkid = hremployee_dates_tran.changereasonunkid AND RTE.mastertype = '49' " & _
                                               "        LEFT JOIN cfcommon_master AS TEC ON TEC.masterunkid = hremployee_dates_tran.changereasonunkid AND TEC.mastertype = '48' " & _
                                               "    WHERE hremployee_dates_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_dates_tran.effectivedate,112) <= @efdate " & _
                                               "    AND hremployee_dates_tran.datetypeunkid = 4 AND hremployee_dates_tran.employeeunkid = @employeeunkid " & _
                                               ") AS ED ON ED.employeeunkid = hremployee_master.employeeunkid AND ED.rno = 1 AND ED.date1 IS NOT NULL " & _
                                               "LEFT JOIN " & _
                                               "( " & _
                                               "    SELECT " & _
                                               "         hremployee_dates_tran.employeeunkid " & _
                                               "        ,hremployee_dates_tran.date1 " & _
                                               "        ,hremployee_dates_tran.date2 " & _
                                               "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC) AS rno " & _
                                               "    FROM hremployee_dates_tran " & _
                                               "    WHERE hremployee_dates_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_dates_tran.effectivedate,112) <= @efdate " & _
                                               "    AND hremployee_dates_tran.datetypeunkid = 3 AND hremployee_dates_tran.employeeunkid = @employeeunkid  " & _
                                               ") AS SP ON SP.employeeunkid = hremployee_master.employeeunkid AND SP.rno = 1 " & _
                                               "LEFT JOIN " & _
                                               "( " & _
                                               "    SELECT " & _
                                               "         hremployee_dates_tran.employeeunkid " & _
                                               "        ,hremployee_dates_tran.date1 " & _
                                               "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY hremployee_dates_tran.effectivedate DESC) AS rno " & _
                                               "    FROM hremployee_dates_tran " & _
                                               "    WHERE hremployee_dates_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_dates_tran.effectivedate,112) <= @efdate " & _
                                               "    AND hremployee_dates_tran.datetypeunkid = 6 AND hremployee_dates_tran.employeeunkid = @employeeunkid " & _
                                               ") AS RT ON RT.employeeunkid = hremployee_master.employeeunkid AND RT.rno = 1 " & _
                                               "LEFT JOIN " & _
                                               "( " & _
                                               "    SELECT " & _
                                               "         hremployee_categorization_tran.employeeunkid " & _
                                               "        ,hremployee_categorization_tran.jobunkid " & _
                                               "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_categorization_tran.employeeunkid ORDER BY hremployee_categorization_tran.effectivedate DESC) AS rno " & _
                                               "    FROM hremployee_categorization_tran " & _
                                               "    WHERE hremployee_categorization_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_categorization_tran.effectivedate,112) <= @efdate " & _
                                               "    AND hremployee_categorization_tran.employeeunkid = @employeeunkid " & _
                                               ") AS RC ON RC.employeeunkid = hremployee_master.employeeunkid AND RC.rno = 1 " & _
                                               "LEFT JOIN hrjob_master ON RC.jobunkid = hrjob_master.jobunkid " & _
                                               "LEFT JOIN " & _
                                               "( " & _
                                               "    SELECT " & _
                                               "         hremployee_transfer_tran.employeeunkid " & _
                                               "        ,hremployee_transfer_tran.departmentunkid " & _
                                               "        ,hremployee_transfer_tran.classgroupunkid " & _
                                               "        ,hremployee_transfer_tran.classunkid " & _
                                               "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_transfer_tran.employeeunkid ORDER BY hremployee_transfer_tran.effectivedate DESC) AS rno " & _
                                               "    FROM hremployee_transfer_tran " & _
                                               "    WHERE hremployee_transfer_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_transfer_tran.effectivedate,112) <= @efdate " & _
                                               "    AND hremployee_transfer_tran.employeeunkid = @employeeunkid " & _
                                               ") AS TF ON TF.employeeunkid = hremployee_master.employeeunkid AND TF.rno = 1 " & _
                                               "LEFT JOIN hrdepartment_master ON TF.departmentunkid = hrdepartment_master.departmentunkid " & _
                                               "LEFT JOIN hrclassgroup_master ON TF.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
                                               "LEFT JOIN hrclasses_master ON TF.classunkid = hrclasses_master.classesunkid " & _
                                               "LEFT JOIN cfcommon_master AS tt ON tt.masterunkid = hremployee_master.titleunkid AND tt.mastertype = 24 " & _
                                               "LEFT JOIN " & _
                                               "( " & _
                                               "    SELECT " & _
                                               "         hremployee_rehire_tran.employeeunkid " & _
                                               "        ,hremployee_rehire_tran.reinstatment_date " & _
                                               "        ,ROW_NUMBER()OVER(PARTITION BY hremployee_rehire_tran.employeeunkid ORDER BY hremployee_rehire_tran.effectivedate DESC) AS rno " & _
                                               "    FROM hremployee_rehire_tran " & _
                                               "    WHERE hremployee_rehire_tran.isvoid = 0 AND CONVERT(NVARCHAR(8),hremployee_rehire_tran.effectivedate,112) <= @efdate " & _
                                               "    AND hremployee_rehire_tran.employeeunkid = @employeeunkid " & _
                                               ") AS RH ON RH.employeeunkid = hremployee_master.employeeunkid AND RH.rno = 1 " & _
                                               "LEFT JOIN " & _
                                               "( " & _
                                               "    SELECT " & _
                                               "         lvleaveIssue_master.employeeunkid " & _
                                               "        ,lvleaveIssue_tran.leavedate ldate " & _
                                               "    FROM lvleaveIssue_tran " & _
                                               "    JOIN lvleaveIssue_master ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                                               "    WHERE lvleaveIssue_tran.isvoid = 0 AND lvleaveIssue_master.isvoid = 0 AND CONVERT(NVARCHAR(8),lvleaveIssue_tran.leavedate,112) = @efdate " & _
                                               "    AND lvleaveIssue_master.employeeunkid = @employeeunkid " & _
                                               ") AS LV ON LV.employeeunkid = hremployee_master.employeeunkid " & _
                                               "LEFT JOIN " & _
                                               "( " & _
                                               "    SELECT " & _
                                               "         T.EmpId " & _
                                               "        ,T.EOC " & _
                                               "        ,T.LEAVING " & _
                                               "        ,T.isexclude_payroll AS IsExPayroll " & _
                                               "    FROM " & _
                                               "    ( " & _
                                               "        SELECT " & _
                                               "             employeeunkid AS EmpId " & _
                                               "            ,date1 AS EOC " & _
                                               "            ,date2 AS LEAVING " & _
                                               "            ,effectivedate " & _
                                               "            ,isexclude_payroll " & _
                                               "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                               "        FROM  hremployee_dates_tran WHERE datetypeunkid IN(4) AND isvoid = 0 " & _
                                               "        AND CONVERT(CHAR(8),effectivedate,112) <= @efdate " & _
                                               "        AND hremployee_dates_tran.employeeunkid = @employeeunkid " & _
                                               "    ) AS T WHERE T.xNo = 1 " & _
                                               ") AS TRM ON TRM.EmpId = hremployee_master.employeeunkid " & _
                                               "LEFT JOIN " & _
                                               "( " & _
                                               "    SELECT " & _
                                               "         R.EmpId " & _
                                               "        ,R.RETIRE " & _
                                               "    FROM " & _
                                               "    ( " & _
                                               "        SELECT " & _
                                               "             employeeunkid AS EmpId " & _
                                               "            ,date1 AS RETIRE " & _
                                               "            ,effectivedate " & _
                                               "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                               "        FROM  hremployee_dates_tran WHERE datetypeunkid IN(6) AND isvoid = 0 " & _
                                               "        AND CONVERT(CHAR(8),effectivedate,112) <= @efdate " & _
                                               "        AND hremployee_dates_tran.employeeunkid = @employeeunkid " & _
                                               "    ) AS R WHERE R.xNo = 1 " & _
                                               ") AS RET ON RET.EmpId = hremployee_master.employeeunkid " & _
                                               "LEFT JOIN " & _
                                               "( " & _
                                               "    SELECT " & _
                                               "         RH.EmpId " & _
                                               "        ,RH.REHIRE " & _
                                               "    FROM " & _
                                               "    ( " & _
                                               "        SELECT " & _
                                               "             employeeunkid AS EmpId " & _
                                               "            ,reinstatment_date AS REHIRE " & _
                                               "            ,effectivedate " & _
                                               "            ,ROW_NUMBER() OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                                               "        FROM  hremployee_rehire_tran WHERE isvoid = 0 " & _
                                               "        AND CONVERT(CHAR(8),effectivedate,112) <= @efdate " & _
                                               "        AND hremployee_rehire_tran.employeeunkid = @employeeunkid " & _
                                               "    ) AS RH WHERE RH.xNo = 1 " & _
                                               ") AS HIRE ON HIRE.EmpId = hremployee_master.employeeunkid " & _
                                               "WHERE hremployee_master.isapproved = 1 AND hremployee_master.employeeunkid = @employeeunkid "

                                        Dim dsEmp As New DataSet
                                        xDataOpr.ClearParameters()
                                        xDataOpr.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                                        xDataOpr.AddParameter("@efdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, dtEmpAsOnDate)
                                        dsEmp = xDataOpr.ExecQuery(StrQ, "List")

                                        If xDataOpr.ErrorMessage <> "" Then
                                            exForce = New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
                                            Throw exForce
                                        End If
                                        'oCmd.CommandText = StrQ
                                        'oCmd.Parameters.Clear()
                                        'oCmd.Parameters.AddWithValue("@employeeunkid", mintEmployeeunkid)
                                        'oCmd.Parameters.AddWithValue("@efdate", dtEmpAsOnDate)
                                        'oDa = New SqlClient.SqlDataAdapter(oCmd)
                                        'oDa.Fill(dsEmp)

                                        If dsEmp.Tables(0).Rows.Count > 0 Then
                                            StrQ = "INSERT INTO DataImportTable " & _
                                                   "( " & _
                                                   "     EmployeeReference " & _
                                                   "    ,FirstName " & _
                                                   "    ,InitLet " & _
                                                   "    ,LastName " & _
                                                   "    ,CardNumber " & _
                                                   "    ,ActiveDate " & _
                                                   "    ,PersonalData1 " & _
                                                   "    ,PersonalData2 " & _
                                                   "    ,PersonalData3 " & _
                                                   "    ,PersonalData4 " & _
                                                   "    ,PersonalData5 " & _
                                                   "    ,PersonalData6 " & _
                                                   "    ,PersonalData7 " & _
                                                   "    ,PersonalData8 " & _
                                                   "    ,PersonalData9 " & _
                                                   "    ,PersonalData10 " & _
                                                   "    ,PersonalData11 " & _
                                                   "    ,PersonalData12 " & _
                                                   "    ,RecordRequest " & _
                                                   "    ,RecordStatus " & _
                                                   "    ,ImportNow " & _
                                                   ") " & _
                                                   " Values " & _
                                                   "( " & _
                                                   "     @EmployeeReference " & _
                                                   "    ,@FirstName " & _
                                                   "    ,@InitLet " & _
                                                   "    ,@LastName " & _
                                                   "    ,@CardNumber " & _
                                                   "    ,@ActiveDate " & _
                                                   "    ,@PersonalData1 " & _
                                                   "    ,@PersonalData2 " & _
                                                   "    ,@PersonalData3 " & _
                                                   "    ,@PersonalData4 " & _
                                                   "    ,@PersonalData5 " & _
                                                   "    ,@PersonalData6 " & _
                                                   "    ,@PersonalData7 " & _
                                                   "    ,@PersonalData8 " & _
                                                   "    ,@PersonalData9 " & _
                                                   "    ,@PersonalData10 " & _
                                                   "    ,@PersonalData11 " & _
                                                   "    ,@PersonalData12" & _
                                                   "    ,@RecordRequest " & _
                                                   "    ,@RecordStatus  " & _
                                                   "    ,@ImportNow " & _
                                                   " ) "

                                            oCmd.CommandText = StrQ
                                            oCmd.Parameters.Clear()
                                            oCmd.Parameters.AddWithValue("@EmployeeReference", dsEmp.Tables(0).Rows(0).Item("EmployeeReference"))
                                            oCmd.Parameters.AddWithValue("@FirstName", dsEmp.Tables(0).Rows(0).Item("FirstName"))
                                            oCmd.Parameters.AddWithValue("@InitLet", dsEmp.Tables(0).Rows(0).Item("InitLet"))
                                            oCmd.Parameters.AddWithValue("@LastName", dsEmp.Tables(0).Rows(0).Item("LastName"))
                                            oCmd.Parameters.AddWithValue("@CardNumber", dsEmp.Tables(0).Rows(0).Item("CardNumber"))
                                            oCmd.Parameters.AddWithValue("@ActiveDate", dsEmp.Tables(0).Rows(0).Item("ActiveDate"))
                                            oCmd.Parameters.AddWithValue("@PersonalData1", dsEmp.Tables(0).Rows(0).Item("PersonalData1").ToString.Substring(0, IIf(Len(dsEmp.Tables(0).Rows(0).Item("PersonalData1").ToString) < 40, Len(dsEmp.Tables(0).Rows(0).Item("PersonalData1").ToString), 40)))
                                            oCmd.Parameters.AddWithValue("@PersonalData2", dsEmp.Tables(0).Rows(0).Item("PersonalData2"))
                                            oCmd.Parameters.AddWithValue("@PersonalData3", dsEmp.Tables(0).Rows(0).Item("PersonalData3").ToString.Substring(0, IIf(Len(dsEmp.Tables(0).Rows(0).Item("PersonalData3").ToString) < 40, Len(dsEmp.Tables(0).Rows(0).Item("PersonalData3").ToString), 40)))
                                            oCmd.Parameters.AddWithValue("@PersonalData4", dsEmp.Tables(0).Rows(0).Item("PersonalData4").ToString.Substring(0, IIf(Len(dsEmp.Tables(0).Rows(0).Item("PersonalData4").ToString) < 40, Len(dsEmp.Tables(0).Rows(0).Item("PersonalData4").ToString), 40)))
                                            oCmd.Parameters.AddWithValue("@PersonalData5", dsEmp.Tables(0).Rows(0).Item("PersonalData5").ToString.Substring(0, IIf(Len(dsEmp.Tables(0).Rows(0).Item("PersonalData5").ToString) < 40, Len(dsEmp.Tables(0).Rows(0).Item("PersonalData5").ToString), 40)))
                                            oCmd.Parameters.AddWithValue("@PersonalData6", dsEmp.Tables(0).Rows(0).Item("PersonalData6").ToString.Substring(0, IIf(Len(dsEmp.Tables(0).Rows(0).Item("PersonalData6").ToString) < 40, Len(dsEmp.Tables(0).Rows(0).Item("PersonalData6").ToString), 40)))
                                            oCmd.Parameters.AddWithValue("@PersonalData7", dsEmp.Tables(0).Rows(0).Item("PersonalData7").ToString.Substring(0, IIf(Len(dsEmp.Tables(0).Rows(0).Item("PersonalData7").ToString) < 40, Len(dsEmp.Tables(0).Rows(0).Item("PersonalData7").ToString), 40)))
                                            oCmd.Parameters.AddWithValue("@PersonalData8", dsEmp.Tables(0).Rows(0).Item("PersonalData8"))
                                            oCmd.Parameters.AddWithValue("@PersonalData9", dsEmp.Tables(0).Rows(0).Item("PersonalData9"))
                                            oCmd.Parameters.AddWithValue("@PersonalData10", dsEmp.Tables(0).Rows(0).Item("PersonalData10"))
                                            oCmd.Parameters.AddWithValue("@PersonalData11", dsEmp.Tables(0).Rows(0).Item("PersonalData11"))
                                            oCmd.Parameters.AddWithValue("@PersonalData12", dsEmp.Tables(0).Rows(0).Item("PersonalData12"))
                                            oCmd.Parameters.AddWithValue("@RecordRequest", 0)
                                            oCmd.Parameters.AddWithValue("@RecordStatus", 0)
                                            oCmd.Parameters.AddWithValue("@ImportNow", 1)
                                            oCmd.ExecuteNonQuery()

                                        End If
                                    End If
                                End Using
                            End Using
                        End If
                    End If
                End If
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Integrate_Symmetry; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [10-MAY-2017] -- END


    'S.SANDEEP [21-Mar-2018] -- START
    'ISSUE/ENHANCEMENT : {#ARUTI-57}
    Public Function Allow_Void_Rehire(ByVal intemployeeunkid As Integer, ByVal mdtRehiredate As DateTime) As String
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Dim StrMsg As String = "" : Dim intRowCnt = -1
        Try
            Using objDo As New clsDataOperation

                StrQ = "SELECT 1 " & _
                       "FROM prpayrollprocess_tran " & _
                       "    JOIN prtnaleave_tran ON prpayrollprocess_tran.tnaleavetranunkid = prtnaleave_tran.tnaleavetranunkid " & _
                       "WHERE prpayrollprocess_tran.isvoid = 0 AND prtnaleave_tran.isvoid = 0 " & _
                       "AND CONVERT(NVARCHAR(8),processdate,112) >= @hiredate AND prpayrollprocess_tran.employeeunkid = @employeeunkid "

                objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intemployeeunkid)
                objDo.AddParameter("@hiredate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtRehiredate).ToString())

                intRowCnt = objDo.RecordCount(StrQ)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

                If intRowCnt > 0 Then
                    StrMsg = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this rehired information. Reason: Payroll is already processed for this employee.")
                    Exit Try
                End If

                'S.SANDEEP [27-Mar-2018] -- START
                'ISSUE/ENHANCEMENT : {Internal_Finding}
                intRowCnt = -1 : objDo.ClearParameters()
                Dim intRehireTranId As Integer = 0
                StrQ = "SELECT " & _
                       "    @rehiretranunkid = rehiretranunkid " & _
                       "FROM hremployee_rehire_tran WHERE employeeunkid = @employeeunkid " & _
                       "AND CONVERT(NVARCHAR(8),reinstatment_date,112) = @hiredate "

                objDo.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRehireTranId, ParameterDirection.InputOutput)
                objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intemployeeunkid)
                objDo.AddParameter("@hiredate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtRehiredate).ToString())

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If
                If IsDBNull(objDo.GetParameterValue("@rehiretranunkid")) = False Then
                    intRehireTranId = objDo.GetParameterValue("@rehiretranunkid")
                End If

                If intRehireTranId > 0 Then
                    objDo.ClearParameters()
                    StrQ = "SELECT 1 FROM hremployee_transfer_tran WHERE employeeunkid = @employeeunkid AND rehiretranunkid <> @rehiretranunkid AND CONVERT(NVARCHAR(8),effectivedate,112) <= @hiredate UNION " & _
                           "SELECT 1 FROM hremployee_categorization_tran WHERE employeeunkid = @employeeunkid AND rehiretranunkid <> @rehiretranunkid AND CONVERT(NVARCHAR(8),effectivedate,112) <= @hiredate "

                    objDo.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRehireTranId)
                    objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intemployeeunkid)
                    objDo.AddParameter("@hiredate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtRehiredate).ToString())

                    intRowCnt = objDo.RecordCount(StrQ)

                    If objDo.ErrorMessage <> "" Then
                        exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                        Throw exForce
                    End If

                    If intRowCnt = 0 Then
                        StrMsg = Language.getMessage(mstrModuleName, 5, "Sorry, you cannot delete this rehired information. Reason: Employee does not have any other movement information present whose effective date is less than the reinstatement date.")
                        Exit Try
                    End If

                End If
                'S.SANDEEP [27-Mar-2018] -- END

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Allow_Void_Rehire; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return StrMsg
    End Function
    'S.SANDEEP [21-Mar-2018] -- END


    'Pinkal (18-Aug-2018) -- Start
    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

    Private Function SetEnableDisableUserInAD(ByVal objDataOperation As clsDataOperation, ByVal xEmployeeID As Integer, ByVal xCurrentDate As Date) As Boolean
        Dim strQ As String = ""
        Dim mdtDate As DateTime = Nothing
        Dim mstrDisplayName As String = ""
        Dim mstrADProperty As String = ""
        Dim exForce As Exception = Nothing
        Try

            Dim mblnEnable As Boolean = True
            Dim lstEmpDates As New List(Of DateTime)
            Dim dtRehireDate As DateTime = Nothing


            Dim objEmpMst As New clsEmployee_Master
            objEmpMst._DataOperation = objDataOperation
            objEmpMst._Employeeunkid(xCurrentDate.Date) = xEmployeeID

            If objEmpMst._Empl_Enddate <> Nothing AndAlso IsDBNull(objEmpMst._Empl_Enddate) = False Then  'EOC DATE
                lstEmpDates.Add(objEmpMst._Empl_Enddate.Date)
            End If

            If objEmpMst._Termination_From_Date <> Nothing AndAlso IsDBNull(objEmpMst._Termination_From_Date) = False Then  'LEAVING DATE
                lstEmpDates.Add(objEmpMst._Termination_From_Date.Date)
            End If

            If objEmpMst._Termination_To_Date <> Nothing AndAlso IsDBNull(objEmpMst._Termination_To_Date) = False Then  'RETIREMENT DATE
                lstEmpDates.Add(objEmpMst._Termination_To_Date.Date)
            End If

            If IsDBNull(objEmpMst._Reinstatementdate.Date) = False Then 'REHIRE DATE
                dtRehireDate = objEmpMst._Reinstatementdate.Date
            End If

            mstrDisplayName = objEmpMst._Displayname

            objEmpMst = Nothing

            Dim dtTerminationDate As DateTime = Nothing
            If lstEmpDates IsNot Nothing AndAlso lstEmpDates.Count > 0 Then
                dtTerminationDate = lstEmpDates.Min()

                If dtTerminationDate <> Nothing AndAlso dtTerminationDate.Date <= xCurrentDate.Date Then
                    mblnEnable = False
                End If

                If dtRehireDate <> Nothing AndAlso dtRehireDate.Date <= xCurrentDate.Date Then
                    mblnEnable = True
                End If

            Else
                If dtRehireDate <> Nothing AndAlso dtRehireDate.Date <= xCurrentDate.Date Then
                    mblnEnable = True
                End If
            End If

            Dim mstrADIPAddress As String = ""
            Dim mstrADDomain As String = ""
            Dim mstrADUserName As String = ""
            Dim mstrADUserPwd As String = ""
            GetADConnection(mstrADIPAddress, mstrADDomain, mstrADUserName, mstrADUserPwd, objDataOperation)
            EnableDisableActiveDirectoryUser(mblnEnable, mstrDisplayName, mstrADIPAddress, mstrADDomain, mstrADUserName, mstrADUserPwd)

        Catch ex As Exception
            Throw New Exception(mstrModuleName & "SetEnableDisableUserInAD:- " & ex.Message)
        End Try
        Return True
    End Function
    'Pinkal (18-Aug-2018) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, transfer information is already present for the selected effective date.")
            Language.setMessage(mstrModuleName, 2, "Sorry, transfer information is already present for the selected reinstatement date.")
            Language.setMessage(mstrModuleName, 3, "WEB")
            Language.setMessage(mstrModuleName, 4, "Sorry, you cannot delete this rehired information. Reason: Payroll is already processed for this employee.")
            Language.setMessage(mstrModuleName, 5, "Sorry, you cannot delete this rehired information. Reason: Employee does not have any other movement information present whose effective date is less than the reinstatement date.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class