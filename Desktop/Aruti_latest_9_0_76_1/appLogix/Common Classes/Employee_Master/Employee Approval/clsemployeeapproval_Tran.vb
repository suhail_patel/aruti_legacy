﻿'************************************************************************************************************************************
'Class Name : clsemployeeapproval_Tran.vb
'Purpose    :
'Date       :30-Jun-2018
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Xml
Imports System.Net
Imports System.IO

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsemployeeapproval_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsemployeeapproval_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mstrTranguid As String = String.Empty
    Private mintEmployeeunkid As Integer
    Private mdtTransactiondate As Date
    Private mintMappingunkid As Integer
    Private mintStatusunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mblnIsfinal As Boolean
    Private mblnIsnotified As Boolean
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintVoiduserunkid As Integer
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer
    Private mstrIp As String = String.Empty
    Private mstrHostname As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranguid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Tranguid() As String
        Get
            Return mstrTranguid
        End Get
        Set(ByVal value As String)
            mstrTranguid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Isnotified
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isnotified() As Boolean
        Get
            Return mblnIsnotified
        End Get
        Set(ByVal value As Boolean)
            mblnIsnotified = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hostname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Hostname() As String
        Get
            Return mstrHostname
        End Get
        Set(ByVal value As String)
            mstrHostname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If


        Try
            strQ = "SELECT " & _
                      "  tranguid " & _
                      ", employeeunkid " & _
                      ", transactiondate " & _
                      ", mappingunkid " & _
                      ", statusunkid " & _
                      ", remark " & _
                      ", isfinal " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", voiduserunkid " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", ip " & _
                      ", hostname " & _
                      ", form_name " & _
                      ", isweb " & _
                     "FROM hremployeeapproval_tran " & _
                     "WHERE tranguid = @tranguid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mstrTranguid = dtRow.Item("tranguid").ToString
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtTransactiondate = dtRow.Item("transactiondate")
                mintMappingunkid = CInt(dtRow.Item("mappingunkid"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mstrRemark = dtRow.Item("remark").ToString
                mblnIsfinal = CBool(dtRow.Item("isfinal"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))

                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintAudittype = CInt(dtRow.Item("audittype"))
                mintAudituserunkid = CInt(dtRow.Item("audituserunkid"))
                mstrIp = dtRow.Item("ip").ToString
                mstrHostname = dtRow.Item("hostname").ToString
                mstrForm_Name = dtRow.Item("form_name").ToString
                mblnIsweb = CBool(dtRow.Item("isweb"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal iEmployeeID As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = " SELECT " & _
                      "  tranguid " & _
                      ", employeeunkid " & _
                      ", transactiondate " & _
                      ", mappingunkid " & _
                      ", statusunkid " & _
                      ", remark " & _
                      ", isfinal " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", voiduserunkid " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", ip " & _
                      ", hostname " & _
                      ", form_name " & _
                      ", isweb " & _
                      " FROM hremployeeapproval_tran "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid  = 0 "
            End If

            If iEmployeeID > 0 Then
                strQ &= " AND hremployeeapproval_tran.employeeunkid = @employeeunkid"
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeID)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

   
    'Public Function Insert(ByVal mdtTable As DataTable, ByVal strDatabaseName As String _
    '                                     , ByVal intCompanyId As Integer _
    '                                     , ByVal intYearId As Integer _
    '                                     , ByVal blnSubmitApproval As Boolean _
    '                                     , ByVal strUserAccessMode As String _
    '                                     , ByVal intPrivilgeId As Integer _
    '                                     , Optional ByVal objDataOpr As clsDataOperation = Nothing _
    '                                     , Optional ByVal strFormName As String = "" _
    '                                     , Optional ByVal eMode As enLogin_Mode = enLogin_Mode.DESKTOP _
    '                                     , Optional ByVal intLoginUserId As Integer = 0 _
    '                                     , Optional ByVal strSenderAddress As String = "" _
    '                                     , Optional ByVal ArutiSelfServiceURL As String = "") As Boolean
    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployeeapproval_tran) </purpose>
    Public Function Insert(ByVal mdtTable As DataTable, _
                           ByVal xDatabaseName As String, _
                           ByVal xYearId As Integer, _
                           ByVal xCompanyunkid As Integer, _
                           ByVal dtTotal_Active_Employee_AsOnDate As Date, _
                           ByVal blnIsArutiDemo As String, _
                           ByVal intTotal_Active_Employee_ForAllCompany As Integer, _
                           ByVal intNoOfEmployees As Integer, _
                           ByVal intUserUnkId As Integer, _
                           ByVal blnDonotAttendanceinSeconds As Boolean, _
                           ByVal xUserModeSetting As String, _
                           ByVal xPeriodStart As DateTime, _
                           ByVal xPeriodEnd As DateTime, _
                           ByVal xOnlyApproved As Boolean, _
                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                           ByVal blnAllowToApproveEarningDeduction As Boolean, _
                           ByVal dtCurrentDateTime As DateTime, _
                           ByVal mblnCreateADUserFromEmp As Boolean, _
                           ByVal mblnUserMustChangePwdOnNextLogOn As Boolean, _
                           ByVal blnSendDetailToEmployee As Boolean, _
                           ByVal blnSMSCompanyDetailToNewEmp As Boolean, _
                           Optional ByVal xHostName As String = "", _
                           Optional ByVal xIPAddr As String = "", _
                           Optional ByVal xLoggedUserName As String = "", _
                           Optional ByVal xLoginMod As enLogin_Mode = enLogin_Mode.DESKTOP, _
                           Optional ByVal blnIsFlexcubeIntergrated As Boolean = False, _
                           Optional ByVal mdicFlexcubeServiceCollection As Dictionary(Of Integer, String) = Nothing, _
                           Optional ByVal strAccountCategory As String = "", _
                           Optional ByVal strAccountClass As String = "", _
                           Optional ByVal strSenderAddress As String = "", _
                           Optional ByVal strSMSGatewayEmail As String = "", _
                           Optional ByVal intSMSGatewayEmailType As Integer = 0 _
                           ) As Boolean
        'Sohail (27 Nov 2018) - [blnSendDetailToEmployee, blnSMSCompanyDetailToNewEmp, strSenderAddress]
       'Pinkal (18-Aug-2018) -- 'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273]. [ByVal mblnCreateADUserFromEmp As Boolean]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objEmp As New clsEmployee_Master
        If mdtTable Is Nothing Then Return False

        Dim objDataOperation As New clsDataOperation

        'Pinkal (04-Apr-2020) -- Start
        'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
        objDataOperation.BindTransaction()
        'Pinkal (04-Apr-2020) -- End

        Try

            strQ = "INSERT INTO hremployeeapproval_tran ( " & _
                   "  tranguid " & _
                   ", employeeunkid " & _
                   ", transactiondate " & _
                   ", mappingunkid " & _
                   ", statusunkid " & _
                   ", remark " & _
                   ", isfinal " & _
                   ", isnotified " & _
                   ", isvoid " & _
                   ", voiddatetime " & _
                   ", voidreason " & _
                   ", voiduserunkid " & _
                   ", audittype " & _
                   ", audituserunkid " & _
                   ", ip " & _
                   ", hostname " & _
                   ", form_name " & _
                   ", isweb" & _
                 ") VALUES (" & _
                   "  @tranguid " & _
                   ", @employeeunkid " & _
                   ", @transactiondate " & _
                   ", @mappingunkid " & _
                   ", @statusunkid " & _
                   ", @remark " & _
                   ", @isfinal " & _
                   ", @isnotified " & _
                   ", @isvoid " & _
                   ", @voiddatetime " & _
                   ", @voidreason " & _
                   ", @voiduserunkid " & _
                   ", @audittype " & _
                   ", @audituserunkid " & _
                   ", @ip " & _
                   ", @hostname " & _
                   ", @form_name " & _
                   ", @isweb" & _
                 "); "


            For Each dr As DataRow In mdtTable.Rows

                'Gajanan [9-July-2019] -- Start      
                'If isExist(CInt(dr("employeeunkid")), mintStatusunkid, "", objDataOperation) = False Then
                If isExist(CInt(dr("employeeunkid")), mintStatusunkid, "", objDataOperation, mintMappingunkid) = False Then
                    'Gajanan [7-July-2019] -- End
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
                    objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
                    objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
                    objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
                    objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
                    objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(dr("isfinal")).ToString)
                    If mintStatusunkid = clsEmployee_Master.EmpApprovalStatus.Approved OrElse mintStatusunkid = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                        objDataOperation.AddParameter("@isnotified", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                    Else
                        objDataOperation.AddParameter("@isnotified", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsnotified.ToString)
                    End If
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime = Nothing, DBNull.Value, mdtVoiddatetime))
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
                    objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
                    objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
                    objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
                    objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
                    objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)

                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If CBool(dr("isfinal")) Then
                        If mintStatusunkid = clsEmployee_Master.EmpApprovalStatus.Approved Then


                            'Pinkal (10-Jun-2020) -- Start
                            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
                            Dim objConfiguation As New clsConfigOptions
                            Dim mblnImgInDb As Boolean = objConfiguation.GetKeyValue(xCompanyunkid, "IsImgInDataBase", objDataOperation)
                            objEmp._blnImgInDb = mblnImgInDb
                            objConfiguation = Nothing
                            'Pinkal (10-Jun-2020) -- End


                            objEmp._DataOperation = objDataOperation
                            objEmp._Employeeunkid(xPeriodEnd) = CInt(dr("employeeunkid"))
                            objEmp._Isapproved = True

                            'Pinkal (18-Aug-2018) -- Start
                            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

                            'If objEmp.Update(xDatabaseName, _
                            '   xYearId, _
                            '   xCompanyunkid, _
                            '   dtTotal_Active_Employee_AsOnDate, _
                            '   blnIsArutiDemo, _
                            '   intTotal_Active_Employee_ForAllCompany, _
                            '   intNoOfEmployees, _
                            '   intUserUnkId, _
                            '   blnDonotAttendanceinSeconds, _
                            '   xUserModeSetting, xPeriodStart, _
                            '   xPeriodEnd, _
                            '   xOnlyApproved, _
                            '   xIncludeIn_ActiveEmployee, _
                            '   blnAllowToApproveEarningDeduction, _
                            '   dtCurrentDateTime, , , , , , , , , objDataOperation, , , , xHostName, xIPAddr, xLoggedUserName, xLoginMod) = False Then

                            If objEmp.Update(xDatabaseName, _
                               xYearId, _
                               xCompanyunkid, _
                               dtTotal_Active_Employee_AsOnDate, _
                               blnIsArutiDemo, _
                               intTotal_Active_Employee_ForAllCompany, _
                               intNoOfEmployees, _
                               intUserUnkId, _
                               blnDonotAttendanceinSeconds, _
                               xUserModeSetting, xPeriodStart, _
                               xPeriodEnd, _
                               xOnlyApproved, _
                               xIncludeIn_ActiveEmployee, _
                               blnAllowToApproveEarningDeduction, _
                               dtCurrentDateTime, mblnCreateADUserFromEmp, mblnUserMustChangePwdOnNextLogOn, , , , , , , , , objDataOperation, , , , xHostName, xIPAddr, xLoggedUserName, xLoginMod) = False Then
                              'Pinkal (18-Aug-2018) -- End

                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            'Sohail (27 Nov 2018) -- Start
                            'NMB Issue - Email is sent to employee everytime whenver employee is updated and send company details to newly approved employee option is ticked in 75.1.
                            If blnSendDetailToEmployee = True OrElse blnSMSCompanyDetailToNewEmp = True Then
                                Dim StrMessage As String = Set_Notification_Employee_EmailPassword(objEmp._Firstname, objEmp._Surname, objEmp._Email, objEmp._Password)

                                Dim objSendMail As New clsSendMail
                                objSendMail._Message = StrMessage
                                objSendMail._Form_Name = mstrForm_Name
                                objSendMail._LogEmployeeUnkid = -1
                                objSendMail._OperationModeId = xLoginMod
                                objSendMail._UserUnkid = intUserUnkId
                                objSendMail._SenderAddress = strSenderAddress
                                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT

                                If blnSendDetailToEmployee = True AndAlso objEmp._Email.Trim <> "" Then
                                    objSendMail._Subject = Language.getMessage(mstrModuleName, 103, "Notification Email & Password")
                                    objSendMail._ToEmail = objEmp._Email
                                    objSendMail.SendMail(xCompanyunkid)
                                End If
                                If blnSMSCompanyDetailToNewEmp = True Then
                                    If intSMSGatewayEmailType = clsEmployee_Master.EmpColEnum.Col_Present_Mobile Then
                                        objSendMail._Subject = objEmp._Present_Mobile
                                    ElseIf intSMSGatewayEmailType = clsEmployee_Master.EmpColEnum.Col_Domicile_Mobile Then
                                        objSendMail._Subject = objEmp._Domicile_Mobile
                                    Else
                                        objSendMail._Subject = ""
                                    End If
                                    If objSendMail._Subject.Trim <> "" Then
                                        objSendMail._ToEmail = strSMSGatewayEmail
                                        objSendMail.SendMail(xCompanyunkid)
                                    End If
                                End If
                            End If
                            'Sohail (27 Nov 2018) -- End

                            'S.SANDEEP [26-SEP-2018] -- START
                            If blnIsFlexcubeIntergrated Then
                                Dim strxmlData As String = String.Empty
                                Dim strMessageId As String = String.Empty
                                Dim oResponseData As String = String.Empty
                                Dim strErrorDesc As String = String.Empty
                                Dim strCustNo As String = String.Empty
                                Dim blnerror As Boolean = False
                                Dim strFileName As String = String.Empty
                                Dim strPrefix As String = "CUST_"
                                'S.SANDEEP |25-NOV-2019| -- START
                                'ISSUE/ENHANCEMENT : FlexCube
                                Dim strFailedEmailNotification As String = ""
                                Dim objConfig As New clsConfigOptions
                                strFailedEmailNotification = objConfig.GetKeyValue(xCompanyunkid, "FailedRequestNotificationEmails", objDataOperation)
                                objConfig = Nothing
                                'S.SANDEEP |25-NOV-2019| -- END
                                strxmlData = objEmp.GenerateFileString(1, strAccountCategory, strAccountClass, CInt(dr("employeeunkid")).ToString, xPeriodEnd, "", strMessageId)
                                If strxmlData.Trim.Length > 0 Then
                                    oResponseData = PostData(strxmlData, mdicFlexcubeServiceCollection(enFlexcubeServiceInfo.FLX_CUSTOMER_SRV), strErrorDesc)
                                    If oResponseData.Trim().Length > 0 Then
                                        Dim ds As New DataSet()
                                        ds.ReadXml(New System.IO.StringReader(oResponseData))

                                        'S.SANDEEP |25-NOV-2019| -- START
                                        'ISSUE/ENHANCEMENT : FlexCube
                                        'If ds.Tables("FCUBS_HEADER").Rows.Count > 0 Then
                                        '    If ds.Tables("FCUBS_HEADER").Rows(0)("MSGSTAT").ToString() <> "SUCCESS" Then
                                        '        If ds.Tables.Contains("ERROR") = True Then
                                        '            blnerror = True
                                        '            If ds.Tables("ERROR").Rows.Count > 0 Then
                                        '                strErrorDesc = String.Join(",", ds.Tables("ERROR").AsEnumerable().Select(Function(x) x.Field(Of String)("ECODE").ToString() & " -> " & x.Field(Of String)("EDESC")).ToArray())
                                        '            End If
                                        '        End If
                                        '    Else
                                        '        If ds.Tables.Contains("Customer-Full") = True Then
                                        '            If ds.Tables("Customer-Full").Rows.Count > 0 Then
                                        '                strCustNo = ds.Tables("Customer-Full").Rows(0)("CUSTNO").ToString()
                                        '            End If
                                        '        End If
                                        '    End If
                                        'End If
                                        If ds.Tables.Contains("FCUBS_HEADER") = True Then
                                        If ds.Tables("FCUBS_HEADER").Rows.Count > 0 Then
                                            If ds.Tables("FCUBS_HEADER").Rows(0)("MSGSTAT").ToString() <> "SUCCESS" Then
                                                If ds.Tables.Contains("ERROR") = True Then
                                                    blnerror = True
                                                    If ds.Tables("ERROR").Rows.Count > 0 Then
                                                        strErrorDesc = String.Join(",", ds.Tables("ERROR").AsEnumerable().Select(Function(x) x.Field(Of String)("ECODE").ToString() & " -> " & x.Field(Of String)("EDESC")).ToArray())
                                                    End If
                                                End If
                                            Else
                                                If ds.Tables.Contains("Customer-Full") = True Then
                                                    If ds.Tables("Customer-Full").Rows.Count > 0 Then
                                                        strCustNo = ds.Tables("Customer-Full").Rows(0)("CUSTNO").ToString()
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                        'S.SANDEEP |25-NOV-2019| -- END
                                    End If

                                    If strErrorDesc.Trim().Length > 0 Then
                                        blnerror = True
                                    End If

                                    strFileName = strPrefix & objEmp._Employeecode.ToString() & "_" & Date.Now.ToString("yyyymmdd") & ".xml"

                                    'S.SANDEEP |08-APR-2019| -- START
                                    'If objEmp.InsertFlexcubeRequest(CInt(dr("employeeunkid")), 1, 1, strFileName, strxmlData, strCustNo, strMessageId, oResponseData, blnerror, strErrorDesc, objDataOperation) = False Then

                                    'S.SANDEEP |25-NOV-2019| -- START
                                    'ISSUE/ENHANCEMENT : FlexCube
                                    'If objEmp.InsertFlexcubeRequest(CInt(dr("employeeunkid")), enRequestType.ADD, enFiletype.CUSTOMER, strFileName, strxmlData, strCustNo, strMessageId, oResponseData, blnerror, strErrorDesc, enRequestForm.EMPLOYEE, objEmp._Employeecode, objDataOperation) = False Then
                                    If strErrorDesc.Trim().Length > 0 Then
                                        If strFailedEmailNotification.Trim.Length > 0 Then
                                            Dim mDicError As New Dictionary(Of String, String)
                                            mDicError.Add("REQUEST TYPE", "Customer")
                                            mDicError.Add("MSGID/CORRELID", strMessageId)
                                            mDicError.Add("REQUESTDATE", Now)
                                            mDicError.Add("ERROR", strErrorDesc)
                                            Call SendFlex_Notification(strFailedEmailNotification, mDicError, xCompanyunkid, "Customer Creation Failed Request [" + Now + "]", intUserUnkId, 0)
                                        End If
                                    End If
                                    If objEmp.InsertFlexcubeRequest(CInt(dr("employeeunkid")), enRequestType.ADD, enFiletype.CUSTOMER, strFileName, strxmlData, strCustNo, strMessageId, oResponseData, blnerror, strErrorDesc, enRequestForm.EMPLOYEE, objEmp._Employeecode, "", IIf(xIPAddr.Trim.Length <= 0, getIP(), xIPAddr), objDataOperation) = False Then
                                        'S.SANDEEP |25-NOV-2019| -- END

                                        'S.SANDEEP |08-APR-2019| -- END

                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    If blnerror = False Then
                                        strPrefix = "ACCT_"
                                        oResponseData = ""
                                        strFileName = ""
                                        blnerror = False
                                        strErrorDesc = ""
                                        strxmlData = objEmp.GenerateFileString(2, strAccountCategory, strAccountClass, CInt(dr("employeeunkid")).ToString, xPeriodEnd, strCustNo, strMessageId)
                                        If strxmlData.Trim.Length > 0 Then
                                            oResponseData = PostData(strxmlData, mdicFlexcubeServiceCollection(enFlexcubeServiceInfo.FLX_ACCOUNT_SRV), strErrorDesc)
                                            If oResponseData.Trim().Length > 0 Then
                                                Dim ds As New DataSet()
                                                ds.ReadXml(New System.IO.StringReader(oResponseData))
                                                'If ds.Tables("FCUBS_HEADER").Rows.Count > 0 Then
                                                '    If ds.Tables("FCUBS_HEADER").Rows(0)("MSGSTAT").ToString() <> "SUCCESS" Then
                                                '        If ds.Tables.Contains("ERROR") = True Then
                                                '            blnerror = True
                                                '            If ds.Tables("ERROR").Rows.Count > 0 Then
                                                '                strErrorDesc = String.Join(",", ds.Tables("ERROR").AsEnumerable().Select(Function(x) x.Field(Of String)("ECODE").ToString() & " -> " & x.Field(Of String)("EDESC")).ToArray())
                                                '            End If
                                                '        End If
                                                '    Else
                                                '        If ds.Tables.Contains("Customer-Full") = True Then
                                                '            If ds.Tables("Customer-Full").Rows.Count > 0 Then
                                                '                strCustNo = ds.Tables("Customer-Full").Rows(0)("CUSTNO").ToString()
                                                '            End If
                                                '        End If
                                                '    End If
                                                'End If
                                                If ds.Tables("ERROR").Rows.Count > 0 Then
                                                If ds.Tables("FCUBS_HEADER").Rows.Count > 0 Then
                                                    If ds.Tables("FCUBS_HEADER").Rows(0)("MSGSTAT").ToString() <> "SUCCESS" Then
                                                        If ds.Tables.Contains("ERROR") = True Then
                                                            blnerror = True
                                                            If ds.Tables("ERROR").Rows.Count > 0 Then
                                                                strErrorDesc = String.Join(",", ds.Tables("ERROR").AsEnumerable().Select(Function(x) x.Field(Of String)("ECODE").ToString() & " -> " & x.Field(Of String)("EDESC")).ToArray())
                                                            End If
                                                        End If
                                                    Else
                                                        If ds.Tables.Contains("Customer-Full") = True Then
                                                            If ds.Tables("Customer-Full").Rows.Count > 0 Then
                                                                strCustNo = ds.Tables("Customer-Full").Rows(0)("CUSTNO").ToString()
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                            End If
                                            If strErrorDesc.Trim().Length > 0 Then
                                                blnerror = True
                                            End If
                                            strFileName = strPrefix & objEmp._Employeecode.ToString() & "_" & Date.Now.ToString("yyyymmdd") & ".xml"
                                            'S.SANDEEP |08-APR-2019| -- START
                                            'If objEmp.InsertFlexcubeRequest(CInt(dr("employeeunkid")), 1, 2, strFileName, strxmlData, strCustNo, strMessageId, oResponseData, blnerror, strErrorDesc, objDataOperation) = False Then
                                            'S.SANDEEP |25-NOV-2019| -- START
                                            'ISSUE/ENHANCEMENT : FlexCube
                                            'If objEmp.InsertFlexcubeRequest(CInt(dr("employeeunkid")), enRequestType.ADD, enFiletype.ACCOUNT, strFileName, strxmlData, strCustNo, strMessageId, oResponseData, blnerror, strErrorDesc, enRequestForm.EMPLOYEE, objEmp._Employeecode, objDataOperation) = False Then

                                            If strErrorDesc.Trim().Length > 0 Then
                                                If strFailedEmailNotification.Trim.Length > 0 Then
                                                    Dim mDicError As New Dictionary(Of String, String)
                                                    mDicError.Add("REQUEST TYPE", "Customer")
                                                    mDicError.Add("MSGID/CORRELID", strMessageId)
                                                    mDicError.Add("REQUESTDATE", Now)
                                                    mDicError.Add("ERROR", strErrorDesc)
                                                    Call SendFlex_Notification(strFailedEmailNotification, mDicError, xCompanyunkid, "Customer Creation Failed Request [" + Now + "]", intUserUnkId, 0)
                                                End If
                                            End If

                                            If objEmp.InsertFlexcubeRequest(CInt(dr("employeeunkid")), enRequestType.ADD, enFiletype.ACCOUNT, strFileName, strxmlData, strCustNo, strMessageId, oResponseData, blnerror, strErrorDesc, enRequestForm.EMPLOYEE, objEmp._Employeecode, "", IIf(xIPAddr.Trim.Length <= 0, getIP(), xIPAddr), objDataOperation) = False Then
                                                'S.SANDEEP |25-NOV-2019| -- END

                                                'S.SANDEEP |08-APR-2019| -- END
                                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                            'S.SANDEEP [26-SEP-2018] -- END
                        End If
                    End If
                End If
            Next

            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (04-Apr-2020) -- End

            Return True
        Catch ex As Exception
            'Pinkal (04-Apr-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            objDataOperation.ReleaseTransaction(False)

            If ex.Message.ToString.Contains("Sorry, record cannot be saved. Some allocation does not exist in active directory.") Then
                mstrMessage = ex.Message
            ElseIf ex.Message.ToString().Contains("System cannot connect with Active Directory at the moment. Please check Active Directory connection.  Also username or password (Aruti to AD Server) may be incorrect.") Then
                mstrMessage = ex.Message
            Else
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            End If
            'Pinkal (04-Apr-2020) -- End

            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objEmp = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployeeapproval_tran) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)

            strQ = "UPDATE hremployeeapproval_tran SET " & _
              "  employeeunkid = @employeeunkid" & _
              ", transactiondate = @transactiondate" & _
              ", mappingunkid = @mappingunkid" & _
              ", statusunkid = @statusunkid" & _
              ", remark = @remark" & _
              ", isfinal = @isfinal" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", audittype = @audittype" & _
              ", audituserunkid = @audituserunkid" & _
              ", ip = @ip" & _
              ", hostname = @hostname" & _
              ", form_name = @form_name" & _
              ", isweb = @isweb " & _
            "WHERE tranguid = @tranguid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployeeapproval_tran) </purpose>
    Public Function Delete(ByVal strtranguid As String, ByVal intuserunkid As Integer, ByVal intempid As Integer, ByVal strreasion As String) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "hremployeeapproval_tran SET " & _
                   "isvoid = 1,voiddatetime = getdate(),voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                   "WHERE tranguid = @tranguid AND employeeunkid = @employeeunkid "

            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strtranguid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strreasion)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, intuserunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intempid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@tranguid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 


    'Gajanan [9-July-2019] -- Start      


    'Public Function isExist(ByVal intEmpID As Integer, ByVal intStatusId As Integer, Optional ByVal intUnkid As String = "", Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
    Public Function isExist(ByVal intEmpID As Integer, ByVal intStatusId As Integer, Optional ByVal intUnkid As String = "", Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal xMappingunkid As Integer = 0) As Boolean
        'Gajanan [9-July-2019] -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            'strQ = " SELECT " & _
            '          "  tranguid " & _
            '          ", employeeunkid " & _
            '          ", transactiondate " & _
            '          ", mappingunkid " & _
            '          ", statusunkid " & _
            '          ", remark " & _
            '          ", isfinal " & _
            '          ", isvoid " & _
            '          ", voiddatetime " & _
            '          ", voidreason " & _
            '          ", voiduserunkid " & _
            '          ", audittype " & _
            '          ", audituserunkid " & _
            '          ", ip " & _
            '          ", hostname " & _
            '          ", form_name " & _
            '          ", isweb " & _
            '         " FROM hremployeeapproval_tran " & _
            '         " WHERE isvoid = 0 "


            'Gajanan [9-July-2019] -- Start      


            'strQ = "SELECT " & _
            '       "     A.employeeunkid " & _
            '       "    ,A.statusunkid " & _
            '       "    ,A.tranguid " & _
            '       "FROM " & _
            '       "( " & _
            '       "    SELECT " & _
            '       "         ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY transactiondate DESC) AS rno " & _
            '       "        ,employeeunkid " & _
            '       "        ,statusunkid " & _
            '       "        ,tranguid " & _
            '       "    FROM hremployeeapproval_tran " & _
            '       "    WHERE isvoid = 0 " & _
            '       ") AS A WHERE A.rno = 1 AND employeeunkid = @employeeunkid AND statusunkid = @statusunkid "


            strQ = "SELECT " & _
                   "     A.employeeunkid " & _
                   "    ,A.statusunkid " & _
                   "    ,A.tranguid " & _
                   "    ,A.mappingunkid " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY transactiondate DESC) AS rno " & _
                   "        ,employeeunkid " & _
                   "        ,statusunkid " & _
                   "        ,tranguid " & _
                   "        ,mappingunkid " & _
                   "    FROM hremployeeapproval_tran " & _
                   "    WHERE isvoid = 0 " & _
                   ") AS A WHERE A.rno = 1 AND employeeunkid = @employeeunkid AND statusunkid = @statusunkid "

            'Gajanan [9-July-2019] -- End

            If intUnkid <> "" Then
                strQ &= " AND tranguid <> @tranguid"
            End If


            'Gajanan [9-July-2019] -- Start      
            If xMappingunkid > 0 Then
                strQ &= " AND mappingunkid = @mappingunkid"
                objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xMappingunkid)
            End If
            'Gajanan [9-July-2019] -- End



            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusId)
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Sub GetUACFilters(ByVal strDatabaseName As String, ByVal strUserAccessMode As String, ByRef strFilter As String, ByRef strJoin As String, ByRef strSelect As String, ByRef strAccessJoin As String, ByRef strOuterJoin As String, ByRef strFinalString As String, ByVal intUserId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim objDataOperation As New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim StrQ As String = ""
        Try
            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
            Dim strvalues() As String = strUserAccessMode.Split(",")
            For index As Integer = 0 To strvalues.Length - 1
                Select Case strvalues(index)
                    Case enAllocation.BRANCH
                        strSelect &= ",ISNULL(BR.branchid,0) AS branchid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as branchid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.BRANCH) & "' " & _
                                         "    ) AS BR ON BR.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(BR.branchid,0) > 0 "
                        strJoin &= "AND Fn.branchid = [@emp].branchid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrstation_master ON Fn.branchid = hrstation_master.stationunkid "

                    Case enAllocation.DEPARTMENT_GROUP
                        strSelect &= ",ISNULL(DG.deptgrpid,0) AS deptgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as deptgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT_GROUP) & "' " & _
                                         "    ) AS DG ON DG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(DG.deptgrpid,0) > 0 "
                        strJoin &= "AND Fn.deptgrpid = [@emp].deptgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_group_master ON Fn.deptgrpid = hrdepartment_group_master.deptgroupunkid "

                    Case enAllocation.DEPARTMENT
                        strSelect &= ",ISNULL(DP.deptid,0) AS deptid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as deptid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT) & "' " & _
                                         "    ) AS DP ON DP.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(DP.deptid,0) > 0 "
                        strJoin &= "AND Fn.deptid = [@emp].deptid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_master ON Fn.deptid = hrdepartment_master.departmentunkid "

                    Case enAllocation.SECTION_GROUP
                        strSelect &= ",ISNULL(SG.secgrpid,0) AS secgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as secgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION_GROUP) & "' " & _
                                         "    ) AS SG ON SG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(SG.secgrpid,0) > 0 "
                        strJoin &= "AND Fn.secgrpid = [@emp].secgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsectiongroup_master ON Fn.secgrpid = hrsectiongroup_master.sectiongroupunkid "

                    Case enAllocation.SECTION
                        strSelect &= ",ISNULL(SC.secid,0) AS secid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as secid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION) & "' " & _
                                         "    ) AS SC ON SC.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(SC.secid,0) > 0 "
                        strJoin &= "AND Fn.secid = [@emp].secid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsection_master ON Fn.secid = hrsection_master.sectionunkid "

                    Case enAllocation.UNIT_GROUP
                        strSelect &= ",ISNULL(UG.unitgrpid,0) AS unitgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as unitgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT_GROUP) & "' " & _
                                         "    ) AS UG ON UG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(UG.unitgrpid,0) > 0 "
                        strJoin &= "AND Fn.unitgrpid = [@emp].unitgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunitgroup_master ON Fn.unitgrpid = hrunitgroup_master.unitgroupunkid "

                    Case enAllocation.UNIT
                        strSelect &= ",ISNULL(UT.unitid,0) AS unitid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as unitid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT) & "' " & _
                                         "    ) AS UT ON UT.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(UT.unitid,0) > 0 "
                        strJoin &= "AND Fn.unitid = [@emp].unitid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunit_master ON Fn.unitid = hrunit_master.unitunkid "

                    Case enAllocation.TEAM
                        strSelect &= ",ISNULL(TM.teamid,0) AS teamid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as teamid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.TEAM) & "' " & _
                                         "    ) AS TM ON TM.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(TM.teamid,0) > 0 "
                        strJoin &= "AND Fn.teamid = [@emp].teamid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrteam_master ON Fn.teamid = hrteam_master.teamunkid "

                    Case enAllocation.JOB_GROUP
                        strSelect &= " ,ISNULL(JG.jgrpid,0) AS jgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as jgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOB_GROUP) & "' " & _
                                         "    ) AS JG ON JG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(JG.jgrpid,0) > 0 "
                        strJoin &= "AND Fn.jgrpid = [@emp].jgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjobgroup_master ON Fn.jgrpid = hrjobgroup_master.jobgroupunkid "

                    Case enAllocation.JOBS
                        strSelect &= ",ISNULL(JB.jobid,0) AS jobid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as jobid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOBS) & "' " & _
                                         "    ) AS JB ON JB.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(JB.jobid,0) > 0 "
                        strJoin &= "AND Fn.jobid = [@emp].jobid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjob_master ON Fn.jobid = hrjob_master.jobunkid "

                    Case enAllocation.CLASS_GROUP
                        strSelect &= ",ISNULL(CG.clsgrpid,0) AS clsgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as clsgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASS_GROUP) & "' " & _
                                         "    ) AS CG ON CG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(CG.clsgrpid,0) > 0 "
                        strJoin &= "AND Fn.clsgrpid = [@emp].clsgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master ON Fn.clsgrpid = hrclassgroup_master.classgroupunkid "

                    Case enAllocation.CLASSES
                        strSelect &= ",ISNULL(CL.clsid,0) AS clsid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as clsid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASSES) & "' " & _
                                         "    ) AS CL ON CL.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(CL.clsid,0) > 0 "
                        strJoin &= "AND Fn.clsid = [@emp].clsid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclasses_master ON Fn.clsid = hrclasses_master.classesunkid "

                End Select

                objDataOperation.ClearParameters()
                StrQ = "SELECT @Value = ISNULL(STUFF((SELECT DISTINCT ',' + CAST(allocationunkid AS NVARCHAR(MAX)) FROM hrmsConfiguration..cfuseraccess_privilege_tran WHERE useraccessprivilegeunkid IN (SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE userunkid = " & intUserId & " AND referenceunkid = " & strvalues(index) & ") FOR XML PATH('')),1,1,''),'') "
                Dim strvalue = ""
                objDataOperation.AddParameter("@Value", SqlDbType.NVarChar, 4000, strvalue, ParameterDirection.InputOutput)

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If

                strvalue = objDataOperation.GetParameterValue("@Value")

                If strvalue.Trim.Length > 0 Then
                    Select Case strvalues(index)
                        Case enAllocation.BRANCH
                            strFinalString &= " AND Fn.branchid IN (" & strvalue & ") "

                        Case enAllocation.DEPARTMENT_GROUP
                            strFinalString &= " AND Fn.deptgrpid IN (" & strvalue & ") "

                        Case enAllocation.DEPARTMENT
                            strFinalString &= " AND Fn.deptid IN (" & strvalue & ") "

                        Case enAllocation.SECTION_GROUP
                            strFinalString &= " AND Fn.secgrpid IN (" & strvalue & ") "

                        Case enAllocation.SECTION
                            strFinalString &= " AND Fn.secid IN (" & strvalue & ") "

                        Case enAllocation.UNIT_GROUP
                            strFinalString &= " AND Fn.unitgrpid IN (" & strvalue & ") "

                        Case enAllocation.UNIT
                            strFinalString &= " AND Fn.unitid IN (" & strvalue & ") "

                        Case enAllocation.TEAM
                            strFinalString &= " AND Fn.teamid IN (" & strvalue & ") "

                        Case enAllocation.JOB_GROUP
                            strFinalString &= " AND Fn.jgrpid IN (" & strvalue & ") "

                        Case enAllocation.JOBS
                            strFinalString &= " AND Fn.jobid IN (" & strvalue & ") "

                        Case enAllocation.CLASS_GROUP
                            strFinalString &= " AND Fn.clsgrpid IN (" & strvalue & ") "

                        Case enAllocation.CLASSES
                            strFinalString &= " AND Fn.clsid IN (" & strvalue & ") "

                    End Select
                End If
            Next
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
            If strJoin.Trim.Length > 0 Then strJoin = strJoin.Substring(3)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUACFilters; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP |16-APR-2020| -- START
    'ISSUE/ENHANCEMENT : SLOWNESS IN GETTING LIST OF EMPLOYEE DUE TO ALLOCATION USED WHERE HAVING 100+ VALUE
    Public Function GetEmployeeApprovalData(ByVal strDatabaseName As String _
                                          , ByVal intCompanyId As Integer _
                                          , ByVal intYearId As Integer _
                                          , ByVal strUserAccessMode As String _
                                          , ByVal intPrivilegeId As Integer _
                                          , ByVal intUserId As Integer _
                                          , ByVal intPriorityId As Integer _
                                          , ByVal blnOnlyMyApprovals As Boolean _
                                          , Optional ByVal objDataOpr As clsDataOperation = Nothing) As DataTable
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim oData As DataTable
        Dim objDataOperation As New clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try

            Dim strFilter As String = String.Empty
            Dim strJoin As String = String.Empty
            Dim strSelect As String = String.Empty
            Dim strAccessJoin As String = String.Empty
            Dim strOuterJoin As String = String.Empty
            Dim strFinalString As String = ""

            StrQ = "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
                   "DROP TABLE #USR "
            StrQ &= "SELECT " & _
                    "* " & _
                    "INTO #USR " & _
                    "FROM " & _
                    "( "

            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
            Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
            For index As Integer = 0 To strvalues.Length - 1
                Dim xStrJoinColName As String = ""
                Dim xIntAllocId As Integer = 0
                Select Case CInt(strvalues(index))
                    Case enAllocation.BRANCH
                        xStrJoinColName = "stationunkid"
                        xIntAllocId = CInt(enAllocation.BRANCH)
                    Case enAllocation.DEPARTMENT_GROUP
                        xStrJoinColName = "deptgroupunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT_GROUP)
                    Case enAllocation.DEPARTMENT
                        xStrJoinColName = "departmentunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT)
                    Case enAllocation.SECTION_GROUP
                        xStrJoinColName = "sectiongroupunkid"
                        xIntAllocId = CInt(enAllocation.SECTION_GROUP)
                    Case enAllocation.SECTION
                        xStrJoinColName = "sectionunkid"
                        xIntAllocId = CInt(enAllocation.SECTION)
                    Case enAllocation.UNIT_GROUP
                        xStrJoinColName = "unitgroupunkid"
                        xIntAllocId = CInt(enAllocation.UNIT_GROUP)
                    Case enAllocation.UNIT
                        xStrJoinColName = "unitunkid"
                        xIntAllocId = CInt(enAllocation.UNIT)
                    Case enAllocation.TEAM
                        xStrJoinColName = "teamunkid"
                        xIntAllocId = CInt(enAllocation.TEAM)
                    Case enAllocation.JOB_GROUP
                        xStrJoinColName = "jobgroupunkid"
                        xIntAllocId = CInt(enAllocation.JOB_GROUP)
                    Case enAllocation.JOBS
                        xStrJoinColName = "jobunkid"
                        xIntAllocId = CInt(enAllocation.JOBS)
                    Case enAllocation.CLASS_GROUP
                        xStrJoinColName = "classgroupunkid"
                        xIntAllocId = CInt(enAllocation.CLASS_GROUP)
                    Case enAllocation.CLASSES
                        xStrJoinColName = "classunkid"
                        xIntAllocId = CInt(enAllocation.CLASSES)
                End Select
                StrQ &= "SELECT " & _
                        "    B" & index.ToString() & ".userunkid " & _
                        "   ,A.employeeunkid " & _
                        "FROM " & _
                        "( " & _
                        "   SELECT " & _
                        "        AEM.employeeunkid " & _
                        "       ,ISNULL(AEM.departmentunkid, 0) AS departmentunkid " & _
                        "       ,ISNULL(AEM.jobunkid, 0) AS jobunkid " & _
                        "       ,ISNULL(AEM.classgroupunkid, 0) AS classgroupunkid " & _
                        "       ,ISNULL(AEM.classunkid, 0) AS classunkid " & _
                        "       ,ISNULL(AEM.stationunkid, 0) AS stationunkid " & _
                        "       ,ISNULL(AEM.deptgroupunkid, 0) AS deptgroupunkid " & _
                        "       ,ISNULL(AEM.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                        "       ,ISNULL(AEM.sectionunkid, 0) AS sectionunkid " & _
                        "       ,ISNULL(AEM.unitgroupunkid, 0) AS unitgroupunkid " & _
                        "       ,ISNULL(AEM.unitunkid, 0) AS unitunkid " & _
                        "       ,ISNULL(AEM.teamunkid, 0) AS teamunkid " & _
                        "       ,ISNULL(AEM.jobgroupunkid, 0) AS jobgroupunkid " & _
                        "   FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
                        "   JOIN " & strDatabaseName & "..hremployeeapproval_tran AS TAT ON TAT.employeeunkid = AEM.employeeunkid " & _
                        "   WHERE TAT.isvoid = 0 "
                StrQ &= ") AS A " & _
                        "JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        UPM.userunkid " & _
                        "       ,UPT.allocationunkid " & _
                        "   FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                        "       JOIN hremp_appusermapping AS CAM ON CAM.mapuserunkid = UPM.userunkid AND CAM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & "  " & _
                        "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                        "   WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid IN (" & xIntAllocId & ") " & _
                        "   AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
                        ") AS B" & index.ToString() & " ON A." & xStrJoinColName & " = B" & index.ToString() & ".allocationunkid "
                If index < strvalues.Length - 1 Then
                    StrQ &= " INTERSECT "
                End If
            Next

            StrQ &= ") AS Fl "

            StrQ &= "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
                   "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
                   "SELECT " & _
                    "     hremployee_master.employeeunkid " & _
                    "    ,hremployee_master.departmentunkid " & _
                    "    ,hremployee_master.jobunkid " & _
                    "    ,hremployee_master.classgroupunkid " & _
                    "    ,hremployee_master.classunkid	" & _
                    "    ,hremployee_master.stationunkid	" & _
                    "    ,hremployee_master.deptgroupunkid	" & _
                    "    ,hremployee_master.sectiongroupunkid	" & _
                    "    ,hremployee_master.sectionunkid	" & _
                    "    ,hremployee_master.unitgroupunkid	" & _
                    "    ,hremployee_master.unitunkid	" & _
                    "    ,hremployee_master.teamunkid	" & _
                    "    ,hremployee_master.jobgroupunkid	" & _
                   "FROM " & strDatabaseName & "..hremployee_master " & _
                   "JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "        EAT.employeeunkid AS iEmpId " & _
                   "       ,EAT.statusunkid " & _
                   "       ,ROW_NUMBER()OVER(PARTITION BY EAT.employeeunkid ORDER BY EAT.transactiondate DESC) as xrno " & _
                   "    FROM " & strDatabaseName & "..hremployeeapproval_tran AS EAT " & _
                   "        JOIN " & strDatabaseName & "..hremployee_master ON EAT.employeeunkid = hremployee_master.employeeunkid " & _
                   "    WHERE EAT.isvoid = 0 AND EAT.statusunkid = 1 AND hremployee_master.isapproved = 0 " & _
                   ") AS A ON A.iEmpId = hremployee_master.employeeunkid " & _
                   "SELECT DISTINCT " & _
                   "     Fn.userunkid " & _
                   "    ,CAST(0 AS BIT) AS icheck " & _
                   "    ,Fn.username " & _
                   "    ,hremployee_master.employeecode + ' - ' + hremployee_master.firstname +' '+hremployee_master.surname AS Employee " & _
                   "    ,ISNULL(EDP.name,'') AS Department " & _
                   "    ,ISNULL(EJB.job_name,'') AS Job " & _
                   "    ,ISNULL(ECG.name,'') AS ClassGroup " & _
                   "    ,ISNULL(ECM.name,'') AS Class " & _
                   "    ,ISNULL(B.iStatus,@Pending) AS [Status] " & _
                   "    ,SUM(1) OVER (PARTITION BY hremployee_master.employeeunkid) AS stotal " & _
                   "    ,hremployee_master.employeeunkid " & _
                   "    ,ISNULL(B.iStatusId,1) AS iStatusId " & _
                   "    ,ISNULL(B.isfinal,0) AS isfinal " & _
                   "    ,Fn.priority " & _
                   "    ,ISNULL(Fn.LvName,'') AS Level " & _
                   "    ,ISNULL(Fn.mappingid,0) AS mappingid " & _
                   "    ,CASE WHEN B.iStatusId IS NULL THEN 0 ELSE 1 END AS iRead " & _
                   "FROM @emp " & _
                    "JOIN #USR ON [@emp].empid = #USR.employeeunkid " & _
                    "JOIN " & strDatabaseName & "..hremployee_master ON empid = hremployee_master.employeeunkid " & _
                   "LEFT JOIN " & strDatabaseName & "..hrdepartment_master AS EDP ON EDP.departmentunkid = hremployee_master.departmentunkid " & _
                   "LEFT JOIN " & strDatabaseName & "..hrjob_master AS EJB ON EJB.jobunkid = hremployee_master.jobunkid " & _
                   "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master AS ECG ON ECG.classgroupunkid = hremployee_master.classgroupunkid " & _
                   "LEFT JOIN " & strDatabaseName & "..hrclasses_master AS ECM ON ECM.classesunkid = hremployee_master.classunkid " & _
                   "JOIN " & _
                   "( " & _
                   "    SELECT DISTINCT " & _
                   "         cfuser_master.userunkid " & _
                   "        ,cfuser_master.username " & _
                   "        ,cfuser_master.email " & _
                   strSelect & " " & _
                   "        ,LM.priority " & _
                   "        ,LM.empapplevelname AS LvName " & _
                   "        ,EM.mappingunkid AS mappingid " & _
                   "    FROM hrmsConfiguration..cfuser_master " & _
                   "    JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                   "    JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                   strAccessJoin & " " & _
                   "    JOIN " & strDatabaseName & "..hremp_appusermapping EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
                   "    JOIN " & strDatabaseName & "..hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
                    "    WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 " & _
                   "        /*AND companyunkid = @C*/ AND yearunkid = @Y AND privilegeunkid = @P AND EM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " AND LM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
                    ") AS Fn ON #USR.userunkid = Fn.userunkid /*" & IIf(strJoin.Trim.Length <= 0, " 1 = 1 ", strJoin).ToString() & "*/ " & _
                   strOuterJoin & " " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         iEmpId,iStatusId,iStatus,iRno,mappingunkid,mapuserunkid,deptid,jobid,clsgrpid,clsid " & _
                   "        ,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid,iPriority,isfinal,isnotified " & _
                   "    FROM " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             EAT.employeeunkid AS iEmpId " & _
                   "            ,EAT.statusunkid AS iStatusId " & _
                   "            ,CASE EAT.statusunkid WHEN 1 THEN @Pending " & _
                   "                                  WHEN 2 THEN @Approved +' : [ '+ USR.username + ' ]' " & _
                   "                                  WHEN 3 THEN @Reject +' : [ '+ USR.username + ' ]' " & _
                   "             ELSE @PendingApproval END AS iStatus " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY EAT.employeeunkid ORDER BY EAT.transactiondate DESC) AS iRno " & _
                   "            ,ISNULL(EUM.mappingunkid,0) AS mappingunkid " & _
                   "            ,ISNULL(EUM.mapuserunkid,0) AS mapuserunkid " & _
                   "            ,EPM.departmentunkid AS deptid " & _
                   "            ,EPM.jobunkid AS jobid " & _
                   "            ,EPM.classgroupunkid AS clsgrpid " & _
                   "            ,EPM.classunkid	AS clsid " & _
                   "            ,EPM.stationunkid AS branchid " & _
                   "            ,EPM.deptgroupunkid	AS deptgrpid " & _
                   "            ,EPM.sectiongroupunkid AS secgrpid " & _
                   "            ,EPM.sectionunkid AS secid " & _
                   "            ,EPM.unitgroupunkid	AS unitgrpid " & _
                   "            ,EPM.unitunkid AS unitid " & _
                   "            ,EPM.teamunkid AS teamid " & _
                   "            ,EPM.jobgroupunkid AS jgrpid " & _
                   "            ,ALM.priority AS iPriority " & _
                   "            ,EAT.isfinal AS isfinal " & _
                   "            ,EAT.isnotified AS isnotified " & _
                   "        FROM hremployeeapproval_tran AS EAT " & _
                   "            JOIN @emp ON [@emp].empid = EAT.employeeunkid " & _
                   "            JOIN hremployee_master AS EPM ON EAT.employeeunkid = EPM.employeeunkid " & _
                   "            LEFT JOIN hremp_appusermapping AS EUM ON EAT.mappingunkid = EUM.mappingunkid AND EUM.isactive = 1 AND EUM.isvoid = 0 AND EUM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & "  " & _
                   "            LEFT JOIN hrempapproverlevel_master ALM ON ALM.empapplevelunkid = EUM.empapplevelunkid AND ALM.isactive = 1 AND ALM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
                   "            LEFT JOIN hrmsConfiguration..cfuser_master AS USR ON USR.userunkid = EUM.mapuserunkid  " & _
                   "        WHERE EAT.isvoid = 0 /*AND EAT.statusunkid IN (2,3)*/ " & _
                   "    ) AS A WHERE A.iRno = 1 " & _
                      ") AS B ON #USR.userunkid = B.mapuserunkid AND Fn.priority = B.iPriority AND B.iEmpId = [@emp].empid " & _
            "WHERE 1 = 1 " & strFinalString & " "

            StrQ &= " AND Fn.priority <= @Priority "

            StrQ &= " ORDER BY employeeunkid, Fn.priority ASC "

            objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)
            objDataOperation.AddParameter("@Priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriorityId)

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 100, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 102, "Rejected"))
            objDataOperation.AddParameter("@PendingApproval", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 103, "Pending for Approval"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            oData = dsList.Tables(0).Copy()

            If blnOnlyMyApprovals Then
                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim blnFlag As Boolean = False
                    Dim iPriority As List(Of Integer) = dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Distinct().ToList()
                    Dim intSecondLastPriority As Integer = -1
                    If iPriority.Min() = intPriorityId Then
                        intSecondLastPriority = intPriorityId
                        blnFlag = True
                    Else
                        'S.SANDEEP [14-AUG-2018] -- START
                        'ISSUE/ENHANCEMENT : {Ref#244}
                        'intSecondLastPriority = iPriority.Item(iPriority.IndexOf(intPriorityId) - 1)
                        Try
                        intSecondLastPriority = iPriority.Item(iPriority.IndexOf(intPriorityId) - 1)
                        Catch ex As Exception
                            intSecondLastPriority = intPriorityId
                            blnFlag = True
                        End Try
                        'S.SANDEEP [14-AUG-2018] -- END
                    End If
                    Dim dr() As DataRow = Nothing
                    dr = dsList.Tables(0).Select("priority = '" & intSecondLastPriority & "'", "employeeunkid, priority")
                    If dr.Length > 0 Then
                        Dim row = dr.AsEnumerable().Where(Function(x) x.Field(Of Integer)("iStatusId") = CInt(clsEmployee_Master.EmpApprovalStatus.Approved))
                        Dim strIds As String
                        If row.Count() > 0 Then
                            'strIds = String.Join(",", row.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).Distinct().ToArray())
                            strIds = String.Join(",", dr.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToList().Except(row.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToList()).Distinct().ToArray())
                            If blnFlag Then
                                strIds = ""
                            End If
                            If strIds.Trim.Length > 0 Then
                                oData = New DataView(dsList.Tables(0), "userunkid = " & intUserId & " AND iStatusId = 1 AND employeeunkid NOT IN (" & strIds & ") ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                            Else
                                oData = New DataView(dsList.Tables(0), "userunkid = " & intUserId & " AND iStatusId = 1", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                            End If
                        Else
                            'Dim joinedStr As String = String.Join("|", arr.[Select](Function(p) p.ToString()).ToArray())
                            strIds = String.Join(",", dr.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).Distinct().ToArray())
                            If blnFlag = False Then
                                strIds = " AND employeeunkid NOT IN(" & strIds & ") "
                            Else
                                strIds = ""
                            End If
                            oData = New DataView(dsList.Tables(0), "userunkid = " & intUserId & " AND iStatusId = 1 " & strIds, "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                        End If
                    Else
                        oData = New DataView(dsList.Tables(0), "userunkid = " & intUserId, "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable() '& " AND iStatusId = 1 "
                    End If
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return oData
    End Function

    'Public Function GetEmployeeApprovalData(ByVal strDatabaseName As String _
    '                                      , ByVal intCompanyId As Integer _
    '                                      , ByVal intYearId As Integer _
    '                                      , ByVal strUserAccessMode As String _
    '                                      , ByVal intPrivilegeId As Integer _
    '                                      , ByVal intUserId As Integer _
    '                                      , ByVal intPriorityId As Integer _
    '                                      , ByVal blnOnlyMyApprovals As Boolean _
    '                                      , Optional ByVal objDataOpr As clsDataOperation = Nothing) As DataTable
    '    Dim StrQ As String = String.Empty
    '    Dim dsList As New DataSet
    '    Dim oData As DataTable
    '    Dim objDataOperation As New clsDataOperation
    '    If objDataOpr IsNot Nothing Then
    '        objDataOperation = objDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    objDataOperation.ClearParameters()
    '    Try
    '        Dim strFilter As String = String.Empty
    '        Dim strJoin As String = String.Empty
    '        Dim strSelect As String = String.Empty
    '        Dim strAccessJoin As String = String.Empty
    '        Dim strOuterJoin As String = String.Empty
    '        Dim strFinalString As String = ""

    '        Call GetUACFilters(strDatabaseName, strUserAccessMode, strFilter, strJoin, strSelect, strAccessJoin, strOuterJoin, strFinalString, intUserId, objDataOperation)

    '        'If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
    '        'Dim strvalues() As String = strUserAccessMode.Split(",")
    '        'Dim strFilter As String = String.Empty
    '        'Dim strJoin As String = String.Empty
    '        'Dim strSelect As String = String.Empty
    '        'Dim strAccessJoin As String = String.Empty
    '        'Dim strOuterJoin As String = String.Empty

    '        'Dim strFinalString As String = ""
    '        'For index As Integer = 0 To strvalues.Length - 1
    '        '    Select Case strvalues(index)
    '        '        Case enAllocation.BRANCH
    '        '            strSelect &= ",ISNULL(BR.branchid,0) AS branchid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as branchid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.BRANCH) & "' " & _
    '        '                             "    ) AS BR ON BR.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(BR.branchid,0) > 0 "
    '        '            strJoin &= "AND Fn.branchid = [@emp].branchid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrstation_master ON Fn.branchid = hrstation_master.stationunkid "

    '        '        Case enAllocation.DEPARTMENT_GROUP
    '        '            strSelect &= ",ISNULL(DG.deptgrpid,0) AS deptgrpid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as deptgrpid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT_GROUP) & "' " & _
    '        '                             "    ) AS DG ON DG.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(DG.deptgrpid,0) > 0 "
    '        '            strJoin &= "AND Fn.deptgrpid = [@emp].deptgrpid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_group_master ON Fn.deptgrpid = hrdepartment_group_master.deptgroupunkid "

    '        '        Case enAllocation.DEPARTMENT
    '        '            strSelect &= ",ISNULL(DP.deptid,0) AS deptid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as deptid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT) & "' " & _
    '        '                             "    ) AS DP ON DP.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(DP.deptid,0) > 0 "
    '        '            strJoin &= "AND Fn.deptid = [@emp].deptid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_master ON Fn.deptid = hrdepartment_master.departmentunkid "

    '        '        Case enAllocation.SECTION_GROUP
    '        '            strSelect &= ",ISNULL(SG.secgrpid,0) AS secgrpid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as secgrpid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION_GROUP) & "' " & _
    '        '                             "    ) AS SG ON SG.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(SG.secgrpid,0) > 0 "
    '        '            strJoin &= "AND Fn.secgrpid = [@emp].secgrpid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsectiongroup_master ON Fn.secgrpid = hrsectiongroup_master.sectiongroupunkid "

    '        '        Case enAllocation.SECTION
    '        '            strSelect &= ",ISNULL(SC.secid,0) AS secid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as secid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION) & "' " & _
    '        '                             "    ) AS SC ON SC.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(SC.secid,0) > 0 "
    '        '            strJoin &= "AND Fn.secid = [@emp].secid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsection_master ON Fn.secid = hrsection_master.sectionunkid "

    '        '        Case enAllocation.UNIT_GROUP
    '        '            strSelect &= ",ISNULL(UG.unitgrpid,0) AS unitgrpid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as unitgrpid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT_GROUP) & "' " & _
    '        '                             "    ) AS UG ON UG.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(UG.unitgrpid,0) > 0 "
    '        '            strJoin &= "AND Fn.unitgrpid = [@emp].unitgrpid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunitgroup_master ON Fn.unitgrpid = hrunitgroup_master.unitgroupunkid "

    '        '        Case enAllocation.UNIT
    '        '            strSelect &= ",ISNULL(UT.unitid,0) AS unitid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as unitid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT) & "' " & _
    '        '                             "    ) AS UT ON UT.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(UT.unitid,0) > 0 "
    '        '            strJoin &= "AND Fn.unitid = [@emp].unitid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunit_master ON Fn.unitid = hrunit_master.unitunkid "

    '        '        Case enAllocation.TEAM
    '        '            strSelect &= ",ISNULL(TM.teamid,0) AS teamid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as teamid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.TEAM) & "' " & _
    '        '                             "    ) AS TM ON TM.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(TM.teamid,0) > 0 "
    '        '            strJoin &= "AND Fn.teamid = [@emp].teamid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrteam_master ON Fn.teamid = hrteam_master.teamunkid "

    '        '        Case enAllocation.JOB_GROUP
    '        '            strSelect &= " ,ISNULL(JG.jgrpid,0) AS jgrpid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as jgrpid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOB_GROUP) & "' " & _
    '        '                             "    ) AS JG ON JG.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(JG.jgrpid,0) > 0 "
    '        '            strJoin &= "AND Fn.jgrpid = [@emp].jgrpid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjobgroup_master ON Fn.jgrpid = hrjobgroup_master.jobgroupunkid "

    '        '        Case enAllocation.JOBS
    '        '            strSelect &= ",ISNULL(JB.jobid,0) AS jobid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as jobid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOBS) & "' " & _
    '        '                             "    ) AS JB ON JB.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(JB.jobid,0) > 0 "
    '        '            strJoin &= "AND Fn.jobid = [@emp].jobid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjob_master ON Fn.jobid = hrjob_master.jobunkid "

    '        '        Case enAllocation.CLASS_GROUP
    '        '            strSelect &= ",ISNULL(CG.clsgrpid,0) AS clsgrpid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as clsgrpid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASS_GROUP) & "' " & _
    '        '                             "    ) AS CG ON CG.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(CG.clsgrpid,0) > 0 "
    '        '            strJoin &= "AND Fn.clsgrpid = [@emp].clsgrpid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master ON Fn.clsgrpid = hrclassgroup_master.classgroupunkid "

    '        '        Case enAllocation.CLASSES
    '        '            strSelect &= ",ISNULL(CL.clsid,0) AS clsid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as clsid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASSES) & "' " & _
    '        '                             "    ) AS CL ON CL.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(CL.clsid,0) > 0 "
    '        '            strJoin &= "AND Fn.clsid = [@emp].clsid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclasses_master ON Fn.clsid = classesunkid "

    '        '    End Select

    '        '    objDataOperation.ClearParameters()
    '        '    StrQ = "SELECT @Value = ISNULL(STUFF((SELECT DISTINCT ',' + CAST(allocationunkid AS NVARCHAR(MAX)) FROM hrmsConfiguration..cfuseraccess_privilege_tran WHERE useraccessprivilegeunkid IN (SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE userunkid = " & intUserId & " AND referenceunkid = " & strvalues(index) & ") FOR XML PATH('')),1,1,''),'') "
    '        '    Dim strvalue = ""
    '        '    objDataOperation.AddParameter("@Value", SqlDbType.NVarChar, 4000, strvalue, ParameterDirection.InputOutput)

    '        '    objDataOperation.ExecNonQuery(StrQ)

    '        '    If objDataOperation.ErrorMessage <> "" Then
    '        '        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        '    End If

    '        '    strvalue = objDataOperation.GetParameterValue("@Value")

    '        '    If strvalue.Trim.Length > 0 Then
    '        '        Select Case strvalues(index)
    '        '            Case enAllocation.BRANCH
    '        '                strFinalString &= " AND Fn.branchid IN (" & strvalue & ") "

    '        '            Case enAllocation.DEPARTMENT_GROUP
    '        '                strFinalString &= " AND Fn.deptgrpid IN (" & strvalue & ") "

    '        '            Case enAllocation.DEPARTMENT
    '        '                strFinalString &= " AND Fn.deptid IN (" & strvalue & ") "

    '        '            Case enAllocation.SECTION_GROUP
    '        '                strFinalString &= " AND Fn.secgrpid IN (" & strvalue & ") "

    '        '            Case enAllocation.SECTION
    '        '                strFinalString &= " AND Fn.secid IN (" & strvalue & ") "

    '        '            Case enAllocation.UNIT_GROUP
    '        '                strFinalString &= " AND Fn.unitgrpid IN (" & strvalue & ") "

    '        '            Case enAllocation.UNIT
    '        '                strFinalString &= " AND Fn.unitid IN (" & strvalue & ") "

    '        '            Case enAllocation.TEAM
    '        '                strFinalString &= " AND Fn.teamid IN (" & strvalue & ") "

    '        '            Case enAllocation.JOB_GROUP
    '        '                strFinalString &= " AND Fn.jgrpid IN (" & strvalue & ") "

    '        '            Case enAllocation.JOBS
    '        '                strFinalString &= " AND Fn.jobid IN (" & strvalue & ") "

    '        '            Case enAllocation.CLASS_GROUP
    '        '                strFinalString &= " AND Fn.clsgrpid IN (" & strvalue & ") "

    '        '            Case enAllocation.CLASSES
    '        '                strFinalString &= " AND Fn.clsid IN (" & strvalue & ") "

    '        '        End Select
    '        '    End If

    '        'Next
    '        'If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
    '        'If strJoin.Trim.Length > 0 Then strJoin = strJoin.Substring(3)

    '        objDataOperation.ClearParameters()
    '        'StrQ = "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
    '        '       "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
    '        '       "SELECT " & _
    '        '       "     employeeunkid " & _
    '        '       "    ,departmentunkid " & _
    '        '       "    ,jobunkid " & _
    '        '       "    ,classgroupunkid " & _
    '        '       "    ,classunkid	" & _
    '        '       "    ,stationunkid	" & _
    '        '       "    ,deptgroupunkid	" & _
    '        '       "    ,sectiongroupunkid	" & _
    '        '       "    ,sectionunkid	" & _
    '        '       "    ,unitgroupunkid	" & _
    '        '       "    ,unitunkid	" & _
    '        '       "    ,teamunkid	" & _
    '        '       "    ,jobgroupunkid	" & _
    '        '       "FROM " & strDatabaseName & "..hremployee_master " & _
    '        '       "JOIN " & _
    '        '       "( " & _
    '        '       "    SELECT " & _
    '        '       "        EAT.employeeunkid AS iEmpId " & _
    '        '       "       ,EAT.statusunkid " & _
    '        '       "       ,ROW_NUMBER()OVER(PARTITION BY EAT.employeeunkid ORDER BY EAT.transactiondate DESC) as xrno " & _
    '        '       "    FROM " & strDatabaseName & "..hremployeeapproval_tran AS EAT " & _
    '        '       "        JOIN " & strDatabaseName & "..hremployee_master ON EAT.employeeunkid = hremployee_master.employeeunkid " & _
    '        '       "    WHERE EAT.isvoid = 0 AND EAT.statusunkid = 1 AND hremployee_master.isapproved = 0 " & _
    '        '       ") AS A ON A.iEmpId = hremployee_master.employeeunkid /*AND A.statusunkid IN (" & clsEmployee_Master.EmpApprovalStatus.SubmitForApproval & ") AND A.xrno = 1*/ " & _
    '        '       "SELECT DISTINCT " & _
    '        '       "     Fn.userunkid " & _
    '        '       "    ,CAST(0 AS BIT) AS icheck " & _
    '        '       "    ,Fn.username " & _
    '        '       "    ,hremployee_master.employeecode + ' - ' + hremployee_master.firstname +' '+hremployee_master.surname AS Employee " & _
    '        '       "    ,ISNULL(EDP.name,'') AS Department " & _
    '        '       "    ,ISNULL(EJB.job_name,'') AS Job " & _
    '        '       "    ,ISNULL(ECG.name,'') AS ClassGroup " & _
    '        '       "    ,ISNULL(ECM.name,'') AS Class " & _
    '        '       "    ,ISNULL(B.iStatus,@Pending) AS [Status] " & _
    '        '       "    ,SUM(1) OVER (PARTITION BY hremployee_master.employeeunkid) AS stotal " & _
    '        '       "    ,hremployee_master.employeeunkid " & _
    '        '       "    ,ISNULL(B.iStatusId,1) AS iStatusId " & _
    '        '       "    ,ISNULL(B.isfinal,0) AS isfinal " & _
    '        '       "    ,Fn.priority " & _
    '        '       "    ,ISNULL(Fn.LvName,'') AS Level " & _
    '        '       "    ,ISNULL(Fn.mappingid,0) AS mappingid " & _
    '        '       "    ,CASE WHEN B.iStatusId IS NULL THEN 0 ELSE 1 END AS iRead " & _
    '        '       "FROM @emp " & _
    '        '       "JOIN " & strDatabaseName & "..hremployee_master ON empid = employeeunkid " & _
    '        '       "LEFT JOIN " & strDatabaseName & "..hrdepartment_master AS EDP ON EDP.departmentunkid = hremployee_master.departmentunkid " & _
    '        '       "LEFT JOIN " & strDatabaseName & "..hrjob_master AS EJB ON EJB.jobunkid = hremployee_master.jobunkid " & _
    '        '       "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master AS ECG ON ECG.classgroupunkid = hremployee_master.classgroupunkid " & _
    '        '       "LEFT JOIN " & strDatabaseName & "..hrclasses_master AS ECM ON ECM.classesunkid = hremployee_master.classunkid " & _
    '        '       "JOIN " & _
    '        '       "( " & _
    '        '       "    SELECT DISTINCT " & _
    '        '       "         cfuser_master.userunkid " & _
    '        '       "        ,cfuser_master.username " & _
    '        '       "        ,cfuser_master.email " & _
    '        '       strSelect & " " & _
    '        '       "        ,LM.priority " & _
    '        '       "        ,LM.empapplevelname AS LvName " & _
    '        '       "        ,EM.mappingunkid AS mappingid " & _
    '        '       "    FROM hrmsConfiguration..cfuser_master " & _
    '        '       "    JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
    '        '       "    JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
    '        '       strAccessJoin & " " & _
    '        '       "    JOIN " & strDatabaseName & "..hremp_appusermapping EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
    '        '       "    JOIN " & strDatabaseName & "..hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
    '        '       "    WHERE cfuser_master.isactive = 1 AND (" & strFilter & " ) AND EM.isactive = 1 AND EM.isvoid = 0 " & _
    '        '       "        AND companyunkid = @C AND yearunkid = @Y AND privilegeunkid = @P  " & _
    '        '       ") AS Fn ON " & strJoin & " " & _
    '        '       strOuterJoin & " " & _
    '        '       "LEFT JOIN " & _
    '        '       "( " & _
    '        '       "    SELECT " & _
    '        '       "        * " & _
    '        '       "    FROM " & _
    '        '       "    ( " & _
    '        '       "        SELECT " & _
    '        '       "             EAT.employeeunkid AS iEmpId " & _
    '        '       "            ,EAT.statusunkid AS iStatusId " & _
    '        '       "            ,CASE EAT.statusunkid WHEN 1 THEN @Pending " & _
    '        '       "                                  WHEN 2 THEN @Approved +' : [ '+ USR.username + ' ]' " & _
    '        '       "                                  WHEN 3 THEN @Reject +' : [ '+ USR.username + ' ]' " & _
    '        '       "             ELSE @PendingApproval END AS iStatus " & _
    '        '       "            ,ROW_NUMBER()OVER(PARTITION BY EAT.employeeunkid ORDER BY EAT.transactiondate DESC) AS iRno " & _
    '        '       "            ,ISNULL(EUM.mappingunkid,0) AS mappingunkid " & _
    '        '       "            ,ISNULL(EUM.mapuserunkid,0) AS mapuserunkid " & _
    '        '       "            ,EPM.departmentunkid AS deptid " & _
    '        '       "            ,EPM.jobunkid AS jobid " & _
    '        '       "            ,EPM.classgroupunkid AS clsgrpid " & _
    '        '       "            ,EPM.classunkid	AS clsid " & _
    '        '       "            ,EPM.stationunkid AS branchid " & _
    '        '       "            ,EPM.deptgroupunkid	AS deptgrpid " & _
    '        '       "            ,EPM.sectiongroupunkid AS secgrpid " & _
    '        '       "            ,EPM.sectionunkid AS secid " & _
    '        '       "            ,EPM.unitgroupunkid	AS unitgrpid " & _
    '        '       "            ,EPM.unitunkid AS unitid " & _
    '        '       "            ,EPM.teamunkid AS teamid " & _
    '        '       "            ,EPM.jobgroupunkid AS jgrpid " & _
    '        '       "            ,ALM.priority AS iPriority " & _
    '        '       "            ,EAT.isfinal AS isfinal " & _
    '        '       "            ,EAT.isfinal AS isnotified " & _
    '        '       "        FROM hremployeeapproval_tran AS EAT " & _
    '        '       "            JOIN hremployee_master AS EPM ON EAT.employeeunkid = EPM.employeeunkid " & _
    '        '       "            LEFT JOIN hremp_appusermapping AS EUM ON EAT.mappingunkid = EUM.mappingunkid AND EUM.isactive = 1 AND EUM.isvoid = 0 " & _
    '        '       "            LEFT JOIN hrempapproverlevel_master ALM ON ALM.empapplevelunkid = EUM.empapplevelunkid AND ALM.isactive = 1 " & _
    '        '       "            LEFT JOIN hrmsConfiguration..cfuser_master AS USR ON USR.userunkid = EUM.mapuserunkid " & _
    '        '       "        WHERE EAT.isvoid = 0 /*AND EAT.statusunkid IN (2,3)*/ " & _
    '        '       "    ) AS A WHERE A.iRno = 1 " & _
    '        '       ") AS B ON " & strJoin.Replace("[@emp]", "B") & " AND Fn.priority = B.iPriority " & _
    '        '       "WHERE 1 = 1 AND (Fn.priority < @Priority OR Fn.priority = @Priority) " & strFinalString & " " & _
    '        '       "ORDER BY employeeunkid, Fn.priority ASC "
    '        StrQ = "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
    '               "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
    '               "SELECT " & _
    '               "     employeeunkid " & _
    '               "    ,departmentunkid " & _
    '               "    ,jobunkid " & _
    '               "    ,classgroupunkid " & _
    '               "    ,classunkid	" & _
    '               "    ,stationunkid	" & _
    '               "    ,deptgroupunkid	" & _
    '               "    ,sectiongroupunkid	" & _
    '               "    ,sectionunkid	" & _
    '               "    ,unitgroupunkid	" & _
    '               "    ,unitunkid	" & _
    '               "    ,teamunkid	" & _
    '               "    ,jobgroupunkid	" & _
    '               "FROM " & strDatabaseName & "..hremployee_master " & _
    '               "JOIN " & _
    '               "( " & _
    '               "    SELECT " & _
    '               "        EAT.employeeunkid AS iEmpId " & _
    '               "       ,EAT.statusunkid " & _
    '               "       ,ROW_NUMBER()OVER(PARTITION BY EAT.employeeunkid ORDER BY EAT.transactiondate DESC) as xrno " & _
    '               "    FROM " & strDatabaseName & "..hremployeeapproval_tran AS EAT " & _
    '               "        JOIN " & strDatabaseName & "..hremployee_master ON EAT.employeeunkid = hremployee_master.employeeunkid " & _
    '               "    WHERE EAT.isvoid = 0 AND EAT.statusunkid = 1 AND hremployee_master.isapproved = 0 " & _
    '               ") AS A ON A.iEmpId = hremployee_master.employeeunkid " & _
    '               "SELECT DISTINCT " & _
    '               "     Fn.userunkid " & _
    '               "    ,CAST(0 AS BIT) AS icheck " & _
    '               "    ,Fn.username " & _
    '               "    ,hremployee_master.employeecode + ' - ' + hremployee_master.firstname +' '+hremployee_master.surname AS Employee " & _
    '               "    ,ISNULL(EDP.name,'') AS Department " & _
    '               "    ,ISNULL(EJB.job_name,'') AS Job " & _
    '               "    ,ISNULL(ECG.name,'') AS ClassGroup " & _
    '               "    ,ISNULL(ECM.name,'') AS Class " & _
    '               "    ,ISNULL(B.iStatus,@Pending) AS [Status] " & _
    '               "    ,SUM(1) OVER (PARTITION BY hremployee_master.employeeunkid) AS stotal " & _
    '               "    ,hremployee_master.employeeunkid " & _
    '               "    ,ISNULL(B.iStatusId,1) AS iStatusId " & _
    '               "    ,ISNULL(B.isfinal,0) AS isfinal " & _
    '               "    ,Fn.priority " & _
    '               "    ,ISNULL(Fn.LvName,'') AS Level " & _
    '               "    ,ISNULL(Fn.mappingid,0) AS mappingid " & _
    '               "    ,CASE WHEN B.iStatusId IS NULL THEN 0 ELSE 1 END AS iRead " & _
    '               "FROM @emp " & _
    '               "JOIN " & strDatabaseName & "..hremployee_master ON empid = employeeunkid " & _
    '               "LEFT JOIN " & strDatabaseName & "..hrdepartment_master AS EDP ON EDP.departmentunkid = hremployee_master.departmentunkid " & _
    '               "LEFT JOIN " & strDatabaseName & "..hrjob_master AS EJB ON EJB.jobunkid = hremployee_master.jobunkid " & _
    '               "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master AS ECG ON ECG.classgroupunkid = hremployee_master.classgroupunkid " & _
    '               "LEFT JOIN " & strDatabaseName & "..hrclasses_master AS ECM ON ECM.classesunkid = hremployee_master.classunkid " & _
    '               "JOIN " & _
    '               "( " & _
    '               "    SELECT DISTINCT " & _
    '               "         cfuser_master.userunkid " & _
    '               "        ,cfuser_master.username " & _
    '               "        ,cfuser_master.email " & _
    '               strSelect & " " & _
    '               "        ,LM.priority " & _
    '               "        ,LM.empapplevelname AS LvName " & _
    '               "        ,EM.mappingunkid AS mappingid " & _
    '               "    FROM hrmsConfiguration..cfuser_master " & _
    '               "    JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
    '               "    JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
    '               strAccessJoin & " " & _
    '               "    JOIN " & strDatabaseName & "..hremp_appusermapping EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
    '               "    JOIN " & strDatabaseName & "..hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
    '               "    WHERE cfuser_master.isactive = 1 AND (" & strFilter & " ) AND EM.isactive = 1 AND EM.isvoid = 0 " & _
    '               "        /*AND companyunkid = @C*/ AND yearunkid = @Y AND privilegeunkid = @P AND EM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " AND LM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
    '               ") AS Fn ON " & strJoin & " " & _
    '               strOuterJoin & " " & _
    '               "LEFT JOIN " & _
    '               "( " & _
    '               "    SELECT " & _
    '               "         iEmpId,iStatusId,iStatus,iRno,mappingunkid,mapuserunkid,deptid,jobid,clsgrpid,clsid " & _
    '               "        ,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid,iPriority,isfinal,isnotified " & _
    '               "    FROM " & _
    '               "    ( " & _
    '               "        SELECT " & _
    '               "             EAT.employeeunkid AS iEmpId " & _
    '               "            ,EAT.statusunkid AS iStatusId " & _
    '               "            ,CASE EAT.statusunkid WHEN 1 THEN @Pending " & _
    '               "                                  WHEN 2 THEN @Approved +' : [ '+ USR.username + ' ]' " & _
    '               "                                  WHEN 3 THEN @Reject +' : [ '+ USR.username + ' ]' " & _
    '               "             ELSE @PendingApproval END AS iStatus " & _
    '               "            ,ROW_NUMBER()OVER(PARTITION BY EAT.employeeunkid ORDER BY EAT.transactiondate DESC) AS iRno " & _
    '               "            ,ISNULL(EUM.mappingunkid,0) AS mappingunkid " & _
    '               "            ,ISNULL(EUM.mapuserunkid,0) AS mapuserunkid " & _
    '               "            ,EPM.departmentunkid AS deptid " & _
    '               "            ,EPM.jobunkid AS jobid " & _
    '               "            ,EPM.classgroupunkid AS clsgrpid " & _
    '               "            ,EPM.classunkid	AS clsid " & _
    '               "            ,EPM.stationunkid AS branchid " & _
    '               "            ,EPM.deptgroupunkid	AS deptgrpid " & _
    '               "            ,EPM.sectiongroupunkid AS secgrpid " & _
    '               "            ,EPM.sectionunkid AS secid " & _
    '               "            ,EPM.unitgroupunkid	AS unitgrpid " & _
    '               "            ,EPM.unitunkid AS unitid " & _
    '               "            ,EPM.teamunkid AS teamid " & _
    '               "            ,EPM.jobgroupunkid AS jgrpid " & _
    '               "            ,ALM.priority AS iPriority " & _
    '               "            ,EAT.isfinal AS isfinal " & _
    '               "            ,EAT.isnotified AS isnotified " & _
    '               "        FROM hremployeeapproval_tran AS EAT " & _
    '               "            JOIN @emp ON [@emp].empid = EAT.employeeunkid " & _
    '               "            JOIN hremployee_master AS EPM ON EAT.employeeunkid = EPM.employeeunkid " & _
    '               "            LEFT JOIN hremp_appusermapping AS EUM ON EAT.mappingunkid = EUM.mappingunkid AND EUM.isactive = 1 AND EUM.isvoid = 0 AND EUM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & "  " & _
    '               "            LEFT JOIN hrempapproverlevel_master ALM ON ALM.empapplevelunkid = EUM.empapplevelunkid AND ALM.isactive = 1 AND ALM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
    '               "            LEFT JOIN hrmsConfiguration..cfuser_master AS USR ON USR.userunkid = EUM.mapuserunkid  " & _
    '               "        WHERE EAT.isvoid = 0 /*AND EAT.statusunkid IN (2,3)*/ " & _
    '               "    ) AS A WHERE A.iRno = 1 " & _
    '               ") AS B ON " & strJoin.Replace("[@emp]", "B") & " AND Fn.priority = B.iPriority AND B.iEmpId = [@emp].empid " & _
    '        "WHERE 1 = 1 " & strFinalString & " "

    '        'Gajanan [6-Aug-2019] -- ADD [AND B.iEmpId = [@emp].empid]


    '        'S.SANDEEP |06-MAR-2019| -- START
    '        'ISSUE/ENHANCEMENT : NOT ABLE TO SEE EMPLOYEE ON APPROVAL SCREEN
    '        'JOIN @emp ON [@emp].empid = EAT.employeeunkid  ----- ADDED
    '        'S.SANDEEP |06-MAR-2019| -- END



    '        'S.SANDEEP [26-SEP-2018] -- START
    '        '/*AND companyunkid = @C -- REMOVED AS ADMIN USER DOES NOT HAVE COMPANY ID*/
    '        'S.SANDEEP [26-SEP-2018] -- END

    '        'If blnOnlyMyApprovals Then
    '        StrQ &= " AND Fn.priority <= @Priority "
    '        'End If

    '        StrQ &= " ORDER BY employeeunkid, Fn.priority ASC "

    '        objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
    '        objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
    '        objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)
    '        objDataOperation.AddParameter("@Priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriorityId)

    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 100, "Pending"))
    '        objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Approved"))
    '        objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 102, "Rejected"))
    '        objDataOperation.AddParameter("@PendingApproval", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 103, "Pending for Approval"))

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        End If

    '        oData = dsList.Tables(0).Copy()

    '        If blnOnlyMyApprovals Then
            'If dsList.Tables(0).Rows.Count > 0 Then
    '                Dim blnFlag As Boolean = False
    '                Dim iPriority As List(Of Integer) = dsList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Distinct().ToList()
    '                Dim intSecondLastPriority As Integer = -1
    '                If iPriority.Min() = intPriorityId Then
    '                    intSecondLastPriority = intPriorityId
    '                    blnFlag = True
    '                Else
    '                    'S.SANDEEP [14-AUG-2018] -- START
    '                    'ISSUE/ENHANCEMENT : {Ref#244}
    '                    'intSecondLastPriority = iPriority.Item(iPriority.IndexOf(intPriorityId) - 1)
    '                    Try
    '                        intSecondLastPriority = iPriority.Item(iPriority.IndexOf(intPriorityId) - 1)
    '                    Catch ex As Exception
    '                        intSecondLastPriority = intPriorityId
    '                        blnFlag = True
    '                    End Try
    '                    'S.SANDEEP [14-AUG-2018] -- END
    '                End If
            '    Dim dr() As DataRow = Nothing
    '                dr = dsList.Tables(0).Select("priority = '" & intSecondLastPriority & "'", "employeeunkid, priority")
            '    If dr.Length > 0 Then
    '                    Dim row = dr.AsEnumerable().Where(Function(x) x.Field(Of Integer)("iStatusId") = CInt(clsEmployee_Master.EmpApprovalStatus.Approved))
    '                    Dim strIds As String
    '                    If row.Count() > 0 Then
    '                        'strIds = String.Join(",", row.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).Distinct().ToArray())
    '                        strIds = String.Join(",", dr.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToList().Except(row.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToList()).Distinct().ToArray())
    '                        If blnFlag Then
    '                            strIds = ""
    '                        End If
            '        If strIds.Trim.Length > 0 Then
    '                            oData = New DataView(dsList.Tables(0), "userunkid = " & intUserId & " AND iStatusId = 1 AND employeeunkid NOT IN (" & strIds & ") ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
    '                        Else
    '                            oData = New DataView(dsList.Tables(0), "userunkid = " & intUserId & " AND iStatusId = 1", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
            '        End If
            '    Else
    '                        'Dim joinedStr As String = String.Join("|", arr.[Select](Function(p) p.ToString()).ToArray())
    '                        strIds = String.Join(",", dr.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).Distinct().ToArray())
    '                        If blnFlag = False Then
    '                            strIds = " AND employeeunkid NOT IN(" & strIds & ") "
            '        Else
    '                            strIds = ""
            '                End If
    '                        oData = New DataView(dsList.Tables(0), "userunkid = " & intUserId & " AND iStatusId = 1 " & strIds, "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
            '            End If
    '                Else
    '                    oData = New DataView(dsList.Tables(0), "userunkid = " & intUserId, "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable() '& " AND iStatusId = 1 "
            '        End If
            '    End If
            'End If


    '        'If dsList.Tables(0).Rows.Count > 0 Then
    '        '    Dim dr() As DataRow = Nothing
    '        '    dr = dsList.Tables(0).Select("stotal > 1 AND priority <= '" & intPriorityId & "' AND iStatusId <> 1 AND userunkid <> " & intUserId, "employeeunkid, priority")
    '        '    If dr.Length > 0 Then
    '        '        Dim strIds As String = String.Join(",", dr.CopyToDataTable().AsEnumerable().Select(Function(x) x.Field(Of Integer)("userunkid").ToString()).ToArray())
    '        '        If strIds.Trim.Length > 0 Then
    '        '            oData = New DataView(dsList.Tables(0), "userunkid NOT IN(" & strIds & ") AND iStatusId = 1 ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
    '        '        End If
    '        '    Else
    '        '        dr = dsList.Tables(0).Select("stotal > 1 AND priority < '" & intPriorityId & "' AND iStatusId = 1 ", "employeeunkid, priority")
    '        '        If dr.Length > 0 Then
    '        '            Dim strIds As String = String.Join(",", dr.CopyToDataTable().AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToArray())
    '        '            If strIds.Trim.Length > 0 Then
    '        '                oData = New DataView(dsList.Tables(0), "employeeunkid NOT IN(" & strIds & ") AND iStatusId = 1 ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
    '        '            End If
    '        '        Else
    '        '            dr = dsList.Tables(0).Select("stotal > 1 AND priority = '" & intPriorityId & "' AND iStatusId = 1 AND userunkid <> " & intUserId, "employeeunkid, priority")
    '        '            If dr.Length > 0 Then
    '        '                Dim strIds As String = String.Join(",", dr.CopyToDataTable().AsEnumerable().Select(Function(x) x.Field(Of Integer)("userunkid").ToString()).ToArray())
    '        '                If strIds.Trim.Length > 0 Then
    '        '                    oData = New DataView(dsList.Tables(0), "userunkid NOT IN(" & strIds & ") AND iStatusId = 1", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
    '        '                End If
    '        '            End If
    '        '        End If
    '        '    End If
    '        'End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApprovalData; Module Name: " & mstrModuleName)
    '    Finally
    '        If objDataOpr Is Nothing Then objDataOperation = Nothing
    '    End Try
    '    Return oData
    'End Function

    'Public Function GetNextEmployeeApprovers(ByVal strDatabaseName As String _
    '                                        , ByVal intCompanyId As Integer _
    '                                        , ByVal intYearId As Integer _
    '                                        , ByVal strEmpids As String _
    '                                        , ByVal strUserAccessMode As String _
    '                                        , ByVal intPrivilegeId As Integer _
    '                                        , Optional ByVal objDataOpr As clsDataOperation = Nothing _
    '                                        , Optional ByVal mblnIsSubmitForApproaval As Boolean = False _
    '                                        , Optional ByVal intCurrentPriorityId As Integer = -1 _
    '                                        ) As DataTable
    '    'S.SANDEEP [12-SEP-2018] -- START
    '    'ISSUE/ENHANCEMENT : {#2517}
    '    'intCurrentPriorityId = 0, intCurrentPriorityId = -1
    '    'S.SANDEEP [12-SEP-2018] -- END

    '    Dim StrQ As String = String.Empty
    '    Dim dsList As New DataSet
    '    Dim objDataOperation As New clsDataOperation
    '    If objDataOpr IsNot Nothing Then
    '        objDataOperation = objDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    objDataOperation.ClearParameters()
    '    Try
    '        Dim strFilter As String = String.Empty
    '        Dim strJoin As String = String.Empty
    '        Dim strSelect As String = String.Empty
    '        Dim strAccessJoin As String = String.Empty
    '        Dim strOuterJoin As String = String.Empty
    '        Dim strFinalString As String = ""

    '        StrQ = "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
    '               "DROP TABLE #USR "
    '        StrQ &= "SELECT " & _
    '                "* " & _
    '                "INTO #USR " & _
    '                "FROM " & _
    '                "( "

    '        If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
    '        Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
    '        For index As Integer = 0 To strvalues.Length - 1
    '            Dim xStrJoinColName As String = ""
    '            Dim xIntAllocId As Integer = 0
    '            Select Case CInt(strvalues(index))
    '                Case enAllocation.BRANCH
    '                    xStrJoinColName = "stationunkid"
    '                    xIntAllocId = CInt(enAllocation.BRANCH)
    '                Case enAllocation.DEPARTMENT_GROUP
    '                    xStrJoinColName = "deptgroupunkid"
    '                    xIntAllocId = CInt(enAllocation.DEPARTMENT_GROUP)
    '                Case enAllocation.DEPARTMENT
    '                    xStrJoinColName = "departmentunkid"
    '                    xIntAllocId = CInt(enAllocation.DEPARTMENT)
    '                Case enAllocation.SECTION_GROUP
    '                    xStrJoinColName = "sectiongroupunkid"
    '                    xIntAllocId = CInt(enAllocation.SECTION_GROUP)
    '                Case enAllocation.SECTION
    '                    xStrJoinColName = "sectionunkid"
    '                    xIntAllocId = CInt(enAllocation.SECTION)
    '                Case enAllocation.UNIT_GROUP
    '                    xStrJoinColName = "unitgroupunkid"
    '                    xIntAllocId = CInt(enAllocation.UNIT_GROUP)
    '                Case enAllocation.UNIT
    '                    xStrJoinColName = "unitunkid"
    '                    xIntAllocId = CInt(enAllocation.UNIT)
    '                Case enAllocation.TEAM
    '                    xStrJoinColName = "teamunkid"
    '                    xIntAllocId = CInt(enAllocation.TEAM)
    '                Case enAllocation.JOB_GROUP
    '                    xStrJoinColName = "jobgroupunkid"
    '                    xIntAllocId = CInt(enAllocation.JOB_GROUP)
    '                Case enAllocation.JOBS
    '                    xStrJoinColName = "jobunkid"
    '                    xIntAllocId = CInt(enAllocation.JOBS)
    '                Case enAllocation.CLASS_GROUP
    '                    xStrJoinColName = "classgroupunkid"
    '                    xIntAllocId = CInt(enAllocation.CLASS_GROUP)
    '                Case enAllocation.CLASSES
    '                    xStrJoinColName = "classunkid"
    '                    xIntAllocId = CInt(enAllocation.CLASSES)
    '            End Select
    '            StrQ &= "SELECT " & _
    '                    "    B" & index.ToString() & ".userunkid " & _
    '                    "   ,A.employeeunkid " & _
    '                    "FROM " & _
    '                    "( " & _
    '                    "   SELECT " & _
    '                    "        AEM.employeeunkid " & _
    '                    "       ,ISNULL(AEM.departmentunkid, 0) AS departmentunkid " & _
    '                    "       ,ISNULL(AEM.jobunkid, 0) AS jobunkid " & _
    '                    "       ,ISNULL(AEM.classgroupunkid, 0) AS classgroupunkid " & _
    '                    "       ,ISNULL(AEM.classunkid, 0) AS classunkid " & _
    '                    "       ,ISNULL(AEM.stationunkid, 0) AS stationunkid " & _
    '                    "       ,ISNULL(AEM.deptgroupunkid, 0) AS deptgroupunkid " & _
    '                    "       ,ISNULL(AEM.sectiongroupunkid, 0) AS sectiongroupunkid " & _
    '                    "       ,ISNULL(AEM.sectionunkid, 0) AS sectionunkid " & _
    '                    "       ,ISNULL(AEM.unitgroupunkid, 0) AS unitgroupunkid " & _
    '                    "       ,ISNULL(AEM.unitunkid, 0) AS unitunkid " & _
    '                    "       ,ISNULL(AEM.teamunkid, 0) AS teamunkid " & _
    '                    "       ,ISNULL(AEM.jobgroupunkid, 0) AS jobgroupunkid " & _
    '                    "   FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
    '                    "   JOIN " & strDatabaseName & "..hremployeeapproval_tran AS TAT ON TAT.employeeunkid = AEM.employeeunkid " & _
    '                    "   WHERE TAT.isvoid = 0 "
    '            StrQ &= ") AS A " & _
    '                    "JOIN " & _
    '                    "( " & _
    '                    "   SELECT " & _
    '                    "        UPM.userunkid " & _
    '                    "       ,UPT.allocationunkid " & _
    '                    "   FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                    "       JOIN hremp_appusermapping AS CAM ON CAM.mapuserunkid = UPM.userunkid AND CAM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & "  " & _
    '                    "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                    "   WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid IN (" & xIntAllocId & ") " & _
    '                    "   AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
    '                    ") AS B" & index.ToString() & " ON A." & xStrJoinColName & " = B" & index.ToString() & ".allocationunkid "
    '            If index < strvalues.Length - 1 Then
    '                StrQ &= " INTERSECT "
    '            End If
    '        Next

    '        StrQ &= ") AS Fl "

    '        StrQ &= "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
    '               "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
    '               "SELECT " & _
    '                "     hremployee_master.employeeunkid " & _
    '                "    ,hremployee_master.departmentunkid " & _
    '                "    ,hremployee_master.jobunkid " & _
    '                "    ,hremployee_master.classgroupunkid " & _
    '                "    ,hremployee_master.classunkid	" & _
    '                "    ,hremployee_master.stationunkid	" & _
    '                "    ,hremployee_master.deptgroupunkid	" & _
    '                "    ,hremployee_master.sectiongroupunkid	" & _
    '                "    ,hremployee_master.sectionunkid	" & _
    '                "    ,hremployee_master.unitgroupunkid	" & _
    '                "    ,hremployee_master.unitunkid	" & _
    '                "    ,hremployee_master.teamunkid	" & _
    '                "    ,hremployee_master.jobgroupunkid	" & _
    '               "FROM " & strDatabaseName & "..hremployee_master " & _
    '                "WHERE hremployee_master.employeeunkid IN (" & strEmpids & ") " & _
    '               "SELECT " & _
    '               "     Fn.userunkid " & _
    '               "    ,Fn.username " & _
    '               "    ,Fn.email " & _
    '               "    ,employeecode + ' - ' + firstname +' '+surname AS Employee " & _
    '               "    ,ISNULL(EDP.name,'') AS Department " & _
    '               "    ,ISNULL(EJB.job_name,'') AS Job " & _
    '               "    ,ISNULL(ECG.name,'') AS ClassGroup " & _
    '               "    ,ISNULL(ECM.name,'') AS Class " & _
    '               "    ,Fn.priority " & _
    '                "    ,hremployee_master.employeeunkid " & _
    '                "    ,ROW_NUMBER()OVER(PARTITION BY hremployee_master.employeeunkid ORDER BY Fn.priority ASC) AS rno " & _
    '               "FROM @emp " & _
    '                "JOIN #USR ON [@emp].empid = #USR.employeeunkid " & _
    '                "JOIN " & strDatabaseName & "..hremployee_master ON empid = hremployee_master.employeeunkid " & _
    '               "LEFT JOIN " & strDatabaseName & "..hrdepartment_master AS EDP ON EDP.departmentunkid = hremployee_master.departmentunkid " & _
    '               "LEFT JOIN " & strDatabaseName & "..hrjob_master AS EJB ON EJB.jobunkid = hremployee_master.jobunkid " & _
    '               "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master AS ECG ON ECG.classgroupunkid = hremployee_master.classgroupunkid " & _
    '               "LEFT JOIN " & strDatabaseName & "..hrclasses_master AS ECM ON ECM.classesunkid = hremployee_master.classunkid " & _
    '               "JOIN " & _
    '               "( " & _
    '               "    SELECT DISTINCT " & _
    '               "         cfuser_master.userunkid " & _
    '               "        ,cfuser_master.username " & _
    '               "        ,cfuser_master.email " & _
    '               strSelect & " " & _
    '               "        ,LM.priority " & _
    '               "    FROM hrmsConfiguration..cfuser_master " & _
    '               "    JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
    '               "    JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
    '               strAccessJoin & " " & _
    '               "    JOIN " & strDatabaseName & "..hremp_appusermapping EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
    '               "    JOIN " & strDatabaseName & "..hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
    '                "    WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 " & _
    '               "        AND EM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " AND LM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
    '               "        /* AND companyunkid = @C */ AND yearunkid = @Y AND privilegeunkid = @P  " & _
    '                ") AS Fn ON #USR.userunkid = Fn.userunkid " & _
    '               strOuterJoin & " " & _
    '               "WHERE 1 = 1 "

    '        If mblnIsSubmitForApproaval = False Then
    '            StrQ &= " AND Fn.priority > " & intCurrentPriorityId
    '        End If

    '        objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
    '        objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
    '        objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApprovers; Module Name: " & mstrModuleName)
    '    Finally
    '        If objDataOpr Is Nothing Then objDataOperation = Nothing
    '    End Try
    '    Return dsList.Tables(0)
    'End Function

    Public Function GetNextEmployeeApprovers(ByVal strDatabaseName As String _
                                            , ByVal intCompanyId As Integer _
                                            , ByVal intYearId As Integer _
                                            , ByVal strEmpids As String _
                                            , ByVal strUserAccessMode As String _
                                            , ByVal intPrivilegeId As Integer _
                                            , Optional ByVal objDataOpr As clsDataOperation = Nothing _
                                            , Optional ByVal mblnIsSubmitForApproaval As Boolean = False _
                                            , Optional ByVal intCurrentPriorityId As Integer = -1 _
                                            ) As DataTable
        'S.SANDEEP [12-SEP-2018] -- START
        'ISSUE/ENHANCEMENT : {#2517}
        'intCurrentPriorityId = 0, intCurrentPriorityId = -1
        'S.SANDEEP [12-SEP-2018] -- END

        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            Dim strFilter As String = String.Empty
            Dim strJoin As String = String.Empty
            Dim strSelect As String = String.Empty
            Dim strAccessJoin As String = String.Empty
            Dim strOuterJoin As String = String.Empty
            Dim strFilterString As String = String.Empty

            Call GetUACFilters(strDatabaseName, strUserAccessMode, strFilter, strJoin, strSelect, strAccessJoin, strOuterJoin, strFilterString, 0, objDataOperation)

            'If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
            'Dim strvalues() As String = strUserAccessMode.Split(",")
            'Dim strFilter As String = String.Empty
            'Dim strJoin As String = String.Empty
            'Dim strSelect As String = String.Empty
            'Dim strAccessJoin As String = String.Empty
            'Dim strOuterJoin As String = String.Empty

            'For index As Integer = 0 To strvalues.Length - 1
            '    Select Case strvalues(index)
            '        Case enAllocation.BRANCH
            '            strSelect &= ",ISNULL(BR.branchid,0) AS branchid "
            '            strAccessJoin &= "    LEFT JOIN " & _
            '                             "    ( " & _
            '                             "        SELECT " & _
            '                             "             UPM.userunkid " & _
            '                             "            ,allocationunkid as branchid " & _
            '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
            '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
            '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.BRANCH) & "' " & _
            '                             "    ) AS BR ON BR.userunkid = cfuser_master.userunkid "
            '            strFilter &= "AND ISNULL(BR.branchid,0) > 0 "
            '            strJoin &= "AND Fn.branchid = [@emp].branchid "
            '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrstation_master ON Fn.branchid = hrstation_master.stationunkid "

            '        Case enAllocation.DEPARTMENT_GROUP
            '            strSelect &= ",ISNULL(DG.deptgrpid,0) AS deptgrpid "
            '            strAccessJoin &= "    LEFT JOIN " & _
            '                             "    ( " & _
            '                             "        SELECT " & _
            '                             "             UPM.userunkid " & _
            '                             "            ,allocationunkid as deptgrpid " & _
            '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
            '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
            '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT_GROUP) & "' " & _
            '                             "    ) AS DG ON DG.userunkid = cfuser_master.userunkid "
            '            strFilter &= "AND ISNULL(DG.deptgrpid,0) > 0 "
            '            strJoin &= "AND Fn.deptgrpid = [@emp].deptgrpid "
            '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_group_master ON Fn.deptgrpid = hrdepartment_group_master.deptgroupunkid "

            '        Case enAllocation.DEPARTMENT
            '            strSelect &= ",ISNULL(DP.deptid,0) AS deptid "
            '            strAccessJoin &= "    LEFT JOIN " & _
            '                             "    ( " & _
            '                             "        SELECT " & _
            '                             "             UPM.userunkid " & _
            '                             "            ,allocationunkid as deptid " & _
            '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
            '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
            '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT) & "' " & _
            '                             "    ) AS DP ON DP.userunkid = cfuser_master.userunkid "
            '            strFilter &= "AND ISNULL(DP.deptid,0) > 0 "
            '            strJoin &= "AND Fn.deptid = [@emp].deptid "
            '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_master ON Fn.deptid = hrdepartment_master.departmentunkid "

            '        Case enAllocation.SECTION_GROUP
            '            strSelect &= ",ISNULL(SG.secgrpid,0) AS secgrpid "
            '            strAccessJoin &= "    LEFT JOIN " & _
            '                             "    ( " & _
            '                             "        SELECT " & _
            '                             "             UPM.userunkid " & _
            '                             "            ,allocationunkid as secgrpid " & _
            '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
            '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
            '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION_GROUP) & "' " & _
            '                             "    ) AS SG ON SG.userunkid = cfuser_master.userunkid "
            '            strFilter &= "AND ISNULL(SG.secgrpid,0) > 0 "
            '            strJoin &= "AND Fn.secgrpid = [@emp].secgrpid "
            '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsectiongroup_master ON Fn.secgrpid = hrsectiongroup_master.sectiongroupunkid "

            '        Case enAllocation.SECTION
            '            strSelect &= ",ISNULL(SC.secid,0) AS secid "
            '            strAccessJoin &= "    LEFT JOIN " & _
            '                             "    ( " & _
            '                             "        SELECT " & _
            '                             "             UPM.userunkid " & _
            '                             "            ,allocationunkid as secid " & _
            '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
            '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
            '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION) & "' " & _
            '                             "    ) AS SC ON SC.userunkid = cfuser_master.userunkid "
            '            strFilter &= "AND ISNULL(SC.secid,0) > 0 "
            '            strJoin &= "AND Fn.secid = [@emp].secid "
            '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsection_master ON Fn.secid = hrsection_master.sectionunkid "

            '        Case enAllocation.UNIT_GROUP
            '            strSelect &= ",ISNULL(UG.unitgrpid,0) AS unitgrpid "
            '            strAccessJoin &= "    LEFT JOIN " & _
            '                             "    ( " & _
            '                             "        SELECT " & _
            '                             "             UPM.userunkid " & _
            '                             "            ,allocationunkid as unitgrpid " & _
            '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
            '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
            '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT_GROUP) & "' " & _
            '                             "    ) AS UG ON UG.userunkid = cfuser_master.userunkid "
            '            strFilter &= "AND ISNULL(UG.unitgrpid,0) > 0 "
            '            strJoin &= "AND Fn.unitgrpid = [@emp].unitgrpid "
            '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunitgroup_master ON Fn.unitgrpid = hrunitgroup_master.unitgroupunkid "

            '        Case enAllocation.UNIT
            '            strSelect &= ",ISNULL(UT.unitid,0) AS unitid "
            '            strAccessJoin &= "    LEFT JOIN " & _
            '                             "    ( " & _
            '                             "        SELECT " & _
            '                             "             UPM.userunkid " & _
            '                             "            ,allocationunkid as unitid " & _
            '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
            '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
            '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT) & "' " & _
            '                             "    ) AS UT ON UT.userunkid = cfuser_master.userunkid "
            '            strFilter &= "AND ISNULL(UT.unitid,0) > 0 "
            '            strJoin &= "AND Fn.unitid = [@emp].unitid "
            '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunit_master ON Fn.unitid = hrunit_master.unitunkid "

            '        Case enAllocation.TEAM
            '            strSelect &= ",ISNULL(TM.teamid,0) AS teamid "
            '            strAccessJoin &= "    LEFT JOIN " & _
            '                             "    ( " & _
            '                             "        SELECT " & _
            '                             "             UPM.userunkid " & _
            '                             "            ,allocationunkid as teamid " & _
            '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
            '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
            '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.TEAM) & "' " & _
            '                             "    ) AS TM ON TM.userunkid = cfuser_master.userunkid "
            '            strFilter &= "AND ISNULL(TM.teamid,0) > 0 "
            '            strJoin &= "AND Fn.teamid = [@emp].teamid "
            '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrteam_master ON Fn.teamid = hrteam_master.teamunkid "

            '        Case enAllocation.JOB_GROUP
            '            strSelect &= " ,ISNULL(JG.jgrpid,0) AS jgrpid "
            '            strAccessJoin &= "    LEFT JOIN " & _
            '                             "    ( " & _
            '                             "        SELECT " & _
            '                             "             UPM.userunkid " & _
            '                             "            ,allocationunkid as jgrpid " & _
            '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
            '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
            '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOB_GROUP) & "' " & _
            '                             "    ) AS JG ON JG.userunkid = cfuser_master.userunkid "
            '            strFilter &= "AND ISNULL(JG.jgrpid,0) > 0 "
            '            strJoin &= "AND Fn.jgrpid = [@emp].jgrpid "
            '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjobgroup_master ON Fn.jgrpid = hrjobgroup_master.jobgroupunkid "

            '        Case enAllocation.JOBS
            '            strSelect &= ",ISNULL(JB.jobid,0) AS jobid "
            '            strAccessJoin &= "    LEFT JOIN " & _
            '                             "    ( " & _
            '                             "        SELECT " & _
            '                             "             UPM.userunkid " & _
            '                             "            ,allocationunkid as jobid " & _
            '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
            '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
            '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOBS) & "' " & _
            '                             "    ) AS JB ON JB.userunkid = cfuser_master.userunkid "
            '            strFilter &= "AND ISNULL(JB.jobid,0) > 0 "
            '            strJoin &= "AND Fn.jobid = [@emp].jobid "
            '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjob_master ON Fn.jobid = hrjob_master.jobunkid "

            '        Case enAllocation.CLASS_GROUP
            '            strSelect &= ",ISNULL(CG.clsgrpid,0) AS clsgrpid "
            '            strAccessJoin &= "    LEFT JOIN " & _
            '                             "    ( " & _
            '                             "        SELECT " & _
            '                             "             UPM.userunkid " & _
            '                             "            ,allocationunkid as clsgrpid " & _
            '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
            '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
            '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASS_GROUP) & "' " & _
            '                             "    ) AS CG ON CG.userunkid = cfuser_master.userunkid "
            '            strFilter &= "AND ISNULL(CG.clsgrpid,0) > 0 "
            '            strJoin &= "AND Fn.clsgrpid = [@emp].clsgrpid "
            '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master ON Fn.clsgrpid = hrclassgroup_master.classgroupunkid "

            '        Case enAllocation.CLASSES
            '            strSelect &= ",ISNULL(CL.clsid,0) AS clsid "
            '            strAccessJoin &= "    LEFT JOIN " & _
            '                             "    ( " & _
            '                             "        SELECT " & _
            '                             "             UPM.userunkid " & _
            '                             "            ,allocationunkid as clsid " & _
            '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
            '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
            '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASSES) & "' " & _
            '                             "    ) AS CL ON CL.userunkid = cfuser_master.userunkid "
            '            strFilter &= "AND ISNULL(CL.clsid,0) > 0 "
            '            strJoin &= "AND Fn.clsid = [@emp].clsid "
            '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclasses_master ON Fn.clsid = classesunkid "

            '    End Select
            'Next
            'If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
            'If strJoin.Trim.Length > 0 Then strJoin = strJoin.Substring(3)

            StrQ &= "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
                    "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
                    "SELECT " & _
                    "     employeeunkid " & _
                    "    ,departmentunkid " & _
                    "    ,jobunkid " & _
                    "    ,classgroupunkid " & _
                    "    ,classunkid	" & _
                    "    ,stationunkid	" & _
                    "    ,deptgroupunkid	" & _
                    "    ,sectiongroupunkid	" & _
                    "    ,sectionunkid	" & _
                    "    ,unitgroupunkid	" & _
                    "    ,unitunkid	" & _
                    "    ,teamunkid	" & _
                    "    ,jobgroupunkid	" & _
                    "FROM " & strDatabaseName & "..hremployee_master " & _
                    "WHERE employeeunkid IN (" & strEmpids & ") " & _
                    "SELECT " & _
                    "     Fn.userunkid " & _
                    "    ,Fn.username " & _
                    "    ,Fn.email " & _
                    "    ,employeecode + ' - ' + firstname +' '+surname AS Employee " & _
                    "    ,ISNULL(EDP.name,'') AS Department " & _
                    "    ,ISNULL(EJB.job_name,'') AS Job " & _
                    "    ,ISNULL(ECG.name,'') AS ClassGroup " & _
                    "    ,ISNULL(ECM.name,'') AS Class " & _
                    "    ,Fn.priority " & _
                    "    ,employeeunkid " & _
                    "    ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY Fn.priority ASC) AS rno " & _
                    "FROM @emp " & _
                    "JOIN " & strDatabaseName & "..hremployee_master ON empid = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & strDatabaseName & "..hrdepartment_master AS EDP ON EDP.departmentunkid = hremployee_master.departmentunkid " & _
                    "LEFT JOIN " & strDatabaseName & "..hrjob_master AS EJB ON EJB.jobunkid = hremployee_master.jobunkid " & _
                    "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master AS ECG ON ECG.classgroupunkid = hremployee_master.classgroupunkid " & _
                    "LEFT JOIN " & strDatabaseName & "..hrclasses_master AS ECM ON ECM.classesunkid = hremployee_master.classunkid " & _
                    "JOIN " & _
                    "( " & _
                    "    SELECT DISTINCT " & _
                    "         cfuser_master.userunkid " & _
                    "        ,cfuser_master.username " & _
                    "        ,cfuser_master.email " & _
                    strSelect & " " & _
                    "        ,LM.priority " & _
                    "    FROM hrmsConfiguration..cfuser_master " & _
                    "    JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                    "    JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                    strAccessJoin & " " & _
                    "    JOIN " & strDatabaseName & "..hremp_appusermapping EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
                    "    JOIN " & strDatabaseName & "..hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
                    "    WHERE cfuser_master.isactive = 1 AND (" & strFilter & " ) AND EM.isactive = 1 AND EM.isvoid = 0 " & _
                    "        AND EM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " AND LM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
                    "        /* AND companyunkid = @C */ AND yearunkid = @Y AND privilegeunkid = @P  " & _
                    ") AS Fn ON " & strJoin & " " & _
                    strOuterJoin & " " & _
                    "WHERE 1 = 1 "

            'S.SANDEEP [26-SEP-2018] -- START
            '/*AND companyunkid = @C -- REMOVED AS ADMIN USER DOES NOT HAVE COMPANY ID*/
            'S.SANDEEP [26-SEP-2018] -- END

            If mblnIsSubmitForApproaval = False Then
                StrQ &= " AND Fn.priority > " & intCurrentPriorityId
            End If

            objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApprovers; Module Name: " & mstrModuleName)
        Finally
            If objDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList.Tables(0)
    End Function
    'S.SANDEEP |16-APR-2020| -- END

    Public Function GetEmployeeList(ByVal strDatabaseName As String, ByVal intCompanyId As Integer, ByVal intYearId As Integer, ByVal xUserModeSetting As String, ByVal intUserId As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            Dim StrAccess As String = ""
            StrAccess = GetUserAccessLevel(intUserId, intCompanyId, intYearId, xUserModeSetting)

            strQ = "SELECT ' ' AS employeecode, ' ' + @Select AS employeename, 0 AS employeeunkid,0 AS isapproved, ' ' + @Select AS EmpCodeName UNION ALL " & _
                   "SELECT " & _
                   "   hremployee_master.employeecode AS employeecode " & _
                   ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                   ",  hremployee_master.employeeunkid As employeeunkid " & _
                   ",  ISNULL(hremployee_master.isapproved,0) AS isapproved " & _
                   ",  ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                   "FROM hremployee_master " & _
                   "WHERE ISNULL(hremployee_master.isapproved,0) = 0 "
            If StrAccess.Trim.Length > 0 Then
                strQ &= " " & StrAccess
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 104, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dfView As DataView = dsList.Tables(0).DefaultView
            dfView.Sort = "employeename"
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dfView.ToTable)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function GetUserAccessLevel(ByVal intUserUnkId As Integer _
                                       , ByVal intComppanyUnkId As Integer _
                                       , ByVal intYearUnkId As Integer _
                                       , ByVal strReferenceUnkIds As String) As String
        Dim strAccessLevel As String = String.Empty
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim strFilter As String = String.Empty
        Try
            objDataOperation = New clsDataOperation


            objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId)
            objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intComppanyUnkId)
            objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearUnkId)

            If strReferenceUnkIds = "" Then strReferenceUnkIds = ConfigParameter._Object._UserAccessModeSetting
            Dim arrID As String() = strReferenceUnkIds.Split(",")
            For i = 0 To arrID.Length - 1
                StrQ = "SELECT " & _
                                  "STUFF((SELECT ',' + CAST(allocationunkid AS NVARCHAR(50)) " & _
                                  "FROM hrmsConfiguration..cfuseraccess_privilege_tran " & _
                                  "JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid " & _
                                  "WHERE userunkid = @UserId AND companyunkid = @CompanyId AND yearunkid = @YearId AND referenceunkid = " & arrID(i).ToString & " " & _
                                  "ORDER BY hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid FOR XML PATH('')),1,1,'') AS CSV "

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("List").Rows.Count > 0 Then
                    If dsList.Tables("List").Rows(0)("CSV").ToString.Trim.Length > 0 Then
                        strAccessLevel = dsList.Tables("List").Rows(0)("CSV").ToString
                    Else
                        strAccessLevel = "0"
                    End If
                Else
                    strAccessLevel = "0"
                End If

                If CInt(arrID(i)) = enAllocation.BRANCH Then
                    strFilter &= " AND  hremployee_master.stationunkid IN (" & strAccessLevel & ") "
                End If
                If CInt(arrID(i)) = enAllocation.DEPARTMENT_GROUP Then
                    strFilter &= " AND  hremployee_master.deptgroupunkid IN (" & strAccessLevel & ") "
                End If
                If CInt(arrID(i)) = enAllocation.DEPARTMENT Then
                    strFilter &= " AND  hremployee_master.departmentunkid IN (" & strAccessLevel & ") "
                End If
                If CInt(arrID(i)) = enAllocation.SECTION_GROUP Then
                    strFilter &= " AND  hremployee_master.sectiongroupunkid IN (" & strAccessLevel & ") "
                End If
                If CInt(arrID(i)) = enAllocation.SECTION Then
                    strFilter &= " AND  hremployee_master.sectionunkid IN (" & strAccessLevel & ") "
                End If
                If CInt(arrID(i)) = enAllocation.UNIT_GROUP Then
                    strFilter &= " AND  hremployee_master.unitgroupunkid IN (" & strAccessLevel & ") "
                End If
                If CInt(arrID(i)) = enAllocation.UNIT Then
                    strFilter &= " AND  hremployee_master.unitunkid IN (" & strAccessLevel & ") "
                End If
                If CInt(arrID(i)) = enAllocation.TEAM Then
                    strFilter &= " AND  hremployee_master.teamunkid IN (" & strAccessLevel & ") "
                End If
                If CInt(arrID(i)) = enAllocation.JOB_GROUP Then
                    strFilter &= " AND  hremployee_master.jobgroupunkid IN (" & strAccessLevel & ") "
                End If
                If CInt(arrID(i)) = enAllocation.JOBS Then
                    strFilter &= " AND hremployee_master.jobunkid IN (" & strAccessLevel & ") "
                End If
                If CInt(arrID(i)) = enAllocation.CLASS_GROUP Then
                    strFilter &= " AND hremployee_master.classgroupunkid IN (" & strAccessLevel & ") "
                End If
                If CInt(arrID(i)) = enAllocation.CLASSES Then
                    strFilter &= " AND hremployee_master.classunkid IN (" & strAccessLevel & ") "
                End If
            Next

            If strFilter = "" Then
                strFilter = " hremployee_master.departmentunkid IN (0) ) "
            End If
            Return strFilter
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetUserAccessLevel", mstrModuleName)
            Return strFilter
        Finally
        End Try
    End Function

    Public Sub SendSubmitNotification(ByVal strDatabaseName As String, ByVal intCompanyId As Integer, ByVal intYearId As Integer, ByVal strEmpUnkids As String, ByVal strUserAccessMode As String, ByVal intPrivilgeId As Integer, ByVal strFormName As String, ByVal eMode As enLogin_Mode, ByVal intLoginUserId As Integer, ByVal strSenderAddress As String, ByVal ArutiSelfServiceURL As String, ByVal iNotifyUpdate As Dictionary(Of Integer, String))
        Dim dtTable As DataTable
        Try
            dtTable = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strEmpUnkids, strUserAccessMode, intPrivilgeId, objDataOperation, True)
            Dim dr() As DataRow = dtTable.Select("rno = 1")
            If ArutiSelfServiceURL.Trim.Length <= 0 Then ArutiSelfServiceURL = ConfigParameter._Object._ArutiSelfServiceURL
            If dtTable IsNot Nothing Then
                Dim dtFilterTab As DataTable = dtTable.DefaultView.ToTable(True, "username", "email", "userunkid")
                For Each row In dtFilterTab.Rows
                    If row("email").ToString().Trim() = "" Then Continue For

                    Dim intUserId As Integer = row("userunkid")
                    Dim iList As List(Of Integer) = dr.AsEnumerable().Where(Function(x) x.Field(Of Integer)("userunkid") = intUserId).Select(Function(y) y.Field(Of Integer)("employeeunkid")).Distinct().ToList()

                    Dim intCnt As Integer = 0
                    intCnt = dtTable.Select("username = '" & row("username").ToString & "' AND rno = 1").Length
                    If intCnt > 0 Then
                        Dim StrMessage As New System.Text.StringBuilder
                        StrMessage.Append("<HTML><BODY>")
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language
                        'StrMessage.Append("Dear <b>" & row("username") & "</b></span></p>")
                        StrMessage.Append(Language.getMessage(mstrModuleName, 10, "Dear") & " " & "<b>" & " " & getTitleCase(row("username").ToString()) & "</b></span></p>")
                        'Gajanan [27-Mar-2019] -- End

                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language
                        'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that total (" & intCnt.ToString & ") employee(s) have been submitted for approval, please go approve them. </span></p>")
                        StrMessage.Append(Language.getMessage(mstrModuleName, 11, "This is to inform you that total") & " (" & intCnt.ToString & ")" & Language.getMessage(mstrModuleName, 12, "employee(s) have been submitted for approval, please go approve them.") & "</span></p>")
                        'Gajanan [27-Mar-2019] -- End


                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                        'S.SANDEEP [21-NOV-2018] -- START -- UAT (NMB) NEED TO REDIRECT TO NEW PAGE WHEN READY FOR WEB
                        'StrMessage.Append(Language.getMessage(mstrModuleName, 1, "Please click the link below to approve/reject employee.") & "</span></p>")
                        'StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                        'StrMessage.Append(ArutiSelfServiceURL & "/HR/wPgEmployeeProfile.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyId & "|" & row("userunkid") & "|" & intYearId & "|" & "1")) & "</span></p>")
                        'S.SANDEEP [21-NOV-2018] -- END


                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language
                        'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                        StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                        'Gajanan [27-Mar-2019] -- End

                        StrMessage.Append("</span></p>")
                        StrMessage.Append("</BODY></HTML>")

                        Dim objSendMail As New clsSendMail
                        objSendMail._ToEmail = row("email")
                        objSendMail._Subject = Language.getMessage(mstrModuleName, 3, "Notifications to Approve newly Hired Employee ")
                        objSendMail._Message = StrMessage.ToString()
                        objSendMail._Form_Name = strFormName
                        objSendMail._LogEmployeeUnkid = -1
                        objSendMail._OperationModeId = eMode
                        objSendMail._UserUnkid = intLoginUserId
                        objSendMail._SenderAddress = strSenderAddress
                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                        Dim strSuccess As String = String.Empty
                        strSuccess = objSendMail.SendMail(intCompanyId)

                        If strSuccess.Trim.Length <= 0 Then
                            If iList.Count > 0 Then
                                For Each item In iList
                                    If iNotifyUpdate.ContainsKey(item) = True Then
                                        Using objDo As New clsDataOperation
                                            objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, item)
                                            objDo.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, iNotifyUpdate(item))
                                            objDo.ExecNonQuery("UPDATE hremployeeapproval_tran SET isnotified = 1 WHERE employeeunkid = @employeeunkid AND tranguid = @tranguid ")
                                        End Using
                                    End If
                                Next
                            End If
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendSubmitNotification; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Sub SendApprlRejectNotification(ByVal blnIsRejection As Boolean, _
                                           ByVal strRejectionRemark As String, _
                                           ByVal strDatabaseName As String, _
                                           ByVal intCompanyId As Integer, _
                                           ByVal intYearId As Integer, _
                                           ByVal strEmpUnkids As String, _
                                           ByVal strUserAccessMode As String, _
                                           ByVal intPrivilgeId As Integer, _
                                           ByVal strFormName As String, _
                                           ByVal eMode As enLogin_Mode, _
                                           ByVal intLoginUserId As Integer, _
                                           ByVal strSenderAddress As String, _
                                           ByVal ArutiSelfServiceURL As String, _
                                           ByVal intCurrentPriority As Integer, _
                                           ByVal strRejectNotificationUserIds As String, _
                                           ByVal intRejectNotificationTemplateId As Integer)
        Dim dtTable As DataTable
        Try
            Dim mstrSubject As String = String.Empty
            If blnIsRejection Then
                mstrSubject = Language.getMessage(mstrModuleName, 5, "Notification for Employee Rejected")
            Else
                mstrSubject = Language.getMessage(mstrModuleName, 4, "Notification for Employee Approved")
            End If
            If blnIsRejection = False Then
                dtTable = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strEmpUnkids, strUserAccessMode, intPrivilgeId, objDataOperation, False, intCurrentPriority)
                If dtTable.Rows.Count > 0 Then
                    dtTable = New DataView(dtTable, "rno = 1", "", DataViewRowState.CurrentRows).ToTable()
                End If
            Else
                dtTable = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strEmpUnkids, strUserAccessMode, intPrivilgeId, objDataOperation, True)
                dtTable = New DataView(dtTable, "priority <= " & intCurrentPriority, "", DataViewRowState.CurrentRows).ToTable()
            End If

            Dim dtFilterTab As DataTable = dtTable.DefaultView.ToTable(True, "username", "email", "userunkid")

            If blnIsRejection Then
                For Each id As String In strRejectNotificationUserIds.Split(",")
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(id)
                    Dim drow As DataRow = dtFilterTab.NewRow()
                    drow("username") = objUser._Username
                    drow("email") = objUser._Email
                    drow("userunkid") = objUser._Userunkid
                    dtFilterTab.Rows.Add(drow)
                    objUser = Nothing
                Next
            End If



            For Each row In dtFilterTab.Rows
                Dim StrMessage As New System.Text.StringBuilder
                If row("email").ToString().Trim() = "" Then Continue For
                If blnIsRejection = False Then
                    Dim intCnt As Integer = 0
                    intCnt = dtTable.Select("username = '" & row("username").ToString & "' AND rno = 1").Length
                    StrMessage.Append("<HTML><BODY>")
                    StrMessage.Append(vbCrLf)
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'StrMessage.Append("Dear <b>" & row("username") & "</b></span></p>")
                    StrMessage.Append("Dear <b>" & getTitleCase(row("username")) & "</b></span></p>")
                    StrMessage.Append(Language.getMessage(mstrModuleName, 10, "Dear") & "<b>" & " " & getTitleCase(row("username")) & "</b></span></p>")
                    'Gajanan [27-Mar-2019] -- End

                    StrMessage.Append(vbCrLf)
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that total (" & intCnt.ToString & ") employee(s) have been approved by approver having lower priority than you, please go approve them. </span></p>")
                    StrMessage.Append(Language.getMessage(mstrModuleName, 11, "This is to inform you that total") & " " & "(" & intCnt.ToString & ")" & " " & Language.getMessage(mstrModuleName, 12, "employee(s) have been approved by approver having lower priority than you, please go approve them.") & "</span></p>")
                    'Gajanan [27-Mar-2019] -- End

                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    'S.SANDEEP [21-NOV-2018] -- START -- UAT (NMB) NEED TO REDIRECT TO NEW PAGE WHEN READY FOR WEB
                    'StrMessage.Append(Language.getMessage(mstrModuleName, 1, "Please click the link below to approve/reject employee.") & "</span></p>")
                    'StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    'StrMessage.Append(ArutiSelfServiceURL & "/HR/wPgEmployeeProfile.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyId & "|" & row("userunkid") & "|" & intYearId & "|" & "1")) & "</span></p>")
                    'S.SANDEEP [21-NOV-2018] -- END
                    StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                    StrMessage.Append("</span></p>")
                    StrMessage.Append("</BODY></HTML>")
                Else
                    Dim intCnt As Integer = 0

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'intCnt = dtTable.Select("username = '" & row("username").ToString & "' AND rno = 1").Length
                    intCnt = dtTable.Select("username = '" & getTitleCase(row("username").ToString) & "' AND rno = 1").Length
                    'Gajanan [27-Mar-2019] -- End


                    'S.SANDEEP [21-NOV-2018] -- START
                    If intCnt <= 0 Then
                        intCnt = dtTable.Select("rno = 1").Length
                    End If
                    'S.SANDEEP [21-NOV-2018] -- END
                    StrMessage.Append("<HTML><BODY>")
                    StrMessage.Append(vbCrLf)
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'StrMessage.Append("Dear <b>" & row("username") & "</b></span></p>")
                    StrMessage.Append("Dear <b>" & getTitleCase(row("username")) & "</b></span></p>")
                    'Gajanan [27-Mar-2019] -- End

                    StrMessage.Append(vbCrLf)
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that total (" & intCnt.ToString & ")  : " & strRejectionRemark & "</span></p>")
                    StrMessage.Append(Language.getMessage(mstrModuleName, 11, "This is to inform you that total") & " " & "(" & intCnt.ToString & ")" & " " & Language.getMessage(mstrModuleName, 13, "employee(s) have been rejected by approver with remark") & ":" & strRejectionRemark & "</span></p>")
                    'Gajanan [27-Mar-2019] -- End


                    StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; please login to aruti to check.</span></p>")
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    'S.SANDEEP [21-NOV-2018] -- START -- UAT (NMB) NEED TO REDIRECT TO NEW PAGE WHEN READY FOR WEB
                    'StrMessage.Append(Language.getMessage(mstrModuleName, 1, "Please click the link below to approve/reject employee.") & "</span></p>")
                    'StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    'StrMessage.Append(ArutiSelfServiceURL & "/HR/wPgEmployeeProfile.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyId & "|" & row("userunkid") & "|" & intYearId & "|" & "1")) & "</span></p>")
                    'S.SANDEEP [21-NOV-2018] -- END

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language

                    'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                    StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                    'Gajanan [27-Mar-2019] -- End


                    StrMessage.Append("</span></p>")
                    StrMessage.Append("</BODY></HTML>")
                End If

                Dim objSendMail As New clsSendMail
                objSendMail._ToEmail = row("email").ToString().Trim()
                objSendMail._Subject = mstrSubject
                objSendMail._Message = StrMessage.ToString()
                objSendMail._Form_Name = strFormName
                objSendMail._LogEmployeeUnkid = -1
                objSendMail._OperationModeId = eMode
                objSendMail._UserUnkid = intLoginUserId
                objSendMail._SenderAddress = strSenderAddress
                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                objSendMail.SendMail(intCompanyId)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendApprlRejectNotification; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP [26-SEP-2018] -- START
    Private Function PostData(ByVal strxmldata As String, ByVal strSrvURL As String, ByRef strError As String) As String
        Dim strResponseData As String = String.Empty
        Try
            Dim xmlDoc As XmlDocument = New XmlDocument()
            xmlDoc.LoadXml(strxmldata)
            Dim request As HttpWebRequest = CType(WebRequest.Create(strSrvURL), HttpWebRequest)
            Dim bytes As Byte()
            bytes = System.Text.Encoding.ASCII.GetBytes(xmlDoc.InnerXml)
            request.ContentType = "text/xml"
            request.ContentLength = bytes.Length
            request.Method = "POST"
            Dim requestStream As Stream = request.GetRequestStream()
            requestStream.Write(bytes, 0, bytes.Length)
            requestStream.Close()
            Using responseReader As New StreamReader(request.GetResponse().GetResponseStream())
                Dim result As String = responseReader.ReadToEnd()
                Dim ResultXML As XDocument = XDocument.Parse(result)
                strResponseData = ResultXML.ToString()
            End Using
        Catch ex As WebException
            If ex.InnerException IsNot Nothing Then
                strError = ex.InnerException.ToString() & Environment.NewLine & ex.Message
            Else
                strError = ex.Message
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: PostData; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strResponseData
    End Function
    'S.SANDEEP [26-SEP-2018] -- END


    'S.SANDEEP |25-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : FlexCube
    Private Sub SendFlex_Notification(ByVal strFailedEmailNotification As String, _
                                     ByVal mDicError As Dictionary(Of String, String), _
                                     ByVal intCompanyId As Integer, _
                                     ByVal strSubject As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal iLoginEmployeeId As Integer)
        Try
            Dim arrEmail() As String = Nothing
            If strFailedEmailNotification.Trim.Contains(",") Then
                arrEmail = strFailedEmailNotification.Split(",")
            ElseIf strFailedEmailNotification.Trim.Contains(";") Then
                arrEmail = strFailedEmailNotification.Split(";")
            End If
            If arrEmail.Length > 0 Then
                Dim objComp As New clsCompany_Master
                objComp._Companyunkid = intCompanyId
                Dim strSenderAddress, strSendName As String
                strSenderAddress = "" : strSendName = ""
                strSenderAddress = objComp._Senderaddress
                strSendName = objComp._Sendername

                Dim strMailBody As String = String.Empty
                If mDicError IsNot Nothing AndAlso mDicError.Keys.Count > 0 Then
                    strMailBody = "<table style='width: 100%;' border='1'>" & vbCrLf
                    strMailBody &= "<tr style='width: 100%;'>" & vbCrLf
                    For Each iKey As String In mDicError.Keys
                        strMailBody &= "<td bgcolor = '#5D7B9D' border='1'><b><font color='#fff'>" & iKey & "</font></b></td>" & vbCrLf
                    Next
                    strMailBody &= "</tr>" & vbCrLf
                    strMailBody &= "<tr style='width: 100%;'>" & vbCrLf
                    For Each iKey As String In mDicError.Keys
                        strMailBody &= "<td border='1'>" & mDicError(iKey) & "</td>" & vbCrLf
                    Next
                    strMailBody &= "</tr>" & vbCrLf
                    strMailBody &= "</table>"
                End If
                Dim objMail As New clsSendMail
                objMail._Form_Name = mstrForm_Name
                objMail._WebClientIP = mstrIp
                objMail._WebHostName = mstrHostname
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = enLogin_Mode.DESKTOP
                objMail._UserUnkid = xUserUnkid
                objMail._Subject = strSubject
                objMail._Message = strMailBody
                objMail._SenderAddress = IIf(strSenderAddress.Trim.Length <= 0, strSendName, strSenderAddress)
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                For i As Integer = 0 To arrEmail.Length - 1
                    objMail._ToEmail = arrEmail(i)
                    objMail.SendMail(intCompanyId)
                Next
                objMail = Nothing
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendFlex_Notification; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |25-NOV-2019| -- END


    'Sohail (27 Nov 2018) -- Start
    'NMB Issue - Email is sent to employee everytime whenver employee is updated and send company details to newly approved employee option is ticked in 75.1.
    Public Shared Function Set_Notification_Employee_EmailPassword(ByVal strFirstname As String, ByVal strSurname As String, ByVal strEmail As String, ByVal strPassword As String) As String
        Dim StrMessage As New System.Text.StringBuilder
        Try
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append(Language.getMessage(mstrModuleName, 5, "Dear") & " <b>" & strFirstname & " " & strSurname & "</b></span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 5, "Dear") & " <b>" & getTitleCase(strFirstname & " " & strSurname) & "</b></span></p>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & Language.getMessage(mstrModuleName, 6, "This mail to inform you that you have been approved as an employee.") & "</span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 6, "This mail to inform you that you have been approved as an employee.") & "</span></p>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & Language.getMessage(mstrModuleName, 7, "Please find the your basic details below :") & "</span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 7, "Please find the your basic details below :") & "</span></p>")
            'Gajanan [27-Mar-2019] -- End


            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & Language.getMessage(mstrModuleName, 8, "Company Email :") & " <b>" & strEmail & "</b></span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 8, "Company Email :") & " <b>" & strEmail & "</b></span></p>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & Language.getMessage(mstrModuleName, 9, "Password :") & " <b> " & strPassword & "</b></span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 9, "Password :") & " <b> " & strPassword & "</b></span></p>")
            'Gajanan [27-Mar-2019] -- End


            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            Return StrMessage.ToString

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Set_Notification_Employee_EmailPassword; Module Name: " & mstrModuleName)
            Return ""
        End Try
    End Function
    'Sohail (27 Nov 2018) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 3, "Notifications to Approve newly Hired Employee")
			Language.setMessage(mstrModuleName, 4, "Notification for Employee Approved")
			Language.setMessage(mstrModuleName, 5, "Notification for Employee Rejected")
			Language.setMessage(mstrModuleName, 6, "This mail to inform you that you have been approved as an employee.")
			Language.setMessage(mstrModuleName, 7, "Please find the your basic details below :")
			Language.setMessage(mstrModuleName, 8, "Company Email :")
			Language.setMessage(mstrModuleName, 9, "Password :")
			Language.setMessage(mstrModuleName, 10, "Dear")
			Language.setMessage(mstrModuleName, 11, "This is to inform you that total")
			Language.setMessage(mstrModuleName, 12, "employee(s) have been submitted for approval, please go approve them.")
			Language.setMessage(mstrModuleName, 13, "employee(s) have been rejected by approver with remark")
            Language.setMessage(mstrModuleName, 100, "Pending")
            Language.setMessage(mstrModuleName, 101, "Approved")
            Language.setMessage(mstrModuleName, 102, "Rejected")
			Language.setMessage(mstrModuleName, 103, "Notification Email & Password")
			Language.setMessage(mstrModuleName, 104, "Select")
			Language.setMessage(mstrModuleName, 5, "Dear")
			Language.setMessage(mstrModuleName, 12, "employee(s) have been approved by approver having lower priority than you, please go approve them.")
            Language.setMessage(mstrModuleName, 103, "Pending for Approval")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class