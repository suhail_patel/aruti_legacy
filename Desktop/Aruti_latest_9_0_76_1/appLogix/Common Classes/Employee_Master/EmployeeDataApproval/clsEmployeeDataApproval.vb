﻿Option Strict On
'************************************************************************************************************************************
'Class Name :clsEmployeeDataApproval.vb
'Purpose    :
'Date       :20-Dec-2018
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports System

''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsEmployeeDataApproval
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeDataApproval"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""


#Region " Private Variables "

    Private mintPrivilegeId As Integer
    Private eScreenType As enScreenName
    Private mintEmpId As Integer
    Private eOperationType As enOperationType


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mdtBenefitTran As DataTable = Nothing
    Private mdtMemTran As DataTable = Nothing
    'Gajanan [22-Feb-2019] -- End

#End Region

#Region " Enum "

    Public Enum enOperationType

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        NONE = 0
        'Gajanan [17-DEC-2018] -- End
        ADDED = 1
        EDITED = 2
        DELETED = 3
    End Enum

    'Gajanan [9-April-2019] -- Start
    Public Enum enToEmailType
        USER = 1
        EMPLOYEE = 2
    End Enum
    'Gajanan [9-April-2019] -- End


#End Region

#Region " Properties "

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public ReadOnly Property _PrivilegeId() As Integer
        Get
            Return mintPrivilegeId
        End Get
    End Property

    Public ReadOnly Property _ScreenType() As enScreenName
        Get
            Return eScreenType
        End Get
    End Property

    Public ReadOnly Property _EmpId() As Integer
        Get
            Return mintEmpId
        End Get
    End Property

    Public ReadOnly Property _OperationType() As enOperationType
        Get
            Return eOperationType
        End Get
    End Property

#End Region

#Region " Constructor "

    Public Sub New()

    End Sub

    Public Sub New(ByVal intPrivilegeId As Integer, ByVal eScreen As enScreenName, ByVal iEmpId As Integer, ByVal eOpernType As enOperationType)
        mintPrivilegeId = intPrivilegeId
        eScreenType = eScreen
        mintEmpId = iEmpId
        eOperationType = eOpernType
    End Sub

#End Region

#Region " Private/Public Methods "

    Public Function EmployeeDataForNotification(ByVal iCompanyId As Integer, ByVal iLanguageId As Integer, Optional ByVal iYearId As Integer = 0, Optional ByVal sFilter As String = "") As DataTable
        Dim dList As New DataSet
        Dim dFinalList As New DataTable("List")
        Try
            dList = EmployeeDataApprovalList(1, iCompanyId, iLanguageId, "List", False)
            Dim dCol As DataColumn = Nothing
            dCol = New DataColumn
            With dCol
                .DataType = GetType(Boolean)
                .ColumnName = "iCheck"
                .DefaultValue = False
            End With
            dFinalList.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .DataType = GetType(String)
                .ColumnName = "screen"
                .DefaultValue = ""
            End With
            dFinalList.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .DataType = GetType(String)
                .ColumnName = "userids"
                .DefaultValue = ""
            End With
            dFinalList.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .DataType = GetType(Int32)
                .ColumnName = "screenid"
                .DefaultValue = 0
            End With
            dFinalList.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .DataType = GetType(Int32)
                .ColumnName = "PrivilegeId"
                .DefaultValue = 0
            End With
            dFinalList.Columns.Add(dCol)

            If dList IsNot Nothing Then
                For Each dr As DataRow In dList.Tables(0).Rows
                    Dim dtrow As DataRow = dFinalList.NewRow()
                    dtrow("screen") = dr("Name").ToString
                    dtrow("screenid") = dr("Id").ToString
                    dtrow("PrivilegeId") = dr("PrivilegeId").ToString
                    dFinalList.Rows.Add(dtrow)
                Next
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: EmployeeDataForNotification; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dFinalList
    End Function

    Public Function EmployeeDataApprovalList(ByVal intUserId As Integer, _
                                             ByVal iCompanyId As Integer, _
                                             ByVal iLanguageId As Integer, _
                                             Optional ByVal strList As String = "List", _
                                             Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim dList As DataTable = Nothing
        Dim objMasterData As New clsMasterData
        Try
            If strList.Trim.Length <= 0 Then strList = "List"

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'StrQ = "SELECT DISTINCT " & _
            '       "     PM.privilegeunkid AS Id " & _
            '       "    ,PM.privilege_name AS Name " & _
            '       "FROM hrmsConfiguration..cfuser_privilege AS UP " & _
            '       "    JOIN hrmsConfiguration..cfuserprivilege_master AS PM ON PM.privilegeunkid = UP.privilegeunkid " & _
            '       "WHERE PM.isactive = 1 AND PM.isdeleted = 'N' AND UP.userunkid = @userunkid AND PM.privilegeunkid IN " & _
            '       " ( " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeQualifications) & _
            '        ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeReferences) & _
            '        ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills) & _
            '        ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences) & " ) "


            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            'Dim strInnList As String = String.Empty
            'strInnList = CInt(enUserPriviledge.AllowToApproveRejectEmployeeQualifications) & _
            '             ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeReferences) & _
            '             ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills) & _
            '             ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences) & _
            '             ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeIdentities) & _
            '             ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants)

            Dim strInnList As String = String.Empty
            strInnList = CInt(enUserPriviledge.AllowToApproveRejectEmployeeQualifications) & _
                         ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeReferences) & _
                         ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills) & _
                         ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences) & _
                         ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeIdentities) & _
                         ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants) & _
                         ", " & CInt(enUserPriviledge.AllowToApproveRejectAddress) & _
                         ", " & CInt(enUserPriviledge.AllowToApproveRejectEmergencyAddress) & _
                         ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeMemberships) & _
                         ", " & CInt(enUserPriviledge.AllowToApproveRejectPersonalInfo)
            'S.SANDEEP |15-APR-2019| -- START
            ' ------- ", " & CInt(enUserPriviledge.AllowToApproveRejectEmployeeMemberships) > ADDED
            ' ------- ", " & CInt(enUserPriviledge.AllowToApproveRejectPersonalInfo) > ADDED
            'S.SANDEEP |15-APR-2019| -- END

            'Gajanan [18-Mar-2019] -- End

            StrQ = "SELECT DISTINCT " & _
                   "     PM.privilegeunkid AS Id " & _
                   "    ,PM.privilege_name AS Name " & _
                   "FROM hrmsConfiguration..cfuser_privilege AS UP " & _
                   "    JOIN hrmsConfiguration..cfuserprivilege_master AS PM ON PM.privilegeunkid = UP.privilegeunkid " & _
                   "WHERE PM.isactive = 1 AND PM.isdeleted = 'N' AND UP.userunkid = @userunkid AND PM.privilegeunkid IN " & _
                   " ( " & strInnList & " ) "

            'Gajanan [22-Feb-2019] -- End

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            StrQ = ""
            If blnAddSelect Then
                StrQ = "SELECT 0 AS Id, @select AS Name, 0 AS PrivilegeId UNION "
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                dList = objMasterData.GetScreenNameForApproverFlowOnEmployeeData(iCompanyId, iLanguageId)
                Dim drtemp() As DataRow = Nothing
                For Each row As DataRow In dsList.Tables(0).Rows
                    Select Case CInt(row("Id"))
                        Case enUserPriviledge.AllowToApproveRejectEmployeeQualifications
                            drtemp = dList.Select("Id = '" & CInt(enScreenName.frmQualificationsList) & "'")
                            If drtemp.Length > 0 Then
                                StrQ &= "SELECT " & enScreenName.frmQualificationsList & " AS Id, '" & drtemp(0)("ScreenName").ToString() & "' AS Name, '" & CInt(row("Id")) & "' AS PrivilegeId UNION "
                            End If
                        Case enUserPriviledge.AllowToApproveRejectEmployeeReferences
                            drtemp = dList.Select("Id = '" & CInt(enScreenName.frmEmployeeRefereeList) & "'")
                            If drtemp.Length > 0 Then
                                StrQ &= "SELECT " & enScreenName.frmEmployeeRefereeList & " AS Id, '" & drtemp(0)("ScreenName").ToString() & "' AS Name, '" & CInt(row("Id")) & "' AS PrivilegeId UNION "
                            End If

                        Case enUserPriviledge.AllowToApproveRejectEmployeeSkills
                            drtemp = dList.Select("Id = '" & CInt(enScreenName.frmEmployee_Skill_List) & "'")
                            If drtemp.Length > 0 Then
                                StrQ &= "SELECT " & enScreenName.frmEmployee_Skill_List & " AS Id, '" & drtemp(0)("ScreenName").ToString() & "' AS Name, '" & CInt(row("Id")) & "' AS PrivilegeId UNION "
                            End If

                        Case enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences
                            drtemp = dList.Select("Id = '" & CInt(enScreenName.frmJobHistory_ExperienceList) & "'")
                            If drtemp.Length > 0 Then
                                StrQ &= "SELECT " & enScreenName.frmJobHistory_ExperienceList & " AS Id, '" & drtemp(0)("ScreenName").ToString() & "' AS Name, '" & CInt(row("Id")) & "' AS PrivilegeId UNION "
                            End If

                            'Gajanan [22-Feb-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        Case enUserPriviledge.AllowToApproveRejectEmployeeIdentities
                            drtemp = dList.Select("Id = '" & CInt(enScreenName.frmIdentityInfoList) & "'")
                            If drtemp.Length > 0 Then
                                StrQ &= "SELECT " & enScreenName.frmIdentityInfoList & " AS Id, '" & drtemp(0)("ScreenName").ToString() & "' AS Name, '" & CInt(row("Id")) & "' AS PrivilegeId UNION "
                            End If


                        Case enUserPriviledge.AllowToApproveRejectEmployeeDependants
                            drtemp = dList.Select("Id = '" & CInt(enScreenName.frmDependantsAndBeneficiariesList) & "'")
                            If drtemp.Length > 0 Then
                                StrQ &= "SELECT " & enScreenName.frmDependantsAndBeneficiariesList & " AS Id, '" & drtemp(0)("ScreenName").ToString() & "' AS Name, '" & CInt(row("Id")) & "' AS PrivilegeId UNION "
                            End If
                            'Gajanan [22-Feb-2019] -- End


                            'Gajanan [18-Mar-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        Case enUserPriviledge.AllowToApproveRejectAddress
                            drtemp = dList.Select("Id = '" & CInt(enScreenName.frmAddressList) & "'")
                            If drtemp.Length > 0 Then
                                StrQ &= "SELECT " & enScreenName.frmAddressList & " AS Id, '" & drtemp(0)("ScreenName").ToString() & "' AS Name, '" & CInt(row("Id")) & "' AS PrivilegeId UNION "
                            End If

                        Case enUserPriviledge.AllowToApproveRejectEmergencyAddress
                            drtemp = dList.Select("Id = '" & CInt(enScreenName.frmEmergencyAddressList) & "'")
                            If drtemp.Length > 0 Then
                                StrQ &= "SELECT " & enScreenName.frmEmergencyAddressList & " AS Id, '" & drtemp(0)("ScreenName").ToString() & "' AS Name, '" & CInt(row("Id")) & "' AS PrivilegeId UNION "
                            End If

                            'Gajanan [18-Mar-2019] -- End

                            'S.SANDEEP |15-APR-2019| -- START
                        Case enUserPriviledge.AllowToApproveRejectEmployeeMemberships
                            drtemp = dList.Select("Id = '" & CInt(enScreenName.frmMembershipInfoList) & "'")
                            If drtemp.Length > 0 Then
                                StrQ &= "SELECT " & enScreenName.frmMembershipInfoList & " AS Id, '" & drtemp(0)("ScreenName").ToString() & "' AS Name, '" & CInt(row("Id")) & "' AS PrivilegeId UNION "
                            End If
                            'S.SANDEEP |15-APR-2019| -- END


                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        Case enUserPriviledge.AllowToApproveRejectPersonalInfo
                            drtemp = dList.Select("Id = '" & CInt(enScreenName.frmBirthinfo) & "'")
                            If drtemp.Length > 0 Then
                                StrQ &= "SELECT " & enScreenName.frmBirthinfo & " AS Id, '" & drtemp(0)("ScreenName").ToString() & "' AS Name, '" & CInt(row("Id")) & "' AS PrivilegeId UNION "
                            End If

                            drtemp = dList.Select("Id = '" & CInt(enScreenName.frmOtherinfo) & "'")
                            If drtemp.Length > 0 Then
                                StrQ &= "SELECT " & enScreenName.frmOtherinfo & " AS Id, '" & drtemp(0)("ScreenName").ToString() & "' AS Name, '" & CInt(row("Id")) & "' AS PrivilegeId UNION "
                            End If

                            'Gajanan [17-April-2019] -- End

                    End Select
                Next

                If StrQ.Trim.Length > 0 Then StrQ = StrQ.Substring(0, StrQ.Length - 6)

                objDataOperation.AddParameter("@select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

                dsList = objDataOperation.ExecQuery(StrQ, strList)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: EmployeeDataApprovalList; Module Name: " & mstrModuleName)
        Finally
            objMasterData = Nothing
        End Try
        Return dsList
    End Function

    Public Sub GetUACFilters(ByVal strDatabaseName As String, _
                             ByVal strUserAccessMode As String, _
                             ByRef strFilter As String, _
                             ByRef strJoin As String, _
                             ByRef strSelect As String, _
                             ByRef strAccessJoin As String, _
                             ByRef strOuterJoin As String, _
                             ByRef strFinalString As String, _
                             ByVal intUserId As Integer, _
                             Optional ByVal xDataOpr As clsDataOperation = Nothing)

        Dim objDataOperation As New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim StrQ As String = ""
        Try
            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
            Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
            For index As Integer = 0 To strvalues.Length - 1
                Select Case CInt(strvalues(index))
                    Case enAllocation.BRANCH
                        strSelect &= ",ISNULL(BR.branchid,0) AS branchid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as branchid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.BRANCH) & "' " & _
                                         "    ) AS BR ON BR.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(BR.branchid,0) > 0 "
                        strJoin &= "AND Fn.branchid = [@emp].branchid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrstation_master ON Fn.branchid = hrstation_master.stationunkid "

                    Case enAllocation.DEPARTMENT_GROUP
                        strSelect &= ",ISNULL(DG.deptgrpid,0) AS deptgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as deptgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT_GROUP) & "' " & _
                                         "    ) AS DG ON DG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(DG.deptgrpid,0) > 0 "
                        strJoin &= "AND Fn.deptgrpid = [@emp].deptgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_group_master ON Fn.deptgrpid = hrdepartment_group_master.deptgroupunkid "

                    Case enAllocation.DEPARTMENT
                        strSelect &= ",ISNULL(DP.deptid,0) AS deptid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as deptid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT) & "' " & _
                                         "    ) AS DP ON DP.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(DP.deptid,0) > 0 "
                        strJoin &= "AND Fn.deptid = [@emp].deptid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_master ON Fn.deptid = hrdepartment_master.departmentunkid "

                    Case enAllocation.SECTION_GROUP
                        strSelect &= ",ISNULL(SG.secgrpid,0) AS secgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as secgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION_GROUP) & "' " & _
                                         "    ) AS SG ON SG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(SG.secgrpid,0) > 0 "
                        strJoin &= "AND Fn.secgrpid = [@emp].secgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsectiongroup_master ON Fn.secgrpid = hrsectiongroup_master.sectiongroupunkid "

                    Case enAllocation.SECTION
                        strSelect &= ",ISNULL(SC.secid,0) AS secid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as secid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION) & "' " & _
                                         "    ) AS SC ON SC.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(SC.secid,0) > 0 "
                        strJoin &= "AND Fn.secid = [@emp].secid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsection_master ON Fn.secid = hrsection_master.sectionunkid "

                    Case enAllocation.UNIT_GROUP
                        strSelect &= ",ISNULL(UG.unitgrpid,0) AS unitgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as unitgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT_GROUP) & "' " & _
                                         "    ) AS UG ON UG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(UG.unitgrpid,0) > 0 "
                        strJoin &= "AND Fn.unitgrpid = [@emp].unitgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunitgroup_master ON Fn.unitgrpid = hrunitgroup_master.unitgroupunkid "

                    Case enAllocation.UNIT
                        strSelect &= ",ISNULL(UT.unitid,0) AS unitid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as unitid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT) & "' " & _
                                         "    ) AS UT ON UT.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(UT.unitid,0) > 0 "
                        strJoin &= "AND Fn.unitid = [@emp].unitid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunit_master ON Fn.unitid = hrunit_master.unitunkid "

                    Case enAllocation.TEAM
                        strSelect &= ",ISNULL(TM.teamid,0) AS teamid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as teamid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.TEAM) & "' " & _
                                         "    ) AS TM ON TM.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(TM.teamid,0) > 0 "
                        strJoin &= "AND Fn.teamid = [@emp].teamid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrteam_master ON Fn.teamid = hrteam_master.teamunkid "

                    Case enAllocation.JOB_GROUP
                        strSelect &= " ,ISNULL(JG.jgrpid,0) AS jgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as jgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOB_GROUP) & "' " & _
                                         "    ) AS JG ON JG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(JG.jgrpid,0) > 0 "
                        strJoin &= "AND Fn.jgrpid = [@emp].jgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjobgroup_master ON Fn.jgrpid = hrjobgroup_master.jobgroupunkid "

                    Case enAllocation.JOBS
                        strSelect &= ",ISNULL(JB.jobid,0) AS jobid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as jobid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOBS) & "' " & _
                                         "    ) AS JB ON JB.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(JB.jobid,0) > 0 "
                        strJoin &= "AND Fn.jobid = [@emp].jobid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjob_master ON Fn.jobid = hrjob_master.jobunkid "

                    Case enAllocation.CLASS_GROUP
                        strSelect &= ",ISNULL(CG.clsgrpid,0) AS clsgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as clsgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASS_GROUP) & "' " & _
                                         "    ) AS CG ON CG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(CG.clsgrpid,0) > 0 "
                        strJoin &= "AND Fn.clsgrpid = [@emp].clsgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master ON Fn.clsgrpid = hrclassgroup_master.classgroupunkid "

                    Case enAllocation.CLASSES
                        strSelect &= ",ISNULL(CL.clsid,0) AS clsid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as clsid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASSES) & "' " & _
                                         "    ) AS CL ON CL.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(CL.clsid,0) > 0 "
                        strJoin &= "AND Fn.clsid = [@emp].clsid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclasses_master ON Fn.clsid = hrclasses_master.classesunkid "
                End Select

                objDataOperation.ClearParameters()
                StrQ = "SELECT @Value = ISNULL(STUFF((SELECT DISTINCT ',' + CAST(allocationunkid AS NVARCHAR(MAX)) FROM hrmsConfiguration..cfuseraccess_privilege_tran WHERE useraccessprivilegeunkid IN (SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE userunkid = " & intUserId & " AND referenceunkid = " & strvalues(index) & ") FOR XML PATH('')),1,1,''),'') "
                Dim strvalue = ""
                objDataOperation.AddParameter("@Value", SqlDbType.NVarChar, 4000, strvalue, ParameterDirection.InputOutput)

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If

                strvalue = CStr(objDataOperation.GetParameterValue("@Value"))

                If strvalue.Trim.Length > 0 Then
                    Select Case CInt(strvalues(index))
                        Case enAllocation.BRANCH
                            strFinalString &= " AND Fn.branchid IN (" & strvalue & ") "

                        Case enAllocation.DEPARTMENT_GROUP
                            strFinalString &= " AND Fn.deptgrpid IN (" & strvalue & ") "

                        Case enAllocation.DEPARTMENT
                            strFinalString &= " AND Fn.deptid IN (" & strvalue & ") "

                        Case enAllocation.SECTION_GROUP
                            strFinalString &= " AND Fn.secgrpid IN (" & strvalue & ") "

                        Case enAllocation.SECTION
                            strFinalString &= " AND Fn.secid IN (" & strvalue & ") "

                        Case enAllocation.UNIT_GROUP
                            strFinalString &= " AND Fn.unitgrpid IN (" & strvalue & ") "

                        Case enAllocation.UNIT
                            strFinalString &= " AND Fn.unitid IN (" & strvalue & ") "

                        Case enAllocation.TEAM
                            strFinalString &= " AND Fn.teamid IN (" & strvalue & ") "

                        Case enAllocation.JOB_GROUP
                            strFinalString &= " AND Fn.jgrpid IN (" & strvalue & ") "

                        Case enAllocation.JOBS
                            strFinalString &= " AND Fn.jobid IN (" & strvalue & ") "

                        Case enAllocation.CLASS_GROUP
                            strFinalString &= " AND Fn.clsgrpid IN (" & strvalue & ") "

                        Case enAllocation.CLASSES
                            strFinalString &= " AND Fn.clsid IN (" & strvalue & ") "

                    End Select
                End If
            Next
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
            If strJoin.Trim.Length > 0 Then strJoin = strJoin.Substring(3)
        Catch ex As Exception
                 'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
               'Gajanan [17-April-2019] -- Start
            Throw New Exception(ex.Message & "; Procedure Name: GetUACFilters; Module Name: " & mstrModuleName)
        Finally
                 'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

            If xDataOpr Is Nothing Then objDataOperation = Nothing
                 'Gajanan [17-April-2019] -- End
        End Try
    End Sub

    Public Function GetEmployeeApprovalData(ByVal strDatabaseName As String _
                                          , ByVal intCompanyId As Integer _
                                          , ByVal intYearId As Integer _
                                          , ByVal strUserAccessMode As String _
                                          , ByVal intPrivilegeId As Integer _
                                          , ByVal intUserId As Integer _
                                          , ByVal intPriorityId As Integer _
                                          , ByVal blnOnlyMyApprovals As Boolean _
                                          , ByVal eScreenType As enScreenName _
                                          , ByVal xEmployeeAsOnDate As String _
                                          , ByVal strAdvFilter As String _
                                          , ByVal eOperationType As enOperationType _
                                          , Optional ByVal objDataOpr As clsDataOperation = Nothing _
                                          , Optional ByVal blnFromWebGroup As Boolean = False) As DataTable
        Dim StrQ As String = String.Empty
        Dim dList As DataTable = Nothing
        Dim oData As DataTable = Nothing
        Try
            dList = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, eScreenType, xEmployeeAsOnDate, intUserId, eOperationType, Nothing, , , strAdvFilter, blnFromWebGroup)
            oData = dList.Copy()
            If dList.Rows.Count > 0 Then
                Dim blnFlag As Boolean = False

                'Gajanan [17-DEC-2018] -- Start
                'Add isgrp=0 condition
                'Dim iPriority As List(Of Integer) = dList.AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Distinct().ToList()
                Dim iPriority As List(Of Integer) = dList.Select("isgrp=0").AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Distinct().ToList()
                'Gajanan [17-DEC-2018] -- End

                Dim intSecondLastPriority As Integer = -1
                If iPriority.Count > 0 Then
                If iPriority.Min() = intPriorityId Then
                    intSecondLastPriority = intPriorityId
                    blnFlag = True
                Else
                    Try
                        intSecondLastPriority = iPriority.Item(iPriority.IndexOf(intPriorityId) - 1)
                    Catch ex As Exception
                        intSecondLastPriority = intPriorityId
                        blnFlag = True
                    End Try
                End If
                End If

                'Gajanan [9-April-2019] -- Start




                'Dim dr() As DataRow = Nothing
                'dr = dList.Select("priority = '" & intSecondLastPriority & "'", "employeeunkid, priority")
                'If dr.Length > 0 Then
                '    Dim row = dr.AsEnumerable().Where(Function(x) x.Field(Of Integer)("iStatusId") = CInt(clsEmployee_Master.EmpApprovalStatus.Approved))
                '    Dim strIds As String
                '    If row.Count() > 0 Then
                '        strIds = String.Join(",", dr.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToList().Except(row.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToList()).Distinct().ToArray())
                '        If blnFlag Then
                '            strIds = ""
                '        End If
                '        If strIds.Trim.Length > 0 Then
                '            oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = 1 AND employeeunkid NOT IN (" & strIds & ") ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                '        Else
                '            oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = 1", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                '        End If
                '    Else
                '        strIds = String.Join(",", dr.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).Distinct().ToArray())
                '        If blnFlag = False Then
                '            strIds = " AND employeeunkid NOT IN(" & strIds & ") "
                '        Else
                '            strIds = ""
                '        End If



                '        oData = New DataView(dList, "userunkid = " & intUserId & " AND iStatusId = 1 " & strIds, "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                '    End If
                'Else
                '    oData = New DataView(dList, "userunkid = " & intUserId, "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable() '& " AND iStatusId = 1 "
                'End If

                oData = New DataView(dList, "userunkid = " & intUserId & " AND mappingunkid <=0 AND (rno = 1 or isgrp = 1) AND employeeunkid <> uempid ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()

                Dim strValue As String = String.Join(",", oData.AsEnumerable().Where(Function(x) CBool(x.Field(Of Integer)("isgrp")) = False And x.Field(Of Integer)("iStatusId") = 2).GroupBy(Function(x) x.Field(Of Integer)("employeeunkid")) _
                                       .[Select](Function(x) New With {Key .barid = x.Key, Key .barCount = x.[Select](Function(z) z.Field(Of Integer)("employeeunkid")) _
                                       .Count()}).Where(Function(y) y.barCount = 1).Select(Function(a) a.barid.ToString).ToArray())

                If strValue.Trim.Length > 0 Then
                    oData = New DataView(oData, "employeeunkid NOT IN (" & strValue & ") ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                        End If

                If strValue.Trim.Length <= 0 Then
                    strValue = String.Join(",", oData.AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("employeeunkid")) _
                                       .[Select](Function(x) New With {Key .barid = x.Key, Key .barCount = x.[Select](Function(z) z.Field(Of Integer)("employeeunkid")) _
                                       .Count()}).Where(Function(y) y.barCount = 1).Select(Function(a) a.barid.ToString).ToArray())

                    If strValue.Trim.Length > 0 Then
                        oData = New DataView(oData, "employeeunkid NOT IN (" & strValue & ") ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                    End If
                End If
                'Gajanan [9-April-2019] -- End

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApprovalData; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return oData
    End Function

    Public Function GetNextEmployeeApprovers(ByVal strDatabaseName As String _
                                            , ByVal intCompanyId As Integer _
                                            , ByVal intYearId As Integer _
                                            , ByVal strUserAccessMode As String _
                                            , ByVal intPrivilegeId As Integer _
                                            , ByVal eScreenType As enScreenName _
                                            , ByVal xEmployeeAsOnDate As String _
                                            , ByVal intUserId As Integer _
                                            , ByVal eOperationType As enOperationType _
                                            , Optional ByVal objDataOpr As clsDataOperation = Nothing _
                                            , Optional ByVal intCurrentPriorityId As Integer = 0 _
                                            , Optional ByVal mblnIsSubmitForApproaval As Boolean = True _
                                            , Optional ByVal mstrFilterString As String = "" _
                                            , Optional ByVal blnFromWebGroup As Boolean = False _
                                            , Optional ByVal blnisidentity_membership_approval As Boolean = False _
                                            ) As DataTable 'Gajanan [17-April-2019] -- Add blnisidentity_membership_approval
        Dim StrQ As String = String.Empty
        Dim dtList As DataTable = Nothing
        Dim objDataOperation As New clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            Dim strFilter As String = String.Empty
            Dim strJoin As String = String.Empty
            Dim strSelect As String = String.Empty
            Dim strAccessJoin As String = String.Empty
            Dim strOuterJoin As String = String.Empty
            Dim strFinalString As String = ""

            'Gajanan [9-NOV-2019] -- Start   
            'Enhancement:Worked On NMB Bio Data Approval Query Optimization   
            'Call GetUACFilters(strDatabaseName, strUserAccessMode, strFilter, strJoin, strSelect, strAccessJoin, strOuterJoin, strFinalString, 0, objDataOperation)

            StrQ = "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
                 "DROP TABLE #USR "
            StrQ &= "SELECT " & _
                    "* " & _
                    "INTO #USR " & _
                    "FROM " & _
                    "( "

            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
            Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
            For index As Integer = 0 To strvalues.Length - 1
                Dim xStrJoinColName As String = ""
                Dim xIntAllocId As Integer = 0
                Select Case CInt(strvalues(index))
                    Case enAllocation.BRANCH
                        xStrJoinColName = "stationunkid"
                        xIntAllocId = CInt(enAllocation.BRANCH)
                    Case enAllocation.DEPARTMENT_GROUP
                        xStrJoinColName = "deptgroupunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT_GROUP)
                    Case enAllocation.DEPARTMENT
                        xStrJoinColName = "departmentunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT)
                    Case enAllocation.SECTION_GROUP
                        xStrJoinColName = "sectiongroupunkid"
                        xIntAllocId = CInt(enAllocation.SECTION_GROUP)
                    Case enAllocation.SECTION
                        xStrJoinColName = "sectionunkid"
                        xIntAllocId = CInt(enAllocation.SECTION)
                    Case enAllocation.UNIT_GROUP
                        xStrJoinColName = "unitgroupunkid"
                        xIntAllocId = CInt(enAllocation.UNIT_GROUP)
                    Case enAllocation.UNIT
                        xStrJoinColName = "unitunkid"
                        xIntAllocId = CInt(enAllocation.UNIT)
                    Case enAllocation.TEAM
                        xStrJoinColName = "teamunkid"
                        xIntAllocId = CInt(enAllocation.TEAM)
                    Case enAllocation.JOB_GROUP
                        xStrJoinColName = "jobgroupunkid"
                        xIntAllocId = CInt(enAllocation.JOB_GROUP)
                    Case enAllocation.JOBS
                        xStrJoinColName = "jobunkid"
                        xIntAllocId = CInt(enAllocation.JOBS)
                    Case enAllocation.CLASS_GROUP
                        xStrJoinColName = "classgroupunkid"
                        xIntAllocId = CInt(enAllocation.CLASS_GROUP)
                    Case enAllocation.CLASSES
                        xStrJoinColName = "classunkid"
                        xIntAllocId = CInt(enAllocation.CLASSES)
                End Select
                StrQ &= "SELECT DISTINCT " & _
                        "    B" & index.ToString() & ".userunkid " & _
                        "   ,A.employeeunkid " & _
                        "   ,B" & index.ToString() & ".approvertypeid " & _
                        "FROM " & _
                        "( " & _
                        "   SELECT " & _
                        "        AEM.employeeunkid " & _
                        "       ,ISNULL(T.departmentunkid, 0) AS departmentunkid " & _
                        "       ,ISNULL(J.jobunkid, 0) AS jobunkid " & _
                        "       ,ISNULL(T.classgroupunkid, 0) AS classgroupunkid " & _
                        "       ,ISNULL(T.classunkid, 0) AS classunkid " & _
                        "       ,ISNULL(T.stationunkid, 0) AS stationunkid " & _
                        "       ,ISNULL(T.deptgroupunkid, 0) AS deptgroupunkid " & _
                        "       ,ISNULL(T.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                        "       ,ISNULL(T.sectionunkid, 0) AS sectionunkid " & _
                        "       ,ISNULL(T.unitgroupunkid, 0) AS unitgroupunkid " & _
                        "       ,ISNULL(T.unitunkid, 0) AS unitunkid " & _
                        "       ,ISNULL(T.teamunkid, 0) AS teamunkid " & _
                        "       ,ISNULL(J.jobgroupunkid, 0) AS jobgroupunkid " & _
                        "   FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
                        "   JOIN #TABLENAME# AS TAT ON TAT.#JOINEMPCOLUMN# = AEM.employeeunkid " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            stationunkid " & _
                        "           ,deptgroupunkid " & _
                        "           ,departmentunkid " & _
                        "           ,sectiongroupunkid " & _
                        "           ,sectionunkid " & _
                        "           ,unitgroupunkid " & _
                        "           ,unitunkid " & _
                        "           ,teamunkid " & _
                        "           ,classgroupunkid " & _
                        "           ,classunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "       FROM " & strDatabaseName & "..hremployee_transfer_tran " & _
                        "       WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        "   ) AS T ON T.employeeunkid = AEM.employeeunkid " & _
                        "   AND T.Rno = 1 " & _
                        "   LEFT JOIN " & _
                        "   ( " & _
                        "       SELECT " & _
                        "            jobgroupunkid " & _
                        "           ,jobunkid " & _
                        "           ,employeeunkid " & _
                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "       FROM " & strDatabaseName & "..hremployee_categorization_tran " & _
                        "       WHERE isvoid = 0 " & _
                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        "   ) AS J ON J.employeeunkid = AEM.employeeunkid " & _
                        "   AND J.Rno = 1 " & _
                        "   WHERE TAT.isprocessed = 0 " & _
                        ") AS A " & _
                        "JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        UPM.userunkid " & _
                        "       ,UPT.allocationunkid " & _
                        "       ,CAM.approvertypeid " & _
                        "   FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                        "       JOIN  hremp_appusermapping AS CAM ON CAM.mapuserunkid = UPM.userunkid " & _
                        "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                        "   WHERE UPM.companyunkid = " & intCompanyId & " AND UPM.yearunkid = @Y AND UPM.referenceunkid IN (" & xIntAllocId & ") " & _
                        "   AND CAM.isvoid = 0 AND CAM.isactive = 1 " & _
                        ") AS B" & index.ToString() & " ON A." & xStrJoinColName & " = B" & index.ToString() & ".allocationunkid "
                If index < strvalues.Length - 1 Then
                    StrQ &= " INTERSECT "
                End If
            Next

            StrQ &= ") AS Fl "

            'Gajanan [9-NOV-2019] -- End

            Dim StrTableName As String = String.Empty
            Dim StrInnQry As String = String.Empty
            Dim StrInnJoin As String = String.Empty
            Dim StrTranUnkidCol As String = String.Empty
            Dim StrExtraFilter As String = String.Empty
            Dim StrTypeFilter As String = String.Empty
            Dim StrRowNumberCol As String = String.Empty
            Dim StrJoinColName As String = String.Empty
            Dim StrSelectCols As String = String.Empty


            'Gajanan [9-April-2019] -- Start

            Dim strMatchCols As String = String.Empty
            'Gajanan [9-April-2019] -- End

            Dim strUACJoin, strUACFilter, xAdvanceJoinQry As String
            strUACJoin = "" : strUACFilter = "" : xAdvanceJoinQry = ""
            modGlobal.NewAccessLevelFilterString(strUACJoin, strUACFilter, eZeeDate.convertDate(xEmployeeAsOnDate), True, strDatabaseName, intUserId, intCompanyId, intYearId, strUserAccessMode, "AEM", True)
            modGlobal.GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(xEmployeeAsOnDate), strDatabaseName, "EM")


            Select Case eScreenType
                Case enScreenName.frmQualificationsList
                    StrTableName = " " & strDatabaseName & "..hremp_qualification_approval_tran "



                    'Gajanan [9-April-2019] -- Start
                    'StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN ROW_NUMBER() OVER (PARTITION BY iData.employeeunkid,iData.qualificationtranunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN DENSE_RANK() OVER (PARTITION BY iData.employeeunkid,iData.instituteunkid,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    'Gajanan [9-April-2019] -- End


                    StrJoinColName = "employeeunkid"

                    StrSelectCols = ",iData.qualificationgrpname " & _
                                   ",iData.tranguid " & _
                                   ",iData.employeeunkid " & _
                                   ",iData.isgrp " & _
                                   ",iData.transactiondate " & _
                                   "/*,iData.mappingunkid */" & _
                                   ",iData.approvalremark " & _
                                   ",iData.isfinal " & _
                                   ",iData.statusunkid " & _
                                   ",iData.isprocessed " & _
                                   ",iData.qualificationname " & _
                                   ",iData.award_start_date " & _
                                   ",iData.award_end_date " & _
                                   ",iData.institute_name " & _
                                   ",iData.certificatedate " & _
                                   ",iData.reference_no " & _
                                   ",iData.qualificationgroupunkid " & _
                                   ",iData.qualificationunkid " & _
                                   ",iData.remark " & _
                                   ",iData.instituteunkid " & _
                                   ",iData.other_qualificationgrp " & _
                                   ",iData.other_qualification " & _
                                   ",iData.other_institute " & _
                                   ",iData.other_resultcode " & _
                                   ",iData.resultunkid " & _
                                   ",iData.gpacode " & _
                                   ",iData.tranguid " & _
                                   ",iData.isprocessed " & _
                                   ",iData.qualificationtranunkid " & _
                                   ",iData.form_name " & _
                                   ",iData.newattachdocumentid " & _
                                   ",iData.deleteattachdocumentid " & _
                                   ",iData.award_start_date_fc " & _
                                   ",iData.award_end_date_fc " & _
                                   ",iData.voidreason "
                    StrInnJoin = "JOIN " & _
                                "( " & _
                                   "SELECT " & _
                                        "a.qualificationgrpname " & _
                                        ",a.tranguid " & _
                                        ",a.employeeunkid " & _
                                        ",a.isgrp " & _
                                        ",a.transactiondate " & _
                                        ",a.mappingunkid " & _
                                        ",a.approvalremark " & _
                                        ",a.isfinal " & _
                                        ",a.statusunkid " & _
                                        ",a.isprocessed " & _
                                        ",a.qualificationname " & _
                                        ",a.award_start_date " & _
                                        ",a.award_end_date " & _
                                        ",a.institute_name " & _
                                        ",a.certificatedate " & _
                                        ",a.reference_no " & _
                                        ",a.qualificationgroupunkid " & _
                                        ",a.qualificationunkid " & _
                                        ",a.remark " & _
                                        ",a.instituteunkid " & _
                                        ",a.other_qualificationgrp " & _
                                        ",a.other_qualification " & _
                                        ",a.other_institute " & _
                                        ",a.other_resultcode " & _
                                        ",a.resultunkid " & _
                                        ",a.gpacode " & _
                                        ",a.qualificationtranunkid " & _
                                        ",a.operationtypeid " & _
                                        ",a.form_name " & _
                                        ",a.newattachdocumentid " & _
                                        ",a.deleteattachdocumentid " & _
                                        ",a.award_start_date_fc " & _
                                        ",a.award_end_date_fc " & _
                                        ",a.voidreason " & _
                                        "FROM ( " & _
                                     "SELECT DISTINCT " & _
                                        "e.employeecode + ' - ' + e.firstname + ' ' + e.surname AS qualificationgrpname " & _
                                        ",'' AS tranguid " & _
                                        ",t.employeeunkid " & _
                                        ",1 AS isgrp " & _
                                        ",NULL AS transactiondate " & _
                                        ",0 AS mappingunkid " & _
                                        ",'' AS approvalremark " & _
                                        ",0 AS isfinal " & _
                                        ",1 AS statusunkid " & _
                                        ",CAST(0 AS BIT) AS isprocessed " & _
                                        ",'' AS qualificationname " & _
                                        ",NULL AS award_start_date " & _
                                        ",NULL AS award_end_date " & _
                                        ",'' AS institute_name " & _
                                        ",NULL AS certificatedate " & _
                                        ",'' AS reference_no " & _
                                        ",0 AS qualificationgroupunkid " & _
                                        ",0 AS qualificationunkid " & _
                                        ",'' AS remark " & _
                                        ",0 AS instituteunkid " & _
                                        ",'' AS other_qualificationgrp " & _
                                        ",'' AS other_qualification " & _
                                        ",'' AS other_institute " & _
                                        ",'' AS other_resultcode " & _
                                        ",0 AS resultunkid " & _
                                        ",0 AS gpacode " & _
                                        ",0 as qualificationtranunkid " & _
                                        "," & eOperationType & " AS operationtypeid " & _
                                        ",'' as form_name " & _
                                        ",'' as newattachdocumentid " & _
                                        ",'' as deleteattachdocumentid " & _
                                        ",'' AS award_start_date_fc " & _
                                        ",'' AS award_end_date_fc " & _
                                        ",'' AS voidreason " & _
                                "FROM hremp_qualification_approval_tran AS t " & _
                                "JOIN hremployee_master AS e " & _
                                     "ON e.employeeunkid = t.employeeunkid " & _
                                "WHERE t.isvoid = 0 " & _
                                "AND #CNDT# " & _
                                "UNION " & _
                                "SELECT " & _
                                    "CASE " & _
                                        "WHEN ISNULL(t.qualificationgroupunkid, 0) > 0 THEN SPACE(5) + ISNULL(c.name, '') " & _
                                        "ELSE SPACE(5) + ISNULL(t.other_qualificationgrp, '') " & _
                                    "END AS qualificationgrpname " & _
                                    ",t.tranguid " & _
                                    ",e.employeeunkid " & _
                                    ",0 AS isgrp " & _
                                    ",t.transactiondate " & _
                                    ",t.approvalremark " & _
                                    ",t.mappingunkid " & _
                                    ",t.isfinal " & _
                                    ",t.statusunkid " & _
                                    ",t.isprocessed " & _
                                    ",CASE " & _
                                        "WHEN ISNULL(t.qualificationunkid, 0) > 0 THEN ISNULL(q.qualificationname, '') " & _
                                        "ELSE ISNULL(t.other_qualification, '') " & _
                                    "END AS qualificationname " & _
                                    ",t.award_start_date " & _
                                    ",t.award_end_date " & _
                                    ",i.institute_name " & _
                                    ",t.certificatedate " & _
                                    ",t.reference_no " & _
                                    ",t.qualificationgroupunkid " & _
                                    ",t.qualificationunkid " & _
                                    ",t.remark " & _
                                    ",t.instituteunkid " & _
                                    ",t.other_qualificationgrp " & _
                                    ",t.other_qualification " & _
                                    ",t.other_institute " & _
                                    ",t.other_resultcode " & _
                                    ",t.resultunkid " & _
                                    ",t.gpacode " & _
                                    ",t.qualificationtranunkid " & _
                                    ",t.operationtypeid " & _
                                    ",t.form_name " & _
                                    ",t.newattachdocumentid " & _
                                    ",t.deleteattachdocumentid " & _
                                    ",convert(varchar(8), cast(t.award_start_date as datetime), 112) as award_start_date_fc " & _
                                    ",convert(varchar(8), cast(t.award_end_date as datetime), 112) as award_end_date_fc " & _
                                    ",qt.voidreason " & _
                                "FROM hremp_qualification_approval_tran AS t " & _
                                "Left JOIN hremployee_master AS e " & _
                                   "ON e.employeeunkid = t.employeeunkid " & _
                                "Left JOIN hrqualification_master AS q " & _
                                   "ON q.qualificationunkid = t.qualificationunkid " & _
                                "Left JOIN hrinstitute_master AS i " & _
                                   "ON i.instituteunkid = t.instituteunkid " & _
                                "Left JOIN cfcommon_master AS c " & _
                                   "ON c.masterunkid = t.qualificationgroupunkid " & _
                                "LEFT JOIN hremp_qualification_tran AS qt ON t.qualificationtranunkid = qt.qualificationtranunkid " & _
                                "WHERE t.isvoid = 0 " & _
                                "AND #CNDT# " & _
                                ") AS a) AS iData " & _
                                     "ON EM.employeeunkid = iData.employeeunkid "
                    StrTranUnkidCol = "qualificationtranunkid"

                    'Gajanan [9-April-2019] -- Start
                    strMatchCols = "instituteunkid"
                    'Gajanan [9-April-2019] -- End

                    'S.SANDEEP |31-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : [Employee Bio Data UAT]
                    'Added  Alias - {On transactiondate,mappingunkid,isfinal,statusunkid}
                    'Replaced : hremp_qualification_approval_tran --> hremp_qualification_tran
                    'S.SANDEEP |31-MAY-2019| -- END

                Case enScreenName.frmJobHistory_ExperienceList
                    StrTableName = " " & strDatabaseName & "..hremp_experience_approval_tran "


                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.

                    'StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN ROW_NUMBER() OVER (PARTITION BY iData.employeeunkid,iData.experiencetranunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN DENSE_RANK() OVER (PARTITION BY iData.employeeunkid,iData.company,B.mappingunkid  ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    'Gajanan [17-April-2019] -- End


                    StrJoinColName = "employeeunkid"

                    StrSelectCols = ",iData.cname " & _
                                    ",iData.tranguid " & _
                                    ",iData.employeeunkid " & _
                                    ",iData.isgrp " & _
                                    ",iData.transactiondate " & _
                                    "/*,iData.mappingunkid */" & _
                                    ",iData.approvalremark " & _
                                    ",iData.isfinal " & _
                                    ",iData.statusunkid " & _
                                    ",iData.isprocessed " & _
                                    ",iData.company " & _
                                    ",iData.old_job " & _
                                    ",iData.start_date " & _
                                    ",iData.end_date " & _
                                    ",iData.supervisor " & _
                                    ",iData.remark " & _
                                    ",iData.experiencetranunkid " & _
                                    ",iData.address " & _
                                    ",iData.contact_no " & _
                                    ",iData.contact_person " & _
                                    ",iData.iscontact_previous " & _
                                    ",iData.otherbenefit " & _
                                    ",iData.currencysign " & _
                                    ",iData.last_pay " & _
                                    ",iData.leave_reason " & _
                                    ",iData.memo " & _
                                    ",iData.benifitplanunkids " & _
                                    ",iData.end_date_fc " & _
                                    ",iData.start_date_fc " & _
                                    ",iData.voidreason " & _
                                    ",iData.newattachdocumentid " & _
                                    ",iData.deleteattachdocumentid "

                    'Gajanan [5-Dec-2019] -- ADD {newattachdocumentid,deleteattachdocumentid}   




                    StrInnJoin = "JOIN " & _
                                "( " & _
                                   "SELECT " & _
                                        "a.cname " & _
                                        ",a.tranguid " & _
                                        ",a.employeeunkid " & _
                                        ",a.isgrp " & _
                                        ",a.transactiondate " & _
                                        ",a.mappingunkid " & _
                                        ",a.approvalremark " & _
                                        ",a.isfinal " & _
                                        ",a.statusunkid " & _
                                        ",a.isprocessed " & _
                                        ",a.company " & _
                                        ",a.old_job " & _
                                        ",a.start_date " & _
                                        ",a.end_date " & _
                                        ",a.supervisor " & _
                                        ",a.remark " & _
                                        ",a.experiencetranunkid " & _
                                        ",a.address " & _
                                        ",a.contact_no " & _
                                        ",a.contact_person " & _
                                        ",a.iscontact_previous " & _
                                        ",a.otherbenefit " & _
                                        ",a.currencysign " & _
                                        ",a.last_pay " & _
                                        ",a.leave_reason " & _
                                        ",a.memo " & _
                                        ",a.benifitplanunkids " & _
                                        ",a.operationtypeid " & _
                                        ",a.end_date_fc " & _
                                        ",a.start_date_fc " & _
                                        ",a.voidreason " & _
                                        ",a.newattachdocumentid " & _
                                        ",a.deleteattachdocumentid " & _
                                     "FROM ( " & _
                                     "SELECT DISTINCT " & _
                                         "e.employeecode + ' - ' + e.firstname + ' ' + e.surname AS cname " & _
                                         ",'' AS tranguid " & _
                                         ",t.employeeunkid " & _
                                         ",1 AS isgrp " & _
                                         ",NULL AS transactiondate " & _
                                         ",0 AS mappingunkid " & _
                                         ",'' AS approvalremark " & _
                                         ",0 AS isfinal " & _
                                         ",1 AS statusunkid " & _
                                         ",CAST(0 AS BIT) AS isprocessed " & _
                                         ",'' as company " & _
                                         ",'' as old_job " & _
                                         ",NULL as start_date " & _
                                         ",NULL as end_date " & _
                                         ",'' as supervisor " & _
                                         ",'' as remark " & _
                                         ",'' as experiencetranunkid " & _
                                         ",'' AS address " & _
                                         ",'' AS contact_no " & _
                                         ",'' AS contact_person " & _
                                         ",0 AS iscontact_previous " & _
                                         ",'' AS otherbenefit " & _
                                         ",'' AS currencysign " & _
                                         ",0 AS last_pay " & _
                                         ",'' AS leave_reason " & _
                                         ",'' AS memo " & _
                                         ",'' AS benifitplanunkids " & _
                                         " ," & eOperationType & " AS operationtypeid " & _
                                         ",convert(varchar(8), cast(t.end_date as datetime), 112) as end_date_fc " & _
                                         ",convert(varchar(8), cast(t.start_date as datetime), 112) as start_date_fc " & _
                                         ",'' as voidreason " & _
                                         ",'' as newattachdocumentid " & _
                                         ",'' as deleteattachdocumentid " & _
                                "FROM hremp_experience_approval_tran AS t " & _
                                "JOIN hremployee_master AS e " & _
                                     "ON e.employeeunkid = t.employeeunkid " & _
                                "WHERE t.isvoid = 0 " & _
                                "AND #CNDT# " & _
                                "UNION " & _
                                "SELECT " & _
                                    "SPACE(5) + t.company AS cname " & _
                                    ",t.tranguid " & _
                                    ",e.employeeunkid " & _
                                    ",0 AS isgrp " & _
                                    ",transactiondate " & _
                                    ",t.approvalremark " & _
                                    ",mappingunkid " & _
                                    ",isfinal " & _
                                    ",statusunkid " & _
                                    ",t.isprocessed " & _
                                    ",t.company " & _
                                    ",t.old_job " & _
                                    ",t.start_date " & _
                                    ",t.end_date " & _
                                    ",t.supervisor " & _
                                    ",t.remark " & _
                                    ",t.experiencetranunkid " & _
                                    ",t.address " & _
                                    ",t.contact_no " & _
                                    ",t.contact_person " & _
                                    ",t.iscontact_previous " & _
                                    ",t.otherbenefit " & _
                                    ",t.currencysign " & _
                                    ",t.last_pay " & _
                                    ",t.leave_reason " & _
                                    ",t.memo " & _
                                    ",t.benifitplanunkids " & _
                                    ",t.operationtypeid " & _
                                    ",convert(varchar(8), cast(t.end_date as datetime), 112) as end_date_fc " & _
                                    ",convert(varchar(8), cast(t.start_date as datetime), 112) as start_date_fc " & _
                                    ",rt.voidreason " & _
                                    ",t.newattachdocumentid " & _
                                    ",t.deleteattachdocumentid " & _
                                "FROM hremp_experience_approval_tran AS t " & _
                                "Left JOIN hremployee_master AS e " & _
                                     "ON e.employeeunkid = t.employeeunkid " & _
                                "LEFT JOIN hremp_experience_tran AS rt ON t.experiencetranunkid = rt.experiencetranunkid " & _
                                "WHERE t.isvoid = 0 " & _
                                "AND #CNDT# " & _
                                ") AS a) AS iData " & _
                                     "ON EM.employeeunkid = iData.employeeunkid "
                    StrTranUnkidCol = "experiencetranunkid"

                    'Gajanan [9-April-2019] -- Start
                    strMatchCols = "company"
                    'Gajanan [9-April-2019] -- End

                    'Gajanan [5-Dec-2019] -- ADD {newattachdocumentid,deleteattachdocumentid}   

                Case enScreenName.frmEmployeeRefereeList

                    StrTableName = " " & strDatabaseName & "..hremployee_referee_approval_tran "


                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN ROW_NUMBER() OVER (PARTITION BY iData.employeeunkid,iData.refereetranunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN DENSE_RANK() OVER (PARTITION BY iData.employeeunkid,iData.name,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    'Gajanan [17-April-2019] -- End

                    StrJoinColName = "employeeunkid"

                    StrSelectCols = ",iData.rname " & _
                                    ",iData.Country " & _
                                    ",iData.Company " & _
                                    ",iData.Email " & _
                                    ",iData.telephone_no " & _
                                    ",iData.mobile_no " & _
                                    ",iData.remark " & _
                                    ",iData.isfinal " & _
                                    ",iData.statusunkid " & _
                                    ",iData.refereetranunkid " & _
                                    ",iData.tranguid " & _
                                    ",iData.isgrp " & _
                                    ",iData.isprocessed " & _
                                    ",iData.Address " & _
                                    ",iData.countryunkid " & _
                                    ",iData.Cityunkid " & _
                                    ",iData.Gender " & _
                                    ",iData.Stateunkid " & _
                                    ",iData.Ref_Position " & _
                                    ",iData.employeeunkid " & _
                                    ",iData.relationunkid " & _
                                    ",iData.Name " & _
                                    ",iData.voidreason "
                    StrInnJoin = "JOIN " & _
                                "( " & _
                                   "SELECT " & _
                                        "a.rname " & _
                                        ",a.Country " & _
                                        ",a.Company " & _
                                        ",a.Email " & _
                                        ",a.telephone_no " & _
                                        ",a.mobile_no " & _
                                        ",a.tranguid " & _
                                        ",a.employeeunkid " & _
                                        ",a.isgrp " & _
                                        ",a.transactiondate " & _
                                        ",a.mappingunkid " & _
                                        ",a.remark " & _
                                        ",a.isfinal " & _
                                        ",a.statusunkid " & _
                                        ",a.refereetranunkid " & _
                                        ",a.isprocessed " & _
                                        ",a.Address " & _
                                        ",a.countryunkid " & _
                                        ",a.Cityunkid " & _
                                        ",a.Gender " & _
                                        ",a.Stateunkid " & _
                                        ",a.Ref_Position " & _
                                        ",a.relationunkid " & _
                                        ",a.name " & _
                                        ",a.operationtypeid " & _
                                        ",a.voidreason " & _
                                     "FROM ( " & _
                                     "SELECT DISTINCT " & _
                                     "e.employeecode + ' - ' + e.firstname + ' ' + e.surname AS rname " & _
                                   ",'' AS tranguid " & _
                                   ",t.employeeunkid " & _
                                   ",1 AS isgrp " & _
                                   ",NULL AS transactiondate " & _
                                   ",0 AS mappingunkid " & _
                                   ",'' AS remark " & _
                                   ",0 AS isfinal " & _
                                   ",1 AS statusunkid " & _
                                   ",0 AS relationunkid " & _
                                   ",CAST(0 AS BIT) AS isprocessed " & _
                                   ",0 AS countryunkid " & _
                                   ",'' AS telephone_no " & _
                                   ",'' AS mobile_no " & _
                                   ",'' AS Company " & _
                                   ",'' AS Country " & _
                                   ",'' AS Email " & _
                                   ",0 AS refereetranunkid " & _
                                   " ,'' as Address " & _
                                   " ,0 as Cityunkid " & _
                                   " ,'' as Gender " & _
                                   " ,0 as Stateunkid " & _
                                   " ,'' as Ref_Position " & _
                                   " ,'' as Name " & _
                                   " ," & eOperationType & " AS operationtypeid " & _
                                   " ,'' as voidreason " & _
                                "FROM hremployee_referee_approval_tran AS t " & _
                                "JOIN hremployee_master AS e " & _
                                     "ON e.employeeunkid = t.employeeunkid " & _
                                "WHERE t.isvoid = 0 " & _
                                "AND #CNDT# " & _
                                "UNION " & _
                                "SELECT " & _
                                     "SPACE(5) + t.name AS rname " & _
                                   ",t.tranguid " & _
                                   ",e.employeeunkid " & _
                                   ",0 AS isgrp " & _
                                   ",transactiondate " & _
                                   ",mappingunkid " & _
                                   ",remark " & _
                                   ",isfinal " & _
                                   ",statusunkid " & _
                                   ",t.relationunkid " & _
                                   ",t.isprocessed " & _
                                   ",t.countryunkid " & _
                                   ",t.telephone_no " & _
                                   ",t.mobile_no " & _
                                   ",t.identify_no AS Company " & _
                                   ",s.country_name AS Country " & _
                                   ",t.email AS Email " & _
                                   ",t.refereetranunkid AS refereetranunkid " & _
                                   ",t.address as Address " & _
                                   ",t.cityunkid as Cityunkid " & _
                                   ",t.gender as Gender" & _
                                   ",t.stateunkid as Stateunkid " & _
                                   ",t.ref_position as Ref_Position " & _
                                   ",t.name as Name " & _
                                   ",t.operationtypeid " & _
                                   ",rt.voidreason " & _
                                "FROM hremployee_referee_approval_tran AS t " & _
                                "Left JOIN cfcommon_master AS c " & _
                                     "ON c.masterunkid = t.relationunkid " & _
                                "Left JOIN hrmsConfiguration..cfcountry_master AS s " & _
                                     "ON s.countryunkid = t.countryunkid " & _
                                "Left JOIN hremployee_master AS e " & _
                                     "ON e.employeeunkid = t.employeeunkid " & _
                                "LEFT JOIN hremployee_referee_tran AS rt ON t.refereetranunkid = rt.refereetranunkid " & _
                                "WHERE t.isvoid = 0 " & _
                                "AND #CNDT# " & _
                                ") AS a) AS iData " & _
                                     "ON EM.employeeunkid = iData.employeeunkid "
                    StrTranUnkidCol = "refereetranunkid"

                    'Gajanan [9-April-2019] -- Start
                    strMatchCols = "name"
                    'Gajanan [9-April-2019] -- End

                Case enScreenName.frmEmployee_Skill_List

                    StrTableName = " " & strDatabaseName & "..hremp_app_skills_approval_tran "


                    'Gajanan [9-April-2019] -- Start
                    'StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN ROW_NUMBER() OVER (PARTITION BY iData.emp_app_unkid,iData.skillunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN DENSE_RANK() OVER (PARTITION BY iData.emp_app_unkid,iData.skillunkid,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    'Gajanan [9-April-2019] -- End

                    StrJoinColName = "emp_app_unkid"

                    StrSelectCols = "  ,iData.scategory " & _
                                    "  ,iData.skill " & _
                                    "  ,iData.remark " & _
                                    "  ,iData.isfinal " & _
                                    "  ,iData.statusunkid " & _
                                    "  ,iData.description " & _
                                    "  ,iData.skillcategoryunkid " & _
                                    "  ,iData.skillunkid " & _
                                    "  ,iData.voidreason " & _
                                    "  ,iData.other_skillcategory " & _
                                    "  ,iData.other_skill " & _
                                    "  ,iData.skillexpertiseunkid "


                    StrInnJoin = "JOIN " & _
                                 "( " & _
                                 "    SELECT " & _
                                 "         a.scategory " & _
                                 "        ,a.skill " & _
                                 "        ,a.tranguid " & _
                                 "        ,a.emp_app_unkid " & _
                                 "        ,a.isgrp " & _
                                 "        ,a.transactiondate " & _
                                 "        ,a.mappingunkid " & _
                                 "        ,a.remark " & _
                                 "        ,a.isfinal " & _
                                 "        ,a.statusunkid " & _
                                 "        ,a.skillstranunkid " & _
                                 "        ,a.isprocessed " & _
                                 "        ,a.skillcategoryunkid " & _
                                 "        ,a.skillunkid " & _
                                 "        ,a.description " & _
                                 "        ,a.voidreason " & _
                                 "        ,a.other_skillcategory " & _
                                 "        ,a.other_skill " & _
                                 "        ,a.skillexpertiseunkid "

                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    If eOperationType <> enOperationType.NONE Then
                        StrInnJoin &= "        ,a.operationtypeid "
                    End If
                    'Gajanan [17-DEC-2018] -- End

                    StrInnJoin &= "    FROM " & _
                                 "    ( " & _
                                 "        SELECT DISTINCT " & _
                                 "             e.employeecode + ' - ' + e.firstname + ' ' + e.surname AS scategory " & _
                                 "            ,'' AS skill " & _
                                 "            ,'' AS tranguid " & _
                                 "            ,t.emp_app_unkid " & _
                                 "            ,1 AS isgrp " & _
                                 "            ,NULL AS transactiondate " & _
                                 "            ,0 AS mappingunkid " & _
                                 "            ,'' AS remark " & _
                                 "            ,0 AS isfinal " & _
                                 "            ,1 AS statusunkid " & _
                                 "            ,0 AS skillstranunkid " & _
                                 "            ,CAST(0 AS BIT) AS isprocessed " & _
                                 "            ,0 AS skillcategoryunkid " & _
                                 "            ,0 AS skillunkid " & _
                                 "            ,'' AS description " & _
                                 "            ," & eOperationType & " AS operationtypeid " & _
                                 "            ,'' AS voidreason " & _
                                 "            ,'' AS other_skillcategory " & _
                                 "            ,'' AS other_skill " & _
                                 "            ,0 AS skillexpertiseunkid " & _
                                 "        FROM hremp_app_skills_approval_tran AS t JOIN hremployee_master AS e ON e.employeeunkid = t.emp_app_unkid " & _
                                 "        WHERE t.isvoid = 0 AND #CNDT# " & _
                                 "        UNION " & _
                                 "        SELECT " & _
                                 "             '/r/n' + c.name AS scategory " & _
                                 "            ,s.skillname AS skill " & _
                                 "            ,t.tranguid " & _
                                 "            ,e.employeeunkid " & _
                                 "            ,0 AS isgrp " & _
                                 "            ,transactiondate " & _
                                 "            ,mappingunkid " & _
                                 "            ,remark " & _
                                 "            ,isfinal " & _
                                 "            ,statusunkid " & _
                                 "            ,t.skillstranunkid " & _
                                 "            ,t.isprocessed " & _
                                 "            ,t.skillcategoryunkid " & _
                                 "            ,t.skillunkid " & _
                                 "            ,t.description " & _
                                 "            ,t.operationtypeid " & _
                                 "            ,st.voidreason " & _
                                  "           ,t.other_skillcategory " & _
                                 "            ,t.other_skill " & _
                                 "            ,t.skillexpertiseunkid " & _
                                 "        FROM hremp_app_skills_approval_tran AS t " & _
                                 "        Left JOIN cfcommon_master AS c ON c.masterunkid = t.skillcategoryunkid " & _
                                 "        Left JOIN hrskill_master AS s ON s.skillunkid = t.skillunkid " & _
                                 "        Left JOIN hremployee_master AS e ON e.employeeunkid = t.emp_app_unkid " & _
                                 "        LEFT JOIN hremp_app_skills_tran AS st ON t.skillstranunkid = st.skillstranunkid " & _
                                 "        WHERE t.isvoid = 0 AND #CNDT# " & _
                                 "    ) AS a " & _
                                 ") AS iData ON EM.employeeunkid = iData.emp_app_unkid "
                    't.isvoid = 0 AND t.skillstranunkid <= 0 AND t.isprocessed = 0
                    StrTranUnkidCol = "skillstranunkid"


                    'Gajanan [9-April-2019] -- Start
                    strMatchCols = "skillunkid"
                    'Gajanan [9-April-2019] -- End


                    'Gajanan [22-Feb-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.

                Case enScreenName.frmDependantsAndBeneficiariesList, enScreenName.frmDependantStatus
                    'Sohail (18 May 2019) - [frmDependantStatus]
                    StrTableName = " " & strDatabaseName & "..hrdependant_beneficiaries_approval_tran "


                    'Gajanan [9-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN ROW_NUMBER() OVER (PARTITION BY iData.employeeunkid,iData.dpndtbeneficetranunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN DENSE_RANK() OVER (PARTITION BY iData.employeeunkid,iData.first_name,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    'Gajanan [9-April-2019] -- End



                    StrJoinColName = "employeeunkid"

                    StrSelectCols = ",iData.dependantname " & _
                                    ",iData.first_name " & _
                                    ",iData.middle_name " & _
                                    ",iData.last_name " & _
                                    ",iData.tranguid " & _
                                    ",iData.employeeunkid " & _
                                    ",iData.isgrp " & _
                                    ",iData.transactiondate " & _
                                    "/*,iData.mappingunkid */" & _
                                    ",iData.approvalremark " & _
                                    ",iData.isfinal " & _
                                    ",iData.statusunkid " & _
                                    ",iData.isprocessed " & _
                                    ",iData.relationunkid " & _
                                    ",iData.relation " & _
                                    ",iData.birthdate " & _
                                    ",iData.address " & _
                                    ",iData.telephone_no " & _
                                    ",iData.mobile_no " & _
                                    ",iData.post_box " & _
                                    ",iData.countryunkid " & _
                                    ",iData.stateunkid " & _
                                    ",iData.cityunkid " & _
                                    ",iData.zipcodeunkid " & _
                                    ",iData.email " & _
                                    ",iData.nationalityunkid " & _
                                    ",iData.identify_no " & _
                                    ",iData.isdependant " & _
                                    ",iData.gender as genderid " & _
                                    ",CASE " & _
                                          "WHEN (iData.gender) = 1 THEN @Male " & _
                                          "WHEN (iData.gender) = 2 THEN @Female " & _
                                    "END as gender " & _
                                    ",iData.psoft_syncdatetime " & _
                                    ",iData.tranguid " & _
                                    ",iData.isgrp " & _
                                    ",iData.isprocessed " & _
                                    ",iData.dpndtbeneficetranunkid " & _
                                    ",iData.tranguid " & _
                                    ",iData.isgrp " & _
                                    ",iData.isprocessed " & _
                                    ",iData.operationtypeid " & _
                                    ",iData.form_name " & _
                                    ",iData.newattachdocumentid " & _
                                    ",iData.deleteattachdocumentid " & _
                                    ",iData.newattachimageid " & _
                                    ",iData.deleteattachimageid " & _
                                    ",iData.voidreason " & _
                                    ",iData.effective_date " & _
                                    ",iData.isactive " & _
                                    ",iData.reasonunkid " & _
                                    ",iData.reason " & _
                                    ",iData.isfromstatus " & _
                                    ",iData.deletedpndtbeneficestatustranids "
                    StrInnJoin = "JOIN " & _
                                "( " & _
                                   "SELECT " & _
                                        "a.dependantname " & _
                                        ",a.first_name " & _
                                        ",a.middle_name " & _
                                        ",a.last_name " & _
                                        ",a.tranguid " & _
                                        ",a.employeeunkid " & _
                                        ",a.isgrp " & _
                                        ",a.transactiondate " & _
                                        ",a.mappingunkid " & _
                                        ",a.approvalremark " & _
                                        ",a.isfinal " & _
                                        ",a.statusunkid " & _
                                        ",a.isprocessed " & _
                                        ",a.relationunkid " & _
                                        ",a.relation " & _
                                        ",a.birthdate " & _
                                        ",a.address " & _
                                        ",a.telephone_no " & _
                                        ",a.mobile_no " & _
                                        ",a.post_box " & _
                                        ",a.countryunkid " & _
                                        ",a.stateunkid " & _
                                        ",a.cityunkid " & _
                                        ",a.zipcodeunkid " & _
                                        ",a.email " & _
                                        ",a.nationalityunkid " & _
                                        ",a.identify_no " & _
                                        ",a.isdependant " & _
                                        ",a.gender " & _
                                        ",a.psoft_syncdatetime " & _
                                        ",a.dpndtbeneficetranunkid " & _
                                        ",a.operationtypeid " & _
                                        ",a.form_name " & _
                                        ",a.newattachdocumentid " & _
                                        ",a.deleteattachdocumentid " & _
                                        ",a.newattachimageid " & _
                                        ",a.deleteattachimageid " & _
                                        ",a.voidreason " & _
                                        ",a.effective_date " & _
                                        ",a.isactive " & _
                                        ",a.reasonunkid " & _
                                        ",a.reason " & _
                                        ",a.isfromstatus " & _
                                        ",a.deletedpndtbeneficestatustranids " & _
                                     "FROM ( " & _
                                     "SELECT DISTINCT " & _
                                         "e.employeecode + ' - ' + e.firstname + ' ' + e.surname AS dependantname " & _
                                         ",'' as first_name " & _
                                         ",'' as middle_name " & _
                                         ",'' as last_name " & _
                                         ",'' AS tranguid " & _
                                         ",t.employeeunkid " & _
                                         ",1 AS isgrp " & _
                                         ",NULL AS transactiondate " & _
                                         ",0 AS mappingunkid " & _
                                         ",'' AS approvalremark " & _
                                         ",0 AS isfinal " & _
                                         ",1 AS statusunkid " & _
                                         ",CAST(0 AS BIT) AS isprocessed " & _
                                         ",0 AS relationunkid " & _
                                         ",'' AS relation " & _
                                         ",NULL AS birthdate " & _
                                         ",'' AS address " & _
                                         ",'' AS telephone_no " & _
                                         ",'' AS mobile_no " & _
                                         ",'' AS post_box " & _
                                         ",0 AS countryunkid " & _
                                         ",0 AS stateunkid " & _
                                         ",0 AS cityunkid " & _
                                         ",0 AS zipcodeunkid " & _
                                         ",'' AS email " & _
                                         ",0 AS nationalityunkid " & _
                                         ",'' AS identify_no " & _
                                         ",0 AS isdependant " & _
                                         ",'' AS gender " & _
                                         ",NULL AS psoft_syncdatetime " & _
                                         ",0 AS dpndtbeneficetranunkid " & _
                                         "," & eOperationType & " AS operationtypeid " & _
                                         ",'' as form_name " & _
                                         ",'' as newattachdocumentid " & _
                                         ",'' as deleteattachdocumentid " & _
                                         ",-1 as newattachimageid " & _
                                         ",-1 as deleteattachimageid " & _
                                         ",'' as voidreason " & _
                                         ",NULL AS effective_date " & _
                                         ",1 AS isactive " & _
                                         ",0 AS reasonunkid " & _
                                         ",'' as reason" & _
                                         ",0 AS isfromstatus " & _
                                         ",'' AS deletedpndtbeneficestatustranids " & _
                                         ",0 AS dpndtbeneficestatustranunkid " & _
                                "FROM hrdependant_beneficiaries_approval_tran AS t " & _
                                "JOIN hremployee_master AS e " & _
                                     "ON e.employeeunkid = t.employeeunkid " & _
                                "WHERE t.isvoid = 0 " & _
                                "AND #CNDT# " & _
                                "UNION " & _
                                "SELECT " & _
                                         "t.first_name + ' - ' + t.middle_name + ' ' + t.last_name AS dependantname " & _
                                         ",t.first_name " & _
                                         ",t.middle_name " & _
                                         ",t.last_name " & _
                                         ",t.tranguid " & _
                                         ",e.employeeunkid " & _
                                         ",0 AS isgrp " & _
                                         ",transactiondate " & _
                                         ",t.approvalremark " & _
                                         ",mappingunkid " & _
                                         ",isfinal " & _
                                         ",statusunkid " & _
                                         ",t.isprocessed " & _
                                         ",t.relationunkid " & _
                                         ",cm.name as relation" & _
                                         ",t.birthdate " & _
                                         ",t.address " & _
                                         ",t.telephone_no " & _
                                         ",t.mobile_no " & _
                                         ",t.post_box " & _
                                         ",t.countryunkid " & _
                                         ",t.stateunkid " & _
                                         ",t.cityunkid " & _
                                         ",t.zipcodeunkid " & _
                                         ",t.email " & _
                                         ",t.nationalityunkid " & _
                                         ",t.identify_no " & _
                                         ",t.isdependant " & _
                                         ",t.gender " & _
                                         ",t.psoft_syncdatetime " & _
                                         ",t.dpndtbeneficetranunkid " & _
                                         ",t.operationtypeid " & _
                                         ",t.form_name " & _
                                         ",t.newattachdocumentid " & _
                                         ",t.deleteattachdocumentid " & _
                                         ",t.newattachimageid " & _
                                         ",t.deleteattachimageid " & _
                                         ",dt.voidreason " & _
                                         ",t.effective_date " & _
                                         ",t.isactive " & _
                                         ",t.reasonunkid " & _
                                         ",ISNULL(rn.name, '') as reason" & _
                                         ",t.isfromstatus " & _
                                         ",t.deletedpndtbeneficestatustranids " & _
                                         ",0 AS dpndtbeneficestatustranunkid " & _
                                "FROM hrdependant_beneficiaries_approval_tran AS t " & _
                                      "LEFT JOIN hremployee_master AS e " & _
                                           "ON e.employeeunkid = t.employeeunkid " & _
                                      "LEFT JOIN hrmsConfiguration..cfcountry_master AS co " & _
                                           "ON co.countryunkid = t.countryunkid " & _
                                      "LEFT JOIN hrmsConfiguration..cfstate_master AS s " & _
                                           "ON s.stateunkid = t.statusunkid " & _
                                      "LEFT JOIN hrmsConfiguration..cfcity_master AS c " & _
                                           "ON c.cityunkid = t.cityunkid " & _
                                      "LEFT JOIN cfcommon_master AS cm " & _
                                           "ON cm.masterunkid = t.relationunkid " & _
                                      "LEFT JOIN hrdependants_beneficiaries_tran AS dt ON t.dpndtbeneficetranunkid = dt.dpndtbeneficetranunkid " & _
                                      "LEFT JOIN cfcommon_master AS rn " & _
                                           "ON rn.masterunkid = t.reasonunkid " & _
                                "WHERE t.isvoid = 0 " & _
                                "AND #CNDT# " & _
                                ") AS a) AS iData " & _
                                     "ON EM.employeeunkid = iData.employeeunkid "
                    'Sohail (03 Jul 2019) - [reason]
                    'Sohail (18 May 2019) - [effective_date, isactive, reasonunkid, deletedpndtbeneficestatustranids]
                    StrTranUnkidCol = "dpndtbeneficetranunkid"

                    'Gajanan [9-April-2019] -- Start
                    strMatchCols = "first_name"
                    'Gajanan [9-April-2019] -- End

                Case enScreenName.frmIdentityInfoList

                    StrTableName = " " & strDatabaseName & "..hremployee_idinfo_approval_tran "

                    StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN DENSE_RANK() OVER (PARTITION BY iData.employeeunkid,iData.idtypeunkid,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "

                    StrJoinColName = "employeeunkid"

                    StrSelectCols = ",iData.IdType " & _
                                    ",iData.serial_no " & _
                                    ",iData.identity_no " & _
                                    ",iData.Country " & _
                                    ",iData.issued_place " & _
                                    ",iData.issue_date " & _
                                    ",iData.expiry_date " & _
                                    ",iData.dl_class " & _
                                    ",iData.isfinal " & _
                                    ",iData.statusunkid " & _
                                    ",iData.approvalremark " & _
                                    ",iData.tranguid " & _
                                    ",iData.isgrp " & _
                                    ",iData.isprocessed " & _
                                    ",iData.operationtypeid " & _
                                    ",iData.idtypeunkid " & _
                                    ",iData.countryunkid " & _
                                    ",iData.isdefault " & _
                                    ",0 AS loginemployeeunkid " & _
                                    ",CAST(0 AS BIT) AS isvoid " & _
                                    ",'' AS voidreason " & _
                                    ",CAST(NULL AS DATETIME) AS auditdatetime " & _
                                    ",0 AS audittype " & _
                                    ",0 AS audituserunkid " & _
                                    ",'' AS ip " & _
                                    ",'' AS host " & _
                                    ",'' AS form_name " & _
                                    ",CAST(0 AS BIT) AS isweb " & _
                                    ",'A' AS AUD " & _
                                    ",iData.newattachdocumentid " & _
                                    ",iData.deleteattachdocumentid " & _
                                    ",CASE WHEN iData.isgrp = 0 THEN IdType ELSE '' END AS identities "

                    StrInnJoin = "JOIN " & _
                                 "( " & _
                                 "  SELECT " & _
                                 "       a.IdType " & _
                                 "      ,a.serial_no " & _
                                 "      ,a.identity_no " & _
                                 "      ,a.Country " & _
                                 "      ,a.issued_place " & _
                                 "      ,a.issue_date " & _
                                 "      ,a.expiry_date " & _
                                 "      ,a.dl_class " & _
                                 "      ,a.tranguid " & _
                                 "      ,a.employeeunkid " & _
                                 "      ,a.isgrp " & _
                                 "      ,a.transactiondate " & _
                                 "      ,a.mappingunkid " & _
                                 "      ,a.isfinal " & _
                                 "      ,a.statusunkid " & _
                                 "      ,a.identitytranunkid " & _
                                 "      ,a.isprocessed " & _
                                 "      ,a.idtypeunkid " & _
                                 "      ,a.countryunkid " & _
                                 "      ,a.operationtypeid " & _
                                 "      ,a.approvalremark " & _
                                 "      ,a.isdefault " & _
                                 "      ,a.newattachdocumentid " & _
                                 "      ,a.deleteattachdocumentid " & _
                                 "  FROM " & _
                                 "  ( " & _
                                 "      SELECT DISTINCT " & _
                                 "           e.employeecode + ' - ' + e.firstname + ' ' + e.surname AS IdType " & _
                                 "          ,'' AS serial_no " & _
                                 "          ,'' AS identity_no " & _
                                 "          ,'' AS Country " & _
                                 "          ,'' AS issued_place " & _
                                 "          ,NULL AS issue_date " & _
                                 "          ,NULL AS expiry_date " & _
                                 "          ,'' AS dl_class " & _
                                 "          ,'' AS tranguid " & _
                                 "          ,t.employeeunkid " & _
                                 "          ,1 AS isgrp " & _
                                 "          ,NULL AS transactiondate " & _
                                 "          ,0 AS mappingunkid " & _
                                 "          ,'' AS approvalremark " & _
                                 "          ,0 AS isfinal " & _
                                 "          ,1 AS statusunkid " & _
                                 "          ,-1 AS identitytranunkid " & _
                                 "          ,CAST(0 AS BIT) AS isprocessed " & _
                                 "          ,0 AS idtypeunkid " & _
                                 "          ,0 AS countryunkid " & _
                                 "          ," & eOperationType & " AS operationtypeid " & _
                                 "          ,CAST(0 AS BIT) AS isdefault " & _
                                 "          ,'' AS newattachdocumentid " & _
                                 "          ,'' AS deleteattachdocumentid " & _
                                 "      FROM hremployee_idinfo_approval_tran AS t " & _
                                 "          JOIN hremployee_master AS e ON e.employeeunkid = t.employeeunkid " & _
                                 "      WHERE t.isvoid = 0 " & _
                                 "          AND #CNDT# " & _
                                 "  UNION " & _
                                 "      SELECT DISTINCT " & _
                                 "           '/r/n' + name AS IdType " & _
                                 "          ,t.serial_no AS serial_no " & _
                                 "          ,t.identity_no AS identity_no " & _
                                 "          ,co.country_name AS Country " & _
                                 "          ,t.issued_place AS issued_place " & _
                                 "          ,t.issue_date AS issue_date " & _
                                 "          ,t.expiry_date AS expiry_date " & _
                                 "          ,t.dl_class AS dl_class " & _
                                 "          ,t.tranguid AS tranguid " & _
                                 "          ,t.employeeunkid " & _
                                 "          ,0 AS isgrp " & _
                                 "          ,t.transactiondate " & _
                                 "          ,t.mappingunkid " & _
                                 "          ,t.approvalremark " & _
                                 "          ,t.isfinal " & _
                                 "          ,t.statusunkid " & _
                                 "          ,t.identitytranunkid " & _
                                 "          ,t.isprocessed " & _
                                 "          ,t.idtypeunkid " & _
                                 "          ,t.countryunkid " & _
                                 "          ,t.operationtypeid " & _
                                 "          ,t.isdefault " & _
                                 "          ,t.newattachdocumentid " & _
                                 "          ,t.deleteattachdocumentid " & _
                                 "      FROM hremployee_idinfo_approval_tran AS t " & _
                                 "          LEFT JOIN hrmsConfiguration..cfcountry_master AS co ON t.countryunkid = co.countryunkid " & _
                                 "          JOIN cfcommon_master ON t.idtypeunkid = cfcommon_master.masterunkid " & _
                                 "          JOIN hremployee_master AS e ON e.employeeunkid = t.employeeunkid " & _
                                 "      WHERE t.isvoid = 0 " & _
                                 "          AND #CNDT# " & _
                                 "  ) AS a " & _
                                 ") AS iData ON EM.employeeunkid = iData.employeeunkid "

                    StrTranUnkidCol = "identitytranunkid"
                    'Gajanan [22-Feb-2019] -- End

                    'Gajanan [9-April-2019] -- Start
                    strMatchCols = "idtypeunkid"
                    'Gajanan [9-April-2019] -- End

                    'Gajanan [18-Mar-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Case enScreenName.frmAddressList

                    StrTableName = " " & strDatabaseName & "..hremployee_address_approval_tran "

                    'S.SANDEEP |26-APR-2019| -- START
                    'StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN ROW_NUMBER() OVER (PARTITION BY iData.employeeunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "

                    'Gajanan [30-SEP-2019] -- Start    
                    'StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN ROW_NUMBER() OVER (PARTITION BY iData.employeeunkid,iData.addressid,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN DENSE_RANK() OVER (PARTITION BY iData.employeeunkid,iData.addressid,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    'Gajanan [30-SEP-2019] -- End
                    'S.SANDEEP |26-APR-2019| -- END

                    StrJoinColName = "employeeunkid"

                    StrSelectCols = ",iData.addresstype " & _
                                    ",iData.tranguid " & _
                                    ",iData.employeeunkid " & _
                                    ",iData.isgrp " & _
                                    ",iData.remark " & _
                                    ",iData.isfinal " & _
                                    ",iData.statusunkid " & _
                                    ",iData.address1 " & _
                                    ",iData.address2 " & _
                                    ",idata.country " & _
                                    ",idata.countryunkid " & _
                                    ",idata.state " & _
                                    ",idata.stateunkid " & _
                                    ",idata.city " & _
                                    ",idata.post_townunkid " & _
                                    ",idata.zipcode_code " & _
                                    ",idata.postcodeunkid " & _
                                    ",idata.provicnce " & _
                                    ",idata.road " & _
                                    ",idata.estate " & _
                                    ",idata.provicnce1 " & _
                                    ",idata.provinceunkid " & _
                                    ",idata.Road1 " & _
                                    ",idata.roadunkid " & _
                                    ",idata.chiefdom " & _
                                    ",idata.chiefdomunkid " & _
                                    ",idata.village " & _
                                    ",idata.villageunkid " & _
                                    ",idata.town " & _
                                    ",idata.town1unkid " & _
                                    ",idata.mobile " & _
                                    ",idata.tel_no " & _
                                    ",idata.plotNo " & _
                                    ",idata.alternateno " & _
                                    ",idata.email " & _
                                    ",idata.fax " & _
                                    ",iData.isprocessed " & _
                                    ",iData.addressid AS addresstypeid "
                                   
                    'S.SANDEEP |26-APR-2019| -- START
                    StrSelectCols &= ",iData.newattachdocumentid " & _
                                     ",iData.deleteattachdocumentid " & _
                                     ",iData.form_name "
                    'S.SANDEEP |26-APR-2019| -- END                                 

                    StrInnJoin = "JOIN " & _
                                 "( " & _
                                 "    SELECT " & _
                                        "a.address1 " & _
                                        ",a.tranguid " & _
                                        ",a.employeeunkid " & _
                                        ",a.isgrp " & _
                                        ",a.transactiondate " & _
                                        ",a.mappingunkid " & _
                                        ",a.remark " & _
                                        ",a.isfinal " & _
                                        ",a.statusunkid " & _
                                        ",a.isprocessed " & _
                                        ",a.address2 " & _
                                        ",a.country " & _
                                        ",a.countryunkid " & _
                                        ",a.state " & _
                                        ",a.stateunkid " & _
                                        ",a.city " & _
                                        ",a.post_townunkid " & _
                                        ",a.zipcode_code " & _
                                        ",a.postcodeunkid " & _
                                        ",a.provicnce " & _
                                        ",a.road " & _
                                        ",a.estate " & _
                                        ",a.provicnce1 " & _
                                        ",a.provinceunkid " & _
                                        ",a.Road1 " & _
                                        ",a.roadunkid " & _
                                        ",a.chiefdom " & _
                                        ",a.chiefdomunkid " & _
                                        ",a.village " & _
                                        ",a.villageunkid " & _
                                        ",a.town " & _
                                        ",a.town1unkid " & _
                                        ",a.mobile " & _
                                        ",a.tel_no " & _
                                        ",a.plotNo " & _
                                        ",a.alternateno " & _
                                        ",a.email " & _
                                        ",a.fax " & _
                                        ",a.addresstype " & _
                                        ",a.addressid "

                    'S.SANDEEP |26-APR-2019| -- START
                    StrInnJoin &= ",a.newattachdocumentid " & _
                                  ",a.deleteattachdocumentid " & _
                                  ",a.form_name "
                    'S.SANDEEP |26-APR-2019| -- END  

                    If eOperationType <> enOperationType.NONE Then
                        StrInnJoin &= "        ,a.operationtypeid "
                    End If

                    StrInnJoin &= "    FROM " & _
                                 "    ( " & _
                                 "        SELECT DISTINCT " & _
                                 "            e.employeecode + ' - ' + e.firstname + ' ' + e.surname AS addresstype " & _
                                 "            ,'' AS tranguid " & _
                                 "            ,t.employeeunkid " & _
                                 "            ,1 AS isgrp " & _
                                 "            ,NULL AS transactiondate " & _
                                 "            ,0 AS mappingunkid " & _
                                 "            ,'' AS remark " & _
                                 "            ,0 AS isfinal " & _
                                 "            ,1 AS statusunkid " & _
                                 "            ,CAST(0 AS BIT) AS isprocessed " & _
                                 "            ,'' AS address1 " & _
                                 "            ,'' AS address2 " & _
                                 "            ,'' AS country " & _
                                 "            ,0 AS countryunkid " & _
                                 "            ,'' AS state " & _
                                 "            ,0 AS stateunkid " & _
                                 "            ,'' AS city " & _
                                 "            ,0 AS post_townunkid " & _
                                 "            ,'' AS zipcode_code " & _
                                 "            ,0 AS postcodeunkid " & _
                                 "            ,'' AS provicnce " & _
                                 "            ,'' AS road " & _
                                 "            ,'' AS estate " & _
                                 "            ,'' AS provicnce1 " & _
                                 "            ,0 AS provinceunkid " & _
                                 "            ,'' AS Road1 " & _
                                 "            ,0 AS roadunkid " & _
                                 "            ,'' AS chiefdom " & _
                                 "            ,0 AS chiefdomunkid " & _
                                 "            ,'' AS village " & _
                                 "            ,0 AS villageunkid " & _
                                 "            ,'' AS town " & _
                                 "            ,0 AS town1unkid " & _
                                 "            ,'' AS mobile " & _
                                 "            ,'' AS tel_no " & _
                                 "            ,'' AS plotNo " & _
                                 "            ,'' AS alternateno " & _
                                 "            ,'' AS email " & _
                                 "            ,'' AS fax " & _
                                 "            ,0 AS addressid " & _
                                 "            ," & eOperationType & " AS operationtypeid " & _
                                 "            ,'' AS newattachdocumentid " & _
                                 "            ,'' AS deleteattachdocumentid " & _
                                 "            ,'' AS form_name " & _
                                 "        FROM hremployee_address_approval_tran AS t JOIN hremployee_master AS e ON e.employeeunkid = t.employeeunkid " & _
                                 "        WHERE t.isvoid = 0 AND #CNDT# " & _
                                 "        UNION " & _
                                 "        SELECT " & _
                                 "            '/r/n' + CASE " & _
                                 "                    WHEN addresstype = 1 THEN 'Present Address' " & _
                                 "                    WHEN addresstype = 2 THEN 'Domicile Address' " & _
                                 "                    WHEN addresstype = 3 THEN 'Recruitment Address' " & _
                                 "              END AS addresstype " & _
                                 "            ,t.tranguid " & _
                                 "            ,e.employeeunkid " & _
                                 "            ,0 AS isgrp " & _
                                 "            ,transactiondate " & _
                                 "            ,mappingunkid " & _
                                 "            ,remark " & _
                                 "            ,isfinal " & _
                                 "            ,statusunkid " & _
                                 "            ,t.isprocessed " & _
                                 "            ,t.address1 " & _
                                 "            ,t.address2 " & _
                                 "            ,co.country_name AS country " & _
                                 "            ,t.countryunkid " & _
                                 "            ,s.name AS state " & _
                                 "            ,t.stateunkid " & _
                                 "            ,c.name AS city " & _
                                 "            ,t.post_townunkid " & _
                                 "            ,z.zipcode_no " & _
                                 "            ,t.postcodeunkid " & _
                                 "            ,t.provicnce " & _
                                 "            ,t.road " & _
                                 "            ,t.estate " & _
                                 "            ,cm_prov.name AS provicnce1 " & _
                                 "            ,t.provinceunkid " & _
                                 "            ,cm_road.name AS Road1 " & _
                                 "            ,t.roadunkid " & _
                                 "            ,cm_chiefdom.name AS chiefdom " & _
                                 "            ,t.chiefdomunkid " & _
                                 "            ,cm_village.name AS village " & _
                                 "            ,t.villageunkid " & _
                                 "            ,cm_town.name AS town " & _
                                 "            ,t.town1unkid " & _
                                 "            ,t.mobile " & _
                                 "            ,t.tel_no " & _
                                 "            ,t.plotNo " & _
                                 "            ,t.alternateno " & _
                                 "            ,t.email " & _
                                 "            ,t.fax " & _
                                 "            ,t.addresstype as addressid " & _
                                 "            ,t.operationtypeid AS operationtypeid " & _
                                 "            ,t.newattachdocumentid " & _
                                 "            ,t.deleteattachdocumentid " & _
                                 "            ,t.form_name " & _
                                 "        FROM hremployee_address_approval_tran AS t " & _
                                 "         LEFT JOIN hrmsConfiguration..cfcountry_master AS co " & _
                                 "              ON co.countryunkid = t.countryunkid " & _
                                 "         LEFT JOIN hrmsConfiguration..cfstate_master AS s " & _
                                 "              ON s.stateunkid = t.stateunkid " & _
                                 "         LEFT JOIN hrmsConfiguration..cfcity_master AS c " & _
                                 "              ON c.cityunkid = t.post_townunkid " & _
                                 "         LEFT JOIN hrmsConfiguration..cfzipcode_master AS z " & _
                                 "              ON z.zipcodeunkid = t.postcodeunkid " & _
                                 "         LEFT JOIN cfcommon_master AS cm_prov " & _
                                 "              ON cm_prov.masterunkid = t.provinceunkid " & _
                                 "         LEFT JOIN cfcommon_master AS cm_road " & _
                                 "              ON cm_prov.masterunkid = t.roadunkid " & _
                                 "         LEFT JOIN cfcommon_master AS cm_chiefdom " & _
                                 "              ON cm_chiefdom.masterunkid = t.chiefdomunkid " & _
                                 "         LEFT JOIN cfcommon_master AS cm_village " & _
                                 "              ON cm_village.masterunkid = t.villageunkid " & _
                                 "         LEFT JOIN cfcommon_master AS cm_town " & _
                                 "              ON cm_town.masterunkid = t.town1unkid " & _
                                 "         LEFT JOIN hremployee_master AS e " & _
                                 "              ON e.employeeunkid = t.employeeunkid " & _
                                 "        WHERE t.isvoid = 0 AND #CNDT# " & _
                                 "    ) AS a " & _
                                 ") AS iData ON EM.employeeunkid = iData.employeeunkid "
                    StrTranUnkidCol = "employeeunkid"

                    'S.SANDEEP |26-APR-2019| -- START {newattachdocumentid,deleteattachdocumentid} -- END

                    'Gajanan [9-April-2019] -- Start
                    strMatchCols = "addresstype"
                    'Gajanan [9-April-2019] -- End

                Case enScreenName.frmEmergencyAddressList

                    StrTableName = " " & strDatabaseName & "..hremployee_emergency_address_approval_tran "

                    'S.SANDEEP |26-APR-2019| -- START
                    'StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN ROW_NUMBER() OVER (PARTITION BY iData.employeeunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "

                    'Gajanan [30-SEP-2019] -- Start    
		    'Issue Approver Not Able To See Data On Approver Screen [Metthew]    
                    'StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN ROW_NUMBER() OVER (PARTITION BY iData.employeeunkid,iData.addressid,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN DENSE_RANK() OVER (PARTITION BY iData.employeeunkid,iData.addressid,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    'Gajanan [30-SEP-2019] -- End

                    'S.SANDEEP |26-APR-2019| -- END

                    StrJoinColName = "employeeunkid"

                    StrSelectCols = ",iData.addresstype " & _
                                   ",iData.tranguid " & _
                                   ",iData.employeeunkid " & _
                                   ",iData.isgrp " & _
                                   ",iData.remark " & _
                                   ",iData.isfinal " & _
                                   ",iData.statusunkid " & _
                                   ",iData.firstname " & _
                                   ",iData.lastname " & _
                                   ",iData.address " & _
                                   ",idata.country " & _
                                   ",idata.countryunkid " & _
                                   ",idata.state " & _
                                   ",idata.stateunkid " & _
                                   ",idata.city " & _
                                   ",idata.townunkid " & _
                                   ",idata.zipcode_no  as  zipcode_code" & _
                                   ",idata.postcodeunkid " & _
                                   ",idata.provicnce " & _
                                   ",idata.road " & _
                                   ",idata.estate " & _
                                   ",idata.mobile " & _
                                   ",idata.tel_no " & _
                                   ",idata.plotNo " & _
                                   ",idata.alternateno " & _
                                   ",idata.email " & _
                                   ",idata.fax " & _
                                   ",iData.isprocessed " & _
                                   ",iData.addressid AS addresstypeid "

                    'S.SANDEEP |26-APR-2019| -- START
                    StrSelectCols &= ",iData.newattachdocumentid " & _
                                     ",iData.deleteattachdocumentid " & _
                                     ",iData.form_name "
                    'S.SANDEEP |26-APR-2019| -- END

                    StrInnJoin = "JOIN " & _
                                 "( " & _
                                 "    SELECT " & _
                                        "a.addresstype " & _
                                        ",a.tranguid " & _
                                        ",a.employeeunkid " & _
                                        ",a.isgrp " & _
                                        ",a.transactiondate " & _
                                        ",a.mappingunkid " & _
                                        ",a.remark " & _
                                        ",a.isfinal " & _
                                        ",a.statusunkid " & _
                                        ",a.isprocessed " & _
                                        ",a.firstname " & _
                                        ",a.lastname " & _
                                        ",a.address " & _
                                        ",a.country " & _
                                        ",a.countryunkid " & _
                                        ",a.state " & _
                                        ",a.stateunkid " & _
                                        ",a.city " & _
                                        ",a.townunkid " & _
                                        ",a.zipcode_no " & _
                                        ",a.postcodeunkid " & _
                                        ",a.provicnce " & _
                                        ",a.road " & _
                                        ",a.estate " & _
                                        ",a.plotNo " & _
                                        ",a.mobile " & _
                                        ",a.alternateno " & _
                                        ",a.tel_no " & _
                                        ",a.fax " & _
                                        ",a.email " & _
                                        ",a.addressid "

                    'S.SANDEEP |26-APR-2019| -- START
                    StrInnJoin &= ",a.newattachdocumentid " & _
                                  ",a.deleteattachdocumentid " & _
                                  ",a.form_name "
                    'S.SANDEEP |26-APR-2019| -- END  

                    If eOperationType <> enOperationType.NONE Then
                        StrInnJoin &= "        ,a.operationtypeid "
                    End If

                    StrInnJoin &= "    FROM " & _
                                 "    ( " & _
                                 "        SELECT DISTINCT " & _
                                 "              e.employeecode + ' - ' + e.firstname + ' ' + e.surname AS addresstype " & _
                                 "              ,'' AS tranguid " & _
                                 "              ,t.employeeunkid " & _
                                 "              ,1 AS isgrp " & _
                                 "              ,NULL AS transactiondate " & _
                                 "              ,0 AS mappingunkid " & _
                                 "              ,'' AS remark " & _
                                 "              ,0 AS isfinal " & _
                                 "              ,1 AS statusunkid " & _
                                 "              ,CAST(0 AS BIT) AS isprocessed " & _
                                 "              ,'' as firstname " & _
                                 "              ,'' as lastname " & _
                                 "              ,'' as address " & _
                                 "              ,'' AS country " & _
                                 "              ,0 as countryunkid " & _
                                 "              ,'' AS state " & _
                                 "              ,0 as stateunkid " & _
                                 "              ,'' AS city " & _
                                 "              ,0 as townunkid " & _
                                 "              ,'' as zipcode_no " & _
                                 "              ,0 AS postcodeunkid " & _
                                 "              ,'' AS provicnce " & _
                                 "              ,'' AS road " & _
                                 "              ,'' AS estate " & _
                                 "              ,'' AS plotNo " & _
                                 "              ,'' AS mobile " & _
                                 "              ,'' AS alternateno " & _
                                 "              ,'' AS tel_no " & _
                                 "              ,'' AS fax " & _
                                 "              ,'' AS email " & _
                                 "              ,0 AS addressid " & _
                                 "             ," & eOperationType & " AS operationtypeid " & _
                                 "              ,'' AS newattachdocumentid " & _
                                 "              ,'' AS deleteattachdocumentid " & _
                                 "              ,'' AS form_name " & _
                                 "        FROM hremployee_emergency_address_approval_tran AS t JOIN hremployee_master AS e ON e.employeeunkid = t.employeeunkid " & _
                                 "        WHERE t.isvoid = 0 AND #CNDT# " & _
                                 "        UNION " & _
                                 "        SELECT " & _
                                             "'/r/n' + " & _
                                            "CASE " & _
                                            "WHEN addresstype = 1 THEN 'Emergency Address-1' " & _
                                            "WHEN addresstype = 2 THEN 'Emergency Address-2' " & _
                                            "WHEN addresstype = 3 THEN 'Emergency Address-3' " & _
                                            "ELSE '' " & _
                                       "END AS addresstype " & _
                                     ",t.tranguid " & _
                                     ",e.employeeunkid " & _
                                     ",0 AS isgrp " & _
                                     ",transactiondate " & _
                                     ",mappingunkid " & _
                                     ",remark " & _
                                     ",isfinal " & _
                                     ",statusunkid " & _
                                     ",t.isprocessed " & _
                                     ",t.firstname " & _
                                     ",t.lastname " & _
                                     ",t.address " & _
                                     ",co.country_name AS country " & _
                                     ",t.countryunkid " & _
                                     ",s.name AS state " & _
                                     ",t.stateunkid " & _
                                     ",c.name AS city " & _
                                     ",t.townunkid " & _
                                     ",z.zipcode_no " & _
                                     ",t.postcodeunkid " & _
                                     ",t.provicnce " & _
                                     ",t.road " & _
                                     ",t.estate " & _
                                     ",t.plotNo " & _
                                     ",t.mobile " & _
                                     ",t.alternateno " & _
                                     ",t.tel_no " & _
                                     ",t.fax " & _
                                     ",t.email " & _
                                     ",t.addresstype as addressid " & _
                                     ",t.operationtypeid AS operationtypeid " & _
                                     ",t.newattachdocumentid " & _
                                     ",t.deleteattachdocumentid " & _
                                     ",t.form_name " & _
                                  "FROM hremployee_emergency_address_approval_tran AS t " & _
                                  "LEFT JOIN hrmsConfiguration..cfcountry_master AS co " & _
                                       "ON co.countryunkid = t.countryunkid " & _
                                  "LEFT JOIN hrmsConfiguration..cfstate_master AS s " & _
                                       "ON s.stateunkid = t.stateunkid " & _
                                  "LEFT JOIN hrmsConfiguration..cfcity_master AS c " & _
                                       "ON c.cityunkid = t.townunkid " & _
                                  "LEFT JOIN hrmsConfiguration..cfzipcode_master AS z " & _
                                       "ON z.zipcodeunkid = t.postcodeunkid " & _
                                  "LEFT JOIN hremployee_master AS e " & _
                                       "ON e.employeeunkid = t.employeeunkid " & _
                                 "        WHERE t.isvoid = 0 AND #CNDT# " & _
                                 "    ) AS a " & _
                                 ") AS iData ON EM.employeeunkid = iData.employeeunkid "
                    StrTranUnkidCol = "employeeunkid"

                    'S.SANDEEP |26-APR-2019| -- START {newattachdocumentid,deleteattachdocumentid} -- END

                    'Gajanan [18-Mar-2019] -- End

                    'Gajanan [9-April-2019] -- Start
                    strMatchCols = "addresstype"
                    'Gajanan [9-April-2019] -- End

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Case enScreenName.frmOtherinfo
                    StrTableName = " " & strDatabaseName & "..hremployee_personal_approval_tran "
                    'Gajanan [9-April-2019] -- Start
                    'StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN ROW_NUMBER() OVER (PARTITION BY iData.emp_app_unkid,iData.skillunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN DENSE_RANK() OVER (PARTITION BY iData.employeeunkid,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    'Gajanan [9-April-2019] -- End

                    StrJoinColName = "employeeunkid"

                    StrSelectCols = ",iData.Complexion " & _
                                    ",iData.tranguid " & _
                                    ",iData.employeeunkid " & _
                                    ",iData.isgrp " & _
                                    ",iData.remark " & _
                                    ",iData.isfinal " & _
                                    ",iData.statusunkid " & _
                                    ",idata.complexionunkid " & _
                                    ",idata.BloodGroup " & _
                                    ",idata.bloodgroupunkid " & _
                                    ",idata.EyeColor " & _
                                    ",idata.eyecolorunkid " & _
                                    ",idata.Nationality " & _
                                    ",idata.nationalityunkid " & _
                                    ",idata.Ethnicity " & _
                                    ",idata.ethnicityunkid " & _
                                    ",idata.Religion " & _
                                    ",idata.religionunkid " & _
                                    ",idata.HairColor " & _
                                    ",idata.hairunkid " & _
                                    ",idata.Maritalstatus " & _
                                    ",idata.maritalstatusunkid " & _
                                    ",idata.ExtTelephoneno " & _
                                    ",idata.language1 " & _
                                    ",idata.language1unkid " & _
                                    ",idata.language2 " & _
                                    ",idata.language2unkid " & _
                                    ",idata.language3 " & _
                                    ",idata.language3unkid " & _
                                    ",idata.language4 " & _
                                    ",idata.language4unkid " & _
                                    ",idata.height " & _
                                    ",idata.weight " & _
                                    ",idata.anniversary_date " & _
                                    ",idata.allergiesunkids " & _
                                    ",iData.Allergies " & _
                                    ",idata.disabilitiesunkids " & _
                                    ",idata.sports_hobbies " & _
                                    ",idata.isbirthinfo "

                    'S.SANDEEP |26-APR-2019| -- START
                    StrSelectCols &= ",idata.newattachdocumentid " & _
                                     ",idata.deleteattachdocumentid " & _
                                     ",idata.form_name "
                    'S.SANDEEP |26-APR-2019| -- END

                    StrInnJoin = "JOIN " & _
                                 "( " & _
                                 " SELECT " & _
                                 " a.Complexion " & _
                                 ",a.tranguid " & _
                                 ",a.employeeunkid " & _
                                 ",a.isgrp " & _
                                 ",a.transactiondate " & _
                                 ",a.mappingunkid " & _
                                 ",a.remark " & _
                                 ",a.isfinal " & _
                                 ",a.statusunkid " & _
                                 ",a.isprocessed " & _
                                 ",a.complexionunkid " & _
                                 ",a.BloodGroup " & _
                                 ",a.bloodgroupunkid " & _
                                 ",a.EyeColor " & _
                                 ",a.eyecolorunkid " & _
                                 ",a.Nationality " & _
                                 ",a.nationalityunkid " & _
                                 ",a.Ethnicity " & _
                                 ",a.ethnicityunkid " & _
                                 ",a.Religion " & _
                                 ",a.religionunkid " & _
                                 ",a.HairColor " & _
                                 ",a.hairunkid " & _
                                 ",a.Maritalstatus " & _
                                 ",a.maritalstatusunkid " & _
                                 ",a.ExtTelephoneno " & _
                                 ",a.language1 " & _
                                 ",a.language1unkid " & _
                                 ",a.language2 " & _
                                 ",a.language2unkid " & _
                                 ",a.language3 " & _
                                 ",a.language3unkid " & _
                                 ",a.language4 " & _
                                 ",a.language4unkid " & _
                                 ",a.height " & _
                                 ",a.weight " & _
                                 ",a.anniversary_date " & _
                                 ",a.allergiesunkids " & _
                                 ",a.Allergies " & _
                                 ",a.disabilitiesunkids " & _
                                 ",a.sports_hobbies " & _
                                 ",a.isbirthinfo "

                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    If eOperationType <> enOperationType.NONE Then
                        StrInnJoin &= "        ,a.operationtypeid "
                    End If
                    'Gajanan [17-DEC-2018] -- End

                    'S.SANDEEP |26-APR-2019| -- START
                    StrInnJoin &= ",a.newattachdocumentid " & _
                                  ",a.deleteattachdocumentid " & _
                                  ",a.form_name "
                    'S.SANDEEP |26-APR-2019| -- END

                    StrInnJoin &= "    FROM " & _
                                 "    ( " & _
                                 "        SELECT DISTINCT " & _
                                 "            e.employeecode + ' - ' + e.firstname + ' ' + e.surname AS Complexion " & _
                                 "            ,'' AS tranguid " & _
                                 "            ,t.employeeunkid " & _
                                 "            ,1 AS isgrp " & _
                                 "            ,NULL AS transactiondate " & _
                                 "            ,0 AS mappingunkid " & _
                                 "            ,'' AS remark " & _
                                 "            ,0 AS isfinal " & _
                                 "            ,1 AS statusunkid " & _
                                 "            ,CAST(0 AS BIT) AS isprocessed " & _
                                 "            ,0  as complexionunkid " & _
                                 "            ,'' AS BloodGroup " & _
                                 "            ,0  as bloodgroupunkid " & _
                                 "            ,'' AS EyeColor " & _
                                 "            ,0  as eyecolorunkid " & _
                                 "            ,'' AS Nationality " & _
                                 "            ,0  as nationalityunkid " & _
                                 "            ,'' AS Ethnicity " & _
                                 "            ,0  As ethnicityunkid " & _
                                 "            ,'' AS Religion " & _
                                 "            ,0  As religionunkid " & _
                                 "            ,'' AS HairColor " & _
                                 "            ,0  As hairunkid " & _
                                 "            ,'' AS Maritalstatus " & _
                                 "            ,0  As maritalstatusunkid " & _
                                 "            ,'' AS ExtTelephoneno " & _
                                 "            ,'' AS language1 " & _
                                 "            ,0  As language1unkid " & _
                                 "            ,'' AS language2 " & _
                                 "            ,0  as language2unkid " & _
                                 "            ,'' AS language3 " & _
                                 "            ,0  AS language3unkid " & _
                                 "            ,'' AS language4 " & _
                                 "            ,0  AS language4unkid " & _
                                 "            ,0 as height " & _
                                 "            ,0 as weight " & _
                                 "            ,null as anniversary_date " & _
                                 "            ,'' as allergiesunkids " & _
                                 "            ,'' as Allergies" & _
                                 "            ,'' as disabilitiesunkids " & _
                                 "            ,'' as sports_hobbies " & _
                                 "            ,0  as isbirthinfo" & _
                                 "            ," & eOperationType & " AS operationtypeid " & _
                                 "            ,'' AS newattachdocumentid " & _
                                 "            ,'' AS deleteattachdocumentid " & _
                                 "            ,'' AS form_name " & _
                                 "        FROM hremployee_personal_approval_tran AS t JOIN hremployee_master AS e ON e.employeeunkid = t.employeeunkid " & _
                                 "        WHERE t.isvoid = 0 AND #CNDT# " & _
                                 "        UNION " & _
                                 "        SELECT " & _
                                 "            '/r/n' + cm_complexion.name AS Complexion " & _
                                 "            ,t.tranguid " & _
                                 "            ,e.employeeunkid " & _
                                 "            ,0 AS isgrp " & _
                                 "            ,transactiondate " & _
                                 "            ,mappingunkid " & _
                                 "            ,remark " & _
                                 "            ,isfinal " & _
                                 "            ,statusunkid " & _
                                 "            ,t.isprocessed " & _
                                 "            ,t.complexionunkid " & _
                                 "            ,cm_bloodgrp.name AS BloodGroup " & _
                                 "            ,t.bloodgroupunkid " & _
                                 "            ,cm_eyecolor.name AS EyeColor " & _
                                 "            ,t.eyecolorunkid " & _
                                 "            ,cm_nationality.name AS Nationality " & _
                                 "            ,t.nationalityunkid " & _
                                 "            ,cm_ethnicity.name AS Ethnicity " & _
                                 "            ,t.ethnicityunkid " & _
                                 "            ,cm_religion.name AS Religion " & _
                                 "            ,t.religionunkid " & _
                                 "            ,cm_hair.name AS HairColor " & _
                                 "            ,t.hairunkid " & _
                                 "            ,cm_maritalstatus.name AS Maritalstatus " & _
                                 "            ,t.maritalstatusunkid " & _
                                 "            ,t.extra_tel_no AS ExtTelephoneno " & _
                                 "            ,cm_language1.name AS language1 " & _
                                 "            ,t.language1unkid " & _
                                 "            ,cm_language2.name AS language2 " & _
                                 "            ,t.language2unkid " & _
                                 "            ,cm_language3.name AS language3 " & _
                                 "            ,t.language3unkid " & _
                                 "            ,cm_language4.name AS language4 " & _
                                 "            ,t.language4unkid " & _
                                 "            ,t.height " & _
                                 "            ,t.weight " & _
                                 "            ,t.anniversary_date " & _
                                 "            ,t.allergiesunkids " & _
                                 "            ,(SELECT " & _
                                               "STUFF " & _
                                                    "((SELECT " & _
                                                                 "',' + s.name " & _
                                                            "FROM cfcommon_master s " & _
                                                            "JOIN (SELECT " & _
                                                                      "CAST(Split.a.value('.', 'VARCHAR(100)') AS INT) AS ovalue " & _
                                                                 "FROM (SELECT " & _
                                                                           "[allergiesunkids] " & _
                                                                         ",CAST('<M>' + REPLACE([allergiesunkids], ',', '</M><M>') + '</M>' AS XML) AS String " & _
                                                                      "FROM hremployee_personal_approval_tran " & _
                                                                      "WHERE hremployee_personal_approval_tran.tranguid = t.tranguid) AS A " & _
                                                                 "CROSS APPLY String.nodes('/M') AS Split (a)) AS O " & _
                                                                 "ON O.ovalue = s.masterunkid " & _
                                                            "ORDER BY s.name " & _
                                                            "FOR XML PATH ('')) " & _
                                                       ", " & _
                                                       "1, 1, '' " & _
                                                       ")) " & _
                                              "AS Allergies " & _
                                 "            ,t.disabilitiesunkids " & _
                                 "            ,t.sports_hobbies " & _
                                 "            ,t.isbirthinfo " & _
                                 "            ,t.operationtypeid AS operationtypeid " & _
                                 "            ,t.newattachdocumentid " & _
                                 "            ,t.deleteattachdocumentid " & _
                                 "            ,t.form_name " & _
                                 "FROM hremployee_personal_approval_tran AS t " & _
                                 "LEFT JOIN cfcommon_master AS cm_complexion " & _
                                      "ON cm_complexion.masterunkid = t.complexionunkid " & _
                                 "LEFT JOIN cfcommon_master AS cm_bloodgrp " & _
                                      "ON cm_bloodgrp.masterunkid = t.bloodgroupunkid " & _
                                 "LEFT JOIN cfcommon_master AS cm_blood " & _
                                      "ON cm_blood.masterunkid = t.bloodgroupunkid " & _
                                 "LEFT JOIN cfcommon_master AS cm_eyecolor " & _
                                      "ON cm_eyecolor.masterunkid = t.eyecolorunkid " & _
                                 "LEFT JOIN cfcommon_master AS cm_nationality " & _
                                      "ON cm_nationality.masterunkid = t.nationalityunkid " & _
                                 "LEFT JOIN cfcommon_master AS cm_ethnicity " & _
                                      "ON cm_ethnicity.masterunkid = t.ethnicityunkid " & _
                                 "LEFT JOIN cfcommon_master AS cm_religion " & _
                                      "ON cm_religion.masterunkid = t.religionunkid " & _
                                 "LEFT JOIN cfcommon_master AS cm_hair " & _
                                      "ON cm_hair.masterunkid = t.hairunkid " & _
                                 "LEFT JOIN cfcommon_master AS cm_maritalstatus " & _
                                      "ON cm_maritalstatus.masterunkid = t.maritalstatusunkid " & _
                                 "LEFT JOIN cfcommon_master AS cm_language1 " & _
                                      "ON cm_language1.masterunkid = t.language1unkid " & _
                                 "LEFT JOIN cfcommon_master AS cm_language2 " & _
                                      "ON cm_language2.masterunkid = t.language1unkid " & _
                                 "LEFT JOIN cfcommon_master AS cm_language3 " & _
                                      "ON cm_language3.masterunkid = t.language1unkid " & _
                                 "LEFT JOIN cfcommon_master AS cm_language4 " & _
                                      "ON cm_language4.masterunkid = t.language1unkid " & _
                                 "LEFT JOIN hremployee_master AS e " & _
                                      "ON e.employeeunkid = t.employeeunkid " & _
                                 "WHERE t.isvoid = 0 AND #CNDT# " & _
                                 "    ) AS a " & _
                                 ") AS iData ON EM.employeeunkid = iData.employeeunkid "
                    't.isvoid = 0 AND t.skillstranunkid <= 0 AND t.isprocessed = 0
                    StrTranUnkidCol = "employeeunkid"


                    'Gajanan [9-April-2019] -- Start
                    strMatchCols = "isbirthinfo"
                    'Gajanan [9-April-2019] -- End

                    'S.SANDEEP |26-APR-2019| -- START {newattachdocumentid,deleteattachdocumentid,form_name} -- END

                Case enScreenName.frmBirthinfo
                    StrTableName = " " & strDatabaseName & "..hremployee_personal_approval_tran "


                    'Gajanan [9-April-2019] -- Start
                    'StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN ROW_NUMBER() OVER (PARTITION BY iData.emp_app_unkid,iData.skillunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN DENSE_RANK() OVER (PARTITION BY iData.employeeunkid,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    'Gajanan [9-April-2019] -- End

                    StrJoinColName = "employeeunkid"

                    StrSelectCols = ",iData.country " & _
                                    ",iData.tranguid " & _
                                    ",iData.employeeunkid " & _
                                    ",iData.isgrp " & _
                                    ",iData.remark " & _
                                    ",iData.isfinal " & _
                                    ",iData.statusunkid " & _
                                    ",idata.birthcountryunkid " & _
                                    ",idata.state " & _
                                    ",iData.birthstateunkid " & _
                                    ",iData.city " & _
                                    ",idata.country " & _
                                    ",idata.birthcityunkid " & _
                                    ",idata.birth_ward " & _
                                    ",idata.birthcertificateno " & _
                                    ",idata.birth_village " & _
                                    ",idata.town " & _
                                    ",idata.birthtownunkid " & _
                                    ",idata.village " & _
                                    ",idata.birthvillageunkid " & _
                                    ",idata.chiefdom " & _
                                    ",idata.birthchiefdomunkid " & _
                                    ",idata.isbirthinfo "

                    'S.SANDEEP |26-APR-2019| -- START
                    StrSelectCols &= ",idata.newattachdocumentid " & _
                                     ",idata.deleteattachdocumentid " & _
                                     ",idata.form_name "
                    'S.SANDEEP |26-APR-2019| -- END

                    StrInnJoin = "JOIN " & _
                                 "( " & _
                                 " SELECT " & _
                                 " a.country " & _
                                 ",a.tranguid " & _
                                 ",a.employeeunkid " & _
                                 ",a.isgrp " & _
                                 ",a.transactiondate " & _
                                 ",a.mappingunkid " & _
                                 ",a.remark " & _
                                 ",a.isfinal " & _
                                 ",a.statusunkid " & _
                                 ",a.isprocessed " & _
                                 ",a.birthcountryunkid " & _
                                 ",a.state " & _
                                 ",a.birthstateunkid " & _
                                 ",a.city " & _
                                 ",a.birthcityunkid " & _
                                 ",a.birth_ward " & _
                                 ",a.birthcertificateno " & _
                                 ",a.birth_village " & _
                                 ",a.town " & _
                                 ",a.birthtownunkid " & _
                                 ",a.village " & _
                                 ",a.birthvillageunkid " & _
                                 ",a.chiefdom " & _
                                 ",a.birthchiefdomunkid " & _
                                 ",a.isbirthinfo "

                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    If eOperationType <> enOperationType.NONE Then
                        StrInnJoin &= "        ,a.operationtypeid "
                    End If
                    'Gajanan [17-DEC-2018] -- End

                    'S.SANDEEP |26-APR-2019| -- START
                    StrInnJoin &= "   ,a.newattachdocumentid " & _
                                  "   ,a.deleteattachdocumentid " & _
                                  "   ,a.form_name "
                    'S.SANDEEP |26-APR-2019| -- END

                    StrInnJoin &= "    FROM " & _
                                 "    ( " & _
                                 "        SELECT DISTINCT " & _
                                 "            e.employeecode + ' - ' + e.firstname + ' ' + e.surname AS country " & _
                                 "            ,'' AS tranguid " & _
                                 "            ,t.employeeunkid " & _
                                 "            ,1 AS isgrp " & _
                                 "            ,NULL AS transactiondate " & _
                                 "            ,0 AS mappingunkid " & _
                                 "            ,'' AS remark " & _
                                 "            ,0 AS isfinal " & _
                                 "            ,1 AS statusunkid " & _
                                 "            ,CAST(0 AS BIT) AS isprocessed " & _
                                 "            ,0 AS birthcountryunkid " & _
                                 "            ,'' AS state " & _
                                 "            ,0 as birthstateunkid " & _
                                 "            ,'' AS city " & _
                                 "            ,0 as birthcityunkid " & _
                                 "            ,'' AS birth_ward " & _
                                 "            ,'' AS birthcertificateno " & _
                                 "            ,'' AS birth_village " & _
                                 "            ,'' AS town " & _
                                 "            ,0 as birthtownunkid " & _
                                 "            ,'' AS village " & _
                                 "            ,0  as birthvillageunkid " & _
                                 "            ,'' AS chiefdom " & _
                                 "            ,0  as birthchiefdomunkid " & _
                                 "            ,1  as isbirthinfo" & _
                                 "            ," & eOperationType & " AS operationtypeid " & _
                                 "            ,'' AS newattachdocumentid " & _
                                 "            ,'' AS deleteattachdocumentid " & _
                                 "            ,'' AS form_name " & _
                                 "        FROM hremployee_personal_approval_tran AS t JOIN hremployee_master AS e ON e.employeeunkid = t.employeeunkid " & _
                                 "        WHERE t.isvoid = 0 AND #CNDT# " & _
                                 "        UNION " & _
                                 "        SELECT " & _
                                 "            '/r/n' + co.country_name AS country " & _
                                 "            ,t.tranguid " & _
                                 "            ,e.employeeunkid " & _
                                 "            ,0 AS isgrp " & _
                                 "            ,transactiondate " & _
                                 "            ,mappingunkid " & _
                                 "            ,remark " & _
                                 "            ,isfinal " & _
                                 "            ,statusunkid " & _
                                 "            ,t.isprocessed " & _
                                 "            ,t.birthcountryunkid " & _
                                 "            ,s.name AS state " & _
                                 "            ,t.birthstateunkid " & _
                                 "            ,c.name AS city " & _
                                 "            ,t.birthcityunkid " & _
                                 "            ,t.birth_ward " & _
                                 "            ,t.birthcertificateno " & _
                                 "            ,t.birth_village " & _
                                 "            ,cm_town.name AS town " & _
                                 "            ,t.birthtownunkid " & _
                                 "            ,cm_village.name AS village " & _
                                 "            ,t.birthvillageunkid " & _
                                 "            ,cm_chiefdom.name AS chiefdom " & _
                                 "            ,t.birthchiefdomunkid " & _
                                 "            ,t.isbirthinfo " & _
                                 "            ,t.operationtypeid AS operationtypeid " & _
                                 "            ,t.newattachdocumentid " & _
                                 "            ,t.deleteattachdocumentid " & _
                                 "            ,t.form_name " & _
                                 "FROM hremployee_personal_approval_tran AS t " & _
                                 "LEFT JOIN hrmsConfiguration..cfcountry_master AS co " & _
                                      "ON co.countryunkid = t.birthcountryunkid " & _
                                 "LEFT JOIN hrmsConfiguration..cfstate_master AS s " & _
                                      "ON s.stateunkid = t.birthstateunkid " & _
                                 "LEFT JOIN hrmsConfiguration..cfcity_master AS c " & _
                                      "ON c.cityunkid = t.birthcityunkid " & _
                                 "LEFT JOIN cfcommon_master AS cm_chiefdom " & _
                                      "ON cm_chiefdom.masterunkid = t.birthchiefdomunkid " & _
                                 "LEFT JOIN cfcommon_master AS cm_village " & _
                                      "ON cm_village.masterunkid = t.birthvillageunkid " & _
                                 "LEFT JOIN cfcommon_master AS cm_town " & _
                                      "ON cm_town.masterunkid = t.birthtownunkid " & _
                                 "LEFT JOIN hremployee_master AS e " & _
                                      "ON e.employeeunkid = t.employeeunkid " & _
                                 "WHERE t.isvoid = 0 AND #CNDT# " & _
                                 "    ) AS a " & _
                                 ") AS iData ON EM.employeeunkid = iData.employeeunkid "
                    't.isvoid = 0 AND t.skillstranunkid <= 0 AND t.isprocessed = 0
                    StrTranUnkidCol = "employeeunkid"


                    'Gajanan [9-April-2019] -- Start
                    strMatchCols = ""
                    'Gajanan [9-April-2019] -- End

                    'Gajanan [17-April-2019] -- End

                    'S.SANDEEP |26-APR-2019| -- START {newattachdocumentid,deleteattachdocumentid,form_name} -- END

                    'S.SANDEEP |15-APR-2019| -- START
                Case enScreenName.frmMembershipInfoList
                    StrTableName = " " & strDatabaseName & "..hremployee_meminfo_approval_tran "
                    StrRowNumberCol = ",CASE WHEN iData.isgrp = 0 THEN ROW_NUMBER() OVER (PARTITION BY iData.employeeunkid,iData.membershipunkid,B.mappingunkid ORDER BY Fn.priority ASC) ELSE 0 END AS rno "
                    StrJoinColName = "employeeunkid"
                    StrSelectCols = ",iData.Category " & _
                                    ",iData.membershipname " & _
                                    ",iData.membershipno " & _
                                    ",iData.issue_date " & _
                                    ",iData.start_date " & _
                                    ",iData.expiry_date " & _
                                    ",iData.remark " & _
                                    ",ISNULL(NV.xPeriodName,ISNULL(B.iPeriodName, iData.period_name)) AS period_name " & _
                                    ",iData.isfinal " & _
                                    ",iData.statusunkid " & _
                                    ",iData.approvalremark " & _
                                    ",iData.membership_categoryunkid " & _
                                    ",iData.membershipunkid " & _
                                    ",ISNULL(NV.xPeriodId,ISNULL(B.iPeriodId, iData.effetiveperiodid)) AS effetiveperiodid " & _
                                    ",ISNULL(NV.xPeriodId,ISNULL(B.iPeriodId, iData.effetiveperiodid)) AS periodunkid " & _
                                    ",iData.newattachdocumentid " & _
                                    ",iData.deleteattachdocumentid " & _
                                    ",0 AS loginemployeeunkid " & _
                                    ",CAST(0 AS BIT) AS isvoid " & _
                                    ",'' AS voidreason " & _
                                    ",CAST(NULL AS DATETIME) AS auditdatetime " & _
                                    ",0 AS audittype " & _
                                    ",0 AS audituserunkid " & _
                                    ",'' AS ip " & _
                                    ",'' AS host " & _
                                    ",'' AS form_name " & _
                                    ",CAST(0 AS BIT) AS isweb " & _
                                    ",'A' AS AUD " & _
                                    ",iData.membershiptranunkid " & _
                                    ",iData.copyedslab " & _
                                    ",iData.overwritehead " & _
                                    ",iData.overwriteslab " & _
                                    ",iData.isactive " & _
                                    ",iData.isdeleted " & _
                                    ",iData.rehiretranunkid " & _
                                    ",iData.emptranheadunkid " & _
                                    ",iData.cotranheadunkid " & _
                                    ",iData.emptranheadunkid AS emptrnheadid " & _
                                    ",iData.cotranheadunkid AS cotrnheadid "

                    StrInnJoin = "JOIN " & _
                                 "( " & _
                                 "     SELECT " & _
                                 "          a.Category " & _
                                 "         ,a.membershipname " & _
                                 "         ,a.membershipno " & _
                                 "         ,a.issue_date " & _
                                 "         ,a.start_date " & _
                                 "         ,a.expiry_date " & _
                                 "         ,a.remark " & _
                                 "         ,a.period_name " & _
                                 "         ,a.isfinal " & _
                                 "         ,a.statusunkid " & _
                                 "         ,a.approvalremark " & _
                                 "         ,a.tranguid " & _
                                 "         ,a.isgrp " & _
                                 "         ,a.isprocessed " & _
                                 "         ,a.operationtypeid " & _
                                 "         ,a.membership_categoryunkid " & _
                                 "         ,a.membershipunkid " & _
                                 "         ,a.periodunkid AS effetiveperiodid " & _
                                 "         ,a.newattachdocumentid " & _
                                 "         ,a.deleteattachdocumentid " & _
                                 "         ,a.employeeunkid " & _
                                 "         ,a.transactiondate " & _
                                 "         ,a.mappingunkid " & _
                                 "         ,a.membershiptranunkid " & _
                                 "         ,a.isactive " & _
                                 "         ,a.isdeleted " & _
                                 "         ,a.rehiretranunkid " & _
                                 "         ,a.copyedslab " & _
                                 "         ,a.overwritehead " & _
                                 "         ,a.overwriteslab " & _
                                 "         ,a.emptranheadunkid " & _
                                 "         ,a.cotranheadunkid " & _
                                 "     FROM " & _
                                 "     ( " & _
                                 "         SELECT DISTINCT " & _
                                 "              EM.employeecode + ' - ' + EM.firstname +' '+EM.surname AS Category " & _
                                 "             ,'' AS membershipname " & _
                                 "             ,'' AS membershipno " & _
                                 "             ,CAST(NULL AS DATETIME) AS issue_date " & _
                                 "             ,CAST(NULL AS DATETIME) AS start_date " & _
                                 "             ,CAST(NULL AS DATETIME) AS expiry_date " & _
                                 "             ,'' AS remark " & _
                                 "             ,'' AS period_name " & _
                                 "             ,'' AS tranguid " & _
                                 "             ,EM.employeeunkid AS employeeunkid " & _
                                 "             ,1 AS isgrp " & _
                                 "             ,NULL AS transactiondate " & _
                                 "             ,0 AS mappingunkid " & _
                                 "             ,0 AS isfinal " & _
                                 "             ,1 AS statusunkid " & _
                                 "             ,-1 AS membershiptranunkid " & _
                                 "             ,CAST(0 AS BIT) AS isprocessed " & _
                                 "             ,0 AS membership_categoryunkid " & _
                                 "             ,0 AS membershipunkid " & _
                                 "             ," & eOperationType & " AS operationtypeid " & _
                                 "             ,'' AS approvalremark " & _
                                 "            ,0 AS periodunkid " & _
                                 "            ,'' AS newattachdocumentid " & _
                                 "            ,'' AS deleteattachdocumentid " & _
                                 "            ,CAST(1 AS BIT) AS isactive " & _
                                 "            ,CAST(0 AS BIT) AS isdeleted " & _
                                 "            ,0 AS rehiretranunkid " & _
                                 "            ,0 AS copyedslab " & _
                                 "            ,0 AS overwritehead " & _
                                 "            ,0 AS overwriteslab " & _
                                 "            ,0 AS emptranheadunkid " & _
                                 "            ,0 AS cotranheadunkid " & _
                                 "         FROM hremployee_meminfo_approval_tran AS t " & _
                                 "             JOIN hremployee_master AS EM ON t.employeeunkid = EM.employeeunkid " & _
                                 "         WHERE t.isvoid = 0 " & _
                                 "         AND #CNDT# " & _
                                 "         UNION " & _
                                 "         SELECT " & _
                                 "             '/r/n' + MCT.name AS Category " & _
                                 "            ,MBM.membershipname " & _
                                 "            ,t.membershipno " & _
                                 "            ,t.issue_date " & _
                                 "            ,t.start_date " & _
                                 "            ,t.expiry_date " & _
                                 "            ,ISNULL(t.remark,'') AS remark " & _
                                 "            ,ISNULL(MCP.period_name,'') AS period_name " & _
                                 "            ,t.tranguid " & _
                                 "            ,t.employeeunkid " & _
                                 "            ,0 AS isgrp " & _
                                 "            ,t.transactiondate " & _
                                 "            ,t.mappingunkid " & _
                                 "            ,t.isfinal " & _
                                 "            ,t.statusunkid " & _
                                 "            ,t.membershiptranunkid " & _
                                 "            ,t.isprocessed " & _
                                 "            ,t.membership_categoryunkid " & _
                                 "            ,t.membershipunkid " & _
                                 "            ,t.operationtypeid " & _
                                 "            ,t.approvalremark " & _
                                 "            ,t.periodunkid " & _
                                 "            ,t.newattachdocumentid " & _
                                 "            ,t.deleteattachdocumentid " & _
                                 "            ,t.isactive " & _
                                 "            ,t.isdeleted " & _
                                 "            ,t.rehiretranunkid " & _
                                 "            ,t.iscopyprevious_slab AS copyedslab " & _
                                 "            ,t.isoverwriteheads AS overwritehead " & _
                                 "            ,t.isoverwriteslab AS overwriteslab " & _
                                 "            ,MBM.emptranheadunkid " & _
                                 "            ,MBM.cotranheadunkid " & _
                                 "         FROM hremployee_meminfo_approval_tran AS t " & _
                                 "             JOIN cfcommon_master MCT ON MCT.masterunkid = t.membership_categoryunkid AND MCT.isactive = 1 " & _
                                 "             JOIN hrmembership_master AS MBM ON t.membershipunkid = MBM.membershipunkid AND MBM.isactive = 1 " & _
                                 "             LEFT JOIN cfcommon_period_tran AS MCP ON t.periodunkid = MCP.periodunkid AND MCP.modulerefid = 1 AND MCP.isactive = 1 " & _
                                 "         WHERE t.isvoid = 0 " & _
                                 "         AND #CNDT# " & _
                                 "     ) AS a " & _
                                 ") AS iData ON EM.employeeunkid = iData.employeeunkid "
                    StrTranUnkidCol = "membershiptranunkid"

                    strMatchCols = "membershipunkid"

                    'S.SANDEEP |15-APR-2019| -- END

            End Select

            'Gajanan [9-NOV-2019] -- Start   
            'Enhancement:Worked On NMB Bio Data Approval Query Optimization   
            StrQ = StrQ.Replace("#TABLENAME#", StrTableName)
            StrQ = StrQ.Replace("#JOINEMPCOLUMN#", StrJoinColName)
            'Gajanan [9-NOV-2019] -- End

            Dim strApprovalDataJoin As String = String.Empty
            'S.SANDEEP |15-APR-2019| -- START
            Dim strExtraCol As String = String.Empty
            Dim strExtraJoin As String = String.Empty
            If eScreenType = enScreenName.frmMembershipInfoList Then
                strExtraCol = ",[HTAT].[periodunkid] AS iPeriodId " & _
                              ",[HTCP].[period_name] AS iPeriodName "
                strExtraJoin = " LEFT JOIN cfcommon_period_tran HTCP ON HTAT.periodunkid = HTCP.periodunkid "
            End If
            'S.SANDEEP |15-APR-2019| -- END

'Gajanan [17-April-2019] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.

              'strApprovalDataJoin = "LEFT JOIN " & _
'                                  "( " & _
'                                  "    SELECT " & _
'                                  "         [HA].[mapuserunkid] " & _
'                                  "        ,[HM].[priority] " & _
'                                  "        ,[HTAT].[statusunkid] AS iStatusId " & _
'                                  "        ,[HTAT].[transactiondate] " & _
'                                  "        ,[HTAT].[mappingunkid] " & _
'                                  "        ,CASE WHEN [HTAT].[statusunkid] = 1 THEN @Pending " & _
'                                  "              WHEN [HTAT].[statusunkid] = 2 THEN @Approved + ' : [ '+ CM.[username] + ' ]' " & _
'                                  "              WHEN [HTAT].[statusunkid] = 3 THEN @Reject+' : [ '+ CM.[username] + ' ]' " & _
'                                  "         ELSE @PendingApproval END AS iStatus " & _
'                                  "        ,ROW_NUMBER()OVER(PARTITION BY [HTAT].[" & StrJoinColName & "] ORDER BY [HTAT].[transactiondate] DESC) AS rno " & _
'                                  "        ,HTAT.[" & StrJoinColName & "] " & _
'                                  "    FROM " & StrTableName & " AS HTAT " & _
'                                  "        JOIN [dbo].[hremp_appusermapping] AS HA ON [HTAT].[mappingunkid] = [HA].[mappingunkid] " & _
'                                  "        JOIN [dbo].[hrempapproverlevel_master] AS HM ON [HA].[empapplevelunkid] = [HM].[empapplevelunkid] " & _
'                                  "        LEFT JOIN [hrmsConfiguration]..[cfuser_master] AS CM ON [CM].[userunkid] = [HA].[mapuserunkid] " & _
'                                   "    WHERE [HA].[isactive] = 1 AND [HA].[isvoid] = 0 AND [HTAT].[isvoid] = 0 " & _
'                                  "     AND [HA].[approvertypeid] = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
'                                  "     AND [HM].[approvertypeid] = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
'                                  "     AND #HTATCND# " & _
'                                  ") AS B ON B.[" & StrJoinColName & "] = iData.[" & StrJoinColName & "] AND [B].[priority] = [Fn].[priority] "
'            'AND [HTAT].[" & StrTranUnkidCol & "] <= 0 AND ISNULL([HTAT].[isprocessed],0) = 0




            strApprovalDataJoin = "LEFT JOIN " & _
                                  "( " & _
                                  "    SELECT " & _
                                  "         [HA].[mapuserunkid] " & _
                                  "        ,[HM].[priority] " & _
                                  "        ,[HTAT].[statusunkid] AS iStatusId " & _
                                  "        ,[HTAT].[transactiondate] " & _
                                  "        ,[HTAT].[mappingunkid] "

            If strMatchCols.ToString().Trim().Length > 0 Then
                strApprovalDataJoin &= "        ,[HTAT].[" & strMatchCols & "] "
            End If

            strApprovalDataJoin &= " " & strExtraCol & " " & _
                                  "        ,CASE WHEN [HTAT].[statusunkid] = 1 THEN @Pending " & _
                                  "              WHEN [HTAT].[statusunkid] = 2 THEN @Approved + ' : [ '+ CM.[username] + ' ]' " & _
                                  "              WHEN [HTAT].[statusunkid] = 3 THEN @Reject+' : [ '+ CM.[username] + ' ]' " & _
                                  "         ELSE @PendingApproval END AS iStatus " & _
                                  "        ,ROW_NUMBER()OVER(PARTITION BY [HTAT].[" & StrJoinColName & "] ORDER BY [HTAT].[transactiondate] DESC) AS rno " & _
                                  "        ,HTAT.[" & StrJoinColName & "] " & _
                                  "    FROM " & StrTableName & " AS HTAT " & _
                                  " " & strExtraJoin & " " & _
                                  "        JOIN [dbo].[hremp_appusermapping] AS HA ON [HTAT].[mappingunkid] = [HA].[mappingunkid] " & _
                                  "        JOIN [dbo].[hrempapproverlevel_master] AS HM ON [HA].[empapplevelunkid] = [HM].[empapplevelunkid] " & _
                                  "        LEFT JOIN [hrmsConfiguration]..[cfuser_master] AS CM ON [CM].[userunkid] = [HA].[mapuserunkid] " & _
                                  "    WHERE [HA].[isactive] = 1 AND [HA].[isvoid] = 0 AND [HTAT].[isvoid] = 0 " & _
                                  "     AND [HA].[approvertypeid] = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
                                  "     AND [HM].[approvertypeid] = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
                                  "     AND #HTATCND# " & _
                                  ") AS B ON B.[" & StrJoinColName & "] = iData.[" & StrJoinColName & "] AND [B].[priority] = [Fn].[priority] "
            If strMatchCols.ToString().Trim().Length > 0 Then
                Select Case eScreenType
                    Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList
                        strApprovalDataJoin &= " AND [B].[" & strMatchCols & "] = [iData].[addressid]"
                    Case Else
                strApprovalDataJoin &= " AND [B].[" & strMatchCols & "] = [iData].[" & strMatchCols & "]"
                End Select
            End If
            'Gajanan [9-April-2019] -- End
            'AND [HTAT].[" & StrTranUnkidCol & "] <= 0 AND ISNULL([HTAT].[isprocessed],0) = 0
'Gajanan [17-April-2019] -- End

            Dim StrCommonFilter As String = String.Empty


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            If blnisidentity_membership_approval = False Then

                Select Case eOperationType
                    Case enOperationType.ADDED

                        'Gajanan [18-Mar-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        'StrCommonFilter = " TAT." & StrTranUnkidCol & " <= 0 "
                        Select Case eScreenType

                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            'Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList
                            Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmBirthinfo, enScreenName.frmOtherinfo
                                'Gajanan [17-April-2019] -- End
                                StrCommonFilter = " TAT." & StrTranUnkidCol & " > 0 "
                            Case Else
                                StrCommonFilter = " TAT." & StrTranUnkidCol & " <= 0 "
                        End Select
                        'Gajanan [18-Mar-2019] -- End

                        'Case enOperationType.EDITED
                        'StrCommonFilter = " TAT." & StrTranUnkidCol & " > 0 "
                        'Case enOperationType.DELETED
                        'StrCommonFilter = " TAT." & StrTranUnkidCol & " > 0 "
                End Select

            End If
            'Gajanan [17-April-2019] -- End


            If StrCommonFilter.Trim.Length > 0 Then
                StrCommonFilter &= " AND ISNULL(TAT.isprocessed,0) = 0 "


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.



                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'If eOperationType <> enOperationType.NONE Then
                '    StrCommonFilter &= " AND TAT.operationtypeid  = '" & CInt(eOperationType) & "'"
                'End If

                If eOperationType <> enOperationType.NONE AndAlso blnisidentity_membership_approval = False Then
                    StrCommonFilter &= " AND TAT.operationtypeid  = '" & CInt(eOperationType) & "'"

                ElseIf eOperationType <> enOperationType.NONE AndAlso blnisidentity_membership_approval Then
                    StrCommonFilter &= " AND TAT.operationtypeid in( " & CInt(enOperationType.ADDED) & " , " & CInt(enOperationType.EDITED) & " , " & CInt(enOperationType.DELETED) & "  ) "
                End If
                'Gajanan [17-DEC-2018] -- End
                'Gajanan [17-April-2019] -- End

            Else
                StrCommonFilter &= " ISNULL(TAT.isprocessed,0) = 0  "


                If blnisidentity_membership_approval Then
                    StrCommonFilter &= " AND TAT.operationtypeid in( " & CInt(enOperationType.ADDED) & " , " & CInt(enOperationType.EDITED) & " , " & CInt(enOperationType.DELETED) & "  ) "
                Else
                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    If eOperationType <> enOperationType.NONE Then
                        StrCommonFilter &= " AND TAT.operationtypeid  = '" & CInt(eOperationType) & "'"
                    End If
                    'Gajanan [17-DEC-2018] -- End
                End If
            End If


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Select Case eScreenType
                Case enScreenName.frmBirthinfo
                    StrCommonFilter &= " AND TAT.isbirthinfo  = 1 "

                Case enScreenName.frmOtherinfo
                    StrCommonFilter &= " AND TAT.isbirthinfo  = 0 "
            End Select

            'Gajanan [17-April-2019] -- End


            Dim StrInnFilter As String = System.Text.RegularExpressions.Regex.Replace(StrCommonFilter, "\bTAT\b", "t")

            StrQ &= "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
                   "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
                   "SELECT DISTINCT " & _
                   "     AEM.employeeunkid " & _
                   "    ,ISNULL(T.departmentunkid,0) AS departmentunkid " & _
                   "    ,ISNULL(J.jobunkid,0) AS jobunkid " & _
                   "    ,ISNULL(T.classgroupunkid,0) AS classgroupunkid " & _
                   "    ,ISNULL(T.classunkid,0) AS classunkid " & _
                   "    ,ISNULL(T.stationunkid,0) AS stationunkid " & _
                   "    ,ISNULL(T.deptgroupunkid,0) AS deptgroupunkid " & _
                   "    ,ISNULL(T.sectiongroupunkid,0) AS sectiongroupunkid " & _
                   "    ,ISNULL(T.sectionunkid,0) AS sectionunkid " & _
                   "    ,ISNULL(T.unitgroupunkid,0) AS unitgroupunkid " & _
                   "    ,ISNULL(T.unitunkid,0) AS unitunkid " & _
                   "    ,ISNULL(T.teamunkid,0) AS teamunkid " & _
                   "    ,ISNULL(J.jobgroupunkid,0) AS jobgroupunkid " & _
                   "FROM " & StrTableName & " AS TAT "

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'Select Case eScreenType
            '    Case enScreenName.frmQualificationsList, enScreenName.frmJobHistory_ExperienceList, enScreenName.frmEmployeeRefereeList
            '        StrQ &= "    JOIN " & strDatabaseName & "..hremployee_master AS AEM ON TAT.employeeunkid = AEM.employeeunkid "
            '    Case enScreenName.frmEmployee_Skill_List
            '        StrQ &= "    JOIN " & strDatabaseName & "..hremployee_master AS AEM ON TAT.emp_app_unkid = AEM.employeeunkid "
            'End Select
            StrQ &= "    JOIN " & strDatabaseName & "..hremployee_master AS AEM ON TAT." & StrJoinColName & " = AEM.employeeunkid "
            'Gajanan [22-Feb-2019] -- End



            If strUACJoin.Trim.Length > 0 Then
                'Gajanan [27-May-2019] -- Start  
                'StrQ &= strUACJoin
                StrQ &= "LEFT " & strUACJoin
                'Gajanan [27-May-2019] -- End
            End If
            StrQ &= "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        stationunkid " & _
                    "       ,deptgroupunkid " & _
                    "       ,departmentunkid " & _
                    "       ,sectiongroupunkid " & _
                    "       ,sectionunkid " & _
                    "       ,unitgroupunkid " & _
                    "       ,unitunkid " & _
                    "       ,teamunkid " & _
                    "       ,classgroupunkid " & _
                    "       ,classunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "   FROM hremployee_transfer_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                    ") AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "   SELECT " & _
                    "        jobgroupunkid " & _
                    "       ,jobunkid " & _
                    "       ,employeeunkid " & _
                    "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                    "   FROM hremployee_categorization_tran " & _
                    "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                    ") AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 "
            'StrQ &= "WHERE TAT.isvoid = 0 AND TAT." & StrTranUnkidCol & " <= 0 " & StrTypeFilter & " AND TAT.statusunkid = 1 AND ISNULL([TAT].[isprocessed],0) = 0 "
            StrQ &= "WHERE TAT.isvoid = 0 AND " & StrCommonFilter & " " & StrTypeFilter & " AND TAT.statusunkid = 1 "
            StrQ &= "SELECT DISTINCT " & _
                    "     CAST(0 AS BIT) AS iCheck " & _
                    StrSelectCols & " " & _
                    "    ,iData.tranguid " & _
                    "    ,iData.isgrp " & _
                    "    ,iData.isprocessed "
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            ',iData.operationtypeid
            If eOperationType <> enOperationType.NONE Then
                StrQ &= " ,iData.operationtypeid"
            End If
            'Gajanan [17-DEC-2018] -- End
            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.




            'StrQ &= "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[transactiondate], iData.transactiondate) ELSE NULL END  AS transactiondate " & _
            '        "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[mappingunkid], iData.mappingunkid) ELSE 0 END AS mappingunkid " & _
            '        "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatus], @Pending) ELSE '' END AS iStatus " & _
            '        "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatusId], iData.statusunkid) ELSE 1 END AS iStatusId " & _
            '        "    ,iData." & StrTranUnkidCol & " " & _
            '        "    ,Fn.userunkid " & _
            '        "    ,CASE WHEN iData.isgrp = 0 THEN Fn.username ELSE '' END AS username " & _
            '        "    ,CASE WHEN iData.isgrp = 0 THEN Fn.email ELSE '' END AS email " & _
            '        "    ,CASE WHEN iData.isgrp = 0 THEN Fn.priority ELSE 0 END AS priority " & _
            '        "    ,EM.employeeunkid AS employeeunkid " & _
            '        "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(Fn.uempid, 0) ELSE 0 END AS uempid " & _
            '        "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(Fn.ecompid, 0) ELSE 0 END AS ecompid " & _
            '        "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(Fn.LvName, '') ELSE '' END AS levelname " & _
            '        "    ,EM.employeecode + ' - ' + EM.firstname + ' ' + EM.surname AS employee" & _
            '        "    " & StrRowNumberCol & " " & _
            '        "FROM @emp " & _
            '        "    JOIN " & strDatabaseName & "..hremployee_master AS EM ON [@emp].empid = EM.employeeunkid " & _
            '        StrInnJoin.Replace("#CNDT#", StrInnFilter) & " "

            StrQ &= "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[transactiondate], iData.transactiondate) ELSE NULL END  AS transactiondate "

            Select Case eScreenType
                Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList
                    StrQ &= "    , ISNULL(B.[mappingunkid], iData.mappingunkid) AS mappingunkid "
                Case Else
                    StrQ &= "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[mappingunkid], iData.mappingunkid) ELSE 0 END AS mappingunkid "
            End Select



            'Gajanan [9-NOV-2019] -- Start   
            'Enhancement:Worked On NMB Bio Data Approval Query Optimization   

            'StrQ &= "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatus], @Pending) ELSE '' END AS iStatus " & _
            '        "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatusId], iData.statusunkid) ELSE 1 END AS iStatusId " & _
            '        "    ,iData." & StrTranUnkidCol & " " & _
            '        "    ,Fn.userunkid " & _
            '        "    ,CASE WHEN iData.isgrp = 0 THEN Fn.username ELSE '' END AS username " & _
            '        "    ,CASE WHEN iData.isgrp = 0 THEN Fn.email ELSE '' END AS approver_email " & _
            '        "    ,CASE WHEN iData.isgrp = 0 THEN Fn.priority ELSE 0 END AS priority " & _
            '        "    ,EM.employeeunkid AS employeeunkid " & _
            '        "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(Fn.uempid, 0) ELSE 0 END AS uempid " & _
            '        "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(Fn.ecompid, 0) ELSE 0 END AS ecompid " & _
            '        "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(Fn.LvName, '') ELSE '' END AS levelname " & _
            '        "    ,EM.employeecode + ' - ' + EM.firstname + ' ' + EM.surname AS employee" & _
            '        "    ,EM.firstname + ' ' + EM.surname AS employee_name" & _
            '        "    " & StrRowNumberCol & " " & _
            '        "FROM @emp " & _
            '        "    JOIN " & strDatabaseName & "..hremployee_master AS EM ON [@emp].empid = EM.employeeunkid " & _
            '        StrInnJoin.Replace("#CNDT#", StrInnFilter) & " "



            StrQ &= "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatus], @Pending) ELSE '' END AS iStatus " & _
                    "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatusId], iData.statusunkid) ELSE 1 END AS iStatusId " & _
                    "    ,iData." & StrTranUnkidCol & " " & _
                    "    ,Fn.userunkid " & _
                    "    ,CASE WHEN iData.isgrp = 0 THEN Fn.username ELSE '' END AS username " & _
                    "    ,CASE WHEN iData.isgrp = 0 THEN Fn.email ELSE '' END AS approver_email " & _
                    "    ,CASE WHEN iData.isgrp = 0 THEN Fn.priority ELSE 0 END AS priority " & _
                    "    ,EM.employeeunkid AS employeeunkid " & _
                    "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(Fn.uempid, 0) ELSE 0 END AS uempid " & _
                    "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(Fn.ecompid, 0) ELSE 0 END AS ecompid " & _
                    "    ,CASE WHEN iData.isgrp = 0 THEN ISNULL(Fn.LvName, '') ELSE '' END AS levelname " & _
                    "    ,EM.employeecode + ' - ' + EM.firstname + ' ' + EM.surname AS employee" & _
                    "    ,EM.firstname + ' ' + EM.surname AS employee_name" & _
                    "    " & StrRowNumberCol & " " & _
                    "FROM @emp " & _
                    "    JOIN #USR ON [@emp].empid = #USR.employeeunkid " & _
                    "    JOIN " & strDatabaseName & "..hremployee_master AS EM ON [@emp].empid = EM.employeeunkid " & _
                    StrInnJoin.Replace("#CNDT#", StrInnFilter) & " "


            'Gajanan [9-NOV-2019] -- End

            'Gajanan [17-April-2019] -- End
            If xAdvanceJoinQry.Trim.Length > 0 Then
                'StrQ &= xAdvanceJoinQry
                StrQ &= System.Text.RegularExpressions.Regex.Replace(xAdvanceJoinQry, "\bEM.employeeunkid\b", "[@emp].empid")
            End If


            'Gajanan [9-NOV-2019] -- Start   
            'Enhancement:Worked On NMB Bio Data Approval Query Optimization   


            'StrQ &= "    JOIN " & _
            '        "    ( " & _
            '        "        SELECT DISTINCT " & _
            '        "             cfuser_master.userunkid " & _
            '        "            ,cfuser_master.firstname + ' ' +cfuser_master.lastname as username " & _
            '        "            ,cfuser_master.email " & _
            '                      strSelect & " " & _
            '        "            ,LM.priority " & _
            '        "            ,LM.empapplevelname AS LvName " & _
            '        "            ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
            '        "            ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
            '        "        FROM hrmsConfiguration..cfuser_master " & _
            '        "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
            '        "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
            '                     strAccessJoin & " " & _
            '        "        JOIN " & strDatabaseName & "..hremp_appusermapping EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
            '        "        JOIN " & strDatabaseName & "..hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
            '        "        WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 "
            'If strFilter.Trim.Length > 0 Then
            '    StrQ &= " AND (" & strFilter & " ) "
            'End If


            StrQ &= "    JOIN " & _
                    "    ( " & _
                    "        SELECT DISTINCT " & _
                    "             cfuser_master.userunkid " & _
                    "            ,cfuser_master.firstname + ' ' +cfuser_master.lastname as username " & _
                    "            ,cfuser_master.email " & _
                    "            ,LM.priority " & _
                    "            ,LM.empapplevelname AS LvName " & _
                    "            ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
                    "            ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
                    "        FROM hrmsConfiguration..cfuser_master " & _
                    "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                    "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                                 strAccessJoin & " " & _
                    "        JOIN " & strDatabaseName & "..hremp_appusermapping EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
                    "        JOIN " & strDatabaseName & "..hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
                    "        WHERE cfuser_master.isactive = 1 AND EM.isactive = 1 AND EM.isvoid = 0 "
            'Gajanan [9-NOV-2019] -- End    


            'StrQ &= "            AND EM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " AND LM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
            '        "            AND yearunkid = @Y AND privilegeunkid = @P  " & _
            '        ") AS Fn ON " & IIf(strJoin.Trim.Length <= 0, " 1 = 1 ", strJoin).ToString() & " " & _
            '        strApprovalDataJoin & " " & _
            '        "WHERE 1 = 1 AND iData." & StrTranUnkidCol & " <= 0 AND iData.statusunkid = 1 AND ISNULL(iData.[isprocessed],0) = 0 "


            'S.SANDEEP |15-APR-2019| -- START
            'StrQ &= "            AND EM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " AND LM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
            '        "            AND yearunkid = @Y AND privilegeunkid = @P  " & _
            '        ") AS Fn ON " & IIf(strJoin.Trim.Length <= 0, " 1 = 1 ", strJoin).ToString() & " " & _
            '        strApprovalDataJoin.Replace("#HTATCND#", System.Text.RegularExpressions.Regex.Replace(StrCommonFilter, "\bTAT\b", "HTAT")) & " " & _
            '        "WHERE 1 = 1 AND iData.statusunkid = 1 AND " & System.Text.RegularExpressions.Regex.Replace(StrCommonFilter, "\bTAT\b", "iData")


            'Gajanan [9-NOV-2019] -- Start   
            'Enhancement:Worked On NMB Bio Data Approval Query Optimization   

            'StrQ &= "            AND EM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " AND LM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
            '        "            AND yearunkid = @Y AND privilegeunkid = @P  " & _
            '        ") AS Fn ON " & IIf(strJoin.Trim.Length <= 0, " 1 = 1 ", strJoin).ToString() & " " & _
            '        strApprovalDataJoin.Replace("#HTATCND#", System.Text.RegularExpressions.Regex.Replace(StrCommonFilter, "\bTAT\b", "HTAT")) & " "


            StrQ &= "            AND EM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " AND LM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
                    "            AND yearunkid = @Y AND privilegeunkid = @P  " & _
                    ") AS Fn ON #USR.userunkid = Fn.userunkid and #USR.approvertypeid = 1 " & _
                    strApprovalDataJoin.Replace("#HTATCND#", System.Text.RegularExpressions.Regex.Replace(StrCommonFilter, "\bTAT\b", "HTAT")) & " "

            'Gajanan [9-NOV-2019] -- End

            If eScreenType = enScreenName.frmMembershipInfoList Then
                StrQ &= "LEFT JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "         X.xPeriodId " & _
                       "        ,X.xPeriodName " & _
                       "        ,X.employeeunkid " & _
                       "        ,X.membershipunkid " & _
                       "    FROM " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             ROW_NUMBER()OVER(PARTITION BY MAT.employeeunkid,MAT.membershipunkid ORDER BY MAT.auditdatetime DESC) as xno " & _
                       "            ,CP.periodunkid AS xPeriodId " & _
                       "            ,CP.period_name AS xPeriodName " & _
                       "            ,MAT.employeeunkid " & _
                       "            ,MAT.membershipunkid " & _
                       "        FROM hremployee_meminfo_approval_tran AS MAT " & _
                       "            JOIN cfcommon_period_tran CP ON MAT.periodunkid = CP.periodunkid " & _
                       "        WHERE MAT.isprocessed = 0 AND MAT.isvoid = 0 " & _
                       "    ) AS X WHERE X.xno = 1 " & _
                       ") AS NV ON NV.employeeunkid = iData.employeeunkid AND NV.membershipunkid = iData.membershipunkid "
            End If
            StrQ &= " WHERE 1 = 1 AND iData.statusunkid = 1 AND " & System.Text.RegularExpressions.Regex.Replace(StrCommonFilter, "\bTAT\b", "iData")
            'S.SANDEEP |15-APR-2019| -- END



            'iData." & StrTranUnkidCol & " <= 0 AND iData.statusunkid = 1 AND ISNULL(iData.[isprocessed],0) = 0



            If mblnIsSubmitForApproaval = False Then
                StrQ &= " AND Fn.priority > " & intCurrentPriorityId
            End If

            If mstrFilterString.Trim.Length > 0 Then
                StrQ &= " AND " & mstrFilterString
            End If

            objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 100, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 102, "Rejected"))
            objDataOperation.AddParameter("@PendingApproval", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 103, "Pending for Approval"))

            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

            If blnFromWebGroup = False Then
                StrQ = StrQ.Replace("'/r/n'", "SPACE(5)")
            Else
                StrQ = StrQ.Replace("'/r/n'", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
            End If

            StrQ &= " ORDER BY EM.employeeunkid,iData.isgrp DESC "

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            dtList = dsList.Tables("List").Copy()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetNextEmployeeApprovers; Module Name: " & mstrModuleName)
        Finally
             'Gajanan [17-April-2019] -- Start
             'Enhancement - Implementing Employee Approver Flow On Employee Data.
              If objDataOpr Is Nothing Then objDataOperation = Nothing
             'Gajanan [17-April-2019] -- End
            End Try
        Return dtList
    End Function


    'Gajanan [18-Mar-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.



    'Public Function InsertAll(ByVal eScreenType As enScreenName, _
    '                          ByVal eOprType As enOperationType, _
    '                          ByVal dtTable As DataTable, _
    '                          ByVal intUserId As Integer, _
    '                          ByVal dtCurrentDateTime As DateTime, _
    '                          ByVal strFormName As String, _
    '                          ByVal blnIsweb As Boolean, _
    '                          ByVal eStatusId As clsEmployee_Master.EmpApprovalStatus, _
    '                          ByVal strRemark As String, _
    '                          ByVal intUserMappingTranId As Integer, _
    '                          ByVal intCompanyId As Integer, _
    '                          Optional ByVal xHostName As String = "", _
    '                          Optional ByVal xIPAddr As String = "", _
    '                          Optional ByVal xLoggedUserName As String = "", _
    '                          Optional ByVal xLoginMod As enLogin_Mode = enLogin_Mode.DESKTOP, _
    '                          Optional ByVal xEmployeeAsOnDate As String = "") As Boolean
    'Gajanan [17-DEC-2018] -- 'Enhancement -Add eOprType

    Public Function InsertAll(ByVal eScreenType As enScreenName, _
                              ByVal eOprType As enOperationType, _
                              ByVal dtTable As DataTable, _
                              ByVal intUserId As Integer, _
                              ByVal dtCurrentDateTime As DateTime, _
                              ByVal strFormName As String, _
                              ByVal blnIsweb As Boolean, _
                              ByVal eStatusId As clsEmployee_Master.EmpApprovalStatus, _
                              ByVal strRemark As String, _
                              ByVal intUserMappingTranId As Integer, _
                              ByVal intCompanyId As Integer, _
                              ByVal xDatabaseName As String, _
                              ByVal xYearId As Integer, _
                              ByVal dtTotal_Active_Employee_AsOnDate As Date, _
                              ByVal blnIsArutiDemo As String, _
                              ByVal intTotal_Active_Employee_ForAllCompany As Integer, _
                              ByVal intNoOfEmployees As Integer, _
                              ByVal intUserUnkId As Integer, _
                              ByVal blnDonotAttendanceinSeconds As Boolean, _
                              ByVal xUserModeSetting As String, _
                              ByVal xPeriodStart As DateTime, _
                              ByVal xPeriodEnd As DateTime, _
                              ByVal xOnlyApproved As Boolean, _
                              ByVal xIncludeIn_ActiveEmployee As Boolean, _
                              ByVal blnAllowToApproveEarningDeduction As Boolean, _
                              ByVal mblnCreateADUserFromEmp As Boolean, _
                              ByVal mblnUserMustChangePwdOnNextLogOn As Boolean, _
                              Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                              Optional ByVal xHostName As String = "", _
                              Optional ByVal xIPAddr As String = "", _
                              Optional ByVal xLoggedUserName As String = "", _
                              Optional ByVal xLoginMod As enLogin_Mode = enLogin_Mode.DESKTOP _
                              ) As Boolean

        'Gajanan [18-Mar-2019] -- End


        'TODO --> DECLARE ALL APPROVAL CLASS OBJECT
        Dim objAEmpSkillTran As clsEmployeeSkill_Approval_Tran
        Dim objAEmpRefreeTran As clsemployee_refree_approval_tran
        Dim objAEmpExperienceTran As clsJobExperience_approval_tran
        Dim objAEmpQualificationTran As clsEmp_Qualification_Approval_Tran
        'Gajanan [22-Feb-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        Dim objAEmpDependant As clsDependant_beneficiaries_approval_tran
        Dim objAEmpIdentity As clsIdentity_Approval_tran
        'Gajanan [22-Feb-2019] -- End

        'S.SANDEEP |15-APR-2019| -- START
        Dim objAEmpMembership As clsMembership_Approval_Tran
        Dim objEmpMembership As clsMembershipTran
        'S.SANDEEP |15-APR-2019| -- END


        'Gajanan [18-Mar-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        Dim objAEmpAddress As clsEmployeeAddress_approval_tran
        Dim objEmpMaster As clsEmployee_Master
        Dim objAEmpEmergencyAddress As clsEmployee_emergency_address_approval

        'Gajanan [18-Mar-2019] -- End


        'Gajanan [17-April-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        Dim objAEmpPersonalinfo As clsEmployeePersonalInfo_Approval
        'Gajanan [17-April-2019] -- End



        'TODO --> DECLARE ALL MAIN CLASS OBJECT
        Dim objEmployeeSkillTran As clsEmployee_Skill_Tran
        Dim objEmployeeRefreeTran As clsEmployee_Refree_tran
        Dim objEmployeeExperienceTran As clsJobExperience_tran
        Dim objEmployeeQualificationTran As clsEmp_Qualification_Tran
        'Gajanan [22-Feb-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        Dim objEmployeeDependantTran As clsDependants_Beneficiary_tran
        Dim objEmpIdentity As clsIdentity_tran
        'Gajanan [22-Feb-2019] -- End


        mstrMessage = ""
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Dim exForce As Exception = Nothing
        Try
            If dtTable.Rows.Count > 0 Then
                Select Case eScreenType


                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.

                    Case enScreenName.frmBirthinfo, enScreenName.frmOtherinfo
                        Dim Employeeid As Integer = 0
                        For Each dRow As DataRow In dtTable.Rows

                            If CInt(dRow("employeeunkid")) <> Employeeid Then
                                Employeeid = CInt(dRow("employeeunkid"))
                                objEmpMaster = New clsEmployee_Master
                                objEmpMaster._Employeeunkid(dtCurrentDateTime.Date) = CInt(dRow("employeeunkid"))
                            End If
                            objAEmpPersonalinfo = New clsEmployeePersonalInfo_Approval

                            objAEmpPersonalinfo._Audittype = enAuditType.ADD
                            objAEmpPersonalinfo._Audituserunkid = intUserId

                            objAEmpPersonalinfo._Employeeunkid = CInt(dRow("employeeunkid"))
                            objAEmpPersonalinfo._IsBirthInfo = CBool(dRow("isbirthinfo"))
                            'Hemant (23 Apr 2020) -- Start
                            'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.
                            objAEmpPersonalinfo._Approvalremark = strRemark
                            'Hemant (23 Apr 2020) -- End
                            If objAEmpPersonalinfo._IsBirthInfo Then
                                objAEmpPersonalinfo._Birthcountryunkid = CInt(dRow("birthcountryunkid"))
                                objAEmpPersonalinfo._Birthstateunkid = CInt(dRow("birthstateunkid"))
                                objAEmpPersonalinfo._Birthcityunkid = CInt(dRow("birthcityunkid"))
                                objAEmpPersonalinfo._Birth_Ward = dRow("birth_ward").ToString()
                                objAEmpPersonalinfo._Birthcertificateno = dRow("birthcertificateno").ToString()
                                objAEmpPersonalinfo._Birth_Village = dRow("birth_village").ToString()
                                objAEmpPersonalinfo._Birthtownunkid = CInt(dRow("birthtownunkid"))
                                objAEmpPersonalinfo._Birthchiefdomunkid = CInt(dRow("birthchiefdomunkid"))
                                objAEmpPersonalinfo._Birthvillageunkid = CInt(dRow("birthvillageunkid"))
                                'S.SANDEEP |26-APR-2019| -- START
                                objAEmpPersonalinfo._Newattachdocumentid = dRow("newattachdocumentid").ToString()
                                objAEmpPersonalinfo._Deleteattachdocumentid = dRow("deleteattachdocumentid").ToString()
                                'S.SANDEEP |26-APR-2019| -- END
                            Else
                                objAEmpPersonalinfo._Complexionunkid = CInt(dRow("complexionunkid"))
                                objAEmpPersonalinfo._Bloodgroupunkid = CInt(dRow("bloodgroupunkid"))
                                objAEmpPersonalinfo._Eyecolorunkid = CInt(dRow("eyecolorunkid"))
                                objAEmpPersonalinfo._Nationalityunkid = CInt(dRow("nationalityunkid"))
                                objAEmpPersonalinfo._Ethincityunkid = CInt(dRow("ethnicityunkid"))
                                objAEmpPersonalinfo._Religionunkid = CInt(dRow("religionunkid"))
                                objAEmpPersonalinfo._Hairunkid = CInt(dRow("hairunkid"))
                                objAEmpPersonalinfo._Maritalstatusunkid = CInt(dRow("maritalstatusunkid"))
                                objAEmpPersonalinfo._Extra_Tel_No = dRow("ExtTelephoneno").ToString()
                                objAEmpPersonalinfo._Language1unkid = CInt(dRow("language1unkid"))
                                objAEmpPersonalinfo._Language2unkid = CInt(dRow("language2unkid"))
                                objAEmpPersonalinfo._Language3unkid = CInt(dRow("language3unkid"))
                                objAEmpPersonalinfo._Language4unkid = CInt(dRow("language4unkid"))
                                objAEmpPersonalinfo._Height = CDec(dRow("height"))
                                objAEmpPersonalinfo._Weight = CDec(dRow("weight"))
                                If IsDBNull(dRow("anniversary_date")) = False AndAlso CDate(dRow("anniversary_date")) <> Nothing Then
                                    objAEmpPersonalinfo._Anniversary_Date = CDate(dRow("anniversary_date"))
                                Else
                                    objAEmpPersonalinfo._Anniversary_Date = Nothing
                                End If
                                objAEmpPersonalinfo._Allergiesunkids = dRow("allergiesunkids").ToString()
                                objAEmpPersonalinfo._Disabilitiesunkids = dRow("disabilitiesunkids").ToString()
                                objAEmpPersonalinfo._Sports_Hobbies = dRow("sports_hobbies").ToString()
                                'S.SANDEEP |26-APR-2019| -- START
                                objAEmpPersonalinfo._Newattachdocumentid = dRow("newattachdocumentid").ToString()
                                objAEmpPersonalinfo._Deleteattachdocumentid = dRow("deleteattachdocumentid").ToString()
                                'S.SANDEEP |26-APR-2019| -- END
                            End If



                            objAEmpPersonalinfo._Form_Name = strFormName
                            objAEmpPersonalinfo._Host = xHostName
                            objAEmpPersonalinfo._Ip = xIPAddr
                            objAEmpPersonalinfo._Isfinal = CBool(dRow("isfinal"))
                            objAEmpPersonalinfo._Isprocessed = False
                            objAEmpPersonalinfo._Isvoid = False
                            objAEmpPersonalinfo._Isweb = blnIsweb
                            objAEmpPersonalinfo._Mappingunkid = intUserMappingTranId
                            objAEmpPersonalinfo._Statusunkid = eStatusId
                            objAEmpPersonalinfo._Transactiondate = Now
                            objAEmpPersonalinfo._Tranguid = Guid.NewGuid.ToString()
                            objAEmpPersonalinfo._Operationtypeid = eOprType

                            'S.SANDEEP |26-APR-2019| -- START
                            'If objAEmpPersonalinfo.Insert(objAEmpPersonalinfo._IsBirthInfo, objDataOperation, True) Then
                            If objAEmpPersonalinfo.Insert(objAEmpPersonalinfo._IsBirthInfo, intCompanyId, objDataOperation, True) Then
                                'S.SANDEEP |26-APR-2019| -- END
                                If eStatusId = clsEmployee_Master.EmpApprovalStatus.Approved Then
                                    If CBool(dRow("isfinal")) Then
                                        If objAEmpPersonalinfo._IsBirthInfo Then

                                            objEmpMaster._Birthcountryunkid = objAEmpPersonalinfo._Birthcountryunkid
                                            objEmpMaster._Birthstateunkid = objAEmpPersonalinfo._Birthstateunkid
                                            objEmpMaster._Birthcityunkid = objAEmpPersonalinfo._Birthcityunkid
                                            objEmpMaster._Birth_Ward = objAEmpPersonalinfo._Birth_Ward
                                            objEmpMaster._Birthcertificateno = objAEmpPersonalinfo._Birthcertificateno
                                            objEmpMaster._Birth_Village = objAEmpPersonalinfo._Birth_Village
                                            objEmpMaster._Birthtownunkid = objAEmpPersonalinfo._Birthtownunkid
                                            objEmpMaster._Birthchiefdomunkid = objAEmpPersonalinfo._Birthchiefdomunkid
                                            objEmpMaster._Birthvillageunkid = objAEmpPersonalinfo._Birthvillageunkid
                                        Else
                                            objEmpMaster._Complexionunkid = objAEmpPersonalinfo._Complexionunkid
                                            objEmpMaster._Bloodgroupunkid = objAEmpPersonalinfo._Bloodgroupunkid
                                            objEmpMaster._Eyecolorunkid = objAEmpPersonalinfo._Eyecolorunkid
                                            objEmpMaster._Nationalityunkid = objAEmpPersonalinfo._Nationalityunkid
                                            objEmpMaster._Ethincityunkid = objAEmpPersonalinfo._Ethincityunkid
                                            objEmpMaster._Religionunkid = objAEmpPersonalinfo._Religionunkid
                                            objEmpMaster._Hairunkid = objAEmpPersonalinfo._Hairunkid
                                            objEmpMaster._Maritalstatusunkid = objAEmpPersonalinfo._Maritalstatusunkid
                                            objEmpMaster._Extra_Tel_No = objAEmpPersonalinfo._Extra_Tel_No
                                            objEmpMaster._Language1unkid = objAEmpPersonalinfo._Language1unkid
                                            objEmpMaster._Language2unkid = objAEmpPersonalinfo._Language2unkid
                                            objEmpMaster._Language3unkid = objAEmpPersonalinfo._Language3unkid
                                            objEmpMaster._Language4unkid = objAEmpPersonalinfo._Language4unkid
                                            objEmpMaster._Height = objAEmpPersonalinfo._Height
                                            objEmpMaster._Weight = objAEmpPersonalinfo._Weight
                                            objEmpMaster._Anniversary_Date = objAEmpPersonalinfo._Anniversary_Date
                                            objEmpMaster._Allergies = objAEmpPersonalinfo._Allergiesunkids
                                            objEmpMaster._Disabilities = objAEmpPersonalinfo._Disabilitiesunkids
                                            objEmpMaster._Sports_Hobbies = objAEmpPersonalinfo._Sports_Hobbies
                                            'Hemant [8-April-2019] -- Start
                                            objEmpMaster._Anniversary_Date = objAEmpPersonalinfo._Anniversary_Date
                                            'Hemant [8-April-2019] -- End
                                        End If

                                        'Gajanan [17-April-2019] -- Start
                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.


                                        'Gajanan [14-AUG-2019] -- Start      
                                        'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                        If blnIsweb Then
                                        objEmpMaster._WebClientIP = xIPAddr
                                        objEmpMaster._WebHostName = xHostName
                                        objEmpMaster._WebFormName = strFormName
                                        End If
                                        objEmpMaster._Companyunkid = intCompanyId
                                        'Gajanan [14-AUG-2019] -- End



                                        Select Case eOprType
                                            Case enOperationType.EDITED, enOperationType.ADDED, enOperationType.DELETED
                                                If objEmpMaster.Update(xDatabaseName, xYearId, intCompanyId, dtTotal_Active_Employee_AsOnDate, _
                                                                       blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, _
                                                                       intUserUnkId, blnDonotAttendanceinSeconds, xUserModeSetting, xPeriodStart, xPeriodEnd, _
                                                                       xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, _
                                                                       dtCurrentDateTime, mblnCreateADUserFromEmp, mblnUserMustChangePwdOnNextLogOn, _
                                                                       Nothing, Nothing, False, "", "", "", False, False, objDataOperation, True, "", Nothing, _
                                                                        xHostName, xIPAddr, xLoggedUserName, xLoginMod, False, True, strFormName) Then

                                                    If objAEmpPersonalinfo.Update(objDataOperation, objAEmpPersonalinfo._Employeeunkid, objAEmpPersonalinfo._IsBirthInfo, objAEmpPersonalinfo._Employeeunkid, "employeeunkid") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If

                                                    If objAEmpPersonalinfo.Update(objDataOperation, objAEmpPersonalinfo._Employeeunkid, objAEmpPersonalinfo._IsBirthInfo, 1, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If

                                                    'S.SANDEEP |26-APR-2019| -- START
                                                    If objAEmpPersonalinfo._Newattachdocumentid.Length > 0 Then
                                                        If objAEmpPersonalinfo.UpdateDeleteDocument(objDataOperation, objAEmpPersonalinfo._Employeeunkid, objAEmpPersonalinfo._Employeeunkid, objAEmpPersonalinfo._Newattachdocumentid, dRow("form_name").ToString(), "transactionunkid", False) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                    End If

                                                    If objAEmpPersonalinfo._Deleteattachdocumentid.Length > 0 Then
                                                        If objAEmpPersonalinfo.UpdateDeleteDocument(objDataOperation, objAEmpPersonalinfo._Employeeunkid, objAEmpPersonalinfo._Employeeunkid, objAEmpPersonalinfo._Deleteattachdocumentid, dRow("form_name").ToString(), "transactionunkid", True) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                    End If
                                                    'S.SANDEEP |26-APR-2019| -- END
                                                Else
                                                    If objEmpMaster._Message.Trim.Length > 0 Then mstrMessage = objEmpMaster._Message
                                                    Return False
                                                End If

                                        End Select
                                    End If
                                ElseIf eStatusId = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                                    'If CBool(dRow("isfinal")) Then     'Gajanan [17-April-2019]
                                        Select Case eOprType
                                            Case enOperationType.ADDED, enOperationType.EDITED, enOperationType.DELETED
                                                If objAEmpPersonalinfo.Update(objDataOperation, objAEmpPersonalinfo._Employeeunkid, objAEmpPersonalinfo._IsBirthInfo, 1, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                'S.SANDEEP |26-APR-2019| -- START
                                                If objAEmpPersonalinfo._Newattachdocumentid.Length > 0 Then
                                                    If objAEmpPersonalinfo.UpdateDeleteDocument(objDataOperation, objAEmpPersonalinfo._Employeeunkid, objAEmpPersonalinfo._Employeeunkid, objAEmpPersonalinfo._Newattachdocumentid, dRow("form_name").ToString(), "transactionunkid", True) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                End If

                                                If objAEmpPersonalinfo._Deleteattachdocumentid.Length > 0 Then
                                                    If objAEmpPersonalinfo.UpdateDeleteDocument(objDataOperation, objAEmpPersonalinfo._Employeeunkid, objAEmpPersonalinfo._Employeeunkid, objAEmpPersonalinfo._Deleteattachdocumentid, dRow("form_name").ToString(), "transactionunkid", False) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                End If
                                                'S.SANDEEP |26-APR-2019| -- END

                                        End Select
                                    'End If     'Gajanan [17-April-2019]
                                End If

                            Else
                                If objAEmpPersonalinfo._Message.Trim.Length > 0 Then mstrMessage = objAEmpPersonalinfo._Message
                                Return False
                            End If

                        Next

                        'Gajanan [17-April-2019] -- End




                    'Gajanan [18-Mar-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.


                    '-----------------------------------------Emergency Approval----------------------------

                    Case enScreenName.frmEmergencyAddressList
                        Dim Employeeid As Integer = 0
                        For Each dRow As DataRow In dtTable.Rows

                            If CInt(dRow("employeeunkid")) <> Employeeid Then
                                Employeeid = CInt(dRow("employeeunkid"))
                                objEmpMaster = New clsEmployee_Master
                                objEmpMaster._Employeeunkid(dtCurrentDateTime.Date) = CInt(dRow("employeeunkid"))
                            End If
                            objAEmpEmergencyAddress = New clsEmployee_emergency_address_approval

                            objAEmpEmergencyAddress._Audittype = enAuditType.ADD
                            objAEmpEmergencyAddress._Audituserunkid = intUserId


                            If CInt(dRow("addresstypeid")) > 0 Then

                                objAEmpEmergencyAddress._Audittype = enAuditType.ADD
                                objAEmpEmergencyAddress._Audituserunkid = intUserId


                                objAEmpEmergencyAddress._Employeeunkid = CInt(dRow("employeeunkid"))
                                objAEmpEmergencyAddress._Firstname = dRow("firstname").ToString().Trim
                                objAEmpEmergencyAddress._Lastname = dRow("lastname").ToString().Trim
                                objAEmpEmergencyAddress._Address = dRow("address").ToString().Trim
                                objAEmpEmergencyAddress._Countryunkid = CInt(dRow("countryunkid"))
                                objAEmpEmergencyAddress._Stateunkid = CInt(dRow("stateunkid"))
                                objAEmpEmergencyAddress._Townunkid = CInt(dRow("townunkid"))
                                objAEmpEmergencyAddress._Postcodeunkid = CInt(dRow("postcodeunkid"))
                                objAEmpEmergencyAddress._Provicnce = dRow("provicnce").ToString()
                                objAEmpEmergencyAddress._Road = dRow("road").ToString()
                                objAEmpEmergencyAddress._Estate = dRow("estate").ToString()
                                objAEmpEmergencyAddress._Plotno = dRow("plotNo").ToString()
                                objAEmpEmergencyAddress._Mobile = dRow("mobile").ToString()
                                objAEmpEmergencyAddress._Alternateno = dRow("alternateno").ToString()
                                objAEmpEmergencyAddress._Tel_No = dRow("tel_no").ToString()
                                objAEmpEmergencyAddress._Fax = dRow("fax").ToString()
                                objAEmpEmergencyAddress._Email = dRow("email").ToString()
                                objAEmpEmergencyAddress._Adddresstype = CInt(dRow("addresstypeid"))

                                objAEmpEmergencyAddress._Form_Name = strFormName
                                objAEmpEmergencyAddress._Host = xHostName
                                objAEmpEmergencyAddress._Ip = xIPAddr
                                objAEmpEmergencyAddress._Isfinal = CBool(dRow("isfinal"))
                                objAEmpEmergencyAddress._Isprocessed = False
                                objAEmpEmergencyAddress._Isvoid = False
                                objAEmpEmergencyAddress._Isweb = blnIsweb
                                objAEmpEmergencyAddress._Mappingunkid = intUserMappingTranId
                                objAEmpEmergencyAddress._Statusunkid = eStatusId
                                objAEmpEmergencyAddress._Transactiondate = Now
                                objAEmpEmergencyAddress._Tranguid = Guid.NewGuid.ToString()
                                objAEmpEmergencyAddress._Operationtypeid = eOprType
                                'S.SANDEEP |26-APR-2019| -- START
                                objAEmpEmergencyAddress._Newattachdocumentid = dRow("newattachdocumentid").ToString()
                                objAEmpEmergencyAddress._Deleteattachdocumentid = dRow("deleteattachdocumentid").ToString()
                                'S.SANDEEP |26-APR-2019| -- END

                                'S.SANDEEP |26-APR-2019| -- START
                                'If objAEmpEmergencyAddress.Insert(CInt(dRow("addresstypeid")), objDataOperation, True) Then
                                If objAEmpEmergencyAddress.Insert(CInt(dRow("addresstypeid")), intCompanyId, objDataOperation, True) Then
                                    'S.SANDEEP |26-APR-2019| -- END
                                    If eStatusId = clsEmployee_Master.EmpApprovalStatus.Approved Then
                                        If CBool(dRow("isfinal")) Then

                                            If CInt(dRow("addresstypeid")) = clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1 Then
                                                objEmpMaster._Emer_Con_Firstname = objAEmpEmergencyAddress._Firstname
                                                objEmpMaster._Emer_Con_Lastname = objAEmpEmergencyAddress._Lastname
                                                objEmpMaster._Emer_Con_Address = objAEmpEmergencyAddress._Address
                                                objEmpMaster._Emer_Con_Countryunkid = objAEmpEmergencyAddress._Countryunkid
                                                objEmpMaster._Emer_Con_State = objAEmpEmergencyAddress._Stateunkid
                                                objEmpMaster._Emer_Con_Post_Townunkid = objAEmpEmergencyAddress._Townunkid
                                                objEmpMaster._Emer_Con_Postcodeunkid = objAEmpEmergencyAddress._Postcodeunkid
                                                objEmpMaster._Emer_Con_Provicnce = objAEmpEmergencyAddress._Provicnce
                                                objEmpMaster._Emer_Con_Road = objAEmpEmergencyAddress._Road
                                                objEmpMaster._Emer_Con_Estate = objAEmpEmergencyAddress._Estate
                                                objEmpMaster._Emer_Con_Plotno = objAEmpEmergencyAddress._Plotno
                                                objEmpMaster._Emer_Con_Mobile = objAEmpEmergencyAddress._Mobile
                                                objEmpMaster._Emer_Con_Alternateno = objAEmpEmergencyAddress._Alternateno
                                                objEmpMaster._Emer_Con_Tel_No = objAEmpEmergencyAddress._Tel_No
                                                objEmpMaster._Emer_Con_Fax = objAEmpEmergencyAddress._Fax
                                                objEmpMaster._Emer_Con_Email = objAEmpEmergencyAddress._Email


                                            ElseIf CInt(dRow("addresstypeid")) = clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2 Then
                                                objEmpMaster._Emer_Con_Firstname2 = objAEmpEmergencyAddress._Firstname
                                                objEmpMaster._Emer_Con_Lastname2 = objAEmpEmergencyAddress._Lastname
                                                objEmpMaster._Emer_Con_Address2 = objAEmpEmergencyAddress._Address
                                                objEmpMaster._Emer_Con_Countryunkid2 = objAEmpEmergencyAddress._Countryunkid
                                                objEmpMaster._Emer_Con_State2 = objAEmpEmergencyAddress._Stateunkid
                                                objEmpMaster._Emer_Con_Post_Townunkid2 = objAEmpEmergencyAddress._Townunkid
                                                objEmpMaster._Emer_Con_Postcodeunkid2 = objAEmpEmergencyAddress._Postcodeunkid
                                                objEmpMaster._Emer_Con_Provicnce2 = objAEmpEmergencyAddress._Provicnce
                                                objEmpMaster._Emer_Con_Road2 = objAEmpEmergencyAddress._Road
                                                objEmpMaster._Emer_Con_Estate2 = objAEmpEmergencyAddress._Estate
                                                objEmpMaster._Emer_Con_Plotno2 = objAEmpEmergencyAddress._Plotno
                                                objEmpMaster._Emer_Con_Mobile2 = objAEmpEmergencyAddress._Mobile
                                                objEmpMaster._Emer_Con_Alternateno2 = objAEmpEmergencyAddress._Alternateno
                                                objEmpMaster._Emer_Con_Tel_No2 = objAEmpEmergencyAddress._Tel_No
                                                objEmpMaster._Emer_Con_Fax2 = objAEmpEmergencyAddress._Fax
                                                objEmpMaster._Emer_Con_Email2 = objAEmpEmergencyAddress._Email


                                            ElseIf CInt(dRow("addresstypeid")) = clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3 Then
                                                objEmpMaster._Emer_Con_Firstname3 = objAEmpEmergencyAddress._Firstname
                                                objEmpMaster._Emer_Con_Lastname3 = objAEmpEmergencyAddress._Lastname
                                                objEmpMaster._Emer_Con_Address3 = objAEmpEmergencyAddress._Address
                                                objEmpMaster._Emer_Con_Countryunkid3 = objAEmpEmergencyAddress._Countryunkid
                                                objEmpMaster._Emer_Con_State3 = objAEmpEmergencyAddress._Stateunkid
                                                objEmpMaster._Emer_Con_Post_Townunkid3 = objAEmpEmergencyAddress._Townunkid
                                                objEmpMaster._Emer_Con_Postcodeunkid3 = objAEmpEmergencyAddress._Postcodeunkid
                                                objEmpMaster._Emer_Con_Provicnce3 = objAEmpEmergencyAddress._Provicnce
                                                objEmpMaster._Emer_Con_Road3 = objAEmpEmergencyAddress._Road
                                                objEmpMaster._Emer_Con_Estate3 = objAEmpEmergencyAddress._Estate
                                                objEmpMaster._Emer_Con_Plotno3 = objAEmpEmergencyAddress._Plotno
                                                objEmpMaster._Emer_Con_Mobile3 = objAEmpEmergencyAddress._Mobile
                                                objEmpMaster._Emer_Con_Alternateno3 = objAEmpEmergencyAddress._Alternateno
                                                objEmpMaster._Emer_Con_Tel_No3 = objAEmpEmergencyAddress._Tel_No
                                                objEmpMaster._Emer_Con_Fax3 = objAEmpEmergencyAddress._Fax
                                                objEmpMaster._Emer_Con_Email3 = objAEmpEmergencyAddress._Email
                                            End If


                                            'Gajanan [14-AUG-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            If blnIsweb Then
                                            objEmpMaster._WebClientIP = xIPAddr
                                            objEmpMaster._WebHostName = xHostName
                                            objEmpMaster._WebFormName = strFormName
                                            End If

                                            'Gajanan [14-AUG-2019] -- End

                                            'Gajanan [9-July-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            objEmpMaster._Companyunkid = intCompanyId
                                            'Gajanan [9-July-2019] -- End
                                            Select Case eOprType


                                                Case enOperationType.EDITED, enOperationType.ADDED, enOperationType.DELETED
                                                    If objEmpMaster.Update(xDatabaseName, xYearId, intCompanyId, dtTotal_Active_Employee_AsOnDate, _
                                                                           blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, _
                                                                           intUserUnkId, blnDonotAttendanceinSeconds, xUserModeSetting, xPeriodStart, xPeriodEnd, _
                                                                           xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, _
                                                                           dtCurrentDateTime, mblnCreateADUserFromEmp, mblnUserMustChangePwdOnNextLogOn, _
                                                                           Nothing, Nothing, False, "", "", "", False, False, objDataOperation, True, "", Nothing, _
                                                                            xHostName, xIPAddr, xLoggedUserName, xLoginMod, False, True, strFormName) Then

                                                        If objAEmpEmergencyAddress.Update(objDataOperation, objAEmpEmergencyAddress._Employeeunkid, objAEmpEmergencyAddress._Employeeunkid, objAEmpEmergencyAddress._Adddresstype, "employeeunkid") = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If

                                                        If objAEmpEmergencyAddress.Update(objDataOperation, objAEmpEmergencyAddress._Employeeunkid, 1, objAEmpEmergencyAddress._Adddresstype, "isprocessed") = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If

                                                        'S.SANDEEP |26-APR-2019| -- START
                                                        If objAEmpEmergencyAddress._Newattachdocumentid.Length > 0 Then
                                                            If objAEmpEmergencyAddress.UpdateDeleteDocument(objDataOperation, objAEmpEmergencyAddress._Employeeunkid, objAEmpEmergencyAddress._Employeeunkid, objAEmpEmergencyAddress._Newattachdocumentid, dRow("form_name").ToString(), "transactionunkid", False) = False Then
                                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                                Throw exForce
                                                            End If
                                                        End If

                                                        If objAEmpEmergencyAddress._Deleteattachdocumentid.Length > 0 Then
                                                            If objAEmpEmergencyAddress.UpdateDeleteDocument(objDataOperation, objAEmpEmergencyAddress._Employeeunkid, objAEmpEmergencyAddress._Employeeunkid, objAEmpEmergencyAddress._Deleteattachdocumentid, dRow("form_name").ToString(), "transactionunkid", True) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                        End If
                                                        'S.SANDEEP |26-APR-2019| -- END
                                                    Else
                                                        If objEmpMaster._Message.Trim.Length > 0 Then mstrMessage = objEmpMaster._Message
                                                        Return False
                                                    End If

                                            End Select
                                        End If
                                    ElseIf eStatusId = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                                        'If CBool(dRow("isfinal")) Then     'Gajanan [17-April-2019]
                                            Select Case eOprType
                                                Case enOperationType.ADDED, enOperationType.EDITED, enOperationType.DELETED
                                                    If objAEmpEmergencyAddress.Update(objDataOperation, objAEmpEmergencyAddress._Employeeunkid, 1, objAEmpEmergencyAddress._Adddresstype, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If

                                                    'S.SANDEEP |26-APR-2019| -- START
                                                    If objAEmpEmergencyAddress._Newattachdocumentid.Length > 0 Then
                                                        If objAEmpEmergencyAddress.UpdateDeleteDocument(objDataOperation, objAEmpEmergencyAddress._Employeeunkid, objAEmpEmergencyAddress._Employeeunkid, objAEmpEmergencyAddress._Newattachdocumentid, dRow("form_name").ToString(), "transactionunkid", True) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                    End If

                                                    If objAEmpEmergencyAddress._Deleteattachdocumentid.Length > 0 Then
                                                        If objAEmpEmergencyAddress.UpdateDeleteDocument(objDataOperation, objAEmpEmergencyAddress._Employeeunkid, objAEmpEmergencyAddress._Employeeunkid, objAEmpEmergencyAddress._Deleteattachdocumentid, dRow("form_name").ToString(), "transactionunkid", False) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                    End If
                                                    'S.SANDEEP |26-APR-2019| -- END
                                            End Select
                                        'End If     'Gajanan [17-April-2019]
                                    End If
                                End If

                            Else
                                If objAEmpEmergencyAddress._Message.Trim.Length > 0 Then mstrMessage = objAEmpEmergencyAddress._Message
                                Return False
                            End If

                        Next


                        '-----------------------------------------Address Approval----------------------------

                    Case enScreenName.frmAddressList
                        Dim Employeeid As Integer = 0
                        For Each dRow As DataRow In dtTable.Rows

                            If CInt(dRow("employeeunkid")) <> Employeeid Then
                                Employeeid = CInt(dRow("employeeunkid"))
                                objEmpMaster = New clsEmployee_Master
                                objEmpMaster._Employeeunkid(dtCurrentDateTime.Date) = CInt(dRow("employeeunkid"))
                            End If
                            objAEmpAddress = New clsEmployeeAddress_approval_tran

                            objAEmpAddress._Audittype = enAuditType.ADD
                            objAEmpAddress._Audituserunkid = intUserId


                            If CInt(dRow("addresstypeid")) > 0 Then

                                objAEmpAddress._Audittype = enAuditType.ADD
                                objAEmpAddress._Audituserunkid = intUserId

                                objAEmpAddress._Employeeunkid = CInt(dRow("employeeunkid"))
                                objAEmpAddress._Address1 = dRow("address1").ToString().Trim
                                objAEmpAddress._Address2 = dRow("address2").ToString().Trim
                                objAEmpAddress._Countryunkid = CInt(dRow("countryunkid"))
                                objAEmpAddress._Stateunkid = CInt(dRow("stateunkid"))
                                objAEmpAddress._Post_Townunkid = CInt(dRow("post_townunkid"))
                                objAEmpAddress._Postcodeunkid = CInt(dRow("postcodeunkid"))
                                objAEmpAddress._Provicnce = dRow("provicnce").ToString()
                                objAEmpAddress._Road = dRow("road").ToString()
                                objAEmpAddress._Estate = dRow("estate").ToString()
                                objAEmpAddress._Provinceunkid = CInt(dRow("provinceunkid"))
                                objAEmpAddress._Roadunkid = CInt(dRow("roadunkid"))
                                objAEmpAddress._Chiefdomunkid = CInt(dRow("chiefdomunkid"))
                                objAEmpAddress._Villageunkid = CInt(dRow("villageunkid"))
                                objAEmpAddress._Town1unkid = CInt(dRow("town1unkid"))
                                objAEmpAddress._Mobile = dRow("mobile").ToString()
                                objAEmpAddress._Tel_No = dRow("tel_no").ToString()
                                objAEmpAddress._Plotno = dRow("plotNo").ToString()
                                objAEmpAddress._Alternateno = dRow("alternateno").ToString()
                                objAEmpAddress._Email = dRow("email").ToString()
                                objAEmpAddress._Fax = dRow("fax").ToString()
                                objAEmpAddress._Adddresstype = CInt(dRow("addresstypeid"))

                                objAEmpAddress._Form_Name = strFormName
                                objAEmpAddress._Host = xHostName
                                objAEmpAddress._Ip = xIPAddr
                                objAEmpAddress._Isfinal = CBool(dRow("isfinal"))
                                objAEmpAddress._Isprocessed = False
                                objAEmpAddress._Isvoid = False
                                objAEmpAddress._Isweb = blnIsweb
                                objAEmpAddress._Mappingunkid = intUserMappingTranId
                                objAEmpAddress._Statusunkid = eStatusId
                                objAEmpAddress._Transactiondate = Now
                                objAEmpAddress._Tranguid = Guid.NewGuid.ToString()
                                objAEmpAddress._Operationtypeid = eOprType
                                'S.SANDEEP |26-APR-2019| -- START
                                objAEmpAddress._Newattachdocumentid = dRow("newattachdocumentid").ToString()
                                objAEmpAddress._Deleteattachdocumentid = dRow("deleteattachdocumentid").ToString()
                                'S.SANDEEP |26-APR-2019| -- END


                                'S.SANDEEP |26-APR-2019| -- START
                                'If objAEmpAddress.Insert(CInt(dRow("addresstypeid")), objDataOperation, True) Then
                                If objAEmpAddress.Insert(CInt(dRow("addresstypeid")), intCompanyId, objDataOperation, True) Then
                                    'S.SANDEEP |26-APR-2019| -- END
                                    If eStatusId = clsEmployee_Master.EmpApprovalStatus.Approved Then
                                        If CBool(dRow("isfinal")) Then

                                            If CInt(dRow("addresstypeid")) = clsEmployeeAddress_approval_tran.enAddressType.PRESENT Then
                                                objEmpMaster._Present_Address1 = objAEmpAddress._Address1
                                                objEmpMaster._Present_Address2 = objAEmpAddress._Address2
                                                objEmpMaster._Present_Countryunkid = objAEmpAddress._Countryunkid
                                                objEmpMaster._Present_Stateunkid = objAEmpAddress._Stateunkid
                                                objEmpMaster._Present_Post_Townunkid = objAEmpAddress._Post_Townunkid
                                                objEmpMaster._Present_Postcodeunkid = objAEmpAddress._Postcodeunkid
                                                objEmpMaster._Present_Provicnce = objAEmpAddress._Provicnce
                                                objEmpMaster._Present_Road = objAEmpAddress._Road
                                                objEmpMaster._Present_Estate = objAEmpAddress._Estate
                                                objEmpMaster._Present_Provinceunkid = objAEmpAddress._Provinceunkid
                                                objEmpMaster._Present_Roadunkid = objAEmpAddress._Roadunkid
                                                objEmpMaster._Present_Chiefdomunkid = objAEmpAddress._Chiefdomunkid
                                                objEmpMaster._Present_Villageunkid = objAEmpAddress._Villageunkid
                                                objEmpMaster._Present_Town1unkid = objAEmpAddress._Town1unkid
                                                objEmpMaster._Present_Mobile = objAEmpAddress._Mobile
                                                objEmpMaster._Present_Tel_No = objAEmpAddress._Tel_No
                                                objEmpMaster._Present_Plotno = objAEmpAddress._Plotno
                                                objEmpMaster._Present_Alternateno = objAEmpAddress._Alternateno
                                                objEmpMaster._Present_Email = objAEmpAddress._Email
                                                objEmpMaster._Present_Fax = objAEmpAddress._Fax

                                            ElseIf CInt(dRow("addresstypeid")) = clsEmployeeAddress_approval_tran.enAddressType.DOMICILE Then
                                                objEmpMaster._Domicile_Address1 = objAEmpAddress._Address1
                                                objEmpMaster._Domicile_Address2 = objAEmpAddress._Address2
                                                objEmpMaster._Domicile_Countryunkid = objAEmpAddress._Countryunkid
                                                objEmpMaster._Domicile_Stateunkid = objAEmpAddress._Stateunkid
                                                objEmpMaster._Domicile_Post_Townunkid = objAEmpAddress._Post_Townunkid
                                                objEmpMaster._Domicile_Postcodeunkid = objAEmpAddress._Postcodeunkid
                                                objEmpMaster._Domicile_Provicnce = objAEmpAddress._Provicnce
                                                objEmpMaster._Domicile_Road = objAEmpAddress._Road
                                                objEmpMaster._Domicile_Estate = objAEmpAddress._Estate
                                                objEmpMaster._domicile_provinceunkid = objAEmpAddress._Provinceunkid
                                                objEmpMaster._domicile_roadunkid = objAEmpAddress._Roadunkid
                                                objEmpMaster._Domicile_Chiefdomunkid = objAEmpAddress._Chiefdomunkid
                                                objEmpMaster._Domicile_Villageunkid = objAEmpAddress._Villageunkid
                                                objEmpMaster._Domicile_Town1unkid = objAEmpAddress._Town1unkid
                                                objEmpMaster._Domicile_Mobile = objAEmpAddress._Mobile
                                                objEmpMaster._Domicile_Tel_No = objAEmpAddress._Tel_No
                                                objEmpMaster._Domicile_Plotno = objAEmpAddress._Plotno
                                                objEmpMaster._Domicile_Alternateno = objAEmpAddress._Alternateno
                                                objEmpMaster._Domicile_Email = objAEmpAddress._Email
                                                objEmpMaster._Domicile_Fax = objAEmpAddress._Fax

                                            ElseIf CInt(dRow("addresstypeid")) = clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT Then
                                                objEmpMaster._Recruitment_Address1 = objAEmpAddress._Address1
                                                objEmpMaster._Recruitment_Address2 = objAEmpAddress._Address2
                                                objEmpMaster._Recruitment_Countryunkid = objAEmpAddress._Countryunkid
                                                objEmpMaster._Recruitment_Stateunkid = objAEmpAddress._Stateunkid
                                                objEmpMaster._Recruitment_Post_Townunkid = objAEmpAddress._Post_Townunkid
                                                objEmpMaster._Recruitment_Postcodeunkid = objAEmpAddress._Postcodeunkid
                                                objEmpMaster._Recruitment_Province = objAEmpAddress._Provicnce
                                                objEmpMaster._Recruitment_Road = objAEmpAddress._Road
                                                objEmpMaster._Recruitment_Estate = objAEmpAddress._Estate
                                                objEmpMaster._Recruitment_Provinceunkid = objAEmpAddress._Provinceunkid
                                                objEmpMaster._Recruitment_Roadunkid = objAEmpAddress._Roadunkid
                                                objEmpMaster._Recruitment_Chiefdomunkid = objAEmpAddress._Chiefdomunkid
                                                objEmpMaster._Recruitment_Villageunkid = objAEmpAddress._Villageunkid
                                                objEmpMaster._Recruitment_Town1unkid = objAEmpAddress._Town1unkid
                                                objEmpMaster._Recruitment_Tel_No = objAEmpAddress._Tel_No
                                                objEmpMaster._Recruitment_Plotno = objAEmpAddress._Plotno
                                            End If


                                            'Gajanan [14-AUG-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            If blnIsweb Then
                                            objEmpMaster._WebClientIP = xIPAddr
                                            objEmpMaster._WebHostName = xHostName
                                            objEmpMaster._WebFormName = strFormName
                                            objEmpMaster._Companyunkid = intCompanyId
                                            End If
                                            'Gajanan [14-AUG-2019] -- End

                                            Select Case eOprType


                                                Case enOperationType.EDITED, enOperationType.ADDED, enOperationType.DELETED
                                                    If objEmpMaster.Update(xDatabaseName, xYearId, intCompanyId, dtTotal_Active_Employee_AsOnDate, _
                                                                           blnIsArutiDemo, intTotal_Active_Employee_ForAllCompany, intNoOfEmployees, _
                                                                           intUserUnkId, blnDonotAttendanceinSeconds, xUserModeSetting, xPeriodStart, xPeriodEnd, _
                                                                           xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, _
                                                                           dtCurrentDateTime, mblnCreateADUserFromEmp, mblnUserMustChangePwdOnNextLogOn, _
                                                                           Nothing, Nothing, False, "", "", "", False, False, objDataOperation, True, "", Nothing, _
                                                                            xHostName, xIPAddr, xLoggedUserName, xLoginMod, False, True, strFormName) Then

                                                        If objAEmpAddress.Update(objDataOperation, objAEmpAddress._Employeeunkid, objAEmpAddress._Employeeunkid, objAEmpAddress._Adddresstype, "employeeunkid") = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If

                                                        If objAEmpAddress.Update(objDataOperation, objAEmpAddress._Employeeunkid, 1, objAEmpAddress._Adddresstype, "isprocessed") = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If

                                                        'S.SANDEEP |26-APR-2019| -- START
                                                        If objAEmpAddress._Newattachdocumentid.Length > 0 Then
                                                            If objAEmpAddress.UpdateDeleteDocument(objDataOperation, objAEmpAddress._Employeeunkid, objAEmpAddress._Employeeunkid, objAEmpAddress._Newattachdocumentid, dRow("form_name").ToString(), "transactionunkid", False) = False Then
                                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                                Throw exForce
                                                            End If
                                                        End If

                                                        If objAEmpAddress._Deleteattachdocumentid.Length > 0 Then
                                                            If objAEmpAddress.UpdateDeleteDocument(objDataOperation, objAEmpAddress._Employeeunkid, objAEmpAddress._Employeeunkid, objAEmpAddress._Deleteattachdocumentid, dRow("form_name").ToString(), "transactionunkid", True) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                        End If
                                                        'S.SANDEEP |26-APR-2019| -- END

                                                    Else
                                                        If objEmpMaster._Message.Trim.Length > 0 Then mstrMessage = objEmpMaster._Message
                                                        Return False
                                                    End If

                                            End Select
                                        End If
                                    ElseIf eStatusId = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                                        'If CBool(dRow("isfinal")) Then     'Gajanan [17-April-2019]
                                            Select Case eOprType
                                                Case enOperationType.ADDED, enOperationType.EDITED, enOperationType.DELETED

                                                    'S.SANDEEP |26-APR-2019| -- START
                                                    If objAEmpAddress._Newattachdocumentid.Length > 0 Then
                                                        If objAEmpAddress.UpdateDeleteDocument(objDataOperation, objAEmpAddress._Employeeunkid, objAEmpAddress._Employeeunkid, objAEmpAddress._Newattachdocumentid, dRow("form_name").ToString(), "transactionunkid", True) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                    End If

                                                    If objAEmpAddress._Deleteattachdocumentid.Length > 0 Then
                                                        If objAEmpAddress.UpdateDeleteDocument(objDataOperation, objAEmpAddress._Employeeunkid, objAEmpAddress._Employeeunkid, objAEmpAddress._Deleteattachdocumentid, dRow("form_name").ToString(), "transactionunkid", False) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                    End If
                                                    'S.SANDEEP |26-APR-2019| -- END

                                                    If objAEmpAddress.Update(objDataOperation, objAEmpAddress._Employeeunkid, 1, objAEmpAddress._Adddresstype, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                            End Select
                                        'End If      'Gajanan [17-April-2019]
                                    End If
                                End If

                            Else
                                If objAEmpAddress._Message.Trim.Length > 0 Then mstrMessage = objAEmpAddress._Message
                                Return False
                            End If

                        Next
                        'Gajanan [18-Mar-2019] -- End


                    'Gajanan [22-Feb-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    Case enScreenName.frmIdentityInfoList
                        Dim xtmpData As DataTable = dtTable.Clone()
                        For Each dRow As DataRow In dtTable.Rows
                            objAEmpIdentity = New clsIdentity_Approval_tran
                            objEmpIdentity = New clsIdentity_tran
                            If xtmpData.Rows.Count > 0 Then xtmpData.Rows.Clear()
                            xtmpData.ImportRow(dRow)
                            xtmpData.Rows(0)("statusunkid") = eStatusId
                            'S.SANDEEP |26-APR-2019| -- START
                            'If objAEmpIdentity.InsertUpdateDelete_IdentityTran(xtmpData, objDataOperation, intUserId) Then
     'Gajanan [17-April-2019] -- Start
     'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            xtmpData.Rows(0)("tranguid") = Guid.NewGuid.ToString()
                                 'Gajanan [17-April-2019] -- End
                              If objAEmpIdentity.InsertUpdateDelete_IdentityTran(xtmpData, objDataOperation, intUserId, intCompanyId) Then
                                'S.SANDEEP |26-APR-2019| -- END
                                If eStatusId = clsEmployee_Master.EmpApprovalStatus.Approved Then
                                    If CBool(dRow("isfinal")) Then
                                        Select Case eOprType
                                            Case enOperationType.ADDED
                                                objEmpIdentity._xDataOperation = objDataOperation
                                                objEmpIdentity._EmployeeUnkid = CInt(xtmpData.Rows(0)("employeeunkid"))
                                                objEmpIdentity._DataList = xtmpData


                                                If objEmpIdentity.InsertUpdateDelete_IdentityTran(objDataOperation, intUserId) Then

                                                    'S.SANDEEP |26-APR-2019| -- START
                                                    If xtmpData.Rows(0)("newattachdocumentid").ToString().Trim.Length > 0 Then
                                                        If objAEmpIdentity.UpdateDeleteDocument(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), objEmpIdentity._IdentityTranId, xtmpData.Rows(0)("newattachdocumentid").ToString(), xtmpData.Rows(0)("form_name").ToString(), "transactionunkid", False) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                    End If

                                                    If xtmpData.Rows(0)("deleteattachdocumentid").ToString().Trim.Length > 0 Then
                                                        If objAEmpIdentity.UpdateDeleteDocument(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("identitytranunkid")), xtmpData.Rows(0)("deleteattachdocumentid").ToString(), xtmpData.Rows(0)("form_name").ToString(), "transactionunkid", True) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                    End If
                                                    'S.SANDEEP |26-APR-2019| -- END

                                                    If objAEmpIdentity.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("idtypeunkid")), objEmpIdentity._IdentityTranId, "identitytranunkid") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If

                                                    If objAEmpIdentity.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("idtypeunkid")), 1, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                Else
                                                    objDataOperation.ReleaseTransaction(False)
                                                    Return False
                                                End If
                                                'IsProcessed = 1
                                            Case enOperationType.EDITED
                                                xtmpData.Rows(0)("AUD") = "U"
                                                xtmpData.AcceptChanges()
                                                objEmpIdentity._xDataOperation = objDataOperation
                                                objEmpIdentity._EmployeeUnkid = CInt(xtmpData.Rows(0)("employeeunkid"))
                                                objEmpIdentity._DataList = xtmpData



                                                If objEmpIdentity.InsertUpdateDelete_IdentityTran(objDataOperation, intUserId) Then

                                                    'S.SANDEEP |26-APR-2019| -- START
                                                    If xtmpData.Rows(0)("newattachdocumentid").ToString().Trim.Length > 0 Then
                                                        If objAEmpIdentity.UpdateDeleteDocument(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("identitytranunkid")), xtmpData.Rows(0)("newattachdocumentid").ToString(), xtmpData.Rows(0)("form_name").ToString(), "transactionunkid", False) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                    End If

                                                    If xtmpData.Rows(0)("deleteattachdocumentid").ToString().Trim.Length > 0 Then
                                                        If objAEmpIdentity.UpdateDeleteDocument(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("identitytranunkid")), xtmpData.Rows(0)("deleteattachdocumentid").ToString(), xtmpData.Rows(0)("form_name").ToString(), "transactionunkid", True) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                    End If
                                                    'S.SANDEEP |26-APR-2019| -- END

                                                    If objAEmpIdentity.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("idtypeunkid")), CInt(xtmpData.Rows(0)("identitytranunkid")), "identitytranunkid") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If

                                                    If objAEmpIdentity.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("idtypeunkid")), 1, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                Else
                                                    mstrMessage = objEmpIdentity._Message
                                                    objDataOperation.ReleaseTransaction(False)
                                                    Return False
                                                End If
                                            Case enOperationType.DELETED

                                                'S.SANDEEP |26-APR-2019| -- START
                                                If xtmpData.Rows(0)("newattachdocumentid").ToString().Trim.Length > 0 Then
                                                    If objAEmpIdentity.UpdateDeleteDocument(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("identitytranunkid")), xtmpData.Rows(0)("newattachdocumentid").ToString(), xtmpData.Rows(0)("form_name").ToString(), "transactionunkid", True) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                End If

                                                If xtmpData.Rows(0)("deleteattachdocumentid").ToString().Trim.Length > 0 Then
                                                    If objAEmpIdentity.UpdateDeleteDocument(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("identitytranunkid")), xtmpData.Rows(0)("deleteattachdocumentid").ToString(), xtmpData.Rows(0)("form_name").ToString(), "transactionunkid", True) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                End If
                                                'S.SANDEEP |26-APR-2019| -- END

                                                If objAEmpIdentity.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("idtypeunkid")), 1, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                        End Select
                                    End If
                                ElseIf eStatusId = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                                         'Gajanan [17-April-2019] -- Start
     'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                 'If CBool(dRow("isfinal")) Then
     'Gajanan [17-April-2019] -- End
                                    Select Case eOprType
                                        Case enOperationType.ADDED

                                            'S.SANDEEP |26-APR-2019| -- START
                                            If xtmpData.Rows(0)("newattachdocumentid").ToString().Trim.Length > 0 Then
                                                If objAEmpIdentity.UpdateDeleteDocument(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("identitytranunkid")), xtmpData.Rows(0)("newattachdocumentid").ToString(), xtmpData.Rows(0)("form_name").ToString(), "transactionunkid", True) = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                            End If

                                            If xtmpData.Rows(0)("deleteattachdocumentid").ToString().Trim.Length > 0 Then
                                                If objAEmpIdentity.UpdateDeleteDocument(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("identitytranunkid")), xtmpData.Rows(0)("deleteattachdocumentid").ToString(), xtmpData.Rows(0)("form_name").ToString(), "transactionunkid", False) = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                            End If
                                            'S.SANDEEP |26-APR-2019| -- END

                                            If objAEmpIdentity.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("idtypeunkid")), 1, "isprocessed") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                        Case enOperationType.EDITED

                                            'S.SANDEEP |26-APR-2019| -- START
                                            If xtmpData.Rows(0)("newattachdocumentid").ToString().Trim.Length > 0 Then
                                                If objAEmpIdentity.UpdateDeleteDocument(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("identitytranunkid")), xtmpData.Rows(0)("newattachdocumentid").ToString(), xtmpData.Rows(0)("form_name").ToString(), "transactionunkid", True) = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                            End If

                                            If xtmpData.Rows(0)("deleteattachdocumentid").ToString().Trim.Length > 0 Then
                                                If objAEmpIdentity.UpdateDeleteDocument(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("identitytranunkid")), xtmpData.Rows(0)("deleteattachdocumentid").ToString(), xtmpData.Rows(0)("form_name").ToString(), "transactionunkid", False) = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                            End If
                                            'S.SANDEEP |26-APR-2019| -- END

                                            If objAEmpIdentity.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("idtypeunkid")), 1, "isprocessed") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                        Case enOperationType.DELETED
                                            objEmpIdentity._xDataOperation = objDataOperation
                                            objEmpIdentity._EmployeeUnkid = CInt(xtmpData.Rows(0)("employeeunkid"))
                                            objEmpIdentity._DataList = xtmpData
                                            If objEmpIdentity.InsertUpdateDelete_IdentityTran(objDataOperation, intUserId) Then

                                                'S.SANDEEP |26-APR-2019| -- START
                                                If xtmpData.Rows(0)("newattachdocumentid").ToString().Trim.Length > 0 Then
                                                    If objAEmpIdentity.UpdateDeleteDocument(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("identitytranunkid")), xtmpData.Rows(0)("newattachdocumentid").ToString(), xtmpData.Rows(0)("form_name").ToString(), "transactionunkid", True) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                End If

                                                If xtmpData.Rows(0)("deleteattachdocumentid").ToString().Trim.Length > 0 Then
                                                    If objAEmpIdentity.UpdateDeleteDocument(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("identitytranunkid")), xtmpData.Rows(0)("deleteattachdocumentid").ToString(), xtmpData.Rows(0)("form_name").ToString(), "transactionunkid", False) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                End If
                                                'S.SANDEEP |26-APR-2019| -- END

                                                If objAEmpIdentity.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("idtypeunkid")), CInt(xtmpData.Rows(0)("identitytranunkid")), "identitytranunkid") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                If objAEmpIdentity.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("idtypeunkid")), 1, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                            Else
                                                objDataOperation.ReleaseTransaction(False)
                                                Return False
                                            End If
                                    End Select
     'Gajanan [17-April-2019] -- Start
     'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                    'End If
     'Gajanan [17-April-2019] -- End
                                End If
                            End If
                        Next
                        'Gajanan [22-Feb-2019] -- End

                        'S.SANDEEP |15-APR-2019| -- START
                    Case enScreenName.frmMembershipInfoList
                        Dim xtmpData As DataTable = dtTable.Clone()
                        For Each dRow As DataRow In dtTable.Rows
                            objAEmpMembership = New clsMembership_Approval_Tran
                            objEmpMembership = New clsMembershipTran
                            If xtmpData.Rows.Count > 0 Then xtmpData.Rows.Clear()
                            xtmpData.ImportRow(dRow)
                            xtmpData.Rows(0)("statusunkid") = eStatusId
                            xtmpData.Rows(0)("auditdatetime") = dtCurrentDateTime
                            xtmpData.Rows(0)("audittype") = enAuditType.ADD
                            xtmpData.Rows(0)("audituserunkid") = intUserId
                            xtmpData.Rows(0)("ip") = xIPAddr
                            xtmpData.Rows(0)("host") = xHostName
                            xtmpData.Rows(0)("form_name") = strFormName
                            xtmpData.Rows(0)("isweb") = blnIsweb
                            xtmpData.Rows(0)("mappingunkid") = intUserMappingTranId
     'Gajanan [17-April-2019] -- Start
     'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            xtmpData.Rows(0)("tranguid") = Guid.NewGuid.ToString()
     'Gajanan [17-April-2019] -- End
                            'Hemant (23 Apr 2020) -- Start
                            'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.
                            xtmpData.Rows(0)("approvalremark") = strRemark
                            xtmpData.Rows(0)("transactiondate") = Now
                            'Hemant (23 Apr 2020) -- End
                            If objAEmpMembership.InsertUpdateDelete_MembershipTran(xtmpData, objDataOperation, intUserId, xDatabaseName, xYearId, intCompanyId, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee) Then
                                If eStatusId = clsEmployee_Master.EmpApprovalStatus.Approved Then
                                    If CBool(dRow("isfinal")) Then
                                        Select Case eOprType
                                            Case enOperationType.ADDED
                                                objEmpMembership._xDataOperation = objDataOperation
                                                objEmpMembership._EmployeeUnkid = CInt(xtmpData.Rows(0)("employeeunkid"))
                                                objEmpMembership._DataList = xtmpData


                                                If objEmpMembership.InsertUpdateDelete_MembershipTran(xDatabaseName, intUserId, xYearId, intCompanyId, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateTime, objDataOperation, False, "") Then
                                                    If objAEmpMembership.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("membershipunkid")), objEmpMembership._MembershipTranId, "membershiptranunkid") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If

                                                    If objAEmpMembership.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("membershipunkid")), 1, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                Else
                                                    objDataOperation.ReleaseTransaction(False)
                                                    Return False
                                                End If
                                            Case enOperationType.EDITED
                                                xtmpData.Rows(0)("AUD") = "U"
                                                xtmpData.AcceptChanges()
                                                objEmpMembership._xDataOperation = objDataOperation
                                                objEmpMembership._EmployeeUnkid = CInt(xtmpData.Rows(0)("employeeunkid"))
                                                objEmpMembership._DataList = xtmpData
                                                If objEmpMembership.InsertUpdateDelete_MembershipTran(xDatabaseName, intUserId, xYearId, intCompanyId, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateTime, objDataOperation, False, "") Then
                                                    If objAEmpMembership.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("membershipunkid")), CInt(xtmpData.Rows(0)("membershiptranunkid")), "membershiptranunkid") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If

                                                    If objAEmpMembership.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("membershipunkid")), 1, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                Else
                                                    mstrMessage = objEmpMembership._ErrorMessage
                                                    objDataOperation.ReleaseTransaction(False)
                                                    Return False
                                                End If
                                            Case enOperationType.DELETED
                                                If objAEmpMembership.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("membershipunkid")), 1, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                        End Select
                                    End If
                                ElseIf eStatusId = clsEmployee_Master.EmpApprovalStatus.Rejected Then
     'Gajanan [17-April-2019] -- Start
     'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                    'If CBool(dRow("isfinal")) Then
     'Gajanan [17-April-2019] -- End
                                    Select Case eOprType
                                        Case enOperationType.ADDED
                                            If objAEmpMembership.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("membershipunkid")), 1, "isprocessed") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                        Case enOperationType.EDITED
                                            If objAEmpMembership.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("membershipunkid")), 1, "isprocessed") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                        Case enOperationType.DELETED
                                            xtmpData.Rows(0)("AUD") = "U"
                                            xtmpData.Rows(0)("isactive") = True
                                            xtmpData.Rows(0)("isdeleted") = False
                                            xtmpData.AcceptChanges()
                                            objEmpMembership._xDataOperation = objDataOperation
                                            objEmpMembership._EmployeeUnkid = CInt(xtmpData.Rows(0)("employeeunkid"))
                                            objEmpMembership._DataList = xtmpData
                                            If objEmpMembership.InsertUpdateDelete_MembershipTran(xDatabaseName, intUserId, xYearId, intCompanyId, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, dtCurrentDateTime, objDataOperation, False, "") Then
                                                If objAEmpMembership.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("membershipunkid")), CInt(xtmpData.Rows(0)("membershiptranunkid")), "membershiptranunkid") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                If objAEmpMembership.Update(objDataOperation, CInt(xtmpData.Rows(0)("employeeunkid")), CInt(xtmpData.Rows(0)("membershipunkid")), 1, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                            Else
                                                objDataOperation.ReleaseTransaction(False)
                                                Return False
                                            End If
                                    End Select
                                End If
     'Gajanan [17-April-2019] -- Start
     'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                'End If
     'Gajanan [17-April-2019] -- End
                                End If
                        Next
                        'S.SANDEEP |15-APR-2019| -- END

                    Case enScreenName.frmDependantsAndBeneficiariesList
                        For Each dRow As DataRow In dtTable.Rows
                            objAEmpDependant = New clsDependant_beneficiaries_approval_tran
                            objEmployeeDependantTran = New clsDependants_Beneficiary_tran

                            objAEmpDependant._Audittype = enAuditType.ADD
                            objAEmpDependant._Audituserunkid = intUserId
                            'Sohail (18 May 2019) -- Start
                            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                            objAEmpDependant._Auditdatetime = dtCurrentDateTime
                            'Sohail (18 May 2019) -- End

                            objAEmpDependant._Address = dRow("address").ToString

                            If IsDBNull(dRow("birthdate")) = False Then
                                objAEmpDependant._Birthdate = CDate(dRow("birthdate"))
                            Else
                                objAEmpDependant._Birthdate = Nothing
                            End If
                            objAEmpDependant._Cityunkid = CInt(dRow("cityunkid"))
                            objAEmpDependant._Countryunkid = CInt(dRow("countryunkid"))
                            objAEmpDependant._Email = dRow("email").ToString
                            objAEmpDependant._Employeeunkid = CInt(dRow("employeeunkid"))
                            objAEmpDependant._First_Name = dRow("first_name").ToString
                            objAEmpDependant._Identify_No = dRow("Identify_No").ToString
                            objAEmpDependant._Isdependant = CBool(dRow("Isdependant").ToString)

                            objAEmpDependant._Last_Name = dRow("last_name").ToString
                            objAEmpDependant._Middle_Name = dRow("middle_name").ToString
                            objAEmpDependant._Mobile_No = dRow("mobile_no").ToString
                            objAEmpDependant._Nationalityunkid = CInt(dRow("nationalityunkid").ToString())
                            objAEmpDependant._Post_Box = dRow("post_box").ToString
                            objAEmpDependant._Relationunkid = CInt(dRow("relationunkid").ToString)
                            objAEmpDependant._Stateunkid = CInt(dRow("stateunkid").ToString)
                            objAEmpDependant._Telephone_No = dRow("telephone_no").ToString
                            objAEmpDependant._Zipcodeunkid = CInt(dRow("zipcodeunkid").ToString)
                            objAEmpDependant._Gender = CInt(dRow("genderid").ToString)
                            objAEmpDependant._CompanyId = ConfigParameter._Object._Companyunkid
                            objAEmpDependant._blnImgInDb = ConfigParameter._Object._IsImgInDataBase

                            objAEmpDependant._Form_Name = strFormName
                            objAEmpDependant._Host = xHostName
                            objAEmpDependant._Ip = xIPAddr
                            objAEmpDependant._Isfinal = CBool(dRow("isfinal"))
                            objAEmpDependant._Isprocessed = False
                            objAEmpDependant._Isvoid = False
                            objAEmpDependant._Isweb = blnIsweb
                            objAEmpDependant._Mappingunkid = intUserMappingTranId
                            objAEmpDependant._Statusunkid = eStatusId
                            objAEmpDependant._Transactiondate = Now
                            objAEmpDependant._Tranguid = Guid.NewGuid.ToString()
                            objAEmpDependant._ImagePath = ""

                            'Gajanan [22-Feb-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            objAEmpDependant._Newattacheddocumnetid = dRow("newattachdocumentid").ToString()
                            objAEmpDependant._Deletedattachdocumnetid = dRow("deleteattachdocumentid").ToString()
                            objAEmpDependant._Newattachedimageid = CInt(dRow("newattachimageid").ToString())
                            objAEmpDependant._Deletedattachedimageid = CInt(dRow("deleteattachimageid").ToString())
                            'Gajanan [22-Feb-2019] -- End

                            'Sohail (18 May 2019) -- Start
                            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                            If IsDBNull(dRow("effective_date")) = False Then
                                objAEmpDependant._Effective_date = CDate(dRow("effective_date"))
                            Else
                                objAEmpDependant._Effective_date = Nothing
                            End If
                            objAEmpDependant._Isactive = CBool(dRow("isactive"))
                            objAEmpDependant._ReasonUnkid = CInt(dRow("reasonUnkid"))
                            objAEmpDependant._IsFromStatus = CBool(dRow("isfromstatus"))
                            objAEmpDependant._DeletedpndtbeneficestatustranIDs = dRow("deletedpndtbeneficestatustranids").ToString
                            'Sohail (18 May 2019) -- End
                            'Hemant (23 Apr 2020) -- Start
                            'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.
                            objAEmpDependant._Approvalremark = strRemark
                            'Hemant (23 Apr 2020) -- End

                            objAEmpDependant._Operationtypeid = eOprType
                            If eOprType = enOperationType.EDITED Or eOprType = enOperationType.DELETED Then
                                objAEmpDependant._Dpndtbeneficetranunkid = CInt(dRow("dpndtbeneficetranunkid"))
                            End If


                            Dim objDependantMemberTran As New clsDependants_Membership_tran
                            Dim objDependantBenefitTran As New clsDependants_Benefit_tran

                            Dim objDependant_beneficiaries_approval_tran As New clsDependant_beneficiaries_approval_tran

                            mdtMemTran = objDependant_beneficiaries_approval_tran.Get_Membership_Tran(dRow("tranguid").ToString, objDataOperation).Tables("MembershipList")
                            mdtBenefitTran = objDependant_beneficiaries_approval_tran.Get_Benefit_Tran(dRow("tranguid").ToString, objDataOperation).Tables("BenefitList")


                            'Sohail (18 May 2019) -- Start
                            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                            'If objAEmpDependant.Insert(objDataOperation, mdtMemTran, mdtBenefitTran, Nothing, False, True) Then
                            If objAEmpDependant.Insert(objDataOperation, mdtMemTran, mdtBenefitTran, Nothing, False, True, False) Then
                                objEmployeeDependantTran._xDataOp = objDataOperation
                                'Sohail (18 May 2019) -- End
                                If eStatusId = clsEmployee_Master.EmpApprovalStatus.Approved Then
                                    If CBool(dRow("isfinal")) Then
                                        If eOprType <> enOperationType.DELETED Then
                                            objEmployeeDependantTran._Dpndtbeneficetranunkid = objAEmpDependant._Dpndtbeneficetranunkid
                                            objEmployeeDependantTran._Address = objAEmpDependant._Address
                                            objEmployeeDependantTran._Birthdate = objAEmpDependant._Birthdate
                                            objEmployeeDependantTran._Cityunkid = objAEmpDependant._Cityunkid
                                            objEmployeeDependantTran._Countryunkid = objAEmpDependant._Countryunkid
                                            objEmployeeDependantTran._Email = objAEmpDependant._Email
                                            objEmployeeDependantTran._Employeeunkid = objAEmpDependant._Employeeunkid
                                            objEmployeeDependantTran._First_Name = objAEmpDependant._First_Name
                                            objEmployeeDependantTran._Identify_No = objAEmpDependant._Identify_No
                                            objEmployeeDependantTran._Isdependant = objAEmpDependant._Isdependant
                                            objEmployeeDependantTran._Last_Name = objAEmpDependant._Last_Name
                                            objEmployeeDependantTran._Middle_Name = objAEmpDependant._Middle_Name
                                            objEmployeeDependantTran._Mobile_No = objAEmpDependant._Mobile_No
                                            objEmployeeDependantTran._Nationalityunkid = objAEmpDependant._Nationalityunkid
                                            objEmployeeDependantTran._Post_Box = objAEmpDependant._Post_Box
                                            objEmployeeDependantTran._Relationunkid = objAEmpDependant._Relationunkid
                                            objEmployeeDependantTran._Stateunkid = objAEmpDependant._Stateunkid
                                            objEmployeeDependantTran._Telephone_No = objAEmpDependant._Telephone_No
                                            objEmployeeDependantTran._Zipcodeunkid = objAEmpDependant._Zipcodeunkid
                                            objEmployeeDependantTran._Gender = objAEmpDependant._Gender
                                            objEmployeeDependantTran._CompanyId = ConfigParameter._Object._Companyunkid
                                            objEmployeeDependantTran._blnImgInDb = ConfigParameter._Object._IsImgInDataBase
                                            objEmployeeDependantTran._Userunkid = intUserId
                                            objEmployeeDependantTran._Isvoid = False
                                            objEmployeeDependantTran._Voiddatetime = Nothing
                                            objEmployeeDependantTran._Voiduserunkid = -1


                                            'Sohail (18 May 2019) -- Start
                                            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                                            objEmployeeDependantTran._Effective_date = objAEmpDependant._Effective_date
                                            objEmployeeDependantTran._Isactive = objAEmpDependant._Isactive
                                            objEmployeeDependantTran._Reasonunkid = objAEmpDependant._ReasonUnkid
                                            objEmployeeDependantTran._IsFromStatus = objAEmpDependant._IsFromStatus
                                            objEmployeeDependantTran._xDataOp = objDataOperation

                                            objEmployeeDependantTran._Loginemployeeunkid = objAEmpDependant._Loginemployeeunkid
                                            objEmployeeDependantTran._AuditDate = objAEmpDependant._Auditdatetime
                                            objEmployeeDependantTran._CompanyUnkid = objAEmpDependant._CompanyId
                                            objEmployeeDependantTran._Isweb = objAEmpDependant._Isweb
                                            'Sohail (18 May 2019) -- End


                                            'Gajanan [14-AUG-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            If blnIsweb Then
                                                objEmployeeDependantTran._WebFormName = objAEmpDependant._Form_Name
                                                objEmployeeDependantTran._WebHostName = objAEmpDependant._Host
                                                objEmployeeDependantTran._WebClientIP = objAEmpDependant._Ip
                                                objEmployeeDependantTran._Userunkid = objAEmpDependant._Audituserunkid
                                            End If
                                            'Gajanan [14-AUG-2019] -- End

                                        End If


                                        If objAEmpDependant._Dpndtbeneficetranunkid > 0 Then
                                            Select Case objAEmpDependant._Operationtypeid
                                                Case enOperationType.EDITED
                                                    If objEmployeeDependantTran.Update(mdtMemTran, mdtBenefitTran, Nothing, objDataOperation, True) Then

                                                        'Gajanan [27-May-2019] -- Start              


                                                        'If objAEmpDependant.Update(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Dpndtbeneficetranunkid, objEmployeeDependantTran._Dpndtbeneficetranunkid, "dpndtbeneficetranunkid") = False Then
                                                        If objAEmpDependant.Update(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Dpndtbeneficetranunkid, objEmployeeDependantTran._Dpndtbeneficetranunkid, "dpndtbeneficetranunkid", dRow("first_name").ToString, dRow("last_name").ToString) = False Then
                                                            'Gajanan [27-May-2019] -- End
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If

                                                        'Gajanan [27-May-2019] -- Start              
                                                        'If objAEmpDependant.Update(objDataOperation, objAEmpDependant._Employeeunkid, objEmployeeDependantTran._Dpndtbeneficetranunkid, 1, "isprocessed") = False Then
                                                        If objAEmpDependant.Update(objDataOperation, objAEmpDependant._Employeeunkid, objEmployeeDependantTran._Dpndtbeneficetranunkid, 1, "isprocessed", dRow("first_name").ToString, dRow("last_name").ToString) = False Then
                                                            'Gajanan [27-May-2019] -- End
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If


                                                        If objAEmpDependant._Newattacheddocumnetid.Length > 0 Then
                                                            If objAEmpDependant.UpdateDeleteDocument(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Newattacheddocumnetid, dRow("form_name").ToString(), objAEmpDependant._Dpndtbeneficetranunkid, "transactionunkid", False) = False Then
                                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                                Throw exForce
                                                            End If
                                                        End If

                                                        If objAEmpDependant._Deletedattachdocumnetid.Length > 0 Then
                                                            If objAEmpDependant.UpdateDeleteDocument(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Deletedattachdocumnetid, dRow("form_name").ToString(), objAEmpDependant._Dpndtbeneficetranunkid, "transactionunkid", True) = False Then
                                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                                Throw exForce
                                                            End If
                                                        End If


                                                        If CInt(dRow("newattachimageid").ToString()) > 0 Then
                                                            If objAEmpDependant.UpdateDeleteImage(objDataOperation, objAEmpDependant._Employeeunkid, CInt(dRow("newattachimageid").ToString()), objAEmpDependant._Dpndtbeneficetranunkid, False) = False Then
                                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                                Throw exForce
                                                            End If
                                                        End If

                                                        If CInt(dRow("deleteattachimageid").ToString()) > 0 Then
                                                            If objAEmpDependant.UpdateDeleteImage(objDataOperation, objAEmpDependant._Employeeunkid, CInt(dRow("deleteattachimageid").ToString()), objAEmpDependant._Dpndtbeneficetranunkid, True) = False Then
                                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                                Throw exForce
                                                            End If
                                                        End If
                                                    Else
                                                        If objEmployeeDependantTran._Message.Trim.Length > 0 Then mstrMessage = objEmployeeDependantTran._Message
                                                        Return False
                                                    End If
                                                Case enOperationType.DELETED

                                                    'Gajanan [10-June-2019] -- Start      
                                                    'ISSUE/ENHANCEMENT : Grievance UAT Changes        


                                                    'If objAEmpDependant.Update(objDataOperation, objAEmpDependant._Employeeunkid, 1, objAEmpDependant._Dpndtbeneficetranunkid, "isprocessed") = False Then
                                                    'Sohail (18 May 2019) -- Start
                                                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                                                    'If objAEmpDependant.Update(objDataOperation, objAEmpDependant._Employeeunkid, 1, objAEmpDependant._Dpndtbeneficetranunkid, "isprocessed", dRow("first_name").ToString, dRow("last_name").ToString) = False Then
                                                    If objAEmpDependant.Update(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Dpndtbeneficetranunkid, 1, "isprocessed", dRow("first_name").ToString, dRow("last_name").ToString) = False Then
                                                        'Sohail (18 May 2019) -- End
                                                        'Gajanan [27-May-2019] -- End
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If


                                                    If objAEmpDependant._Newattacheddocumnetid.Length > 0 Then
                                                        If objAEmpDependant.UpdateDeleteDocument(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Newattacheddocumnetid, dRow("form_name").ToString(), objAEmpDependant._Dpndtbeneficetranunkid, "transactionunkid", True) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                    End If

                                                    If objAEmpDependant._Deletedattachdocumnetid.Length > 0 Then
                                                        If objAEmpDependant.UpdateDeleteDocument(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Deletedattachdocumnetid, dRow("form_name").ToString(), objAEmpDependant._Dpndtbeneficetranunkid, "transactionunkid", False) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                    End If

                                                    If CInt(dRow("newattachimageid").ToString()) > 0 Then
                                                        If objAEmpDependant.UpdateDeleteImage(objDataOperation, objAEmpDependant._Employeeunkid, CInt(dRow("newattachimageid").ToString()), objAEmpDependant._Dpndtbeneficetranunkid, True) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                    End If

                                                    If CInt(dRow("deleteattachimageid").ToString()) > 0 Then
                                                        If objAEmpDependant.UpdateDeleteImage(objDataOperation, objAEmpDependant._Employeeunkid, CInt(dRow("deleteattachimageid").ToString()), objAEmpDependant._Dpndtbeneficetranunkid, False) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                    End If

                                                    'Sohail (18 May 2019) -- Start
                                                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                                                    If objAEmpDependant._DeletedpndtbeneficestatustranIDs.Length > 0 Then
                                                        If objAEmpDependant.UpdateDeleteDPStatus(objDataOperation, objAEmpDependant._DeletedpndtbeneficestatustranIDs, objAEmpDependant._Dpndtbeneficetranunkid, "dpndtbeneficetranunkid", True) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                    End If
                                                    'Sohail (18 May 2019) -- End

                                            End Select

                                        Else
                                            If objEmployeeDependantTran.Insert(mdtMemTran, mdtBenefitTran, Nothing, objDataOperation) Then

                                                'Gajanan [27-May-2019] -- Start              


                                                'If objAEmpDependant.Update(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Dpndtbeneficetranunkid, objEmployeeDependantTran._Dpndtbeneficetranunkid, "dpndtbeneficetranunkid") = False Then
                                                If objAEmpDependant.Update(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Dpndtbeneficetranunkid, objEmployeeDependantTran._Dpndtbeneficetranunkid, "dpndtbeneficetranunkid", dRow("first_name").ToString, dRow("last_name").ToString) = False Then
                                                    'Gajanan [27-May-2019] -- End


                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If


                                                'Gajanan [27-May-2019] -- Start              
                                                'If objAEmpDependant.Update(objDataOperation, objAEmpDependant._Employeeunkid, objEmployeeDependantTran._Dpndtbeneficetranunkid, 1, "isprocessed") = False Then
                                                If objAEmpDependant.Update(objDataOperation, objAEmpDependant._Employeeunkid, objEmployeeDependantTran._Dpndtbeneficetranunkid, 1, "isprocessed", dRow("first_name").ToString, dRow("last_name").ToString) = False Then
                                                    'Gajanan [27-May-2019] -- End
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.


    
                                                If objAEmpDependant._Newattacheddocumnetid.Length > 0 Then
                                                    If objAEmpDependant.UpdateDeleteDocument(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Newattacheddocumnetid, dRow("form_name").ToString(), objEmployeeDependantTran._Dpndtbeneficetranunkid, "transactionunkid", False) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                End If


                                                If objAEmpDependant._Deletedattachdocumnetid.Length > 0 Then
                                                    If objAEmpDependant.UpdateDeleteDocument(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Deletedattachdocumnetid, dRow("form_name").ToString(), objEmployeeDependantTran._Dpndtbeneficetranunkid, "transactionunkid", True) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                End If

                                                If CInt(dRow("newattachimageid").ToString()) > 0 Then
                                                    If objAEmpDependant.UpdateDeleteImage(objDataOperation, objAEmpDependant._Employeeunkid, CInt(dRow("newattachimageid").ToString()), objEmployeeDependantTran._Dpndtbeneficetranunkid, False) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                End If


                                                If CInt(dRow("deleteattachimageid").ToString()) > 0 Then
                                                    If objAEmpDependant.UpdateDeleteImage(objDataOperation, objAEmpDependant._Employeeunkid, CInt(dRow("deleteattachimageid").ToString()), objEmployeeDependantTran._Dpndtbeneficetranunkid, True) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                End If
'Gajanan [17-April-2019] -- End
                                            Else
                                                If objEmployeeDependantTran._Message.Trim.Length > 0 Then mstrMessage = objEmployeeDependantTran._Message
                                                Return False
                                            End If
                                        End If

                                    End If

                                Else
                                    If eStatusId = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                                        If eOprType = enOperationType.DELETED Then
                                            objEmployeeDependantTran._Dpndtbeneficetranunkid = objAEmpDependant._Dpndtbeneficetranunkid
                                            If objAEmpDependant._IsFromStatus = False Then 'Sohail (18 May 2019)
                                            objEmployeeDependantTran._Isvoid = False
                                            objEmployeeDependantTran._Voidreason = ""
                                            objEmployeeDependantTran._Voiddatetime = Nothing
                                            objEmployeeDependantTran._Voiduserunkid = 0


                                                'Gajanan [14-AUG-2019] -- Start      
                                                'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                                If blnIsweb Then
                                            objEmployeeDependantTran._WebClientIP = xIPAddr
                                            objEmployeeDependantTran._WebHostName = xHostName
                                            objEmployeeDependantTran._WebClientIP = strFormName
                                                    objEmployeeDependantTran._CompanyUnkid = intCompanyId
                                                    objEmployeeDependantTran._Userunkid = intUserId
                                                End If
                                                'Gajanan [14-AUG-2019] -- End





                                            Dim drtemp() As DataRow = Nothing
                                            drtemp = mdtMemTran.Select("AUD = 'D'")

                                            If drtemp.Length > 0 Then
                                                For index As Integer = 0 To drtemp.Length - 1
                                                    drtemp(index)("AUD") = "U"
                                                    drtemp(index)("Isvoid") = False
                                                    drtemp(index)("Voiduserunkid") = -1
                                                    drtemp(index)("voiddatetime") = DBNull.Value
                                                Next
                                                drtemp = Nothing
                                            End If
                                            mdtMemTran.AcceptChanges()


                                            drtemp = mdtBenefitTran.Select("AUD = 'D'")
                                            If drtemp.Length > 0 Then
                                                For index As Integer = 0 To drtemp.Length - 1
                                                    drtemp(index)("AUD") = "U"
                                                    drtemp(index)("Isvoid") = False
                                                    drtemp(index)("Voiduserunkid") = -1
                                                    drtemp(index)("Voiddatetime") = DBNull.Value
                                                Next
                                                drtemp = Nothing
                                            End If
                                            mdtBenefitTran.AcceptChanges()
                                            End If 'Sohail (18 May 2019)


                                            If objAEmpDependant._IsFromStatus = False AndAlso objEmployeeDependantTran.Update(mdtMemTran, mdtBenefitTran, Nothing, objDataOperation) Then'Sohail (18 May 2019)
                                                'Sohail (18 May 2019) -- Start
                                                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                                                'Dim objDBStatus As New clsDependant_Benefice_Status_tran
                                                'objDBStatus._Dpndtbeneficestatustranunkid = objEmployeeDependantTran._Dpndtbeneficestatustranunkid
                                                'objDBStatus._Isvoid = False
                                                'objDBStatus._Voidreason = ""
                                                'objDBStatus._Voiddatetime = Nothing
                                                'objDBStatus._Voiduserunkid = 0
                                                'objDBStatus._Voidloginemployeeunkid = 0
                                                'objDBStatus._xDataOp = objDataOperation
                                                'objDBStatus._FormName = strFormName
                                                'objDBStatus._HostName = xHostName
                                                'objDBStatus._ClientIP = xIPAddr
                                                'objDBStatus._AuditUserId = intUserUnkId
                                                'objDBStatus._Loginemployeeunkid = objEmployeeDependantTran._Loginemployeeunkid
                                                'objDBStatus._AuditDate = dtCurrentDateTime
                                                'objDBStatus._Isweb = blnIsweb
                                                'objDBStatus._CompanyUnkid = intCompanyId

                                                'If objDBStatus.Update() = False Then
                                                '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                '    Throw exForce
                                                'End If
                                                'Sohail (18 May 2019) -- End


                                                'Gajanan [27-May-2019] -- Start              


                                                'If objAEmpDependant.Update(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Dpndtbeneficetranunkid, 1, "isprocessed") = False Then
                                                If objAEmpDependant.Update(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Dpndtbeneficetranunkid, 1, "isprocessed", dRow("first_name").ToString, dRow("last_name").ToString) = False Then
                                                    'Gajanan [27-May-2019] -- End

                                           ' If objEmployeeDependantTran.Update(mdtMemTran, mdtBenefitTran, Nothing, objDataOperation) Then
                                             '   If objAEmpDependant.Update(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Dpndtbeneficetranunkid, 1, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                If objAEmpDependant._Newattacheddocumnetid.Length > 0 Then
                                                    If objAEmpDependant.UpdateDeleteDocument(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Newattacheddocumnetid, dRow("form_name").ToString(), objAEmpDependant._Dpndtbeneficetranunkid, "transactionunkid", True) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                End If

                                                If objAEmpDependant._Deletedattachdocumnetid.Length > 0 Then
                                                    If objAEmpDependant.UpdateDeleteDocument(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Deletedattachdocumnetid, dRow("form_name").ToString(), objAEmpDependant._Dpndtbeneficetranunkid, "transactionunkid", False) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                End If

                                                If CInt(dRow("newattachimageid").ToString()) > 0 Then
                                                    If objAEmpDependant.UpdateDeleteImage(objDataOperation, objAEmpDependant._Employeeunkid, CInt(dRow("newattachimageid").ToString()), objAEmpDependant._Dpndtbeneficetranunkid, True) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                End If

                                                If CInt(dRow("deleteattachimageid").ToString()) > 0 Then
                                                    If objAEmpDependant.UpdateDeleteImage(objDataOperation, objAEmpDependant._Employeeunkid, CInt(dRow("deleteattachimageid").ToString()), objAEmpDependant._Dpndtbeneficetranunkid, False) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                End If

                                                'Sohail (18 May 2019) -- Start
                                                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                                                If objAEmpDependant._DeletedpndtbeneficestatustranIDs.Length > 0 Then
                                                    If objAEmpDependant.UpdateDeleteDPStatus(objDataOperation, objAEmpDependant._DeletedpndtbeneficestatustranIDs, objAEmpDependant._Dpndtbeneficetranunkid, "dpndtbeneficetranunkid", False) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                End If

                                            ElseIf objAEmpDependant._DeletedpndtbeneficestatustranIDs.Length > 0 Then
                                                If objAEmpDependant.UpdateDeleteDPStatus(objDataOperation, objAEmpDependant._DeletedpndtbeneficestatustranIDs, objAEmpDependant._Dpndtbeneficetranunkid, "dpndtbeneficetranunkid", False) = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                If objAEmpDependant.Update(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Dpndtbeneficetranunkid, 1, "isprocessed", dRow("first_name").ToString, dRow("last_name").ToString) = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                                'Sohail (18 May 2019) -- End
                                            Else
                                                If objEmployeeDependantTran._Message.Trim.Length > 0 Then mstrMessage = objEmployeeDependantTran._Message
                                                Return False
                                            End If
                                        Else

                                            'Gajanan [27-May-2019] -- Start              


                                            'If objAEmpDependant.Update(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Dpndtbeneficetranunkid, 1, "isprocessed") = False Then
                                            If objAEmpDependant.Update(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Dpndtbeneficetranunkid, 1, "isprocessed", dRow("first_name").ToString, dRow("last_name").ToString) = False Then
                                                'Gajanan [27-May-2019] -- End
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            If objAEmpDependant.UpdateDeleteDocument(objDataOperation, objAEmpDependant._Employeeunkid, objAEmpDependant._Newattacheddocumnetid, objAEmpDependant._Form_Name, objAEmpDependant._Dpndtbeneficetranunkid, "transactionunkid", True) = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            If CInt(dRow("newattachimageid").ToString()) > 0 Then
                                                If objAEmpDependant.UpdateDeleteImage(objDataOperation, objAEmpDependant._Employeeunkid, CInt(dRow("newattachimageid").ToString()), objAEmpDependant._Dpndtbeneficetranunkid, True) = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                            End If

                                            If CInt(dRow("deleteattachimageid").ToString()) > 0 Then
                                                If objAEmpDependant.UpdateDeleteImage(objDataOperation, objAEmpDependant._Employeeunkid, CInt(dRow("deleteattachimageid").ToString()), objAEmpDependant._Dpndtbeneficetranunkid, False) = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                            End If

                                        End If
                                    End If
                                End If

                            Else
                                If objAEmpDependant._Message.Trim.Length > 0 Then mstrMessage = objAEmpDependant._Message
                                Return False
                            End If
                        Next
                        'Gajanan [22-Feb-2019] -- End`

                    Case enScreenName.frmQualificationsList
                        For Each dRow As DataRow In dtTable.Rows


                            objAEmpQualificationTran = New clsEmp_Qualification_Approval_Tran
                            objEmployeeQualificationTran = New clsEmp_Qualification_Tran

                            objAEmpQualificationTran._Approvalremark = strRemark
                            objAEmpQualificationTran._Auditdatetime = Now
                            objAEmpQualificationTran._Audittype = enAuditType.ADD
                            objAEmpQualificationTran._Audituserunkid = intUserId
                            objAEmpQualificationTran._Employeeunkid = CInt(dRow("employeeunkid"))
                            objAEmpQualificationTran._Form_Name = strFormName
                            objAEmpQualificationTran._Host = xHostName
                            objAEmpQualificationTran._Ip = xIPAddr
                            objAEmpQualificationTran._Isfinal = CBool(dRow("isfinal"))
                            objAEmpQualificationTran._Isprocessed = False
                            objAEmpQualificationTran._Isvoid = False
                            objAEmpQualificationTran._Isweb = blnIsweb
                            objAEmpQualificationTran._Mappingunkid = intUserMappingTranId
                            objAEmpQualificationTran._Statusunkid = eStatusId
                            objAEmpQualificationTran._Transactiondate = Now
                            objAEmpQualificationTran._Tranguid = Guid.NewGuid.ToString()

                            If IsDBNull(dRow("award_end_date")) = False AndAlso CDate(dRow("award_end_date")) <> Nothing Then
                                objAEmpQualificationTran._Award_End_Date = CDate(dRow("award_end_date"))
                            Else
                                objAEmpQualificationTran._Award_End_Date = Nothing
                            End If

                            If IsDBNull(dRow("award_start_date")) = False AndAlso CDate(dRow("award_start_date")) <> Nothing Then
                                objAEmpQualificationTran._Award_Start_Date = CDate(dRow("award_start_date"))
                            Else
                                objAEmpQualificationTran._Award_Start_Date = Nothing
                            End If

                            objAEmpQualificationTran._Employeeunkid = CInt(dRow("employeeunkid"))
                            objAEmpQualificationTran._Qualificationgroupunkid = CInt(dRow("qualificationgroupunkid"))
                            objAEmpQualificationTran._Qualificationunkid = CInt(dRow("qualificationunkid"))
                            objAEmpQualificationTran._Reference_No = dRow("reference_no").ToString
                            objAEmpQualificationTran._Remark = dRow("remark").ToString
                            objAEmpQualificationTran._Instituteunkid = CInt(dRow("instituteunkid"))

                            objAEmpQualificationTran._Isvoid = False

                            objAEmpQualificationTran._Resultunkid = CInt(dRow("resultunkid"))
                            objAEmpQualificationTran._Gpacode = CInt(dRow("gpacode"))

                            objAEmpQualificationTran._Other_Qualificationgrp = dRow("other_qualificationgrp").ToString
                            objAEmpQualificationTran._Other_Qualification = dRow("other_qualification").ToString
                            objAEmpQualificationTran._Other_Resultcode = dRow("other_resultcode").ToString
                            objAEmpQualificationTran._Other_Institute = dRow("other_institute").ToString

                            'Gajanan [22-Feb-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            'objAEmpQualificationTran._LinkedMasterId = CInt(dRow("linkedmasterid"))
                            'Gajanan [22-Feb-2019] -- End


                            'Gajanan [22-Feb-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            objAEmpQualificationTran._Newattacheddocumnetid = dRow("newattachdocumentid").ToString()
                            objAEmpQualificationTran._Deletedattachdocumnetid = dRow("deleteattachdocumentid").ToString()
                            'Gajanan [22-Feb-2019] -- End


                            If IsDBNull(dRow("certificatedate")) = False AndAlso CDate(dRow("certificatedate")) <> Nothing Then
                                objAEmpQualificationTran._Certificatedate = CDate(dRow("certificatedate"))
                            Else
                                objAEmpQualificationTran._Certificatedate = Nothing
                            End If
                            objAEmpQualificationTran._OperationTypeId = eOprType
                            If eOprType = enOperationType.EDITED Or eOprType = enOperationType.DELETED Then
                                objAEmpQualificationTran._Qualificationtranunkid = CInt(dRow("Qualificationtranunkid"))
                            End If

                            If objAEmpQualificationTran.Insert(intCompanyId, objDataOperation, True) Then
                                If eStatusId = clsEmployee_Master.EmpApprovalStatus.Approved Then
                                    If CBool(dRow("isfinal")) Then

                                        If eOprType <> enOperationType.DELETED Then
                                            objEmployeeQualificationTran._Qualificationtranunkid = objAEmpQualificationTran._Qualificationtranunkid

                                            objEmployeeQualificationTran._Award_End_Date = objAEmpQualificationTran._Award_End_Date
                                            objEmployeeQualificationTran._Award_Start_Date = objAEmpQualificationTran._Award_Start_Date
                                            'S.SANDEEP |04-FEB-2022| -- START
                                            'objEmployeeQualificationTran._Transaction_Date = objAEmpQualificationTran._Certificatedate
                                            objEmployeeQualificationTran._Transaction_Date = objAEmpQualificationTran._Transactiondate
                                            'S.SANDEEP |04-FEB-2022| -- END

                                            objEmployeeQualificationTran._Employeeunkid = objAEmpQualificationTran._Employeeunkid
                                            objEmployeeQualificationTran._Qualificationgroupunkid = objAEmpQualificationTran._Qualificationgroupunkid
                                            objEmployeeQualificationTran._Qualificationunkid = objAEmpQualificationTran._Qualificationunkid
                                            objEmployeeQualificationTran._Reference_No = objAEmpQualificationTran._Reference_No
                                            objEmployeeQualificationTran._Remark = objAEmpQualificationTran._Remark
                                            objEmployeeQualificationTran._Instituteunkid = objAEmpQualificationTran._Instituteunkid
                                            'S.SANDEEP |04-FEB-2022| -- START
                                            'objEmployeeQualificationTran._Transaction_Date = objAEmpQualificationTran._Certificatedate
                                            objEmployeeQualificationTran._Transaction_Date = objAEmpQualificationTran._Transactiondate
                                            'S.SANDEEP |04-FEB-2022| -- END


                                            objEmployeeQualificationTran._Userunkid = User._Object._Userunkid
                                            objEmployeeQualificationTran._Isvoid = False
                                            objEmployeeQualificationTran._Voiddatetime = Nothing
                                            objEmployeeQualificationTran._Voiduserunkid = -1


                                            objEmployeeQualificationTran._Resultunkid = objAEmpQualificationTran._Resultunkid
                                            objEmployeeQualificationTran._GPAcode = objAEmpQualificationTran._Gpacode

                                            objEmployeeQualificationTran._Other_QualificationGrp = objAEmpQualificationTran._Other_Qualificationgrp
                                            objEmployeeQualificationTran._Other_Qualification = objAEmpQualificationTran._Other_Qualification
                                            objEmployeeQualificationTran._other_ResultCode = objAEmpQualificationTran._Other_Resultcode
                                            objEmployeeQualificationTran._Other_institute = objAEmpQualificationTran._Other_Institute

                                            'Gajanan [14-AUG-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            If blnIsweb Then
                                            objEmployeeQualificationTran._WebClientIP = xIPAddr
                                            objEmployeeQualificationTran._WebHostName = xHostName
                                            objEmployeeQualificationTran._WebFormName = strFormName
                                            End If
                                            objEmployeeQualificationTran._Userunkid = objAEmpQualificationTran._Audituserunkid
                                            'Gajanan [14-AUG-2019] -- End
                                        End If

                                        If objAEmpQualificationTran._Qualificationtranunkid > 0 Then
                                            Select Case objAEmpQualificationTran._OperationTypeId
                                                Case enOperationType.EDITED
                                                    If objEmployeeQualificationTran.Update(Nothing, objDataOperation) Then
                                                        If objAEmpQualificationTran.Update(objDataOperation, objAEmpQualificationTran._Employeeunkid, objAEmpQualificationTran._Instituteunkid, objAEmpQualificationTran._Award_Start_Date, objAEmpQualificationTran._Award_End_Date, objAEmpQualificationTran._Qualificationunkid, objAEmpQualificationTran._Other_Qualification, objAEmpQualificationTran._Qualificationtranunkid, "qualificationtranunkid") = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If

                                                        If objAEmpQualificationTran.Update(objDataOperation, objAEmpQualificationTran._Employeeunkid, objAEmpQualificationTran._Instituteunkid, objAEmpQualificationTran._Award_Start_Date, objAEmpQualificationTran._Award_End_Date, objAEmpQualificationTran._Qualificationunkid, objAEmpQualificationTran._Other_Qualification, 1, "isprocessed") = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If

                                                        'Gajanan [22-Feb-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        'If objAEmpQualificationTran.UpdateDeleteDocument(objDataOperation, objAEmpQualificationTran._Employeeunkid, objAEmpQualificationTran._LinkedMasterId, objAEmpQualificationTran._Form_Name, objAEmpQualificationTran._Transactiondate, objAEmpQualificationTran._Qualificationtranunkid, "transactionunkid", False) = False Then
                                                        If objAEmpQualificationTran.UpdateDeleteDocument(objDataOperation, objAEmpQualificationTran._Employeeunkid, dRow("newattachdocumentid").ToString(), enScanAttactRefId.QUALIFICATIONS, objAEmpQualificationTran._Transactiondate, objAEmpQualificationTran._Qualificationtranunkid, "transactionunkid", False) = False Then
                                                            'Gajanan [22-Feb-2019] -- End
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If


                                                        'Gajanan [22-Feb-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.

                                                        If objAEmpQualificationTran._Deletedattachdocumnetid.Length > 0 Then
                                                            If objAEmpQualificationTran.UpdateDeleteDocument(objDataOperation, objAEmpQualificationTran._Employeeunkid, dRow("deleteattachdocumentid").ToString(), enScanAttactRefId.QUALIFICATIONS, objAEmpQualificationTran._Transactiondate, objAEmpQualificationTran._Qualificationtranunkid, "transactionunkid", True) = False Then
                                                                'Gajanan [22-Feb-2019] -- End
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                    End If


                                                        'Gajanan [22-Feb-2019] -- End

                                                        'Gajanan [22-Feb-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    Else
                                                        If objEmployeeQualificationTran._Message.Trim.Length > 0 Then mstrMessage = objEmployeeQualificationTran._Message
                                                        Return False
                                                        'Gajanan [22-Feb-2019] -- End
                                                    End If
                                                Case enOperationType.DELETED
                                                    If objAEmpQualificationTran.Update(objDataOperation, objAEmpQualificationTran._Employeeunkid, objAEmpQualificationTran._Instituteunkid, objAEmpQualificationTran._Award_Start_Date, objAEmpQualificationTran._Award_End_Date, objAEmpQualificationTran._Qualificationunkid, objAEmpQualificationTran._Other_Qualification, 1, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If

                                                    'Gajanan [22-Feb-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    'If objAEmpQualificationTran.UpdateDeleteDocument(objDataOperation, objAEmpQualificationTran._Employeeunkid, objAEmpQualificationTran._LinkedMasterId, objAEmpQualificationTran._Form_Name, objAEmpQualificationTran._Transactiondate, objAEmpQualificationTran._Qualificationtranunkid, "transactionunkid", False) = False Then
                                                    If objAEmpQualificationTran.UpdateDeleteDocument(objDataOperation, objAEmpQualificationTran._Employeeunkid, dRow("newattachdocumentid").ToString(), enScanAttactRefId.QUALIFICATIONS, objAEmpQualificationTran._Transactiondate, objAEmpQualificationTran._Qualificationtranunkid, "transactionunkid", False) = False Then
                                                        'Gajanan [22-Feb-2019] -- End
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If

                                                    'Gajanan [22-Feb-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objAEmpQualificationTran.UpdateDeleteDocument(objDataOperation, objAEmpQualificationTran._Employeeunkid, dRow("deleteattachdocumentid").ToString(), enScanAttactRefId.QUALIFICATIONS, objAEmpQualificationTran._Transactiondate, objAEmpQualificationTran._Qualificationtranunkid, "transactionunkid", True) = False Then
                                                        'Gajanan [22-Feb-2019] -- End
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                    'Gajanan [22-Feb-2019] -- End

                                            End Select
                                        Else
                                            If objEmployeeQualificationTran.Insert(Nothing, objDataOperation) Then

                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                                                'If objAEmpQualificationTran.Update(objDataOperation, objAEmpQualificationTran._Employeeunkid, objAEmpQualificationTran._Instituteunkid, objAEmpQualificationTran._Award_Start_Date, objAEmpQualificationTran._Award_End_Date, objAEmpQualificationTran._Qualificationunkid, objAEmpQualificationTran._Other_Qualification, objAEmpQualificationTran._Qualificationtranunkid, "qualificationtranunkid") = False Then
                                                If objAEmpQualificationTran.Update(objDataOperation, objAEmpQualificationTran._Employeeunkid, objAEmpQualificationTran._Instituteunkid, objAEmpQualificationTran._Award_Start_Date, objAEmpQualificationTran._Award_End_Date, objAEmpQualificationTran._Qualificationunkid, objAEmpQualificationTran._Other_Qualification, objEmployeeQualificationTran._Qualificationtranunkid, "qualificationtranunkid") = False Then
                                                    'Gajanan [22-Feb-2019] -- End
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                If objAEmpQualificationTran.Update(objDataOperation, objAEmpQualificationTran._Employeeunkid, objAEmpQualificationTran._Instituteunkid, objAEmpQualificationTran._Award_Start_Date, objAEmpQualificationTran._Award_End_Date, objAEmpQualificationTran._Qualificationunkid, objAEmpQualificationTran._Other_Qualification, 1, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If


                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                                                'If objAEmpQualificationTran.UpdateDeleteDocument(objDataOperation, objAEmpQualificationTran._Employeeunkid, objAEmpQualificationTran._LinkedMasterId, objAEmpQualificationTran._Form_Name, objAEmpQualificationTran._Transactiondate, objEmployeeQualificationTran._Qualificationtranunkid, "transactionunkid", False) = False Then
                                                If objAEmpQualificationTran.UpdateDeleteDocument(objDataOperation, objAEmpQualificationTran._Employeeunkid, dRow("newattachdocumentid").ToString(), enScanAttactRefId.QUALIFICATIONS, objAEmpQualificationTran._Transactiondate, objEmployeeQualificationTran._Qualificationtranunkid, "transactionunkid", False) = False Then
                                                    'Gajanan [22-Feb-2019] -- End
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                            Else
                                                If objEmployeeQualificationTran._Message.Trim.Length > 0 Then mstrMessage = objEmployeeQualificationTran._Message
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End
                                            End If
                                        End If
                                    End If
                                Else
                                    If eStatusId = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                                        If eOprType = enOperationType.DELETED Then
                                            objEmployeeQualificationTran._Qualificationtranunkid = objAEmpQualificationTran._Qualificationtranunkid
                                            objEmployeeQualificationTran._Isvoid = False
                                            objEmployeeQualificationTran._Voidreason = ""
                                            objEmployeeQualificationTran._Voiddatetime = Nothing
                                            objEmployeeQualificationTran._Voiduserunkid = 0


                                            'Gajanan [14-AUG-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            If blnIsweb Then
                                            objEmployeeQualificationTran._WebClientIP = xIPAddr
                                            objEmployeeQualificationTran._WebHostName = xHostName
                                            objEmployeeQualificationTran._WebFormName = strFormName
                                            End If
                                            objEmployeeQualificationTran._Userunkid = objAEmpQualificationTran._Audituserunkid
                                            'Gajanan [14-AUG-2019] -- End

                                            If objEmployeeQualificationTran.Update(Nothing, objDataOperation) Then
                                                If objAEmpQualificationTran.Update(objDataOperation, objAEmpQualificationTran._Employeeunkid, objAEmpQualificationTran._Instituteunkid, objAEmpQualificationTran._Award_Start_Date, objAEmpQualificationTran._Award_End_Date, objAEmpQualificationTran._Qualificationunkid, objAEmpQualificationTran._Other_Qualification, 1, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                'If objAEmpQualificationTran.UpdateDeleteDocument(objDataOperation, objAEmpQualificationTran._Employeeunkid, objAEmpQualificationTran._LinkedMasterId, objAEmpQualificationTran._Form_Name, objAEmpQualificationTran._Transactiondate, objAEmpQualificationTran._Qualificationtranunkid, "transactionunkid", True) = False Then
                                                If objAEmpQualificationTran.UpdateDeleteDocument(objDataOperation, objAEmpQualificationTran._Employeeunkid, dRow("newattachdocumentid").ToString(), enScanAttactRefId.QUALIFICATIONS, objAEmpQualificationTran._Transactiondate, objAEmpQualificationTran._Qualificationtranunkid, "transactionunkid", True) = False Then
                                                    'Gajanan [22-Feb-2019] -- End
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If


                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                If objAEmpQualificationTran.UpdateDeleteDocument(objDataOperation, objAEmpQualificationTran._Employeeunkid, dRow("deleteattachdocumentid").ToString(), enScanAttactRefId.QUALIFICATIONS, objAEmpQualificationTran._Transactiondate, objAEmpQualificationTran._Qualificationtranunkid, "transactionunkid", False) = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                                'Gajanan [22-Feb-2019] -- End

                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                            Else
                                                If objEmployeeQualificationTran._Message.Trim.Length > 0 Then mstrMessage = objEmployeeQualificationTran._Message
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End

                                            End If
                                        Else
                                            If objAEmpQualificationTran.Update(objDataOperation, objAEmpQualificationTran._Employeeunkid, objAEmpQualificationTran._Instituteunkid, objAEmpQualificationTran._Award_Start_Date, objAEmpQualificationTran._Award_End_Date, objAEmpQualificationTran._Qualificationunkid, objAEmpQualificationTran._Other_Qualification, 1, "isprocessed") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            'Gajanan [22-Feb-2019] -- Start
                                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                            'If objAEmpQualificationTran.UpdateDeleteDocument(objDataOperation, objAEmpQualificationTran._Employeeunkid, objAEmpQualificationTran._LinkedMasterId, objAEmpQualificationTran._Form_Name, objAEmpQualificationTran._Transactiondate, objAEmpQualificationTran._Qualificationtranunkid, "transactionunkid", True) = False Then
                                            If objAEmpQualificationTran.UpdateDeleteDocument(objDataOperation, objAEmpQualificationTran._Employeeunkid, dRow("newattachdocumentid").ToString(), enScanAttactRefId.QUALIFICATIONS, objAEmpQualificationTran._Transactiondate, objAEmpQualificationTran._Qualificationtranunkid, "transactionunkid", True) = False Then
                                                'Gajanan [22-Feb-2019] -- End
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            'Gajanan [22-Feb-2019] -- Start
                                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                            If objAEmpQualificationTran.UpdateDeleteDocument(objDataOperation, objAEmpQualificationTran._Employeeunkid, dRow("deleteattachdocumentid").ToString(), enScanAttactRefId.QUALIFICATIONS, objAEmpQualificationTran._Transactiondate, objAEmpQualificationTran._Qualificationtranunkid, "transactionunkid", False) = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                            'Gajanan [22-Feb-2019] -- End
                                        End If
                                    End If

                                End If

                                'Gajanan [22-Feb-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            Else
                                If objAEmpQualificationTran._Message.Trim.Length > 0 Then mstrMessage = objAEmpQualificationTran._Message
                                Return False
                                'Gajanan [22-Feb-2019] -- End
                            End If
                        Next

                    Case enScreenName.frmJobHistory_ExperienceList
                        For Each dRow As DataRow In dtTable.Rows
                            objAEmpExperienceTran = New clsJobExperience_approval_tran
                            objEmployeeExperienceTran = New clsJobExperience_tran

                            objAEmpExperienceTran._Approvalremark = strRemark
                            objAEmpExperienceTran._Auditdatetime = Now
                            objAEmpExperienceTran._Audittype = enAuditType.ADD
                            objAEmpExperienceTran._Audituserunkid = intUserId
                            objAEmpExperienceTran._Employeeunkid = CInt(dRow("employeeunkid"))
                            objAEmpExperienceTran._Form_Name = strFormName
                            objAEmpExperienceTran._Host = xHostName
                            objAEmpExperienceTran._Ip = xIPAddr
                            objAEmpExperienceTran._Isfinal = CBool(dRow("isfinal"))
                            objAEmpExperienceTran._Isprocessed = False
                            objAEmpExperienceTran._Isvoid = False
                            objAEmpExperienceTran._Isweb = blnIsweb
                            objAEmpExperienceTran._Mappingunkid = intUserMappingTranId
                            objAEmpExperienceTran._Statusunkid = eStatusId
                            objAEmpExperienceTran._Transactiondate = Now
                            objAEmpExperienceTran._Tranguid = Guid.NewGuid.ToString()

                            objAEmpExperienceTran._Address = dRow("address").ToString
                            objAEmpExperienceTran._Company = dRow("cname").ToString.Trim
                            objAEmpExperienceTran._Contact_No = dRow("contact_no").ToString
                            objAEmpExperienceTran._Contact_Person = dRow("contact_person").ToString
                            objAEmpExperienceTran._Employeeunkid = CInt(dRow("employeeunkid"))

                            If IsDBNull(dRow("end_date")) = False AndAlso CDate(dRow("end_date")) <> Nothing Then
                                objAEmpExperienceTran._End_Date = CDate(dRow("end_date"))
                            Else
                                objAEmpExperienceTran._End_Date = Nothing
                            End If


                            objAEmpExperienceTran._Iscontact_Previous = CBool(dRow("iscontact_previous"))

                            objAEmpExperienceTran._Old_Job = dRow("old_job").ToString
                            objAEmpExperienceTran._Otherbenefit = dRow("otherbenefit").ToString
                            objAEmpExperienceTran._Currencysign = dRow("currencysign").ToString

                            objAEmpExperienceTran._Last_Pay = CDec(dRow("last_pay"))
                            objAEmpExperienceTran._Leave_Reason = dRow("leave_reason").ToString
                            objAEmpExperienceTran._Memo = dRow("memo").ToString
                            objAEmpExperienceTran._Remark = dRow("remark").ToString

                            If IsDBNull(dRow("start_date")) = False AndAlso CDate(dRow("start_date")) <> Nothing Then
                                objAEmpExperienceTran._Start_Date = CDate(dRow("start_date"))
                            Else
                                objAEmpExperienceTran._Start_Date = Nothing
                            End If
                            objAEmpExperienceTran._Supervisor = dRow("supervisor").ToString
                            objAEmpExperienceTran._PreviousBenefitPlan = dRow("benifitplanunkids").ToString

                            'Gajanan [5-Dec-2019] -- Start   
                            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                            objAEmpExperienceTran._Newattacheddocumnetid = dRow("newattachdocumentid").ToString()
                            objAEmpExperienceTran._Deletedattachdocumnetid = dRow("deleteattachdocumentid").ToString()
                            'Gajanan [5-Dec-2019] -- End

                            objAEmpExperienceTran._OperationTypeId = eOprType

                            If eOprType = enOperationType.EDITED Or eOprType = enOperationType.DELETED Then
                                objAEmpExperienceTran._Experiencetranunkid = CInt(dRow("Experiencetranunkid"))
                            End If


                            If objAEmpExperienceTran.Insert(intCompanyId, objDataOperation, True) Then
                                If eStatusId = clsEmployee_Master.EmpApprovalStatus.Approved Then
                                    If CBool(dRow("isfinal")) Then
                                        If eOprType <> enOperationType.DELETED Then

                                            objEmployeeExperienceTran._Experiencetranunkid = objAEmpExperienceTran._Experiencetranunkid
                                            objEmployeeExperienceTran._Address = objAEmpExperienceTran._Address
                                            objEmployeeExperienceTran._Company = objAEmpExperienceTran._Company.Trim
                                            objEmployeeExperienceTran._Contact_No = objAEmpExperienceTran._Contact_No
                                            objEmployeeExperienceTran._Contact_Person = objAEmpExperienceTran._Contact_Person
                                            objEmployeeExperienceTran._Employeeunkid = objAEmpExperienceTran._Employeeunkid
                                            objEmployeeExperienceTran._End_Date = objAEmpExperienceTran._End_Date
                                            objEmployeeExperienceTran._Iscontact_Previous = objAEmpExperienceTran._Iscontact_Previous
                                            objEmployeeExperienceTran._Job = objAEmpExperienceTran._Old_Job
                                            objEmployeeExperienceTran._OtherBenefit = objAEmpExperienceTran._Otherbenefit
                                            objEmployeeExperienceTran._CurrencySign = objAEmpExperienceTran._Currencysign

                                            objEmployeeExperienceTran._Last_Pay = objAEmpExperienceTran._Last_Pay
                                            objEmployeeExperienceTran._Leave_Reason = objAEmpExperienceTran._Leave_Reason
                                            objEmployeeExperienceTran._Memo = objAEmpExperienceTran._Memo
                                            objEmployeeExperienceTran._Remark = objAEmpExperienceTran._Remark
                                            objEmployeeExperienceTran._Start_Date = objAEmpExperienceTran._Start_Date
                                            objEmployeeExperienceTran._Supervisor = objAEmpExperienceTran._Supervisor
                                            objEmployeeExperienceTran._PreviousBenefit = objAEmpExperienceTran._PreviousBenefitPlan

                                            objEmployeeExperienceTran._Userunkid = objAEmpExperienceTran._Audituserunkid
                                            objEmployeeExperienceTran._Voidatetime = Nothing
                                            objEmployeeExperienceTran._VoidLoginEmployeeunkid = 0
                                            objEmployeeExperienceTran._Voidreason = ""
                                            objEmployeeExperienceTran._Voiduserunkid = -1


                                            'Gajanan [14-AUG-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            If blnIsweb Then
                                            objEmployeeExperienceTran._WebClientIP = xIPAddr
                                            objEmployeeExperienceTran._WebFormName = strFormName
                                            objEmployeeExperienceTran._WebHostName = xHostName
                                            End If
                                            objEmployeeExperienceTran._Userunkid = objAEmpExperienceTran._Audituserunkid
                                            'Gajanan [14-AUG-2019] -- End


                                        End If

                                        If objAEmpExperienceTran._Experiencetranunkid > 0 Then
                                            Select Case objAEmpExperienceTran._OperationTypeId
                                                Case enOperationType.EDITED
                                                    If objEmployeeExperienceTran.Update(objDataOperation) Then
                                                        If objAEmpExperienceTran.Update(objDataOperation, objAEmpExperienceTran._Employeeunkid, objAEmpExperienceTran._Company, objAEmpExperienceTran._Start_Date, objAEmpExperienceTran._End_Date, objAEmpExperienceTran._Old_Job, objAEmpExperienceTran._Experiencetranunkid, "experiencetranunkid") = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If

                                                        If objAEmpExperienceTran.Update(objDataOperation, objAEmpExperienceTran._Employeeunkid, objAEmpExperienceTran._Company, objAEmpExperienceTran._Start_Date, objAEmpExperienceTran._End_Date, objAEmpExperienceTran._Old_Job, 1, "isprocessed") = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If


                                                        'Gajanan [5-Dec-2019] -- Start   
                                                        'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                                                        If objAEmpExperienceTran.UpdateDeleteDocument(objDataOperation, objAEmpExperienceTran._Employeeunkid, dRow("newattachdocumentid").ToString(), enScanAttactRefId.JOBHISTORYS, objAEmpExperienceTran._Transactiondate, objAEmpExperienceTran._Experiencetranunkid, "transactionunkid", False) = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If

                                                        If objAEmpExperienceTran._Deletedattachdocumnetid.Length > 0 Then
                                                            If objAEmpExperienceTran.UpdateDeleteDocument(objDataOperation, objAEmpExperienceTran._Employeeunkid, dRow("deleteattachdocumentid").ToString(), enScanAttactRefId.JOBHISTORYS, objAEmpExperienceTran._Transactiondate, objAEmpExperienceTran._Experiencetranunkid, "transactionunkid", True) = False Then
                                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                                Throw exForce
                                                            End If
                                                        End If
                                                        'Gajanan [5-Dec-2019] -- End


                                                        'Gajanan [22-Feb-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        'Else
                                                        '    If objEmployeeExperienceTran._Message <> "" Then mstrMessage = objEmployeeExperienceTran._Message
                                                    Else
                                                        If objEmployeeExperienceTran._Message.Trim.Length > 0 Then mstrMessage = objEmployeeExperienceTran._Message
                                                        Return False
                                                        'Gajanan [22-Feb-2019] -- End
                                                    End If
                                                Case enOperationType.DELETED
                                                    If objAEmpExperienceTran.Update(objDataOperation, objAEmpExperienceTran._Employeeunkid, objAEmpExperienceTran._Company, objAEmpExperienceTran._Start_Date, objAEmpExperienceTran._End_Date, objAEmpExperienceTran._Old_Job, 1, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If


                                                    'Gajanan [5-Dec-2019] -- Start   
                                                    'Enhancement:Worked On ADD Attachment In Bio Data Experience    

                                                    'If objEmployeeExperienceTran.Update(objDataOperation) = False Then
                                                    '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    '    Throw exForce
                                                    'End If

                                                    If objAEmpExperienceTran.UpdateDeleteDocument(objDataOperation, objAEmpExperienceTran._Employeeunkid, dRow("newattachdocumentid").ToString(), enScanAttactRefId.JOBHISTORYS, objAEmpExperienceTran._Transactiondate, objAEmpExperienceTran._Experiencetranunkid, "transactionunkid", False) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If

                                                    If objAEmpExperienceTran.UpdateDeleteDocument(objDataOperation, objAEmpExperienceTran._Employeeunkid, dRow("deleteattachdocumentid").ToString(), enScanAttactRefId.JOBHISTORYS, objAEmpExperienceTran._Transactiondate, objAEmpExperienceTran._Experiencetranunkid, "transactionunkid", True) = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                                    'Gajanan [5-Dec-2019] -- End

                                            End Select
                                        Else
                                            If objEmployeeExperienceTran.Insert(objDataOperation) Then

                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                                                'If objAEmpExperienceTran.Update(objDataOperation, objAEmpExperienceTran._Employeeunkid, objAEmpExperienceTran._Company, objAEmpExperienceTran._Start_Date, objAEmpExperienceTran._End_Date, objAEmpExperienceTran._Old_Job, objAEmpExperienceTran._Experiencetranunkid, "experiencetranunkid") = False Then
                                                If objAEmpExperienceTran.Update(objDataOperation, objAEmpExperienceTran._Employeeunkid, objAEmpExperienceTran._Company, objAEmpExperienceTran._Start_Date, objAEmpExperienceTran._End_Date, objAEmpExperienceTran._Old_Job, objEmployeeExperienceTran._Experiencetranunkid, "experiencetranunkid") = False Then
                                                    'Gajanan [22-Feb-2019] -- End
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                If objAEmpExperienceTran.Update(objDataOperation, objAEmpExperienceTran._Employeeunkid, objAEmpExperienceTran._Company, objAEmpExperienceTran._Start_Date, objAEmpExperienceTran._End_Date, objAEmpExperienceTran._Old_Job, 1, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If


                                                'Gajanan [5-Dec-2019] -- Start   
                                                'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                                                If objAEmpExperienceTran.UpdateDeleteDocument(objDataOperation, objAEmpExperienceTran._Employeeunkid, dRow("newattachdocumentid").ToString(), enScanAttactRefId.JOBHISTORYS, objAEmpExperienceTran._Transactiondate, objEmployeeExperienceTran._Experiencetranunkid, "transactionunkid", False) = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                                'Gajanan [5-Dec-2019] -- End


                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                            Else
                                                If objEmployeeExperienceTran._Message.Trim.Length > 0 Then mstrMessage = objEmployeeExperienceTran._Message
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End
                                            End If
                                        End If


                                    End If
                                Else
                                    If eStatusId = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                                        If eOprType = enOperationType.DELETED Then
                                            objEmployeeExperienceTran._Experiencetranunkid = objAEmpExperienceTran._Experiencetranunkid
                                            objEmployeeExperienceTran._Isvoid = False
                                            objEmployeeExperienceTran._Voidreason = ""
                                            objEmployeeExperienceTran._Voidatetime = Nothing
                                            objEmployeeExperienceTran._Voiduserunkid = 0

                                            'Gajanan [14-AUG-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            If blnIsweb Then
                                            objEmployeeExperienceTran._WebClientIP = xIPAddr
                                            objEmployeeExperienceTran._WebFormName = strFormName
                                            objEmployeeExperienceTran._WebHostName = xHostName
                                            End If
                                            objEmployeeExperienceTran._Userunkid = objAEmpExperienceTran._Audituserunkid
                                            'Gajanan [14-AUG-2019] -- End

                                            If objEmployeeExperienceTran.Update(objDataOperation) Then
                                                If objAEmpExperienceTran.Update(objDataOperation, objAEmpExperienceTran._Employeeunkid, objAEmpExperienceTran._Company, objAEmpExperienceTran._Start_Date, objAEmpExperienceTran._End_Date, objAEmpExperienceTran._Old_Job, 1, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If


                                                'Gajanan [5-Dec-2019] -- Start   
                                                'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                                                If objAEmpExperienceTran.UpdateDeleteDocument(objDataOperation, objAEmpExperienceTran._Employeeunkid, dRow("newattachdocumentid").ToString(), enScanAttactRefId.JOBHISTORYS, objAEmpExperienceTran._Transactiondate, objAEmpExperienceTran._Experiencetranunkid, "transactionunkid", True) = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                If objAEmpExperienceTran.UpdateDeleteDocument(objDataOperation, objAEmpExperienceTran._Employeeunkid, dRow("deleteattachdocumentid").ToString(), enScanAttactRefId.JOBHISTORYS, objAEmpExperienceTran._Transactiondate, objAEmpExperienceTran._Experiencetranunkid, "transactionunkid", False) = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                                'Gajanan [5-Dec-2019] -- End


                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                            Else
                                                If objEmployeeExperienceTran._Message.Trim.Length > 0 Then mstrMessage = objEmployeeExperienceTran._Message
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End
                                            End If
                                        Else
                                            If objAEmpExperienceTran.Update(objDataOperation, objAEmpExperienceTran._Employeeunkid, objAEmpExperienceTran._Company, objAEmpExperienceTran._Start_Date, objAEmpExperienceTran._End_Date, objAEmpExperienceTran._Old_Job, 1, "isprocessed") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If


                                            'Gajanan [5-Dec-2019] -- Start   
                                            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                                            If objAEmpExperienceTran.UpdateDeleteDocument(objDataOperation, objAEmpExperienceTran._Employeeunkid, dRow("newattachdocumentid").ToString(), enScanAttactRefId.JOBHISTORYS, objAEmpExperienceTran._Transactiondate, objAEmpExperienceTran._Experiencetranunkid, "transactionunkid", True) = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If

                                            If objAEmpExperienceTran.UpdateDeleteDocument(objDataOperation, objAEmpExperienceTran._Employeeunkid, dRow("deleteattachdocumentid").ToString(), enScanAttactRefId.JOBHISTORYS, objAEmpExperienceTran._Transactiondate, objAEmpExperienceTran._Experiencetranunkid, "transactionunkid", False) = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                            'Gajanan [5-Dec-2019] -- End

                                        End If
                                    End If

                                End If
                                       
                                'Gajanan [22-Feb-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            Else
                                If objAEmpExperienceTran._Message.Trim.Length > 0 Then mstrMessage = objAEmpExperienceTran._Message
                                Return False
                                'Gajanan [22-Feb-2019] -- End
                            End If
                        Next

                    Case enScreenName.frmEmployeeRefereeList
                        For Each dRow As DataRow In dtTable.Rows
                            objAEmpRefreeTran = New clsemployee_refree_approval_tran
                            objEmployeeRefreeTran = New clsEmployee_Refree_tran

                            objAEmpRefreeTran._Approvalremark = strRemark
                            objAEmpRefreeTran._Audittype = enAuditType.ADD
                            objAEmpRefreeTran._Audituserunkid = intUserId
                            objAEmpRefreeTran._Form_Name = strFormName
                            objAEmpRefreeTran._Host = xHostName
                            objAEmpRefreeTran._Ip = xIPAddr
                            objAEmpRefreeTran._Isfinal = CBool(dRow("isfinal"))
                            objAEmpRefreeTran._Isprocess = False
                            objAEmpRefreeTran._Isvoid = False
                            objAEmpRefreeTran._Isweb = blnIsweb
                            objAEmpRefreeTran._Mappingunkid = intUserMappingTranId
                            objAEmpRefreeTran._Statusunkid = eStatusId
                            objAEmpRefreeTran._Transactiondate = Now
                            objAEmpRefreeTran._Tranguid = Guid.NewGuid.ToString()
                            objAEmpRefreeTran._Transactiondate = Now
                            objAEmpRefreeTran._Host = getHostName()

                            objAEmpRefreeTran._Address = dRow("address").ToString
                            objAEmpRefreeTran._Cityunkid = CInt(dRow("Cityunkid"))
                            objAEmpRefreeTran._Countryunkid = CInt(dRow("countryunkid"))
                            objAEmpRefreeTran._Email = dRow("Email").ToString
                            objAEmpRefreeTran._Employeeunkid = CInt(dRow("employeeunkid"))
                            objAEmpRefreeTran._Gender = CInt(dRow("employeeunkid"))
                            objAEmpRefreeTran._Company = dRow("Company").ToString
                            objAEmpRefreeTran._Mobile_No = dRow("mobile_no").ToString
                            objAEmpRefreeTran._Name = dRow("name").ToString
                            objAEmpRefreeTran._Stateunkid = CInt(dRow("Stateunkid"))
                            objAEmpRefreeTran._Telephone_No = dRow("telephone_no").ToString
                            objAEmpRefreeTran._Relationunkid = CInt(dRow("relationunkid"))
                            objAEmpRefreeTran._Ref_Position = dRow("Ref_Position").ToString
                            objAEmpRefreeTran._OperationTypeId = eOprType

                            If eOprType = enOperationType.EDITED Or eOprType = enOperationType.DELETED Then
                                objAEmpRefreeTran._Refereetranunkid = CInt(dRow("Refereetranunkid"))
                            End If

                            If objAEmpRefreeTran.Insert(intCompanyId, objDataOperation, True) Then
                                If eStatusId = clsEmployee_Master.EmpApprovalStatus.Approved Then
                                    If CBool(dRow("isfinal")) Then
                                        If eOprType <> enOperationType.DELETED Then
                                            objEmployeeRefreeTran._Refereetranunkid = objAEmpRefreeTran._Refereetranunkid
                                            objEmployeeRefreeTran._Address = objAEmpRefreeTran._Address
                                            objEmployeeRefreeTran._Cityunkid = objAEmpRefreeTran._Cityunkid
                                            objEmployeeRefreeTran._Countryunkid = objAEmpRefreeTran._Countryunkid
                                            objEmployeeRefreeTran._Email = objAEmpRefreeTran._Email
                                            objEmployeeRefreeTran._Employeeunkid = objAEmpRefreeTran._Employeeunkid
                                            objEmployeeRefreeTran._Gender = objAEmpRefreeTran._Gender
                                            objEmployeeRefreeTran._Company = objAEmpRefreeTran._Company
                                            objEmployeeRefreeTran._Mobile_No = objAEmpRefreeTran._Mobile_No
                                            objEmployeeRefreeTran._Name = objAEmpRefreeTran._Name
                                            objEmployeeRefreeTran._Stateunkid = objAEmpRefreeTran._Stateunkid
                                            objEmployeeRefreeTran._Telephone_No = objAEmpRefreeTran._Telephone_No
                                            objEmployeeRefreeTran._Userunkid = objAEmpRefreeTran._Audituserunkid
                                            objEmployeeRefreeTran._Voiddatetime = Nothing
                                            objEmployeeRefreeTran._VoidLoginEmployeeunkid = 0
                                            objEmployeeRefreeTran._Voidreason = ""
                                            objEmployeeRefreeTran._Voiduserunkid = -1
                                            objEmployeeRefreeTran._Relationunkid = objAEmpRefreeTran._Relationunkid
                                            objEmployeeRefreeTran._Ref_Position = objAEmpRefreeTran._Ref_Position


                                            'Gajanan [22-Feb-2019] -- Start
                                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                            If blnIsweb Then
                                            objEmployeeRefreeTran._WebClientIP = xIPAddr
                                            objEmployeeRefreeTran._WebHostName = xHostName
                                            objEmployeeRefreeTran._WebFormName = strFormName
                                            End If
                                            objEmployeeRefreeTran._Userunkid = objAEmpRefreeTran._Audituserunkid
                                            'Gajanan [25-May-2019] -- End

                                        End If

                                        If objAEmpRefreeTran._Refereetranunkid > 0 Then
                                            Select Case objAEmpRefreeTran._OperationTypeId
                                                Case enOperationType.EDITED
                                                    If objEmployeeRefreeTran.Update(objDataOperation) Then
                                                        If objAEmpRefreeTran.Update(objDataOperation, objAEmpRefreeTran._Employeeunkid, objAEmpRefreeTran._Refereetranunkid, objAEmpRefreeTran._Name, "Refereetranunkid") = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If

                                                        If objAEmpRefreeTran.Update(objDataOperation, objAEmpRefreeTran._Employeeunkid, 1, objAEmpRefreeTran._Name, "isprocessed") = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                        'Gajanan [22-Feb-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    Else
                                                        If objEmployeeRefreeTran._Message.Trim.Length > 0 Then mstrMessage = objEmployeeRefreeTran._Message
                                                        Return False
                                                        'Gajanan [22-Feb-2019] -- End
                                                    End If
                                                Case enOperationType.DELETED
                                                    If objAEmpRefreeTran.Update(objDataOperation, objAEmpRefreeTran._Employeeunkid, 1, objAEmpRefreeTran._Name, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                            End Select
                                        Else
                                            If objEmployeeRefreeTran.Insert(objDataOperation) Then

                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                'If objAEmpRefreeTran.Update(objDataOperation, objAEmpRefreeTran._Employeeunkid, objAEmpRefreeTran._Refereetranunkid, objAEmpRefreeTran._Name, "refereetranunkid") = False Then
                                                If objAEmpRefreeTran.Update(objDataOperation, objAEmpRefreeTran._Employeeunkid, objEmployeeRefreeTran._Refereetranunkid, objAEmpRefreeTran._Name, "refereetranunkid") = False Then
                                                    'Gajanan [22-Feb-2019] -- End
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                If objAEmpRefreeTran.Update(objDataOperation, objAEmpRefreeTran._Employeeunkid, 1, objAEmpRefreeTran._Name, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                            Else
                                                If objEmployeeRefreeTran._Message.Trim.Length > 0 Then mstrMessage = objEmployeeRefreeTran._Message
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End
                                            End If
                                        End If
                                    End If
                                Else
                                    If eStatusId = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                                        If eOprType = enOperationType.DELETED Then
                                            objEmployeeRefreeTran._Refereetranunkid = objAEmpRefreeTran._Refereetranunkid
                                            objEmployeeRefreeTran._Isvoid = False
                                            objEmployeeRefreeTran._Voidreason = ""
                                            objEmployeeRefreeTran._Voiddatetime = Nothing
                                            objEmployeeRefreeTran._Voiduserunkid = 0


                                            'Gajanan [14-AUG-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            If blnIsweb Then
                                            objEmployeeRefreeTran._WebClientIP = xIPAddr
                                            objEmployeeRefreeTran._WebHostName = xHostName
                                                objEmployeeRefreeTran._WebFormName = strFormName
                                            End If
                                            objEmployeeRefreeTran._Userunkid = objAEmpRefreeTran._Audituserunkid
                                            'Gajanan [14-AUG-2019] -- End



                                            If objEmployeeRefreeTran.Update(objDataOperation) Then
                                                If objAEmpRefreeTran.Update(objDataOperation, objAEmpRefreeTran._Employeeunkid, 1, objAEmpRefreeTran._Name, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                            Else
                                                If objEmployeeRefreeTran._Message.Trim.Length > 0 Then mstrMessage = objEmployeeRefreeTran._Message
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End
                                            End If
                                        Else
                                            If objAEmpRefreeTran.Update(objDataOperation, objAEmpRefreeTran._Employeeunkid, 1, objAEmpRefreeTran._Name, "isprocessed") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                        End If
                                    End If
                                End If
                                'Gajanan [22-Feb-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            Else
                                If objAEmpRefreeTran._Message.Trim.Length > 0 Then mstrMessage = objAEmpRefreeTran._Message
                                Return False
                                'Gajanan [22-Feb-2019] -- End
                            End If

                        Next


                    Case enScreenName.frmEmployee_Skill_List
                        For Each dRow As DataRow In dtTable.Rows
                            objAEmpSkillTran = New clsEmployeeSkill_Approval_Tran
                            objEmployeeSkillTran = New clsEmployee_Skill_Tran

                            objAEmpSkillTran._Approvalremark = strRemark
                            objAEmpSkillTran._Auditdatetime = Now
                            objAEmpSkillTran._Audittype = enAuditType.ADD
                            objAEmpSkillTran._Audituserunkid = intUserId
                            objAEmpSkillTran._Description = dRow("description").ToString
                            objAEmpSkillTran._Emp_App_Unkid = CInt(dRow("employeeunkid"))
                            objAEmpSkillTran._Form_Name = strFormName
                            objAEmpSkillTran._Host = xHostName
                            objAEmpSkillTran._Ip = xIPAddr
                            objAEmpSkillTran._Isapplicant = False
                            objAEmpSkillTran._Isfinal = CBool(dRow("isfinal"))
                            objAEmpSkillTran._Isprocessed = False
                            objAEmpSkillTran._Isvoid = False
                            objAEmpSkillTran._Isweb = blnIsweb
                            objAEmpSkillTran._Mappingunkid = intUserMappingTranId
                            objAEmpSkillTran._Skillcategoryunkid = CInt(dRow("skillcategoryunkid"))
                            objAEmpSkillTran._Skillunkid = CInt(dRow("skillunkid"))
                            objAEmpSkillTran._Statusunkid = eStatusId
                            objAEmpSkillTran._Transactiondate = Now
                            objAEmpSkillTran._Tranguid = Guid.NewGuid.ToString()
                            objAEmpSkillTran._OperationTypeId = eOprType
                            'Pinkal (24-Sep-2020) -- Start
                            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
                            objAEmpSkillTran._Other_SkillCategory = dRow("other_skillcategory").ToString()
                            objAEmpSkillTran._Other_Skill = dRow("other_skill").ToString()
                            objAEmpSkillTran._SkillExpertiseunkid = CInt(dRow("skillexpertiseunkid"))
                            'Pinkal (24-Sep-2020) -- End

                            If eOprType = enOperationType.EDITED Or eOprType = enOperationType.DELETED Then
                                objAEmpSkillTran._Skillstranunkid = CInt(dRow("Skillstranunkid"))
                            End If

                            If objAEmpSkillTran.Insert(intCompanyId, objDataOperation, True) Then
                                If eStatusId = clsEmployee_Master.EmpApprovalStatus.Approved Then
                                    If CBool(dRow("isfinal")) Then
                                        If eOprType <> enOperationType.DELETED Then
                                            objEmployeeSkillTran._Skillstranunkid = objAEmpSkillTran._Skillstranunkid
                                            objEmployeeSkillTran._Description = objAEmpSkillTran._Description
                                            objEmployeeSkillTran._Emp_App_Unkid = objAEmpSkillTran._Emp_App_Unkid
                                            objEmployeeSkillTran._Isapplicant = objAEmpSkillTran._Isapplicant
                                            objEmployeeSkillTran._Isvoid = False
                                            objEmployeeSkillTran._Loginemployeeunkid = objAEmpSkillTran._LoginEmployeeUnkid
                                            objEmployeeSkillTran._Skillcategoryunkid = objAEmpSkillTran._Skillcategoryunkid
                                            objEmployeeSkillTran._Skillunkid = objAEmpSkillTran._Skillunkid
                                            objEmployeeSkillTran._Userunkid = objAEmpSkillTran._Audituserunkid
                                            objEmployeeSkillTran._Voiddatetime = Nothing
                                            objEmployeeSkillTran._Voidloginemployeeunkid = 0
                                            objEmployeeSkillTran._Voidreason = ""
                                            objEmployeeSkillTran._Voiduserunkid = -1
                                            objEmployeeSkillTran._WebClientIP = xIPAddr
                                            objEmployeeSkillTran._WebFormName = strFormName
                                            objEmployeeSkillTran._WebHostName = xHostName

                                            'Pinkal (24-Sep-2020) -- Start
                                            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
                                            objEmployeeSkillTran._Other_SkillCategory = objAEmpSkillTran._Other_SkillCategory
                                            objEmployeeSkillTran._Other_Skill = objAEmpSkillTran._Other_Skill
                                            objEmployeeSkillTran._SkillExpertiseunkid = objAEmpSkillTran._SkillExpertiseunkid
                                            'Pinkal (24-Sep-2020) -- End

                                        End If

                                        If objAEmpSkillTran._Skillstranunkid > 0 Then
                                            Select Case objAEmpSkillTran._OperationTypeId
                                                Case enOperationType.EDITED
                                                    If objEmployeeSkillTran.Update(objDataOperation) Then
                                                        If objAEmpSkillTran.Update(objDataOperation, objAEmpSkillTran._Emp_App_Unkid, objAEmpSkillTran._Skillstranunkid, objAEmpSkillTran._Skillunkid, "skillstranunkid") = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If

                                                        If objAEmpSkillTran.Update(objDataOperation, objAEmpSkillTran._Emp_App_Unkid, 1, objAEmpSkillTran._Skillunkid, "isprocessed") = False Then
                                                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                            Throw exForce
                                                        End If
                                                        'Gajanan [22-Feb-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    Else
                                                        If objEmployeeSkillTran._Message.Trim.Length > 0 Then mstrMessage = objEmployeeSkillTran._Message
                                                        Return False
                                                        'Gajanan [22-Feb-2019] -- End
                                                    End If
                                                Case enOperationType.DELETED
                                                    If objAEmpSkillTran.Update(objDataOperation, objAEmpSkillTran._Emp_App_Unkid, 1, objAEmpSkillTran._Skillunkid, "isprocessed") = False Then
                                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If
                                            End Select
                                        Else
                                            If objEmployeeSkillTran.Insert(objDataOperation) Then

                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                'If objAEmpSkillTran.Update(objDataOperation, objAEmpSkillTran._Emp_App_Unkid, objEmployeeSkillTran._Skillstranunkid, objAEmpSkillTran._Skillunkid, "skillstranunkid") = False Then
                                                If objAEmpSkillTran.Update(objDataOperation, objAEmpSkillTran._Emp_App_Unkid, objEmployeeSkillTran._Skillstranunkid, objEmployeeSkillTran._Skillunkid, "skillstranunkid") = False Then
                                                    'Gajanan [22-Feb-2019] -- End
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If

                                                If objAEmpSkillTran.Update(objDataOperation, objAEmpSkillTran._Emp_App_Unkid, 1, objAEmpSkillTran._Skillunkid, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                            Else
                                                If objEmployeeSkillTran._Message.Trim.Length > 0 Then mstrMessage = objEmployeeSkillTran._Message
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End
                                            End If
                                        End If
                                    End If
                                Else
                                    If eStatusId = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                                        If eOprType = enOperationType.DELETED Then
                                            objEmployeeSkillTran._Skillstranunkid = objAEmpSkillTran._Skillstranunkid
                                            objEmployeeSkillTran._Isvoid = False
                                            objEmployeeSkillTran._Voidreason = ""
                                            objEmployeeSkillTran._Voiddatetime = Nothing
                                            objEmployeeSkillTran._Voiduserunkid = 0

                                            'Gajanan [14-AUG-2019] -- Start      
                                            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                                            If blnIsweb Then
                                            objEmployeeSkillTran._WebClientIP = xIPAddr
                                            objEmployeeSkillTran._WebFormName = strFormName
                                            objEmployeeSkillTran._WebHostName = xHostName
                                            End If
                                            objEmployeeSkillTran._Userunkid = objAEmpSkillTran._Audituserunkid
                                            'Gajanan [14-AUG-2019] -- End




                                            If objEmployeeSkillTran.Update(objDataOperation) Then
                                                If objAEmpSkillTran.Update(objDataOperation, objAEmpSkillTran._Emp_App_Unkid, 1, objAEmpSkillTran._Skillunkid, "isprocessed") = False Then
                                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                    Throw exForce
                                                End If
                                                'Gajanan [22-Feb-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                            Else
                                                If objEmployeeSkillTran._Message.Trim.Length > 0 Then mstrMessage = objEmployeeSkillTran._Message
                                                Return False
                                                'Gajanan [22-Feb-2019] -- End
                                            End If
                                        Else
                                            If objAEmpSkillTran.Update(objDataOperation, objAEmpSkillTran._Emp_App_Unkid, 1, objAEmpSkillTran._Skillunkid, "isprocessed") = False Then
                                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                                Throw exForce
                                            End If
                                        End If
                                    End If
                                End If

                                'Gajanan [22-Feb-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            Else
                                If objAEmpSkillTran._Message.Trim.Length > 0 Then mstrMessage = objAEmpSkillTran._Message
                                Return False
                                'Gajanan [22-Feb-2019] -- End
                            End If

                        Next
                End Select
            End If
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
        Finally
            objAEmpSkillTran = Nothing 'TODO --> DISPOSE ALL APPROVAL CLASS OBJECT
            objEmployeeSkillTran = Nothing 'TODO --> DISPOSE ALL MAIN CLASS OBJECT
        End Try
    End Function


    'Gajanan [9-April-2019] -- Start


    'Private Sub SetEmailBody(ByRef StrMessage As System.Text.StringBuilder, _
    '                         ByVal eScreenType As enScreenName, _
    '                         ByVal intNotificationType As Integer, _
    '                         ByVal dr() As DataRow, _
    '                         ByVal username As String, _
    '                         ByVal strMainText As String, _
    '                         ByVal strRejectionRemark As String, _
    '                         ByVal blnRejection As Boolean)
    '    Try
    '        StrMessage.Append("<HTML><BODY>")
    '        StrMessage.Append(vbCrLf)
    '        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

    '        'Gajanan [27-Mar-2019] -- Start
    '        'Enhancement - Change Email Language
    '        'StrMessage.Append("Dear <b>" & username & "</b></span></p>")
    '        StrMessage.Append(Language.getMessage("frmEmployeeMaster", 147, "Dear") & " " & "<b>" & getTitleCase(username) & "</b></span></p>")
    '        'Gajanan [27-Mar-2019] -- End



    '        StrMessage.Append(vbCrLf)
    '        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

    '        'Gajanan [27-Mar-2019] -- Start
    '        'Enhancement - Change Email Language
    '        'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & strMainText & " </span></p>")
    '        StrMessage.Append(strMainText & " </span></p>")
    '        'Gajanan [27-Mar-2019] -- End

    '        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
    '        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

    '        'Gajanan [27-Mar-2019] -- Start
    '        'Enhancement - Change Email Language
    '        StrMessage.Append("<TABLE border = '1'>")
    '        'Gajanan [27-Mar-2019] -- End

    '        StrMessage.Append(vbCrLf)
    '        StrMessage.Append("<TR bgcolor= 'SteelBlue'>")
    '        StrMessage.Append(vbCrLf)
    '        StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 4, "Employee") & "</span></b></TD>")
    '        Select Case eScreenType
    '            Case enScreenName.frmQualificationsList

    '            Case enScreenName.frmJobHistory_ExperienceList

    '            Case enScreenName.frmEmployeeRefereeList

    '            Case enScreenName.frmEmployee_Skill_List
    '                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 14, "Skill Category") & "</span></b></TD>")
    '                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 15, "Skill") & "</span></b></TD>")
    '                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 16, "Description") & "</span></b></TD>")
    '        End Select
    '        StrMessage.Append(vbCrLf)
    '        StrMessage.Append("</TR>")
    '        For idx As Integer = 0 To dr.Length - 1
    '            StrMessage.Append("<TR>" & vbCrLf)
    '            StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:20%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & dr(idx)("employee").ToString & "</span></TD>")
    '            Select Case eScreenType
    '                Case enScreenName.frmQualificationsList

    '                Case enScreenName.frmJobHistory_ExperienceList

    '                Case enScreenName.frmEmployeeRefereeList

    '                Case enScreenName.frmEmployee_Skill_List
    '                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & dr(idx)("scategory").ToString() & "</span></b></TD>")
    '                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & dr(idx)("skill").ToString() & "</span></b></TD>")
    '                    StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & dr(idx)("description").ToString() & "</span></b></TD>")
    '            End Select
    '            StrMessage.Append("</TR>" & vbCrLf)
    '        Next
    '        StrMessage.Append("</TABLE>")
    '        If strRejectionRemark.Trim.Length > 0 Then
    '            If blnRejection Then
    '                StrMessage.Append("<p><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & Language.getMessage(mstrModuleName, 2, "Rejection Remark:") & " " & strRejectionRemark & " " & "</span></b>")
    '            Else
    '                StrMessage.Append("<p><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & Language.getMessage(mstrModuleName, 3, "Approval Remark:") & " " & strRejectionRemark & " " & "</span></b>")
    '            End If
    '            StrMessage.Append("</span></b></p>")
    '        End If

    '        'Gajanan [27-Mar-2019] -- Start
    '        'Enhancement - Change Email Language
    '        'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
    '        StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
    '        'Gajanan [27-Mar-2019] -- End

    '        StrMessage.Append("</span></p>")
    '        StrMessage.Append("</BODY></HTML>")
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: SetEmailBody; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Sub


    'Gajanan [27-May-2019] -- Start              



    Private Sub SetEmailBodyForImportData(ByRef StrMessage As System.Text.StringBuilder, _
                            ByVal eScreenType As enScreenName, _
                            ByVal intNotificationType As Integer, _
                            ByVal DtTable As DataTable, _
                            ByVal username As String, _
                            ByVal Oprationtype As enOperationType, _
                            ByVal eToemailtype As enToEmailType, _
                            Optional ByVal enaddressType As Integer = 0, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                            Optional ByVal isfromapproval As Boolean = False)
        Try
            Dim HeaderStrMsg As New System.Text.StringBuilder
            Dim BodyStrMsg As New System.Text.StringBuilder
            Dim mstrscreenname As String = String.Empty
            Dim IsChangeExist As Boolean = False


            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
            End If

            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            Select Case eScreenType
                Case enScreenName.frmEmployee_Skill_List
                    mstrscreenname = Language.getMessage(mstrModuleName, 60, "Skill")


                Case enScreenName.frmDependantsAndBeneficiariesList
                    mstrscreenname = Language.getMessage(mstrModuleName, 66, "Dependant And Benifits")

                Case enScreenName.frmEmployeeRefereeList
                    mstrscreenname = Language.getMessage(mstrModuleName, 61, "Referee")


                Case enScreenName.frmQualificationsList
                    mstrscreenname = Language.getMessage(mstrModuleName, 786, "Qualification")


                Case enScreenName.frmJobHistory_ExperienceList
                    mstrscreenname = Language.getMessage(mstrModuleName, 64, "Experience")


                Case enScreenName.frmAddressList

                    If enaddressType = CInt(clsEmployeeAddress_approval_tran.enAddressType.PRESENT) Then
                        mstrscreenname = Language.getMessage(mstrModuleName, 901, "Present Address")
                    ElseIf enaddressType = CInt(clsEmployeeAddress_approval_tran.enAddressType.DOMICILE) Then
                        mstrscreenname = Language.getMessage(mstrModuleName, 902, "Domicile Address")
                    ElseIf enaddressType = CInt(clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT) Then
                        mstrscreenname = Language.getMessage(mstrModuleName, 903, "Recruitment Address")
                    End If


                Case enScreenName.frmEmergencyAddressList
                    If enaddressType = CInt(clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1) Then
                        mstrscreenname = Language.getMessage(mstrModuleName, 904, "Emergency Address1")
                    ElseIf enaddressType = CInt(clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2) Then
                        mstrscreenname = Language.getMessage(mstrModuleName, 905, "Emergency Address2")
                    ElseIf enaddressType = CInt(clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3) Then
                        mstrscreenname = Language.getMessage(mstrModuleName, 906, "Emergency Address3")
                    End If

                Case enScreenName.frmBirthinfo
                    mstrscreenname = Language.getMessage(mstrModuleName, 907, "Birth Info")

                Case enScreenName.frmOtherinfo
                    mstrscreenname = Language.getMessage(mstrModuleName, 908, "Other Info")


                Case enScreenName.frmIdentityInfoList
                    mstrscreenname = Language.getMessage(mstrModuleName, 70, "Employee Identiy")

                Case enScreenName.frmMembershipInfoList
                    mstrscreenname = Language.getMessage(mstrModuleName, 71, "Employee Membership")
            End Select

            Select Case intNotificationType
                Case 1, 2
                    StrMessage.Append(Language.getMessage("frmEmployeeMaster", 147, "Dear") & " " & getTitleCase(username) & "</span></p>")

                    StrMessage.Append(Language.getMessage(mstrModuleName, 54, "Please note that the following changes on bio data for") & " " & Language.getMessage(mstrModuleName, 1999, "employee(s)") & " " & Language.getMessage(mstrModuleName, 55, "have been made and submitted for approval") & "." & " </span></p>")
                    StrMessage.Append(Language.getMessage(mstrModuleName, 302, "Following Information") & " <b>")
                    Select Case Oprationtype
                        Case enOperationType.ADDED
                            StrMessage.Append(Language.getMessage(mstrModuleName, 303, "Newly Added"))

                        Case enOperationType.EDITED
                            StrMessage.Append(Language.getMessage(mstrModuleName, 304, "Edited"))

                        Case enOperationType.DELETED
                            StrMessage.Append(Language.getMessage(mstrModuleName, 305, "Deleted"))

                    End Select


                    StrMessage.Append("</b> " & Language.getMessage(mstrModuleName, 306, "on") & " " & "<b> (" & mstrscreenname & ") </b>.</span></p>")
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            End Select


            If DtTable.Rows.Count > 0 Then

                HeaderStrMsg.Append("<TABLE border = '1'>")
                HeaderStrMsg.Append(vbCrLf)
                HeaderStrMsg.Append("<TR bgcolor= 'SteelBlue'>")
                HeaderStrMsg.Append(vbCrLf)



                Select Case eScreenType
                    Case enScreenName.frmEmployee_Skill_List
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2000, "Skill Category") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2001, "Skill") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2002, "Discription") & "</span></b></TD>")

                    Case enScreenName.frmEmployeeRefereeList
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2003, "Refree Name") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2004, "Reference Type") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2005, "Address") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2006, "Country") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2007, "State") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2008, "City") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2009, "Gender") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2010, "Company") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2011, "Mobile No.") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2012, "Position") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2013, "Telephone No.") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2014, "Email Id") & "</span></b></TD>")


                    Case enScreenName.frmQualificationsList
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2015, "Qualification Group") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2016, "Qualification") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2017, "Reference No") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2018, "Start Date") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2019, "End Date") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2020, "Institute") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2021, "Remark") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2022, "Result") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2023, "IsSync") & "</span></b></TD>")


                    Case enScreenName.frmJobHistory_ExperienceList
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2024, "Company") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2025, "Job") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2018, "Start Date") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2019, "End Date") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2026, "Reason") & "</span></b></TD>")

                    Case enScreenName.frmDependantsAndBeneficiariesList
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2027, "Dependant First Name") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2035, "Dependant Middle Name") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2028, "Dependant Last Name") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2029, "Relation") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2030, "Isbeneficiary") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2031, "Birthdate") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2006, "Country") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2007, "State") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2008, "City") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2032, "Zipcode") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2005, "Address") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2013, "Telephone No.") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2011, "Mobile No.") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2071, "Nationality") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2034, "IdNo") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2009, "Gender") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2036, "Effective Date") & "</span></b></TD>")

                    Case enScreenName.frmAddressList
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2037, "Address-1") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2038, "Address-2") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2039, "Post Country") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2007, "State") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2040, "Post Town") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2041, "Post Code") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2042, "Prov/Region") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2043, "Road/Street") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2044, "Estate") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2045, "Street1") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2046, "Region1") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2047, "Chiefdom") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2048, "Village") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2033, "Town1") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2011, "Mobile No.") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2013, "Telephone No.") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2049, "Plot No") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2050, "Alternative No.") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2014, "Email Id") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2051, "Fax") & "</span></b></TD>")

                    Case enScreenName.frmEmergencyAddressList
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2069, "First Name") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2070, "Last Name") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2005, "Address") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2006, "Country") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2007, "State") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2008, "City") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2041, "Post Code") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2042, "Prov/Region") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2043, "Road/Street") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2044, "Estate") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2049, "Plot No") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2011, "Mobile No.") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2050, "Alternative No.") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2013, "Telephone No.") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2051, "Fax") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2014, "Email Id") & "</span></b></TD>")




                    Case enScreenName.frmBirthinfo
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2006, "Country") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2007, "State") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2008, "City") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2052, "Ward No") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2053, "Certificate No.") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2048, "Village") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2033, "Town1") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2047, "Chiefdom") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2054, "Village1") & "</span></b></TD>")

                    Case enScreenName.frmIdentityInfoList
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2055, "Identity Type") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2056, "Identity No.") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2057, "Isdefault") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2006, "Country") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2058, "Issued Place") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2059, "Dl Class") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2060, "Issue Date") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2061, "Expiry Date") & "</span></b></TD>")

                    Case enScreenName.frmMembershipInfoList
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2062, "Membership Category") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2063, "Membership Name") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2064, "Membership No") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2065, "Issue Date") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2066, "Start Date") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2067, "Expiry Date") & "</span></b></TD>")
                        HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 2068, "Effective Period") & "</span></b></TD>")




                End Select



                HeaderStrMsg.Append("</TR>")


            End If


            Dim GrpColVal As String = ""
            Dim drCol = DtTable.Columns.Cast(Of DataColumn)().AsEnumerable().Where(Function(x) x.ExtendedProperties.Count > 0).Select(Function(x) x.ColumnName)

            For Each iRow As DataRow In DtTable.Rows
                IsChangeExist = True
                Dim StrEmployeeName = ""

                Select Case eScreenType

                    Case enScreenName.frmEmployee_Skill_List, enScreenName.frmEmployeeRefereeList, enScreenName.frmQualificationsList, enScreenName.frmJobHistory_ExperienceList, enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmBirthinfo, enScreenName.frmEmergencyAddressList

                        StrEmployeeName = iRow("ECode").ToString() & "-" & iRow("Firstname").ToString() & " " & iRow("Surname").ToString()
                    Case enScreenName.frmDependantsAndBeneficiariesList
                        StrEmployeeName = iRow("ECode").ToString() & "-" & iRow("empfirstname").ToString() & " " & iRow("emplastname").ToString()

                    Case enScreenName.frmIdentityInfoList
                        StrEmployeeName = iRow("employeecode").ToString() & "-" & iRow("firstname").ToString() & " " & iRow("Surname").ToString()

                    Case enScreenName.frmMembershipInfoList
                        StrEmployeeName = iRow("employeecode").ToString() & "-" & iRow("displayname").ToString()


                End Select



                If GrpColVal.ToUpper() <> StrEmployeeName.ToUpper() Then
                    BodyStrMsg.Append("<TR>")
                    GrpColVal = StrEmployeeName
                    BodyStrMsg.Append("<TD colspan = '" & drCol.Count & "' align = 'LEFT' STYLE='WIDTH:10%;Background-Color:#ccc'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & StrEmployeeName.Trim() & "</span></b></TD>")
                    BodyStrMsg.Append("</TR>")

                End If

                BodyStrMsg.Append("<TR>")

                For index As Integer = 0 To drCol.Count - 1


                    'CDate(dr(idx)(EmailDt.Rows(index)("iC").ToString())).ToShortDateString()


                    Select Case eScreenType
                        Case enScreenName.frmQualificationsList, enScreenName.frmJobHistory_ExperienceList

                            If drCol.ElementAt(index).ToString().ToUpper() = "STARTDATE" Or drCol.ElementAt(index).ToString().ToUpper() = "ENDDATE" Then
                                If iRow(drCol.ElementAt(index)).ToString().Length > 0 AndAlso IsDate(iRow(drCol.ElementAt(index)).ToString()) Then
                                    iRow(drCol.ElementAt(index)) = CDate(iRow(drCol.ElementAt(index))).ToShortDateString()
                                End If
                            End If

                        Case enScreenName.frmDependantsAndBeneficiariesList
                            If drCol.ElementAt(index).ToString().ToUpper() = "BIRTHDATE" Or drCol.ElementAt(index).ToString().ToUpper() = "EFFECTIVEDATE" Then

                                If iRow(drCol.ElementAt(index)).ToString().Length > 0 AndAlso IsDate(iRow(drCol.ElementAt(index)).ToString()) Then
                                    iRow(drCol.ElementAt(index)) = CDate(iRow(drCol.ElementAt(index))).ToShortDateString()
                                End If
                            End If

                        Case enScreenName.frmIdentityInfoList
                            If drCol.ElementAt(index).ToString().ToUpper() = "ISSUE_DATE" Or drCol.ElementAt(index).ToString().ToUpper() = "EXPIRY_DATE" Then
                                If iRow(drCol.ElementAt(index)).ToString().Length > 0 AndAlso IsDate(iRow(drCol.ElementAt(index)).ToString()) Then
                                    iRow(drCol.ElementAt(index)) = CDate(iRow(drCol.ElementAt(index))).ToShortDateString()
                                End If
                            End If

                        Case enScreenName.frmMembershipInfoList
                            If drCol.ElementAt(index).ToString().ToUpper() = "ISSUEDATE" Or drCol.ElementAt(index).ToString().ToUpper() = "STARTDATE" Or drCol.ElementAt(index).ToString().ToUpper() = "EXPIRYDATE" Then
                                If iRow(drCol.ElementAt(index)).ToString().Length > 0 AndAlso IsDate(iRow(drCol.ElementAt(index)).ToString()) Then
                                    iRow(drCol.ElementAt(index)) = CDate(iRow(drCol.ElementAt(index))).ToShortDateString()
                                End If
                            End If

                    End Select



                    BodyStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & iRow(drCol.ElementAt(index)).ToString() & "</span></b></TD>")
                Next
                BodyStrMsg.Append("</TR>")


                'Select Case eScreenType
                '    Case enScreenName.frmEmployee_Skill_List
                '        If iRow("scategory").ToString().Trim.Length > 0 Then
                '            BodyStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & iRow("scategory").ToString().Trim() & "</span></b></TD>")
                '        End If

                '        If iRow("skill").ToString().Trim.Length > 0 Then
                '            BodyStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & iRow("skill").ToString().Trim() & "</span></b></TD>")
                '        End If

                '        If iRow("description").ToString().Trim.Length > 0 Then
                '            BodyStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & iRow("description").ToString().Trim() & "</span></b></TD>")
                '        End If
                'End Select

                'BodyStrMsg.Append("</TR>")

            Next


            If IsChangeExist Then
                StrMessage.Append(HeaderStrMsg)
                StrMessage.Append(BodyStrMsg)
                StrMessage.Append("</TABLE>")

                Select Case intNotificationType
                    Case 1, 2
                        StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                        StrMessage.Append(Language.getMessage(mstrModuleName, 56, "Please login to Aruti application to approve/reject the changes.") & "</span></p>")
                End Select


                If StrMessage.Length > 0 Then
                    StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                    StrMessage.Append("</span></p>")
                    StrMessage.Append("</BODY></HTML>")
                End If

            Else
                StrMessage.Length = 0
            End If




        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: SetEmailBody; Module Name: " & mstrModuleName)
        End Try


    End Sub
    'Gajanan [27-May-2019] -- End

    Private Function GetColumnValue(ByVal strColName As String, ByVal strDatatype As String, ByVal eScreenType As enScreenName) As String

        Dim ColumnValue As String = String.Empty

        Try

            If strColName.ToString().ToUpper() = "" Then


            Else

                Select Case eScreenType

                    Case enScreenName.frmEmployee_Skill_List


                End Select

            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetEmailBody; Module Name: " & mstrModuleName)
        End Try

        Return ColumnValue
    End Function

    Private Sub SetEmailBody(ByRef StrMessage As System.Text.StringBuilder, _
                             ByVal eScreenType As enScreenName, _
                             ByVal intNotificationType As Integer, _
                             ByVal dr() As DataRow, _
                             ByVal username As String, _
                             ByVal strRejectionRemark As String, _
                             ByVal blnRejection As Boolean, _
                             ByVal strEmployeeName As String, _
                             ByVal Oprationtype As enOperationType, _
                             ByVal eToemailtype As enToEmailType, _
                             Optional ByVal enaddressType As Integer = 0, _
                             Optional ByVal extrafilter As String = "", _
                             Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                             Optional ByVal isfromapproval As Boolean = False, _
                             Optional ByVal mdtTable As DataTable = Nothing)
        'Gajanan [9-April-2019] -- [enaddressType]
        Try
            Dim EmailDt As DataTable = Nothing
            Dim IsChangeExist As Boolean = False
            Dim StrQ As String = ""

            Dim UniqueCol As String = ""
            Dim MatchingColName As String = ""
            Dim Table1 As String = ""
            Dim Table2 As String = ""
            Dim DisplayColName As String = String.Empty

            Dim mstrscreenname As String = String.Empty

            Dim attachmentnewadded As Boolean = False
            Dim attachmentdeleted As Boolean = False

            Dim HeaderStrMsg As New System.Text.StringBuilder
            Dim MasterDataStrMsg As New System.Text.StringBuilder
            Dim DependantStrMsg As New System.Text.StringBuilder
            Dim DependantMembershipStrMsg As New System.Text.StringBuilder
            Dim IdentityStrMsg As New System.Text.StringBuilder
            Dim MembershipStrMsg As New System.Text.StringBuilder
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            Dim DependantStatusMsg As New System.Text.StringBuilder
            'Sohail (18 May 2019) -- End

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
            End If
            'Gajanan [17-April-2019] -- End
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("Dear <b>" & username & "</b></span></p>")

            Select Case eScreenType
                Case enScreenName.frmEmployee_Skill_List
                    mstrscreenname = Language.getMessage(mstrModuleName, 60, "Skill")
                    MatchingColName = "emp_app_unkid"
                    UniqueCol = "skillstranunkid"
                    Table1 = "hremp_app_skills_tran" 'Original Table
                    Table2 = "hremp_app_skills_approval_tran" 'Approver Table


                Case enScreenName.frmDependantsAndBeneficiariesList
                    mstrscreenname = Language.getMessage(mstrModuleName, 66, "Dependant And Benifits")
                    MatchingColName = "employeeunkid"
                    UniqueCol = "dpndtbeneficetranunkid"
                    Table1 = "hrdependants_beneficiaries_tran"
                    Table2 = "hrdependant_beneficiaries_approval_tran"

                Case enScreenName.frmEmployeeRefereeList
                    mstrscreenname = Language.getMessage(mstrModuleName, 61, "Referee")
                    MatchingColName = "employeeunkid"
                    UniqueCol = "refereetranunkid"
                    Table1 = "hremployee_referee_tran" 'Original Table
                    Table2 = "hremployee_referee_approval_tran" 'Approver Table

                Case enScreenName.frmQualificationsList
                    mstrscreenname = Language.getMessage(mstrModuleName, 786, "Qualification")
                    MatchingColName = "employeeunkid"
                    UniqueCol = "qualificationtranunkid"
                    Table1 = "hremp_qualification_tran" 'Original Table
                    Table2 = "hremp_qualification_approval_tran" 'Approver Table

                Case enScreenName.frmJobHistory_ExperienceList
                    mstrscreenname = Language.getMessage(mstrModuleName, 2075, "Experience")
                    MatchingColName = "employeeunkid"
                    UniqueCol = "experiencetranunkid"
                    Table1 = "hremp_experience_tran" 'Original Table
                    Table2 = "hremp_experience_approval_tran" 'Approver Table

                Case enScreenName.frmAddressList
                    mstrscreenname = String.Join(",", dr.Select(Function(x) x.Field(Of String)("addresstype").Trim).Distinct().ToArray())
                    'Language.getMessage(mstrModuleName, 64, "Address")
                    MatchingColName = "employeeunkid"
                    UniqueCol = "employeeunkid"
                    Table1 = "hremployee_master"
                    Table2 = "hremployee_address_approval_tran"

                Case enScreenName.frmEmergencyAddressList
                    mstrscreenname = String.Join(",", dr.Select(Function(x) x.Field(Of String)("addresstype").Trim).Distinct().ToArray())
                    'mstrscreenname = Language.getMessage(mstrModuleName, 65, "Emergency Contact")
                    MatchingColName = "employeeunkid"
                    UniqueCol = "employeeunkid"
                    Table1 = "hremployee_master"
                    Table2 = "hremployee_emergency_address_approval_tran"

                Case enScreenName.frmBirthinfo
                    mstrscreenname = Language.getMessage(mstrModuleName, 907, "Birth Info")
                    MatchingColName = "employeeunkid"
                    UniqueCol = "employeeunkid"
                    Table1 = "hremployee_master"
                    Table2 = "hremployee_personal_approval_tran"

                Case enScreenName.frmOtherinfo
                    mstrscreenname = Language.getMessage(mstrModuleName, 908, "Other Info")
                    MatchingColName = "employeeunkid"
                    UniqueCol = "employeeunkid"
                    Table1 = "hremployee_master"
                    Table2 = "hremployee_personal_approval_tran"

                Case enScreenName.frmIdentityInfoList
                    mstrscreenname = Language.getMessage(mstrModuleName, 70, "Employee Identiy")

                Case enScreenName.frmMembershipInfoList
                    mstrscreenname = Language.getMessage(mstrModuleName, 71, "Employee Membership")

                    'Sohail (18 May 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                Case enScreenName.frmDependantStatus
                    mstrscreenname = Language.getMessage(mstrModuleName, 72, "Dependant Status")
                    MatchingColName = "dpndtbeneficetranunkid"
                    UniqueCol = "dpndtbeneficetranunkid"
                    Table1 = "hrdependant_beneficiaries_status_tran"
                    Table2 = "hrdependant_beneficiaries_approval_tran"
                    'Sohail (18 May 2019) -- End

            End Select

            Select Case intNotificationType
                Case 1, 2

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'StrMessage.Append(Language.getMessage("frmEmployeeMaster", 147, "Dear") & " " & Language.getMessage("frmEmployeeMaster", 300, "Approver") & " " & getTitleCase(username) & "</span></p>")
                    StrMessage.Append(Language.getMessage("frmEmployeeMaster", 147, "Dear") & " " & getTitleCase(username) & "</span></p>")
                    'Gajanan [17-April-2019] -- End

                    'Sohail (18 May 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                    'StrMessage.Append(Language.getMessage(mstrModuleName, 54, "Please note that the following changes on bio data for") & "<b> " & "(" & getTitleCase(strEmployeeName) & ") </b>" & Language.getMessage(mstrModuleName, 55, "have been made and submitted for approval") & "." & " </span></p>")
                    Select Case eScreenType

                        Case enScreenName.frmDependantStatus
                            StrMessage.Append(Language.getMessage(mstrModuleName, 54, "Please note that the following changes on bio data for") & " " & Language.getMessage(mstrModuleName, 9001, "some employees") & " " & Language.getMessage(mstrModuleName, 55, "have been made and submitted for approval") & "." & " </span></p>")

                        Case Else
                    StrMessage.Append(Language.getMessage(mstrModuleName, 54, "Please note that the following changes on bio data for") & "<b> " & "(" & getTitleCase(strEmployeeName) & ") </b>" & Language.getMessage(mstrModuleName, 55, "have been made and submitted for approval") & "." & " </span></p>")

                    End Select
                    'Sohail (18 May 2019) -- End


                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.

                    StrMessage.Append(Language.getMessage(mstrModuleName, 302, "Following Information") & " ")
                    Select Case eScreenType
                        Case enScreenName.frmMembershipInfoList, enScreenName.frmIdentityInfoList, enScreenName.frmDependantStatus
                            'Sohail (18 May 2019) - [frmDependantStatus]
                            StrMessage.Append(Language.getMessage(mstrModuleName, 909, "has been change") & " ")
                        Case Else
                            StrMessage.Append(" <b>(")

                    Select Case Oprationtype
                        Case enOperationType.ADDED
                            StrMessage.Append(Language.getMessage(mstrModuleName, 303, "Newly Added"))

                        Case enOperationType.EDITED
                            StrMessage.Append(Language.getMessage(mstrModuleName, 304, "Edited"))

                        Case enOperationType.DELETED
                            StrMessage.Append(Language.getMessage(mstrModuleName, 305, "Deleted"))

                    End Select
                    StrMessage.Append(")</b> ")

                    End Select



                    StrMessage.Append(Language.getMessage(mstrModuleName, 306, "on ") & "<b> (" & mstrscreenname & ") </b>.</span></p>")
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    'Gajanan [17-April-2019] -- End

                Case 3

                    Select Case eToemailtype
                        Case enToEmailType.USER
                            StrMessage.Append(Language.getMessage("frmEmployeeMaster", 147, "Dear") & " " & getTitleCase(username) & "</span></p>")
                            StrMessage.Append(Language.getMessage(mstrModuleName, 301, "Please note bio data changes made by") & " " & getTitleCase(strEmployeeName) & " " & Language.getMessage(mstrModuleName, 913, "on") & " " & "<b>(" & mstrscreenname & ")</b> " & Language.getMessage(mstrModuleName, 59, "have been rejected") & "." & " </span></p>")
                        Case enToEmailType.EMPLOYEE
                            StrMessage.Append(Language.getMessage("frmEmployeeMaster", 147, "Dear") & " " & getTitleCase(strEmployeeName) & "</span></p>")
                            StrMessage.Append(Language.getMessage(mstrModuleName, 915, "Please note your bio data changes made by you on ") & " " & "<b>(" & mstrscreenname & ")</b> " & Language.getMessage(mstrModuleName, 59, "have been rejected") & "." & " </span></p>")
                        Case Else

                    End Select
                Case 4
                    Select Case eToemailtype
                        Case enToEmailType.EMPLOYEE
                            StrMessage.Append(Language.getMessage("frmEmployeeMaster", 147, "Dear") & " " & getTitleCase(strEmployeeName) & "</span></p>")
                            StrMessage.Append(Language.getMessage(mstrModuleName, 912, "Please note your bio data changes made by you on ") & " " & "<b>(" & mstrscreenname & ")</b> " & Language.getMessage(mstrModuleName, 58, "have been approved") & "." & " </span></p>")
                        Case Else
                    End Select
            End Select



            Select Case intNotificationType
                Case 1, 2

                    'Sohail (18 May 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                    Dim strPrevDepn As String = ""
                    Dim strDepn As String = ""
                    'Sohail (18 May 2019) -- End

                    For idx As Integer = 0 To dr.Length - 1
                        Select Case eScreenType

                            Case enScreenName.frmAddressList
                                Select Case CType(dr(idx)("addresstypeid"), clsEmployeeAddress_approval_tran.enAddressType)
                                    Case clsEmployeeAddress_approval_tran.enAddressType.PRESENT
                                        mstrscreenname = Language.getMessage(mstrModuleName, 901, "Present Address")
                                        DisplayColName = " AND s.COLUMN_NAME LIKE 'present_%' "
                                    Case clsEmployeeAddress_approval_tran.enAddressType.DOMICILE
                                        mstrscreenname = Language.getMessage(mstrModuleName, 902, "Domicile Address")
                                        DisplayColName = " AND s.COLUMN_NAME LIKE 'domicile_%' "
                                    Case clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT
                                        mstrscreenname = Language.getMessage(mstrModuleName, 903, "Recruitment Address")
                                        DisplayColName = " AND (s.COLUMN_NAME LIKE 'rec%' OR s.COLUMN_NAME LIKE 'rc%') "
                                End Select

                            Case enScreenName.frmEmergencyAddressList
                                Select Case CType(dr(idx)("addresstypeid"), clsEmployee_emergency_address_approval.enEmeAddressType)
                                    Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1
                                        mstrscreenname = Language.getMessage(mstrModuleName, 904, "Emergency Address1")
                                        DisplayColName = " AND s.COLUMN_NAME  LIKE 'emer_con_%' and right(COLUMN_NAME,1) not in ( '2', '3') "
                                    Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2
                                        mstrscreenname = Language.getMessage(mstrModuleName, 905, "Emergency Address2")
                                        DisplayColName = " AND s.COLUMN_NAME  LIKE 'emer_con_%' and right(COLUMN_NAME,1) = '2' "
                                    Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3
                                        mstrscreenname = Language.getMessage(mstrModuleName, 906, "Emergency Address3")
                                        DisplayColName = " AND s.COLUMN_NAME  LIKE 'emer_con_%' and right(COLUMN_NAME,1) = '3'  "
                                End Select
                            Case enScreenName.frmBirthinfo
                                DisplayColName = " AND S.COLUMN_NAME like 'birth%' and S.COLUMN_NAME <> 'birthdate' "
                            Case enScreenName.frmOtherinfo
                                DisplayColName = " AND s.COLUMN_NAME  IN (SELECT COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where table_name ='" & Table2 & "' and (COLUMN_NAME NOT like 'birth%')) "
                        End Select



                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.

                        'StrMessage.Append(Language.getMessage(mstrModuleName, 302, "Following Information <b>("))
                        'Select Case Oprationtype
                        '    Case enOperationType.ADDED
                        '        StrMessage.Append(Language.getMessage(mstrModuleName, 303, "Newly Added"))

                        '    Case enOperationType.EDITED
                        '        StrMessage.Append(Language.getMessage(mstrModuleName, 304, "Edited"))

                        '    Case enOperationType.DELETED
                        '        StrMessage.Append(Language.getMessage(mstrModuleName, 305, "Deleted"))

                        'End Select
                        'StrMessage.Append(")</b> ")

                        'StrMessage.Append(Language.getMessage(mstrModuleName, 306, "on ") & "<b> (" & mstrscreenname & ") </b>.</span></p>")
                        'StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                        'Gajanan [17-April-2019] -- End


                        If Table1 <> String.Empty AndAlso Table2 <> String.Empty Then '#1

                            StrQ = "DECLARE @UniqueCol AS NVARCHAR(MAX) " & _
                                   "DECLARE @ExcludeColNames AS NVARCHAR(MAX) " & _
                                   "DECLARE @MatchingColName AS NVARCHAR(MAX) " & _
                                   "DECLARE @condition AS nvarchar(max) " & _
                                   "DECLARE @table AS TABLE(iQ NVARCHAR(MAX),iC NVARCHAR(MAX), iD NVARCHAR(MAX), iM NVARCHAR(MAX),iU NVARCHAR(MAX), iA NVARCHAR(MAX),iE NVARCHAR(MAX)) " & _
                                   "SET @MatchingColName = '" & MatchingColName & "' " & _
                                   "SET @ExcludeColNames = '' " & _
                                   "SET @UniqueCol = '" & UniqueCol & "' " & _
                                   "SET @condition = ' AND #tab2 .' + @UniqueCol + '  = #idcol ' " & _
                                   "INSERT INTO @table ([@table].iQ, [@table].iC, [@table].iD, [@table].iM, [@table].iU, [@table].iA, [@table].iE) " & _
                                        "SELECT " & _
                                             "A.iCol " & _
                                           ",A.COLUMN_NAME " & _
                                           ",A.DATA_TYPE " & _
                                           ",ROW_NUMBER() OVER (ORDER BY A.ORDINAL_POSITION) " & _
                                           ",'#tab1' " & _
                                           ",A.iAdd " & _
                                           ",A.iColE " & _
                                        "FROM (SELECT " & _
                                                  "CASE " & _
                                               "WHEN @MatchingColName <> s.COLUMN_NAME THEN 'SELECT #tab1.' + s.COLUMN_NAME + ' AS oldval , #tab2.' + s.COLUMN_NAME + ' AS newval FROM #tab1,#tab2 WHERE #tab1.' + @MatchingColName + ' = #tab2.' + @MatchingColName + " & _
                                                    "' AND #tab1.' + s.COLUMN_NAME + ' <> ' + '#tab2.' + s.COLUMN_NAME + ' AND #tab2.operationtypeid = " & CType(Oprationtype, enOperationType) & " /*AND #tab1.' + @UniqueCol + ' = #idcol*/  AND isfinal = 0 AND statusunkid=1 AND isprocessed=0 AND #tab2.isvoid = 0 ' "
                            'Sohail (18 May 2019) - [AND isvoid = 0]

                            'Sohail (18 May 2019) -- Start
                            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                            If eScreenType <> enScreenName.frmDependantStatus Then
                                StrQ &= " + ' AND #tab1.' + @UniqueCol + ' = #idcol AND #tab2.' + @UniqueCol + ' = #idcol ' "
                            End If
                            'Sohail (18 May 2019) -- End

                            StrQ &= "END AS iCol " & _
                                                ",s.COLUMN_NAME " & _
                                                ",s.DATA_TYPE " & _
                                                ",s.ORDINAL_POSITION " & _
                                        ",'SELECT ' + s.COLUMN_NAME + ' as newval FROM #tab2 WHERE #tab2.' + @MatchingColName + ' = #val AND isfinal = 0 AND statusunkid=1 AND isprocessed=0 AND #tab2.isvoid = 0 AND #tab2.operationtypeid = #opr"
                            'Sohail (18 May 2019) - [AND isvoid = 0]

                            If CType(Oprationtype, enOperationType) = enOperationType.EDITED Or CType(Oprationtype, enOperationType) = enOperationType.DELETED Then
                                StrQ &= " ' + @condition + ' "
                            End If

                            StrQ &= "' AS iAdd " & _
                                    ",CASE " & _
                            "   WHEN @MatchingColName <> s.COLUMN_NAME THEN 'SELECT #tab1.' + s.COLUMN_NAME + ' AS oldval , #tab2.' + s.COLUMN_NAME + ' AS newval FROM #tab1,#tab2 WHERE #tab1.' + @MatchingColName + ' = #tab2.' + @MatchingColName + " & _
                            "' AND #tab2.operationtypeid = 2 /*AND #tab1.' + @UniqueCol + ' = #idcol*/  AND isfinal = 1 AND isprocessed=1 AND #tab2.isvoid = 0' "

                            'Sohail (18 May 2019) -- Start
                            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                            If eScreenType <> enScreenName.frmDependantStatus Then
                                StrQ &= " + ' AND #tab1.' + @UniqueCol + ' = #idcol  AND #tab2.' + @UniqueCol + ' = #idcol ' "
                            End If
                            'Sohail (18 May 2019) -- End

                            StrQ &= "END AS iColE " & _
                                    "FROM INFORMATION_SCHEMA.COLUMNS s " & _
                                    "JOIN SYS.IDENTITY_COLUMNS AS I " & _
                                            "ON s.TABLE_NAME = OBJECT_NAME(I.object_id) " & _
                                            "WHERE TABLE_NAME = '" & Table1 & "' " & _
                                            "AND s.COLUMN_NAME <> I.name " & _
                                    "AND s.COLUMN_NAME NOT IN ('transaction_date','userunkid', 'isvoid', 'voiduserunkid', 'voiddatetime', 'voidreason','loginemployeeunkid','voidloginemployeeunkid','isapplicant') "
                            'Sohail (18 May 2019) -- Start
                            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                            If eScreenType = enScreenName.frmDependantStatus Then
                                StrQ &= " AND s.COLUMN_NAME  <> 'dpndtbeneficetranunkid' "
                            End If
                            'Sohail (18 May 2019) -- End

                            StrQ &= DisplayColName

                            'Sohail (18 May 2019) -- Start
                            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                            If eScreenType = enScreenName.frmDependantsAndBeneficiariesList Then
                                StrQ &= "UNION " & _
                                     "SELECT " & _
                                                  "CASE " & _
                                               "WHEN 'dpndtbeneficetranunkid' <> s.COLUMN_NAME THEN 'SELECT hrdependant_beneficiaries_status_tran.' + s.COLUMN_NAME + ' AS oldval , #tab2.' + s.COLUMN_NAME + ' AS newval FROM hrdependant_beneficiaries_status_tran,#tab2 WHERE hrdependant_beneficiaries_status_tran.' + 'dpndtbeneficetranunkid' + ' = #tab2.' + 'dpndtbeneficetranunkid' + " & _
                                                    "' AND hrdependant_beneficiaries_status_tran.' + s.COLUMN_NAME + ' <> ' + '#tab2.' + s.COLUMN_NAME + ' AND #tab2.operationtypeid = " & CType(Oprationtype, enOperationType) & " AND #tab2.' + @UniqueCol + ' = #idcol AND hrdependant_beneficiaries_status_tran.#UniqueCol = #dpndtbeneficestatustranunkid  AND isfinal = 0 AND statusunkid=1 AND isprocessed=0 AND #tab2.isvoid = 0' " & _
                                                  "END AS iCol " & _
                                                ",s.COLUMN_NAME " & _
                                                ",s.DATA_TYPE " & _
                                                ",s.ORDINAL_POSITION " & _
                                        ",'SELECT ' + s.COLUMN_NAME + ' as newval FROM #tab2 WHERE #tab2.' + @MatchingColName + ' = #val AND isfinal = 0 AND statusunkid=1 AND isprocessed=0 AND #tab2.isvoid = 0 AND #tab2.operationtypeid = #opr"
                                'Sohail (18 May 2019) - [AND isvoid = 0]

                            If CType(Oprationtype, enOperationType) = enOperationType.EDITED Or CType(Oprationtype, enOperationType) = enOperationType.DELETED Then
                                StrQ &= " ' + @condition + ' "
                            End If

                            StrQ &= "' AS iAdd " & _
                                    ",CASE " & _
                                "   WHEN 'dpndtbeneficetranunkid' <> s.COLUMN_NAME THEN 'SELECT hrdependant_beneficiaries_status_tran.' + s.COLUMN_NAME + ' AS oldval , #tab2.' + s.COLUMN_NAME + ' AS newval FROM hrdependant_beneficiaries_status_tran,#tab2 WHERE hrdependant_beneficiaries_status_tran.' + 'dpndtbeneficetranunkid' + ' = #tab2.' + 'dpndtbeneficetranunkid' + " & _
                                "' AND #tab2.operationtypeid = 2 AND #tab2.' + @UniqueCol + ' = #idcol AND hrdependant_beneficiaries_status_tran.#UniqueCol = #dpndtbeneficestatustranunkid  AND isfinal = 1 AND isprocessed=1 AND #tab2.isvoid = 0' " & _
                                    "END AS iColE " & _
                                    "FROM INFORMATION_SCHEMA.COLUMNS s " & _
                                    "JOIN SYS.IDENTITY_COLUMNS AS I " & _
                                            "ON s.TABLE_NAME = OBJECT_NAME(I.object_id) " & _
                                                "WHERE TABLE_NAME = 'hrdependant_beneficiaries_status_tran' " & _
                                            "AND s.COLUMN_NAME <> I.name " & _
                                                "AND s.COLUMN_NAME <> @UniqueCol " & _
                                    "AND s.COLUMN_NAME NOT IN ('transaction_date','userunkid', 'isvoid', 'voiduserunkid', 'voiddatetime', 'voidreason','loginemployeeunkid','voidloginemployeeunkid','isapplicant') "
                            StrQ &= DisplayColName

                            End If
                            'Sohail (18 May 2019) -- End

                            StrQ &= ") AS A WHERE A.iCol IS NOT NULL " & _
                                           "SELECT " & _
                                                "'INSERT INTO cfmessages (messagecode,module_name,message,message1,message2)" & _
                                                "SELECT ' + [@table].iM + ',''' + [@table].iU + ''',''' + REPLACE(REPLACE([@table].iC, '_', ' '), 'unkid', '') + ''',''' + REPLACE(REPLACE([@table].iC, '_', ' '), 'unkid', '') + ''',''' + REPLACE(REPLACE([@table].iC, '_', ' '), 'unkid', '') + ''' WHERE NOT EXISTS ' + " & _
                                                "'(SELECT * FROM cfmessages WHERE messagecode = ' + [@table].iM + ' AND module_name = ''' + [@table].iU + ''' AND message = ''' + REPLACE(REPLACE([@table].iC, '_', ' '), 'unkid', '') + ''' )' AS Step1 " & _
                                              ",[@table].iQ AS Step2 " & _
                                              ",'DECLARE @langId AS INT ' + " & _
                                                "'SET @langId = (SELECT languageunkid FROM hrmsConfiguration..cfuser_master WHERE userunkid = 1) ' + " & _
                                                "'SELECT ' + " & _
                                                "'CASE @langId ' + " & _
                                                "'WHEN 1 THEN message1 ' + " & _
                                                "'WHEN 2 THEN message2 ' + " & _
                                                "'ELSE message END AS msg ' + " & _
                                                "'FROM cfmessages ' + " & _
                                        "'WHERE messagecode = ''' + [@table].iM + ''' AND module_name = ''' + [@table].iU + ''' AND message = ''' + REPLACE(REPLACE([@table].iC, '_', ' '), 'unkid', '') + '''' AS Step21 "

                            If Oprationtype = enOperationType.ADDED AndAlso extrafilter.Length > 0 Then
                                StrQ &= ",[@table].iA " & " + 'and " & extrafilter.Replace("'", "''") & "' as iA"
                            Else
                                StrQ &= ",[@table].iA "
                            End If

                            StrQ &= ",iD " & _
                                    ",iC " & _
                                    ",iE " & _
                                    "FROM @table "

                            StrQ = StrQ.Replace("#tab1", Table1)
                            StrQ = StrQ.Replace("#tab2", Table2)

                           
                            Select Case eScreenType
                                Case enScreenName.frmEmployee_Skill_List
                                    StrQ = StrQ.Replace("#idcol", dr(idx)("skillstranunkid").ToString().Trim())
                                Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmBirthinfo, enScreenName.frmOtherinfo
                                    StrQ = StrQ.Replace("#idcol", dr(idx)("employeeunkid").ToString().Trim())

                                Case enScreenName.frmAddressList, enScreenName.frmDependantsAndBeneficiariesList
                                    StrQ = StrQ.Replace("#idcol", dr(idx)("dpndtbeneficetranunkid").ToString().Trim())
                                    'Sohail (18 May 2019) -- Start
                                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                                    Dim sQ As String = "SELECT TOP 1 " & _
                                                            "dpndtbeneficestatustranunkid " & _
                                                        "FROM hrdependant_beneficiaries_status_tran " & _
                                                        "WHERE isvoid = 0 " & _
                                                              "AND dpndtbeneficetranunkid = " & CInt(dr(idx)("dpndtbeneficetranunkid")) & " " & _
                                                              "AND CONVERT(CHAR(8), effective_date, 112) <= '" & eZeeDate.convertDate(CDate(dr(idx)("effective_date"))) & "' " & _
                                                              "ORDER BY effective_date DESC, dpndtbeneficestatustranunkid DESC "

                                    Dim ds As DataSet = objDataOperation.ExecQuery(sQ, "List")
                                    If objDataOperation.ErrorMessage <> "" Then
                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    End If

                                    If ds.Tables(0).Rows.Count > 0 Then
                                        StrQ = StrQ.Replace("#UniqueCol", "dpndtbeneficestatustranunkid")
                                        StrQ = StrQ.Replace("#dpndtbeneficestatustranunkid", ds.Tables(0).Rows(0).Item("dpndtbeneficestatustranunkid").ToString)
                                    Else
                                        StrQ = StrQ.Replace("#UniqueCol", "@UniqueCol")
                                        StrQ = StrQ.Replace("#dpndtbeneficestatustranunkid", dr(idx)("dpndtbeneficetranunkid").ToString().Trim())
                                    End If
                                    'Sohail (18 May 2019) -- End


                                Case enScreenName.frmEmployeeRefereeList
                                    StrQ = StrQ.Replace("#idcol", dr(idx)("refereetranunkid").ToString().Trim())

                                Case (enScreenName.frmQualificationsList)
                                    StrQ = StrQ.Replace("#idcol", dr(idx)("qualificationtranunkid").ToString().Trim())

                                    'Sohail (18 May 2019) -- Start
                                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                                Case enScreenName.frmDependantStatus
                                    'StrQ = StrQ.Replace("#idcol", dr(idx)("dpndtbeneficetranunkid").ToString().Trim())
                                    'Sohail (18 May 2019) -- End

                                Case enScreenName.frmJobHistory_ExperienceList
                                    StrQ = StrQ.Replace("#idcol", dr(idx)("experiencetranunkid").ToString().Trim())
                            End Select

                            StrQ = StrQ.Replace("#opr", CInt(Oprationtype).ToString())


                            If IsNothing(objDataOperation) Then
                                objDataOperation = New clsDataOperation
                            End If



                            EmailDt = objDataOperation.ExecQuery(StrQ, "List").Tables("List")

                            If objDataOperation.ErrorMessage <> "" Then
                                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            End If


                            'Sohail (18 May 2019) -- Start
                            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                            'If EmailDt.Rows.Count > 0 Then
                            If EmailDt.Rows.Count > 0 AndAlso eScreenType <> enScreenName.frmDependantStatus Then

                                If HeaderStrMsg.ToString.Trim = "" Then
                                    'Sohail (18 May 2019) -- End

                                HeaderStrMsg.Append("<TABLE border = '1'>")
                                HeaderStrMsg.Append(vbCrLf)
                                HeaderStrMsg.Append("<TR bgcolor= 'SteelBlue'>")
                                HeaderStrMsg.Append(vbCrLf)

                                If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then
                                    HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 200, "Particulars") & "</span></b></TD>")
                                    HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 201, "Values") & "</span></b></TD>")
                                Else
                                    HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 202, "Particular") & "</span></b></TD>")
                                    HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 203, "New Value") & "</span></b></TD>")
                                    HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 204, "Old Value") & "</span></b></TD>")
                                End If

                                HeaderStrMsg.Append("</TR>")

                                End If 'Sohail (18 May 2019)

                                StrQ = ""
                                StrQ = String.Join(" ", EmailDt.AsEnumerable().Select(Function(x) x.Field(Of String)("step1")).ToArray())



                                objDataOperation.ExecNonQuery(StrQ)

                                'Sohail (18 May 2019) -- Start
                                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                                Select Case eScreenType

                                    Case enScreenName.frmDependantsAndBeneficiariesList
                                        strDepn = dr(idx).Item("dependantname").ToString

                                End Select
                                'Sohail (18 May 2019) -- End


                                For index As Integer = 0 To EmailDt.Rows.Count - 1

                                    Dim OldValue As String = ""
                                    Dim NewValue As String = ""
                                    Dim ColumnName = ""
                                    Dim dttemp As DataTable = Nothing

                                    If CType(Oprationtype, enOperationType) = enOperationType.ADDED Then
                                        dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step21").ToString(), "ColumnName").Tables("ColumnName")

                                        'Gajanan [17-April-2019] -- Start
                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                        If objDataOperation.ErrorMessage <> "" Then
                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        End If
                                        'Gajanan [17-April-2019] -- End
                                        ColumnName = dttemp.Rows(0)("msg").ToString()
                                        dttemp = Nothing

                                        StrQ = EmailDt(index)("iA").ToString()
                                        StrQ = StrQ.Replace("#val", dr(idx)("employeeunkid").ToString())
                                        Select Case eScreenType
                                            Case enScreenName.frmAddressList
                                                If CType(dr(idx)("addresstypeid"), clsEmployeeAddress_approval_tran.enAddressType) > clsEmployeeAddress_approval_tran.enAddressType.NONE Then
                                                    Select Case CType(dr(idx)("addresstypeid"), clsEmployeeAddress_approval_tran.enAddressType)
                                                        Case clsEmployeeAddress_approval_tran.enAddressType.PRESENT
                                                            dttemp = objDataOperation.ExecQuery(StrQ.Replace("present_", "") & " AND addresstype = " & clsEmployeeAddress_approval_tran.enAddressType.PRESENT, "ColumnVal").Tables("ColumnVal")

                                                            'Gajanan [17-April-2019] -- Start
                                                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                            If objDataOperation.ErrorMessage <> "" Then
                                                                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                            End If
                                                            'Gajanan [17-April-2019] -- End

                                                        Case clsEmployeeAddress_approval_tran.enAddressType.DOMICILE
                                                            dttemp = objDataOperation.ExecQuery(StrQ.Replace("domicile_", "") & " AND addresstype = " & clsEmployeeAddress_approval_tran.enAddressType.DOMICILE, "ColumnVal").Tables("ColumnVal")
                                                        Case clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT

                                                            If StrQ.Contains("recruitment_") Then
                                                                dttemp = objDataOperation.ExecQuery(StrQ.Replace("recruitment_", "") & " AND addresstype = " & clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT, "ColumnVal").Tables("ColumnVal")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End
                                                            ElseIf StrQ.Contains("rc_") Then
                                                                If StrQ.Contains("rc_province") Then
                                                                    dttemp = objDataOperation.ExecQuery(StrQ.Replace("rc_province", "provicnce") & " AND addresstype = " & clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                Else
                                                                    dttemp = objDataOperation.ExecQuery(StrQ.Replace("rc_", "") & " AND addresstype = " & clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End

                                                                End If
                                                            End If
                                                    End Select
                                                Else
                                                    dttemp = objDataOperation.ExecQuery(StrQ, "ColumnVal").Tables("ColumnVal")
                                                    'Gajanan [17-April-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    End If
                                                    'Gajanan [17-April-2019] -- End
                                                End If
                                            Case enScreenName.frmEmergencyAddressList
                                                If CType(dr(idx)("addresstypeid"), clsEmployee_emergency_address_approval.enEmeAddressType) > 0 Then
                                                    Select Case CType(dr(idx)("addresstypeid"), clsEmployee_emergency_address_approval.enEmeAddressType)
                                                        Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1
                                                            If StrQ.Contains("emer_con_state") Then
                                                                dttemp = objDataOperation.ExecQuery(StrQ.Replace("emer_con_state", "stateunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1, "ColumnVal").Tables("ColumnVal")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End
                                                            ElseIf StrQ.Contains("emer_con_post_townunkid") Then
                                                                dttemp = objDataOperation.ExecQuery(StrQ.Replace("emer_con_post_townunkid", "townunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1, "ColumnVal").Tables("ColumnVal")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End
                                                            Else
                                                                dttemp = objDataOperation.ExecQuery(StrQ.Replace("emer_con_", "") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1, "ColumnVal").Tables("ColumnVal")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End
                                                            End If

                                                        Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2
                                                            If StrQ.Contains("emer_con_state2") Then
                                                                dttemp = objDataOperation.ExecQuery(StrQ.Replace("emer_con_state2", "stateunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2, "ColumnVal").Tables("ColumnVal")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End
                                                            ElseIf StrQ.Contains("emer_con_post_townunkid2") Then
                                                                dttemp = objDataOperation.ExecQuery(StrQ.Replace("emer_con_post_townunkid2", "townunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2, "ColumnVal").Tables("ColumnVal")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End
                                                            Else
                                                                dttemp = objDataOperation.ExecQuery(StrQ.Replace(EmailDt(index)("iC").ToString(), EmailDt(index)("iC").ToString().Replace("emer_con_", "").Substring(0, EmailDt(index)("iC").ToString().Replace("emer_con_", "").Length - 1)) & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2, "ColumnVal").Tables("ColumnVal")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End
                                                            End If

                                                        Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3
                                                            If StrQ.Contains("emer_con_state3") Then
                                                                dttemp = objDataOperation.ExecQuery(StrQ.Replace("emer_con_state3", "stateunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3, "ColumnVal").Tables("ColumnVal")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End
                                                            ElseIf StrQ.Contains("emer_con_post_townunkid3") Then
                                                                dttemp = objDataOperation.ExecQuery(StrQ.Replace("emer_con_post_townunkid3", "townunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3, "ColumnVal").Tables("ColumnVal")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End
                                                            Else
                                                                dttemp = objDataOperation.ExecQuery(StrQ.Replace(EmailDt(index)("iC").ToString(), EmailDt(index)("iC").ToString().Replace("emer_con_", "").Substring(0, EmailDt(index)("iC").ToString().Replace("emer_con_", "").Length - 1)) & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3, "ColumnVal").Tables("ColumnVal")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End
                                                            End If
                                                    End Select
                                                Else
                                                    dttemp = objDataOperation.ExecQuery(StrQ, "ColumnVal").Tables("ColumnVal")
                                                    'Gajanan [17-April-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    End If
                                                    'Gajanan [17-April-2019] -- End
                                                End If
                                            Case enScreenName.frmBirthinfo
                                                dttemp = objDataOperation.ExecQuery(StrQ & " AND isbirthinfo = 1 ", "ColumnVal").Tables("ColumnVal")
                                                'Gajanan [17-April-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                If objDataOperation.ErrorMessage <> "" Then
                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                End If
                                                'Gajanan [17-April-2019] -- End
                                            Case enScreenName.frmOtherinfo
                                                dttemp = objDataOperation.ExecQuery(StrQ & " AND isbirthinfo = 0 ", "ColumnVal").Tables("ColumnVal")
                                                'Gajanan [17-April-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                If objDataOperation.ErrorMessage <> "" Then
                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                End If
                                                'Gajanan [17-April-2019] -- End
                                            Case Else
                                                dttemp = objDataOperation.ExecQuery(StrQ, "ColumnVal").Tables("ColumnVal")
                                                'Gajanan [17-April-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                If objDataOperation.ErrorMessage <> "" Then
                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                End If
                                                'Gajanan [17-April-2019] -- End
                                        End Select

                                    ElseIf CType(Oprationtype, enOperationType) = enOperationType.EDITED Or CType(Oprationtype, enOperationType) = enOperationType.DELETED Then
                                        dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step21").ToString(), "ColumnName").Tables("ColumnName")
                                        'Gajanan [17-April-2019] -- Start
                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                        If objDataOperation.ErrorMessage <> "" Then
                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        End If
                                        'Gajanan [17-April-2019] -- End
                                        ColumnName = dttemp.Rows(0)("msg").ToString()


                                        If intNotificationType = 3 Or intNotificationType = 4 Then
                                            Select Case eScreenType
                                                Case enScreenName.frmAddressList

                                                    If CType(dr(idx)("addresstypeid"), clsEmployeeAddress_approval_tran.enAddressType) > clsEmployeeAddress_approval_tran.enAddressType.NONE Then

                                                        Select Case CType(dr(idx)("addresstypeid"), clsEmployeeAddress_approval_tran.enAddressType)
                                                            Case clsEmployeeAddress_approval_tran.enAddressType.PRESENT
                                                                dttemp = objDataOperation.ExecQuery(EmailDt(index)("iE").ToString().Replace("hremployee_address_approval_tran.present_", "hremployee_address_approval_tran.") & " AND addresstype = " & clsEmployeeAddress_approval_tran.enAddressType.PRESENT, "ColumnVal").Tables("ColumnVal")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                            Case clsEmployeeAddress_approval_tran.enAddressType.DOMICILE
                                                                dttemp = objDataOperation.ExecQuery(EmailDt(index)("iE").ToString().Replace("hremployee_address_approval_tran.domicile_", "hremployee_address_approval_tran.") & " AND addresstype = " & clsEmployeeAddress_approval_tran.enAddressType.DOMICILE, "ColumnVal").Tables("ColumnVal")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                            Case clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT
                                                                If EmailDt(index)("iE").ToString().Contains("hremployee_address_approval_tran.recruitment_") Then
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("iE").ToString().Replace("hremployee_address_approval_tran.recruitment_", "hremployee_address_approval_tran.") & " AND addresstype = " & clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End

                                                                ElseIf EmailDt(index)("iE").ToString().Contains("hremployee_address_approval_tran.rc_") Then
                                                                    If EmailDt(index)("iE").ToString().Contains("hremployee_address_approval_tran.rc_province") Then
                                                                        dttemp = objDataOperation.ExecQuery(EmailDt(index)("iE").ToString().Replace("hremployee_address_approval_tran.rc_province", "hremployee_address_approval_tran.provicnce") & " AND addresstype = " & clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT, "ColumnVal").Tables("ColumnVal")
                                                                        'Gajanan [17-April-2019] -- Start
                                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                        If objDataOperation.ErrorMessage <> "" Then
                                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                        End If
                                                                        'Gajanan [17-April-2019] -- End
                                                                    Else
                                                                        dttemp = objDataOperation.ExecQuery(EmailDt(index)("iE").ToString().Replace("hremployee_address_approval_tran.rc_", "hremployee_address_approval_tran.") & " AND addresstype = " & clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT, "ColumnVal").Tables("ColumnVal")
                                                                        'Gajanan [17-April-2019] -- Start
                                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                        If objDataOperation.ErrorMessage <> "" Then
                                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                        End If
                                                                        'Gajanan [17-April-2019] -- End

                                                                    End If
                                                                End If

                                                        End Select
                                                    Else
                                                        dttemp = objDataOperation.ExecQuery(EmailDt(index)("iE").ToString(), "ColumnVal").Tables("ColumnVal")
                                                        'Gajanan [17-April-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        If objDataOperation.ErrorMessage <> "" Then
                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        End If
                                                        'Gajanan [17-April-2019] -- End
                                                    End If
                                                Case enScreenName.frmEmergencyAddressList
                                                    If CType(dr(idx)("addresstypeid"), clsEmployee_emergency_address_approval.enEmeAddressType) > 0 Then

                                                        Select Case CType(dr(idx)("addresstypeid"), clsEmployee_emergency_address_approval.enEmeAddressType)
                                                            Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1
                                                                If EmailDt(index)("iE").ToString().Contains("hremployee_emergency_address_approval_tran.emer_con_state") Then
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("iE").ToString().Replace("hremployee_emergency_address_approval_tran.emer_con_state", "hremployee_emergency_address_approval_tran.stateunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                ElseIf EmailDt(index)("iE").ToString().Contains("hremployee_emergency_address_approval_tran.emer_con_post_townunkid") Then
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("iE").ToString().Replace("hremployee_emergency_address_approval_tran.emer_con_post_townunkid", "hremployee_emergency_address_approval_tran.townunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                Else
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("iE").ToString().Replace("hremployee_emergency_address_approval_tran.emer_con_", "hremployee_emergency_address_approval_tran.") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                End If

                                                            Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2
                                                                If EmailDt(index)("iE").ToString().Contains("hremployee_emergency_address_approval_tran.emer_con_state2") Then
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("iE").ToString().Replace("hremployee_emergency_address_approval_tran.emer_con_state2", "hremployee_emergency_address_approval_tran.stateunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                ElseIf EmailDt(index)("iE").ToString().Contains("hremployee_emergency_address_approval_tran.emer_con_post_townunkid2") Then
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("iE").ToString().Replace("hremployee_emergency_address_approval_tran.emer_con_post_townunkid2", "hremployee_emergency_address_approval_tran.townunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                Else
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("iE").ToString().Replace("hremployee_emergency_address_approval_tran." & EmailDt(index)("iC").ToString(), "hremployee_emergency_address_approval_tran." & EmailDt(index)("iC").ToString().Replace("emer_con_", "").Substring(0, EmailDt(index)("iC").ToString().Replace("emer_con_", "").Length - 1)) & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                End If

                                                            Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3
                                                                If EmailDt(index)("iE").ToString().Contains("hremployee_emergency_address_approval_tran.emer_con_state3") Then
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("iE").ToString().Replace("hremployee_emergency_address_approval_tran.emer_con_state3", "hremployee_emergency_address_approval_tran.stateunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                ElseIf EmailDt(index)("iE").ToString().Contains("hremployee_emergency_address_approval_tran.emer_con_post_townunkid3") Then
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("iE").ToString().Replace("hremployee_emergency_address_approval_tran.emer_con_post_townunkid3", "hremployee_emergency_address_approval_tran.townunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                Else
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("iE").ToString().Replace("hremployee_emergency_address_approval_tran." & EmailDt(index)("iC").ToString(), "hremployee_emergency_address_approval_tran." & EmailDt(index)("iC").ToString().Replace("emer_con_", "").Substring(0, EmailDt(index)("iC").ToString().Replace("emer_con_", "").Length - 1)) & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                End If
                                                        End Select
                                                    Else
                                                        dttemp = objDataOperation.ExecQuery(EmailDt(index)("iE").ToString(), "ColumnVal").Tables("ColumnVal")
                                                        'Gajanan [17-April-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        If objDataOperation.ErrorMessage <> "" Then
                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        End If
                                                        'Gajanan [17-April-2019] -- End
                                                    End If
                                                Case Else
                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("iE").ToString(), "ColumnVal").Tables("ColumnVal")
                                                    'Gajanan [17-April-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    End If
                                                    'Gajanan [17-April-2019] -- End
                                            End Select


                                        Else
                                            Select Case eScreenType
                                                Case enScreenName.frmEmployee_Skill_List, _
                                                     enScreenName.frmDependantsAndBeneficiariesList, _
                                                     enScreenName.frmEmployeeRefereeList, _
                                                     enScreenName.frmQualificationsList

                                                    If Oprationtype = enOperationType.DELETED Then
                                                        dttemp = objDataOperation.ExecQuery(EmailDt(index)("iA").ToString().Replace("#val", dr(idx)("employeeunkid").ToString()), "ColumnVal").Tables("ColumnVal")
                                                        'Gajanan [17-April-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        If objDataOperation.ErrorMessage <> "" Then
                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        End If
                                                        'Gajanan [17-April-2019] -- End
                                                    Else
                                                        dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString(), "ColumnVal").Tables("ColumnVal")
                                                        'Gajanan [17-April-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        If objDataOperation.ErrorMessage <> "" Then
                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        End If
                                                        'Gajanan [17-April-2019] -- End
                                                    End If

                                                Case enScreenName.frmAddressList

                                                    If CType(dr(idx)("addresstypeid"), clsEmployeeAddress_approval_tran.enAddressType) > clsEmployeeAddress_approval_tran.enAddressType.NONE Then

                                                        Select Case CType(dr(idx)("addresstypeid"), clsEmployeeAddress_approval_tran.enAddressType)
                                                            Case clsEmployeeAddress_approval_tran.enAddressType.PRESENT
                                                                dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString().Replace("hremployee_address_approval_tran.present_", "hremployee_address_approval_tran.") & " AND addresstype = " & clsEmployeeAddress_approval_tran.enAddressType.PRESENT, "ColumnVal").Tables("ColumnVal")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                            Case clsEmployeeAddress_approval_tran.enAddressType.DOMICILE
                                                                dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString().Replace("hremployee_address_approval_tran.domicile_", "hremployee_address_approval_tran.") & " AND addresstype = " & clsEmployeeAddress_approval_tran.enAddressType.DOMICILE, "ColumnVal").Tables("ColumnVal")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                            Case clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT
                                                                If EmailDt(index)("Step2").ToString().Contains("hremployee_address_approval_tran.recruitment_") Then
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString().Replace("hremployee_address_approval_tran.recruitment_", "hremployee_address_approval_tran.") & " AND addresstype = " & clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                ElseIf EmailDt(index)("Step2").ToString().Contains("hremployee_address_approval_tran.rc_") Then
                                                                    If EmailDt(index)("Step2").ToString().Contains("hremployee_address_approval_tran.rc_province") Then
                                                                        dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString().Replace("hremployee_address_approval_tran.rc_province", "hremployee_address_approval_tran.provicnce") & " AND addresstype = " & clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT, "ColumnVal").Tables("ColumnVal")
                                                                        'Gajanan [17-April-2019] -- Start
                                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                        If objDataOperation.ErrorMessage <> "" Then
                                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                        End If
                                                                        'Gajanan [17-April-2019] -- End
                                                                    Else
                                                                        dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString().Replace("hremployee_address_approval_tran.rc_", "hremployee_address_approval_tran.") & " AND addresstype = " & clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT, "ColumnVal").Tables("ColumnVal")
                                                                        'Gajanan [17-April-2019] -- Start
                                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                        If objDataOperation.ErrorMessage <> "" Then
                                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                        End If
                                                                        'Gajanan [17-April-2019] -- End

                                                                    End If
                                                                End If

                                                        End Select
                                                    Else
                                                        dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString(), "ColumnVal").Tables("ColumnVal")
                                                        'Gajanan [17-April-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        If objDataOperation.ErrorMessage <> "" Then
                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        End If
                                                        'Gajanan [17-April-2019] -- End
                                                    End If
                                                Case enScreenName.frmEmergencyAddressList
                                                    If CType(dr(idx)("addresstypeid"), clsEmployee_emergency_address_approval.enEmeAddressType) > 0 Then

                                                        Select Case CType(dr(idx)("addresstypeid"), clsEmployee_emergency_address_approval.enEmeAddressType)
                                                            Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1
                                                                If EmailDt(index)("Step2").ToString().Contains("hremployee_emergency_address_approval_tran.emer_con_state") Then
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString().Replace("hremployee_emergency_address_approval_tran.emer_con_state", "hremployee_emergency_address_approval_tran.stateunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                ElseIf EmailDt(index)("Step2").ToString().Contains("hremployee_emergency_address_approval_tran.emer_con_post_townunkid") Then
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString().Replace("hremployee_emergency_address_approval_tran.emer_con_post_townunkid", "hremployee_emergency_address_approval_tran.townunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                Else
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString().Replace("hremployee_emergency_address_approval_tran.emer_con_", "hremployee_emergency_address_approval_tran.") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                End If

                                                            Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2
                                                                If EmailDt(index)("Step2").ToString().Contains("hremployee_emergency_address_approval_tran.emer_con_state2") Then
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString().Replace("hremployee_emergency_address_approval_tran.emer_con_state2", "hremployee_emergency_address_approval_tran.stateunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                ElseIf EmailDt(index)("Step2").ToString().Contains("hremployee_emergency_address_approval_tran.emer_con_post_townunkid2") Then
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString().Replace("hremployee_emergency_address_approval_tran.emer_con_post_townunkid2", "hremployee_emergency_address_approval_tran.townunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                Else
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString().Replace("hremployee_emergency_address_approval_tran." & EmailDt(index)("iC").ToString(), "hremployee_emergency_address_approval_tran." & EmailDt(index)("iC").ToString().Replace("emer_con_", "").Substring(0, EmailDt(index)("iC").ToString().Replace("emer_con_", "").Length - 1)) & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                End If

                                                            Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3
                                                                If EmailDt(index)("Step2").ToString().Contains("hremployee_emergency_address_approval_tran.emer_con_state3") Then
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString().Replace("hremployee_emergency_address_approval_tran.emer_con_state3", "hremployee_emergency_address_approval_tran.stateunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                ElseIf EmailDt(index)("Step2").ToString().Contains("hremployee_emergency_address_approval_tran.emer_con_post_townunkid3") Then
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString().Replace("hremployee_emergency_address_approval_tran.emer_con_post_townunkid3", "hremployee_emergency_address_approval_tran.townunkid") & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                Else
                                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString().Replace("hremployee_emergency_address_approval_tran." & EmailDt(index)("iC").ToString(), "hremployee_emergency_address_approval_tran." & EmailDt(index)("iC").ToString().Replace("emer_con_", "").Substring(0, EmailDt(index)("iC").ToString().Replace("emer_con_", "").Length - 1)) & " AND addresstype = " & clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3, "ColumnVal").Tables("ColumnVal")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                End If
                                                        End Select
                                                    Else
                                                        dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString(), "ColumnVal").Tables("ColumnVal")
                                                        'Gajanan [17-April-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        If objDataOperation.ErrorMessage <> "" Then
                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        End If
                                                        'Gajanan [17-April-2019] -- End
                                                    End If
                                                Case enScreenName.frmBirthinfo
                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString() & " AND isbirthinfo = 1 ", "ColumnVal").Tables("ColumnVal")
                                                    'Gajanan [17-April-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    End If
                                                    'Gajanan [17-April-2019] -- End
                                                Case enScreenName.frmOtherinfo
                                                    If EmailDt(index)("Step2").ToString().Contains("anniversary_date") Then
                                                        dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString().Replace("hremployee_master.anniversary_date <> hremployee_personal_approval_tran.anniversary_date", "(hremployee_master.anniversary_date <> hremployee_personal_approval_tran.anniversary_date  or hremployee_master.anniversary_date is null or  hremployee_personal_approval_tran.anniversary_date is NULL )") & " AND isbirthinfo = 0 ", "ColumnVal").Tables("ColumnVal")
                                                        'Gajanan [17-April-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        If objDataOperation.ErrorMessage <> "" Then
                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        End If
                                                        'Gajanan [17-April-2019] -- End
                                                    Else
                                                        dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString() & " AND isbirthinfo = 0 ", "ColumnVal").Tables("ColumnVal")
                                                        'Gajanan [17-April-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        If objDataOperation.ErrorMessage <> "" Then
                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        End If
                                                        'Gajanan [17-April-2019] -- End
                                                    End If
                                                Case Else
                                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString(), "ColumnVal").Tables("ColumnVal")
                                                    'Gajanan [17-April-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    End If
                                                    'Gajanan [17-April-2019] -- End
                                            End Select

                                        End If

                                    End If



                                    If IsNothing(dttemp) = False AndAlso dttemp.Rows.Count > 0 Then

                                        Dim oldValueDt As DataTable = Nothing
                                        Dim newValueDt As DataTable = Nothing


                                        If EmailDt.Rows(index)("iD").ToString = "int" Or (EmailDt.Rows(index)("iC").ToString() = "gender" AndAlso eScreenType = enScreenName.frmEmployeeRefereeList) Then


                                            If EmailDt.Rows(index)("iC").ToString() = "countryunkid" Or EmailDt.Rows(index)("iC").ToString() = "present_countryunkid" Or _
                                               EmailDt.Rows(index)("iC").ToString() = "domicile_countryunkid" Or EmailDt.Rows(index)("iC").ToString() = "rc_countryunkid" Or _
                                               EmailDt.Rows(index)("iC").ToString() = "emer_con_countryunkid" Or EmailDt.Rows(index)("iC").ToString() = "emer_con_countryunkid2" Or _
                                               EmailDt.Rows(index)("iC").ToString() = "emer_con_countryunkid3" Or EmailDt.Rows(index)("iC").ToString() = "birthcountryunkid" Or _
                                               EmailDt.Rows(index)("iC").ToString() = "nationalityunkid" Then

                                                If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                    If intNotificationType = 3 Or intNotificationType = 4 Then
                                                        newValueDt = objDataOperation.ExecQuery("select country_name from hrmsConfiguration..cfcountry_master where countryunkid = " & dr(idx)("countryunkid").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                        'Gajanan [17-April-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        If objDataOperation.ErrorMessage <> "" Then
                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        End If
                                                        'Gajanan [17-April-2019] -- End
                                                        If newValueDt.Rows.Count > 0 Then
                                                            NewValue = newValueDt.Rows(0)("country_name").ToString()
                                                        End If
                                                    Else


                                                        Select Case Oprationtype
                                                            Case enOperationType.ADDED
                                                                newValueDt = objDataOperation.ExecQuery("select country_name from hrmsConfiguration..cfcountry_master where countryunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                            Case enOperationType.DELETED
                                                                Select Case eScreenType
                                                                    Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                        newValueDt = objDataOperation.ExecQuery("select country_name from hrmsConfiguration..cfcountry_master where countryunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                        'Gajanan [17-April-2019] -- Start
                                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                        If objDataOperation.ErrorMessage <> "" Then
                                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                        End If
                                                                        'Gajanan [17-April-2019] -- End
                                                                    Case Else
                                                                        newValueDt = objDataOperation.ExecQuery("select country_name from hrmsConfiguration..cfcountry_master where countryunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                        'Gajanan [17-April-2019] -- Start
                                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                        If objDataOperation.ErrorMessage <> "" Then
                                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                        End If
                                                                        'Gajanan [17-April-2019] -- End
                                                                End Select

                                                        End Select

                                                        If newValueDt.Rows.Count > 0 Then
                                                            NewValue = newValueDt.Rows(0)("country_name").ToString()
                                                        End If
                                                    End If

                                                Else
                                                    newValueDt = objDataOperation.ExecQuery("select country_name from hrmsConfiguration..cfcountry_master where countryunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                    'Gajanan [17-April-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    End If
                                                    'Gajanan [17-April-2019] -- End
                                                    If newValueDt.Rows.Count > 0 Then
                                                        NewValue = newValueDt.Rows(0)("country_name").ToString()
                                                    End If

                                                    oldValueDt = objDataOperation.ExecQuery("select country_name from hrmsConfiguration..cfcountry_master where countryunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                    'Gajanan [17-April-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    End If
                                                    'Gajanan [17-April-2019] -- End
                                                    If oldValueDt.Rows.Count > 0 Then
                                                        OldValue = oldValueDt.Rows(0)("country_name").ToString()
                                                    End If


                                                End If

                                            ElseIf EmailDt.Rows(index)("iC").ToString() = "stateunkid" Or EmailDt.Rows(index)("iC").ToString() = "present_stateunkid" Or _
                                                    EmailDt.Rows(index)("iC").ToString() = "domicile_stateunkid" Or EmailDt.Rows(index)("iC").ToString() = "rc_stateunkid" Or _
                                                    EmailDt.Rows(index)("iC").ToString() = "emer_con_state" Or EmailDt.Rows(index)("iC").ToString() = "emer_con_state2" Or _
                                                EmailDt.Rows(index)("iC").ToString() = "emer_con_state3" Or EmailDt.Rows(index)("iC").ToString() = "birthstateunkid" Then
                                                If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                    If intNotificationType = 3 Or intNotificationType = 4 Then
                                                        newValueDt = objDataOperation.ExecQuery("select name from hrmsConfiguration..cfstate_master where stateunkid = " & dr(idx)("stateunkid").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                        'Gajanan [17-April-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        If objDataOperation.ErrorMessage <> "" Then
                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        End If
                                                        'Gajanan [17-April-2019] -- End
                                                        If newValueDt.Rows.Count > 0 Then
                                                            NewValue = newValueDt.Rows(0)("name").ToString()
                                                        End If
                                                    Else

                                                        Select Case Oprationtype
                                                            Case enOperationType.ADDED
                                                                newValueDt = objDataOperation.ExecQuery("select name from hrmsConfiguration..cfstate_master where stateunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End


                                                            Case enOperationType.DELETED
                                                                Select Case eScreenType
                                                                    Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                        newValueDt = objDataOperation.ExecQuery("select name from hrmsConfiguration..cfstate_master where stateunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                        'Gajanan [17-April-2019] -- Start
                                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                        If objDataOperation.ErrorMessage <> "" Then
                                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                        End If
                                                                        'Gajanan [17-April-2019] -- End
                                                                    Case Else
                                                                        newValueDt = objDataOperation.ExecQuery("select name from hrmsConfiguration..cfstate_master where stateunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                        'Gajanan [17-April-2019] -- Start
                                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                        If objDataOperation.ErrorMessage <> "" Then
                                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                        End If
                                                                        'Gajanan [17-April-2019] -- End
                                                                End Select
                                                        End Select


                                                        If newValueDt.Rows.Count > 0 Then
                                                            NewValue = newValueDt.Rows(0)("name").ToString()
                                                        End If
                                                    End If

                                                Else
                                                    newValueDt = objDataOperation.ExecQuery("select name from hrmsConfiguration..cfstate_master where stateunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                    'Gajanan [17-April-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    End If
                                                    'Gajanan [17-April-2019] -- End
                                                    If newValueDt.Rows.Count > 0 Then
                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                    End If

                                                    oldValueDt = objDataOperation.ExecQuery("select name from hrmsConfiguration..cfstate_master where stateunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                    'Gajanan [17-April-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    End If
                                                    'Gajanan [17-April-2019] -- End
                                                    If oldValueDt.Rows.Count > 0 Then
                                                        OldValue = oldValueDt.Rows(0)("name").ToString()
                                                    End If


                                                End If

                                            ElseIf EmailDt.Rows(index)("iC").ToString() = "cityunkid" Or EmailDt.Rows(index)("iC").ToString() = "present_post_townunkid" Or _
                                                   EmailDt.Rows(index)("iC").ToString() = "domicile_post_townunkid" Or EmailDt.Rows(index)("iC").ToString() = "rc_post_townunkid" Or _
                                                   EmailDt.Rows(index)("iC").ToString() = "emer_con_post_townunkid" Or EmailDt.Rows(index)("iC").ToString() = "emer_con_post_townunkid2" Or _
                                               EmailDt.Rows(index)("iC").ToString() = "emer_con_post_townunkid3" Or EmailDt.Rows(index)("iC").ToString() = "birthcityunkid" Then
                                                If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                    If intNotificationType = 3 Or intNotificationType = 4 Then
                                                        newValueDt = objDataOperation.ExecQuery("select name from hrmsConfiguration..cfcity_master where cityunkid = " & dr(idx)("cityunkid").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                        'Gajanan [17-April-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        If objDataOperation.ErrorMessage <> "" Then
                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        End If
                                                        'Gajanan [17-April-2019] -- End
                                                        If newValueDt.Rows.Count > 0 Then
                                                            NewValue = newValueDt.Rows(0)("name").ToString()
                                                        End If
                                                    Else
                                                        Select Case Oprationtype
                                                            Case enOperationType.ADDED
                                                                newValueDt = objDataOperation.ExecQuery("select name from hrmsConfiguration..cfcity_master where cityunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End
                                                            Case enOperationType.DELETED
                                                                Select Case eScreenType
                                                                    Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                        newValueDt = objDataOperation.ExecQuery("select name from hrmsConfiguration..cfcity_master where cityunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                        'Gajanan [17-April-2019] -- Start
                                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                        If objDataOperation.ErrorMessage <> "" Then
                                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                        End If
                                                                        'Gajanan [17-April-2019] -- End
                                                                    Case Else
                                                                        newValueDt = objDataOperation.ExecQuery("select name from hrmsConfiguration..cfcity_master where cityunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                        'Gajanan [17-April-2019] -- Start
                                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                        If objDataOperation.ErrorMessage <> "" Then
                                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                        End If
                                                                        'Gajanan [17-April-2019] -- End
                                                                End Select
                                                        End Select

                                                        If newValueDt.Rows.Count > 0 Then
                                                            NewValue = newValueDt.Rows(0)("name").ToString()
                                                        End If
                                                    End If

                                                Else
                                                    newValueDt = objDataOperation.ExecQuery("select name from hrmsConfiguration..cfcity_master where cityunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                    'Gajanan [17-April-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    End If
                                                    'Gajanan [17-April-2019] -- End
                                                    If newValueDt.Rows.Count > 0 Then
                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                    End If

                                                    oldValueDt = objDataOperation.ExecQuery("select name from hrmsConfiguration..cfcity_master where cityunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                    'Gajanan [17-April-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    End If
                                                    'Gajanan [17-April-2019] -- End
                                                    If oldValueDt.Rows.Count > 0 Then
                                                        OldValue = oldValueDt.Rows(0)("name").ToString()
                                                    End If


                                                End If

                                            ElseIf EmailDt.Rows(index)("iC").ToString() = "zipcodeunkid" Or EmailDt.Rows(index)("iC").ToString() = "present_postcodeunkid" Or _
                                                    EmailDt.Rows(index)("iC").ToString() = "domicile_postcodeunkid" Or EmailDt.Rows(index)("iC").ToString() = "rc_postcodeunkid" Or _
                                                    EmailDt.Rows(index)("iC").ToString() = "emer_con_postcodeunkid" Or EmailDt.Rows(index)("iC").ToString() = "emer_con_postcodeunkid2" Or _
                                                    EmailDt.Rows(index)("iC").ToString() = "emer_con_postcodeunkid3" Then
                                                If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                    If intNotificationType = 3 Or intNotificationType = 4 Then
                                                        newValueDt = objDataOperation.ExecQuery("select zipcode_no from hrmsConfiguration..cfzipcode_master where zipcodeunkid = " & dr(idx)("zipcodeunkid").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                        'Gajanan [17-April-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        If objDataOperation.ErrorMessage <> "" Then
                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        End If
                                                        'Gajanan [17-April-2019] -- End
                                                        If newValueDt.Rows.Count > 0 Then
                                                            NewValue = newValueDt.Rows(0)("zipcode_no").ToString()
                                                        End If
                                                    Else
                                                        Select Case Oprationtype
                                                            Case enOperationType.ADDED
                                                                newValueDt = objDataOperation.ExecQuery("select zipcode_no from hrmsConfiguration..cfzipcode_master where zipcodeunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End
                                                            Case enOperationType.DELETED
                                                                Select Case eScreenType
                                                                    Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                        newValueDt = objDataOperation.ExecQuery("select zipcode_no from hrmsConfiguration..cfzipcode_master where zipcodeunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                        'Gajanan [17-April-2019] -- Start
                                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                        If objDataOperation.ErrorMessage <> "" Then
                                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                        End If
                                                                        'Gajanan [17-April-2019] -- End
                                                                    Case Else
                                                                        newValueDt = objDataOperation.ExecQuery("select zipcode_no from hrmsConfiguration..cfzipcode_master where zipcodeunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                        'Gajanan [17-April-2019] -- Start
                                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                        If objDataOperation.ErrorMessage <> "" Then
                                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                        End If
                                                                        'Gajanan [17-April-2019] -- End
                                                                End Select
                                                        End Select

                                                        If newValueDt.Rows.Count > 0 Then
                                                            NewValue = newValueDt.Rows(0)("zipcode_no").ToString()
                                                        End If
                                                    End If

                                                Else
                                                    newValueDt = objDataOperation.ExecQuery("select zipcode_no from hrmsConfiguration..cfzipcode_master where zipcodeunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                    'Gajanan [17-April-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    End If
                                                    'Gajanan [17-April-2019] -- End
                                                    If newValueDt.Rows.Count > 0 Then
                                                        NewValue = newValueDt.Rows(0)("zipcode_no").ToString()
                                                    End If

                                                    oldValueDt = objDataOperation.ExecQuery("select zipcode_no from hrmsConfiguration..cfzipcode_master where zipcodeunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                    'Gajanan [17-April-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    End If
                                                    'Gajanan [17-April-2019] -- End
                                                    If oldValueDt.Rows.Count > 0 Then
                                                        OldValue = oldValueDt.Rows(0)("zipcode_no").ToString()
                                                    End If


                                                End If

                                            ElseIf EmailDt.Rows(index)("iC").ToString() = "nationalityunkid" Then
                                                If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                    If intNotificationType = 3 Or intNotificationType = 4 Then
                                                        newValueDt = objDataOperation.ExecQuery("select country_name from hrmsConfiguration..cfcountry_master where countryunkid = " & dr(idx)("nationalityunkid").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                        'Gajanan [17-April-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        If objDataOperation.ErrorMessage <> "" Then
                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        End If
                                                        'Gajanan [17-April-2019] -- End
                                                        If newValueDt.Rows.Count > 0 Then
                                                            NewValue = newValueDt.Rows(0)("country_name").ToString()
                                                        End If
                                                    Else

                                                        Select Case Oprationtype
                                                            Case enOperationType.ADDED
                                                                newValueDt = objDataOperation.ExecQuery("select country_name from hrmsConfiguration..cfcountry_master where countryunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End


                                                            Case enOperationType.DELETED
                                                                Select Case eScreenType
                                                                    Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                        newValueDt = objDataOperation.ExecQuery("select country_name from hrmsConfiguration..cfcountry_master where countryunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                        'Gajanan [17-April-2019] -- Start
                                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                        If objDataOperation.ErrorMessage <> "" Then
                                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                        End If
                                                                        'Gajanan [17-April-2019] -- End


                                                                    Case Else
                                                                        newValueDt = objDataOperation.ExecQuery("select country_name from hrmsConfiguration..cfcountry_master where countryunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                        'Gajanan [17-April-2019] -- Start
                                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                        If objDataOperation.ErrorMessage <> "" Then
                                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                        End If
                                                                        'Gajanan [17-April-2019] -- End

                                                                End Select
                                                        End Select


                                                        If newValueDt.Rows.Count > 0 Then
                                                            NewValue = newValueDt.Rows(0)("country_name").ToString()
                                                        End If
                                                    End If

                                                Else
                                                    newValueDt = objDataOperation.ExecQuery("select name from hrmsConfiguration..cfcountry_master where countryunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                    'Gajanan [17-April-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    End If
                                                    'Gajanan [17-April-2019] -- End
                                                    If newValueDt.Rows.Count > 0 Then
                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                    End If

                                                    oldValueDt = objDataOperation.ExecQuery("select name from hrmsConfiguration..cfcountry_master where countryunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                    'Gajanan [17-April-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    End If
                                                    'Gajanan [17-April-2019] -- End
                                                    If oldValueDt.Rows.Count > 0 Then
                                                        OldValue = oldValueDt.Rows(0)("name").ToString()
                                                    End If


                                                End If


                                            ElseIf EmailDt.Rows(index)("iC").ToString() = "gender" Then
                                                If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                    If intNotificationType = 3 Or intNotificationType = 4 Then
                                                        If dr(idx)("gender").ToString().Length > 0 Then
                                                            NewValue = dr(idx)("gender").ToString()
                                                        End If
                                                    Else

                                                        StrQ = "SELECT * from ( " & _
                                                               "SELECT @Select As Name, 0 As id UNION " & _
                                                               "SELECT @Male As Name, 1 As id UNION " & _
                                                               "SELECT @Female As Name, 2 As id " & _
                                                               ") as gender "
                                                        Select Case Oprationtype
                                                            Case enOperationType.ADDED
                                                                StrQ &= "WHERE gender.id = " & dttemp.Rows(0)("newval").ToString() & " "
                                                            Case enOperationType.DELETED
                                                                Select Case eScreenType
                                                                    Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                        StrQ &= "WHERE gender.id = " & dttemp.Rows(0)("oldval").ToString() & " "
                                                                    Case Else
                                                                        StrQ &= "WHERE gender.id = " & dttemp.Rows(0)("newval").ToString() & " "
                                                                End Select
                                                        End Select

                                                        objDataOperation.ClearParameters()
                                                        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 72, "Select"))
                                                        objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
                                                        objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

                                                        newValueDt = objDataOperation.ExecQuery(StrQ, "NewColvalue").Tables("NewColvalue")
                                                        'Gajanan [17-April-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                        If objDataOperation.ErrorMessage <> "" Then
                                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        End If
                                                        'Gajanan [17-April-2019] -- End
                                                        If newValueDt.Rows.Count > 0 Then
                                                            NewValue = newValueDt.Rows(0)("Name").ToString()
                                                        End If
                                                    End If

                                                Else

                                                    StrQ = "SELECT * from ( " & _
                                                                "SELECT @Select As Name, 0 As id UNION " & _
                                                                "SELECT @Male As Name, 1 As id UNION " & _
                                                                "SELECT @Female As Name, 1 As id " & _
                                                                ") as gender " & _
                                                                "WHERE gender.id = " & dttemp.Rows(0)("newval").ToString() & " "
                                                    objDataOperation.ClearParameters()
                                                    objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 72, "Select"))
                                                    objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
                                                    objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

                                                    newValueDt = objDataOperation.ExecQuery(StrQ, "NewColvalue").Tables("NewColvalue")
                                                    'Gajanan [17-April-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    End If
                                                    'Gajanan [17-April-2019] -- End
                                                    If newValueDt.Rows.Count > 0 Then
                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                    End If



                                                    StrQ = "SELECT * from ( " & _
                                                                "SELECT @Select As Name, 0 As id UNION " & _
                                                                "SELECT @Male As Name, 1 As id UNION " & _
                                                                "SELECT @Female As Name, 1 As id " & _
                                                                ") as gender " & _
                                                                "WHERE gender.id = " & dttemp.Rows(0)("oldval").ToString() & " "

                                                    objDataOperation.ClearParameters()
                                                    objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 72, "Select"))
                                                    objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
                                                    objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

                                                    oldValueDt = objDataOperation.ExecQuery(StrQ, "NewColvalue").Tables("NewColvalue")
                                                    'Gajanan [17-April-2019] -- Start
                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    If objDataOperation.ErrorMessage <> "" Then
                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                    End If
                                                    'Gajanan [17-April-2019] -- End

                                                    If oldValueDt.Rows.Count > 0 Then
                                                        OldValue = oldValueDt.Rows(0)("name").ToString()
                                                    End If


                                                End If

                                            Else
                                                Select Case eScreenType
                                                    Case enScreenName.frmEmployee_Skill_List


                                                        If EmailDt.Rows(index)("iC").ToString() = "skillcategoryunkid" Then
                                                            If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dr(idx)("skillcategoryunkid").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If

                                                                Else
                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                End If

                                                            Else
                                                                newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                                If newValueDt.Rows.Count > 0 Then
                                                                    NewValue = newValueDt.Rows(0)("name").ToString()
                                                                End If

                                                                oldValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End


                                                                If oldValueDt.Rows.Count > 0 Then
                                                                    OldValue = oldValueDt.Rows(0)("name").ToString()
                                                                End If

                                                            End If


                                                        ElseIf EmailDt.Rows(index)("iC").ToString() = "skillunkid" Then
                                                            If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                                    newValueDt = objDataOperation.ExecQuery("select skillname from hrskill_master where skillunkid = " & dr(idx)("skillunkid").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("skillname").ToString()
                                                                    End If

                                                                Else
                                                                    newValueDt = objDataOperation.ExecQuery("select skillname from hrskill_master where skillunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("skillname").ToString()
                                                                    End If
                                                                End If
                                                            Else
                                                                newValueDt = objDataOperation.ExecQuery("select skillname from hrskill_master where skillunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End
                                                                NewValue = newValueDt.Rows(0)("skillname").ToString()

                                                                oldValueDt = objDataOperation.ExecQuery("select skillname from hrskill_master where skillunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End
                                                                OldValue = oldValueDt.Rows(0)("skillname").ToString()
                                                            End If
                                                        End If

                                                    Case enScreenName.frmDependantsAndBeneficiariesList
                                                        If EmailDt.Rows(index)("iC").ToString() = "relationunkid" Then
                                                            If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dr(idx)("relationunkid").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End

                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                Else
                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                End If


                                                            Else
                                                                newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                                If newValueDt.Rows.Count > 0 Then
                                                                    NewValue = newValueDt.Rows(0)("name").ToString()
                                                                End If
                                                                oldValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                                'Sohail (18 May 2019) -- Start
                                                                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                                                                'If newValueDt.Rows.Count > 0 Then
                                                                If oldValueDt.Rows.Count > 0 Then
                                                                    'Sohail (18 May 2019) -- End
                                                                    OldValue = oldValueDt.Rows(0)("name").ToString()
                                                                End If
                                                            End If

                                                            'Sohail (18 May 2019) -- Start
                                                            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                                                        ElseIf EmailDt.Rows(index)("iC").ToString() = "reasonunkid" Then
                                                            If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dr(idx)("reasonunkid").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If

                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                Else
                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")

                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If

                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                End If


                                                            Else
                                                                newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")

                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If


                                                                If newValueDt.Rows.Count > 0 Then
                                                                    NewValue = newValueDt.Rows(0)("name").ToString()
                                                                End If
                                                                oldValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")

                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If

                                                                If oldValueDt.Rows.Count > 0 Then
                                                                    OldValue = oldValueDt.Rows(0)("name").ToString()
                                                                End If
                                                            End If
                                                            'Sohail (18 May 2019) -- End



                                                        End If
                                                    Case enScreenName.frmAddressList, enScreenName.frmBirthinfo
                                                        If EmailDt.Rows(index)("iC").ToString() = "present_provinceunkid" Or EmailDt.Rows(index)("iC").ToString() = "domicile_provinceunkid" Or _
                                                           EmailDt.Rows(index)("iC").ToString() = "recruitment_provinceunkid" Then
                                                            If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dr(idx)("present_provinceunkid").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End

                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                Else
                                                                    Select Case Oprationtype
                                                                        Case enOperationType.ADDED
                                                                            newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                            'Gajanan [17-April-2019] -- Start
                                                                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                            If objDataOperation.ErrorMessage <> "" Then
                                                                                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                            End If
                                                                            'Gajanan [17-April-2019] -- End
                                                                        Case enOperationType.DELETED
                                                                            Select Case eScreenType
                                                                                Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                                    'Gajanan [17-April-2019] -- Start
                                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                                    End If
                                                                                    'Gajanan [17-April-2019] -- End
                                                                                Case Else
                                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue") 'Gajanan [17-April-2019] -- Start
                                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                                    End If
                                                                                    'Gajanan [17-April-2019] -- End
                                                                            End Select
                                                                    End Select

                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                End If
                                                            Else
                                                                newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                                If newValueDt.Rows.Count > 0 Then
                                                                    NewValue = newValueDt.Rows(0)("name").ToString()
                                                                End If
                                                                oldValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                                If oldValueDt.Rows.Count > 0 Then
                                                                    OldValue = oldValueDt.Rows(0)("name").ToString()
                                                                End If
                                                            End If
                                                        End If

                                                        If EmailDt.Rows(index)("iC").ToString() = "present_roadunkid" Or EmailDt.Rows(index)("iC").ToString() = "domicile_roadunkid" Or _
                                                           EmailDt.Rows(index)("iC").ToString() = "recruitment_roadunkid" Then
                                                            If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dr(idx)("present_roadunkid").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End

                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                Else

                                                                    Select Case Oprationtype
                                                                        Case enOperationType.ADDED
                                                                            newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                            'Gajanan [17-April-2019] -- Start
                                                                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                            If objDataOperation.ErrorMessage <> "" Then
                                                                                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                            End If
                                                                            'Gajanan [17-April-2019] -- End


                                                                        Case enOperationType.DELETED
                                                                            Select Case eScreenType
                                                                                Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                                    'Gajanan [17-April-2019] -- Start
                                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                                    End If
                                                                                    'Gajanan [17-April-2019] -- End
                                                                                Case Else
                                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                                    'Gajanan [17-April-2019] -- Start
                                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                                    End If
                                                                                    'Gajanan [17-April-2019] -- End
                                                                            End Select
                                                                    End Select


                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                End If
                                                            Else
                                                                newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                                If newValueDt.Rows.Count > 0 Then
                                                                    NewValue = newValueDt.Rows(0)("name").ToString()
                                                                End If
                                                                oldValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                                If oldValueDt.Rows.Count > 0 Then
                                                                    OldValue = oldValueDt.Rows(0)("name").ToString()
                                                                End If
                                                            End If
                                                        End If
                                                        If EmailDt.Rows(index)("iC").ToString() = "present_chiefdomunkid" Or EmailDt.Rows(index)("iC").ToString() = "domicile_chiefdomunkid" Or _
                                                       EmailDt.Rows(index)("iC").ToString() = "recruitment_chiefdomunkid" Or EmailDt.Rows(index)("iC").ToString() = "birthchiefdomunkid" Then
                                                            If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dr(idx)("").ToString() & "present_chiefdomunkid", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End

                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                Else

                                                                    Select Case Oprationtype
                                                                        Case enOperationType.ADDED
                                                                            newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                            'Gajanan [17-April-2019] -- Start
                                                                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                            If objDataOperation.ErrorMessage <> "" Then
                                                                                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                            End If
                                                                            'Gajanan [17-April-2019] -- End


                                                                        Case enOperationType.DELETED
                                                                            Select Case eScreenType
                                                                                Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                                    'Gajanan [17-April-2019] -- Start
                                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                                    End If
                                                                                    'Gajanan [17-April-2019] -- End
                                                                                Case Else
                                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                                    'Gajanan [17-April-2019] -- Start
                                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                                    End If
                                                                                    'Gajanan [17-April-2019] -- End
                                                                            End Select
                                                                    End Select


                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                End If
                                                            Else
                                                                newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                                If newValueDt.Rows.Count > 0 Then
                                                                    NewValue = newValueDt.Rows(0)("name").ToString()
                                                                End If
                                                                oldValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                                If oldValueDt.Rows.Count > 0 Then
                                                                    OldValue = oldValueDt.Rows(0)("name").ToString()
                                                                End If
                                                            End If
                                                        End If
                                                        If EmailDt.Rows(index)("iC").ToString() = "present_villageunkid" Or EmailDt.Rows(index)("iC").ToString() = "domicile_villageunkid" Or _
                                                       EmailDt.Rows(index)("iC").ToString() = "recruitment_villageunkid" Or EmailDt.Rows(index)("iC").ToString() = "birthvillageunkid" Then
                                                            If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dr(idx)("").ToString() & "present_villageunkid", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End

                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                Else
                                                                    Select Case Oprationtype
                                                                        Case enOperationType.ADDED
                                                                            newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                            'Gajanan [17-April-2019] -- Start
                                                                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                            If objDataOperation.ErrorMessage <> "" Then
                                                                                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                            End If
                                                                            'Gajanan [17-April-2019] -- End


                                                                        Case enOperationType.DELETED
                                                                            Select Case eScreenType
                                                                                Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                                    'Gajanan [17-April-2019] -- Start
                                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                                    End If
                                                                                    'Gajanan [17-April-2019] -- End
                                                                                Case Else
                                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                                    'Gajanan [17-April-2019] -- Start
                                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                                    End If
                                                                                    'Gajanan [17-April-2019] -- End
                                                                            End Select
                                                                    End Select


                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                End If
                                                            Else
                                                                newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                                If newValueDt.Rows.Count > 0 Then
                                                                    NewValue = newValueDt.Rows(0)("name").ToString()
                                                                End If
                                                                oldValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                                If oldValueDt.Rows.Count > 0 Then
                                                                    OldValue = oldValueDt.Rows(0)("name").ToString()
                                                                End If
                                                            End If
                                                        End If

                                                        If EmailDt.Rows(index)("iC").ToString() = "present_town1unkid" Or EmailDt.Rows(index)("iC").ToString() = "domicile_town1unkid" Or _
                                                       EmailDt.Rows(index)("iC").ToString() = "recruitment_town1unkid" Or EmailDt.Rows(index)("iC").ToString() = "birthtownunkid" Then
                                                            If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dr(idx)("").ToString() & "present_town1unkid", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End

                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                Else

                                                                    Select Case Oprationtype
                                                                        Case enOperationType.ADDED
                                                                            newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                            'Gajanan [17-April-2019] -- Start
                                                                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                            If objDataOperation.ErrorMessage <> "" Then
                                                                                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                            End If
                                                                            'Gajanan [17-April-2019] -- End


                                                                        Case enOperationType.DELETED
                                                                            Select Case eScreenType
                                                                                Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                                    'Gajanan [17-April-2019] -- Start
                                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                                    End If
                                                                                    'Gajanan [17-April-2019] -- End
                                                                                Case Else
                                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                                    'Gajanan [17-April-2019] -- Start
                                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                                    End If
                                                                                    'Gajanan [17-April-2019] -- End
                                                                            End Select
                                                                    End Select

                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                End If
                                                            Else
                                                                newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                                If newValueDt.Rows.Count > 0 Then
                                                                    NewValue = newValueDt.Rows(0)("name").ToString()
                                                                End If
                                                                oldValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                                If oldValueDt.Rows.Count > 0 Then
                                                                    OldValue = oldValueDt.Rows(0)("name").ToString()
                                                                End If
                                                            End If
                                                        End If

                                                    Case enScreenName.frmEmployeeRefereeList
                                                        If EmailDt.Rows(index)("iC").ToString() = "relationunkid" Then
                                                            If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dr(idx)("relationunkid").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If

                                                                Else
                                                                    Select Case Oprationtype
                                                                        Case enOperationType.ADDED
                                                                            newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                            'Gajanan [17-April-2019] -- Start
                                                                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                            If objDataOperation.ErrorMessage <> "" Then
                                                                                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                            End If
                                                                            'Gajanan [17-April-2019] -- End
                                                                        Case enOperationType.DELETED
                                                                            Select Case eScreenType
                                                                                Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                                    'Gajanan [17-April-2019] -- Start
                                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                                    End If
                                                                                    'Gajanan [17-April-2019] -- End
                                                                                Case Else
                                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                                    'Gajanan [17-April-2019] -- Start
                                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                                    End If
                                                                                    'Gajanan [17-April-2019] -- End
                                                                            End Select
                                                                    End Select

                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                End If
                                                            End If
                                                        End If

                                                    Case enScreenName.frmOtherinfo
                                                        If EmailDt.Rows(index)("iC").ToString() = "complexionunkid" Or EmailDt.Rows(index)("iC").ToString() = "bloodgroupunkid" Or _
                                                           EmailDt.Rows(index)("iC").ToString() = "eyecolorunkid" Or EmailDt.Rows(index)("iC").ToString() = "ethnicityunkid" Or _
                                                            EmailDt.Rows(index)("iC").ToString() = "religionunkid" Or EmailDt.Rows(index)("iC").ToString() = "hairunkid" Or _
                                                            EmailDt.Rows(index)("iC").ToString() = "language1unkid" Or EmailDt.Rows(index)("iC").ToString() = "language2unkid" Or _
                                                            EmailDt.Rows(index)("iC").ToString() = "language3unkid" Or EmailDt.Rows(index)("iC").ToString() = "language4unkid" Or _
                                                            EmailDt.Rows(index)("iC").ToString() = "maritalstatusunkid" Then
                                                            If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dr(idx)("").ToString() & "present_town1unkid", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End

                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                Else
                                                                    Select Case Oprationtype
                                                                        Case enOperationType.ADDED
                                                                            newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                            'Gajanan [17-April-2019] -- Start
                                                                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                            If objDataOperation.ErrorMessage <> "" Then
                                                                                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                            End If
                                                                            'Gajanan [17-April-2019] -- End
                                                                        Case enOperationType.DELETED
                                                                            Select Case eScreenType
                                                                                Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                                    'Gajanan [17-April-2019] -- Start
                                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                                    End If
                                                                                    'Gajanan [17-April-2019] -- End


                                                                                Case Else
                                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                                    'Gajanan [17-April-2019] -- Start
                                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                                    End If
                                                                                    'Gajanan [17-April-2019] -- End

                                                                            End Select
                                                                    End Select


                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                End If
                                                            Else
                                                                newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                                If newValueDt.Rows.Count > 0 Then
                                                                    NewValue = newValueDt.Rows(0)("name").ToString()
                                                                End If
                                                                oldValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                'Gajanan [17-April-2019] -- Start
                                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                If objDataOperation.ErrorMessage <> "" Then
                                                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                End If
                                                                'Gajanan [17-April-2019] -- End

                                                                If oldValueDt.Rows.Count > 0 Then
                                                                    OldValue = oldValueDt.Rows(0)("name").ToString()
                                                                End If
                                                            End If
                                                        End If

                                                    Case enScreenName.frmQualificationsList
                                                        If EmailDt.Rows(index)("iC").ToString() = "qualificationgroupunkid" Then
                                                            If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dr(idx)("qualificationgroupunkid").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If

                                                                Else
                                                                    newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If
                                                                End If
                                                            End If

                                                        ElseIf EmailDt.Rows(index)("iC").ToString() = "qualificationunkid" Then
                                                            If Oprationtype = enOperationType.ADDED Then

                                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                                    newValueDt = objDataOperation.ExecQuery("select qualificationname from hrqualification_master where qualificationunkid = " & dr(idx)("qualificationunkid").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("name").ToString()
                                                                    End If

                                                                Else
                                                                    newValueDt = objDataOperation.ExecQuery("select qualificationname from hrqualification_master where qualificationunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("qualificationname").ToString()
                                                                    End If
                                                                End If
                                                            End If


                                                        ElseIf EmailDt.Rows(index)("iC").ToString() = "instituteunkid" Then
                                                            If Oprationtype = enOperationType.ADDED Then

                                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                                    newValueDt = objDataOperation.ExecQuery("select institute_name from hrinstitute_master where instituteunkid = " & dr(idx)("instituteunkid").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("institute_name").ToString()
                                                                    End If

                                                                Else
                                                                    newValueDt = objDataOperation.ExecQuery("select institute_name from hrinstitute_master where instituteunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("institute_name").ToString()
                                                                    End If
                                                                End If
                                                            End If


                                                        ElseIf EmailDt.Rows(index)("iC").ToString() = "resultunkid" Then
                                                            If Oprationtype = enOperationType.ADDED Then

                                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                                    newValueDt = objDataOperation.ExecQuery("select resultname from hrresult_master where resultunkid = " & dr(idx)("resultunkid").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("resultname").ToString()
                                                                    End If

                                                                Else
                                                                    newValueDt = objDataOperation.ExecQuery("select resultname from hrresult_master where resultunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                                    'Gajanan [17-April-2019] -- Start
                                                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                                    If objDataOperation.ErrorMessage <> "" Then
                                                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                                    End If
                                                                    'Gajanan [17-April-2019] -- End
                                                                    If newValueDt.Rows.Count > 0 Then
                                                                        NewValue = newValueDt.Rows(0)("resultname").ToString()
                                                                    End If
                                                                End If
                                                            End If
                                                        End If
                                                End Select
                                            End If

                                        ElseIf EmailDt.Rows(index)("iD").ToString = "nvarchar" Then
                                            If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                    NewValue = dr(idx)(EmailDt.Rows(index)("iC").ToString()).ToString()
                                                Else

                                                    Select Case Oprationtype
                                                        Case enOperationType.ADDED
                                                            NewValue = dttemp.Rows(0)("newval").ToString()
                                                        Case enOperationType.DELETED
                                                            Select Case eScreenType
                                                                Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                    NewValue = dttemp.Rows(0)("oldval").ToString()
                                                                Case Else
                                                                    NewValue = dttemp.Rows(0)("newval").ToString()
                                                            End Select
                                                    End Select

                                                End If


                                            Else



                                                OldValue = dttemp.Rows(0)("oldval").ToString()
                                                NewValue = dttemp.Rows(0)("newval").ToString()
                                            End If


                                        ElseIf EmailDt.Rows(index)("iD").ToString = "float" Then
                                            If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                    NewValue = dr(idx)(EmailDt.Rows(index)("iC").ToString()).ToString()
                                                Else

                                                    


                                                    Select Case Oprationtype
                                                        Case enOperationType.ADDED
                                                            If CDec(dttemp.Rows(0)("newval").ToString()) > 0 Then
                                                                NewValue = dttemp.Rows(0)("newval").ToString()
                                                            Else
                                                                NewValue = ""
                                                            End If

                                                        Case enOperationType.DELETED
                                                            Select Case eScreenType
                                                                Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                    If CDec(dttemp.Rows(0)("oldval").ToString()) > 0 Then
                                                                        NewValue = dttemp.Rows(0)("oldval").ToString()
                                                                    Else
                                                                        NewValue = ""
                                                                    End If

                                                                Case Else

                                                                    If CDec(dttemp.Rows(0)("newval").ToString()) > 0 Then
                                                                        NewValue = dttemp.Rows(0)("newval").ToString()
                                                                    Else
                                                                        NewValue = ""
                                                                    End If
                                                            End Select
                                                    End Select

                                                End If


                                            Else
                                                If CDec(dttemp.Rows(0)("oldval").ToString()) > 0 Then
                                                    OldValue = dttemp.Rows(0)("oldval").ToString()
                                                Else
                                                    OldValue = ""
                                                End If

                                                If CDec(dttemp.Rows(0)("newval").ToString()) > 0 Then
                                                    NewValue = dttemp.Rows(0)("newval").ToString()
                                                Else
                                                    NewValue = ""
                                                End If


                                            End If


                                        ElseIf EmailDt.Rows(index)("iD").ToString = "datetime" Then
                                            If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then
                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                    If IsDBNull(dr(idx)(EmailDt.Rows(index)("iC").ToString)) = False AndAlso _
                                                    CDate(dr(idx)(EmailDt.Rows(index)("iC").ToString())) <> Nothing Then
                                                        NewValue = CDate(dr(idx)(EmailDt.Rows(index)("iC").ToString())).ToShortDateString()
                                                    End If

                                                Else

                                                    Select Case Oprationtype
                                                        Case enOperationType.ADDED
                                                            If IsDBNull(dttemp.Rows(0)("newval")) = False AndAlso _
                                                                CDate(dttemp.Rows(0)("newval").ToString()) <> Nothing Then
                                                                NewValue = CDate(dttemp.Rows(0)("newval").ToString()).ToShortDateString()
                                                            End If


                                                        Case enOperationType.DELETED
                                                            Select Case eScreenType
                                                                Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                    If IsDBNull(dttemp.Rows(0)("oldval")) = False AndAlso _
                                                                    CDate(dttemp.Rows(0)("oldval").ToString()) <> Nothing Then
                                                                        NewValue = CDate(dttemp.Rows(0)("oldval").ToString()).ToShortDateString()
                                                                    End If


                                                                Case Else
                                                                    If IsDBNull(dttemp.Rows(0)("newval")) = False AndAlso _
                                                                   CDate(dttemp.Rows(0)("newval").ToString()) <> Nothing Then
                                                                        NewValue = CDate(dttemp.Rows(0)("newval").ToString()).ToShortDateString()
                                                                    End If

                                                            End Select
                                                    End Select

                                                End If

                                            Else

                                                If IsDBNull(dttemp.Rows(0)("oldval").ToString()) = False AndAlso _
                                                   dttemp.Rows(0)("oldval").ToString() <> "" AndAlso _
                                                    CDate(dttemp.Rows(0)("oldval").ToString()) <> Nothing Then
                                                    OldValue = CDate(dttemp.Rows(0)("oldval").ToString()).ToShortDateString()
                                                End If

                                                If IsDBNull(dttemp.Rows(0)("newval").ToString()) = False AndAlso _
                                                 dttemp.Rows(0)("newval").ToString() <> "" AndAlso _
                                                    CDate(dttemp.Rows(0)("newval").ToString()) <> Nothing Then
                                                    NewValue = CDate(dttemp.Rows(0)("newval").ToString()).ToShortDateString()
                                                End If

                                            End If

                                        ElseIf EmailDt.Rows(index)("iD").ToString = "bit" Then
                                            If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then

                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                    If IsDBNull(dr(idx)(EmailDt.Rows(index)("iC").ToString())) = False AndAlso _
                                                    CDate(dr(idx)(EmailDt.Rows(index)("iC").ToString())) <> Nothing Then
                                                        NewValue = CBool(dr(idx)(EmailDt.Rows(index)("iC").ToString())).ToString()
                                                    End If

                                                Else
                                                    Select Case Oprationtype
                                                        Case enOperationType.ADDED
                                                            NewValue = CBool(dttemp.Rows(0)("newval").ToString()).ToString()


                                                        Case enOperationType.DELETED
                                                            Select Case eScreenType
                                                                Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                    NewValue = CBool(dttemp.Rows(0)("oldval").ToString()).ToString()
                                                                Case Else
                                                                    NewValue = CBool(dttemp.Rows(0)("newval").ToString()).ToString()
                                                            End Select
                                                    End Select

                                                End If

                                            Else
                                                OldValue = CBool(dttemp.Rows(0)("oldval").ToString()).ToString()
                                                NewValue = CBool(dttemp.Rows(0)("newval").ToString()).ToString()
                                            End If


                                        ElseIf EmailDt.Rows(index)("iD").ToString = "decimal" Then
                                            If Oprationtype = enOperationType.ADDED Then

                                                If intNotificationType = 3 Or intNotificationType = 4 Then
                                                    If IsDBNull(dr(idx)(EmailDt.Rows(index)("iC").ToString())) = False AndAlso _
                                                    CDate(dr(idx)(EmailDt.Rows(index)("iC").ToString())) <> Nothing Then
                                                        NewValue = CBool(dr(idx)(EmailDt.Rows(index)("iC").ToString())).ToString()
                                                    End If

                                                Else

                                                    Select Case Oprationtype
                                                        Case enOperationType.ADDED
                                                            NewValue = dttemp.Rows(0)("newval").ToString()


                                                        Case enOperationType.DELETED
                                                            Select Case eScreenType
                                                                Case enScreenName.frmAddressList, enScreenName.frmEmergencyAddressList, enScreenName.frmOtherinfo, enScreenName.frmBirthinfo
                                                                    NewValue = dttemp.Rows(0)("oldval").ToString()


                                                                Case Else
                                                                    NewValue = dttemp.Rows(0)("newval").ToString()

                                                            End Select
                                                    End Select

                                                End If

                                            Else
                                                OldValue = dttemp.Rows(0)("oldval").ToString()
                                                NewValue = dttemp.Rows(0)("newval").ToString()
                                            End If

                                        End If


                                        If NewValue <> OldValue Then
                                            IsChangeExist = True

                                            'Sohail (18 May 2019) -- Start
                                            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                                            If strPrevDepn <> strDepn Then

                                                Select Case eScreenType

                                                    Case enScreenName.frmDependantsAndBeneficiariesList
                                                        MasterDataStrMsg.Append("<TR>")
                                                        If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then
                                                            MasterDataStrMsg.Append("<TD colspan = '2' align = 'LEFT' STYLE='WIDTH:10%;Background-Color:#ccc'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & strDepn & "</span></b></TD>")
                                                        Else
                                                            MasterDataStrMsg.Append("<TD colspan = '3' align = 'LEFT' STYLE='WIDTH:10%;Background-Color:#ccc'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & strDepn & "</span></b></TD>")
                                                        End If
                                                        MasterDataStrMsg.Append("</TR>")

                                                End Select

                                            End If
                                            'Sohail (18 May 2019) -- End

                                            If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then
                                                MasterDataStrMsg.Append("<TR>")
                                                MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & ColumnName & "</span></b></TD>")
                                                MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & NewValue & "</span></b></TD>")
                                                MasterDataStrMsg.Append("</TR>")

                                            Else
                                                MasterDataStrMsg.Append("<TR>")
                                                MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & ColumnName & "</span></b></TD>")
                                                MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & NewValue & "</span></b></TD>")
                                                MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & OldValue & "</span></b></TD>")
                                                MasterDataStrMsg.Append("</TR>")
                                            End If

                                    End If
                                    End If

                                    'Sohail (18 May 2019) -- Start
                                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                                    Select Case eScreenType

                                        Case enScreenName.frmDependantsAndBeneficiariesList
                                            strPrevDepn = strDepn

                                    End Select
                                    'Sohail (18 May 2019) -- End

                                Next
                            End If
                        End If '#1

                        'Sohail (18 May 2019) -- Start
                        'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                        If eScreenType = enScreenName.frmDependantStatus Then
                            Exit For
                        End If
                        'Sohail (18 May 2019) -- End

                    Next





                    Dim Cols As String = ""
                    Dim OldDataTableName As String = ""
                    Dim OldValue1 As String = ""
                    Dim NewValue1 As String = ""
                    Select Case eScreenType
                        Case enScreenName.frmJobHistory_ExperienceList
                            Cols = "'benifitplanunkids'"
                        Case enScreenName.frmOtherinfo
                            Cols = "'allergiesunkids','disabilitiesunkids'"
                            OldDataTableName = "hrallergies_tran"
                    End Select

                    Select Case eScreenType
                        Case enScreenName.frmJobHistory_ExperienceList, enScreenName.frmOtherinfo
                            For idx As Integer = 0 To dr.Length - 1
                                Dim previous_benefit As DataTable = Nothing


                                StrQ = "DECLARE @UniqueCol AS NVARCHAR(MAX)" & _
                                       "DECLARE @Col AS NVARCHAR(MAX)" & _
                                       "DECLARE @ExcludeColNames AS NVARCHAR(MAX) " & _
                                   "DECLARE @table AS TABLE(iC NVARCHAR(MAX), iQ NVARCHAR(MAX),  iE NVARCHAR(MAX)) " & _
                                   "SET @UniqueCol = '" & UniqueCol & "' " & _
                                       "SET @ExcludeColNames = '''' " & _
                                   "INSERT INTO @table ([@table].iC, [@table].iQ, [@table].iE) " & _
                                            "SELECT " & _
                                               "A.COLUMN_NAME " & _
                                               ",a.iCol " & _
                                           ",a.iE " & _
                                            "FROM (SELECT " & _
                                        "'SELECT  STUFF ((SELECT '','' + #MasterColumn# FROM #MasterTable# as s JOIN  (SELECT CAST(Split.A.value(''.'', ''VARCHAR(100)'') AS INT) AS ovalue FROM (SELECT ' + s.COLUMN_NAME + ' ,CAST(''<M>'' + REPLACE(' + s.COLUMN_NAME + ', '','', ''</M><M>'') + ''</M>'' AS XML) AS string   FROM #tab1 WHERE isfinal=0 and isprocessed=0 and statusunkid = 1 "

                                If Oprationtype = enOperationType.ADDED AndAlso extrafilter.Length > 0 Then
                                    StrQ &= "and " & extrafilter.Replace("'", "''") & ""
                                Else
                                    StrQ &= "and ' + @UniqueCol + ' = " & dr(idx)(UniqueCol).ToString() & "  "
                                End If

                                Select Case eScreenType
                                    Case enScreenName.frmOtherinfo
                                        StrQ &= " AND isbirthinfo = 0 "
                                    Case Else

                                End Select

                                StrQ &= "  ) AS A CROSS APPLY string.nodes(''/M'') AS Split (A)) AS O  ON O.ovalue = s.#Masterunkidcol# ORDER BY s.#MasterColumn# FOR XML PATH ('''')) , 1, 1, '''' ) ' AS iCol " & _
                                        ", 'SELECT  STUFF ((SELECT '','' + s.name FROM cfcommon_master s JOIN  (SELECT CAST(Split.A.value(''.'', ''VARCHAR(100)'') AS INT) AS ovalue FROM (SELECT ' + s.COLUMN_NAME + ' ,CAST(''<M>'' + REPLACE(' + s.COLUMN_NAME + ', '','', ''</M><M>'') + ''</M>'' AS XML) AS string   FROM #tab2 WHERE  "

                                If Oprationtype = enOperationType.ADDED AndAlso extrafilter.Length > 0 Then
                                    StrQ &= " " & extrafilter.Replace("'", "''") & ""
                                Else
                                    StrQ &= " ' + @UniqueCol + ' = " & dr(idx)(UniqueCol).ToString() & "  "
                                End If

                                StrQ &= "  ) AS A CROSS APPLY string.nodes(''/M'') AS Split (A)) AS O  ON O.ovalue = s.masterunkid ORDER BY s.name FOR XML PATH ('''')) , 1, 1, '''' ) ' AS iE " & _
                                                           ",s.COLUMN_NAME " & _
                                                           ",s.TABLE_NAME " & _
                                                        "FROM INFORMATION_SCHEMA.COLUMNS s " & _
                                                        "WHERE TABLE_NAME = '#tab1' " & _
                                                    "AND s.COLUMN_NAME IN (#Col)) AS A " & _
                                              "SELECT " & _
                                                 "[@table].iQ AS Step1 " & _
                                             ",[@table].iE AS Step21" & _
                                                 ",iC " & _
                                              "FROM @table "
                                StrQ = StrQ.Replace("#tab1", Table2)
                                StrQ = StrQ.Replace("#tab2", OldDataTableName)
                                StrQ = StrQ.Replace("#Col", Cols)


                                'Gajanan [5-Dec-2019] -- Start   
                                'Enhancement:Worked On ADD Attachment In Bio Data Experience  

                                Select Case eScreenType
                                    Case enScreenName.frmJobHistory_ExperienceList
                                        StrQ = StrQ.Replace("#MasterColumn#", "benefitplanname")
                                        StrQ = StrQ.Replace("#MasterTable#", "hrbenefitplan_master")
                                        StrQ = StrQ.Replace("#Masterunkidcol#", "benefitplanunkid")
                                    Case enScreenName.frmOtherinfo
                                        StrQ = StrQ.Replace("#MasterColumn#", "name")
                                        StrQ = StrQ.Replace("#MasterTable#", "cfcommon_master")
                                        StrQ = StrQ.Replace("#Masterunkidcol#", "masterunkid")
                                End Select
                                'Gajanan [5-Dec-2019] -- End




                                previous_benefit = objDataOperation.ExecQuery(StrQ, "previous_benefit").Tables("previous_benefit")
                                'Gajanan [17-April-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                If objDataOperation.ErrorMessage <> "" Then
                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                End If
                                'Gajanan [17-April-2019] -- End

                                If IsNothing(previous_benefit) = False AndAlso previous_benefit.Rows.Count > 0 Then
                                    'StrMessage.Append("<TR>")
                                    'StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & previous_benefit(idx)("iC").ToString() & "</span></b></TD>")

                                    Dim dttemp As DataTable = Nothing

                                    For idV As Integer = 0 To previous_benefit.Rows.Count - 1


                                        dttemp = objDataOperation.ExecQuery(previous_benefit(idV)("Step1").ToString(), "ColumnVal").Tables("ColumnVal")
                                        'Gajanan [17-April-2019] -- Start
                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                        If objDataOperation.ErrorMessage <> "" Then
                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        End If
                                        'Gajanan [17-April-2019] -- End
                                        NewValue1 = dttemp(0)("Column1").ToString()
                                        'StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & dttemp(0)("Column1").ToString() & "</span></b></TD>")

                                        StrQ = "SELECT " & _
                                               "STUFF " & _
                                               "((SELECT " & _
                                                         "',' + s.#mastercol " & _
                                                    "FROM #tab1 s " & _
                                                    "JOIN (SELECT DISTINCT " & _
                                                              "CAST(Split.a.value('.', 'VARCHAR(100)') AS INT) AS ovalue " & _
                                                         "FROM (SELECT " & _
                                                                       "CAST('<M>' + REPLACE(#tab2.#ColName, ',', '</M><M>') + '</M>' AS XML) AS String " & _
                                                              "FROM #tab2 " & _
                                                              "WHERE "
                                        If Oprationtype = enOperationType.ADDED AndAlso extrafilter.Length > 0 Then
                                            StrQ &= extrafilter
                                        Else
                                            StrQ &= " #UniqueCol= " & dr(idx)(UniqueCol).ToString() & ""
                                        End If
                                        StrQ &= ")AS A " & _
                                                         "CROSS APPLY String.nodes('/M') AS Split (a)) AS O " & _
                                                         "ON O.ovalue = s.#masterunkidcol " & _
                                                    "ORDER BY s.#mastercol " & _
                                                    "FOR XML PATH ('')) " & _
                                               ", " & _
                                               "1, 1, '' " & _
                                               ") as ColValue"

                                        Select Case eScreenType
                                            Case enScreenName.frmJobHistory_ExperienceList

                                                StrQ = StrQ.Replace("#tab1", "hrbenefitplan_master")
                                                StrQ = StrQ.Replace("#tab2", "hremp_experience_approval_tran")
                                                StrQ = StrQ.Replace("#ColName", "benifitplanunkids")
                                                StrQ = StrQ.Replace("#UniqueCol", UniqueCol)
                                                StrQ = StrQ.Replace("#mastercol", "benefitplanname")
                                                StrQ = StrQ.Replace("#masterunkidcol", "benefitplanunkid")


                                            Case enScreenName.frmOtherinfo
                                                StrQ = StrQ.Replace("#tab1", "cfcommon_master")
                                                StrQ = StrQ.Replace("#UniqueCol", UniqueCol)
                                                StrQ = StrQ.Replace("#mastercol", "name")
                                                StrQ = StrQ.Replace("#masterunkidcol", "masterunkid")
                                                If previous_benefit(idV)("iC").ToString() = "allergiesunkids" Then
                                                    StrQ = StrQ.Replace("#tab2", "hrallergies_tran")
                                                    StrQ = StrQ.Replace("#ColName", "allergiesunkid")

                                                ElseIf previous_benefit(idV)("iC").ToString() = "disabilitiesunkids" Then
                                                    StrQ = StrQ.Replace("#tab2", "hrdisabilities_tran")
                                                    StrQ = StrQ.Replace("#ColName", "disabilitiesunkid")
                                                End If

                                        End Select

                                        dttemp = objDataOperation.ExecQuery(StrQ, "previous_benefit").Tables("previous_benefit")
                                        'Gajanan [17-April-2019] -- Start
                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                        If objDataOperation.ErrorMessage <> "" Then
                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        End If
                                        'Gajanan [17-April-2019] -- End



                                        If IsNothing(dttemp) = False AndAlso dttemp.Rows.Count > 0 Then
                                            OldValue1 = dttemp(0)("ColValue").ToString()
                                            If NewValue1 <> OldValue1 Then
                                                IsChangeExist = True
                                                If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then
                                                    MasterDataStrMsg.Append("<TR>")
                                                    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & previous_benefit(idV)("iC").ToString() & "</span></b></TD>")
                                                    If Oprationtype = enOperationType.ADDED Then
                                                    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & NewValue1 & "</span></b></TD>")
                                                    ElseIf Oprationtype = enOperationType.DELETED Then
                                                        MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & OldValue1 & "</span></b></TD>")
                                                    End If
                                                    MasterDataStrMsg.Append("</TR>")
                                                Else
                                                    MasterDataStrMsg.Append("<TR>")
                                                    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & previous_benefit(idV)("iC").ToString() & "</span></b></TD>")
                                                    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & NewValue1 & "</span></b></TD>")
                                                    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & OldValue1 & "</span></b></TD>")
                                                    MasterDataStrMsg.Append("</TR>")
                                                End If
                                            End If
                                        End If


                                    Next


                                End If

                            Next

                    End Select
                    If MasterDataStrMsg.Length > 0 Then
                        StrMessage.Append(HeaderStrMsg)
                        StrMessage.Append(MasterDataStrMsg.ToString())
                        StrMessage.Append(vbCrLf)
                        StrMessage.Append("</TABLE>")
                    End If


                    Select Case eScreenType
                        Case enScreenName.frmDependantsAndBeneficiariesList

                            For idx As Integer = 0 To dr.Length - 1

                                Dim BenifitDt As DataTable = Nothing
                                Dim Membershipdt As DataTable = Nothing

                                Dim BenifitDependantQuery As String = String.Empty

                                BenifitDependantQuery = "DECLARE @ExcludeColNames AS NVARCHAR(MAX) " & _
                                       "DECLARE @MatchingColName AS NVARCHAR(MAX) " & _
                                       "DECLARE @UniqueColName AS NVARCHAR(MAX) " & _
                                       "DECLARE @table AS TABLE(iQ NVARCHAR(MAX),iC NVARCHAR(MAX), iD NVARCHAR(MAX), iM NVARCHAR(MAX),iU NVARCHAR(MAX),iQ2 NVARCHAR(MAX)) " & _
                                       "SET @MatchingColName ='#MatchingColName' " & _
                                       "SET @ExcludeColNames ='#ExcludeColNames' " & _
                                       "SET @UniqueColName = '#UniqueColName' " & _
                                       "INSERT INTO @table ([@table].iQ, [@table].iC, [@table].iD, [@table].iM, [@table].iU,[@table].iQ2) " & _
                                         "SELECT " & _
                                           "A.iCol " & _
                                          ",A.COLUMN_NAME " & _
                                          ",A.DATA_TYPE " & _
                                          ",ROW_NUMBER() OVER (ORDER BY A.ORDINAL_POSITION) " & _
                                          ",'#tab1' " & _
                                          ",a.iCol2 " & _
                                         "FROM (SELECT " & _
                                             "CASE " & _
                                               "WHEN @MatchingColName <> s.COLUMN_NAME THEN 'SELECT #tab1.' + s.COLUMN_NAME + ' As ovalue, #tab2.' + s.COLUMN_NAME + ' As nvalue,CASE #tab2.audittype WHEN 0 THEN '''' WHEN 1 THEN ''Newly Added'' WHEN 2 THEN ''Edited'' WHEN 3 THEN ''Deleted'' END  AS audittype,#tab2.audittype as audittypeid FROM #tab1,#tab2 WHERE #tab1.' + @MatchingColName + ' = #tab2.' + @MatchingColName + " & _
                                                 "' AND #tab2.approvaltranguid = ''#tranguid'' and #tab1.' + @UniqueColName + '= #tab2.' + @UniqueColName + ' and #tab2.audittype NOT IN (1,3)' " & _
                                             "END AS iCol " & _
                                             ",CASE " & _
                                               "WHEN @MatchingColName <> s.COLUMN_NAME THEN 'SELECT #tab2.' + s.COLUMN_NAME + ' As nvalue,CASE #tab2.audittype WHEN 0 THEN '''' WHEN 1 THEN ''Newly Added'' WHEN 2 THEN ''Edited'' WHEN 3 THEN ''Deleted'' END  AS audittype,#tab2.audittype as audittypeid FROM #tab2 WHERE #tab2.approvaltranguid = ''#tranguid''  and #tab2.audittype <> 2 ' " & _
                                             "END AS iCol2 " & _
                                            ",s.COLUMN_NAME " & _
                                            ",s.DATA_TYPE " & _
                                            ",s.ORDINAL_POSITION " & _
                                           "FROM INFORMATION_SCHEMA.COLUMNS s " & _
                                           "JOIN SYS.IDENTITY_COLUMNS AS I " & _
                                             "ON s.TABLE_NAME = OBJECT_NAME(I.object_id) " & _
                                           "WHERE TABLE_NAME = '#tab1' " & _
                                           "AND s.COLUMN_NAME <> I.name " & _
                                           "AND s.COLUMN_NAME NOT IN ('userunkid', 'isvoid', 'voiduserunkid', 'voiddatetime', 'voidreason', 'psoft_syncdatetime')) AS A " & _
                                         "WHERE A.iCol IS NOT NULL " & _
                                       " " & _
                                       "SELECT " & _
                                         "'INSERT INTO cfmessages (messagecode,module_name,message,message1,message2) ' + " & _
                                         "'SELECT ' + [@table].iM + ',''' + [@table].iU + ''',''' + REPLACE(REPLACE([@table].iC, '_', ' '), 'unkid', '') + ''',''' + REPLACE(REPLACE([@table].iC, '_', ' '), 'unkid', '') + ''',''' + REPLACE(REPLACE([@table].iC, '_', ' '), 'unkid', '') + ''' WHERE NOT EXISTS ' + " & _
                                         "'(SELECT * FROM cfmessages WHERE messagecode = ' + [@table].iM + ' AND module_name = ''' + [@table].iU + ''' AND message = ''' + REPLACE(REPLACE([@table].iC, '_', ' '), 'unkid', '') + ''' )' AS Step1 " & _
                                        ",[@table].iQ AS Step2 " & _
                                        ",[@table].iQ2 AS Step23 " & _
                                        ",'DECLARE @langId AS INT ' + " & _
                                         "'SET @langId = (SELECT languageunkid FROM hrmsConfiguration..cfuser_master WHERE userunkid = 1) ' + " & _
                                         "'SELECT ' + " & _
                                         "'CASE @langId ' + " & _
                                         "'WHEN 1 THEN message1 ' + " & _
                                         "'WHEN 2 THEN message2 ' + " & _
                                         "'ELSE message END AS msg ' + " & _
                                         "'FROM cfmessages ' + " & _
                                         "'WHERE messagecode = ''' + [@table].iM + ''' AND module_name = ''' + [@table].iU + '''' AS Step21 " & _
                                         ",iD " & _
                                         ",iC " & _
                                         ",iM " & _
                                       "FROM @table "

                                StrQ = BenifitDependantQuery
                                StrQ = StrQ.Replace("#tab1", "hrdependant_benefit_tran")
                                StrQ = StrQ.Replace("#tab2", "hrdependant_approval_benefit_tran")
                                StrQ = StrQ.Replace("#tranguid", dr(idx)("tranguid").ToString())
                                StrQ = StrQ.Replace("#MatchingColName", "dpndtbeneficetranunkid")
                                StrQ = StrQ.Replace("#ExcludeColNames", "''")
                                StrQ = StrQ.Replace("#UniqueColName", "dpndtbenefittranunkid")

                                BenifitDt = objDataOperation.ExecQuery(StrQ, "DeptBenifitslist").Tables("DeptBenifitslist")
                                'Gajanan [17-April-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                If objDataOperation.ErrorMessage <> "" Then
                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                End If
                                'Gajanan [17-April-2019] -- End


                                If BenifitDt.Rows.Count > 0 Then


                                    StrQ = ""
                                    StrQ = String.Join(" ", BenifitDt.AsEnumerable().Select(Function(x) x.Field(Of String)("step1")).ToArray())
                                    objDataOperation.ExecNonQuery(StrQ)

                                    Dim DeptBenifitsList As New DataTable
                                    Dim DeptBenifitsEditList As New DataTable

                                    DependantStrMsg.Append("<h3 style='text-decoration:underline'>" & Language.getMessage(mstrModuleName, 800, "BeniftInfo") & "</h3> ")
                                    DependantStrMsg.Append("<TABLE border = '1'>")
                                    DependantStrMsg.Append("<TR bgcolor= 'SteelBlue'>")

                                    DeptBenifitsList.Columns.Add(Language.getMessage(mstrModuleName, 801, "Benefit Group"))
                                    DependantStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 801, "Benefit Group") & "</span></b></TD>")
                                    DeptBenifitsList.Columns(Language.getMessage(mstrModuleName, 801, "Benefit Group")).ExtendedProperties.Add("1", "1")

                                    DeptBenifitsList.Columns.Add(Language.getMessage(mstrModuleName, 802, "Benefit Type"))
                                    DependantStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 802, "Benefit Type") & "</span></b></TD>")
                                    DeptBenifitsList.Columns(Language.getMessage(mstrModuleName, 802, "Benefit Type")).ExtendedProperties.Add("2", "2")

                                    DeptBenifitsList.Columns.Add(Language.getMessage(mstrModuleName, 803, "Value Basis"))
                                    DependantStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 803, "Value Basis") & "</span></b></TD>")
                                    DeptBenifitsList.Columns(Language.getMessage(mstrModuleName, 803, "Value Basis")).ExtendedProperties.Add("3", "3")

                                    DeptBenifitsList.Columns.Add(Language.getMessage(mstrModuleName, 804, "Percent"))
                                    DependantStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 804, "Percent") & "</span></b></TD>")
                                    DeptBenifitsList.Columns(Language.getMessage(mstrModuleName, 804, "Percent")).ExtendedProperties.Add("4", "4")

                                    DeptBenifitsList.Columns.Add(Language.getMessage(mstrModuleName, 805, "Amount"))
                                    DependantStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 805, "Amount") & "</span></b></TD>")
                                    DeptBenifitsList.Columns(Language.getMessage(mstrModuleName, 805, "Amount")).ExtendedProperties.Add("5", "5")

                                    DeptBenifitsList.Columns.Add(Language.getMessage(mstrModuleName, 806, "Opration"))
                                    DependantStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 806, "Opration") & "</span></b></TD>")
                                    DeptBenifitsList.Columns(Language.getMessage(mstrModuleName, 806, "Opration")).ExtendedProperties.Add("5", "5")

                                    DependantStrMsg.Append("</TR>")

                                    DeptBenifitsEditList = DeptBenifitsList.Copy()

                                    For Benifitindex As Integer = 0 To BenifitDt.Rows.Count - 1


                                        Dim iValue As Integer = 0
                                        Dim newValueDt As DataTable = Nothing
                                        Dim newoldValueDt As DataTable = Nothing

                                        newValueDt = objDataOperation.ExecQuery(BenifitDt(Benifitindex)("Step23").ToString(), "ColumnValue").Tables("ColumnValue")
                                        'Gajanan [17-April-2019] -- Start
                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                        If objDataOperation.ErrorMessage <> "" Then
                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        End If
                                        'Gajanan [17-April-2019] -- End
                                        newoldValueDt = objDataOperation.ExecQuery(BenifitDt(Benifitindex)("Step2").ToString(), "ColumnValue").Tables("ColumnValue")
                                        'Gajanan [17-April-2019] -- Start
                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                        If objDataOperation.ErrorMessage <> "" Then
                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        End If
                                        'Gajanan [17-April-2019] -- End


                                        If IsNothing(newoldValueDt) = False AndAlso newoldValueDt.Rows.Count > 0 Then
                                            For newvalidx As Integer = 0 To newoldValueDt.Rows.Count - 1
                                                If DeptBenifitsEditList.Rows.Count < newoldValueDt.Rows.Count Then

                                                    DeptBenifitsEditList.Rows.Add(DeptBenifitsEditList.NewRow())
                                                    DeptBenifitsEditList.Rows(DeptBenifitsEditList.Rows.Count - 1)(DeptBenifitsEditList.Columns.Count - 1) = newoldValueDt.Rows(newvalidx)("audittype").ToString()
                                                End If


                                                Dim xCol As String = DeptBenifitsEditList.Columns().Cast(Of DataColumn).AsEnumerable().Where(Function(x) x.ExtendedProperties.ContainsKey(BenifitDt.Rows(Benifitindex)("iM"))).Select(Function(x) x.ColumnName).FirstOrDefault()
                                                If xCol IsNot Nothing Then
                                                    If newoldValueDt.Rows(newvalidx)(0).ToString() <> newoldValueDt.Rows(newvalidx)(1).ToString() Then
                                                        If BenifitDt.Rows(Benifitindex)("iD").ToString() = "int" Then
                                                            DeptBenifitsEditList.Rows(newvalidx)(xCol) = getDependantColumnValue(objDataOperation, BenifitDt.Rows(Benifitindex)("iC").ToString(), newoldValueDt.Rows(newvalidx)(0).ToString() + "<BR>" + newoldValueDt.Rows(newvalidx)(1).ToString() + "</BR>")
                                                        Else
                                                            DeptBenifitsEditList.Rows(newvalidx)(xCol) = newoldValueDt.Rows(newvalidx)(0).ToString() + "<BR>" + newoldValueDt.Rows(newvalidx)(1).ToString() + "</BR>"
                                                        End If
                                                    Else
                                                        If BenifitDt.Rows(Benifitindex)("iD").ToString() = "int" Then
                                                            DeptBenifitsEditList.Rows(newvalidx)(xCol) = getDependantColumnValue(objDataOperation, BenifitDt.Rows(Benifitindex)("iC").ToString(), newoldValueDt.Rows(newvalidx)(0).ToString())
                                                        Else
                                                            DeptBenifitsEditList.Rows(newvalidx)(xCol) = newoldValueDt.Rows(newvalidx)(0).ToString()
                                                        End If
                                                    End If
                                                End If
                                            Next

                                        End If


                                        If IsNothing(newValueDt) = False AndAlso newValueDt.Rows.Count > 0 Then
                                            For newvalidx As Integer = 0 To newValueDt.Rows.Count - 1

                                                If DeptBenifitsList.Rows.Count < newValueDt.Rows.Count Then
                                                    DeptBenifitsList.Rows.Add(DeptBenifitsList.NewRow())
                                                    DeptBenifitsList.Rows(DeptBenifitsList.Rows.Count - 1)(DeptBenifitsList.Columns.Count - 1) = newValueDt.Rows(newvalidx)("audittype").ToString()

                                                End If


                                                Dim xCol As String = DeptBenifitsList.Columns().Cast(Of DataColumn).AsEnumerable().Where(Function(x) x.ExtendedProperties.ContainsKey(BenifitDt.Rows(Benifitindex)("iM"))).Select(Function(x) x.ColumnName).FirstOrDefault()
                                                If xCol IsNot Nothing Then

                                                    If BenifitDt.Rows(Benifitindex)("iD").ToString() = "int" Then
                                                        DeptBenifitsList.Rows(newvalidx)(xCol) = getDependantColumnValue(objDataOperation, BenifitDt.Rows(Benifitindex)("iC").ToString(), newValueDt.Rows(newvalidx)(0).ToString())
                                                    Else
                                                        DeptBenifitsList.Rows(newvalidx)(xCol) = newValueDt.Rows(newvalidx)(0).ToString()
                                                    End If

                                                End If
                                            Next
                                        End If
                                    Next


                                    'Gajanan [17-April-2019] -- Start
                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.

                                    If DeptBenifitsList.Rows.Count = 0 Then
                                        DependantStrMsg.Length = 0
                                    Else
                                        IsChangeExist = True
                                    End If
                                    'Gajanan [17-April-2019] -- End

                                    For Each row As DataRow In DeptBenifitsList.Rows
                                        DependantStrMsg.Append("<TR>")
                                        For cIndex As Integer = 0 To DeptBenifitsList.Columns.Count - 1
                                            DependantStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & row(cIndex).ToString() & "</span></b></TD>")
                                        Next
                                        DependantStrMsg.Append("</TR>")
                                    Next

                                    For Each row As DataRow In DeptBenifitsEditList.Rows
                                        DependantStrMsg.Append("<TR>")
                                        For cIndex As Integer = 0 To DeptBenifitsList.Columns.Count - 1

                                            If row(cIndex).ToString().Contains("<BR>") Then
                                                DependantStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'>")
                                                DependantStrMsg.Append("<b style='display:block;border-bottom:1px solid #000;'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & row(cIndex).ToString().Replace("<BR>", "<b>(old)</b></span></b><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>").Replace("</BR>", String.Empty) & "<b>(new)</b></span></b>")
                                            Else
                                                DependantStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & row(cIndex).ToString() & "</span></b></TD>")
                                            End If
                                        Next
                                        DependantStrMsg.Append("</TR>")
                                    Next
                                    DependantStrMsg.Append("</TABLE>")
                                End If



                                '----------------------------------------Membership--------------------------------
                                StrQ = BenifitDependantQuery
                                StrQ = StrQ.Replace("#tab1", "hrdependant_membership_tran")
                                StrQ = StrQ.Replace("#tab2", "hrdependant_approval_membership_tran")
                                StrQ = StrQ.Replace("#tranguid", dr(idx)("tranguid").ToString())
                                StrQ = StrQ.Replace("#MatchingColName", "dpndtbeneficetranunkid")
                                StrQ = StrQ.Replace("#ExcludeColNames", "''")
                                StrQ = StrQ.Replace("#UniqueColName", "dpndtmembershiptranunkid")

                                Membershipdt = objDataOperation.ExecQuery(StrQ, "DeptMembershipslist").Tables("DeptMembershipslist")
                                'Gajanan [17-April-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                If objDataOperation.ErrorMessage <> "" Then
                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                End If
                                'Gajanan [17-April-2019] -- End


                                If Membershipdt.Rows.Count > 0 Then


                                    StrQ = ""
                                    StrQ = String.Join(" ", Membershipdt.AsEnumerable().Select(Function(x) x.Field(Of String)("step1")).ToArray())
                                    objDataOperation.ExecNonQuery(StrQ)

                                    Dim DeptMembershipsList As New DataTable
                                    Dim DeptMembershipsEditList As New DataTable

                                    DependantMembershipStrMsg.Append("<h3 style='text-decoration:underline'>" & Language.getMessage(mstrModuleName, 911, "Membership Info") & "</h3> ")
                                    DependantMembershipStrMsg.Append("<TABLE border = '1'>")
                                    DependantMembershipStrMsg.Append("<TR bgcolor= 'SteelBlue'>")

                                    DeptMembershipsList.Columns.Add(Language.getMessage(mstrModuleName, 807, "Membership Type"))
                                    DependantMembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 807, "Membership Type") & "</span></b></TD>")
                                    DeptMembershipsList.Columns(Language.getMessage(mstrModuleName, 807, "Membership Type")).ExtendedProperties.Add("1", "1")

                                    DeptMembershipsList.Columns.Add(Language.getMessage(mstrModuleName, 808, "Membership"))
                                    DependantMembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 808, "Membership") & "</span></b></TD>")
                                    DeptMembershipsList.Columns(Language.getMessage(mstrModuleName, 808, "Membership")).ExtendedProperties.Add("2", "2")

                                    DeptMembershipsList.Columns.Add(Language.getMessage(mstrModuleName, 809, "Membership No"))
                                    DependantMembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 809, "Membership No") & "</span></b></TD>")
                                    DeptMembershipsList.Columns(Language.getMessage(mstrModuleName, 809, "Membership No")).ExtendedProperties.Add("3", "3")


                                    DeptMembershipsList.Columns.Add(Language.getMessage(mstrModuleName, 806, "Opration"))
                                    DependantMembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 806, "Opration") & "</span></b></TD>")
                                    DeptMembershipsList.Columns(Language.getMessage(mstrModuleName, 806, "Opration")).ExtendedProperties.Add("5", "5")


                                    DependantMembershipStrMsg.Append("</TR>")

                                    DeptMembershipsEditList = DeptMembershipsList.Copy()

                                    For Membershipindex As Integer = 0 To Membershipdt.Rows.Count - 1
                                        Dim iValue As Integer = 0
                                        Dim newValueDt As DataTable = Nothing
                                        Dim newoldValueDt As DataTable = Nothing

                                        newValueDt = objDataOperation.ExecQuery(Membershipdt(Membershipindex)("Step23").ToString(), "ColumnValue").Tables("ColumnValue")
                                        'Gajanan [17-April-2019] -- Start
                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                        If objDataOperation.ErrorMessage <> "" Then
                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        End If
                                        'Gajanan [17-April-2019] -- End
                                        newoldValueDt = objDataOperation.ExecQuery(Membershipdt(Membershipindex)("Step2").ToString(), "ColumnValue").Tables("ColumnValue")
                                        'Gajanan [17-April-2019] -- Start
                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                        If objDataOperation.ErrorMessage <> "" Then
                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        End If
                                        'Gajanan [17-April-2019] -- End


                                        If IsNothing(newoldValueDt) = False AndAlso newoldValueDt.Rows.Count > 0 Then
                                            For newvalidx As Integer = 0 To newoldValueDt.Rows.Count - 1
                                                If DeptMembershipsEditList.Rows.Count < newoldValueDt.Rows.Count Then
                                                    DeptMembershipsEditList.Rows.Add(DeptMembershipsEditList.NewRow())
                                                    DeptMembershipsEditList.Rows(DeptMembershipsEditList.Rows.Count - 1)(DeptMembershipsEditList.Columns.Count - 1) = newoldValueDt.Rows(newvalidx)("audittype").ToString()
                                                End If


                                                Dim xCol As String = DeptMembershipsEditList.Columns().Cast(Of DataColumn).AsEnumerable().Where(Function(x) x.ExtendedProperties.ContainsKey(Membershipdt.Rows(Membershipindex)("iM"))).Select(Function(x) x.ColumnName).FirstOrDefault()
                                                If xCol IsNot Nothing Then
                                                    If newoldValueDt.Rows(newvalidx)(0).ToString() <> newoldValueDt.Rows(newvalidx)(1).ToString() Then
                                                        If Membershipdt.Rows(Membershipindex)("iD").ToString() = "int" Then
                                                            DeptMembershipsEditList.Rows(newvalidx)(xCol) = getDependantColumnValue(objDataOperation, Membershipdt.Rows(Membershipindex)("iC").ToString(), newoldValueDt.Rows(newvalidx)(0).ToString() + "<BR>" + newoldValueDt.Rows(newvalidx)(1).ToString() + "</BR>")
                                                        Else
                                                            DeptMembershipsEditList.Rows(newvalidx)(xCol) = newoldValueDt.Rows(newvalidx)(0).ToString() + "<BR>" + newoldValueDt.Rows(newvalidx)(1).ToString() + "</BR>"
                                                        End If
                                                    Else
                                                        If Membershipdt.Rows(Membershipindex)("iD").ToString() = "int" Then
                                                            DeptMembershipsEditList.Rows(newvalidx)(xCol) = getDependantColumnValue(objDataOperation, Membershipdt.Rows(Membershipindex)("iC").ToString(), newoldValueDt.Rows(newvalidx)(0).ToString())
                                                        Else
                                                            DeptMembershipsEditList.Rows(newvalidx)(xCol) = newoldValueDt.Rows(newvalidx)(0).ToString()
                                                        End If
                                                    End If
                                                End If
                                            Next

                                        End If


                                        If IsNothing(newValueDt) = False AndAlso newValueDt.Rows.Count > 0 Then
                                            For newvalidx As Integer = 0 To newValueDt.Rows.Count - 1

                                                If DeptMembershipsList.Rows.Count < newValueDt.Rows.Count Then
                                                    DeptMembershipsList.Rows.Add(DeptMembershipsList.NewRow())
                                                    DeptMembershipsList.Rows(DeptMembershipsList.Rows.Count - 1)(DeptMembershipsList.Columns.Count - 1) = newValueDt.Rows(newvalidx)("audittype").ToString()

                                                End If


                                                Dim xCol As String = DeptMembershipsList.Columns().Cast(Of DataColumn).AsEnumerable().Where(Function(x) x.ExtendedProperties.ContainsKey(Membershipdt.Rows(Membershipindex)("iM"))).Select(Function(x) x.ColumnName).FirstOrDefault()
                                                If xCol IsNot Nothing Then

                                                    If Membershipdt.Rows(Membershipindex)("iD").ToString() = "int" Then
                                                        DeptMembershipsList.Rows(newvalidx)(xCol) = getDependantColumnValue(objDataOperation, Membershipdt.Rows(Membershipindex)("iC").ToString(), newValueDt.Rows(newvalidx)(0).ToString())
                                                    Else
                                                        DeptMembershipsList.Rows(newvalidx)(xCol) = newValueDt.Rows(newvalidx)(0).ToString()
                                                    End If

                                                End If
                                            Next
                                        End If
                                    Next


                                    'Gajanan [17-April-2019] -- Start
                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.



                                    If DeptMembershipsList.Rows.Count = 0 Then
                                        DependantMembershipStrMsg.Length = 0
                                    Else
                                        IsChangeExist = True
                                    End If
                                    'Gajanan [17-April-2019] -- End


                                    For Each row As DataRow In DeptMembershipsList.Rows
                                        DependantMembershipStrMsg.Append("<TR>")
                                        For cIndex As Integer = 0 To DeptMembershipsList.Columns.Count - 1
                                            DependantMembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & row(cIndex).ToString() & "</span></b></TD>")
                                        Next
                                        DependantMembershipStrMsg.Append("</TR>")
                                    Next

                                    For Each row As DataRow In DeptMembershipsEditList.Rows
                                        DependantMembershipStrMsg.Append("<TR>")
                                        For cIndex As Integer = 0 To DeptMembershipsList.Columns.Count - 1

                                            If row(cIndex).ToString().Contains("<BR>") Then
                                                DependantMembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'>")
                                                DependantMembershipStrMsg.Append("<b style='display:block;border-bottom:1px solid #000;'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & row(cIndex).ToString().Replace("<BR>", "<b>(old)</b></span></b><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>").Replace("</BR>", String.Empty) & "<b>(new)</b></span></b>")
                                            Else
                                                DependantMembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & row(cIndex).ToString() & "</span></b></TD>")
                                            End If
                                        Next
                                        DependantMembershipStrMsg.Append("</TR>")
                                    Next
                                    DependantMembershipStrMsg.Append("</TABLE>")
                                End If
                            Next

                            '------------------------------------------Identity--------------------------
                        Case enScreenName.frmIdentityInfoList
                            If IsNothing(mdtTable) = False AndAlso mdtTable.Select("AUD <> ''").Length > 0 Then


                                If mdtTable.Rows.Count > 0 Then
                                IsChangeExist = True
                                End If


                                If isfromapproval Then
                                    mdtTable = mdtTable.Select("isgrp =0 and icheck= true").CopyToDataTable()
                                End If


                                IdentityStrMsg.Append("<h3 style='text-decoration:underline'>" & Language.getMessage(mstrModuleName, 810, "Identity Information") & "</h3> ")
                                IdentityStrMsg.Append("<TABLE border = '1'>")
                                IdentityStrMsg.Append("<TR bgcolor= 'SteelBlue'>")
                                IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 811, "Identity Type") & "</span></b></TD>")
                                IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 812, "Serial No") & "</span></b></TD>")
                                IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 813, "Identity No") & "</span></b></TD>")
                                IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 814, "Country") & "</span></b></TD>")
                                IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 815, "Place Of Issue") & "</span></b></TD>")
                                IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 816, "DL Class") & "</span></b></TD>")
                                IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 817, "Issue Date") & "</span></b></TD>")
                                IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 818, "Expiry Date") & "</span></b></TD>")
                                IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 819, "IsDefault") & "</span></b></TD>")
                                IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 820, "Opration") & "</span></b></TD>")




                                IdentityStrMsg.Append("</TR>")



                                Dim mdtoldEmpIdentity As DataTable = Nothing

                                Dim Newval As String = ""
                                Dim Oldval As String = ""
                                Dim FinalVal As String = ""

                                Dim strCols As String() = {"identities", "serial_no", "identity_no", "country", "issued_place", "dl_class", "issue_date", "expiry_date", "isdefault"}
                                Dim objEmpIdTran As New clsIdentity_tran

                                attachmentnewadded = False
                                attachmentdeleted = False

                                Dim identityDt As DataView = mdtTable.DefaultView
                                identityDt.RowFilter = "AUD <> ''"
                                mdtTable = identityDt.ToTable()

                                For row As Integer = 0 To mdtTable.Rows.Count - 1


                                    If mdtTable.Rows(row)("newattachdocumentid").ToString().Length > 0 Then
                                        attachmentnewadded = True
                                    End If

                                    If mdtTable.Rows(row)("deleteattachdocumentid").ToString().Length > 0 Then
                                        attachmentdeleted = True
                                    End If


                                    IdentityStrMsg.Append("<TR>")

                                    For index As Integer = 0 To strCols.Length - 1
                                        mdtTable.Rows(row).Item(strCols(index)) = mdtTable.Rows(row).Item(strCols(index))
                                        Newval = ""
                                        Oldval = ""
                                        FinalVal = ""


                                        StrQ = ""
                                        If (isfromapproval = False AndAlso CInt(mdtTable.Rows(row)("identitytranunkid")) > 0) Or _
                                           isfromapproval AndAlso Oprationtype = enOperationType.EDITED Then

                                            Select Case strCols(index).ToString()

                                                Case "identities"
                                                    StrQ = "Select cfcommon_master.name as oldval from hremployee_idinfo_tran  " & _
                                                           "left join cfcommon_master on cfcommon_master.masterunkid= hremployee_idinfo_tran.idtypeunkid " & _
                                                           "where identitytranunkid = " & mdtTable.Rows(row).Item("identitytranunkid").ToString() & ""

                                                Case "serial_no"
                                                    StrQ = "Select hremployee_idinfo_tran.serial_no as oldval from hremployee_idinfo_tran  " & _
                                                          "where identitytranunkid = " & mdtTable.Rows(row).Item("identitytranunkid").ToString() & ""

                                                Case "identity_no"
                                                    StrQ = "Select hremployee_idinfo_tran.identity_no as oldval from hremployee_idinfo_tran  " & _
                                                        "where identitytranunkid = " & mdtTable.Rows(row).Item("identitytranunkid").ToString() & ""

                                                Case "country"
                                                    StrQ = "Select hrmsConfiguration..cfcountry_master.country_name as oldval from hremployee_idinfo_tran  " & _
                                                           "left join hrmsConfiguration..cfcountry_master on hrmsConfiguration..cfcountry_master.countryunkid= hremployee_idinfo_tran.countryunkid " & _
                                                           "where identitytranunkid = " & mdtTable.Rows(row).Item("identitytranunkid").ToString() & ""

                                                Case "issued_place"
                                                    StrQ = "Select hremployee_idinfo_tran.issued_place as oldval from hremployee_idinfo_tran  " & _
                                                        "where identitytranunkid = " & mdtTable.Rows(row).Item("identitytranunkid").ToString() & ""

                                                Case "dl_class"
                                                    StrQ = "Select hremployee_idinfo_tran.dl_class as oldval from hremployee_idinfo_tran  " & _
                                                       "where identitytranunkid = " & mdtTable.Rows(row).Item("identitytranunkid").ToString() & ""

                                                Case "issue_date"
                                                    StrQ = "Select hremployee_idinfo_tran.issue_date as oldval from hremployee_idinfo_tran  " & _
                                                       "where identitytranunkid = " & mdtTable.Rows(row).Item("identitytranunkid").ToString() & ""

                                                Case "expiry_date"
                                                    StrQ = "Select hremployee_idinfo_tran.expiry_date as oldval from hremployee_idinfo_tran  " & _
                                                       "where identitytranunkid = " & mdtTable.Rows(row).Item("identitytranunkid").ToString() & ""

                                                Case "isdefault"
                                                    StrQ = "Select hremployee_idinfo_tran.isdefault as oldval from hremployee_idinfo_tran  " & _
                                                       "where identitytranunkid = " & mdtTable.Rows(row).Item("identitytranunkid").ToString() & ""

                                            End Select

                                        End If


                                        If StrQ.Length > 0 Then
                                            mdtoldEmpIdentity = objDataOperation.ExecQuery(StrQ, "mdtoldEmpIdentity").Tables("mdtoldEmpIdentity")
                                            'Gajanan [17-April-2019] -- Start
                                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                            If objDataOperation.ErrorMessage <> "" Then
                                                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            End If
                                            'Gajanan [17-April-2019] -- End
                                        End If


                                        If mdtTable.Rows(row).Item(strCols(index)).ToString().Length > 0 Then

                                            If (isfromapproval = False AndAlso CInt(mdtTable.Rows(row)("identitytranunkid")) > 0) Or _
                                                Oprationtype = enOperationType.EDITED Then

                                               

                                                Select Case strCols(index).ToString()

                                                    Case "issue_date", "expiry_date"
                                                        Newval = CDate(mdtTable.Rows(row).Item(strCols(index)).ToString()).ToShortDateString()
                                                        Oldval = CDate(mdtoldEmpIdentity.Rows(0)("oldval").ToString()).ToShortDateString()

                                                    Case Else
                                                        Newval = mdtTable.Rows(row).Item(strCols(index)).ToString()
                                                        Oldval = mdtoldEmpIdentity.Rows(0)("oldval").ToString()
                                                End Select

                                            ElseIf (isfromapproval = False And mdtTable.Rows(row)("AUD").ToString() = "A") Or _
                                                Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Or _
                                                (isfromapproval = False And mdtTable.Rows(row)("AUD").ToString() = "D") Then

                                                'Gajanan [17-April-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.



                                                'Newval = mdtTable.Rows(row).Item(strCols(index)).ToString()
                                                'Gajanan [17-April-2019] -- End


                                                Select Case strCols(index).ToString()

                                                    Case "issue_date", "expiry_date"
                                                        Newval = CDate(mdtTable.Rows(row).Item(strCols(index)).ToString()).ToShortDateString()

                                                        'Gajanan [17-April-2019] -- Start
                                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                                    Case Else
                                                        Newval = mdtTable.Rows(row).Item(strCols(index)).ToString()
                                                        'Gajanan [17-April-2019] -- End

                                                End Select

                                            End If

                                        End If


                                        If Newval.Length > 0 AndAlso Oldval.Length > 0 Then

                                            If Newval.Trim.ToString() = Oldval.Trim.ToString() Then
                                                FinalVal = Newval.Trim.ToString()
                                            Else
                                                FinalVal = Newval.Trim.ToString() & "(" & Oldval.Trim.ToString() & ")"
                                            End If
                                        Else
                                            FinalVal = Newval.Trim.ToString()
                                        End If

                                        IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & FinalVal & "</span></b></TD>")

                                        'Gajanan [17-April-2019] -- Start
                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                        Newval = ""
                                        Oldval = ""
                                        'Gajanan [17-April-2019] -- End

                                    Next

                                    If (isfromapproval = False) Then

                                        If CInt(mdtTable.Rows(row)("identitytranunkid")) > 0 Then
                                            IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 822, "Edited") & "</span></b></TD>")
                                        Else
                                            Select Case mdtTable.Rows(row)("AUD").ToString()

                                                Case "A"
                                                    IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 821, "Newly Added") & "</span></b></TD>")

                                                Case "U"
                                                    IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 822, "Edited") & "</span></b></TD>")

                                                Case "D"
                                                    IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 823, "Deleted") & "</span></b></TD>")
                                            End Select
                                        End If
                                    Else

                                        If Oprationtype = enOperationType.ADDED Then
                                            IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 821, "Newly Added") & "</span></b></TD>")

                                        ElseIf Oprationtype = enOperationType.EDITED Then
                                            IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 822, "Edited") & "</span></b></TD>")

                                        ElseIf Oprationtype = enOperationType.DELETED Then
                                            IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 823, "Deleted") & "</span></b></TD>")

                                        End If

                                    End If


                                    IdentityStrMsg.Append("</TR>")
                                Next
                                IdentityStrMsg.Append("</TABLE>")
                            End If

                            '------------------------------------------Membership------------------------
                        Case enScreenName.frmMembershipInfoList

                            If mdtTable.Select("AUD <> ''").Length > 0 Then

                                If mdtTable.Rows.Count > 0 Then
                                    IsChangeExist = True
                                End If


                                If isfromapproval Then
                                    mdtTable = mdtTable.Select("isgrp =0 and icheck= true").CopyToDataTable()
                                End If

                                MembershipStrMsg.Append("<h3 style='text-decoration:underline'>" & Language.getMessage(mstrModuleName, 831, "Membership Information") & "</h3> ")
                                MembershipStrMsg.Append("<TABLE border = '1'>")
                                MembershipStrMsg.Append("<TR bgcolor= 'SteelBlue'>")
                                MembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 824, "Membership Category") & "</span></b></TD>")
                                MembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 825, "Membership") & "</span></b></TD>")
                                MembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 826, "Membership No") & "</span></b></TD>")
                                MembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 827, "Issue Date") & "</span></b></TD>")
                                MembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 828, "Start Date") & "</span></b></TD>")
                                MembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 829, "Expiry Date") & "</span></b></TD>")
                                MembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 830, "Remark") & "</span></b></TD>")
                                MembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 820, "Opration") & "</span></b></TD>")




                                MembershipStrMsg.Append("</TR>")



                                Dim mdtoldEmpMembership As DataTable = Nothing

                                Dim Newval As String = ""
                                Dim Oldval As String = ""
                                Dim FinalVal As String = ""

                                If mdtTable.Columns.Contains("Category") Then
                                    mdtTable.Columns("Category").ColumnName = "membership_category"
                                End If

                                Dim strCols As String() = {"membership_category", "membershipname", "membershipno", "issue_date", "start_date", "expiry_date", "remark"}
                                Dim objEmpIdTran As New clsIdentity_tran


                                Dim MembershipDt As DataView = mdtTable.DefaultView
                                MembershipDt.RowFilter = "AUD <> ''"
                                mdtTable = MembershipDt.ToTable()

                                For row As Integer = 0 To mdtTable.Rows.Count - 1
                                    MembershipStrMsg.Append("<TR>")

                                    For index As Integer = 0 To strCols.Length - 1
                                        mdtTable.Rows(row).Item(strCols(index)) = mdtTable.Rows(row).Item(strCols(index))
                                        Newval = ""
                                        Oldval = ""
                                        FinalVal = ""

                                        StrQ = ""

                                        'Gajanan [17-April-2019] -- Start
                                        'Enhancement - Implementing Employee Approver Flow On Employee Data.

                                        'If (isfromapproval = False AndAlso mdtTable.Rows(row)("AUD").ToString() = "U") Or _
                                        '   isfromapproval AndAlso Oprationtype = enOperationType.EDITED Then

                                        If (isfromapproval = False AndAlso CInt(mdtTable.Rows(row)("membershiptranunkid")) > 0) Or _
                                         isfromapproval AndAlso Oprationtype = enOperationType.EDITED Then
                                            'Gajanan [17-April-2019] -- End

                                            Select Case strCols(index).ToString()

                                                'Gajanan [17-April-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.




                                                Case "membership_category"
                                                    StrQ = "Select cfcommon_master.name as oldval from cfcommon_master LEFT JOIN hremployee_meminfo_tran " & _
                                                           " on cfcommon_master.masterunkid= hremployee_meminfo_tran.membership_categoryunkid where cfcommon_master.masterunkid = " & mdtTable.Rows(row).Item("membership_categoryunkid").ToString() & " "

                                                Case "membershipname"
                                                    StrQ = "Select hrmembership_master.membershipname as oldval from hrmembership_master " & _
                                                          "left join hremployee_meminfo_tran on hrmembership_master.membershipunkid= hremployee_meminfo_tran.membershipunkid where hrmembership_master.membershipunkid = " & mdtTable.Rows(row).Item("membershipunkid").ToString() & " "

                                                Case "membershipno"
                                                    StrQ = "Select hremployee_meminfo_tran.membershipno as oldval from hremployee_meminfo_tran  " & _
                                                        "where membershiptranunkid = " & mdtTable.Rows(row).Item("membershiptranunkid").ToString() & ""

                                                Case "issue_date"
                                                    StrQ = "Select hremployee_meminfo_tran.issue_date as oldval from hremployee_meminfo_tran  " & _
                                                       "where membershiptranunkid = " & mdtTable.Rows(row).Item("membershiptranunkid").ToString() & ""

                                                Case "start_date"
                                                    StrQ = "Select hremployee_meminfo_tran.start_date as oldval from hremployee_meminfo_tran  " & _
                                                       "where membershiptranunkid = " & mdtTable.Rows(row).Item("membershiptranunkid").ToString() & ""

                                                Case "expiry_date"
                                                    StrQ = "Select hremployee_meminfo_tran.expiry_date as oldval from hremployee_meminfo_tran  " & _
                                                       "where membershiptranunkid = " & mdtTable.Rows(row).Item("membershiptranunkid").ToString() & ""

                                                Case "remark"
                                                    StrQ = "Select hremployee_meminfo_tran.remark as oldval from hremployee_meminfo_tran  " & _
                                                        "where membershiptranunkid = " & mdtTable.Rows(row).Item("membershiptranunkid").ToString() & ""

                                                    'Gajanan [17-April-2019] -- End
                                            End Select

                                        End If


                                        If StrQ.Length > 0 Then
                                            mdtoldEmpMembership = objDataOperation.ExecQuery(StrQ, "mdtoldEmpMembership").Tables("mdtoldEmpMembership")
                                            'Gajanan [17-April-2019] -- Start
                                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                            If objDataOperation.ErrorMessage <> "" Then
                                                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            End If
                                            'Gajanan [17-April-2019] -- End
                                        End If


                                        If mdtTable.Rows(row).Item(strCols(index)).ToString().Length > 0 Then


                                            'Gajanan [17-April-2019] -- Start
                                            'Enhancement - Implementing Employee Approver Flow On Employee Data.



                                            'If (isfromapproval = False And mdtTable.Rows(row)("AUD").ToString() = "U") Or _
                                            '    Oprationtype = enOperationType.EDITED Then

                                            If (isfromapproval = False AndAlso CInt(mdtTable.Rows(row)("membershiptranunkid")) > 0) Or _
                                          Oprationtype = enOperationType.EDITED Then

                                                'Gajanan [17-April-2019] -- End


                                                'Gajanan [17-April-2019] -- Start
                                                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                                                'Newval = mdtTable.Rows(row)("").ToString()
                                                'Oldval = mdtoldEmpIdentity.Rows(0)("oldval").ToString()
                                                Select Case strCols(index).ToString()

                                                    Case "issue_date", "start_date", "expiry_date"
                                                        Newval = CDate(mdtTable.Rows(row).Item(strCols(index)).ToString()).ToShortDateString()
                                                        Oldval = CDate(mdtoldEmpMembership.Rows(0)("oldval").ToString()).ToShortDateString()

                                                    Case Else
                                                        Newval = mdtTable.Rows(row).Item(strCols(index)).ToString()
                                                        Oldval = mdtoldEmpMembership.Rows(0)("oldval").ToString()
                                                End Select
                                                'Gajanan [17-April-2019] -- End


                                            ElseIf (isfromapproval = False And mdtTable.Rows(row)("AUD").ToString() = "A") Or _
                                                Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Or _
                                                (isfromapproval = False And mdtTable.Rows(row)("AUD").ToString() = "D") Then



                                                Select Case strCols(index).ToString()

                                                    Case "issue_date", "start_date", "expiry_date"
                                                        Newval = CDate(mdtTable.Rows(row).Item(strCols(index)).ToString()).ToShortDateString()

                                                    Case Else
                                                        Newval = mdtTable.Rows(row).Item(strCols(index)).ToString()
                                                End Select
                                            End If
                                        End If


                                        If Newval.Length > 0 AndAlso Oldval.Length > 0 Then
                                            If Newval.Trim.ToString() = Oldval.Trim.ToString() Then

                                                FinalVal = Newval
                                            Else
                                                FinalVal = Newval & "(" & Oldval & ")"
                                            End If
                                        Else
                                            FinalVal = Newval
                                        End If

                                        MembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & FinalVal & "</span></b></TD>")
                                        Newval = ""
                                        Oldval = ""

                                    Next


                                    'Gajanan [17-April-2019] -- Start
                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.



                                    If (isfromapproval = False) Then

                                        If CInt(mdtTable.Rows(row)("membershiptranunkid")) > 0 Then
                                            MembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 822, "Edited") & "</span></b></TD>")
                                        Else
                                            Select Case mdtTable.Rows(row)("AUD").ToString()

                                                Case "A"
                                                    MembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 821, "Newly Added") & "</span></b></TD>")

                                                Case "U"
                                                    MembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 822, "Edited") & "</span></b></TD>")

                                                Case "D"
                                                    MembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 823, "Deleted") & "</span></b></TD>")
                                            End Select
                                        End If
                                    Else

                                        If Oprationtype = enOperationType.ADDED Then
                                            MembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 821, "Newly Added") & "</span></b></TD>")

                                        ElseIf Oprationtype = enOperationType.EDITED Then
                                            MembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 822, "Edited") & "</span></b></TD>")

                                        ElseIf Oprationtype = enOperationType.DELETED Then
                                            MembershipStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 823, "Deleted") & "</span></b></TD>")

                                        End If

                                    End If
                                    'Gajanan [17-April-2019] -- End


                                    MembershipStrMsg.Append("</TR>")
                                Next
                                MembershipStrMsg.Append("</TABLE>")
                            End If


                            'Sohail (18 May 2019) -- Start
                            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                        Case enScreenName.frmDependantStatus
                            If IsNothing(mdtTable) = False Then
                                IsChangeExist = True
                                'If isfromapproval Then
                                '    mdtTable = mdtTable.Select("isgrp =0 and icheck= true").CopyToDataTable()
                                'End If

                                'Sohail (03 Jul 2019) -- Start
                                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                                StrQ = ""
                                StrQ = String.Join(" ", EmailDt.AsEnumerable().Select(Function(x) x.Field(Of String)("step1")).ToArray())

                                objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                End If
                                'Sohail (03 Jul 2019) -- End

                                'IdentityStrMsg.Append("<h3 style='text-decoration:underline'>" & Language.getMessage(mstrModuleName, 810, "Identity Information") & "</h3> ")
                                'IdentityStrMsg.Append("<TABLE border = '1'>")
                                'IdentityStrMsg.Append("<TR bgcolor= 'SteelBlue'>")
                                'IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 811, "Identity Type") & "</span></b></TD>")
                                'IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 812, "Serial No") & "</span></b></TD>")
                                'IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 813, "Identity No") & "</span></b></TD>")
                                'IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 814, "Country") & "</span></b></TD>")
                                'IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 815, "Place Of Issue") & "</span></b></TD>")
                                'IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 816, "DL Class") & "</span></b></TD>")
                                'IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 817, "Issue Date") & "</span></b></TD>")
                                'IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 818, "Expiry Date") & "</span></b></TD>")
                                'IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 819, "IsDefault") & "</span></b></TD>")
                                'IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 820, "Opration") & "</span></b></TD>")
                                DependantStatusMsg.Append("<h3 style='text-decoration:underline'>" & Language.getMessage(mstrModuleName, 916, "Dependant Status") & "</h3> ")
                                DependantStatusMsg.Append("<TABLE border = '1'>")
                                DependantStatusMsg.Append(vbCrLf)
                                DependantStatusMsg.Append("<TR bgcolor= 'SteelBlue'>")
                                DependantStatusMsg.Append(vbCrLf)

                                Dim OldValue As String = ""
                                Dim NewValue As String = ""
                                Dim FinalVal As String = ""
                                Dim ColumnName = ""
                                Dim dttemp As DataTable = Nothing

                                'DependantStatusMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 300, "Employee") & "</span></b></TD>")
                                DependantStatusMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 400, "Dependant") & "</span></b></TD>")

                                For index As Integer = 0 To EmailDt.Rows.Count - 1
                                    dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step21").ToString(), "ColumnName").Tables("ColumnName")
                                    If objDataOperation.ErrorMessage <> "" Then
                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    End If

                                    ColumnName = dttemp.Rows(0)("msg").ToString()

                                    DependantStatusMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & ColumnName & "</span></b></TD>")

                                Next
                                DependantStatusMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 820, "Opration") & "</span></b></TD>")
                                DependantStatusMsg.Append("</TR>")




                                'If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then
                                '    HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 300, "Employee") & "</span></b></TD>")
                                '    HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 400, "Dependant") & "</span></b></TD>")
                                '    HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 200, "Particulars") & "</span></b></TD>")
                                '    HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 201, "Values") & "</span></b></TD>")
                                'Else
                                '    HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 500, "Employee") & "</span></b></TD>")
                                '    HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 600, "Dependant") & "</span></b></TD>")
                                '    HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 202, "Particular") & "</span></b></TD>")
                                '    HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 203, "New Value") & "</span></b></TD>")
                                '    HeaderStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & Language.getMessage(mstrModuleName, 204, "Old Value") & "</span></b></TD>")
                                'End If

                                'HeaderStrMsg.Append("</TR>")
                                'IdentityStrMsg.Append("</TR>")



                                'Dim mdtoldEmpIdentity As DataTable = Nothing

                                'Dim OldValue As String = ""
                                'Dim NewValue As String = ""
                                'Dim ColumnName = ""
                                'Dim dttemp As DataTable = Nothing
                                'Dim strCols As String() = {"dteffective_date", "isactive", "reason"}


                                attachmentnewadded = False
                                attachmentdeleted = False

                                'Dim identityDt As DataView = mdtTable.DefaultView
                                'identityDt.RowFilter = "AUD <> ''"
                                'mdtTable = identityDt.ToTable()
                                Dim strPrevEmp As String = ""
                                Dim strEmp As String = ""
                                For row As Integer = 0 To mdtTable.Rows.Count - 1

                                    strEmp = mdtTable.Rows(row).Item("EmpId").ToString
                                    'If mdtTable.Rows(row)("newattachdocumentid").ToString().Length > 0 Then
                                    '    attachmentnewadded = True
                                    'End If

                                    'If mdtTable.Rows(row)("deleteattachdocumentid").ToString().Length > 0 Then
                                    '    attachmentdeleted = True
                                    'End If
                                    If strPrevEmp <> strEmp Then
                                        DependantStatusMsg.Append("<TR>")
                                        DependantStatusMsg.Append("<TD colspan = '" & EmailDt.Rows.Count + 2 & "' align = 'LEFT' STYLE='WIDTH:10%;Background-Color:#ccc'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & mdtTable.Rows(row).Item("EmpName").ToString & "</span></b></TD>")
                                        DependantStatusMsg.Append("</TR>")


                                        strPrevEmp = strEmp
                                    End If

                                    DependantStatusMsg.Append("<TR>")

                                    'DependantStatusMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & mdtTable.Rows(row).Item("EmpName").ToString & "</span></b></TD>")
                                    DependantStatusMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & mdtTable.Rows(row).Item("DpndtBefName").ToString & "</span></b></TD>")

                                    For index As Integer = 0 To EmailDt.Rows.Count - 1
                                        NewValue = ""
                                        OldValue = ""
                                        FinalVal = ""

                                        dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step21").ToString(), "ColumnName").Tables("ColumnName")
                                        If objDataOperation.ErrorMessage <> "" Then
                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        End If

                                        ColumnName = dttemp.Rows(0)("msg").ToString()
                                        dttemp = Nothing

                                        dttemp = objDataOperation.ExecQuery(EmailDt(index)("Step2").ToString() & " AND hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid = " & mdtTable.Rows(row).Item("dpndtbeneficestatustranunkid").ToString() & " ", "ColumnVal").Tables("ColumnVal")
                                        If objDataOperation.ErrorMessage <> "" Then
                                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        End If

                                        If IsNothing(dttemp) = False AndAlso dttemp.Rows.Count > 0 Then

                                            Dim oldValueDt As DataTable = Nothing
                                            Dim newValueDt As DataTable = Nothing

                                            Select Case EmailDt(index)("iD").ToString

                                                Case "int"

                                                    Select Case EmailDt(index)("iC").ToString

                                                        Case "reasonunkid"
                                                            newValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("newval").ToString() & "", "NewColvalue").Tables("NewColvalue")
                                                            If objDataOperation.ErrorMessage <> "" Then
                                                                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                            End If

                                                            If newValueDt.Rows.Count > 0 Then
                                                                NewValue = newValueDt.Rows(0)("name").ToString()
                                                            End If
                                                            oldValueDt = objDataOperation.ExecQuery("select name from cfcommon_master where masterunkid = " & dttemp.Rows(0)("oldval").ToString() & "", "NewColvalue").Tables("NewColvalue")

                                                            If objDataOperation.ErrorMessage <> "" Then
                                                                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                            End If

                                                            If oldValueDt.Rows.Count > 0 Then
                                                                OldValue = oldValueDt.Rows(0)("name").ToString()
                                                            End If

                                                    End Select

                                                Case "datetime"
                                                    OldValue = CDate(dttemp.Rows(0)("oldval").ToString()).ToShortDateString
                                                    NewValue = CDate(dttemp.Rows(0)("newval").ToString()).ToShortDateString
                                                Case Else
                                                    OldValue = dttemp.Rows(0)("oldval").ToString()
                                                    NewValue = dttemp.Rows(0)("newval").ToString()
                                            End Select


                                        End If


                                        If NewValue <> OldValue Then
                                            FinalVal = NewValue.Trim.ToString() & " (" & OldValue.Trim.ToString() & ")"
                                            DependantStatusMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & NewValue.Trim.ToString() & " (<strike>" & OldValue.Trim.ToString() & "</strike>)" & "</span></b></TD>")
                                        Else
                                            FinalVal = NewValue.Trim.ToString()
                                            DependantStatusMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & FinalVal & "</span></b></TD>")
                                        End If

                                        'DependantStatusMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & FinalVal & "</span></b></TD>")

                                        'If NewValue <> OldValue Then
                                        'IsChangeExist = True

                                        'If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then
                                        '    MasterDataStrMsg.Append("<TR>")
                                        '    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & mdtTable.Rows(row).Item("EmpName").ToString & "</span></b></TD>")
                                        '    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & mdtTable.Rows(row).Item("DpndtBefName").ToString & "</span></b></TD>")
                                        '    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & ColumnName & "</span></b></TD>")
                                        '    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & NewValue & "</span></b></TD>")
                                        '    MasterDataStrMsg.Append("</TR>")

                                        'Else
                                        '    MasterDataStrMsg.Append("<TR>")
                                        '    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & mdtTable.Rows(row).Item("EmpName").ToString & "</span></b></TD>")
                                        '    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & mdtTable.Rows(row).Item("DpndtBefName").ToString & "</span></b></TD>")
                                        '    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & ColumnName & "</span></b></TD>")
                                        '    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & NewValue & "</span></b></TD>")
                                        '    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & OldValue & "</span></b></TD>")
                                        '    MasterDataStrMsg.Append("</TR>")
                                        'End If

                                        'Else
                                        'If Oprationtype = enOperationType.ADDED Or Oprationtype = enOperationType.DELETED Then
                                        '    MasterDataStrMsg.Append("<TR>")
                                        '    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & ColumnName & "</span></b></TD>")
                                        '    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & NewValue & "</span></b></TD>")
                                        '    MasterDataStrMsg.Append("</TR>")

                                        'Else
                                        '    MasterDataStrMsg.Append("<TR>")
                                        '    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & ColumnName & "</span></b></TD>")
                                        '    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & NewValue & "</span></b></TD>")
                                        '    MasterDataStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & OldValue & "</span></b></TD>")
                                        '    MasterDataStrMsg.Append("</TR>")
                                        'End If
                                        'FinalVal = Newval.Trim.ToString() & "(" & Oldval.Trim.ToString() & ")"
                                        'End If
                                        'Else
                                        'FinalVal = Newval.Trim.ToString()
                                        'End If

                                        'IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & FinalVal & "</span></b></TD>")

                                        NewValue = ""
                                        OldValue = ""

                                    Next

                                    If (isfromapproval = False) Then

                                        'If CInt(mdtTable.Rows(row)("identitytranunkid")) > 0 Then
                                        DependantStatusMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 822, "Edited") & "</span></b></TD>")
                                        'Else
                                        '    Select Case mdtTable.Rows(row)("AUD").ToString()

                                        '        Case "A"
                                        '            IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 821, "Newly Added") & "</span></b></TD>")

                                        '        Case "U"
                                        '            IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 822, "Edited") & "</span></b></TD>")

                                        '        Case "D"
                                        '            IdentityStrMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 823, "Deleted") & "</span></b></TD>")
                                        '    End Select
                                        'End If
                                    Else

                                        If Oprationtype = enOperationType.ADDED Then
                                            DependantStatusMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 821, "Newly Added") & "</span></b></TD>")

                                        ElseIf Oprationtype = enOperationType.EDITED Then
                                            DependantStatusMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 822, "Edited") & "</span></b></TD>")

                                        ElseIf Oprationtype = enOperationType.DELETED Then
                                            DependantStatusMsg.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>" & Language.getMessage(mstrModuleName, 823, "Deleted") & "</span></b></TD>")

                                        End If

                                    End If


                                    DependantStatusMsg.Append("</TR>")
                                    'Next
                                    'IdentityStrMsg.Append("</TABLE>")


                                    'IdentityStrMsg.Append("</TR>")

                                Next
                                DependantStatusMsg.Append("</TABLE>")
                                'If MasterDataStrMsg.Length > 0 Then
                                '    StrMessage.Append(HeaderStrMsg)
                                '    StrMessage.Append(MasterDataStrMsg.ToString())
                                '    StrMessage.Append(vbCrLf)
                                '    StrMessage.Append("</TABLE>")
                                'End If
                            End If
                            'Sohail (18 May 2019) -- End

                    End Select

            End Select


            If DependantStrMsg.Length > 0 Then
                StrMessage.Append(DependantStrMsg.ToString())
            End If

            If DependantMembershipStrMsg.Length > 0 Then
                StrMessage.Append(DependantMembershipStrMsg.ToString())
            End If

            If IdentityStrMsg.Length > 0 Then
                StrMessage.Append(IdentityStrMsg.ToString())
            End If

            If MembershipStrMsg.Length > 0 Then
                StrMessage.Append(MembershipStrMsg.ToString())
            End If

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            If DependantStatusMsg.Length > 0 Then
                StrMessage.Append(DependantStatusMsg.ToString())
            End If
            'Sohail (18 May 2019) -- End

            Select Case eScreenType
                Case enScreenName.frmAddressList, _
                     enScreenName.frmEmergencyAddressList, _
                     enScreenName.frmDependantsAndBeneficiariesList, _
                     enScreenName.frmQualificationsList, _
                     enScreenName.frmOtherinfo, _
                     enScreenName.frmBirthinfo, _
                     enScreenName.frmJobHistory_ExperienceList



                    Dim Attachmentdt As DataTable = Nothing

                    For idx As Integer = 0 To dr.Length - 1
                        StrQ = "DECLARE @ExcludeColNames AS NVARCHAR(MAX) " & _
                                      "DECLARE @table AS TABLE(iC NVARCHAR(MAX), iD NVARCHAR(MAX), iM NVARCHAR(MAX),iU NVARCHAR(MAX),iQ NVARCHAR(MAX)) " & _
                                      "SET @ExcludeColNames = '''' " & _
                                      "INSERT INTO @table ([@table].iC, [@table].iD, [@table].iM, [@table].iQ) " & _
                                           "SELECT " & _
                                                "A.COLUMN_NAME " & _
                                              ",A.DATA_TYPE " & _
                                              ",ROW_NUMBER() OVER (ORDER BY A.ORDINAL_POSITION) " & _
                                              ",a.iCol " & _
                                           "FROM (SELECT " & _
                                                     "'SELECT #tab1.' + s.COLUMN_NAME + ' As nvalue,CASE #tab1.audittype WHEN 0 THEN '''' WHEN 1 THEN ''Newly Added'' WHEN 2 THEN ''Edited'' WHEN 3 THEN ''Deleted'' END  AS audittype,#tab1.audittype as audittypeid FROM #tab1 WHERE #tab1.tranguid = ''#tranguid'' and #tab1.' + s.COLUMN_NAME + ' <> '''' ' " & _
                                                     "AS iCol " & _
                                                   ",s.COLUMN_NAME " & _
                                                   ",s.DATA_TYPE " & _
                                                   ",s.ORDINAL_POSITION " & _
                                                   ",s.TABLE_NAME " & _
                                                "FROM INFORMATION_SCHEMA.COLUMNS s " & _
                                                "WHERE TABLE_NAME = '#tab1' " & _
                                                "AND s.COLUMN_NAME IN ('newattachdocumentid', 'deleteattachdocumentid')) AS A " & _
                                      "SELECT " & _
                                         "[@table].iQ AS Step1 " & _
                                         ",iD " & _
                                         ",iC " & _
                                         ",iM " & _
                                      "FROM @table "
                        StrQ = StrQ.Replace("#tranguid", dr(idx)("tranguid").ToString())

                        StrQ = StrQ.Replace("#tab1", Table2)
                        Attachmentdt = objDataOperation.ExecQuery(StrQ, "Attachmentdt").Tables("Attachmentdt")
                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        If objDataOperation.ErrorMessage <> "" Then
                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        End If
                        'Gajanan [17-April-2019] -- End
                    Next

                    attachmentnewadded = False
                    attachmentdeleted = False


                    If IsNothing(Attachmentdt) = False AndAlso Attachmentdt.Rows.Count > 0 Then

                        For newvalidx As Integer = 0 To Attachmentdt.Rows.Count - 1
                            Dim attachmentValueDt As DataTable = Nothing
                            attachmentValueDt = objDataOperation.ExecQuery(Attachmentdt(newvalidx)("Step1").ToString(), "ColumnValue").Tables("ColumnValue")
                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            If objDataOperation.ErrorMessage <> "" Then
                                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            End If
                            'Gajanan [17-April-2019] -- End

                            If IsNothing(attachmentValueDt) = False AndAlso attachmentValueDt.Rows.Count > 0 Then
                                If Attachmentdt.Rows(newvalidx)("iC").ToString() = "newattachdocumentid" Then
                                    attachmentnewadded = True
                                    IsChangeExist = True
                                ElseIf Attachmentdt.Rows(newvalidx)("iC").ToString() = "deleteattachdocumentid" Then
                                    attachmentdeleted = True
                                    IsChangeExist = True
                                End If
                            End If
                        Next

                    End If



            End Select

            If attachmentnewadded = True And attachmentdeleted = True Then
                StrMessage.Append(Language.getMessage(mstrModuleName, 1000, "There are some attachment(s) which are added and removed"))

            ElseIf attachmentnewadded = True Then
                StrMessage.Append(Language.getMessage(mstrModuleName, 917, "There are some attachment(s) which are added"))

            ElseIf attachmentdeleted = True Then
                StrMessage.Append(Language.getMessage(mstrModuleName, 918, "There are some attachment(s) which are removed"))
            End If



            Select Case intNotificationType
                Case 3, 4
                    If strRejectionRemark.Trim.Length > 0 Then
                        If blnRejection Then
                            StrMessage.Append("<p><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & Language.getMessage(mstrModuleName, 2, "Rejection Remark:") & " " & strRejectionRemark & " " & "</span></b>")
                        Else
                            StrMessage.Append("<p><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";  '>" & Language.getMessage(mstrModuleName, 3, "Approval Remark:") & " " & strRejectionRemark & " " & "</span></b>")
                        End If
                        StrMessage.Append("</span></b></p>")
                    End If
            End Select

            Select Case intNotificationType
                Case 1, 2
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    StrMessage.Append(Language.getMessage(mstrModuleName, 56, "Please login to Aruti application to approve/reject the changes.") & "</span></p>")
            End Select


            'Gajanan [8-April-2019] -- End
            If StrMessage.Length > 0 Then
                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                'Gajanan [27-Mar-2019] -- End
                StrMessage.Append("</span></p>")
                StrMessage.Append("</BODY></HTML>")
            End If

            Select Case intNotificationType
                Case 1, 2

                    If IsChangeExist = False Then
                        StrMessage.Length = 0
                    End If
            End Select
        Catch ex As Exception
            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            'Gajanan [17-April-2019] -- End
            Throw New Exception(ex.Message & "; Procedure Name: SetEmailBody; Module Name: " & mstrModuleName)
        Finally
            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing

            'Gajanan [17-April-2019] -- End
        End Try
    End Sub
    'Gajanan [9-April-2019] -- End


    'Gajanan [9-April-2019] -- Start

    'Public Sub SendNotification(ByVal intNotificationType As Integer, _
    '                        ByVal strDatabaseName As String, _
    '                        ByVal strUserAccessMode As String, _
    '                        ByVal intCompanyId As Integer, _
    '                        ByVal intYearId As Integer, _
    '                        ByVal intPrivilegeId As Integer, _
    '                        ByVal eScreenType As enScreenName, _
    '                        ByVal xEmployeeAsOnDate As String, _
    '                        ByVal intUserId As Integer, _
    '                        ByVal strFormName As String, _
    '                        ByVal eMode As enLogin_Mode, _
    '                        ByVal strSenderAddress As String, _
    '                        ByVal eOperationType As enOperationType, _
    '                        Optional ByVal intPriority As Integer = 0, _
    '                        Optional ByVal intEmployeeIds As String = "", _
    '                        Optional ByVal strRemark As String = "", _
    '                        Optional ByVal mdicRejectUserIds As Dictionary(Of Integer, String) = Nothing)
    '    Try
    '        Dim dtAppr As DataTable = Nothing
    '        Dim StrMessage As New System.Text.StringBuilder
    '        Select Case intNotificationType
    '            Case 1, 3
    '                dtAppr = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, eScreenType, xEmployeeAsOnDate, intUserId, eOperationType, Nothing)
    '            Case 2
    '                dtAppr = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, eScreenType, xEmployeeAsOnDate, intUserId, eOperationType, Nothing, intPriority, False)
    '        End Select
    '        Dim dr() As DataRow = Nothing
    '        Dim dtFilterTab As DataTable = dtAppr.DefaultView.ToTable(True, "username", "email", "userunkid")
    '        Dim strRejectUserIds As String = String.Empty
    '        Dim strSubject As String = ""
    '        Dim strMainText As String = ""
    '        Dim strRejectText As String = ""
    '        If intNotificationType = 3 Then

    '            If IsNothing(mdicRejectUserIds) = False Then
    '                If mdicRejectUserIds.ContainsKey(eScreenType) Then
    '                    strRejectUserIds = mdicRejectUserIds(eScreenType)
    '                End If
    '            End If


    '            Select Case eScreenType
    '                Case enScreenName.frmQualificationsList
    '                    strSubject = Language.getMessage(mstrModuleName, 6, "Notification to Approve/Reject Emplyoee Qualification(s)")
    '                    strMainText = Language.getMessage(mstrModuleName, 10, "This is to inform you that following employee(s) qualification has been added, Please login to aruti to approve/reject them.")
    '                    strRejectText = Language.getMessage(mstrModuleName, 17, "This is to inform you that following employee(s) qualification has been rejected.")
    '                Case enScreenName.frmJobHistory_ExperienceList
    '                    strSubject = Language.getMessage(mstrModuleName, 7, "Notification to Approve/Reject Emplyoee Job Experience(s)")
    '                    strMainText = Language.getMessage(mstrModuleName, 11, "This is to inform you that following employee(s) job experience has been added, Please login to aruti to approve/reject them.")
    '                    strRejectText = Language.getMessage(mstrModuleName, 18, "This is to inform you that following employee(s) job experience has been rejected.")

    '                Case enScreenName.frmEmployeeRefereeList
    '                    strSubject = Language.getMessage(mstrModuleName, 8, "Notification to Approve/Reject Emplyoee References")
    '                    strMainText = Language.getMessage(mstrModuleName, 12, "This is to inform you that following employee(s) references has been added, Please login to aruti to approve/reject them.")
    '                    strRejectText = Language.getMessage(mstrModuleName, 19, "This is to inform you that following employee(s) references has been rejected.")

    '                Case enScreenName.frmEmployee_Skill_List
    '                    strSubject = Language.getMessage(mstrModuleName, 9, "Notification to Approve/Reject Emplyoee Skill(s)")
    '                    strMainText = Language.getMessage(mstrModuleName, 13, "This is to inform you that following employee(s) skills has been added, Please login to aruti to approve/reject them.")
    '                    strRejectText = Language.getMessage(mstrModuleName, 20, "This is to inform you that following employee(s) skills has been rejected.")

    '            End Select
    '            If strRejectUserIds.Trim.Length > 0 Then
    '                For Each id As String In strRejectUserIds.Split(CChar(","))
    '                    Dim objUser As New clsUserAddEdit
    '                    objUser._Userunkid = CInt(id)
    '                    Dim drow As DataRow = dtFilterTab.NewRow()
    '                    drow("username") = objUser._Username
    '                    drow("email") = objUser._Email
    '                    drow("userunkid") = objUser._Userunkid
    '                    dtFilterTab.Rows.Add(drow)
    '                    objUser = Nothing
    '                Next
    '            End If
    '        End If

    '        For Each iRow As DataRow In dtFilterTab.Rows
    '            StrMessage = New System.Text.StringBuilder
    '            Select Case intNotificationType
    '                Case 1  'NOTIFICATION FOR SUBMIT FOR APPROVAL
    '                    dr = dtAppr.Select("rno = 1 AND userunkid = " & CInt(iRow("userunkid")) & " " & IIf(intEmployeeIds.Trim.Length <= 0, "", " AND employeeunkid IN (" & intEmployeeIds & ") AND isgrp = 0").ToString())
    '                    If dr.Length > 0 Then
    '                        Call SetEmailBody(StrMessage, eScreenType, intNotificationType, dr, iRow("username").ToString, strMainText, strRemark, False)
    '                        Dim objSendMail As New clsSendMail
    '                        objSendMail._ToEmail = iRow("email").ToString().Trim()
    '                        objSendMail._Subject = strSubject
    '                        objSendMail._Message = StrMessage.ToString()
    '                        objSendMail._Form_Name = strFormName
    '                        objSendMail._LogEmployeeUnkid = -1
    '                        objSendMail._OperationModeId = eMode
    '                        objSendMail._UserUnkid = intUserId
    '                        objSendMail._SenderAddress = strSenderAddress
    '                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
    '                        objSendMail.SendMail(intCompanyId)
    '                    End If

    '                Case 2  'NOTIFICATION FOR NEXT LEVEL OF APPROVAL
    '                    dr = dtAppr.Select("rno = 1 AND priority > " & intPriority & " AND userunkid = " & CInt(iRow("userunkid")) & "  " & IIf(intEmployeeIds.Trim.Length <= 0, "", " AND employeeunkid IN (" & intEmployeeIds & ") AND isgrp = 0").ToString())
    '                    If dr.Length > 0 Then
    '                        Call SetEmailBody(StrMessage, eScreenType, intNotificationType, dr, iRow("username").ToString, strMainText, strRemark, False)
    '                        Dim objSendMail As New clsSendMail
    '                        objSendMail._ToEmail = iRow("email").ToString().Trim()
    '                        objSendMail._Subject = strSubject 'Language.getMessage(mstrModuleName, 46, "Notification to Approve/Reject Emplyoee Movement")
    '                        objSendMail._Message = StrMessage.ToString()
    '                        objSendMail._Form_Name = strFormName
    '                        objSendMail._LogEmployeeUnkid = -1
    '                        objSendMail._OperationModeId = eMode
    '                        objSendMail._UserUnkid = intUserId
    '                        objSendMail._SenderAddress = strSenderAddress
    '                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
    '                        objSendMail.SendMail(intCompanyId)

    '                    End If
    '                Case 3  'NOTIFICATION FOR REJECTION OF EMPLOYEE MOVEMENT
    '                    dr = dtAppr.Select("priority <= " & intPriority & " AND userunkid = " & CInt(iRow("userunkid")) & " " & IIf(intEmployeeIds.Trim.Length <= 0, "", " AND employeeunkid IN (" & intEmployeeIds & ") AND isgrp = 0").ToString())
    '                    If dr.Length > 0 Then
    '                        Call SetEmailBody(StrMessage, eScreenType, intNotificationType, dr, iRow("username").ToString, strRejectText, strRemark, True)
    '                        Dim objSendMail As New clsSendMail
    '                        objSendMail._ToEmail = iRow("email").ToString().Trim()
    '                        objSendMail._Subject = strSubject
    '                        objSendMail._Message = StrMessage.ToString()
    '                        objSendMail._Form_Name = strFormName
    '                        objSendMail._LogEmployeeUnkid = -1
    '                        objSendMail._OperationModeId = eMode
    '                        objSendMail._UserUnkid = intUserId
    '                        objSendMail._SenderAddress = strSenderAddress
    '                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
    '                        objSendMail.SendMail(intCompanyId)
    '                    End If
    '            End Select
    '        Next
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: SendNotification; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    Public Sub SendNotification(ByVal intNotificationType As Integer, _
                                ByVal strDatabaseName As String, _
                                ByVal strUserAccessMode As String, _
                                ByVal intCompanyId As Integer, _
                                ByVal intYearId As Integer, _
                                ByVal intPrivilegeId As Integer, _
                                ByVal eScreenType As enScreenName, _
                                ByVal xEmployeeAsOnDate As String, _
                                ByVal intUserId As Integer, _
                                ByVal strFormName As String, _
                                ByVal eMode As enLogin_Mode, _
                                ByVal strSenderAddress As String, _
                                ByVal eOperationType As enOperationType, _
                                Optional ByVal intPriority As Integer = 0, _
                                Optional ByVal intEmployeeIds As String = "", _
                                Optional ByVal strRemark As String = "", _
                                Optional ByVal mdicRejectUserIds As Dictionary(Of Integer, String) = Nothing, _
                                Optional ByVal FilterString As String = "", _
                                Optional ByVal mdtdatatable As DataTable = Nothing, _
                                Optional ByVal isfromapproval As Boolean = False, _
                                Optional ByVal enaddressType As clsEmployeeAddress_approval_tran.enAddressType = clsEmployeeAddress_approval_tran.enAddressType.NONE, _
                                Optional ByVal FilterStringForEmailBody As String = "", _
                                Optional ByVal xDataOpr As clsDataOperation = Nothing)
        'Gajanan [9-April-2019] -- [enaddressType]
        Try
            Dim dtAppr As DataTable = Nothing
            Dim StrMessage As New System.Text.StringBuilder

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            If xDataOpr Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOpr
            End If
            'Gajanan [17-April-2019] -- End

            If (isfromapproval = False AndAlso CType(eScreenType, enScreenName) = enScreenName.frmMembershipInfoList) OrElse _
                (isfromapproval = False AndAlso CType(eScreenType, enScreenName) = enScreenName.frmIdentityInfoList) OrElse _
                (isfromapproval = False AndAlso CType(eScreenType, enScreenName) = enScreenName.frmDependantStatus) Then
                'Sohail (18 May 2019) - [frmDependantStatus]

                Select Case intNotificationType
                    Case 1, 3
                        dtAppr = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, eScreenType, xEmployeeAsOnDate, intUserId, eOperationType, objDataOperation, , , , , True)
                    Case 2, 4
                        dtAppr = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, eScreenType, xEmployeeAsOnDate, intUserId, eOperationType, objDataOperation, intPriority, False, , , True)
                End Select

            Else
                If IsNothing(mdtdatatable) Then
                    Select Case intNotificationType
                        Case 1, 3
                            dtAppr = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, eScreenType, xEmployeeAsOnDate, intUserId, eOperationType, objDataOperation)
                        Case 2, 4
                            dtAppr = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, eScreenType, xEmployeeAsOnDate, intUserId, eOperationType, objDataOperation, intPriority, False)
                    End Select
                Else
                    dtAppr = mdtdatatable
                End If
            End If





            Dim dr() As DataRow = Nothing
            Dim dtFilterTab As DataTable = Nothing


            If IsNothing(dtAppr) = False AndAlso dtAppr.Rows.Count > 0 Then
                'S.SANDEEP |21-JAN-2020| -- START
                'ISSUE/ENHANCEMENT : THE SOURCE CONTAINS NO DATAROWS DUE TO DIRECT USE OF ".CopyToDataTable()"
                'If FilterString <> String.Empty Then
                '    dtAppr.DefaultView.RowFilter = FilterString
                '    If (isfromapproval = False AndAlso CType(eScreenType, enScreenName) = enScreenName.frmMembershipInfoList) Or _
                '       (isfromapproval = False AndAlso CType(eScreenType, enScreenName) = enScreenName.frmIdentityInfoList) Then
                '        dtFilterTab = dtAppr.DefaultView.ToTable(True, "username", "approver_email", "userunkid", "employee_name", "employeeunkid", "icheck", "priority").Select("approver_email<>''").CopyToDataTable()
                '    Else
                '        dtFilterTab = dtAppr.DefaultView.ToTable(True, "username", "approver_email", "userunkid", "employee_name", "employeeunkid", "icheck", "tranguid", "priority").Select("approver_email<>''").CopyToDataTable()
                '    End If

                'Else
                '    If (isfromapproval = False AndAlso CType(eScreenType, enScreenName) = enScreenName.frmMembershipInfoList) Or _
                '      (isfromapproval = False AndAlso CType(eScreenType, enScreenName) = enScreenName.frmIdentityInfoList) Then
                '        dtFilterTab = dtAppr.DefaultView.ToTable(True, "username", "approver_email", "userunkid", "employee_name", "employeeunkid", "icheck", "priority").Select("approver_email<>'' and icheck = true").CopyToDataTable()
                '    Else
                '        dtFilterTab = dtAppr.DefaultView.ToTable(True, "username", "approver_email", "userunkid", "employee_name", "employeeunkid", "icheck", "tranguid", "priority").Select("approver_email<>'' and icheck = true").CopyToDataTable()
                '    End If
                'End If
                Dim dttmp As DataRow() = Nothing
                If FilterString <> String.Empty Then
                    dtAppr.DefaultView.RowFilter = FilterString
                    If (isfromapproval = False AndAlso CType(eScreenType, enScreenName) = enScreenName.frmMembershipInfoList) Or _
                       (isfromapproval = False AndAlso CType(eScreenType, enScreenName) = enScreenName.frmIdentityInfoList) Then
                        dttmp = dtAppr.DefaultView.ToTable(True, "username", "approver_email", "userunkid", "employee_name", "employeeunkid", "icheck", "priority").Select("approver_email<>''")
                    Else
                        dttmp = dtAppr.DefaultView.ToTable(True, "username", "approver_email", "userunkid", "employee_name", "employeeunkid", "icheck", "tranguid", "priority").Select("approver_email<>''")
                    End If
                Else
                    If (isfromapproval = False AndAlso CType(eScreenType, enScreenName) = enScreenName.frmMembershipInfoList) Or _
                      (isfromapproval = False AndAlso CType(eScreenType, enScreenName) = enScreenName.frmIdentityInfoList) Then
                        dttmp = dtAppr.DefaultView.ToTable(True, "username", "approver_email", "userunkid", "employee_name", "employeeunkid", "icheck", "priority").Select("approver_email<>'' and icheck = true")
                    Else
                        dttmp = dtAppr.DefaultView.ToTable(True, "username", "approver_email", "userunkid", "employee_name", "employeeunkid", "icheck", "tranguid", "priority").Select("approver_email<>'' and icheck = true")
                    End If
                End If
                If dttmp IsNot Nothing AndAlso dttmp.Length > 0 Then
                    dtFilterTab = dttmp.CopyToDataTable()
                Else
                    dtFilterTab = dtAppr.DefaultView.ToTable(True, "username", "approver_email", "userunkid", "employee_name", "employeeunkid", "icheck", "tranguid", "priority").Clone()
                End If
                'S.SANDEEP |21-JAN-2020| -- END
            End If

            Dim strRejectUserIds As String = String.Empty
            Dim strSubject As String = ""
            Dim strMainText As String = ""
            Dim strRejectText As String = ""
            If intNotificationType = 3 Then

                If IsNothing(mdicRejectUserIds) = False Then
                    If mdicRejectUserIds.ContainsKey(eScreenType) Then
                        strRejectUserIds = mdicRejectUserIds(eScreenType)
                    End If
                End If


                'Select Case eScreenType
                '    Case enScreenName.frmQualificationsList
                '        strSubject = Language.getMessage(mstrModuleName, 6, "Notification to Approve/Reject Emplyoee Qualification(s)")
                '        strMainText = Language.getMessage(mstrModuleName, 10, "This is to inform you that following employee(s) qualification has been added, Please login to aruti to approve/reject them.")
                '        strRejectText = Language.getMessage(mstrModuleName, 17, "This is to inform you that following employee(s) qualification has been rejected.")
                '    Case enScreenName.frmJobHistory_ExperienceList
                '        strSubject = Language.getMessage(mstrModuleName, 7, "Notification to Approve/Reject Emplyoee Job Experience(s)")
                '        strMainText = Language.getMessage(mstrModuleName, 11, "This is to inform you that following employee(s) job experience has been added, Please login to aruti to approve/reject them.")
                '        strRejectText = Language.getMessage(mstrModuleName, 18, "This is to inform you that following employee(s) job experience has been rejected.")

                '    Case enScreenName.frmEmployeeRefereeList
                '        strSubject = Language.getMessage(mstrModuleName, 8, "Notification to Approve/Reject Emplyoee References")
                '        strMainText = Language.getMessage(mstrModuleName, 12, "This is to inform you that following employee(s) references has been added, Please login to aruti to approve/reject them.")
                '        strRejectText = Language.getMessage(mstrModuleName, 19, "This is to inform you that following employee(s) references has been rejected.")

                '    Case enScreenName.frmEmployee_Skill_List
                '        strSubject = Language.getMessage(mstrModuleName, 9, "Notification to Approve/Reject Emplyoee Skill(s)")
                '        strMainText = Language.getMessage(mstrModuleName, 13, "This is to inform you that following employee(s) skills has been added, Please login to aruti to approve/reject them.")
                '        strRejectText = Language.getMessage(mstrModuleName, 20, "This is to inform you that following employee(s) skills has been rejected.")

                'End Select


                If strRejectUserIds.Trim.Length > 0 Then
                    'For Each id As String In strRejectUserIds.Split(CChar(","))
                    '    Dim objUser As New clsUserAddEdit
                    '    objUser._Userunkid = CInt(id)
                    '    Dim drow As DataRow = dtFilterTab.NewRow()
                    '    drow("username") = objUser._Username
                    '    drow("approver_email") = objUser._Email
                    '    drow("userunkid") = objUser._Userunkid
                    '    drow("priority") = intPriority
                    '    dtFilterTab.Rows.Add(drow)
                    '    objUser = Nothing
                    'Next
                    If intNotificationType = 3 Then
                        For Each id As String In strRejectUserIds.Split(CChar(","))
                            Dim objUser As New clsUserAddEdit
                            objUser._Userunkid = CInt(id)
                            If objUser._Email.Trim.Length > 0 Then
                                For Each empid As String In intEmployeeIds.Split(CChar(","))
                                    Dim objemp As New clsEmployee_Master
                                    objemp._Employeeunkid(eZeeDate.convertDate(xEmployeeAsOnDate), objDataOperation) = CInt(empid)

                                    'Gajanan [17-April-2019] -- Start
                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                    'Call SetEmailBody(StrMessage, eScreenType, intNotificationType, Nothing, objUser._Username, strRemark, False, objemp._Surname & " " & objemp._Firstname, eOperationType, enToEmailType.USER, enaddressType, FilterStringForEmailBody, objDataOperation, isfromapproval, mdtdatatable)
                                    Call SetEmailBody(StrMessage, eScreenType, intNotificationType, Nothing, objUser._Username, strRemark, False, objemp._Firstname & " " & objemp._Surname, eOperationType, enToEmailType.USER, enaddressType, FilterStringForEmailBody, objDataOperation, isfromapproval, mdtdatatable)
                                    'Gajanan [17-April-2019] -- End

                                    Dim objSendMail As New clsSendMail
                                    objSendMail._ToEmail = objUser._Email
                                    objSendMail._Subject = Language.getMessage(mstrModuleName, 2074, "Notification For Rejection of Bio Data Changes")
                                    objSendMail._Message = StrMessage.ToString()
                                    objSendMail._Form_Name = strFormName
                                    objSendMail._LogEmployeeUnkid = -1
                                    objSendMail._OperationModeId = eMode
                                    objSendMail._UserUnkid = intUserId
                                    objSendMail._SenderAddress = strSenderAddress
                                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                                    objSendMail.SendMail(intCompanyId)
                                    objSendMail = Nothing
                                Next
                            End If
                        Next
                    End If
                End If
            End If

    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.

            Select Case intNotificationType
                Case 3
                    dtFilterTab.Columns.Remove("tranguid")
                    dtFilterTab = dtFilterTab.DefaultView.ToTable(True, "username", "approver_email", "userunkid", "employee_name", "employeeunkid", "icheck", "priority")

            End Select
    'Gajanan [17-April-2019] -- End



            Dim Employeeid As Integer = 0



            Dim UserList As New Dictionary(Of Integer, Boolean)

            If IsNothing(dtFilterTab) = False AndAlso dtFilterTab.Rows.Count > 0 Then
                For Each iRow As DataRow In dtFilterTab.Rows
                    'For Each empid As String In intEmployeeIds.Split(CChar(","))
                    Dim empid As String = iRow("employeeunkid").ToString()
                        Dim objemp As New clsEmployee_Master
                        objemp._Employeeunkid(eZeeDate.convertDate(xEmployeeAsOnDate), objDataOperation) = CInt(empid)

                    Select Case eScreenType
                        Case enScreenName.frmDependantStatus

                            If IsNothing(UserList) = False Then
                                If UserList.ContainsKey(CInt(iRow("userunkid").ToString())) Then Continue For
                                UserList.Add(CInt(iRow("userunkid")), True)
                            Else
                                UserList.Add(CInt(iRow("userunkid")), True)
                            End If


                            dtAppr.DefaultView.RowFilter = ""
                            dtAppr.DefaultView.RowFilter = "userunkid = " & iRow("userunkid").ToString() & ""

                            Dim emplist As String = String.Join(",", dtAppr.DefaultView.ToTable(True, "employeeunkid").AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToArray())

                            mdtdatatable.DefaultView.RowFilter = ""
                            If emplist.Length > 0 Then
                                mdtdatatable.DefaultView.RowFilter = "empid in (" & emplist & ")"
                            End If

                    End Select


                        StrMessage = New System.Text.StringBuilder
                        Select Case intNotificationType
                            Case 1  'NOTIFICATION FOR SUBMIT FOR APPROVAL
                                dr = dtAppr.DefaultView.ToTable().Select("rno = 1 AND isgrp = 0 AND userunkid = " & CInt(iRow("userunkid")) & " " & "And uempid <> " & empid & " AND employeeunkid = " & empid & "")
                                If dr.Length > 0 Then

                                'Gajanan [25-May-2019] -- Start              
                                'Call SetEmailBody(StrMessage, eScreenType, intNotificationType, dr, iRow("username").ToString, strRemark, False, objemp._Surname & " " & objemp._Firstname, eOperationType, enToEmailType.USER, enaddressType, FilterStringForEmailBody, objDataOperation, isfromapproval, mdtdatatable)
                                Dim dtTemp As DataTable = Nothing
                                If mdtdatatable IsNot Nothing Then
                                    dtTemp = mdtdatatable.DefaultView.ToTable
                                End If
                                Call SetEmailBody(StrMessage, eScreenType, intNotificationType, dr, iRow("username").ToString, strRemark, False, objemp._Firstname & " " & objemp._Surname, eOperationType, enToEmailType.USER, enaddressType, FilterStringForEmailBody, objDataOperation, isfromapproval, dtTemp)
                                'Gajanan [25-May-2019] -- End

                                    'Hemant [8-April-2019] -- [enaddressType]
                                    'Hemant [8-April-2019] -- Start
                                    If StrMessage.ToString().Length > 0 Then
                                        'Hemant [8-April-2019] -- Start
                                        Dim objSendMail As New clsSendMail
                                        objSendMail._ToEmail = iRow("approver_email").ToString().Trim()
                                        objSendMail._Subject = Language.getMessage(mstrModuleName, 51, "Notification For Bio Data Changes")
                                        objSendMail._Message = StrMessage.ToString()
                                        objSendMail._Form_Name = strFormName
                                        objSendMail._LogEmployeeUnkid = -1
                                        objSendMail._OperationModeId = eMode
                                        objSendMail._UserUnkid = intUserId
                                        objSendMail._SenderAddress = strSenderAddress
                                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                                        objSendMail.SendMail(intCompanyId)
                                    End If  'Hemant [8-April-2019] 
                                End If


                            Case 2  'NOTIFICATION FOR NEXT LEVEL OF APPROVAL
                                dr = dtAppr.DefaultView.ToTable().Select("rno = 1 AND isgrp = 0 AND priority > " & intPriority & " AND userunkid = " & CInt(iRow("userunkid")) & "And uempid <> " & empid & " AND employeeunkid =" & empid & " AND tranguid = '" & iRow("tranguid").ToString() & "'")
                                If dr.Length > 0 Then


                                'Gajanan [17-April-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                'Call SetEmailBody(StrMessage, eScreenType, intNotificationType, dr, iRow("username").ToString, strRemark, False, objemp._Surname & " " & objemp._Firstname, eOperationType, enToEmailType.USER, enaddressType, FilterStringForEmailBody, objDataOperation, isfromapproval, mdtdatatable)
                                Call SetEmailBody(StrMessage, eScreenType, intNotificationType, dr, iRow("username").ToString, strRemark, False, objemp._Firstname & " " & objemp._Surname, eOperationType, enToEmailType.USER, enaddressType, FilterStringForEmailBody, objDataOperation, isfromapproval, mdtdatatable)
                                'Gajanan [17-April-2019] -- End

                                Dim objSendMail As New clsSendMail
                                    objSendMail._ToEmail = iRow("approver_email").ToString().Trim()
                                    objSendMail._Subject = Language.getMessage(mstrModuleName, 52, "Notification For Approval of Bio Data Changes")
                                    objSendMail._Message = StrMessage.ToString()
                                    objSendMail._Form_Name = strFormName
                                    objSendMail._LogEmployeeUnkid = -1
                                    objSendMail._OperationModeId = eMode
                                    objSendMail._UserUnkid = intUserId
                                    objSendMail._SenderAddress = strSenderAddress
                                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                                    objSendMail.SendMail(intCompanyId)

                                End If

                        Case 3
                            'NOTIFICATION FOR REJECTION OF EMPLOYEE MOVEMENT
                                'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.


                            'dr = dtAppr.Select("priority <= " & intPriority & " AND isgrp = 0 AND userunkid = " & CInt(iRow("userunkid")) & " " & "AND employeeunkid = " & empid & " ")
                            dr = dtAppr.Select("priority <= " & intPriority & " AND isgrp = 0 AND userunkid = " & CInt(iRow("userunkid")) & " " & "AND employeeunkid = " & empid & " AND iCheck = 1 ")
    'Gajanan [17-April-2019] -- End
                                'This is for Future iF they want email don't get to those user which are reject this Data
                                'dr = dtAppr.Select("priority <= " & intPriority & " AND userunkid = " & CInt(iRow("userunkid")) & " AND userunkid <> " & intUserId & "  " & IIf(intEmployeeIds.Trim.Length <= 0, "", " AND employeeunkid IN (" & intEmployeeIds & ")").ToString())

                                Dim objSendMail As New clsSendMail
                            If dr.Length > 0 Then

                                'Gajanan [1-NOV-2019] -- Start    
                                'Enhancement:Worked On NMB ESS Comment  

                                ''Gajanan [17-April-2019] -- Start
                                ''Enhancement - Implementing Employee Approver Flow On Employee Data.
                                ''Call SetEmailBody(StrMessage, eScreenType, intNotificationType, dr, iRow("username").ToString, strRemark, False, objemp._Surname & " " & objemp._Firstname, eOperationType, enToEmailType.USER, enaddressType, FilterStringForEmailBody, objDataOperation, isfromapproval, mdtdatatable)
                                'Call SetEmailBody(StrMessage, eScreenType, intNotificationType, dr, iRow("username").ToString, strRemark, False, objemp._Firstname & " " & objemp._Surname, eOperationType, enToEmailType.USER, enaddressType, FilterStringForEmailBody, objDataOperation, isfromapproval, mdtdatatable)
                                ''Gajanan [17-April-2019] -- End
                                'objSendMail._ToEmail = iRow("approver_email").ToString().Trim()
                                'objSendMail._Subject = Language.getMessage(mstrModuleName, 53, "Notification For Rejection of Bio Data Changes")
                                'objSendMail._Message = StrMessage.ToString()
                                'objSendMail._Form_Name = strFormName
                                'objSendMail._LogEmployeeUnkid = -1
                                'objSendMail._OperationModeId = eMode
                                'objSendMail._UserUnkid = intUserId
                                'objSendMail._SenderAddress = strSenderAddress
                                'objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                                'objSendMail.SendMail(intCompanyId)

                                'Gajanan [1-NOV-2019] -- End
                            Else
                                If CInt(iRow("priority")) > intPriority Then Continue For
                                dr = dtAppr.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(empid) And CBool(x.Field(Of Integer)("isgrp")) = False).Take(1).ToArray()
                                'If d IsNot Nothing Then
                                '    dr.ToList().Add(d)
                                'End If

                                If dr Is Nothing OrElse dr.Length <= 0 Then Continue For

                                'Gajanan [17-April-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                'Call SetEmailBody(StrMessage, eScreenType, intNotificationType, dr, iRow("username").ToString, strRemark, True, objemp._Surname & " " & objemp._Firstname, eOperationType, enToEmailType.USER, enaddressType, FilterStringForEmailBody, objDataOperation, isfromapproval, mdtdatatable)
                                Call SetEmailBody(StrMessage, eScreenType, intNotificationType, dr, iRow("username").ToString, strRemark, True, objemp._Firstname & " " & objemp._Surname, eOperationType, enToEmailType.USER, enaddressType, FilterStringForEmailBody, objDataOperation, isfromapproval, mdtdatatable)
                                'Gajanan [17-April-2019] -- End

                                objSendMail._ToEmail = iRow("approver_email").ToString().Trim()
                                objSendMail._Subject = Language.getMessage(mstrModuleName, 2074, "Notification For Rejection of Bio Data Changes")
                                objSendMail._Message = StrMessage.ToString()
                                objSendMail._Form_Name = strFormName
                                objSendMail._LogEmployeeUnkid = -1
                                objSendMail._OperationModeId = eMode
                                objSendMail._UserUnkid = intUserId
                                objSendMail._SenderAddress = strSenderAddress
                                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                                objSendMail.SendMail(intCompanyId)


                            End If

                            If dr.Length > 0 Then
                                If Employeeid <> CInt(empid) Then
                                    Employeeid = CInt(empid)
                                    StrMessage = New System.Text.StringBuilder

                                    'Gajanan [17-April-2019] -- Start
                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                    'Call SetEmailBody(StrMessage, eScreenType, intNotificationType, dr, iRow("username").ToString, strRemark, True, objemp._Surname & " " & objemp._Firstname, eOperationType, enToEmailType.EMPLOYEE, enaddressType, FilterStringForEmailBody, objDataOperation, isfromapproval, mdtdatatable)
                                    Call SetEmailBody(StrMessage, eScreenType, intNotificationType, dr, iRow("username").ToString, strRemark, True, objemp._Firstname & " " & objemp._Surname, eOperationType, enToEmailType.EMPLOYEE, enaddressType, FilterStringForEmailBody, objDataOperation, isfromapproval, mdtdatatable)
                                    'Gajanan [17-April-2019] -- End

                                    objSendMail._ToEmail = objemp._Email.Trim()
                                    objSendMail._Subject = Language.getMessage(mstrModuleName, 2074, "Notification For Rejection of Bio Data Changes")
                                    objSendMail._Message = StrMessage.ToString()
                                    objSendMail._Form_Name = strFormName
                                    objSendMail._LogEmployeeUnkid = -1
                                    objSendMail._OperationModeId = eMode
                                    objSendMail._UserUnkid = intUserId
                                    objSendMail._SenderAddress = strSenderAddress
                                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                                    objSendMail.SendMail(intCompanyId)

                                End If
                            End If
                            Case 4

                                dr = dtAppr.DefaultView.ToTable().Select("rno = 1 AND isgrp = 0 AND employeeunkid = " & empid & "")


                                If dr.Length > 0 Then
                                    If Employeeid <> CInt(empid) Then
                                        Employeeid = CInt(empid)

                                    'Gajanan [17-April-2019] -- Start
                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                    'Call SetEmailBody(StrMessage, eScreenType, intNotificationType, dr, iRow("username").ToString, strRemark, False, objemp._Surname & " " & objemp._Firstname, eOperationType, enToEmailType.EMPLOYEE, enaddressType, FilterStringForEmailBody, objDataOperation, isfromapproval, mdtdatatable)
                                    Call SetEmailBody(StrMessage, eScreenType, intNotificationType, dr, iRow("username").ToString, strRemark, False, objemp._Firstname & " " & objemp._Surname, eOperationType, enToEmailType.EMPLOYEE, enaddressType, FilterStringForEmailBody, objDataOperation, isfromapproval, mdtdatatable)
                                    'Gajanan [17-April-2019] -- End

                                    Dim objSendMail As New clsSendMail

                                        If objemp._Email.Length > 0 Then
                                            objSendMail._ToEmail = objemp._Email
                                        objSendMail._Subject = Language.getMessage(mstrModuleName, 2073, "Notification For Approval of Bio Data Changes")
                                            objSendMail._Message = StrMessage.ToString()
                                            objSendMail._Form_Name = strFormName
                                            objSendMail._LogEmployeeUnkid = -1
                                            objSendMail._OperationModeId = eMode
                                            objSendMail._UserUnkid = intUserId
                                            objSendMail._SenderAddress = strSenderAddress
                                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                                            objSendMail.SendMail(intCompanyId)
                                        End If
                                    End If
                                End If
                        End Select
                    'Next
                Next
            Else

            End If



        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: SendNotification; Module Name: " & mstrModuleName)
        Finally
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    'Gajanan [9-April-2019] -- End


    'Gajanan [27-May-2019] -- Start              
    Public Sub ImportDataSendNotification(ByVal intNotificationType As Integer, _
                                ByVal strDatabaseName As String, _
                                ByVal strUserAccessMode As String, _
                                ByVal intCompanyId As Integer, _
                                ByVal intYearId As Integer, _
                                ByVal intPrivilegeId As Integer, _
                                ByVal eScreenType As enScreenName, _
                                ByVal xEmployeeAsOnDate As String, _
                                ByVal intUserId As Integer, _
                                ByVal strFormName As String, _
                                ByVal eMode As enLogin_Mode, _
                                ByVal strSenderAddress As String, _
                                ByVal eOperationType As enOperationType, _
                                ByVal intloginEmployeeId As Integer, _
                                ByVal strIPAddress As String, _
                                ByVal strHostName As String, _
                                ByVal blnIsWeb As Boolean, _
                                ByVal intAuditUserId As Integer, _
                                ByVal mdtAuditDate As DateTime, _
                                ByVal mdtdatatable As DataTable, _
                                Optional ByVal intPriority As Integer = 0, _
                                Optional ByVal intEmployeeIds As String = "", _
                                Optional ByVal isfromapproval As Boolean = False, _
                                Optional ByVal enaddressType As Integer = -1, _
                                Optional ByVal xDataOpr As clsDataOperation = Nothing)


        Try

            If IsNothing(mdtdatatable) = False AndAlso mdtdatatable.Rows.Count > 0 Then
                Dim dtAppr As DataTable = Nothing
                Dim dtFilterTab As DataTable = Nothing

                If xDataOpr IsNot Nothing Then
                    objDataOperation = xDataOpr
                Else
                    objDataOperation = New clsDataOperation
                End If

                If intEmployeeIds.Length > 0 Then

                    Select Case intNotificationType
                        Case 1, 3


                            Select Case eScreenType
                                Case enScreenName.frmEmployee_Skill_List
                                    dtAppr = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, eScreenType, xEmployeeAsOnDate, intUserId, eOperationType, objDataOperation, , , "iData.emp_app_unkid in (" & intEmployeeIds & ")", , True)
                                Case Else
                                    dtAppr = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, eScreenType, xEmployeeAsOnDate, intUserId, eOperationType, objDataOperation, , , "iData.employeeunkid in (" & intEmployeeIds & ")", , True)


                            End Select

                        Case 2, 4
                            dtAppr = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, intPrivilegeId, eScreenType, xEmployeeAsOnDate, intUserId, eOperationType, objDataOperation, intPriority, False, , , True)
                    End Select


                    If IsNothing(dtAppr) = False AndAlso dtAppr.Rows.Count > 0 Then
                        dtFilterTab = dtAppr.DefaultView.ToTable(True, "username", "approver_email", "userunkid", "priority", "rno").Select("approver_email<>'' and rno=1").CopyToDataTable()



                        'Gajanan [27-May-2019] -- Start              
                        For Each iRow As DataRow In dtFilterTab.Rows

                            dtAppr.DefaultView.RowFilter = "userunkid = " & iRow("userunkid").ToString() & ""

                            Dim emplist As String = String.Join(",", dtAppr.DefaultView.ToTable(True, "employeeunkid").AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToArray())
                            If emplist.Length > 0 Then
                                mdtdatatable.DefaultView.RowFilter = ""
                                mdtdatatable.DefaultView.RowFilter = "employeeid in (" & emplist & ")"
                            End If

                            Dim StrMessage As New System.Text.StringBuilder
                            '"employee_name", "employeeunkid"
                            dtAppr.DefaultView.RowFilter = "userunkid = " & iRow.Item("userunkid").ToString() & " and isgrp = 0 "
                            Call SetEmailBodyForImportData(StrMessage, eScreenType, intNotificationType, mdtdatatable.DefaultView.ToTable(), iRow("username").ToString, eOperationType, enToEmailType.USER, enaddressType, objDataOperation, isfromapproval)
                            Dim objSendMail As New clsSendMail
                            objSendMail._ToEmail = iRow("approver_email").ToString
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 51, "Notification For Bio Data Changes")
                            objSendMail._Message = StrMessage.ToString()
                            objSendMail._Form_Name = strFormName
                            objSendMail._LogEmployeeUnkid = intloginEmployeeId
                            objSendMail._OperationModeId = eMode
                            objSendMail._UserUnkid = intUserId
                            objSendMail._SenderAddress = strSenderAddress
                            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                            objSendMail.SendMail(intCompanyId)
                            objSendMail = Nothing
                        Next
                        'Gajanan [27-May-2019] -- End

                    End If


                    'Gajanan [27-May-2019] -- Start              



                    'For Each iRow As DataRow In dtFilterTab.Rows
                    '    Dim StrMessage As New System.Text.StringBuilder

                    '    '"employee_name", "employeeunkid"

                    '    dtAppr.DefaultView.RowFilter = "userunkid = " & iRow.Item("userunkid").ToString() & " and isgrp = 0 "


                    '    Call SetEmailBodyForImportData(StrMessage, eScreenType, intNotificationType, mdtdatatable, iRow("username").ToString, eOperationType, enToEmailType.USER, enaddressType, objDataOperation, isfromapproval)

                    '    Dim objSendMail As New clsSendMail
                    '    objSendMail._ToEmail = iRow("approver_email").ToString
                    '    objSendMail._Subject = Language.getMessage(mstrModuleName, 51, "Notification For Bio Data Changes")
                    '    objSendMail._Message = StrMessage.ToString()
                    '    objSendMail._Form_Name = strFormName
                    '    objSendMail._LogEmployeeUnkid = intloginEmployeeId
                    '    objSendMail._OperationModeId = eMode
                    '    objSendMail._UserUnkid = intUserId
                    '    objSendMail._SenderAddress = strSenderAddress
                    '    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                    '    objSendMail.SendMail(intCompanyId)
                    '    objSendMail = Nothing
                    'Next
                    'Gajanan [27-May-2019] -- End
                End If
            End If


        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: SetEmailBody; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Gajanan [27-May-2019] -- End





    Public Function IsApproverPresent(ByVal eScreenType As enScreenName, _
                                      ByVal strDatabaseName As String, _
                                      ByVal strUserAccessMode As String, _
                                      ByVal intCompanyId As Integer, _
                                      ByVal intYearId As Integer, _
                                      ByVal intPrivilegeId As Integer, _
                                      ByVal intUserId As Integer, _
                                      ByVal xEmployeeAsOnDate As String, _
                                      ByVal intEmployeeUnkid As Integer, _
                                      Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean
        'Gajanan [17-April-2019] -- delete eDates






        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Dim strMessage As String = ""

        mstrMessage = ""

        'Gajanan [17-April-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        Dim blnFlag As Boolean = False
        'Gajanan [17-April-2019] -- End

        Try
            Dim strUACJoin, strUACFilter As String
            strUACJoin = "" : strUACFilter = ""

            Dim strFilter As String = String.Empty
            Dim strJoin As String = String.Empty
            Dim strSelect As String = String.Empty
            Dim strAccessJoin As String = String.Empty
            Dim strOuterJoin As String = String.Empty
            Dim strFinalString As String = ""


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.




            'StrQ = "SELECT " & _
            '       "     UM.userunkid " & _
            '       "    ,UM.username " & _
            '       "    ,UM.email " & _
            '       "    ,LM.priority " & _
            '       "    ,LM.empapplevelname AS LvName " & _
            '       "FROM " & strDatabaseName & "..hremp_appusermapping AS EM " & _
            '       "    JOIN hrmsConfiguration..cfuser_master UM ON UM.userunkid = EM.mapuserunkid " & _
            '       "    JOIN hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
            '       "    JOIN hrmsConfiguration..cfcompanyaccess_privilege AS CP ON CP.userunkid = UM.userunkid " & _
            '       "    JOIN hrmsConfiguration..cfuser_privilege AS UP ON UM.userunkid = UP.userunkid " & _
            '       "WHERE isvoid = 0 AND EM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " AND LM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
            '       " AND CP.yearunkid = @Y AND UP.privilegeunkid = @P "


            StrQ = "SELECT " & _
                   "     UM.userunkid " & _
                   "    ,UM.username " & _
                   "    ,UM.email " & _
                   "    ,LM.priority " & _
                   "    ,LM.empapplevelname AS LvName " & _
                   "FROM " & strDatabaseName & "..hremp_appusermapping AS EM " & _
                   "    JOIN hrmsConfiguration..cfuser_master UM ON UM.userunkid = EM.mapuserunkid " & _
                   "    JOIN hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
                   "    JOIN hrmsConfiguration..cfcompanyaccess_privilege AS CP ON CP.userunkid = UM.userunkid " & _
                   "    JOIN hrmsConfiguration..cfuser_privilege AS UP ON UM.userunkid = UP.userunkid " & _
                  "WHERE isvoid = 0 AND EM.isactive = 1 AND EM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " AND LM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
                   " AND CP.yearunkid = @Y AND UP.privilegeunkid = @P "


            'Gajanan [17-April-2019] -- End


            objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")
            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If
            'Gajanan [17-April-2019] -- End

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.


            If dsList.Tables(0).Rows.Count <= 0 Then

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'strMessage = Language.getMessage(mstrModuleName, 5, "Sorry, No approver(s) defined based on selected for employee user access setting.")
                mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, No approver(s) defined based on selected for employee user access setting.")
                'Exit Try
                'Gajanan [17-April-2019] -- End

            End If


            'Dim blnFlag As Boolean = False
            'Gajanan [17-April-2019] -- End

            For Each dr As DataRow In dsList.Tables(0).Rows
                modGlobal.NewAccessLevelFilterString(strUACJoin, strUACFilter, eZeeDate.convertDate(xEmployeeAsOnDate), True, strDatabaseName, CInt(dr("userunkid")), intCompanyId, intYearId, strUserAccessMode, "AEM", True)
                StrQ = "SELECT " & _
                   "     AEM.employeeunkid " & _
                   "    ,ISNULL(T.departmentunkid,0) AS departmentunkid " & _
                   "    ,ISNULL(J.jobunkid,0) AS jobunkid " & _
                   "    ,ISNULL(T.classgroupunkid,0) AS classgroupunkid " & _
                   "    ,ISNULL(T.classunkid,0) AS classunkid " & _
                   "    ,ISNULL(T.stationunkid,0) AS stationunkid " & _
                   "    ,ISNULL(T.deptgroupunkid,0) AS deptgroupunkid " & _
                   "    ,ISNULL(T.sectiongroupunkid,0) AS sectiongroupunkid " & _
                   "    ,ISNULL(T.sectionunkid,0) AS sectionunkid " & _
                   "    ,ISNULL(T.unitgroupunkid,0) AS unitgroupunkid " & _
                   "    ,ISNULL(T.unitunkid,0) AS unitunkid " & _
                   "    ,ISNULL(T.teamunkid,0) AS teamunkid " & _
                   "    ,ISNULL(J.jobgroupunkid,0) AS jobgroupunkid " & _
                   "FROM " & strDatabaseName & "..hremployee_master AS AEM "
                If strUACJoin.Trim.Length > 0 Then
                    StrQ &= strUACJoin
                End If
                StrQ &= "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        stationunkid " & _
                        "       ,deptgroupunkid " & _
                        "       ,departmentunkid " & _
                        "       ,sectiongroupunkid " & _
                        "       ,sectionunkid " & _
                        "       ,unitgroupunkid " & _
                        "       ,unitunkid " & _
                        "       ,teamunkid " & _
                        "       ,classgroupunkid " & _
                        "       ,classunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "   FROM hremployee_transfer_tran " & _
                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                        ") AS T ON T.employeeunkid = AEM.employeeunkid AND T.Rno = 1 " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        jobgroupunkid " & _
                        "       ,jobunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                        "   FROM hremployee_categorization_tran " & _
                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' " & _
                            ") AS J ON J.employeeunkid = AEM.employeeunkid AND J.Rno = 1 " & _
                            "WHERE AEM.employeeunkid = '" & intEmployeeUnkid & "' "

                Dim dsEmp As DataSet = objDataOperation.ExecQuery(StrQ, "List")
                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If
                'Gajanan [17-April-2019] -- End

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If

                If dsEmp.Tables(0).Rows.Count > 0 Then
                    blnFlag = True
                    Exit For
                End If
            Next

            If blnFlag = False Then

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'strMessage = Language.getMessage(mstrModuleName, 5, "Sorry, No approver(s) defined based on selected for employee user access setting.")
                mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, No approver(s) defined based on selected for employee user access setting.")


                'Exit Try
                'Gajanan [17-April-2019] -- End

            End If

            Return blnFlag


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsApproverPresent; Module Name: " & mstrModuleName)
        Finally

          
        End Try
        'Return strMessage
    End Function

    Public Function getOperationTypeList(Optional ByVal strList As String = "List", Optional ByVal blnAddSelect As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            If strList.Trim.Length <= 0 Then strList = "List"
            If blnAddSelect Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            StrQ &= "SELECT '" & CInt(enOperationType.ADDED) & "' AS Id, @ADDED AS NAME " & _
                    "UNION SELECT '" & CInt(enOperationType.EDITED) & "' AS Id, @EDITED AS NAME " & _
                    "UNION SELECT '" & CInt(enOperationType.DELETED) & "' AS Id, @DELETED AS NAME "

            objDataOperation.AddParameter("@select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 47, "Select"))
            objDataOperation.AddParameter("@ADDED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 48, "Newly Added Information"))
            objDataOperation.AddParameter("@EDITED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 49, "Information Edited"))
            objDataOperation.AddParameter("@DELETED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 50, "Information Deleted"))

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getOperationTypeList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function


    Public Function getDependantColumnValue(ByVal objdataopration As clsDataOperation, ByVal colName As String, ByVal colValue As String) As String
        Dim dtColvalue As DataTable = Nothing
        Dim ColumnValue As String = String.Empty
        Dim strQ As String = String.Empty

        Try

            If colName = "benefitgroupunkid" Or colName = "membership_categoryunkid" Then
                dtColvalue = objdataopration.ExecQuery("select name from cfcommon_master where masterunkid = '" & colValue & "'", "Colvalue").Tables("Colvalue")

                If dtColvalue.Rows.Count > 0 Then
                    ColumnValue = dtColvalue.Rows(0)("name").ToString()
                    Return ColumnValue
                End If

            ElseIf colName = "benefitplanunkid" Then
                dtColvalue = objdataopration.ExecQuery("select benefitplanname from hrbenefitplan_master where benefitplanunkid = '" & colValue & "'", "Colvalue").Tables("Colvalue")

                If dtColvalue.Rows.Count > 0 Then
                    ColumnValue = dtColvalue.Rows(0)("benefitplanname").ToString()
                    Return ColumnValue
                End If


            ElseIf colName = "value_id" Then


                strQ = "SELECT * from ( " & _
                       "SELECT 0 AS Id,@Select AS NAME " & _
                       " UNION SELECT 1 AS Id,@Value AS NAME " & _
                       " UNION SELECT 2 AS Id,@Percent AS NAME " & _
                       ") as valueType " & _
                       "WHERE valueType.id = " & colValue & " "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 106, "Select"))
                objDataOperation.AddParameter("@Value", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 107, "Value"))
                objDataOperation.AddParameter("@Percent", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 108, "Percent"))
                dtColvalue = objDataOperation.ExecQuery(strQ, "ColumnValue").Tables("ColumnValue")

                If dtColvalue.Rows.Count > 0 Then
                    ColumnValue = dtColvalue.Rows(0)("name").ToString()
                End If


            ElseIf colName = "membershipunkid" Then
                dtColvalue = objdataopration.ExecQuery("select membershipname from hrmembership_master where membershipunkid = '" & colValue & "'", "Colvalue").Tables("Colvalue")

                If dtColvalue.Rows.Count > 0 Then
                    ColumnValue = dtColvalue.Rows(0)("membershipname").ToString()
                    Return ColumnValue
                End If

            End If

        Catch ex As Exception

        End Try
        Return ColumnValue
    End Function

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Rejection Remark:")
			Language.setMessage(mstrModuleName, 3, "Approval Remark:")
            Language.setMessage(mstrModuleName, 5, "Sorry, No approver(s) defined based on selected for employee user access setting.")
            Language.setMessage(mstrModuleName, 47, "Select")
			Language.setMessage(mstrModuleName, 48, "Newly Added Information")
			Language.setMessage(mstrModuleName, 49, "Information Edited")
			Language.setMessage(mstrModuleName, 50, "Information Deleted")
			Language.setMessage(mstrModuleName, 51, "Notification For Bio Data Changes")
			Language.setMessage(mstrModuleName, 52, "Notification For Approval of Bio Data Changes")
			Language.setMessage(mstrModuleName, 53, "Notification For Rejection of Bio Data Changes")
			Language.setMessage(mstrModuleName, 54, "Please note that the following changes on bio data for")
			Language.setMessage(mstrModuleName, 55, "have been made and submitted for approval")
			Language.setMessage(mstrModuleName, 56, "Please login to Aruti application to approve/reject the changes.")
			Language.setMessage(mstrModuleName, 58, "have been approved")
			Language.setMessage(mstrModuleName, 59, "have been rejected")
			Language.setMessage(mstrModuleName, 60, "Skill")
			Language.setMessage(mstrModuleName, 61, "Referee")
			Language.setMessage(mstrModuleName, 63, "Experience")
			Language.setMessage(mstrModuleName, 66, "Dependant And Benifits")
			Language.setMessage(mstrModuleName, 70, "Employee Identiy")
			Language.setMessage(mstrModuleName, 71, "Employee Membership")
			Language.setMessage(mstrModuleName, 72, "Dependant Status")
			Language.setMessage("clsMasterData", 73, "Male")
			Language.setMessage("clsMasterData", 74, "Female")
			Language.setMessage(mstrModuleName, 100, "Pending")
			Language.setMessage(mstrModuleName, 101, "Approved")
			Language.setMessage(mstrModuleName, 102, "Rejected")
			Language.setMessage(mstrModuleName, 103, "Pending for Approval")
			Language.setMessage("clsMasterData", 106, "Select")
			Language.setMessage("clsMasterData", 107, "Value")
			Language.setMessage("clsMasterData", 108, "Percent")
			Language.setMessage("frmEmployeeMaster", 147, "Dear")
			Language.setMessage(mstrModuleName, 200, "Particulars")
			Language.setMessage(mstrModuleName, 201, "Values")
			Language.setMessage(mstrModuleName, 202, "Particular")
			Language.setMessage(mstrModuleName, 203, "New Value")
			Language.setMessage(mstrModuleName, 204, "Old Value")
			Language.setMessage(mstrModuleName, 301, "Please note bio data changes made by")
			Language.setMessage(mstrModuleName, 302, "Following Information")
			Language.setMessage(mstrModuleName, 303, "Newly Added")
			Language.setMessage(mstrModuleName, 304, "Edited")
			Language.setMessage(mstrModuleName, 305, "Deleted")
			Language.setMessage(mstrModuleName, 306, "on")
			Language.setMessage(mstrModuleName, 400, "Dependant")
			Language.setMessage(mstrModuleName, 786, "Qualification")
			Language.setMessage(mstrModuleName, 800, "BeniftInfo")
			Language.setMessage(mstrModuleName, 801, "Benefit Group")
			Language.setMessage(mstrModuleName, 802, "Benefit Type")
			Language.setMessage(mstrModuleName, 803, "Value Basis")
			Language.setMessage(mstrModuleName, 804, "Percent")
			Language.setMessage(mstrModuleName, 805, "Amount")
			Language.setMessage(mstrModuleName, 806, "Opration")
			Language.setMessage(mstrModuleName, 807, "Membership Type")
			Language.setMessage(mstrModuleName, 808, "Membership")
			Language.setMessage(mstrModuleName, 809, "Membership No")
			Language.setMessage(mstrModuleName, 810, "Identity Information")
			Language.setMessage(mstrModuleName, 811, "Identity Type")
			Language.setMessage(mstrModuleName, 812, "Serial No")
			Language.setMessage(mstrModuleName, 813, "Identity No")
			Language.setMessage(mstrModuleName, 814, "Country")
			Language.setMessage(mstrModuleName, 815, "Place Of Issue")
			Language.setMessage(mstrModuleName, 816, "DL Class")
			Language.setMessage(mstrModuleName, 817, "Issue Date")
			Language.setMessage(mstrModuleName, 818, "Expiry Date")
			Language.setMessage(mstrModuleName, 819, "IsDefault")
			Language.setMessage(mstrModuleName, 820, "Opration")
			Language.setMessage(mstrModuleName, 821, "Newly Added")
			Language.setMessage(mstrModuleName, 822, "Edited")
			Language.setMessage(mstrModuleName, 823, "Deleted")
			Language.setMessage(mstrModuleName, 824, "Membership Category")
			Language.setMessage(mstrModuleName, 825, "Membership")
			Language.setMessage(mstrModuleName, 826, "Membership No")
			Language.setMessage(mstrModuleName, 827, "Issue Date")
			Language.setMessage(mstrModuleName, 828, "Start Date")
			Language.setMessage(mstrModuleName, 829, "Expiry Date")
			Language.setMessage(mstrModuleName, 830, "Remark")
			Language.setMessage(mstrModuleName, 831, "Membership Information")
			Language.setMessage(mstrModuleName, 901, "Present Address")
			Language.setMessage(mstrModuleName, 902, "Domicile Address")
			Language.setMessage(mstrModuleName, 903, "Recruitment Address")
			Language.setMessage(mstrModuleName, 904, "Emergency Address1")
			Language.setMessage(mstrModuleName, 905, "Emergency Address2")
			Language.setMessage(mstrModuleName, 906, "Emergency Address3")
			Language.setMessage(mstrModuleName, 907, "Birth Info")
			Language.setMessage(mstrModuleName, 908, "Other Info")
			Language.setMessage(mstrModuleName, 909, "has been change")
			Language.setMessage(mstrModuleName, 911, "Membership Info")
			Language.setMessage(mstrModuleName, 912, "Please note your bio data changes made by you on")
			Language.setMessage(mstrModuleName, 913, "on")
			Language.setMessage(mstrModuleName, 915, "Please note your bio data changes made by you on")
			Language.setMessage(mstrModuleName, 916, "Dependant Status")
			Language.setMessage(mstrModuleName, 917, "There are some attachment(s) which are added")
			Language.setMessage(mstrModuleName, 918, "There are some attachment(s) which are removed")
			Language.setMessage(mstrModuleName, 1000, "There are some attachment(s) which are added and removed")
			Language.setMessage(mstrModuleName, 1999, "employee(s)")
			Language.setMessage(mstrModuleName, 2000, "Skill Category")
			Language.setMessage(mstrModuleName, 2001, "Skill")
			Language.setMessage(mstrModuleName, 2002, "Discription")
			Language.setMessage(mstrModuleName, 2003, "Refree Name")
			Language.setMessage(mstrModuleName, 2004, "Reference Type")
			Language.setMessage(mstrModuleName, 2005, "Address")
			Language.setMessage(mstrModuleName, 2006, "Country")
			Language.setMessage(mstrModuleName, 2007, "State")
			Language.setMessage(mstrModuleName, 2008, "City")
			Language.setMessage(mstrModuleName, 2009, "Gender")
			Language.setMessage(mstrModuleName, 2010, "Company")
			Language.setMessage(mstrModuleName, 2011, "Mobile No.")
			Language.setMessage(mstrModuleName, 2012, "Position")
			Language.setMessage(mstrModuleName, 2013, "Telephone No.")
			Language.setMessage(mstrModuleName, 2014, "Email Id")
			Language.setMessage(mstrModuleName, 2015, "Qualification Group")
			Language.setMessage(mstrModuleName, 2016, "Qualification")
			Language.setMessage(mstrModuleName, 2017, "Reference No")
			Language.setMessage(mstrModuleName, 2018, "Start Date")
			Language.setMessage(mstrModuleName, 2019, "End Date")
			Language.setMessage(mstrModuleName, 2020, "Institute")
			Language.setMessage(mstrModuleName, 2021, "Remark")
			Language.setMessage(mstrModuleName, 2022, "Result")
			Language.setMessage(mstrModuleName, 2023, "IsSync")
			Language.setMessage(mstrModuleName, 2024, "Company")
			Language.setMessage(mstrModuleName, 2025, "Job")
			Language.setMessage(mstrModuleName, 2026, "Reason")
			Language.setMessage(mstrModuleName, 2027, "Dependant First Name")
			Language.setMessage(mstrModuleName, 2028, "Dependant Last Name")
			Language.setMessage(mstrModuleName, 2029, "Relation")
			Language.setMessage(mstrModuleName, 2030, "Isbeneficiary")
			Language.setMessage(mstrModuleName, 2031, "Birthdate")
			Language.setMessage(mstrModuleName, 2032, "Zipcode")
			Language.setMessage(mstrModuleName, 2033, "Town1")
			Language.setMessage(mstrModuleName, 2034, "IdNo")
			Language.setMessage(mstrModuleName, 2035, "Dependant Middle Name")
			Language.setMessage(mstrModuleName, 2036, "Effective Date")
			Language.setMessage(mstrModuleName, 2037, "Address-1")
			Language.setMessage(mstrModuleName, 2038, "Address-2")
			Language.setMessage(mstrModuleName, 2039, "Post Country")
			Language.setMessage(mstrModuleName, 2040, "Post Town")
			Language.setMessage(mstrModuleName, 2041, "Post Code")
			Language.setMessage(mstrModuleName, 2042, "Prov/Region")
			Language.setMessage(mstrModuleName, 2043, "Road/Street")
			Language.setMessage(mstrModuleName, 2044, "Estate")
			Language.setMessage(mstrModuleName, 2045, "Street1")
			Language.setMessage(mstrModuleName, 2046, "Region1")
			Language.setMessage(mstrModuleName, 2047, "Chiefdom")
			Language.setMessage(mstrModuleName, 2048, "Village")
			Language.setMessage(mstrModuleName, 2049, "Plot No")
			Language.setMessage(mstrModuleName, 2050, "Alternative No.")
			Language.setMessage(mstrModuleName, 2051, "Fax")
			Language.setMessage(mstrModuleName, 2052, "Ward No")
			Language.setMessage(mstrModuleName, 2053, "Certificate No.")
			Language.setMessage(mstrModuleName, 2054, "Village1")
			Language.setMessage(mstrModuleName, 2055, "Identity Type")
			Language.setMessage(mstrModuleName, 2056, "Identity No.")
			Language.setMessage(mstrModuleName, 2057, "Isdefault")
			Language.setMessage(mstrModuleName, 2058, "Issued Place")
			Language.setMessage(mstrModuleName, 2059, "Dl Class")
			Language.setMessage(mstrModuleName, 2060, "Issue Date")
			Language.setMessage(mstrModuleName, 2061, "Expiry Date")
			Language.setMessage(mstrModuleName, 2062, "Membership Category")
			Language.setMessage(mstrModuleName, 2063, "Membership Name")
			Language.setMessage(mstrModuleName, 2064, "Membership No")
			Language.setMessage(mstrModuleName, 2065, "Issue Date")
			Language.setMessage(mstrModuleName, 2066, "Start Date")
			Language.setMessage(mstrModuleName, 2067, "Expiry Date")
			Language.setMessage(mstrModuleName, 2068, "Effective Period")
			Language.setMessage(mstrModuleName, 2069, "First Name")
			Language.setMessage(mstrModuleName, 2070, "Last Name")
			Language.setMessage(mstrModuleName, 2071, "Nationality")
			Language.setMessage(mstrModuleName, 2073, "Notification For Approval of Bio Data Changes")
			Language.setMessage(mstrModuleName, 2074, "Notification For Rejection of Bio Data Changes")
			Language.setMessage(mstrModuleName, 9001, "some employees")
			Language.setMessage(mstrModuleName, 53, "Notification For Approval of Bio Data Changes")
			Language.setMessage("clsMasterData", 72, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
