﻿Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master

Public Class clsEmp_Qualification_Approval_Tran
    Private Const mstrModuleName = "clsEmp_Qualification_Approval_Tran"
    Dim objDataOperation As clsDataOperation
    Private objDocument As New clsScan_Attach_Documents
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mstrTranguid As String = String.Empty
    Private mdtTransactiondate As Date
    Private mintMappingunkid As Integer
    Private mstrApprovalremark As String = String.Empty
    Private mblnIsfinal As Boolean
    Private mintStatusunkid As Integer
    Private mintQualificationtranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintQualificationgroupunkid As Integer
    Private mintQualificationunkid As Integer
    Private mdtCertificatedate As Date
    Private mstrReference_No As String = String.Empty
    Private mdtAward_Start_Date As Date
    Private mdtAward_End_Date As Date
    Private mintInstituteunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mintResultunkid As Integer
    Private mdecGpacode As Decimal
    Private mstrOther_Qualificationgrp As String = String.Empty
    Private mstrOther_Qualification As String = String.Empty
    Private mstrOther_Resultcode As String = String.Empty
    Private mstrOther_Institute As String = String.Empty
    Private mblnIsvoid As Boolean = False
    Private mdtAuditdatetime As Date
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer
    Private mstrIp As String = String.Empty
    Private mstrHost As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean
    Private mblnIsprocess As Boolean

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    'Private mintLinkedMasterId As Integer = 0
    'Gajanan [22-Feb-2019] -- End

    Private mintLoginEmployeeUnkid As Integer = 0
    Private mintOperationTypeId As Integer = 0


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mstrnewattachdocumnetid As String = String.Empty
    Private mstrdeleteattachdocumnetid As String = String.Empty
    'Gajanan [22-Feb-2019] -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranguid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Tranguid() As String
        Get
            Return mstrTranguid
        End Get
        Set(ByVal value As String)
            mstrTranguid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvalremark
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Approvalremark() As String
        Get
            Return mstrApprovalremark
        End Get
        Set(ByVal value As String)
            mstrApprovalremark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set qualificationtranunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Qualificationtranunkid() As Integer
        Get
            Return mintQualificationtranunkid
        End Get
        Set(ByVal value As Integer)
            mintQualificationtranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set qualificationgroupunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Qualificationgroupunkid() As Integer
        Get
            Return mintQualificationgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintQualificationgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set qualificationunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Qualificationunkid() As Integer
        Get
            Return mintQualificationunkid
        End Get
        Set(ByVal value As Integer)
            mintQualificationunkid = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Certificatedate() As Date
        Get
            Return mdtCertificatedate
        End Get
        Set(ByVal value As Date)
            mdtCertificatedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reference_no
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Reference_No() As String
        Get
            Return mstrReference_No
        End Get
        Set(ByVal value As String)
            mstrReference_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set award_start_date
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Award_Start_Date() As Date
        Get
            Return mdtAward_Start_Date
        End Get
        Set(ByVal value As Date)
            mdtAward_Start_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set award_end_date
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Award_End_Date() As Date
        Get
            Return mdtAward_End_Date
        End Get
        Set(ByVal value As Date)
            mdtAward_End_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set instituteunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Instituteunkid() As Integer
        Get
            Return mintInstituteunkid
        End Get
        Set(ByVal value As Integer)
            mintInstituteunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set resultunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Resultunkid() As Integer
        Get
            Return mintResultunkid
        End Get
        Set(ByVal value As Integer)
            mintResultunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gpacode
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Gpacode() As Decimal
        Get
            Return mdecGpacode
        End Get
        Set(ByVal value As Decimal)
            mdecGpacode = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set other_qualificationgrp
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Other_Qualificationgrp() As String
        Get
            Return mstrOther_Qualificationgrp
        End Get
        Set(ByVal value As String)
            mstrOther_Qualificationgrp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set other_qualification
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Other_Qualification() As String
        Get
            Return mstrOther_Qualification
        End Get
        Set(ByVal value As String)
            mstrOther_Qualification = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set other_resultcode
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Other_Resultcode() As String
        Get
            Return mstrOther_Resultcode
        End Get
        Set(ByVal value As String)
            mstrOther_Resultcode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set other_institute
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Other_Institute() As String
        Get
            Return mstrOther_Institute
        End Get
        Set(ByVal value As String)
            mstrOther_Institute = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set auditdatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Auditdatetime() As Date
        Get
            Return mdtAuditdatetime
        End Get
        Set(ByVal value As Date)
            mdtAuditdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set host
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Host() As String
        Get
            Return mstrHost
        End Get
        Set(ByVal value As String)
            mstrHost = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isprocess
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isprocessed() As Boolean
        Get
            Return mblnIsprocess
        End Get
        Set(ByVal value As Boolean)
            mblnIsprocess = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set linkedmasterid
    ''' Modify By: Gajanan
    ''' </summary>


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    'Public Property _LinkedMasterId() As Integer
    '    Get
    '        Return mintLinkedMasterId
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintLinkedMasterId = value
    '    End Set
    'End Property
    'Gajanan [22-Feb-2019] -- End

    Public Property _LoginEmployeeUnkid() As Integer
        Get
            Return mintLoginEmployeeUnkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeUnkid = value
        End Set
    End Property

    Public Property _OperationTypeId() As Integer
        Get
            Return mintOperationTypeId
        End Get
        Set(ByVal value As Integer)
            mintOperationTypeId = value
        End Set
    End Property


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Public Property _Newattacheddocumnetid() As String
        Get
            Return mstrnewattachdocumnetid
        End Get
        Set(ByVal value As String)
            mstrnewattachdocumnetid = value
        End Set
    End Property

    Public Property _Deletedattachdocumnetid() As String
        Get
            Return mstrdeleteattachdocumnetid
        End Get
        Set(ByVal value As String)
            mstrdeleteattachdocumnetid = value
        End Set
    End Property

    'Gajanan [22-Feb-2019] -- End


#End Region

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal strFilerString As String = "", _
                            Optional ByVal IsUsedAsMSS As Boolean = True, _
                            Optional ByVal mblnAddApprovalCondition As Boolean = True) As DataSet

        'S.SANDEEP [20-JUN-2018] -- 'Enhancement - Implementing Employee Approver Flow For NMB .[Optional ByVal mblnAddApprovalCondition As Boolean = True]

        'Pinkal (28-Dec-2015) -- Start    'Enhancement - Working on Changes in SS for Employee Master. [Optional ByVal IsUsedAsMSS As Boolean = True] 

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , mblnAddApprovalCondition)

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation

                StrQ = "SELECT " & _
                       "     ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                       "    ,CASE WHEN ISNULL(cfcommon_master.masterunkid, 0) > 0 THEN ISNULL(cfcommon_master.name, '') ELSE ISNULL(hremp_qualification_approval_tran.other_qualificationgrp, '') END AS QGrp " & _
                       "    ,CASE WHEN ISNULL(hrqualification_master.qualificationunkid, 0) > 0 THEN ISNULL(hrqualification_master.qualificationname, '') ELSE ISNULL(hremp_qualification_approval_tran.other_qualification, '') END AS Qualify " & _
                       "    ,CONVERT(CHAR(8), hremp_qualification_approval_tran.transactiondate, 112) AS Date " & _
                       "    ,CONVERT(CHAR(8), hremp_qualification_approval_tran.award_start_date, 112) AS StDate " & _
                       "    ,CONVERT(CHAR(8), hremp_qualification_approval_tran.award_end_date, 112) AS EnDate " & _
                       "    ,ISNULL(hremp_qualification_approval_tran.reference_no, '') AS RefNo " & _
                       "    ,CASE WHEN ISNULL(hrqualification_master.qualificationunkid, 0) > 0 THEN ISNULL(hrinstitute_master.institute_name, '') ELSE ISNULL(hremp_qualification_approval_tran.other_institute,'') END AS Institute " & _
                       "    ,hremployee_master.employeeunkid AS EmpId " & _
                       "    ,ISNULL(cfcommon_master.masterunkid,0) AS QGrpId " & _
                       "    ,ISNULL(hrqualification_master.qualificationunkid,0) AS QualifyId " & _
                       "    ,hremp_qualification_approval_tran.qualificationtranunkid AS QulifyTranId " & _
                       "    ,hrinstitute_master.instituteunkid As InstituteId " & _
                       "    ,0 AS loginemployeeunkid " & _
                       "    ,ISNULL(hremp_qualification_approval_tran.resultunkid,0) As resultunkid " & _
                       "    ,hremp_qualification_approval_tran.gpacode As gpacode " & _
                       "    ,ISNULL(hremp_qualification_approval_tran.other_qualificationgrp,'') As other_qualificationgrp " & _
                       "    ,ISNULL(hremp_qualification_approval_tran.other_qualification,'') As other_qualification " & _
                       "    ,ISNULL(hremp_qualification_approval_tran.other_resultcode,'') As other_resultcode  " & _
                       "    ,ISNULL(hremp_qualification_approval_tran.other_institute,'') As other_institute  " & _
                       "    ,hremp_qualification_approval_tran.tranguid As tranguid " & _
                       "    ,ISNULL(hremp_qualification_approval_tran.operationtypeid,0) AS operationtypeid " & _
                       "    ,CASE WHEN ISNULL(hremp_qualification_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.ADDED) & "' THEN @ADDED " & _
                       "          WHEN ISNULL(hremp_qualification_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.EDITED) & "' THEN @EDITED " & _
                       "          WHEN ISNULL(hremp_qualification_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeDataApproval.enOperationType.DELETED) & "' THEN @DELETED " & _
                       "     END AS OperationType " & _
                       "FROM hremp_qualification_approval_tran "
                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                StrQ &= "   LEFT JOIN hremp_qualification_tran ON hremp_qualification_approval_tran.qualificationtranunkid = hremp_qualification_tran.qualificationtranunkid " & _
                       "    LEFT JOIN hrinstitute_master ON hremp_qualification_approval_tran.instituteunkid = hrinstitute_master.instituteunkid " & _
                       "    LEFT JOIN hrqualification_master ON hremp_qualification_approval_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                       "    LEFT JOIN hremployee_master ON hremp_qualification_approval_tran.employeeunkid = hremployee_master.employeeunkid " & _
                       "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_qualification_approval_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.QUALIFICATION_COURSE_GROUP
                'Gajanan [17-April-2019] -- End

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then

                        'Gajanan [27-May-2019] -- Start              
                        'StrQ &= xUACQry
                        StrQ &= "LEFT " & xUACQry
                        'Gajanan [27-Ma y-2019] -- End


                    End If
                End If


                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= "WHERE ISNULL(hremp_qualification_approval_tran.isvoid,0) = 0 AND ISNULL(hremp_qualification_approval_tran.isprocessed,0) = 0 "

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If strFilerString.Trim.Length > 0 Then
                    StrQ &= " AND " & strFilerString
                End If

                objDo.AddParameter("@ADDED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 62, "Newly Added Information"))
                objDo.AddParameter("@EDITED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 63, "Information Edited"))
                objDo.AddParameter("@DELETED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeDataApproval", 64, "Information Deleted"))

                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremp_qualification_approval_tran) </purpose>
    Public Function Insert(ByVal intCompanyId As Integer, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal blnFromApproval As Boolean = False, _
                           Optional ByVal mdtTable As DataTable = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        If blnFromApproval = False Then
            If isExist(mintEmployeeunkid, mintQualificationunkid, mstrReference_No, mintInstituteunkid, mintOperationTypeId, "", mstrOther_Qualification, objDataOperation) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Qualification is already assigned to particular employee. Please assign new Qualification.")
                Return False
            End If
        End If

        objDataOperation.ClearParameters()
        Try

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            'If mintLinkedMasterId <= 0 Then
            '    Dim iCount As Integer = 1
            '    While iCount > 0
            '        strQ = "SELECT (ISNULL(MAX(linkedmasterid),0) + 1) AS ival FROM hremp_qualification_approval_tran "
            '        dsList = objDataOperation.ExecQuery(strQ, "List")
            '        If objDataOperation.ErrorMessage <> "" Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '        mintLinkedMasterId = CInt(dsList.Tables("List").Rows(0)("ival"))
            '        strQ = "SELECT 1 FROM hremp_qualification_approval_tran WHERE linkedmasterid = '" & mintLinkedMasterId & "' "
            '        iCount = objDataOperation.RecordCount(strQ)
            '        If objDataOperation.ErrorMessage <> "" Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '        If iCount <= 0 Then
            '            Exit While
            '        End If
            '    End While
            'End If
            'Gajanan [22-Feb-2019] -- End


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            Dim objDocument As New clsScan_Attach_Documents
            Dim dtTran As DataTable = objDocument._Datatable
            Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\"

            Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.QUALIFICATIONS).Tables(0).Rows(0)("Name").ToString
            If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"

            Dim dr As DataRow

            If IsNothing(mdtTable) = False Then
                For Each drow As DataRow In mdtTable.Rows
                    dr = dtTran.NewRow
                    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                    dr("documentunkid") = drow("documentunkid")
                    dr("employeeunkid") = drow("employeeunkid")
                    dr("filename") = drow("filename")
                    dr("scanattachrefid") = drow("scanattachrefid")
                    dr("modulerefid") = drow("modulerefid")
                    dr("form_name") = drow("form_name")
                    dr("userunkid") = drow("userunkid")
                    dr("transactionunkid") = drow("transactionunkid")
                    dr("userunkid") = User._Object._Userunkid
                    dr("attached_date") = drow("attached_date")
                    dr("orgfilepath") = drow("localpath")
                    dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                    dr("AUD") = drow("AUD")
                    'dr("userunkid") = User._Object._Userunkid

                    dr("fileuniquename") = drow("fileuniquename")
                    dr("filepath") = drow("filepath")
                    dr("filesize") = drow("filesize_kb")

                    dtTran.Rows.Add(dr)

                Next
                objDocument._Datatable = dtTran
                objDocument.InsertUpdateDelete_Documents(objDataOperation)
            End If

            'Gajanan [22-Feb-2019] -- End



            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate)
            End If
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@approvalremark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApprovalremark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationtranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationgroupunkid.ToString)
            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationunkid.ToString)
            objDataOperation.AddParameter("@reference_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReference_No.ToString)
            If mdtAward_Start_Date = Nothing Then
                objDataOperation.AddParameter("@award_start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@award_start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAward_Start_Date)
            End If

            If mdtAward_End_Date = Nothing Then
                objDataOperation.AddParameter("@award_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@award_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAward_End_Date)
            End If

            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@resultunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mintResultunkid)
            objDataOperation.AddParameter("@gpacode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdecGpacode)
            objDataOperation.AddParameter("@other_qualificationgrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_Qualificationgrp.ToString)
            objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_Qualification.ToString)
            objDataOperation.AddParameter("@other_resultcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_Resultcode.ToString)
            objDataOperation.AddParameter("@other_institute", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOther_Institute.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHost.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocess.ToString)

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation.AddParameter("@linkedmasterid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLinkedMasterId)
            'Gajanan [22-Feb-2019] -- End

            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeUnkid)
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationTypeId)

            If mdtCertificatedate = Nothing Then
                objDataOperation.AddParameter("@certificatedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@certificatedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCertificatedate)
            End If

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            If objDocument._Newattachdocumentids.Length > 0 Then
                objDataOperation.AddParameter("@newattachdocumentid", SqlDbType.NVarChar, objDocument._Newattachdocumentids.Length, objDocument._Newattachdocumentids)
            Else
                objDataOperation.AddParameter("@newattachdocumentid", SqlDbType.NVarChar, mstrnewattachdocumnetid.Length, mstrnewattachdocumnetid)
            End If

            If objDocument._Deleteattachdocumentids.Length > 0 Then
                objDataOperation.AddParameter("@deleteattachdocumentid", SqlDbType.NVarChar, objDocument._Deleteattachdocumentids.Length, objDocument._Deleteattachdocumentids)
            Else
                objDataOperation.AddParameter("@deleteattachdocumentid", SqlDbType.NVarChar, mstrdeleteattachdocumnetid.Length, mstrdeleteattachdocumnetid)
            End If

            'Gajanan [22-Feb-2019] -- End

            strQ = "INSERT INTO hremp_qualification_approval_tran ( " & _
                      "  tranguid " & _
                      ", transactiondate " & _
                      ", mappingunkid " & _
                      ", approvalremark " & _
                      ", isfinal " & _
                      ", statusunkid " & _
                      ", qualificationtranunkid " & _
                      ", employeeunkid " & _
                      ", qualificationgroupunkid " & _
                      ", qualificationunkid " & _
                      ", reference_no " & _
                      ", award_start_date " & _
                      ", award_end_date " & _
                      ", instituteunkid " & _
                      ", remark " & _
                      ", resultunkid " & _
                      ", gpacode " & _
                      ", other_qualificationgrp " & _
                      ", other_qualification " & _
                      ", other_resultcode " & _
                      ", other_institute " & _
                      ", isvoid " & _
                      ", auditdatetime " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", ip " & _
                      ", host " & _
                      ", form_name " & _
                      ", isweb " & _
                      ", isprocessed " & _
                      ", loginemployeeunkid " & _
                      ", operationtypeid " & _
                      ", certificatedate " & _
                      ", newattachdocumentid " & _
                      ", deleteattachdocumentid " & _
                    ") VALUES (" & _
                      "  @tranguid " & _
                      ", @transactiondate " & _
                      ", @mappingunkid " & _
                      ", @approvalremark " & _
                      ", @isfinal " & _
                      ", @statusunkid " & _
                      ", @qualificationtranunkid " & _
                      ", @employeeunkid " & _
                      ", @qualificationgroupunkid " & _
                      ", @qualificationunkid " & _
                      ", @reference_no " & _
                      ", @award_start_date " & _
                      ", @award_end_date " & _
                      ", @instituteunkid " & _
                      ", @remark " & _
                      ", @resultunkid " & _
                      ", @gpacode " & _
                      ", @other_qualificationgrp " & _
                      ", @other_qualification " & _
                      ", @other_resultcode " & _
                      ", @other_institute " & _
                      ", @isvoid " & _
                      ", GetDate() " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @ip " & _
                      ", @host " & _
                      ", @form_name " & _
                      ", @isweb " & _
                      ", @isprocessed " & _
                      ", @loginemployeeunkid " & _
                      ", @operationtypeid " & _
                      ", @certificatedate " & _
                      ", @newattachdocumentid " & _
                      ", @deleteattachdocumentid " & _
                    ") "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtTable IsNot Nothing Then
'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.


                'Dim dtTab As DataTable = Nothing
               'Dim row As DataRow() = mdtTable.Select("form_name = '" & mstrForm_Name & "' and aud <> 'D'")
                'If row.Length > 0 Then
                '    row.ToList.ForEach(Function(x) UpdateRow(x, "transactionunkid", mintLinkedMasterId.ToString()))
                '    dtTab = row.CopyToDataTable()
                '    objDocument._Datatable = dtTab
                '    objDocument.InsertUpdateDelete_Documents(objDataOperation)
                '    If objDataOperation.ErrorMessage <> "" Then
                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '        Throw exForce
                '    End If
                'End If
            'End If


                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.





                'Dim objDocument As New clsScan_Attach_Documents
                'Dim dtTran As DataTable = objDocument._Datatable
                'Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\"

                'Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.QUALIFICATIONS).Tables(0).Rows(0)("Name").ToString
                'If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"

                'Dim dr As DataRow
                'For Each drow As DataRow In mdtTable.Rows
                '    dr = dtTran.NewRow
                '    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                '    dr("documentunkid") = drow("documentunkid")
                '    dr("employeeunkid") = drow("employeeunkid")
                '    dr("filename") = drow("filename")
                '    dr("scanattachrefid") = drow("scanattachrefid")
                '    dr("modulerefid") = drow("modulerefid")
                '    dr("form_name") = drow("form_name")
                '    dr("userunkid") = drow("userunkid")

                '    dr("transactionunkid") = mintLinkedMasterId

                '    dr("userunkid") = User._Object._Userunkid
                '    dr("attached_date") = drow("attached_date")
                '    dr("orgfilepath") = drow("localpath")
                '    dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                '    dr("AUD") = drow("AUD")
                '    'dr("userunkid") = User._Object._Userunkid

                '    dr("fileuniquename") = drow("fileuniquename")
                '    dr("filepath") = drow("filepath")
                '    dr("filesize") = drow("filesize_kb")

                '    dtTran.Rows.Add(dr)
                'Next
                'objDocument._Datatable = dtTran
                'objDocument.InsertUpdateDelete_Documents(objDataOperation)
                'Gajanan [22-Feb-2019] -- End


                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

'Gajanan [17-DEC-2018] -- End
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremp_qualification_approval_tran) </purpose>
    'Public Function Update() As Boolean
    '    If isExist(mstrName, mstrTranguid) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@tranguid", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrtranguid.ToString)
    '        objDataOperation.AddParameter("@transactiondate", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdttransactiondate.ToString)
    '        objDataOperation.AddParameter("@mappingunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintmappingunkid.ToString)
    '        objDataOperation.AddParameter("@approvalremark", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrapprovalremark.ToString)
    '        objDataOperation.AddParameter("@isfinal", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisfinal.ToString)
    '        objDataOperation.AddParameter("@statusunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstatusunkid.ToString)
    '        objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintqualificationtranunkid.ToString)
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintemployeeunkid.ToString)
    '        objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintqualificationgroupunkid.ToString)
    '        objDataOperation.AddParameter("@qualificationunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintqualificationunkid.ToString)
    '        objDataOperation.AddParameter("@transaction_date", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdttransaction_date.ToString)
    '        objDataOperation.AddParameter("@reference_no", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrreference_no.ToString)
    '        objDataOperation.AddParameter("@award_start_date", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtaward_start_date.ToString)
    '        objDataOperation.AddParameter("@award_end_date", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtaward_end_date.ToString)
    '        objDataOperation.AddParameter("@instituteunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintinstituteunkid.ToString)
    '        objDataOperation.AddParameter("@remark", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrremark.ToString)
    '        objDataOperation.AddParameter("@other_qualificationgrp", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrother_qualificationgrp.ToString)
    '        objDataOperation.AddParameter("@other_qualification", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrother_qualification.ToString)
    '        objDataOperation.AddParameter("@other_resultcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrother_resultcode.ToString)
    '        objDataOperation.AddParameter("@other_institute", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrother_institute.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@auditdatetime", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtauditdatetime.ToString)
    '        objDataOperation.AddParameter("@audittype", SqlDbType.int, eZeeDataType.INT_SIZE, mintaudittype.ToString)
    '        objDataOperation.AddParameter("@audituserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintaudituserunkid.ToString)
    '        objDataOperation.AddParameter("@ip", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrip.ToString)
    '        objDataOperation.AddParameter("@host", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrhost.ToString)
    '        objDataOperation.AddParameter("@form_name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrform_name.ToString)
    '        objDataOperation.AddParameter("@isweb", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisweb.ToString)
    '        objDataOperation.AddParameter("@isprocessed", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisprocess.ToString)
    '        objDataOperation.AddParameter("@linkedmasterid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLinkedMasterId)

    '        StrQ = "UPDATE hremp_qualification_approval_tran SET " & _
    '                  "  transactiondate = @transactiondate" & _
    '                  ", mappingunkid = @mappingunkid" & _
    '                  ", approvalremark = @approvalremark" & _
    '                  ", isfinal = @isfinal" & _
    '                  ", statusunkid = @statusunkid" & _
    '                  ", qualificationtranunkid = @qualificationtranunkid" & _
    '                  ", employeeunkid = @employeeunkid" & _
    '                  ", qualificationgroupunkid = @qualificationgroupunkid" & _
    '                  ", qualificationunkid = @qualificationunkid" & _
    '                  ", transaction_date = @transaction_date" & _
    '                  ", reference_no = @reference_no" & _
    '                  ", award_start_date = @award_start_date" & _
    '                  ", award_end_date = @award_end_date" & _
    '                  ", instituteunkid = @instituteunkid" & _
    '                  ", remark = @remark" & _
    '                  ", other_qualificationgrp = @other_qualificationgrp" & _
    '                  ", other_qualification = @other_qualification" & _
    '                  ", other_resultcode = @other_resultcode" & _
    '                  ", other_institute = @other_institute" & _
    '                  ", isvoid = @isvoid" & _
    '                  ", auditdatetime = @auditdatetime" & _
    '                  ", audittype = @audittype" & _
    '                  ", audituserunkid = @audituserunkid" & _
    '                  ", ip = @ip" & _
    '                  ", host = @host" & _
    '                  ", form_name = @form_name" & _
    '                  ", isweb = @isweb" & _
    '                  ", isprocessed = @isprocessed" & _
    '                  ", linkedmasterid = @linkedmasterid " & _
    '                "WHERE tranguid = @tranguid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremp_qualification_approval_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal xVoidReason As String, ByVal intCompanyId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            Dim objEmp_Qualification_Tran As clsEmp_Qualification_Tran
            objEmp_Qualification_Tran = New clsEmp_Qualification_Tran
            objEmp_Qualification_Tran._Qualificationtranunkid = intUnkid

            Me._Audittype = enAuditType.ADD
            Me._Award_End_Date = objEmp_Qualification_Tran._Award_End_Date

            Me._Award_Start_Date = objEmp_Qualification_Tran._Award_Start_Date

            Me._Employeeunkid = objEmp_Qualification_Tran._Employeeunkid
            Me._Qualificationgroupunkid = objEmp_Qualification_Tran._Qualificationgroupunkid
            Me._Qualificationunkid = objEmp_Qualification_Tran._Qualificationunkid
            Me._Reference_No = objEmp_Qualification_Tran._Reference_No
            Me._Remark = objEmp_Qualification_Tran._Remark
            Me._Instituteunkid = objEmp_Qualification_Tran._Instituteunkid
            Me._Certificatedate = objEmp_Qualification_Tran._Transaction_Date

            Me._Resultunkid = objEmp_Qualification_Tran._Resultunkid
            Me._Gpacode = objEmp_Qualification_Tran._GPAcode

            If objEmp_Qualification_Tran._Other_QualificationGrp.Trim.Length > 0 Then
                Me._Other_Qualificationgrp = objEmp_Qualification_Tran._Other_QualificationGrp
                Me._Other_Qualification = objEmp_Qualification_Tran._Other_Qualification
                Me._Other_Resultcode = objEmp_Qualification_Tran._other_ResultCode
                Me._Qualificationgroupunkid = 0
                Me._Qualificationunkid = 0
                Me._Resultunkid = 0
                Me._Other_Institute = objEmp_Qualification_Tran._Other_institute
            Else
                Me._Other_Qualificationgrp = ""
                Me._Other_Qualification = ""
                Me._Other_Resultcode = ""
                Me._Other_Institute = ""
            End If

            Me._Qualificationtranunkid = intUnkid
            Me._Isprocessed = False
            Me._Isvoid = False
            Me._Tranguid = Guid.NewGuid.ToString()
            Me._Transactiondate = Now
            Me._Approvalremark = ""
            Me._Mappingunkid = 0
            Me._Isfinal = False
            Me._OperationTypeId = clsEmployeeDataApproval.enOperationType.DELETED
            Me._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim objDocument As New clsScan_Attach_Documents
            Me._Deletedattachdocumnetid = objDocument.GetCSVAttachmentids(objEmp_Qualification_Tran._Employeeunkid, CInt(enScanAttactRefId.QUALIFICATIONS), intUnkid, objDataOperation)
            'Gajanan [22-Feb-2019] -- End

            blnFlag = Insert(intCompanyId, objDataOperation)

            If blnFlag = True AndAlso mstrMessage.Trim.Length <= 0 Then

                objEmp_Qualification_Tran._Voidloginemployeeunkid = -1
                objEmp_Qualification_Tran._Voidreason = xVoidReason
                objEmp_Qualification_Tran._Voiduserunkid = Me._Audituserunkid
                objEmp_Qualification_Tran._Isvoid = True
                objEmp_Qualification_Tran._Voiddatetime = Now

                'Gajanan [14-AUG-2019] -- Start      
                'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                If mblnIsweb Then
                objEmp_Qualification_Tran._WebClientIP = Me._Ip
                objEmp_Qualification_Tran._WebFormName = Me._Form_Name
                objEmp_Qualification_Tran._WebHostName = Me._Host
                End If
                objEmp_Qualification_Tran._Userunkid = Me._Audituserunkid
                objEmp_Qualification_Tran._Loginemployeeunkid = Me._LoginEmployeeUnkid
                'Gajanan [14-AUG-2019] -- End


                If objEmp_Qualification_Tran.Delete(intUnkid, objDataOperation) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@tranguid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intEmpId As Integer, _
                            ByVal intQualificationId As Integer, _
                            ByVal strRefNo As String, _
                            ByVal intProvider As Integer, _
                            ByVal xeOprType As clsEmployeeDataApproval.enOperationType, _
                            Optional ByVal strTranguid As String = "", _
                            Optional ByVal strOtherQualification As String = "", _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                            Optional ByRef intOperationTypeId As Integer = 0, _
                            Optional ByVal blnPreventNewEntry As Boolean = True) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()


        Try
            strQ = "SELECT " & _
              "  tranguid " & _
              ", transactiondate " & _
              ", mappingunkid " & _
              ", approvalremark " & _
              ", isfinal " & _
              ", statusunkid " & _
              ", qualificationtranunkid " & _
              ", employeeunkid " & _
              ", qualificationgroupunkid " & _
              ", qualificationunkid " & _
              ", reference_no " & _
              ", award_start_date " & _
              ", award_end_date " & _
              ", instituteunkid " & _
              ", remark " & _
              ", other_qualificationgrp " & _
              ", other_qualification " & _
              ", other_resultcode " & _
              ", other_institute " & _
              ", isvoid " & _
              ", auditdatetime " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", ip " & _
              ", host " & _
              ", form_name " & _
              ", isweb " & _
              ", isprocessed " & _
              ", operationtypeid " & _
             "FROM hremp_qualification_approval_tran " & _
             "WHERE employeeunkid = @EmpId " & _
             "AND instituteunkid = @InstId " & _
             "AND isvoid = 0 "

            If strTranguid.Trim.Length > 0 Then
                strQ &= " AND tranguid <> @tranguid"
            End If

            If mdtAward_Start_Date <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),award_start_date,112) = '" & eZeeDate.convertDate(mdtAward_Start_Date).ToString & "' "
            End If

            If mdtAward_End_Date <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),award_end_date,112) = '" & eZeeDate.convertDate(mdtAward_End_Date).ToString & "' "
            End If

            If strOtherQualification.Trim.Length > 0 Then
                strQ &= " AND other_qualification <> '' AND other_qualification = @other_qualification "
                'Else
                '    strQ &= " AND qualificationunkid = @QualifyId "
            End If


            'Gajanan [10-June-2019] -- Start      
            'If blnPreventNewEntry Then
            '    strQ &= " AND ISNULL(hremp_qualification_approval_tran.isprocessed,0)= 0 "
            'End If

                strQ &= " AND ISNULL(hremp_qualification_approval_tran.isprocessed,0)= 0 "
            'Gajanan [10-June-2019] -- End

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@QualifyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intQualificationId)
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strTranguid)
            objDataOperation.AddParameter("@RefId", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strRefNo)
            objDataOperation.AddParameter("@InstId", SqlDbType.Int, eZeeDataType.INT_SIZE, intProvider)
            objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strOtherQualification)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intOperationTypeId = CInt(dsList.Tables(0).Rows(0)("operationtypeid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Private Function UpdateRow(ByVal dr As DataRow, ByVal strColName As String, ByVal strValue As String) As Boolean
        Try
            If strColName.Trim.Length > 0 Then
                dr(strColName) = strValue
            End If
            dr("filesize") = dr("filesize_kb")
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateRow; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    Public Function Update(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal instituteunkid As Integer, ByVal mdtAward_Start_Date As DateTime, ByVal mdtAward_End_Date As DateTime, ByVal intQualification As Integer, ByVal strOtherQualification As String, ByVal intQualificationTranUnkid As Integer, ByVal strColName As String) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE hremp_qualification_approval_tran SET " & _
                   " " & strColName & " = '" & intQualificationTranUnkid & "' " & _
                   "WHERE isprocessed= 0 and instituteunkid = @instituteunkid AND employeeunkid = @employeeunkid "

            If mdtAward_Start_Date <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),award_start_date,112) = '" & eZeeDate.convertDate(mdtAward_Start_Date).ToString & "' "
            End If

            If mdtAward_End_Date <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),award_end_date,112) = '" & eZeeDate.convertDate(mdtAward_End_Date).ToString & "' "
            End If

            If strOtherQualification.Trim.Length > 0 Then
                strQ &= " AND other_qualification =@other_qualification "
            Else
                strQ &= " AND qualificationunkid = @qualificationunkid  "
            End If


            'Gajanan [31-May-2020] -- Start
            objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strOtherQualification)
            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intQualification)
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, instituteunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            'Gajanan [31-May-2020] -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function UpdateDeleteDocument(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal intlinkedUnkid As String, _
                                    ByVal scanattachrefid As Integer, ByVal dtTransectionDate As DateTime, ByVal intQualificationTranUnkid As Integer, ByVal strColName As String, ByVal blnisDelete As Boolean) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            If intlinkedUnkid.Length > 0 Then
                strQ = "UPDATE hrdocuments_tran SET " & _
                   " " & strColName & " = '" & intQualificationTranUnkid & "'"

            If blnisDelete Then
                    strQ &= ", isactive = 0 "
                Else
                    strQ &= ", isactive = 1 "
                End If
               'Gajanan [27-May-2019] -- Start              
               'strQ &= "WHERE scanattachrefid = " & scanattachrefid & "  AND employeeunkid = '" & intEmployeeId & "' AND scanattachtranunkid in (" & intlinkedUnkid & " )"
              strQ &= "WHERE employeeunkid = '" & intEmployeeId & "' AND scanattachtranunkid in (" & intlinkedUnkid & " )"
              'Gajanan [27-Ma y-2019] -- End

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'If dtTransectionDate <> Nothing Then
                '    strQ &= " AND CONVERT(CHAR(8),attached_date,112) = '" & eZeeDate.convertDate(dtTransectionDate).ToString & "' "
                'End If
                'Gajanan [22-Feb-2019] -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            End If
            'Gajanan [22-Feb-2019] -- End




            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If


            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
End Class
