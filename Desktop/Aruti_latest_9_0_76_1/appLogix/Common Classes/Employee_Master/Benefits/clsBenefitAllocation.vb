﻿'************************************************************************************************************************************
'Class Name : clsBenefitAllocation.vb
'Purpose    :
'Date       :18/08/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master
Imports System.Text

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsBenefitAllocation
    Private Shared ReadOnly mstrModuleName As String = "clsBenefitAllocation"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBenefitallocationunkid As Integer
    Private mintStationunkid As Integer
    Private mintDepartmentunkid As Integer
    Private mintSectionunkid As Integer
    Private mintUnitunkid As Integer
    Private mintJobunkid As Integer
    Private mintGradeunkid As Integer
    Private mintClassunkid As Integer
    Private mintCostcenterunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintBenefitgroupunkid As Integer


    'Pinkal (24-Feb-2017) -- Start
    'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
    Private mintDepartmentGroupunkid As Integer
    Private mintSectionGroupunkid As Integer
    Private mintUnitGroupunkid As Integer
    Private mintTeamunkid As Integer
    Private mintJobGroupunkid As Integer
    Private mintGradeGroupunkid As Integer
    Private mintGradeLevelunkid As Integer
    Private mintClassGroupunkid As Integer
    'Pinkal (24-Feb-2017) -- End
    'Sohail (17 Sep 2019) -- Start
    'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
    Private mintPeriodunkid As Integer
    Private mblnIsactive As Boolean = True
    'Sohail (17 Sep 2019) -- End


#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set benefitallocationunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Benefitallocationunkid() As Integer
        Get
            Return mintBenefitallocationunkid
        End Get
        Set(ByVal value As Integer)
            mintBenefitallocationunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stationunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Stationunkid() As Integer
        Get
            Return mintStationunkid
        End Get
        Set(ByVal value As Integer)
            mintStationunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set departmentunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Departmentunkid() As Integer
        Get
            Return mintDepartmentunkid
        End Get
        Set(ByVal value As Integer)
            mintDepartmentunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sectionunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Sectionunkid() As Integer
        Get
            Return mintSectionunkid
        End Get
        Set(ByVal value As Integer)
            mintSectionunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unitunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Unitunkid() As Integer
        Get
            Return mintUnitunkid
        End Get
        Set(ByVal value As Integer)
            mintUnitunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Gradeunkid() As Integer
        Get
            Return mintGradeunkid
        End Get
        Set(ByVal value As Integer)
            mintGradeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set classunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Classunkid() As Integer
        Get
            Return mintClassunkid
        End Get
        Set(ByVal value As Integer)
            mintClassunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set costcenterunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Costcenterunkid() As Integer
        Get
            Return mintCostcenterunkid
        End Get
        Set(ByVal value As Integer)
            mintCostcenterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set benefitgroupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Benefitgroupunkid() As Integer
        Get
            Return mintBenefitgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintBenefitgroupunkid = value
        End Set
    End Property


    'Pinkal (24-Feb-2017) -- Start
    'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

    ''' <summary>
    ''' Purpose: Get or Set deptgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DepartmentGroupunkid() As Integer
        Get
            Return mintDepartmentGroupunkid
        End Get
        Set(ByVal value As Integer)
            mintDepartmentGroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sectiongroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _SectionGroupunkid() As Integer
        Get
            Return mintSectionGroupunkid
        End Get
        Set(ByVal value As Integer)
            mintSectionGroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sectiongroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _UnitGroupunkid() As Integer
        Get
            Return mintUnitGroupunkid
        End Get
        Set(ByVal value As Integer)
            mintUnitGroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set teamunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Teamunkid() As Integer
        Get
            Return mintTeamunkid
        End Get
        Set(ByVal value As Integer)
            mintTeamunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set JobGroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _JobGroupunkid() As Integer
        Get
            Return mintJobGroupunkid
        End Get
        Set(ByVal value As Integer)
            mintJobGroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set GradeGroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _GradeGroupunkid() As Integer
        Get
            Return mintGradeGroupunkid
        End Get
        Set(ByVal value As Integer)
            mintGradeGroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Gradelevelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Gradelevelunkid() As Integer
        Get
            Return mintGradeLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintGradeLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Classgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClassGroupunkid() As Integer
        Get
            Return mintClassGroupunkid
        End Get
        Set(ByVal value As Integer)
            mintClassGroupunkid = value
        End Set
    End Property
    'Pinkal (24-Feb-2017) -- End

    'Sohail (17 Sep 2019) -- Start
    'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    Public Property _IsActive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property
    'Sohail (17 Sep 2019) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

            'strQ = "SELECT " & _
            '   "  benefitallocationunkid " & _
            '   ", stationunkid " & _
            '   ", departmentunkid " & _
            '   ", sectionunkid " & _
            '   ", unitunkid " & _
            '   ", jobunkid " & _
            '   ", gradeunkid " & _
            '   ", classunkid " & _
            '   ", costcenterunkid " & _
            '   ", userunkid " & _
            '   ", isvoid " & _
            '   ", voiduserunkid " & _
            '   ", voiddatetime " & _
            '   ", voidreason " & _
            '   ", benefitgroupunkid " & _
            '  "FROM hrbenefit_allocation_master " & _
            '  "WHERE benefitallocationunkid = @benefitallocationunkid "


            strQ = "SELECT " & _
              "  benefitallocationunkid " & _
              ", stationunkid " & _
              ", departmentunkid " & _
              ", sectionunkid " & _
              ", unitunkid " & _
              ", jobunkid " & _
              ", gradeunkid " & _
              ", classunkid " & _
              ", costcenterunkid " & _
                        ", deptgroupunkid " & _
                        ", sectiongroupunkid " & _
                        ", unitgroupunkid " & _
                        ", teamunkid " & _
                        ", jobgroupunkid " & _
                        ", gradegroupunkid " & _
                        ", gradelevelunkid " & _
                        ", classgroupunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", benefitgroupunkid " & _
              ", ISNULL(hrbenefit_allocation_master.periodunkid, 1) AS periodunkid " & _
              ", ISNULL(hrbenefit_allocation_master.isactive, 1) AS isactive " & _
             "FROM hrbenefit_allocation_master " & _
             "WHERE benefitallocationunkid = @benefitallocationunkid "
            'Sohail (17 Sep 2019) - [periodunkid, isactive]
            'Pinkal (24-Feb-2017) -- End

            objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitallocationunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintBenefitallocationunkid = CInt(dtRow.Item("benefitallocationunkid"))
                mintStationunkid = CInt(dtRow.Item("stationunkid"))
                mintDepartmentunkid = CInt(dtRow.Item("departmentunkid"))
                mintSectionunkid = CInt(dtRow.Item("sectionunkid"))
                mintUnitunkid = CInt(dtRow.Item("unitunkid"))
                mintJobunkid = CInt(dtRow.Item("jobunkid"))
                mintGradeunkid = CInt(dtRow.Item("gradeunkid"))
                mintClassunkid = CInt(dtRow.Item("classunkid"))
                mintCostcenterunkid = CInt(dtRow.Item("costcenterunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintBenefitgroupunkid = CInt(dtRow.Item("benefitgroupunkid"))



                'Pinkal (24-Feb-2017) -- Start
                'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
                mintDepartmentGroupunkid = CInt(dtRow.Item("deptgroupunkid"))
                mintSectionGroupunkid = CInt(dtRow.Item("sectiongroupunkid"))
                mintUnitGroupunkid = CInt(dtRow.Item("unitgroupunkid"))
                mintTeamunkid = CInt(dtRow.Item("teamunkid"))
                mintJobGroupunkid = CInt(dtRow.Item("jobgroupunkid"))
                mintGradeGroupunkid = CInt(dtRow.Item("gradegroupunkid"))
                mintGradeLevelunkid = CInt(dtRow.Item("gradelevelunkid"))
                mintClassGroupunkid = CInt(dtRow.Item("classgroupunkid"))
                'Pinkal (24-Feb-2017) -- End
                'Sohail (17 Sep 2019) -- Start
                'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                'Sohail (17 Sep 2019) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal dtAsOnDate As Date, Optional ByVal intActiveInActive As Integer = 0, Optional ByVal intUnkId As Integer = 0) As DataSet
        'Sohail (17 Sep 2019) - [dtAsOnDate, intActiveInActive, intUnkId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

            'strQ = "SELECT " & _
            '            " hrbenefit_allocation_master.benefitallocationunkid " & _
            '            ",hrbenefit_allocation_master.stationunkid " & _
            '            ",hrbenefit_allocation_master.departmentunkid " & _
            '            ",hrbenefit_allocation_master.sectionunkid " & _
            '            ",hrbenefit_allocation_master.unitunkid " & _
            '            ",hrbenefit_allocation_master.jobunkid " & _
            '            ",hrbenefit_allocation_master.gradeunkid " & _
            '            ",hrbenefit_allocation_master.classunkid " & _
            '            ",hrbenefit_allocation_master.costcenterunkid " & _
            '            ",hrbenefit_allocation_master.userunkid " & _
            '            ",hrbenefit_allocation_master.isvoid " & _
            '            ",hrbenefit_allocation_master.voiduserunkid " & _
            '            ",hrbenefit_allocation_master.voiddatetime " & _
            '            ",hrbenefit_allocation_master.voidreason " & _
            '            ",ISNULL(prcostcenter_master.costcentername,'') AS CostCenter " & _
            '            ",ISNULL(hrclasses_master.name,'') AS ClassName " & _
            '            ",ISNULL(hrgrade_master.name,'') AS Grade " & _
            '            ",ISNULL(hrunit_master.name,'') AS Unit " & _
            '            ",ISNULL(hrsection_master.name,'') AS Section " & _
            '            ",ISNULL(hrdepartment_master.name,'') AS Department " & _
            '            ",ISNULL(hrstation_master.name,'') AS Station " & _
            '            ",ISNULL(cfcommon_master.name,'') AS BenefitGrp " & _
            '            ",ISNULL(hrjob_master.job_name,'') AS Job " & _
            '        "FROM hrbenefit_allocation_master " & _
            '            "LEFT JOIN hrjob_master ON hrbenefit_allocation_master.jobunkid = hrjob_master.jobunkid " & _
            '            "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid=hrbenefit_allocation_master.benefitgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.BENEFIT_GROUP & _
            '            "LEFT JOIN prcostcenter_master ON hrbenefit_allocation_master.costcenterunkid = prcostcenter_master.costcenterunkid " & _
            '            "LEFT JOIN hrclasses_master ON hrbenefit_allocation_master.classunkid = hrclasses_master.classesunkid " & _
            '            "LEFT JOIN hrgrade_master ON hrbenefit_allocation_master.gradeunkid = hrgrade_master.gradeunkid " & _
            '            "LEFT JOIN hrunit_master ON hrbenefit_allocation_master.unitunkid = hrunit_master.unitunkid " & _
            '            "LEFT JOIN hrsection_master ON hrbenefit_allocation_master.sectionunkid = hrsection_master.sectionunkid " & _
            '            "LEFT JOIN hrdepartment_master ON hrbenefit_allocation_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '            "LEFT JOIN hrstation_master ON hrbenefit_allocation_master.stationunkid = hrstation_master.stationunkid " & _
            '        "WHERE ISNULL(hrbenefit_allocation_master.isvoid,0)=0  " & _
            '        "ORDER BY hrbenefit_allocation_master.benefitallocationunkid "

            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            'strQ = "SELECT " & _
            '            " hrbenefit_allocation_master.benefitallocationunkid " & _
            '            ",hrbenefit_allocation_master.stationunkid " & _
            '           ",hrbenefit_allocation_master.deptgroupunkid " & _
            '            ",hrbenefit_allocation_master.departmentunkid " & _
            '           ",hrbenefit_allocation_master.sectiongroupunkid " & _
            '            ",hrbenefit_allocation_master.sectionunkid " & _
            '           ",hrbenefit_allocation_master.unitgroupunkid " & _
            '            ",hrbenefit_allocation_master.unitunkid " & _
            '           ",hrbenefit_allocation_master.teamunkid " & _
            '           ",hrbenefit_allocation_master.gradegroupunkid " & _
            '           ",hrbenefit_allocation_master.gradeunkid " & _
            '           ",hrbenefit_allocation_master.gradelevelunkid " & _
            '           ",hrbenefit_allocation_master.jobgroupunkid " & _
            '            ",hrbenefit_allocation_master.jobunkid " & _
            '           ",hrbenefit_allocation_master.classgroupunkid " & _
            '            ",hrbenefit_allocation_master.classunkid " & _
            '            ",hrbenefit_allocation_master.costcenterunkid " & _
            '            ",hrbenefit_allocation_master.userunkid " & _
            '            ",hrbenefit_allocation_master.isvoid " & _
            '            ",hrbenefit_allocation_master.voiduserunkid " & _
            '            ",hrbenefit_allocation_master.voiddatetime " & _
            '            ",hrbenefit_allocation_master.voidreason " & _
            '            ",ISNULL(prcostcenter_master.costcentername,'') AS CostCenter " & _
            '            ",ISNULL(hrclasses_master.name,'') AS ClassName " & _
            '            ",ISNULL(hrgrade_master.name,'') AS Grade " & _
            '            ",ISNULL(hrunit_master.name,'') AS Unit " & _
            '            ",ISNULL(hrsection_master.name,'') AS Section " & _
            '            ",ISNULL(hrdepartment_master.name,'') AS Department " & _
            '            ",ISNULL(hrstation_master.name,'') AS Station " & _
            '            ",ISNULL(cfcommon_master.name,'') AS BenefitGrp " & _
            '            ",ISNULL(hrjob_master.job_name,'') AS Job " & _
            '           ",ISNULL(hrdepartment_group_master.name,'') AS DeptGrp " & _
            '           ",ISNULL(hrsectiongroup_master.name,'') AS SectionGrp " & _
            '           ",ISNULL(hrunitgroup_master.name,'') AS UnitGrp " & _
            '           ",ISNULL(hrteam_master.name,'') AS Team " & _
            '           ",ISNULL(hrjobgroup_master.name,'') AS JobGrp " & _
            '           ",ISNULL(hrgradegroup_master.name,'') AS GradeGrp " & _
            '           ",ISNULL(hrgradelevel_master.name,'') AS GradeLevel " & _
            '           ",ISNULL(hrclassgroup_master.name,'') AS ClassGrp " & _
            '       " FROM hrbenefit_allocation_master " & _
            '           " LEFT JOIN hrjob_master ON hrbenefit_allocation_master.jobunkid = hrjob_master.jobunkid " & _
            '           " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid=hrbenefit_allocation_master.benefitgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.BENEFIT_GROUP & _
            '           " LEFT JOIN prcostcenter_master ON hrbenefit_allocation_master.costcenterunkid = prcostcenter_master.costcenterunkid " & _
            '           " LEFT JOIN hrclasses_master ON hrbenefit_allocation_master.classunkid = hrclasses_master.classesunkid " & _
            '           " LEFT JOIN hrgrade_master ON hrbenefit_allocation_master.gradeunkid = hrgrade_master.gradeunkid " & _
            '           " LEFT JOIN hrunit_master ON hrbenefit_allocation_master.unitunkid = hrunit_master.unitunkid " & _
            '           " LEFT JOIN hrsection_master ON hrbenefit_allocation_master.sectionunkid = hrsection_master.sectionunkid " & _
            '           " LEFT JOIN hrdepartment_master ON hrbenefit_allocation_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '           " LEFT JOIN hrstation_master ON hrbenefit_allocation_master.stationunkid = hrstation_master.stationunkid " & _
            '           " LEFT JOIN hrdepartment_group_master ON hrbenefit_allocation_master.deptgroupunkid = hrdepartment_group_master.deptgroupunkid " & _
            '           " LEFT JOIN hrsectiongroup_master ON hrbenefit_allocation_master.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
            '           " LEFT JOIN hrunitgroup_master ON hrbenefit_allocation_master.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
            '           " LEFT JOIN hrteam_master ON hrbenefit_allocation_master.teamunkid = hrteam_master.teamunkid " & _
            '           " LEFT JOIN hrjobgroup_master ON hrbenefit_allocation_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
            '           " LEFT JOIN hrgradegroup_master ON hrbenefit_allocation_master.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
            '           " LEFT JOIN hrgradelevel_master ON hrbenefit_allocation_master.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
            '           " LEFT JOIN hrclassgroup_master ON hrbenefit_allocation_master.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
            '       " WHERE ISNULL(hrbenefit_allocation_master.isvoid,0)=0  " & _
            '       " ORDER BY hrbenefit_allocation_master.benefitallocationunkid "
            strQ = "SELECT  A.benefitallocationunkid " & _
                        ", A.stationunkid " & _
                        ", A.deptgroupunkid " & _
                        ", A.departmentunkid " & _
                        ", A.sectiongroupunkid " & _
                        ", A.sectionunkid " & _
                        ", A.unitgroupunkid " & _
                        ", A.unitunkid " & _
                        ", A.teamunkid " & _
                        ", A.gradegroupunkid " & _
                        ", A.gradeunkid " & _
                        ", A.gradelevelunkid " & _
                        ", A.jobgroupunkid " & _
                        ", A.jobunkid " & _
                        ", A.classgroupunkid " & _
                        ", A.classunkid " & _
                        ", A.costcenterunkid " & _
                        ", A.userunkid " & _
                        ", A.isvoid " & _
                        ", A.voiduserunkid " & _
                        ", A.voiddatetime " & _
                        ", A.voidreason " & _
                        ", A.CostCenter " & _
                        ", A.ClassName " & _
                        ", A.Grade " & _
                        ", A.Unit " & _
                        ", A.Section " & _
                        ", A.Department " & _
                        ", A.Station " & _
                        ", A.BenefitGrp " & _
                        ", A.Job " & _
                        ", A.DeptGrp " & _
                        ", A.SectionGrp " & _
                        ", A.UnitGrp " & _
                        ", A.Team " & _
                        ", A.JobGrp " & _
                        ", A.GradeGrp " & _
                        ", A.GradeLevel " & _
                        ", A.ClassGrp " & _
                        ", A.periodunkid " & _
                        ", A.period_name " & _
                        ", A.start_date " & _
                        ", A.end_date " & _
                        ", A.isactive " & _
                    "FROM ( " & _
                            "SELECT hrbenefit_allocation_master.benefitallocationunkid " & _
                        ",hrbenefit_allocation_master.stationunkid " & _
                       ",hrbenefit_allocation_master.deptgroupunkid " & _
                        ",hrbenefit_allocation_master.departmentunkid " & _
                       ",hrbenefit_allocation_master.sectiongroupunkid " & _
                        ",hrbenefit_allocation_master.sectionunkid " & _
                       ",hrbenefit_allocation_master.unitgroupunkid " & _
                        ",hrbenefit_allocation_master.unitunkid " & _
                       ",hrbenefit_allocation_master.teamunkid " & _
                       ",hrbenefit_allocation_master.gradegroupunkid " & _
                       ",hrbenefit_allocation_master.gradeunkid " & _
                       ",hrbenefit_allocation_master.gradelevelunkid " & _
                       ",hrbenefit_allocation_master.jobgroupunkid " & _
                        ",hrbenefit_allocation_master.jobunkid " & _
                       ",hrbenefit_allocation_master.classgroupunkid " & _
                        ",hrbenefit_allocation_master.classunkid " & _
                        ",hrbenefit_allocation_master.costcenterunkid " & _
                        ",hrbenefit_allocation_master.userunkid " & _
                        ",hrbenefit_allocation_master.isvoid " & _
                        ",hrbenefit_allocation_master.voiduserunkid " & _
                        ",hrbenefit_allocation_master.voiddatetime " & _
                        ",hrbenefit_allocation_master.voidreason " & _
                        ",ISNULL(prcostcenter_master.costcentername,'') AS CostCenter " & _
                        ",ISNULL(hrclasses_master.name,'') AS ClassName " & _
                        ",ISNULL(hrgrade_master.name,'') AS Grade " & _
                        ",ISNULL(hrunit_master.name,'') AS Unit " & _
                        ",ISNULL(hrsection_master.name,'') AS Section " & _
                        ",ISNULL(hrdepartment_master.name,'') AS Department " & _
                        ",ISNULL(hrstation_master.name,'') AS Station " & _
                        ",ISNULL(cfcommon_master.name,'') AS BenefitGrp " & _
                        ",ISNULL(hrjob_master.job_name,'') AS Job " & _
                       ",ISNULL(hrdepartment_group_master.name,'') AS DeptGrp " & _
                       ",ISNULL(hrsectiongroup_master.name,'') AS SectionGrp " & _
                       ",ISNULL(hrunitgroup_master.name,'') AS UnitGrp " & _
                       ",ISNULL(hrteam_master.name,'') AS Team " & _
                       ",ISNULL(hrjobgroup_master.name,'') AS JobGrp " & _
                       ",ISNULL(hrgradegroup_master.name,'') AS GradeGrp " & _
                       ",ISNULL(hrgradelevel_master.name,'') AS GradeLevel " & _
                       ",ISNULL(hrclassgroup_master.name,'') AS ClassGrp " & _
                                    ", ISNULL(hrbenefit_allocation_master.periodunkid, 1) AS periodunkid " & _
                                    ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
                                    ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112), '19000101') AS start_date " & _
                                    ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112), '19000101') AS end_date " & _
                                    ", ISNULL(hrbenefit_allocation_master.isactive, 1) AS isactive " & _
                                    ", DENSE_RANK() OVER (PARTITION BY hrbenefit_allocation_master.benefitgroupunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                   " FROM hrbenefit_allocation_master " & _
                               "LEFT JOIN cfcommon_period_tran ON hrbenefit_allocation_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                       " LEFT JOIN hrjob_master ON hrbenefit_allocation_master.jobunkid = hrjob_master.jobunkid " & _
                       " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid=hrbenefit_allocation_master.benefitgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.BENEFIT_GROUP & _
                       " LEFT JOIN prcostcenter_master ON hrbenefit_allocation_master.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                       " LEFT JOIN hrclasses_master ON hrbenefit_allocation_master.classunkid = hrclasses_master.classesunkid " & _
                       " LEFT JOIN hrgrade_master ON hrbenefit_allocation_master.gradeunkid = hrgrade_master.gradeunkid " & _
                       " LEFT JOIN hrunit_master ON hrbenefit_allocation_master.unitunkid = hrunit_master.unitunkid " & _
                       " LEFT JOIN hrsection_master ON hrbenefit_allocation_master.sectionunkid = hrsection_master.sectionunkid " & _
                       " LEFT JOIN hrdepartment_master ON hrbenefit_allocation_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                       " LEFT JOIN hrstation_master ON hrbenefit_allocation_master.stationunkid = hrstation_master.stationunkid " & _
                       " LEFT JOIN hrdepartment_group_master ON hrbenefit_allocation_master.deptgroupunkid = hrdepartment_group_master.deptgroupunkid " & _
                       " LEFT JOIN hrsectiongroup_master ON hrbenefit_allocation_master.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
                       " LEFT JOIN hrunitgroup_master ON hrbenefit_allocation_master.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
                       " LEFT JOIN hrteam_master ON hrbenefit_allocation_master.teamunkid = hrteam_master.teamunkid " & _
                       " LEFT JOIN hrjobgroup_master ON hrbenefit_allocation_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
                       " LEFT JOIN hrgradegroup_master ON hrbenefit_allocation_master.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                       " LEFT JOIN hrgradelevel_master ON hrbenefit_allocation_master.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
                       " LEFT JOIN hrclassgroup_master ON hrbenefit_allocation_master.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
                           " WHERE hrbenefit_allocation_master.isvoid = 0  "

            If intUnkId > 0 Then
                strQ &= " AND hrbenefit_allocation_master.benefitallocationunkid = @benefitallocationunkid "
                objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkId)
            End If

            If dtAsOnDate <> Nothing Then
                strQ &= "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
                objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            End If

            strQ &= " ) AS A "

            strQ &= "WHERE   1 = 1 "

            If dtAsOnDate <> Nothing Then
                strQ &= " AND   A.ROWNO = 1 "
            End If

            If intActiveInActive = enTranHeadActiveInActive.ACTIVE Then
                strQ &= " AND A.isactive = 1 "
            ElseIf intActiveInActive = enTranHeadActiveInActive.INACTIVE Then
                strQ &= " AND A.isactive = 0 "
            End If

            strQ &= " ORDER BY A.period_name "
            'Sohail (17 Sep 2019) -- End
            'Pinkal (24-Feb-2017) -- End


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrbenefit_allocation_master) </purpose>
    Public Function Insert(ByVal dtAllocaionTran As DataTable) As Boolean

        If isExist(mintPeriodunkid, mintStationunkid, mintDepartmentunkid, mintSectionunkid, mintUnitunkid, mintJobunkid, mintGradeunkid, mintClassunkid, _
                            mintCostcenterunkid, mintBenefitgroupunkid, mintBenefitallocationunkid) Then 'Sohail (23 Jan 2012)
            'Sohail (17 Sep 2019) - [mintPeriodunkid]
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Benefit Group is already assigned for the selected information. Please assign new Benefit Group.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()




        Try
            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)

            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentGroupunkid.ToString)
            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionGroupunkid.ToString)
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitGroupunkid.ToString)
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobGroupunkid.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeGroupunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeLevelunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassGroupunkid.ToString)

            'Pinkal (24-Feb-2017) -- End

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitgroupunkid.ToString)
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive)
            'Sohail (17 Sep 2019) -- End

            strQ = "INSERT INTO hrbenefit_allocation_master ( " & _
              "  stationunkid " & _
              ", departmentunkid " & _
              ", sectionunkid " & _
              ", unitunkid " & _
              ", jobunkid " & _
              ", gradeunkid " & _
              ", classunkid " & _
              ", costcenterunkid " & _
                      ", deptgroupunkid " & _
                      ", sectiongroupunkid " & _
                      ", unitgroupunkid " & _
                      ", teamunkid " & _
                      ", jobgroupunkid " & _
                      ", gradegroupunkid " & _
                      ", gradelevelunkid " & _
                      ", classgroupunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", benefitgroupunkid" & _
              ", periodunkid" & _
              ", isactive" & _
            ") VALUES (" & _
              "  @stationunkid " & _
              ", @departmentunkid " & _
              ", @sectionunkid " & _
              ", @unitunkid " & _
              ", @jobunkid " & _
              ", @gradeunkid " & _
              ", @classunkid " & _
              ", @costcenterunkid " & _
                      ", @deptgroupunkid " & _
                      ", @sectiongroupunkid " & _
                      ", @unitgroupunkid " & _
                      ", @teamunkid " & _
                      ", @jobgroupunkid " & _
                      ", @gradegroupunkid " & _
                      ", @gradelevelunkid " & _
                      ", @classgroupunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @benefitgroupunkid" & _
              ", @periodunkid" & _
              ", @isactive" & _
            "); SELECT @@identity"
            'Sohail (17 Sep 2019) - [periodunkid, isactive]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBenefitallocationunkid = dsList.Tables(0).Rows(0).Item(0)

            If dtAllocaionTran IsNot Nothing AndAlso dtAllocaionTran.Rows.Count > 0 Then
                Dim objAllocTran As New clsBenefit_allocation_tran
                objAllocTran._Benefitallocationunkid = mintBenefitallocationunkid
                objAllocTran._Datasource = dtAllocaionTran
                If objAllocTran.InsertByDataTable() = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrbenefit_allocation_master) </purpose>
    Public Function Update() As Boolean

        If isExist(mintPeriodunkid, mintStationunkid, mintDepartmentunkid, mintSectionunkid, mintUnitunkid, mintJobunkid, mintGradeunkid, mintClassunkid, _
                            mintCostcenterunkid, mintBenefitgroupunkid, mintBenefitallocationunkid) Then
            'Sohail (17 Sep 2019) - [mintPeriodunkid]
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Benefit Group is already assigned for the selected information. Please assign new Benefit Group.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()


        Try
            objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitallocationunkid.ToString)
            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)

            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentGroupunkid.ToString)
            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionGroupunkid.ToString)
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitGroupunkid.ToString)
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobGroupunkid.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeGroupunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeLevelunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassGroupunkid.ToString)

            'Pinkal (24-Feb-2017) -- End

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitgroupunkid.ToString)
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive)
            'Sohail (17 Sep 2019) -- End

            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

            'strQ = "UPDATE hrbenefit_allocation_master SET " & _
            '           "  stationunkid = @stationunkid" & _
            '           ", departmentunkid = @departmentunkid" & _
            '           ", sectionunkid = @sectionunkid" & _
            '           ", unitunkid = @unitunkid" & _
            '           ", jobunkid = @jobunkid" & _
            '           ", gradeunkid = @gradeunkid" & _
            '           ", classunkid = @classunkid" & _
            '           ", costcenterunkid = @costcenterunkid" & _
            '           ", userunkid = @userunkid" & _
            '           ", isvoid = @isvoid" & _
            '           ", voiduserunkid = @voiduserunkid" & _
            '           ", voiddatetime = @voiddatetime" & _
            '           ", voidreason = @voidreason" & _
            '           ", benefitgroupunkid = @benefitgroupunkid " & _
            '         "WHERE benefitallocationunkid = @benefitallocationunkid "


            strQ = "UPDATE hrbenefit_allocation_master SET " & _
              "  stationunkid = @stationunkid" & _
              ", departmentunkid = @departmentunkid" & _
              ", sectionunkid = @sectionunkid" & _
              ", unitunkid = @unitunkid" & _
              ", jobunkid = @jobunkid" & _
              ", gradeunkid = @gradeunkid" & _
              ", classunkid = @classunkid" & _
              ", costcenterunkid = @costcenterunkid" & _
                     ", deptgroupunkid = @deptgroupunkid " & _
                     ", sectiongroupunkid = @sectiongroupunkid" & _
                     ", unitgroupunkid = @unitgroupunkid " & _
                     ", teamunkid = @teamunkid " & _
                     ", jobgroupunkid = @jobgroupunkid " & _
                     ", gradegroupunkid = @gradegroupunkid " & _
                     ", gradelevelunkid = @gradelevelunkid " & _
                     ", classgroupunkid = @classgroupunkid " & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason" & _
              ", benefitgroupunkid = @benefitgroupunkid " & _
              ", periodunkid = @periodunkid " & _
              ", isactive = @isactive " & _
            "WHERE benefitallocationunkid = @benefitallocationunkid "
            'Sohail (17 Sep 2019) - [periodunkid, isactive]
            'Pinkal (24-Feb-2017) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrbenefit_allocation_master", mintBenefitallocationunkid, "benefitallocationunkid", 2) Then

                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrbenefit_allocation_master", "benefitallocationunkid", mintBenefitallocationunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrbenefit_allocation_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal dtAllocaionTran As DataTable = Nothing) As Boolean 'Sohail (23 Jan 2012) - [dtAllocaionTran]) As Boolean
        Dim objDataOperation As clsDataOperation
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()


        Try
            strQ = "UPDATE hrbenefit_allocation_master " & _
                        " SET isvoid = 1 " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                    "WHERE benefitallocationunkid=@benefitallocationunkid  "

            objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objAllocTran As New clsBenefit_allocation_tran
            If objAllocTran.VoidAll(intUnkid, mintVoiduserunkid, mdtVoiddatetime, mstrVoidreason) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If dtAllocaionTran IsNot Nothing AndAlso dtAllocaionTran.Rows.Count > 0 Then
                If Insert(dtAllocaionTran) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "TABLE_NAME AS TableName " & _
                    "FROM INFORMATION_SCHEMA.COLUMNS " & _
                    "WHERE COLUMN_NAME = 'benefitallocationunkid' "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("TableName") = "hrbenefit_allocation_master" OrElse dtRow.Item("TableName") = "hrbenefit_allocation_tran" Then Continue For 'Sohail (23 Jan 2012) - [hrbenefit_allocation_tran]
                strQ = "SELECT benefitallocationunkid FROM " & dtRow.Item("TableName").ToString & " WHERE benefitallocationunkid = @benefitallocationunkid "
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intPeriodUnkId As Integer, _
                            Optional ByVal intStationId As Integer = -1, _
                            Optional ByVal intDeptId As Integer = -1, _
                            Optional ByVal intSectionId As Integer = -1, _
                            Optional ByVal intUnitId As Integer = -1, _
                            Optional ByVal intJobId As Integer = -1, _
                            Optional ByVal intGradeId As Integer = -1, _
                            Optional ByVal intClassId As Integer = -1, _
                            Optional ByVal intCostCenterId As Integer = -1, _
                            Optional ByVal intBenefitGroupId As Integer = -1, _
                            Optional ByVal intUnkid As Integer = -1 _
                            ) As String
        'Sohail (17 Sep 2019) - [periodunkid, isactive]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.ClearParameters()


            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

            'strQ = "SELECT " & _
            '           "  hrbenefit_allocation_master.benefitallocationunkid " & _
            '           ", hrbenefit_allocation_master.stationunkid " & _
            '           ", hrbenefit_allocation_master.departmentunkid " & _
            '           ", hrbenefit_allocation_master.sectionunkid " & _
            '           ", hrbenefit_allocation_master.unitunkid " & _
            '           ", hrbenefit_allocation_master.jobunkid " & _
            '           ", hrbenefit_allocation_master.gradeunkid " & _
            '           ", hrbenefit_allocation_master.classunkid " & _
            '           ", hrbenefit_allocation_master.costcenterunkid " & _
            '           ", hrbenefit_allocation_master.userunkid " & _
            '           ", hrbenefit_allocation_master.isvoid " & _
            '           ", hrbenefit_allocation_master.voiduserunkid " & _
            '           ", hrbenefit_allocation_master.voiddatetime " & _
            '           ", hrbenefit_allocation_master.voidreason " & _
            '          "FROM hrbenefit_allocation_master " & _
            '          "WHERE ISNULL(hrbenefit_allocation_master.isvoid,0) = 0 " & _
            '          "AND hrbenefit_allocation_master.benefitgroupunkid = @benefitgroupunkid "

            strQ = "SELECT " & _
                      "  hrbenefit_allocation_master.benefitallocationunkid " & _
                      ", hrbenefit_allocation_master.stationunkid " & _
                      ", hrbenefit_allocation_master.departmentunkid " & _
                      ", hrbenefit_allocation_master.sectionunkid " & _
                      ", hrbenefit_allocation_master.unitunkid " & _
                      ", hrbenefit_allocation_master.jobunkid " & _
                      ", hrbenefit_allocation_master.gradeunkid " & _
                      ", hrbenefit_allocation_master.classunkid " & _
                      ", hrbenefit_allocation_master.costcenterunkid " & _
                      ", hrbenefit_allocation_master.deptgroupunkid " & _
                      ", hrbenefit_allocation_master.sectiongroupunkid " & _
                      ", hrbenefit_allocation_master.unitgroupunkid " & _
                      ", hrbenefit_allocation_master.teamunkid " & _
                      ", hrbenefit_allocation_master.jobgroupunkid " & _
                      ", hrbenefit_allocation_master.gradegroupunkid " & _
                      ", hrbenefit_allocation_master.gradelevelunkid " & _
                      ", hrbenefit_allocation_master.classgroupunkid " & _
                      ", hrbenefit_allocation_master.userunkid " & _
                      ", hrbenefit_allocation_master.isvoid " & _
                      ", hrbenefit_allocation_master.voiduserunkid " & _
                      ", hrbenefit_allocation_master.voiddatetime " & _
                      ", hrbenefit_allocation_master.voidreason " & _
                      " FROM hrbenefit_allocation_master " & _
                      " WHERE ISNULL(hrbenefit_allocation_master.isvoid,0) = 0 " & _
                      " AND hrbenefit_allocation_master.benefitgroupunkid = @benefitgroupunkid " & _
                      " AND hrbenefit_allocation_master.periodunkid = @periodunkid "
            'Sohail (17 Sep 2019) - [periodunkid]
            'Pinkal (24-Feb-2017) -- End

            objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBenefitGroupId)
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            'Sohail (17 Sep 2019) -- End


            'If intStationId > 0 Then'Sohail (23 Jan 2012)
                strQ &= "AND hrbenefit_allocation_master.stationunkid = @stationunkid "
                objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStationId)
            'End If

            'If intDeptId > 0 Then 'Sohail (23 Jan 2012)
                strQ &= "AND hrbenefit_allocation_master.departmentunkid = @departmentunkid "
                objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDeptId)
            'End If

            'If intSectionId > 0 Then 'Sohail (23 Jan 2012)
                strQ &= "AND hrbenefit_allocation_master.sectionunkid = @sectionunkid "
                objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSectionId)
            'End If

            'If intUnitId > 0 Then 'Sohail (23 Jan 2012)
                strQ &= "AND hrbenefit_allocation_master.unitunkid = @unitunkid "
                objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnitId)
            'End If

            'If intJobId > 0 Then 'Sohail (23 Jan 2012)
                strQ &= "AND hrbenefit_allocation_master.jobunkid = @jobunkid "
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobId)
            'End If

            'If intGradeId > 0 Then 'Sohail (23 Jan 2012)
                strQ &= "AND hrbenefit_allocation_master.gradeunkid = @gradeunkid "
                objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeId)
            'End If

            'If intClassId > 0 Then 'Sohail (23 Jan 2012)
                strQ &= "AND hrbenefit_allocation_master.classunkid = @classunkid "
                objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intClassId)
            'End If

            'If intCostCenterId > 0 Then 'Sohail (23 Jan 2012)
                strQ &= "AND hrbenefit_allocation_master.costcenterunkid = @costcenterunkid "
                objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCostCenterId)
            'End If

            If intUnkid > 0 Then
                strQ &= " AND hrbenefit_allocation_master.benefitallocationunkid <> @benefitallocationunkid"
                objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetBenefitGroup(ByVal strList As String, _
                                    ByVal dtAsOnDate As Date, _
                                    Optional ByVal blnAddSelect As Boolean = False, _
                                    Optional ByVal intStationId As Integer = 0, _
                                    Optional ByVal intDepartmentId As Integer = 0, _
                                    Optional ByVal intSectionId As Integer = 0, _
                                    Optional ByVal intUnitId As Integer = 0, _
                                    Optional ByVal intJobId As Integer = 0, _
                                    Optional ByVal intGradeId As Integer = 0, _
                                    Optional ByVal intClassId As Integer = 0, _
                                    Optional ByVal intCostCenterId As Integer = 0, _
                                    Optional ByVal intDeptGrpId As Integer = 0, _
                                    Optional ByVal intSectionGrpId As Integer = 0, _
                                    Optional ByVal intUnitGrpId As Integer = 0, _
                                    Optional ByVal intTeamId As Integer = 0, _
                                    Optional ByVal intJobGrpId As Integer = 0, _
                                    Optional ByVal intGradeGrpId As Integer = 0, _
                                    Optional ByVal intGradeLevelId As Integer = 0, _
                                    Optional ByVal intClassGrpId As Integer = 0) As DataSet


        'Pinkal (24-Feb-2017) -- Start
        'Enhancement - Working on Employee Group Benefit Enhancement For TRA.
        'Optional ByVal intDeptGrpId As Integer = 0, _
        'Optional ByVal intSectionGrpId As Integer = 0, _
        'Optional ByVal intUnitGrpId As Integer = 0, _
        'Optional ByVal intTeamId As Integer = 0, _
        'Optional ByVal intJobGrpId As Integer = 0, _
        'Optional ByVal intGradeGrpId As Integer = 0, _
        'Optional ByVal intGradeLevelId As Integer = 0, _
        'Optional ByVal intClassGrpId As Integer = 0
        'Pinkal (24-Feb-2017) -- End


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            If blnAddSelect = True Then
                strQ = "SELECT @Select As BGroup,0 As Id " & _
                       ", 0 AS benefitallocationunkid " & _
                       "UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
            End If

            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            strQ &= "SELECT " & _
                       "  A.BGroup " & _
                       ", A.Id " & _
                       ", A.benefitallocationunkid " & _
                   "FROM ( "
            'Sohail (17 Sep 2019) -- End

            strQ &= "SELECT " & _
                    "	 ISNULL(cfcommon_master.name,'') AS BGroup " & _
                    "	,hrbenefit_allocation_master.benefitgroupunkid As Id " & _
                    "   ,hrbenefit_allocation_master.benefitallocationunkid " & _
                    ", DENSE_RANK() OVER (PARTITION BY hrbenefit_allocation_master.benefitgroupunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                    "FROM hrbenefit_allocation_master " & _
                    "	JOIN cfcommon_master ON cfcommon_master.masterunkid = hrbenefit_allocation_master.benefitgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.BENEFIT_GROUP & _
                    "LEFT JOIN cfcommon_period_tran ON hrbenefit_allocation_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                    "WHERE ISNULL(hrbenefit_allocation_master.isvoid,0) = 0 "
            'Sohail (17 Sep 2019) - [ROWNO, LEFT JOIN cfcommon_period_tran]

            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            If dtAsOnDate <> Nothing Then
                strQ &= "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
                objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            End If

            strQ &= " ) AS A "

            strQ &= "WHERE   1 = 1 "

            If dtAsOnDate <> Nothing Then
                strQ &= " AND   ROWNO = 1 "
            End If
            'Sohail (17 Sep 2019) -- End

                strQ &= "AND hrbenefit_allocation_master.stationunkid = @stationunkid "
                objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStationId)

                strQ &= "AND hrbenefit_allocation_master.departmentunkid = @departmentunkid "
                objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDepartmentId)

                strQ &= "AND hrbenefit_allocation_master.sectionunkid = @sectionunkid "
                objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSectionId)

                strQ &= "AND hrbenefit_allocation_master.unitunkid = @unitunkid "
                objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnitId)

                strQ &= "AND hrbenefit_allocation_master.jobunkid = @jobunkid "
                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobId)

                strQ &= "AND hrbenefit_allocation_master.gradeunkid = @gradeunkid "
                objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeId)

                strQ &= "AND hrbenefit_allocation_master.classunkid = @classunkid "
                objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intClassId)

                strQ &= "AND hrbenefit_allocation_master.costcenterunkid = @costcenterunkid "
                objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCostCenterId)

            'Pinkal (24-Feb-2017) -- Start
            'Enhancement - Working on Employee Group Benefit Enhancement For TRA.

            strQ &= "AND hrbenefit_allocation_master.deptgroupunkid = @deptgroupunkid "
            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDeptGrpId)

            strQ &= "AND hrbenefit_allocation_master.sectiongroupunkid = @sectiongroupunkid "
            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSectionGrpId)

            strQ &= "AND hrbenefit_allocation_master.unitgroupunkid = @unitgroupunkid "
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnitGrpId)

            strQ &= "AND hrbenefit_allocation_master.teamunkid = @teamunkid "
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTeamId)

            strQ &= "AND hrbenefit_allocation_master.jobgroupunkid = @jobgroupunkid "
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobGrpId)

            strQ &= "AND hrbenefit_allocation_master.gradegroupunkid = @gradegroupunkid "
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeGrpId)

            strQ &= "AND hrbenefit_allocation_master.gradelevelunkid = @gradelevelunkid "
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeLevelId)

            strQ &= "AND hrbenefit_allocation_master.classgroupunkid = @classgroupunkid "
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intClassGrpId)

            'Pinkal (24-Feb-2017) -- End

            strQ &= " Order By Id "

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            'Sohail (17 Sep 2019) -- Start
            'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
            'DisplayError.Show("-1", ex.Message, "GetBenefitGroup", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: GetBenefitGroup; Module Name: " & mstrModuleName)
            'Sohail (17 Sep 2019) -- End
            Return Nothing
        End Try
    End Function

    'Sohail (17 Sep 2019) -- Start
    'NMB Enhancement # TC006: system should be able to bind earning/deduction (benefit) with specific allocation and automatically assign to employee as per his/her allocation.
    Public Function MakeActiveInactive(ByVal blnMakeActive As Boolean, ByVal intUnkid As Integer, ByVal intUserID As Integer _
                                       , ByVal dtVoidDateTime As Date _
                                       , ByVal strVoidReason As String) As Boolean
        If blnMakeActive = False Then
            'If isUsed(intUnkid) Then
            '    mstrMessage = Language.getMessage(mstrModuleName, 3, "This Transaction Head is already in use.")
            '    Return False
            'End If
        End If

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try


            strQ = "UPDATE hrbenefit_allocation_master SET " & _
                        " isactive = @isactive " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", userunkid = @userunkid" & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                    "WHERE benefitallocationunkid = @benefitallocationunkid "

            If blnMakeActive = True Then
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            Else
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserID.ToString)
            End If

            If dtVoidDateTime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            End If
            objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserID.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrbenefit_allocation_master", "benefitallocationunkid", intUnkid, "", "", -1, 2, 0) = False Then
                Return False
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: MakeActiveInactive; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (17 Sep 2019) -- End

    'Sohail (27 Sep 2019) -- Start
    'NMB Enhancement # : System should not allow user to map allocation with benefit while payroll is already processed for employees in the same allocation.
    Public Function GetEmployeeAllocationInPeriod(ByVal xDataOp As clsDataOperation _
                                                , ByVal xDatabaseName As String _
                                                , ByVal xUserUnkid As Integer _
                                                , ByVal xYearUnkid As Integer _
                                                , ByVal xCompanyUnkid As Integer _
                                                , ByVal xPeriodStart As DateTime _
                                                , ByVal xPeriodEnd As DateTime _
                                                , ByVal xUserModeSetting As String _
                                                , ByVal xOnlyApproved As Boolean _
                                                , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                , ByVal strTableName As String _
                                                , ByVal blnApplyUserAccessFilter As Boolean _
                                                , ByVal strFilter As String _
                                                ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim dsEmpAllocation As DataSet = Nothing
        Dim strQ As String = ""
        Dim strB As New StringBuilder
        Dim strEmployeeList As String = ""

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        If blnApplyUserAccessFilter = True Then
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        End If
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strB.Length = 0
            strB.Append("SELECT   hremployee_master.employeeunkid " & _
                           ", hremployee_master.employeecode " & _
                           ", hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname AS employeename " & _
                    "FROM hremployee_master " _
                    )

            If xDateJoinQry.Trim.Length > 0 Then
                strB.Append(xDateJoinQry)
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strB.Append(xUACQry)
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strB.Append(xAdvanceJoinQry)
            End If

            strQ &= "WHERE 1 = 1 "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strB.Append(xDateFilterQry)
                End If
            End If

            'If mstrAdvanceFilter.Length > 0 AndAlso mstrAdvanceFilter.Contains("ADF.") = True Then
            '    strB.Append("AND " & mstrAdvanceFilter)
            'End If

            objDataOperation.ClearParameters()

            dsList = objDataOperation.ExecQuery(strB.ToString, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                strEmployeeList = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("employeeunkid").ToString)).ToArray)

                strB.Length = 0
                strB.Append("SELECT employeeunkid " & _
                             ", effectivedate " & _
                             ", stationunkid " & _
                             ", deptgroupunkid " & _
                             ", departmentunkid " & _
                             ", sectiongroupunkid " & _
                             ", sectionunkid " & _
                             ", unitgroupunkid " & _
                             ", unitunkid " & _
                             ", teamunkid " & _
                             ", classgroupunkid " & _
                             ", classunkid " & _
                             ", 0 AS jobgroupunkid " & _
                             ", 0 AS jobunkid " & _
                             ", 0 AS costcenterunkid " & _
                             ", 0 AS gradegroupunkid " & _
                             ", 0 AS gradeunkid " & _
                             ", 0 AS gradelevelunkid " & _
                        "FROM " & _
                        "( " & _
                            "SELECT ETT.employeeunkid " & _
                                 ", ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
                                 ", ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
                                 ", ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
                                 ", ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                                 ", ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
                                 ", ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
                                 ", ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
                                 ", ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
                                 ", ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
                                 ", ISNULL(ETT.classunkid, 0) AS classunkid " & _
                                 ", CASE " & _
                                       "WHEN CONVERT(CHAR(8), ETT.effectivedate, 112) < @start_date THEN @start_date " & _
                                       "ELSE CONVERT(CHAR(8), ETT.effectivedate, 112) END AS effectivedate " & _
                                 ", ROW_NUMBER() OVER (PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC, ETT.transferunkid DESC) AS Rno " & _
                            "FROM hremployee_transfer_tran AS ETT " & _
                            "WHERE isvoid = 0 " & _
                                  "AND ETT.employeeunkid IN (" & strEmployeeList & ") " & _
                                  "AND CONVERT(CHAR(8), effectivedate, 112) <= @start_date " & _
                        ") AS TRF " & _
                        "WHERE TRF.Rno = 1 " & _
                        "UNION ALL " & _
                        "SELECT ETT.employeeunkid " & _
                             ", CONVERT(CHAR(8), ETT.effectivedate, 112) AS effectivedate " & _
                             ", ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
                             ", ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
                             ", ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
                             ", ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                             ", ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
                             ", ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
                             ", ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
                             ", ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
                             ", ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
                             ", ISNULL(ETT.classunkid, 0) AS classunkid " & _
                             ", 0 AS jobgroupunkid " & _
                             ", 0 AS jobunkid " & _
                             ", 0 AS costcenterunkid " & _
                             ", 0 AS gradegroupunkid " & _
                             ", 0 AS gradeunkid " & _
                             ", 0 AS gradelevelunkid " & _
                        "FROM hremployee_transfer_tran AS ETT " & _
                        "WHERE isvoid = 0 " & _
                              "AND ETT.employeeunkid IN (" & strEmployeeList & ") " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) BETWEEN @start_date + 1 AND @end_date " _
                              )

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@start_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))

                dsEmpAllocation = objDataOperation.ExecQuery(strB.ToString, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If

                If strFilter.ToLower.Contains("jobgroupunkid >") = True OrElse strFilter.ToLower.Contains("jobunkid >") = True Then
                    strB.Length = 0
                    strB.Append("SELECT employeeunkid " & _
                                     ", effectivedate " & _
                                     ", jobgroupunkid " & _
                                     ", jobunkid " & _
                                "FROM " & _
                                "( " & _
                                    "SELECT ECT.employeeunkid " & _
                                         ", ECT.jobgroupunkid " & _
                                         ", ECT.jobunkid " & _
                                         ", CASE WHEN CONVERT(CHAR(8), ECT.effectivedate, 112) < @start_date THEN @start_date " & _
                                               "ELSE CONVERT(CHAR(8), ECT.effectivedate, 112) " & _
                                           "END AS effectivedate " & _
                                         ", ROW_NUMBER() OVER (PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC, ECT.categorizationtranunkid DESC) AS Rno " & _
                                         ", ECT.categorizationtranunkid " & _
                                    "FROM hremployee_categorization_tran AS ECT " & _
                                    "WHERE isvoid = 0 " & _
                                          "AND ECT.employeeunkid IN (" & strEmployeeList & ") " & _
                                          "AND CONVERT(CHAR(8), effectivedate, 112) <= @start_date " & _
                                ") AS Cat " & _
                                "WHERE Cat.Rno = 1 " & _
                                "UNION ALL " & _
                                "SELECT ECT.employeeunkid " & _
                                     ", CONVERT(CHAR(8), ECT.effectivedate, 112) AS effectivedate " & _
                                     ", ECT.jobgroupunkid " & _
                                     ", ECT.jobunkid " & _
                                "FROM hremployee_categorization_tran AS ECT " & _
                                "WHERE isvoid = 0 " & _
                                      "AND ECT.employeeunkid IN (" & strEmployeeList & ") " & _
                                      "AND CONVERT(CHAR(8), effectivedate, 112) BETWEEN @start_date + 1 AND @end_date " _
                                      )

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@start_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                    objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))

                    Dim ds As DataSet = objDataOperation.ExecQuery(strB.ToString, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    End If


                    For Each drJob As DataRow In ds.Tables(0).Select("1 = 1", "employeeunkid, effectivedate")

                        Dim drFirst() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drJob.Item("employeeunkid")) & " AND effectivedate = '" & drJob.Item("effectivedate").ToString & "' ")

                        If drFirst.Length <= 0 Then
                            Dim drLast() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drJob.Item("employeeunkid")) & " AND effectivedate <= '" & drJob.Item("effectivedate").ToString & "' ", "employeeunkid, effectivedate DESC")
                            If drLast.Length > 0 Then
                                Dim d As DataRow = dsEmpAllocation.Tables(0).NewRow
                                d.ItemArray = drLast(0).ItemArray

                                d.Item("effectivedate") = drJob.Item("effectivedate").ToString
                                d.Item("jobgroupunkid") = CInt(drJob.Item("jobgroupunkid"))
                                d.Item("jobunkid") = CInt(drJob.Item("jobunkid"))

                                dsEmpAllocation.Tables(0).Rows.Add(d)
                            End If
                        Else
                            For Each d As DataRow In drFirst
                                d.Item("jobgroupunkid") = CInt(drJob.Item("jobgroupunkid"))
                                d.Item("jobunkid") = CInt(drJob.Item("jobunkid"))
                            Next
                            dsEmpAllocation.Tables(0).AcceptChanges()
                        End If

                        Dim drNext() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drJob.Item("employeeunkid")) & " AND effectivedate > '" & drJob.Item("effectivedate").ToString & "' ", "employeeunkid, effectivedate")

                        For Each d As DataRow In drNext
                            d.Item("jobgroupunkid") = CInt(drJob.Item("jobgroupunkid"))
                            d.Item("jobunkid") = CInt(drJob.Item("jobunkid"))
                        Next
                        dsEmpAllocation.Tables(0).AcceptChanges()
                    Next
                End If

                If strFilter.ToLower.Contains("costcenterunkid >") = True Then
                    strB.Length = 0
                    strB.Append("SELECT employeeunkid " & _
                                     ", effectivedate " & _
                                     ", costcenterunkid " & _
                                "FROM " & _
                                "( " & _
                                    "SELECT CCT.employeeunkid " & _
                                         ", CCT.cctranheadvalueid AS costcenterunkid " & _
                                         ", CASE WHEN CONVERT(CHAR(8), CCT.effectivedate, 112) < @start_date THEN @start_date " & _
                                               "ELSE CONVERT(CHAR(8), CCT.effectivedate, 112) " & _
                                           "END AS effectivedate " & _
                                         ", ROW_NUMBER() OVER (PARTITION BY CCT.employeeunkid ORDER BY CCT.effectivedate DESC, CCT.cctranheadunkid DESC) AS Rno " & _
                                    "FROM hremployee_cctranhead_tran AS CCT " & _
                                    "WHERE CCT.isvoid = 0 " & _
                                          "AND CCT.employeeunkid IN (" & strEmployeeList & ") " & _
                                          "AND CCT.istransactionhead = 0 " & _
                                          "AND CONVERT(CHAR(8), CCT.effectivedate, 112) <= @start_date " & _
                                ") AS CCT " & _
                                "WHERE CCT.Rno = 1 " & _
                                "UNION ALL " & _
                                "SELECT CCT.employeeunkid " & _
                                     ", CONVERT(CHAR(8), CCT.effectivedate, 112) AS effectivedate " & _
                                     ", CCT.cctranheadvalueid AS costcenterunkid " & _
                                "FROM hremployee_cctranhead_tran AS CCT " & _
                                "WHERE CCT.isvoid = 0 " & _
                                      "AND CCT.istransactionhead = 0 " & _
                                      "AND CCT.employeeunkid IN (" & strEmployeeList & ") " & _
                                      "AND CONVERT(CHAR(8), CCT.effectivedate, 112) BETWEEN @start_date + 1 AND @end_date " _
                                      )

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@start_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                    objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))

                    Dim ds As DataSet = objDataOperation.ExecQuery(strB.ToString, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    End If

                    For Each drCC As DataRow In ds.Tables(0).Select("1 = 1", "employeeunkid, effectivedate")

                        Dim drFirst() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drCC.Item("employeeunkid")) & " AND effectivedate = '" & drCC.Item("effectivedate").ToString & "' ")

                        If drFirst.Length <= 0 Then
                            Dim drLast() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drCC.Item("employeeunkid")) & " AND effectivedate <= '" & drCC.Item("effectivedate").ToString & "' ", "employeeunkid, effectivedate DESC")
                            If drLast.Length > 0 Then
                                Dim d As DataRow = dsEmpAllocation.Tables(0).NewRow
                                d.ItemArray = drLast(0).ItemArray

                                d.Item("effectivedate") = drCC.Item("effectivedate").ToString
                                d.Item("costcenterunkid") = CInt(drCC.Item("costcenterunkid"))

                                dsEmpAllocation.Tables(0).Rows.Add(d)
                            End If
                        Else
                            For Each d As DataRow In drFirst
                                d.Item("costcenterunkid") = CInt(drCC.Item("costcenterunkid"))
                            Next
                            dsEmpAllocation.Tables(0).AcceptChanges()
                        End If

                        Dim drNext() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drCC.Item("employeeunkid")) & " AND effectivedate > '" & drCC.Item("effectivedate").ToString & "' ", "employeeunkid, effectivedate")

                        For Each d As DataRow In drNext
                            d.Item("costcenterunkid") = CInt(drCC.Item("costcenterunkid"))
                        Next
                        dsEmpAllocation.Tables(0).AcceptChanges()
                    Next
                End If

                If strFilter.ToLower.Contains("gradegroupunkid >") = True OrElse strFilter.ToLower.Contains("gradeunkid >") = True OrElse strFilter.ToLower.Contains("gradelevelunkid >") = True Then
                    strB.Length = 0
                    strB.Append("SELECT employeeunkid " & _
                                     ", effectivedate " & _
                                     ", gradegroupunkid " & _
                                     ", gradeunkid " & _
                                     ", gradelevelunkid " & _
                                "FROM " & _
                                "( " & _
                                    "SELECT gradegroupunkid " & _
                                         ", gradeunkid " & _
                                         ", gradelevelunkid " & _
                                         ", employeeunkid " & _
                                         ", CASE WHEN CONVERT(CHAR(8), incrementdate, 112) < @start_date THEN @start_date " & _
                                               "ELSE CONVERT(CHAR(8), incrementdate, 112) " & _
                                           "END AS effectivedate " & _
                                         ", ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS Rno " & _
                                    "FROM prsalaryincrement_tran " & _
                                    "WHERE isvoid = 0 " & _
                                          "AND isapproved = 1 " & _
                                          "AND prsalaryincrement_tran.employeeunkid IN (" & strEmployeeList & ") " & _
                                          "AND CONVERT(CHAR(8), incrementdate, 112) <= @start_date " & _
                                ") AS GRD " & _
                                "WHERE GRD.Rno = 1 " & _
                                "UNION ALL " & _
                                "SELECT employeeunkid " & _
                                     ", CONVERT(CHAR(8), incrementdate, 112) AS effectivedate " & _
                                     ", gradegroupunkid " & _
                                     ", gradeunkid " & _
                                     ", gradelevelunkid " & _
                                "FROM prsalaryincrement_tran " & _
                                "WHERE isvoid = 0 " & _
                                      "AND isapproved = 1 " & _
                                      "AND prsalaryincrement_tran.employeeunkid IN (" & strEmployeeList & ") " & _
                                      "AND CONVERT(CHAR(8), incrementdate, 112) BETWEEN @start_date + 1 AND @end_date " _
                                      )

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@start_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                    objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))

                    Dim ds As DataSet = objDataOperation.ExecQuery(strB.ToString, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    End If

                    For Each drGrade As DataRow In ds.Tables(0).Select("1 = 1", "employeeunkid, effectivedate")

                        Dim drFirst() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drGrade.Item("employeeunkid")) & " AND effectivedate = '" & drGrade.Item("effectivedate").ToString & "' ")

                        If drFirst.Length <= 0 Then
                            Dim drLast() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drGrade.Item("employeeunkid")) & " AND effectivedate <= '" & drGrade.Item("effectivedate").ToString & "' ", "employeeunkid, effectivedate DESC")
                            If drLast.Length > 0 Then
                                Dim d As DataRow = dsEmpAllocation.Tables(0).NewRow
                                d.ItemArray = drLast(0).ItemArray

                                d.Item("effectivedate") = drGrade.Item("effectivedate").ToString
                                d.Item("gradegroupunkid") = CInt(drGrade.Item("gradegroupunkid"))
                                d.Item("gradeunkid") = CInt(drGrade.Item("gradeunkid"))
                                d.Item("gradelevelunkid") = CInt(drGrade.Item("gradelevelunkid"))

                                dsEmpAllocation.Tables(0).Rows.Add(d)
                            End If
                        Else
                            For Each d As DataRow In drFirst
                                d.Item("gradegroupunkid") = CInt(drGrade.Item("gradegroupunkid"))
                                d.Item("gradeunkid") = CInt(drGrade.Item("gradeunkid"))
                                d.Item("gradelevelunkid") = CInt(drGrade.Item("gradelevelunkid"))
                            Next
                            dsEmpAllocation.Tables(0).AcceptChanges()
                        End If

                        Dim drNext() As DataRow = dsEmpAllocation.Tables(0).Select("employeeunkid = " & CInt(drGrade.Item("employeeunkid")) & " AND effectivedate > '" & drGrade.Item("effectivedate").ToString & "' ", "employeeunkid, effectivedate")

                        For Each d As DataRow In drNext
                            d.Item("gradegroupunkid") = CInt(drGrade.Item("gradegroupunkid"))
                            d.Item("gradeunkid") = CInt(drGrade.Item("gradeunkid"))
                            d.Item("gradelevelunkid") = CInt(drGrade.Item("gradelevelunkid"))
                        Next
                        dsEmpAllocation.Tables(0).AcceptChanges()
                    Next
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeAllocationInPeriod; Module Name: " & mstrModuleName)
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsEmpAllocation
    End Function
    'Sohail (27 Sep 2019) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Benefit Group is already assigned for the selected information. Please assign new Benefit Group.")
			Language.setMessage(mstrModuleName, 2, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

''************************************************************************************************************************************
''Class Name : clsBenefitAllocation.vb
''Purpose    :
''Date       :27/07/2010
''Written By :Sandeep J. Sharma
''Modified   :
''************************************************************************************************************************************

'Imports eZeeCommonLib
'Imports Aruti.Data.clsCommon_Master
'''' <summary>
'''' Purpose: 
'''' Developer: Sandeep J. Sharma
'''' </summary>
'Public Class clsBenefitAllocation
'    Private Shared Readonly mstrModuleName As String = "clsBenefitAllocation"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "
'    Private mintBenefitallocationunkid As Integer
'    Private mintStationunkid As Integer
'    Private mintDepartmentunkid As Integer
'    Private mintSectionunkid As Integer
'    Private mintUnitunkid As Integer
'    Private mintJobunkid As Integer
'    Private mintGradeunkid As Integer
'    Private mintClassunkid As Integer
'    Private mintCostcenterunkid As Integer
'    Private mintUserunkid As Integer
'    Private mblnIsvoid As Boolean
'    Private mintVoiduserunkid As Integer
'    Private mdtVoiddatetime As Date
'    Private mstrVoidreason As String = String.Empty
'    Private mstrBenefitGroup As String = String.Empty
'#End Region

'#Region " Properties "
'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set benefitallocationunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Benefitallocationunkid() As Integer
'        Get
'            Return mintBenefitallocationunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintBenefitallocationunkid = Value
'            Call getData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set stationunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Stationunkid() As Integer
'        Get
'            Return mintStationunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintStationunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set departmentunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Departmentunkid() As Integer
'        Get
'            Return mintDepartmentunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintDepartmentunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set sectionunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Sectionunkid() As Integer
'        Get
'            Return mintSectionunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintSectionunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set unitunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Unitunkid() As Integer
'        Get
'            Return mintUnitunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintUnitunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set jobunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Jobunkid() As Integer
'        Get
'            Return mintJobunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintJobunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set gradeunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Gradeunkid() As Integer
'        Get
'            Return mintGradeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintGradeunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set classunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Classunkid() As Integer
'        Get
'            Return mintClassunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintClassunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set costcenterunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Costcenterunkid() As Integer
'        Get
'            Return mintCostcenterunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintCostcenterunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set userunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Userunkid() As Integer
'        Get
'            Return mintUserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintUserunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isvoid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Isvoid() As Boolean
'        Get
'            Return mblnIsvoid
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsvoid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiduserunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voiduserunkid() As Integer
'        Get
'            Return mintVoiduserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintVoiduserunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiddatetime
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voiddatetime() As Date
'        Get
'            Return mdtVoiddatetime
'        End Get
'        Set(ByVal value As Date)
'            mdtVoiddatetime = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voidreason
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voidreason() As String
'        Get
'            Return mstrVoidreason
'        End Get
'        Set(ByVal value As String)
'            mstrVoidreason = Value
'        End Set
'    End Property

'    Public Property _BenefitGroup() As String
'        Get
'            Return mstrBenefitGroup
'        End Get
'        Set(ByVal value As String)
'            mstrBenefitGroup = value
'        End Set
'    End Property

'#End Region

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                          "  benefitallocationunkid " & _
'                          ", stationunkid " & _
'                          ", departmentunkid " & _
'                          ", sectionunkid " & _
'                          ", unitunkid " & _
'                          ", jobunkid " & _
'                          ", gradeunkid " & _
'                          ", classunkid " & _
'                          ", costcenterunkid " & _
'                          ", userunkid " & _
'                          ", isvoid " & _
'                          ", voiduserunkid " & _
'                          ", voiddatetime " & _
'                          ", voidreason " & _
'                         "FROM hrbenefit_allocation_master " & _
'                         "WHERE benefitallocationunkid = @benefitallocationunkid "

'            objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintBenefitallocationUnkId.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintbenefitallocationunkid = CInt(dtRow.Item("benefitallocationunkid"))
'                mintstationunkid = CInt(dtRow.Item("stationunkid"))
'                mintdepartmentunkid = CInt(dtRow.Item("departmentunkid"))
'                mintsectionunkid = CInt(dtRow.Item("sectionunkid"))
'                mintunitunkid = CInt(dtRow.Item("unitunkid"))
'                mintjobunkid = CInt(dtRow.Item("jobunkid"))
'                mintgradeunkid = CInt(dtRow.Item("gradeunkid"))
'                mintclassunkid = CInt(dtRow.Item("classunkid"))
'                mintcostcenterunkid = CInt(dtRow.Item("costcenterunkid"))
'                mintuserunkid = CInt(dtRow.Item("userunkid"))
'                mblnisvoid = CBool(dtRow.Item("isvoid"))
'                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
'                If IsDBNull(dtRow.Item("voiddatetime")) Then
'                    mdtVoiddatetime = Nothing
'                Else
'                    mdtVoiddatetime = dtRow.Item("voiddatetime")
'                End If
'                mstrVoidreason = dtRow.Item("voidreason").ToString
'                Exit For
'            Next

'            mstrBenefitGroup = Get_BenefitGroup()
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                           " hrbenefit_allocation_master.benefitallocationunkid " & _
'                           ",hrbenefit_allocation_master.stationunkid " & _
'                           ",hrbenefit_allocation_master.departmentunkid " & _
'                           ",hrbenefit_allocation_master.sectionunkid " & _
'                           ",hrbenefit_allocation_master.unitunkid " & _
'                           ",hrbenefit_allocation_master.jobunkid " & _
'                           ",hrbenefit_allocation_master.gradeunkid " & _
'                           ",hrbenefit_allocation_master.classunkid " & _
'                           ",hrbenefit_allocation_master.costcenterunkid " & _
'                           ",hrbenefit_allocation_master.userunkid " & _
'                           ",hrbenefit_allocation_master.isvoid " & _
'                           ",hrbenefit_allocation_master.voiduserunkid " & _
'                           ",hrbenefit_allocation_master.voiddatetime " & _
'                           ",hrbenefit_allocation_master.voidreason " & _
'                           ",ISNULL(prcostcenter_master.costcentername,'') AS CostCenter " & _
'                           ",ISNULL(hrclasses_master.name,'') AS ClassName " & _
'                           ",ISNULL(hrgrade_master.name,'') AS Grade " & _
'                           ",ISNULL(hrunit_master.name,'') AS Unit " & _
'                           ",ISNULL(hrsection_master.name,'') AS Section " & _
'                           ",ISNULL(hrdepartment_master.name,'') AS Department " & _
'                           ",ISNULL(hrstation_master.name,'') AS Station " & _
'                           ",ISNULL(cfcommon_master.name,'') AS BenefitGrp " & _
'                           ",ISNULL(hrjob_master.job_name,'') AS Job " & _
'                           ",hrbenefit_allocation_tran.benefitallocationtranunkid " & _
'                        "FROM hrbenefit_allocation_master " & _
'                             "LEFT JOIN hrjob_master ON hrbenefit_allocation_master.jobunkid = hrjob_master.jobunkid " & _
'                             "LEFT JOIN hrbenefit_allocation_tran ON hrbenefit_allocation_master.benefitallocationunkid = hrbenefit_allocation_tran.benefitallocationunkid " & _
'                             "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid=hrbenefit_allocation_tran.benefitgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.BENEFIT_GROUP & _
'                             "LEFT JOIN prcostcenter_master ON hrbenefit_allocation_master.costcenterunkid = prcostcenter_master.costcenterunkid " & _
'                             "LEFT JOIN hrclasses_master ON hrbenefit_allocation_master.classunkid = hrclasses_master.classesunkid " & _
'                             "LEFT JOIN hrgrade_master ON hrbenefit_allocation_master.gradeunkid = hrgrade_master.gradeunkid " & _
'                             "LEFT JOIN hrunit_master ON hrbenefit_allocation_master.unitunkid = hrunit_master.unitunkid " & _
'                             "LEFT JOIN hrsection_master ON hrbenefit_allocation_master.sectionunkid = hrsection_master.sectionunkid " & _
'                             "LEFT JOIN hrdepartment_master ON hrbenefit_allocation_master.departmentunkid = hrdepartment_master.     departmentunkid " & _
'                             "LEFT JOIN hrstation_master ON hrbenefit_allocation_master.stationunkid = hrstation_master.stationunkid " & _
'                        "WHERE ISNULL(hrbenefit_allocation_master.isvoid,0)=0  " & _
'                        "AND ISNULL(hrbenefit_allocation_tran.isvoid,0)=0 " & _
'                        "ORDER BY hrbenefit_allocation_master.benefitallocationunkid "



'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (hrbenefit_allocation_master) </purpose>
'    Public Function Insert() As Boolean
'        Dim strBenefitId As String = ""
'        isExist(strBenefitId, _
'                mintStationunkid, _
'                mintDepartmentunkid, _
'                mintSectionunkid, _
'                mintUnitunkid, _
'                mintJobunkid, _
'                mintGradeunkid, _
'                mintClassunkid, _
'                mintCostcenterunkid, _
'                mstrBenefitGroup)
'        If strBenefitId.Length = 0 Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "Selected Benefit Group is already defined. Please define new Benefit Group.")
'            Return False
'        Else
'            mstrBenefitGroup = strBenefitId
'        End If
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation
'        objDataOperation.BindTransaction()
'        Try
'            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
'            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
'            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
'            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitunkid.ToString)
'            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
'            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
'            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
'            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            If mdtVoiddatetime = Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            End If
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'            strQ = "INSERT INTO hrbenefit_allocation_master ( " & _
'                              "  stationunkid " & _
'                              ", departmentunkid " & _
'                              ", sectionunkid " & _
'                              ", unitunkid " & _
'                              ", jobunkid " & _
'                              ", gradeunkid " & _
'                              ", classunkid " & _
'                              ", costcenterunkid " & _
'                              ", userunkid " & _
'                              ", isvoid " & _
'                              ", voiduserunkid " & _
'                              ", voiddatetime " & _
'                              ", voidreason" & _
'                        ") VALUES (" & _
'                              "  @stationunkid " & _
'                              ", @departmentunkid " & _
'                              ", @sectionunkid " & _
'                              ", @unitunkid " & _
'                              ", @jobunkid " & _
'                              ", @gradeunkid " & _
'                              ", @classunkid " & _
'                              ", @costcenterunkid " & _
'                              ", @userunkid " & _
'                              ", @isvoid " & _
'                              ", @voiduserunkid " & _
'                              ", @voiddatetime " & _
'                              ", @voidreason" & _
'                            "); SELECT @@identity"

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintBenefitallocationunkid = dsList.Tables(0).Rows(0).Item(0)

'            If mstrBenefitGroup.Length > 0 Then
'                Dim blnFlag As Boolean = False
'                blnFlag = InsertBenefitGroup()
'                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
'            End If

'            objDataOperation.ReleaseTransaction(True)

'            Return True

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (hrbenefit_allocation_master) </purpose>
'    Public Function Update() As Boolean
'        Dim strBenefitId As String = ""
'        isExist(strBenefitId, _
'                mintStationunkid, _
'                mintDepartmentunkid, _
'                mintSectionunkid, _
'                mintUnitunkid, _
'                mintJobunkid, _
'                mintGradeunkid, _
'                mintClassunkid, _
'                mintCostcenterunkid, _
'                mstrBenefitGroup, _
'                mintBenefitallocationunkid)
'        If strBenefitId.Length = 0 Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "Selected Benefit Group is already defined. Please define new Benefit Group.")
'            Return False
'        Else
'            mstrBenefitGroup = strBenefitId
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation
'        objDataOperation.BindTransaction()
'        Try
'            objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbenefitallocationunkid.ToString)
'            objDataOperation.AddParameter("@stationunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstationunkid.ToString)
'            objDataOperation.AddParameter("@departmentunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintdepartmentunkid.ToString)
'            objDataOperation.AddParameter("@sectionunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintsectionunkid.ToString)
'            objDataOperation.AddParameter("@unitunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintunitunkid.ToString)
'            objDataOperation.AddParameter("@jobunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintjobunkid.ToString)
'            objDataOperation.AddParameter("@gradeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintgradeunkid.ToString)
'            objDataOperation.AddParameter("@classunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintclassunkid.ToString)
'            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintcostcenterunkid.ToString)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
'            If mdtVoiddatetime = Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            End If
'            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

'            strQ = "UPDATE hrbenefit_allocation_master SET " & _
'                          "  stationunkid = @stationunkid" & _
'                          ", departmentunkid = @departmentunkid" & _
'                          ", sectionunkid = @sectionunkid" & _
'                          ", unitunkid = @unitunkid" & _
'                          ", jobunkid = @jobunkid" & _
'                          ", gradeunkid = @gradeunkid" & _
'                          ", classunkid = @classunkid" & _
'                          ", costcenterunkid = @costcenterunkid" & _
'                          ", userunkid = @userunkid" & _
'                          ", isvoid = @isvoid" & _
'                          ", voiduserunkid = @voiduserunkid" & _
'                          ", voiddatetime = @voiddatetime" & _
'                          ", voidreason = @voidreason " & _
'                        "WHERE benefitallocationunkid = @benefitallocationunkid "

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If mstrBenefitGroup.Length > 0 Then
'                Dim blnFlag As Boolean = False
'                blnFlag = InsertBenefitGroup()
'                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
'            End If

'            objDataOperation.ReleaseTransaction(True)

'            Return True

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (hrbenefit_allocation_master) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal blnIsSingle As Boolean = False) As Boolean
'        'If isUsed(intUnkid) Then
'        '    mstrMessage = "<Message>"
'        '    Return False
'        'End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Dim blnFlag As Boolean = False

'        objDataOperation = New clsDataOperation
'        objDataOperation.BindTransaction()
'        Try
'            If blnIsSingle = True Then
'                strQ = "UPDATE hrbenefit_allocation_master SET " & _
'                          "  isvoid = @isvoid" & _
'                          ", voiduserunkid = @voiduserunkid" & _
'                          ", voiddatetime = @voiddatetime" & _
'                          ", voidreason = @voidreason " & _
'                        "WHERE benefitallocationunkid = @benefitallocationunkid "

'                objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
'                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'                Call objDataOperation.ExecNonQuery(strQ)

'                If objDataOperation.ErrorMessage <> "" Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If

'                blnFlag = DeleteBenefitGroup(intUnkid, True)
'                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

'            Else
'                blnFlag = DeleteBenefitGroup(intUnkid, False)
'                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
'            End If


'            objDataOperation.ReleaseTransaction(True)

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "<Query>"

'            objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.tables(0).rows.count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(ByRef StrInsertId As String, _
'                            Optional ByVal intStationId As Integer = -1, _
'                            Optional ByVal intDeptId As Integer = -1, _
'                            Optional ByVal intSectionId As Integer = -1, _
'                            Optional ByVal intUnitId As Integer = -1, _
'                            Optional ByVal intJobId As Integer = -1, _
'                            Optional ByVal intGradeId As Integer = -1, _
'                            Optional ByVal intClassId As Integer = -1, _
'                            Optional ByVal intCostCenterId As Integer = -1, _
'                            Optional ByVal strBenefitGroup As String = "", _
'                            Optional ByVal intUnkid As Integer = -1) As String
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Dim StrBenefitGroupId() As String = Nothing

'        If strBenefitGroup.Length > 0 Then
'            StrBenefitGroupId = strBenefitGroup.Split("|")
'        End If

'        objDataOperation = New clsDataOperation

'        Try
'            For i As Integer = 0 To StrBenefitGroupId.Length - 1
'                objDataOperation.ClearParameters()
'                strQ = "SELECT " & _
'                          "  hrbenefit_allocation_master.benefitallocationunkid " & _
'                          ", hrbenefit_allocation_master.stationunkid " & _
'                          ", hrbenefit_allocation_master.departmentunkid " & _
'                          ", hrbenefit_allocation_master.sectionunkid " & _
'                          ", hrbenefit_allocation_master.unitunkid " & _
'                          ", hrbenefit_allocation_master.jobunkid " & _
'                          ", hrbenefit_allocation_master.gradeunkid " & _
'                          ", hrbenefit_allocation_master.classunkid " & _
'                          ", hrbenefit_allocation_master.costcenterunkid " & _
'                          ", hrbenefit_allocation_master.userunkid " & _
'                          ", hrbenefit_allocation_master.isvoid " & _
'                          ", hrbenefit_allocation_master.voiduserunkid " & _
'                          ", hrbenefit_allocation_master.voiddatetime " & _
'                          ", hrbenefit_allocation_master.voidreason " & _
'                         "FROM hrbenefit_allocation_master " & _
'                         "JOIN hrbenefit_allocation_tran ON hrbenefit_allocation_master.benefitallocationunkid = hrbenefit_allocation_tran.benefitallocationunkid " & _
'                         "WHERE ISNULL(hrbenefit_allocation_master.isvoid,0) = 0 " & _
'                         "AND hrbenefit_allocation_tran.benefitgroupunkid = @benefitgroupunkid "

'                objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, StrBenefitGroupId(i))


'                If intStationId > 0 Then
'                    strQ &= "AND hrbenefit_allocation_master.stationunkid = @stationunkid "
'                    objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStationId)
'                End If

'                If intDeptId > 0 Then
'                    strQ &= "AND hrbenefit_allocation_master.departmentunkid = @departmentunkid "
'                    objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDeptId)
'                End If

'                If intSectionId > 0 Then
'                    strQ &= "AND hrbenefit_allocation_master.sectionunkid = @sectionunkid "
'                    objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSectionId)
'                End If

'                If intUnitId > 0 Then
'                    strQ &= "AND hrbenefit_allocation_master.unitunkid = @unitunkid "
'                    objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnitId)
'                End If

'                If intJobId > 0 Then
'                    strQ &= "AND hrbenefit_allocation_master.jobunkid = @jobunkid "
'                    objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobId)
'                End If

'                If intGradeId > 0 Then
'                    strQ &= "AND hrbenefit_allocation_master.gradeunkid = @gradeunkid "
'                    objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGradeId)
'                End If

'                If intClassId > 0 Then
'                    strQ &= "AND hrbenefit_allocation_master.classunkid = @classunkid "
'                    objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intClassId)
'                End If

'                If intCostCenterId > 0 Then
'                    strQ &= "AND hrbenefit_allocation_master.costcenterunkid = @costcenterunkid "
'                    objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCostCenterId)
'                End If

'                If intUnkid > 0 Then
'                    strQ &= " AND hrbenefit_allocation_master.benefitallocationunkid <> @benefitallocationunkid"
'                    objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
'                End If

'                dsList = objDataOperation.ExecQuery(strQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If

'                If dsList.Tables(0).Rows.Count = 0 Then
'                    If StrInsertId.Length <= 0 Then
'                        StrInsertId &= StrBenefitGroupId(i)
'                    Else
'                        StrInsertId &= "|" & StrBenefitGroupId(i)
'                    End If
'                Else
'                    StrInsertId = ""
'                End If
'            Next

'            Return StrInsertId

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'            Return Nothing
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function


'#Region " Private Functions "
'    Public Function InsertBenefitGroup() As Boolean
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Dim strBenefitGroup_value() As String = Nothing
'        Try

'            Call DeleteBenefitGroup(-1, True)

'            If mstrBenefitGroup.Length > 0 Then
'                strBenefitGroup_value = mstrBenefitGroup.Split("|")
'                For i As Integer = 0 To strBenefitGroup_value.Length - 1
'                    objDataOperation.ClearParameters()
'                    objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitallocationunkid.ToString)
'                    objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strBenefitGroup_value(i).ToString)
'                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'                    If mdtVoiddatetime = Nothing Then
'                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'                    Else
'                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'                    End If
'                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'                    StrQ = "INSERT INTO hrbenefit_allocation_tran ( " & _
'                                        "  benefitallocationunkid " & _
'                                        ", benefitgroupunkid " & _
'                                        ", isvoid " & _
'                                        ", voiduserunkid " & _
'                                        ", voiddatetime " & _
'                                        ", voidreason" & _
'                                ") VALUES (" & _
'                                        "  @benefitallocationunkid " & _
'                                        ", @benefitgroupunkid " & _
'                                        ", @isvoid " & _
'                                        ", @voiduserunkid " & _
'                                        ", @voiddatetime " & _
'                                        ", @voidreason" & _
'                                "); SELECT @@identity"

'                    objDataOperation.ExecNonQuery(StrQ)

'                    If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If
'                Next
'            End If

'            Return True

'        Catch ex As Exception

'        End Try
'    End Function

'    Public Function DeleteBenefitGroup(Optional ByVal intUnkid As Integer = -1, Optional ByVal isAllDelete As Boolean = False) As Boolean
'        Dim StrQ As String = ""
'        Dim exForce As Exception

'        Try


'            StrQ = "DELETE FROM hrbenefit_allocation_tran "

'            If isAllDelete = False Then
'                StrQ &= "WHERE benefitallocationtranunkid = @TranId "
'            Else
'                StrQ &= "WHERE benefitallocationunkid = @TranId "
'            End If


'            objDataOperation.ClearParameters()

'            objDataOperation.AddParameter("@TranId", SqlDbType.Int, eZeeDataType.NAME_SIZE, IIf(intUnkid = -1, mintBenefitallocationunkid, intUnkid))
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            If mdtVoiddatetime = Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            End If
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'            Call objDataOperation.ExecNonQuery(StrQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
'        End Try
'    End Function

'    Private Function Get_BenefitGroup() As String
'        Dim strBenefitGroup As String = String.Empty
'        Dim StrQ As String = ""
'        Dim dsList As New DataSet
'        Dim exForce As Exception
'        Try
'            StrQ = "SELECT " & _
'                            "  benefitallocationunkid " & _
'                            ", benefitgroupunkid " & _
'                       "FROM hrbenefit_allocation_tran " & _
'                       "WHERE benefitallocationunkid = " & mintBenefitallocationunkid & _
'                       " AND ISNULL(isVoid,0) = 0 "

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                If strBenefitGroup.Length <= 0 Then
'                    strBenefitGroup = dtRow.Item("benefitgroupunkid").ToString
'                Else
'                    strBenefitGroup &= "|" & dtRow.Item("benefitgroupunkid").ToString
'                End If
'            Next
'            Return strBenefitGroup
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Get_BenefitGroup", mstrModuleName)
'            Return Nothing
'        End Try
'    End Function

'    Public Function GetCount(ByVal intUnkid As Integer, ByRef intCnt As Integer) As Integer
'        Dim StrQ As String = ""
'        Dim dsList As New DataSet
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "SELECT " & _
'                            "	COUNT(*)  " & _
'                       "FROM hrbenefit_allocation_tran  " & _
'                       "WHERE ISNULL(isvoid,0)= 0  " & _
'                       "AND benefitallocationunkid = @TransId "

'            objDataOperation.AddParameter("@TransId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            intCnt = dsList.Tables(0).Rows(0)(0)

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetCount", mstrModuleName)
'        End Try
'    End Function
'#End Region

'End Class