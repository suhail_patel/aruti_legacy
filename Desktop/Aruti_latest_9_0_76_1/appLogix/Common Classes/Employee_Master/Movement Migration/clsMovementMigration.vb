﻿'************************************************************************************************************************************
'Class Name : clsclsMovementMigration.vb
'Purpose    :
'Date       :24-09-2019
'Written By :Gajanan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Gajanan
''' </summary>
Public Class clsMovementMigration
    Private Const mstrModuleName = "clsclsMovementMigration"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private mdtTran As DataTable

#Region " Private variables "
    Private mintMigrationunkid As Integer
    Private mintFromapproverunkid As Integer = 0
    Private mintToapproverunkid As Integer = 0
    Private mintTransactionid As Integer = 0
    Private mintEmployeeunkid As Integer = 0
    Private mintAssesoroprationtype As Integer = 0
    Private mintModuletype As Integer = 0
    Private mintScreentype As Integer = 0
    Private mblnIsskip As Boolean = True
    Private mblnIsfromapproval As Boolean = False
    Private mintOprationtype As Integer = 0
    Private mblnIsprocess As Boolean = False

    Private mdtTransactiondate As Date
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintVoiduserunkid As Integer
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer
    Private mstrIp As String = String.Empty
    Private mstrHostname As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean

    Private mintLoginemployeeunkid As Integer = -1
    Private mintVoidloginemployeeunkid As Integer = -1


#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set migrationunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Migrationunkid() As Integer
        Get
            Return mintMigrationunkid
        End Get
        Set(ByVal value As Integer)
            mintMigrationunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fromapproverunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Fromapproverunkid() As Integer
        Get
            Return mintFromapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintFromapproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set toapproverunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Toapproverunkid() As Integer
        Get
            Return mintToapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintToapproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactionid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Transactionid() As Integer
        Get
            Return mintTransactionid
        End Get
        Set(ByVal value As Integer)
            mintTransactionid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assesoroprationtype
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Assesoroprationtype() As Integer
        Get
            Return mintAssesoroprationtype
        End Get
        Set(ByVal value As Integer)
            mintAssesoroprationtype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set moduletype
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Moduletype() As Integer
        Get
            Return mintModuletype
        End Get
        Set(ByVal value As Integer)
            mintModuletype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set screentype
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Screentype() As Integer
        Get
            Return mintScreentype
        End Get
        Set(ByVal value As Integer)
            mintScreentype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isskip
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isskip() As Boolean
        Get
            Return mblnIsskip
        End Get
        Set(ByVal value As Boolean)
            mblnIsskip = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfromapproval
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isfromapproval() As Boolean
        Get
            Return mblnIsfromapproval
        End Get
        Set(ByVal value As Boolean)
            mblnIsfromapproval = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set oprationtype
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Oprationtype() As Integer
        Get
            Return mintOprationtype
        End Get
        Set(ByVal value As Integer)
            mintOprationtype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isprocess
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isprocess() As Boolean
        Get
            Return mblnIsprocess
        End Get
        Set(ByVal value As Boolean)
            mblnIsprocess = value
        End Set
    End Property

    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = value
        End Set
    End Property

    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = value
        End Set
    End Property

    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    Public Property _Hostname() As String
        Get
            Return mstrHostname
        End Get
        Set(ByVal value As String)
            mstrHostname = value
        End Set
    End Property

    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = value
        End Set
    End Property

    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = value
        End Set
    End Property
#End Region


#Region " Constructor "

    Public Sub New()
        mdtTran = New DataTable("ApproverMigration")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("ScreenType")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("OldApprover")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("NewApprover")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isSkip")
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = True
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("OldApproverId")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("NewApproverId")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("transactionid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("assesoroprationtype")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("moduletype")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("approverunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("oldapproverunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isReviewer")
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = 0
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isExternal")
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = 0
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ExpenseType")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("MigrationTranid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)


            'Gajanan [04-June-2020] -- Start
            dCol = New DataColumn("IsActive")
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)
            'Gajanan [04-June-2020] -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  migrationunkid " & _
              ", fromapproverunkid " & _
              ", toapproverunkid " & _
              ", transactionid " & _
              ", employeeunkid " & _
              ", assesoroprationtype " & _
              ", moduletype " & _
              ", screentype " & _
              ", isskip " & _
              ", isfromapproval " & _
              ", oprationtype " & _
              ", isprocess " & _
             "FROM hremployee_movement_migration " & _
             "WHERE migrationunkid = @migrationunkid "

            objDataOperation.AddParameter("@migrationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMigrationunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintMigrationunkid = CInt(dtRow.Item("migrationunkid"))
                mintFromapproverunkid = CInt(dtRow.Item("fromapproverunkid"))
                mintToapproverunkid = CInt(dtRow.Item("toapproverunkid"))
                mintTransactionid = CInt(dtRow.Item("transactionid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintAssesoroprationtype = CInt(dtRow.Item("assesoroprationtype"))
                mintModuletype = CInt(dtRow.Item("moduletype"))
                mintScreentype = CInt(dtRow.Item("screentype"))
                mblnIsskip = CBool(dtRow.Item("isskip"))
                mblnIsfromapproval = CBool(dtRow.Item("isfromapproval"))
                mintOprationtype = CInt(dtRow.Item("oprationtype"))
                mblnIsprocess = CBool(dtRow.Item("isprocess"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    Public Sub GetEmployeeWiseApprover()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  migrationunkid " & _
              ", fromapproverunkid " & _
              ", toapproverunkid " & _
              ", transactionid " & _
              ", employeeunkid " & _
              ", assesoroprationtype " & _
              ", moduletype " & _
              ", screentype " & _
              ", isskip " & _
              ", isfromapproval " & _
              ", oprationtype " & _
              ", isprocess " & _
             "FROM hremployee_movement_migration " & _
             "WHERE migrationunkid = @migrationunkid "

            objDataOperation.AddParameter("@migrationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMigrationunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintMigrationunkid = CInt(dtRow.Item("migrationunkid"))
                mintFromapproverunkid = CInt(dtRow.Item("fromapproverunkid"))
                mintToapproverunkid = CInt(dtRow.Item("toapproverunkid"))
                mintTransactionid = CInt(dtRow.Item("transactionid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintAssesoroprationtype = CInt(dtRow.Item("assesoroprationtype"))
                mintModuletype = CInt(dtRow.Item("moduletype"))
                mintScreentype = CInt(dtRow.Item("screentype"))
                mblnIsskip = CBool(dtRow.Item("isskip"))
                mblnIsfromapproval = CBool(dtRow.Item("isfromapproval"))
                mintOprationtype = CInt(dtRow.Item("oprationtype"))
                mblnIsprocess = CBool(dtRow.Item("isprocess"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  migrationunkid " & _
              ", fromapproverunkid " & _
              ", toapproverunkid " & _
              ", transactionid " & _
              ", employeeunkid " & _
              ", assesoroprationtype " & _
              ", moduletype " & _
              ", screentype " & _
              ", isskip " & _
              ", isfromapproval " & _
              ", oprationtype " & _
              ", isprocess " & _
             "FROM hremployee_movement_migration "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function getMovementApprovalData(ByVal xmoduleType As enApprovalMigrationScreenType, _
                                            ByVal xemployeeUnkid As Integer, _
                                            ByVal xoprationtype As Integer, _
                                            ByVal xDataOpr As clsDataOperation) As DataTable
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        'If xDataOpr IsNot Nothing Then
        '    objDataOperation = xDataOpr
        'Else
        '    objDataOperation = New clsDataOperation
        '    objDataOperation.BindTransaction()
        'End If

        objDataOperation = xDataOpr
        objDataOperation.ClearParameters()


        Try
            strQ = "SELECT * from hremployee_movement_migration where screentype = " & CInt(xmoduleType) & " and " & _
                   "employeeunkid = " & xemployeeUnkid & " And isfromapproval = 1 and  isprocess = 0 and oprationtype = " & xoprationtype & ""

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            mdtTran.Rows.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim drrow As DataRow = mdtTran.NewRow
                drrow("isSkip") = CInt(dtRow.Item("isskip"))
                drrow("OldApproverId") = CInt(dtRow.Item("fromapproverunkid"))
                drrow("NewApproverId") = CInt(dtRow.Item("toapproverunkid"))
                drrow("transactionid") = CInt(dtRow.Item("transactionid"))
                drrow("employeeunkid") = CInt(dtRow.Item("employeeunkid"))
                drrow("assesoroprationtype") = CInt(dtRow.Item("assesoroprationtype"))
                drrow("moduletype") = CInt(dtRow.Item("moduletype"))
                drrow("screentype") = CInt(dtRow.Item("screentype"))

                Dim OldApproverEmployeeid As Integer = 0
                Dim NewApproverEmployeeid As Integer = 0

                Select Case CType(CInt(dtRow.Item("moduletype")), enApprovalMigration)
                   
                    Case enApprovalMigration.Leave

                        Dim objLeaveapprover As New clsleaveapprover_master
                        objLeaveapprover._DoOperation = objDataOperation

                        objLeaveapprover._Approverunkid = CInt(dtRow.Item("fromapproverunkid"))
                        OldApproverEmployeeid = objLeaveapprover._leaveapproverunkid

                        objLeaveapprover._Approverunkid = CInt(dtRow.Item("toapproverunkid"))
                        NewApproverEmployeeid = objLeaveapprover._leaveapproverunkid
                        drrow("isExternal") = objLeaveapprover._Isexternalapprover

                        objLeaveapprover = Nothing

                    Case enApprovalMigration.Loan

                        Dim objLoanapprover As New clsLoanApprover_master

                        objLoanapprover._lnApproverunkid(objDataOperation) = CInt(dtRow.Item("fromapproverunkid"))
                        OldApproverEmployeeid = objLoanapprover._ApproverEmpunkid

                        objLoanapprover._lnApproverunkid(objDataOperation) = CInt(dtRow.Item("toapproverunkid"))
                        NewApproverEmployeeid = objLoanapprover._ApproverEmpunkid
                        drrow("isExternal") = objLoanapprover._IsExternalApprover

                        objLoanapprover = Nothing

                    Case enApprovalMigration.Claim
                        Dim objExpenseapprover As New clsExpenseApprover_Master

                        objExpenseapprover._crApproverunkid(objDataOperation) = CInt(dtRow.Item("fromapproverunkid"))
                        OldApproverEmployeeid = objExpenseapprover._Employeeunkid

                        objExpenseapprover._crApproverunkid(objDataOperation) = CInt(dtRow.Item("toapproverunkid"))
                        NewApproverEmployeeid = objExpenseapprover._Employeeunkid

                        drrow("ExpenseType") = CInt(objExpenseapprover._Expensetypeid)
                        drrow("isExternal") = objExpenseapprover._Isexternalapprover

                        objExpenseapprover = Nothing

                    Case enApprovalMigration.BudgetTimesheet
                        Dim objBugetTimeSheetapprover As New clstsapprover_master

                        objBugetTimeSheetapprover._Tsapproverunkid(objDataOperation) = CInt(dtRow.Item("fromapproverunkid"))
                        OldApproverEmployeeid = objBugetTimeSheetapprover._Employeeapproverunkid

                        objBugetTimeSheetapprover._Tsapproverunkid(objDataOperation) = CInt(dtRow.Item("toapproverunkid"))
                        NewApproverEmployeeid = objBugetTimeSheetapprover._Employeeapproverunkid
                        drrow("isExternal") = objBugetTimeSheetapprover._Isexternalapprover
                        objBugetTimeSheetapprover = Nothing

                    Case enApprovalMigration.Performance
                        Dim objAssesmentApprover As New clsAssessor

                        objAssesmentApprover._Assessormasterunkid(objDataOperation) = CInt(dtRow.Item("fromapproverunkid"))
                        OldApproverEmployeeid = objAssesmentApprover._EmployeeId

                        objAssesmentApprover._Assessormasterunkid(objDataOperation) = CInt(dtRow.Item("toapproverunkid"))
                        NewApproverEmployeeid = objAssesmentApprover._EmployeeId

                        drrow("isReviewer") = objAssesmentApprover._Isreviewer
                        drrow("isExternal") = objAssesmentApprover._ExternalAssessorReviewer
                        objAssesmentApprover = Nothing

                        'S.SANDEEP |04-MAR-2020| -- START
                        'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
                    Case enApprovalMigration.OTRequisition
                        Dim objOTApprover As New clsTnaapprover_master
                        objOTApprover._Tnamappingunkid(objDataOperation) = CInt(dtRow.Item("fromapproverunkid"))
                        OldApproverEmployeeid = objOTApprover._Tnamappingunkid

                        objOTApprover._Tnamappingunkid(objDataOperation) = CInt(dtRow.Item("toapproverunkid"))
                        NewApproverEmployeeid = objOTApprover._Tnamappingunkid
                        drrow("isExternal") = objOTApprover._IsExternalApprover

                        objOTApprover = Nothing
                        'S.SANDEEP |04-MAR-2020| -- END

                End Select

                drrow("approverunkid") = NewApproverEmployeeid
                drrow("oldapproverunkid") = OldApproverEmployeeid
                drrow("MigrationTranid") = CInt(dtRow.Item("migrationunkid"))
                mdtTran.Rows.Add(drrow)
            Next

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getMovementApprovalData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return mdtTran
    End Function


    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_movement_migration) </purpose>
    Public Function Insert(ByVal xDatabaseName As String, ByVal xmdtDatatable As DataTable, ByVal xTransactionid As Integer, _
                           ByVal xEmployeeunkid As Integer, ByVal xScreentype As Integer, ByVal xisfromapproval As Boolean, _
                           ByVal xEmployeeAsOnDate As String, ByVal xPaymentApprovalwithLeaveApproval As Boolean, _
                           ByVal xUserid As Integer, ByVal xFormname As String, ByVal xOprationType As Integer, _
                           ByVal mblnCreateADUserFromEmpMst As Boolean, _
                           Optional ByVal isFromApprovalScreen As Boolean = False, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal eStatusId As clsEmployee_Master.EmpApprovalStatus = 0) As Boolean


        'Pinkal (12-Oct-2020) -- Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.[ByVal xDatabaseName As String]

        'Pinkal (07-Dec-2019) -- Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[ByVal mblnCreateADUserFromEmpMst As Boolean]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        Try
            If isFromApprovalScreen = True AndAlso IsNothing(xmdtDatatable) = False Then
                xmdtDatatable = getMovementApprovalData(xScreentype, xEmployeeunkid, xOprationType, objDataOperation)
            End If


            If IsNothing(xmdtDatatable) = False Then
                For Each drow As DataRow In xmdtDatatable.Rows

                    If isFromApprovalScreen = False Then

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@fromapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drow("OldApproverId")))
                        objDataOperation.AddParameter("@toapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drow("NewApproverId")))
                        objDataOperation.AddParameter("@transactionid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTransactionid)
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeunkid)

                        If CInt(drow("moduletype")) = CInt(enApprovalMigration.Performance) Then
                            objDataOperation.AddParameter("@assesoroprationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drow("assesoroprationtype")))
                        Else
                            objDataOperation.AddParameter("@assesoroprationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                        End If

                        objDataOperation.AddParameter("@moduletype", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drow("moduletype")))
                        objDataOperation.AddParameter("@screentype", SqlDbType.Int, eZeeDataType.INT_SIZE, xScreentype)
                        objDataOperation.AddParameter("@isskip", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(drow("isSkip")))
                        objDataOperation.AddParameter("@isfromapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, xisfromapproval)
                        objDataOperation.AddParameter("@oprationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, xOprationType)

                        If xisfromapproval Then
                            objDataOperation.AddParameter("@isprocess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        Else
                            objDataOperation.AddParameter("@isprocess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                        End If

                        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserid)
                        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)

                        If mdtVoiddatetime = Nothing Then
                            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                        Else
                            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                        End If
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                        objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
                        objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

                        strQ = "INSERT INTO hremployee_movement_migration ( " & _
                          "  fromapproverunkid " & _
                          ", toapproverunkid " & _
                          ", transactionid " & _
                          ", employeeunkid " & _
                          ", assesoroprationtype " & _
                          ", moduletype " & _
                          ", screentype " & _
                          ", isskip " & _
                          ", isfromapproval " & _
                          ", oprationtype " & _
                          ", isprocess" & _
                          ", userunkid" & _
                          ", isvoid" & _
                          ", voiddatetime" & _
                          ", voidreason" & _
                        ") VALUES (" & _
                          "  @fromapproverunkid " & _
                          ", @toapproverunkid " & _
                          ", @transactionid " & _
                          ", @employeeunkid " & _
                          ", @assesoroprationtype " & _
                          ", @moduletype " & _
                          ", @screentype " & _
                          ", @isskip " & _
                          ", @isfromapproval " & _
                          ", @oprationtype " & _
                          ", @isprocess " & _
                          ", @userunkid " & _
                          ", @isvoid " & _
                          ", @voiddatetime " & _
                          ", @voidreason " & _
                        "); SELECT @@identity"

                        dsList = objDataOperation.ExecQuery(strQ, "List")
                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        mintMigrationunkid = dsList.Tables("List").Rows(0).Item(0)

                        mintAudituserunkid = xUserid


                        If InsertAuditTrailForMovementMigration(objDataOperation, 1, drow, xEmployeeunkid, _
                                                                xTransactionid, xScreentype, xisfromapproval, If(xisfromapproval, False, True), _
                                                                xOprationType) = False Then

                            If xDataOpr Is Nothing Then
                                objDataOperation.ReleaseTransaction(False)
                            End If
                            Return False
                        End If


                    ElseIf isFromApprovalScreen = True Then
                        objDataOperation.ClearParameters()


                        If xOprationType = CInt(clsEmployeeMovmentApproval.enOperationType.ADDED) Then
                            strQ = "Update hremployee_movement_migration set isprocess = 1, transactionid = @transactionid where " & _
                                   " migrationunkid = @migrationunkid and oprationtype = @oprationtype and  screentype =@screentype "
                        ElseIf xOprationType = CInt(clsEmployeeMovmentApproval.enOperationType.EDITED) Or xOprationType = CInt(clsEmployeeMovmentApproval.enOperationType.DELETED) Then
                            strQ = "Update hremployee_movement_migration set isprocess = 1 where " & _
                                   " migrationunkid = @migrationunkid and oprationtype = @oprationtype and transactionid = @transactionid and  screentype =@screentype "
                        End If

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@migrationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drow("MigrationTranid")))
                        objDataOperation.AddParameter("@transactionid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTransactionid)
                        objDataOperation.AddParameter("@oprationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, xOprationType)
                        objDataOperation.AddParameter("@screentype", SqlDbType.Int, eZeeDataType.INT_SIZE, xScreentype)

                        dsList = objDataOperation.ExecQuery(strQ, "List")
                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        mintAudituserunkid = xUserid
                        If InsertAuditTrailForMovementMigration(objDataOperation, 1, drow, xEmployeeunkid, _
                                                               xTransactionid, xScreentype, xisfromapproval, True, _
                                                               xOprationType) = False Then
                            If xDataOpr Is Nothing Then
                                objDataOperation.ReleaseTransaction(False)
                            End If
                            Return False
                        End If

                    End If


                    If (xisfromapproval = True AndAlso isFromApprovalScreen = True And eStatusId = clsEmployee_Master.EmpApprovalStatus.Approved) Or xisfromapproval = False Then
                        Dim Dt As DataTable = Nothing
                        If CInt(drow("moduletype")) = CInt(enApprovalMigration.Leave) And CBool(drow("isSkip")) = False Then
                            Call createDatatable(drow, Dt, enApprovalMigration.Leave, xUserid, objDataOperation)
                            If Dt IsNot Nothing AndAlso Dt.Rows.Count > 0 Then
                                Dim objApproverTran As New clsleaveapprover_Tran

                                'Pinkal (12-Oct-2020) -- Start
                                'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

                                'If objApproverTran.Migration_Insert(eZeeDate.convertDate(xEmployeeAsOnDate), Dt _
                                '                          , CInt(drow("NewApproverId")), _
                                '                          CInt(drow("approverunkid")), _
                                '                          xPaymentApprovalwithLeaveApproval, xUserid, objDataOperation) = False Then

                                If objApproverTran.Migration_Insert(xDatabaseName, eZeeDate.convertDate(xEmployeeAsOnDate), Dt _
                                                          , CInt(drow("NewApproverId")), _
                                                          CInt(drow("approverunkid")), _
                                                        xPaymentApprovalwithLeaveApproval, True, xUserid, objDataOperation) = False Then

                                    'Pinkal (12-Oct-2020) -- End

                                    'Gajanan [26-OCT-2019] -- Start    
                                    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                    'Gajanan [26-OCT-2019] -- End
                                End If
                            End If
                        End If

                        If CInt(drow("moduletype")) = CInt(enApprovalMigration.Loan) And CBool(drow("isSkip")) = False Then
                            Call createDatatable(drow, Dt, enApprovalMigration.Loan, xUserid, objDataOperation)
                            If Dt IsNot Nothing AndAlso Dt.Rows.Count > 0 Then
                                Dim objLoanApprover_tran As New clsLoanApprover_tran
                                If objLoanApprover_tran.Approver_Migration(Dt, CInt(drow("NewApproverId")), CInt(drow("approverunkid")), objDataOperation) = False Then
                                    'Gajanan [26-OCT-2019] -- Start    
                                    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                    'Gajanan [26-OCT-2019] -- End
                                End If
                            End If
                        End If

                        If CInt(drow("moduletype")) = CInt(enApprovalMigration.ReportingTo) And CBool(drow("isSkip")) = False Then
                            Dim objReportingToEmployee As New clsReportingToEmployee
							'Gajanan [24-OCT-2019] -- Start   
							'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   

                            objReportingToEmployee._UserId = xUserid
                            objReportingToEmployee._Host = mstrHostname
                            objReportingToEmployee._Ip = mstrIp
                            objReportingToEmployee._Form_Name = xFormname
                            objReportingToEmployee._Isweb = mblnIsweb
							'Gajanan [24-OCT-2019] -- End

                            'Pinkal (07-Dec-2019) -- Start
                            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                            'If objReportingToEmployee.Approver_Migration(CInt(drow("employeeunkid")), CInt(drow("OldApproverId")), CInt(drow("NewApproverId")), eZeeDate.convertDate(xEmployeeAsOnDate), objDataOperation) = False Then
                            If objReportingToEmployee.Approver_Migration(CInt(drow("employeeunkid")), CInt(drow("OldApproverId")), CInt(drow("NewApproverId")), eZeeDate.convertDate(xEmployeeAsOnDate) _
                                                                                                , mblnCreateADUserFromEmpMst, objDataOperation) = False Then
                                'Pinkal (07-Dec-2019) -- End

                            End If
                        End If

                        If CInt(drow("moduletype")) = CInt(enApprovalMigration.Performance) And CBool(drow("isSkip")) = False Then
                            Dim objAssessorTran As New clsAssessor_tran
                            Dt = objAssessorTran._DataTable.Clone
                            Call createDatatable(drow, Dt, enApprovalMigration.Performance, xUserid, objDataOperation)

                            objAssessorTran._EmployeeAsOnDate = xEmployeeAsOnDate
                            If Dt IsNot Nothing AndAlso Dt.Rows.Count > 0 Then


                                'Gajanan [26-NOV-2019] -- Start   
                                'If objAssessorTran.Perform_Migration(CInt(drow("OldApproverId")), Dt, _
                                '  CInt(drow("NewApproverId")), CBool(drow("isAssesor")), _
                                '  CInt(drow("oldapproverunkid")), xUserid, CBool(drow("isExternal")), _
                                '  xEmployeeAsOnDate, CType(drow("assesoroprationtype"), clsAssessor_tran.enOperationType), _
                                '  CType(IIf(CBool(drow("isAssesor")) = True, enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT), enAssessmentMode), _
                                '  CInt(drow("approverunkid")), xFormname, objDataOperation) = False Then

                                If objAssessorTran.Perform_Migration(CInt(drow("OldApproverId")), Dt, _
                                                                  CInt(drow("NewApproverId")), IIf(CBool(drow("isReviewer")) = True, False, True), _
                                                                  CInt(drow("oldapproverunkid")), xUserid, CBool(drow("isExternal")), _
                                                                  xEmployeeAsOnDate, CType(drow("assesoroprationtype"), clsAssessor_tran.enOperationType), _
                                                                  CType(IIf(CBool(drow("isReviewer")) = True, enAssessmentMode.REVIEWER_ASSESSMENT, enAssessmentMode.APPRAISER_ASSESSMENT), enAssessmentMode), _
                                                                  CInt(drow("approverunkid")), xFormname, objDataOperation) = False Then

                                    'Gajanan [26-NOV-2019] -- End


                                    'Gajanan [26-OCT-2019] -- Start    
                                    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                    'Gajanan [26-OCT-2019] -- End
                                End If

                            End If
                        End If


                        If CInt(drow("moduletype")) = CInt(enApprovalMigration.Claim) And CBool(drow("isSkip")) = False Then

                            Call createDatatable(drow, Dt, enApprovalMigration.Claim, xUserid, objDataOperation)

                            If Dt IsNot Nothing AndAlso Dt.Rows.Count > 0 Then
                                Dim objExAssessorTran As New clsExpenseApprover_Tran
                                If objExAssessorTran.Perform_Migration(CInt(drow("OldApproverId")), CInt(drow("NewApproverId")), _
                                                                       Dt, xUserid, CInt(drow("approverunkid")), DateTime.Now, objDataOperation) = False Then
                                    'Gajanan [26-OCT-2019] -- Start    
                                    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                    'Gajanan [26-OCT-2019] -- End
                                End If
                            End If
                        End If

                        If CInt(drow("moduletype")) = CInt(enApprovalMigration.BudgetTimesheet) And CBool(drow("isSkip")) = False Then
                            Call createDatatable(drow, Dt, enApprovalMigration.BudgetTimesheet, xUserid, objDataOperation)
                            If Dt IsNot Nothing AndAlso Dt.Rows.Count > 0 Then
                                Dim objApproverTran As New clstsapprover_Tran

                                objApproverTran._Userunkid = xUserid

                                If objApproverTran.Approver_Migration(Dt, CInt(drow("NewApproverId")), CInt(drow("approverunkid")), _
                                                                      eZeeDate.convertDate(xEmployeeAsOnDate), objDataOperation) = False Then
                                    'Gajanan [26-OCT-2019] -- Start    
                                    'Enhancement:Worked On NMB Approver Migration Enforcement Comment  
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                    'Gajanan [26-OCT-2019] -- End
                                End If
                            End If
                        End If

                        'S.SANDEEP |04-MAR-2020| -- START
                        'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
                        If CInt(drow("moduletype")) = CInt(enApprovalMigration.OTRequisition) And CBool(drow("isSkip")) = False Then
                            Call createDatatable(drow, Dt, enApprovalMigration.OTRequisition, xUserid, objDataOperation)
                            Dim objOTApprover As New clsTnaapprover_master
                            objOTApprover._AuditUserId = xUserid
                            objOTApprover._AuditDatetime = Now
                            objOTApprover._HostName = mstrHostname
                            objOTApprover._ClientIP = mstrIp
                            objOTApprover._FormName = mstrForm_Name
                            objOTApprover._IsFromWeb = mblnIsweb
                            If objOTApprover.Migration_Insert(CInt(drow("OldApproverId")), CInt(drow("NewApproverId")), Dt, xUserid, objDataOperation) = False Then
                                objDataOperation.ReleaseTransaction(False)
                                Return False
                            End If
                        End If
                        'S.SANDEEP |04-MAR-2020| -- END

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                Next
            End If


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If


            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrailForMovementMigration(ByVal objDataOperation As clsDataOperation, _
                                                         ByVal AuditType As Integer, ByVal drow As DataRow, _
                                                         ByVal xEmployeeunkid As Integer, ByVal xTransactionid As Integer, _
                                                         ByVal xScreentype As Integer, ByVal xisfromapproval As Boolean, _
                                                         ByVal xIsprocess As Boolean, _
                                                         ByVal xOprationType As Integer) As Boolean

        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "INSERT INTO athremployee_movement_migration ( " & _
            "  migrationunkid " & _
            ", fromapproverunkid " & _
            ", toapproverunkid " & _
            ", transactionid " & _
            ", employeeunkid " & _
            ", assesoroprationtype " & _
            ", moduletype " & _
            ", screentype " & _
            ", isskip " & _
            ", isfromapproval " & _
            ", oprationtype " & _
            ", isprocess " & _
            ", audittype " & _
            ", audituserunkid " & _
            ", auditdatetime " & _
            ", ip " & _
            ", hostname " & _
            ", form_name " & _
            ", isweb " & _
          ") VALUES (" & _
            "  @migrationunkid " & _
            ", @fromapproverunkid " & _
            ", @toapproverunkid " & _
            ", @transactionid " & _
            ", @employeeunkid " & _
            ", @assesoroprationtype " & _
            ", @moduletype " & _
            ", @screentype " & _
            ", @isskip " & _
            ", @isfromapproval " & _
            ", @oprationtype " & _
            ", @isprocess " & _
            ", @audittype " & _
            ", @audituserunkid " & _
            ", GetDate() " & _
            ", @ip " & _
            ", @hostname " & _
            ", @form_name " & _
            ", @isweb " & _
         "); "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@migrationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMigrationunkid)
            objDataOperation.AddParameter("@fromapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drow("OldApproverId")))
            objDataOperation.AddParameter("@toapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drow("NewApproverId")))
            objDataOperation.AddParameter("@transactionid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTransactionid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeunkid)

            If CInt(drow("moduletype")) = CInt(enApprovalMigration.Performance) Then
                objDataOperation.AddParameter("@assesoroprationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drow("assesoroprationtype")))
            Else
                objDataOperation.AddParameter("@assesoroprationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
            End If

            objDataOperation.AddParameter("@moduletype", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drow("moduletype")))
            objDataOperation.AddParameter("@screentype", SqlDbType.Int, eZeeDataType.INT_SIZE, xScreentype)
            objDataOperation.AddParameter("@isskip", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(drow("isSkip")))
            objDataOperation.AddParameter("@isfromapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, xisfromapproval)
            objDataOperation.AddParameter("@oprationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, xOprationType)
            objDataOperation.AddParameter("@isprocess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, xIsprocess)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrIp)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForEmpSkillTran", mstrModuleName)
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_movement_migration) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintMigrationunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@migrationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMigrationunkid.ToString)
            objDataOperation.AddParameter("@fromapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFromapproverunkid.ToString)
            objDataOperation.AddParameter("@toapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintToapproverunkid.ToString)
            objDataOperation.AddParameter("@transactionid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@assesoroprationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssesoroprationtype.ToString)
            objDataOperation.AddParameter("@moduletype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModuletype.ToString)
            objDataOperation.AddParameter("@screentype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScreentype.ToString)
            objDataOperation.AddParameter("@isskip", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsskip.ToString)
            objDataOperation.AddParameter("@isfromapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromapproval.ToString)
            objDataOperation.AddParameter("@oprationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOprationtype.ToString)
            objDataOperation.AddParameter("@isprocess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocess.ToString)

            strQ = "UPDATE hremployee_movement_migration SET " & _
              "  fromapproverunkid = @fromapproverunkid" & _
              ", toapproverunkid = @toapproverunkid" & _
              ", transactionid = @transactionid" & _
              ", employeeunkid = @employeeunkid" & _
              ", assesoroprationtype = @assesoroprationtype" & _
              ", moduletype = @moduletype" & _
              ", screentype = @screentype" & _
              ", isskip = @isskip" & _
              ", isfromapproval = @isfromapproval" & _
              ", oprationtype = @oprationtype" & _
              ", isprocess = @isprocess " & _
            "WHERE migrationunkid = @migrationunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployee_movement_migration) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "DELETE FROM hremployee_movement_migration " & _
            "WHERE migrationunkid = @migrationunkid "

            objDataOperation.AddParameter("@migrationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@migrationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Gajanan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    'Gajanan [26-OCT-2019] -- Start    
    'Enhancement:Worked On NMB Approver Migration Enforcement Comment 
    Public Function isMigrationInProcessExist(ByVal strdatabase As String) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  migrationunkid " & _
             "FROM " & strdatabase & "..hremployee_movement_migration " & _
             "WHERE isvoid = 0 and isprocess =0 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            End If

            Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Gajanan [26-OCT-2019] -- End

    Private Sub createDatatable(ByVal drrow As DataRow, ByRef dt As DataTable, ByVal ModuleType As enApprovalMigration, _
                               ByVal xUserid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing)


        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            Select Case ModuleType

                Case enApprovalMigration.BudgetTimesheet
                    If dt Is Nothing Then dt = New DataTable
                    dt.Columns.Add("tsapproverunkid")
                    dt.Columns.Add("employeeunkid")
                    dt.Columns.Add("tsapprovertranunkid")

                    Dim dr As DataRow = dt.NewRow()
                    Dim objbudgettsapprover As New clstsapprover_Tran
                    Dim Tranid As Integer = objbudgettsapprover.GetApproverTranIdFromEmployeeAndApprover(CInt(drrow("OldApproverid")), CInt(drrow("Employeeunkid")), objDataOperation)

                    dr("tsapproverunkid") = drrow("OldApproverid")
                    dr("employeeunkid") = drrow("Employeeunkid")
                    dr("tsapprovertranunkid") = Tranid
                    dt.Rows.Add(dr)

                Case enApprovalMigration.Leave
                    If dt Is Nothing Then dt = New DataTable
                    dt.Columns.Add("approverunkid")
                    dt.Columns.Add("leaveapproverunkid")
                    dt.Columns.Add("employeeunkid")
                    dt.Columns.Add("leaveapprovertranunkid")

                    Dim dr As DataRow = dt.NewRow()
                    Dim objLeaveapprover As New clsleaveapprover_Tran
                    Dim Tranid As Integer = objLeaveapprover.GetApproverTranIdFromEmployeeAndApprover(CInt(drrow("OldApproverid")), CInt(drrow("Employeeunkid")), objDataOperation)

                    dr("approverunkid") = drrow("OldApproverid")
                    dr("leaveapproverunkid") = drrow("approverunkid")
                    dr("employeeunkid") = drrow("Employeeunkid")
                    dr("leaveapprovertranunkid") = Tranid
                    dt.Rows.Add(dr)

                Case enApprovalMigration.Loan

                    If dt Is Nothing Then dt = New DataTable
                    dt.Columns.Add("lnapproverunkid")
                    dt.Columns.Add("employeeunkid")
                    dt.Columns.Add("lnapprovertranunkid")

                    Dim dr As DataRow = dt.NewRow()
                    Dim objapprover As New clsLoanApprover_tran
                    Dim Tranid As Integer = objapprover.GetApproverTranIdFromEmployeeAndApprover(CInt(drrow("OldApproverid")), CInt(drrow("Employeeunkid")), objDataOperation)

                    dr("lnapproverunkid") = drrow("OldApproverid")
                    dr("employeeunkid") = drrow("Employeeunkid")
                    dr("lnapprovertranunkid") = Tranid
                    dt.Rows.Add(dr)

                Case enApprovalMigration.Claim
                    If dt Is Nothing Then dt = New DataTable
                    dt.Columns.Add("crapprovertranunkid")
                    dt.Columns.Add("crapproverunkid")
                    dt.Columns.Add("employeeunkid")
                    dt.Columns.Add("isvoid")
                    dt.Columns.Add("voidreason")
                    dt.Columns.Add("voiddatetime")
                    dt.Columns.Add("voiduserunkid")
                    dt.Columns.Add("AUD")
                    dt.Columns.Add("userunkid")

                    Dim dr As DataRow = dt.NewRow()
                    Dim objapprover As New clsExpenseApprover_Tran
                    Dim Tranid As Integer = objapprover.GetApproverTranIdFromEmployeeAndApprover(CInt(drrow("OldApproverid")), CInt(drrow("Employeeunkid")), objDataOperation)

                    dr("crapprovertranunkid") = Tranid
                    dr("crapproverunkid") = 0
                    dr("employeeunkid") = drrow("Employeeunkid")
                    dr("isvoid") = False
                    dr("voidreason") = ""
                    dr("voiddatetime") = DBNull.Value
                    dr("voiduserunkid") = 0
                    dr("AUD") = ""
                    dr("userunkid") = xUserid
                    dt.Rows.Add(dr)


                Case enApprovalMigration.Performance
                    Dim objapprover As New clsAssessor_tran
                    Dim Tranid As Integer = objapprover.GetApproverTranIdFromEmployeeAndApprover(CInt(drrow("OldApproverid")), CInt(drrow("Employeeunkid")), objDataOperation)

                    Dim dr As DataRow = dt.NewRow()
                    dr("assessortranunkid") = Tranid
                    dr("employeeunkid") = drrow("Employeeunkid")
                    dt.Rows.Add(dr)

                    'S.SANDEEP |04-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
                Case enApprovalMigration.OTRequisition
                    If dt Is Nothing Then dt = New DataTable
                    dt.Columns.Add("tnamappingunkid")
                    dt.Columns.Add("employeeunkid")
                    dt.Columns.Add("tnaapprovertranunkid")
                    Dim objOTApprover As New clsTnaapprover_master

                    Dim Tranid As Integer = objOTApprover.GetApproverTranIdFromEmployeeAndApprover(CInt(drrow("OldApproverid")), CInt(drrow("Employeeunkid")), objDataOperation)

                    Dim dr As DataRow = dt.NewRow()
                    dr("tnamappingunkid") = drrow("OldApproverid")
                    dr("employeeunkid") = drrow("Employeeunkid")
                    dr("tnaapprovertranunkid") = Tranid
                    dt.Rows.Add(dr)
                    'S.SANDEEP |04-MAR-2020| -- END
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: createDatatable; Module Name: " & mstrModuleName)
        Finally
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try

    End Sub


    Public Sub isMovementInApprovalFlow(ByRef mdtMigratin As DataTable, ByVal xmintEmployeeunkid As Integer, _
                                        ByVal xmintOprationType As clsEmployeeMovmentApproval.enOperationType, _
                                        ByVal xmintScreenType As enApprovalMigrationScreenType, _
                                        ByVal xmintTransactionid As Integer, _
                                        Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
             "  fromapproverunkid " & _
             "  ,toapproverunkid " & _
             "  ,transactionid " & _
             "  ,employeeunkid " & _
             "  ,assesoroprationtype " & _
             "  ,moduletype " & _
             "  ,screentype " & _
             "  ,moduletype " & _
             "  ,isskip " & _
             "  ,isfromapproval " & _
             "  ,oprationtype " & _
             "  ,isprocess " & _
             "FROM hremployee_movement_migration " & _
             "WHERE screentype = @screentype and oprationtype = @oprationtype " & _
             "and isprocess = 0 and employeeunkid = @employeeunkid and isfromapproval = 1 "


            objDataOperation.ClearParameters()
            Select Case xmintOprationType
                Case clsEmployeeMovmentApproval.enOperationType.EDITED, clsEmployeeMovmentApproval.enOperationType.DELETED
                    strQ &= " and transactionid = @transactionid"
                    objDataOperation.AddParameter("@transactionid", SqlDbType.Int, eZeeDataType.INT_SIZE, xmintTransactionid)
            End Select

            objDataOperation.AddParameter("@screentype", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(xmintScreenType))
            objDataOperation.AddParameter("@oprationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(xmintOprationType))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xmintEmployeeunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim drow As DataRow = mdtMigratin.NewRow()

                drow.Item("isSkip") = CBool(dtRow("isskip"))
                drow.Item("OldApproverId") = CInt(dtRow("fromapproverunkid"))
                drow.Item("NewApproverId") = CInt(dtRow("toapproverunkid"))
                drow.Item("NewApproverId") = CInt(dtRow("toapproverunkid"))
                drow.Item("transactionid") = CInt(dtRow("transactionid"))
                drow.Item("employeeunkid") = CInt(dtRow("employeeunkid"))
                drow.Item("assesoroprationtype") = CInt(dtRow("assesoroprationtype"))
                drow.Item("assesoroprationtype") = CInt(dtRow("assesoroprationtype"))
                drow.Item("moduletype") = CInt(dtRow("moduletype"))
                drow.Item("screentype") = CInt(dtRow("screentype"))

                Select Case CInt(dtRow("moduletype"))
                    Case enApprovalMigration.Leave
                        Dim objLeaveapprover As New clsleaveapprover_master

                        objLeaveapprover._Approverunkid = CInt(dtRow("fromapproverunkid"))
                        drow.Item("oldapproverunkid") = objLeaveapprover._leaveapproverunkid

                        objLeaveapprover._Approverunkid = CInt(dtRow("toapproverunkid"))
                        drow.Item("approverunkid") = objLeaveapprover._leaveapproverunkid
                        drow.Item("isExternal") = objLeaveapprover._Isexternalapprover

                    Case enApprovalMigration.Claim
                        Dim objExpenseapprover As New clsExpenseApprover_Master

                        objExpenseapprover._crApproverunkid = CInt(dtRow("fromapproverunkid"))
                        drow.Item("oldapproverunkid") = objExpenseapprover._Employeeunkid

                        objExpenseapprover._crApproverunkid = CInt(dtRow("toapproverunkid"))
                        drow.Item("approverunkid") = objExpenseapprover._Employeeunkid
                        drow.Item("isExternal") = objExpenseapprover._Isexternalapprover

                    Case enApprovalMigration.Loan
                        Dim objLoanapprover As New clsLoanApprover_master

                        objLoanapprover._lnApproverunkid = CInt(dtRow("fromapproverunkid"))
                        drow.Item("oldapproverunkid") = objLoanapprover._ApproverEmpunkid

                        objLoanapprover._lnApproverunkid = CInt(dtRow("toapproverunkid"))
                        drow.Item("approverunkid") = objLoanapprover._ApproverEmpunkid
                        drow.Item("isExternal") = objLoanapprover._IsExternalApprover

                    Case enApprovalMigration.BudgetTimesheet
                        Dim objBugetTimeSheetapprover As New clstsapprover_master

                        objBugetTimeSheetapprover._Tsapproverunkid = CInt(dtRow("fromapproverunkid"))
                        drow.Item("oldapproverunkid") = objBugetTimeSheetapprover._Employeeapproverunkid

                        objBugetTimeSheetapprover._Tsapproverunkid = CInt(dtRow("toapproverunkid"))
                        drow.Item("approverunkid") = objBugetTimeSheetapprover._Employeeapproverunkid
                        drow.Item("isExternal") = objBugetTimeSheetapprover._Isexternalapprover

                    Case enApprovalMigration.Performance
                        Dim objAssesmentApprover As New clsAssessor

                        objAssesmentApprover._Assessormasterunkid = CInt(dtRow("fromapproverunkid"))
                        drow.Item("oldapproverunkid") = objAssesmentApprover._EmployeeId

                        objAssesmentApprover._Assessormasterunkid = CInt(dtRow("toapproverunkid"))
                        drow.Item("approverunkid") = objAssesmentApprover._EmployeeId
                        drow.Item("isExternal") = CBool(objAssesmentApprover._ExternalAssessorReviewer)
                        drow.Item("Isreviewer") = CBool(objAssesmentApprover._Isreviewer)


                    Case enApprovalMigration.OTRequisition

                        Dim objTnaApprover As New clsTnaapprover_master

                        objTnaApprover._Tnamappingunkid = CInt(dtRow("fromapproverunkid"))
                        drow.Item("oldapproverunkid") = objTnaApprover._Tnamappingunkid

                        objTnaApprover._Tnamappingunkid = CInt(dtRow("toapproverunkid"))
                        drow.Item("approverunkid") = objTnaApprover._Tnamappingunkid
                        drow.Item("isExternal") = objTnaApprover._IsExternalApprover

                End Select
                mdtMigratin.Rows.Add(drow)
                mdtMigratin.AcceptChanges()

            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Sub

End Class