Imports eZeeCommonLib
Imports Aruti.Data
Imports System.ComponentModel

Public Class FingerPrintDevice
    Implements IDisposable

    Private Shared ReadOnly mstrModuleName As String = "FingerPrintDevice"
    ' Dim m_DigitalPersona_Verify As DPFPDevXLib.DPFPCapture
    Dim m_SecuGenBSP_Verify As Object
    Dim m_SecuGenBSP_Error As Object
    Dim axCZKEM1 As Object
    Dim mdtfpdata As DataTable = Nothing
    Dim mdtEmpCode As DataTable = Nothing
    Private disposed As Boolean = False
    Private Component As New Component

    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        m_SecuGenBSP_Verify = Nothing
        m_SecuGenBSP_Error = Nothing
        axCZKEM1 = Nothing
        GC.SuppressFinalize(Me)
    End Sub

    Public Overloads Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                Component.Dispose()
                disposed = True
            End If
        End If
    End Sub

    Protected Overrides Sub Finalize()
        Dispose(False)
        MyBase.Finalize()
    End Sub

#Region "Communication"

    Public Function SearchDevice(ByVal mstrIPAddress As String, ByVal mstrPortNo As String, ByVal intDevice As Integer, ByVal intConncetion As Integer, _
                                             Optional ByVal mstrMachinrSrNo As String = "", Optional ByVal mstrBaudrate As String = "", Optional ByVal mstrDeviceCode As String = "") As Object
        Dim asm As Reflection.Assembly = Nothing
        Try
            Select Case intDevice
                Case 1   'DigitalPersona

                    '  m_DigitalPersona_Verify = New DPFPDevXLib.DPFPCapture

                Case 2   'SecuGen

                    asm = Reflection.Assembly.Load("SecuBSPMx.NET")
                    m_SecuGenBSP_Verify = asm.CreateInstance("SecuGen.SecuBSPPro.Windows.SecuBSPMx", False)
                    m_SecuGenBSP_Error = asm.CreateInstance("SecuGen.SecuBSPPro.Windows.BSPError", False)
                    If Not m_SecuGenBSP_Verify Is Nothing Then
                        'blnResult = enFingerPrintDevice.SecuGen
                    End If

                Case 3 'ZKSoftware

                    asm = Reflection.Assembly.Load("Interop.zkemkeeper")
                    axCZKEM1 = asm.CreateInstance("zkemkeeper.CZKEMClass", False)

                    If Not axCZKEM1 Is Nothing Then

                        Dim axCZKEM1 As New zkemkeeper.CZKEM
                        Dim bIsConnected = False
                        Dim iMachineNumber As Integer
                        Dim idwErrorCode As Integer

                        axCZKEM1.Disconnect()

                        Select Case intConncetion

                            Case 1 ' TcpIp
                                bIsConnected = axCZKEM1.Connect_Net(mstrIPAddress, mstrPortNo)
                                iMachineNumber = 1     'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.

                            Case 2   ' RS232485

                                Dim iPort As Integer
                                Dim sPort As String = mstrPortNo
                                For iPort = 1 To 9
                                    If sPort.IndexOf(iPort.ToString()) > -1 Then
                                        Exit For
                                    End If
                                Next

                                iMachineNumber = Convert.ToInt32(mstrMachinrSrNo)
                                bIsConnected = axCZKEM1.Connect_Com(iPort, iMachineNumber, Convert.ToInt32(mstrBaudrate))

                            Case 3 ' UsbClient

                                Dim sCom As String = ""
                                Dim bSearch As Boolean
                                Dim objUsb As New SearchforUSBCom
                                bSearch = objUsb.SearchforCom(sCom)

                                If bSearch = False Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Cannot find the virtual serial port that can be used."), enMsgBoxStyle.Exclamation)
                                    axCZKEM1 = Nothing
                                    asm = Nothing
                                    Return axCZKEM1
                                End If

                                Dim iPort As Integer
                                For iPort = 1 To 9
                                    If sCom.IndexOf(iPort.ToString()) > -1 Then
                                        Exit For
                                    End If
                                Next

                                iMachineNumber = Convert.ToInt32(mstrMachinrSrNo)
                                If iMachineNumber = 0 Or iMachineNumber > 255 Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "The Machine Number is invalid!"), enMsgBoxStyle.Exclamation)
                                    axCZKEM1 = Nothing
                                    asm = Nothing
                                    Return axCZKEM1
                                End If

                                bIsConnected = axCZKEM1.Connect_Com(iPort, iMachineNumber, mstrBaudrate)

                        End Select

                        If bIsConnected Then
                            Return axCZKEM1
                        Else
                            axCZKEM1.GetLastError(idwErrorCode)
                            axCZKEM1.Disconnect()
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Unable to connect the device ") & mstrDeviceCode & " [" & mstrIPAddress & "].", enMsgBoxStyle.Information)
                            axCZKEM1 = Nothing
                            asm = Nothing
                            Return Nothing
                        End If

                    End If

            End Select

        Catch ex As Runtime.InteropServices.SEHException

        Catch ex As AccessViolationException

        Catch ex As ExecutionEngineException

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SearchDevice", mstrModuleName)
        Finally
            Dispose()
        End Try
        Return Nothing
    End Function

#End Region

#Region "Enrollment Methods"

    Public Sub Enroll_DigitalPersona(ByVal intUserUnkId As Integer)
        'Try
        '    Using objfrm As New frmEnterFingerPrint
        '        If User._Object._RightToLeft = True Then
        '            objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
        '            objfrm.RightToLeftLayout = True
        '            Call Language.ctlRightToLeftlayOut(objfrm)
        '        End If

        '        objfrm.displayDialog(intUserUnkId)
        '    End Using
        'Catch ex As System.Exception
        '    Call DisplayError.Show("-1", ex.Message, "Enroll_DigitalPersona", "FingerPrintDevice")
        'End Try
    End Sub

    Public Function Enroll_SecuGen(ByVal strData As String) As String
        Dim CaptureFIRText As String = strData
        Try

            m_SecuGenBSP_Verify.DeviceID = 255 '0x00FF
            m_SecuGenBSP_Error = m_SecuGenBSP_Verify.OpenDevice()

            If Not m_SecuGenBSP_Error = 0 Then
                Throw New Exception(m_SecuGenBSP_Error.ToString)
            End If

            m_SecuGenBSP_Error = m_SecuGenBSP_Verify.Enroll(CaptureFIRText)

            If m_SecuGenBSP_Error = 0 Then
                CaptureFIRText = m_SecuGenBSP_Verify.FIRTextData
            ElseIf m_SecuGenBSP_Error = 513 Then
            Else
                Throw New Exception(m_SecuGenBSP_Error.ToString)
            End If
        Finally
            m_SecuGenBSP_Verify.CloseDevice()
        End Try

        Return CaptureFIRText
    End Function

#End Region

#Region "Verification Methods"

    Public Function Verify_DigitalPersona(ByVal pCaptureSet As Object, _
                                     ByVal pStoreSet As Object) As Boolean

        'Dim Ver As New DPFPEngXLib.DPFPVerification
        'Dim Result As Object = Nothing
        'Dim Template As DPFPShrXLib.DPFPTemplate = New DPFPShrXLib.DPFPTemplateClass
        'Template.Deserialize(pStoreSet)

        'If Template Is Nothing Then
        '    Throw New Exception(DPFPCtlXLib.DPFPEventHandlerStatusEnum.EventHandlerStatusFailure.ToString)
        'Else
        '    Result = Ver.Verify(pCaptureSet, Template)
        'End If

        'If Result Is Nothing Then
        '    Throw New Exception(DPFPCtlXLib.DPFPEventHandlerStatusEnum.EventHandlerStatusFailure.ToString)
        'Else
        '    Return Result.Verified
        'End If
    End Function

    Public Function Verify_SecuGen(ByVal pCaptureSet As String, _
                                     ByVal pStoreSet As String) As Boolean

        If pStoreSet.Length = 0 Or pCaptureSet.Length = 0 Then Return False

        m_SecuGenBSP_Error = m_SecuGenBSP_Verify.VerifyMatch(pCaptureSet, pStoreSet)

        If (m_SecuGenBSP_Error = 0) Then
            Return m_SecuGenBSP_Verify.IsMatched
        Else
            Throw New Exception(m_SecuGenBSP_Error.ToString)
        End If
    End Function


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    Public Sub Verify(ByVal DeviceId As Integer, ByVal intCommunicationTypeID As Integer, ByVal pCaptureSet As Object, ByRef intOutEmpUnkid As Integer, ByRef intOutShiftunkid As Integer, ByRef strOutName As String)

        'Dim DeviceId As enFingerPrintDevice = CType(ConfigParameter._Object._DeviceId, enFingerPrintDevice)
        GetemployeeData(intCommunicationTypeID)

        Select Case DeviceId
            Case 1  ' DigitalPersona
            '    For Each dtRow As DataRow In mdtfpdata.Rows
            '        If Verify_DigitalPersona(pCaptureSet, dtRow.Item("fingerprint_data")) Then
            '            intOutEmpUnkid = dtRow.Item("employeeunkid")
            '            strOutName = dtRow.Item("displayname")
            '            Exit For
            '        End If
            '    Next

            Case 2 ' SecuGen
            '    For Each dtRow As DataRow In mdtEmpCode.Rows
            '        Dim enc As New System.Text.UTF8Encoding()
            '        If Verify_SecuGen(pCaptureSet.ToString, enc.GetString(dtRow.Item("fingerprint_data"))) Then
            '            intOutEmpUnkid = dtRow.Item("employeeunkid")
            '            strOutName = dtRow.Item("displayname")
            '            Exit For
            '        End If
            '    Next

            Case 3  'ZKSoftware
                For Each dtRow As DataRow In mdtEmpCode.Rows
                    If dtRow.Item("employeeunkid") = pCaptureSet.ToString Then
                        intOutEmpUnkid = dtRow.Item("employeeunkid")
                        intOutShiftunkid = dtRow.Item("shiftunkid")
                        strOutName = dtRow.Item("displayname")
                        Exit For
                    End If
                Next

            Case Else
                intOutEmpUnkid = -1
                strOutName = ""
        End Select
    End Sub

    'Pinkal (20-Jan-2012) -- End


#End Region

#Region "Captureing Methods"

    Public Function Capture_SecuGen() As String
        Dim CaptureFIRText As String = String.Empty
        Try

            m_SecuGenBSP_Verify.DeviceID = 255 '0x00FF
            m_SecuGenBSP_Error = m_SecuGenBSP_Verify.OpenDevice()
            m_SecuGenBSP_Verify.CaptureWindowOption.WindowStyle = 1

            If Not m_SecuGenBSP_Error = 0 Then
                Throw New Exception(m_SecuGenBSP_Error.ToString)
            End If
Line_1:
            m_SecuGenBSP_Error = m_SecuGenBSP_Verify.Capture(1)

            If m_SecuGenBSP_Error = 0 Then
                CaptureFIRText = m_SecuGenBSP_Verify.FIRTextData
            ElseIf m_SecuGenBSP_Error = 515 Then
                GoTo Line_1
            Else
                Throw New Exception(m_SecuGenBSP_Error.ToString)
            End If

        Finally
            m_SecuGenBSP_Verify.CloseDevice()
        End Try

        Return CaptureFIRText

    End Function

#End Region

#Region "Private Methods"

    Private Sub GetemployeeData(ByVal intCommunicationTypeID As Integer)

        If intCommunicationTypeID = 1 Then 'FingerPrint

            Dim objFingurePrint As New clsEmployee_Finger
            mdtfpdata = objFingurePrint.getEmployee_FingerData()
            Dim objEmp As New clsEmployee_Master
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'mdtEmpCode = objEmp.GetList("EmpCode").Tables(0)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    mdtEmpCode = objEmp.GetList("EmpCode", , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)).Tables(0)
            'Else
            '    mdtEmpCode = objEmp.GetList("EmpCode").Tables(0)
            'End If

            mdtEmpCode = objEmp.GetList(FinancialYear._Object._DatabaseName, _
                                        User._Object._Userunkid, _
                                        FinancialYear._Object._YearUnkid, _
                                        Company._Object._Companyunkid, _
                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                        ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                        "EmpCode", ConfigParameter._Object._ShowFirstAppointmentDate).Tables(0)
            'S.SANDEEP [04 JUN 2015] -- END


            'Sohail (06 Jan 2012) -- End
            objFingurePrint = Nothing

        End If

        'Pinkal (20-Jan-2012) -- End

    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Unable to connect the device")
            Language.setMessage(mstrModuleName, 2, "Cannot find the virtual serial port that can be used.")
            Language.setMessage(mstrModuleName, 3, "The Machine Number is invalid!")

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
