﻿#Region " Imports "
Imports eZeeCommonLib
#End Region

Public Class clsSwipeCardData
    Private ReadOnly mstrModuleName As String = "clsSwipeCardData"
    Dim objDataOperation As clsDataOperation

#Region " Property Variables "
    Private mintSwipeCardUnkId As Integer = -1
    Private mintEmployeeUnkId As Integer = -1
    Private mstrCardData As String = ""
    Private mstrEmployeeName As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _CardData() As String
        Get
            Return mstrCardData
        End Get
        Set(ByVal value As String)
            mstrCardData = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _EmployeeUnkId() As Integer
        Get
            Return mintEmployeeUnkId
        End Get
        Set(ByVal value As Integer)
            mintEmployeeUnkId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _EmployeeName() As String
        Get
            Return mstrEmployeeName
        End Get
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property
#End Region

    ''' <summary>
    ''' For assign database tabel (cardScanData) value into property.
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Sub VerifyData(ByVal strData As String, ByRef intEmpId As Integer, ByRef intshiftId As Integer, Optional ByRef EmployeeName As String = Nothing)
        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'strQ = "SELECT " & _
            '           "  swipecardunkid " & _
            '           ", swipedata " & _
            '           ", hremployee_master.employeeunkid " & _
            '           ", shiftunkid " & _
            '       "FROM hrcardscandata " & _
            '       "JOIN hremployee_master ON hremployee_master.employeeunkid=hrcardscandata.employeeunkid " & _
            '       " WHERE swipedata=@strdata"

            'Pinkal (03-Jan-2011) -- Start

            strQ = "SELECT " & _
                       "  swipecardunkid " & _
                       ", swipedata " & _
                       ", hremployee_master.employeeunkid " & _
                       ", shiftunkid " & _
                       ", displayname " & _
                   "FROM hrcardscandata " & _
                   "JOIN hremployee_master ON hremployee_master.employeeunkid=hrcardscandata.employeeunkid " & _
                   " WHERE swipedata=@strdata"

            'Pinkal (03-Jan-2011) -- End

            objDataOperation.AddParameter("@strdata", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strData)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtrow As DataRow In dsList.Tables(0).Rows
                intEmpId = dtrow.Item("employeeunkid")
                intshiftId = dtrow.Item("shiftunkid")
                EmployeeName = dtrow.Item("displayname")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "VerifyData", mstrModuleName)
        Finally
            exForce = Nothing
            dsList = Nothing
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' For insert given property value in database table (cardScanData).
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> </purpose>
    Public Function Insert() As Boolean
        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            Call Delete(mintEmployeeUnkId)
            objDataOperation = New clsDataOperation

            objDataOperation.AddParameter("@cardData", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrCardData)
            objDataOperation.AddParameter("@employeeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkId)

            strQ = "INSERT INTO hrcardScanData ( " & _
                       "employeeunkid " & _
                        ", swipedata " & _
                    ") VALUES (" & _
                        "  @employeeid" & _
                        ", @carddata " & _
                    ") ; SELECT @@identity "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintSwipeCardUnkId = dsList.Tables(0).Rows(0).Item(0)

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "Insert", mstrModuleName)
            Return False
        Finally
            objDataOperation.ReleaseTransaction(False)
            exForce = Nothing
            dsList = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Delete(ByVal intUserUnkId As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId)
            strQ = "DELETE FROM hrcardscandata " & _
                        "WHERE employeeunkid = @empunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Delete", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    Public Function IsUsed(ByVal strCardData As String, ByVal intUnkId As Integer) As Boolean
        Dim iCnt As Integer = 1
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            strQ &= "SELECT employeeunkid " & _
                        "FROM hrcardscandata " & _
                    "WHERE swipedata = @swipedata " & _
                    "AND employeeunkid <> @empunkid "

            objDataOperation.AddParameter("@swipedata", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strCardData)
            objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkId)

            iCnt = objDataOperation.RecordCount(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return (iCnt > 0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsUsed", mstrModuleName)
            Return True
        Finally
            exForce = Nothing
            iCnt = Nothing
            If objDataOperation IsNot Nothing Then
                objDataOperation.Dispose()
                objDataOperation = Nothing
            End If
        End Try
    End Function

    'Pinkal (03-Jan-2011) -- Start

    Public Function GetCardNo(ByVal intEmployeeid As Integer) As String
        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation = New clsDataOperation

            strQ = "SELECT " & _
                       "  swipecardunkid " & _
                       ", swipedata " & _
                   "FROM hrcardscandata " & _
                   " WHERE employeeunkid=@employeeunkid"

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList.Tables(0).Rows.Count > 0 Then

                Return dsList.Tables(0).Rows(0)("swipedata").ToString

            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCardNo", mstrModuleName)
        End Try
        Return Nothing
    End Function

    'Pinkal (03-Jan-2011) -- End

End Class
