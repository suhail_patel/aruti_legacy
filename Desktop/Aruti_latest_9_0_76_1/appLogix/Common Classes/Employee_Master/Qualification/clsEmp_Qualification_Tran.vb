﻿'************************************************************************************************************************************
'Class Name : clsEmp_Qualification_Tran.vb
'Purpose    :
'Date       :10/11/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.clsCommon_Master
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsEmp_Qualification_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsEmp_Qualification_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintQualificationtranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintQualificationgroupunkid As Integer
    Private mintQualificationunkid As Integer
    Private mdtTransaction_Date As Date
    Private mstrReference_No As String = String.Empty
    Private mdtAward_Start_Date As Date
    Private mdtAward_End_Date As Date
    Private mintInstituteunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty

    'S.SANDEEP [ 12 OCT 2011 ] -- START
    Private mintLoginemployeeunkid As Integer = -1
    Private mintVoidloginemployeeunkid As Integer = -1
    'S.SANDEEP [ 12 OCT 2011 ] -- END 
'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes
    Private mintResultunkid As Integer
    Private mdblGPAcode As Decimal
    'Pinkal (12-Oct-2011) -- End


    'Pinkal (25-APR-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrOtherQualificationGrp As String = ""
    Private mstrOtherQualification As String = ""
    Private mstrOtherResultCode As String = ""
    'Pinkal (25-APR-2012) -- End


    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    'S.SANDEEP [ 13 AUG 2012 ] -- END

    'S.SANDEEP [23 JUL 2016] -- START
    'CCK- OTHER QUALIFICATION CHANGE
    Private mstrOther_institute As String = String.Empty
    'S.SANDEEP [23 JUL 2016] -- END



    'Pinkal (24-Sep-2020) -- Start
    'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
    Private mstrCertificateNo As String = ""
    Private mblnIsHighestQualfication As Boolean = False
    'Pinkal (24-Sep-2020) -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set qualificationtranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Qualificationtranunkid() As Integer
        Get
            Return mintQualificationtranunkid
        End Get
        Set(ByVal value As Integer)
            mintQualificationtranunkid = value
            'Call GetData()
            Call GetData(objDataOperation) 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set qualificationgroupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Qualificationgroupunkid() As Integer
        Get
            Return mintQualificationgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintQualificationgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set qualificationunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Qualificationunkid() As Integer
        Get
            Return mintQualificationunkid
        End Get
        Set(ByVal value As Integer)
            mintQualificationunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transaction_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Transaction_Date() As Date
        Get
            Return mdtTransaction_Date
        End Get
        Set(ByVal value As Date)
            mdtTransaction_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reference_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reference_No() As String
        Get
            Return mstrReference_No
        End Get
        Set(ByVal value As String)
            mstrReference_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set award_start_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Award_Start_Date() As Date
        Get
            Return mdtAward_Start_Date
        End Get
        Set(ByVal value As Date)
            mdtAward_Start_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set award_end_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Award_End_Date() As Date
        Get
            Return mdtAward_End_Date
        End Get
        Set(ByVal value As Date)
            mdtAward_End_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set instituteunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Instituteunkid() As Integer
        Get
            Return mintInstituteunkid
        End Get
        Set(ByVal value As Integer)
            mintInstituteunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'S.SANDEEP [ 12 OCT 2011 ] -- START
    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = value
        End Set
    End Property
    'S.SANDEEP [ 12 OCT 2011 ] -- END 

'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set resulunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Resultunkid() As Integer
        Get
            Return mintResultunkid
        End Get
        Set(ByVal value As Integer)
            mintResultunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set GPAcode
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _GPAcode() As Decimal
        Get
            Return mdblGPAcode
        End Get
        Set(ByVal value As Decimal)
            mdblGPAcode = value
        End Set
    End Property

    'Pinkal (12-Oct-2011) -- End


    'Pinkal (25-APR-2012) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set other_Qualificationgrp
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Other_QualificationGrp() As String
        Get
            Return mstrOtherQualificationGrp
        End Get
        Set(ByVal value As String)
            mstrOtherQualificationGrp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set other_Qualification
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Other_Qualification() As String
        Get
            Return mstrOtherQualification
        End Get
        Set(ByVal value As String)
            mstrOtherQualification = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set other_ResultCode
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _other_ResultCode() As String
        Get
            Return mstrOtherResultCode
        End Get
        Set(ByVal value As String)
            mstrOtherResultCode = value
        End Set
    End Property

    'Pinkal (25-APR-2012) -- End


    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

    'S.SANDEEP [23 JUL 2016] -- START
    'CCK- OTHER QUALIFICATION CHANGE
    Public Property _Other_institute() As String
        Get
            Return mstrOther_institute
        End Get
        Set(ByVal value As String)
            mstrOther_institute = value
        End Set
    End Property
    'S.SANDEEP [23 JUL 2016] -- END
    

    'Pinkal (24-Sep-2020) -- Start
    'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.

    Public Property _CertificateNo() As String
        Get
            Return mstrCertificateNo
        End Get
        Set(ByVal value As String)
            mstrCertificateNo = value
        End Set
    End Property

    Public Property _IsHighestQualification() As Boolean
        Get
            Return mblnIsHighestQualfication
        End Get
        Set(ByVal value As Boolean)
            mblnIsHighestQualfication = value
        End Set
    End Property

    'Pinkal (24-Sep-2020) -- End


#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOperation As clsDataOperation = Nothing) 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
        'Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sandeep [ 16 Oct 2010 ] -- Start
        'objDataOperation = New clsDataOperation


        'S.SANDEEP [ 27 APRIL 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'Dim objDataOperation As New clsDataOperation
        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        'S.SANDEEP [ 27 APRIL 2012 ] -- END



        'Sandeep [ 16 Oct 2010 ] -- End 

        Try


            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '  "  qualificationtranunkid " & _
            '  ", employeeunkid " & _
            '  ", qualificationgroupunkid " & _
            '  ", qualificationunkid " & _
            '  ", transaction_date " & _
            '  ", reference_no " & _
            '  ", award_start_date " & _
            '  ", award_end_date " & _
            '  ", instituteunkid " & _
            '  ", remark " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiddatetime " & _
            '  ", voiduserunkid " & _
            '  ", voidreason " & _
            '  ", ISNULL(loginemployeeunkid,-1) AS loginemployeeunkid " & _
            '  ", ISNULL(voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
            ' ", ISNULL(resultunkid,0) resultunkid " & _
            ' ", ISNULL(gpacode,0)  gpacode " & _
            ' "FROM hremp_qualification_tran " & _
            ' "WHERE qualificationtranunkid = @qualificationtranunkid "

            strQ = "SELECT " & _
              "  qualificationtranunkid " & _
              ", employeeunkid " & _
              ", qualificationgroupunkid " & _
              ", qualificationunkid " & _
              ", transaction_date " & _
              ", reference_no " & _
              ", award_start_date " & _
              ", award_end_date " & _
              ", instituteunkid " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
                          ", ISNULL(loginemployeeunkid,-1) AS loginemployeeunkid " & _
                          ", ISNULL(voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
             ", ISNULL(resultunkid,0) resultunkid " & _
             ", ISNULL(gpacode,0)  gpacode " & _
            ", ISNULL(other_qualificationgrp,'')  other_qualificationgrp " & _
            ", ISNULL(other_qualification,'')  other_qualification " & _
            ", ISNULL(other_resultcode,'')  other_resultcode " & _
              ", ISNULL(other_institute,'')  other_institute " & _
              ", ISNULL(certificateno,'')  certificateno " & _
              ", ISNULL(ishighestqualification,0)  ishighestqualification " & _
              " FROM hremp_qualification_tran " & _
              " WHERE qualificationtranunkid = @qualificationtranunkid "


            'Pinkal (24-Sep-2020) -- Enhancement NMB - Changes for Recruitment Enhancement required by NMB.[  ", ISNULL(certificateno,'')  certificateno  ", ISNULL(ishighestqualification,0)  ishighestqualification " & _]


            'Pinkal (25-APR-2012) -- End


            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.ClearParameters()
            'S.SANDEEP [ 27 APRIL 2012 ] -- END
            objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationtranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintQualificationtranunkid = CInt(dtRow.Item("qualificationtranunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintQualificationgroupunkid = CInt(dtRow.Item("qualificationgroupunkid"))
                mintQualificationunkid = CInt(dtRow.Item("qualificationunkid"))
                mdtTransaction_Date = dtRow.Item("transaction_date")
                mstrReference_No = dtRow.Item("reference_no").ToString

                If IsDBNull(dtRow.Item("award_start_date")) Then
                    mdtAward_Start_Date = Nothing
                Else
                    mdtAward_Start_Date = dtRow.Item("award_start_date")
                End If

                If IsDBNull(dtRow.Item("award_end_date")) Then
                    mdtAward_End_Date = Nothing
                Else
                    mdtAward_End_Date = dtRow.Item("award_end_date")
                End If

                mintInstituteunkid = CInt(dtRow.Item("instituteunkid"))
                mstrRemark = dtRow.Item("remark").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString

                'S.SANDEEP [ 12 OCT 2011 ] -- START
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mintVoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                'S.SANDEEP [ 12 OCT 2011 ] -- END 
              
               'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes
                mintResultunkid = CInt(dtRow.Item("resultunkid").ToString())
                mdblGPAcode = CDec(dtRow.Item("gpacode").ToString())
                'Pinkal (12-Oct-2011) -- End



                'Pinkal (25-APR-2012) -- Start
                'Enhancement : TRA Changes
                mstrOtherQualificationGrp = dtRow.Item("other_qualificationgrp").ToString()
                mstrOtherQualification = dtRow.Item("other_qualification").ToString()
                mstrOtherResultCode = dtRow.Item("other_resultcode").ToString()
                'Pinkal (25-APR-2012) -- End

                'S.SANDEEP [23 JUL 2016] -- START
                'CCK- OTHER QUALIFICATION CHANGE
                mstrOther_institute = dtRow.Item("Other_institute")
                'S.SANDEEP [23 JUL 2016] -- END


                'Pinkal (24-Sep-2020) -- Start
                'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
                mstrCertificateNo = dtRow.Item("certificateno")
                mblnIsHighestQualfication = CBool(dtRow.Item("ishighestqualification").ToString())
                'Pinkal (24-Sep-2020) -- End


                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal strFilerString As String = "", _
                            Optional ByVal IsUsedAsMSS As Boolean = True, _
                            Optional ByVal mblnAddApprovalCondition As Boolean = True) As DataSet

        'S.SANDEEP [20-JUN-2018] -- 'Enhancement - Implementing Employee Approver Flow For NMB .[Optional ByVal mblnAddApprovalCondition As Boolean = True]

        'Pinkal (28-Dec-2015) -- Start    'Enhancement - Working on Changes in SS for Employee Master. [Optional ByVal IsUsedAsMSS As Boolean = True] 

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)

            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            'If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , mblnAddApprovalCondition)
            'S.SANDEEP [20-JUN-2018] -- End

            'Pinkal (28-Dec-2015) -- End

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation

                StrQ = "SELECT " & _
                       "     ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                       "    ,CASE WHEN hremp_qualification_tran.qualificationgroupunkid > 0 THEN ISNULL(cfcommon_master.name, '') ELSE ISNULL(hremp_qualification_tran.other_qualificationgrp, '') END AS QGrp " & _
                       "    ,CASE WHEN hremp_qualification_tran.qualificationunkid > 0 THEN ISNULL(hrqualification_master.qualificationname, '') ELSE ISNULL(hremp_qualification_tran.other_qualification, '') END AS Qualify " & _
                       "    ,CASE WHEN ISNULL(hremp_qualification_tran.resultunkid, 0) > 0 THEN ISNULL(hrresult_master.resultcode, '') ELSE ISNULL(hremp_qualification_tran.other_resultcode, '') END AS resultcode " & _
                       "    ,CONVERT(CHAR(8), hremp_qualification_tran.transaction_date, 112) AS Date " & _
                       "    ,CONVERT(CHAR(8), hremp_qualification_tran.award_start_date, 112) AS StDate " & _
                       "    ,CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112) AS EnDate " & _
                       "    ,ISNULL(hremp_qualification_tran.reference_no, '') AS RefNo " & _
                       "    ,CASE WHEN ISNULL(hremp_qualification_tran.instituteunkid, 0) > 0 THEN ISNULL(hrinstitute_master.institute_name, '') ELSE ISNULL(hremp_qualification_tran.other_institute,'') END AS Institute " & _
                       "    ,hremployee_master.employeeunkid AS EmpId " & _
                       "    ,ISNULL(cfcommon_master.masterunkid,0) AS QGrpId " & _
                       "    ,ISNULL(hrqualification_master.qualificationunkid,0) AS QualifyId " & _
                       "    ,hremp_qualification_tran.qualificationtranunkid AS QulifyTranId " & _
                       "    ,hremp_qualification_tran.instituteunkid As InstituteId " & _
                       "    ,ISNULL(loginemployeeunkid,-1) AS loginemployeeunkid " & _
                       "    ,ISNULL(voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
                       "    ,ISNULL(hremp_qualification_tran.resultunkid,0) As resultunkid " & _
                       "    ,hremp_qualification_tran.gpacode As gpacode " & _
                       "    ,ISNULL(hremp_qualification_tran.other_qualificationgrp,'') As other_qualificationgrp " & _
                       "    ,ISNULL(hremp_qualification_tran.other_qualification,'') As other_qualification " & _
                       "    ,ISNULL(hremp_qualification_tran.other_resultcode,'') As other_resultcode  " & _
                       "    ,ISNULL(hremp_qualification_tran.other_institute,'') As other_institute  " & _
                       "    ,0 AS operationtypeid " & _
                       "    ,'' AS OperationType " & _
                       "    , ISNULL(hremp_qualification_tran.certificateno,'')  certificateno " & _
                       "    , ISNULL(hremp_qualification_tran.ishighestqualification,0)  ishighestqualification " & _
                       "    , ISNULL(hremp_qualification_tran.remark, '')  remark " & _
                       "    , '' AS AUD " & _
                       "    , '' AS GUID " & _
                       "    , ISNULL(cfcommon_master.qlevel, -1) AS qlevel " & _
                       "    , ISNULL(hrresult_master.result_level,0) AS result_level " & _
                       "    , YEAR(hremp_qualification_tran.award_start_date) award_start_Year " & _
                       "    , YEAR(hremp_qualification_tran.award_end_date) award_end_Year " & _
                       "FROM hremp_qualification_tran " & _
                       "    LEFT JOIN hrinstitute_master ON hremp_qualification_tran.instituteunkid = hrinstitute_master.instituteunkid " & _
                       "    LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                       "    LEFT JOIN hremployee_master ON hremp_qualification_tran.employeeunkid = hremployee_master.employeeunkid " & _
                       "    LEFT JOIN hrresult_master ON hremp_qualification_tran.resultunkid = hrresult_master.resultunkid " & _
                       "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.QUALIFICATION_COURSE_GROUP


                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                'Pinkal (20-Oct-2020) -- Enhancement Recruitment NMB -   Working on Recruitement Changes for NMB. [  "    , ISNULL(cfcommon_master.qlevel, -1) AS qlevel , ISNULL(hrresult_master.result_level,0) AS result_level  1 , YEAR(hremp_qualification_tran.award_start_date) award_start_Year  , YEAR(hremp_qualification_tran.award_end_date) award_end_Year " & _ " & _]

                'Sohail (25 Sep 2020) - [AUD, GUID]
                'Pinkal (24-Sep-2020) -- Enhancement NMB - Changes for Recruitment Enhancement required by NMB.[ " ISNULL(hremp_qualification_tran.certificateno,'')  certificateno  , ISNULL(hremp_qualification_tran.ishighestqualification,0)  ishighestqualification " & _


                'S.SANDEEP [23 JUL 2016] -- START {other_institute} -- END

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                '"    ,0 AS operationtypeid " --- ADDED
                '"    ,'' AS OperationType " --- ADDED
                'Gajanan [17-DEC-2018] -- End

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

               

                StrQ &= "WHERE ISNULL(hremp_qualification_tran.isvoid, 0) = 0 "

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If strFilerString.Trim.Length > 0 Then
                    StrQ &= " AND " & strFilerString
                End If

                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    'Public Function GetList(ByVal strTableName As String, _
    '                        Optional ByVal strIncludeInactiveEmployee As String = "", _
    '                        Optional ByVal strEmployeeAsOnDate As String = "", _
    '                        Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet
    '    'Public Function GetList(ByVal strTableName As String) As DataSet

    '    'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        'strQ = "SELECT " & _
    '        '            "    ISNULL(hremployee_master.firstname, '') + ' ' " & _
    '        '            "    + ISNULL(hremployee_master.othername, '') + ' ' " & _
    '        '            "    + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '        '            "   ,ISNULL(cfcommon_master.name, '') AS QGrp " & _
    '        '            "   ,ISNULL(hrqualification_master.qualificationname, '') AS Qualify " & _
    '        '            "   ,CONVERT(CHAR(8), hremp_qualification_tran.transaction_date, 112) AS Date " & _
    '        '            "   ,CONVERT(CHAR(8), hremp_qualification_tran.award_start_date, 112) AS StDate " & _
    '        '            "   ,CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112) AS EnDate " & _
    '        '            "   ,ISNULL(hremp_qualification_tran.reference_no, '') AS RefNo " & _
    '        '            "   ,ISNULL(hrinstitute_master.institute_name, '') AS Institute " & _
    '        '            "   ,hremployee_master.employeeunkid AS EmpId " & _
    '        '            "   ,cfcommon_master.masterunkid AS QGrpId " & _
    '        '            "   ,hrqualification_master.qualificationunkid AS QualifyId " & _
    '        '            "   ,hremp_qualification_tran.qualificationtranunkid AS QulifyTranId " & _
    '        '            "   ,hrinstitute_master.instituteunkid As InstituteId " & _
    '        '            "FROM hremp_qualification_tran " & _
    '        '            "    LEFT JOIN hrinstitute_master ON hremp_qualification_tran.instituteunkid = hrinstitute_master.instituteunkid " & _
    '        '            "    LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
    '        '            "    LEFT JOIN hremployee_master ON hremp_qualification_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '            "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.QUALIFICATION_COURSE_GROUP & _
    '        '            "WHERE ISNULL(hremp_qualification_tran.isvoid, 0) = 0 "


    '        'S.SANDEEP [ 12 OCT 2011 ] -- START
    '        'strQ = "SELECT " & _
    '        '                        "    ISNULL(hremployee_master.firstname, '') + ' ' " & _
    '        '                        "    + ISNULL(hremployee_master.othername, '') + ' ' " & _
    '        '                        "    + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '        '                        "   ,ISNULL(cfcommon_master.name, '') AS QGrp " & _
    '        '                        "   ,ISNULL(hrqualification_master.qualificationname, '') AS Qualify " & _
    '        '                        "   ,CONVERT(CHAR(8), hremp_qualification_tran.transaction_date, 112) AS Date " & _
    '        '                        "   ,CONVERT(CHAR(8), hremp_qualification_tran.award_start_date, 112) AS StDate " & _
    '        '                        "   ,CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112) AS EnDate " & _
    '        '                        "   ,ISNULL(hremp_qualification_tran.reference_no, '') AS RefNo " & _
    '        '                        "   ,ISNULL(hrinstitute_master.institute_name, '') AS Institute " & _
    '        '                        "   ,hremployee_master.employeeunkid AS EmpId " & _
    '        '                        "   ,cfcommon_master.masterunkid AS QGrpId " & _
    '        '                        "   ,hrqualification_master.qualificationunkid AS QualifyId " & _
    '        '                        "   ,hremp_qualification_tran.qualificationtranunkid AS QulifyTranId " & _
    '        '                        "   ,hrinstitute_master.instituteunkid As InstituteId " & _
    '        '                        "FROM hremp_qualification_tran " & _
    '        '                        "    LEFT JOIN hrinstitute_master ON hremp_qualification_tran.instituteunkid = hrinstitute_master.instituteunkid " & _
    '        '                        "    LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
    '        '                        "    LEFT JOIN hremployee_master ON hremp_qualification_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '                        "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.QUALIFICATION_COURSE_GROUP & _
    '        '                        "WHERE ISNULL(hremp_qualification_tran.isvoid, 0) = 0 "

    '        strQ = "SELECT " & _
    '                    "    ISNULL(hremployee_master.firstname, '') + ' ' " & _
    '                    "    + ISNULL(hremployee_master.othername, '') + ' ' " & _
    '                    "    + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '                    "   , CASE WHEN ISNULL(cfcommon_master.masterunkid, 0) > 0 then  " & _
    '                    "                       ISNULL(cfcommon_master.name, '') " & _
    '                    "               ELSE " & _
    '                    "                   ISNULL(hremp_qualification_tran.other_qualificationgrp, '') " & _
    '                    "     END AS QGrp " & _
    '                    "   ,CASE WHEN ISNULL(hrqualification_master.qualificationunkid, 0) > 0 then " & _
    '                    "                       ISNULL(hrqualification_master.qualificationname, '') " & _
    '                    "              ELSE " & _
    '                    "                       ISNULL(hremp_qualification_tran.other_qualification, '') " & _
    '                    "    END AS Qualify " & _
    '                    "   ,CONVERT(CHAR(8), hremp_qualification_tran.transaction_date, 112) AS Date " & _
    '                    "   ,CONVERT(CHAR(8), hremp_qualification_tran.award_start_date, 112) AS StDate " & _
    '                    "   ,CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112) AS EnDate " & _
    '                    "   ,ISNULL(hremp_qualification_tran.reference_no, '') AS RefNo " & _
    '                    "   ,ISNULL(hrinstitute_master.institute_name, '') AS Institute " & _
    '                    "   ,hremployee_master.employeeunkid AS EmpId " & _
    '                    "   ,ISNULL(cfcommon_master.masterunkid,0) AS QGrpId " & _
    '                    "   ,ISNULL(hrqualification_master.qualificationunkid,0) AS QualifyId " & _
    '                    "   ,hremp_qualification_tran.qualificationtranunkid AS QulifyTranId " & _
    '                    "   ,hrinstitute_master.instituteunkid As InstituteId "

    '        'Anjan (21 Nov 2011)-Start
    '        'ENHANCEMENT : TRA COMMENTS
    '        strQ &= ",ISNULL(hremployee_master.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                 ",ISNULL(hremployee_master.unitgroupunkid,0) AS unitgroupunkid " & _
    '                 ",ISNULL(hremployee_master.teamunkid,0) AS teamunkid " & _
    '                 ",ISNULL(hremployee_master.stationunkid,0) AS stationunkid " & _
    '                 ",ISNULL(hremployee_master.deptgroupunkid,0) AS deptgroupunkid " & _
    '                 ",ISNULL(hremployee_master.departmentunkid,0) AS departmentunkid " & _
    '                 ",ISNULL(hremployee_master.sectionunkid,0) AS sectionunkid " & _
    '                 ",ISNULL(hremployee_master.unitunkid,0) AS unitunkid " & _
    '                 ",ISNULL(hremployee_master.jobunkid,0) AS jobunkid " & _
    '                 ",ISNULL(hremployee_master.classgroupunkid,0) AS classgroupunkid " & _
    '                 ",ISNULL(hremployee_master.classunkid,0) AS classunkid " & _
    '                 ",ISNULL(hremployee_master.jobgroupunkid,0) AS jobgroupunkid " & _
    '                 ",ISNULL(hremployee_master.gradegroupunkid,0) AS gradegroupunkid " & _
    '                 ",ISNULL(hremployee_master.gradeunkid,0) AS gradeunkid " & _
    '                 ",ISNULL(hremployee_master.gradelevelunkid,0) AS gradelevelunkid " & _
    '                 ",ISNULL(loginemployeeunkid,-1) AS loginemployeeunkid " & _
    '                 ",ISNULL(voidloginemployeeunkid,-1) AS voidloginemployeeunkid " & _
    '                 ",ISNULL(hremp_qualification_tran.resultunkid,0) As resultunkid " & _
    '                 ",hremp_qualification_tran.gpacode As gpacode " & _
    '                 ",ISNULL(hremp_qualification_tran.other_qualificationgrp,'') As other_qualificationgrp " & _
    '                 ",ISNULL(hremp_qualification_tran.other_qualification,'') As other_qualification " & _
    '                 ",ISNULL(hremp_qualification_tran.other_resultcode,'') As other_resultcode  "
    '        'Anjan (21 Nov 2011)-End 


    '        strQ &= " FROM hremp_qualification_tran " & _
    '                    "    LEFT JOIN hrinstitute_master ON hremp_qualification_tran.instituteunkid = hrinstitute_master.instituteunkid " & _
    '                    "    LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
    '                    "    LEFT JOIN hremployee_master ON hremp_qualification_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.QUALIFICATION_COURSE_GROUP & _
    '                    "WHERE ISNULL(hremp_qualification_tran.isvoid, 0) = 0 "
    '        'S.SANDEEP [ 12 OCT 2011 ] -- END 



    '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        ''S.SANDEEP [ 29 JUNE 2011 ] -- START
    '        ''ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    '        'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '        '    'Sohail (06 Jan 2012) -- Start
    '        '    'TRA - ENHANCEMENT
    '        '    'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '        '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

    '        '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    'Sohail (06 Jan 2012) -- End
    '        'End If
    '        ''S.SANDEEP [ 29 JUNE 2011 ] -- END 

    '        If strIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '        End If

    '        If CBool(strIncludeInactiveEmployee) = False Then
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '        End If
    '        'S.SANDEEP [ 27 APRIL 2012 ] -- END




    '        'Anjan (24 Jun 2011)-Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.

    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '        'End If


    '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '        '        End If
    '        'End Select

    '        If strUserAccessLevelFilterString = "" Then
    '            strQ &= UserAccessLevel._AccessLevelFilterString
    '        Else
    '            strQ &= strUserAccessLevelFilterString
    '        End If
    '        'S.SANDEEP [ 27 APRIL 2012 ] -- END



    '        'Pinkal (25-APR-2012) -- Start
    '        'Enhancement : TRA Changes
    '        strQ &= " ORDER BY CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112) DESC "
    '        'Pinkal (25-APR-2012) -- End



    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function
    'S.SANDEEP [04 JUN 2015] -- END

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremp_qualification_tran) </purpose>
    Public Function Insert(Optional ByVal mdtTable As DataTable = Nothing, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'Gajanan [17-DEC-2018] -- Start {xDataOpr} -- End

        'Pinkal (25-APR-2012) -- Start
        'Enhancement : TRA Changes

        'If isExist(mintEmployeeunkid, mintQualificationunkid, mstrReference_No, mintInstituteunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Qualification is already assigned to particular employee. Please assign new Qualification.")
        '    Return False
        'End If
        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        'Dim dsList As DataSet = Nothing
        'Dim strQ As String = ""
        'Dim exForce As Exception

        'objDataOperation = New clsDataOperation
        ''Sandeep [ 16 Oct 2010 ] -- Start
        'objDataOperation.BindTransaction()
        ''Sandeep [ 16 Oct 2010 ] -- End 

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        'Gajanan [17-DEC-2018] -- End
        If isExist(mintEmployeeunkid, mintQualificationunkid, mstrReference_No, mintInstituteunkid, -1, mstrOtherQualification, objDataOperation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Qualification is already assigned to particular employee. Please assign new Qualification.")
            Return False
        End If

        'Pinkal (25-APR-2012) -- End

        



        'Gajanan [17-DEC-2018] -- End
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationgroupunkid.ToString)
            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationunkid.ToString)
'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.



 'objDataOperation.AddParameter("@transaction_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransaction_Date)
            If mdtTransaction_Date = Nothing Then
                objDataOperation.AddParameter("@transaction_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
            objDataOperation.AddParameter("@transaction_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransaction_Date)
            End If
            'Gajanan [17-DEC-2018] -- End
            objDataOperation.AddParameter("@reference_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReference_No.ToString)

            If mdtAward_Start_Date = Nothing Then
                objDataOperation.AddParameter("@award_start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@award_start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAward_Start_Date)
            End If

            If mdtAward_End_Date = Nothing Then
                objDataOperation.AddParameter("@award_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@award_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAward_End_Date)
            End If

            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultunkid.ToString)
            objDataOperation.AddParameter("@gpacode", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblGPAcode.ToString)



            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes


            objDataOperation.AddParameter("@other_qualificationgrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualificationGrp.ToString)
            objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualification.ToString)
            objDataOperation.AddParameter("@other_ResultCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherResultCode.ToString)
            'S.SANDEEP [23 JUL 2016] -- START
            'CCK- OTHER QUALIFICATION CHANGE
            objDataOperation.AddParameter("@other_institute", SqlDbType.NVarChar, mstrOther_institute.Length, mstrOther_institute)
            'S.SANDEEP [23 JUL 2016] -- END



            'Pinkal (24-Sep-2020) -- Start
            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
            objDataOperation.AddParameter("@certificateno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCertificateNo)
            objDataOperation.AddParameter("@ishighestqualification", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsHighestQualfication)
            'Pinkal (24-Sep-2020) -- End


            'strQ = "INSERT INTO hremp_qualification_tran ( " & _
            '                  "  employeeunkid " & _
            '                  ", qualificationgroupunkid " & _
            '                  ", qualificationunkid " & _
            '                  ", transaction_date " & _
            '                  ", reference_no " & _
            '                  ", award_start_date " & _
            '                  ", award_end_date " & _
            '                  ", instituteunkid " & _
            '                  ", remark " & _
            '                  ", userunkid " & _
            '                  ", isvoid " & _
            '                  ", voiddatetime " & _
            '                  ", voiduserunkid " & _
            '                  ", voidreason" & _
            '                ", loginemployeeunkid " & _
            '                ", voidloginemployeeunkid" & _
            '                ", resultunkid " & _
            '                ", gpacode " & _
            '            ") VALUES (" & _
            '                  "  @employeeunkid " & _
            '                  ", @qualificationgroupunkid " & _
            '                  ", @qualificationunkid " & _
            '                  ", @transaction_date " & _
            '                  ", @reference_no " & _
            '                  ", @award_start_date " & _
            '                  ", @award_end_date " & _
            '                  ", @instituteunkid " & _
            '                  ", @remark " & _
            '                  ", @userunkid " & _
            '                  ", @isvoid " & _
            '                  ", @voiddatetime " & _
            '                  ", @voiduserunkid " & _
            '                  ", @voidreason" & _
            '                ", @loginemployeeunkid " & _
            '                ", @voidloginemployeeunkid" & _
            '                 ", @resultunkid " & _
            '                ", @gpacode " & _
            '            "); SELECT @@identity"


            strQ = "INSERT INTO hremp_qualification_tran ( " & _
                              "  employeeunkid " & _
                              ", qualificationgroupunkid " & _
                              ", qualificationunkid " & _
                              ", transaction_date " & _
                              ", reference_no " & _
                              ", award_start_date " & _
                              ", award_end_date " & _
                              ", instituteunkid " & _
                              ", remark " & _
                              ", userunkid " & _
                              ", isvoid " & _
                              ", voiddatetime " & _
                              ", voiduserunkid " & _
                              ", voidreason" & _
                            ", loginemployeeunkid " & _
                            ", voidloginemployeeunkid" & _
                            ", resultunkid " & _
                            ", gpacode " & _ 
                            ", other_qualificationgrp " & _
                            ", other_qualification " & _
                            ", other_ResultCode " & _
                            ", other_institute " & _
                            ", certificateno " & _
                            ", ishighestqualification " & _
                        ") VALUES (" & _
                              "  @employeeunkid " & _
                              ", @qualificationgroupunkid " & _
                              ", @qualificationunkid " & _
                              ", @transaction_date " & _
                              ", @reference_no " & _
                              ", @award_start_date " & _
                              ", @award_end_date " & _
                              ", @instituteunkid " & _
                              ", @remark " & _
                              ", @userunkid " & _
                              ", @isvoid " & _
                              ", @voiddatetime " & _
                              ", @voiduserunkid " & _
                              ", @voidreason" & _
                            ", @loginemployeeunkid " & _
                            ", @voidloginemployeeunkid" & _
                             ", @resultunkid " & _
                            ", @gpacode " & _
                            ", @other_qualificationgrp " & _
                            ", @other_qualification " & _
                            ", @other_ResultCode " & _
                            ", @other_institute " & _
                            ", @certificateno " & _
                            ", @ishighestqualification " & _
                        "); SELECT @@identity"


            'Pinkal (24-Sep-2020) -- Enhancement NMB - Changes for Recruitment Enhancement required by NMB.[ ", @certificateno , @ishighestqualification "]

            'S.SANDEEP [23 JUL 2016] -- START {other_institute} -- END

            'Pinkal (25-APR-2012) -- End

    

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintQualificationtranunkid = dsList.Tables(0).Rows(0).Item(0)


            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            If mdtTable IsNot Nothing Then
                Dim objDocument As New clsScan_Attach_Documents
                Dim dtTran As DataTable = objDocument._Datatable
                Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\"

                'SHANI (16 JUL 2015) -- Start
                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                'Upload Image folder to access those attachemts from Server and 
                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                'Dim dsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                'Dim strFolderName As String = (From p In dsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.QUALIFICATIONS) Select (p.Item("Name").ToString)).FirstOrDefault
                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data
                 Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.QUALIFICATIONS).Tables(0).Rows(0)("Name").ToString
                 'Gajanan [17-DEC-2018] -- End                
                'SHANI (16 JUL 2015) -- End
                If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"

                Dim dr As DataRow
                For Each drow As DataRow In mdtTable.Rows
                    dr = dtTran.NewRow
                    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                    dr("documentunkid") = drow("documentunkid")
                    dr("employeeunkid") = drow("employeeunkid")
                    dr("filename") = drow("filename")
                    dr("scanattachrefid") = drow("scanattachrefid")
                    dr("modulerefid") = drow("modulerefid")
					'Gajanan [17-DEC-2018] -- Start
					'Enhancement - Implementing Employee Approver Flow On Employee Data           
					dr("form_name") = drow("form_name")
                    dr("userunkid") = drow("userunkid")
                    'Gajanan [17-DEC-2018] -- End        

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'passing Qualification tranunkid for the Other Qualification , reason the attachment is not coming for Other Qualification. 
                    'dr("transactionunkid") = drow("transactionunkid")
                    dr("transactionunkid") = mintQualificationtranunkid
                    'Shani(24-Aug-2015) -- End
					'Gajanan [17-DEC-2018] -- Start
					'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'dr("userunkid") = User._Object._Userunkid
					'Gajanan [17-DEC-2018] -- End
                    dr("attached_date") = drow("attached_date")
                    dr("orgfilepath") = drow("localpath")
                    dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                    dr("AUD") = drow("AUD")
                    'SHANI (03 JUL 2015) -- Start
                    dr("userunkid") = mintUserunkid
                    'SHANI (03 JUL 2015) -- End 

                    'SHANI (16 JUL 2015) -- Start
                    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                    'Upload Image folder to access those attachemts from Server and 
                    'all client machines if ArutiSelfService is installed otherwise save then on Document path
                    dr("fileuniquename") = drow("fileuniquename")
                    dr("filepath") = drow("filepath")
                    dr("filesize") = drow("filesize_kb")
                    'SHANI (16 JUL 2015) -- End 

                    dtTran.Rows.Add(dr)
                Next
                objDocument._Datatable = dtTran
                objDocument.InsertUpdateDelete_Documents(objDataOperation)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If
            'SHANI (20 JUN 2015) -- End 


            'Sandeep [ 16 Oct 2010 ] -- Start
            If InsertAuditTrailForQualification(objDataOperation, 1) Then
                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'objDataOperation.ReleaseTransaction(True)
                If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
                End If
                'Gajanan [17-DEC-2018] -- End
            Else

                'Anjan (11 Jun 2011)-Start
                'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
                'objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
                'Anjan (11 Jun 2011)-End 


            End If
            'Sandeep [ 16 Oct 2010 ] -- End 


            Return True
        Catch ex As Exception
            'Sandeep [ 16 Oct 2010 ] -- Start
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation.ReleaseTransaction(False)
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If
            'Gajanan [17-DEC-2018] -- End
            'Sandeep [ 16 Oct 2010 ] -- End 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [17-DEC-2018] -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremp_qualification_tran) </purpose>
    Public Function Update(Optional ByVal mdtTable As DataTable = Nothing, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'SHANI (20 JUN 2015) -- [mdtTable]


        'Pinkal (25-APR-2012) -- Start
        'Enhancement : TRA Changes

        'If isExist(mintEmployeeunkid, mintQualificationunkid, mstrReference_No, mintInstituteunkid, mintQualificationtranunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Qualification is already assigned to particular employee. Please assign new Qualification.")
        '    Return False
        'End If


        If isExist(mintEmployeeunkid, mintQualificationunkid, mstrReference_No, mintInstituteunkid, mintQualificationtranunkid, mstrOtherQualification) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Qualification is already assigned to particular employee. Please assign new Qualification.")
            Return False
        End If

        'Pinkal (25-APR-2012) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.



        'objDataOperation = New clsDataOperation
        ''Sandeep [ 16 Oct 2010 ] -- Start
        'objDataOperation.BindTransaction()
        ''Sandeep [ 16 Oct 2010 ] -- End 

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
'Gajanan [17-DEC-2018] -- End
        Try
            objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationtranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationgroupunkid.ToString)
            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationunkid.ToString)

            If mdtTransaction_Date = Nothing Then
                objDataOperation.AddParameter("@transaction_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
            objDataOperation.AddParameter("@transaction_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransaction_Date)
            End If

            objDataOperation.AddParameter("@reference_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReference_No.ToString)

            If mdtAward_Start_Date = Nothing Then
                objDataOperation.AddParameter("@award_start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@award_start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAward_Start_Date)
            End If

            If mdtAward_End_Date = Nothing Then
                objDataOperation.AddParameter("@award_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@award_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAward_End_Date)
            End If

            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
             objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultunkid.ToString)
            objDataOperation.AddParameter("@gpacode", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblGPAcode.ToString)



            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes

            objDataOperation.AddParameter("@other_qualificationgrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualificationGrp.ToString)
            objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualification.ToString)
            objDataOperation.AddParameter("@other_ResultCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherResultCode.ToString)

            'S.SANDEEP [23 JUL 2016] -- START
            'CCK- OTHER QUALIFICATION CHANGE
            objDataOperation.AddParameter("@other_institute", SqlDbType.NVarChar, mstrOther_institute.Length, mstrOther_institute.ToString)
            'S.SANDEEP [23 JUL 2016] -- END


            'Pinkal (24-Sep-2020) -- Start
            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
            objDataOperation.AddParameter("@certificateno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCertificateNo)
            objDataOperation.AddParameter("@ishighestqualification", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsHighestQualfication)
            'Pinkal (24-Sep-2020) -- End


            'strQ = "UPDATE hremp_qualification_tran SET " & _
            '              "  employeeunkid = @employeeunkid" & _
            '              ", qualificationgroupunkid = @qualificationgroupunkid" & _
            '              ", qualificationunkid = @qualificationunkid" & _
            '              ", transaction_date = @transaction_date" & _
            '              ", reference_no = @reference_no" & _
            '              ", award_start_date = @award_start_date" & _
            '              ", award_end_date = @award_end_date" & _
            '              ", instituteunkid = @instituteunkid" & _
            '              ", remark = @remark" & _
            '              ", userunkid = @userunkid" & _
            '              ", isvoid = @isvoid" & _
            '              ", voiddatetime = @voiddatetime" & _
            '              ", voiduserunkid = @voiduserunkid" & _
            '              ", voidreason = @voidreason " & _
            '              ", loginemployeeunkid = @loginemployeeunkid" & _
            '              ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
            '              ", resultunkid = @resultunkid" & _
            '              ", gpacode = @gpacode " & _
            '            "WHERE qualificationtranunkid = @qualificationtranunkid "


            strQ = "UPDATE hremp_qualification_tran SET " & _
                          "  employeeunkid = @employeeunkid" & _
                          ", qualificationgroupunkid = @qualificationgroupunkid" & _
                          ", qualificationunkid = @qualificationunkid" & _
                          ", transaction_date = @transaction_date" & _
                          ", reference_no = @reference_no" & _
                          ", award_start_date = @award_start_date" & _
                          ", award_end_date = @award_end_date" & _
                          ", instituteunkid = @instituteunkid" & _
                          ", remark = @remark" & _
                          ", userunkid = @userunkid" & _
                          ", isvoid = @isvoid" & _
                          ", voiddatetime = @voiddatetime" & _
                          ", voiduserunkid = @voiduserunkid" & _
                          ", voidreason = @voidreason " & _
                          ", loginemployeeunkid = @loginemployeeunkid" & _
                          ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                          ", resultunkid = @resultunkid" & _
                          ", gpacode = @gpacode " & _
                        ", other_qualificationgrp  = @other_qualificationgrp " & _
                        ", other_qualification  = @other_qualification " & _
                        ", other_ResultCode  = @other_ResultCode " & _
                          ", other_institute  = @other_institute " & _
                          ", certificateno  = @certificateno " & _
                          ", ishighestqualification  = @ishighestqualification " & _
                          " WHERE qualificationtranunkid = @qualificationtranunkid "


            'Pinkal (24-Sep-2020) --Enhancement NMB - Changes for Recruitment Enhancement required by NMB.[ ", certificateno  = @certificateno , ishighestqualification  = @ishighestqualification ]

            'S.SANDEEP [23 JUL 2016] -- START {other_institute} -- END

          'Pinkal (25-APR-2012) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            If mdtTable IsNot Nothing Then
                Dim objDocument As New clsScan_Attach_Documents
                Dim dtTran As DataTable = objDocument._Datatable
                Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\"

                'SHANI (16 JUL 2015) -- Start
                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                'Upload Image folder to access those attachemts from Server and 
                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                'Dim dsDoc As DataSet = (New clsScan_Attach_Documents).GetDocType("Docs")
                'Dim strFolderName As String = (From p In dsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.QUALIFICATIONS) Select (p.Item("Name").ToString)).FirstOrDefault
                Dim strFolderName As String = ""
                
                'SHANI (16 JUL 2015) -- End
                If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"

                Dim dr As DataRow
                For Each drow As DataRow In mdtTable.Rows
                    dr = dtTran.NewRow
                    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                    dr("documentunkid") = drow("documentunkid")
                    dr("employeeunkid") = drow("employeeunkid")
                    dr("filename") = drow("filename")
                    dr("scanattachrefid") = drow("scanattachrefid")
                    dr("modulerefid") = drow("modulerefid")
                    dr("transactionunkid") = drow("transactionunkid")
                    dr("userunkid") = User._Object._Userunkid
                    dr("attached_date") = drow("attached_date")
                    dr("orgfilepath") = drow("localpath")
                    dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                    dr("AUD") = drow("AUD")
                    'SHANI (03 JUL 2015) -- Start
                    dr("userunkid") = mintUserunkid
                    'SHANI (03 JUL 2015) -- End 

                    'SHANI (16 JUL 2015) -- Start
                    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                    'Upload Image folder to access those attachemts from Server and 
                    'all client machines if ArutiSelfService is installed otherwise save then on Document path
                    dr("fileuniquename") = drow("fileuniquename")
                    dr("filepath") = drow("filepath")
                    dr("filesize") = drow("filesize_kb")
                    'SHANI (16 JUL 2015) -- End 

                    dtTran.Rows.Add(dr)
                Next
                objDocument._Datatable = dtTran
                objDocument.InsertUpdateDelete_Documents(objDataOperation)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If
            'SHANI (20 JUN 2015) -- End 

            'Sandeep [ 16 Oct 2010 ] -- Start
            If InsertAuditTrailForQualification(objDataOperation, 2) Then
             'Gajanan [17-DEC-2018] -- Start
             'Enhancement - Implementing Employee Approver Flow On Employee Data.
             'objDataOperation.ReleaseTransaction(True)
                 If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
                End If
'Gajanan [17-DEC-2018] -- End
            Else
                'objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sandeep [ 16 Oct 2010 ] -- End 

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            'Sandeep [ 16 Oct 2010 ] -- Start
'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.



            'objDataOperation.ReleaseTransaction(False)

            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If
'Gajanan [17-DEC-2018] -- End
            'Sandeep [ 16 Oct 2010 ] -- End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If xDataOpr Is Nothing Then objDataOperation = Nothing

'Gajanan [17-DEC-2018] -- End

        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremp_qualification_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.



        'objDataOperation = New clsDataOperation
        ''Sandeep [ 16 Oct 2010 ] -- Start
        'objDataOperation.BindTransaction()
        ''Sandeep [ 16 Oct 2010 ] -- End 

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
'Gajanan [17-DEC-2018] -- End

        Try


            'S.SANDEEP [18-NOV-2017] -- START
            'Bug : Urgent Finca Tanzania issue # 0001540: Inactive staff from 5 months ago are showing up in October application event logs
            Dim xVoidUserID As Integer = 0
            xVoidUserID = mintVoiduserunkid
            'S.SANDEEP [18-NOV-2017] -- END


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'strQ = "UPDATE hremp_qualification_tran SET " & _
            '                       "  isvoid = @isvoid" & _
            '                       ", voiddatetime = @voiddatetime" & _
            '                       ", voiduserunkid = @voiduserunkid" & _
            '                       ", voidreason = @voidreason " & _
            '                       "WHERE qualificationtranunkid = @qualificationtranunkid "
            If mintVoidloginemployeeunkid <= -1 Then
            strQ = "UPDATE hremp_qualification_tran SET " & _
                       "  isvoid = @isvoid" & _
                       ", voiddatetime = @voiddatetime" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voidreason = @voidreason " & _
                       "WHERE qualificationtranunkid = @qualificationtranunkid "
            Else
                strQ = "UPDATE hremp_qualification_tran SET " & _
                       "  isvoid = @isvoid" & _
                       ", voiddatetime = @voiddatetime" & _
                       ", voidloginemployeeunkid  = @voidloginemployeeunkid " & _
                       ", voidreason = @voidreason " & _
                       "WHERE qualificationtranunkid = @qualificationtranunkid "
            End If
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            If mintVoidloginemployeeunkid > 0 Then
                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            End If
            'S.SANDEEP [ 12 OCT 2011 ] -- END 


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sandeep [ 16 Oct 2010 ] -- Start
            _Qualificationtranunkid = intUnkid


            'S.SANDEEP [18-NOV-2017] -- START
            'Bug : Urgent Finca Tanzania issue # 0001540: Inactive staff from 5 months ago are showing up in October application event logs
            mintVoiduserunkid = xVoidUserID
            'S.SANDEEP [18-NOV-2017] -- END 


            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            'Intensionally Passed QualificationId Instead Of TransactionId [Parameter Name : intTranunkid Method {DeleteTransacation} ] Of Table (hrdocuments_tran)

            Dim objDocument As New clsScan_Attach_Documents
            If objDocument.DeleteTransacation(_Employeeunkid, enScanAttactRefId.QUALIFICATIONS, intUnkid, objDataOperation, _WebFormName) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'SHANI (20 JUN 2015) -- End 

            If InsertAuditTrailForQualification(objDataOperation, 3) Then
'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.



                'objDataOperation.ReleaseTransaction(True)

                If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
                End If
'Gajanan [17-DEC-2018] -- End
            Else
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sandeep [ 16 Oct 2010 ] -- End 

            Return True

        Catch ex As Exception
            'Sandeep [ 16 Oct 2010 ] -- Start
'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.



            'objDataOperation.ReleaseTransaction(False)
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If

            'Sandeep [ 16 Oct 2010 ] -- End 
'Gajanan [17-DEC-2018] -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If xDataOpr Is Nothing Then objDataOperation = Nothing
'Gajanan [17-DEC-2018] -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'Sandeep [ 25 APRIL 2011 ] -- Start
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' Public Function isExist(ByVal intEmpId As Integer, ByVal intQualificationId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
    Public Function isExist(ByVal intEmpId As Integer, ByVal intQualificationId As Integer, ByVal strRefNo As String, ByVal intProvider As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal strOtherQualification As String = "", Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'Sandeep [ 25 APRIL 2011 ] -- End 


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        objDataOperation = New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Gajanan [17-DEC-2018] -- End

        Try


            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '              "  qualificationtranunkid " & _
            '              ", employeeunkid " & _
            '              ", qualificationgroupunkid " & _
            '              ", qualificationunkid " & _
            '              ", transaction_date " & _
            '              ", reference_no " & _
            '              ", award_start_date " & _
            '              ", award_end_date " & _
            '              ", instituteunkid " & _
            '          ", resultunkid " & _
            '          ", gpacode " & _
            '              ", remark " & _
            '              ", userunkid " & _
            '              ", isvoid " & _
            '              ", voiddatetime " & _
            '              ", voiduserunkid " & _
            '              ", voidreason " & _
            '             "FROM hremp_qualification_tran " & _
            '             "WHERE employeeunkid = @EmpId " & _
            '             "AND qualificationunkid = @QualifyId " & _
            '             "AND reference_no = @RefId " & _
            '           "AND instituteunkid = @InstId " & _
            '           "AND isvoid = 0 "

            strQ = "SELECT " & _
                          "  qualificationtranunkid " & _
                          ", employeeunkid " & _
                          ", qualificationgroupunkid " & _
                          ", qualificationunkid " & _
                          ", transaction_date " & _
                          ", reference_no " & _
                          ", award_start_date " & _
                          ", award_end_date " & _
                          ", instituteunkid " & _
                        ", resultunkid " & _
                        ", gpacode " & _
                          ", remark " & _
                          ", userunkid " & _
                          ", isvoid " & _
                          ", voiddatetime " & _
                          ", voiduserunkid " & _
                          ", voidreason " & _
                          ", other_qualificationgrp " & _
                          ", other_qualification " & _
                          ", other_resultcode " & _
                          ", other_institute " & _
                          ", certificateno " & _
                          ", ishighestqualification " & _
                          " FROM hremp_qualification_tran " & _
                          " WHERE employeeunkid = @EmpId " & _
                          " AND instituteunkid = @InstId " & _
                          " AND isvoid = 0 "

            'Pinkal (24-Sep-2020) -- Enhancement NMB - Changes for Recruitment Enhancement required by NMB.[  ", other_qualificationgrp , other_qualification , other_resultcode , other_institute , certificateno, ishighestqualification " ]

            If mdtAward_Start_Date <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),award_start_date,112) = '" & eZeeDate.convertDate(mdtAward_Start_Date).ToString & "' "
            End If

            If mdtAward_End_Date <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),award_end_date,112) = '" & eZeeDate.convertDate(mdtAward_End_Date).ToString & "' "
            End If

            'If strRefNo.Trim.Length > 0 Then
            '    strQ &= " AND reference_no = @RefId "
            'End If

            If strOtherQualification.Trim.Length > 0 Then
                strQ &= " AND other_qualification <> '' AND other_qualification = @other_qualification "
            Else
                strQ &= " AND qualificationunkid = @QualifyId "
            End If

            'Pinkal (25-APR-2012) -- End



            If intUnkid > 0 Then
                strQ &= " AND qualificationtranunkid <> @qualificationtranunkid"
            End If

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@QualifyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intQualificationId)
            objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            'Sandeep [ 25 APRIL 2011 ] -- Start
            objDataOperation.AddParameter("@RefId", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strRefNo)
            objDataOperation.AddParameter("@InstId", SqlDbType.Int, eZeeDataType.INT_SIZE, intProvider)
            'Sandeep [ 25 APRIL 2011 ] -- End 


            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes
            objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strOtherQualification)
            'Pinkal (25-APR-2012) -- End


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [17-DEC-2018] -- End
        End Try
    End Function


    'Sandeep [ 16 Oct 2010 ] -- Start
    Private Function InsertAuditTrailForQualification(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try



            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = "INSERT INTO athremp_qualification_tran ( " & _
            '            "  qualificationtranunkid " & _
            '            ", employeeunkid " & _
            '            ", qualificationgroupunkid " & _
            '            ", qualificationunkid " & _
            '            ", transaction_date " & _
            '            ", reference_no " & _
            '            ", award_start_date " & _
            '            ", award_end_date " & _
            '            ", instituteunkid " & _
            '            ", resultunkid " & _
            '            ", gpacode " & _
            '            ", remark " & _
            '            ", audittype " & _
            '            ", audituserunkid " & _
            '            ", auditdatetime " & _
            '            ", ip " & _
            '            ", machine_name" & _
            '       ") VALUES (" & _
            '            "  @qualificationtranunkid " & _
            '            ", @employeeunkid " & _
            '            ", @qualificationgroupunkid " & _
            '            ", @qualificationunkid " & _
            '            ", @transaction_date " & _
            '            ", @reference_no " & _
            '            ", @award_start_date " & _
            '            ", @award_end_date " & _
            '            ", @instituteunkid " & _
            '            ", @resultunkid " & _
            '            ", @gpacode " & _
            '            ", @remark " & _
            '            ", @audittype " & _
            '            ", @audituserunkid " & _
            '            ", @auditdatetime " & _
            '            ", @ip " & _
            '            ", @machine_name" & _
            '       "); SELECT @@identity"


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            'strQ = "INSERT INTO athremp_qualification_tran ( " & _
            '            "  qualificationtranunkid " & _
            '            ", employeeunkid " & _
            '            ", qualificationgroupunkid " & _
            '            ", qualificationunkid " & _
            '            ", transaction_date " & _
            '            ", reference_no " & _
            '            ", award_start_date " & _
            '            ", award_end_date " & _
            '            ", instituteunkid " & _
            '            ", resultunkid " & _
            '            ", gpacode " & _
            '            ", remark " & _
            '            ", audittype " & _
            '            ", audituserunkid " & _
            '            ", auditdatetime " & _
            '            ", ip " & _
            '            ", machine_name" & _
            '           ", other_qualificationgrp " & _
            '           ", other_qualification " & _
            '           ", other_ResultCode " & _
            '       ") VALUES (" & _
            '            "  @qualificationtranunkid " & _
            '            ", @employeeunkid " & _
            '            ", @qualificationgroupunkid " & _
            '            ", @qualificationunkid " & _
            '            ", @transaction_date " & _
            '            ", @reference_no " & _
            '            ", @award_start_date " & _
            '            ", @award_end_date " & _
            '            ", @instituteunkid " & _
            '            ", @resultunkid " & _
            '            ", @gpacode " & _
            '            ", @remark " & _
            '            ", @audittype " & _
            '            ", @audituserunkid " & _
            '            ", @auditdatetime " & _
            '            ", @ip " & _
            '            ", @machine_name" & _
            '           ", @other_qualificationgrp " & _
            '           ", @other_qualification " & _
            '           ", @other_ResultCode " & _
            '       "); SELECT @@identity"

            strQ = "INSERT INTO athremp_qualification_tran ( " & _
                        "  qualificationtranunkid " & _
                        ", employeeunkid " & _
                        ", qualificationgroupunkid " & _
                        ", qualificationunkid " & _
                        ", transaction_date " & _
                        ", reference_no " & _
                        ", award_start_date " & _
                        ", award_end_date " & _
                        ", instituteunkid " & _
                        ", resultunkid " & _
                        ", gpacode " & _
                        ", remark " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name" & _
                       ", other_qualificationgrp " & _
                       ", other_qualification " & _
                       ", other_ResultCode " & _
                         ", certificateno " & _
                         ", ishighestqualification " & _
                     ", form_name " & _
                     ", module_name1 " & _
                     ", module_name2 " & _
                     ", module_name3 " & _
                     ", module_name4 " & _
                     ", module_name5 " & _
                     ", isweb " & _
                        ", loginemployeeunkid " & _
                        ", other_institute " & _
                   ") VALUES (" & _
                        "  @qualificationtranunkid " & _
                        ", @employeeunkid " & _
                        ", @qualificationgroupunkid " & _
                        ", @qualificationunkid " & _
                        ", @transaction_date " & _
                        ", @reference_no " & _
                        ", @award_start_date " & _
                        ", @award_end_date " & _
                        ", @instituteunkid " & _
                        ", @resultunkid " & _
                        ", @gpacode " & _
                        ", @remark " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name" & _
                       ", @other_qualificationgrp " & _
                       ", @other_qualification " & _
                       ", @other_ResultCode " & _
                       ", @certificateno " & _
                       ", @ishighestqualification " & _
                     ", @form_name " & _
                     ", @module_name1 " & _
                     ", @module_name2 " & _
                     ", @module_name3 " & _
                     ", @module_name4 " & _
                     ", @module_name5 " & _
                     ", @isweb " & _
                        ", @loginemployeeunkid " & _
                        ", @other_institute " & _
                    "); SELECT @@identity" 'S.SANDEEP [ 13 AUG 2012(loginemployeeunkid)] -- START -- END


            'Pinkal (24-Sep-2020) -- Enhancement NMB - Changes for Recruitment Enhancement required by NMB.[", @certificateno ", @ishighestqualification " ]


            'S.SANDEEP [23 JUL 2016] -- START {other_institute} -- END

            'Pinkal (25-APR-2012) -- End

            'S.SANDEEP [ 19 JULY 2012 ] -- END


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationtranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationgroupunkid.ToString)
            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationunkid.ToString)
'Gajanan [17-DEC-2018] -- Start
'Enhancement - Implementing Employee Approver Flow On Employee Data.
         
If mdtTransaction_Date = Nothing Then
                objDataOperation.AddParameter("@transaction_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
            objDataOperation.AddParameter("@transaction_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransaction_Date)
            End If
'Gajanan [17-DEC-2018] -- End  
            objDataOperation.AddParameter("@reference_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReference_No.ToString)
            If mdtAward_Start_Date = Nothing Then
                objDataOperation.AddParameter("@award_start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@award_start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAward_Start_Date)
            End If

            If mdtAward_End_Date = Nothing Then
                objDataOperation.AddParameter("@award_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@award_end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAward_End_Date)
            End If
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.SmallInt, eZeeDataType.INT_SIZE, intAuditType.ToString)
            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid.ToString)

            'S.SANDEEP [18-NOV-2017] -- START
            'Bug : Urgent Finca Tanzania issue # 0001540: Inactive staff from 5 months ago are showing up in October application event logs
            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            'S.SANDEEP [18-NOV-2017] -- END

            'S.SANDEEP [ 27 APRIL 2012 ] -- END
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)

            'S.SANDEEP [ 13 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP)
            'objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))
            'S.SANDEEP [ 13 AUG 2012 ] -- END

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultunkid)
            objDataOperation.AddParameter("@gpacode", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblGPAcode)
            'Pinkal (12-Oct-2011) -- End


            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes
            objDataOperation.AddParameter("@other_qualificationgrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualificationGrp.ToString)
            objDataOperation.AddParameter("@other_qualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualification.ToString)
            objDataOperation.AddParameter("@other_ResultCode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherResultCode.ToString)
            'S.SANDEEP [23 JUL 2016] -- START
            'CCK- OTHER QUALIFICATION CHANGE
            objDataOperation.AddParameter("@other_institute", SqlDbType.NVarChar, mstrOther_institute.Length, mstrOther_institute.ToString)
            'S.SANDEEP [23 JUL 2016] -- END

            'Pinkal (25-APR-2012) -- End



            'Pinkal (24-Sep-2020) -- Start
            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
            objDataOperation.AddParameter("@certificateno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCertificateNo)
            objDataOperation.AddParameter("@ishighestqualification", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsHighestQualfication)
            'Pinkal (24-Sep-2020) -- End


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            If mstrWebFormName.Trim.Length <= 0 Then
                'S.SANDEEP [ 11 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim frm As Form
                'For Each frm In Application.OpenForms
                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                '    End If
                'Next
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                'S.SANDEEP [ 11 AUG 2012 ] -- END
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 3, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            'S.SANDEEP [ 19 JULY 2012 ] -- END

            'S.SANDEEP [ 13 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            'S.SANDEEP [ 13 AUG 2012 ] -- END



            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForQualification", mstrModuleName)
            Return False
        End Try
    End Function
    'Sandeep [ 16 Oct 2010 ] -- End 

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES


    'S.SANDEEP [ 26 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Function GetQualificationData_Export() As DataSet
    Public Function GetQualificationData_Export(Optional ByVal blnIsOtherQualif As Boolean = False) As DataSet
        'S.SANDEEP [ 26 APRIL 2012 ] -- END



        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'Gajanan (24 Nov 2018) -- Start
            'Enhancement : Import template column headers should read from language set for 
            'users (custom 1) and columns' date formats for importation should be clearly known
            'Replace Column Name With Getmessage

            StrQ = "SELECT " & _
                     " ISNULL(hremployee_master.employeecode,'') AS [" & Language.getMessage(mstrModuleName, 5, "ECODE") & "]" & _
                     ",ISNULL(hremployee_master.firstname,'') AS  [" & Language.getMessage(mstrModuleName, 6, "FIRSTNAME") & "]" & _
                     ",ISNULL(hremployee_master.surname,'') AS  [" & Language.getMessage(mstrModuleName, 7, "SURNAME") & "]"
            'S.SANDEEP [14-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {NMB}
            If blnIsOtherQualif = False Then
                'StrQ &= ",ISNULL(cfcommon_master.name,'') AS QUALIFICATION_GROUP " & _
                '     ",ISNULL(hrqualification_master.qualificationname,'') AS QUALIFICATION " & _
                '        ",ISNULL(hrresult_master.resultname,'') AS RESULT " & _
                '        ",ISNULL(hrinstitute_master.institute_name,'') AS INSTITUTE "

                StrQ &= ",ISNULL(cfcommon_master.name,'') AS [" & Language.getMessage(mstrModuleName, 8, "QUALIFICATION_GROUP") & "]" & _
                     ",ISNULL(hrqualification_master.qualificationname,'') AS [" & Language.getMessage(mstrModuleName, 9, "QUALIFICATION_NAME") & "]" & _
                        ",ISNULL(hrresult_master.resultname,'') AS [" & Language.getMessage(mstrModuleName, 10, "RESULT") & "]" & _
                        ",ISNULL(hrinstitute_master.institute_name,'') AS [" & Language.getMessage(mstrModuleName, 11, "INSTITUTE") & "]"
            Else
                
                'StrQ &= ",ISNULL(other_qualificationgrp,'') AS OTHER_QUALIFICATION_GROUP " & _
                '     ",ISNULL(other_qualification,'') AS OTHER_QUALIFICATION " & _
                '        ",ISNULL(other_resultcode,'') AS OTHER_RESULT " & _
                '        ",ISNULL(hrinstitute_master.other_institute,'') AS INSTITUTE "

                StrQ &= ",ISNULL(other_qualificationgrp,'') AS [" & Language.getMessage(mstrModuleName, 12, "OTHER_QUALIFICATION_GROUP") & "]" & _
                        ",ISNULL(other_qualification,'') AS [" & Language.getMessage(mstrModuleName, 13, "OTHER_QUALIFICATION_NAME") & "]" & _
                        ",ISNULL(other_resultcode,'') AS [" & Language.getMessage(mstrModuleName, 14, "OTHER_RESULT") & "]" & _
                        ",ISNULL(other_institute,'') AS [" & Language.getMessage(mstrModuleName, 15, "INSTITUTE") & "]"
                'S.SANDEEP [14-JUN-2018] -- END

                
            End If
            StrQ &= ",ISNULL(hremp_qualification_tran.reference_no,'') AS [" & Language.getMessage(mstrModuleName, 16, "REF_NO") & "]" & _
                     ",hremp_qualification_tran.award_start_date AS  [" & Language.getMessage(mstrModuleName, 17, "START_DATE") & "]" & _
                     ",hremp_qualification_tran.award_end_date AS  [" & Language.getMessage(mstrModuleName, 18, "END_DATE") & "]" & _
                     ",ISNULL(hremp_qualification_tran.remark,'') AS  [" & Language.getMessage(mstrModuleName, 19, "REMARK") & "]" & _
                     ",CASE WHEN hrinstitute_master.issync_recruit = 1 THEN 'TRUE' ELSE 'FALSE' END AS [" & Language.getMessage(mstrModuleName, 20, "ISSYNC_RECRUIT") & "]" & _
                     ", ISNULL(certificateno,'') AS [" & Language.getMessage(mstrModuleName, 21, "CERTIFICATE_NO") & "]" & _
                     ", ISNULL(ishighestqualification,0) AS [" & Language.getMessage(mstrModuleName, 22, "ISHIGHQUALIFICATION") & "]" & _
                   "FROM hremp_qualification_tran " & _
                     "LEFT JOIN hrinstitute_master ON hremp_qualification_tran.instituteunkid = hrinstitute_master.instituteunkid " & _
                     "LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                     "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid AND mastertype = " & enCommonMaster.QUALIFICATION_COURSE_GROUP & " " & _
                     "JOIN hremployee_master ON hremp_qualification_tran.employeeunkid = hremployee_master.employeeunkid " & _
                     "LEFT JOIN hrresult_master ON hrresult_master.resultunkid = hremp_qualification_tran.resultunkid " & _
                   "WHERE isvoid = 0 "


            'Pinkal (24-Sep-2020) -- Enhancement NMB - Changes for Recruitment Enhancement required by NMB.[ ", ISNULL(certificateno,'') AS [" & Language.getMessage(mstrModuleName, 21, "CERTIFICATE_NO") ", ISNULL(ishighestqualification,0) AS [" & Language.getMessage(mstrModuleName, 22, "ISHIGHQUALIFICATION") & "]" & _]

            'Gajanan (24 Nov 2018) -- End
            If blnIsOtherQualif = False Then
                StrQ &= "AND ISNULL(other_qualificationgrp,'') = '' AND ISNULL(other_qualification,'') = '' "
            Else
                StrQ &= "AND ISNULL(other_qualificationgrp,'') <> '' AND ISNULL(other_qualification,'') <> '' "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetQualificationData_Export ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP [ 26 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Function GetQualificationTranId(ByVal intEmpId As Integer, ByVal StrQualification As String) As Integer
    Public Function GetQualificationTranId(ByVal intEmpId As Integer, _
                                           ByVal StrQualification As String, _
                                           ByVal dtStartDate As String, _
                                           ByVal dtEndDate As String, _
                                           Optional ByVal blnIsOtherQualif As Boolean = False) As Integer
        'S.SANDEEP [ 26 APRIL 2012 ] -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            If blnIsOtherQualif = False Then
                strQ = "SELECT " & _
                       " qualificationtranunkid " & _
                       "FROM hremp_qualification_tran " & _
                       "    JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                       "WHERE employeeunkid = @EmpId AND LTRIM(RTRIM(qualificationname)) LIKE LTRIM(RTRIM(@Qualification)) AND isvoid = 0 "
            Else
                strQ = "SELECT " & _
                       " qualificationtranunkid " & _
                       "FROM hremp_qualification_tran " & _
                       "WHERE employeeunkid = @EmpId AND LTRIM(RTRIM(other_qualification)) LIKE LTRIM(RTRIM(@Qualification)) AND isvoid = 0 "
            End If


            'Anjan (17 Apr 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            If dtStartDate.Trim.Length > 0 Then
                If IsDate(dtStartDate) Then
                    strQ &= " AND CONVERT(CHAR(8),hremp_qualification_tran.award_start_date,112) = '" & eZeeDate.convertDate(CDate(dtStartDate)).ToString & "' "
                End If
            End If

            If dtEndDate.Trim.Length Then
                If IsDate(dtEndDate) Then
                    strQ &= " AND CONVERT(CHAR(8),hremp_qualification_tran.award_end_date,112) = '" & eZeeDate.convertDate(CDate(dtEndDate)).ToString & "' "
                End If
            End If
            'Anjan (17 Apr 2012)-End 



            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@Qualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrQualification)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("qualificationtranunkid")
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetQualificationTranId ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 07 NOV 2011 ] -- END


    'S.SANDEEP [ 04 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Shared Function GetEmployeeQualification(ByVal intEmpId As Integer, Optional ByVal blnFlag As Boolean = False, Optional ByVal strOrderBy As String = "") As DataSet
        'Sohail (03 Nov 2020) - [strOrderBy]
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            Dim objDataOperation As New clsDataOperation

            If blnFlag = True Then
                StrQ = "SELECT 0 As Id,@Select AS Name  UNION "
            End If
            StrQ &= "SELECT " & _
                        "    hrqualification_master.qualificationunkid AS Id " & _
                        "   ,ISNULL(hrqualification_master.qualificationname, '') AS Name " & _
                        "FROM hremp_qualification_tran " & _
                        "    LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                        "WHERE ISNULL(hremp_qualification_tran.isvoid, 0) = 0 AND employeeunkid = @EmpId "

            'Sohail (03 Nov 2020) -- Start
            'NMB Enhancement : # : Pick last qualification name on Talent Screening screen.
            If strOrderBy.Trim <> "" Then
                StrQ &= " ORDER BY " & strOrderBy
            End If
            'Sohail (03 Nov 2020) -- End

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId.ToString)
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select Qualification"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Function
    'S.SANDEEP [ 04 FEB 2012 ] -- END


    'Pinkal (24-Sep-2020) -- Start
    'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.

    Public Function GetRecruitmentEmpQualification(ByVal blnOtherQualification As Boolean, ByVal intEmployeeunkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            Dim objDataOperation As New clsDataOperation

            If blnOtherQualification Then

                strQ = "SELECT    " & _
                          " ISNULL(STUFF(( SELECT DISTINCT CASE WHEN other_qualificationgrp <> '' THEN  ',' + CAST(other_qualificationgrp AS VARCHAR(max)) ELSE  " & _
                          " CAST(other_qualificationgrp AS VARCHAR(max)) END " & _
                          " FROM hremp_qualification_tran " & _
                          " WHERE hremp_qualification_tran.employeeunkid =  @employeeunkid AND hremp_qualification_tran.isvoid = 0 " & _
                          " FOR  XML PATH('')), 1, 1, ''),'') AS QualificationGrp, " & _
                          " ISNULL(STUFF(( SELECT DISTINCT CASE WHEN other_qualification <> '' THEN ',' + CAST(other_qualification AS VARCHAR(max)) ELSE " & _
                          " CAST(other_qualification AS VARCHAR(max)) END   " & _
                          " FROM hremp_qualification_tran " & _
                          " WHERE hremp_qualification_tran.employeeunkid = @employeeunkid  AND hremp_qualification_tran.isvoid = 0 " & _
                          " FOR  XML PATH('')), 1, 1, ''),'')  AS Qualification," & _
                          " ISNULL(STUFF(( SELECT  ',' + CAST(hremp_qualification_tran.gpacode AS VARCHAR(max))  " & _
                          " FROM hremp_qualification_tran  " & _
                          " WHERE hremp_qualification_tran.employeeunkid = @employeeunkid  AND hremp_qualification_tran.isvoid = 0 FOR  XML PATH('')), 1, 1, ''),'')  AS gpacode "

            Else

                strQ = "SELECT    " & _
                          " ISNULL(STUFF(( SELECT  ',' + CAST(cfcommon_master.name AS VARCHAR(max)) " & _
                          " FROM hremp_qualification_tran " & _
                          " JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid AND dbo.cfcommon_master.mastertype =" & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & _
                          " WHERE hremp_qualification_tran.employeeunkid =  @employeeunkid  AND hremp_qualification_tran.isvoid = 0 " & _
                          " ORDER BY cfcommon_master.masterunkid" & _
                          " FOR  XML PATH('')), 1, 1, ''),'') AS QualificationGrp, " & _
                          " ISNULL(STUFF(( SELECT  ',' + CAST(hrqualification_master.qualificationname AS VARCHAR(max)) " & _
                          " FROM hremp_qualification_tran " & _
                          " JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid  " & _
                          " WHERE hremp_qualification_tran.employeeunkid = @employeeunkid  AND hremp_qualification_tran.isvoid = 0 " & _
                          " ORDER BY hrqualification_master.qualificationunkid " & _
                          " FOR  XML PATH('')), 1, 1, ''),'')  AS Qualification," & _
                          " ISNULL(STUFF(( SELECT  ',' + CAST(hremp_qualification_tran.gpacode AS VARCHAR(max))  " & _
                          " FROM hremp_qualification_tran  " & _
                          " WHERE hremp_qualification_tran.employeeunkid =@employeeunkid  AND hremp_qualification_tran.isvoid = 0 FOR  XML PATH('')), 1, 1, ''),'')  AS gpacode "

            End If


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetRecruitmentEmpQualification; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    'Pinkal (24-Sep-2020) -- End



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Qualification is already assigned to particular employee. Please assign new Qualification.")
			Language.setMessage(mstrModuleName, 2, "Select Qualification")
			Language.setMessage(mstrModuleName, 3, "WEB")
            Language.setMessage(mstrModuleName, 5, "ECODE")
            Language.setMessage(mstrModuleName, 6, "FIRSTNAME")
            Language.setMessage(mstrModuleName, 7, "SURNAME")
            Language.setMessage(mstrModuleName, 8, "QUALIFICATION_GROUP")
            Language.setMessage(mstrModuleName, 9, "QUALIFICATION_NAME")
            Language.setMessage(mstrModuleName, 10, "RESULT")
            Language.setMessage(mstrModuleName, 11, "INSTITUTE")
            Language.setMessage(mstrModuleName, 12, "OTHER_QUALIFICATION_GROUP")
            Language.setMessage(mstrModuleName, 13, "OTHER_QUALIFICATION_NAME")
            Language.setMessage(mstrModuleName, 14, "OTHER_RESULT")
            Language.setMessage(mstrModuleName, 15, "INSTITUTE")
            Language.setMessage(mstrModuleName, 16, "REF_NO")
            Language.setMessage(mstrModuleName, 17, "START_DATE")
            Language.setMessage(mstrModuleName, 18, "END_DATE")
            Language.setMessage(mstrModuleName, 19, "REMARK")
            Language.setMessage(mstrModuleName, 20, "ISSYNC_RECRUIT")
			Language.setMessage(mstrModuleName, 21, "CERTIFICATE_NO")
			Language.setMessage(mstrModuleName, 22, "ISHIGHQUALIFICATION")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

''************************************************************************************************************************************
''Class Name : clsEmp_Qualification_Tran.vb
''Purpose    :
''Date       :18/07/2010
''Written By :Sandeep J. Sharma
''Modified   :
''************************************************************************************************************************************

'Imports eZeeCommonLib
'Imports Aruti.Data.clsCommon_Master
'''' <summary>
'''' Purpose: 
'''' Developer: Sandeep J. Sharma
'''' </summary>
'Public Class clsEmp_Qualification_Tran
'    Private Shared Readonly mstrModuleName As String = "clsEmp_Qualification_Tran"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "
'    Private mintQualificationtranunkid As Integer
'    Private mintEmployeeunkid As Integer
'    Private mintQualificationgroupunkid As Integer
'    Private mintQualificationunkid As Integer
'    Private mdtQualification_Date As Date
'    Private mstrAward_Name As String = String.Empty
'    Private mstrReference_No As String = String.Empty
'    Private mstrInstitute_Name As String = String.Empty
'    Private mstrInstitute_Address As String = String.Empty
'    Private mstrContact_Person As String = String.Empty
'    Private mstrContact_No As String = String.Empty
'    Private mstrRemark As String = String.Empty
'    Private mintUserunkid As Integer
'    Private mblnIsvoid As Boolean
'    Private mdtVoiddatetime As Date
'    Private mintVoiduserunkid As Integer
'    Private mstrVoidreason As String = String.Empty
'#End Region

'#Region " Properties "
'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set qualificationtranunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Qualificationtranunkid() As Integer
'        Get
'            Return mintQualificationtranunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintQualificationtranunkid = Value
'            Call getData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set employeeunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Employeeunkid() As Integer
'        Get
'            Return mintEmployeeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintEmployeeunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set qualificationgroupunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Qualificationgroupunkid() As Integer
'        Get
'            Return mintQualificationgroupunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintQualificationgroupunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set qualificationunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Qualificationunkid() As Integer
'        Get
'            Return mintQualificationunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintQualificationunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set qualification_date
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Qualification_Date() As Date
'        Get
'            Return mdtQualification_Date
'        End Get
'        Set(ByVal value As Date)
'            mdtQualification_Date = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set award_name
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Award_Name() As String
'        Get
'            Return mstrAward_Name
'        End Get
'        Set(ByVal value As String)
'            mstrAward_Name = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set reference_no
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Reference_No() As String
'        Get
'            Return mstrReference_No
'        End Get
'        Set(ByVal value As String)
'            mstrReference_No = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set institute_name
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Institute_Name() As String
'        Get
'            Return mstrInstitute_Name
'        End Get
'        Set(ByVal value As String)
'            mstrInstitute_Name = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set institute_address
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Institute_Address() As String
'        Get
'            Return mstrInstitute_Address
'        End Get
'        Set(ByVal value As String)
'            mstrInstitute_Address = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set contact_person
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Contact_Person() As String
'        Get
'            Return mstrContact_Person
'        End Get
'        Set(ByVal value As String)
'            mstrContact_Person = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set contact_no
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Contact_No() As String
'        Get
'            Return mstrContact_No
'        End Get
'        Set(ByVal value As String)
'            mstrContact_No = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set remark
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Remark() As String
'        Get
'            Return mstrRemark
'        End Get
'        Set(ByVal value As String)
'            mstrRemark = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set userunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Userunkid() As Integer
'        Get
'            Return mintUserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintUserunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isvoid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Isvoid() As Boolean
'        Get
'            Return mblnIsvoid
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsvoid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiddatetime
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voiddatetime() As Date
'        Get
'            Return mdtVoiddatetime
'        End Get
'        Set(ByVal value As Date)
'            mdtVoiddatetime = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiduserunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voiduserunkid() As Integer
'        Get
'            Return mintVoiduserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintVoiduserunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voidreason
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voidreason() As String
'        Get
'            Return mstrVoidreason
'        End Get
'        Set(ByVal value As String)
'            mstrVoidreason = Value
'        End Set
'    End Property

'#End Region

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "SELECT " & _
'              "  qualificationtranunkid " & _
'              ", employeeunkid " & _
'              ", qualificationgroupunkid " & _
'              ", qualificationunkid " & _
'              ", qualification_date " & _
'              ", award_name " & _
'              ", reference_no " & _
'              ", institute_name " & _
'              ", institute_address " & _
'              ", contact_person " & _
'              ", contact_no " & _
'              ", remark " & _
'              ", userunkid " & _
'              ", isvoid " & _
'              ", voiddatetime " & _
'              ", voiduserunkid " & _
'              ", voidreason " & _
'             "FROM hremp_qualification_tran " & _
'             "WHERE qualificationtranunkid = @qualificationtranunkid "

'            objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintQualificationTranUnkId.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintQualificationtranunkid = CInt(dtRow.Item("qualificationtranunkid"))
'                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
'                mintqualificationgroupunkid = CInt(dtRow.Item("qualificationgroupunkid"))
'                mintQualificationunkid = CInt(dtRow.Item("qualificationunkid"))
'                'Sandeep [ 21 Aug 2010 ] -- Start
'                'mdtQualification_Date = dtRow.Item("qualification_date")
'                If IsDBNull(dtRow.Item("qualification_date")) Then
'                    mdtQualification_Date = Now
'                Else
'                    mdtQualification_Date = dtRow.Item("qualification_date")
'                End If
'                'Sandeep [ 21 Aug 2010 ] -- End 
'                mstrAward_Name = dtRow.Item("award_name").ToString
'                mstrreference_no = dtRow.Item("reference_no").ToString
'                mstrinstitute_name = dtRow.Item("institute_name").ToString
'                mstrinstitute_address = dtRow.Item("institute_address").ToString
'                mstrcontact_person = dtRow.Item("contact_person").ToString
'                mstrcontact_no = dtRow.Item("contact_no").ToString
'                mstrremark = dtRow.Item("remark").ToString
'                mintuserunkid = CInt(dtRow.Item("userunkid"))
'                mblnIsvoid = CBool(dtRow.Item("isvoid"))
'                If IsDBNull(dtRow.Item("voiddatetime")) Then
'                    mdtVoiddatetime = Nothing
'                Else
'                    mdtVoiddatetime = dtRow.Item("voiddatetime")
'                End If
'                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
'                mstrvoidreason = dtRow.Item("voidreason").ToString
'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub


'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                        "	 ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
'                        "	,ISNULL(cfcommon_master.name,'') AS QGrp " & _
'                        "	,ISNULL(hrqualification_master.qualificationname,'') AS Qualify " & _
'                        "	,CONVERT(CHAR(8),hremp_qualification_tran.qualification_date,112) AS Date " & _
'                        "	,ISNULL(hremp_qualification_tran.award_name,'') AS Award " & _
'                        "	,ISNULL(hremp_qualification_tran.reference_no,'') AS RefNo " & _
'                        "	,hremployee_master.employeeunkid AS EmpId " & _
'                        "	,cfcommon_master.masterunkid AS QGrpId " & _
'                        "	,hrqualification_master.qualificationunkid AS QualifyId " & _
'                        "   ,hremp_qualification_tran.qualificationtranunkid As QulifyTranId " & _
'                        "FROM hremp_qualification_tran " & _
'                        "	LEFT JOIN hrqualification_master ON hremp_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
'                        "	LEFT JOIN hremployee_master ON hremp_qualification_tran.employeeunkid = hremployee_master.employeeunkid " & _
'                        "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremp_qualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & enCommonMaster.QUALIFICATION_COURSE_GROUP & _
'                        "WHERE ISNULL(hremp_qualification_tran.isvoid,0) = 0 "

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (hremp_qualification_tran) </purpose>
'    Public Function Insert() As Boolean
'        If isExist(mintEmployeeunkid, mintQualificationunkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Qualification is already assigned to particular employee. Please assign new Qualification.")
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintemployeeunkid.ToString)
'            objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintqualificationgroupunkid.ToString)
'            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQualificationunkid.ToString)
'            'Sandeep [ 21 Aug 2010 ] -- Start
'            'objDataOperation.AddParameter("@qualification_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtQualification_Date)
'            If mdtQualification_Date = Nothing Then
'                objDataOperation.AddParameter("@qualification_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@qualification_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtQualification_Date)
'            End If
'            'Sandeep [ 21 Aug 2010 ] -- End 
'            objDataOperation.AddParameter("@award_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAward_Name.ToString)
'            objDataOperation.AddParameter("@reference_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReference_No.ToString)
'            objDataOperation.AddParameter("@institute_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrInstitute_Name.ToString)
'            objDataOperation.AddParameter("@institute_address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrInstitute_Address.ToString)
'            objDataOperation.AddParameter("@contact_person", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContact_Person.ToString)
'            objDataOperation.AddParameter("@contact_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContact_No.ToString)
'            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            If mdtVoiddatetime = Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            End If
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'            strQ = "INSERT INTO hremp_qualification_tran ( " & _
'                      "  employeeunkid " & _
'                      ", qualificationgroupunkid " & _
'                      ", qualificationunkid " & _
'                      ", qualification_date " & _
'                      ", award_name " & _
'                      ", reference_no " & _
'                      ", institute_name " & _
'                      ", institute_address " & _
'                      ", contact_person " & _
'                      ", contact_no " & _
'                      ", remark " & _
'                      ", userunkid " & _
'                      ", isvoid " & _
'                      ", voiddatetime " & _
'                      ", voiduserunkid " & _
'                      ", voidreason" & _
'                    ") VALUES (" & _
'                      "  @employeeunkid " & _
'                      ", @qualificationgroupunkid " & _
'                      ", @qualificationunkid " & _
'                      ", @qualification_date " & _
'                      ", @award_name " & _
'                      ", @reference_no " & _
'                      ", @institute_name " & _
'                      ", @institute_address " & _
'                      ", @contact_person " & _
'                      ", @contact_no " & _
'                      ", @remark " & _
'                      ", @userunkid " & _
'                      ", @isvoid " & _
'                      ", @voiddatetime " & _
'                      ", @voiduserunkid " & _
'                      ", @voidreason" & _
'                    "); SELECT @@identity"

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintQualificationtranunkid = dsList.Tables(0).Rows(0).Item(0)

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (hremp_qualification_tran) </purpose>
'    Public Function Update() As Boolean
'        If isExist(mintEmployeeunkid, mintQualificationunkid, mintQualificationtranunkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Qualification is already assigned to particular employee. Please assign new Qualification.")
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintqualificationtranunkid.ToString)
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintemployeeunkid.ToString)
'            objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintqualificationgroupunkid.ToString)
'            objDataOperation.AddParameter("@qualificationunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintqualificationunkid.ToString)
'            objDataOperation.AddParameter("@qualification_date", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtqualification_date)
'            objDataOperation.AddParameter("@award_name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraward_name.ToString)
'            objDataOperation.AddParameter("@reference_no", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrreference_no.ToString)
'            objDataOperation.AddParameter("@institute_name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrinstitute_name.ToString)
'            objDataOperation.AddParameter("@institute_address", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrinstitute_address.ToString)
'            objDataOperation.AddParameter("@contact_person", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcontact_person.ToString)
'            objDataOperation.AddParameter("@contact_no", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcontact_no.ToString)
'            objDataOperation.AddParameter("@remark", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrremark.ToString)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
'            If mdtVoiddatetime = Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            End If
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
'            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

'            strQ = "UPDATE hremp_qualification_tran SET " & _
'                      "  employeeunkid = @employeeunkid" & _
'                      ", qualificationgroupunkid = @qualificationgroupunkid" & _
'                      ", qualificationunkid = @qualificationunkid" & _
'                      ", qualification_date = @qualification_date" & _
'                      ", award_name = @award_name" & _
'                      ", reference_no = @reference_no" & _
'                      ", institute_name = @institute_name" & _
'                      ", institute_address = @institute_address" & _
'                      ", contact_person = @contact_person" & _
'                      ", contact_no = @contact_no" & _
'                      ", remark = @remark" & _
'                      ", userunkid = @userunkid" & _
'                      ", isvoid = @isvoid" & _
'                      ", voiddatetime = @voiddatetime" & _
'                      ", voiduserunkid = @voiduserunkid" & _
'                      ", voidreason = @voidreason " & _
'                    "WHERE qualificationtranunkid = @qualificationtranunkid "

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (hremp_qualification_tran) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer) As Boolean
'        'If isUsed(intUnkid) Then
'        '    mstrMessage = "<Message>"
'        '    Return False
'        'End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "UPDATE hremp_qualification_tran SET " & _
'                      "  isvoid = @isvoid" & _
'                      ", voiddatetime = @voiddatetime" & _
'                      ", voiduserunkid = @voiduserunkid" & _
'                      ", voidreason = @voidreason " & _
'                    "WHERE qualificationtranunkid = @qualificationtranunkid "

'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
'            objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "<Query>"

'            objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.tables(0).rows.count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(ByVal intEmpId As Integer, ByVal intQualification As String, Optional ByVal intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                        "  qualificationtranunkid " & _
'                        ", employeeunkid " & _
'                        ", qualificationgroupunkid " & _
'                        ", qualificationunkid " & _
'                        ", qualification_date " & _
'                        ", award_name " & _
'                        ", reference_no " & _
'                        ", institute_name " & _
'                        ", institute_address " & _
'                        ", contact_person " & _
'                        ", contact_no " & _
'                        ", remark " & _
'                        ", userunkid " & _
'                        ", isvoid " & _
'                        ", voiddatetime " & _
'                        ", voiduserunkid " & _
'                        ", voidreason " & _
'                   "FROM hremp_qualification_tran " & _
'                   "WHERE employeeunkid = @EmpId " & _
'                   "AND qualificationunkid = @QualifyId " & _
'                   "AND isvoid = 0 "

'            If intUnkid > 0 Then
'                strQ &= " AND qualificationtranunkid <> @qualificationtranunkid"
'            End If

'            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
'            objDataOperation.AddParameter("@QualifyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intQualification)
'            objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'End Class
