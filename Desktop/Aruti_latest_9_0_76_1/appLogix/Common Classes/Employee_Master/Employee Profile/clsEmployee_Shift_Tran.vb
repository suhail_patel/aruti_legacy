﻿'************************************************************************************************************************************
'Class Name :clsReportingToEmployee.vb
'Purpose    :
'Date       :26/10/2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class clsEmployee_Shift_Tran

#Region " Private Variable "

    Private Shared ReadOnly mstrModuleName As String = "clsEmployee_Shift_Tran"
    Private objDataOperation As clsDataOperation
    Private mstrMessage As String = ""
    Private mdtTran As DataTable
    Private mintEmployeeId As Integer = -1
    Private mintshifttranunkid As Integer = -1
    'Pinkal (10-Aug-2016) -- Start
    'Enhancement - Implementing Status of Day Off and Suspension for Zanzibar Residence.
    Private mdtEffectiveDate As Date = Nothing
    Private mintShiftID As Integer = -1
    Private mintUserID As Integer = -1
    Private mblnIsvoid As Boolean = False
    'Pinkal (10-Aug-2016) -- End

    'S.SANDEEP [19 OCT 2016] -- START
    'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
    Private xDataOperation As clsDataOperation = Nothing
    'S.SANDEEP [19 OCT 2016] -- END

#End Region

#Region " Properties "

    'Pinkal (10-Aug-2016) -- Start
    'Enhancement - Implementing Status of Day Off and Suspension for Zanzibar Residence.

    ''' <summary>
    ''' Purpose: Get or Set Message
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _effectivedate() As Date
        Get
            Return mdtEffectiveDate
        End Get
        Set(ByVal value As Date)
            mdtEffectiveDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ShiftUnkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ShiftUnkid() As Integer
        Get
            Return mintShiftID
        End Get
        Set(ByVal value As Integer)
            mintShiftID = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserID
        End Get
        Set(ByVal value As Integer)
            mintUserID = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    'Pinkal (10-Aug-2016) -- End


    Public Property _EmployeeUnkid() As Integer
        Get
            Return mintEmployeeId
        End Get
        Set(ByVal value As Integer)
            mintEmployeeId = value
            Call Get_ShiftList()
        End Set
    End Property

    Public Property _SDataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property


    'S.SANDEEP [19 OCT 2016] -- START
    'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
    Public WriteOnly Property _xDataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOperation = value
        End Set
    End Property
    'S.SANDEEP [19 OCT 2016] -- END

#End Region

#Region " Constructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("SList")
            Dim dCol As DataColumn

            dCol = New DataColumn
            dCol.ColumnName = "shifttranunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "employeeunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "shiftunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "isdefault"
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "userunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "isvoid"
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voiduserunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voiddatetime"
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voidreason"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "AUD"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "GUID"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            'S.SANDEEP [ 26 SEPT 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dCol = New DataColumn
            dCol.ColumnName = "effectivedate"
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)
            'S.SANDEEP [ 26 SEPT 2013 ] -- END


            '/********************** DATA DISPLAY **********************/
            dCol = New DataColumn
            dCol.ColumnName = "shifttype"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "shiftname"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "effdate"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)
            '/********************** DATA DISPLAY **********************/

        Catch ex As IndexOutOfRangeException

        Catch ex As OutOfMemoryException

        Catch ex As AccessViolationException

        Catch ex As Exception
            'Sohail (22 Oct 2013) -- Start
            'TRA - ENHANCEMENT
            'Call DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
            'Sohail (22 Oct 2013) -- End
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub Get_ShiftList()
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try

            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'objDataOperation = New clsDataOperation
            If xDataOperation IsNot Nothing Then
                objDataOperation = xDataOperation
            Else
            objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()
            'S.SANDEEP [19 OCT 2016] -- END


            StrQ = "SELECT " & _
                   "  hremployee_shift_tran.shifttranunkid " & _
                   ", hremployee_shift_tran.employeeunkid " & _
                   ", hremployee_shift_tran.shiftunkid " & _
                   ", hremployee_shift_tran.isdefault " & _
                   ", hremployee_shift_tran.userunkid " & _
                   ", hremployee_shift_tran.isvoid " & _
                   ", hremployee_shift_tran.voiduserunkid " & _
                   ", hremployee_shift_tran.voiddatetime " & _
                   ", hremployee_shift_tran.voidreason " & _
                   ", '' AS AUD " & _
                   ", ISNULL(cfcommon_master.name,'') AS shifttype " & _
                   ", ISNULL(tnashift_master.shiftname,'') AS shiftname " & _
                   ", hremployee_shift_tran.effectivedate AS effectivedate " & _
                   ", CONVERT(CHAR(8),hremployee_shift_tran.effectivedate,112) AS effdate " & _
                   "FROM hremployee_shift_tran " & _
                   " JOIN tnashift_master ON hremployee_shift_tran.shiftunkid = tnashift_master.shiftunkid " & _
                   " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = tnashift_master.shifttypeunkid " & _
                   "WHERE employeeunkid = @employeeunkid AND hremployee_shift_tran.isvoid = 0 "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables("List").Rows
                dRow.Item("effdate") = eZeeDate.convertDate(dRow.Item("effdate").ToString).ToShortDateString
                mdtTran.ImportRow(dRow)
            Next

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Get_ShiftList", mstrModuleName)
        End Try
    End Sub

    Public Function InsertUpdateDelete(Optional ByVal intUserUnkId As Integer = 0, Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal mblnOverWriteIfExist As Boolean = False) As Boolean

        'Pinkal (29-Jul-2019) -- 'Enhancement - Working on Papaaye Shift Overriding feature On Employee Shift Assignment.[ Optional ByVal mblnOverWriteIfExist As Boolean = False]

        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try

            If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            Else
                objDataOperation = xDataOpr
            End If

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then


                        'Pinkal (29-Jul-2019) -- Start
                        'Enhancement - Working on Papaaye Shift Overriding feature On Employee Shift Assignment.
                        If mblnOverWriteIfExist Then
                            Dim xShiftTranId As Integer = GetShiftTranunkid(CDate(.Item("effectivedate")).Date, mintEmployeeId, objDataOperation)
                            If xShiftTranId > 0 Then
                                .Item("shifttranunkid") = xShiftTranId
                                .Item("AUD") = "U"
                            End If
                        End If

                        If CInt(.Item("userunkid")) > 0 Then intUserUnkId = CInt(.Item("userunkid"))

                        objDataOperation.ClearParameters()

                        'Pinkal (29-Jul-2019) -- End


                        Select Case .Item("AUD")
                            Case "A"
                                If isExist(CDate(.Item("effectivedate")), mintEmployeeId, objDataOperation, .Item("shiftunkid")).ToString.Trim.Length <= 0 Then
                                StrQ = "INSERT INTO hremployee_shift_tran ( " & _
                                            "  employeeunkid " & _
                                            ", shiftunkid " & _
                                            ", isdefault " & _
                                            ", userunkid " & _
                                            ", isvoid " & _
                                            ", voiduserunkid " & _
                                            ", voiddatetime " & _
                                            ", voidreason" & _
                                            ", effectivedate " & _
                                       ") VALUES (" & _
                                            "  @employeeunkid " & _
                                            ", @shiftunkid " & _
                                            ", @isdefault " & _
                                            ", @userunkid " & _
                                            ", @isvoid " & _
                                            ", @voiduserunkid " & _
                                            ", @voiddatetime " & _
                                            ", @voidreason" & _
                                            ", @effectivedate " & _
                                           "); SELECT @@identity"

                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId.ToString)
                                objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("shiftunkid").ToString)
                                objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isdefault").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("effectivedate"))

                                Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                mintshifttranunkid = dsList.Tables(0).Rows(0)(0)
                                If .Item("employeeunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_shift_tran", "shifttranunkid", mintshifttranunkid, 2, 1, , intUserUnkId) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", mintEmployeeId, "hremployee_shift_tran", "shifttranunkid", mintshifttranunkid, 2, 1, , intUserUnkId) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                End If

                            Case "U"


                                'Pinkal (29-Jul-2019) -- Start
                                'Enhancement - Working on Papaaye Shift Overriding feature On Employee Shift Assignment.

                                StrQ = "UPDATE hremployee_shift_tran SET " & _
                                       "  employeeunkid = @employeeunkid" & _
                                       ", shiftunkid = @shiftunkid" & _
                                       ", isdefault = @isdefault" & _
                                       ", userunkid = @userunkid" & _
                                       ", isvoid = @isvoid" & _
                                       ", voiduserunkid = @voiduserunkid" & _
                                       ", voiddatetime = @voiddatetime" & _
                                       ", voidreason = @voidreason " & _
                                       ", effectivedate = @effectivedate " & _
                                       "WHERE shifttranunkid = @shifttranunkid  AND isvoid = 0"

                                objDataOperation.AddParameter("@shifttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("shifttranunkid").ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
                                objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("shiftunkid").ToString)
                                objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isdefault").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("effectivedate"))

                                objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", mintEmployeeId, "hremployee_shift_tran", "shifttranunkid", .Item("shifttranunkid"), 2, 2, , intUserUnkId) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Pinkal (29-Jul-2019) -- End

                            Case "D"
                                StrQ = "UPDATE hremployee_shift_tran SET " & _
                                      "  isvoid = @isvoid" & _
                                      ", voiduserunkid = @voiduserunkid" & _
                                      ", voiddatetime = @voiddatetime" & _
                                      ", voidreason = @voidreason " & _
                                      "WHERE shifttranunkid = @shifttranunkid "

                                objDataOperation.AddParameter("@shifttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("shifttranunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_shift_tran", "shifttranunkid", .Item("shifttranunkid"), 2, 3, , intUserUnkId) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next

            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete; Module Name: " & mstrModuleName)
        End Try
    End Function

    'Pinkal (07-Oct-2015) -- Start
    'Enhancement - WORKING ON HYATT URGENT CHANGES REGARDING SHIFT.

    'Public Shared Function Shift_Assignment(ByVal intOldShiftId As Integer, ByVal intNewShiftId As Integer) As Boolean
    '    Dim StrQ As String = String.Empty
    '    Dim dsOldShift As New DataSet
    '    Dim dsNewShift As New DataSet
    '    Try
    '        Dim objDataOperation As New clsDataOperation

    '        StrQ = "SELECT " & _
    '               "	tnashift_tran.dayid,tnashift_tran.starttime,tnashift_tran.endtime,tnashift_tran.isweekend,tnashift_tran.workinghrs " & _
    '               "FROM tnashift_tran " & _
    '               "	JOIN tnashift_master ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid " & _
    '               "WHERE tnashift_master.shiftunkid = '" & intOldShiftId & "' AND isactive = 1 "

    '        dsOldShift = objDataOperation.ExecQuery(StrQ, "OList")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '        End If

    '        StrQ = "SELECT " & _
    '               "	tnashift_tran.dayid,tnashift_tran.starttime,tnashift_tran.endtime,tnashift_tran.isweekend,tnashift_tran.workinghrs " & _
    '               "FROM tnashift_tran " & _
    '               "	JOIN tnashift_master ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid " & _
    '               "WHERE tnashift_master.shiftunkid = '" & intNewShiftId & "' AND isactive = 1 "

    '        dsNewShift = objDataOperation.ExecQuery(StrQ, "NList")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '        End If

    '        If dsOldShift.Tables(0).Rows.Count <= 0 Or dsNewShift.Tables(0).Rows.Count <= 0 Then
    '            Return False
    '        End If

    '        For Each dORow As DataRow In dsOldShift.Tables("OList").Rows
    '            Dim dNRow() As DataRow = dsNewShift.Tables("NList").Select("dayid = '" & dORow.Item("dayid") & "'")
    '            If dNRow.Length > 0 Then
    '                If CInt(dNRow(0).Item("workinghrs")) > 0 Then
    '                    'Pinkal (07-Oct-2015) -- Start
    '                    'Enhancement - WORKING ON HYATT URGENT CHANGES REGARDING SHIFT.
    '                    If Convert.ToDateTime(dNRow(0).Item("starttime")) <= Convert.ToDateTime(dORow.Item("endtime")) Then
    '                        Return False
    '                    End If
    '                    'If Convert.ToDateTime(dNRow(0).Item("starttime")).ToShortTimeString <= Convert.ToDateTime(dORow.Item("endtime")).ToShortTimeString Then
    '                    '    Return False
    '                    'End If
    '                    'Pinkal (07-Oct-2015) -- End

    '                End If
    '            End If
    '        Next
    '        Return True
    '    Catch ex As Exception
    '        Call DisplayError.Show("-1", ex.Message, "Shift_Assignment", mstrModuleName)
    '        Return False
    '    End Try
    'End Function

    Public Shared Function Shift_Assignment(ByVal intOldShiftId As Integer, ByVal intNewShiftId As Integer, ByVal dtOldDate As Date, ByVal dtNewDate As Date) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsOldShift As New DataSet
        Dim dsNewShift As New DataSet
        Try
            Dim objDataOperation As New clsDataOperation

            StrQ = "SELECT " & _
                   "	tnashift_tran.dayid,tnashift_tran.starttime,tnashift_tran.endtime,tnashift_tran.isweekend,tnashift_tran.workinghrs " & _
                   "FROM tnashift_tran " & _
                   "	JOIN tnashift_master ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid " & _
                   "WHERE tnashift_master.shiftunkid = '" & intOldShiftId & "' AND isactive = 1 "

            dsOldShift = objDataOperation.ExecQuery(StrQ, "OList")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            StrQ = "SELECT " & _
                   "	tnashift_tran.dayid,tnashift_tran.starttime,tnashift_tran.endtime,tnashift_tran.isweekend,tnashift_tran.workinghrs " & _
                   "FROM tnashift_tran " & _
                   "	JOIN tnashift_master ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid " & _
                   "WHERE tnashift_master.shiftunkid = '" & intNewShiftId & "' AND isactive = 1 "

            dsNewShift = objDataOperation.ExecQuery(StrQ, "NList")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsOldShift.Tables(0).Rows.Count <= 0 Or dsNewShift.Tables(0).Rows.Count <= 0 Then
                Return False
            End If

            For Each dORow As DataRow In dsOldShift.Tables("OList").Rows
                Dim dNRow() As DataRow = dsNewShift.Tables("NList").Select("dayid = '" & dORow.Item("dayid") & "'")
                If dNRow.Length > 0 Then
                    If CInt(dNRow(0).Item("workinghrs")) > 0 Then
                        'Pinkal (07-Oct-2015) -- Start
                        'Enhancement - WORKING ON HYATT URGENT CHANGES REGARDING SHIFT.
                        'If Convert.ToDateTime(dNRow(0).Item("starttime")) <= Convert.ToDateTime(dORow.Item("endtime")) Then
                        '    Return False
                        'End If
                        If CDate(dtNewDate.Date & " " & Convert.ToDateTime(dNRow(0).Item("starttime")).ToShortTimeString) <= CDate(dtOldDate.Date & " " & Convert.ToDateTime(dORow.Item("endtime")).ToShortTimeString) Then
                            Return False
                        End If
                        'Pinkal (07-Oct-2015) -- End

                    End If
                End If
            Next
            Return True
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Shift_Assignment", mstrModuleName)
            Return False
        End Try
    End Function

    'Pinkal (07-Oct-2015) -- End

    Public Function isExist(ByVal iDate As DateTime, ByVal iEmployeeId As Integer, Optional ByVal objDataOperation As clsDataOperation = Nothing, Optional ByVal iShiftId As Integer = 0) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim iValue As String = String.Empty

        If objDataOperation Is Nothing Then
        objDataOperation = New clsDataOperation
        End If

        Try
            'S.SANDEEP [ 15 OCT 2013 ] -- START
            'ENHANCEMENT : ENHANCEMENT
            If iShiftId > 0 Then
                strQ = "SELECT ISNULL(STUFF((SELECT DISTINCT ',' + CAST(shiftunkid AS NVARCHAR(50)) FROM hremployee_shift_tran WHERE employeeunkid = '" & iEmployeeId & "' AND shiftunkid = '" & iShiftId & "' AND CONVERT(CHAR(8),effectivedate,112) >= '" & eZeeDate.convertDate(iDate) & "' AND isvoid = 0 FOR XML PATH('')),1,1,''),'') AS iShift "
            Else
            strQ = "SELECT ISNULL(STUFF((SELECT DISTINCT ',' + CAST(shiftunkid AS NVARCHAR(50)) FROM hremployee_shift_tran WHERE employeeunkid = '" & iEmployeeId & "' AND CONVERT(CHAR(8),effectivedate,112) >= '" & eZeeDate.convertDate(iDate) & "' AND isvoid = 0 FOR XML PATH('')),1,1,''),'') AS iShift "
            End If
            'S.SANDEEP [ 15 OCT 2013 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                iValue = dsList.Tables("List").Rows(0).Item("iShift")
            End If

            Return iValue
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
    'Public Function Get_List(ByVal sListName As String, ByVal dStDate As DateTime, ByVal dEdDate As DateTime, _
    '                         Optional ByVal iEmpId As Integer = 0, _
    '                         Optional ByVal iSftypeId As Integer = 0, _
    '                         Optional ByVal iShiftId As Integer = 0, _
    '                         Optional ByVal mStrFilter As String = "") As DataTable
    Public Function Get_List(ByVal xDatabaseName As String, _
                             ByVal xUserUnkid As Integer, _
                             ByVal xYearUnkid As Integer, _
                             ByVal xCompanyUnkid As Integer, _
                             ByVal xUserModeSetting As String, _
                             ByVal sListName As String, _
                             ByVal dStDate As DateTime, _
                             ByVal dEdDate As DateTime, _
                             Optional ByVal iEmpId As Integer = 0, _
                             Optional ByVal iSftypeId As Integer = 0, _
                             Optional ByVal iShiftId As Integer = 0, _
                             Optional ByVal mStrFilter As String = "", _
                             Optional ByVal xDataOp As clsDataOperation = Nothing, _
                             Optional ByVal blnApplyDateFilter As Boolean = True, _
                             Optional ByVal strAdvanceJoinFilter As String = "" _
                             ) As DataTable
        'Sohail (12 Oct 2021) - [blnApplyDateFilter, strAdvanceJoinFilter]
        'Sohail (03 May 2018) - [xDataOp]
        'Shani(24-Aug-2015) -- End

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet : Dim dTable As DataTable = Nothing
        Try
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = New clsDataOperation
            If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOp
            End If
            objDataOperation.ClearParameters()
            'Sohail (03 May 2018) -- End


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dStDate, dEdDate, , , xDatabaseName)
            'Call GetAdvanceFilterQry(xAdvanceJoinQry, dEdDate, xDatabaseName)
            If blnApplyDateFilter = True Then
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dStDate, dEdDate, , , xDatabaseName)
            End If
            If strAdvanceJoinFilter.Trim.Length > 0 Then
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dEdDate, xDatabaseName)
            End If
            'Sohail (12 Oct 2021) -- End
            'Shani(24-Aug-2015) -- End

            StrQ = "SELECT " & _
                   "  CAST(0 AS BIT) AS ischeck " & _
                   " ,employeecode AS ecode " & _
                   " ,firstname+' '+surname AS ename " & _
                   " ,CONVERT(CHAR(8),effectivedate,112) AS effectivedate " & _
                   " ,CONVERT(CHAR(8),effectivedate,112) AS streffectivedate " & _
                   " ,hremployee_shift_tran.shiftunkid " & _
                   " ,tnashift_master.shiftname " & _
                   " ,hremployee_shift_tran.shifttranunkid " & _
                   " ,hremployee_master.employeeunkid " & _
                   "FROM hremployee_shift_tran " & _
                   " JOIN tnashift_master ON hremployee_shift_tran.shiftunkid = tnashift_master.shiftunkid " & _
                   " JOIN hremployee_master ON hremployee_shift_tran.employeeunkid = hremployee_master.employeeunkid "


            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

           
            'Shani(24-Aug-2015) -- End

            'S.SANDEEP [12 MAY 2015] -- START
            StrQ &= "WHERE hremployee_shift_tran.isvoid = 0 AND hremployee_master.isapproved = 1 "
            'S.SANDEEP [12 MAY 2015] -- END

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If
            'Shani(24-Aug-2015) -- End

            'Pinkal (03-Jan-2014) -- Start
            'Enhancement : Oman Changes

            '   AND CONVERT(CHAR(8),effectivedate,112) BETWEEN '" & eZeeDate.convertDate(dStDate) & "' AND '" & eZeeDate.convertDate(dEdDate) & "' "

            If dStDate <> Nothing Then
                StrQ &= " AND  CONVERT(CHAR(8),effectivedate,112) >= '" & eZeeDate.convertDate(dStDate) & "'"
            End If

            If dEdDate <> Nothing Then
                StrQ &= " AND  CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dEdDate) & "'"
            End If

            'Pinkal (03-Jan-2014) -- End

            If iEmpId > 0 Then
                StrQ &= "AND hremployee_master.employeeunkid = '" & iEmpId & "' "
            End If

            If iSftypeId > 0 Then
                StrQ &= "AND tnashift_master.shifttypeunkid = '" & iSftypeId & "' "
            End If

            If iShiftId > 0 Then
                StrQ &= "AND tnashift_master.shiftunkid = '" & iShiftId & "' "
            End If

            If mStrFilter.Trim.Length > 0 Then
                StrQ &= " AND " & mStrFilter
            End If

            'Pinkal (16-Nov-2021)-- Start
            'Enhancement :  Zuri want to sorting by effective date descending.
            If iEmpId > 0 Then
                StrQ &= " ORDER BY CONVERT(CHAR(8),effectivedate,112) DESC "
            Else
            StrQ &= " ORDER BY firstname+' '+surname "
            End If
            'Pinkal (16-Nov-2021)-- End

            dsList = objDataOperation.ExecQuery(StrQ, IIf(sListName = "", "List", sListName))
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dTable = dsList.Tables(0).Clone
            dTable.Rows.Clear()

            For Each dRow As DataRow In dsList.Tables(0).Rows
                dRow.Item("effectivedate") = eZeeDate.convertDate(dRow.Item("effectivedate").ToString).ToShortDateString
                dTable.ImportRow(dRow)
            Next

            Return dTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_List", mstrModuleName)
            Return Nothing
        Finally
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
    End Function

    Public Function GetEmployee_Old_ShiftId(ByVal iDate As DateTime, ByVal iEmployId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim iValue As Integer = 0
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT TOP 1 shiftunkid	AS iShift FROM hremployee_shift_tran WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) = '" & eZeeDate.convertDate(iDate) & "' AND employeeunkid = '" & iEmployId & "' ORDER BY shifttranunkid DESC "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                iValue = dsList.Tables("List").Rows(0).Item("iShift")
            End If

            Return iValue
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployee_Old_ShiftId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function Void_Assigned_Shift(ByVal iUnkid As Integer, ByVal iEmployeeId As Integer, ByVal sViodReason As String, ByVal iUserUnkid As Integer, ByVal iDate As Date, ByVal iShiftId As Integer) As Boolean 'S.SANDEEP [ 31 OCT 2013 ] -- START {iDate,iShiftId} -- END
        Dim StrQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            objDataOperation.BindTransaction()

            StrQ = "UPDATE hremployee_shift_tran SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE shifttranunkid = @shifttranunkid "

            objDataOperation.AddParameter("@shifttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUserUnkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, sViodReason)

            Call objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 31 OCT 2013 ] -- START
            Dim iReturnDt As Date = Nothing
            iReturnDt = GetCurrShiftEndDate(iEmployeeId, iShiftId, iDate, objDataOperation)
            If iReturnDt <> Nothing Then
                StrQ = " UPDATE tnalogin_summary SET isabsentprocess = 0 WHERE employeeunkid = '" & iEmployeeId & "' AND shiftunkid = '" & iShiftId & "' AND CONVERT(CHAR(8),login_date,112) BETWEEN '" & eZeeDate.convertDate(iDate) & "' AND '" & eZeeDate.convertDate(iReturnDt) & "' "
            ElseIf iReturnDt = Nothing Then
                StrQ = " UPDATE tnalogin_summary SET isabsentprocess = 0 WHERE employeeunkid = '" & iEmployeeId & "' AND shiftunkid = '" & iShiftId & "' AND CONVERT(CHAR(8),login_date,112) >= '" & eZeeDate.convertDate(iDate) & "' "
            End If

            Call objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP [ 31 OCT 2013 ] -- END

            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", iEmployeeId, "hremployee_shift_tran", "shifttranunkid", iUnkid, 2, 3, , iUserUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Void_Assigned_Shift; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
    End Function

    Public Function GetEmployee_Current_ShiftId(ByVal iDate As DateTime, ByVal iEmployId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim iValue As Integer = 0
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT TOP 1 shiftunkid	AS iShift FROM hremployee_shift_tran WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(iDate) & "' AND employeeunkid = '" & iEmployId & "' AND effectivedate IS NOT NULL ORDER BY CONVERT(CHAR(8),effectivedate,112) DESC "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                iValue = dsList.Tables("List").Rows(0).Item("iShift")
            End If

            Return iValue
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployee_Current_ShiftId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
    'Public Sub GetWorkingDaysAndHours(ByVal intEmployeeID As Integer, ByVal dtStartDate As Date, ByVal dtEndDate As Date, ByRef intWorkingDays As Integer, ByRef dblWorkingHours As Double)
    Public Sub GetWorkingDaysAndHours(ByVal xDatabaseName As String, _
                                      ByVal xUserUnkid As Integer, _
                                      ByVal xYearUnkid As Integer, _
                                      ByVal xCompanyUnkid As Integer, _
                                      ByVal xUserModeSetting As String, _
                                      ByVal intEmployeeID As Integer, _
                                      ByVal dtStartDate As Date, _
                                      ByVal dtEndDate As Date, _
                                      ByRef intWorkingDays As Integer, _
                                      ByRef dblWorkingHours As Double, _
                                      Optional ByVal xDataOp As clsDataOperation = Nothing, _
                                      Optional ByVal blnApplyDateFilter As Boolean = True, _
                                      Optional ByVal strAdvanceJoinFilter As String = "" _
                                      )
        'Sohail (12 Oct 2021) - [blnApplyDateFilter, strAdvanceJoinFilter]
        'Sohail (03 May 2018) - [xDataOp]
        'Shani(24-Aug-2015) -- End
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dicShift As New Dictionary(Of String, Integer)
        Dim dicDates As New Dictionary(Of String, String) '(Dates , Dates) 'Sohail (28 Oct 2017)
        'Dim arr As ArrayList
        Dim intShiftUnkId As Integer = 0

        Try
            'Sohail (17 Jan 2014) -- Start
            'Enhancement - Oman
            intWorkingDays = 0
            dblWorkingHours = 0
            'Sohail (17 Jan 2014) -- End


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
            'dtTable = Get_List("Shift", dtStartDate, dtEndDate, intEmployeeID)
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'dtTable = Get_List(xDatabaseName, _
            '                   xUserUnkid, _
            '                   xYearUnkid, _
            '                   xCompanyUnkid, _
            '                   xUserModeSetting, "Shift", _
            '                   dtStartDate, _
            '                   dtEndDate, _
            '                   intEmployeeID)
            dtTable = Get_List(xDatabaseName, _
                               xUserUnkid, _
                               xYearUnkid, _
                               xCompanyUnkid, _
                               xUserModeSetting, "Shift", _
                               dtStartDate, _
                               dtEndDate, _
                              intEmployeeID, , , , objDataOperation, blnApplyDateFilter, strAdvanceJoinFilter)
            'Sohail (12 Oct 2021) - [blnApplyDateFilter, strAdvanceJoinFilter]
            'Sohail (03 May 2018) -- End
            'Shani(24-Aug-2015) -- End
            'Sohail (01 Nov 2019) -- Start
            'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
            'If dtTable.Select("effectivedate = '" & dtStartDate & "' ").Length <= 0 Then
            If dtTable.Select("streffectivedate = '" & eZeeDate.convertDate(dtStartDate) & "' ").Length <= 0 Then
                'Sohail (01 Nov 2019) -- End
                Dim dRow As DataRow = dtTable.NewRow
                'Sohail (01 Nov 2019) -- Start
                'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
                'dRow.Item("effectivedate") = dtStartDate.Date
                dRow.Item("streffectivedate") = eZeeDate.convertDate(dtStartDate)
                'Sohail (01 Nov 2019) -- End
                dRow.Item("shiftunkid") = GetEmployee_Current_ShiftId(dtStartDate, intEmployeeID)
                dtTable.Rows.Add(dRow)
            End If
            'Sohail (01 Nov 2019) -- Start
            'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
            'dtTable = New DataView(dtTable, "", "effectivedate", DataViewRowState.CurrentRows).ToTable
            dtTable = New DataView(dtTable, "", "streffectivedate", DataViewRowState.CurrentRows).ToTable
            'Sohail (01 Nov 2019) -- End

            For Each dtRow As DataRow In dtTable.Rows
                'Sohail (01 Nov 2019) -- Start
                'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
                'If dicShift.ContainsKey(eZeeDate.convertDate(CDate(dtRow.Item("effectivedate")))) = False Then
                If dicShift.ContainsKey(dtRow.Item("streffectivedate").ToString) = False Then
                    'Sohail (01 Nov 2019) -- End
                    'arr = New ArrayList
                    'arr.Add(CInt(dtRow.Item("shiftunkid"))) 'Shift ID
                    'arr.Add(0) 'Days
                    'arr.Add(0) 'Hours
                    'Sohail (01 Nov 2019) -- Start
                    'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
                    'dicShift.Add(eZeeDate.convertDate(CDate(dtRow.Item("effectivedate"))), CInt(dtRow.Item("shiftunkid")))
                    dicShift.Add(dtRow.Item("streffectivedate").ToString, CInt(dtRow.Item("shiftunkid")))
                    'Sohail (01 Nov 2019) -- End
                End If
            Next

            'Sohail (28 Oct 2017) -- Start
            'TRA Enhancement - 70.1 - (Ref. Id - 67) - When employee is re-hired salary should be calculated as prorated salary from rehire date,Currently aruti is calculating full monthly salary.
            Dim strPrevDate As String = ""
            'Sohail (01 Nov 2019) -- Start
            'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
            'dtTable = New DataView(dtTable, "", "effectivedate DESC", DataViewRowState.CurrentRows).ToTable
            dtTable = New DataView(dtTable, "", "streffectivedate DESC", DataViewRowState.CurrentRows).ToTable
            'Sohail (01 Nov 2019) -- End
            For Each dtRow As DataRow In dtTable.Rows
                'Sohail (01 Nov 2019) -- Start
                'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
                'If dicDates.ContainsKey(eZeeDate.convertDate(CDate(dtRow.Item("effectivedate")))) = False Then
                If dicDates.ContainsKey(dtRow.Item("streffectivedate").ToString) = False Then
                    'Sohail (01 Nov 2019) -- End
                    'arr = New ArrayList
                    'arr.Add(CInt(dtRow.Item("shiftunkid"))) 'Shift ID
                    'arr.Add(0) 'Days
                    'arr.Add(0) 'Hours
                    If strPrevDate = "" Then
                        'Sohail (01 Nov 2019) -- Start
                        'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
                        'dicDates.Add(eZeeDate.convertDate(CDate(dtRow.Item("effectivedate"))), eZeeDate.convertDate(dtEndDate))
                        dicDates.Add(dtRow.Item("streffectivedate").ToString, eZeeDate.convertDate(dtEndDate))
                        'Sohail (01 Nov 2019) -- End
                    Else
                        'Sohail (01 Nov 2019) -- Start
                        'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
                        'dicDates.Add(eZeeDate.convertDate(CDate(dtRow.Item("effectivedate"))), strPrevDate)
                        dicDates.Add(dtRow.Item("streffectivedate").ToString, strPrevDate)
                        'Sohail (01 Nov 2019) -- End
                    End If
                End If
                'Sohail (01 Nov 2019) -- Start
                'Hill Packaging Issue # 0004252 : Employees having wrong payable days.
                'strPrevDate = eZeeDate.convertDate(CDate(dtRow.Item("effectivedate")).AddDays(-1))
                strPrevDate = eZeeDate.convertDate(eZeeDate.convertDate(dtRow.Item("streffectivedate").ToString).AddDays(-1))
                'Sohail (01 Nov 2019) -- End
            Next
            'Sohail (28 Oct 2017) -- End

            Dim objShifttran As New clsshift_tran
            Dim dtDate As Date = dtStartDate
            Dim dtEnd As Date = dtEndDate

            'Sohail (28 Oct 2017) -- Start
            'TRA Enhancement - 70.1 - (Ref. Id - 67) - When employee is re-hired salary should be calculated as prorated salary from rehire date,Currently aruti is calculating full monthly salary.
            'While dtDate <= dtEnd
            '    If dicShift.ContainsKey(eZeeDate.convertDate(dtDate)) Then
            '        intShiftUnkId = dicShift.Item(eZeeDate.convertDate(dtDate))
            '        If intShiftUnkId <= 0 Then
            '            For Each pair In dicShift
            '                If pair.Value > 0 Then
            '                    intShiftUnkId = pair.Value
            '                    Exit For
            '                End If
            '            Next
            '        End If

            '        objShifttran.GetShiftTran(intShiftUnkId)
            '    End If

            '    Dim intDay As Integer = GetWeekDayNumber(dtDate.DayOfWeek.ToString)
            '    Dim drRow() As DataRow = objShifttran._dtShiftday.Select("dayid = " & intDay & " AND isweekend = 0")
            '    If drRow.Length > 0 Then
            '        intWorkingDays += 1
            '        dblWorkingHours += CInt(drRow(0).Item("workinghrsinsec"))
            '    End If

            '    dtDate = dtDate.AddDays(1)
            'End While
            For Each pair In dicDates
                Dim dtS As Date = eZeeDate.convertDate(pair.Key)
                Dim dtE As Date = eZeeDate.convertDate(pair.Value)
                'Dim arrDt() As Date = Enumerable.Range(0, 1 + dtE.Subtract(dtS).Days).Select(Function(x) dtS.AddDays(x)).ToArray

                intShiftUnkId = dicShift.Item(eZeeDate.convertDate(dtS))
                    If intShiftUnkId <= 0 Then
                    For Each pr In dicShift
                        If pr.Value > 0 Then
                            intShiftUnkId = pr.Value
                                Exit For
                            End If
                        Next
                    End If

                'Sohail (03 May 2018) -- Start
                'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                'objShifttran.GetShiftTran(intShiftUnkId)
                objShifttran.GetShiftTran(intShiftUnkId, objDataOperation)
                'Sohail (03 May 2018) -- End

                For Each dr As DataRow In objShifttran._dtShiftday.Select("isweekend = 0")
                    Dim intCount As Integer = CountNoOfWeekDays(CInt(dr.Item("dayid")), dtS, dtE)

                    If intCount > 0 Then
                        intWorkingDays += intCount
                        dblWorkingHours += (intCount * CInt(dr.Item("workinghrsinsec")))
                End If

                Next
            Next
            'Sohail (28 Oct 2017) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetWorkingDaysAndHours; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
    'Public Sub GetWeekendDaysAndHours(ByVal intEmployeeID As Integer, ByVal dtStartDate As Date, ByVal dtEndDate As Date, ByRef intWeekendDays As Integer, ByRef dblWeekendHours As Double)
    Public Sub GetWeekendDaysAndHours(ByVal xDatabaseName As String, _
                                      ByVal xUserUnkid As Integer, _
                                      ByVal xYearUnkid As Integer, _
                                      ByVal xCompanyUnkid As Integer, _
                                      ByVal xUserModeSetting As String, _
                                      ByVal intEmployeeID As Integer, _
                                      ByVal dtStartDate As Date, _
                                      ByVal dtEndDate As Date, _
                                      ByRef intWeekendDays As Integer, _
                                      ByRef dblWeekendHours As Double, _
                                      Optional ByVal xDataOp As clsDataOperation = Nothing, _
                                      Optional ByVal blnApplyDateFilter As Boolean = True, _
                                      Optional ByVal strAdvanceJoinFilter As String = "" _
                                      )
        'Sohail (12 Oct 2021) - [blnApplyDateFilter, strAdvanceJoinFilter]
        'Sohail (03 May 2018) - [xDataOp]
        'Shani(24-Aug-2015) -- End

        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dicShift As New Dictionary(Of String, Integer)
        Dim intShiftUnkId As Integer = 0
        Dim intTotalDays As Integer = 0
        Dim dblTotalHours As Double = 0
        Try
            intWeekendDays = 0
            dblWeekendHours = 0


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
            'dtTable = Get_List("Shift", dtStartDate, dtEndDate, intEmployeeID)
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'dtTable = Get_List(xDatabaseName, _
            '                   xUserUnkid, _
            '                   xYearUnkid, _
            '                   xCompanyUnkid, _
            '                   xUserModeSetting, "Shift", _
            '                   dtStartDate, _
            '                   dtEndDate, intEmployeeID)
            dtTable = Get_List(xDatabaseName, _
                               xUserUnkid, _
                               xYearUnkid, _
                               xCompanyUnkid, _
                               xUserModeSetting, "Shift", _
                               dtStartDate, _
                               dtEndDate, intEmployeeID, , , , xDataOp, blnApplyDateFilter, strAdvanceJoinFilter)
            'Sohail (12 Oct 2021) - [blnApplyDateFilter, strAdvanceJoinFilter]
            'Sohail (03 May 2018) -- End
            'Shani(24-Aug-2015) -- End
            'Sohail (03 Nov 2015) -- Start
            'Issue - Date comparision issue causing of adding one more days in total days.
            'If dtTable.Select("streffectivedate = '" & dtStartDate & "' ").Length <= 0 Then
            If dtTable.Select("streffectivedate = '" & eZeeDate.convertDate(dtStartDate) & "' ").Length <= 0 Then
                'Sohail (03 Novt 2015) -- End
                Dim dRow As DataRow = dtTable.NewRow
                dRow.Item("streffectivedate") = eZeeDate.convertDate(dtStartDate)
                dRow.Item("shiftunkid") = GetEmployee_Current_ShiftId(dtStartDate, intEmployeeID)
                dtTable.Rows.Add(dRow)
            End If
            dtTable = New DataView(dtTable, "", "streffectivedate", DataViewRowState.CurrentRows).ToTable

            Dim objShifttran As New clsshift_tran
            Dim dtRow As DataRow
            dtTable.Columns.Add("enddate", System.Type.GetType("System.String")).DefaultValue = ""

            For i As Integer = 0 To dtTable.Rows.Count - 1
                dtRow = dtTable.Rows(i)
                If CInt(dtRow.Item("shiftunkid")) = 0 AndAlso dtTable.Rows.Count > 1 Then
                    dtRow.Item("shiftunkid") = CInt(dtTable.Rows(1).Item("shiftunkid")) 'To assign first shift of apponited date if employee joins in between period
                    dtRow.AcceptChanges()
                End If

                If i > 0 Then
                    dtTable.Rows(i - 1).Item("enddate") = eZeeDate.convertDate(eZeeDate.convertDate(dtRow.Item("streffectivedate").ToString).AddDays(-1))
                End If
            Next
            dtTable.Rows(dtTable.Rows.Count - 1).Item("enddate") = eZeeDate.convertDate(dtEndDate)

            For i As Integer = 0 To dtTable.Rows.Count - 1
                dtRow = dtTable.Rows(i)
                'Sohail (03 May 2018) -- Start
                'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                'GetShiftWeekendDaysHours(CInt(dtRow.Item("shiftunkid")), eZeeDate.convertDate(dtRow.Item("streffectivedate").ToString), eZeeDate.convertDate(dtRow.Item("enddate").ToString), intTotalDays, dblTotalHours)
                GetShiftWeekendDaysHours(CInt(dtRow.Item("shiftunkid")), eZeeDate.convertDate(dtRow.Item("streffectivedate").ToString), eZeeDate.convertDate(dtRow.Item("enddate").ToString), intTotalDays, dblTotalHours, xDataOp)
                'Sohail (03 May 2018) -- End
                intWeekendDays += intTotalDays
                dblWeekendHours += dblTotalHours
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetWorkingDaysAndHours; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
    End Sub

    Public Sub GetShiftWeekendDaysHours(ByVal intShiftID As Integer, ByVal dtStartDate As Date, ByVal dtEndDate As Date, ByRef intWeekendDays As Integer, ByRef dblWeekendHours As Double, Optional ByVal xDataOp As clsDataOperation = Nothing)
        'Sohail (03 May 2018) - [xDataOp]
        Dim strQ As String = ""
        Dim dsList As DataSet
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try

            strQ = "WITH    MYCTE " & _
                              "AS ( SELECT   CAST(@startdate AS DATETIME) DateValue " & _
                                   "UNION ALL " & _
                                   "SELECT   DateValue + 1 " & _
                                   "FROM     MYCTE " & _
                                   "WHERE    DateValue + 1 <= @enddate " & _
                                 ") " & _
                        "SELECT  COUNT(workinghrs) AS TotalDays  " & _
                              ", ISNULL(SUM(workinghrs), 0) AS TotalHours " & _
                        "FROM    tnashift_tran " & _
                                "LEFT JOIN MYCTE ON dayid = (DATEPART(WEEKDAY, DateValue) - 1) " & _
                        "WHERE   shiftunkid = @shiftunkid " & _
                                "AND isweekend = 1 " & _
                    "OPTION  ( MAXRECURSION 0 ) "

            objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtStartDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtEndDate))
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intShiftID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intWeekendDays = CInt(dsList.Tables("List").Rows(0).Item("TotalDays"))
                dblWeekendHours = CDbl(dsList.Tables("List").Rows(0).Item("TotalHours"))
            Else
                intWeekendDays = 0
                dblWeekendHours = 0
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetShiftWeekendDaysHours; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
    'Public Sub GetPHDaysAndHours(ByVal intEmployeeID As Integer, ByVal dtStartDate As Date, ByVal dtEndDate As Date, ByRef intPHDays As Integer, ByRef dblPHHours As Double)
    Public Sub GetPHDaysAndHours(ByVal xDatabaseName As String, _
                                 ByVal xUserUnkid As Integer, _
                                 ByVal xYearUnkid As Integer, _
                                 ByVal xCompanyUnkid As Integer, _
                                 ByVal xUserModeSetting As String, _
                                 ByVal intEmployeeID As Integer, _
                                 ByVal dtStartDate As Date, _
                                 ByVal dtEndDate As Date, _
                                 ByRef intPHDays As Integer, _
                                 ByRef dblPHHours As Double, _
                                 Optional ByVal xDataOp As clsDataOperation = Nothing, _
                                 Optional ByVal blnApplyDateFilter As Boolean = True, _
                                 Optional ByVal strAdvanceJoinFilter As String = "" _
                                 )
        'Sohail (12 Oct 2021) - [blnApplyDateFilter, strAdvanceJoinFilter]
        'Sohail (03 May 2018) - [xDataOp]
        'Shani(24-Aug-2015) -- End

        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dicShift As New Dictionary(Of String, Integer)
        Dim intShiftUnkId As Integer = 0
        Dim intTotalDays As Integer = 0
        Dim dblTotalHours As Double = 0
        Try
            intPHDays = 0
            dblPHHours = 0


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
            'dtTable = Get_List("Shift", dtStartDate, dtEndDate, intEmployeeID)
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'dtTable = Get_List(xDatabaseName, _
            '                   xUserUnkid, _
            '                   xYearUnkid, _
            '                   xCompanyUnkid, _
            '                   xUserModeSetting, _
            '                   "Shift", _
            '                   dtStartDate, _
            '                   dtEndDate, _
            '                   intEmployeeID)
            dtTable = Get_List(xDatabaseName, _
                               xUserUnkid, _
                               xYearUnkid, _
                               xCompanyUnkid, _
                               xUserModeSetting, _
                               "Shift", _
                               dtStartDate, _
                               dtEndDate, _
                               intEmployeeID, , , , _
                               xDataOp, blnApplyDateFilter, strAdvanceJoinFilter)
            'Sohail (12 Oct 2021) - [blnApplyDateFilter, strAdvanceJoinFilter]
            'Sohail (03 May 2018) -- End
            'Shani(24-Aug-2015) -- End
            If dtTable.Select("streffectivedate = '" & eZeeDate.convertDate(dtStartDate) & "' ").Length <= 0 Then
                Dim dRow As DataRow = dtTable.NewRow
                dRow.Item("streffectivedate") = eZeeDate.convertDate(dtStartDate)
                dRow.Item("shiftunkid") = GetEmployee_Current_ShiftId(dtStartDate, intEmployeeID)
                dtTable.Rows.Add(dRow)
            End If
            dtTable = New DataView(dtTable, "", "streffectivedate", DataViewRowState.CurrentRows).ToTable

            Dim objShifttran As New clsshift_tran
            Dim dtRow As DataRow
            dtTable.Columns.Add("enddate", System.Type.GetType("System.String")).DefaultValue = ""

            For i As Integer = 0 To dtTable.Rows.Count - 1
                dtRow = dtTable.Rows(i)
                If CInt(dtRow.Item("shiftunkid")) = 0 AndAlso dtTable.Rows.Count > 1 Then
                    dtRow.Item("shiftunkid") = CInt(dtTable.Rows(1).Item("shiftunkid")) 'To assign first shift of apponited date if employee joins in between period
                    dtRow.AcceptChanges()
                End If

                If i > 0 Then
                    dtTable.Rows(i - 1).Item("enddate") = eZeeDate.convertDate(eZeeDate.convertDate(dtRow.Item("streffectivedate").ToString).AddDays(-1))
                End If
            Next
            dtTable.Rows(dtTable.Rows.Count - 1).Item("enddate") = eZeeDate.convertDate(dtEndDate)

            For i As Integer = 0 To dtTable.Rows.Count - 1
                dtRow = dtTable.Rows(i)
                'Sohail (03 May 2018) -- Start
                'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                'GetShiftPHDaysHours(CInt(dtRow.Item("shiftunkid")), intEmployeeID, eZeeDate.convertDate(dtRow.Item("streffectivedate").ToString), eZeeDate.convertDate(dtRow.Item("enddate").ToString), intTotalDays, dblTotalHours)
                GetShiftPHDaysHours(CInt(dtRow.Item("shiftunkid")), intEmployeeID, eZeeDate.convertDate(dtRow.Item("streffectivedate").ToString), eZeeDate.convertDate(dtRow.Item("enddate").ToString), intTotalDays, dblTotalHours, xDataOp)
                'Sohail (03 May 2018) -- End
                intPHDays += intTotalDays
                dblPHHours += dblTotalHours
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPHDaysAndHours; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
    End Sub

    Public Sub GetShiftPHDaysHours(ByVal intShiftID As Integer, ByVal intEmployeeID As Integer, ByVal dtStartDate As Date, ByVal dtEndDate As Date, ByRef intPHDays As Integer, ByRef dblPHHours As Double, Optional ByVal xDataOp As clsDataOperation = Nothing)
        'Sohail (03 May 2018) - [xDataOp]
        Dim strQ As String = ""
        Dim dsList As DataSet
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try

            strQ = "SELECT  COUNT(workinghrs) AS TotalDays  " & _
                          ", ISNULL(SUM(workinghrs), 0) AS TotalHours " & _
                    "FROM    lvemployee_holiday " & _
                            "JOIN lvholiday_master ON lvholiday_master.holidayunkid = lvemployee_holiday.holidayunkid " & _
                            "JOIN tnashift_tran ON dayid = (DATEPART(WEEKDAY, holidaydate) - 1) " & _
                    "WHERE   isactive = 1 " & _
                            "AND employeeunkid = @employeeunkid " & _
                            "AND shiftunkid = @shiftunkid " & _
                            "AND isweekend = 0 " & _
                            "AND CONVERT(CHAR(8), holidaydate, 112) BETWEEN @startdate AND @enddate "

            objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtStartDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtEndDate))
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intShiftID)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intPHDays = CInt(dsList.Tables("List").Rows(0).Item("TotalDays"))
                dblPHHours = CDbl(dsList.Tables("List").Rows(0).Item("TotalHours"))
            Else
                intPHDays = 0
                dblPHHours = 0
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetShiftPHDaysHours; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
    End Sub


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
    'Public Sub GetTotalWorkingHours(ByVal intEmployeeID As Integer, ByVal dtStartDate As Date, ByVal dtEndDate As Date, ByRef intWorkingDays As Integer, ByRef dblWorkingHours As Double)
    Public Sub GetTotalWorkingHours(ByVal xDatabaseName As String, _
                                    ByVal xUserUnkid As Integer, _
                                    ByVal xYearUnkid As Integer, _
                                    ByVal xCompanyUnkid As Integer, _
                                    ByVal xUserModeSetting As String, _
                                    ByVal intEmployeeID As Integer, _
                                    ByVal dtStartDate As Date, _
                                    ByVal dtEndDate As Date, _
                                    ByRef intWorkingDays As Integer, _
                                    ByRef dblWorkingHours As Double, _
                                    Optional ByVal isIncludeWeekend As Boolean = True, _
                                    Optional ByVal xDataOp As clsDataOperation = Nothing, _
                                    Optional ByVal blnApplyDateFilter As Boolean = True, _
                                    Optional ByVal strAdvanceJoinFilter As String = "" _
                                    )
        'Sohail (12 Oct 2021) - [blnApplyDateFilter, strAdvanceJoinFilter]
        'Pinkal (28-Mar-2018) --'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.[Optional ByVal isIncludeWeekend As Boolean = True]
        'Sohail (03 May 2018) - [xDataOp]
        'Shani(24-Aug-2015) -- End

        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dicShift As New Dictionary(Of String, Integer)
        Dim intShiftUnkId As Integer = 0
        Dim intTotalDays As Integer = 0
        Dim dblTotalHours As Double = 0
        Try
            intWorkingDays = 0
            dblWorkingHours = 0


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
            'dtTable = Get_List("Shift", dtStartDate, dtEndDate, intEmployeeID)
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'dtTable = Get_List(xDatabaseName, _
            '                   xUserUnkid, _
            '                   xYearUnkid, _
            '                   xCompanyUnkid, _
            '                   xUserModeSetting, _
            '                   "Shift", _
            '                   dtStartDate, _
            '                   dtEndDate, _
            '                   intEmployeeID)
            dtTable = Get_List(xDatabaseName, _
                               xUserUnkid, _
                               xYearUnkid, _
                               xCompanyUnkid, _
                               xUserModeSetting, _
                               "Shift", _
                               dtStartDate, _
                               dtEndDate, _
                               intEmployeeID, , , , xDataOp, blnApplyDateFilter, strAdvanceJoinFilter)
            'Sohail (12 Oct 2021) - [blnApplyDateFilter, strAdvanceJoinFilter]
            'Sohail (03 May 2018) -- End
            'Shani(24-Aug-2015) -- End
            If dtTable.Select("streffectivedate = '" & eZeeDate.convertDate(dtStartDate) & "' ").Length <= 0 Then
                Dim dRow As DataRow = dtTable.NewRow
                dRow.Item("streffectivedate") = eZeeDate.convertDate(dtStartDate)
                dRow.Item("shiftunkid") = GetEmployee_Current_ShiftId(dtStartDate, intEmployeeID)
                dtTable.Rows.Add(dRow)
            End If
            dtTable = New DataView(dtTable, "", "streffectivedate", DataViewRowState.CurrentRows).ToTable

            Dim objShifttran As New clsshift_tran
            Dim dtRow As DataRow
            dtTable.Columns.Add("enddate", System.Type.GetType("System.String")).DefaultValue = ""

            For i As Integer = 0 To dtTable.Rows.Count - 1
                dtRow = dtTable.Rows(i)
                If CInt(dtRow.Item("shiftunkid")) = 0 AndAlso dtTable.Rows.Count > 1 Then
                    dtRow.Item("shiftunkid") = CInt(dtTable.Rows(1).Item("shiftunkid")) 'To assign first shift of apponited date if employee joins in between period
                    dtRow.AcceptChanges()
                End If

                If i > 0 Then
                    dtTable.Rows(i - 1).Item("enddate") = eZeeDate.convertDate(eZeeDate.convertDate(dtRow.Item("streffectivedate").ToString).AddDays(-1))
                End If
            Next
            dtTable.Rows(dtTable.Rows.Count - 1).Item("enddate") = eZeeDate.convertDate(dtEndDate)

            For i As Integer = 0 To dtTable.Rows.Count - 1
                dtRow = dtTable.Rows(i)
                'Sohail (03 May 2018) -- Start
                'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
                'GetShiftTotalWorkingHours(CInt(dtRow.Item("shiftunkid")), intEmployeeID, eZeeDate.convertDate(dtRow.Item("streffectivedate").ToString), eZeeDate.convertDate(dtRow.Item("enddate").ToString), intTotalDays, dblTotalHours)
                GetShiftTotalWorkingHours(CInt(dtRow.Item("shiftunkid")), intEmployeeID, eZeeDate.convertDate(dtRow.Item("streffectivedate").ToString), eZeeDate.convertDate(dtRow.Item("enddate").ToString), intTotalDays, dblTotalHours, isIncludeWeekend, xDataOp)
                'Sohail (03 May 2018) -- End
                intWorkingDays += intTotalDays
                dblWorkingHours += dblTotalHours
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalWorkingHours; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
    End Sub

    Public Sub GetShiftTotalWorkingHours(ByVal intShiftID As Integer, ByVal intEmployeeID As Integer, ByVal dtStartDate As Date, ByVal dtEndDate As Date, ByRef intPHDays As Integer, ByRef dblPHHours As Double, Optional ByVal isIncludeWeekend As Boolean = True, Optional ByVal xDataOp As clsDataOperation = Nothing)
         'Pinkal (28-Mar-2018) --'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.[Optional ByVal isIncludeWeekend As Boolean = True]
         'Sohail (03 May 2018) - [xDataOp]
        Dim strQ As String = ""
        Dim dsList As DataSet
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.

            strQ = "WITH    MYCTE " & _
                              "AS ( SELECT   CAST(@startdate AS DATETIME) DateValue " & _
                                   "UNION ALL " & _
                                   "SELECT   DateValue + 1 " & _
                                   "FROM     MYCTE " & _
                                   "WHERE    DateValue + 1 <= @enddate " & _
                                 ") " & _
                        "SELECT  COUNT(workinghrs) AS TotalDays  " & _
                              ", ISNULL(SUM(workinghrs), 0) AS TotalHours " & _
                        "FROM    tnashift_tran " & _
                                "LEFT JOIN MYCTE ON dayid = (DATEPART(WEEKDAY, DateValue) - 1) " & _
                        "WHERE   shiftunkid = @shiftunkid "

            If isIncludeWeekend = False Then
                strQ &= " AND tnashift_tran.isweekend = 0 AND  (DATEPART(WEEKDAY, DateValue) - 1) IS NOT NULL "
            End If

            strQ &= " OPTION  ( MAXRECURSION 0 ) "

            'Pinkal (28-Mar-2018) -- End

            objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtStartDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtEndDate))
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intShiftID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intPHDays = CInt(dsList.Tables("List").Rows(0).Item("TotalDays"))
                dblPHHours = CDbl(dsList.Tables("List").Rows(0).Item("TotalHours"))
            Else
                intPHDays = 0
                dblPHHours = 0
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetShiftTotalWorkingHours; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
    End Sub

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
    'Public Function GetEmployeeDistinctShift(ByVal intEmployeeID As Integer, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime) As ArrayList
    Public Function GetEmployeeDistinctShift(ByVal xDatabaseName As String, _
                                             ByVal xUserUnkid As Integer, _
                                             ByVal xYearUnkid As Integer, _
                                             ByVal xCompanyUnkid As Integer, _
                                             ByVal xUserModeSetting As String, _
                                             ByVal intEmployeeID As Integer, _
                                             ByVal dtStartDate As DateTime, _
                                             ByVal dtEndDate As DateTime, _
                                             Optional ByVal xDataOp As clsDataOperation = Nothing, _
                                             Optional ByVal blnApplyDateFilter As Boolean = True, _
                                             Optional ByVal strAdvanceJoinFilter As String = "" _
                                             ) As ArrayList
        'Sohail (12 Oct 2021) - [blnApplyDateFilter, strAdvanceJoinFilter]
        'Sohail (03 May 2018) - [xDataOp]
        'Shani(24-Aug-2015) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End
        Dim dtTable As DataTable = Nothing
        Dim arrShift As New ArrayList

        Try


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'To Change GetEmployeeShift_PolicyList Method set Active Employee Query wise.
            'dtTable = Get_List("Shift", dtStartDate, dtEndDate, intEmployeeID)
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'dtTable = Get_List(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, "Shift", dtStartDate, dtEndDate, intEmployeeID)
            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            'dtTable = Get_List(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, "Shift", dtStartDate, dtEndDate, intEmployeeID, , , , objDataOperation)
            dtTable = Get_List(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xUserModeSetting, "Shift", dtStartDate, dtEndDate, intEmployeeID, , , , objDataOperation, blnApplyDateFilter, strAdvanceJoinFilter)
            'Sohail (12 Oct 2021) -- End
            'Sohail (03 May 2018) -- End
            'Shani(24-Aug-2015) -- End
            If dtTable.Select("effectivedate = '" & dtStartDate & "' ").Length <= 0 Then
                Dim dRow As DataRow = dtTable.NewRow
                dRow.Item("effectivedate") = dtStartDate.Date
                dRow.Item("shiftunkid") = GetEmployee_Current_ShiftId(dtStartDate, intEmployeeID)
                dtTable.Rows.Add(dRow)
            End If
            dtTable = New DataView(dtTable, "", "effectivedate", DataViewRowState.CurrentRows).ToTable(True, "shiftunkid")

            For Each dtRow As DataRow In dtTable.Rows
                arrShift.Add(CInt(dtRow.Item("shiftunkid")))
            Next

            Return arrShift
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeShift; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
    End Function

    Public Function GetEmployeeShift(ByVal iDate As DateTime, ByVal iEmployId As Integer) As DataTable
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try


            'Pinkal (25-Mar-2019) -- Start
            'Enhancement - Working on Leave Changes for NMB.

            'strQ = "SELECT TOP 1 hremployee_shift_tran.shiftunkid " & _
            '          ", tnashift_master.shiftname " & _
            '          " FROM hremployee_shift_tran " & _
            '          " JOIN tnashift_master ON hremployee_shift_tran.shiftunkid = tnashift_master.shiftunkid AND tnashift_master.isactive = 1 " & _
            '          "  WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(iDate) & "' " & _
            '          " AND employeeunkid = '" & iEmployId & "' AND effectivedate IS NOT NULL ORDER BY CONVERT(CHAR(8),effectivedate,112) DESC "

            strQ = "SELECT TOP 1 hremployee_shift_tran.shifttranunkid, hremployee_shift_tran.shiftunkid " & _
                      ", tnashift_master.shiftname " & _
                      " FROM hremployee_shift_tran " & _
                      " JOIN tnashift_master ON hremployee_shift_tran.shiftunkid = tnashift_master.shiftunkid AND tnashift_master.isactive = 1 " & _
                      "  WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(iDate) & "' " & _
                      " AND employeeunkid = '" & iEmployId & "' AND effectivedate IS NOT NULL ORDER BY CONVERT(CHAR(8),effectivedate,112) DESC "


            'Pinkal (25-Mar-2019) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeShift; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function isUsed(ByVal iEmpId As Integer, ByVal iDate As DateTime, ByRef iMessage As String, ByVal iShiftID As Integer, ByVal strDatabaseName As String) As String
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT 1 FROM hremployee_shift_tran WHERE isvoid = 0 AND employeeunkid = '" & iEmpId & "' "

            If objDataOperation.RecordCount(StrQ) = 1 Then
                iMessage = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot delete this shift." & vbCrLf & _
                                                   "Reasons : " & vbCrLf & _
                                                   "1]. There is only one shift assigned to the employee." & vbCrLf & _
                                                   "2]. Period is already closed." & vbCrLf & _
                                                   "3]. There is a transaction in Time and Attendance for the selected date." & vbCrLf & _
                                                   "Due to this checked employee will be skipped from the operation.")
                GoTo iMsg
            End If

            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ConfigParameter._Object._IsArutiDemo Then
                Dim objMaster As New clsMasterData
                Dim iPeriod As Integer = 0
                iPeriod = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, iDate)

                If iPeriod > 0 Then
                    Dim objPrd As New clscommom_period_Tran
                    objPrd._Periodunkid(strDatabaseName) = iPeriod
                    If objPrd._Statusid = 2 Then
                        iMessage = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot delete this shift." & vbCrLf & _
                                                       "Reasons : " & vbCrLf & _
                                                       "1]. There is only one shift assigned to the employee." & vbCrLf & _
                                                       "2]. Period is already closed." & vbCrLf & _
                                                       "3]. There is a transaction in Time and Attendance for the selected date." & vbCrLf & _
                                                       "Due to this checked employee will be skipped from the operation.")
                        GoTo iMsg
                    End If


                    Dim objLeaveTran As New clsTnALeaveTran

                    Dim mdtTnADate As DateTime = Nothing
                    If objPrd._TnA_EndDate.Date < iDate.Date Then
                        mdtTnADate = iDate.Date
                    Else
                        mdtTnADate = objPrd._TnA_EndDate.Date
                    End If

                    If objLeaveTran.IsPayrollProcessDone(objPrd._Periodunkid(strDatabaseName), iEmpId.ToString(), mdtTnADate.Date, enModuleReference.TnA) Then
                        iMessage = Language.getMessage(mstrModuleName, 3, "Sorry, you cannot remove assiged shift from checked employee(s).Reason:Payroll Process already done for checked employee(s) for last date of current open period.")
                        GoTo iMsg
                    End If
                    objPrd = Nothing
                End If
                objMaster = Nothing
            End If



            'Pinkal (30-Oct-2013) -- Start
            'Enhancement : Oman Changes

            'StrQ = "SELECT 1 FROM tnalogin_summary WHERE employeeunkid = '" & iEmpId & "' AND CONVERT(CHAR(8),login_date,112) = '" & eZeeDate.convertDate(iDate).ToString & "' AND shiftunkid = "

            'dsList = objDataOperation.ExecQuery(StrQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'If dsList.Tables("List").Rows.Count > 0 Then
            '    iMessage = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot delete this shift." & vbCrLf & _
            '                                       "Reasons : " & vbCrLf & _
            '                                       "1]. There is only one shift assigned to the employee." & vbCrLf & _
            '                                       "2]. Period is already closed." & vbCrLf & _
            '                                       "3]. There is a transaction in Time and Attendance for the selected date." & vbCrLf & _
            '                                       "Due to this checked employee will be skipped from the operation.")
            '    GoTo iMsg
            'End If

            'Pinkal (30-Oct-2013) -- End

iMsg:       Return iMessage

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
            Return iMessage
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetCurrShiftEndDate(ByVal iEmployeeId As Integer, ByVal iShiftId As Integer, ByVal iDate As DateTime, ByVal objDataOperation As clsDataOperation) As Date
        Dim iRetDate As Date = Nothing
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT TOP 1 login_date FROM tnalogin_summary WHERE employeeunkid = '" & iEmployeeId & "' AND CONVERT(CHAR(8),login_date, 112) > '" & eZeeDate.convertDate(iDate).ToString & "' AND shiftunkid <> '" & iShiftId & "'  ORDER BY login_date ASC "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                iRetDate = CDate(dsList.Tables("List").Rows(0).Item("login_date")).Date.AddDays(-1)
            End If

            Return iRetDate
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCurrShiftEndDate", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'Pinkal (10-Aug-2016) -- Start
    'Enhancement - Implementing Status of Day Off and Suspension for Zanzibar Residence.

    Public Function ImportInsertEmployeeShift() As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            StrQ = "INSERT INTO hremployee_shift_tran ( " & _
                        "  employeeunkid " & _
                        ", shiftunkid " & _
                        ", isdefault " & _
                        ", userunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason" & _
                        ", effectivedate " & _
                   ") VALUES (" & _
                        "  @employeeunkid " & _
                        ", @shiftunkid " & _
                        ", @isdefault " & _
                        ", @userunkid " & _
                        ", @isvoid " & _
                        ", @voiduserunkid " & _
                        ", @voiddatetime " & _
                        ", @voidreason" & _
                        ", @effectivedate " & _
                   "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectiveDate.Date)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId.ToString)
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftID)
            objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserID)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            mintshifttranunkid = dsList.Tables(0).Rows(0)(0)


            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", mintEmployeeId, "hremployee_shift_tran", "shifttranunkid", mintshifttranunkid, 2, 1, , mintUserID) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: ImportInsertEmployeeShift; Module Name: " & mstrModuleName)
        End Try
    End Function

    'S.SANDEEP |29-MAR-2019| -- START
    'ENHANCEMENT : 0002803 {PAPAYE}.
    Public Function ImportUpdateEmployeeShift(ByVal xShiftTranId As Integer) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            StrQ = "UPDATE hremployee_shift_tran SET " & _
                   "  employeeunkid = @employeeunkid " & _
                   ", shiftunkid = @shiftunkid " & _
                   ", isdefault = @isdefault " & _
                   ", userunkid = @userunkid " & _
                   ", isvoid = @isvoid " & _
                   ", voiduserunkid = @voiduserunkid " & _
                   ", voiddatetime = @voiddatetime " & _
                   ", voidreason= @voidreason" & _
                   ", effectivedate = @effectivedate " & _
                   " WHERE shifttranunkid  = @shifttranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@shifttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xShiftTranId)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectiveDate.Date)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId.ToString)
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftID)
            objDataOperation.AddParameter("@isdefault", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserID)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", mintEmployeeId, "hremployee_shift_tran", "shifttranunkid", xShiftTranId, 2, 2, False, mintUserID) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: ImportUpdateEmployeeShift; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Function GetEmployeePreviousFutureShift(ByVal iDate As DateTime, ByVal iEmployId As Integer, Optional ByVal mblnFutureShift As Boolean = False) As DataTable
        Dim dtShift As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim iValue As Integer = 0
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT shifttranunkid " & _
                      ", employeeunkid " & _
                      ", shiftunkid " & _
                      ", isdefault " & _
                      ", effectivedate " & _
                      " FROM hremployee_shift_tran  " & _
                      " WHERE isvoid = 0 "

            If mblnFutureShift = False Then
                strQ &= " AND CONVERT(CHAR(8),effectivedate,112) < @effectivedate "
            Else
                strQ &= " AND CONVERT(CHAR(8),effectivedate,112) > @effectivedate "
            End If

            strQ &= " AND employeeunkid = @employeeunkid " & _
                        " AND effectivedate IS NOT NULL"


            If mblnFutureShift = False Then
                strQ &= " ORDER BY CONVERT(CHAR(8),effectivedate,112) DESC "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(iDate))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployId)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtShift = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeePreviousFutureShift; Module Name: " & mstrModuleName)
        End Try
        Return dtShift
    End Function
    'S.SANDEEP |29-MAR-2019| -- END


    'Pinkal (29-Jul-2019) -- Start
    'Enhancement - Working on Papaaye Shift Overriding feature On Employee Shift Assignment.
    Public Function GetShiftTranunkid(ByVal xDate As Date, ByVal xEmployeeId As Integer, Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim iValue As Integer = 0

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If


        Try
            strQ = "SELECT shifttranunkid " & _
                      ", employeeunkid " & _
                      ", shiftunkid " & _
                      ", isdefault " & _
                      ", effectivedate " & _
                      " FROM hremployee_shift_tran  " & _
                      " WHERE isvoid = 0 " & _
                      " AND CONVERT(CHAR(8),effectivedate,112) = @effectivedate " & _
                      " AND employeeunkid = @employeeunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                iValue = CInt(dsList.Tables(0).Rows(0)("shifttranunkid"))
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetShiftTranunkid; Module Name: " & mstrModuleName)
        End Try
        Return iValue
    End Function
    'Pinkal (29-Jul-2019) -- End



#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, you cannot delete this shift." & vbCrLf & _
                                                   "Reasons : " & vbCrLf & _
                                                   "1]. There is only one shift assigned to the employee." & vbCrLf & _
                                                   "2]. Period is already closed." & vbCrLf & _
                                                   "3]. There is a transaction in Time and Attendance for the selected date." & vbCrLf & _
                                                   "Due to this checked employee will be skipped from the operation.")
			Language.setMessage(mstrModuleName, 3, "Sorry, you cannot remove assiged shift from checked employee(s).Reason:Payroll Process already done for checked employee(s) for last date of current open period.")
			Language.setMessage(mstrModuleName, 1, "Sorry, you cannot delete this shift." & vbCrLf & _
                                                     "Reasons : " & vbCrLf & _
                                                     "1]. There is only one shift assigned to the employee." & vbCrLf & _
                                                     "2]. Period is already closed." & vbCrLf & _
                                                     "3]. There is a transaction in Time and Attendance for the selected date." & vbCrLf & _
                                                     "Due to this checked employee will be skipped from the operation.")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
