﻿'************************************************************************************************************************************
'Class Name : clsIdentity_tran.vb
'Purpose    :
'Date       :30/06/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsMembershipTran
#Region " Private Variables "
    Private Shared ReadOnly mstrModuleName As String = "clsMembershipTran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private mintEmployeeUnkid As Integer = -1
    Private mdtTran As DataTable

    'S.SANDEEP [ 12 OCT 2011 ] -- START
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    Private mintMembershipTranId As Integer = 0
    'S.SANDEEP [ 12 OCT 2011 ] -- END 

    'S.SANDEEP [ 17 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIsNewEmp As Boolean = False
    Private mdtAppointedDate As Date = Nothing
    'S.SANDEEP [ 17 OCT 2012 ] -- END

    'S.SANDEEP [14 APR 2015] -- START
    Private mintRehiretranunkid As Integer = 0
    'S.SANDEEP [14 APR 2015] -- END

    'Shani(06-APR-2016) -- Start
    'Enhancement : new form for Global Void Membership 
    Private mstrErrorMessage As String = ""
    'Shani(06-APR-2016) -- End 

    'S.SANDEEP [19 OCT 2016] -- START
    'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
    Private xDataOperation As clsDataOperation = Nothing
    'S.SANDEEP [19 OCT 2016] -- END

#End Region

#Region " Properties "
    Public Property _EmployeeUnkid() As Integer
        Get
            Return mintEmployeeUnkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeUnkid = value
            Call GetMembership_Tran()
        End Set
    End Property

    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    'S.SANDEEP [ 17 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _IsNewEmp() As Boolean
        Set(ByVal value As Boolean)
            mblnIsNewEmp = value
        End Set
    End Property

    Public WriteOnly Property _ADate() As Date
        Set(ByVal value As Date)
            mdtAppointedDate = value
        End Set
    End Property
    'S.SANDEEP [ 17 OCT 2012 ] -- END

    'S.SANDEEP [14 APR 2015] -- START
    Public WriteOnly Property _Rehiretranunkid() As Integer
        Set(ByVal value As Integer)
            mintRehiretranunkid = value
        End Set
    End Property
    'S.SANDEEP [14 APR 2015] -- END

    'Shani(06-APR-2016) -- Start
    'Enhancement : new form for Global Void Membership 
    Public ReadOnly Property _ErrorMessage() As String
        Get
            Return mstrErrorMessage
        End Get
    End Property
    'Shani(06-APR-2016) -- End 

    'S.SANDEEP [19 OCT 2016] -- START
    'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
    Public WriteOnly Property _xDataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOperation = value
        End Set
    End Property
    'S.SANDEEP [19 OCT 2016] -- END


    'S.SANDEEP |15-APR-2019| -- START
    Public ReadOnly Property _MembershipTranId() As Integer
        Get
            Return mintMembershipTranId
        End Get
    End Property
    'S.SANDEEP |15-APR-2019| -- END

#End Region

#Region " Contructor "

    Public Sub New()
        mdtTran = New DataTable("Membership")
        Dim dCol As DataColumn
        Try

            dCol = New DataColumn("membershiptranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("membership_categoryunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("membershipunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("membershipno")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("issue_date")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("start_date")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("expiry_date")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("remark")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            'Sohail (28 Dec 2010) -- Start
            dCol = New DataColumn("isactive")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)
            'Sohail (28 Dec 2010) -- End

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dCol = New DataColumn("catagory")
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("membership")
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

            'S.SANDEEP [ 27 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dCol = New DataColumn("emptrnheadid")
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("cotrnheadid")
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("effetiveperiodid")
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)
            'S.SANDEEP [ 27 AUG 2012 ] -- END

            'S.SANDEEP [ 17 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dCol = New DataColumn("isdeleted")
            dCol.DefaultValue = False
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ccategory")
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ccategoryid")
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)
            'S.SANDEEP [ 17 OCT 2012 ] -- END

            'S.SANDEEP [ 11 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dCol = New DataColumn("copyedslab")
            dCol.DefaultValue = False
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("overwriteslab")
            dCol.DefaultValue = False
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("overwritehead")
            dCol.DefaultValue = False
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)
            'S.SANDEEP [ 11 DEC 2012 ] -- END

            'S.SANDEEP [14 APR 2015] -- START
            dCol = New DataColumn("rehiretranunkid")
            dCol.DefaultValue = 0
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)
            'S.SANDEEP [14 APR 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Functions "
    Private Sub GetMembership_Tran()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim dRowID_Tran As DataRow
        Dim exForce As Exception
        Try

            objDataOperation = New clsDataOperation

            'Sohail (28 Dec 2010) -- Start
            'Changes : isactive field added.
            'strQ = "SELECT  membershiptranunkid " & _
            '            ",employeeunkid " & _
            '            ",membership_categoryunkid " & _
            '            ",membershipunkid " & _
            '            ",membershipno " & _
            '            ",issue_date " & _
            '            ",start_date " & _
            '            ",expiry_date " & _
            '            ",remark " & _
            '            ",'' As AUD " & _
            '        "FROM hremployee_meminfo_tran " & _
            '        "WHERE employeeunkid = @employeeunkid "


            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT  membershiptranunkid " & _
            '       ",employeeunkid " & _
            '       ",membership_categoryunkid " & _
            '       ",membershipunkid " & _
            '       ",membershipno " & _
            '       ",issue_date " & _
            '       ",start_date " & _
            '       ",expiry_date " & _
            '       ",remark " & _
            '       ",isactive " & _
            '       ",'' As AUD " & _
            '       "FROM hremployee_meminfo_tran " & _
            '       "WHERE employeeunkid = @employeeunkid " & _
            '       "AND isactive = 1 "

            'S.SANDEEP [ 17 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '       " hremployee_meminfo_tran.membershiptranunkid " & _
            '       ",hremployee_meminfo_tran.employeeunkid " & _
            '       ",hremployee_meminfo_tran.membership_categoryunkid " & _
            '       ",hremployee_meminfo_tran.membershipunkid " & _
            '       ",hremployee_meminfo_tran.membershipno " & _
            '       ",hremployee_meminfo_tran.issue_date " & _
            '       ",hremployee_meminfo_tran.start_date " & _
            '       ",hremployee_meminfo_tran.expiry_date " & _
            '       ",hremployee_meminfo_tran.remark " & _
            '       ",hremployee_meminfo_tran.isactive " & _
            '       ",'' As AUD " & _
            '       ",ISNULL(cfcommon_master.name,'') AS catagory " & _
            '       ",ISNULL(hrmembership_master.membershipname,'') AS membership " & _
            '       "FROM hremployee_meminfo_tran " & _
            '       "    LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
            '       "    LEFT JOIN cfcommon_master ON hremployee_meminfo_tran.membership_categoryunkid = cfcommon_master.masterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & "' " & _
            '       "WHERE hremployee_meminfo_tran.employeeunkid = @employeeunkid " & _
            '           "AND hremployee_meminfo_tran.isactive = 1 "

            strQ = "SELECT " & _
                       " hremployee_meminfo_tran.membershiptranunkid " & _
                       ",hremployee_meminfo_tran.employeeunkid " & _
                       ",hremployee_meminfo_tran.membership_categoryunkid " & _
                       ",hremployee_meminfo_tran.membershipunkid " & _
                       ",hremployee_meminfo_tran.membershipno " & _
                       ",hremployee_meminfo_tran.issue_date " & _
                       ",hremployee_meminfo_tran.start_date " & _
                       ",hremployee_meminfo_tran.expiry_date " & _
                       ",hremployee_meminfo_tran.remark " & _
                       ",hremployee_meminfo_tran.isactive " & _
                        ",'' As AUD " & _
                       ",ISNULL(cfcommon_master.name,'') AS catagory " & _
                       ",ISNULL(hrmembership_master.membershipname,'') AS membership " & _
                   "    ,CASE WHEN (hremployee_meminfo_tran.isactive = 1 AND hremployee_meminfo_tran.isdeleted = 0) THEN @Active " & _
                   "          WHEN (hremployee_meminfo_tran.isactive = 0 AND hremployee_meminfo_tran.isdeleted = 0) THEN @Inactive END AS ccategory " & _
                   "    ,CASE WHEN (hremployee_meminfo_tran.isactive = 1 AND hremployee_meminfo_tran.isdeleted = 0) THEN 1 " & _
                   "          WHEN (hremployee_meminfo_tran.isactive = 0 AND hremployee_meminfo_tran.isdeleted = 0) THEN 2 END AS ccategoryid " & _
                   "    ,ISNULL(hrmembership_master.emptranheadunkid,0) as emptranheadunkid  " & _
                   "    ,ISNULL(hrmembership_master.cotranheadunkid,0) as  cotranheadunkid " & _
                       ",hremployee_meminfo_tran.rehiretranunkid " & _
                    "FROM hremployee_meminfo_tran " & _
                       "LEFT JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                       "LEFT JOIN cfcommon_master ON hremployee_meminfo_tran.membership_categoryunkid = cfcommon_master.masterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & "' " & _
                   "WHERE hremployee_meminfo_tran.employeeunkid = @employeeunkid " & _
                       " AND hremployee_meminfo_tran.isdeleted = 0 " 'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END


            objDataOperation.AddParameter("@Active", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Active"))
            objDataOperation.AddParameter("@Inactive", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Inactive"))
            'S.SANDEEP [ 17 OCT 2012 ] -- END


            'S.SANDEEP [ 05 MARCH 2012 ] -- END



            'Sohail (28 Dec 2010) -- End

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    dRowID_Tran = mdtTran.NewRow()

                    dRowID_Tran.Item("membershiptranunkid") = .Item("membershiptranunkid")
                    dRowID_Tran.Item("employeeunkid") = .Item("employeeunkid")
                    dRowID_Tran.Item("membership_categoryunkid") = .Item("membership_categoryunkid")
                    dRowID_Tran.Item("membershipunkid") = .Item("membershipunkid")
                    dRowID_Tran.Item("membershipno") = .Item("membershipno")
                    dRowID_Tran.Item("issue_date") = .Item("issue_date")
                    dRowID_Tran.Item("start_date") = .Item("start_date")
                    dRowID_Tran.Item("expiry_date") = .Item("expiry_date")
                    dRowID_Tran.Item("remark") = .Item("remark")
                    dRowID_Tran.Item("isactive") = .Item("isactive") 'Sohail (28 Dec 2010)
                    dRowID_Tran.Item("AUD") = .Item("AUD")
                    dRowID_Tran.Item("catagory") = .Item("catagory")
                    dRowID_Tran.Item("membership") = .Item("membership")

                    'S.SANDEEP [ 17 OCT 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    dRowID_Tran.Item("ccategory") = .Item("ccategory")
                    dRowID_Tran.Item("ccategoryid") = .Item("ccategoryid")
                    dRowID_Tran.Item("emptrnheadid") = .Item("emptranheadunkid")
                    dRowID_Tran.Item("cotrnheadid") = .Item("cotranheadunkid")
                    'S.SANDEEP [ 17 OCT 2012 ] -- END

                    'S.SANDEEP [14 APR 2015] -- START
                    dRowID_Tran.Item("rehiretranunkid") = .Item("rehiretranunkid")
                    'S.SANDEEP [14 APR 2015] -- END

                    mdtTran.Rows.Add(dRowID_Tran)
                End With
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetMembership_Tran", mstrModuleName)
        End Try
    End Sub

    Public Function InsertUpdateDelete_MembershipTran(ByVal strDatabaseName As String _
                                                      , ByVal intUserUnkId As Integer _
                                                      , ByVal xYearUnkid As Integer _
                                                      , ByVal xCompanyUnkid As Integer _
                                                      , ByVal xPeriodStart As DateTime _
                                                      , ByVal xPeriodEnd As DateTime _
                                                      , ByVal xUserModeSetting As String _
                                                      , ByVal xOnlyApproved As Boolean _
                                                      , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                      , ByVal blnAllowToApproveEarningDeduction As Boolean _
                                                      , ByVal dtCurrentDateAndTime As Date _
                                                      , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                                      , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                                      , Optional ByVal strFilerString As String = "" _
                                                      ) As Boolean 'Sohail (23 Apr 2012) - [intUserUnkId]
        'Public Function InsertUpdateDelete_MembershipTran(Optional ByVal intUserUnkId As Integer = 0) As Boolean 'S.SANDEEP [14 APR 2015] -- START -- END
        'Sohail (21 Aug 2015) - [strDatabaseName, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter]

        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            'S.SANDEEP [14 APR 2015] -- START
            'objDataOperation = New clsDataOperation
            'objDataOperation.BindTransaction()
            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            End If
            'S.SANDEEP [14 APR 2015] -- END

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO hremployee_meminfo_tran ( " & _
                                            "  employeeunkid " & _
                                            ", membership_categoryunkid " & _
                                            ", membershipunkid " & _
                                            ", membershipno " & _
                                            ", issue_date " & _
                                            ", start_date " & _
                                            ", expiry_date " & _
                                            ", remark" & _
                                            ", isactive" & _
                                            ", isdeleted " & _
                                            ", rehiretranunkid " & _
                                       ") VALUES (" & _
                                            "  @employeeunkid " & _
                                            ", @membership_categoryunkid " & _
                                            ", @membershipunkid " & _
                                            ", @membershipno " & _
                                            ", @issue_date " & _
                                            ", @start_date " & _
                                            ", @expiry_date " & _
                                            ", @remark" & _
                                            ", @isactive" & _
                                            ", @isdeleted " & _
                                            ", @rehiretranunkid " & _
                                        "); SELECT @@identity" 'Sohail (28 Dec 2010)
                                'S.SANDEEP [ 17 OCT 2012 isdeleted ]
                                'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END

                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid.ToString)
                                objDataOperation.AddParameter("@membership_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("membership_categoryunkid").ToString)
                                objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("membershipunkid").ToString)
                                objDataOperation.AddParameter("@membershipno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("membershipno").ToString)

                                'Vimal 30-Aug-2010 --Start
                                If .Item("issue_date").ToString = Nothing Then
                                    objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("issue_date"))
                                End If

                                If .Item("start_date").ToString = Nothing Then
                                    objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, CDate(.Item("start_date")))
                                End If

                                If .Item("expiry_date").ToString = Nothing Then
                                    objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("expiry_date"))
                                End If
                                'Vimal 30-Aug-2010 --End

                                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("remark").ToString)
                                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(.Item("isactive"))) 'Sohail (28 Dec 2010)

                                'S.SANDEEP [ 17 OCT 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                objDataOperation.AddParameter("@isdeleted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(.Item("isdeleted")))
                                'S.SANDEEP [ 17 OCT 2012 ] -- END

                                'S.SANDEEP [14 APR 2015] -- START
                                objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid)
                                'S.SANDEEP [14 APR 2015] -- END


                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'objDataOperation.ExecNonQuery(strQ)
                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                                If objDataOperation.ErrorMessage <> "" Then
                                    'Shani(06-APR-2016) -- Start
                                    'Enhancement : new form for Global Void Membership 
                                    mstrErrorMessage = objDataOperation.ErrorMessage
                                    'Shani(06-APR-2016) -- End 
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                mintMembershipTranId = dsList.Tables(0).Rows(0)(0)

                                If .Item("employeeunkid") > 0 Then
                                    'Sohail (23 Apr 2012) -- Start
                                    'TRA - ENHANCEMENT
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_meminfo_tran", "membershiptranunkid", mintMembershipTranId, 2, 1, , intUserUnkId) = False Then
                                        'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_meminfo_tran", "membershiptranunkid", mintMembershipTranId, 2, 1) = False Then
                                        'Sohail (23 Apr 2012) -- End
                                        'Shani(06-APR-2016) -- Start
                                        'Enhancement : new form for Global Void Membership 
                                        mstrErrorMessage = objDataOperation.ErrorMessage
                                        'Shani(06-APR-2016) -- End 
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    'Sohail (23 Apr 2012) -- Start
                                    'TRA - ENHANCEMENT

                                    'Shani(16-DEC-2015) -- Start
                                    'ISSUE :  Adding New Employee
                                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", mintEmployeeUnkid, "hremployee_meminfo_tran", "membershiptranunkid", mintMembershipTranId, 1, 1, intUserUnkId) = False Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", mintEmployeeUnkid, "hremployee_meminfo_tran", "membershiptranunkid", mintMembershipTranId, 1, 1, , intUserUnkId) = False Then
                                        'Shani(16-DEC-2015) -- End

                                        'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", mintEmployeeUnkid, "hremployee_meminfo_tran", "membershiptranunkid", mintMembershipTranId, 1, 1) = False Then
                                        'Sohail (23 Apr 2012) -- End
                                        'Shani(06-APR-2016) -- Start
                                        'Enhancement : new form for Global Void Membership 
                                        mstrErrorMessage = objDataOperation.ErrorMessage
                                        'Shani(06-APR-2016) -- End 
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                                'S.SANDEEP [ 27 AUG 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'Dim objEd As New clsEarningDeduction
                                'Dim objTranHead As New clsTransactionHead
                                'For iTHead As Integer = 0 To 1
                                '    Select Case iTHead
                                '        Case 0
                                '            objTranHead._Tranheadunkid = .Item("emptrnheadid")
                                '        Case 1
                                '            objTranHead._Tranheadunkid = .Item("cotrnheadid")
                                '    End Select
                                '    If objTranHead._Tranheadunkid > 0 Then

                                '        'S.SANDEEP [ 17 OCT 2012 ] -- START
                                '        'ENHANCEMENT : TRA CHANGES
                                '        Dim intPeriodId As Integer
                                '        If mblnIsNewEmp = True Then
                                '            Dim objMaster As New clsMasterData
                                '            intPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, mdtAppointedDate.Date, 1)
                                '            If intPeriodId <= 0 Then
                                '                intPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll)
                                '            End If
                                '        End If
                                '        'S.SANDEEP [ 17 OCT 2012 ] -- END

                                '        objEd._Tranheadunkid = objTranHead._Tranheadunkid
                                '        objEd._Employeeunkid = mintEmployeeUnkid
                                '        objEd._Trnheadtype_Id = objTranHead._Trnheadtype_Id
                                '        objEd._Typeof_Id = objTranHead._Typeof_id
                                '        objEd._Calctype_Id = objTranHead._Calctype_Id
                                '        objEd._Formula = objTranHead._Formula
                                '        objEd._FormulaId = objTranHead._Formulaid
                                '        objEd._Userunkid = User._Object._Userunkid
                                '        objEd._Isapproved = User._Object.Privilege._AllowToApproveEarningDeduction
                                '        If User._Object.Privilege._AllowToApproveEarningDeduction = False Then
                                '            objEd._Approveruserunkid = -1
                                '        Else
                                '            objEd._Approveruserunkid = User._Object._Userunkid
                                '        End If
                                '        objEd._Periodunkid = IIf(CInt(.Item("effetiveperiodid")) <= 0, intPeriodId, CInt(.Item("effetiveperiodid")))

                                '        'S.SANDEEP [ 17 OCT 2012 ] -- START
                                '        'ENHANCEMENT : TRA CHANGES
                                '        objEd._MembershipTranUnkid = mintMembershipTranId
                                '        'S.SANDEEP [ 17 OCT 2012 ] -- END

                                '        If objEd.Insert(False, True, objDataOperation) = False Then
                                '            If objEd._Message <> "" Then
                                '                eZeeMsgBox.Show(objEd._Message, enMsgBoxStyle.Information)
                                '            Else
                                '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '                Throw exForce
                                '            End If
                                '        End If
                                '    End If
                                'Next

                                'S.SANDEEP [ 11 DEC 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'If Post_ED_Heads(mintMembershipTranId, .Item("effetiveperiodid"), objDataOperation, .Item("emptrnheadid"), .Item("cotrnheadid")) = False Then
                                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '    Throw exForce
                                'End If

                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'If Post_ED_Heads(mintMembershipTranId, .Item("effetiveperiodid"), objDataOperation, .Item("emptrnheadid"), .Item("cotrnheadid"), .Item("copyedslab"), .Item("overwriteslab"), .Item("overwritehead"), intUserUnkId) = False Then
                                If Post_ED_Heads(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintMembershipTranId, .Item("effetiveperiodid"), objDataOperation, .Item("emptrnheadid"), .Item("cotrnheadid"), .Item("copyedslab"), .Item("overwriteslab"), .Item("overwritehead"), blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString) = False Then
                                    'Sohail (21 Aug 2015) -- End
                                    'Shani(06-APR-2016) -- Start
                                    'Enhancement : new form for Global Void Membership 
                                    mstrErrorMessage = objDataOperation.ErrorMessage
                                    'Shani(06-APR-2016) -- End 
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP [ 11 DEC 2012 ] -- END


                            Case "U"
                                strQ = "UPDATE hremployee_meminfo_tran SET " & _
                                         "  employeeunkid = @employeeunkid" & _
                                         ", membership_categoryunkid = @membership_categoryunkid" & _
                                         ", membershipunkid = @membershipunkid" & _
                                         ", membershipno = @membershipno" & _
                                         ", issue_date = @issue_date" & _
                                         ", start_date = @start_date" & _
                                         ", expiry_date = @expiry_date" & _
                                         ", remark = @remark " & _
                                         ", isactive = @isactive " & _
                                         ", isdeleted = @isdeleted " & _
                                         ", rehiretranunkid = @rehiretranunkid " & _
                                       "WHERE membershiptranunkid = @membershiptranunkid " 'Sohail (28 Dec 2010)
                                'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END

                                objDataOperation.AddParameter("@membershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("membershiptranunkid").ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOperation.AddParameter("@membership_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("membership_categoryunkid").ToString)
                                objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("membershipunkid").ToString)
                                objDataOperation.AddParameter("@membershipno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("membershipno").ToString)

                                'Sandeep [ 01 MARCH 2011 ] -- Start
                                'If .Item("issue_date") = Nothing Then
                                If .Item("issue_date").ToString = Nothing Then
                                    'Sandeep [ 01 MARCH 2011 ] -- End 
                                    objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("issue_date"))
                                End If


                                'Sandeep [ 01 MARCH 2011 ] -- Start
                                'If .Item("start_date") = Nothing Then
                                If .Item("start_date").ToString = Nothing Then
                                    'Sandeep [ 01 MARCH 2011 ] -- End 
                                    objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("start_date"))
                                End If


                                'Sandeep [ 01 MARCH 2011 ] -- Start
                                'If .Item("expiry_date") = Nothing Then
                                If .Item("expiry_date").ToString = Nothing Then
                                    'Sandeep [ 01 MARCH 2011 ] -- End 
                                    objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("expiry_date"))
                                End If

                                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("remark").ToString)
                                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(.Item("isactive"))) 'Sohail (28 Dec 2010)

                                'S.SANDEEP [ 17 OCT 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                objDataOperation.AddParameter("@isdeleted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(.Item("isdeleted")))
                                'S.SANDEEP [ 17 OCT 2012 ] -- END

                                'S.SANDEEP [14 APR 2015] -- START
                                objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("rehiretranunkid"))
                                'S.SANDEEP [14 APR 2015] -- END

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    'Shani(06-APR-2016) -- Start
                                    'Enhancement : new form for Global Void Membership 
                                    mstrErrorMessage = objDataOperation.ErrorMessage
                                    'Shani(06-APR-2016) -- End 
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'Sohail (23 Apr 2012) -- Start
                                'TRA - ENHANCEMENT
                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_meminfo_tran", "membershiptranunkid", .Item("membershiptranunkid"), 2, 2, , intUserUnkId) = False Then
                                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_meminfo_tran", "membershiptranunkid", .Item("membershiptranunkid"), 2, 2) = False Then
                                    'Sohail (23 Apr 2012) -- End
                                    'Shani(06-APR-2016) -- Start
                                    'Enhancement : new form for Global Void Membership 
                                    mstrErrorMessage = objDataOperation.ErrorMessage
                                    'Shani(06-APR-2016) -- End 
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                                'S.SANDEEP [ 11 DEC 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'If Post_ED_Heads(.Item("membershiptranunkid"), .Item("effetiveperiodid"), objDataOperation, .Item("emptrnheadid"), .Item("cotrnheadid")) = False Then
                                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '    Throw exForce
                                'End If

                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'If Post_ED_Heads(.Item("membershiptranunkid"), .Item("effetiveperiodid"), objDataOperation, .Item("emptrnheadid"), .Item("cotrnheadid"), .Item("copyedslab"), .Item("overwriteslab"), .Item("overwritehead"), intUserUnkId) = False Then
                                If Post_ED_Heads(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, .Item("membershiptranunkid"), .Item("effetiveperiodid"), objDataOperation, .Item("emptrnheadid"), .Item("cotrnheadid"), .Item("copyedslab"), .Item("overwriteslab"), .Item("overwritehead"), blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString) = False Then
                                    'Sohail (21 Aug 2015) -- End
                                    'Shani(06-APR-2016) -- Start
                                    'Enhancement : new form for Global Void Membership 
                                    mstrErrorMessage = objDataOperation.ErrorMessage
                                    'Shani(06-APR-2016) -- End 
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP [ 11 DEC 2012 ] -- END

                            Case "D"

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

                                'S.SANDEEP [ 07 NOV 2011 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'If .Item("identitytranunkid") > 0 Then
                                If .Item("membershiptranunkid") > 0 Then
                                    'S.SANDEEP [ 07 NOV 2011 ] -- END`
                                    'Sohail (23 Apr 2012) -- Start
                                    'TRA - ENHANCEMENT
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_meminfo_tran", "membershiptranunkid", .Item("membershiptranunkid"), 2, 3, , intUserUnkId) = False Then
                                        'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", .Item("employeeunkid"), "hremployee_meminfo_tran", "membershiptranunkid", .Item("membershiptranunkid"), 2, 3) = False Then
                                        'Sohail (23 Apr 2012) -- End

                                        'Shani(06-APR-2016) -- Start
                                        'Enhancement : new form for Global Void Membership 
                                        mstrErrorMessage = objDataOperation.ErrorMessage
                                        'Shani(06-APR-2016) -- End 


                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END

                                'Sohail (28 Dec 2010) -- Start
                                'strQ = "DELETE FROM hremployee_meminfo_tran " & _
                                '            "WHERE membershiptranunkid = @membershiptranunkid "

                                'S.SANDEEP [ 17 OCT 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'strQ = "UPDATE hremployee_meminfo_tran SET " & _
                                '       " isactive = 0 " & _
                                '       " WHERE membershiptranunkid = @membershiptranunkid "

                                strQ = "UPDATE hremployee_meminfo_tran SET " & _
                                            " isactive = 0 " & _
                                       ",isdeleted = @isdeleted " & _
                                            "WHERE membershiptranunkid = @membershiptranunkid "
                                'S.SANDEEP [ 17 OCT 2012 ] -- END


                                'Sohail (28 Dec 2010) -- End



                                'S.SANDEEP [ 17 OCT 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                If .Item("membershiptranunkid") > 0 Then
                                    If .Item("emptrnheadid") > 0 Or .Item("cotrnheadid") > 0 Then
                                        Dim iCurrPeriod As Integer = -1
                                        Dim objMData As New clsMasterData
                                        Dim objPdata As New clscommom_period_Tran
                                        Dim objED As New clsEarningDeduction

                                        'S.SANDEEP [04 JUN 2015] -- START
                                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                        'iCurrPeriod = objMData.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
                                        iCurrPeriod = objMData.getFirstPeriodID(enModuleReference.Payroll, xYearUnkid, 1)
                                        'S.SANDEEP [04 JUN 2015] -- END

                                        'Sohail (21 Aug 2015) -- Start
                                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                        'objPdata._Periodunkid = iCurrPeriod
                                        objPdata._Periodunkid(strDatabaseName) = iCurrPeriod
                                        'Sohail (21 Aug 2015) -- End
                                        Dim dList As New DataSet
                                        'Sohail (21 Aug 2015) -- Start
                                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                        'dList = objED.GetList("List", mintEmployeeUnkid, , , , , , , , , , , , iCurrPeriod, , , , , objPdata._End_Date.Date, , , , objDataOperation)
                                        dList = objED.GetList(strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "List", True, mintEmployeeUnkid, , iCurrPeriod, , , , objPdata._End_Date.Date, , objDataOperation)
                                        'Sohail (21 Aug 2015) -- End
                                        If dList.Tables("List").Rows.Count > 0 Then
                                            Dim dRow() As DataRow = dList.Tables("List").Select("membershiptranunkid = '" & .Item("membershiptranunkid") & "'")
                                            If dRow.Length > 0 Then
                                                For j As Integer = 0 To dRow.Length - 1
                                                    'S.SANDEEP [21 NOV 2016] -- START
                                                    'objDataOperation.ClearParameters()
                                                    'S.SANDEEP [21 NOV 2016] -- END

                                                    'Sohail (21 Aug 2015) -- Start
                                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                                    'If objED.Void(objDataOperation, dRow(j)("edunkid"), User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, Language.getMessage(mstrModuleName, 6, "voided when membership deactivated.")) = False Then
                                                    If objED.Void(objDataOperation, dRow(j)("edunkid"), intUserUnkId, ConfigParameter._Object._CurrentDateAndTime, Language.getMessage(mstrModuleName, 6, "voided when membership deactivated."), strDatabaseName) = False Then
                                                        'Sohail (21 Aug 2015) -- End
                                                        'Shani(06-APR-2016) -- Start
                                                        'Enhancement : new form for Global Void Membership 
                                                        mstrErrorMessage = objDataOperation.ErrorMessage
                                                        'Shani(06-APR-2016) -- End 
                                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                                        Throw exForce
                                                    End If

                                                    'S.SANDEEP [21 NOV 2016] -- START
                                                    objDataOperation.ClearParameters()
                                                    'S.SANDEEP [21 NOV 2016] -- END
                                                Next
                                            End If
                                            'objDataOperation.AddParameter("@isdeleted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(False))
                                            .Item("isdeleted") = False
                                        Else
                                            .Item("isdeleted") = False
                                        End If
                                        objMData = Nothing : objPdata = Nothing : objED = Nothing
                                    Else
                                        'objDataOperation.AddParameter("@isdeleted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(True))
                                        .Item("isdeleted") = True
                                    End If
                                    'Else
                                    'objDataOperation.AddParameter("@isdeleted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(.Item("isdeleted")))
                                End If
                                'S.SANDEEP [ 17 OCT 2012 ] -- END

                                objDataOperation.AddParameter("@isdeleted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(.Item("isdeleted")))
                                objDataOperation.AddParameter("@membershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("membershiptranunkid").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    'Shani(06-APR-2016) -- Start
                                    'Enhancement : new form for Global Void Membership 
                                    mstrErrorMessage = objDataOperation.ErrorMessage
                                    'Shani(06-APR-2016) -- End 
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next

            'S.SANDEEP [14 APR 2015] -- START
            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP [14 APR 2015] -- END


            Return True

        Catch ex As Exception
            'S.SANDEEP [14 APR 2015] -- START
            'objDataOperation.ReleaseTransaction(False)
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            'S.SANDEEP [14 APR 2015] -- END

            'Shani(06-APR-2016) -- Start
            'Enhancement : new form for Global Void Membership 
            mstrErrorMessage = ex.Message
            'Shani(06-APR-2016) -- End 

            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_MembershipTran; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    'Sohail (28 Dec 2010) -- Start
    Public Function isUsed(ByVal intMembershipTranUnkID As Integer, ByVal intEmployeeID As Integer) As Boolean
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            strQ = "SELECT   edunkid " & _
                    "FROM    prearningdeduction_master " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND employeeunkid = @employeeunkid " & _
                            "AND membershiptranunkid = @membershiptranunkid "
            'Sohail (28 Jan 2016) - [AND vendorunkid = @membershiptranunkid] = [AND membershiptranunkid = @membershiptranunkid]

            objDataOperation.AddParameter("@membershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMembershipTranUnkID)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (28 Dec 2010) -- End

    'S.SANDEEP [ 18 May 2011 ] -- START
    Public Function GetBlankTemplate(Optional ByVal StrName As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'Gajanan (24 Nov 2018) -- Start
            'Enhancement : Import template column headers should read from language set for 
            'users (custom 1) and columns' date formats for importation should be clearly known
            'Replace Column Name With Getmessage



            'S.SANDEEP [ 02 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT " & _
            '           "	 ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
            '           "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS name " & _
            '           "	,ISNULL(cfcommon_master.name,'') AS membershipcategory " & _
            '           "	,ISNULL(hrmembership_master.membershipname,'') AS membership " & _
            '           "	,ISNULL(hremployee_meminfo_tran.membershipno,'') AS membershipno " & _
            '           "	,ISNULL(CONVERT(CHAR(8),hremployee_meminfo_tran.issue_date),'') AS issuedate " & _
            '           "	,ISNULL(CONVERT(CHAR(8),hremployee_meminfo_tran.start_date),'') AS startdate " & _
            '           "	,ISNULL(CONVERT(CHAR(8),hremployee_meminfo_tran.expiry_date),'') AS expirydate " & _
            '           "FROM hremployee_meminfo_tran " & _
            '           "	JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
            '           "	JOIN cfcommon_master ON hremployee_meminfo_tran.membership_categoryunkid = cfcommon_master.masterunkid AND cfcommon_master.mastertype =" & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & _
            '           "	JOIN hremployee_master ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '           "WHERE hremployee_meminfo_tran.isactive = 1 "
            'StrQ = "SELECT " & _
            '            "	 ISNULL(hremployee_master.employeecode,'') AS EmployeeCode " & _
            '            "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Displayname " & _
            '            "	,ISNULL(cfcommon_master.name,'') AS MembershipCategory " & _
            '            "	,ISNULL(hrmembership_master.membershipname,'') AS MembershipName " & _
            '            "	,ISNULL(hremployee_meminfo_tran.membershipno,'') AS MembershipNo " & _
            '            "	,ISNULL(CONVERT(CHAR(8),hremployee_meminfo_tran.issue_date),'') AS IssueDate " & _
            '            "	,ISNULL(CONVERT(CHAR(8),hremployee_meminfo_tran.start_date),'') AS StartDate " & _
            '            "	,ISNULL(CONVERT(CHAR(8),hremployee_meminfo_tran.expiry_date),'') AS ExpiryDate " & _
            '           "    ,'' AS EffectivePeriod " & _
            '            "FROM hremployee_meminfo_tran " & _
            '            "	JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
            '            "	JOIN cfcommon_master ON hremployee_meminfo_tran.membership_categoryunkid = cfcommon_master.masterunkid AND cfcommon_master.mastertype =" & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & _
            '            "	JOIN hremployee_master ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '           "WHERE hremployee_meminfo_tran.isdeleted = 0 " 'S.SANDEEP [14-JUN-2018] -- START {ALIAS CHANGED} -- END
            'S.SANDEEP [ 02 JAN 2013 ] -- END

            'S.SANDEEP |22-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Training requisition UAT]
            'StrQ = "SELECT " & _
            '            "	 ISNULL(hremployee_master.employeecode,'') AS  [" & Language.getMessage(mstrModuleName, 7, "EmployeeCode") & "]" & _
            '            "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS  [" & Language.getMessage(mstrModuleName, 8, "Displayname") & "]" & _
            '            "	,ISNULL(cfcommon_master.name,'') AS  [" & Language.getMessage(mstrModuleName, 9, "MembershipCategory") & "]" & _
            '            "	,ISNULL(hrmembership_master.membershipname,'') AS  [" & Language.getMessage(mstrModuleName, 10, "MembershipName") & "]" & _
            '            "	,ISNULL(hremployee_meminfo_tran.membershipno,'') AS MembershipNo [" & Language.getMessage(mstrModuleName, 11, "Employee_Code") & "]" & _
            '            "	,ISNULL(CONVERT(CHAR(8),hremployee_meminfo_tran.issue_date),'') AS  [" & Language.getMessage(mstrModuleName, 12, "IssueDate") & "]" & _
            '            "	,ISNULL(CONVERT(CHAR(8),hremployee_meminfo_tran.start_date),'') AS  [" & Language.getMessage(mstrModuleName, 13, "StartDate") & "]" & _
            '            "	,ISNULL(CONVERT(CHAR(8),hremployee_meminfo_tran.expiry_date),'') AS  [" & Language.getMessage(mstrModuleName, 14, "ExpiryDate") & "]" & _
            '            "    ,'' AS  [" & Language.getMessage(mstrModuleName, 15, "EffectivePeriod") & "]" & _
            '            "FROM hremployee_meminfo_tran " & _
            '            "	JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
            '            "	JOIN cfcommon_master ON hremployee_meminfo_tran.membership_categoryunkid = cfcommon_master.masterunkid AND cfcommon_master.mastertype =" & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & _
            '            "	JOIN hremployee_master ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '           "WHERE hremployee_meminfo_tran.isdeleted = 0 "

            StrQ = "SELECT " & _
                        "	 ISNULL(hremployee_master.employeecode,'') AS  [" & Language.getMessage(mstrModuleName, 7, "EmployeeCode") & "]" & _
                        "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS  [" & Language.getMessage(mstrModuleName, 8, "Displayname") & "]" & _
                        "	,ISNULL(cfcommon_master.name,'') AS  [" & Language.getMessage(mstrModuleName, 9, "MembershipCategory") & "]" & _
                        "	,ISNULL(hrmembership_master.membershipname,'') AS  [" & Language.getMessage(mstrModuleName, 10, "MembershipName") & "]" & _
                        "	,ISNULL(hremployee_meminfo_tran.membershipno,'') AS [" & Language.getMessage(mstrModuleName, 16, "MembershipNo") & "]" & _
                        "	,ISNULL(CONVERT(CHAR(8),hremployee_meminfo_tran.issue_date),'') AS  [" & Language.getMessage(mstrModuleName, 12, "IssueDate") & "]" & _
                        "	,ISNULL(CONVERT(CHAR(8),hremployee_meminfo_tran.start_date),'') AS  [" & Language.getMessage(mstrModuleName, 13, "StartDate") & "]" & _
                        "	,ISNULL(CONVERT(CHAR(8),hremployee_meminfo_tran.expiry_date),'') AS  [" & Language.getMessage(mstrModuleName, 14, "ExpiryDate") & "]" & _
                        "    ,'' AS  [" & Language.getMessage(mstrModuleName, 15, "EffectivePeriod") & "]" & _
                        "FROM hremployee_meminfo_tran " & _
                        "	JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                        "	JOIN cfcommon_master ON hremployee_meminfo_tran.membership_categoryunkid = cfcommon_master.masterunkid AND cfcommon_master.mastertype =" & clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY & _
                        "	JOIN hremployee_master ON hremployee_meminfo_tran.employeeunkid = hremployee_master.employeeunkid " & _
                       "WHERE hremployee_meminfo_tran.isdeleted = 0 "
            'S.SANDEEP |22-MAY-2019| -- END


            'Gajanan (24 Nov 2018) -- End


            'Anjan (24 Jun 2011)-Start
            'Issue : According to privilege that lower level user should not see superior level employees.

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    StrQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            'End If

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            StrQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            '        End If
            'End Select
            StrQ &= UserAccessLevel._AccessLevelFilterString
            'S.SANDEEP [ 01 JUNE 2012 ] -- END


            'S.SANDEEP [ 04 FEB 2012 ] -- END

            'Anjan (24 Jun 2011)-End 

            dsList = objDataOperation.ExecQuery(StrQ, StrName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetBlankTemplate", mstrModuleName)
            Return Nothing
        Finally
            dsList = Nothing
        End Try
    End Function

    Public Function GetEmployeeMembershipUnkid(ByVal intEmployeeId As Integer, ByVal StrMName As String) As Integer
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                       "	hremployee_meminfo_tran.membershipunkid " & _
                       "FROM hremployee_meminfo_tran " & _
                       "	JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                       "WHERE 1 = 1 " & _
                       "	AND employeeunkid = @EmpId AND membershipname = @name "

            'Sohail (13 Jan 2020) -- Start
            'Ifakara Health issue # : Employee membership already exist message coming even after deleting membership for all employees.
            StrQ &= " AND hremployee_meminfo_tran.isdeleted = 0 "
            'Sohail (13 Jan 2020) -- End

            'S.SANDEEP [22-May-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002267}
            'REMOVED :->  hremployee_meminfo_tran.isactive = 1 From Where Block
            'S.SANDEEP [22-May-2018] -- END

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrMName)

            dsList = objDataOperation.ExecQuery(StrQ, StrMName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0)("membershipunkid")
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeMembershipUnkid", mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 18 May 2011 ] -- END 

#End Region

    ' Pinkal (22-Dec-2010) -- Start


    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Function GetMembership_TranList(Optional ByVal strTableName As String = "List")
    Public Function GetMembership_TranList(Optional ByVal strTableName As String = "List", Optional ByVal intEmployeeId As Integer = -1, Optional ByVal intMembershipId As Integer = -1, Optional ByVal intMemTranId As Integer = -1)
        'S.SANDEEP [ 05 MARCH 2012 ] -- END
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'objDataOperation = New clsDataOperation
            If xDataOperation IsNot Nothing Then
                objDataOperation = xDataOperation
            Else
            objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()
            'S.SANDEEP [19 OCT 2016] -- END


            strQ = "SELECT " & _
                   " membershiptranunkid " & _
                        ",employeeunkid " & _
                        ",membership_categoryunkid " & _
                        ",membershipunkid " & _
                        ",membershipno " & _
                        ",issue_date " & _
                        ",start_date " & _
                        ",expiry_date " & _
                        ",remark " & _
                        ",'' As AUD " & _
                   "FROM hremployee_meminfo_tran " & _
                   "WHERE 1 = 1 "



            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If intEmployeeId > 0 Then
                strQ &= "AND employeeunkid = '" & intEmployeeId & "' "
            End If

            If intMembershipId > 0 Then
                strQ &= "AND membershipunkid = '" & intMembershipId & "' "
            End If

            If intMemTranId > 0 Then
                strQ &= "AND membershiptranunkid = '" & intMemTranId & "' "
            End If

            strQ &= "AND isactive = 1 "
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetMembership_TranList", mstrModuleName)
        End Try
        Return dsList
    End Function

    ' Pinkal (22-Dec-2010) -- End


    'S.SANDEEP [ 27 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function IsMembership_Mapped(ByVal intEmpId As Integer, ByVal intTrnHeadId As Integer) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Try
            objDataOperation = New clsDataOperation

            strQ = "SELECT edunkid " & _
                   "FROM prearningdeduction_master " & _
                   "JOIN cfcommon_period_tran ON prearningdeduction_master.periodunkid = cfcommon_period_tran.periodunkid " & _
                   "WHERE employeeunkid = '" & intEmpId & "' AND tranheadunkid = '" & intTrnHeadId & "' AND cfcommon_period_tran.statusid = 1 " & _
                   "AND ISNULL(prearningdeduction_master.isvoid,0) = 0 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsMembership_Mapped", mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 27 AUG 2012 ] -- END


    'S.SANDEEP [ 17 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function IsHead_Mapped(ByVal xDatabaseName As String _
                                 , ByVal xUserUnkid As Integer _
                                 , ByVal xYearUnkid As Integer _
                                 , ByVal xCompanyUnkid As Integer _
                                 , ByVal xPeriodStart As DateTime _
                                 , ByVal xPeriodEnd As DateTime _
                                 , ByVal xUserModeSetting As String _
                                 , ByVal xOnlyApproved As Boolean _
                                 , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                 , ByVal intEmployeeId As Integer _
                                 , ByVal intEtranHeadId As Integer _
                                 , ByVal intCTranHeadId As Integer _
                                 , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                 , Optional ByVal strFilerString As String = "" _
                                 ) As String
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strFilerString]

        Dim StrMsg As String = String.Empty
        Dim iCurrPeriod As Integer = -1
        Dim objMData As New clsMasterData
        Dim objPdata As New clscommom_period_Tran
        Dim objED As New clsEarningDeduction
        Dim dsList As New DataSet
        Try

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'iCurrPeriod = objMData.getFirstPeriodID(enModuleReference.Payroll, 1, FinancialYear._Object._YearUnkid)
            iCurrPeriod = objMData.getFirstPeriodID(enModuleReference.Payroll, xYearUnkid, 1)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPdata._Periodunkid = iCurrPeriod
            objPdata._Periodunkid(xDatabaseName) = iCurrPeriod
            'Sohail (21 Aug 2015) -- End

            '/****************************** IsPayrollProcessDone **************************
            Dim objProcessed As New clsTnALeaveTran
            If objProcessed.IsPayrollProcessDone(iCurrPeriod, intEmployeeId, objPdata._End_Date.Date) = True Then
                StrMsg = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot deactivate this membership. Reason : Payroll is already processed for the current period.")
                GoTo MsgRet
            End If
            objProcessed = Nothing
            '/****************************** IsPayrollProcessDone **************************

            '/****************************** Current Slab Exists Or Not **************************
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objED.GetAssignedEDPeriodList("List", intEmployeeId, False, 1)
            dsList = objED.GetAssignedEDPeriodList("List", intEmployeeId, xYearUnkid, False, 1)
            'Sohail (21 Aug 2015) -- End
            If dsList.Tables("List").Rows.Count > 0 Then
                Dim dRow() As DataRow = dsList.Tables("List").Select("periodunkid ='" & iCurrPeriod & "'")
                If dRow.Length <= 0 Then
                    StrMsg = Language.getMessage(mstrModuleName, 4, "Sorry, You cannot deactivate this membership. Reason : Transaction Heads are not copied to current period slab. Please copy them first from payroll.")
                    GoTo MsgRet
                End If
            End If
            '/****************************** Current Slab Exists Or Not **************************

            '/****************************** GetAssignedEDPeriodList (FUTURE) **************************
            If dsList.Tables("List").Rows.Count > 0 Then
                Dim dRow() As DataRow = dsList.Tables("List").Select("periodunkid NOT IN(" & iCurrPeriod & ") ")
                If dRow.Length > 0 Then
                    'S.SANDEEP [ 16 JAN 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    Dim dList As New DataSet
                    For i As Integer = 0 To dRow.Length - 1
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'dList = objED.GetList("List", intEmployeeId, , , , , , , , , , , , CInt(dRow(i)("periodunkid")), , , , , eZeeDate.convertDate(dRow(i)("end_date")))
                        dList = objED.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, "List", False, intEmployeeId, , CInt(dRow(i)("periodunkid")), , , , eZeeDate.convertDate(dRow(i)("end_date")), , objDataOperation)
                        'Sohail (21 Aug 2015) -- End
                        If dList.Tables(0).Rows.Count > 0 Then
                            Dim dtRow() As DataRow = dList.Tables(0).Select("tranheadunkid IN(" & intEtranHeadId & "," & intCTranHeadId & ")")
                            If dtRow.Length > 0 Then
                                StrMsg = Language.getMessage(mstrModuleName, 5, "Sorry, You cannot deactivate this membership. Reason : Transaction Heads are posted for the future period. Please remove them first from payroll.")
                                GoTo MsgRet
                            End If
                        End If
                    Next
                    'S.SANDEEP [ 16 JAN 2013 ] -- END
                End If
            End If
            '/****************************** GetAssignedEDPeriodList (FUTURE) **************************

MsgRet:     Return StrMsg

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsHead_Mapped; Module Name: " & mstrModuleName)
        Finally
            objMData = Nothing : objPdata = Nothing : objED = Nothing
        End Try
    End Function

    Public Function Void_Membership(ByVal intUnkid As Integer, ByVal intEmpUnkid As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Sohail (06 Sep 2019) - [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            'Sohail (06 Sep 2019) -- Start
            'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
            'objDataOperation = New clsDataOperation
            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
            objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()
            'Sohail (06 Sep 2019) -- End

            strQ = "UPDATE hremployee_meminfo_tran SET " & _
                       " isactive = 0 " & _
                       " WHERE membershiptranunkid = @membershiptranunkid "

            objDataOperation.AddParameter("@membershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremployee_master", "employeeunkid", intEmpUnkid, "hremployee_meminfo_tran", "membershiptranunkid", intUnkid, 2, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Void_Membership; Module Name: " & mstrModuleName)
            'Sohail (06 Sep 2019) -- Start
            'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (06 Sep 2019) -- End
        End Try
    End Function

    Public Function Get_Mem_EDUnkid(ByVal intSelEDUnkid As Integer, ByVal intMemTranId As Integer, ByVal intPeriod As Integer, ByVal intEmpUnkid As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            strQ = "SELECT " & _
                       "  edunkid " & _
                       "FROM prearningdeduction_master " & _
                       "WHERE employeeunkid = '" & intEmpUnkid & "' " & _
                       "AND periodunkid = '" & intPeriod & "' " & _
                       "AND membershiptranunkid = '" & intMemTranId & "' " & _
                       "AND edunkid <> '" & intSelEDUnkid & "' " & _
                       "AND ISNULL(isvoid,0) = 0 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item("edunkid").ToString)
            Else
                Return -1
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Mem_EDUnkid; Module Name: " & mstrModuleName)
        End Try
    End Function

    Private Function Post_ED_Heads(ByVal strDatabaseName As String _
                                   , ByVal intUserUnkId As Integer _
                                   , ByVal xYearUnkid As Integer _
                                   , ByVal xCompanyUnkid As Integer _
                                   , ByVal xPeriodStart As DateTime _
                                   , ByVal xPeriodEnd As DateTime _
                                   , ByVal xUserModeSetting As String _
                                   , ByVal xOnlyApproved As Boolean _
                                   , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                   , ByVal intTranId As Integer _
                                   , ByVal intCPeriodId As Integer _
                                   , ByVal objDataOperation As clsDataOperation _
                                   , ByVal iemptrnheadid As Integer _
                                   , ByVal icotrnheadid As Integer _
                                   , ByVal mblnIsCopyPrevoiusSLAB As Boolean _
                                   , ByVal mblnIsOverwritePrevoiusSLAB As Boolean _
                                   , ByVal mblnOverwriteHead As Boolean _
                                   , ByVal blnAllowToApproveEarningDeduction As Boolean _
                                   , ByVal dtCurrentDateAndTime As Date _
                                   , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                   , Optional ByVal strFilerString As String = "" _
                                   ) As Boolean
        'Sohail (21 Aug 2015) - [strDatabaseName, intUserUnkId, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, blnApplyUserAccessFilter, strFilerString]

        Try

            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If intCPeriodId <= 0 Then Return True
            If mblnIsNewEmp = False Then
                If intCPeriodId <= 0 Then Return True
            End If
            'S.SANDEEP [ 16 JAN 2013 ] -- END

            Dim objEd As New clsEarningDeduction
            Dim objTranHead As New clsTransactionHead
            Dim objPMaster As New clscommom_period_Tran

            'S.SANDEEP [ 11 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'For iTHead As Integer = 0 To 1
            '    Select Case iTHead
            '        Case 0
            '            objTranHead._Tranheadunkid = iemptrnheadid
            '        Case 1
            '            objTranHead._Tranheadunkid = icotrnheadid
            '    End Select
            '    If objTranHead._Tranheadunkid > 0 Then

            '        'S.SANDEEP [ 17 OCT 2012 ] -- START
            '        'ENHANCEMENT : TRA CHANGES
            '        Dim intPeriodId As Integer
            '        If mblnIsNewEmp = True Then
            '            Dim objMaster As New clsMasterData
            '            intPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, mdtAppointedDate.Date, 1)
            '            If intPeriodId <= 0 Then
            '                intPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll)
            '            End If
            '        End If
            '        'S.SANDEEP [ 17 OCT 2012 ] -- END

            '        objEd._Tranheadunkid = objTranHead._Tranheadunkid
            '        objEd._Employeeunkid = mintEmployeeUnkid
            '        objEd._Trnheadtype_Id = objTranHead._Trnheadtype_Id
            '        objEd._Typeof_Id = objTranHead._Typeof_id
            '        objEd._Calctype_Id = objTranHead._Calctype_Id
            '        objEd._Formula = objTranHead._Formula
            '        objEd._FormulaId = objTranHead._Formulaid
            '        objEd._Userunkid = User._Object._Userunkid
            '        objEd._Isapproved = User._Object.Privilege._AllowToApproveEarningDeduction
            '        If User._Object.Privilege._AllowToApproveEarningDeduction = False Then
            '            objEd._Approveruserunkid = -1
            '        Else
            '            objEd._Approveruserunkid = User._Object._Userunkid
            '        End If
            '        objEd._Periodunkid = IIf(CInt(intCPeriodId) <= 0, intPeriodId, CInt(intCPeriodId))

            '        'S.SANDEEP [ 17 OCT 2012 ] -- START
            '        'ENHANCEMENT : TRA CHANGES
            '        objEd._MembershipTranUnkid = intTranId
            '        'S.SANDEEP [ 17 OCT 2012 ] -- END

            '        If objEd.Insert(False, True, objDataOperation) = False Then
            '            If objEd._Message <> "" Then
            '                eZeeMsgBox.Show(objEd._Message, enMsgBoxStyle.Information)
            '            Else
            '                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            End If
            '        End If
            '    End If
            'Next

            Dim dTable As DataTable
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objEd._Employeeunkid = mintEmployeeUnkid

            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            objEd._DataOperation = objDataOperation
            'S.SANDEEP [19 OCT 2016] -- END

            objEd._Employeeunkid(strDatabaseName) = mintEmployeeUnkid
            'Sohail (21 Aug 2015) -- End
            dTable = objEd._DataSource.Copy
            dTable.Rows.Clear()
            For iTHead As Integer = 0 To 1

                Dim intPeriodId As Integer
                If mblnIsNewEmp = True Then
                    Dim objMaster As New clsMasterData

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'intPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, mdtAppointedDate.Date, 1)

                    'S.SANDEEP [19 OCT 2016] -- START
                    'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
                    'intPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, mdtAppointedDate.Date, xYearUnkid, 1)
                    intPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, mdtAppointedDate.Date, xYearUnkid, 1, , , objDataOperation)
                    'S.SANDEEP [19 OCT 2016] -- END

                    'S.SANDEEP [04 JUN 2015] -- END

                    If intPeriodId <= 0 Then

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'intPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll)

                        'S.SANDEEP [19 OCT 2016] -- START
                        'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
                        'intPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, xYearUnkid)
                        intPeriodId = objMaster.getFirstPeriodID(enModuleReference.Payroll, xYearUnkid, , , , objDataOperation)
                        'S.SANDEEP [19 OCT 2016] -- END

                        'S.SANDEEP [04 JUN 2015] -- END

                    End If
                End If

                Select Case iTHead
                    Case 0
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objTranHead._Tranheadunkid = iemptrnheadid

                        'S.SANDEEP [19 OCT 2016] -- START
                        'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
                        objTranHead._xDataOperation = objDataOperation
                        'S.SANDEEP [19 OCT 2016] -- END
                        objTranHead._Tranheadunkid(strDatabaseName) = iemptrnheadid

                        'Sohail (21 Aug 2015) -- End
                    Case 1
                        'Sohail (21 Aug 2015) -- Start
                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                        'objTranHead._Tranheadunkid = icotrnheadid

                        'S.SANDEEP [19 OCT 2016] -- START
                        'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
                        objTranHead._xDataOperation = objDataOperation
                        'S.SANDEEP [19 OCT 2016] -- END

                        objTranHead._Tranheadunkid(strDatabaseName) = icotrnheadid
                        'Sohail (21 Aug 2015) -- End
                End Select
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If objTranHead._Tranheadunkid > 0 Then
                If objTranHead._Tranheadunkid(strDatabaseName) > 0 Then
                    'Sohail (21 Aug 2015) -- End
                    Dim dRow As DataRow = dTable.NewRow
                    dRow.Item("edunkid") = -1
                    dRow.Item("employeeunkid") = mintEmployeeUnkid
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dRow.Item("tranheadunkid") = objTranHead._Tranheadunkid
                    dRow.Item("tranheadunkid") = objTranHead._Tranheadunkid(strDatabaseName)
                    'Sohail (21 Aug 2015) -- End
                    dRow.Item("trnheadname") = objTranHead._Trnheadname
                    dRow.Item("batchtransactionunkid") = -1
                    dRow.Item("amount") = 0
                    dRow.Item("isdeduct") = True
                    dRow.Item("trnheadtype_id") = objTranHead._Trnheadtype_Id
                    dRow.Item("typeof_id") = objTranHead._Typeof_id
                    dRow.Item("calctype_id") = objTranHead._Calctype_Id
                    dRow.Item("computeon_id") = objTranHead._Computeon_Id
                    dRow.Item("formula") = ""
                    dRow.Item("formulaid") = 0
                    dRow.Item("currencyid") = 0
                    dRow.Item("vendorid") = 0
                    dRow.Item("userunkid") = IIf(intUserUnkId <= 0, User._Object._Userunkid, intUserUnkId)
                    dRow.Item("isvoid") = False
                    dRow.Item("voiduserunkid") = 0
                    dRow.Item("voiddatetime") = DBNull.Value
                    dRow.Item("voidreason") = ""
                    dRow.Item("membership_categoryunkid") = 0
                    If User._Object.Privilege._AllowToApproveEarningDeduction Then
                        dRow.Item("isapproved") = True
                        dRow.Item("approveruserunkid") = IIf(intUserUnkId <= 0, User._Object._Userunkid, intUserUnkId)
                    Else
                        dRow.Item("isapproved") = False
                        dRow.Item("approveruserunkid") = 0
                    End If
                    dRow.Item("periodunkid") = IIf(CInt(intCPeriodId) <= 0, intPeriodId, CInt(intCPeriodId))

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objPMaster._Periodunkid = dRow.Item("periodunkid")

                    'S.SANDEEP [19 OCT 2016] -- START
                    'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
                    objPMaster._xDataOperation = objDataOperation
                    'S.SANDEEP [19 OCT 2016] -- END

                    objPMaster._Periodunkid(strDatabaseName) = dRow.Item("periodunkid")
                    'Sohail (21 Aug 2015) -- End
                    dRow.Item("period_name") = objPMaster._Period_Name
                    dRow.Item("start_date") = eZeeDate.convertDate(objPMaster._Start_Date)
                    dRow.Item("end_date") = eZeeDate.convertDate(objPMaster._End_Date)

                    'S.SANDEEP [25 OCT 2016] -- START
                    'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
                    'objPMaster = Nothing
                    'S.SANDEEP [25 OCT 2016] -- END


                    dRow.Item("AUD") = "A"
                    dRow.Item("isrecurrent") = False
                    dRow.Item("costcenterunkid") = 0
                    dRow.Item("medicalrefno") = ""
                    dRow.Item("membershiptranunkid") = intTranId
                    dTable.Rows.Add(dRow)
                End If
            Next

            'S.SANDEEP [25 OCT 2016] -- START
            'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
            objPMaster = Nothing
            'S.SANDEEP [25 OCT 2016] -- END

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If objEd.InsertAllByDataTable(objDataOperation, mintEmployeeUnkid.ToString, dTable, mblnOverwriteHead, True, mblnIsCopyPrevoiusSLAB, IIf(intUserUnkId <= 0, User._Object._Userunkid, intUserUnkId), , mblnIsOverwritePrevoiusSLAB) = False Then
            'Sohail (01 Dec 2016) -- Start
            'Enhancement - 64.1 - Insert Membership heads on Ed if not assigned.
            'If objEd.InsertAllByDataTable(strDatabaseName, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, objDataOperation, mintEmployeeUnkid.ToString, dTable, mblnOverwriteHead, intUserUnkId, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, True, mblnIsCopyPrevoiusSLAB, mblnIsOverwritePrevoiusSLAB, blnApplyUserAccessFilter, strFilerString) = False Then
            If objEd.InsertAllByDataTable(strDatabaseName, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, objDataOperation, mintEmployeeUnkid.ToString, dTable, mblnOverwriteHead, intUserUnkId, blnAllowToApproveEarningDeduction, dtCurrentDateAndTime, True, mblnIsCopyPrevoiusSLAB, mblnIsOverwritePrevoiusSLAB, blnApplyUserAccessFilter, strFilerString, False) = False Then
                'Sohail (01 Dec 2016) -- End
                'Sohail (21 Aug 2015) -- End
                If objEd._Message <> "" Then
                    eZeeMsgBox.Show(objEd._Message, enMsgBoxStyle.Information)
                Else
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If
            End If
            'S.SANDEEP [ 11 DEC 2012 ] -- END
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Mem_EDUnkid; Module Name: " & mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 17 OCT 2012 ] -- END

    'S.SANDEEP [ 11 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Can_Assign_Membership(ByVal mintEmplrHeadId As Integer, ByVal intEmpId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iCnt As Integer = 0
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            'StrQ = "SELECT " & _
            '       " * " & _
            '       "FROM hremployee_meminfo_tran " & _
            '       "JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
            '       "WHERE employeeunkid = '" & intEmpId & "' AND isdeleted= 0 " & _
            '       "AND hrmembership_master.cotranheadunkid = '" & mintEmplrHeadId & "' "

            StrQ = "SELECT " & _
                   " * " & _
                   "FROM hremployee_meminfo_tran " & _
                        "JOIN hrmembership_master ON hremployee_meminfo_tran.membershipunkid = hrmembership_master.membershipunkid " & _
                   "WHERE employeeunkid = '" & intEmpId & "' AND isdeleted= 0 AND hremployee_meminfo_tran.isactive = 1 " & _
                   "AND hrmembership_master.cotranheadunkid = '" & mintEmplrHeadId & "' "
            'S.SANDEEP [ 14 May 2013 ] -- END

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Can_Assign_Membership", mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 11 DEC 2012 ] -- END

    'Shani(06-APR-2016) -- Start
    'Enhancement : new form for Global Void Membership 
    Public Function GetEmployee_By_Membership(ByVal xDatabaseName As String, _
                                              ByVal xUserUnkid As Integer, _
                                              ByVal xYearUnkid As Integer, _
                                              ByVal xCompanyUnkid As Integer, _
                                              ByVal xPeriodStart As DateTime, _
                                              ByVal xPeriodEnd As DateTime, _
                                              ByVal xUserModeSetting As String, _
                                              ByVal xOnlyApproved As Boolean, _
                                              ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                              ByVal intMembershipUnkId As Integer, _
                                              Optional ByVal strTableName As String = "List", _
                                              Optional ByVal mstrEmployeeIds As String = "", _
                                              Optional ByVal strFilerString As String = "") As DataSet
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            strQ = "SELECT " & _
                   "     hremployee_master.employeecode AS employeecode " & _
                   "    ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                   "    ,hremployee_meminfo_tran.membershiptranunkid " & _
                   "    ,hremployee_master.employeeunkid " & _
                   "    ,hrmembership_master.membershipcategoryunkid " & _
                   "    ,hrmembership_master.emptranheadunkid " & _
                   "    ,hrmembership_master.cotranheadunkid " & _
                   "    ,hremployee_meminfo_tran.isactive " & _
                   "    ,hremployee_meminfo_tran.membershiptranunkid " & _
                   "FROM hremployee_meminfo_tran " & _
                   "    JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_meminfo_tran.employeeunkid " & _
                   "    JOIN hrmembership_master ON hrmembership_master.membershipunkid = hremployee_meminfo_tran.membershipunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE hremployee_meminfo_tran.isactive = 1 AND  hremployee_meminfo_tran.isdeleted = 0 "

            If intMembershipUnkId > 0 Then
                strQ &= "AND hremployee_meminfo_tran.membershipunkid = '" & intMembershipUnkId & "' "
            End If

            If mstrEmployeeIds.Trim.Length > 0 Then
                strQ &= "AND hremployee_meminfo_tran.employeeunkid IN (" & mstrEmployeeIds & ") "

            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDataFilterQry.Trim.Length > 0 Then
                    strQ &= xDataFilterQry
                End If
            End If

            If strFilerString.Trim.Length > 0 Then
                strQ &= " AND " & strFilerString
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployee_By_Membership", mstrModuleName)
        End Try
        Return dsList
    End Function

    'Shani(06-APR-2016) -- End 

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Active")
            Language.setMessage(mstrModuleName, 2, "Inactive")
            Language.setMessage(mstrModuleName, 3, "Sorry, You cannot deactivate this membership. Reason : Payroll is already processed for the current period.")
            Language.setMessage(mstrModuleName, 4, "Sorry, You cannot deactivate this membership. Reason : Transaction Heads are not copied to current period slab. Please copy them first from payroll.")
            Language.setMessage(mstrModuleName, 5, "Sorry, You cannot deactivate this membership. Reason : Transaction Heads are posted for the future period. Please remove them first from payroll.")
            Language.setMessage(mstrModuleName, 6, "voided when membership deactivated.")
			Language.setMessage(mstrModuleName, 7, "EmployeeCode")
			Language.setMessage(mstrModuleName, 8, "Displayname")
			Language.setMessage(mstrModuleName, 9, "MembershipCategory")
			Language.setMessage(mstrModuleName, 10, "MembershipName")
			Language.setMessage(mstrModuleName, 11, "Employee_Code")
			Language.setMessage(mstrModuleName, 12, "IssueDate")
			Language.setMessage(mstrModuleName, 13, "StartDate")
			Language.setMessage(mstrModuleName, 14, "ExpiryDate")
			Language.setMessage(mstrModuleName, 15, "EffectivePeriod")
            Language.setMessage(mstrModuleName, 16, "MembershipNo")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
