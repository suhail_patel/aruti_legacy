﻿'************************************************************************************************************************************
'Class Name : clsDependants_Membership_tran.vb
'Purpose    :
'Date       :01/09/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsDependants_Membership_tran

#Region " Private Variables "
    Private Const mstrModuleName As String = "clsDependants_Membership_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private mintDependantTranUnkid As Integer = -1
    Private mdtTran As DataTable

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mstrApprovalTranguid As String = String.Empty

    Dim Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Dim DependantApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmDependantsAndBeneficiariesList)))
    'Gajanan [22-Feb-2019] -- End



#End Region

#Region " Properties "
    Public Property _DependantTranUnkid() As Integer
        Get
            Return mintDependantTranUnkid
        End Get
        Set(ByVal value As Integer)
            mintDependantTranUnkid = value
            Call Get_Dependant_Tran()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Public Property _ApprovalTranguid() As String
        Get
            Return mstrApprovalTranguid
        End Get
        Set(ByVal value As String)
            mstrApprovalTranguid = value
        End Set
    End Property

    'Gajanan [22-Feb-2019] -- End


#End Region

#Region " Contructor "
    Public Sub New()
        mdtTran = New DataTable("DependantTran")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("dpndtmembershiptranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("dpndtbeneficetranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("membership_categoryunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("membershipunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("membershipno")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = DBNull.Value
            dCol.AllowDBNull = True
            mdtTran.Columns.Add(dCol)

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            'Shani(04-MAR-2017) -- Start
            'Enhancement - Add Membership and Benefit in employee dependant's screen on web
            dCol = New DataColumn("Category")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = DBNull.Value
            dCol.AllowDBNull = True
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("membershipname")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = DBNull.Value
            dCol.AllowDBNull = True
            mdtTran.Columns.Add(dCol)
            'Shani(04-MAR-2017) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Functions "

    'Pinkal(17-Nov-2010)----- Start

    Public Function GetList(Optional ByVal strTableName As String = "List") As DataSet
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            strQ = "SELECT " & _
                  "  dpndtmembershiptranunkid " & _
                  ", dpndtbeneficetranunkid " & _
                  ", membership_categoryunkid " & _
                  ", membershipunkid " & _
                  ", membershipno " & _
                  ", isvoid " & _
                  ", voiduserunkid " & _
                  ", voiddatetime " & _
                  ", voidreason " & _
                 "FROM hrdependant_membership_tran "
             
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetList", mstrModuleName)
        End Try
        Return dsList
    End Function

    'Pinkal(17-Nov-2010)----- End



    Private Sub Get_Dependant_Tran()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim dRowD_Tran As DataRow
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'Shani(04-MAR-2017) -- Start
            'Enhancement - Add Membership and Benefit in employee dependant's screen on web
            'strQ = "SELECT " & _
            '        "  dpndtmembershiptranunkid " & _
            '        ", dpndtbeneficetranunkid " & _
            '        ", membership_categoryunkid " & _
            '        ", membershipunkid " & _
            '        ", membershipno " & _
            '        ", '' As AUD " & _
            '       "FROM hrdependant_membership_tran " & _
            '       "WHERE dpndtbeneficetranunkid = @dpndtbeneficetranunkid " & _
            '       "AND ISNULL(isvoid,0) = 0 "
            strQ = "SELECT " & _
                   "     hrdependant_membership_tran.dpndtmembershiptranunkid " & _
                   "    ,hrdependant_membership_tran.dpndtbeneficetranunkid " & _
                   "    ,hrdependant_membership_tran.membership_categoryunkid " & _
                   "    ,hrdependant_membership_tran.membershipunkid " & _
                   "    ,hrdependant_membership_tran.membershipno " & _
                   "    ,cfcommon_master.name AS Category " & _
                   "    ,hrmembership_master.membershipname " & _
                   "    , '' As AUD " & _
                   "FROM hrdependant_membership_tran " & _
                   "    LEFT JOIN cfcommon_master  ON cfcommon_master.masterunkid = membership_categoryunkid AND cfcommon_master.mastertype = 16 " & _
                   "    LEFT JOIN hrmembership_master ON hrmembership_master.membershipunkid =  hrdependant_membership_tran.membershipunkid " & _
                   "WHERE hrdependant_membership_tran.dpndtbeneficetranunkid = @dpndtbeneficetranunkid " & _
                   "    AND ISNULL(isvoid,0) = 0 "
            'Shani(04-MAR-2017) -- End

            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantTranUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    dRowD_Tran = mdtTran.NewRow()

                    dRowD_Tran.Item("dpndtmembershiptranunkid") = .Item("dpndtmembershiptranunkid")
                    dRowD_Tran.Item("dpndtbeneficetranunkid") = .Item("dpndtbeneficetranunkid")
                    dRowD_Tran.Item("membership_categoryunkid") = .Item("membership_categoryunkid")
                    dRowD_Tran.Item("membershipunkid") = .Item("membershipunkid")
                    dRowD_Tran.Item("membershipno") = .Item("membershipno")
                    dRowD_Tran.Item("AUD") = .Item("AUD")

                    'Shani(04-MAR-2017) -- Start
                    'Enhancement - Add Membership and Benefit in employee dependant's screen on web
                    dRowD_Tran.Item("Category") = .Item("Category")
                    dRowD_Tran.Item("membershipname") = .Item("membershipname")
                    'Shani(04-MAR-2017) -- End

                    mdtTran.Rows.Add(dRowD_Tran)
                End With
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Dependant_Tran", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 27 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Function InsertUpdateDelete_DependantMembership_Tran() As Boolean

    'Shani(04-MAR-2017) -- Start
    'Enhancement - Add Membership and Benefit in employee dependant's screen on web
    'Public Function InsertUpdateDelete_DependantMembership_Tran(Optional ByVal intUserUnkid As Integer = 0) As Boolean
    Public Function InsertUpdateDelete_DependantMembership_Tran(ByVal objOp As clsDataOperation, Optional ByVal intUserUnkid As Integer = 0) As Boolean
        'Shani(04-MAR-2017) -- End

        'S.SANDEEP [ 27 APRIL 2012 ] -- END
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try

            'Shani(04-MAR-2017) -- Start
            'Enhancement - Add Membership and Benefit in employee dependant's screen on web
            'objDataOperation = New clsDataOperation
            'objDataOperation.BindTransaction()
            objDataOperation = objOp
            'Shani(04-MAR-2017) -- End

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO hrdependant_membership_tran ( " & _
                                            "  dpndtbeneficetranunkid " & _
                                            ", membership_categoryunkid " & _
                                            ", membershipunkid " & _
                                            ", membershipno " & _
                                            ", isvoid " & _
                                            ", voiduserunkid " & _
                                            ", voiddatetime " & _
                                            ", voidreason" & _
                                          ") VALUES (" & _
                                            "  @dpndtbeneficetranunkid " & _
                                            ", @membership_categoryunkid " & _
                                            ", @membershipunkid " & _
                                            ", @membershipno " & _
                                            ", @isvoid " & _
                                            ", @voiduserunkid " & _
                                            ", @voiddatetime " & _
                                            ", @voidreason" & _
                                          "); SELECT @@identity"

                                objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantTranUnkid.ToString)
                                objDataOperation.AddParameter("@membership_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("membership_categoryunkid").ToString)
                                objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("membershipunkid").ToString)
                                objDataOperation.AddParameter("@membershipno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("membershipno").ToString)
                          
                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                'objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")

                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))

                                'Gajanan [22-Feb-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.


                                'Gajanan [27-May-2019] -- Start              

                                'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))

                                'If IsDBNull(.Item("voiddatetime")) = False Then
                                '    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                'Else
                                'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                'End If

                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))

                                'Gajanan [27-May-2019] -- End

                                'Gajanan [22-Feb-2019] -- End


                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                'objDataOperation.ExecNonQuery(strQ)

                                Dim dList As DataSet = objDataOperation.ExecQuery(strQ, "List")
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 



                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If



                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                ''S.SANDEEP [ 12 OCT 2011 ] -- START
                                ''ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'If .Item("dpndtbeneficetranunkid") > 0 Then
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", .Item("dpndtbeneficetranunkid"), "hrdependant_membership_tran", "dpndtmembershiptranunkid", dList.Tables(0).Rows(0)(0), 2, 1) = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'Else
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", mintDependantTranUnkid, "hrdependant_membership_tran", "dpndtmembershiptranunkid", dList.Tables(0).Rows(0)(0), 1, 1) = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If
                                ''S.SANDEEP [ 12 OCT 2011 ] -- END 
                                
                                If .Item("dpndtbeneficetranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", .Item("dpndtbeneficetranunkid"), "hrdependant_membership_tran", "dpndtmembershiptranunkid", dList.Tables(0).Rows(0)(0), 2, 1, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", mintDependantTranUnkid, "hrdependant_membership_tran", "dpndtmembershiptranunkid", dList.Tables(0).Rows(0)(0), 1, 1, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                            Case "U"
                                strQ = "UPDATE hrdependant_membership_tran SET " & _
                                        "  dpndtbeneficetranunkid = @dpndtbeneficetranunkid" & _
                                        ", membership_categoryunkid = @membership_categoryunkid" & _
                                        ", membershipunkid = @membershipunkid" & _
                                        ", membershipno = @membershipno" & _
                                        ", isvoid = @isvoid" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voidreason = @voidreason " & _
                                      "WHERE dpndtmembershiptranunkid = @dpndtmembershiptranunkid "

                                objDataOperation.AddParameter("@dpndtmembershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dpndtmembershiptranunkid").ToString)
                                objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dpndtbeneficetranunkid").ToString)
                                objDataOperation.AddParameter("@membership_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("membership_categoryunkid").ToString)
                                objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("membershipunkid").ToString)
                                objDataOperation.AddParameter("@membershipno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("membershipno").ToString)

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                'objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")

                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If



                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                ''S.SANDEEP [ 12 OCT 2011 ] -- START
                                ''ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", .Item("dpndtbeneficetranunkid"), "hrdependant_membership_tran", "dpndtmembershiptranunkid", .Item("dpndtmembershiptranunkid"), 2, 2) = False Then
                                '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                '    Throw exForce
                                'End If
                                ''S.SANDEEP [ 12 OCT 2011 ] -- END 

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", .Item("dpndtbeneficetranunkid"), "hrdependant_membership_tran", "dpndtmembershiptranunkid", .Item("dpndtmembershiptranunkid"), 2, 2, , intUserUnkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                            Case "D"
                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'strQ = "DELETE FROM hrdependant_membership_tran " & _
                                '       "WHERE dpndtmembershiptranunkid = @dpndtmembershiptranunkid "



                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'If .Item("dpndtmembershiptranunkid") > 0 Then
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", .Item("dpndtbeneficetranunkid"), "hrdependant_membership_tran", "dpndtmembershiptranunkid", .Item("dpndtmembershiptranunkid"), 2, 3) = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If

                                If .Item("dpndtmembershiptranunkid") > 0 Then

                                    'Gajanan [22-Feb-2019] -- Start
                                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", .Item("dpndtbeneficetranunkid"), "hrdependant_membership_tran", "dpndtmembershiptranunkid", .Item("dpndtmembershiptranunkid"), 2, 3, , intUserUnkid) = False Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", .Item("dpndtbeneficetranunkid"), "hrdependant_membership_tran", "dpndtmembershiptranunkid", .Item("dpndtmembershiptranunkid"), 2, 3, False, intUserUnkid) = False Then
                                        'Gajanan [22-Feb-2019] -- End
                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                                strQ = "UPDATE hrdependant_membership_tran SET " & _
                                        "  isvoid = @isvoid" & _
                                        " ,voiduserunkid = @voiduserunkid" & _
                                        " ,voiddatetime = @voiddatetime" & _
                                        " ,voidreason = @voidreason " & _
                                      "WHERE dpndtmembershiptranunkid = @dpndtmembershiptranunkid "
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 
                                objDataOperation.AddParameter("@dpndtmembershiptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dpndtmembershiptranunkid").ToString)

                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If
                End With
            Next
            Return True

            'Shani(04-MAR-2017) -- Start
            'Enhancement - Add Membership and Benefit in employee dependant's screen on web
            'DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_DependantMembership_Tran", mstrModuleName)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_DependantMembership_Tran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            'Shani(04-MAR-2017) -- End
        End Try
    End Function


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.

    'Public Function Void_All(ByVal isVoid As Boolean, _
    '                         ByVal dtVoidDate As DateTime, _
    '                         ByVal strVoidRemark As String, _
    '                         ByVal intVoidUserId As Integer, _
    '                         ) As Boolean
    Public Function Void_All(ByVal isVoid As Boolean, _
                             ByVal dtVoidDate As DateTime, _
                             ByVal strVoidRemark As String, _
                             ByVal intVoidUserId As Integer, _
                             Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        'Gajanan [22-Feb-2019] -- End
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception

        'Gajanan [22-Feb-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.

        'Dim objDataOp As New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Gajanan [22-Feb-2019] -- End


        Try
            strQ = "UPDATE hrdependant_membership_tran SET " & _
                     "  isvoid = @isvoid" & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voidreason = @voidreason " & _
                    "WHERE dpndtbeneficetranunkid = @dpndtbeneficetranunkid "


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            xDataOpr.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, isVoid.ToString)
            xDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDate)
            xDataOpr.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strVoidRemark.ToString)
            xDataOpr.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserId)
            xDataOpr.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantTranUnkid.ToString)

            Call xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If
            'Gajanan [22-Feb-2019] -- End


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'Gajanan [22-Feb-2019] -- End

            Return True
        Catch ex As Exception
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan [22-Feb-2019] -- End
            DisplayError.Show("-1", ex.Message, "Void_All", mstrModuleName)
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [22-Feb-2019] -- End
        End Try
    End Function
#End Region

End Class
