﻿'************************************************************************************************************************************
'Class Name : clsDependants_Beneficiary_tran.vb
'Purpose    :
'Date       :21/07/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master
Imports System.Security.Cryptography

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsDependants_Beneficiary_tran
    Private Shared ReadOnly mstrModuleName As String = "clsDependants_Beneficiary_tran"
    Dim objDataOperation As clsDataOperation
    Dim objImages As New clshr_images_tran
    Dim mstrMessage As String = ""
    'Sandeep [ 21 Aug 2010 ] -- Start
    Dim objDependantMemInfoTran As New clsDependants_Membership_tran
    Dim objDependantBenefitTran As New clsDependants_Benefit_tran
    'Sandeep [ 21 Aug 2010 ] -- End 


#Region " Private variables "
    Private mintDpndtbeneficetranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mstrFirst_Name As String = String.Empty
    Private mstrLast_Name As String = String.Empty
    Private mstrMiddle_Name As String = String.Empty
    Private mintRelationunkid As Integer
    Private mdtBirthdate As Date
    Private mstrAddress As String = String.Empty
    Private mstrTelephone_No As String = String.Empty
    Private mstrMobile_No As String = String.Empty
    Private mstrPost_Box As String = String.Empty
    Private mintCountryunkid As Integer
    Private mintStateunkid As Integer
    Private mintCityunkid As Integer
    Private mintZipcodeunkid As Integer
    Private mstrEmail As String = String.Empty
    Private mintNationalityunkid As Integer
    Private mstrIdentify_No As String = String.Empty
    Private mblnIsdependant As Boolean
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrImagePath As String = String.Empty

    'S.SANDEEP [ 23 JAN 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
    Private mintGender As Integer
    'S.SANDEEP [ 23 JAN 2012 ] -- END


    'Pinkal (27-Mar-2013) -- Start
    'Enhancement : TRA Changes
    Private mintCompanyID As Integer = -1
    Private mblnImgInDb As Boolean = False
    Private mbytPhoto As Byte() = Nothing
    'Pinkal (27-Mar-2013) -- End


    'Pinkal (01-Apr-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    'Pinkal (01-Apr-2013) -- End

    'Sohail (27 Nov 2014) -- Start
    'HYATT Enhancement - OUTBOUND file integration with Aruti.
    Private mdtPSoft_SyncDateTime As Date
    'Sohail (27 Nov 2014) -- End


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintOldDatabaseImageUnkid As Integer = 0
    'Gajanan [22-Feb-2019] -- End

    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    Private mdtEffective_date As Date
    Private mblnIsactive As Boolean = True
    Private mintReasonunkid As Integer = 0
    Private mintDpndtbeneficestatustranunkid As Integer = 0
    'Sohail (18 May 2019) -- End    

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dpndtbeneficetranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Dpndtbeneficetranunkid() As Integer
        Get
            Return mintDpndtbeneficetranunkid
        End Get
        Set(ByVal value As Integer)
            mintDpndtbeneficetranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set first_name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _First_Name() As String
        Get
            Return mstrFirst_Name
        End Get
        Set(ByVal value As String)
            mstrFirst_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set last_name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Last_Name() As String
        Get
            Return mstrLast_Name
        End Get
        Set(ByVal value As String)
            mstrLast_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set middle_name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Middle_Name() As String
        Get
            Return mstrMiddle_Name
        End Get
        Set(ByVal value As String)
            mstrMiddle_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set relationunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Relationunkid() As Integer
        Get
            Return mintRelationunkid
        End Get
        Set(ByVal value As Integer)
            mintRelationunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set birthdate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Birthdate() As Date
        Get
            Return mdtBirthdate
        End Get
        Set(ByVal value As Date)
            mdtBirthdate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Address() As String
        Get
            Return mstrAddress
        End Get
        Set(ByVal value As String)
            mstrAddress = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set telephone_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Telephone_No() As String
        Get
            Return mstrTelephone_No
        End Get
        Set(ByVal value As String)
            mstrTelephone_No = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mobile_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Mobile_No() As String
        Get
            Return mstrMobile_No
        End Get
        Set(ByVal value As String)
            mstrMobile_No = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set post_box
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Post_Box() As String
        Get
            Return mstrPost_Box
        End Get
        Set(ByVal value As String)
            mstrPost_Box = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stateunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Stateunkid() As Integer
        Get
            Return mintStateunkid
        End Get
        Set(ByVal value As Integer)
            mintStateunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cityunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Cityunkid() As Integer
        Get
            Return mintCityunkid
        End Get
        Set(ByVal value As Integer)
            mintCityunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set zipcodeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Zipcodeunkid() As Integer
        Get
            Return mintZipcodeunkid
        End Get
        Set(ByVal value As Integer)
            mintZipcodeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set email
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Email() As String
        Get
            Return mstrEmail
        End Get
        Set(ByVal value As String)
            mstrEmail = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set nationalityunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Nationalityunkid() As Integer
        Get
            Return mintNationalityunkid
        End Get
        Set(ByVal value As Integer)
            mintNationalityunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set identify_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Identify_No() As String
        Get
            Return mstrIdentify_No
        End Get
        Set(ByVal value As String)
            mstrIdentify_No = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isdependant
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isdependant() As Boolean
        Get
            Return mblnIsdependant
        End Get
        Set(ByVal value As Boolean)
            mblnIsdependant = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set Image Path
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _ImagePath() As String
        Get
            Return mstrImagePath
        End Get
        Set(ByVal value As String)
            mstrImagePath = value
        End Set
    End Property


    'S.SANDEEP [ 23 JAN 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
    ''' <summary>
    ''' Purpose: Get or Set gender
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Gender() As Integer
        Get
            Return mintGender
        End Get
        Set(ByVal value As Integer)
            mintGender = Value
        End Set
    End Property
    'S.SANDEEP [ 23 JAN 2012 ] -- END


    'Pinkal (27-Mar-2013) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set companyunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _CompanyId() As Integer
        Get
            Return mintCompanyID
        End Get
        Set(ByVal value As Integer)
            mintCompanyID = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set blnImgInDb
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _blnImgInDb() As Boolean
        Get
            Return mblnImgInDb
        End Get
        Set(ByVal value As Boolean)
            mblnImgInDb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Photo
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Photo() As Byte()
        Get
            Return mbytPhoto
        End Get
        Set(ByVal value As Byte())
            mbytPhoto = value
        End Set
    End Property

    'Pinkal (27-Mar-2013) -- End


    'Pinkal (01-Apr-2013) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    'Pinkal (01-Apr-2013) -- End

    'Sohail (27 Nov 2014) -- Start
    'HYATT Enhancement - OUTBOUND file integration with Aruti.
    Public Property _PSoft_SyncDateTime() As Date
        Get
            Return mdtPSoft_SyncDateTime
        End Get
        Set(ByVal value As Date)
            mdtPSoft_SyncDateTime = value
        End Set
    End Property
    'Sohail (27 Nov 2014) -- End

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Public ReadOnly Property _OldDatabaseImageUnkid() As Integer
        Get
            Return mintOldDatabaseImageUnkid
        End Get
    End Property
    'Gajanan [22-Feb-2019] -- End

    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    Public Property _Effective_date() As Date
        Get
            Return mdtEffective_date
        End Get
        Set(ByVal value As Date)
            mdtEffective_date = value
        End Set
    End Property

    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    Public Property _Reasonunkid() As Integer
        Get
            Return mintReasonunkid
        End Get
        Set(ByVal value As Integer)
            mintReasonunkid = value
        End Set
    End Property

    Private mblnIsFromStatus As Boolean = False
    Public WriteOnly Property _IsFromStatus() As Boolean

        Set(ByVal value As Boolean)
            mblnIsFromStatus = value
        End Set
    End Property

    Public ReadOnly Property _Dpndtbeneficestatustranunkid() As Integer
        Get
            Return mintDpndtbeneficestatustranunkid
        End Get
    End Property

    Private xDataOp As clsDataOperation
    Public WriteOnly Property _xDataOp() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOp = value
        End Set
    End Property

    Private mintLoginemployeeunkid As Integer = 0
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = Value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private mblnIsweb As Boolean = False
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mstrDeletedpndtbeneficestatustranIDs As String = String.Empty
    Public WriteOnly Property _DeletedpndtbeneficestatustranIDs() As String
        Set(ByVal value As String)
            mstrDeletedpndtbeneficestatustranIDs = value
        End Set
    End Property
    'Sohail (18 May 2019) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal intDpndtbeneficetranunkid As Integer = 0, Optional ByVal dtAsOnDate As Date = Nothing)
        'Sohail (18 May 2019) - [intDpndtbeneficetranunkid, dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (18 May 2019) -- Start
        'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (18 May 2019) -- End

        'Sohail (18 May 2019) -- Start
        'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
        If intDpndtbeneficetranunkid > 0 Then mintDpndtbeneficetranunkid = intDpndtbeneficetranunkid
        'Sohail (18 May 2019) -- End

        Try
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            strQ = "SELECT * " & _
                        "INTO #TableDepn " & _
                        "FROM " & _
                        "( " & _
                            "SELECT * " & _
                                 ", DENSE_RANK() OVER (PARTITION BY dpndtbeneficetranunkid ORDER BY effective_date DESC, dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                            "FROM hrdependant_beneficiaries_status_tran " & _
                            "WHERE isvoid = 0 "

            If mintDpndtbeneficetranunkid > 0 Then
                strQ &= " AND hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = @dpndtbeneficetranunkid "
            End If

            If dtAsOnDate <> Nothing Then
                strQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
                objDataOperation.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            strQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End

            strQ &= "SELECT " & _
                      "  hrdependants_beneficiaries_tran.dpndtbeneficetranunkid " & _
                      ", hrdependants_beneficiaries_tran.employeeunkid " & _
                      ", hrdependants_beneficiaries_tran.first_name " & _
                      ", hrdependants_beneficiaries_tran.last_name " & _
                      ", hrdependants_beneficiaries_tran.middle_name " & _
                      ", hrdependants_beneficiaries_tran.relationunkid " & _
                      ", hrdependants_beneficiaries_tran.birthdate " & _
                      ", hrdependants_beneficiaries_tran.address " & _
                      ", hrdependants_beneficiaries_tran.telephone_no " & _
                      ", hrdependants_beneficiaries_tran.mobile_no " & _
                      ", hrdependants_beneficiaries_tran.post_box " & _
                      ", hrdependants_beneficiaries_tran.countryunkid " & _
                      ", hrdependants_beneficiaries_tran.stateunkid " & _
                      ", hrdependants_beneficiaries_tran.cityunkid " & _
                      ", hrdependants_beneficiaries_tran.zipcodeunkid " & _
                      ", hrdependants_beneficiaries_tran.email " & _
                      ", hrdependants_beneficiaries_tran.nationalityunkid " & _
                      ", hrdependants_beneficiaries_tran.identify_no " & _
                      ", hrdependants_beneficiaries_tran.isdependant " & _
                      ", hrdependants_beneficiaries_tran.userunkid " & _
                      ", hrdependants_beneficiaries_tran.isvoid " & _
                      ", hrdependants_beneficiaries_tran.voiduserunkid " & _
                      ", hrdependants_beneficiaries_tran.voiddatetime " & _
                      ", hrdependants_beneficiaries_tran.voidreason " & _
                      ", hrdependants_beneficiaries_tran.gender " & _
                      ", hrdependants_beneficiaries_tran.psoft_syncdatetime " & _
                      ", CONVERT(CHAR(8), #TableDepn.effective_date, 112) AS effective_date " & _
                      ", ISNULL(#TableDepn.isactive, 1) AS isactive " & _
                      ", ISNULL(#TableDepn.reasonunkid, 0) AS reasonunkid " & _
                      ", ISNULL(#TableDepn.dpndtbeneficestatustranunkid, 0) AS dpndtbeneficestatustranunkid " & _
                     "FROM hrdependants_beneficiaries_tran " & _
                     " LEFT JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                     "WHERE hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = @dpndtbeneficetranunkid "
            'Sohail (18 May 2019) - [effective_date, isactive, reasonunkid, dpndtbeneficestatustranunkid, ROWNO, JOIN #TableDep]
            'Sohail (27 Nov 2014) - [psoft_syncdatetime]

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            strQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintDpndtbeneficeTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintdpndtbeneficetranunkid = CInt(dtRow.Item("dpndtbeneficetranunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mstrfirst_name = dtRow.Item("first_name").ToString
                mstrlast_name = dtRow.Item("last_name").ToString
                mstrmiddle_name = dtRow.Item("middle_name").ToString
                mintRelationunkid = CInt(dtRow.Item("relationunkid"))
                If IsDBNull(dtRow.Item("birthdate")) Then
                    mdtBirthdate = Nothing
                Else
                    mdtBirthdate = dtRow.Item("birthdate")
                End If
                mstrAddress = dtRow.Item("address").ToString
                mstrtelephone_no = dtRow.Item("telephone_no").ToString
                mstrmobile_no = dtRow.Item("mobile_no").ToString
                mstrpost_box = dtRow.Item("post_box").ToString

                'Gajanan [21-June-2019] -- Start      
                'mintcountryunkid = CInt(dtRow.Item("countryunkid"))
                'mintstateunkid = CInt(dtRow.Item("stateunkid"))
                'mintcityunkid = CInt(dtRow.Item("cityunkid"))
                'mintzipcodeunkid = CInt(dtRow.Item("zipcodeunkid"))
                If CInt(dtRow.Item("countryunkid")) > 0 Then
                mintcountryunkid = CInt(dtRow.Item("countryunkid"))
                Else
                    mintCountryunkid = 0
                End If

                If CInt(dtRow.Item("stateunkid")) > 0 Then
                mintstateunkid = CInt(dtRow.Item("stateunkid"))
                Else
                    mintStateunkid = 0
                End If

                If CInt(dtRow.Item("cityunkid")) > 0 Then
                mintcityunkid = CInt(dtRow.Item("cityunkid"))
                Else
                    mintCityunkid = 0
                End If


                If CInt(dtRow.Item("zipcodeunkid")) > 0 Then
                mintzipcodeunkid = CInt(dtRow.Item("zipcodeunkid"))
                Else
                    mintZipcodeunkid = 0
                End If
                'Gajanan [21-June-2019] -- End


              
                mstremail = dtRow.Item("email").ToString
                'Gajanan [21-June-2019] -- Start
                'mintNationalityunkid = CInt(dtRow.Item("nationalityunkid"))
                If CInt(dtRow.Item("nationalityunkid")) > 0 Then
                mintnationalityunkid = CInt(dtRow.Item("nationalityunkid"))
                Else
                    mintNationalityunkid = 0
                End If
                'Gajanan [21-June-2019] -- End

                mstridentify_no = dtRow.Item("identify_no").ToString
                mblnisdependant = CBool(dtRow.Item("isdependant"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintGender = CInt(dtRow.Item("gender"))
                'Sohail (27 Nov 2014) -- Start
                'HYATT Enhancement - OUTBOUND file integration with Aruti.
                If IsDBNull(dtRow.Item("psoft_syncdatetime")) Then
                    mdtPSoft_SyncDateTime = Nothing
                Else
                    mdtPSoft_SyncDateTime = dtRow.Item("psoft_syncdatetime")
                End If
                'Sohail (27 Nov 2014) -- End
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                If IsDBNull(dtRow.Item("effective_date")) Then
                    mdtEffective_date = Nothing
                Else
                    mdtEffective_date = eZeeDate.convertDate(dtRow.Item("effective_date").ToString)
                End If
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mintReasonunkid = CBool(dtRow.Item("reasonunkid"))
                mintDpndtbeneficestatustranunkid = CInt(dtRow.Item("dpndtbeneficestatustranunkid"))
                'Sohail (18 May 2019) -- End

                Exit For
            Next


            'Sandeep [ 09 Oct 2010 ] -- Start
            'Issues Reported by Vimal
            'objImages._Referenceid = 2
            objImages._Referenceid = enImg_Email_RefId.Dependants_Beneficiaries
            'Sandeep [ 09 Oct 2010 ] -- End 
            objImages._Transactionid = mintDpndtbeneficetranunkid
            objImages._Employeeunkid = mintEmployeeunkid
            mstrImagePath = objImages._Imagename


            'Pinkal (27-Mar-2013) -- Start
            'Enhancement : TRA Changes
            If mblnImgInDb Then
                Dim objDependantImg As New clsdependant_Images
                objDependantImg.GetData(mintCompanyID, mintEmployeeunkid, mintDpndtbeneficetranunkid, True, objDataOperation)
                mbytPhoto = objDependantImg._Photo

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                mintOldDatabaseImageUnkid = objDependantImg._Dependantimgunkid
                'Gajanan [22-Feb-2019] -- End

            End If
            'Pinkal (27-Mar-2013) -- End


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (18 May 2019) -- End
        End Try
    End Sub



    'Pinkal (06-Mar-2013) -- Start
    'Enhancement : TRA Changes

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal intEmployeeID As Integer = -1, Optional ByVal strFilerString As String = "", _
                            Optional ByVal IsUsedAsMSS As Boolean = True, _
                            Optional ByVal mblnAddApprovalCondition As Boolean = True, _
                            Optional ByVal dtAsOnDate As Date = Nothing, _
                            Optional ByVal intActiveInactive As Integer = 1, _
                            Optional ByVal blnAddGrouping As Boolean = False, _
                            Optional ByVal intDependantBenfTranUnkId As Integer = 0 _
                            ) As DataSet
        'Sohail (18 May 2019) - [dtAsOnDate, intActiveInactive, blnAddGrouping]

        'S.SANDEEP [20-JUN-2018] -- 'Enhancement - Implementing Employee Approver Flow For NMB .[Optional ByVal mblnAddApprovalCondition As Boolean = True]
        'Pinkal (28-Dec-2015) -- Start    'Enhancement - Working on Changes in SS for Employee Master. [Optional ByVal IsUsedAsMSS As Boolean = True] 

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)

            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            'If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , mblnAddApprovalCondition)
            'S.SANDEEP [20-JUN-2018] -- End

            'Pinkal (28-Dec-2015) -- End
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation

                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                StrQ = "SELECT * " & _
                       "INTO #TableDepn " & _
                       "FROM " & _
                       "( " & _
                           "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                           "FROM hrdependant_beneficiaries_status_tran "

                If intEmployeeID > 0 Then
                    StrQ &= "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid "
                End If

                StrQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

                If intEmployeeID > 0 Then
                    StrQ &= "AND hrdependants_beneficiaries_tran.employeeunkid = @employeeunkid "
                End If

                If intDependantBenfTranUnkId > 0 Then
                    StrQ &= " AND hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = @dpndtbeneficetranunkid "
                End If

                If dtAsOnDate <> Nothing Then
                    StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
                    objDo.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
                Else
                    'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
                End If

                StrQ &= ") AS A " & _
                        "WHERE 1 = 1 "

                If intDependantBenfTranUnkId <= 0 Then
                    StrQ &= " AND A.ROWNO = 1 "
                End If
                'Sohail (18 May 2019) -- End

                StrQ &= "SELECT " & _
                       " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
                       ",ISNULL(hrdependants_beneficiaries_tran.first_name,'') +  ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name,'') +' '+ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS DpndtBefName " & _
                       ",ISNULL(cfcommon_master.name,'') AS Relation " & _
                       ",ISNULL(hrdependants_beneficiaries_tran.identify_no,'') AS IdNo " & _
                       ",ISNULL(hrdependants_beneficiaries_tran.first_name,'') AS FName " & _
                       ",ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS LName " & _
                       ",cfcommon_master.masterunkid AS RelationId " & _
                       ",hrdependants_beneficiaries_tran.countryunkid AS CountryId " & _
                       ",hremployee_master.employeeunkid AS EmpId " & _
                       ",hrdependants_beneficiaries_tran.dpndtbeneficetranunkid AS DpndtTranId " & _
                       ",ISNULL(hrdependants_beneficiaries_tran.isdependant,0) AS IsDependant " & _
                       ",CASE WHEN hrdependants_beneficiaries_tran.gender = 1 THEN @Male " & _
                       "       WHEN hrdependants_beneficiaries_tran.gender = 2 THEN @Female " & _
                       " ELSE '' END AS Gender " & _
                       ",hrdependants_beneficiaries_tran.gender AS GenderId " & _
                       ",ISNULL(CONVERT(CHAR(8),hrdependants_beneficiaries_tran.birthdate,112),'') as birthdate " & _
                       ", hrdependants_beneficiaries_tran.birthdate AS dtbirthdate " & _
                       ", hrdependants_beneficiaries_tran.middle_name " & _
                       ", hrdependants_beneficiaries_tran.address " & _
                       ", hrdependants_beneficiaries_tran.telephone_no " & _
                       ", hrdependants_beneficiaries_tran.mobile_no " & _
                       ", hrdependants_beneficiaries_tran.post_box " & _
                       ", hrdependants_beneficiaries_tran.stateunkid " & _
                       ", hrdependants_beneficiaries_tran.cityunkid " & _
                       ", hrdependants_beneficiaries_tran.zipcodeunkid " & _
                       ", hrdependants_beneficiaries_tran.email " & _
                       ", hrdependants_beneficiaries_tran.nationalityunkid " & _
                       ", #TableDepn.effective_date AS dteffective_date " & _
                       ", CONVERT(CHAR(8), #TableDepn.effective_date, 112) AS effective_date " & _
                       ", ISNULL(#TableDepn.isactive, 1) AS isactive " & _
                       ", ISNULL(#TableDepn.reasonunkid, 0) AS reasonunkid " & _
                       ", ISNULL(DepReason.name, '') AS Reason " & _
                       ", ISNULL(#TableDepn.dpndtbeneficestatustranunkid, 0) AS dpndtbeneficestatustranunkid " & _
                       ", ISNULL(#TableDepn.ROWNO, 1) AS ROWNO " & _
                       ", 0 AS operationtypeid " & _
                       ", '' AS OperationType " & _
                       ", CAST(0 AS BIT) AS IsGrp " & _
                       ", CAST(0 AS BIT) AS IsChecked " & _
                       ", hremployee_master.isapproved " & _
                       "FROM hrdependants_beneficiaries_tran " & _
                       " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND cfcommon_master.mastertype = " & enCommonMaster.RELATIONS & _
                       " LEFT JOIN hremployee_master ON hrdependants_beneficiaries_tran.employeeunkid = hremployee_master.employeeunkid "

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= " LEFT JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                       " LEFT JOIN cfcommon_master AS DepReason ON DepReason.masterunkid = #TableDepn.reasonunkid AND DepReason.mastertype = " & enCommonMaster.DEPENDANT_ACTIVE_INACTIVE_REASON & " "
                'Sohail (18 May 2019) - [middle_name, address, telephone_no, mobile_no, post_box, stateunkid, cityunkid, zipcodeunkid, email, nationalityunkid, effective_date, isactive, reasonunkid, ROWNO, IsGrp, isapproved]

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END



                StrQ &= " WHERE ISNULL(hrdependants_beneficiaries_tran.isvoid,0) = 0 "

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If intEmployeeID > 0 Then
                    StrQ &= " AND hrdependants_beneficiaries_tran.employeeunkid = @employeeunkid  "
                    objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
                End If

                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                If intDependantBenfTranUnkId > 0 Then
                    StrQ &= " AND hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = @dpndtbeneficetranunkid "
                    objDo.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDependantBenfTranUnkId)
                End If

                If intActiveInactive = 1 Then
                    StrQ &= "AND #TableDepn.isactive = 1 "
                ElseIf intActiveInactive = 2 Then
                    StrQ &= "AND #TableDepn.isactive = 0 "
                End If
                'Sohail (18 May 2019) -- End

                If strFilerString.Trim.Length > 0 Then
                    StrQ &= " AND " & strFilerString
                End If

                objDo.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
                objDo.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

                StrQ &= " ORDER BY ISNULL(CONVERT(CHAR(8),hrdependants_beneficiaries_tran.birthdate,112),'') "

                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                StrQ &= " DROP TABLE #TableDepn "
                'Sohail (18 May 2019) -- End

                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                If blnAddGrouping = True Then

                    Dim dt As DataTable = New DataView(dsList.Tables(0)).ToTable(True, "EmpName", "EmpId")
                    Dim dtCol As New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                    dtCol.DefaultValue = False
                    dtCol.AllowDBNull = False
                    dt.Columns.Add(dtCol)

                    dtCol = New DataColumn("IsGrp", System.Type.GetType("System.Boolean"))
                    dtCol.DefaultValue = True
                    dtCol.AllowDBNull = False
                    dt.Columns.Add(dtCol)

                    dt.Merge(dsList.Tables(0), False)
                    dt.DefaultView.Sort = "EmpName, EmpId, IsGrp DESC"

                    dsList.Tables.Clear()
                    dsList.Tables.Add(dt.DefaultView.ToTable)
                End If
                'Sohail (18 May 2019) -- End

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    'Public Function GetList(ByVal strTableName As String, _
    '                        Optional ByVal strIncludeInactiveEmployee As String = "", _
    '                        Optional ByVal strEmployeeAsOnDate As String = "", _
    '                        Optional ByVal strUserAccessLevelFilterString As String = "", _
    '                        Optional ByVal intEmployeeID As Integer = -1) As DataSet


    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try


    '        'Pinkal (19-Nov-2012) -- Start
    '        'Enhancement : TRA Changes
    '        '",ISNULL(hrdependants_beneficiaries_tran.first_name,'') +' '+ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS DpndtBefName " & _
    '        'Pinkal (19-Nov-2012) -- End


    '        strQ = "SELECT " & _
    '                 " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '                 ",ISNULL(hrdependants_beneficiaries_tran.first_name,'') +  ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name,'') +' '+ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS DpndtBefName " & _
    '                 ",ISNULL(cfcommon_master.name,'') AS Relation " & _
    '                 ",ISNULL(hrdependants_beneficiaries_tran.identify_no,'') AS IdNo " & _
    '                 ",ISNULL(hrdependants_beneficiaries_tran.first_name,'') AS FName " & _
    '                 ",ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS LName " & _
    '                 ",cfcommon_master.masterunkid AS RelationId " & _
    '                 ",hrdependants_beneficiaries_tran.countryunkid AS CountryId " & _
    '                 ",hremployee_master.employeeunkid AS EmpId " & _
    '                 ",hrdependants_beneficiaries_tran.dpndtbeneficetranunkid AS DpndtTranId " & _
    '                 ",ISNULL(hrdependants_beneficiaries_tran.isdependant,0) AS IsDependant "

    '        'Anjan (21 Nov 2011)-Start
    '        'ENHANCEMENT : TRA COMMENTS
    '        strQ &= ",ISNULL(hremployee_master.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                   ",ISNULL(hremployee_master.unitgroupunkid,0) AS unitgroupunkid " & _
    '                   ",ISNULL(hremployee_master.teamunkid,0) AS teamunkid " & _
    '                   ",ISNULL(hremployee_master.stationunkid,0) AS stationunkid " & _
    '                   ",ISNULL(hremployee_master.deptgroupunkid,0) AS deptgroupunkid " & _
    '                   ",ISNULL(hremployee_master.departmentunkid,0) AS departmentunkid " & _
    '                   ",ISNULL(hremployee_master.sectionunkid,0) AS sectionunkid " & _
    '                   ",ISNULL(hremployee_master.unitunkid,0) AS unitunkid " & _
    '                   ",ISNULL(hremployee_master.jobunkid,0) AS jobunkid " & _
    '                   ",ISNULL(hremployee_master.classgroupunkid,0) AS classgroupunkid " & _
    '                   ",ISNULL(hremployee_master.classunkid,0) AS classunkid " & _
    '                   ",ISNULL(hremployee_master.jobgroupunkid,0) AS jobgroupunkid " & _
    '                   ",ISNULL(hremployee_master.gradegroupunkid,0) AS gradegroupunkid " & _
    '                   ",ISNULL(hremployee_master.gradeunkid,0) AS gradeunkid " & _
    '                   ",ISNULL(hremployee_master.gradelevelunkid,0) AS gradelevelunkid " & _
    '                   ", hrdependants_beneficiaries_tran.psoft_syncdatetime "
    '        'Sohail (27 Nov 2014) - [psoft_syncdatetime]
    '        'Anjan (21 Nov 2011)-End 

    '        'S.SANDEEP [ 23 JAN 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
    '        strQ &= ", CASE WHEN hrdependants_beneficiaries_tran.gender = 1 THEN @Male " & _
    '                "       WHEN hrdependants_beneficiaries_tran.gender = 2 THEN @Female " & _
    '                "  ELSE '' END AS Gender " & _
    '                ", hrdependants_beneficiaries_tran.gender AS GenderId " & _
    '                ", ISNULL(CONVERT(CHAR(8),hrdependants_beneficiaries_tran.birthdate,112),'') as birthdate "
    '        'S.SANDEEP [ 23 JAN 2012 ] -- END

    '        strQ &= "FROM " & _
    '                    "hrdependants_beneficiaries_tran " & _
    '                    "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
    '                        "AND cfcommon_master.mastertype = " & enCommonMaster.RELATIONS & _
    '                 "LEFT JOIN hremployee_master ON hrdependants_beneficiaries_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '            "WHERE ISNULL(hrdependants_beneficiaries_tran.isvoid,0) = 0 "





    '        'Pinkal (06-Mar-2013) -- Start
    '        'Enhancement : TRA Changes

    '        If intEmployeeID > 0 Then
    '            strQ &= " AND hrdependants_beneficiaries_tran.employeeunkid  = " & intEmployeeID & "  "
    '        End If
    '        'Pinkal (06-Mar-2013) -- End



    '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        ''S.SANDEEP [ 29 JUNE 2011 ] -- START
    '        ''ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    '        'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '        '    'Sohail (06 Jan 2012) -- Start
    '        '    'TRA - ENHANCEMENT
    '        '    'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '        '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

    '        '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    'Sohail (06 Jan 2012) -- End
    '        'End If
    '        ''S.SANDEEP [ 29 JUNE 2011 ] -- END
    '        If strIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '        End If

    '        If CBool(strIncludeInactiveEmployee) = False Then
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '        End If
    '        'S.SANDEEP [ 27 APRIL 2012 ] -- END




    '        'Anjan (24 Jun 2011)-Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.


    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '        'End If


    '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES

    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
    '        '        End If
    '        'End Select

    '        If strUserAccessLevelFilterString = "" Then
    '            strQ &= UserAccessLevel._AccessLevelFilterString
    '        Else
    '            strQ &= strUserAccessLevelFilterString
    '        End If
    '        'S.SANDEEP [ 27 APRIL 2012 ] -- END



    '        'S.SANDEEP [ 04 FEB 2012 ] -- END

    '        'Anjan (24 Jun 2011)-End 


    '        'S.SANDEEP [ 23 JAN 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
    '        objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
    '        objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))
    '        'S.SANDEEP [ 23 JAN 2012 ] -- END


    '        'Pinkal (12-Jun-2012) -- Start
    '        'Enhancement : TRA Changes
    '        strQ &= " ORDER BY ISNULL(CONVERT(CHAR(8),hrdependants_beneficiaries_tran.birthdate,112),'') "
    '        'Pinkal (12-Jun-2012) -- End


    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'S.SANDEEP [04 JUN 2015] -- END

    'Pinkal (06-Mar-2013) -- End

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrdependants_beneficiaries_tran) </purpose>
    ''' Sandeep [ 21 Aug 2010 ] -- Start
    ''' Public Function Insert() As Boolean
    '''SHANI (20 JUN 2015) -- Start
    '''Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    '''Public Function Insert(Optional ByVal dtMemTran As DataTable = Nothing, Optional ByVal dtBenfTran As DataTable = Nothing) As Boolean
    ''' 


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.

    'Public Function Insert(Optional ByVal dtMemTran As DataTable = Nothing, Optional ByVal dtBenfTran As DataTable = Nothing) As Boolean
    Public Function Insert(Optional ByVal dtMemTran As DataTable = Nothing, _
                           Optional ByVal dtBenfTran As DataTable = Nothing, _
                           Optional ByVal dtDependantAttachment As DataTable = Nothing, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'Gajanan [22-Feb-2019] -- End

        'SHANI (20 JUN 2015) -- End 
        'Sandeep [ 21 Aug 2010 ] -- End 

        'Sandeep [ 14 Aug 2010 ] -- Start
        If isExist(mstrFirst_Name, mstrLast_Name, mstrMiddle_Name, mstrIdentify_No, mintEmployeeunkid) Then
            If mblnIsdependant = True Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Dependant is already defined. Please define new Dependant.")
            Else
                mstrMessage = Language.getMessage(mstrModuleName, 2, "This Beneficiary is already defined. Please define new Beneficiary.")
            End If
            Return False
        End If
        'Sandeep [ 14 Aug 2010 ] -- End 
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Gajanan [22-Feb-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.

        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Gajanan [22-Feb-2019] -- End



        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@first_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFirst_Name.ToString)
            objDataOperation.AddParameter("@last_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLast_Name.ToString)
            objDataOperation.AddParameter("@middle_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMiddle_Name.ToString)
            objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelationunkid.ToString)
            If mdtBirthdate = Nothing Then
                objDataOperation.AddParameter("@birthdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@birthdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtBirthdate)
            End If
            objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress.ToString)
            objDataOperation.AddParameter("@telephone_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTelephone_No.ToString)
            objDataOperation.AddParameter("@mobile_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMobile_No.ToString)
            objDataOperation.AddParameter("@post_box", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPost_Box.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDataOperation.AddParameter("@zipcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintZipcodeunkid.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@nationalityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNationalityunkid.ToString)
            objDataOperation.AddParameter("@identify_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIdentify_No.ToString)
            'objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipunkid.ToString)
            'objDataOperation.AddParameter("@membership_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMembership_No.ToString)
            'objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitgroupunkid.ToString)
            'objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitplanunkid.ToString)
            'objDataOperation.AddParameter("@benefit_percent", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblBenefit_Percent.ToString)
            'objDataOperation.AddParameter("@benefit_amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblBenefit_Amount.ToString)
            objDataOperation.AddParameter("@isdependant", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsdependant.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@gender", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGender.ToString)
            'Sohail (27 Nov 2014) -- Start
            'HYATT Enhancement - OUTBOUND file integration with Aruti.
            If mdtPSoft_SyncDateTime = Nothing Then
                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPSoft_SyncDateTime)
            End If
            'Sohail (27 Nov 2014) -- End

            strQ = "INSERT INTO hrdependants_beneficiaries_tran ( " & _
                          "  employeeunkid " & _
                          ", first_name " & _
                          ", last_name " & _
                          ", middle_name " & _
                          ", relationunkid " & _
                          ", birthdate " & _
                          ", address " & _
                          ", telephone_no " & _
                          ", mobile_no " & _
                          ", post_box " & _
                          ", countryunkid " & _
                          ", stateunkid " & _
                          ", cityunkid " & _
                          ", zipcodeunkid " & _
                          ", email " & _
                          ", nationalityunkid " & _
                          ", identify_no " & _
                          ", isdependant " & _
                          ", userunkid " & _
                          ", isvoid " & _
                          ", voiduserunkid " & _
                          ", voiddatetime " & _
                          ", voidreason" & _
                          ", gender " & _
                          ", psoft_syncdatetime " & _
                        ") VALUES (" & _
                          "  @employeeunkid " & _
                          ", @first_name " & _
                          ", @last_name " & _
                          ", @middle_name " & _
                          ", @relationunkid " & _
                          ", @birthdate " & _
                          ", @address " & _
                          ", @telephone_no " & _
                          ", @mobile_no " & _
                          ", @post_box " & _
                          ", @countryunkid " & _
                          ", @stateunkid " & _
                          ", @cityunkid " & _
                          ", @zipcodeunkid " & _
                          ", @email " & _
                          ", @nationalityunkid " & _
                          ", @identify_no " & _
                          ", @isdependant " & _
                          ", @userunkid " & _
                          ", @isvoid " & _
                          ", @voiduserunkid " & _
                          ", @voiddatetime " & _
                          ", @voidreason" & _
                          ", @gender " & _
                          ", @psoft_syncdatetime " & _
                        "); SELECT @@identity"
            'Sohail (27 Nov 2014) - [psoft_syncdatetime]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintDpndtbeneficetranunkid = dsList.Tables(0).Rows(0).Item(0)

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            Dim objDBStatus As New clsDependant_Benefice_Status_tran
            objDBStatus._Dpndtbeneficetranunkid = mintDpndtbeneficetranunkid
            objDBStatus._Effective_Date = mdtEffective_date
            objDBStatus._Isactive = True
            objDBStatus._Reasonunkid = 0
            objDBStatus._Userunkid = mintUserunkid
            objDBStatus._xDataOp = objDataOperation
            objDBStatus._FormName = mstrWebFormName
            objDBStatus._HostName = mstrWebHostName
            objDBStatus._ClientIP = mstrWebClientIP
            objDBStatus._AuditUserId = mintUserunkid
            objDBStatus._Loginemployeeunkid = mintLoginemployeeunkid
            objDBStatus._AuditDate = mdtAuditDate
            objDBStatus._Isweb = mblnIsweb
            objDBStatus._CompanyUnkid = mintCompanyID

            If objDBStatus.Insert() = False Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If
            'Sohail (18 May 2019) -- End


            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            If dtDependantAttachment IsNot Nothing Then
                Dim objDocument As New clsScan_Attach_Documents
                Dim dtTran As DataTable = objDocument._Datatable
                Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\"
                Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.DEPENDANTS).Tables(0).Rows(0)("Name").ToString

                If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"

                Dim dr As DataRow
                For Each drow As DataRow In dtDependantAttachment.Rows
                    dr = dtTran.NewRow
                    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                    dr("documentunkid") = drow("documentunkid")
                    dr("employeeunkid") = drow("employeeunkid")
                    dr("filename") = drow("filename")
                    dr("scanattachrefid") = drow("scanattachrefid")
                    dr("modulerefid") = drow("modulerefid")
                    dr("transactionunkid") = mintDpndtbeneficetranunkid
                    dr("userunkid") = User._Object._Userunkid
                    dr("attached_date") = drow("attached_date")
                    dr("orgfilepath") = drow("localpath")
                    dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                    dr("AUD") = drow("AUD")
                    'SHANI (03 JUL 2015) -- Start
                    dr("userunkid") = mintUserunkid
                    'SHANI (03 JUL 2015) -- End 

                    'SHANI (16 JUL 2015) -- Start
                    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                    'Upload Image folder to access those attachemts from Server and 
                    'all client machines if ArutiSelfService is installed otherwise save then on Document path
                    dr("fileuniquename") = drow("fileuniquename")
                    dr("filepath") = drow("filepath")
                    dr("filesize") = drow("filesize_kb")
                    'SHANI (16 JUL 2015) -- End 


                    'Gajanan [22-Feb-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    dr("form_name") = drow("form_name")
                    'Gajanan [22-Feb-2019] -- End


                    dtTran.Rows.Add(dr)
                Next
                objDocument._Datatable = dtTran
                objDocument.InsertUpdateDelete_Documents(objDataOperation)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If
            'SHANI (20 JUN 2015) -- End 


            'Sandeep [ 21 Aug 2010 ] -- Start

            Dim blnToInsert As Boolean = True

            If dtMemTran IsNot Nothing Then
                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                'objDependantMemInfoTran._DependantTranUnkid = mintDpndtbeneficetranunkid
                'objDependantMemInfoTran._DataTable = dtMemTran
                'Dim blnFlag As Boolean = False
                'blnFlag = objDependantMemInfoTran.InsertUpdateDelete_DependantMembership_Tran()
                'If blnFlag = False Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If
                If dtMemTran.Rows.Count > 0 Then
                    objDependantMemInfoTran._DependantTranUnkid = mintDpndtbeneficetranunkid
                    objDependantMemInfoTran._DataTable = dtMemTran
                    Dim blnFlag As Boolean = False

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'blnFlag = objDependantMemInfoTran.InsertUpdateDelete_DependantMembership_Tran()

                    'Shani(04-MAR-2017) -- Start
                    'Enhancement - Add Membership and Benefit in employee dependant's screen on web
                    'blnFlag = objDependantMemInfoTran.InsertUpdateDelete_DependantMembership_Tran(mintUserunkid)
                    blnFlag = objDependantMemInfoTran.InsertUpdateDelete_DependantMembership_Tran(objDataOperation, mintUserunkid)
                    'Shani(04-MAR-2017) -- End

                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    If blnFlag = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    blnToInsert = False
                Else
                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrdependants_beneficiaries_tran", mintDpndtbeneficetranunkid, "dpndtbeneficetranunkid", 2) Then
                        blnToInsert = True
                    End If
                End If
                'S.SANDEEP [ 12 OCT 2011 ] -- END 
            End If

            If dtBenfTran IsNot Nothing Then
                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                'objDependantBenefitTran._DependantTranUnkid = mintDpndtbeneficetranunkid
                'objDependantBenefitTran._DataTable = dtBenfTran
                'Dim blnFlag As Boolean = False
                'blnFlag = objDependantBenefitTran.InsertUpdateDelete_DependantBenefit_Tran()
                'If blnFlag = False Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If
                If dtBenfTran.Rows.Count > 0 Then
                    objDependantBenefitTran._DependantTranUnkid = mintDpndtbeneficetranunkid
                    objDependantBenefitTran._DataTable = dtBenfTran
                    Dim blnFlag As Boolean = False

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'blnFlag = objDependantBenefitTran.InsertUpdateDelete_DependantBenefit_Tran()

                    'Shani(04-MAR-2017) -- Start
                    'Enhancement - Add Membership and Benefit in employee dependant's screen on web
                    'blnFlag = objDependantBenefitTran.InsertUpdateDelete_DependantBenefit_Tran(mintUserunkid)
                    blnFlag = objDependantBenefitTran.InsertUpdateDelete_DependantBenefit_Tran(objDataOperation, mintUserunkid)
                    'Shani(04-MAR-2017) -- End

                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    If blnFlag = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    blnToInsert = False
                Else
                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrdependants_beneficiaries_tran", mintDpndtbeneficetranunkid, "dpndtbeneficetranunkid", 2, objDataOperation) Then
                        blnToInsert = True
                    End If
                End If
                'S.SANDEEP [ 12 OCT 2011 ] -- END 
            End If

            If blnToInsert = True Then

                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", mintDpndtbeneficetranunkid, "hrdependant_membership_tran", "dpndtmembershiptranunkid", -1, 1, 0) = False Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If

                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", mintDpndtbeneficetranunkid, "hrdependant_membership_tran", "dpndtmembershiptranunkid", -1, 1, 0, , mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'S.SANDEEP [ 27 APRIL 2012 ] -- END

            End If

            'Sandeep [ 21 Aug 2010 ] -- End 

            'Pinkal (27-Mar-2013) -- Start
            'Enhancement : TRA Changes

            If mblnImgInDb = False Then

                If mstrImagePath.Trim <> "" Then
                    objImages._Referenceid = enImg_Email_RefId.Dependants_Beneficiaries
                    objImages._Employeeunkid = mintEmployeeunkid
                    objImages._Imagename = mstrImagePath
                    objImages._Isapplicant = False
                    objImages._Transactionid = mintDpndtbeneficetranunkid
                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objImages.Insert()
                    If objImages.Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END
                Else
                    objImages._Referenceid = enImg_Email_RefId.Dependants_Beneficiaries
                    objImages._Employeeunkid = mintEmployeeunkid
                    objImages._Imagename = mstrImagePath
                    objImages._Isapplicant = False
                    objImages._Transactionid = mintDpndtbeneficetranunkid
                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objImages.Delete()
                    If objImages.Delete(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END
                End If
                objImages = Nothing

            Else

                If SaveEmpDependantImage(objDataOperation, mintCompanyID, mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'Pinkal (27-Mar-2013) -- End

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'Gajanan [22-Feb-2019] -- End
            Return True

        Catch ex As Exception
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan [22-Feb-2019] -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [22-Feb-2019] -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrdependants_beneficiaries_tran) </purpose>
    ''' Sandeep [ 21 Aug 2010 ] -- Start
    ''' Public Function Update() As Boolean
    '''SHANI (20 JUN 2015) -- Start
    '''Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    ''' Public Function Update(Optional ByVal dtMemTran As DataTable = Nothing, Optional ByVal dtBenfTran As DataTable = Nothing) As Boolean
    ''' 


    'Public Function Update(Optional ByVal dtMemTran As DataTable = Nothing, Optional ByVal dtBenfTran As DataTable = Nothing, Optional ByVal dtDependantAttachment As DataTable = Nothing) As Boolean
    Public Function Update(Optional ByVal dtMemTran As DataTable = Nothing, Optional ByVal dtBenfTran As DataTable = Nothing, Optional ByVal dtDependantAttachment As DataTable = Nothing, Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal Isfromapproval As Boolean = False) As Boolean



        'SHANI (20 JUN 2015) -- End 
        'Sandeep [ 21 Aug 2010 ] -- End

        'Sandeep [ 14 Aug 2010 ] -- Start
        If isExist(mstrFirst_Name, mstrLast_Name, mstrMiddle_Name, mstrIdentify_No, mintEmployeeunkid, mintDpndtbeneficetranunkid) Then
            If mblnIsdependant = True Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Dependant is already defined. Please define new Dependant.")
            Else
                mstrMessage = Language.getMessage(mstrModuleName, 2, "This Beneficiary is already defined. Please define new Beneficiary.")
            End If
            Return False
        End If
        'Sandeep [ 14 Aug 2010 ] -- End 

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception



        'Gajanan [22-Feb-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        'Gajanan [22-Feb-2019] -- End



        Try
            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDpndtbeneficetranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@first_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFirst_Name.ToString)
            objDataOperation.AddParameter("@last_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLast_Name.ToString)
            objDataOperation.AddParameter("@middle_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMiddle_Name.ToString)
            objDataOperation.AddParameter("@relationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelationunkid.ToString)
            If mdtBirthdate = Nothing Then
                objDataOperation.AddParameter("@birthdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@birthdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtBirthdate)
            End If
            objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress.ToString)
            objDataOperation.AddParameter("@telephone_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTelephone_No.ToString)
            objDataOperation.AddParameter("@mobile_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMobile_No.ToString)
            objDataOperation.AddParameter("@post_box", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPost_Box.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDataOperation.AddParameter("@zipcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintZipcodeunkid.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@nationalityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNationalityunkid.ToString)
            objDataOperation.AddParameter("@identify_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIdentify_No.ToString)
            'objDataOperation.AddParameter("@membershipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershipunkid.ToString)
            'objDataOperation.AddParameter("@membership_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMembership_No.ToString)
            'objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitgroupunkid.ToString)
            'objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitplanunkid.ToString)
            'objDataOperation.AddParameter("@benefit_percent", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblBenefit_Percent.ToString)
            'objDataOperation.AddParameter("@benefit_amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblBenefit_Amount.ToString)
            objDataOperation.AddParameter("@isdependant", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsdependant.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@gender", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGender.ToString)
            'Sohail (27 Nov 2014) -- Start
            'HYATT Enhancement - OUTBOUND file integration with Aruti.
            If mdtPSoft_SyncDateTime = Nothing Then
                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPSoft_SyncDateTime)
            End If
            'Sohail (27 Nov 2014) -- End

            strQ = "UPDATE hrdependants_beneficiaries_tran SET " & _
                      "  employeeunkid = @employeeunkid" & _
                      ", first_name = @first_name" & _
                      ", last_name = @last_name" & _
                      ", middle_name = @middle_name" & _
                      ", relationunkid = @relationunkid" & _
                      ", birthdate = @birthdate" & _
                      ", address = @address" & _
                      ", telephone_no = @telephone_no" & _
                      ", mobile_no = @mobile_no" & _
                      ", post_box = @post_box" & _
                      ", countryunkid = @countryunkid" & _
                      ", stateunkid = @stateunkid" & _
                      ", cityunkid = @cityunkid" & _
                      ", zipcodeunkid = @zipcodeunkid" & _
                      ", email = @email" & _
                      ", nationalityunkid = @nationalityunkid" & _
                      ", identify_no = @identify_no" & _
                      ", isdependant = @isdependant" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                      ", gender = @gender " & _
                      ", psoft_syncdatetime  = @psoft_syncdatetime " & _
                    "WHERE dpndtbeneficetranunkid = @dpndtbeneficetranunkid "
            'Sohail (27 Nov 2014) - [psoft_syncdatetime]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            If mblnIsFromStatus = True Then
                Dim objDBStatus As New clsDependant_Benefice_Status_tran
                objDBStatus._Dpndtbeneficetranunkid = mintDpndtbeneficetranunkid
                objDBStatus._Effective_Date = mdtEffective_date
                objDBStatus._Isactive = mblnIsactive
                objDBStatus._Reasonunkid = mintReasonunkid
                objDBStatus._Userunkid = mintUserunkid
                objDBStatus._xDataOp = objDataOperation
                objDBStatus._FormName = mstrWebFormName
                objDBStatus._HostName = mstrWebHostName
                objDBStatus._ClientIP = mstrWebClientIP
                objDBStatus._AuditUserId = mintUserunkid
                objDBStatus._Loginemployeeunkid = mintLoginemployeeunkid
                objDBStatus._AuditDate = mdtAuditDate
                objDBStatus._Isweb = mblnIsweb
                objDBStatus._CompanyUnkid = mintCompanyID

                If objDBStatus.IsTableDataChanged(mintDpndtbeneficetranunkid, objDataOperation) = True Then 'Sohail (06 Sep 2021)
                If objDBStatus.Insert() = False Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If
                End If 'Sohail (06 Sep 2021)
            End If
            'Sohail (18 May 2019) -- End

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            If dtDependantAttachment IsNot Nothing Then
                Dim objDocument As New clsScan_Attach_Documents
                Dim dtTran As DataTable = objDocument._Datatable
                Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\"
                Dim dsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")

                Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.DEPENDANTS).Tables(0).Rows(0)("Name").ToString
                If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"

                Dim dr As DataRow
                For Each drow As DataRow In dtDependantAttachment.Rows
                    dr = dtTran.NewRow
                    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                    dr("documentunkid") = drow("documentunkid")
                    dr("employeeunkid") = drow("employeeunkid")
                    dr("filename") = drow("filename")
                    dr("scanattachrefid") = drow("scanattachrefid")
                    dr("modulerefid") = drow("modulerefid")
                    dr("transactionunkid") = drow("transactionunkid")
                    dr("userunkid") = User._Object._Userunkid
                    dr("attached_date") = drow("attached_date")
                    dr("orgfilepath") = drow("localpath")
                    dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                    dr("AUD") = drow("AUD")
                    'SHANI (03 JUL 2015) -- Start
                    dr("userunkid") = mintUserunkid
                    'SHANI (03 JUL 2015) -- End 

                    'SHANI (16 JUL 2015) -- Start
                    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                    'Upload Image folder to access those attachemts from Server and 
                    'all client machines if ArutiSelfService is installed otherwise save then on Document path
                    dr("fileuniquename") = drow("fileuniquename")
                    dr("filepath") = drow("filepath")
                    dr("filesize") = drow("filesize_kb")
                    'SHANI (16 JUL 2015) -- End 

                    dtTran.Rows.Add(dr)
                Next
                objDocument._Datatable = dtTran
                objDocument.InsertUpdateDelete_Documents(objDataOperation)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If
            'SHANI (20 JUN 2015) -- End 

            'Sandeep [ 21 Aug 2010 ] -- Start

            Dim blnToInsert As Boolean = True

            If dtMemTran IsNot Nothing Then

                'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog","",mintDpndtbeneficetranunkid,"",2)
                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                Dim dt() As DataRow = dtMemTran.Select("AUD=''")
                If dt.Length = dtMemTran.Rows.Count Then
                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrdependants_beneficiaries_tran", mintDpndtbeneficetranunkid, "dpndtbeneficetranunkid", 2) Then
                    '    blnToInsert = True
                    'End If
                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrdependants_beneficiaries_tran", mintDpndtbeneficetranunkid, "dpndtbeneficetranunkid", 2, objDataOperation) Then
                        blnToInsert = True
                    End If
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                Else
                    objDependantMemInfoTran._DependantTranUnkid = mintDpndtbeneficetranunkid
                    objDependantMemInfoTran._DataTable = dtMemTran
                    Dim blnFlag As Boolean = False


                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'blnFlag = objDependantMemInfoTran.InsertUpdateDelete_DependantMembership_Tran()

                    'Shani(04-MAR-2017) -- Start
                    'Enhancement - Add Membership and Benefit in employee dependant's screen on web
                    'blnFlag = objDependantMemInfoTran.InsertUpdateDelete_DependantMembership_Tran(mintUserunkid)
                    blnFlag = objDependantMemInfoTran.InsertUpdateDelete_DependantMembership_Tran(objDataOperation, mintUserunkid)
                    'Shani(04-MAR-2017) -- End

                    'S.SANDEEP [ 27 APRIL 2012 ] -- END


                    If blnFlag = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    blnToInsert = False
                End If
                'S.SANDEEP [ 12 OCT 2011 ] -- END 
            End If

            If dtBenfTran IsNot Nothing Then

                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                Dim dt() As DataRow = dtBenfTran.Select("AUD=''")
                If dt.Length = dtBenfTran.Rows.Count Then

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrdependants_beneficiaries_tran", mintDpndtbeneficetranunkid, "dpndtbeneficetranunkid", 2) Then
                    '    blnToInsert = True
                    'End If

                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrdependants_beneficiaries_tran", mintDpndtbeneficetranunkid, "dpndtbeneficetranunkid", 2, objDataOperation) Then
                        blnToInsert = True
                    End If
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                Else
                    objDependantBenefitTran._DependantTranUnkid = mintDpndtbeneficetranunkid
                    objDependantBenefitTran._DataTable = dtBenfTran
                    Dim blnFlag As Boolean = False

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'blnFlag = objDependantBenefitTran.InsertUpdateDelete_DependantBenefit_Tran()

                    'Shani(04-MAR-2017) -- Start
                    'Enhancement - Add Membership and Benefit in employee dependant's screen on web
                    'blnFlag = objDependantBenefitTran.InsertUpdateDelete_DependantBenefit_Tran(mintUserunkid)
                    blnFlag = objDependantBenefitTran.InsertUpdateDelete_DependantBenefit_Tran(objDataOperation, mintUserunkid)
                    'Shani(04-MAR-2017) -- End

                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    If blnFlag = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    blnToInsert = False
                End If
                'S.SANDEEP [ 12 OCT 2011 ] -- END 
            End If
            'Sandeep [ 21 Aug 2010 ] -- End 

            If blnToInsert = True Then

                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", mintDpndtbeneficetranunkid, "hrdependant_membership_tran", "dpndtmembershiptranunkid", -1, 2, 0) = False Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If

                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", mintDpndtbeneficetranunkid, "hrdependant_membership_tran", "dpndtmembershiptranunkid", -1, 2, 0, , mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'S.SANDEEP [ 27 APRIL 2012 ] -- END

            End If


            If Isfromapproval = False Then

            If mblnImgInDb = False Then

                If mstrImagePath.Trim <> "" Then
                    objImages._Referenceid = enImg_Email_RefId.Dependants_Beneficiaries
                    objImages._Employeeunkid = mintEmployeeunkid
                    objImages._Imagename = mstrImagePath
                    objImages._Isapplicant = False
                    objImages._Transactionid = mintDpndtbeneficetranunkid
                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objImages.Insert()
                    If objImages.Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END
                Else
                    objImages._Referenceid = enImg_Email_RefId.Dependants_Beneficiaries
                    objImages._Employeeunkid = mintEmployeeunkid
                    objImages._Imagename = mstrImagePath
                    objImages._Isapplicant = False
                    objImages._Transactionid = mintDpndtbeneficetranunkid
                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objImages.Delete()
                    If objImages.Delete(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    'S.SANDEEP [04 JUN 2015] -- END
                End If
                objImages = Nothing

            Else

                If SaveEmpDependantImage(objDataOperation, mintCompanyID, mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If
            End If



            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If

            'Gajanan [22-Feb-2019] -- End

            Return True

        Catch ex As Exception
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation.ReleaseTransaction(False)
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan [22-Feb-2019] -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [22-Feb-2019] -- End
        End Try
    End Function

    '' <summary>
    '' Modify By: Sandeep J. Sharma
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> Delete Database Table (hrdependants_beneficiaries_tran) </purpose>
    '' 

    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    'Public Function Delete(ByVal intUnkid As Integer) As Boolean

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    'Public Function Delete(ByVal intUnkid As Integer, ByVal intEmpUnkID As Integer) As Boolean
    Public Function Delete(ByVal intUnkid As Integer, ByVal intEmpUnkID As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'Gajanan [22-Feb-2019] -- End

        'SHANI (20 JUN 2015) -- End

        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'S.SANDEEP [ 27 APRIL 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Dim blnIsChildPresent As Boolean = False
        'S.SANDEEP [ 27 APRIL 2012 ] -- END


        'Gajanan [22-Feb-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.

        'objDataOperation = New clsDataOperation
        ''S.SANDEEP [ 12 OCT 2011 ] -- START
        ''ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        'objDataOperation.BindTransaction()
        ''S.SANDEEP [ 12 OCT 2011 ] -- END 

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        'Gajanan [22-Feb-2019] -- End




        Try
            'Sandeep [ 14 Aug 2010 ] -- Start
            strQ = "UPDATE hrdependants_beneficiaries_tran SET " & _
                      " isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                    "WHERE dpndtbeneficetranunkid = @dpndtbeneficetranunkid "

            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            'Sandeep [ 14 Aug 2010 ] -- End 


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            If mstrDeletedpndtbeneficestatustranIDs.Trim.Length > 0 Then
                Dim objDPStatus As New clsDependant_Benefice_Status_tran

                For Each strID As String In mstrDeletedpndtbeneficestatustranIDs.Split(CChar(","))

                    objDPStatus._xDataOp = objDataOperation
                    objDPStatus._Dpndtbeneficestatustranunkid = CInt(strID)
                    objDPStatus._Isvoid = mblnIsvoid
                    objDPStatus._Voiddatetime = mdtVoiddatetime
                    objDPStatus._Voiduserunkid = mintUserunkid
                    objDPStatus._Voidreason = mstrVoidreason
                    objDPStatus._ClientIP = mstrWebClientIP
                    objDPStatus._FormName = mstrWebFormName
                    objDPStatus._HostName = mstrWebHostName
                    objDPStatus._Loginemployeeunkid = mintLoginemployeeunkid
                    objDPStatus._AuditDate = mdtAuditDate
                    objDPStatus._Isweb = mblnIsweb
                    objDPStatus._CompanyUnkid = mintCompanyID
                    objDPStatus._xDataOp = objDataOperation

                    If objDPStatus.Delete() = False Then
                        If objDPStatus._Message <> "" Then
                            exForce = New Exception(objDPStatus._Message)
                            Throw exForce
                        End If
                    End If
                Next
            End If
            'Sohail (18 May 2019) -- End

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen

            Dim objDocument As New clsScan_Attach_Documents
            If objDocument.DeleteTransacation(intEmpUnkID, enScanAttactRefId.DEPENDANTS, intUnkid, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'SHANI (20 JUN 2015) -- End 

            'Pinkal (01-Apr-2013) -- Start
            'Enhancement : TRA Changes

            If mblnImgInDb Then
                Dim objDependantImg As New clsdependant_Images
                objDependantImg._WebForm_Name = mstrWebFormName
                objDependantImg._Ip = mstrWebClientIP
                objDependantImg._Machine_Name = mstrWebHostName
                objDependantImg.GetData(mintCompanyID, mintEmployeeunkid, intUnkid, True)
                If objDependantImg.Delete(objDataOperation, objDependantImg._Dependantimgunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'Pinkal (01-Apr-2013) -- End


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            Dim dTran As DataSet = clsCommonATLog.GetChildList(objDataOperation, "hrdependant_benefit_tran", "dpndtbeneficetranunkid", intUnkid)

            If dTran.Tables(0).Rows.Count > 0 Then

                objDependantBenefitTran._DependantTranUnkid = intUnkid

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'If objDependantBenefitTran.Void_All(mblnIsvoid, mdtVoiddatetime, mstrVoidreason, mintVoiduserunkid) = False Then
                If objDependantBenefitTran.Void_All(mblnIsvoid, mdtVoiddatetime, mstrVoidreason, mintVoiduserunkid, objDataOperation) = False Then
                    'Gajanan [22-Feb-2019] -- End
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                blnIsChildPresent = True
                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                For Each dRow As DataRow In dTran.Tables(0).Rows
                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", intUnkid, "hrdependant_benefit_tran", "dpndtbenefittranunkid", dRow.Item("dpndtbenefittranunkid"), 3, 3) = False Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", intUnkid, "hrdependant_benefit_tran", "dpndtbenefittranunkid", dRow.Item("dpndtbenefittranunkid"), 3, 3, , mintVoiduserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                Next
            End If

            dTran.Dispose()
            dTran = clsCommonATLog.GetChildList(objDataOperation, "hrdependant_membership_tran", "dpndtbeneficetranunkid", intUnkid)
            If dTran.Tables(0).Rows.Count > 0 Then

                objDependantMemInfoTran._DependantTranUnkid = intUnkid
                If objDependantMemInfoTran.Void_All(mblnIsvoid, mdtVoiddatetime, mstrVoidreason, mintVoiduserunkid, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                blnIsChildPresent = True
                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                For Each dRow As DataRow In dTran.Tables(0).Rows
                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", intUnkid, "hrdependant_membership_tran", "dpndtmembershiptranunkid", dRow.Item("dpndtmembershiptranunkid"), 3, 3) = False Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", intUnkid, "hrdependant_membership_tran", "dpndtmembershiptranunkid", dRow.Item("dpndtmembershiptranunkid"), 3, 3, , mintVoiduserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                Next
            End If

            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If blnIsChildPresent = False Then
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", intUnkid, "hrdependant_membership_tran", "dpndtmembershiptranunkid", -1, 3, 0, , mintVoiduserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Sohail (18 May 2019) -- End
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [22-Feb-2019] -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sandeep [ 14 Aug 2010 ] -- Start
    '''' <summary>
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '          "  dpndtbeneficetranunkid " & _
    '          ", employeeunkid " & _
    '          ", first_name " & _
    '          ", last_name " & _
    '          ", middle_name " & _
    '          ", relationunkid " & _
    '          ", birthdate " & _
    '          ", address " & _
    '          ", telephone_no " & _
    '          ", mobile_no " & _
    '          ", post_box " & _
    '          ", countryunkid " & _
    '          ", stateunkid " & _
    '          ", cityunkid " & _
    '          ", zipcodeunkid " & _
    '          ", email " & _
    '          ", nationalityunkid " & _
    '          ", identify_no " & _
    '          ", membershipunkid " & _
    '          ", membership_no " & _
    '          ", benefitgroupunkid " & _
    '          ", benefitplanunkid " & _
    '          ", benefit_percent " & _
    '          ", benefit_amount " & _
    '          ", isdependant " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '         "FROM hrdependants_beneficiaries_tran " & _
    '         "WHERE name = @name " & _
    '         "AND code = @code "

    '        If intUnkid > 0 Then
    '            strQ &= " AND dpndtbeneficetranunkid <> @dpndtbeneficetranunkid"
    '        End If

    '        objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, _
                            ByVal strLastName As String, _
                            ByVal strMiddleName As String, _
                            ByVal strIdNo As String, _
                            ByVal intEmployeeId As Integer, _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Gajanan [22-Feb-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.

        'objDataOperation = New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        'Gajanan [22-Feb-2019] -- End

        Try
            strQ = "SELECT " & _
              "  dpndtbeneficetranunkid " & _
              ", employeeunkid " & _
              ", first_name " & _
              ", last_name " & _
              ", middle_name " & _
              ", relationunkid " & _
              ", birthdate " & _
              ", address " & _
              ", telephone_no " & _
              ", mobile_no " & _
              ", post_box " & _
              ", countryunkid " & _
              ", stateunkid " & _
              ", cityunkid " & _
              ", zipcodeunkid " & _
              ", email " & _
              ", nationalityunkid " & _
              ", identify_no " & _
              ", isdependant " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrdependants_beneficiaries_tran " & _
             "WHERE first_name = @first_name " & _
             "AND last_name = @last_name " & _
             "AND employeeunkid = @EmpId "

            'Shani (19-Oct-2016) -- Start
            'Enhancement - Add Gender In Depandent Importation Screen
            strQ &= " AND isvoid = 0 "
            'Shani (19-Oct-2016) -- End



            If strMiddleName.Trim.Length > 0 Then
                strQ &= "AND middle_name = @middle_name "
            End If

            If strIdNo.Trim.Length > 0 Then
                strQ &= "AND identify_no = @identify_no "
            End If

            If intUnkid > 0 Then
                strQ &= " AND dpndtbeneficetranunkid <> @dpndtbeneficetranunkid"
            End If

            objDataOperation.AddParameter("@first_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@last_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strLastName)

            If strMiddleName.Trim.Length > 0 Then
                objDataOperation.AddParameter("@middle_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strMiddleName)
            End If

            If strIdNo.Trim.Length > 0 Then
                objDataOperation.AddParameter("@identify_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strIdNo)
            End If

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)

            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [22-Feb-2019] -- End
        End Try
    End Function
    'Sandeep [ 14 Aug 2010 ] -- End 




    'Sandeep [ 17 DEC 2010 ] -- Start
    'Issue : Checking the Total Allocated (%) To Beneficiaries Of Particular Employee
    Public Sub GetAllocate_Percent(ByVal intEmpId As Integer, ByRef intTotBeneficiay As Integer, ByRef dblTotPercent As Double)
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                    " COUNT(TotBenef) AS TotBenef " & _
                    ",ISNULL(SUM(TotPercent),0) AS TotPercent " & _
                    "FROM " & _
                    "( " & _
                        "SELECT " & _
                            " hrdependants_beneficiaries_tran.dpndtbeneficetranunkid AS TotBenef " & _
                            ",ISNULL(SUM(benefit_percent),0) AS TotPercent " & _
                        "FROM hrdependants_beneficiaries_tran " & _
                            "JOIN hrdependant_benefit_tran ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = hrdependant_benefit_tran.dpndtbeneficetranunkid " & _
                        "WHERE hrdependants_beneficiaries_tran.employeeunkid = @EmpId " & _
                            "AND ISNULL(hrdependants_beneficiaries_tran.isvoid,0)=0 " & _
                            "AND ISNULL(hrdependant_benefit_tran.isvoid,0)=0 " & _
                        "GROUP BY hrdependants_beneficiaries_tran.dpndtbeneficetranunkid " & _
                    ") AS TOT "

            objDataOperation.AddParameter("EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intTotBeneficiay = dsList.Tables("List").Rows(0)("TotBenef")
                dblTotPercent = dsList.Tables("List").Rows(0)("TotPercent")
            Else
                intTotBeneficiay = 0
                dblTotPercent = 0
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAllocate_Percent", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 17 DEC 2010 ] -- End 


    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function GetDependantsData_Export(Optional ByVal dtAsOnDate As Date = Nothing) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            StrQ = "SELECT * " & _
                        "INTO #TableDepn " & _
                        "FROM " & _
                        "( " & _
                            "SELECT * " & _
                                 ", DENSE_RANK() OVER (PARTITION BY dpndtbeneficetranunkid ORDER BY effective_date DESC, dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                            "FROM hrdependant_beneficiaries_status_tran " & _
                            "WHERE isvoid = 0 "

            If dtAsOnDate <> Nothing Then
                StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
                objDataOperation.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            StrQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End

            StrQ &= "SELECT " & _
                         " ISNULL(hremployee_master.employeecode,'') AS EMPLOYEE_CODE " & _
                         ",ISNULL(hremployee_master.firstname,'') AS EMPLOYEE_FIRSTNAME " & _
                         ",ISNULL(hremployee_master.surname,'') AS EMPLOYEE_SURNAME " & _
                         ",ISNULL(hrdependants_beneficiaries_tran.first_name,'') AS DEPENDANT_FIRSTNAME " & _
                         ",ISNULL(hrdependants_beneficiaries_tran.middle_name,'') AS DEPENDANT_MIDDLENAME " & _
                         ",ISNULL(hrdependants_beneficiaries_tran.last_name,'') AS DEPENDANT_LASTNAME " & _
                         ",ISNULL(cfcommon_master.name,'') AS RELATION " & _
                         ",hrdependants_beneficiaries_tran.birthdate AS BIRTHDATE " & _
                         ",hrdependants_beneficiaries_tran.address AS ADDRESS " & _
                         ",hrdependants_beneficiaries_tran.telephone_no AS TELEPHONE " & _
                         ",hrdependants_beneficiaries_tran.mobile_no AS MOBILENO " & _
                         ",ISNULL(hrdependants_beneficiaries_tran.identify_no,'') AS ID_NO " & _
                         ",CASE WHEN isdependant = 1 THEN 'TRUE' ELSE 'FALSE' END AS ISBENEFICIARY " & _
                         ",CASE WHEN hrdependants_beneficiaries_tran.gender = 1 THEN @Male " & _
                         "       WHEN hrdependants_beneficiaries_tran.gender = 2 THEN @Female " & _
                         " ELSE '' END AS GENDER " & _
                         ",CAST(ISNULL(#TableDepn.isactive, 0) AS BIT) AS isactive " & _
                    "FROM hrdependants_beneficiaries_tran " & _
                         "LEFT JOIN #TableDepn ON #TableDepn.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid " & _
                         "LEFT JOIN hrmsConfiguration..cfcountry_master AS NATION ON NATION.countryunkid = hrdependants_beneficiaries_tran.countryunkid " & _
                         "LEFT JOIN hrmsConfiguration..cfzipcode_master ON hrmsConfiguration..cfzipcode_master.zipcodeunkid = hrdependants_beneficiaries_tran.zipcodeunkid " & _
                         "LEFT JOIN hrmsConfiguration..cfcity_master ON hrmsConfiguration..cfcity_master.cityunkid = hrdependants_beneficiaries_tran.cityunkid " & _
                         "LEFT JOIN hrmsConfiguration..cfstate_master ON hrmsConfiguration..cfstate_master.stateunkid = hrdependants_beneficiaries_tran.stateunkid " & _
                         "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hrdependants_beneficiaries_tran.countryunkid " & _
                         "JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
                         "JOIN hremployee_master ON hrdependants_beneficiaries_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "WHERE hrdependants_beneficiaries_tran.isvoid = 0 "
            'Sohail (18 May 2019) - [isactive]

            'Pinkal (20-Jul-2018) -- 'Enhancement - Adding Dependant Middle Name Column as per NMB Requirement.[ ",ISNULL(hrdependants_beneficiaries_tran.middle_name,'') AS DEPENDANT_MIDDLENAME " & _]


            'S.SANDEEP [14-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {NMB}
            '",ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS COUNTRY " & _
            '            ",ISNULL(hrmsConfiguration..cfstate_master.name,'') AS STATE " & _
            '            ",ISNULL(hrmsConfiguration..cfcity_master.name,'') AS CITY " & _
            '            ",ISNULL(hrmsConfiguration..cfzipcode_master.zipcode_no,'') AS POSTCODE " & _
            '            ",ISNULL(NATION.country_name,'') AS NATIONALITY " & _

            objDataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
            objDataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))
            'S.SANDEEP [14-JUN-2018] -- END
            
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            StrQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : GetDependantsData_Export ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetDependantUnkid(ByVal intEmpId As Integer, ByVal strFirstname As String, ByVal strLastname As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ &= "SELECT " & _
                    " dpndtbeneficetranunkid " & _
                    "FROM hrdependants_beneficiaries_tran " & _
                    "WHERE employeeunkid = @EmpId AND LTRIM(RTRIM(first_name)) LIKE LTRIM(RTRIM(@FName)) AND LTRIM(RTRIM(last_name))LIKE LTRIM(RTRIM(@LName)) AND isvoid = 0 "

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@FName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strFirstname)
            objDataOperation.AddParameter("@LName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strLastname)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("dpndtbeneficetranunkid")
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & " " & "GetDependantUnkid" & " " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 07 NOV 2011 ] -- END


    Public Function GetQualifiedDepedant(ByVal intEmployeeunkid As Integer, ByVal blnIsMedical As Boolean, ByVal blnLeave As Boolean, ByVal mdtDate As Date, Optional ByVal blnFlag As Boolean = False _
                                                          , Optional ByVal blnMedicalBenefitRequire As Boolean = True, Optional ByVal blnIncludeMedicalException As Boolean = True, Optional ByVal intRelationID As Integer = -1 _
                                                          , Optional ByVal intAgeFrom As Integer = 0, Optional ByVal intAgeTo As Integer = 0, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal dtAsOnDate As Date = Nothing) As DataSet
        'Sohail (18 May 2019) - [dtAsOnDate]

        'Sohail (03 May 2018) - [xDataOp]
        'Sohail (22 Aug 2017) - [intAgeFrom, intAgeTo]
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception

        Try
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim objDataOperation As New clsDataOperation
            Dim objDataOperation As clsDataOperation
            If xDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOp
            End If
            objDataOperation.ClearParameters()
            'Sohail (03 May 2018) -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            strQ = "SELECT * " & _
                        "INTO #TableDepn " & _
                        "FROM " & _
                        "( " & _
                            "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                 ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                            "FROM hrdependant_beneficiaries_status_tran "

            If intEmployeeunkid > 0 Then
                strQ &= "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid "
            End If

            strQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If intEmployeeunkid > 0 Then
                strQ &= "AND hrdependants_beneficiaries_tran.employeeunkid = @employeeunkid "
            End If

            If dtAsOnDate <> Nothing Then
                strQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            strQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End

            If blnFlag Then
                strQ &= " SELECT 0 as dependent_Id, @select as dependants,0 as age,0 as Months, '' as relation, '' as gender UNION "
            End If

            'S.SANDEEP [02 JAN 2017] -- START
            'ENHANCEMENT : AGE CALUCATION ISSUE IN QUERY
            'strQ &= "SELECT " & _
            '          " hrdependants_beneficiaries_tran.dpndtbeneficetranunkid as dependent_Id " & _
            '          ", ISNULL(hrdependants_beneficiaries_tran.first_name,'') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name,'') + ' '  +  ISNULL(hrdependants_beneficiaries_tran.last_name,'') dependants " & _
            '          ",YEAR(GETDATE()) - YEAR(birthdate) age " & _
            '          ",(DATEDIFF(MONTH,birthdate,GETDATE())+12)%12 - CASE WHEN DAY(birthdate)>DAY(GETDATE()) THEN 1 ELSE 0 END AS Months " & _
            '          ",cfcommon_master.name AS relation " & _
            '          ", CASE WHEN gender = 1 then @male  " & _
            '          "           WHEN gender = 2 then @female " & _
            '          "           WHEN gender = 3 then @other " & _
            '          " ELSE  ''  " & _
            '          " END as gender " & _
            '          " FROM hrdependants_beneficiaries_tran " & _
            '          "JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & _
            '          " WHERE employeeunkid = @employeeunkid AND  ((YEAR(GETDATE())-YEAR(birthdate)) <= ISNULL(cfcommon_master.dependant_limit,0)  or ISNULL(cfcommon_master.dependant_limit,0) = 0) " & _
            '          " AND hrdependants_beneficiaries_tran.isvoid = 0 "

            strQ &= "SELECT " & _
                      " hrdependants_beneficiaries_tran.dpndtbeneficetranunkid as dependent_Id " & _
                      ", ISNULL(hrdependants_beneficiaries_tran.first_name,'') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name,'') + ' '  +  ISNULL(hrdependants_beneficiaries_tran.last_name,'') dependants " & _
                      ",((CAST(CONVERT(CHAR(8),GETDATE(),112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000) age " & _
                      ",(DATEDIFF(MONTH,birthdate,GETDATE())+12)%12 - CASE WHEN DAY(birthdate)>DAY(GETDATE()) THEN 1 ELSE 0 END AS Months " & _
                      ",cfcommon_master.name AS relation " & _
                      ", CASE WHEN gender = 1 then @male  " & _
                      "           WHEN gender = 2 then @female " & _
                      "           WHEN gender = 3 then @other " & _
                      " ELSE  ''  " & _
                      " END as gender " & _
                      " FROM hrdependants_beneficiaries_tran " & _
                      " JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                      " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & _
                      " WHERE employeeunkid = @employeeunkid AND  (((CAST(CONVERT(CHAR(8),GETDATE(),112) AS INT)-CAST(CONVERT(CHAR(8),birthdate,112) AS INT))/10000) <= ISNULL(cfcommon_master.dependant_limit,0)  or ISNULL(cfcommon_master.dependant_limit,0) = 0) " & _
                      " AND hrdependants_beneficiaries_tran.isvoid = 0 " & _
                      " AND #TableDepn.isactive = 1 "
            'S.SANDEEP |09-APR-2019| -- START {AND hrdependants_beneficiaries_tran.isvoid = 0} -- END
            'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]
            'S.SANDEEP [02 JAN 2017] -- END
            
            If intRelationID > 0 Then
                strQ &= " AND cfcommon_master.masterunkid = " & intRelationID & " "
            End If

            If blnMedicalBenefitRequire Then
                strQ &= "AND cfcommon_master.ismedicalbenefit = 1 "
            End If

            'Sohail (22 Aug 2017) -- Start
            'Enhancement - 69.1 - New functions to qualify Age of dependents in payroll 
            If intAgeFrom <> 0 Then
                strQ &= "AND ((CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT) - CAST(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate, 112) AS INT)) / 10000) >= @intAgeFrom "
            End If

            If intAgeTo <> 0 Then
                strQ &= "AND ((CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT) - CAST(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate, 112) AS INT)) / 10000) <= @intAgeTo "
            End If
            'Sohail (22 Aug 2017) -- End

            If blnIncludeMedicalException Then

                If blnIsMedical Then

                    'S.SANDEEP [02 JAN 2017] -- START
                    'ENHANCEMENT : AGE CALUCATION ISSUE IN QUERY
                    'strQ &= " UNION " & _
                    '            " SELECT " & _
                    '            " mddependant_exception.dependantunkid as dependent_Id" & _
                    '            ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS dependants " & _
                    '            ", YEAR(GETDATE()) - YEAR(hrdependants_beneficiaries_tran.birthdate) AS age " & _
                    '              ",(DATEDIFF(MONTH,hrdependants_beneficiaries_tran.birthdate,GETDATE())+12)%12 - CASE WHEN DAY(hrdependants_beneficiaries_tran.birthdate)>DAY(GETDATE()) THEN 1 ELSE 0 END AS Months " & _
                    '            ", ISNULL(cfcommon_master.name, '') AS relation " & _
                    '            ", CASE WHEN hrdependants_beneficiaries_tran.gender = 1 then @male  " & _
                    '            "           WHEN hrdependants_beneficiaries_tran.gender = 2 then @female " & _
                    '            "           WHEN hrdependants_beneficiaries_tran.gender = 3 then @other " & _
                    '            " ELSE  ''  " & _
                    '            " END as gender " & _
                    '            " FROM  mddependant_exception " & _
                    '            " JOIN hremployee_master ON mddependant_exception.employeeunkid = hremployee_master.employeeunkid " & _
                    '            " JOIN hrdependants_beneficiaries_tran ON mddependant_exception.employeeunkid = hrdependants_beneficiaries_tran.employeeunkid " & _
                    '            " AND mddependant_exception.dependantunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid " & _
                    '            " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
                    '            " AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " AND cfcommon_master.isactive = 1  " & _
                    '            " WHERE mddependant_exception.isvoid = 0 AND mddependant_exception.employeeunkid = @employeeunkid " & _
                    '            " AND mddependant_exception.ismedical = 1 AND (mddependant_exception.mdstopdate IS NULL OR  Convert(Char(8),mddependant_exception.mdstopdate,112) >= '" & eZeeDate.convertDate(mdtDate.Date) & "')"

                    strQ &= " UNION " & _
                                " SELECT " & _
                                " mddependant_exception.dependantunkid as dependent_Id" & _
                                ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS dependants " & _
                                ",((CAST(CONVERT(CHAR(8),GETDATE(),112) AS INT)-CAST(CONVERT(CHAR(8),hrdependants_beneficiaries_tran.birthdate,112) AS INT))/10000) AS age " & _
                                  ",(DATEDIFF(MONTH,hrdependants_beneficiaries_tran.birthdate,GETDATE())+12)%12 - CASE WHEN DAY(hrdependants_beneficiaries_tran.birthdate)>DAY(GETDATE()) THEN 1 ELSE 0 END AS Months " & _
                                ", ISNULL(cfcommon_master.name, '') AS relation " & _
                                ", CASE WHEN hrdependants_beneficiaries_tran.gender = 1 then @male  " & _
                                "           WHEN hrdependants_beneficiaries_tran.gender = 2 then @female " & _
                                "           WHEN hrdependants_beneficiaries_tran.gender = 3 then @other " & _
                                " ELSE  ''  " & _
                                " END as gender " & _
                                " FROM  mddependant_exception " & _
                                " JOIN hremployee_master ON mddependant_exception.employeeunkid = hremployee_master.employeeunkid " & _
                                " JOIN hrdependants_beneficiaries_tran ON mddependant_exception.employeeunkid = hrdependants_beneficiaries_tran.employeeunkid " & _
                                " JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                                " AND mddependant_exception.dependantunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid " & _
                                " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
                                " AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " AND cfcommon_master.isactive = 1  " & _
                                " WHERE mddependant_exception.isvoid = 0 AND mddependant_exception.employeeunkid = @employeeunkid " & _
                                " AND mddependant_exception.ismedical = 1 AND (mddependant_exception.mdstopdate IS NULL OR  Convert(Char(8),mddependant_exception.mdstopdate,112) >= '" & eZeeDate.convertDate(mdtDate.Date) & "') " & _
                                " AND hrdependants_beneficiaries_tran.isvoid = 0 " & _
                                " AND #TableDepn.isactive = 1 "
                    'S.SANDEEP |09-APR-2019| -- START {AND hrdependants_beneficiaries_tran.isvoid = 0} -- END
                    'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]
                    'S.SANDEEP [02 JAN 2017] -- END

                    If intRelationID > 0 Then
                        strQ &= " AND cfcommon_master.masterunkid = " & intRelationID & " "
                    End If


                    'Sohail (22 Aug 2017) -- Start
                    'Enhancement - 69.1 - New functions to qualify Age of dependents in payroll 
                    If intAgeFrom <> 0 Then
                        strQ &= "AND ((CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT) - CAST(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate, 112) AS INT)) / 10000) >= @intAgeFrom "
                    End If

                    If intAgeTo <> 0 Then
                        strQ &= "AND ((CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT) - CAST(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate, 112) AS INT)) / 10000) <= @intAgeTo "
                    End If
                    'Sohail (22 Aug 2017) -- End

                End If

                If blnLeave Then

                    'S.SANDEEP [02 JAN 2017] -- START
                    'ENHANCEMENT : AGE CALUCATION ISSUE IN QUERY
                    'strQ &= " UNION " & _
                    '            " SELECT " & _
                    '            " mddependant_exception.dependantunkid as dependent_Id" & _
                    '            ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS dependants " & _
                    '            ", YEAR(GETDATE()) - YEAR(hrdependants_beneficiaries_tran.birthdate) AS age " & _
                    '              ",(DATEDIFF(MONTH,hrdependants_beneficiaries_tran.birthdate,GETDATE())+12)%12 - CASE WHEN DAY(hrdependants_beneficiaries_tran.birthdate)>DAY(GETDATE()) THEN 1 ELSE 0 END AS Months " & _
                    '            ", ISNULL(cfcommon_master.name, '') AS relation " & _
                    '            ", CASE WHEN hrdependants_beneficiaries_tran.gender = 1 then @male  " & _
                    '            "           WHEN hrdependants_beneficiaries_tran.gender = 2 then @female " & _
                    '            "           WHEN hrdependants_beneficiaries_tran.gender = 3 then @other " & _
                    '            " ELSE  ''  " & _
                    '            " END as gender " & _
                    '            " FROM  mddependant_exception " & _
                    '            " JOIN hremployee_master ON mddependant_exception.employeeunkid = hremployee_master.employeeunkid " & _
                    '            " JOIN hrdependants_beneficiaries_tran ON mddependant_exception.employeeunkid = hrdependants_beneficiaries_tran.employeeunkid " & _
                    '            " AND mddependant_exception.dependantunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid " & _
                    '            " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
                    '            " AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " AND cfcommon_master.isactive = 1  " & _
                    '                " WHERE mddependant_exception.isvoid = 0 AND mddependant_exception.employeeunkid = @employeeunkid " & _
                    '                " AND mddependant_exception.isleave = 1 AND (mddependant_exception.lvstopdate IS NULL OR  Convert(Char(8),mddependant_exception.lvstopdate,112) >= '" & eZeeDate.convertDate(mdtDate.Date) & "')"

                    strQ &= " UNION " & _
                                " SELECT " & _
                                " mddependant_exception.dependantunkid as dependent_Id" & _
                                ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS dependants " & _
                            ",((CAST(CONVERT(CHAR(8),GETDATE(),112) AS INT)-CAST(CONVERT(CHAR(8),hrdependants_beneficiaries_tran.birthdate,112) AS INT))/10000) AS age " & _
                                  ",(DATEDIFF(MONTH,hrdependants_beneficiaries_tran.birthdate,GETDATE())+12)%12 - CASE WHEN DAY(hrdependants_beneficiaries_tran.birthdate)>DAY(GETDATE()) THEN 1 ELSE 0 END AS Months " & _
                                ", ISNULL(cfcommon_master.name, '') AS relation " & _
                                ", CASE WHEN hrdependants_beneficiaries_tran.gender = 1 then @male  " & _
                                "           WHEN hrdependants_beneficiaries_tran.gender = 2 then @female " & _
                                "           WHEN hrdependants_beneficiaries_tran.gender = 3 then @other " & _
                                " ELSE  ''  " & _
                                " END as gender " & _
                                " FROM  mddependant_exception " & _
                                " JOIN hremployee_master ON mddependant_exception.employeeunkid = hremployee_master.employeeunkid " & _
                                " JOIN hrdependants_beneficiaries_tran ON mddependant_exception.employeeunkid = hrdependants_beneficiaries_tran.employeeunkid " & _
                                " JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                                " AND mddependant_exception.dependantunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid " & _
                                " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
                                " AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " AND cfcommon_master.isactive = 1  " & _
                                    " WHERE mddependant_exception.isvoid = 0 AND mddependant_exception.employeeunkid = @employeeunkid " & _
                                " AND mddependant_exception.isleave = 1 " & _
                                " AND hrdependants_beneficiaries_tran.isvoid = 0 " & _
                                " AND #TableDepn.isactive = 1 "
                    'S.SANDEEP |09-APR-2019| -- START {AND hrdependants_beneficiaries_tran.isvoid = 0} -- END
                    'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]
                    'S.SANDEEP [02 JAN 2017] -- END

                    If intRelationID > 0 Then
                        strQ &= " AND cfcommon_master.masterunkid = " & intRelationID & " "
                    End If

                    'Sohail (22 Aug 2017) -- Start
                    'Enhancement - 69.1 - New functions to qualify Age of dependents in payroll 
                    If intAgeFrom <> 0 Then
                        strQ &= "AND ((CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT) - CAST(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate, 112) AS INT)) / 10000) >= @intAgeFrom "
                    End If

                    If intAgeTo <> 0 Then
                        strQ &= "AND ((CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT) - CAST(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate, 112) AS INT)) / 10000) <= @intAgeTo "
                    End If
                    'Sohail (22 Aug 2017) -- End
                End If

            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 23, "Select"))
            objDataOperation.AddParameter("@male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 7, "Male"))
            objDataOperation.AddParameter("@female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 8, "Female"))
            objDataOperation.AddParameter("@other", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 9, "Other"))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            'Sohail (22 Aug 2017) -- Start
            'Enhancement - 69.1 - New functions to qualify Age of dependents in payroll 
            If intAgeFrom <> 0 Then
                objDataOperation.AddParameter("@intAgeFrom", SqlDbType.Int, eZeeDataType.INT_SIZE, intAgeFrom)
            End If

            If intAgeTo <> 0 Then
                objDataOperation.AddParameter("@intAgeTo", SqlDbType.Int, eZeeDataType.INT_SIZE, intAgeTo)
            End If
            'Sohail (22 Aug 2017) -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            If dtAsOnDate <> Nothing Then
                objDataOperation.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            End If

            strQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetQualifiedDepedant", mstrModuleName)
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dsList
    End Function

    'Pinkal (21-Jul-2014) -- End
    'Pinkal (3-Sep-2014) -- Start
    'Enhancement - FOR FDRC CREATED INACTIVE DEPENDENT METHOD FOR PAYROLL FORMULA

    Public Function GetInActiveDependents(ByVal intEmployeeunkid As Integer, ByVal blnIsMedical As Boolean, ByVal blnLeave As Boolean, ByVal mdtDate As Date, Optional ByVal blnFlag As Boolean = False, Optional ByVal blnMedicalBenefitRequire As Boolean = True, _
                                                           Optional ByVal blnIncludeMedicalException As Boolean = True, Optional ByVal intRelationID As Integer = -1, Optional ByVal intAgeFrom As Integer = 0, Optional ByVal intAgeTo As Integer = 0, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal dtAsOnDate As Date = Nothing) As DataSet
        'Sohail (18 May 2019) - [dtAsOnDate]
        'Sohail (03 May 2018) - [xDataOp]
        'Sohail (22 Aug 2017) - [intAgeFrom, intAgeTo]
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Try
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim objDataOperation As New clsDataOperation
            Dim objDataOperation As clsDataOperation
            If xDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOp
            End If
            objDataOperation.ClearParameters()
            'Sohail (03 May 2018) -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            strQ = "SELECT * " & _
                       "INTO #TableDepn " & _
                       "FROM " & _
                       "( " & _
                           "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                           "FROM hrdependant_beneficiaries_status_tran "

            If intEmployeeunkid > 0 Then
                strQ &= "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid "
            End If

            strQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If intEmployeeunkid > 0 Then
                strQ &= "AND hrdependants_beneficiaries_tran.employeeunkid = @employeeunkid "
            End If

            If dtAsOnDate <> Nothing Then
                strQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            strQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End

            If blnFlag Then
                strQ &= " SELECT 0 as dependent_Id, @select as dependants,0 as age,0 as Months, '' as relation, '' as gender UNION "
            End If

            strQ &= "SELECT " & _
                      " hrdependants_beneficiaries_tran.dpndtbeneficetranunkid as dependent_Id " & _
                      ", ISNULL(hrdependants_beneficiaries_tran.first_name,'') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name,'') + ' '  +  ISNULL(hrdependants_beneficiaries_tran.last_name,'') dependants " & _
                      ",YEAR(GETDATE()) - YEAR(birthdate) age " & _
                      ",(DATEDIFF(MONTH,birthdate,GETDATE())+12)%12 - CASE WHEN DAY(birthdate)>DAY(GETDATE()) THEN 1 ELSE 0 END AS Months " & _
                      ",cfcommon_master.name AS relation " & _
                      ", CASE WHEN gender = 1 then @male  " & _
                      "           WHEN gender = 2 then @female " & _
                      "           WHEN gender = 3 then @other " & _
                      " ELSE  ''  " & _
                      " END as gender " & _
                      " FROM hrdependants_beneficiaries_tran " & _
                      " JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                      "JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & _
                      " WHERE employeeunkid = @employeeunkid AND  (YEAR(GETDATE())-YEAR(birthdate)) > ISNULL(cfcommon_master.dependant_limit,0)   AND ISNULL(cfcommon_master.dependant_limit,0)  <>  0  " & _
                      " AND hrdependants_beneficiaries_tran.isvoid = 0 " & _
                      " AND #TableDepn.isactive = 0 "
            'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]

            'Pinkal (01-Oct-2014) -- Start
            'Enhancement -  Changes For FDRC Report

            If intRelationID > 0 Then
                strQ &= " AND cfcommon_master.masterunkid = " & intRelationID & " "
            End If

            'Pinkal (01-Oct-2014) -- End

            If blnMedicalBenefitRequire Then
                strQ &= "AND cfcommon_master.ismedicalbenefit = 1 "
            End If

            'Sohail (25 Jul 2019) -- Start
            'PACRA issue # 0003796 - 76.1 - Wrong calculations of age for the allowance head.
            Dim strAsOnDate As String = "@asondate"
            If dtAsOnDate <> Nothing Then
                strAsOnDate = "@dtAsOnDate"
            End If
            'Sohail (25 Jul 2019) -- End

            'Sohail (22 Aug 2017) -- Start
            'Enhancement - 69.1 - New functions to qualify Age of dependents in payroll 
            If intAgeFrom <> 0 Then
                'Sohail (25 Jul 2019) -- Start
                'PACRA issue # 0003796 - 76.1 - Wrong calculations of age for the allowance head.
                'strQ &= "AND ((CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT) - CAST(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate, 112) AS INT)) / 10000) >= @intAgeFrom "
                strQ &= "AND ((CAST(CONVERT(CHAR(8), " & strAsOnDate & ", 112) AS INT) - CAST(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate, 112) AS INT)) / 10000) >= @intAgeFrom "
                'Sohail (25 Jul 2019) -- End
            End If

            If intAgeTo <> 0 Then
                'Sohail (25 Jul 2019) -- Start
                'PACRA issue # 0003796 - 76.1 - Wrong calculations of age for the allowance head.
                'strQ &= "AND ((CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT) - CAST(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate, 112) AS INT)) / 10000) <= @intAgeTo "
                strQ &= "AND ((CAST(CONVERT(CHAR(8), " & strAsOnDate & ", 112) AS INT) - CAST(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate, 112) AS INT)) / 10000) <= @intAgeTo "
                'Sohail (25 Jul 2019) -- End
            End If
            'Sohail (22 Aug 2017) -- End

            If blnIncludeMedicalException Then

                If blnIsMedical Then

                    strQ &= " UNION " & _
                                " SELECT " & _
                                " mddependant_exception.dependantunkid as dependent_Id" & _
                                ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS dependants " & _
                                ", YEAR(GETDATE()) - YEAR(hrdependants_beneficiaries_tran.birthdate) AS age " & _
                                  ",(DATEDIFF(MONTH,hrdependants_beneficiaries_tran.birthdate,GETDATE())+12)%12 - CASE WHEN DAY(hrdependants_beneficiaries_tran.birthdate)>DAY(GETDATE()) THEN 1 ELSE 0 END AS Months " & _
                                ", ISNULL(cfcommon_master.name, '') AS relation " & _
                                ", CASE WHEN hrdependants_beneficiaries_tran.gender = 1 then @male  " & _
                                "           WHEN hrdependants_beneficiaries_tran.gender = 2 then @female " & _
                                "           WHEN hrdependants_beneficiaries_tran.gender = 3 then @other " & _
                                " ELSE  ''  " & _
                                " END as gender " & _
                                " FROM  mddependant_exception " & _
                                " JOIN hremployee_master ON mddependant_exception.employeeunkid = hremployee_master.employeeunkid " & _
                                " JOIN hrdependants_beneficiaries_tran ON mddependant_exception.employeeunkid = hrdependants_beneficiaries_tran.employeeunkid " & _
                                " JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                                " AND mddependant_exception.dependantunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid " & _
                                " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
                                " AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " AND cfcommon_master.isactive = 1  " & _
                                " WHERE mddependant_exception.isvoid = 0 AND mddependant_exception.employeeunkid = @employeeunkid " & _
                                " AND mddependant_exception.ismedical = 1 AND  Convert(Char(8),mddependant_exception.mdstopdate,112) < @asondate " & _
                                " AND hrdependants_beneficiaries_tran.isvoid = 0 " & _
                                " AND #TableDepn.isactive = 0 "
                    'S.SANDEEP |09-APR-2019| -- START {AND hrdependants_beneficiaries_tran.isvoid = 0} -- END
                    'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]

                    'Pinkal (01-Oct-2014) -- Start
                    'Enhancement -  Changes For FDRC Report

                    If intRelationID > 0 Then
                        strQ &= " AND cfcommon_master.masterunkid = " & intRelationID & " "
                    End If

                    'Pinkal (01-Oct-2014) -- End

                    'Sohail (22 Aug 2017) -- Start
                    'Enhancement - 69.1 - New functions to qualify Age of dependents in payroll 
                    If intAgeFrom <> 0 Then
                        strQ &= "AND ((CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT) - CAST(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate, 112) AS INT)) / 10000) >= @intAgeFrom "
                    End If

                    If intAgeTo <> 0 Then
                        strQ &= "AND ((CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT) - CAST(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate, 112) AS INT)) / 10000) <= @intAgeTo "
                    End If
                    'Sohail (22 Aug 2017) -- End

                End If

                If blnLeave Then

                    strQ &= " UNION " & _
                                " SELECT " & _
                                " mddependant_exception.dependantunkid as dependent_Id" & _
                                ", ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name, '') AS dependants " & _
                                ", YEAR(GETDATE()) - YEAR(hrdependants_beneficiaries_tran.birthdate) AS age " & _
                                  ",(DATEDIFF(MONTH,hrdependants_beneficiaries_tran.birthdate,GETDATE())+12)%12 - CASE WHEN DAY(hrdependants_beneficiaries_tran.birthdate)>DAY(GETDATE()) THEN 1 ELSE 0 END AS Months " & _
                                ", ISNULL(cfcommon_master.name, '') AS relation " & _
                                ", CASE WHEN hrdependants_beneficiaries_tran.gender = 1 then @male  " & _
                                "           WHEN hrdependants_beneficiaries_tran.gender = 2 then @female " & _
                                "           WHEN hrdependants_beneficiaries_tran.gender = 3 then @other " & _
                                " ELSE  ''  " & _
                                " END as gender " & _
                                " FROM  mddependant_exception " & _
                                " JOIN hremployee_master ON mddependant_exception.employeeunkid = hremployee_master.employeeunkid " & _
                                " JOIN hrdependants_beneficiaries_tran ON mddependant_exception.employeeunkid = hrdependants_beneficiaries_tran.employeeunkid " & _
                                " JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                                " AND mddependant_exception.dependantunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid " & _
                                " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid " & _
                                " AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RELATIONS & " AND cfcommon_master.isactive = 1  " & _
                                " WHERE mddependant_exception.isvoid = 0 AND mddependant_exception.employeeunkid = @employeeunkid " & _
                                " AND mddependant_exception.isleave = 1 AND  Convert(Char(8),mddependant_exception.lvstopdate,112) < @asondate " & _
                                " AND hrdependants_beneficiaries_tran.isvoid = 0 " & _
                                " AND #TableDepn.isactive = 0 "
                    'S.SANDEEP |09-APR-2019| -- START {AND hrdependants_beneficiaries_tran.isvoid = 0} -- END
                    'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]

                    'Pinkal (01-Oct-2014) -- Start
                    'Enhancement -  Changes For FDRC Report

                    If intRelationID > 0 Then
                        strQ &= " AND cfcommon_master.masterunkid = " & intRelationID & " "
                    End If

                    'Pinkal (01-Oct-2014) -- End

                    'Sohail (22 Aug 2017) -- Start
                    'Enhancement - 69.1 - New functions to qualify Age of dependents in payroll 
                    If intAgeFrom <> 0 Then
                        strQ &= "AND ((CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT) - CAST(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate, 112) AS INT)) / 10000) >= @intAgeFrom "
                    End If

                    If intAgeTo <> 0 Then
                        strQ &= "AND ((CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT) - CAST(CONVERT(CHAR(8), hrdependants_beneficiaries_tran.birthdate, 112) AS INT)) / 10000) <= @intAgeTo "
                    End If
                    'Sohail (22 Aug 2017) -- End

                End If

            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 23, "Select"))
            objDataOperation.AddParameter("@male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 7, "Male"))
            objDataOperation.AddParameter("@female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 8, "Female"))
            objDataOperation.AddParameter("@other", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 9, "Other"))
            objDataOperation.AddParameter("@asondate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            'Sohail (22 Aug 2017) -- Start
            'Enhancement - 69.1 - New functions to qualify Age of dependents in payroll 
            If intAgeFrom <> 0 Then
                objDataOperation.AddParameter("@intAgeFrom", SqlDbType.Int, eZeeDataType.INT_SIZE, intAgeFrom)
            End If

            If intAgeTo <> 0 Then
                objDataOperation.AddParameter("@intAgeTo", SqlDbType.Int, eZeeDataType.INT_SIZE, intAgeTo)
            End If
            'Sohail (22 Aug 2017) -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            If dtAsOnDate <> Nothing Then
                objDataOperation.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            End If

            strQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetInActiveDependents; Module Name: " & mstrModuleName)
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dsList
    End Function

    'Pinkal (3-Sep-2014) -- End


    'Pinkal (27-Mar-2013) -- Start
    'Enhancement : TRA Changes

    Private Function SaveEmpDependantImage(ByVal objDataOperation As clsDataOperation, ByVal mintCompanyId As Integer, ByVal intUserId As Integer) As Boolean
        Dim exForce As Exception
        Try
            Dim objDependantImages As New clsdependant_Images
            objDependantImages._Companyunkid = mintCompanyId
            objDependantImages._Employeeunkid = mintEmployeeunkid
            objDependantImages._Dependantunkid = mintDpndtbeneficetranunkid

            If intUserId <= 0 Then
                intUserId = User._Object._Userunkid
            End If

            objDependantImages.GetData(objDependantImages._Companyunkid, objDependantImages._Employeeunkid, objDependantImages._Dependantunkid)
            objDependantImages._Userunkid = intUserId

            'Pinkal (01-Apr-2013) -- Start
            'Enhancement : TRA Changes

            objDependantImages._WebForm_Name = mstrWebFormName
            objDependantImages._Ip = mstrWebClientIP
            objDependantImages._Machine_Name = mstrWebHostName

            'Pinkal (01-Apr-2013) -- End


            If mbytPhoto IsNot Nothing Then
                Dim blnIsInserted As Boolean = False

                If objDependantImages._Photo IsNot Nothing Then

                    If objDependantImages._Photo.Length = mbytPhoto.Length Then

                        Dim Hash1() As Byte = New MD5CryptoServiceProvider().ComputeHash(objDependantImages._Photo)
                        Dim Hash2() As Byte = New MD5CryptoServiceProvider().ComputeHash(mbytPhoto)

                        For i As Int32 = 0 To Hash2.Length - 1
                            If Hash1(i) = Hash2(i) Then
                                blnIsInserted = True
                                Exit For
                            End If
                        Next

                    End If

                End If

                If blnIsInserted = False Then
                    objDependantImages._Photo = mbytPhoto
                    If objDependantImages.Insert(objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

            Else

                If objDependantImages.Delete(objDataOperation, objDependantImages._Dependantimgunkid) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SaveEmpDependantImage", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    'Pinkal (27-Mar-2013) -- End


    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Public Function GetEmployeeMaxCountCRBenefitDependants(ByVal mintEmployeeID As Integer, Optional ByVal dtAsOnDate As Date = Nothing) As DataTable
        'Sohail (18 May 2019) - [dtAsOnDate]
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Dim mdtTable As DataTable = Nothing
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            strQ = "SELECT * " & _
                       "INTO #TableDepn " & _
                       "FROM " & _
                       "( " & _
                           "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                           "FROM hrdependant_beneficiaries_status_tran "

            If mintEmployeeID > 0 Then
                strQ &= "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid "
            End If

            strQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If mintEmployeeID > 0 Then
                strQ &= "AND hrdependants_beneficiaries_tran.employeeunkid = @EmployeeID "
            End If

            If dtAsOnDate <> Nothing Then
                strQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            strQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End

            strQ &= " SELECT " & _
                      " cfcommon_master.name AS relation " & _
                      ",CASE WHEN maxcnt_crdependants >= COUNT(cfcommon_master.name) THEN  COUNT(cfcommon_master.name) ELSE maxcnt_crdependants END MaxCount " & _
                      " FROM hrdependants_beneficiaries_tran " & _
                      " JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdependants_beneficiaries_tran.relationunkid AND mastertype =  " & clsCommon_Master.enCommonMaster.RELATIONS & _
                      " JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                      " WHERE employeeunkid = @EmployeeID " & _
                      " AND hrdependants_beneficiaries_tran.isvoid = 0 AND #TableDepn.isactive = 1 " & _
                      " AND (((CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT) - CAST(CONVERT(CHAR(8), birthdate, 112) AS INT)) / 10000) <= ISNULL(cfcommon_master.dependant_limit, 0) " & _
                      " OR ISNULL(cfcommon_master.dependant_limit, 0) = 0) AND hrdependants_beneficiaries_tran.isvoid = 0 " & _
                      " AND cfcommon_master.iscrbenefit = 1 AND maxcnt_crdependants > 0 " & _
                      " AND (((CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT) - CAST(CONVERT(CHAR(8), birthdate, 112) AS INT)) / 10000) >= ISNULL(cfcommon_master.minagelimit_crdependants, 0) " & _
                      " OR ISNULL(cfcommon_master.minagelimit_crdependants, 0) = 0) " & _
                      " GROUP BY cfcommon_master.name,maxcnt_crdependants "
            'Sohail (18 May 2019) - [JOIN #TableDepn, AND hrdependants_beneficiaries_tran.isvoid = 0 AND #TableDepn.isactive = 0]
            'Pinkal (13-Jun-2019) -- Start  'Enhancement - NMB FINAL LEAVE UAT CHANGES. [" OR ISNULL(cfcommon_master.minagelimit_crdependants, 0) = 0) ]

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            If dtAsOnDate <> Nothing Then
                objDataOperation.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            End If

            strQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            objDataOperation.AddParameter("@EmployeeID", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeID)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing Then
                mdtTable = dsList.Tables(0)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeMaxCountCRBenefitDependants; Module Name: " & mstrModuleName)
        End Try
        Return mdtTable
    End Function
    'Pinkal (25-Oct-2018) -- End

    'S.SANDEEP |04-JUN-2019| -- START
    'ISSUE/ENHANCEMENT : [Relation filters on Allocation Advance search]
    Public Shared Function GetCSV_EmpIds(ByVal intRelationId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim StrString As String = String.Empty
        Try
            Dim objDataOperation As New clsDataOperation

            StrQ = "SELECT ISNULL(STUFF((SELECT DISTINCT ',' + CAST(s.employeeunkid AS NVARCHAR(50)) FROM hrdependants_beneficiaries_tran s WHERE s.relationunkid IN (" & intRelationId & ") FOR XML PATH('')),1,1,''), '') AS CSV "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                StrString = dsList.Tables(0).Rows(0).Item("CSV")
            End If
            Return StrString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCSV_EmpIds", mstrModuleName)
            Return ""
        End Try
    End Function
    'S.SANDEEP |04-JUN-2019| -- END

    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    Public Function GetListForCombo(ByVal intEmployeeunkid As Integer, Optional ByVal blnFlag As Boolean = False, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal dtAsOnDate As Date = Nothing, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        'Sohail (18 May 2019) - [dtAsOnDate]

        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Try

            Dim objDataOperation As clsDataOperation
            If xDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOp
            End If
            objDataOperation.ClearParameters()

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            strQ = "SELECT * " & _
                       "INTO #TableDepn " & _
                       "FROM " & _
                       "( " & _
                           "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                           "FROM hrdependant_beneficiaries_status_tran "

            If intEmployeeunkid > 0 Then
                strQ &= "LEFT JOIN hrdependants_beneficiaries_tran ON hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid = hrdependants_beneficiaries_tran.dpndtbeneficetranunkid "
            End If

            strQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If intEmployeeunkid > 0 Then
                strQ &= "AND hrdependants_beneficiaries_tran.employeeunkid = @employeeunkid "
            End If

            If dtAsOnDate <> Nothing Then
                strQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            strQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End

            If blnFlag Then
                strQ &= " SELECT 0 AS Id, @SELECT AS Name UNION "
                objDataOperation.AddParameter("@SELECT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, " Select"))
            End If

            strQ &= "SELECT hrdependants_beneficiaries_tran.dpndtbeneficetranunkid AS Id " & _
                         ", REPLACE(ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name, '') + ' ' + ISNULL(hrdependants_beneficiaries_tran.last_name, ''), '  ',' ') AS Name " & _
                    "FROM hrdependants_beneficiaries_tran " & _
                    " JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                    "WHERE hrdependants_beneficiaries_tran.isvoid = 0 "
            'Sohail (18 May 2019) - [JOIN #TableDepn]

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            If blnOnlyActive = True Then
                strQ &= "AND #TableDepn.isactive = 1 "
            End If
            'Sohail (18 May 2019) -- End

            If intEmployeeunkid > 0 Then
                strQ &= " AND hrdependants_beneficiaries_tran.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            End If

            If blnFlag Then
                strQ &= "ORDER BY Name "
            Else
                strQ &= "ORDER BY ISNULL(hrdependants_beneficiaries_tran.first_name, '') + ISNULL(hrdependants_beneficiaries_tran.middle_name, '') + ISNULL(hrdependants_beneficiaries_tran.last_name, '') "
            End If

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            If dtAsOnDate <> Nothing Then
                objDataOperation.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            End If

            strQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetInActiveDependents; Module Name: " & mstrModuleName)
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (18 May 2019) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Dependant is already defined. Please define new Dependant.")
            Language.setMessage(mstrModuleName, 2, "This Beneficiary is already defined. Please define new Beneficiary.")
            Language.setMessage("clsEmployee_Master", 7, "Male")
            Language.setMessage("clsEmployee_Master", 8, "Female")
            Language.setMessage("clsEmployee_Master", 9, "Other")
            Language.setMessage("clsEmployee_Master", 23, "Select")
            Language.setMessage("clsMasterData", 73, "Male")
            Language.setMessage("clsMasterData", 74, "Female")

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class