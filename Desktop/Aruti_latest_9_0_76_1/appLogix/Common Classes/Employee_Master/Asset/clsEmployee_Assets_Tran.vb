﻿'************************************************************************************************************************************
'Class Name : clsEmployee_Assets_Tran.vb
'Purpose    :
'Date       :15/07/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master
Imports System
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END


''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsEmployee_Assets_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsEmployee_Assets_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAssetstranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintAssetunkid As Integer
    Private mstrAsset_No As String = String.Empty
    Private mintConditionunkid As Integer
    Private mintStatusunkid As Integer
    Private mdtAssign_Return_Date As Date
    Private mstrRemark As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mdtVoiddatetime As Date

    'Sandeep [ 17 DEC 2010 ] -- Start
    'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
    Private mstrAssetserial_No As String = String.Empty
    'Sandeep [ 17 DEC 2010 ] -- End 

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetstranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assetstranunkid() As Integer
        Get
            Return mintAssetstranunkid
        End Get
        Set(ByVal value As Integer)
            mintAssetstranunkid = value
            Call GetData(objDataOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assetunkid() As Integer
        Get
            Return mintAssetunkid
        End Get
        Set(ByVal value As Integer)
            mintAssetunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set asset_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Asset_No() As String
        Get
            Return mstrAsset_No
        End Get
        Set(ByVal value As String)
            mstrAsset_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set conditionunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Conditionunkid() As Integer
        Get
            Return mintConditionunkid
        End Get
        Set(ByVal value As Integer)
            mintConditionunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set asset_statusunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assign_return_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assign_Return_Date() As Date
        Get
            Return mdtAssign_Return_Date
        End Get
        Set(ByVal value As Date)
            mdtAssign_Return_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    'Sandeep [ 17 DEC 2010 ] -- Start
    'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
    ''' <summary>
    ''' Purpose: Get or Set assetserial_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assetserial_No() As String
        Get
            Return mstrAssetserial_No
        End Get
        Set(ByVal value As String)
            mstrAssetserial_No = value
        End Set
    End Property
    'Sandeep [ 17 DEC 2010 ] -- End 

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sandeep [ 16 Oct 2010 ] -- Start
        'objDataOperation = New clsDataOperation


        'S.SANDEEP [ 27 APRIL 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'Dim objDataOperation As New clsDataOperation
        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        'S.SANDEEP [ 27 APRIL 2012 ] -- END
        'Sandeep [ 16 Oct 2010 ] -- End 


        Try
            'Sandeep [ 17 DEC 2010 ] -- Start
            'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
            'strQ = "SELECT " & _
            '      "  assetstranunkid " & _
            '      ", employeeunkid " & _
            '      ", assetunkid " & _
            '      ", asset_no " & _
            '      ", conditionunkid " & _
            '      ", asset_statusunkid " & _
            '      ", assign_return_date " & _
            '      ", remark " & _
            '      ", userunkid " & _
            '      ", isvoid " & _
            '      ", voiduserunkid " & _
            '      ", voidreason " & _
            '      ", voiddatetime " & _
            '     "FROM hremployee_assets_tran " & _
            '     "WHERE assetstranunkid = @assetstranunkid "

            strQ = "SELECT " & _
              "  assetstranunkid " & _
              ", employeeunkid " & _
              ", assetunkid " & _
              ", asset_no " & _
              ", conditionunkid " & _
              ", asset_statusunkid " & _
              ", assign_return_date " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
              ", assetserial_no " & _
             "FROM hremployee_assets_tran " & _
             "WHERE assetstranunkid = @assetstranunkid "
            'Sandeep [ 17 DEC 2010 ] -- End 

            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.ClearParameters()
            'S.SANDEEP [ 27 APRIL 2012 ] -- END
            objDataOperation.AddParameter("@assetstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetstranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAssetstranunkid = CInt(dtRow.Item("assetstranunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintAssetunkid = CInt(dtRow.Item("assetunkid"))
                mstrAsset_No = dtRow.Item("asset_no").ToString
                mintConditionunkid = CInt(dtRow.Item("conditionunkid"))
                mintStatusunkid = CInt(dtRow.Item("asset_statusunkid"))
                mdtAssign_Return_Date = dtRow.Item("assign_return_date")
                mstrRemark = dtRow.Item("remark").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                If IsDBNull(dtRow.Item("voiddatetime")).ToString Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                'Sandeep [ 17 DEC 2010 ] -- Start
                'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
                mstrAssetserial_No = dtRow.Item("assetserial_no").ToString
                'Sandeep [ 17 DEC 2010 ] -- End 

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '''' <summary>
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Sohail (04 Jan 2012) -- Start
    'Public Function GetList(ByVal strTableName As String) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        'Sandeep [ 17 DEC 2010 ] -- Start
    '        'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
    '        'strQ = "SELECT " & _
    '        '        " ISNULL(Asset.name,'') AS Asset " & _
    '        '        ",ISNULL(Condition.name,'') AS Condtion " & _
    '        '        ",ISNULL(Status.name,'') AS Status " & _
    '        '        ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '        ",hremployee_assets_tran.asset_no AS AssetNo " & _
    '        '        ",CONVERT(CHAR(8),hremployee_assets_tran.assign_return_date,112) AS Date " & _
    '        '        ",hremployee_assets_tran.remark AS Remark " & _
    '        '        ",hremployee_master.employeeunkid AS EmpId " & _
    '        '        ",Asset.masterunkid AS AssetId " & _
    '        '        ",Condition.masterunkid AS CondId " & _
    '        '        ",Status.masterunkid AS StatusId " & _
    '        '        ",hremployee_assets_tran.assetstranunkid AS AssetTranUnkid " & _
    '        '    " FROM hremployee_assets_tran " & _
    '        '        " LEFT JOIN cfcommon_master AS Status ON Status.masterunkid = hremployee_assets_tran.asset_statusunkid AND Status.mastertype = " & enCommonMaster.ASSET_STATUS & _
    '        '        " LEFT JOIN cfcommon_master AS Condition ON Condition.masterunkid = hremployee_assets_tran.conditionunkid AND Condition.mastertype = " & enCommonMaster.ASSET_CONDITION & _
    '        '        " LEFT JOIN cfcommon_master AS Asset ON Asset.masterunkid = hremployee_assets_tran.assetunkid AND Asset.mastertype = " & enCommonMaster.ASSETS & _
    '        '        " LEFT JOIN hremployee_master ON hremployee_assets_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '    " WHERE ISNULL(hremployee_assets_tran.isvoid,0) = 0 "



    'strQ = "SELECT " & _
    '        " ISNULL(Asset.name,'') AS Asset " & _
    '        ",ISNULL(Condition.name,'') AS Condtion " & _
    '        ",ISNULL(Status.name,'') AS Status " & _
    '        ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        ",hremployee_assets_tran.asset_no AS AssetNo " & _
    '        ",CONVERT(CHAR(8),hremployee_assets_tran.assign_return_date,112) AS Date " & _
    '        ",hremployee_assets_tran.remark AS Remark " & _
    '        ",hremployee_master.employeeunkid AS EmpId " & _
    '        ",Asset.masterunkid AS AssetId " & _
    '        ",Condition.masterunkid AS CondId " & _
    '        ",Status.masterunkid AS StatusId " & _
    '        ",hremployee_assets_tran.assetstranunkid AS AssetTranUnkid " & _
    '                             ", assetserial_no As assetserial_no " & _
    '    " FROM hremployee_assets_tran " & _
    '        " LEFT JOIN cfcommon_master AS Status ON Status.masterunkid = hremployee_assets_tran.asset_statusunkid AND Status.mastertype = " & enCommonMaster.ASSET_STATUS & _
    '        " LEFT JOIN cfcommon_master AS Condition ON Condition.masterunkid = hremployee_assets_tran.conditionunkid AND Condition.mastertype = " & enCommonMaster.ASSET_CONDITION & _
    '        " LEFT JOIN cfcommon_master AS Asset ON Asset.masterunkid = hremployee_assets_tran.assetunkid AND Asset.mastertype = " & enCommonMaster.ASSETS & _
    '        " LEFT JOIN hremployee_master ON hremployee_assets_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '    " WHERE ISNULL(hremployee_assets_tran.isvoid,0) = 0 "


    '        'S.SANDEEP [ 29 JUNE 2011 ] -- START
    '        'ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '        End If
    '        'S.SANDEEP [ 29 JUNE 2011 ] -- END


    '        'Anjan (24 Jun 2011)-Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.
    '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")   "
    '        End If
    '        'Anjan (24 Jun 2011)-End 




    '        'Sandeep [ 17 DEC 2010 ] -- End 

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function



    'S.SANDEEP [ 27 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Function GetList(ByVal strTableName As String, _
    '                        Optional ByVal intEmployeeUnkId As Integer = 0, _
    '                        Optional ByVal intAssetId As Integer = 0, _
    '                        Optional ByVal intConditionId As Integer = 0, _
    '                        Optional ByVal intStatusId As Integer = 0, _
    '                        Optional ByVal strAssestNo As String = "", _
    '                        Optional ByVal mstrAdvanceFilter As String = "") As DataSet

    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal intEmployeeID As Integer = -1, _
                            Optional ByVal strFilerString As String = "", _
                            Optional ByVal intAssetId As Integer = 0, _
                            Optional ByVal intConditionId As Integer = 0, _
                            Optional ByVal intStatusId As Integer = 0, _
                            Optional ByVal strAssestNo As String = "", _
                            Optional ByVal IsUsedAsMSS As Boolean = True, _
                            Optional ByVal mblnAddApprovalCondition As Boolean = True) As DataSet

        'S.SANDEEP [20-JUN-2018] -- 'Enhancement - Implementing Employee Approver Flow For NMB .[Optional ByVal mblnAddApprovalCondition As Boolean = True]
        'Pinkal (28-Dec-2015) -- Start    'Enhancement - Working on Changes in SS for Employee Master. [Optional ByVal IsUsedAsMSS As Boolean = True] 

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim strStatus As String
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)

            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            'If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, "", mblnAddApprovalCondition)
            'S.SANDEEP [20-JUN-2018] -- End


            'Pinkal (06-Jan-2016) -- End

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation
                strStatus = ", CASE WHEN hremployee_assets_tran.asset_statusunkid > 4 THEN 0 ELSE hremployee_assets_tran.asset_statusunkid END AS asset_statusunkid " & _
                            ", CASE hremployee_assets_tran.asset_statusunkid WHEN 1 THEN @Assigned " & _
                            "                                                WHEN 2 THEN @Returned  " & _
                            "                                                WHEN 3 THEN @WrittenOff  " & _
                            "                                                WHEN 4 THEN @Lost  " & _
                            "  ELSE @Select END AS Status "

                StrQ = "SELECT " & _
                       "     hremployee_assets_tran.assetstranunkid " & _
                       "    ,hremployee_assets_tran.employeeunkid " & _
                       "    ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
                       "    ,hremployee_assets_tran.assetunkid " & _
                       "    ,ISNULL(Asset.name, '') AS Asset " & _
                       "    ,hremployee_assets_tran.asset_no AS AssetNo " & _
                       "    ,hremployee_assets_tran.conditionunkid " & _
                       "    ,ISNULL(Condition.name, '') AS Condtion " & _
                       strStatus & _
                       "    ,CONVERT(CHAR(8),hremployee_assets_tran.assign_return_date,112) AS Date " & _
                       "    ,hremployee_assets_tran.remark " & _
                       "    ,hremployee_assets_tran.userunkid " & _
                       "    ,hremployee_assets_tran.isvoid " & _
                       "    ,hremployee_assets_tran.voiduserunkid " & _
                       "    ,hremployee_assets_tran.voidreason " & _
                       "    ,hremployee_assets_tran.voiddatetime " & _
                       "    ,hremployee_assets_tran.assetserial_no " & _
                       "FROM hremployee_assets_tran " & _
                       "    LEFT JOIN hremployee_master ON hremployee_assets_tran.employeeunkid = hremployee_master.employeeunkid " & _
                       "    LEFT JOIN cfcommon_master AS Asset ON Asset.masterunkid = hremployee_assets_tran.assetunkid AND Asset.mastertype = " & enCommonMaster.ASSETS & " " & _
                       "    LEFT JOIN cfcommon_master AS Condition ON Condition.masterunkid = hremployee_assets_tran.conditionunkid AND Condition.mastertype = " & enCommonMaster.ASSET_CONDITION & " "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                StrQ &= "WHERE ISNULL(hremployee_assets_tran.isvoid,0) = 0 "

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If intEmployeeID > 0 Then
                    StrQ &= " AND hremployee_assets_tran.employeeunkid = @employeeunkid  "
                    objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
                End If

                If intAssetId > 0 Then
                    StrQ &= " AND hremployee_assets_tran.assetunkid = @assetunkid "
                    objDo.AddParameter("@assetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetId)
                End If

                If intConditionId > 0 Then
                    StrQ &= " AND hremployee_assets_tran.conditionunkid = @conditionunkid "
                    objDo.AddParameter("@conditionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intConditionId)
                End If

                If intStatusId > 0 Then
                    StrQ &= " AND hremployee_assets_tran.asset_statusunkid = @asset_statusunkid "
                    objDo.AddParameter("@asset_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusId)
                End If


                'Anjan (20 Aug 2018) -- Start
                'Internal issue - On searching for asset no in company asset list it was giving error as alias for asset no column was used in filter.
                'If strAssestNo.Trim <> "" Then
                '    StrQ &= "AND AssetNo LIKE '%" & strAssestNo.Trim & "%'" & " "
                'End If
                If strAssestNo.Trim <> "" Then
                    StrQ &= "AND hremployee_assets_tran.asset_no LIKE '%" & strAssestNo.Trim & "%'" & " "
                End If
                'Anjan (20 Aug 2018) -- End



                If strFilerString.Trim.Length > 0 Then
                    StrQ &= " AND " & strFilerString
                End If


                objDo.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
                objDo.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Assigned"))
                objDo.AddParameter("@Returned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Returned"))
                objDo.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "WrittenOff"))
                objDo.AddParameter("@Lost", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Lost"))

                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    'Public Function GetList(ByVal strTableName As String, _
    '                        Optional ByVal intEmployeeUnkId As Integer = 0, _
    '                        Optional ByVal intAssetId As Integer = 0, _
    '                        Optional ByVal intConditionId As Integer = 0, _
    '                        Optional ByVal intStatusId As Integer = 0, _
    '                        Optional ByVal strAssestNo As String = "", _
    '                        Optional ByVal mstrAdvanceFilter As String = "", _
    '                        Optional ByVal strIncludeInactiveEmployee As String = "", _
    '                        Optional ByVal strEmployeeAsOnDate As String = "", _
    '                        Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet

    '    'S.SANDEEP [ 27 APRIL 2012 ] -- END

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim strStatus As String
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try

    '        'Issue : If you change any of this series order below please give effect on clsmaster data on related function (getComboListForEmployeeAseetStatus)
    '        strStatus = ", CASE WHEN hremployee_assets_tran.asset_statusunkid > 4 THEN 0 ELSE hremployee_assets_tran.asset_statusunkid END AS asset_statusunkid "
    '        strStatus &= ", CASE hremployee_assets_tran.asset_statusunkid WHEN 1 THEN @Assigned " & _
    '                                                             "WHEN 2 THEN @Returned  " & _
    '                                                             "WHEN 3 THEN @WrittenOff  " & _
    '                                                             "WHEN 4 THEN @Lost  " & _
    '                                                             "ELSE @Select END AS Status "


    '        strQ = "SELECT  hremployee_assets_tran.assetstranunkid " & _
    '                      ", hremployee_assets_tran.employeeunkid " & _
    '                             ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '                      ", hremployee_assets_tran.assetunkid " & _
    '                      ", ISNULL(Asset.name, '') AS Asset " & _
    '                             ",hremployee_assets_tran.asset_no AS AssetNo " & _
    '                      ", hremployee_assets_tran.conditionunkid " & _
    '                      ", ISNULL(Condition.name, '') AS Condtion " & _
    '                         strStatus & _
    '                             ",CONVERT(CHAR(8),hremployee_assets_tran.assign_return_date,112) AS Date " & _
    '                      ", hremployee_assets_tran.remark " & _
    '                      ", hremployee_assets_tran.userunkid " & _
    '                      ", hremployee_assets_tran.isvoid " & _
    '                      ", hremployee_assets_tran.voiduserunkid " & _
    '                      ", hremployee_assets_tran.voidreason " & _
    '                      ", hremployee_assets_tran.voiddatetime " & _
    '                      ", hremployee_assets_tran.assetserial_no " & _
    '                        " FROM hremployee_assets_tran " & _
    '                             " LEFT JOIN hremployee_master ON hremployee_assets_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                        "LEFT JOIN cfcommon_master AS Asset ON Asset.masterunkid = hremployee_assets_tran.assetunkid " & _
    '                                                              "AND Asset.mastertype = " & enCommonMaster.ASSETS & " " & _
    '                        "LEFT JOIN cfcommon_master AS Condition ON Condition.masterunkid = hremployee_assets_tran.conditionunkid " & _
    '                                                                  "AND Condition.mastertype = " & enCommonMaster.ASSET_CONDITION & " " & _
    '                        " WHERE ISNULL(hremployee_assets_tran.isvoid,0) = 0 "




    '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES

    '        ''ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    '        'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '        '    'Sohail (06 Jan 2012) -- Start
    '        '    'TRA - ENHANCEMENT
    '        '    'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '        '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

    '        '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    'Sohail (06 Jan 2012) -- End
    '        'End If

    '        If strIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '        End If

    '        If CBool(strIncludeInactiveEmployee) = False Then
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '        End If
    '        'S.SANDEEP [ 27 APRIL 2012 ] -- END





    '        'Issue : According to privilege that lower level user should not see superior level employees.


    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")   "
    '        'End If



    '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES

    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")   "
    '        '        End If
    '        'End Select

    '        If strUserAccessLevelFilterString = "" Then
    '            strQ &= UserAccessLevel._AccessLevelFilterString
    '        Else
    '            strQ &= strUserAccessLevelFilterString
    '        End If
    '        'S.SANDEEP [ 27 APRIL 2012 ] -- END



    '        'S.SANDEEP [ 04 FEB 2012 ] -- END


    '        If intEmployeeUnkId > 0 Then
    '            strQ &= " AND hremployee_assets_tran.employeeunkid = @employeeunkid "
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
    '        End If

    '        If intAssetId > 0 Then
    '            strQ &= " AND hremployee_assets_tran.assetunkid = @assetunkid "
    '            objDataOperation.AddParameter("@assetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetId)
    '        End If

    '        If intConditionId > 0 Then
    '            strQ &= " AND hremployee_assets_tran.conditionunkid = @conditionunkid "
    '            objDataOperation.AddParameter("@conditionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intConditionId)
    '        End If

    '        If intStatusId > 0 Then
    '            strQ &= " AND hremployee_assets_tran.asset_statusunkid = @asset_statusunkid "
    '            objDataOperation.AddParameter("@asset_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusId)
    '        End If

    '        If strAssestNo.Trim <> "" Then
    '            strQ &= "AND AssetNo LIKE '%" & strAssestNo.Trim & "%'" & " "
    '        End If

    '        'Anjan (21 Nov 2011)-Start
    '        'ENHANCEMENT : TRA COMMENTS
    '        If mstrAdvanceFilter.Length > 0 Then
    '            strQ &= "AND " & mstrAdvanceFilter
    '        End If
    '        'Anjan (21 Nov 2011)-End 

    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
    '        objDataOperation.AddParameter("@Assigned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Assigned"))
    '        objDataOperation.AddParameter("@Returned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Returned"))
    '        objDataOperation.AddParameter("@WrittenOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "WrittenOff"))
    '        objDataOperation.AddParameter("@Lost", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Lost"))

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'S.SANDEEP [04 JUN 2015] -- END

    'Sohail (04 Jan 2012) -- End


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_assets_tran) </purpose>
    Public Function Insert() As Boolean
        'Sandeep [ 17 DEC 2010 ] -- Start
        'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
        'If isExist(mintEmployeeunkid, mintAssetunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Asset is already bind to selected employee. Please add new asset.")
        '    Return False
        'End If

        If isExist(mintEmployeeunkid, mintAssetunkid, mstrAssetserial_No) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Asset is already linked to selected employee. Please add new asset.")
            Return False
        End If
        'Sandeep [ 17 DEC 2010 ] -- End 

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFalg As Boolean = False
        Dim objStatus As New clsEmployee_Assets_Status_Tran

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@assetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetunkid.ToString)
            objDataOperation.AddParameter("@asset_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAsset_No.ToString)
            objDataOperation.AddParameter("@conditionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintConditionunkid.ToString)
            objDataOperation.AddParameter("@asset_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@assign_return_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAssign_Return_Date)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            'Sandeep [ 17 DEC 2010 ] -- Start
            'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
            objDataOperation.AddParameter("@assetserial_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssetserial_No.ToString)

            'strQ = "INSERT INTO hremployee_assets_tran ( " & _
            '          "  employeeunkid " & _
            '          ", assetunkid " & _
            '          ", asset_no " & _
            '          ", conditionunkid " & _
            '          ", asset_statusunkid " & _
            '          ", assign_return_date " & _
            '          ", remark " & _
            '          ", userunkid " & _
            '          ", isvoid " & _
            '          ", voiduserunkid " & _
            '          ", voidreason" & _
            '          ", voiddatetime " & _
            '        ") VALUES (" & _
            '          "  @employeeunkid " & _
            '          ", @assetunkid " & _
            '          ", @asset_no " & _
            '          ", @conditionunkid " & _
            '          ", @asset_statusunkid " & _
            '          ", @assign_return_date " & _
            '          ", @remark " & _
            '          ", @userunkid " & _
            '          ", @isvoid " & _
            '          ", @voiduserunkid " & _
            '          ", @voidreason" & _
            '          ", @voiddatetime " & _
            '        "); SELECT @@identity"

            strQ = "INSERT INTO hremployee_assets_tran ( " & _
                      "  employeeunkid " & _
                      ", assetunkid " & _
                      ", asset_no " & _
                      ", conditionunkid " & _
                      ", asset_statusunkid " & _
                      ", assign_return_date " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidreason" & _
                      ", voiddatetime " & _
                      ", assetserial_no " & _
                    ") VALUES (" & _
                      "  @employeeunkid " & _
                      ", @assetunkid " & _
                      ", @asset_no " & _
                      ", @conditionunkid " & _
                      ", @asset_statusunkid " & _
                      ", @assign_return_date " & _
                      ", @remark " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voidreason" & _
                      ", @voiddatetime " & _
                      ", @assetserial_no " & _
                    "); SELECT @@identity"
            'Sandeep [ 17 DEC 2010 ] -- End 



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAssetstranunkid = dsList.Tables(0).Rows(0).Item(0)

            With objStatus
                ._Assetstranunkid = mintAssetstranunkid
                ._Status_Date = mdtAssign_Return_Date
                ._Statusunkid = mintStatusunkid
                ._Remark = ""
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                '._Userunkid = User._Object._Userunkid
                ._Userunkid = mintUserunkid
                'S.SANDEEP [ 27 APRIL 2012 ] -- END
                ._Isvoid = False
                ._Voiddatetime = Nothing
                ._Voiduserunkid = -1
                'S.SANDEEP [ 04 DEC 2013 ] -- START
                'blnFalg = .Insert(True)
                blnFalg = .Insert(True, objDataOperation)
                'S.SANDEEP [ 04 DEC 2013 ] -- END
            End With

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
            'InsertAuditTrailForEmpAssetsTran(objDataOperation, 1)
            If InsertAuditTrailForEmpAssetsTran(objDataOperation, 1) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Anjan (11 Jun 2011)-End 

            If blnFalg = True Then
                objDataOperation.ReleaseTransaction(True)
            Else
                objDataOperation.ReleaseTransaction(False)
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_assets_tran) </purpose>
    Public Function Update(Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [ 04 DEC 2013 ] -- START -- END
        'Public Function Update() As Boolean
        'Sandeep [ 17 DEC 2010 ] -- Start
        'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
        'If isExist(mintEmployeeunkid, mintAssetunkid, mintAssetstranunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Asset is already bind to selected employee. Please add new asset.")
        '    Return False
        'End If
        If isExist(mintEmployeeunkid, mintAssetunkid, mstrAssetserial_No, mintAssetstranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Asset is already linked to selected employee. Please add new asset.")
            Return False
        End If
        'Sandeep [ 17 DEC 2010 ] -- End 

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [ 04 DEC 2013 ] -- START
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If
        'S.SANDEEP [ 04 DEC 2013 ] -- END

        Try
            objDataOperation.AddParameter("@assetstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetstranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@assetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetunkid.ToString)
            objDataOperation.AddParameter("@asset_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAsset_No.ToString)
            objDataOperation.AddParameter("@conditionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintConditionunkid.ToString)
            objDataOperation.AddParameter("@asset_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@assign_return_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAssign_Return_Date)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            'Sandeep [ 17 DEC 2010 ] -- Start
            'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
            objDataOperation.AddParameter("@assetserial_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssetserial_No.ToString)

            'strQ = "UPDATE hremployee_assets_tran SET " & _
            '          "  employeeunkid = @employeeunkid" & _
            '          ", assetunkid = @assetunkid" & _
            '          ", asset_no = @asset_no" & _
            '          ", conditionunkid = @conditionunkid" & _
            '          ", asset_statusunkid = @asset_statusunkid" & _
            '          ", assign_return_date = @assign_return_date" & _
            '          ", remark = @remark" & _
            '          ", userunkid = @userunkid" & _
            '          ", isvoid = @isvoid" & _
            '          ", voiduserunkid = @voiduserunkid" & _
            '          ", voidreason = @voidreason " & _
            '          ", voiddatetime = @voiddatetime " & _
            '      "WHERE assetstranunkid = @assetstranunkid "

            strQ = "UPDATE hremployee_assets_tran SET " & _
                      "  employeeunkid = @employeeunkid" & _
                      ", assetunkid = @assetunkid" & _
                      ", asset_no = @asset_no" & _
                      ", conditionunkid = @conditionunkid" & _
                      ", asset_statusunkid = @asset_statusunkid" & _
                      ", assign_return_date = @assign_return_date" & _
                      ", remark = @remark" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                      ", voiddatetime = @voiddatetime " & _
                      ", assetserial_no = @assetserial_no " & _
                  "WHERE assetstranunkid = @assetstranunkid "
            'Sandeep [ 17 DEC 2010 ] -- End 

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
            'InsertAuditTrailForEmpAssetsTran(objDataOperation, 2)
            If InsertAuditTrailForEmpAssetsTran(objDataOperation, 2) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Anjan (11 Jun 2011)-End 


            'S.SANDEEP [ 04 DEC 2013 ] -- START
            'objDataOperation.ReleaseTransaction(True)
            If objDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP [ 04 DEC 2013 ] -- END

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployee_assets_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            'Pinkal (05-Mar-2012) -- Start
            'Enhancement : TRA Changes

            strQ = "UPDATE hremployee_assets_status_tran SET " & _
                     "  isvoid = @isvoid" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voiddatetime = @voiddatetime " & _
                     "WHERE assetstranunkid = @assetstranunkid AND statusunkid = 1 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (05-Mar-2012) -- End


            strQ = "UPDATE hremployee_assets_tran SET " & _
              "  isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidreason = @voidreason " & _
              ", voiddatetime = @voiddatetime " & _
            "WHERE assetstranunkid = @assetstranunkid "

            'Sandeep [ 16 Oct 2010 ] -- Start
            'objDataOperation.AddParameter("@assetstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            'objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsVoid.ToString)
            'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUser.ToString)
            'objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strReason.ToString)
            'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtvoiddate)
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            'Sandeep [ 16 Oct 2010 ] -- End 


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sandeep [ 16 Oct 2010 ] -- Start
            'mintAssetstranunkid = intUnkid
            _Assetstranunkid = intUnkid
            'Sandeep [ 16 Oct 2010 ] -- End 


            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
            'InsertAuditTrailForEmpAssetsTran(objDataOperation, 3)
            If InsertAuditTrailForEmpAssetsTran(objDataOperation, 3) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Anjan (11 Jun 2011)-End 

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@assetstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sandeep [ 17 DEC 2010 ] -- Start
    'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' Public Function isExist(ByVal intEmpId As String, ByVal intAssetId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
    Public Function isExist(ByVal intEmpId As String, ByVal intAssetId As Integer, ByVal strSrNo As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        'Sandeep [ 17 DEC 2010 ] -- End 
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sandeep [ 14 Aug 2010 ] -- Start
            'strQ = "SELECT " & _
            '        "  assetstranunkid " & _
            '        ", employeeunkid " & _
            '        ", assetunkid " & _
            '        ", asset_no " & _
            '        ", conditionunkid " & _
            '        ", asset_statusunkid " & _
            '        ", assign_return_date " & _
            '        ", remark " & _
            '        ", userunkid " & _
            '        ", isvoid " & _
            '        ", voiduserunkid " & _
            '        ", voidreason " & _
            '    "FROM hremployee_assets_tran " & _
            '    "WHERE employeeunkid = @EmpId " & _
            '    "AND assetunkid = @AssetId "

            'Sandeep [ 17 DEC 2010 ] -- Start
            'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
            'strQ = "SELECT " & _
            '        "  assetstranunkid " & _
            '        ", employeeunkid " & _
            '        ", assetunkid " & _
            '        ", asset_no " & _
            '        ", conditionunkid " & _
            '        ", asset_statusunkid " & _
            '        ", assign_return_date " & _
            '        ", remark " & _
            '        ", userunkid " & _
            '        ", isvoid " & _
            '        ", voiduserunkid " & _
            '        ", voidreason " & _
            '    "FROM hremployee_assets_tran " & _
            '    "WHERE employeeunkid = @EmpId " & _
            '    "AND assetunkid = @AssetId " & _
            '    "AND ISNULL(isvoid,0) = 0 "

            strQ = "SELECT " & _
                    "  assetstranunkid " & _
                    ", employeeunkid " & _
                    ", assetunkid " & _
                    ", asset_no " & _
                    ", conditionunkid " & _
                    ", asset_statusunkid " & _
                    ", assign_return_date " & _
                    ", remark " & _
                    ", userunkid " & _
                    ", isvoid " & _
                    ", voiduserunkid " & _
                    ", voidreason " & _
                "FROM hremployee_assets_tran " & _
                "WHERE employeeunkid = @EmpId " & _
                "AND assetunkid = @AssetId " & _
                "AND assetserial_no = @assetserial_no " & _
                "AND ISNULL(isvoid,0) = 0 "
            'Sandeep [ 17 DEC 2010 ] -- End 

            'Sandeep [ 14 Aug 2010 ] -- End 


            If intUnkid > 0 Then
                strQ &= " AND assetstranunkid <> @assetstranunkid"
            End If

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@AssetId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetId)
            objDataOperation.AddParameter("@assetstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            'Sandeep [ 17 DEC 2010 ] -- Start
            'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
            objDataOperation.AddParameter("@assetserial_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strSrNo)
            'Sandeep [ 17 DEC 2010 ] -- End 

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'Anjan (11 Jun 2011)-Start
    'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
    'Public Sub InsertAuditTrailForEmpAssetsTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer)
    Public Function InsertAuditTrailForEmpAssetsTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean

        'Anjan (11 Jun 2011)-End 

        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Try
            'Sandeep [ 17 DEC 2010 ] -- Start
            'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
            'strQ = "INSERT INTO athremployee_assets_tran ( " & _
            '            "  assetstranunkid " & _
            '            ", employeeunkid " & _
            '            ", assetunkid " & _
            '            ", asset_no " & _
            '            ", conditionunkid " & _
            '            ", asset_statusunkid " & _
            '            ", assign_return_date " & _
            '            ", remark " & _
            '            ", audittype " & _
            '            ", audituserunkid " & _
            '            ", auditdatetime " & _
            '            ", ip " & _
            '            ", machine_name " & _
            '          ") VALUES (" & _
            '            "  @assetstranunkid " & _
            '            ", @employeeunkid " & _
            '            ", @assetunkid " & _
            '            ", @asset_no " & _
            '            ", @conditionunkid " & _
            '            ", @asset_statusunkid " & _
            '            ", @assign_return_date" & _
            '            ", @remark " & _
            '            ", @audittype " & _
            '            ", @audituserunkid " & _
            '            ", @auditdatetime " & _
            '            ", @ip" & _
            '            ", @machine_name " & _
            '          "); "


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            '  strQ = "INSERT INTO athremployee_assets_tran ( " & _
            '  "  assetstranunkid " & _
            '  ", employeeunkid " & _
            '  ", assetunkid " & _
            '  ", asset_no " & _
            '  ", conditionunkid " & _
            '  ", asset_statusunkid " & _
            '  ", assign_return_date " & _
            '  ", remark " & _
            '  ", audittype " & _
            '  ", audituserunkid " & _
            '  ", auditdatetime " & _
            '  ", ip " & _
            '  ", machine_name " & _
            '              ", assetserial_no " & _
            '") VALUES (" & _
            '  "  @assetstranunkid " & _
            '  ", @employeeunkid " & _
            '  ", @assetunkid " & _
            '  ", @asset_no " & _
            '  ", @conditionunkid " & _
            '  ", @asset_statusunkid " & _
            '  ", @assign_return_date" & _
            '  ", @remark " & _
            '  ", @audittype " & _
            '  ", @audituserunkid " & _
            '  ", @auditdatetime " & _
            '  ", @ip" & _
            '  ", @machine_name " & _
            '              ", @assetserial_no " & _
            '"); "
            'Sandeep [ 17 DEC 2010 ] -- End 


            'S.SANDEEP [ 13 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "INSERT INTO athremployee_assets_tran ( " & _
            '            "  assetstranunkid " & _
            '            ", employeeunkid " & _
            '            ", assetunkid " & _
            '            ", asset_no " & _
            '            ", conditionunkid " & _
            '            ", asset_statusunkid " & _
            '            ", assign_return_date " & _
            '            ", remark " & _
            '            ", audittype " & _
            '            ", audituserunkid " & _
            '            ", auditdatetime " & _
            '            ", ip " & _
            '            ", machine_name " & _
            '            ", assetserial_no " & _
            '            ", form_name " & _
            '            ", module_name1 " & _
            '            ", module_name2 " & _
            '            ", module_name3 " & _
            '            ", module_name4 " & _
            '            ", module_name5 " & _
            '            ", isweb " & _
            '      ") VALUES (" & _
            '            "  @assetstranunkid " & _
            '            ", @employeeunkid " & _
            '            ", @assetunkid " & _
            '            ", @asset_no " & _
            '            ", @conditionunkid " & _
            '            ", @asset_statusunkid " & _
            '            ", @assign_return_date" & _
            '            ", @remark " & _
            '            ", @audittype " & _
            '            ", @audituserunkid " & _
            '            ", @auditdatetime " & _
            '            ", @ip" & _
            '            ", @machine_name " & _
            '            ", @assetserial_no " & _
            '            ", @form_name " & _
            '            ", @module_name1 " & _
            '            ", @module_name2 " & _
            '            ", @module_name3 " & _
            '            ", @module_name4 " & _
            '            ", @module_name5 " & _
            '            ", @isweb " & _
            '      "); "
            strQ = "INSERT INTO athremployee_assets_tran ( " & _
            "  assetstranunkid " & _
            ", employeeunkid " & _
            ", assetunkid " & _
            ", asset_no " & _
            ", conditionunkid " & _
            ", asset_statusunkid " & _
            ", assign_return_date " & _
            ", remark " & _
            ", audittype " & _
            ", audituserunkid " & _
            ", auditdatetime " & _
            ", ip " & _
            ", machine_name " & _
                        ", assetserial_no " & _
                      ", form_name " & _
                      ", module_name1 " & _
                      ", module_name2 " & _
                      ", module_name3 " & _
                      ", module_name4 " & _
                      ", module_name5 " & _
                      ", isweb " & _
                        ", loginemployeeunkid " & _
          ") VALUES (" & _
            "  @assetstranunkid " & _
            ", @employeeunkid " & _
            ", @assetunkid " & _
            ", @asset_no " & _
            ", @conditionunkid " & _
            ", @asset_statusunkid " & _
            ", @assign_return_date" & _
            ", @remark " & _
            ", @audittype " & _
            ", @audituserunkid " & _
            ", @auditdatetime " & _
            ", @ip" & _
            ", @machine_name " & _
                        ", @assetserial_no " & _
                      ", @form_name " & _
                      ", @module_name1 " & _
                      ", @module_name2 " & _
                      ", @module_name3 " & _
                      ", @module_name4 " & _
                      ", @module_name5 " & _
                      ", @isweb " & _
                        ", @loginemployeeunkid " & _
          "); "
            'S.SANDEEP [ 13 AUG 2012 ] -- END




            'S.SANDEEP [ 19 JULY 2012 ] -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetstranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@assetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetunkid.ToString)
            objDataOperation.AddParameter("@asset_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAsset_No.ToString)
            objDataOperation.AddParameter("@conditionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintConditionunkid.ToString)
            objDataOperation.AddParameter("@asset_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@assign_return_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAssign_Return_Date)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            'S.SANDEEP [ 27 APRIL 2012 ] -- END
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)

            'S.SANDEEP [ 13 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
            'objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))
            'S.SANDEEP [ 13 AUG 2012 ] -- END


            'Sandeep [ 17 DEC 2010 ] -- Start
            'Issue : Discussed With Mr. Rutta to Keep Serial No. Along with Asset No
            objDataOperation.AddParameter("@assetserial_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssetserial_No.ToString)
            'Sandeep [ 17 DEC 2010 ] -- End 

            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            If mstrWebFormName.Trim.Length <= 0 Then
                'S.SANDEEP [ 11 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim frm As Form
                'For Each frm In Application.OpenForms
                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                '    End If
                'Next
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                'S.SANDEEP [ 11 AUG 2012 ] -- END
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 7, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            'S.SANDEEP [ 19 JULY 2012 ] -- END

            'S.SANDEEP [ 13 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
            'S.SANDEEP [ 13 AUG 2012 ] -- END

            'S.SANDEEP [ 19 JULY 2012 ] -- END

            objDataOperation.ExecNonQuery(strQ)

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
            Return True
            'Anjan (11 Jun 2011)-End 



            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForEmpAssetsTran", mstrModuleName)
            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there is any error occurs at time of insertion in AT table.
            Return False
            'Anjan (11 Jun 2011)-End 
        End Try
    End Function


    'S.SANDEEP [ 12 JAN 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function GetImportFileFormat(Optional ByVal StrList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOperation = New clsDataOperation
            'Gajanan (24 Nov 2018) -- Start
            'Enhancement : Import template column headers should read from language set for 
            'users (custom 1) and columns' date formats for importation should be clearly known
            'Replace Column Name With Getmessage
            StrQ = "SELECT " & _
                        " '' AS [" & Language.getMessage(mstrModuleName, 8, "Employee_Code") & "]" & _
                        ",'' AS [" & Language.getMessage(mstrModuleName, 9, "Company_Asset") & "]" & _
                        ",'' AS [" & Language.getMessage(mstrModuleName, 10, "Assignment_Date") & "]" & _
                        ",'' AS [" & Language.getMessage(mstrModuleName, 11, "Asset_Condition") & "]" & _
                        ",'' AS [" & Language.getMessage(mstrModuleName, 12, "Asset_Number") & "]" & _
                        ",'' AS [" & Language.getMessage(mstrModuleName, 13, "Serial_Number") & "]" & _
                        ",'' AS [" & Language.getMessage(mstrModuleName, 14, "Remark") & "] "
            'Gajanan (24 Nov 2018) -- End

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetImportFileFormat; Module Name: " & mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 12 JAN 2012 ] -- END


    'Public Function GetIdForAuditTrailDelete(ByVal intUnkid As Integer) As DataSet
    'Dim dsList As DataSet = Nothing
    'Dim strQ As String = ""
    'Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '          "  assetstranunkid " & _
    '          ", employeeunkid " & _
    '          ", assetunkid " & _
    '          ", asset_no " & _
    '          ", conditionunkid " & _
    '          ", asset_statusunkid " & _
    '          ", assign_return_date " & _
    '          ", remark " & _
    '         "FROM hremployee_assets_tran " & _
    '         "WHERE assetstranunkid = @assetstranunkid "

    '        objDataOperation.AddParameter("@assetstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        For Each dtRow As DataRow In dsList.Tables(0).Rows
    '            mintAssetstranunkid = CInt(dtRow.Item("assetstranunkid"))
    '            mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
    '            mintAssetunkid = CInt(dtRow.Item("assetunkid"))
    '            mstrAsset_No = dtRow.Item("asset_no").ToString
    '            mintConditionunkid = CInt(dtRow.Item("conditionunkid"))
    '            mintStatusunkid = CInt(dtRow.Item("asset_statusunkid"))
    '            mdtAssign_Return_Date = dtRow.Item("assign_return_date")
    '            mstrRemark = dtRow.Item("remark").ToString

    '            Exit For
    '        Next
    '        Return dsList
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetIdForAuditTrailDelete; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Asset is already linked to selected employee. Please add new asset.")
            Language.setMessage(mstrModuleName, 2, "Select")
            Language.setMessage(mstrModuleName, 3, "Assigned")
            Language.setMessage(mstrModuleName, 4, "Returned")
            Language.setMessage(mstrModuleName, 5, "WrittenOff")
            Language.setMessage(mstrModuleName, 6, "Lost")
            Language.setMessage(mstrModuleName, 7, "WEB")
			Language.setMessage(mstrModuleName, 8, "Employee_Code")
			Language.setMessage(mstrModuleName, 9, "Company_Asset")
			Language.setMessage(mstrModuleName, 10, "Assignment_Date")
			Language.setMessage(mstrModuleName, 11, "Asset_Condition")
			Language.setMessage(mstrModuleName, 12, "Asset_Number")
			Language.setMessage(mstrModuleName, 13, "Serial_Number")
            Language.setMessage(mstrModuleName, 14, "Remark")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
