﻿'************************************************************************************************************************************
'Class Name : clsemployee_holiday.vb
'Purpose    :
'Date       :09/07/2010
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsemployee_holiday
    Private Shared ReadOnly mstrModuleName As String = "clsemployee_holiday"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintHolidaytranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintHolidayunkid As Integer
    Private mintYearunkid As Integer
    Private mstrRemarks As String = String.Empty
    Private mblnIsvoid As Boolean
    Private mdtVoiddatime As Date
    Private mintUserunkid As Integer
    Private mintVoiduserunkid As Integer
#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set holidaytranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Holidaytranunkid() As Integer
        Get
            Return mintHolidaytranunkid
        End Get
        Set(ByVal value As Integer)
            mintHolidaytranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set holidayunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Holidayunkid() As Integer
        Get
            Return mintHolidayunkid
        End Get
        Set(ByVal value As Integer)
            mintHolidayunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Remarks
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remarks() As String
        Get
            Return mstrRemarks
        End Get
        Set(ByVal value As String)
            mstrRemarks = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  holidaytranunkid " & _
              ", employeeunkid " & _
              ", holidayunkid " & _
              ", Remarks " & _
              ", Userunkid " & _
             "FROM lvemployee_holiday " & _
             "WHERE holidaytranunkid = @holidaytranunkid"

            objDataOperation.AddParameter("@holidaytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHolidaytranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintHolidaytranunkid = CInt(dtRow.Item("holidaytranunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintHolidayunkid = CInt(dtRow.Item("holidayunkid"))
                mstrRemarks = dtRow.Item("Remarks").ToString
                mintUserunkid = CInt(dtRow.Item("Userunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal intEmpId As Integer = -1, Optional ByVal dtPeriodStart As DateTime = Nothing _
    '                        , Optional ByVal dtPeriodEnd As DateTime = Nothing _
    '                        , Optional ByVal objDataOperation As clsDataOperation = Nothing, Optional ByVal strUserAccessLevelFilterString As String = "" _
    '                        , Optional ByVal mstrFilter As String = "") As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    If objDataOperation Is Nothing Then
    '    objDataOperation = New clsDataOperation
    '    End If
    '    objDataOperation.ClearParameters()

    '    Try



    '        strQ = "SELECT " & _
    '          "  holidaytranunkid " & _
    '          ", lvemployee_holiday.employeeunkid " & _
    '          ", isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') as name" & _
    '          ", lvemployee_holiday.holidayunkid " & _
    '          ", lvholiday_master.holidayname " & _
    '          ", convert(char(8),lvholiday_master.holidaydate,112) as holidaydate " & _
    '          ", Remarks " & _
    '          ", lvholiday_master.color " & _
    '          ", Userunkid "


    '        strQ &= ",ISNULL(hremployee_master.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                 ",ISNULL(hremployee_master.unitgroupunkid,0) AS unitgroupunkid " & _
    '                 ",ISNULL(hremployee_master.teamunkid,0) AS teamunkid " & _
    '                 ",ISNULL(hremployee_master.stationunkid,0) AS stationunkid " & _
    '                 ",ISNULL(hremployee_master.deptgroupunkid,0) AS deptgroupunkid " & _
    '                 ",ISNULL(hremployee_master.departmentunkid,0) AS departmentunkid " & _
    '                 ",ISNULL(hremployee_master.sectionunkid,0) AS sectionunkid " & _
    '                 ",ISNULL(hremployee_master.unitunkid,0) AS unitunkid " & _
    '                 ",ISNULL(hremployee_master.jobunkid,0) AS jobunkid " & _
    '                 ",ISNULL(hremployee_master.classgroupunkid,0) AS classgroupunkid " & _
    '                 ",ISNULL(hremployee_master.classunkid,0) AS classunkid " & _
    '                 ",ISNULL(hremployee_master.jobgroupunkid,0) AS jobgroupunkid " & _
    '                 ",ISNULL(hremployee_master.gradegroupunkid,0) AS gradegroupunkid " & _
    '                 ",ISNULL(hremployee_master.gradeunkid,0) AS gradeunkid " & _
    '                 ",ISNULL(hremployee_master.gradelevelunkid,0) AS gradelevelunkid "

    '        strQ &= "FROM lvemployee_holiday " & _
    '         " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = lvemployee_holiday.employeeunkid " & _
    '         " JOIN lvholiday_master on lvholiday_master.holidayunkid = lvemployee_holiday.holidayunkid "


    '        If strUserAccessLevelFilterString = "" Then
    '            strQ &= UserAccessLevel._AccessLevelFilterString
    '        Else
    '            strQ &= strUserAccessLevelFilterString
    '                End If

    '        If intEmpId > 0 Then
    '            strQ &= " AND lvemployee_holiday.employeeunkid = " & intEmpId
    '        End If


    '        If dtPeriodStart <> Nothing AndAlso dtPeriodEnd <> Nothing Then
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodStart))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
    '        End If


    '        'Pinkal (06-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        strQ &= " AND hremployee_master.isapproved = 1 "
    '        'Pinkal (06-Feb-2013) -- End



    '        'Pinkal (06-May-2014) -- Start
    '        'Enhancement : TRA Changes
    '        If mstrFilter.Trim.Length > 0 Then
    '            strQ &= " AND " & mstrFilter
    '        End If
    '        'Pinkal (06-May-2014) -- End



    '        strQ &= " ORDER BY name "
    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                            , ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal xYearUnkid As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                            , Optional ByVal intEmpId As Integer = -1 _
                            , Optional ByVal objDataOperation As clsDataOperation = Nothing _
                            , Optional ByVal strUserAccessLevelFilterString As String = "" _
                            , Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            strQ = "SELECT " & _
              "  holidaytranunkid " & _
              ", lvemployee_holiday.employeeunkid " & _
              ", isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') as name" & _
              ", lvemployee_holiday.holidayunkid " & _
              ", lvholiday_master.holidayname " & _
              ", convert(char(8),lvholiday_master.holidaydate,112) as holidaydate " & _
              ", Remarks " & _
              ", lvholiday_master.color " & _
                     ", Userunkid " & _
                     ",ISNULL(Alloc.sectiongroupunkid,0) AS sectiongroupunkid " & _
                     ",ISNULL(Alloc.unitgroupunkid,0) AS unitgroupunkid " & _
                     ",ISNULL(Alloc.teamunkid,0) AS teamunkid " & _
                     ",ISNULL(Alloc.stationunkid,0) AS stationunkid " & _
                     ",ISNULL(Alloc.deptgroupunkid,0) AS deptgroupunkid " & _
                     ",ISNULL(Alloc.departmentunkid,0) AS departmentunkid " & _
                     ",ISNULL(Alloc.sectionunkid,0) AS sectionunkid " & _
                     ",ISNULL(Alloc.unitunkid,0) AS unitunkid " & _
                     ",ISNULL(Alloc.classgroupunkid,0) AS classgroupunkid " & _
                     ",ISNULL(Alloc.classunkid,0) AS classunkid " & _
                     ",ISNULL(Jobs.jobunkid,0) AS jobunkid " & _
                     ",ISNULL(Jobs.jobgroupunkid,0) AS jobgroupunkid " & _
                     ",ISNULL(Grds.gradegroupunkid,0) AS gradegroupunkid " & _
                     ",ISNULL(Grds.gradeunkid,0) AS gradeunkid " & _
                     ",ISNULL(Grds.gradelevelunkid,0) AS gradelevelunkid " & _
                     " FROM lvemployee_holiday " & _
             " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = lvemployee_holiday.employeeunkid " & _
                     " JOIN lvholiday_master on lvholiday_master.holidayunkid = lvemployee_holiday.holidayunkid " & _
                     " LEFT JOIN " & _
                     "( " & _
                     "    SELECT " & _
                     "         stationunkid " & _
                     "        ,deptgroupunkid " & _
                     "        ,departmentunkid " & _
                     "        ,sectiongroupunkid " & _
                     "        ,sectionunkid " & _
                     "        ,unitgroupunkid " & _
                     "        ,unitunkid " & _
                     "        ,teamunkid " & _
                     "        ,classgroupunkid " & _
                     "        ,classunkid " & _
                     "        ,employeeunkid " & _
                     "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                     "    FROM hremployee_transfer_tran " & _
                     "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobunkid " & _
                    "        ,jobgroupunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         gradegroupunkid " & _
                    "        ,gradeunkid " & _
                    "        ,gradelevelunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                    "    FROM prsalaryincrement_tran " & _
                    "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= " WHERE 1 = 1 "


            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry & " "
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If
            End If

            If intEmpId > 0 Then
                strQ &= " AND lvemployee_holiday.employeeunkid = " & intEmpId
            End If

            strQ &= " AND hremployee_master.isapproved = 1 "

            If mstrFilter.Trim.Length > 0 Then
                strQ &= mstrFilter
            End If

            strQ &= " ORDER BY name "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Pinkal (25-Jan-2018) -- Start
            'Bug - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.
            'objDataOperation = Nothing
            'Pinkal (25-Jan-2018) -- End
        End Try
        Return dsList
    End Function


    'Pinkal (24-Aug-2015) -- End


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvemployee_holiday) </purpose>
    Public Function InsertEmployeeHoliday(ByVal strEmpList As String, ByVal strHolidayList As String, ByVal strDatabaseName As String _
                                                            , ByVal mblnAllowOverTimeInBudgetTimesheet As Boolean, Optional ByVal IsImport As Boolean = False _
                                                            , Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean

        'Pinkal (28-Mar-2018) --  'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.[ByVal mblnAllowOverTimeInBudgetTimesheet As Boolean]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim strEmp As String() = Nothing
        Dim strHoliday As String() = Nothing

        Dim exForce As Exception

        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOpr
            objDataOperation.ClearParameters()
        End If

        Try

            strEmp = strEmpList.Split(",")
            strHoliday = strHolidayList.Split(",")

            If strEmp.Length > 0 And strHoliday.Length > 0 Then

                For i As Integer = 0 To strEmp.Length - 1
                    mintEmployeeunkid = CInt(strEmp.GetValue(i))

                    Dim dList As DataSet = clsCommonATLog.GetChildList(objDataOperation, "lvemployee_holiday", "employeeunkid", mintEmployeeunkid)
                    Dim dtDel() As DataRow = Nothing
                    If strHolidayList.Trim.Length > 0 AndAlso dList.Tables(0).Rows.Count > 0 Then
                        dtDel = dList.Tables(0).Select("holidayunkid NOT IN (" & strHolidayList & ") AND employeeunkid = " & mintEmployeeunkid)
                        If dtDel.Length > 0 Then
                            For k As Integer = 0 To dtDel.Length - 1

                                'STRAT FOR DELETE ALL RECORDS OF THE PARTICUALR EMPLOYEE IN EMPLOYEE HOLIDAY TABLE
                                'DeleteEmployeeHoliday(objDataOperation, mintEmployeeunkid, CInt(dtDel(k)("holidayunkid")))


                                'Pinkal (13-Jan-2015) -- Start
                                'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 
                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'If DeleteEmployeeHoliday(objDataOperation, mintEmployeeunkid, CInt(dtDel(k)("holidayunkid")), mintUserunkid) = False Then
                                If DeleteEmployeeHoliday(objDataOperation, mintEmployeeunkid, strDatabaseName, CInt(dtDel(k)("holidayunkid")), mintUserunkid) = False Then
                                    'Sohail (21 Aug 2015) -- End
                                    Continue For
                                End If
                                'Pinkal (13-Jan-2015) -- End

                                'END FOR DELETE ALL RECORDS OF THE PARTICUALR EMPLOYEE IN EMPLOYEE HOLIDAY TABLE

                                dList.Tables(0).Rows.Remove(dtDel(k))
                            Next
                            dList.AcceptChanges()
                        End If
                    End If


                    'STRAT FOR DELETE ALL RECORDS OF THE PARTICUALR EMPLOYEE IN EMPLOYEE HOLIDAY TABLE

                    'If IsImport = False Then DeleteEmployeeHoliday(objDataOperation, mintEmployeeunkid)

                    'END FOR DELETE ALL RECORDS OF THE PARTICUALR EMPLOYEE IN EMPLOYEE HOLIDAY TABLE

                    'Pinkal (12-Oct-2011) -- End


                    For j As Integer = 0 To strHoliday.Length - 1

                        If dList IsNot Nothing AndAlso dList.Tables(0).Rows.Count > 0 Then
                            dtDel = dList.Tables(0).Select("holidayunkid = " & CInt(strHoliday.GetValue(j)))
                            If dtDel.Length > 0 Then Continue For
                        End If

                        mintHolidayunkid = CInt(strHoliday.GetValue(j))
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@emp_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                        objDataOperation.AddParameter("@holidayunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHolidayunkid.ToString)
                        objDataOperation.AddParameter("@Remarks", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemarks.ToString)
                        objDataOperation.AddParameter("@Userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)

                        strQ = "INSERT INTO lvemployee_holiday ( " & _
                          "  employeeunkid " & _
                          ", holidayunkid " & _
                          ", Remarks " & _
                          ", Userunkid " & _
                        ") VALUES (" & _
                          "  @emp_unkid " & _
                          ", @holidayunkid " & _
                          ", @Remarks " & _
                          ", @Userunkid " & _
                        "); SELECT @@identity"

                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If dsList.Tables(0).Rows.Count > 0 Then mintHolidaytranunkid = dsList.Tables(0).Rows(0)(0)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "lvemployee_holiday", "holidaytranunkid", mintHolidaytranunkid, , mintUserunkid) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'Pinkal (28-Mar-2018) -- Start
                        'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.
                        If mblnAllowOverTimeInBudgetTimesheet = False Then
                            Dim mintEmpTimesheetID As Integer = 0
                            Dim objHoliday As New clsholiday_master
                            objHoliday._Holidayunkid(objDataOperation) = CInt(strHoliday.GetValue(j))
                            strQ = "SELECT ISNULL(emptimesheetunkid,0) AS emptimesheetunkid FROM ltbemployee_timesheet WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND CONVERT(CHAR(8),activitydate,112) = @activitydate AND issubmit_approval = 0"
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                            objDataOperation.AddParameter("@activitydate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objHoliday._Holidaydate))
                            Dim dsEmpTimesheeList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                            If dsEmpTimesheeList IsNot Nothing AndAlso dsEmpTimesheeList.Tables(0).Rows.Count > 0 Then
                                Dim objBgetTimesheet As New clsBudgetEmp_timesheet
                                For Each dr In dsEmpTimesheeList.Tables(0).Rows
                                    mintEmpTimesheetID = CInt(dr("emptimesheetunkid"))
                                    If mintEmpTimesheetID > 0 Then
                                        objBgetTimesheet._Voiduserunkid = mintUserunkid
                                        objBgetTimesheet._Voidreason = "On Holiday Delete Budget Timesheet Entry."
                                        If objBgetTimesheet.Delete(strDatabaseName, objHoliday._Holidaydate, mintEmpTimesheetID, objDataOperation) = False Then
                                            exForce = New Exception(objBgetTimesheet._Message)
                                            Throw exForce
                                        End If
                                    End If
                                Next
                                objBgetTimesheet = Nothing
                            End If

                            objHoliday = Nothing
                        End If
                        'Pinkal (28-Mar-2018) --  End

                    Next

                Next
            End If

            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            If objDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Pinkal (03-May-2019) -- End
            Return True
        Catch ex As Exception
            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            If objDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Pinkal (03-May-2019) -- End
            Throw New Exception(ex.Message & "; Procedure Name: InsertEmployeeHoliday; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            If objDataOpr Is Nothing Then objDataOperation = Nothing
            'Pinkal (03-May-2019) -- End
        End Try
    End Function


    'Pinkal (12-Oct-2011) -- Start
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvemployee_holiday) </purpose>
    Public Function DeleteEmployeeHoliday(ByVal objDataOperation As clsDataOperation, ByVal intEmployeeId As Integer, ByVal strDatabaseName As String, Optional ByVal intHolidayunkid As Integer = -1 _
                                          , Optional ByVal intUserUnkId As Integer = 0) As Boolean 'Sohail (23 Apr 2012) - [intUserUnkId]
        'Sohail (21 Aug 2015) - [strDatabaseName]
        'If isUsed(intUnkid) Then
        'mstrMessage = "<Message>"
        ' Return False
        ' End If

        Dim strQ As String = ""
        Dim exForce As Exception
        Try




            'Pinkal (13-Jan-2015) -- Start
            'Enhancement - CHANGES FOR VOLTAMP GIVE WARNING IN LEAVE & TNA TRANSACTION WHEN PAYMENT IS DONE AND PERIOD IS OPEN. 
            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
                Dim objHoliday As New clsholiday_master

                'S.SANDEEP [19 OCT 2016] -- START
                'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
                'objHoliday._Holidayunkid = intHolidayunkid
                objHoliday._Holidayunkid(objDataOperation) = intHolidayunkid
                'S.SANDEEP [19 OCT 2016] -- END

                Dim objMaster As New clsMasterData

                'S.SANDEEP [19 OCT 2016] -- START
                'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
                'Dim intPeriodId As Integer = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, objHoliday._Holidaydate.Date)
                Dim intPeriodId As Integer = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, objHoliday._Holidaydate.Date, , objDataOperation)
                'S.SANDEEP [19 OCT 2016] -- END

                Dim objPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = intPeriodId

                'S.SANDEEP [19 OCT 2016] -- START
                'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
                objPeriod._xDataOperation = objDataOperation
                'S.SANDEEP [19 OCT 2016] -- END


                objPeriod._Periodunkid(strDatabaseName) = intPeriodId
                'Sohail (21 Aug 2015) -- End
                If objPeriod._Statusid = 2 Then  'Close
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "You can't unassign some holiday(s) for these employee(s).Reason: Holiday dates are falling in a period which is already closed.")
                    Return False
                ElseIf objPeriod._Statusid = 1 Then  'Open

                    Dim mdtTnAStartdate As Date = Nothing
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If objPeriod._Periodunkid > 0 Then
                    'mdtTnAStartdate = objPeriod.GetTnA_StartDate(objPeriod._Periodunkid)
                    If objPeriod._Periodunkid(strDatabaseName) > 0 Then
                        'Sohail (21 Aug 2015) -- End

                        'S.SANDEEP [19 OCT 2016] -- START
                        'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
                        'mdtTnAStartdate = objPeriod.GetTnA_StartDate(objPeriod._Periodunkid(strDatabaseName))
                        mdtTnAStartdate = objPeriod.GetTnA_StartDate(objPeriod._Periodunkid(strDatabaseName), , , , objDataOperation)
                        'S.SANDEEP [19 OCT 2016] -- END

                    End If

                    Dim objPaymentTran As New clsPayment_tran

                    'S.SANDEEP [19 OCT 2016] -- START
                    'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
                    'If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, intEmployeeId, mdtTnAStartdate, intPeriodId, objHoliday._Holidaydate.Date) > 0 Then
                    If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, intEmployeeId, mdtTnAStartdate, intPeriodId, objHoliday._Holidaydate.Date, , objDataOperation) > 0 Then
                        'S.SANDEEP [19 OCT 2016] -- END
                        mstrMessage = Language.getMessage(mstrModuleName, 2, "Payroll payment for these employee(s) are already done for this tenure.you can't unassign some holiday(s) to these employee(s).")
                        Return False
                    End If
                End If
            End If
            'Pinkal (13-Jan-2015) -- End




            'strQ = "DELETE FROM lvemployee_holiday " & _
            '"WHERE employeeunkid = @employeeunkid "

            strQ = "Select ISNULL(holidaytranunkid,0) holidaytranunkid from lvemployee_holiday WHERE employeeunkid = @employeeunkid  "
            '
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)

            If intHolidayunkid > 0 Then
                strQ &= " AND holidayunkid = @holidayunkid"
                objDataOperation.AddParameter("@holidayunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intHolidayunkid)
            End If


            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")


            If dsList.Tables.Count > 0 AndAlso dsList.Tables(0).Rows.Count > 0 Then
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lvemployee_holiday", "holidaytranunkid", CInt(dsList.Tables(0).Rows(0)("holidaytranunkid")), , intUserUnkId) = False Then
                    'If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lvemployee_holiday", "holidaytranunkid", CInt(dsList.Tables(0).Rows(0)("holidaytranunkid"))) = False Then
                    'Sohail (23 Apr 2012) -- End
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


            strQ = "DELETE FROM lvemployee_holiday " & _
                       "WHERE employeeunkid = @employeeunkid  "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)

            If intHolidayunkid > 0 Then
                strQ &= " AND holidayunkid = @holidayunkid"
                objDataOperation.AddParameter("@holidayunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intHolidayunkid)
            End If

            objDataOperation.ExecNonQuery(strQ)


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DeleteEmployeeHoliday; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

    'Pinkal (12-Oct-2011) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hradvertise_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Employee Holiday. Reason: This Employee Holiday is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        'Pinkal (12-Oct-2011) -- End


        Try


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lvemployee_holiday", "holidaytranunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- End


            strQ = "DELETE FROM lvemployee_holiday " & _
            "WHERE holidaytranunkid = @holidaytranunkid "

            objDataOperation.AddParameter("@holidaytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@holidaytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeHoliday(ByVal intEmployeeid As Integer) As DataTable
        Dim dtList As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  holidaytranunkid " & _
              ", holidayunkid " & _
             "FROM lvemployee_holiday " & _
             "WHERE employeeunkid = @employeeunkid"

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeid.ToString)

            dtList = objDataOperation.ExecQuery(strQ, "List").Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dtList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeHoliday; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function


    'START FOR USED IMPORITNG HOLIDAY DATA OTHERWISE NOT TO USE

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function ImportDelete(ByVal intholidayunkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try


            strQ = "DELETE FROM lvemployee_holiday " & _
            "WHERE holidayunkid = @holidayunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@holidayunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intholidayunkid)
            objDataOperation.ExecNonQuery(strQ)


            strQ = "DELETE FROM lvholiday_master " & _
           "WHERE holidayunkid = @holidayunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@holidayunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intholidayunkid)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: ImportDelete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'END FOR USED IMPORITNG HOLIDAY DATA OTHERWISE NOT TO USE
    'Pinkal (26-Jul-2011) -- Start

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function GetEmployeeHoliday(ByVal Employeeunkid As Integer, ByVal Holidaydate As Date) As DataTable
        Dim dtHoliday As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            objDataOperation = New clsDataOperation
            strQ = "SELECT  holidaytranunkid " & _
                      ", lvemployee_holiday.holidayunkid  " & _
                      ", holidayname " & _
                      ", holidaydate " & _
                      ", color " & _
                      ", Employeeunkid " & _
                      " FROM lvemployee_holiday " & _
                      " JOIN lvholiday_master ON lvemployee_holiday.holidayunkid = lvholiday_master.holidayunkid " & _
                      " WHERE  lvemployee_holiday.Employeeunkid = @employeeunkid " & _
                      " AND  CONVERT(CHAR(8),holidaydate,112) = @holidaydate"

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Employeeunkid.ToString)
            objDataOperation.AddParameter("@holidaydate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(Holidaydate))

            dtHoliday = objDataOperation.ExecQuery(strQ, "List").Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeHoliday", mstrModuleName)
        End Try
        Return dtHoliday
    End Function



    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function GetEmployeeReCurrentHoliday(ByVal Employeeunkid As Integer, ByVal Holidaydate As Date) As Boolean
        Dim mblnFlag As Boolean = False
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            objDataOperation = New clsDataOperation



            'Pinkal (25-Nov-2014) -- Start
            'Enhancement -  CHANGES FOR AKFTZ REQUEST BY MR.ANDREW AND MR.RUTTA BOTH.


            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            'Issue : Previoulsy Count Only Recurrent Holiday for leave.Now Taking Recurrent and Once Effect Both Holiday type.

            strQ = "SELECT  holidaytranunkid " & _
                      ", lvemployee_holiday.holidayunkid  " & _
                      ", holidayname " & _
                      ", holidaydate " & _
                      ", color " & _
                      ", Employeeunkid " & _
                      " FROM lvemployee_holiday " & _
                      " JOIN lvholiday_master ON lvemployee_holiday.holidayunkid = lvholiday_master.holidayunkid AND lvholiday_master.holidaytypeid = 1 " & _
                      " WHERE  lvemployee_holiday.Employeeunkid = @employeeunkid " & _
                      " AND  RIGHT(CONVERT(CHAR(8),holidaydate,112),4) = @holidaydate"

            'strQ = "SELECT  holidaytranunkid " & _
            '          ", lvemployee_holiday.holidayunkid  " & _
            '          ", holidayname " & _
            '          ", holidaydate " & _
            '          ", color " & _
            '          ", Employeeunkid " & _
            '          " FROM lvemployee_holiday " & _
            '        " JOIN lvholiday_master ON lvemployee_holiday.holidayunkid = lvholiday_master.holidayunkid " & _
            '          " WHERE  lvemployee_holiday.Employeeunkid = @employeeunkid " & _
            '          " AND  RIGHT(CONVERT(CHAR(8),holidaydate,112),4) = @holidaydate"


            'Pinkal (21-Jul-2014) -- End

            'Pinkal (25-Nov-2014) -- End

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Employeeunkid.ToString)
            objDataOperation.AddParameter("@holidaydate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(Holidaydate).Substring(4))

            Dim dsHoliday As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsHoliday IsNot Nothing AndAlso dsHoliday.Tables(0).Rows.Count > 0 Then
                mblnFlag = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeReCurrentHoliday", mstrModuleName)
        End Try
        Return mblnFlag
    End Function


    'Pinkal (06-May-2014) -- End


    'Pinkal (01-Oct-2014) -- Start
    'Enhancement -  Changes For FDRC Report


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetEmployeeHolidayCount(ByVal intEmployeeid As Integer, Optional ByVal mdtStartDate As Date = Nothing, Optional ByVal mdtEndDate As Date = Nothing) As DataTable
    '    Dim dtList As DataTable = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation
    '    objDataOperation.ClearParameters()
    '    Try
    '        strQ = "SELECT " & _
    '                  " lvemployee_holiday.employeeunkid   " & _
    '                  ", count(*) AS TotalHoliday " & _
    '                  "  FROM lvemployee_holiday " & _
    '                  " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = lvemployee_holiday.employeeunkid " & _
    '                  " JOIN lvholiday_master on lvholiday_master.holidayunkid = lvemployee_holiday.holidayunkid " & _
    '                  " WHERE 1 = 1 "

    '        If intEmployeeid > 0 Then
    '            strQ &= " AND lvemployee_holiday.employeeunkid = @employeeunkid"
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeid.ToString)
    '        End If

    '        If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then

    '            strQ &= " AND CONVERT(char(8),lvholiday_master.holidaydate,112) >= @startdate AND CONVERT(char(8),lvholiday_master.holidaydate,112) <= @enddate "

    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " '

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
    '        End If

    '        strQ &= " Group by  lvemployee_holiday.employeeunkid  "

    '        dtList = objDataOperation.ExecQuery(strQ, "List").Tables(0)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        Return dtList
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeHolidayCount; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeHolidayCount(ByVal xDatabaseName As String _
                                                                 , ByVal xPeriodStart As DateTime _
                                                                 , ByVal xPeriodEnd As DateTime _
                                                                 , ByVal intEmployeeid As Integer) As DataTable
        Dim dtList As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Dim xDateJoinQry, xDateFilterQry As String
        xDateJoinQry = "" : xDateFilterQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

        Try
            strQ = "SELECT " & _
                      " lvemployee_holiday.employeeunkid   " & _
                      ", count(*) AS TotalHoliday " & _
                      "  FROM lvemployee_holiday " & _
                      " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = lvemployee_holiday.employeeunkid " & _
                      " JOIN lvholiday_master on lvholiday_master.holidayunkid = lvemployee_holiday.holidayunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If


            strQ &= " WHERE 1 = 1 "

            If intEmployeeid > 0 Then
                strQ &= " AND lvemployee_holiday.employeeunkid = @employeeunkid"
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeid.ToString)
            End If

            'If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then

            '    strQ &= " AND CONVERT(char(8),lvholiday_master.holidaydate,112) >= @startdate AND CONVERT(char(8),lvholiday_master.holidaydate,112) <= @enddate "

            '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '            " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " '

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
            'End If

            If xDateFilterQry.Trim.Length > 0 Then
                strQ &= xDateFilterQry & " "
            End If

            strQ &= " Group by  lvemployee_holiday.employeeunkid  "

            dtList = objDataOperation.ExecQuery(strQ, "List").Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dtList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeHolidayCount; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function


    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

    Public Function GetEmployeeHolidayList(ByVal xEmployeeID As Integer, ByVal xStartDate As DateTime, ByVal xEndDate As DateTime) As DataTable
        Dim dtList As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = " SELECT " & _
                      " lvemployee_holiday.employeeunkid   " & _
                      ",CONVERT(char(8),lvholiday_master.holidaydate,112) AS Date " & _
                      ",lvholiday_master.holidayname " & _
                      " FROM lvemployee_holiday " & _
                      " JOIN lvholiday_master on lvholiday_master.holidayunkid = lvemployee_holiday.holidayunkid " & _
                      " WHERE lvemployee_holiday.employeeunkid = @employeeunkid AND CONVERT(CHAR(8),lvholiday_master.holidaydate,112) BETWEEN @startDate AND @EndDate"


            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeID.ToString())
            objDataOperation.AddParameter("@StartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xStartDate))
            objDataOperation.AddParameter("@EndDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xEndDate))

            dtList = objDataOperation.ExecQuery(strQ, "List").Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dtList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeHolidayList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (28-Jul-2018) -- End




    'Pinkal (24-Aug-2015) -- End


    'Pinkal (01-Oct-2014) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "You can't unassign some holiday(s) for these employee(s).Reason: Holiday dates are falling in a period which is already closed.")
            Language.setMessage(mstrModuleName, 2, "Payroll payment for these employee(s) are already done for this tenure.you can't unassign some holiday(s) to these employee(s).")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class