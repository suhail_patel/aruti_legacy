﻿'************************************************************************************************************************************
'Class Name : clsleaveapprover_Tran.vb
'Purpose    :
'Date       :10/07/2010
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsleaveapprover_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsleaveapprover_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintApprovertranunkid As Integer = -1
    Private mintApproverunkid As Integer = -1
    Private mintUserunkid As Integer = -1
    Private mdtTran As DataTable
#End Region

#Region " Properties "
   

    ''' <summary>
    ''' Purpose: Get or Set approvertranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approvertranunkid() As Integer
        Get
            Return mintApprovertranunkid
        End Get
        Set(ByVal value As Integer)
            mintApprovertranunkid = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set approverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approverunkid() As Integer
        Get
            Return mintApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverunkid = value
        End Set
    End Property



    ''' <summary>
    ''' Purpose: Get or Set DataList
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property


    'S.SANDEEP [ 13 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property
    'S.SANDEEP [ 13 FEB 2013 ] -- END


#End Region

#Region " Constructor "

    Public Sub New()
        mdtTran = New DataTable("ApproverTran")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("leaveapprovertranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("approverunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("approvername")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)


            'Pinkal (23-oct-2010) -- START

            dCol = New DataColumn("name")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            'Pinkal (23-oct-2010) -- END

            dCol = New DataColumn("departmentunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("departmentname")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("sectionunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("sectionname")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("jobunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("jobname")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)


            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Sub GetApproverTran(Optional ByVal intApproverunkid As Integer = 0, Optional ByVal intDepartmenunkid As Integer = -1, Optional ByVal intSectionunkid As Integer = -1, Optional ByVal intJobunkid As Integer = -1, Optional ByVal objDataOperation1 As clsDataOperation = Nothing) 'S.SANDEEP [ 24 DEC 2012 ] -- START -- END
    '    'Public Sub GetApproverTran(Optional ByVal intApproverunkid As Integer = 0, Optional ByVal intDepartmenunkid As Integer = -1, Optional ByVal intSectionunkid As Integer = -1, Optional ByVal intJobunkid As Integer = -1)
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As clsDataOperation

    '    'S.SANDEEP [ 24 DEC 2012 ] -- START
    '    'ENHANCEMENT : TRA CHANGES
    '    If objDataOperation1 IsNot Nothing Then
    '        objDataOperation = objDataOperation1
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    'S.SANDEEP [ 24 DEC 2012 ] -- END


    '    Try


    '        'Pinkal (18-Dec-2012) -- Start
    '        'Enhancement : TRA Changes

    '        strQ = "SELECT " & _
    '          "  leaveapprovertranunkid " & _
    '          ", lvleaveapprover_tran.approverunkid " & _
    '          ", isnull(Emp1.firstname,'') + ' ' + isnull(Emp1.surname,'') as approvername" & _
    '          ", lvleaveapprover_tran.employeeunkid " & _
    '          ", isnull(Emp2.firstname,'') + ' ' + isnull(Emp2.surname,'') as name" & _
    '          ", Emp2.departmentunkid " & _
    '          ", hrdepartment_master.name as departmentname " & _
    '          ", Emp2.sectionunkid " & _
    '          ", hrsection_master.name as sectionname " & _
    '          ", Emp2.jobunkid " & _
    '          ", hrjob_master.job_name as jobname " & _
    '          ", '' as AUD ,'' as GUID " & _
    '          ", isnull(Emp2.employeecode,'')  employeecode " & _
    '          " FROM lvleaveapprover_tran " & _
    '          " LEFT JOIN lvleaveapprover_master on lvleaveapprover_master.approverunkid = lvleaveapprover_tran.approverunkid " & _
    '          " LEFT JOIN hremployee_master Emp1 on lvleaveapprover_master.leaveapproverunkid = Emp1.employeeunkid " & _
    '          " LEFT JOIN hremployee_master Emp2 on lvleaveapprover_tran.employeeunkid = Emp2.employeeunkid " & _
    '          " LEFT JOIN hrdepartment_master on hrdepartment_master.departmentunkid = Emp2.departmentunkid " & _
    '          " LEFT JOIN hrsection_master on Emp2.sectionunkid = hrsection_master.sectionunkid " & _
    '          " LEFT JOIN hrjob_master on hrjob_master.jobunkid = Emp2.jobunkid " & _
    '          " WHERE ISNULL(lvleaveapprover_tran.isvoid,0) = 0"


    '        'Pinkal (18-Dec-2012) -- End

    '        If intApproverunkid > 0 Then
    '            strQ &= " AND lvleaveapprover_tran.approverunkid = @approverunkid"
    '            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverunkid)
    '        End If


    '        'Pinkal (23-Jan-2015) -- Start
    '        'Enhancement - CHANGE FOR TRA WHEN MIGRATING 1 EMPLOYEE FROM OLD APPROVER TO NEW APPROVER.OLD APPROVER WILL BE VOID EVEN IF APPROVER HAD ACTIVE ASSIGNED EMPLOYEES.

    '        'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then

    '        '    strQ &= " AND CONVERT(CHAR(8),Emp1.appointeddate,112) <= @enddate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),Emp1.termination_from_date,112),@startdate) >= @startdate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),Emp1.termination_to_date,112),@startdate) >= @startdate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),Emp1.empl_enddate,112), @startdate) >= @startdate " & _
    '        '               " AND CONVERT(CHAR(8),Emp2.appointeddate,112) <= @enddate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),Emp2.termination_from_date,112),@startdate) >= @startdate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),Emp2.termination_to_date,112),@startdate) >= @startdate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),Emp2.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END
    '        '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

    '        'End If

    '        'Pinkal (23-Jan-2015) -- End

    '        'Pinkal (06-Feb-2013) -- Start
    '        'Enhancement : TRA Changes
    '        strQ &= " AND Emp1.isapproved = 1 AND Emp2.isapproved = 1 "
    '        'Pinkal (06-Feb-2013) -- End


    '        strQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "Emp1")
    '        strQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "Emp2")


    '        dsList = objDataOperation.ExecQuery(strQ, "List")
    '        mdtTran = dsList.Tables("List")
    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetApproverTran; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try

    'End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetApproverTran(ByVal xDatabaseName As String, ByVal mdtEmployeeAsonDate As Date _
                                             , Optional ByVal intApproverunkid As Integer = 0, Optional ByVal intDepartmenunkid As Integer = -1 _
                                             , Optional ByVal intSectionunkid As Integer = -1, Optional ByVal intJobunkid As Integer = -1 _
                                             , Optional ByVal mblnInlcudeInactiveEmployee As Boolean = False _
                                             , Optional ByVal objDataOperation1 As clsDataOperation = Nothing)

        'Pinkal (12-Oct-2020) -- Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.[ByVal xDatabaseName As String,Optional ByVal mblnInlcudeInactiveEmployee As Boolean = False]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If objDataOperation1 IsNot Nothing Then
            objDataOperation = objDataOperation1
        Else
            objDataOperation = New clsDataOperation
        End If

        'Pinkal (12-Oct-2020) -- Start
        'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
        Dim xDateJoinQry, xDateFilterQry As String
        xDateJoinQry = "" : xDateFilterQry = ""

        If mblnInlcudeInactiveEmployee = False Then
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsonDate, mdtEmployeeAsonDate, , , xDatabaseName, "Emp2")
        End If
        'Pinkal (12-Oct-2020) -- End


        Try

            strQ = "SELECT " & _
              "  leaveapprovertranunkid " & _
              ", lvleaveapprover_tran.approverunkid " & _
              ", '' as approvername" & _
              ", lvleaveapprover_tran.employeeunkid " & _
              ", isnull(Emp2.firstname,'') + ' ' + isnull(Emp2.surname,'') as name" & _
              ", Alloc.departmentunkid " & _
              ", hrdepartment_master.name as departmentname " & _
              ", Alloc.sectionunkid " & _
              ", hrsection_master.name as sectionname " & _
              ", Jobs.jobunkid " & _
              ", hrjob_master.job_name as jobname " & _
              ", '' as AUD ,'' as GUID " & _
              ", isnull(Emp2.employeecode,'')  employeecode " & _
              " FROM lvleaveapprover_tran " & _
              " LEFT JOIN lvleaveapprover_master on lvleaveapprover_master.approverunkid = lvleaveapprover_tran.approverunkid " & _
              " LEFT JOIN hremployee_master Emp2 on lvleaveapprover_tran.employeeunkid = Emp2.employeeunkid " & _
              " LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                    ") AS Alloc ON Alloc.employeeunkid = Emp2.employeeunkid AND Alloc.rno = 1 " & _
              " LEFT JOIN hrdepartment_master on hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
              " LEFT JOIN hrsection_master on hrsection_master.sectionunkid = Alloc.sectionunkid " & _
              " LEFT JOIN " & _
              " ( " & _
              "    SELECT " & _
              "         jobunkid " & _
              "        ,jobgroupunkid " & _
              "        ,employeeunkid " & _
              "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
              "    FROM hremployee_categorization_tran " & _
              "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
              " ) AS Jobs ON Jobs.employeeunkid = Emp2.employeeunkid AND Jobs.rno = 1 " & _
                      " LEFT JOIN hrjob_master on hrjob_master.jobunkid = Jobs.jobunkid "

            
            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            strQ &= " WHERE ISNULL(lvleaveapprover_tran.isvoid,0) = 0"


            If mblnInlcudeInactiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If
            End If

            'Pinkal (12-Oct-2020) -- End

            If intApproverunkid > 0 Then
                strQ &= " AND lvleaveapprover_tran.approverunkid = @approverunkid"
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverunkid)
            End If

            strQ &= " AND Emp2.isapproved = 1 "

            dsList = objDataOperation.ExecQuery(strQ, "List")
            mdtTran = dsList.Tables("List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverTran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()

            'Pinkal (30-Oct-2015) -- Start
            'Enhancement - WORKING ON TRA LEAVE APPROVER MIGRATION PROBLEM.
            If objDataOperation1 Is Nothing Then objDataOperation = Nothing
            'Pinkal (30-Oct-2015) -- End
        End Try

    End Sub


    'Pinkal (24-Aug-2015) -- End


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveapprover_tran) </purpose>
    Public Function InsertDelete_LeaveApproverData(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try

            If mdtTran Is Nothing Then Return True

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"


                                'Pinkal (18-Dec-2012) -- Start
                                'Enhancement : TRA Changes

                                'strQ = "Select count(*) as 'Countemp' From lvleaveapprover_tran " & _
                                '    " where approverunkid = @approverunkid AND employeeunkid = @empunkid "

                                strQ = "Select count(*) as 'Countemp' From lvleaveapprover_tran " & _
                                       " where approverunkid = @approverunkid AND employeeunkid = @empunkid AND lvleaveapprover_tran.isvoid = 0"

                                'Pinkal (18-Dec-2012) -- End

                                

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                                objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                Dim dscount As DataSet = objDataOperation.ExecQuery(strQ, "Count")
                                If Not dscount Is Nothing And CInt(dscount.Tables("Count").Rows(0)("Countemp")) > 0 Then Continue For

                                strQ = "INSERT INTO lvleaveapprover_tran ( " & _
                                            " approverunkid " & _
                                            ", employeeunkid " & _
                                            ", userunkid " & _
                                            ", isvoid " & _
                                            ", voiduserunkid " & _
                                        ") VALUES (" & _
                                            "  @approverunkid " & _
                                            ", @employeeunkid " & _
                                            ", @userunkid " & _
                                            ", @isvoid " & _
                                            ", @voiduserunkid " & _
                                            "); SELECT @@identity"

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)

                                'Pinkal (16-Feb-2011) -- Start
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                'Pinkal (16-Feb-2011) -- End

                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(0))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintApprovertranunkid = dsList.Tables(0).Rows(0).Item(0)



                                'Pinkal (06-Apr-2013) -- Start
                                'Enhancement : TRA Changes

                                'If .Item("approverunkid") > 0 Then
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveapprover_master", "approverunkid", .Item("approverunkid"), "lvleaveapprover_tran", "leaveapprovertranunkid", mintApprovertranunkid, 2, 1) = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'Else
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveapprover_master", "approverunkid", mintApproverunkid, "lvleaveapprover_tran", "leaveapprovertranunkid", mintApprovertranunkid, 1, 1) = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If

                                If .Item("approverunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveapprover_master", "approverunkid", .Item("approverunkid"), "lvleaveapprover_tran", "leaveapprovertranunkid", mintApprovertranunkid, 2, 1, False, mintUserunkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveapprover_master", "approverunkid", mintApproverunkid, "lvleaveapprover_tran", "leaveapprovertranunkid", mintApprovertranunkid, 1, 1, False, mintUserunkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                

                                'Pinkal (06-Apr-2013) -- End
                                
                            Case "D"


                                'Pinkal (19-APR-2012) -- Start
                                'Enhancement : TRA Changes


                                'START FOR GET LEAVE APPROVER TRAN UNK ID

                                'strQ = "Select isnull(leaveapprovertranunkid,0) as leaveapprovertranunkid From lvleaveapprover_tran " & _
                                '       " WHERE approverunkid = @approverunkid AND employeeunkid = @empunkid "

                                'objDataOperation.ClearParameters()
                                'objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                                'objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                'Dim dscount As DataSet = objDataOperation.ExecQuery(strQ, "List")
                                'If dscount.Tables("List").Rows.Count > 0 Then mintApprovertranunkid = CInt(dscount.Tables("List").Rows(0)("leaveapprovertranunkid"))

                                ''END FOR GET LEAVE APPROVER TRAN UNK ID


                                ''Pinkal (12-Oct-2011) -- Start
                                ''ENHANCEMENT : AUDIT TRAIL MAINTENANCE

                                'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveapprover_master", "approverunkid", mintApproverunkid, "lvleaveapprover_tran", "leaveapprovertranunkid", mintApprovertranunkid, 2, 3) = False Then
                                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '    Throw exForce
                                'End If


                                'strQ = "DELETE FROM lvleaveapprover_tran " & _
                                '            "WHERE approverunkid = @approverunkid AND employeeunkid = @empunkid "

                                'objDataOperation.ClearParameters()
                                'objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                                'objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                'Dim p As Integer = objDataOperation.ExecNonQuery(strQ)

                                strQ = " Update lvleaveapprover_tran set " & _
                                            " isvoid = 1,voiddatetime=@voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                                            " WHERE approverunkid = @approverunkid AND employeeunkid = @employeeunkid"

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 2, "UnAssign"))
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'Pinkal (06-Apr-2013) -- Start
                                'Enhancement : TRA Changes

                                'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveapprover_master", "approverunkid", mintApproverunkid, "lvleaveapprover_tran", "leaveapprovertranunkid", CInt(.Item("leaveapprovertranunkid").ToString), 2, 3) = False Then
                                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '    Throw exForce
                                'End If

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveapprover_master", "approverunkid", mintApproverunkid, "lvleaveapprover_tran", "leaveapprovertranunkid", CInt(.Item("leaveapprovertranunkid").ToString), 2, 3, False, mintUserunkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'Pinkal (06-Apr-2013) -- End

                                'Pinkal (19-APR-2012) -- End


                        End Select
                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            'Pinkal (30-Oct-2015) -- Start
            'Enhancement - WORKING ON TRA LEAVE APPROVER MIGRATION PROBLEM.
            'DisplayError.Show("-1", ex.Message, "InsertDelete_LeaveApproverData", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: InsertDelete_LeaveApproverData; Module Name: " & mstrModuleName)
            'Pinkal (30-Oct-2015) -- End


            Return False
        End Try
    End Function


    'Anjan (11 Jun 2011)-Start
    'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
    'Public Sub AuditInsert(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal intEmployeeunkid As Integer)
    Public Function AuditInsert(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal intEmployeeunkid As Integer) As Boolean
        'Anjan (11 Jun 2011)-End
        Dim strQ As String = ""
        Try
            objDataOperation.ClearParameters()
            strQ = "INSERT INTO atlvleaveapprover_tran ( " & _
                                          " leaveapprovertranunkid" & _
                                          ", approverunkid " & _
                                          ", employeeunkid " & _
                                          ", audittype " & _
                                          ", audituserunkid " & _
                                          ", auditdatetime " & _
                                          ", ip " & _
                                          ", machine_name " & _
                                      ") VALUES (" & _
                                          "  @leaveapprovertranunkid " & _
                                          ", @approverunkid " & _
                                          ", @employeeunkid " & _
                                          ", @audittype " & _
                                          ", @audituserunkid " & _
                                          ", @auditdatetime " & _
                                          ", @ip " & _
                                          ", @machine_name)"

            objDataOperation.AddParameter("@leaveapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertranunkid.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)

            'Pinkal (16-Feb-2011) -- Start
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            'Pinkal (16-Feb-2011) -- End

            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)
            objDataOperation.ExecNonQuery(strQ)

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            Return True
            'Anjan (11 Jun 2011)-End

        Catch ex As Exception
            'Pinkal (30-Oct-2015) -- Start
            'Enhancement - WORKING ON TRA LEAVE APPROVER MIGRATION PROBLEM.
            'DisplayError.Show("-1", ex.Message, "AuditInsert", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: AuditInsert; Module Name: " & mstrModuleName)
            'Pinkal (30-Oct-2015) -- End
        End Try
    End Function

   
#Region "Migration Approver"


    'Pinkal (12-Oct-2020) -- Start
    'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.

    'Public Function Migration_Insert(ByVal mdtEmployeeAsonDate As Date, ByVal dtTable As DataTable, ByVal intApproverMstID As Integer _
    '                                             , ByVal intLeaveapproverEmpId As Integer, ByVal mblnPaymentApprovalwithLeaveApproval As Boolean, _
    '                                             Optional ByVal intUserId As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

    Public Function Migration_Insert(ByVal xDataBaseName As String, ByVal mdtEmployeeAsonDate As Date, ByVal dtTable As DataTable, ByVal intApproverMstID As Integer _
                                               , ByVal intLeaveapproverEmpId As Integer, ByVal mblnPaymentApprovalwithLeaveApproval As Boolean _
                                               , ByVal xIncludeInActiveEmp As Boolean, Optional ByVal intUserId As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        'Pinkal (12-Oct-2020) -- End


        'Gajanan [23-SEP-2019] -- Add {xDataOpr}    

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mintApproverId As Integer = -1
        Dim mstrEmployeeID As String = ""
        Try


            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.

            'Dim objDataOperation As New clsDataOperation
            Dim objDataOperation As clsDataOperation

            'objDataOperation.BindTransaction()
            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            End If
            'Gajanan [23-SEP-2019] -- End


            If dtTable Is Nothing Then Return True

            If intUserId <= 0 Then
                intUserId = User._Object._Userunkid
            End If

            For i = 0 To dtTable.Rows.Count - 1

                mintApproverId = CInt(dtTable.Rows(i)("approverunkid"))

                strQ = " Update lvleaveapprover_tran set " & _
                          " isvoid = 1,voiddatetime=@voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                          " WHERE approverunkid = @approverunkid AND employeeunkid = @employeeunkid"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtTable.Rows(i)("approverunkid").ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtTable.Rows(i)("employeeunkid").ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(intUserId <= 0, User._Object._Userunkid, intUserId))
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 1, "Migration"))
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveapprover_master", "approverunkid", CInt(dtTable.Rows(i)("approverunkid").ToString), "lvleaveapprover_tran", "leaveapprovertranunkid", CInt(dtTable.Rows(i)("leaveapprovertranunkid").ToString), 2, 3, False, intUserId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                strQ = " SELECT ISNULL(employeeunkid,0) employeeunkid FROM lvleaveapprover_tran WHERE approverunkid = @approverunkid AND employeeunkid = @employeeunkid AND isvoid = 0 "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverMstID)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtTable.Rows(i)("employeeunkid").ToString)
                Dim dtEmpCount As DataSet = objDataOperation.ExecQuery(strQ, "List")
                If dtEmpCount.Tables(0).Rows.Count > 0 Then
                    If CInt(dtEmpCount.Tables(0).Rows(0)("employeeunkid")) > 0 Then
                        mstrEmployeeID &= dtTable.Rows(i)("employeeunkid").ToString() & ","
                        Continue For
                    End If
                End If

                strQ = "INSERT INTO lvleaveapprover_tran ( " & _
                                          " approverunkid " & _
                                          ", employeeunkid " & _
                                          ", userunkid " & _
                                          ", isvoid " & _
                                          ", voiduserunkid " & _
                                      ") VALUES (" & _
                                          "  @approverunkid " & _
                                          ", @employeeunkid " & _
                                          ", @userunkid " & _
                                          ", @isvoid " & _
                                          ", @voiduserunkid " & _
                                          "); SELECT @@identity"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverMstID)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtTable.Rows(i)("employeeunkid").ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(intUserId <= 0, User._Object._Userunkid, intUserId))
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim mintApprovertranunkid As Integer = dsList.Tables(0).Rows(0).Item(0)
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveapprover_master", "approverunkid", intApproverMstID, "lvleaveapprover_tran", "leaveapprovertranunkid", mintApprovertranunkid, 2, 1, False, intUserId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim objLeavForm As New clsleaveform
                Dim mstrFormId As String = objLeavForm.GetApproverPendingLeaveForm(mintApproverId, dtTable.Rows(i)("employeeunkid").ToString, objDataOperation)

                If mstrFormId.Trim.Length > 0 Then

                    'Pinkal (01-Apr-2020) -- Start
                    'ENHANCEMENT NMB:  Working on Shift Master changes and Implementing in OT Requisition module. 
                    'strQ = " Update lvpendingleave_tran set approverunkid  = @newapproverunkid,approvertranunkid = @newapprovertranunkid  WHERE formunkid in (" & mstrFormId & ") AND approvertranunkid = @approvertranunkid AND lvpendingleave_tran.isvoid = 0 "
                    strQ = " Update lvpendingleave_tran set approverunkid  = @newapproverunkid,approvertranunkid = @newapprovertranunkid  WHERE formunkid in (" & mstrFormId & ") AND approvertranunkid = @approvertranunkid AND lvpendingleave_tran.isvoid = 0 AND statusunkid = 2 "
                    'Pinkal (01-Apr-2020) -- End


                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@newapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveapproverEmpId)
                    objDataOperation.AddParameter("@newapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverMstID)
                    objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverId)
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    strQ = "Select ISNULL(pendingleavetranunkid,0) pendingleavetranunkid FROM lvpendingleave_tran WHERE approverunkid = @approverunkid AND approvertranunkid = @approvertranunkid AND formunkid  in (" & mstrFormId & ") AND isvoid = 0"
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveapproverEmpId)
                    objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverMstID)
                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If dsList.Tables(0).Rows.Count > 0 Then
                        For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lvpendingleave_tran", "pendingleavetranunkid", dsList.Tables(0).Rows(k)("pendingleavetranunkid").ToString(), False) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                        Next
                    End If


                Dim arFormId As String() = mstrFormId.Trim.Split(",")

                If arFormId.Length > 0 Then

                    For iDay As Integer = 0 To arFormId.Length - 1
                            'Pinkal (30-Oct-2015) -- Start
                            'Enhancement - WORKING ON TRA LEAVE APPROVER MIGRATION PROBLEM.
                            objLeavForm._DoOperation = objDataOperation
                            'Pinkal (30-Oct-2015) -- End

                        objLeavForm._Formunkid = CInt(arFormId(iDay))


                            'Pinkal (01-Mar-2016) -- Start
                            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                            'If objLeavForm._Statusunkid <> 1 Then Continue For
                            If (objLeavForm._Statusunkid = 6 OrElse objLeavForm._Statusunkid = 7) Then Continue For
                            'Pinkal (01-Mar-2016) -- End 


                            'Pinkal (04-Oct-2017) -- Start
                            'Bug - Problem solving For TRA Leave Process List Issue.
                            'strQ = " UPDATE lvleaveday_fraction SET approverunkid = @approverunkid WHERE approverunkid > 0 AND formunkid = " & CInt(arFormId(iDay))
                            strQ = " UPDATE lvleaveday_fraction SET approverunkid = @approverunkid WHERE approverunkid > 0 AND approverunkid = @oldapproverunkid AND formunkid = " & CInt(arFormId(iDay))
                        objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveapproverEmpId)
                            objDataOperation.AddParameter("@oldapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtTable.Rows(i)("leaveapproverunkid").ToString()))
                            'Pinkal (04-Oct-2017) -- End
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                        strQ = "Select ISNULL(dayfractionunkid,0) AS dayfractionunkid  FROM lvleaveday_fraction WHERE approverunkid = @approverunkid AND formunkid  = " & CInt(arFormId(iDay)) & " AND isvoid = 0"
                        objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveapproverEmpId)
                        Dim dsFractionList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If dsFractionList IsNot Nothing AndAlso dsFractionList.Tables(0).Rows.Count > 0 Then
                            For k As Integer = 0 To dsFractionList.Tables(0).Rows.Count - 1

                                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lvleaveday_fraction", "dayfractionunkid", dsFractionList.Tables(0).Rows(k)("dayfractionunkid").ToString(), False) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                Next

                            End If

                            Next

                        End If

                End If


                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                If mblnPaymentApprovalwithLeaveApproval Then

                    Dim mstrClaimFormId As String = ""
                    Dim dsClaimList As DataSet = Nothing

                    strQ = " SELECT ISNULL(STUFF( " & _
                              " (SELECT  DISTINCT ',' +  CAST(cmclaim_request_master.crmasterunkid AS NVARCHAR(max)) " & _
                              "   FROM cmclaim_request_master " & _
                              "   JOIN cmclaim_approval_tran ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid  " & _
                              "  WHERE cmclaim_request_master.statusunkid in (2) AND cmclaim_request_master.employeeunkid IN (" & dtTable.Rows(i)("employeeunkid").ToString & ")" & _
                              " AND cmclaim_request_master.isvoid = 0  AND cmclaim_approval_tran.isvoid = 0 AND modulerefunkid = " & enModuleReference.Leave & _
                              "  FOR XML PATH('')),1,1,''),'') AS CSV"

                    objDataOperation.ClearParameters()
                    dsClaimList = objDataOperation.ExecQuery(strQ, "FormList")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    mstrClaimFormId = dsClaimList.Tables(0).Rows(0)("CSV").ToString()


                    If mstrClaimFormId.Trim.Length > 0 Then

                        strQ = " Update cmclaim_approval_tran set approveremployeeunkid  = @newempapproverunkid,crapproverunkid = @newcrapproverunkid " & _
                                  " WHERE crmasterunkid in (" & mstrClaimFormId & ") AND crapproverunkid = @crapproverunkid AND cmclaim_approval_tran.isvoid = 0 "
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@newempapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveapproverEmpId)
                        objDataOperation.AddParameter("@newcrapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverMstID)
                        objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverId)
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                        strQ = "Select ISNULL(crapprovaltranunkid,0) crapprovaltranunkid FROM cmclaim_approval_tran " & _
                                  " WHERE approveremployeeunkid = @approveremployeeunkid AND crapproverunkid = @newcrapproverunkid AND crmasterunkid  in (" & mstrClaimFormId & ") AND isvoid = 0"
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveapproverEmpId)
                        objDataOperation.AddParameter("@newcrapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverMstID)
                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If dsList.Tables(0).Rows.Count > 0 Then
                            For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "cmclaim_approval_tran", "crapprovaltranunkid", dsList.Tables(0).Rows(k)("crapprovaltranunkid").ToString(), False) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                    Next
                        End If

                End If
                End If

                'Pinkal (01-Mar-2016) -- End



                mstrEmployeeID &= dtTable.Rows(i)("employeeunkid").ToString() & ","
            Next

            If mstrEmployeeID.Trim.Length > 0 Then
                mstrEmployeeID = mstrEmployeeID.Substring(0, mstrEmployeeID.Trim.Length - 1)
            End If

            Dim objMigration As New clsMigration
            objMigration._Migrationdate = ConfigParameter._Object._CurrentDateAndTime
            objMigration._Migrationfromunkid = mintApproverId
            objMigration._Migrationtounkid = intApproverMstID
            objMigration._Usertypeid = enUserType.Approver
            objMigration._Userunkid = IIf(intUserId <= 0, User._Object._Userunkid, intUserId)
            objMigration._Migratedemployees = mstrEmployeeID

            If objMigration.Insert(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
            'GetApproverTran(mdtEmployeeAsonDate, mintApproverId, -1, -1, -1, objDataOperation)
            GetApproverTran(xDataBaseName, mdtEmployeeAsonDate, mintApproverId, -1, -1, -1, xIncludeInActiveEmp, objDataOperation)
            'Pinkal (12-Oct-2020) -- End


            If mdtTran.Rows.Count <= 0 Then

                strQ = " Update lvleaveapprover_master set " & _
                   " isvoid = 1,voiddatetime=@voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                   " WHERE approverunkid = @approverunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverId)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(intUserId <= 0, User._Object._Userunkid, intUserId))
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 1, "Migration"))
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveapprover_master", "approverunkid", mintApproverId, "lvleaveapprover_tran", "leaveapprovertranunkid", -1, 3, 3, False, intUserId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strQ = "SELECT ISNULL(mappingunkid,0) mappingunkid FROM hrapprover_usermapping WHERE approverunkid = @approverunkid and Usertypeid =" & enUserType.Approver
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverId)
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrapprover_usermapping", "mappingunkid", CInt(dsList.Tables(0).Rows(i)("mappingunkid")), False, intUserId) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

                strQ = "Delete from hrapprover_usermapping WHERE approverunkid = @approverunkid and Usertypeid =" & enUserType.Approver
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverId)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim objLeaveTypeMapping As New clsapprover_leavetype_mapping
                objLeaveTypeMapping._Isvoid = True
                objLeaveTypeMapping._Voiduserunkid = intUserId
                objLeaveTypeMapping._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objLeaveTypeMapping._Voidreason = Language.getMessage(mstrModuleName, 1, "Migration")
                If objLeaveTypeMapping.Delete(objDataOperation, mintApproverId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


            End If


            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'Gajanan [23-SEP-2019] -- End

            Return True
        Catch ex As Exception
            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'objDataOperation.ReleaseTransaction(False)
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan [23-SEP-2019] -- End

            'Pinkal (30-Oct-2015) -- Start
            'Enhancement - WORKING ON TRA LEAVE APPROVER MIGRATION PROBLEM.
            ' DisplayError.Show("-1", ex.Message, "Migration_Insert", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: Migration_Insert; Module Name: " & mstrModuleName)
            'Pinkal (30-Oct-2015) -- End

        End Try

    End Function



    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
    Public Function GetApproverTranIdFromEmployeeAndApprover(ByVal ApproverId As Integer, ByVal EmployeeId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = " SELECT ISNULL(leaveapprovertranunkid,0) as leaveapprovertranunkid FROM lvleaveapprover_tran WHERE approverunkid = @approverunkid AND employeeunkid = @employeeunkid AND isvoid = 0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, ApproverId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, EmployeeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables("List").Rows(0)("leaveapprovertranunkid").ToString())
            End If

            Return 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverTranIdFromEmployeeAndApprover; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try



    End Function
    'Gajanan [23-SEP-2019] -- End

#End Region


    'Pinkal (06-Feb-2013) -- Start
    'Enhancement : TRA Changes

#Region " Import Employee With Approver"

    Public Function ImportInsertEmployee(ByVal mstrEmployeeId As String, ByRef intApprTranId As Integer) As Boolean
        Dim strQ As String = String.Empty
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Try
            mstrMessage = ""
            Dim objDataOperation As New clsDataOperation

            If mstrEmployeeId.Trim.Length <= 0 Then Return True

            strQ = "SELECT leaveapprovertranunkid AS Id FROM lvleaveapprover_tran WHERE approverunkid = '" & mintApproverunkid & "' AND employeeunkid = '" & CInt(mstrEmployeeId) & "' "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                mstrMessage = objDataOperation.ErrorMessage
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intApprTranId = dsList.Tables(0).Rows(0).Item("Id")
                Return False
            End If

            strQ = "INSERT INTO lvleaveapprover_tran ( " & _
                              " approverunkid " & _
                              ", employeeunkid " & _
                              ", userunkid " & _
                              ", isvoid " & _
                              ", voiduserunkid " & _
                          ") VALUES (" & _
                              "  @approverunkid " & _
                              ", @employeeunkid " & _
                              ", @userunkid " & _
                              ", @isvoid " & _
                              ", @voiduserunkid " & _
                              "); SELECT @@identity"


            Dim arEmployee() As String = mstrEmployeeId.Trim.Split(",")

            For i As Integer = 0 To arEmployee.Length - 1

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, arEmployee(i).ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                dsList = objDataOperation.ExecQuery(strQ, "List")


                If objDataOperation.ErrorMessage <> "" Then
                    mstrMessage = objDataOperation.ErrorMessage
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintApprovertranunkid = dsList.Tables(0).Rows(0).Item(0)

                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveapprover_master", "approverunkid", mintApproverunkid, "lvleaveapprover_tran", "leaveapprovertranunkid", mintApprovertranunkid, 1, 1) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

            Return True

        Catch ex As Exception

            'Pinkal (30-Oct-2015) -- Start
            'Enhancement - WORKING ON TRA LEAVE APPROVER MIGRATION PROBLEM.
            'DisplayError.Show("-1", ex.Message, "ImportInsertEmloyee", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: ImportInsertEmloyee; Module Name: " & mstrModuleName)
            'Pinkal (30-Oct-2015) -- End
            Return False
        End Try
    End Function

#End Region

    'Pinkal (06-Feb-2013) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Migration")
			Language.setMessage(mstrModuleName, 2, "UnAssign")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class