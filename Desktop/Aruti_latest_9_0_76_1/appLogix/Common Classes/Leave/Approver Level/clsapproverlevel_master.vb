﻿'************************************************************************************************************************************
'Class Name : clsapproverlevel_master.vb
'Purpose    : All Approver Level Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :25/06/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 4
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsapproverlevel_master


    Private Shared ReadOnly mstrModuleName As String = "clsapproverlevel_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLevelunkid As Integer
    Private mstrLevelcode As String = String.Empty
    Private mstrLevelname As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mintPriority As Integer
    Private mblnIsmandatory As Boolean
    Private mstrLevelname1 As String = String.Empty
    Private mstrLevelname2 As String = String.Empty

    'Pinkal (26-Oct-2015) -- Start
    'Enhancement - Working on TRA Claim & Request Report Issue.
    Dim objDoOperation As clsDataOperation
    'Pinkal (26-Oct-2015) -- End

    'Pinkal (01-Apr-2019) -- Start
    'Enhancement - Working on Leave Changes for NMB.
    Private mintEscalation_Days As Integer = 0
    'Pinkal (01-Apr-2019) -- End
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelcode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelcode() As String
        Get
            Return mstrLevelcode
        End Get
        Set(ByVal value As String)
            mstrLevelcode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelname() As String
        Get
            Return mstrLevelname
        End Get
        Set(ByVal value As String)
            mstrLevelname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priority
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ismandatory
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ismandatory() As Boolean
        Get
            Return mblnIsmandatory
        End Get
        Set(ByVal value As Boolean)
            mblnIsmandatory = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelname1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelname1() As String
        Get
            Return mstrLevelname1
        End Get
        Set(ByVal value As String)
            mstrLevelname1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelname2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelname2() As String
        Get
            Return mstrLevelname2
        End Get
        Set(ByVal value As String)
            mstrLevelname2 = value
        End Set
    End Property


    'Pinkal (26-Oct-2015) -- Start
    'Enhancement - Working on TRA Claim & Request Report Issue.

    Public WriteOnly Property _DoOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDoOperation = value
        End Set
    End Property

    'Pinkal (26-Oct-2015) -- End.


    'Pinkal (01-Apr-2019) -- Start
    'Enhancement - Working on Leave Changes for NMB.

    ''' <summary>
    ''' Purpose: Get or Set Escalation_Days
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Escalation_Days() As Integer
        Get
            Return mintEscalation_Days
        End Get
        Set(ByVal value As Integer)
            mintEscalation_Days = value
        End Set
    End Property

    'Pinkal (01-Apr-2019) -- End
#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (26-Oct-2015) -- Start
        'Enhancement - Working on TRA Claim & Request Report Issue.
        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()
        'Pinkal (26-Oct-2015) -- End.



        Try
            strQ = "SELECT " & _
              "  levelunkid " & _
              ", levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", ismandatory " & _
              ", isactive " & _
              ", levelname1 " & _
              ", levelname2 " & _
                      ", levelname2 " & _
                      ", ISNULL(escalation_days,0) AS escalation_days " & _
             "FROM lvapproverlevel_master " & _
             "WHERE levelunkid = @levelunkid "

            'Pinkal (01-Apr-2019) --'Enhancement - Working on Leave Changes for NMB.[", ISNULL(escalation_days,0) AS escalation_days " & _]


            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mstrLevelcode = dtRow.Item("levelcode").ToString
                mstrLevelname = dtRow.Item("levelname").ToString
                If dtRow.Item("priority") IsNot DBNull.Value Then mintPriority = dtRow.Item("priority")
                If dtRow.Item("ismandatory") IsNot DBNull.Value Then mblnIsmandatory = dtRow.Item("ismandatory")
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrLevelname1 = dtRow.Item("levelname1").ToString
                mstrLevelname2 = dtRow.Item("levelname2").ToString

                'Pinkal (01-Apr-2019) -- Start
                'Enhancement - Working on Leave Changes for NMB.
                mintEscalation_Days = CInt(dtRow.Item("escalation_days"))
                'Pinkal (01-Apr-2019) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Pinkal (26-Oct-2015) -- Start
            'Enhancement - Working on TRA Claim & Request Report Issue.
            If objDoOperation Is Nothing Then objDataOperation = Nothing
            'Pinkal (26-Oct-2015) -- End.
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  levelunkid " & _
              ", levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", ismandatory " & _
              ", isactive " & _
              ", levelname1 " & _
              ", levelname2 " & _
                      ", ISNULL(escalation_days,0) AS escalation_days " & _
             "FROM lvapproverlevel_master "

            'Pinkal (01-Apr-2019) -- 'Enhancement - Working on Leave Changes for NMB.[", ISNULL(escalation_days,0) AS escalation_days " & _]

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvapproverlevel_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrLevelcode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Return False
        ElseIf isExist("", mstrLevelname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Return False
        ElseIf isPriorityExist(mintPriority) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Approver Level Priority is already assigned. Please assign new Approver Level Priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelcode.ToString)
            objDataOperation.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@ismandatory", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsmandatory.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@levelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname1.ToString)
            objDataOperation.AddParameter("@levelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname2.ToString)


            'Pinkal (01-Apr-2019) -- Start
            'Enhancement - Working on Leave Changes for NMB.
            objDataOperation.AddParameter("@escalation_days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEscalation_Days.ToString)
            'Pinkal (01-Apr-2019) -- End


            strQ = "INSERT INTO lvapproverlevel_master ( " & _
              "  levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", ismandatory " & _
              ", isactive " & _
              ", levelname1 " & _
              ", levelname2" & _
                      ", escalation_days" & _
            ") VALUES (" & _
              "  @levelcode " & _
              ", @levelname " & _
              ", @priority " & _
              ", @ismandatory " & _
              ", @isactive " & _
              ", @levelname1 " & _
              ", @levelname2" & _
                      ", @escalation_days" & _
            "); SELECT @@identity"


            'Pinkal (01-Apr-2019) -- 'Enhancement - Working on Leave Changes for NMB.[", escalation_days" & ]


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLevelunkid = dsList.Tables(0).Rows(0).Item(0)

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "lvapproverlevel_master", "levelunkid", mintLevelunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lvapproverlevel_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrLevelcode, "", mintLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Return False
        ElseIf isExist("", mstrLevelname, mintLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Return False
        ElseIf isPriorityExist(mintPriority, mintLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Approver Level Priority is already assigned. Please assign new Approver Level Priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelcode.ToString)
            objDataOperation.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@ismandatory", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsmandatory.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@levelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname1.ToString)
            objDataOperation.AddParameter("@levelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname2.ToString)

            'Pinkal (01-Apr-2019) -- Start
            'Enhancement - Working on Leave Changes for NMB.
            objDataOperation.AddParameter("@escalation_days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEscalation_Days.ToString)
            'Pinkal (01-Apr-2019) -- End

            strQ = "UPDATE lvapproverlevel_master SET " & _
              "  levelcode = @levelcode " & _
              ", levelname = @levelname " & _
              ", priority  = @priority  " & _
              ", ismandatory = @ismandatory " & _
              ", isactive = @isactive" & _
              ", levelname1 = @levelname1" & _
              ", levelname2 = @levelname2 " & _
              ", escalation_days = @escalation_days " & _
              " WHERE levelunkid = @levelunkid "


            'Pinkal (01-Apr-2019) --  'Enhancement - Working on Leave Changes for NMB.[", escalation_days = @escalation_days " & _]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "lvapproverlevel_master", mintLevelunkid, "levelunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lvapproverlevel_master", "levelunkid", mintLevelunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End


            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvapproverlevel_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, You cannot delete selected Approver Level. Reason: This Approver Level is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            strQ = "Update lvapproverlevel_master set isactive = 0 " & _
            "WHERE levelunkid = @levelunkid "

            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lvapproverlevel_master", "levelunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "select isnull(levelunkid,0) from lvleaveapprover_master WHERE levelunkid = @levelunkid"
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  levelunkid " & _
              ", levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", ismandatory " & _
              ", isactive " & _
              ", levelname1 " & _
              ", levelname2 " & _
                      ", ISNULL(escalation_days,0) AS escalation_days " & _
             "FROM lvapproverlevel_master " & _
             "WHERE 1=1 "

            'Pinkal (01-Apr-2019) -- 'Enhancement - Working on Leave Changes for NMB.[", ISNULL(escalation_days,0) AS escalation_days " & _]

            If strCode.Length > 0 Then
                strQ &= " AND levelcode = @code "
            End If
            If strName.Length > 0 Then
                strQ &= " AND levelname = @name "
            End If

            If intUnkid > 0 Then
                strQ &= " AND levelunkid <> @levelunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isPriorityExist(ByVal mintPriority As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  levelunkid " & _
              ", levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", ismandatory " & _
              ", isactive " & _
              ", levelname1 " & _
              ", levelname2 " & _
                      ", ISNULL(escalation_days,0) AS escalation_days " & _
                      " FROM lvapproverlevel_master " & _
                      " WHERE priority = @priority"

            'Pinkal (01-Apr-2019) -- 'Enhancement - Working on Leave Changes for NMB.[", ISNULL(escalation_days,0) AS escalation_days " & _]


            If intUnkid > 0 Then
                strQ &= " AND levelunkid <> @levelunkid"
            End If

            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isPriorityExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as levelunkid, ' ' +  @name  as name UNION "
            End If
            strQ &= "SELECT levelunkid, levelname as name FROM lvapproverlevel_master where isactive = 1 ORDER BY name "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetApproverLevelUnkId(ByVal strLevelName As String, Optional ByVal strLevelCode As String = "") As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "Select isnull(levelunkid,0) levelunkid from lvapproverlevel_master" & _
                       " where 1 = 1 "

            If strLevelName <> "" And strLevelCode = "" Then

                strQ &= " AND levelname = @levelname"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strLevelName)

            ElseIf strLevelCode <> "" And strLevelName = "" Then

                strQ &= " AND levelcode = @levelcode"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strLevelCode)

            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("levelunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverLevelUnkId", mstrModuleName)
        End Try
        Return -1
    End Function


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Language.setMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Language.setMessage(mstrModuleName, 3, "This Approver Level Priority is already assigned. Please assign new Approver Level Priority.")
            Language.setMessage(mstrModuleName, 4, "Select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class