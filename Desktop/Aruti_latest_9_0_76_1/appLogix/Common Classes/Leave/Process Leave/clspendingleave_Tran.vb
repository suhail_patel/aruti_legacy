﻿'************************************************************************************************************************************
'Class Name : clspendingleave_Tran.vb
'Purpose    :
'Date       :12/07/2010
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Web
Imports System

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clspendingleave_Tran
    Private Shared ReadOnly mstrModuleName As String = "clspendingleave_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintPendingleavetranunkid As Integer
    Private mintFormunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintApproverunkid As Integer
    Private mdtApprovaldate As Date
    Private mdtStartDate As Date
    Private mdtEnddate As Date
    Private mintTransactionheadunkid As Integer
    Private mintStatusunkid As Integer
    Private mdecAmount As Decimal
    Private mstrRemark As String = String.Empty
    Private mstrVoidreason As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mblnIsCancelForm As Boolean = False
    Private mintApproverTranunkid As Integer
    Private mdtExpense As DataTable
    Private mdtFraction As DataTable
    Private mdcDayFraction As Decimal = 0
    Private mstrWebFrmName As String = String.Empty
    Private mintClaimRequestMasterId As Integer = 0
    Private mblnPaymentApprovalwithLeaveApproval As Boolean = False
    Private mintYearId As Integer = -1
    Private mintVisibleId As Integer = -1
    Private mintCompanyId As Integer
    Private mstrArutiSelfServiceURL As String = ""
    Private mintLoginMode As enLogin_Mode
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""


    'Pinkal (10-Jan-2017) -- Start
    'Enhancement - Working on TRA C&R Module Changes with Leave Module.
    Private objClaimRMst As clsclaim_request_master = Nothing
    Private mdtClaimAttchment As DataTable = Nothing
    'Pinkal (10-Jan-2017) -- End


    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mintRelieverEmpunkid As Integer = 0
    'Pinkal (01-Oct-2018) -- End


    'Pinkal (01-Apr-2019) -- Start
    'Enhancement - Working on Leave Changes for NMB.
    Private mdtEscalationDate As DateTime = Nothing
    'Pinkal (01-Apr-2019) -- End


#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pendingleavetranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Pendingleavetranunkid() As Integer
        Get
            Return mintPendingleavetranunkid
        End Get
        Set(ByVal value As Integer)
            mintPendingleavetranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Formunkid() As Integer
        Get
            Return mintFormunkid
        End Get
        Set(ByVal value As Integer)
            mintFormunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approverunkid() As Integer
        Get
            Return mintApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Startdate() As Date
        Get
            Return mdtStartDate
        End Get
        Set(ByVal value As Date)
            mdtStartDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enddate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Enddate() As Date
        Get
            Return mdtEnddate
        End Get
        Set(ByVal value As Date)
            mdtEnddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvaldate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approvaldate() As Date
        Get
            Return mdtApprovaldate
        End Get
        Set(ByVal value As Date)
            mdtApprovaldate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactionheadunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Transactionheadunkid() As Integer
        Get
            Return mintTransactionheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTransactionheadunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set amount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Amount() As Decimal
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal)
            mdecAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _VoidReason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsCancelForm() As Boolean
        Get
            Return mblnIsCancelForm
        End Get
        Set(ByVal value As Boolean)
            mblnIsCancelForm = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvertranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approvertranunkid() As Integer
        Get
            Return mintApproverTranunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverTranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdtExpense
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _DtExpense() As DataTable
        Get
            Return mdtExpense
        End Get
        Set(ByVal value As DataTable)
            mdtExpense = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdecDayFraction
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _DayFraction() As Decimal
        Get
            Return mdcDayFraction
        End Get
        Set(ByVal value As Decimal)
            mdcDayFraction = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdtFraction
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _DtFraction() As DataTable
        Get
            Return mdtFraction
        End Get
        Set(ByVal value As DataTable)
            mdtFraction = value
        End Set
    End Property

    Public WriteOnly Property _WebFrmName() As String
        Set(ByVal value As String)
            mstrWebFrmName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClaimRequestMasterId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClaimRequestMasterId() As Integer
        Get
            Return mintClaimRequestMasterId
        End Get
        Set(ByVal value As Integer)
            mintClaimRequestMasterId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _PaymentApprovalwithLeaveApproval
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _PaymentApprovalwithLeaveApproval() As Boolean
        Get
            Return mblnPaymentApprovalwithLeaveApproval
        End Get
        Set(ByVal value As Boolean)
            mblnPaymentApprovalwithLeaveApproval = value
        End Set
    End Property

    Public Property _YearId() As Integer
        Get
            Return mintYearId
        End Get
        Set(ByVal value As Integer)
            mintYearId = value
        End Set
    End Property

    Public Property _VisiblelId() As Integer
        Get
            Return mintVisibleId
        End Get
        Set(ByVal value As Integer)
            mintVisibleId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _CompanyID
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _CompanyID() As Integer
        Get
            Return mintCompanyId
        End Get
        Set(ByVal value As Integer)
            mintCompanyId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _ArutiSelfServiceURL
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _ArutiSelfServiceURL() As String
        Get
            Return mstrArutiSelfServiceURL
        End Get
        Set(ByVal value As String)
            mstrArutiSelfServiceURL = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _LoginMode
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _LoginMode() As enLogin_Mode
        Get
            Return mintLoginMode
        End Get
        Set(ByVal value As enLogin_Mode)
            mintLoginMode = value
        End Set
    End Property

    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property


    'Pinkal (10-Jan-2017) -- Start
    'Enhancement - Working on TRA C&R Module Changes with Leave Module.

    ''' <summary>
    ''' Purpose: Set ObjClaimRequestMst
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _ObjClaimRequestMst() As clsclaim_request_master
        Set(ByVal value As clsclaim_request_master)
            objClaimRMst = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set mdtClaimAttchment
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _dtClaimAttchment() As DataTable
        Get
            Return mdtClaimAttchment
        End Get
        Set(ByVal value As DataTable)
            mdtClaimAttchment = value
        End Set
    End Property

    'Pinkal (10-Jan-2017) -- End


    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    ''' <summary>
    ''' Purpose: Set ReliverEmpId
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _RelieverEmpId() As Integer
        Get
            Return mintRelieverEmpunkid
        End Get
        Set(ByVal value As Integer)
            mintRelieverEmpunkid = value
        End Set
    End Property

    'Pinkal (01-Oct-2018) -- End


    'Pinkal (01-Apr-2019) -- Start
    'Enhancement - Working on Leave Changes for NMB.

    ''' <summary>
    ''' Purpose: Set EscalationDate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _EscalationDate() As DateTime
        Get
            Return mdtEscalationDate
        End Get
        Set(ByVal value As DateTime)
            mdtEscalationDate = value
        End Set
    End Property

    'Pinkal (01-Apr-2019) -- End



#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
              "  pendingleavetranunkid " & _
              ", formunkid " & _
              ", employeeunkid " & _
              ", approverunkid " & _
              ", startdate " & _
              ", enddate " & _
              ", approvaldate " & _
              ", transactionheadunkid " & _
              ", statusunkid " & _
              ", amount " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
             ", iscancelform " & _
                    ", ISNULL(approvertranunkid,0)  as approvertranunkid " & _
             ", ISNULL(dayfraction,0)  as dayfraction " & _
             ", ISNULL(visibleid,statusunkid) AS visibleid " & _
                      ", ISNULL(relieverempunkid,0) AS relieverempunkid " & _
                     ", escalation_date " & _
                     " FROM lvpendingleave_tran " & _
                     " WHERE pendingleavetranunkid = @pendingleavetranunkid "

            'Pinkal (01-Apr-2019) --  'Enhancement - Working on Leave Changes for NMB.[", escalation_date " & _]

            'Pinkal (01-Oct-2018) --  'Enhancement - Leave Enhancement for NMB.[", ISNULL(relieverempunkid,0) AS relieverempunkid " & _]


            objDataOperation.AddParameter("@pendingleavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingleavetranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPendingleavetranunkid = CInt(dtRow.Item("pendingleavetranunkid"))
                mintFormunkid = CInt(dtRow.Item("formunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintApproverunkid = CInt(dtRow.Item("approverunkid"))
                If dtRow.Item("startdate").ToString <> "" Then mdtStartDate = dtRow.Item("startdate")
                If dtRow.Item("enddate").ToString <> "" Then mdtEnddate = dtRow.Item("enddate")
                If dtRow.Item("approvaldate").ToString <> "" Then mdtApprovaldate = dtRow.Item("approvaldate")
                mintTransactionheadunkid = CInt(dtRow.Item("transactionheadunkid"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mdecAmount = CDec(dtRow.Item("amount"))
                mstrRemark = dtRow.Item("remark").ToString
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If dtRow.Item("voiddatetime").ToString <> "" Then mdtVoiddatetime = dtRow.Item("voiddatetime")
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mblnIsCancelForm = CBool(dtRow.Item("voiddatetime"))
                End If
                mintApproverTranunkid = CInt(dtRow.Item("approvertranunkid"))
                mdcDayFraction = CDec(dtRow.Item("dayfraction"))
                mintVisibleId = CInt(dtRow.Item("visibleid"))

                'Pinkal (01-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                mintRelieverEmpunkid = CInt(dtRow.Item("relieverempunkid"))
                'Pinkal (01-Oct-2018) -- End


                'Pinkal (01-Apr-2019) -- Start
                'Enhancement - Working on Leave Changes for NMB.
                If Not IsDBNull(dtRow.Item("escalation_date")) Then
                    mdtEscalationDate = CDate(dtRow.Item("escalation_date"))
                End If
                'Pinkal (01-Apr-2019) -- End


                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal blnIncludeInactiveEmployee As String = "", Optional ByVal strEmployeeAsOnDate As String = "", Optional ByVal strUserAccessLevelFilterString As String = "", Optional ByVal intUserID As Integer = -1, Optional ByVal strFilter As String = "") As DataSet         'Pinkal (25-APR-2012) -- Start
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try


    '        'Pinkal (15-Jul-2013) -- Start
    '        'Enhancement : TRA Changes

    '        'strQ = "SELECT " & _
    '        '  "  pendingleavetranunkid " & _
    '        '  ", ISNULL(leaveissueunkid,'') as leaveissueunkid " & _
    '        '  ", lvpendingleave_tran.formunkid " & _
    '        '  ", lvleaveform.formno " & _
    '        '  ", lvleaveform.leavetypeunkid " & _
    '        '  ", lvleavetype_master.leavename " & _
    '        '  ", lvpendingleave_tran.employeeunkid " & _
    '        '  ", h1.employeecode " & _
    '        '  ", isnull(h1.firstname,'') + ' ' + isnull(h1.surname,'') as employeename " & _
    '        '  ", lvleaveapprover_master.approverunkid as leaveapproverunkid " & _
    '        '  ", lvpendingleave_tran.approverunkid " & _
    '        '  ", isnull(h2.firstname,'') + ' ' + isnull(h2.surname,'') as approvername " & _
    '        '  ", lvleaveapprover_master.levelunkid " & _
    '        '  ", lvapproverlevel_master.levelname " & _
    '        '  ", lvapproverlevel_master.priority " & _
    '        '  ", convert(char(8),lvpendingleave_tran.startdate,112) as startdate " & _
    '        '  ", convert(char(8),lvpendingleave_tran.enddate,112) as enddate  " & _
    '        '  ", convert(char(8),lvpendingleave_tran.approvaldate,112) as approvaldate " & _
    '        '  ", ISNULL((SELECT SUM(dayfraction) FROM lvleaveday_fraction WHERE lvleaveday_fraction.formunkid = lvpendingleave_tran.formunkid AND lvleaveday_fraction.isvoid = 0 AND leavedate BETWEEN lvpendingleave_tran.startdate AND lvpendingleave_tran.enddate),0.00) days " & _
    '        '  ", transactionheadunkid " & _
    '        '  ", lvpendingleave_tran.statusunkid " & _
    '        '  ", case when lvpendingleave_tran.statusunkid = 1 then @Approve " & _
    '        '  "          when lvpendingleave_tran.statusunkid = 2 then @Pending  " & _
    '        '  "          when lvpendingleave_tran.statusunkid = 3 then @Reject " & _
    '        '  "          when lvpendingleave_tran.statusunkid = 4 then @ReSchedule " & _
    '        '  "          when lvpendingleave_tran.statusunkid = 7 then @Issued end as status" & _
    '        '  ", lvpendingleave_tran.amount " & _
    '        '  ", lvpendingleave_tran.remark " & _
    '        '  ", lvpendingleave_tran.userunkid " & _
    '        '  ", ISNULL(hrapprover_usermapping.userunkid,-1) as mapuserunkid " & _
    '        '  ", ISNULL(hrapprover_usermapping.approverunkid,-1) as mapapproverunkid " & _
    '        '  ", lvpendingleave_tran.isvoid " & _
    '        '  ", lvpendingleave_tran.voiddatetime " & _
    '        '  ", lvpendingleave_tran.voiduserunkid " & _
    '        '  ", lvpendingleave_tran.voidreason " & _
    '        '  ", CONVERT(CHAR(8),lvleaveform.applydate,112) As applydate " & _
    '        '  ", isnull(lvpendingleave_tran.iscancelform,0) iscancelform " & _
    '        ' ", ISNULL(lvpendingleave_tran.approvertranunkid ,0) as approvertranunkid " & _
    '        ' ", ISNULL(lvleaveIssue_master.userunkid,-1) AS IssueUserId " & _
    '        ' ", ISNULL(hrmsConfiguration..cfuser_master.username,'') AS IssueUserName "


    'strQ = "SELECT " & _
    '  "  pendingleavetranunkid " & _
    '  ", ISNULL(leaveissueunkid,'') as leaveissueunkid " & _
    '  ", lvpendingleave_tran.formunkid " & _
    '  ", lvleaveform.formno " & _
    '  ", lvleaveform.leavetypeunkid " & _
    '  ", lvleavetype_master.leavename " & _
    '  ", lvpendingleave_tran.employeeunkid " & _
    '  ", h1.employeecode " & _
    '  ", isnull(h1.firstname,'') + ' ' + isnull(h1.surname,'') as employeename " & _
    '          ", ISNULL(lvleaveapprover_master.approverunkid,-1) as leaveapproverunkid " & _
    '  ", lvpendingleave_tran.approverunkid " & _
    '  ", isnull(h2.firstname,'') + ' ' + isnull(h2.surname,'') as approvername " & _
    '          ", ISNULL(lvleaveapprover_master.levelunkid,-1) AS levelunkid " & _
    '          ", ISNULL(lvapproverlevel_master.levelname,'') AS  levelname " & _
    '          ", ISNULL(lvapproverlevel_master.priority,-1) AS priority " & _
    '  ", convert(char(8),lvpendingleave_tran.startdate,112) as startdate " & _
    '  ", convert(char(8),lvpendingleave_tran.enddate,112) as enddate  " & _
    '  ", convert(char(8),lvpendingleave_tran.approvaldate,112) as approvaldate " & _
    '       ", ISNULL(dayfraction,0.00) days " & _
    '  ", transactionheadunkid " & _
    '  ", lvpendingleave_tran.statusunkid " & _
    '          ", lvleaveform.statusunkid AS formstatusunkid " & _
    '  ", case when lvpendingleave_tran.statusunkid = 1 then @Approve " & _
    '  "          when lvpendingleave_tran.statusunkid = 2 then @Pending  " & _
    '  "          when lvpendingleave_tran.statusunkid = 3 then @Reject " & _
    '  "          when lvpendingleave_tran.statusunkid = 4 then @ReSchedule " & _
    '  "          when lvpendingleave_tran.statusunkid = 7 then @Issued end as status" & _
    '  ", lvpendingleave_tran.amount " & _
    '  ", lvpendingleave_tran.remark " & _
    '  ", lvpendingleave_tran.userunkid " & _
    '  ", ISNULL(hrapprover_usermapping.userunkid,-1) as mapuserunkid " & _
    '  ", ISNULL(hrapprover_usermapping.approverunkid,-1) as mapapproverunkid " & _
    '  ", lvpendingleave_tran.isvoid " & _
    '  ", lvpendingleave_tran.voiddatetime " & _
    '  ", lvpendingleave_tran.voiduserunkid " & _
    '  ", lvpendingleave_tran.voidreason " & _
    '  ", CONVERT(CHAR(8),lvleaveform.applydate,112) As applydate " & _
    '  ", isnull(lvpendingleave_tran.iscancelform,0) iscancelform " & _
    ' ", ISNULL(lvpendingleave_tran.approvertranunkid ,0) as approvertranunkid " & _
    ' ", ISNULL(lvleaveIssue_master.userunkid,-1) AS IssueUserId " & _
    '         ", ISNULL(hrmsConfiguration..cfuser_master.username,'') AS IssueUserName " & _
    '         ", ISNULL(visibleid,lvpendingleave_tran.statusunkid) AS visibleid "

    '        'Pinkal (15-Jul-2013) -- End

    '        strQ &= ",ISNULL(h1.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                 ",ISNULL(h1.unitgroupunkid,0) AS unitgroupunkid " & _
    '                 ",ISNULL(h1.teamunkid,0) AS teamunkid " & _
    '                 ",ISNULL(h1.stationunkid,0) AS stationunkid " & _
    '                 ",ISNULL(h1.deptgroupunkid,0) AS deptgroupunkid " & _
    '                 ",ISNULL(h1.departmentunkid,0) AS departmentunkid " & _
    '                 ",ISNULL(h1.sectionunkid,0) AS sectionunkid " & _
    '                 ",ISNULL(h1.unitunkid,0) AS unitunkid " & _
    '                 ",ISNULL(h1.jobunkid,0) AS jobunkid " & _
    '                 ",ISNULL(h1.classgroupunkid,0) AS classgroupunkid " & _
    '                 ",ISNULL(h1.classunkid,0) AS classunkid " & _
    '                 ",ISNULL(h1.jobgroupunkid,0) AS jobgroupunkid " & _
    '                 ",ISNULL(h1.gradegroupunkid,0) AS gradegroupunkid " & _
    '                 ",ISNULL(h1.gradeunkid,0) AS gradeunkid " & _
    '                 ",ISNULL(h1.gradelevelunkid,0) AS gradelevelunkid "

    '        strQ &= " FROM lvpendingleave_tran " & _
    '          " JOIN lvleaveform on lvleaveform.formunkid = lvpendingleave_tran.formunkid and lvleaveform.isvoid = 0" & _
    '          " LEFT JOIN lvleaveIssue_master ON lvleaveIssue_master.formunkid = lvleaveform.formunkid " & _
    '          " LEFT JOIN lvleavetype_master on lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid " & _
    '          " LEFT JOIN hremployee_master h1 on h1.employeeunkid = lvpendingleave_tran.employeeunkid  " & _
    '          " LEFT JOIN hremployee_master h2 on h2.employeeunkid = lvpendingleave_tran.approverunkid  " & _
    '          " LEFT JOIN lvleaveapprover_master ON lvleaveapprover_master.leaveapproverunkid = lvpendingleave_tran.approverunkid  AND lvleaveapprover_master.approverunkid = lvpendingleave_tran.approvertranunkid " & _
    '          " LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid	" & _
    '          " LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = lvleaveapprover_master.approverunkid " & " AND hrapprover_usermapping.usertypeid = " & enUserType.Approver

    '        If intUserID <= 0 Then
    '            If User._Object._Userunkid > 1 Then
    '                strQ &= " AND hrapprover_usermapping.userunkid = " & User._Object._Userunkid
    '            End If
    '        Else
    '            strQ &= " AND hrapprover_usermapping.userunkid = " & intUserID
    '        End If


    '        strQ &= " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = lvleaveIssue_master.userunkid " & _
    '          " WHERE 1 = 1 "



    '        If blnOnlyActive Then
    '            strQ &= " AND lvpendingleave_tran.isvoid = 0 "
    '        End If

    '        'Sohail (01 Jan 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        If strFilter.Trim.Length > 0 Then
    '            strQ &= " AND " & strFilter
    '        End If
    '        'Sohail (01 Jan 2013) -- End

    '        'HERE ONLY PUT ACTIVE EMPLOYEE CONDITION NOT FOR APPROVER CONDITION 

    '        objDataOperation.ClearParameters()

    '        If blnIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            blnIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString()
    '        End If

    '        If CBool(blnIncludeInactiveEmployee) = False Then

    '            strQ &= " AND CONVERT(CHAR(8),h1.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),h1.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),h1.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),h1.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))

    '        End If


    '        'Pinkal (06-Feb-2013) -- Start
    '        'Enhancement : TRA Changes

    '        strQ &= " AND h1.isapproved = 1 "

    '        'Pinkal (06-Feb-2013) -- End


    '        strQ &= " order by lvpendingleave_tran.formunkid,lvapproverlevel_master.priority "


    '        objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
    '        objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
    '        objDataOperation.AddParameter("@ReSchedule", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 113, "Re-Scheduled"))
    '        objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 276, "Issued"))


    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function


    'Pinkal (15-Mar-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                       , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                       , ByVal mdtEmployeeAsOnDate As Date _
                                       , ByVal xOnlyApproved As Boolean _
                                       , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                       , Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal strFilter As String = "" _
                                       , Optional ByVal objDoOperation As clsDataOperation = Nothing _
                                       , Optional ByVal mblnIncludeCloseFYTransactions As Boolean = False _
                                       , Optional ByVal iEmployeeId As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            If mblnIncludeCloseFYTransactions AndAlso iEmployeeId > 0 Then

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

                strQ = "SELECT  yearunkid " & _
                           ", financialyear_name " & _
                           ", database_name " & _
                           ", companyunkid " & _
                           ", isclosed  " & _
                           ", convert(char(8),start_date,112) AS start_date  " & _
                           ", convert(char(8),end_date,112) AS end_date " & _
                           " FROM  hrmsconfiguration..cffinancial_year_tran " & _
                          " WHERE companyunkid = @companyunkid " & _
                          "ORDER BY convert(CHAR(8), start_date, 112) "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyUnkid)
                Dim dsYear As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

                strQ = ""

                dsList = New DataSet()
                For Each dRow As DataRow In dsYear.Tables(0).Rows
                    Dim iCnt As Integer = 0
                    strQ = "SELECT COUNT(*) FROM " & dRow("database_name").ToString() & "..hremployee_master WHERE employeeunkid = @employeeunkid "
            objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeId)
                    iCnt = objDataOperation.RecordCount(strQ)

                    If iCnt > 0 Then

                        Dim dstemp As DataSet = GetEmployeeLeaveHistoryDetails(strTableName, dRow("database_name").ToString(), xUserUnkid, dRow("yearunkid").ToString(), xCompanyUnkid, mdtEmployeeAsOnDate, xOnlyApproved _
                                                                                                           , xIncludeIn_ActiveEmployee, blnOnlyActive, strFilter, objDoOperation, mblnIncludeCloseFYTransactions, iEmployeeId)

                        If dsList.Tables.Count <= 0 Then
                            dsList.Tables.Add(dstemp.Tables(0).Copy())
                Else
                            dsList.Tables(0).Merge(dstemp.Tables(0), True)
                End If

                        dstemp.Clear()
                        dstemp = Nothing

                End If

                Next
                Else
                dsList = GetEmployeeLeaveHistoryDetails(strTableName, xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, mdtEmployeeAsOnDate, xOnlyApproved _
                                                                          , xIncludeIn_ActiveEmployee, blnOnlyActive, strFilter, objDoOperation, mblnIncludeCloseFYTransactions)
                End If


            Dim dView As DataView = New DataView(dsList.Tables(0).Copy, "", "formunkid,priority", DataViewRowState.CurrentRows)
            dsList.Tables(0).Clear()
            dsList.Tables(0).Merge(dView.ToTable)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Pinkal (15-Mar-2019) -- End


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvpendingleave_tran) </purpose>
    Public Function Insert(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            '  If mintStatusunkid <> 2 Then
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
            objDataOperation.AddParameter("@startDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtStartDate <> Nothing, mdtStartDate, DBNull.Value))
            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEnddate <> Nothing, mdtEnddate, DBNull.Value))
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtApprovaldate <> Nothing, mdtApprovaldate, DBNull.Value))
            objDataOperation.AddParameter("@transactionheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionheadunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@iscancelform", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCancelForm)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid)
            objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdcDayFraction)
            objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleId)


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@relieverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelieverEmpunkid)
            'Pinkal (01-Oct-2018) -- End


            'Pinkal (01-Apr-2019) -- Start
            'Enhancement - Working on Leave Changes for NMB.
            objDataOperation.AddParameter("@escalation_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEscalationDate <> Nothing, mdtEscalationDate, DBNull.Value))
            'Pinkal (01-Apr-2019) -- End



            strQ = "INSERT INTO lvpendingleave_tran ( " & _
              "  formunkid " & _
              ", employeeunkid " & _
              ", approverunkid " & _
              ", startdate " & _
              ", enddate " & _
              ", approvaldate " & _
              ", transactionheadunkid " & _
              ", statusunkid " & _
              ", amount " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid" & _
              ", voidreason " & _
             ", iscancelform " & _
             ", approvertranunkid " & _
             ", dayfraction " & _
             ", visibleid " & _
                     ", relieverempunkid " & _
                     ", escalation_date " & _
            ") VALUES (" & _
              "  @formunkid " & _
              ", @employeeunkid " & _
              ", @approverunkid " & _
              ", @startdate " & _
              ", @enddate " & _
              ", @approvaldate " & _
              ", @transactionheadunkid " & _
              ", @statusunkid " & _
              ", @amount " & _
              ", @remark " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiddatetime " & _
              ", @voiduserunkid" & _
              ", @voidreason " & _
             ", @iscancelform " & _
             ", @approvertranunkid " & _
             ", @dayfraction " & _
             ", @visibleid " & _
                     ", @relieverempunkid " & _
                     ", @escalation_date " & _
            "); SELECT @@identity"

            'Pinkal (01-Apr-2019) -- 'Enhancement - Working on Leave Changes for NMB.[escalation_date]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPendingleavetranunkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "lvpendingleave_tran", "pendingleavetranunkid", mintPendingleavetranunkid, False, mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'START FOR UPDATE THE EARING AND DEDUCTION OF THE EMPLOYEE EARNING AND DEDUCTION

            strQ = "Update prearningdeduction_master set amount = @allowance where employeeunkid = @empunkid AND tranheadunkid = @tranheadunkid"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@allowance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionheadunkid.ToString)
            objDataOperation.ExecNonQuery(strQ)


            'END FOR UPDATE THE EARING AND DEDUCTION OF THE EMPLOYEE EARNING AND DEDUCTION

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            '   objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (lvpendingleave_tran) </purpose>
    'Public Function Insert(Optional ByVal blnIncludeInactiveEmployee As String = "", Optional ByVal strEmployeeAsOnDate As String = "") As Boolean

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    ' START FOR PENDING STATUS NO ENTRY FOR PENDING LEAVE TRAN TABLE
    '    If mintStatusunkid = 2 And mintPendingleavetranunkid = 0 Then
    '        Return True
    '    End If
    '    ' END FOR PENDING STATUS NO ENTRY FOR PENDING LEAVE TRAN TABLE

    '    Dim objDataOperation As New clsDataOperation
    '    objDataOperation.BindTransaction()

    '    Try

    '        If mintPendingleavetranunkid > 0 Then

    '            strQ = "Update lvpendingleave_tran set isvoid = 1,voiddatetime =getdate() where pendingleavetranunkid = @pending_leavetranunkid and formunkid = @form_unkid"

    '            objDataOperation.AddParameter("@form_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
    '            objDataOperation.AddParameter("@pending_leavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingleavetranunkid.ToString)
    '            objDataOperation.ExecNonQuery(strQ)

    '        End If

    '        If mintStatusunkid <> 2 Then
    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
    '            objDataOperation.AddParameter("@startDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtStartDate <> Nothing, mdtStartDate, DBNull.Value))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEnddate <> Nothing, mdtEnddate, DBNull.Value))
    '            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtApprovaldate <> Nothing, mdtApprovaldate, DBNull.Value))
    '            objDataOperation.AddParameter("@transactionheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionheadunkid.ToString)
    '            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
    '            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
    '            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
    '            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
    '            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtApprovaldate, DBNull.Value))
    '            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '            objDataOperation.AddParameter("@iscancelform", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCancelForm)
    '            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)


    '            'Pinkal (15-Jul-2013) -- Start
    '            'Enhancement : TRA Changes
    '            objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdcDayFraction.ToString)
    '            'Pinkal (15-Jul-2013) -- End


    '            'Pinkal (09-Jul-2014) -- Start
    '            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2
    '            objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleId.ToString)
    '            'Pinkal (09-Jul-2014) -- End


    '            strQ = "INSERT INTO lvpendingleave_tran ( " & _
    '              "  formunkid " & _
    '              ", employeeunkid " & _
    '              ", approverunkid " & _
    '              ", startdate " & _
    '              ", enddate " & _
    '              ", approvaldate " & _
    '              ", transactionheadunkid " & _
    '              ", statusunkid " & _
    '              ", amount " & _
    '              ", remark " & _
    '              ", userunkid " & _
    '              ", isvoid " & _
    '              ", voiddatetime " & _
    '              ", voiduserunkid" & _
    '              ", voidreason " & _
    '             ", iscancelform " & _
    '            ", approvertranunkid " & _
    '            ", dayfraction " & _
    '            ", visibleid " & _
    '            ") VALUES (" & _
    '              "  @formunkid " & _
    '              ", @employeeunkid " & _
    '              ", @approverunkid " & _
    '              ", @startdate " & _
    '              ", @enddate " & _
    '              ", @approvaldate " & _
    '              ", @transactionheadunkid " & _
    '              ", @statusunkid " & _
    '              ", @amount " & _
    '              ", @remark " & _
    '              ", @userunkid " & _
    '              ", @isvoid " & _
    '              ", @voiddatetime " & _
    '              ", @voiduserunkid" & _
    '              ", @voidreason " & _
    '             ", @iscancelform " & _
    '            ", @approvertranunkid " & _
    '            ", @dayfraction " & _
    '              ", @visibleid " & _
    '            "); SELECT @@identity"

    '            'Pinkal (15-Jul-2013) [dayfraction]


    '            dsList = objDataOperation.ExecQuery(strQ, "List")
    '            mintPendingleavetranunkid = dsList.Tables(0).Rows(0).Item(0)


    '            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "lvpendingleave_tran", "pendingleavetranunkid", mintPendingleavetranunkid, False, mintUserunkid) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If


    '            'START FOR UPDATE THE EARING AND DEDUCTION OF THE EMPLOYEE EARNING AND DEDUCTION

    '            strQ = "Update prearningdeduction_master set amount = @allowance where employeeunkid = @empunkid AND tranheadunkid = @tranheadunkid"

    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@allowance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
    '            objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionheadunkid.ToString)
    '            objDataOperation.ExecNonQuery(strQ)

    '            'END FOR UPDATE THE EARING AND DEDUCTION OF THE EMPLOYEE EARNING AND DEDUCTION

    '        End If

    '        dsList = GetEmployeeWiseApproverLevels(objDataOperation, mintEmployeeunkid, mintApproverunkid)

    '        If dsList.Tables("List").Rows.Count > 0 Then

    '            Dim objApprover As New clsleaveapprover_master
    '            If blnIncludeInactiveEmployee.Trim.Length <= 0 Then
    '                blnIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString()
    '            End If

    '            Dim dsApprover As DataSet = objApprover.GetList("List", True, blnIncludeInactiveEmployee, strEmployeeAsOnDate)
    '            Dim dtList As DataTable = New DataView(dsApprover.Tables("List"), "leaveapproverunkid =" & mintApproverunkid, "", DataViewRowState.CurrentRows).ToTable
    '            If dtList.Rows.Count > 0 Then
    '                objApprover._Approverunkid = CInt(dtList.Rows(0)("approverunkid"))
    '            End If

    '            Dim objLevel As New clsapproverlevel_master
    '            objLevel._Levelunkid = objApprover._Levelunkid


    '            If CInt(dsList.Tables("List").Compute("Max(priority)", "")) = objLevel._Priority Then



    '                'Pinkal (15-Jul-2013) -- Start
    '                'Enhancement : TRA Changes

    '                'START FOR UPDATE LEAVE FORM APPROVED START DATE,END DATE AND APPROVED DAYS
    '                Dim mblnApproved As Boolean = False
    '                If mintStatusunkid = 1 Then
    '                    mblnApproved = True
    '                End If
    '                If clsleaveform.ApprovedLeaveFormtenure(objDataOperation, mintFormunkid, mdtStartDate, mdtEnddate, mdcDayFraction, mblnApproved, mintUserunkid) = False Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If

    '                'END FOR UPDATE LEAVE FORM APPROVED START DATE,END DATE AND APPROVED DAYS

    '                'Pinkal (15-Jul-2013) -- End


    '                ' START FOR UPDATE THE STATUS OF THE LEAVE FORM
    '                'Pinkal (25-APR-2012) -- Start
    '                'Enhancement : TRA Changes
    '                'clsleaveform.UpdateLeaveFormStatus(objDataOperation, mintFormunkid, mintStatusunkid)
    '                clsleaveform.UpdateLeaveFormStatus(objDataOperation, mintFormunkid, mintStatusunkid, mintUserunkid)
    '                'Pinkal (25-APR-2012) -- End

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If



    '                ' END FOR UPDATE THE STATUS OF THE LEAVE FORM

    '            End If

    '            End If


    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        objDataOperation.ReleaseTransaction(True)
    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        '   objDataOperation = Nothing
    '    End Try
    'End Function



    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> UPDATE INTO Database Table (lvpendingleave_tran) </purpose>
    'Public Function Update(Optional ByVal intYearID As Integer = 0, Optional ByVal blnIncludeInactiveEmployee As String = "", _
    '                                 Optional ByVal strEmployeeAsOnDate As String = "", Optional ByVal mdtDbstartdate As Date = Nothing, Optional ByVal mdtDbEnddate As Date = Nothing, _
    '                                 Optional ByVal blnLeaveApproverForLeaveType As String = "", Optional ByVal intLeavebalanceSetting As Integer = -1, Optional ByVal mstrIsAutomaticIssueLeave As String = "") As Boolean

    '    'Pinkal (01-Feb-2014) -- Start  [Optional ByVal intLeavebalanceSetting As Integer = -1, Optional ByVal blnIsAutomaticIssueLeave As Boolean = False]
    '    'Pinkal (27-May-2015) -- Start 'Issue : WORKED ON AUTOMATIC LEAVE ISSUE PROBLEM CAME IN TRA.[Optional ByVal mstrIsAutomaticIssueLeave As String = ""]


    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception



    '    'Pinkal (01-Feb-2014) -- Start
    '    'Enhancement : TRA Changes
    '    Dim objApprover As New clsleaveapprover_master
    '    'Pinkal (01-Feb-2014) -- End


    '    Dim objDataOperation As New clsDataOperation
    '    objDataOperation.BindTransaction()

    '    Try

    '        If mintStatusunkid <> 2 Then
    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@pendingleavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingleavetranunkid.ToString)
    '            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
    '            objDataOperation.AddParameter("@startDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtStartDate <> Nothing, mdtStartDate, DBNull.Value))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEnddate <> Nothing, mdtEnddate, DBNull.Value))
    '            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtApprovaldate <> Nothing, mdtApprovaldate, DBNull.Value))
    '            objDataOperation.AddParameter("@transactionheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionheadunkid.ToString)
    '            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
    '            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
    '            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
    '            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
    '            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtApprovaldate, DBNull.Value))
    '            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '            objDataOperation.AddParameter("@iscancelform", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCancelForm)
    '            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
    '            objDataOperation.AddParameter("@dayFraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdcDayFraction.ToString)

    '            'Pinkal (09-Jul-2014) -- Start
    '            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2
    '            objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleId.ToString)
    '            'Pinkal (09-Jul-2014) -- End



    '            strQ = "Update lvpendingleave_tran set " & _
    '              "  formunkid = formunkid " & _
    '              ", employeeunkid = @employeeunkid " & _
    '              ", approverunkid = @approverunkid" & _
    '              ", startdate = @startdate" & _
    '              ", enddate = @enddate" & _
    '              ", approvaldate = @approvaldate" & _
    '              ", transactionheadunkid = @transactionheadunkid" & _
    '              ", statusunkid = @statusunkid" & _
    '              ", amount = @amount" & _
    '              ", remark = @remark" & _
    '              ", userunkid = @userunkid" & _
    '              ", isvoid = @isvoid" & _
    '              ", voiddatetime = @voiddatetime" & _
    '              ", voiduserunkid = @voiduserunkid" & _
    '              ", voidreason = @voidreason" & _
    '            ", iscancelform = @iscancelform " & _
    '                       ", approvertranunkid = @approvertranunkid " & _
    '              ", dayFraction = @dayFraction " & _
    '              ", visibleid = @visibleid " & _
    '              "  WHERE pendingleavetranunkid = @pendingleavetranunkid"

    '            objDataOperation.ExecNonQuery(strQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lvpendingleave_tran", "pendingleavetranunkid", mintPendingleavetranunkid, False, mintUserunkid) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            'START FOR UPDATE THE EARING AND DEDUCTION OF THE EMPLOYEE EARNING AND DEDUCTION

    '            strQ = "Update prearningdeduction_master set amount = @allowance where employeeunkid = @empunkid AND tranheadunkid = @tranheadunkid"

    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@allowance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
    '            objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionheadunkid.ToString)
    '            objDataOperation.ExecNonQuery(strQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            'END FOR UPDATE THE EARING AND DEDUCTION OF THE EMPLOYEE EARNING AND DEDUCTION


    '            dsList = GetEmployeeApproverListWithPriority(mintEmployeeunkid, mintFormunkid, objDataOperation)

    '            If dsList.Tables("List").Rows.Count > 0 Then


    '                'Pinkal (01-Feb-2014) -- Start
    '                'Enhancement : TRA Changes
    '                'Dim objApprover As New clsleaveapprover_master
    '                objApprover = New clsleaveapprover_master
    '                'Pinkal (01-Feb-2014) -- End



    '                If blnIncludeInactiveEmployee.Trim.Length <= 0 Then
    '                    blnIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString()
    '                End If

    '                Dim dsApprover As DataSet = Nothing



    '                'Pinkal (15-Oct-2014) -- Start
    '                'Enhancement -  AKFTZ LEAVE APPROVER PROBLEM FOR VISIBILITY IN LEAVE PROCESS PENDING TRAN
    '                'If UserAccessLevel._AccessLevelFilterString.Trim.Contains("(0)") Then
    '                '    dsApprover = objApprover.GetList("List", True, blnIncludeInactiveEmployee, strEmployeeAsOnDate, mintUserunkid, "AND 1=1")
    '                'Else
    '                '    dsApprover = objApprover.GetList("List", True, blnIncludeInactiveEmployee, strEmployeeAsOnDate, mintUserunkid)
    '                'End If

    '                dsApprover = objApprover.GetApproverFromEmployee(-1, mintEmployeeunkid, -1, blnLeaveApproverForLeaveType, strEmployeeAsOnDate)

    '                'Pinkal (15-Oct-2014) -- End


    '                Dim dtList As DataTable = New DataView(dsApprover.Tables("List"), "leaveapproverunkid =" & mintApproverunkid & " AND approverunkid = " & mintApproverTranunkid, "", DataViewRowState.CurrentRows).ToTable

    '            If dtList.Rows.Count > 0 Then
    '                objApprover._Approverunkid = CInt(dtList.Rows(0)("approverunkid"))
    '            End If

    '            Dim objLevel As New clsapproverlevel_master
    '            objLevel._Levelunkid = objApprover._Levelunkid


    '                'Pinkal (09-Jul-2014) -- Start
    '                'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2

    '                strQ = " UPDATE lvpendingleave_tran set " & _
    '                          " visibleid = " & mintStatusunkid & _
    '                          " WHERE  formunkid = @formunkid and employeeunkid = @employeeunkid AND approvertranunkid = @approvertranunkid "

    '                If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then

    '                    Dim dtVisibility As DataTable = New DataView(dsApprover.Tables(0), "priority = " & objLevel._Priority, "", DataViewRowState.CurrentRows).ToTable

    '                    For i As Integer = 0 To dtVisibility.Rows.Count - 1

    '                        If objLevel._Priority = CInt(dtVisibility.Rows(i)("priority")) Then
    '                            objDataOperation.ClearParameters()
    '                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
    '                            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid)
    '                            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtVisibility.Rows(i)("approverunkid")))
    '                            objDataOperation.ExecNonQuery(strQ)

    '                            If objDataOperation.ErrorMessage <> "" Then
    '                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                Throw exForce
    '                            End If

    '                        End If

    '                    Next

    '                    Dim intMinPriority As Integer = IIf(IsDBNull(dsApprover.Tables(0).Compute("Min(priority)", "priority > " & objLevel._Priority)), -1, dsApprover.Tables(0).Compute("Min(priority)", "priority > " & objLevel._Priority))

    '                    dtVisibility = New DataView(dsApprover.Tables(0), "priority = " & intMinPriority & " AND priority <> -1", "", DataViewRowState.CurrentRows).ToTable

    '                    If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then

    '                        If mintStatusunkid = 1 Then   'ONLY APPROVED STATUS

    '                            strQ = " UPDATE lvpendingleave_tran set " & _
    '                                      " visibleid =  2 " & _
    '                                      " WHERE  formunkid = @formunkid and employeeunkid = @employeeunkid AND approvertranunkid = @approvertranunkid "

    '                        ElseIf mintStatusunkid = 3 OrElse mintStatusunkid = 4 Then   ' REJECTED OR RE-SCHEDULED STATUS
    '                            strQ = " UPDATE lvpendingleave_tran set " & _
    '                                      " visibleid =  -1 " & _
    '                                      " WHERE  formunkid = @formunkid and employeeunkid = @employeeunkid AND approvertranunkid = @approvertranunkid "
    '                        End If

    '                        For i As Integer = 0 To dtVisibility.Rows.Count - 1
    '                            objDataOperation.ClearParameters()
    '                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
    '                            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid)
    '                            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtVisibility.Rows(i)("approverunkid")))
    '                            objDataOperation.ExecNonQuery(strQ)

    '                            If objDataOperation.ErrorMessage <> "" Then
    '                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                Throw exForce
    '                            End If

    '                        Next

    '                    End If

    '                End If

    '                'Pinkal (09-Jul-2014) -- End



    '            If CInt(dsList.Tables("List").Compute("Max(priority)", "1=1")) = objLevel._Priority Or mintStatusunkid = 3 Or mintStatusunkid = 4 Then

    '                    ' START FOR VOID THE ISSUE LEAVE WHEN LEAVE STATUS IS REJECT OR RE-SCHEDULE

    '                    If mintStatusunkid = 3 Or mintStatusunkid = 4 Then

    '                        strQ = "Update lvleaveIssue_master set isvoid = 1,voiddatetime = @voiddatetime,voiduserunkid = @voiduserunkid,voidreason = @voidreason where formunkid = @form_unkid"
    '                        objDataOperation.ClearParameters()
    '                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
    '                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
    '                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark)
    '                        objDataOperation.AddParameter("@form_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
    '                        objDataOperation.ExecNonQuery(strQ)

    '                        ' END FOR VOID THE ISSUE LEAVE WHEN LEAVE STATUS IS REJECT OR RE-SCHEDULE

    '                        Dim mintLeaveIssueunkid As Integer
    '                        strQ = "Select isnull(leaveissueunkid,0) leaveissueunkid from lvleaveIssue_master where formunkid = @form_unkid"
    '                        objDataOperation.ClearParameters()
    '                        objDataOperation.AddParameter("@form_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
    '                        Dim dsIssue As DataSet = objDataOperation.ExecQuery(strQ, "LeaveIssue")
    '                        If objDataOperation.ErrorMessage <> "" Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If

    '                        If dsIssue.Tables("LeaveIssue").Rows.Count > 0 Then
    '                            Dim TotalLeave As Integer
    '                            mintLeaveIssueunkid = CInt(dsIssue.Tables("LeaveIssue").Rows(0)("leaveissueunkid"))

    '                            ' START FOR GET THE TOTAL NO OF ISSUE LEAVE

    '                            strQ = "Select *  from lvleaveIssue_tran where leaveissueunkid = @leaveissueunkid and isvoid = 0"
    '                            objDataOperation.ClearParameters()
    '                            objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveIssueunkid.ToString)
    '                            dsIssue = objDataOperation.ExecQuery(strQ, "RecodrCnt")

    '                            If objDataOperation.ErrorMessage <> "" Then
    '                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                Throw exForce
    '                            End If

    '                            ' END FOR GET THE TOTAL NO OF ISSUE LEAVE

    '                            If dsIssue.Tables("RecodrCnt").Rows.Count > 0 Then

    '                                TotalLeave = dsIssue.Tables("RecodrCnt").Rows.Count

    '                                ' START FOR VOID THE ISSUE LEAVE TRAN WHEN LEAVE STATUS IS REJECT OR RE-SCHEDULE

    '                                Dim objIssueTran As New clsleaveissue_Tran

    '                                For i As Integer = 0 To dsIssue.Tables(0).Rows.Count - 1

    '                                    objIssueTran._Leaveissuetranunkid = CInt(dsIssue.Tables(0).Rows(i)("leaveissuetranunkid"))

    '                                    If objIssueTran.InsertAuditTrailForLeaveIssue(objDataOperation, 3, CDate(dsIssue.Tables(0).Rows(i)("leavedate")), mintUserunkid) = False Then
    '                                        objDataOperation.ReleaseTransaction(False)
    '                                        Return False
    '                                    End If

    '                                Next

    '                                strQ = "Update lvleaveIssue_tran set isvoid = 1,voiddatetime = @voiddatetime,voiduserunkid = @voiduserunkid,voidreason = @voidreason where leaveissueunkid = @leaveissueunkid"
    '                                objDataOperation.ClearParameters()
    '                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
    '                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
    '                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRemark)
    '                                objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveIssueunkid.ToString)
    '                                objDataOperation.ExecNonQuery(strQ)

    '                                If objDataOperation.ErrorMessage <> "" Then
    '                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                    Throw exForce
    '                                End If


    '                                ' END FOR VOID THE ISSUE LEAVE TRAN WHEN LEAVE STATUS IS REJECT OR RE-SCHEDULE

    '                                ' START FOR UPDATE THE LEAVE BALANCE

    '                                If TotalLeave > 0 Then

    '                                    Dim objLeaveForm As New clsleaveform
    '                                    objLeaveForm._Formunkid = mintFormunkid

    '                                    Dim objLeaveIssue As New clsleaveissue_Tran
    '                                    objLeaveIssue._TotalIssue = TotalLeave

    '                                    If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
    '                                        blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
    '                                    End If

    '                                    objLeaveIssue.UpdateEmployeeAccrueBalance(objDataOperation, DateDiff(DateInterval.Day, mdtStartDate.Date, mdtEnddate.Date.AddDays(1)), mdtStartDate, mdtEnddate _
    '                                                                                                              , IIf(intYearID <= 0, FinancialYear._Object._YearUnkid, intYearID), mintEmployeeunkid, objLeaveForm._Leavetypeunkid, _
    '                                                                                                              True, 0, mintUserunkid, mdtDbstartdate, mdtDbEnddate, blnLeaveApproverForLeaveType)

    '                                End If

    '                                ' END FOR UPDATE THE LEAVE BALANCE

    '                            End If

    '                        End If


    '                        'Pinkal (22-Jun-2015) -- Start
    '                        'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.

    '                        ' START FOR REJECT CLAIM FORM FOR ALL APPROVERS WHEN LEAVE ISSUE IS AUTOMATIC AND PAYMENT APPROVAL FOR CLAIM IS SERIAL.

    '                        If mblnPaymentApprovalwithLeaveApproval = False AndAlso mintClaimRequestMasterId > 0 Then
    '                            Dim objExpenseApprover As New clsExpenseApprover_Master
    '                            Dim dsClaimApprover As DataSet = objExpenseApprover.GetEmployeeApprovers(enExpenseType.EXP_LEAVE, mintEmployeeunkid, "List", objDataOperation)
    '                            If dsClaimApprover IsNot Nothing AndAlso dsClaimApprover.Tables(0).Rows.Count > 0 Then
    '                                Dim objExpenseApproverTran As New clsclaim_request_approval_tran
    '                                objExpenseApproverTran._EmployeeID = mintEmployeeunkid
    '                                objExpenseApproverTran._YearId = mintYearId

    '                                Dim drClaimApprover() As DataRow = dsClaimApprover.Tables(0).Select("crpriority = MAX(crpriority)")
    '                                Dim mintMaxClaimApproverID As Integer = -1
    '                                If drClaimApprover.Length > 0 Then
    '                                    mintMaxClaimApproverID = CInt(drClaimApprover(0)("crapproverunkid"))
    '                                End If
    '                                Dim mblnLastClaimApprover As Boolean = False
    '                                For i As Integer = 0 To dsClaimApprover.Tables(0).Rows.Count - 1
    '                                    objExpenseApproverTran._DataTable.Rows.Clear()
    '                                    If CInt(dsClaimApprover.Tables(0).Rows(i)("crapproverunkid")) = mintMaxClaimApproverID Then mblnLastClaimApprover = True
    '                                    objExpenseApproverTran._DataTable = New DataView(mdtExpense, "crapproverunkid = " & CInt(dsClaimApprover.Tables(0).Rows(i)("crapproverunkid")), "", DataViewRowState.CurrentRows).ToTable

    '                                    'Shani(08-Aug-2015) -- Start
    '                                    'Enhancement - C&R Enhancement Given by glory(CR Revised.docx)
    '                                    'If objExpenseApproverTran.Insert_Update_ApproverData(CInt(dsClaimApprover.Tables(0).Rows(i)("employeeunkid")), CInt(dsClaimApprover.Tables(0).Rows(i)("crapproverunkid")), IIf(mintStatusunkid = 4, 3, mintStatusunkid), IIf(mintStatusunkid = 4, 3, mintStatusunkid), mintUserunkid, mintClaimRequestMasterId, objDataOperation, mstrRemark, mblnLastClaimApprover) = False Then
    '                                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                    '    Throw exForce
    '                                    'End If
    '                                    If objExpenseApproverTran.Insert_Update_ApproverData(CInt(dsClaimApprover.Tables(0).Rows(i)("employeeunkid")), CInt(dsClaimApprover.Tables(0).Rows(i)("crapproverunkid")), IIf(mintStatusunkid = 4, 3, mintStatusunkid), IIf(mintStatusunkid = 4, 3, mintStatusunkid), mintUserunkid, mintClaimRequestMasterId, objDataOperation, mstrRemark, mblnLastClaimApprover, mdtApprovaldate) = False Then
    '                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                        Throw exForce
    '                                    End If
    '                                    'Shani(08-Aug-2015) -- End


    '                                Next
    '                                objExpenseApproverTran = Nothing
    '                            End If
    '                            objExpenseApprover = Nothing
    '                        End If

    '                        ' END FOR REJECT CLAIM FORM FOR ALL APPROVERS WHEN LEAVE ISSUE IS AUTOMATIC AND PAYMENT APPROVAL FOR CLAIM IS SERIAL.

    '                        'Pinkal (22-Jun-2015) -- End

    '                    ElseIf mintStatusunkid = 1 Then

    '                        Dim drApprover() As DataRow = dsApprover.Tables("List").Select("priority = " & objLevel._Priority)
    '                        If drApprover.Length > 0 Then


    '                            strQ = "Update lvpendingleave_tran set " & _
    '                                       " startdate = @startdate " & _
    '                                       ",enddate = @enddate " & _
    '                                       ",dayfraction = @dayfraction " & _
    '                                       " WHERE approverunkid = @approverunkid " & _
    '                                       " AND formunkid = @formunkid and employeeunkid = @employeeunkid"


    '                            For i As Integer = 0 To drApprover.Length - 1
    '                                objDataOperation.ClearParameters()
    '                                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drApprover(i)("leaveapproverunkid")))
    '                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
    '                                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid)
    '                                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartDate)
    '                                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
    '                                objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdcDayFraction)
    '                                objDataOperation.ExecNonQuery(strQ)

    '                                If objDataOperation.ErrorMessage <> "" Then
    '                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                    Throw exForce
    '                                End If

    '                            Next

    '                        End If

    '                    End If


    '                    'START FOR UPDATE LEAVE FORM APPROVED START DATE,END DATE AND APPROVED DAYS

    '                    Dim mblnApproved As Boolean = False
    '                    If mintStatusunkid = 1 Then
    '                        mblnApproved = True
    '                    End If

    '                    If clsleaveform.ApprovedLeaveFormtenure(objDataOperation, mintFormunkid, mdtStartDate, mdtEnddate, mdcDayFraction, mblnApproved, mintUserunkid) = False Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If

    '                    'END FOR UPDATE LEAVE FORM APPROVED START DATE,END DATE AND APPROVED DAYS


    '                    ' START FOR UPDATE THE STATUS OF THE LEAVE FORM

    '                    clsleaveform.UpdateLeaveFormStatus(objDataOperation, mintFormunkid, mintStatusunkid, mintUserunkid)

    '                    If objDataOperation.ErrorMessage <> "" Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If

    '                    ' END FOR UPDATE THE STATUS OF THE LEAVE FORM


    '                    'Pinkal (22-Jun-2015) -- Start
    '                    'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.
    '                    If CBool(mstrIsAutomaticIssueLeave) = False AndAlso mblnPaymentApprovalwithLeaveApproval = False AndAlso mintClaimRequestMasterId > 0 AndAlso mintStatusunkid = 1 Then
    '                        If UpdateLeaveExpenseVisibility(objDataOperation, mintFormunkid, blnLeaveApproverForLeaveType, intLeavebalanceSetting) = False Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If
    '                    End If
    '                    'Pinkal (22-Jun-2015) -- End



    '                    'START FOR UPDATE THE DATE OF HIGHER LEVEL APPROVER 


    '                ElseIf CInt(dsList.Tables("List").Compute("Max(priority)", "1=1")) > objLevel._Priority Then


    '                strQ = "Update lvpendingleave_tran set " & _
    '                       " startdate = @startdate " & _
    '                       ",enddate = @enddate " & _
    '                       ",dayfraction = @dayfraction " & _
    '                       " WHERE approverunkid = @approverunkid " & _
    '                       " AND formunkid = @formunkid and employeeunkid = @employeeunkid"

    '                For i As Integer = 0 To dsApprover.Tables("List").Rows.Count - 1

    '                    If objApprover._leaveapproverunkid = CInt(dsApprover.Tables("List").Rows(i)("leaveapproverunkid")) Then Continue For

    '                        If objLevel._Priority <= CInt(dsApprover.Tables("List").Rows(i)("priority")) Then
    '                        objDataOperation.ClearParameters()
    '                        objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsApprover.Tables("List").Rows(i)("leaveapproverunkid")))
    '                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
    '                        objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid)
    '                        objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartDate)
    '                        objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
    '                            objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdcDayFraction)
    '                        objDataOperation.ExecNonQuery(strQ)

    '                            If objDataOperation.ErrorMessage <> "" Then
    '                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                Throw exForce
    '                            End If

    '                    End If
    '                Next

    '            End If
    '            'END FOR UPDATE THE DATE OF HIGHER LEVEL APPROVER

    '        End If

    '        End If

    '        'Pinkal (06-Mar-2014) -- Start
    '        'Enhancement : TRA Changes

    '        'If mdtExpense IsNot Nothing AndAlso mdtExpense.Rows.Count > 0 Then
    '        '    Dim objExpense As New clsleaveexpense
    '        '    objExpense._Formunkid = mintFormunkid
    '        '    objExpense._dtExpense = mdtExpense
    '        '    objExpense._Userunkid = mintUserunkid
    '        '    If Not IsDBNull(objExpense._dtExpense.Rows(0)("loginemployeeunkid")) Then
    '        '        objExpense._Loginemployeeunkid = CInt(objExpense._dtExpense.Rows(0)("loginemployeeunkid"))
    '        '    End If
    '        '    If objExpense.InsertUpdateDelete_Expense(objDataOperation) = False Then
    '        '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        '        Throw exForce
    '        '    End If
    '        'End If

    '        If mintClaimRequestMasterId > 0 AndAlso mblnPaymentApprovalwithLeaveApproval AndAlso mintStatusunkid <> 2 Then

    '            Dim objClaimMst As New clsclaim_request_master
    '            objClaimMst._Crmasterunkid = mintClaimRequestMasterId

    '            If objClaimMst._Statusunkid = 2 Then

    '                Dim mstrRejecteRemark As String = ""
    '                Dim objAppr As New clsleaveapprover_master
    '                Dim objApprLevel As New clsapproverlevel_master

    '                objAppr._Approverunkid = mintApproverTranunkid
    '                objApprLevel._Levelunkid = objApprover._Levelunkid

    '                Dim objExpenseApproverTran As New clsclaim_request_approval_tran
    '                objExpenseApproverTran._DataTable = mdtExpense


    '                Dim dtApproverTable As DataTable = New DataView(dsList.Tables(0), "priority >= " & objApprLevel._Priority, "priority asc", DataViewRowState.CurrentRows).ToTable
    '                Dim mintMaxPriority As Integer = CInt(dtApproverTable.Compute("Max(priority)", "1=1"))

    '                For i As Integer = 0 To dtApproverTable.Rows.Count - 1

    '                    Dim statusunkid As Integer = mintStatusunkid
    '                    'Shani(08-Aug-2015) -- Start
    '                    'Enhancement - C&R Enhancement Given by glory(CR Revised.docx)
    '                    Dim mdtClaimApprovalDate As DateTime = Nothing
    '                    'Shani(08-Aug-2015) -- End

    '                    Dim mblnLastApproverForExpense As Boolean = False

    '                    objAppr._Approverunkid = CInt(dtApproverTable.Rows(i)("approverunkid"))

    '                    If mintApproverTranunkid = CInt(dtApproverTable.Rows(i)("approverunkid")) AndAlso statusunkid = 1 Then
    '                        statusunkid = 1
    '                        mintVisibleId = 1
    '                        'Shani(08-Aug-2015) -- Start
    '                        'Enhancement - C&R Enhancement Given by glory(CR Revised.docx)
    '                        mdtClaimApprovalDate = mdtApprovaldate
    '                        'Shani(08-Aug-2015) -- End
    '                        If mintMaxPriority = CInt(dtApproverTable.Rows(i)("priority")) Then mblnLastApproverForExpense = True
    '                        mstrRejecteRemark = ""
    '                    ElseIf mintApproverTranunkid = CInt(dtApproverTable.Rows(i)("approverunkid")) AndAlso (statusunkid = 3 OrElse statusunkid = 4) Then
    '                        statusunkid = 3
    '                        mintVisibleId = 3
    '                        'Shani(08-Aug-2015) -- Start
    '                        'Enhancement - C&R Enhancement Given by glory(CR Revised.docx)
    '                        mdtClaimApprovalDate = mdtApprovaldate
    '                        'Shani(08-Aug-2015) -- End
    '                        mblnLastApproverForExpense = True
    '                        mstrRejecteRemark = mstrRemark
    '                    Else

    '                        Dim mintPriority As Integer = -1

    '                        Dim dRow() As DataRow = dtApproverTable.Select("priority = " & objApprLevel._Priority & " AND approverunkid = " & objAppr._Approverunkid)
    '                        If dRow.Length > 0 Then
    '                            mintVisibleId = 1
    '                            statusunkid = 2
    '                        Else
    '                            mintPriority = IIf(IsDBNull(dtApproverTable.Compute("MIN(priority)", "priority >" & objApprLevel._Priority)), -1, (dtApproverTable.Compute("MIN(priority)", "priority >" & objApprLevel._Priority)))
    '                        If mintPriority <= -1 Then Continue For
    '                        If objApprLevel._Priority = CInt(dtApproverTable.Rows(i)("priority")) Then
    '                            mintVisibleId = 1
    '                        ElseIf mintPriority = CInt(dtApproverTable.Rows(i)("priority")) Then
    '                            mintVisibleId = 2
    '                        Else
    '                            mintVisibleId = -1
    '                        End If
    '                            mstrRejecteRemark = ""
    '                        statusunkid = 2
    '                        End If
    '            End If
    '                    objExpenseApproverTran._EmployeeID = mintEmployeeunkid
    '                    objExpenseApproverTran._YearId = mintYearId
    '                    If objExpenseApproverTran.Insert_Update_ApproverData(objAppr._leaveapproverunkid, objAppr._Approverunkid, statusunkid, mintVisibleId, mintUserunkid, mintClaimRequestMasterId, objDataOperation, mstrRejecteRemark, mblnLastApproverForExpense, mdtClaimApprovalDate) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '                    If (statusunkid = 3 OrElse statusunkid = 4) Then Exit For

    '                Next

    '                objExpenseApproverTran = Nothing

    '            End If
    '            objClaimMst = Nothing
    '        End If


    '        'Pinkal (15-Jul-2013) -- Start
    '        'Enhancement : TRA Changes
    '        If mdtFraction IsNot Nothing AndAlso mdtFraction.Rows.Count > 0 Then
    '            Dim objFraction As New clsleaveday_fraction
    '            objFraction._Formunkid = mintFormunkid
    '            objFraction._PendingLeaveTranId = mintPendingleavetranunkid
    '            objFraction._dtfraction = mdtFraction
    '            objFraction._Userunkid = mintUserunkid
    '            objFraction._Approverunkid = mintApproverunkid  'IT'S LEAVE APPROVER EMPLOYEE ID NOT A LEAVE APPROVER TRANUNKID
    '            If objFraction.InsertUpdateDelete_Fraction(objDataOperation) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If
    '        End If
    '        'Pinkal (15-Jul-2013) -- End


    '        'Pinkal (01-Feb-2014) -- Start
    '        'Enhancement : TRA Changes   FOR AUTOMATIC LEAVE ISSUE FOR FINAL APPROVER

    '        'Pinkal (27-May-2015) -- Start
    '        'Issue : WORKED ON AUTOMATIC LEAVE ISSUE PROBLEM CAME IN TRA.

    '        'Dim objLvform As New clsleaveform
    '        'objLvform._Formunkid = mintFormunkid

    '        Dim mintFinalStatusid As Integer = 2
    '        strQ = " SELECT @statusid =  ISNULL(statusunkid,2) from lvleaveform where isvoid = 0 and formunkid =  " & mintFormunkid
    '        objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalStatusid, ParameterDirection.InputOutput)
    '        objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '            End If

    '        mintFinalStatusid = objDataOperation.GetParameterValue("@statusid")

    '        'If objLvform._Statusunkid = 1 Then  'For Approved Leave Form

    '        If mintFinalStatusid = 1 Then  'For Approved Leave Form

    '            'If blnIsAutomaticIssueLeave <> ConfigParameter._Object._IsAutomaticIssueOnFinalApproval Then
    '            '    blnIsAutomaticIssueLeave = ConfigParameter._Object._IsAutomaticIssueOnFinalApproval
    '            'End If

    '            If mstrIsAutomaticIssueLeave.Trim.Length <= 0 Then
    '                mstrIsAutomaticIssueLeave = ConfigParameter._Object._IsAutomaticIssueOnFinalApproval
    '            End If

    '            If CBool(mstrIsAutomaticIssueLeave) Then

    '                'Pinkal (27-May-2015) -- End

    '                'Pinkal (22-Jun-2015) -- Start
    '                'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.

    '                If mblnPaymentApprovalwithLeaveApproval = False Then
    '                    If UpdateLeaveExpenseVisibility(objDataOperation, mintFormunkid, blnLeaveApproverForLeaveType, intLeavebalanceSetting) = False Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    End If
    '                End If

    '                'Pinkal (22-Jun-2015) -- End
    '                If intLeavebalanceSetting <= 0 Then
    '                    intLeavebalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
    '                End If

    '                If InsertAutomaticLeaveIssue(objDataOperation, mdtStartDate, mdtEnddate, blnLeaveApproverForLeaveType, intLeavebalanceSetting, objApprover._leaveapproverunkid, mblnPaymentApprovalwithLeaveApproval, objApprover._Approverunkid) = False Then
    '                    If mstrMessage.Trim.Length > 0 Then
    '                        Return False
    '                    Else
    '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    End If
    '                    Throw exForce
    '                End If


    '                'Pinkal (09-Jul-2014) -- Start
    '                'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2
    '                'UpdatePendingFormStatus(mintFormunkid, mintEmployeeunkid, objApprover._leaveapproverunkid,  mintUserunkid, objDataOperation)  'UPDATING LEAVE FORM STATUS APPROVED TO ISSUED
    '                UpdatePendingFormStatus(mintFormunkid, mintEmployeeunkid, objApprover._leaveapproverunkid, mintApproverTranunkid, mintUserunkid, objDataOperation)  'UPDATING LEAVE FORM STATUS APPROVED TO ISSUED
    '                'Pinkal (09-Jul-2014) -- End


    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '                _Statusunkid = 7  'ITS SET APPROVER'S STATUS APPROVE TO ISSUED
    '            End If
    '        End If





    '        objDataOperation.ReleaseTransaction(True)
    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        '   objDataOperation = Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvpendingleave_tran) </purpose>
    Public Function Insert(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                      , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                       , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                      , Optional ByVal blnIncludeInactiveEmployee As String = "", Optional ByVal strEmployeeAsOnDate As String = "") As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        ' START FOR PENDING STATUS NO ENTRY FOR PENDING LEAVE TRAN TABLE
        If mintStatusunkid = 2 And mintPendingleavetranunkid = 0 Then
            Return True
        End If
        ' END FOR PENDING STATUS NO ENTRY FOR PENDING LEAVE TRAN TABLE

        'Pinkal (02-Dec-2015) -- Start
        'Enhancement - Solving Leave bug in Self Service.
        Dim objLvform As New clsleaveform
        'Pinkal (02-Dec-2015) -- End

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If mintPendingleavetranunkid > 0 Then

                strQ = "Update lvpendingleave_tran set isvoid = 1,voiddatetime =getdate() where pendingleavetranunkid = @pending_leavetranunkid and formunkid = @form_unkid"

                objDataOperation.AddParameter("@form_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
                objDataOperation.AddParameter("@pending_leavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingleavetranunkid.ToString)
                objDataOperation.ExecNonQuery(strQ)

            End If

            If mintStatusunkid <> 2 Then
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
                objDataOperation.AddParameter("@startDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtStartDate <> Nothing, mdtStartDate, DBNull.Value))
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEnddate <> Nothing, mdtEnddate, DBNull.Value))
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtApprovaldate <> Nothing, mdtApprovaldate, DBNull.Value))
                objDataOperation.AddParameter("@transactionheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionheadunkid.ToString)
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtApprovaldate, DBNull.Value))
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@iscancelform", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCancelForm)
                objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
                objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdcDayFraction.ToString)
                objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleId.ToString)


                'Pinkal (01-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                objDataOperation.AddParameter("@relieverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelieverEmpunkid.ToString)
                'Pinkal (01-Oct-2018) -- End

                'Pinkal (01-Apr-2019) -- Start
                'Enhancement - Working on Leave Changes for NMB.
                objDataOperation.AddParameter("@escalation_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEscalationDate <> Nothing, mdtEscalationDate, DBNull.Value))
                'Pinkal (01-Apr-2019) -- End



                strQ = "INSERT INTO lvpendingleave_tran ( " & _
                  "  formunkid " & _
                  ", employeeunkid " & _
                  ", approverunkid " & _
                  ", startdate " & _
                  ", enddate " & _
                  ", approvaldate " & _
                  ", transactionheadunkid " & _
                  ", statusunkid " & _
                  ", amount " & _
                  ", remark " & _
                  ", userunkid " & _
                  ", isvoid " & _
                  ", voiddatetime " & _
                  ", voiduserunkid" & _
                  ", voidreason " & _
                 ", iscancelform " & _
                ", approvertranunkid " & _
                ", dayfraction " & _
                ", visibleid " & _
                        ", relieverempunkid " & _
                          ", escalation_date " & _
                ") VALUES (" & _
                  "  @formunkid " & _
                  ", @employeeunkid " & _
                  ", @approverunkid " & _
                  ", @startdate " & _
                  ", @enddate " & _
                  ", @approvaldate " & _
                  ", @transactionheadunkid " & _
                  ", @statusunkid " & _
                  ", @amount " & _
                  ", @remark " & _
                  ", @userunkid " & _
                  ", @isvoid " & _
                  ", @voiddatetime " & _
                  ", @voiduserunkid" & _
                  ", @voidreason " & _
                 ", @iscancelform " & _
                ", @approvertranunkid " & _
                ", @dayfraction " & _
                  ", @visibleid " & _
                          ", @relieverempunkid " & _
                          ", @escalation_date " & _
                "); SELECT @@identity"


                'Pinkal (01-Apr-2019) --  'Enhancement - Working on Leave Changes for NMB.[escalation_date]

                'Pinkal (01-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[relieverempunkid]

                dsList = objDataOperation.ExecQuery(strQ, "List")
                mintPendingleavetranunkid = dsList.Tables(0).Rows(0).Item(0)


                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "lvpendingleave_tran", "pendingleavetranunkid", mintPendingleavetranunkid, False, mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                'START FOR UPDATE THE EARING AND DEDUCTION OF THE EMPLOYEE EARNING AND DEDUCTION

                strQ = "Update prearningdeduction_master set amount = @allowance where employeeunkid = @empunkid AND tranheadunkid = @tranheadunkid"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@allowance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
                objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionheadunkid.ToString)
                objDataOperation.ExecNonQuery(strQ)

                'END FOR UPDATE THE EARING AND DEDUCTION OF THE EMPLOYEE EARNING AND DEDUCTION

            End If

            'S.SANDEEP [30 JAN 2016] -- START
            'COMMENTED ON PINKAL'S REQUEST -- NO NEED
            'dsList = GetEmployeeWiseApproverLevels(objDataOperation, mintEmployeeunkid, mintApproverunkid)

            'If dsList.Tables("List").Rows.Count > 0 Then

            '    Dim objApprover As New clsleaveapprover_master
            '    If blnIncludeInactiveEmployee.Trim.Length <= 0 Then
            '        blnIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString()
            '    End If


            '    'Pinkal (24-Aug-2015) -- Start
            '    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

            '    'Dim dsApprover As DataSet = objApprover.GetList("List", True, blnIncludeInactiveEmployee, strEmployeeAsOnDate)
            '    'Dim dtList As DataTable = New DataView(dsApprover.Tables("List"), "leaveapproverunkid =" & mintApproverunkid, "", DataViewRowState.CurrentRows).ToTable
            '    'If dtList.Rows.Count > 0 Then
            '    '    objApprover._Approverunkid = CInt(dtList.Rows(0)("approverunkid"))
            '    'End If

            '    Dim dsApprover As DataSet = objApprover.GetList("List", xDatabaseName, xUserUnkid _
            '                                                                             , xYearUnkid, xCompanyUnkid, strEmployeeAsOnDate _
            '                                                                             , xUserModeSetting, True, blnIncludeInactiveEmployee, xOnlyApproved, False, -1 _
            '                                                                             , objDataOperation, "", "")
            'Dim dtList As DataTable = New DataView(dsApprover.Tables("List"), "leaveapproverunkid =" & mintApproverunkid, "", DataViewRowState.CurrentRows).ToTable
            'If dtList.Rows.Count > 0 Then
            '    objApprover._Approverunkid = CInt(dtList.Rows(0)("approverunkid"))
            'End If


            '    'Pinkal (24-Aug-2015) -- End

            '    Dim objLevel As New clsapproverlevel_master
            '    objLevel._Levelunkid = objApprover._Levelunkid


            '    If CInt(dsList.Tables("List").Compute("Max(priority)", "")) = objLevel._Priority Then

            '        'START FOR UPDATE LEAVE FORM APPROVED START DATE,END DATE AND APPROVED DAYS
            '        Dim mblnApproved As Boolean = False
            '        If mintStatusunkid = 1 Then
            '            mblnApproved = True
            '        End If

            '        'Pinkal (02-Dec-2015) -- Start
            '        'Enhancement - Solving Leave bug in Self Service.
            '        'If clsleaveform.ApprovedLeaveFormtenure(objDataOperation, mintFormunkid, mdtStartDate, mdtEnddate, mdcDayFraction, mblnApproved, mintUserunkid) = False Then
            '        '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        '    Throw exForce
            '        'End If
            '        If objLvform.ApprovedLeaveFormtenure(objDataOperation, mintFormunkid, mdtStartDate, mdtEnddate, mdcDayFraction, mblnApproved, mintUserunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            '        'Pinkal (02-Dec-2015) -- End

            '        'END FOR UPDATE LEAVE FORM APPROVED START DATE,END DATE AND APPROVED DAYS


            '        ' START FOR UPDATE THE STATUS OF THE LEAVE FORM

            '        'Pinkal (02-Dec-2015) -- Start
            '        'Enhancement - Solving Leave bug in Self Service.
            '        'clsleaveform.UpdateLeaveFormStatus(objDataOperation, mintFormunkid, mintStatusunkid, mintUserunkid)
            '        objLvform.UpdateLeaveFormStatus(objDataOperation, mintFormunkid, mintStatusunkid, mintUserunkid)
            '        'Pinkal (02-Dec-2015) -- End


            '        If objDataOperation.ErrorMessage <> "" Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '        ' END FOR UPDATE THE STATUS OF THE LEAVE FORM

            '    End If

            'End If
            'S.SANDEEP [30 JAN 2016] -- END

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            '   objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> UPDATE INTO Database Table (lvpendingleave_tran) </purpose>
    Public Function Update(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                      , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                      , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                      , ByVal dtCurrentDateAndTime As DateTime _
                                      , Optional ByVal blnIncludeInactiveEmployee As String = "" _
                                      , Optional ByVal strEmployeeAsOnDate As String = "", Optional ByVal mdtDbstartdate As Date = Nothing, Optional ByVal mdtDbEnddate As Date = Nothing _
                                      , Optional ByVal blnLeaveApproverForLeaveType As String = "", Optional ByVal intLeavebalanceSetting As Integer = -1, Optional ByVal mstrIsAutomaticIssueLeave As String = "" _
                                      , Optional ByVal blnSkipEmployeeMovementApprovalFlow As Boolean = True _
                                      , Optional ByVal blnCreateADUserFromEmpMst As Boolean = False _
                                      ) As Boolean


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        mstrMessage = ""
        Dim objApprover As New clsleaveapprover_master
        Dim objLvform As New clsleaveform

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            Dim objConfig As New clsConfigOptions
            Dim xLeaveApplicationEscalationSettings As Integer = 0
            Dim mstrLeaveApplicationEscalationSettings As String = objConfig.GetKeyValue(xCompanyUnkid, "LeaveApplicationEscalationSettings").ToString().Trim()
            If mstrLeaveApplicationEscalationSettings.Trim.Length > 0 Then xLeaveApplicationEscalationSettings = mstrLeaveApplicationEscalationSettings.Trim()
            objConfig = Nothing

            If mintStatusunkid <> 2 Then
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@pendingleavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingleavetranunkid.ToString)
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
                objDataOperation.AddParameter("@startDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtStartDate <> Nothing, mdtStartDate, DBNull.Value))
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEnddate <> Nothing, mdtEnddate, DBNull.Value))
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtApprovaldate <> Nothing, mdtApprovaldate, DBNull.Value))
                objDataOperation.AddParameter("@transactionheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionheadunkid.ToString)
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtApprovaldate, DBNull.Value))
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@iscancelform", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCancelForm)
                objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
                objDataOperation.AddParameter("@dayFraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdcDayFraction.ToString)
                objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleId.ToString)
                objDataOperation.AddParameter("@relieverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelieverEmpunkid.ToString)
                objDataOperation.AddParameter("@escalation_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEscalationDate <> Nothing, mdtEscalationDate, DBNull.Value))


                strQ = "Update lvpendingleave_tran set " & _
                  "  formunkid = formunkid " & _
                  ", employeeunkid = @employeeunkid " & _
                  ", approverunkid = @approverunkid" & _
                  ", startdate = @startdate" & _
                  ", enddate = @enddate" & _
                  ", approvaldate = @approvaldate" & _
                  ", transactionheadunkid = @transactionheadunkid" & _
                  ", statusunkid = @statusunkid" & _
                  ", amount = @amount" & _
                  ", remark = @remark" & _
                  ", userunkid = @userunkid" & _
                  ", isvoid = @isvoid" & _
                  ", voiddatetime = @voiddatetime" & _
                  ", voiduserunkid = @voiduserunkid" & _
                  ", voidreason = @voidreason" & _
                ", iscancelform = @iscancelform " & _
                           ", approvertranunkid = @approvertranunkid " & _
                  ", dayFraction = @dayFraction " & _
                  ", visibleid = @visibleid " & _
                          ", relieverempunkid = @relieverempunkid " & _
                          ", escalation_date  = @escalation_date " & _
                  "  WHERE pendingleavetranunkid = @pendingleavetranunkid"

                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lvpendingleave_tran", "pendingleavetranunkid", mintPendingleavetranunkid, False, mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'START FOR UPDATE THE EARING AND DEDUCTION OF THE EMPLOYEE EARNING AND DEDUCTION

                If mdecAmount > 0 Then

                strQ = "Update prearningdeduction_master set amount = @allowance where employeeunkid = @empunkid AND tranheadunkid = @tranheadunkid"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@allowance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
                objDataOperation.AddParameter("@empunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionheadunkid.ToString)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                End If

                'END FOR UPDATE THE EARING AND DEDUCTION OF THE EMPLOYEE EARNING AND DEDUCTION


                dsList = GetEmployeeApproverListWithPriority(mintEmployeeunkid, mintFormunkid, objDataOperation)

                If dsList.Tables("List").Rows.Count > 0 Then
                    objApprover = New clsleaveapprover_master

                    If blnIncludeInactiveEmployee.Trim.Length <= 0 Then
                        blnIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString()
                    End If

                    Dim dsApprover As DataSet = dsList.Copy()
                    Dim dtList As DataTable = New DataView(dsApprover.Tables("List"), "leaveapproverunkid =" & mintApproverunkid & " AND approverunkid = " & mintApproverTranunkid, "", DataViewRowState.CurrentRows).ToTable

                    If dtList.Rows.Count > 0 Then
                        objApprover._DoOperation = objDataOperation
                        objApprover._Approverunkid = CInt(dtList.Rows(0)("approverunkid"))
                    End If

                    Dim objLevel As New clsapproverlevel_master
                    objLevel._DoOperation = objDataOperation
                    objLevel._Levelunkid = objApprover._Levelunkid


                    strQ = " UPDATE lvpendingleave_tran set " & _
                              " visibleid = " & mintStatusunkid & _
                              " WHERE  formunkid = @formunkid and employeeunkid = @employeeunkid AND approvertranunkid = @approvertranunkid "

                    If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then

                        Dim dtVisibility As DataTable = New DataView(dsApprover.Tables(0), "priority = " & objLevel._Priority, "", DataViewRowState.CurrentRows).ToTable

                        For i As Integer = 0 To dtVisibility.Rows.Count - 1

                            If objLevel._Priority = CInt(dtVisibility.Rows(i)("priority")) Then
                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid)
                                objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtVisibility.Rows(i)("approverunkid")))
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            End If

                        Next

                        Dim intMinPriority As Integer = IIf(IsDBNull(dsApprover.Tables(0).Compute("Min(priority)", "priority > " & objLevel._Priority)), -1, dsApprover.Tables(0).Compute("Min(priority)", "priority > " & objLevel._Priority))

                        dtVisibility = New DataView(dsApprover.Tables(0), "priority = " & intMinPriority & " AND priority <> -1", "", DataViewRowState.CurrentRows).ToTable

                        If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then

                            If mintStatusunkid = 1 Then   'ONLY APPROVED STATUS

                                strQ = " UPDATE lvpendingleave_tran set " & _
                                          " visibleid =  2 " & _
                                          ", escalation_date = @escalation_date " & _
                                          " WHERE  formunkid = @formunkid and employeeunkid = @employeeunkid AND approvertranunkid = @approvertranunkid "


                            ElseIf mintStatusunkid = 3 OrElse mintStatusunkid = 4 Then   ' REJECTED OR RE-SCHEDULED STATUS
                                strQ = " UPDATE lvpendingleave_tran set " & _
                                          " visibleid =  -1 " & _
                                          " WHERE  formunkid = @formunkid and employeeunkid = @employeeunkid AND approvertranunkid = @approvertranunkid "
                            End If

                            For i As Integer = 0 To dtVisibility.Rows.Count - 1
                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid)
                                objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtVisibility.Rows(i)("approverunkid")))

                                If mintStatusunkid = 1 AndAlso xLeaveApplicationEscalationSettings > 0 Then
                                    mdtEscalationDate = mdtApprovaldate.AddDays(CInt(dtVisibility.Rows(i)("escalation_days"))).AddDays(1).Date
                                Else
                                    mdtEscalationDate = Nothing
                                End If

                                objDataOperation.AddParameter("@escalation_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEscalationDate <> Nothing, mdtEscalationDate, DBNull.Value))
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Next

                        End If

                    End If

                    If CInt(dsList.Tables("List").Compute("Max(priority)", "1=1")) = objLevel._Priority Or mintStatusunkid = 3 Or mintStatusunkid = 4 Then ' If CInt(dsList.Tables("List").Compute("Max(priority)", "1=1")) = objLevel._Priority Or mintStatusunkid = 3 Or mintStatusunkid = 4 Then

                        ' START FOR VOID THE ISSUE LEAVE WHEN LEAVE STATUS IS REJECT OR RE-SCHEDULE

                        If mintStatusunkid = 3 Or mintStatusunkid = 4 Then  ' If mintStatusunkid = 3 Or mintStatusunkid = 4 Then


                            ' START FOR REJECT CLAIM FORM FOR ALL APPROVERS WHEN LEAVE ISSUE IS AUTOMATIC AND PAYMENT APPROVAL FOR CLAIM IS SERIAL.

                            If mblnPaymentApprovalwithLeaveApproval = False AndAlso mintClaimRequestMasterId > 0 Then

                                Dim objExpenseApprover As New clsExpenseApprover_Master
                                Dim dsClaimApprover As DataSet = objExpenseApprover.GetEmployeeApprovers(enExpenseType.EXP_LEAVE, mintEmployeeunkid, "List", objDataOperation)
                                If dsClaimApprover IsNot Nothing AndAlso dsClaimApprover.Tables(0).Rows.Count > 0 Then
                                    Dim objExpenseApproverTran As New clsclaim_request_approval_tran
                                    objExpenseApproverTran._EmployeeID = mintEmployeeunkid
                                    objExpenseApproverTran._YearId = mintYearId

                                    Dim drClaimApprover() As DataRow = dsClaimApprover.Tables(0).Select("crpriority = MAX(crpriority)")
                                    Dim mintMaxClaimApproverID As Integer = -1
                                    If drClaimApprover.Length > 0 Then
                                        mintMaxClaimApproverID = CInt(drClaimApprover(0)("crapproverunkid"))
                                    End If

                                    Dim mblnLastClaimApprover As Boolean = False
                                    For i As Integer = 0 To dsClaimApprover.Tables(0).Rows.Count - 1
                                        objExpenseApproverTran._DataTable.Rows.Clear()
                                        If CInt(dsClaimApprover.Tables(0).Rows(i)("crapproverunkid")) = mintMaxClaimApproverID Then mblnLastClaimApprover = True
                                        objExpenseApproverTran._DataTable = New DataView(mdtExpense, "crapproverunkid = " & CInt(dsClaimApprover.Tables(0).Rows(i)("crapproverunkid")), "", DataViewRowState.CurrentRows).ToTable

                                        If objExpenseApproverTran.Insert_Update_ApproverData(CInt(dsClaimApprover.Tables(0).Rows(i)("employeeunkid")), CInt(dsClaimApprover.Tables(0).Rows(i)("crapproverunkid")), IIf(mintStatusunkid = 4, 3, mintStatusunkid), IIf(mintStatusunkid = 4, 3, mintStatusunkid), mintUserunkid, dtCurrentDateAndTime, mintClaimRequestMasterId, objDataOperation, mstrRemark, mblnLastClaimApprover, mdtApprovaldate) = False Then
                                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            Throw exForce
                                        End If
                                    Next

                                    objExpenseApproverTran = Nothing

                                End If

                                objExpenseApprover = Nothing

                            End If

                            ' END FOR REJECT CLAIM FORM FOR ALL APPROVERS WHEN LEAVE ISSUE IS AUTOMATIC AND PAYMENT APPROVAL FOR CLAIM IS SERIAL.

                        ElseIf mintStatusunkid = 1 Then  ' If mintStatusunkid = 3 Or mintStatusunkid = 4 Then

                            Dim drApprover() As DataRow = dsApprover.Tables("List").Select("priority = " & objLevel._Priority)
                            If drApprover.Length > 0 Then


                                strQ = "Update lvpendingleave_tran set " & _
                                           " startdate = @startdate " & _
                                           ",enddate = @enddate " & _
                                           ",dayfraction = @dayfraction " & _
                                           ",relieverempunkid = @relieverempunkid " & _
                                           " WHERE approverunkid = @approverunkid " & _
                                           " AND formunkid = @formunkid and employeeunkid = @employeeunkid"


                                For i As Integer = 0 To drApprover.Length - 1
                                    objDataOperation.ClearParameters()
                                    objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drApprover(i)("leaveapproverunkid")))
                                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                                    objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid)
                                    objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartDate)
                                    objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
                                    objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdcDayFraction)
                                    objDataOperation.AddParameter("@relieverempunkid", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mintRelieverEmpunkid)

                                    objDataOperation.ExecNonQuery(strQ)

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                Next

                            End If

                        End If  ' If mintStatusunkid = 3 Or mintStatusunkid = 4 Then


                        'START FOR UPDATE LEAVE FORM APPROVED START DATE,END DATE AND APPROVED DAYS

                        Dim mblnApproved As Boolean = False
                        If mintStatusunkid = 1 Then
                            mblnApproved = True
                        End If

                        If objLvform.ApprovedLeaveFormtenure(objDataOperation, mintFormunkid, mdtStartDate, mdtEnddate, mdcDayFraction, mblnApproved, mintUserunkid) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'END FOR UPDATE LEAVE FORM APPROVED START DATE,END DATE AND APPROVED DAYS


                        ' START FOR UPDATE THE STATUS OF THE LEAVE FORM
                        objLvform.UpdateLeaveFormStatus(objDataOperation, mintFormunkid, mintStatusunkid, mintUserunkid)
                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        ' END FOR UPDATE THE STATUS OF THE LEAVE FORM


                        If CBool(mstrIsAutomaticIssueLeave) = False AndAlso mblnPaymentApprovalwithLeaveApproval = False AndAlso mintClaimRequestMasterId > 0 AndAlso mintStatusunkid = 1 Then
                            If UpdateLeaveExpenseVisibility(objDataOperation, mintFormunkid, blnLeaveApproverForLeaveType, intLeavebalanceSetting, xDatabaseName, strEmployeeAsOnDate) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If

                        'START FOR UPDATE THE DATE OF HIGHER LEVEL APPROVER 


                    ElseIf CInt(dsList.Tables("List").Compute("Max(priority)", "1=1")) > objLevel._Priority Then  ' If CInt(dsList.Tables("List").Compute("Max(priority)", "1=1")) = objLevel._Priority Or mintStatusunkid = 3 Or mintStatusunkid = 4 Then


                        strQ = "Update lvpendingleave_tran set " & _
                               " startdate = @startdate " & _
                               ",enddate = @enddate " & _
                               ",dayfraction = @dayfraction " & _
                               ",relieverempunkid = @relieverempunkid " & _
                               " WHERE approverunkid = @approverunkid " & _
                               " AND formunkid = @formunkid and employeeunkid = @employeeunkid"

                        For i As Integer = 0 To dsApprover.Tables("List").Rows.Count - 1

                            If objApprover._leaveapproverunkid = CInt(dsApprover.Tables("List").Rows(i)("leaveapproverunkid")) Then Continue For

                            If objLevel._Priority <= CInt(dsApprover.Tables("List").Rows(i)("priority")) Then
                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsApprover.Tables("List").Rows(i)("leaveapproverunkid")))
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid)
                                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartDate)
                                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
                                objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdcDayFraction)
                                objDataOperation.AddParameter("@relieverempunkid", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mintRelieverEmpunkid)
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            End If
                        Next

                    End If
                    'END FOR UPDATE THE DATE OF HIGHER LEVEL APPROVER

                End If

            End If


            If mintClaimRequestMasterId <= 0 AndAlso mblnPaymentApprovalwithLeaveApproval AndAlso objClaimRMst IsNot Nothing AndAlso mdtExpense IsNot Nothing Then
                objClaimRMst._ApproverTranID = mintApproverTranunkid
                objClaimRMst._ApproverID = mintApproverunkid
                If objClaimRMst.Insert(xDatabaseName, xYearUnkid, xCompanyUnkid, mdtApprovaldate, blnIncludeInactiveEmployee, mdtExpense, dtCurrentDateAndTime, mdtClaimAttchment, objDataOperation, mblnPaymentApprovalwithLeaveApproval) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                mintClaimRequestMasterId = objClaimRMst._Crmasterunkid

                Dim objExpApproverTran As New clsclaim_request_approval_tran
                mdtExpense = objExpApproverTran.GetApproverExpesneList("List", False, mblnPaymentApprovalwithLeaveApproval, xDatabaseName _
                                                                                                     , mintUserunkid, eZeeDate.convertDate(mdtApprovaldate), enExpenseType.EXP_LEAVE _
                                                                                                     , False, True, mintApproverTranunkid, "", mintClaimRequestMasterId, objDataOperation).Tables(0)


                Dim drRow() As DataRow = mdtExpense.Select("AUD <> 'D'")
                If drRow.Length > 0 Then
                    For Each dr In drRow
                        dr("AUD") = "U"
                        dr.AcceptChanges()
                    Next
                End If

                objExpApproverTran = Nothing

            End If

            If mintClaimRequestMasterId > 0 AndAlso mblnPaymentApprovalwithLeaveApproval AndAlso mintStatusunkid <> 2 Then

                Dim objClaimMst As New clsclaim_request_master
                objClaimMst._DoOperation = objDataOperation
                objClaimMst._Crmasterunkid = mintClaimRequestMasterId

                If objClaimMst._Statusunkid = 2 Then

                    Dim mstrRejecteRemark As String = ""

                    Dim objAppr As New clsleaveapprover_master
                    Dim objApprLevel As New clsapproverlevel_master
                    objAppr._DoOperation = objDataOperation
                    objAppr._Approverunkid = mintApproverTranunkid
                    objApprLevel._DoOperation = objDataOperation
                    objApprLevel._Levelunkid = objApprover._Levelunkid

                    Dim objExpenseApproverTran As New clsclaim_request_approval_tran
                    objExpenseApproverTran._DataTable = mdtExpense


                    Dim dtApproverTable As DataTable = New DataView(dsList.Tables(0), "priority >= " & objApprLevel._Priority, "priority asc", DataViewRowState.CurrentRows).ToTable
                    Dim mintMaxPriority As Integer = CInt(dtApproverTable.Compute("Max(priority)", "1=1"))

                    For i As Integer = 0 To dtApproverTable.Rows.Count - 1

                        Dim statusunkid As Integer = mintStatusunkid
                        Dim mdtClaimApprovalDate As DateTime = Nothing
                        Dim mblnLastApproverForExpense As Boolean = False
                        objAppr._DoOperation = objDataOperation
                        objAppr._Approverunkid = CInt(dtApproverTable.Rows(i)("approverunkid"))

                        If mintApproverTranunkid = CInt(dtApproverTable.Rows(i)("approverunkid")) AndAlso statusunkid = 1 Then
                            statusunkid = 1
                            mintVisibleId = 1
                            mdtClaimApprovalDate = mdtApprovaldate
                            If mintMaxPriority = CInt(dtApproverTable.Rows(i)("priority")) Then mblnLastApproverForExpense = True
                            mstrRejecteRemark = ""

                        ElseIf mintApproverTranunkid = CInt(dtApproverTable.Rows(i)("approverunkid")) AndAlso (statusunkid = 3 OrElse statusunkid = 4) Then
                            statusunkid = 3
                            mintVisibleId = 3
                            mdtClaimApprovalDate = mdtApprovaldate
                            mblnLastApproverForExpense = True
                            mstrRejecteRemark = mstrRemark
                        Else

                            Dim mintPriority As Integer = -1

                            Dim dRow() As DataRow = dtApproverTable.Select("priority = " & objApprLevel._Priority & " AND approverunkid = " & objAppr._Approverunkid)
                            If dRow.Length > 0 Then
                                mintVisibleId = 1
                                statusunkid = 2
                            Else
                                mintPriority = IIf(IsDBNull(dtApproverTable.Compute("MIN(priority)", "priority >" & objApprLevel._Priority)), -1, (dtApproverTable.Compute("MIN(priority)", "priority >" & objApprLevel._Priority)))
                                If mintPriority <= -1 Then Continue For
                                If objApprLevel._Priority = CInt(dtApproverTable.Rows(i)("priority")) Then
                                    mintVisibleId = 1
                                ElseIf mintPriority = CInt(dtApproverTable.Rows(i)("priority")) Then
                                    mintVisibleId = 2
                                Else
                                    mintVisibleId = -1
                                End If
                                mstrRejecteRemark = ""
                                statusunkid = 2
                            End If
                        End If

                        objExpenseApproverTran._EmployeeID = mintEmployeeunkid
                        objExpenseApproverTran._YearId = mintYearId
                        objExpenseApproverTran._WebFormName = mstrWebFrmName
                        objExpenseApproverTran._WebClientIP = mstrWebClientIP
                        objExpenseApproverTran._WebHostName = mstrWebHostName

                        If objExpenseApproverTran.Insert_Update_ApproverData(objAppr._leaveapproverunkid, objAppr._Approverunkid, statusunkid, mintVisibleId, mintUserunkid, dtCurrentDateAndTime, mintClaimRequestMasterId, objDataOperation, mstrRejecteRemark, mblnLastApproverForExpense, mdtClaimApprovalDate) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If (statusunkid = 3 OrElse statusunkid = 4) Then Exit For

                    Next

                    objExpenseApproverTran = Nothing

                End If
                objClaimMst = Nothing
            End If


            If mdtFraction IsNot Nothing AndAlso mdtFraction.Rows.Count > 0 Then
                Dim objFraction As New clsleaveday_fraction
                objFraction._Formunkid = mintFormunkid
                objFraction._PendingLeaveTranId = mintPendingleavetranunkid
                objFraction._dtfraction = mdtFraction
                objFraction._Userunkid = mintUserunkid
                objFraction._Approverunkid = mintApproverunkid  'IT'S LEAVE APPROVER EMPLOYEE ID NOT A LEAVE APPROVER TRANUNKID
                If objFraction.InsertUpdateDelete_Fraction(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            Dim mintFinalStatusid As Integer = 2
            strQ = " SELECT @statusid =  ISNULL(statusunkid,2) from lvleaveform where isvoid = 0 and formunkid =  " & mintFormunkid
            objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalStatusid, ParameterDirection.InputOutput)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintFinalStatusid = objDataOperation.GetParameterValue("@statusid")

            If mintFinalStatusid = 1 Then  'For Approved Leave Form

                If mstrIsAutomaticIssueLeave.Trim.Length <= 0 Then
                    mstrIsAutomaticIssueLeave = ConfigParameter._Object._IsAutomaticIssueOnFinalApproval
                End If

                If CBool(mstrIsAutomaticIssueLeave) Then

                    If mblnPaymentApprovalwithLeaveApproval = False Then

                        If UpdateLeaveExpenseVisibility(objDataOperation, mintFormunkid, blnLeaveApproverForLeaveType, intLeavebalanceSetting, xDatabaseName, strEmployeeAsOnDate) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        End If

                    End If

                    If intLeavebalanceSetting <= 0 Then
                        intLeavebalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
                    End If

                    If InsertAutomaticLeaveIssue(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                            , xUserModeSetting, xOnlyApproved, objDataOperation, mdtStartDate, mdtEnddate _
                                                            , blnLeaveApproverForLeaveType, intLeavebalanceSetting, objApprover._leaveapproverunkid _
                                                        , mblnPaymentApprovalwithLeaveApproval, objApprover._Approverunkid, strEmployeeAsOnDate, blnIncludeInactiveEmployee, objApprover._Isexternalapprover, blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst) = False Then

                        If mstrMessage.Trim.Length > 0 Then
                            exForce = New Exception(mstrMessage)
                        Else
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        End If
                        Throw exForce
                    End If

                    UpdatePendingFormStatus(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, strEmployeeAsOnDate, xUserModeSetting, True, blnIncludeInactiveEmployee, mintFormunkid, mintEmployeeunkid, objApprover._leaveapproverunkid, mintApproverTranunkid, mintUserunkid, objDataOperation)   'UPDATING LEAVE FORM STATUS APPROVED TO ISSUED

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    _Statusunkid = 7  'ITS SET APPROVER'S STATUS APPROVE TO ISSUED
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Dim objlvFrm As New clsleaveform
            objlvFrm._Formunkid = mintFormunkid
            Call objlvFrm.BlockEmployeeInCBS(mintEmployeeunkid, mdtStartDate, mdtEnddate, objlvFrm._Formno, dtCurrentDateAndTime, xCompanyUnkid, "D", objDataOperation, xUserUnkid, 0)
            objlvFrm = Nothing

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> UPDATE INTO Database Table (lvpendingleave_tran) </purpose>
    Public Function Update(ByVal objDataOperation As clsDataOperation) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@startDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtStartDate <> Nothing, mdtStartDate, DBNull.Value))
            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEnddate <> Nothing, mdtEnddate, DBNull.Value))
            objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtApprovaldate <> Nothing, mdtApprovaldate, DBNull.Value))
            objDataOperation.AddParameter("@transactionheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactionheadunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtApprovaldate, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@iscancelform", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCancelForm.ToString)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
            objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdcDayFraction.ToString)
            objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleId.ToString)


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@relieverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelieverEmpunkid.ToString)
            'Pinkal (01-Oct-2018) -- End

            'Pinkal (01-Apr-2019) -- Start
            'Enhancement - Working on Leave Changes for NMB.
            objDataOperation.AddParameter("@escalation_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEscalationDate <> Nothing, mdtEscalationDate, DBNull.Value))
            'Pinkal (01-Apr-2019) -- End


            strQ = "Update lvpendingleave_tran set " & _
              " employeeunkid = @employeeunkid " & _
              ", approverunkid = @approverunkid" & _
              ", startdate = @startdate" & _
              ", enddate = @enddate" & _
              ", approvaldate = @approvaldate" & _
              ", transactionheadunkid = @transactionheadunkid" & _
              ", statusunkid = @statusunkid" & _
              ", amount = @amount" & _
              ", remark = @remark" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidreason = @voidreason" & _
              ", iscancelform = @iscancelform " & _
                      ", approvertranunkid = @approvertranunkid" & _
                      ", dayfraction = @dayfraction " & _
              ", visibleid = @visibleid " & _
                      ", relieverempunkid = @relieverempunkid " & _
                      ", escalation_date = @escalation_date " & _
              "  WHERE formunkid = @formunkid"

            'Pinkal (01-Apr-2019) -- 'Enhancement - Working on Leave Changes for NMB.[", escalation_date = @escalation_date " & _]

            'Pinkal (01-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[  ", relieverempunkid = @relieverempunkid " & _]


            If mintApproverunkid > 0 Then

                'Pinkal (08-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                'strQ &= " AND approverunkid = @approverunkid "
                strQ &= " AND approverunkid = @approverunkid AND  approvertranunkid = @approvertranunkid"
                'Pinkal (08-Oct-2018) -- End


            End If

            objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            'strQ = " Select Isnull(pendingleavetranunkid,0) pendingleavetranunkid from lvpendingleave_tran where formunkid = @formunkid AND approverunkid = @approverunkid "
            strQ = " Select Isnull(pendingleavetranunkid,0) pendingleavetranunkid from lvpendingleave_tran where formunkid = @formunkid AND approverunkid = @approverunkid  AND  approvertranunkid = @approvertranunkid  "
            'Pinkal (08-Oct-2018) -- End


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)

            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
            'Pinkal (08-Oct-2018) -- End

            Dim dtList As DataSet = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dtList.Tables.Count > 0 AndAlso dtList.Tables(0).Rows.Count > 0 Then
                mintPendingleavetranunkid = CInt(dtList.Tables(0).Rows(0)("pendingleavetranunkid"))
            End If


            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "lvpendingleave_tran", mintPendingleavetranunkid, "pendingleavetranunkid", 2, objDataOperation) Then

                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lvpendingleave_tran", "pendingleavetranunkid", mintPendingleavetranunkid, False, mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            If mdtExpense IsNot Nothing AndAlso mdtExpense.Rows.Count > 0 Then
                Dim objExpense As New clsleaveexpense
                objExpense._Formunkid = mintFormunkid
                objExpense._dtExpense = mdtExpense
                objExpense._Userunkid = mintUserunkid
                If Not IsDBNull(objExpense._dtExpense.Rows(0)("loginemployeeunkid")) Then
                    objExpense._Loginemployeeunkid = CInt(objExpense._dtExpense.Rows(0)("loginemployeeunkid"))
                End If
                If objExpense.InsertUpdateDelete_Expense(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


            If mdtFraction IsNot Nothing AndAlso mdtFraction.Rows.Count > 0 Then
                Dim objFraction As New clsleaveday_fraction
                objFraction._Formunkid = mintFormunkid
                objFraction._PendingLeaveTranId = mintPendingleavetranunkid
                objFraction._dtfraction = mdtFraction
                objFraction._Userunkid = mintUserunkid
                objFraction._Approverunkid = mintApproverunkid  'IT'S LEAVE APPROVER EMPLOYEE ID NOT A LEAVE APPROVER TRANUNKID
                If objFraction.InsertUpdateDelete_Fraction(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@pendingleavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP [30 JAN 2016] -- START
    'COMMENTED ON PINKAL'S REQUEST -- NO NEED
    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetEmployeeWiseApproverLevels(ByVal objDataOperation As clsDataOperation, ByVal intEmployeeunkid As Integer, Optional ByVal intLeaveApproverunkid As Integer = -1) As DataSet
    '    Dim strQ As String = ""
    '    Dim dsList As DataSet = Nothing
    '    Try

    '        If objDataOperation Is Nothing Then
    '            objDataOperation = New clsDataOperation
    '        End If

    '        objDataOperation.ClearParameters()

    '        strQ = "  SELECT lvapproverlevel_master.priority " & _
    '               ", lvapproverlevel_master.ismandatory " & _
    '               " FROM lvapproverlevel_master " & _
    '                  " JOIN lvleaveapprover_master ON lvleaveapprover_master.levelunkid = lvapproverlevel_master.levelunkid " & _
    '                  " AND lvleaveapprover_master.isvoid = 0 "

    '        If intLeaveApproverunkid > 0 Then
    '            strQ &= " AND lvleaveapprover_master.leaveapproverunkid = @leaveapproverunkid"
    '            objDataOperation.AddParameter("@leaveapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveApproverunkid)
    '        End If

    '        strQ &= " UNION " & _
    '               " SELECT lvapproverlevel_master.priority, lvapproverlevel_master.ismandatory " & _
    '               " FROM lvapproverlevel_master " & _
    '               " JOIN lvleaveapprover_master ON lvleaveapprover_master.levelunkid = lvapproverlevel_master.levelunkid " & _
    '               " JOIN lvleaveapprover_tran ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid " & _
    '               " WHERE lvleaveapprover_tran.employeeunkid = @employeeunkid  AND lvleaveapprover_master.isvoid = 0 "


    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
    '        dsList = objDataOperation.ExecQuery(strQ, "List")


    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetEmployeeWiseApproverLevels", mstrModuleName)
    '    End Try
    '    Return dsList
    'End Function
    'S.SANDEEP [30 JAN 2016] -- START

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmployeePendingForms(ByVal intEmployeeId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "select count(*) RecordCount from lvpendingleave_tran where employeeunkid = @employeeid and statusunkid = 2 and isvoid = 0"
            objDataOperation.AddParameter("@employeeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                If CInt(dsList.Tables(0).Rows(0)("RecordCount")) > 0 Then Return True
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeePendingForms", mstrModuleName)
        End Try
        Return False
    End Function


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Sub UpdatePendingFormStatus(ByVal intFormunkid As Integer, ByVal intEmployeeunkid As Integer, ByVal intApproverunkid As Integer, ByVal intApprovertranunkid As Integer, Optional ByVal intUserId As Integer = 0 _
    '                                                      , Optional ByVal objDOpearation As clsDataOperation = Nothing)
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception


    '    'Pinkal (01-Feb-2014) -- Start
    '    'Enhancement : TRA Changes
    '    Dim objDataOperation As New clsDataOperation
    '    If objDOpearation Is Nothing Then
    '        objDataOperation.BindTransaction()
    '    Else
    '        objDataOperation = objDOpearation
    '    End If

    '    'Pinkal (01-Feb-2014) -- End

    '    Try

    '        objDataOperation.ClearParameters()



    '        'Pinkal (09-Jul-2014) -- Start
    '        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2

    '        strQ = "Update lvpendingleave_tran set statusunkid = 7 where formunkid = @formunkid and employeeunkid = @employeeunkid and approverunkid = @approverunkid AND approvertranunkid = @approvertranunkid AND isvoid = 0 "
    '        objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
    '        objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverunkid)
    '        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApprovertranunkid)
    '        objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        strQ = " Select Isnull(pendingleavetranunkid,0) pendingleavetranunkid from lvpendingleave_tran where formunkid = @formunkid AND approverunkid = @approverunkid and employeeunkid = @employeeunkid"
    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid.ToString)
    '        objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverunkid.ToString)
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
    '        Dim dtList As DataSet = objDataOperation.ExecQuery(strQ, "List")

    '        If dtList.Tables.Count > 0 AndAlso dtList.Tables(0).Rows.Count > 0 Then
    '            mintPendingleavetranunkid = CInt(dtList.Tables(0).Rows(0)("pendingleavetranunkid"))
    '        End If


    '        If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lvpendingleave_tran", "pendingleavetranunkid", mintPendingleavetranunkid, False, intUserId) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        Dim objApprover As New clsleaveapprover_master
    '        Dim blnIncludeInactiveEmployee As String = ""

    '        If blnIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            blnIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString()
    '        End If

    '        Dim dsApprover As DataSet = Nothing
    '        If UserAccessLevel._AccessLevelFilterString.Trim.Contains("(0)") Then
    '            dsApprover = objApprover.GetList("List", True, blnIncludeInactiveEmployee, "", intUserId, "AND 1=1")
    '        Else
    '            dsApprover = objApprover.GetList("List", True, blnIncludeInactiveEmployee, "", intUserId)
    '        End If


    '        Dim dtApproverList As DataTable = New DataView(dsApprover.Tables("List"), "leaveapproverunkid =" & intApproverunkid & " AND approverunkid = " & intApprovertranunkid, "", DataViewRowState.CurrentRows).ToTable

    '        dsApprover = Nothing

    '        If dtApproverList.Rows.Count > 0 Then
    '            objApprover._Approverunkid = CInt(dtApproverList.Rows(0)("approverunkid"))
    '        End If

    '        Dim objLevel As New clsapproverlevel_master
    '        objLevel._Levelunkid = objApprover._Levelunkid


    '        dsList = GetList("List", True, blnIncludeInactiveEmployee, "", "", -1, "lvpendingleave_tran.formunkid =" & intFormunkid & " AND priority = " & objLevel._Priority)

    '        strQ = "Update lvpendingleave_tran set visibleid = 7 where formunkid = @formunkid and employeeunkid = @employeeunkid and approverunkid = @approverunkid AND approvertranunkid = @approvertranunkid AND isvoid = 0 "

    '        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
    '            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
    '                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
    '                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables(0).Rows(i)("approverunkid")))
    '                objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables(0).Rows(i)("approvertranunkid")))
    '                objDataOperation.ExecNonQuery(strQ)

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If
    '            Next
    '        End If

    '        'Pinkal (09-Jul-2014) -- End


    '        objDataOperation.ClearParameters()
    '        strQ = "Update lvleaveform set statusunkid = 7 where formunkid = @formunkid and employeeunkid = @employeeunkid "
    '        objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lvleaveform", "formunkid", intFormunkid, False, intUserId) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        'Pinkal (01-Feb-2014) -- Start
    '        'Enhancement : TRA Changes
    '        If objDOpearation Is Nothing Then objDataOperation.ReleaseTransaction(True)
    '        'Pinkal (01-Feb-2014) -- End

    '    Catch ex As Exception

    '        'Pinkal (01-Feb-2014) -- Start
    '        'Enhancement : TRA Changes
    '        If objDOpearation Is Nothing Then objDataOperation.ReleaseTransaction(False)
    '        'Pinkal (01-Feb-2014) -- End
    '        DisplayError.Show("-1", ex.Message, "UpdatePendingFormStatus", mstrModuleName)
    '    End Try
    'End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub UpdatePendingFormStatus(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                                           , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                           , ByVal strEmployeeAsOnDate As String _
                                                           , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                           , ByVal intFormunkid As Integer, ByVal intEmployeeunkid As Integer, ByVal intApproverunkid As Integer _
                                                           , ByVal intApprovertranunkid As Integer, Optional ByVal intUserId As Integer = 0 _
                                                           , Optional ByVal objDOpearation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Pinkal (01-Feb-2014) -- Start
        'Enhancement : TRA Changes
        Dim objDataOperation As New clsDataOperation
        If objDOpearation Is Nothing Then
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOpearation
        End If

        'Pinkal (01-Feb-2014) -- End

        Try

            objDataOperation.ClearParameters()



            'Pinkal (09-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2

            strQ = "Update lvpendingleave_tran set statusunkid = 7 where formunkid = @formunkid and employeeunkid = @employeeunkid and approverunkid = @approverunkid AND approvertranunkid = @approvertranunkid AND isvoid = 0 "
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverunkid)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApprovertranunkid)
            objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            strQ = " Select Isnull(pendingleavetranunkid,0) pendingleavetranunkid from lvpendingleave_tran where formunkid = @formunkid AND approverunkid = @approverunkid and employeeunkid = @employeeunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            Dim dtList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If dtList.Tables.Count > 0 AndAlso dtList.Tables(0).Rows.Count > 0 Then
                mintPendingleavetranunkid = CInt(dtList.Tables(0).Rows(0)("pendingleavetranunkid"))
            End If


            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lvpendingleave_tran", "pendingleavetranunkid", mintPendingleavetranunkid, False, intUserId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'Dim objApprover As New clsleaveapprover_master
            'Dim blnIncludeInactiveEmployee As String = ""

            'If blnIncludeInactiveEmployee.Trim.Length <= 0 Then
            '    blnIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString()
            'End If

            'Dim dsApprover As DataSet = Nothing
            'If UserAccessLevel._AccessLevelFilterString.Trim.Contains("(0)") Then
            '    dsApprover = objApprover.GetList("List", True, blnIncludeInactiveEmployee, "", intUserId, "AND 1=1")
            'Else
            '    dsApprover = objApprover.GetList("List", True, blnIncludeInactiveEmployee, "", intUserId)
            'End If


            Dim objApprover As New clsleaveapprover_master
            'Pinkal (26-Oct-2015) -- Start
            'Enhancement - Working on TRA Claim & Request Report Issue.
            objApprover._DoOperation = objDataOperation
            'Pinkal (26-Oct-2015) -- End.
            Dim dsApprover As DataSet = Nothing

            'S.SANDEEP [30 JAN 2016] -- START
            'If UserAccessLevel._AccessLevelFilterString.Trim.Contains("(0)") Then
            '    dsApprover = objApprover.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid _
            '                                                    , xCompanyUnkid, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee _
            '                                                    , True, False, -1, objDataOperation, "AND 1=1", "")
            'Else
            '    dsApprover = objApprover.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid _
            '                                                    , xCompanyUnkid, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee _
            '                                                    , True, False, -1, objDataOperation, "", "")
            'End If
            dsApprover = objApprover.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee _
                                                                , True, False, -1, objDataOperation, "", "")


            'S.SANDEEP [30 JAN 2016] -- END

            'Pinkal (24-Aug-2015) -- End


            Dim dtApproverList As DataTable = New DataView(dsApprover.Tables("List"), "leaveapproverunkid =" & intApproverunkid & " AND approverunkid = " & intApprovertranunkid, "", DataViewRowState.CurrentRows).ToTable

            dsApprover = Nothing

            If dtApproverList.Rows.Count > 0 Then
                'Pinkal (26-Oct-2015) -- Start
                'Enhancement - Working on TRA Claim & Request Report Issue.
                objApprover._DoOperation = objDataOperation
                'Pinkal (26-Oct-2015) -- End.
                objApprover._Approverunkid = CInt(dtApproverList.Rows(0)("approverunkid"))
            End If

            Dim objLevel As New clsapproverlevel_master
            'Pinkal (26-Oct-2015) -- Start
            'Enhancement - Working on TRA Claim & Request Report Issue.
            objLevel._DoOperation = objDataOperation
            'Pinkal (26-Oct-2015) -- End.
            objLevel._Levelunkid = objApprover._Levelunkid



            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'dsList = GetList("List", True, blnIncludeInactiveEmployee, "", "", -1, "lvpendingleave_tran.formunkid =" & intFormunkid & " AND priority = " & objLevel._Priority)
            dsList = GetList("List", xDatabaseName, xUserUnkid, xYearUnkid _
                                    , xCompanyUnkid, eZeeDate.convertDate(strEmployeeAsOnDate) _
                                    , True, xIncludeIn_ActiveEmployee, True, "lvpendingleave_tran.formunkid =" & intFormunkid & " AND priority = " & objLevel._Priority, objDataOperation)
            'Pinkal (24-Aug-2015) -- End



            strQ = "Update lvpendingleave_tran set visibleid = 7 where formunkid = @formunkid and employeeunkid = @employeeunkid and approverunkid = @approverunkid AND approvertranunkid = @approvertranunkid AND isvoid = 0 "

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
                    objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables(0).Rows(i)("approverunkid")))
                    objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dsList.Tables(0).Rows(i)("approvertranunkid")))
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If

            'Pinkal (09-Jul-2014) -- End


            objDataOperation.ClearParameters()
            strQ = "Update lvleaveform set statusunkid = 7 where formunkid = @formunkid and employeeunkid = @employeeunkid "
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lvleaveform", "formunkid", intFormunkid, False, intUserId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (01-Feb-2014) -- Start
            'Enhancement : TRA Changes
            If objDOpearation Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Pinkal (01-Feb-2014) -- End

        Catch ex As Exception

            'Pinkal (01-Feb-2014) -- Start
            'Enhancement : TRA Changes
            If objDOpearation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Pinkal (01-Feb-2014) -- End
            DisplayError.Show("-1", ex.Message, "UpdatePendingFormStatus", mstrModuleName)
        End Try
    End Sub

    'Pinkal (24-Aug-2015) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Shared Sub UpdateLeaveStatus(ByVal objDataOperation As clsDataOperation, ByVal intFormunkid As Integer, Optional ByVal IntUserID As Integer = 0)
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception = Nothing
        Try

            strQ = "Select isnull(pendingleavetranunkid,0) pendingleavetranunkid From lvpendingleave_tran where formunkid = @formunkid  and isvoid = 0"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                strQ = "Update lvpendingleave_tran set iscancelform = 1 where formunkid = @formunkid and isvoid = 0"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lvpendingleave_tran", "pendingleavetranunkid", CInt(dsList.Tables(0).Rows(i)("pendingleavetranunkid")), False, IntUserID) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            End If

        Catch
            Throw exForce
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetApproverMaxPriorityStatusId(ByVal intFormID As Integer, ByVal intEmpID As Integer, ByVal intMaxPriority As Integer) As Integer
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception = Nothing
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()

            strQ = "SELECT statusunkid  " & _
                      " FROM lvpendingleave_tran  " & _
                      " JOIN lvleaveapprover_master ON lvpendingleave_tran.approverunkid = lvleaveapprover_master.leaveapproverunkid " & _
                      " JOIN lvapproverlevel_master ON lvleaveapprover_master.levelunkid = lvapproverlevel_master.levelunkid AND priority = " & intMaxPriority & _
                      "  JOIN lvleaveapprover_tran ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_tran.employeeunkid = " & intEmpID & _
                      " WHERE formunkid = " & intFormID

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then

                Dim drRow As DataRow() = Nothing
                drRow = dsList.Tables("List").Select("statusunkid in (3,4,6,7)")   'FOR REJCETED,RESCHEDULED,CANCELLED AND ISSUED
                If drRow.Length > 0 Then
                    Return 0
                Else
                    drRow = dsList.Tables("List").Select("statusunkid in (1)")  'FOR APPROVED
                    If drRow.Length > 0 Then
                        Return 1
                    End If
                End If
            Else
                Return 0
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverMaxPriority", mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeApproverListWithPriority(ByVal intEmployeeID As Integer, Optional ByVal intFormID As Integer = -1, Optional ByVal objDataOperation As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            If objDataOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            End If

            If intEmployeeID > 0 AndAlso intFormID <= 0 Then

                strQ = " SELECT lvleaveapprover_master.approverunkid " & _
                          "   ,lvleaveapprover_master.leaveapproverunkid " & _
                          "   ,priority " & _
                          "   ,ISNULL(lvapproverlevel_master.escalation_days,0) AS escalation_days  " & _
                          " FROM lvleaveapprover_tran " & _
                           " JOIN lvleaveapprover_master ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_master.isvoid = 0 " & _
                           " JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid AND lvapproverlevel_master.isactive = 1 " & _
                          " WHERE employeeunkid = " & intEmployeeID & " AND lvleaveapprover_tran.isvoid = 0"


                'Pinkal (01-Apr-2019) -- 'Enhancement - Working on Leave Changes for NMB.[  ",ISNULL(lvapproverlevel_master.escalation_days,0) AS escalation_days  " & _]


            ElseIf intEmployeeID > 0 AndAlso intFormID > 0 Then


                'Pinkal (10-Jan-2017) -- Start
                'Enhancement - Working on TRA C&R Module Changes with Leave Module.

                'strQ = " SELECT lvpendingleave_tran.pendingleavetranunkid,lvleaveapprover_master.approverunkid,lvleaveapprover_master.leaveapproverunkid,priority,lvpendingleave_tran.statusunkid " & _
                '          "  FROM lvpendingleave_tran " & _
                '          " JOIN lvleaveapprover_master ON lvleaveapprover_master.approverunkid = lvpendingleave_tran.approvertranunkid AND lvleaveapprover_master.isvoid = 0 " & _
                '          " JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid AND lvapproverlevel_master.isactive = 1 " & _
                '          " WHERE employeeunkid = " & intEmployeeID & " AND formunkid = " & intFormID & "  AND lvpendingleave_tran.isvoid = 0"


                strQ = " SELECT lvpendingleave_tran.pendingleavetranunkid " & _
                          " ,lvleaveapprover_master.approverunkid " & _
                          " ,lvleaveapprover_master.leaveapproverunkid " & _
                          " ,priority " & _
                          " ,lvpendingleave_tran.statusunkid " & _
                          " , lvleaveapprover_master.leaveapproverunkid as employeeunkid  " & _
                          " ,ISNULL(lvapproverlevel_master.escalation_days,0) AS escalation_days " & _
                          " ,ISNULL(lvpendingleave_tran.relieverempunkid,0) AS relieverempunkid " & _
                          "  FROM lvpendingleave_tran " & _
                           " JOIN lvleaveapprover_master ON lvleaveapprover_master.approverunkid = lvpendingleave_tran.approvertranunkid AND lvleaveapprover_master.isvoid = 0 " & _
                           " JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid AND lvapproverlevel_master.isactive = 1 " & _
                          " WHERE employeeunkid = " & intEmployeeID & " AND formunkid = " & intFormID & "  AND lvpendingleave_tran.isvoid = 0"


                'Pinkal (25-May-2019) -- Start       'Enhancement - NMB FINAL LEAVE UAT CHANGES.[" ,ISNULL(lvpendingleave_tran.relieverempunkid,0) AS relieverempunkid " & _]

                'Pinkal (01-Apr-2019) -- 'Enhancement - Working on Leave Changes for NMB.[ISNULL(lvapproverlevel_master.escalation_days,0) AS escalation_days]

                'Pinkal (10-Jan-2017) -- End

            End If
            objDataOperation.ClearParameters()
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeApproverListWithPriority", mstrModuleName)
        End Try
        Return dsList
    End Function


    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : Oman Changes Add Leave Type Name in Email Notification for leave application as per Mr.gogi's Request.


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Public Sub SendMailToApprover(ByVal intEmpId As Integer, ByVal mstrFormNo As String, ByVal intPriority As Integer, ByVal intStatusID As Integer, _
    '                                               ByVal mstrLeaveType As String, Optional ByVal intCompanyUnkId As Integer = 0, _
    '                                              Optional ByVal strArutiSelfServiceURL As String = "", _
    '                                              Optional ByVal iLoginTypeId As Integer = 0, _
    '                                              Optional ByVal iLoginEmployeeId As Integer = 0, _
    '                                              Optional ByVal iUserId As Integer = 0) 'S.SANDEEP [ 28 JAN 2014 ] -- START {iLoginTypeId,iLoginEmployeeId,iUserId} -- END

    '    Dim strLink As String 'Sohail (01 Jan 2013)
    '    Try

    '        'Sohail (01 Jan 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        If intCompanyUnkId <= 0 Then intCompanyUnkId = Company._Object._Companyunkid
    '        If strArutiSelfServiceURL = "" Then strArutiSelfServiceURL = ConfigParameter._Object._ArutiSelfServiceURL
    '        'Sohail (01 Jan 2013) -- End


    '        Dim objNet As New clsNetConnectivity
    '        If objNet._Conected = False Then Exit Sub

    '        If intStatusID <> 1 Then Exit Sub

    '        Dim dsPedingList As DataSet = GetList("PendingProcess")
    '        Dim dtPendingList As DataTable = New DataView(dsPedingList.Tables(0), "employeeunkid = " & intEmpId & " AND formno = '" & mstrFormNo.Trim & "' AND priority > " & intPriority, "", DataViewRowState.CurrentRows).ToTable

    '        If dtPendingList.Rows.Count <= 0 Then Exit Sub

    '        Dim objEmp As New clsEmployee_Master
    '        Dim objMail As New clsSendMail
    '        Dim drRow() As DataRow = dtPendingList.Select("priority = " & CInt(dtPendingList.Rows(0)("priority")))

    '        If drRow.Length > 0 Then

    '            For i As Integer = 0 To drRow.Length - 1

    '                objEmp._Employeeunkid = CInt(drRow(i)("approverunkid"))

    '                'Sohail (01 Jan 2013) -- Start
    '                'TRA - ENHANCEMENT
    '                strLink = strArutiSelfServiceURL & "/Leave/ProcessPendingLeave.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyUnkId.ToString & "|" & drRow(i)("mapuserunkid").ToString & "|" & intEmpId.ToString & "|" & drRow(i)("approverunkid").ToString & "|" & drRow(i)("formunkid").ToString & "|" & drRow(i)("pendingleavetranunkid").ToString))
    '                'Sohail (01 Jan 2013) -- End
    '                If objEmp._Email.Trim.Length <= 0 Then Continue For
    '                objMail._Subject = Language.getMessage(mstrModuleName, 1, "Notification for approving Leave Application form")

    '                Dim strMessage As String = ""

    '                strMessage = "<HTML> <BODY>"

    '                strMessage &= Language.getMessage(mstrModuleName, 2, "Dear") & " " & drRow(i)("approvername").ToString() & ", <BR><BR>"
    '                strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 3, "This is the notification for approving leave application no") & " " & mstrFormNo.Trim & _
    '                                    Language.getMessage(mstrModuleName, 7, " with ") & mstrLeaveType & Language.getMessage(mstrModuleName, 4, " of ") & drRow(i)("employeename").ToString() & Language.getMessage(mstrModuleName, 5, " having Start date : ") & _
    '                                       eZeeDate.convertDate(drRow(i)("startdate").ToString()).Date & Language.getMessage(mstrModuleName, 6, " and End Date : ") & eZeeDate.convertDate(drRow(i)("enddate").ToString()).Date & "."
    '                strMessage &= "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 8, "Please click on the following link to approve leave.")
    '                strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
    '                strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
    '                strMessage &= "</BODY></HTML>"

    '                objMail._Message = strMessage
    '                objMail._ToEmail = objEmp._Email
    '                'S.SANDEEP [ 28 JAN 2014 ] -- START
    '                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
    '                If mstrWebFrmName.Trim.Length > 0 Then
    '                    objMail._Form_Name = mstrWebFrmName
    '                End If
    '                objMail._LogEmployeeUnkid = iLoginEmployeeId
    '                objMail._OperationModeId = iLoginTypeId
    '                objMail._UserUnkid = iUserId
    '                objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
    '                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT
    '                'S.SANDEEP [ 28 JAN 2014 ] -- END
    '                objMail.SendMail()

    '            Next

    '        End If


    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "SendMailToApprover", mstrModuleName)
    '    End Try
    'End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub SendMailToApprover(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                                  , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                  , ByVal mdtEmployeeAsOnDate As Date _
                                                  , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                  , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                  , ByVal intEmpId As Integer, ByVal mstrFormNo As String, ByVal intPriority As Integer _
                                                  , ByVal intStatusID As Integer, ByVal mstrLeaveType As String _
                                                  , Optional ByVal strArutiSelfServiceURL As String = "" _
                                                  , Optional ByVal iLoginTypeId As Integer = 0 _
                                                  , Optional ByVal iLoginEmployeeId As Integer = 0)

        Dim strLink As String
        Try
            If xCompanyUnkid <= 0 Then xCompanyUnkid = Company._Object._Companyunkid
            If strArutiSelfServiceURL = "" Then strArutiSelfServiceURL = ConfigParameter._Object._ArutiSelfServiceURL

            Dim objNet As New clsNetConnectivity
            If objNet._Conected = False Then Exit Sub

            If intStatusID <> 1 Then Exit Sub



            'Pinkal (08-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.	

            'Dim dsPedingList As DataSet = GetList("PendingProcess", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
            '                                              , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
            '                                              , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, "")

            'Dim dtPendingList As DataTable = New DataView(dsPedingList.Tables(0), "employeeunkid = " & intEmpId & " AND formno = '" & mstrFormNo.Trim & "' AND priority > " & intPriority, "", DataViewRowState.CurrentRows).ToTable


            Dim mstrFilter As String = ""
            mstrFilter = "lvleaveform.formno = '" & mstrFormNo.Trim & "' AND lvapproverlevel_master.priority > " & intPriority

            Dim dsPedingList As DataSet = GetList("PendingProcess", FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                      , Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) _
                                                                      , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, mstrFilter, Nothing, False, intEmpId)

            Dim dtPendingList As DataTable = dsPedingList.Tables(0)

            'Pinkal (08-Feb-2022) -- End





            If dtPendingList.Rows.Count <= 0 Then Exit Sub

            'S.SANDEEP [30 JAN 2016] -- START
            'Dim objEmp As New clsEmployee_Master
            'Dim objMail As New clsSendMail
            'Dim drRow() As DataRow = dtPendingList.Select("priority = " & CInt(dtPendingList.Rows(0)("priority")))

            'If drRow.Length > 0 Then

            '    For i As Integer = 0 To drRow.Length - 1

            '        'S.SANDEEP [04 JUN 2015] -- START
            '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '        'objEmp._Employeeunkid = CInt(drRow(i)("approverunkid"))
            '        objEmp._Employeeunkid(mdtEmployeeAsOnDate) = CInt(drRow(i)("approverunkid"))
            '        'S.SANDEEP [04 JUN 2015] -- END

            '        strLink = strArutiSelfServiceURL & "/Leave/ProcessPendingLeave.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(xCompanyUnkid.ToString & "|" & drRow(i)("mapuserunkid").ToString & "|" & intEmpId.ToString & "|" & drRow(i)("approverunkid").ToString & "|" & drRow(i)("formunkid").ToString & "|" & drRow(i)("pendingleavetranunkid").ToString))
            '        If objEmp._Email.Trim.Length <= 0 Then Continue For
            '        objMail._Subject = Language.getMessage(mstrModuleName, 1, "Notification for approving Leave Application form")

            '        Dim strMessage As String = ""

            '        strMessage = "<HTML> <BODY>"

            '        strMessage &= Language.getMessage(mstrModuleName, 2, "Dear") & " " & drRow(i)("approvername").ToString() & ", <BR><BR>"
            '        strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 3, "This is the notification for approving leave application no") & " " & mstrFormNo.Trim & _
            '                            Language.getMessage(mstrModuleName, 7, " with ") & mstrLeaveType & Language.getMessage(mstrModuleName, 4, " of ") & drRow(i)("employeename").ToString() & Language.getMessage(mstrModuleName, 5, " having Start date : ") & _
            '                               eZeeDate.convertDate(drRow(i)("startdate").ToString()).Date & Language.getMessage(mstrModuleName, 6, " and End Date : ") & eZeeDate.convertDate(drRow(i)("enddate").ToString()).Date & "."
            '        strMessage &= "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 8, "Please click on the following link to approve leave.")
            '        strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
            '        strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            '        strMessage &= "</BODY></HTML>"

            '        objMail._Message = strMessage
            '        objMail._ToEmail = objEmp._Email
            '        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
            '        If mstrWebFrmName.Trim.Length > 0 Then
            '            objMail._Form_Name = mstrWebFrmName
            '            'Pinkal (22-Oct-2015) -- Start
            '            'Enhancement - WORKING ON AKFTZ LEAVE ISSUE ON WEB.
            '            objMail._WebClientIP = mstrWebClientIP
            '            objMail._WebHostName = mstrWebHostName
            '            'Pinkal (22-Oct-2015) -- End
            '        End If
            '        objMail._LogEmployeeUnkid = iLoginEmployeeId
            '        objMail._OperationModeId = iLoginTypeId
            '        objMail._UserUnkid = xUserUnkid
            '        objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
            '        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT
            '        objMail.SendMail()

            '    Next

            'End If


            Dim objMail As New clsSendMail
            Dim drRow() As DataRow = dtPendingList.Select("priority = " & CInt(dtPendingList.Rows(0)("priority")))

            If drRow.Length > 0 Then

                For i As Integer = 0 To drRow.Length - 1

                    strLink = strArutiSelfServiceURL & "/Leave/wPg_ProcessLeaveAddEdit.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(xCompanyUnkid.ToString & "|" & drRow(i)("mapuserunkid").ToString & "|" & intEmpId.ToString & "|" & drRow(i)("approverunkid").ToString & "|" & drRow(i)("formunkid").ToString & "|" & drRow(i)("pendingleavetranunkid").ToString & "|" & drRow(i)("isexternalapprover").ToString))
                    If drRow(i)("approveremail").Trim.Length <= 0 Then Continue For

                    objMail._Subject = Language.getMessage(mstrModuleName, 1, "Notification for approving Leave Application form")

                    Dim strMessage As String = ""

                    strMessage = "<HTML> <BODY>"


                    'Pinkal (13-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'strMessage &= Language.getMessage(mstrModuleName, 2, "Dear") & " " & drRow(i)("approvername").ToString() & ", <BR><BR>"

                    Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                    strMessage &= Language.getMessage(mstrModuleName, 2, "Dear") & " " & info1.ToTitleCase(drRow(i)("approvername").ToString().ToLower()) & ", <BR><BR>"
                    'Pinkal (13-Mar-2019) -- End

                    


                    'Pinkal (13-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                    'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 3, "This is a notification for approving leave application no") & " " & mstrFormNo.Trim & _
                    '                    Language.getMessage(mstrModuleName, 7, " with ") & mstrLeaveType & Language.getMessage(mstrModuleName, 4, " of ") & drRow(i)("employeename").ToString() & Language.getMessage(mstrModuleName, 5, " having Start date : ") & _
                    '                       eZeeDate.convertDate(drRow(i)("startdate").ToString()).Date & Language.getMessage(mstrModuleName, 6, " and End Date : ") & eZeeDate.convertDate(drRow(i)("enddate").ToString()).Date & "."
                    'strMessage &= "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 8, "Please click on the following link to approve leave.")
                    'strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"



                    'Pinkal (15-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'strMessage &= Language.getMessage(mstrModuleName, 3, "This is a notification for approving leave application no") & " " & mstrFormNo.Trim & " " & _
                    '                      Language.getMessage(mstrModuleName, 7, "with") & " " & mstrLeaveType & " " & Language.getMessage(mstrModuleName, 4, "of") & " " & drRow(i)("employeename").ToString() & " " & Language.getMessage(mstrModuleName, 5, " having Start date : ") & _
                    '                      eZeeDate.convertDate(drRow(i)("startdate").ToString()).Date & " " & Language.getMessage(mstrModuleName, 6, "and End Date : ") & eZeeDate.convertDate(drRow(i)("enddate").ToString()).Date & "."


                    'Pinkal (01-Apr-2019) -- Start
                    'Enhancement - Working on Leave Changes for NMB.

                    'strMessage &= Language.getMessage(mstrModuleName, 3, "This is a notification for approving leave application no") & " " & mstrFormNo.Trim & " " & _
                    '                      Language.getMessage(mstrModuleName, 7, "with") & " " & mstrLeaveType & " " & Language.getMessage(mstrModuleName, 4, "of") & " " & info1.ToTitleCase(drRow(i)("employeename").ToString()) & " " & Language.getMessage(mstrModuleName, 5, " having Start date : ") & _
                    '                      eZeeDate.convertDate(drRow(i)("startdate").ToString()).Date & " " & Language.getMessage(mstrModuleName, 6, "and End Date : ") & eZeeDate.convertDate(drRow(i)("enddate").ToString()).Date & "."

                    strMessage &= Language.getMessage(mstrModuleName, 3, "This is a notification for approving leave application no") & " <B>(" & mstrFormNo.Trim & ")</B> " & _
                                          Language.getMessage(mstrModuleName, 7, "with") & " <B>(" & mstrLeaveType & ")</B> " & Language.getMessage(mstrModuleName, 4, "of") & " " & info1.ToTitleCase(drRow(i)("employeename").ToString()) & " " & Language.getMessage(mstrModuleName, 5, " having Start date : ") & _
                                          eZeeDate.convertDate(drRow(i)("startdate").ToString()).Date & " " & Language.getMessage(mstrModuleName, 6, "and End Date : ") & eZeeDate.convertDate(drRow(i)("enddate").ToString()).Date & "."

                    'Pinkal (01-Apr-2019) -- End


                    

                    'Pinkal (15-Mar-2019) -- End

                    strMessage &= "<BR></BR><BR></BR>" & Language.getMessage(mstrModuleName, 8, "Please click on the following link to approve leave.")
                    strMessage &= "<BR></BR><a href='" & strLink & "'>" & strLink & "</a>"

                    'Pinkal (13-Mar-2019) -- End

                    strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                    strMessage &= "</BODY></HTML>"

                    objMail._Message = strMessage
                    objMail._ToEmail = drRow(i)("approveremail")
                    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                    If mstrWebFrmName.Trim.Length > 0 Then
                        objMail._Form_Name = mstrWebFrmName
                        objMail._WebClientIP = mstrWebClientIP
                        objMail._WebHostName = mstrWebHostName
                    End If
                    objMail._LogEmployeeUnkid = iLoginEmployeeId
                    objMail._OperationModeId = iLoginTypeId
                    objMail._UserUnkid = xUserUnkid
                    objMail._SenderAddress = IIf(drRow(i)("approveremail") = "", drRow(i)("approvername"), drRow(i)("approveremail"))
                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objMail.SendMail()
                    objMail.SendMail(xCompanyUnkid)
                    'Sohail (30 Nov 2017) -- End
                Next
            End If
            'S.SANDEEP [30 JAN 2016] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SendMailToApprover", mstrModuleName)
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeHighestApprover(ByVal intFormId As Integer, ByVal intEmployeeId As Integer) As DataTable
        Dim mdtApprover As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Try
            strQ = "Select ISNULL(lvpendingleave_tran.pendingleavetranunkid,0) AS Pendingleavetranunkid " & _
                      ", ISNULL(lvpendingleave_tran.approverunkid,0) as ApproverId " & _
                      ", ISNULL(lvpendingleave_tran.approvertranunkid,0) as ApproverTraId " & _
                      ", lvapproverlevel_master.levelunkid " & _
                      ", lvapproverlevel_master.levelname " & _
                      ", lvapproverlevel_master.priority " & _
                      " FROM lvpendingleave_tran " & _
                      " LEFT JOIN lvleaveapprover_master ON lvpendingleave_tran.approvertranunkid = lvleaveapprover_master.approverunkid " & _
                      " LEFT JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid " & _
                      " WHERE lvpendingleave_tran.formunkid = @formunkid AND  lvpendingleave_tran.employeeunkid = @employeeunkid " & _
                      " AND lvpendingleave_tran.statusunkid = 7  AND lvpendingleave_tran.isvoid = 0"

            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If dsList IsNot Nothing Then
                mdtApprover = dsList.Tables(0)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeHighestApprover", mstrModuleName)
        End Try
        Return mdtApprover
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function IsExist(ByVal objDataOperation As clsDataOperation, ByVal iEmployeeId As Integer, ByVal iFormId As Integer, ByVal iApproverId As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iValueId As Integer = 0
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT pendingleavetranunkid " & _
                   "FROM lvpendingleave_tran " & _
                   "WHERE formunkid = '" & iFormId & "' AND employeeunkid = '" & iEmployeeId & "' AND approverunkid = '" & iApproverId & "' " & _
                   "AND isvoid = 0 "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                iValueId = dsList.Tables("List").Rows(0).Item("pendingleavetranunkid")
            End If

            Return iValueId

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsExist", mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAutomaticLeaveIssue(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                                                   , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                   , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                                   , ByVal objDataOperation As clsDataOperation, ByVal mdtStartDate As Date, ByVal mdtEndDate As Date _
                                                                   , ByVal blnLeaveApproverForLeaveType As String, ByVal intLeavebalanceSetting As Integer, ByVal intLeaveApproverId As Integer _
                                                                   , ByVal mblnPaymentApprovalwithLeaveApproval As Boolean, ByVal intApproverTranId As Integer _
                                                                   , ByVal strEmployeeAsOnDate As String, Optional ByVal blnIncludeInactiveEmployee As String = "" _
                                                                   , Optional ByVal mblnIsExternalApprover As Boolean = False _
                                                                   , Optional ByVal blnSkipEmployeeMovementApprovalFlow As Boolean = True _
                                                                   , Optional ByVal blnCreateADUserFromEmpMst As Boolean = False _
                                                                   ) As Boolean
        'Sohail (21 Oct 2019) - [blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst]
        Dim mdblLeaveAmout As Decimal = 0
        Dim exForce As Exception = Nothing
        Try
            Dim mintTotalDays As Integer = DateDiff(DateInterval.Day, mdtStartDate, mdtEndDate.AddDays(1))
            Dim mdecTotalIssue As Decimal = 0
            Dim arGetLeaveDate As ArrayList = Nothing
            Dim arGetdate() As String = Nothing

            Dim objleaveIssue As New clsleaveissue_master
            Dim objLeaveIssueTran As New clsleaveissue_Tran
            Dim objlvform As New clsleaveform

            Dim objLvDayFraction As New clsleaveday_fraction
            objlvform._DoOperation = objDataOperation
            objlvform._Formunkid = mintFormunkid

            arGetLeaveDate = New ArrayList()

            Dim objLeaveType As New clsleavetype_master
            objLeaveType._DoOperation = objDataOperation
            objLeaveType._Leavetypeunkid = objlvform._Leavetypeunkid

            If objLeaveType._IsPaid Then
                Dim objAcccure As New clsleavebalance_tran
                Dim dsList As DataSet = Nothing




                'Pinkal (04-Jul-2020) -- Start
                'Problem Solving in Leave Approval - working on Solving Problem in Leave Approval.

                'If intLeavebalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                '    dsList = objAcccure.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid _
                '                                           , xCompanyUnkid, strEmployeeAsOnDate, xUserModeSetting _
                '                                           , xOnlyApproved, blnIncludeInactiveEmployee, True, True, False _
                '                                           , mintEmployeeunkid, False, False, False, "", objDataOperation, mblnIsExternalApprover)
                'ElseIf intLeavebalanceSetting = enLeaveBalanceSetting.ELC Then
                '    dsList = objAcccure.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid _
                '                                           , xCompanyUnkid, strEmployeeAsOnDate, xUserModeSetting _
                '                                           , xOnlyApproved, blnIncludeInactiveEmployee, True, True, False _
                '                                           , mintEmployeeunkid, True, True, False, "", objDataOperation, mblnIsExternalApprover)
                'End If

                'Dim dtList As DataTable = New DataView(dsList.Tables("List"), " yearunkid = " & mintYearId & " AND leavetypeunkid = " & objLeaveType._Leavetypeunkid, "", DataViewRowState.CurrentRows).ToTable

                If intLeavebalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    dsList = objAcccure.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid _
                                                           , xCompanyUnkid, strEmployeeAsOnDate, xUserModeSetting _
                                                           , xOnlyApproved, blnIncludeInactiveEmployee, True, False, False _
                                                           , mintEmployeeunkid, False, False, False, "lvleavebalance_tran.yearunkid = " & mintYearId & " AND lvleavebalance_tran.leavetypeunkid = " & objLeaveType._Leavetypeunkid, objDataOperation, mblnIsExternalApprover)
                ElseIf intLeavebalanceSetting = enLeaveBalanceSetting.ELC Then
                    dsList = objAcccure.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid _
                                                           , xCompanyUnkid, strEmployeeAsOnDate, xUserModeSetting _
                                                           , xOnlyApproved, blnIncludeInactiveEmployee, True, False, False _
                                                           , mintEmployeeunkid, True, True, False, "lvleavebalance_tran.yearunkid = " & mintYearId & " AND lvleavebalance_tran.leavetypeunkid = " & objLeaveType._Leavetypeunkid, objDataOperation, mblnIsExternalApprover)
                End If

                Dim dtList As DataTable = dsList.Tables(0)

                'Pinkal (04-Jul-2020) -- End


                If dtList IsNot Nothing Then
                    If dtList.Rows.Count > 0 Then
                        mdblLeaveAmout = CDec(dtList.Rows(0)("accrue_amount"))
                    End If
                End If
            Else
                mdblLeaveAmout = objLeaveType._MaxAmount
            End If

            For i As Integer = 0 To mintTotalDays - 1
                Dim mdcDayFraction As Decimal = objLvDayFraction.GetEmployeeLeaveDay_Fraction(eZeeDate.convertDate(mdtStartDate.AddDays(i).Date), mintFormunkid, mintEmployeeunkid, blnLeaveApproverForLeaveType, intLeaveApproverId, objDataOperation)
                If mdcDayFraction > 0 Then
                    arGetLeaveDate.Add(mdtStartDate.AddDays(i).ToShortDateString)
                    mdecTotalIssue += mdcDayFraction
                End If
            Next

            If arGetLeaveDate IsNot Nothing Then
                arGetdate = CType(arGetLeaveDate.ToArray(GetType(String)), String())
            End If

            If CBool(blnLeaveApproverForLeaveType) = True Then
                If arGetdate.Count <= 0 Then
                    mstrMessage = Language.getMessage(mstrModuleName, 9, "Please Map this Leave type to this employee's Approver(s).")
                    Return False
                End If
            End If

            objleaveIssue._Formunkid = mintFormunkid
            objleaveIssue._Employeeunkid = mintEmployeeunkid
            objleaveIssue._Leavetypeunkid = objlvform._Leavetypeunkid
            objleaveIssue._StartDate = mdtStartDate
            objleaveIssue._EndDate = mdtEndDate
            objleaveIssue._TotalIssue = mdecTotalIssue
            objleaveIssue._Leaveyearunkid = mintYearId
            objleaveIssue._Userunkid = mintUserunkid
            objleaveIssue._ConsiderLeaveOnTnAPeriod = objlvform._ConsiderLeaveOnTnAPeriod

            If mblnPaymentApprovalwithLeaveApproval Then

                objleaveIssue._PaymentApprovalwithLeaveApproval = mblnPaymentApprovalwithLeaveApproval
                Dim objExpApproverTran As New clsclaim_request_approval_tran
                Dim objClaimRequestMst As New clsclaim_request_master
                objleaveIssue._ClaimRequestMasterId = objClaimRequestMst.GetClaimRequestMstIDFromLeaveForm(mintFormunkid, objDataOperation)
                objleaveIssue._DtExpense = objExpApproverTran.GetApproverExpesneList("List", False, mblnPaymentApprovalwithLeaveApproval, xDatabaseName _
                                                                                                                            , xUserUnkid, strEmployeeAsOnDate, enExpenseType.EXP_LEAVE, False, True, intApproverTranId, "", -1, objDataOperation).Tables(0)
                objExpApproverTran = Nothing
                objClaimRequestMst = Nothing

            End If


            If objleaveIssue.Insert(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                          , strEmployeeAsOnDate, xUserModeSetting, True, blnIncludeInactiveEmployee _
                                          , arGetdate, objLeaveType._IsPaid, mdblLeaveAmout, intLeavebalanceSetting, blnLeaveApproverForLeaveType _
                                          , intLeaveApproverId, objDataOperation, False, mblnIsExternalApprover, objLeaveType._Isexempt_from_payroll, blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst) = False Then

                If objleaveIssue._Message <> "" Then
                    mstrMessage = objleaveIssue._Message
                    exForce = New Exception(objleaveIssue._Message)
                Else
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAutomaticLeaveIssue; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function UpdateLeaveExpenseVisibility(ByVal objDataOperation As clsDataOperation, ByVal IntFormID As Integer, ByVal blnLeaveApproverForLeaveType As Boolean _
                                                                        , ByVal intLeavebalanceSetting As Integer, ByVal xDatabaseName As String, ByVal strEmployeeAsOnDate As String) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Dim mstrClaimFormNo As String = ""
        Try
            strQ = " SELECT crmasterunkid,claimrequestno from CMCLAIM_REQUEST_MASTER where modulerefunkid =@modulerefunkid AND referenceunkid = @formunkid and frommoduleid = @frommoduleid and isvoid = 0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IntFormID)
            objDataOperation.AddParameter("@modulerefunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, enModuleReference.Leave)
            objDataOperation.AddParameter("@frommoduleid", SqlDbType.Int, eZeeDataType.INT_SIZE, enExpFromModuleID.FROM_LEAVE)
            Dim dsClaim As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If dsClaim IsNot Nothing AndAlso dsClaim.Tables(0).Rows.Count > 0 Then
                mintClaimRequestMasterId = CInt(dsClaim.Tables(0).Rows(0)("crmasterunkid"))
                mstrClaimFormNo = dsClaim.Tables(0).Rows(0)("claimrequestno").ToString()

                Dim objClaimApprover As New clsExpenseApprover_Master
                Dim dsList As DataSet = objClaimApprover.GetEmployeeApprovers(enExpenseType.EXP_LEAVE, mintEmployeeunkid, "List", objDataOperation)

                Dim mintFirstClaimApproverId As Integer = -1
                Dim objClaimApproverTran As New clsclaim_request_approval_tran
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    strQ = " UPDATE cmclaim_approval_tran SET visibleId = 2 WHERE crapproverunkid = @crapproverunkid AND crmasterunkid = @crmasterunkid AND isvoid = 0 "
                    Dim drRow() As DataRow = dsList.Tables(0).Select("crpriority = MIN(crpriority)")
                    Dim mintMinPriority As Integer = -1
                    If drRow.Length > 0 Then
                        For i As Integer = 0 To drRow.Length - 1
                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow(i)("crapproverunkid")))
                            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimRequestMasterId)
                            objDataOperation.ExecNonQuery(strQ)
                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            'Pinkal (12-Jul-2019) -- Start
                            'Defect - Problem solved for same email notification sending to claim approver multiple times .
                            'objClaimApproverTran.SendMailToApprover(enExpenseType.EXP_LEAVE, mblnPaymentApprovalwithLeaveApproval _
                            '                                                             , mintClaimRequestMasterId, mstrClaimFormNo, mintEmployeeunkid, CInt(drRow(i)("crpriority")), 1 _
                            '                                                             , "crpriority = " & CInt(drRow(i)("crpriority")), xDatabaseName, strEmployeeAsOnDate, mintCompanyId _
                            '                                                             , mstrArutiSelfServiceURL, mintLoginMode, 0, mintUserunkid, mstrWebFrmName, objDataOperation)

                            mintMinPriority = CInt(drRow(i)("crpriority"))
                        Next

                            objClaimApproverTran.SendMailToApprover(enExpenseType.EXP_LEAVE, mblnPaymentApprovalwithLeaveApproval _
                                                                                          , mintClaimRequestMasterId, mstrClaimFormNo, mintEmployeeunkid, mintMinPriority, 1 _
                                                                                          , "crpriority = " & mintMinPriority, xDatabaseName, strEmployeeAsOnDate, mintCompanyId _
                                                                                          , mstrArutiSelfServiceURL, mintLoginMode, 0, mintUserunkid, mstrWebFrmName, objDataOperation)

                        'Pinkal (12-Jul-2019) -- End

                    End If
                End If
                dsList = Nothing
                objClaimApprover = Nothing
            End If
            dsClaim = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateLeaveExpenseVisibility; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function


    'Pinkal (15-Mar-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Private Function GetEmployeeLeaveHistoryDetails(ByVal strTableName As String, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                                                           , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                           , ByVal mdtEmployeeAsOnDate As Date _
                                                                           , ByVal xOnlyApproved As Boolean _
                                                                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                                           , Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal strFilter As String = "" _
                                                                           , Optional ByVal objDoOperation As clsDataOperation = Nothing _
                                                                           , Optional ByVal mblnIncludeCloseFYTransactions As Boolean = False _
                                                                           , Optional ByVal iEmployeeId As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            If objDoOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDoOperation
            End If
            objDataOperation.ClearParameters()

            Dim StrQCondition As String = String.Empty
            Dim StrQCommonQry As String = String.Empty
            Dim dsCompany As New DataSet


            strQ = "SELECT DISTINCT " & _
                   "     ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                   "    ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                   "    ,ISNULL(EffDt.key_value,'') AS EDate " & _
                   "FROM " & xDatabaseName & "..lvpendingleave_tran " & _
                   "    JOIN " & xDatabaseName & "..lvleaveapprover_master ON lvleaveapprover_master.approverunkid = lvpendingleave_tran.approvertranunkid " & _
                   "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lvleaveapprover_master.leaveapproverunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid "

            If mblnIncludeCloseFYTransactions = False Then
                strQ &= "  AND cffinancial_year_tran.isclosed = 0"
            End If

            strQ &= "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             cfconfiguration.companyunkid " & _
                   "            ,cfconfiguration.key_value " & _
                   "        FROM hrmsConfiguration..cfconfiguration " & _
                   "        WHERE UPPER(cfconfiguration.key_name) IN ('EMPLOYEEASONDATE') " & _
                   "    ) AS EffDt ON EffDt.companyunkid = cffinancial_year_tran.companyunkid " & _
                   "WHERE lvpendingleave_tran.isvoid = 0 AND lvleaveapprover_master.isexternalapprover = 1 "

            dsCompany = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""


            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsOnDate, xDatabaseName)


            strQ = "SELECT " & _
                      "  pendingleavetranunkid " & _
                      ", ISNULL(leaveissueunkid,'') as leaveissueunkid " & _
                      ", lvpendingleave_tran.formunkid " & _
                      ", lvleaveform.formno " & _
                      ", lvleaveform.leavetypeunkid " & _
                      ", lvleavetype_master.leavename " & _
                      ", lvpendingleave_tran.employeeunkid " & _
                      ", h1.employeecode " & _
                      ", ISNULL(h1.firstname,'') + ' ' + isnull(h1.surname,'') as employeename " & _
                      ", ISNULL(lvleaveapprover_master.approverunkid,-1) as leaveapproverunkid " & _
                      ", lvpendingleave_tran.approverunkid " & _
                      ", #APPR_NAME_VALUE# AS approvername " & _
                      ", #APPR_EMAIL_VALUE# AS approveremail " & _
                      ", ISNULL(lvleaveapprover_master.levelunkid,-1) AS levelunkid " & _
                      ", ISNULL(lvapproverlevel_master.levelname,'') AS  levelname " & _
                      ", ISNULL(lvapproverlevel_master.priority,-1) AS priority " & _
                      ", CONVERT(CHAR(8),lvpendingleave_tran.startdate,112) as startdate " & _
                      ", CONVERT(CHAR(8),lvpendingleave_tran.enddate,112) as enddate  " & _
                      ", CONVERT(CHAR(8),lvpendingleave_tran.approvaldate,112) as approvaldate " & _
                      ", ISNULL(dayfraction,0.00) days " & _
                      ", transactionheadunkid " & _
                      ", lvpendingleave_tran.statusunkid " & _
                      ", lvleaveform.statusunkid AS formstatusunkid " & _
                      ", CASE WHEN lvpendingleave_tran.statusunkid = 1 then @Approve " & _
                      "       WHEN lvpendingleave_tran.statusunkid = 2 then @Pending  " & _
                      "       WHEN lvpendingleave_tran.statusunkid = 3 then @Reject " & _
                      "       WHEN lvpendingleave_tran.statusunkid = 4 then @ReSchedule " & _
                      "       WHEN lvpendingleave_tran.statusunkid = 7 then @Issued end as status" & _
                      ", lvpendingleave_tran.amount " & _
                      ", lvpendingleave_tran.remark " & _
                      ", lvpendingleave_tran.userunkid " & _
                      ", ISNULL(hrapprover_usermapping.userunkid,-1) as mapuserunkid " & _
                      ", ISNULL(hrapprover_usermapping.approverunkid,-1) as mapapproverunkid " & _
                      ", lvpendingleave_tran.isvoid " & _
                      ", lvpendingleave_tran.voiddatetime " & _
                      ", lvpendingleave_tran.voiduserunkid " & _
                      ", lvpendingleave_tran.voidreason " & _
                      ", CONVERT(CHAR(8),lvleaveform.applydate,112) As applydate " & _
                      ",ISNULL(lvpendingleave_tran.iscancelform,0) iscancelform " & _
                      ", ISNULL(lvpendingleave_tran.approvertranunkid ,0) as approvertranunkid " & _
                      ", ISNULL(lvleaveIssue_master.userunkid,-1) AS IssueUserId " & _
                      ", CASE WHEN  ISNULL(hrmsConfiguration..cfuser_master.username,'') <> '' AND  ISNULL(hrmsConfiguration..cfuser_master.lastname,'') <> '' THEN ISNULL(hrmsConfiguration..cfuser_master.firstname,'') + ' ' + ISNULL(hrmsConfiguration..cfuser_master.lastname,'') ELSE ISNULL(hrmsConfiguration..cfuser_master.username,'') END AS IssueUserName " & _
                      ", ISNULL(visibleid,lvpendingleave_tran.statusunkid) AS visibleid " & _
                      ",#DATA_VALUE# " & _
                      ",lvleaveapprover_master.isexternalapprover " & _
                      ",ISNULL(lvpendingleave_tran.relieverempunkid,0) AS relieverempunkid " & _
                      ", h3.employeecode " & _
                      ", ISNULL(h3.firstname,'') + ' ' + isnull(h3.surname,'') as relivername " & _
                      ", lvpendingleave_tran.escalation_date " & _
                      ", ISNULL(lvapproverlevel_master.escalation_days,0) AS  escalation_days " & _
                      " FROM " & xDatabaseName & "..lvpendingleave_tran " & _
                      " JOIN " & xDatabaseName & "..lvleaveform on lvleaveform.formunkid = lvpendingleave_tran.formunkid and lvleaveform.isvoid = 0" & _
                      " LEFT JOIN " & xDatabaseName & "..lvleaveIssue_master ON lvleaveIssue_master.formunkid = lvleaveform.formunkid " & _
                      " LEFT JOIN " & xDatabaseName & "..lvleavetype_master on lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid " & _
                      " LEFT JOIN " & xDatabaseName & "..hremployee_master h1 on h1.employeeunkid = lvpendingleave_tran.employeeunkid  " & _
                      " #APPR_NAME_JOIN#  " & _
                      " LEFT JOIN " & xDatabaseName & "..lvleaveapprover_master ON lvleaveapprover_master.leaveapproverunkid = lvpendingleave_tran.approverunkid  AND lvleaveapprover_master.approverunkid = lvpendingleave_tran.approvertranunkid " & _
                      " LEFT JOIN " & xDatabaseName & "..lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid	" & _
                      " LEFT JOIN " & xDatabaseName & "..hrapprover_usermapping ON hrapprover_usermapping.approverunkid = lvleaveapprover_master.approverunkid " & " AND hrapprover_usermapping.usertypeid = " & enUserType.Approver & _
                      " LEFT JOIN " & xDatabaseName & "..hremployee_master h3 ON h3.employeeunkid = lvpendingleave_tran.relieverempunkid "


            'Pinkal (03-May-2019) --  'Enhancement - Working on Leave UAT Changes for NMB. [CASE WHEN  ISNULL(hrmsConfiguration..cfuser_master.username,'') <> '' AND  ISNULL(hrmsConfiguration..cfuser_master.lastname,'') <> '' THEN ISNULL(hrmsConfiguration..cfuser_master.firstname,'') + ' ' + ISNULL(hrmsConfiguration..cfuser_master.lastname,'') ELSE ISNULL(hrmsConfiguration..cfuser_master.username,'') END AS IssueUserName]

            'Pinkal (01-Apr-2019) -- 'Enhancement - Working on Leave Changes for NMB.[", lvpendingleave_tran.escalation_date , ISNULL(lvapproverlevel_master.escalation_days,0) AS  escalation_days " & _]

            If xUserUnkid <= 0 Then
                If User._Object._Userunkid > 1 Then
                    strQ &= " AND hrapprover_usermapping.userunkid = " & User._Object._Userunkid
                End If
            Else
                strQ &= " AND hrapprover_usermapping.userunkid = " & xUserUnkid
            End If

            strQ &= " #DATA_JOIN# " & _
                        " LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = lvleaveIssue_master.userunkid "

            Dim StrDataJoin As String = "LEFT JOIN " & _
                                                    "( " & _
                                                    "    SELECT " & _
                                                    "         stationunkid " & _
                                                    "        ,deptgroupunkid " & _
                                                    "        ,departmentunkid " & _
                                                    "        ,sectiongroupunkid " & _
                                                    "        ,sectionunkid " & _
                                                    "        ,unitgroupunkid " & _
                                                    "        ,unitunkid " & _
                                                    "        ,teamunkid " & _
                                                    "        ,classgroupunkid " & _
                                                    "        ,classunkid " & _
                                                    "        ,employeeunkid " & _
                                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                    "    FROM " & xDatabaseName & "..hremployee_transfer_tran " & _
                                                                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '#EFF_DATE#' " & _
                                                    " ) AS Alloc ON Alloc.employeeunkid = h1.employeeunkid AND Alloc.rno = 1 " & _
                                                    " LEFT JOIN " & _
                                                    "( " & _
                                                    "    SELECT " & _
                                                    "         jobunkid " & _
                                                    "        ,jobgroupunkid " & _
                                                    "        ,employeeunkid " & _
                                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                                    "    FROM " & xDatabaseName & "..hremployee_categorization_tran " & _
                                                                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '#EFF_DATE#' " & _
                                                    ") AS Jobs ON Jobs.employeeunkid = h1.employeeunkid AND Jobs.rno = 1 " & _
                                                    " JOIN " & _
                                                    " ( " & _
                                                    "    SELECT " & _
                                                    "         gradegroupunkid " & _
                                                    "        ,gradeunkid " & _
                                                    "        ,gradelevelunkid " & _
                                                    "        ,employeeunkid " & _
                                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                                                    "    FROM " & xDatabaseName & "..prsalaryincrement_tran " & _
                                                                        "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '#EFF_DATE#' " & _
                                                    " ) AS Grds ON Grds.employeeunkid = h1.employeeunkid AND Grds.rno = 1 "

            StrQCondition &= " WHERE 1 = 1 AND lvleaveapprover_master.isexternalapprover = #ExValue# "

            If blnOnlyActive Then
                StrQCondition &= " AND lvpendingleave_tran.isvoid = 0 "
            End If

            If strFilter.Trim.Length > 0 Then
                StrQCondition &= " AND " & strFilter
            End If

            StrQCondition &= " AND h1.isapproved = 1 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@ReSchedule", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 113, "Re-Scheduled"))
            objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 276, "Issued"))

            StrQCommonQry = strQ

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry.Replace("hremployee_master", "h1") & " "
            End If

            strQ &= StrQCondition

            strQ = strQ.Replace("#APPR_NAME_JOIN#", "LEFT JOIN hremployee_master h2 on h2.employeeunkid = lvpendingleave_tran.approverunkid")
            strQ = strQ.Replace("#APPR_NAME_VALUE#", "ISNULL(h2.firstname,'') + ' ' + ISNULL(h2.surname,'') ")
            strQ = strQ.Replace("#APPR_EMAIL_VALUE#", "ISNULL(h2.email,'') ")
            strQ = strQ.Replace("#ExValue#", "0")
            strQ = strQ.Replace("#DATA_JOIN#", StrDataJoin)
            strQ = strQ.Replace("#EFF_DATE#", eZeeDate.convertDate(mdtEmployeeAsOnDate))
            strQ = strQ.Replace("#DATA_VALUE#", " ISNULL(Alloc.sectiongroupunkid,0) AS sectiongroupunkid " & _
                                                ",ISNULL(Alloc.unitgroupunkid,0) AS unitgroupunkid " & _
                                                ",ISNULL(Alloc.teamunkid,0) AS teamunkid " & _
                                                ",ISNULL(Alloc.stationunkid,0) AS stationunkid " & _
                                                ",ISNULL(Alloc.deptgroupunkid,0) AS deptgroupunkid " & _
                                                ",ISNULL(Alloc.departmentunkid,0) AS departmentunkid " & _
                                                ",ISNULL(Alloc.sectionunkid,0) AS sectionunkid " & _
                                                ",ISNULL(Alloc.unitunkid,0) AS unitunkid " & _
                                                ",ISNULL(Alloc.classgroupunkid,0) AS classgroupunkid " & _
                                                ",ISNULL(Alloc.classunkid,0) AS classunkid " & _
                                                ",ISNULL(Jobs.jobunkid,0) AS jobunkid " & _
                                                ",ISNULL(Jobs.jobgroupunkid,0) AS jobgroupunkid " & _
                                                ",ISNULL(Grds.gradegroupunkid,0) AS gradegroupunkid " & _
                                                ",ISNULL(Grds.gradeunkid,0) AS gradeunkid " & _
                                                ",ISNULL(Grds.gradelevelunkid,0) AS gradelevelunkid ")

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr In dsCompany.Tables(0).Rows
                Dim dstemp As New DataSet
                Dim StrExJoin As String = String.Empty

                xDateJoinQry = "" : xAdvanceJoinQry = "" : xDateFilterQry = ""

                strQ = StrQCommonQry
                StrExJoin = " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = lvpendingleave_tran.approverunkid "
                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    strQ = strQ.Replace("#APPR_NAME_JOIN#", StrExJoin)
                    strQ = strQ.Replace("#APPR_NAME_VALUE#", "ISNULL(UEmp.username,'') ")
                    strQ = strQ.Replace("#APPR_EMAIL_VALUE#", "ISNULL(UEmp.email,'') ")
                    strQ = strQ.Replace("#DATA_JOIN#", "")
                    strQ = strQ.Replace("#DATA_VALUE#", "''")

                Else

                    Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(dr("EDate").ToString), dr("DName"))

                    strQ = strQ.Replace("#DATA_JOIN#", StrDataJoin)

                    strQ = strQ.Replace("#EFF_DATE#", dr("EDate").ToString())


                    StrExJoin &= " JOIN " & dr("DName") & "..hremployee_master h2 on h2.employeeunkid = UEmp.employeeunkid AND h2.isapproved = 1"
                    strQ = strQ.Replace("#APPR_NAME_JOIN#", StrExJoin)
                    strQ = strQ.Replace("#APPR_NAME_VALUE#", "CASE WHEN ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.surname, '') END")
                    strQ = strQ.Replace("#APPR_EMAIL_VALUE#", "CASE WHEN ISNULL(h2.email,'') = '' THEN ISNULL(UEmp.email,'') ELSE ISNULL(h2.email,'') END ")
                    strQ = strQ.Replace("#DATA_VALUE#", " ISNULL(Alloc.sectiongroupunkid,0) AS sectiongroupunkid " & _
                                                ",ISNULL(Alloc.unitgroupunkid,0) AS unitgroupunkid " & _
                                                ",ISNULL(Alloc.teamunkid,0) AS teamunkid " & _
                                                ",ISNULL(Alloc.stationunkid,0) AS stationunkid " & _
                                                ",ISNULL(Alloc.deptgroupunkid,0) AS deptgroupunkid " & _
                                                ",ISNULL(Alloc.departmentunkid,0) AS departmentunkid " & _
                                                ",ISNULL(Alloc.sectionunkid,0) AS sectionunkid " & _
                                                ",ISNULL(Alloc.unitunkid,0) AS unitunkid " & _
                                                ",ISNULL(Alloc.classgroupunkid,0) AS classgroupunkid " & _
                                                ",ISNULL(Alloc.classunkid,0) AS classunkid " & _
                                                ",ISNULL(Jobs.jobunkid,0) AS jobunkid " & _
                                                ",ISNULL(Jobs.jobgroupunkid,0) AS jobgroupunkid " & _
                                                ",ISNULL(Grds.gradegroupunkid,0) AS gradegroupunkid " & _
                                                ",ISNULL(Grds.gradeunkid,0) AS gradeunkid " & _
                                                ",ISNULL(Grds.gradelevelunkid,0) AS gradelevelunkid ")

                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    strQ &= xAdvanceJoinQry.Replace("hremployee_master", "h1") & " "
                End If

                strQ &= StrQCondition

                strQ = strQ.Replace("#ExValue#", "1")

                strQ &= " AND UEmp.companyunkid = " & dr("companyunkid") & " "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
                objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
                objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
                objDataOperation.AddParameter("@ReSchedule", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 113, "Re-Scheduled"))
                objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 276, "Issued"))

                dstemp = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstemp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstemp.Tables(0), True)
                End If

            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeLeaveHistoryDetails; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function


    'Pinkal (15-Mar-2019) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Notification for approving Leave Application form")
            Language.setMessage(mstrModuleName, 2, "Dear")
            Language.setMessage(mstrModuleName, 3, "This is a notification for approving leave application no")
			Language.setMessage(mstrModuleName, 4, "of")
            Language.setMessage(mstrModuleName, 5, " having Start date :")
			Language.setMessage(mstrModuleName, 6, "and End Date :")
			Language.setMessage(mstrModuleName, 7, "with")
            Language.setMessage(mstrModuleName, 8, "Please click on the following link to approve leave.")
            Language.setMessage(mstrModuleName, 9, "Please Map this Leave type to this employee's Approver(s).")
			Language.setMessage("clsMasterData", 110, "Approved")
			Language.setMessage("clsMasterData", 111, "Pending")
			Language.setMessage("clsMasterData", 112, "Rejected")
			Language.setMessage("clsMasterData", 113, "Re-Scheduled")
			Language.setMessage("clsMasterData", 276, "Issued")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class