﻿'************************************************************************************************************************************
'Class Name : clsleaveissue_Tran.vb
'Purpose    :
'Date       :14/07/2010
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsleaveissue_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsleaveissue_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLeaveissuetranunkid As Integer
    Private mintLeaveissueunkid As Integer
    Private mintLeaveAdjustmentunkid As Integer = -1
    Private mdtLeavedate As Date
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintVoiduserunkid As Integer
    Private mdblTotalAccrue As Double
    Private mintTotalIssue As Decimal
    Private mblnIsPaid As Boolean = False
    Private mobjDataOperation As clsDataOperation = Nothing
    Private mstrWebFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1
    Private mdclDayfraction As Decimal = 0
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Private mblnIsProcess As Boolean = False

    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mblnSkipApproverFlow As Boolean = False
    'Pinkal (01-Oct-2018) -- End


    'Pinkal (01-Jan-2019) -- Start
    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
    Private mblnConsiderLeaveOnTnAPeriod As Boolean = False
    'Pinkal (01-Jan-2019) -- End


#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leaveissuetranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leaveissuetranunkid() As Integer
        Get
            Return mintLeaveissuetranunkid
        End Get
        Set(ByVal value As Integer)
            mintLeaveissuetranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leaveissueunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leaveissueunkid() As Integer
        Get
            Return mintLeaveissueunkid
        End Get
        Set(ByVal value As Integer)
            mintLeaveissueunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavedate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leavedate() As Date
        Get
            Return mdtLeavedate
        End Get
        Set(ByVal value As Date)
            mdtLeavedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set total_accrue
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _TotalAccrue() As Double
        Get
            Return mdblTotalAccrue
        End Get
        Set(ByVal value As Double)
            mdblTotalAccrue = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set totalissue
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _TotalIssue() As Integer
        Get
            Return mintTotalIssue
        End Get
        Set(ByVal value As Integer)
            mintTotalIssue = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ispaid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsPaid() As Boolean
        Get
            Return mblnIsPaid
        End Get
        Set(ByVal value As Boolean)
            mblnIsPaid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leaveadjustmentunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _LeaveAdjustmentunkid() As Integer
        Get
            Return mintLeaveAdjustmentunkid
        End Get
        Set(ByVal value As Integer)
            mintLeaveAdjustmentunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mObjDataoperation
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _mObjDataoperation() As clsDataOperation
        Get
            Return mobjDataOperation
        End Get
        Set(ByVal value As clsDataOperation)
            mobjDataOperation = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mObjDataoperation
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LoginEmployeeUnkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DayFraction
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DayFraction() As Decimal
        Get
            Return mdclDayfraction
        End Get
        Set(ByVal value As Decimal)
            mdclDayfraction = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsProcess
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsProcess() As Boolean
        Get
            Return mblnIsProcess
        End Get
        Set(ByVal value As Boolean)
            mblnIsProcess = value
        End Set
    End Property

    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    ''' <summary>
    ''' Purpose: Get or Set SkipApproverFlow
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _SkipApproverFlow() As Boolean
        Get
            Return mblnSkipApproverFlow
        End Get
        Set(ByVal value As Boolean)
            mblnSkipApproverFlow = value
        End Set
    End Property

    'Pinkal (01-Oct-2018) -- End


    'Pinkal (01-Jan-2019) -- Start
    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.

    ''' <summary>
    ''' Purpose: Get or Set ConsiderLeaveOnTnAPeriod
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _ConsiderLeaveOnTnAPeriod() As Boolean
        Get
            Return mblnConsiderLeaveOnTnAPeriod
        End Get
        Set(ByVal value As Boolean)
            mblnConsiderLeaveOnTnAPeriod = value
        End Set
    End Property
    'Pinkal (01-Jan-2019) -- End


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If mobjDataOperation IsNot Nothing Then
            objDataOperation = mobjDataOperation
        Else
        objDataOperation = New clsDataOperation
        End If
        Try

            strQ = "SELECT " & _
              "  leaveissuetranunkid " & _
              ", leaveissueunkid " & _
              ", leavedate " & _
              ", ISNULL(ispaid,0) AS ispaid " & _
              ", isnull(leaveadjustmentunkid,-1) as leaveadjustmentunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
                       ", ISNULL(dayfraction,0) AS dayfraction " & _
              ", ISNULL(isprocess,0) AS isprocess " & _
                          ", ISNULL(istnaperiodlinked,0)  AS istnaperiodlinked " & _
             "FROM lvleaveIssue_tran " & _
             "WHERE leaveissuetranunkid = @leaveissuetranunkid "

            'Pinkal (01-Jan-2019) --  'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[", ISNULL(istnaperiodlinked,0)  AS istnaperiodlinked " & _]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leaveissuetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissuetranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLeaveissuetranunkid = CInt(dtRow.Item("leaveissuetranunkid"))
                mintLeaveissueunkid = CInt(dtRow.Item("leaveissueunkid"))
                mdtLeavedate = dtRow.Item("leavedate")
                If Not IsDBNull(dtRow("ispaid")) Then mblnIsPaid = CBool(dtRow("ispaid"))
                mintLeaveAdjustmentunkid = CInt(dtRow("leaveadjustmentunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If dtRow.Item("voiddatetime") IsNot DBNull.Value Then mdtVoiddatetime = dtRow.Item("voiddatetime")
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mdclDayfraction = CDbl(dtRow.Item("dayfraction"))
                If Not IsDBNull(dtRow.Item("isprocess")) Then
                    mblnIsProcess = CBool(dtRow.Item("isprocess"))
                End If

                'Pinkal (01-Jan-2019) -- Start
                'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                mblnConsiderLeaveOnTnAPeriod = CBool(dtRow.Item("istnaperiodlinked"))
                'Pinkal (01-Jan-2019) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True _
                                     , Optional ByVal intFormunkid As Integer = -1, Optional ByVal xEmployeeID As Integer = 0 _
                                     , Optional ByVal xStartDate As Date = Nothing, Optional ByVal xEndDate As Date = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try

            strQ = "SELECT " & _
              "  leaveissuetranunkid " & _
              ", lvleaveIssue_tran.leaveissueunkid " & _
              ", lvleaveIssue_Master.employeeunkid " & _
              ", convert(char(8),lvleaveIssue_tran.leavedate,112) as leavedate " & _
              ", lvleavetype_master.leavetypeunkid " & _
              ", lvleavetype_master.color " & _
              ", lvleavetype_master.leavename " & _
              ", lvleavetype_master.ispaid " & _
              ", lvleaveform.formunkid " & _
              ", lvleaveform.formno " & _
              ", isnull(lvleaveIssue_tran.leaveadjustmentunkid,-1) as leaveadjustmentunkid  " & _
              ", lvleaveIssue_tran.userunkid " & _
              ", lvleaveIssue_tran.isvoid " & _
              ", lvleaveIssue_tran.voiddatetime " & _
              ", lvleaveIssue_tran.voiduserunkid " & _
              ", lvleaveIssue_tran.voidreason " & _
                      ", ISNULL(lvleaveIssue_tran.dayfraction,0) dayfraction " & _
              ", ISNULL(lvleaveIssue_tran.isprocess,0) isprocess " & _
                      ",ISNULL(lvleaveform.istnaperiodlinked,0) AS istnaperiodlinked " & _
              " FROM lvleaveIssue_tran " & _
              " LEFT JOIN lvleaveIssue_Master on lvleaveIssue_Master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
              " LEFT JOIN lvleavetype_master on lvleavetype_master.leavetypeunkid = lvleaveIssue_Master.leavetypeunkid " & _
              " LEFT JOIN lvleaveform on lvleaveform.formunkid =  lvleaveIssue_Master.formunkid " & _
              " WHERE 1=1 "

            'Pinkal (01-Jan-2019) --  'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[",ISNULL(lvleaveform.istnaperiodlinked,0) AS istnaperiodlinked " & _]

            objDataOperation.ClearParameters()

            If blnOnlyActive Then
                strQ &= " AND lvleaveIssue_tran.isvoid = 0 "
            End If

            If intFormunkid > 0 Then
                strQ &= " AND lvleaveIssue_Master.formunkid = @formunkid"
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
            End If

            If xEmployeeID > 0 Then
                strQ &= " AND lvleaveform.employeeunkid = @employeeunkid"
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeID)
            End If

            If xStartDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),lvleaveIssue_tran.leavedate,112) >= @startdate"
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xStartDate))
            End If

            If xEndDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),lvleaveIssue_tran.leavedate,112) <= @enddate"
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xEndDate))
            End If

            strQ &= " ORDER BY convert(char(8),lvleaveIssue_tran.leavedate,112) "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (lvleaveIssue_tran) </purpose>
    'Public Function InsertDelete(ByVal objDataOperation As clsDataOperation, ByVal strLeavedate() As String, ByVal intYearunkid As Integer, ByVal intEmployeeunkid As Integer _
    '                                      , ByVal intLeaveTypeunkid As Integer, Optional ByVal intLeaveBalanceSetting As Integer = -1 _
    '                                      , Optional ByVal blnLeaveApproverForLeaveType As String = "", Optional ByVal intApproverID As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Try

    '        'START FOR DELETE RECORDS FROM LEAVE ISSUE TRAN TABLE FOR SPECIFIC LEAVE ISSUE ID

    '        strQ = " SELECT  leaveissuetranunkid " & _
    '                  ", lvleaveissue_tran.leaveissueunkid " & _
    '                  ", lvleaveissue_tran.leavedate " & _
    '                  ", lvleaveday_fraction.dayfraction " & _
    '                  " FROM lvleaveissue_tran " & _
    '                  " JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
    '                  " JOIN lvleaveday_fraction ON lvleaveIssue_master.formunkid = lvleaveday_fraction.formunkid AND lvleaveissue_tran.leavedate = lvleaveday_fraction.leavedate " & _
    '                  " WHERE lvleaveissue_tran.leaveissueunkid = @leaveissue_unkid"

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@leaveissue_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        strQ = "delete from lvleaveissue_tran where leaveissuetranunkid = @leaveissuetranunkid "

    '        For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1

    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@leaveissuetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("List").Rows(i)("leaveissuetranunkid").ToString)
    '            objDataOperation.ExecNonQuery(strQ)

    '            'INSERT FOR AUDIT TRAIL
    '            mintLeaveissuetranunkid = CInt(dsList.Tables("List").Rows(i)("leaveissuetranunkid").ToString)

    '            If InsertAuditTrailForLeaveIssue(objDataOperation, 3, dsList.Tables("List").Rows(i)("leavedate").ToString, mintUserunkid) = False Then
    '                objDataOperation.ReleaseTransaction(False)
    '                Return False
    '            End If

    '        Next

    '        If dsList.Tables("List").Rows.Count > 0 Then
    '            mintTotalIssue = CDec(dsList.Tables("List").Compute("sum(dayfraction)", "1=1"))
    '        End If

    '        Dim objLeaveType As New clsleavetype_master
    '        objLeaveType._Leavetypeunkid = intLeaveTypeunkid
    '        If objLeaveType._DeductFromLeaveTypeunkid > 0 Then
    '            UpdateEmployeeAccrueBalance(objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing), IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, objLeaveType._DeductFromLeaveTypeunkid, True, , mintUserunkid, , , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID)
    '        Else
    '            UpdateEmployeeAccrueBalance(objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing), IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, intLeaveTypeunkid, True, , mintUserunkid, , , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID)
    '        End If

    '        'END FOR DELETE RECORDS FROM LEAVE ISSUE TRAN TABLE FOR SPECIFIC LEAVE ISSUE ID


    '        strQ = "INSERT INTO lvleaveIssue_tran ( " & _
    '                  "  leaveissueunkid " & _
    '                  ", leavedate " & _
    '                  ", leaveadjustmentunkid " & _
    '                  ", userunkid " & _
    '                  ", isvoid " & _
    '                  ", voiddatetime " & _
    '                  ", voiduserunkid" & _
    '                  ", voidreason" & _
    '                  ", dayfraction " & _
    '                  ", isprocess " & _
    '               ") VALUES (" & _
    '                 "  @leaveissueunkid " & _
    '                 ", @leavedate " & _
    '                 ", @leaveadjustmentunkid " & _
    '                 ", @userunkid " & _
    '                 ", @isvoid " & _
    '                 ", @voiddatetime " & _
    '                 ", @voiduserunkid" & _
    '                 ", @voidreason " & _
    '                 ", @dayfraction " & _
    '                 ", @isprocess " & _
    '                ") select @@identity; "


    '        If strLeavedate.Length > 0 Then

    '            For i As Integer = 0 To strLeavedate.Length - 1

    '                Dim objIssue As New clsleaveissue_master
    '                objIssue._ObjDataOperation = objDataOperation
    '                objIssue._Leaveissueunkid = mintLeaveissueunkid
    '                Dim objFraction As New clsleaveday_fraction
    '                mdclDayfraction = CDec(objFraction.GetEmployeeLeaveDay_Fraction(eZeeDate.convertDate(CDate(strLeavedate(i))), objIssue._Formunkid, intEmployeeunkid, blnLeaveApproverForLeaveType, intApproverID))
    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdclDayfraction.ToString)
    '                objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
    '                objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, strLeavedate(i).ToString)
    '                objDataOperation.AddParameter("@leaveadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveAdjustmentunkid)
    '                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
    '                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
    '                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, IIf(mstrVoidreason <> Nothing, mstrVoidreason.ToString, ""))
    '                objDataOperation.AddParameter("@isprocess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsProcess.ToString)
    '                dsList = objDataOperation.ExecQuery(strQ, "List")

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If
    '                mintTotalIssue += CDec(objFraction.GetEmployeeLeaveDay_Fraction(eZeeDate.convertDate(CDate(strLeavedate(i))), -1, intEmployeeunkid, blnLeaveApproverForLeaveType, intApproverID))
    '                If dsList.Tables("List").Rows.Count > 0 Then
    '                    mintLeaveissuetranunkid = CInt(dsList.Tables("List").Rows(0).Item(0))
    '                End If

    '                If InsertAuditTrailForLeaveIssue(objDataOperation, 1, strLeavedate(i).ToString, mintUserunkid) = False Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If

    '            Next


    '            'START FOR CHECK ACCRUE SETTING

    '            Dim strAtQ As String = ""
    '            Dim ispaid As Boolean = mblnIsPaid

    '            If mblnIsPaid = False Then   'FOR UNPAID LEAVE

    '                strQ = "Update lvleaveIssue_tran set ispaid = @ispaid where leaveissueunkid = @leaveissueunkid"
    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
    '                objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, ispaid)
    '                objDataOperation.ExecNonQuery(strQ)


    '                strAtQ = "Update atlvleaveIssue_tran set ispaid = @ispaid where leaveissueunkid = @leaveissueunkid"
    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
    '                objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, ispaid)
    '                objDataOperation.ExecNonQuery(strAtQ)

    '                If objLeaveType._DeductFromLeaveTypeunkid > 0 Then
    '                    UpdateEmployeeAccrueBalance(objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing), IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, objLeaveType._DeductFromLeaveTypeunkid, False, , mintUserunkid, , , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID)
    '                Else
    '                    UpdateEmployeeAccrueBalance(objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing), IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, intLeaveTypeunkid, False, , mintUserunkid, , , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID)
    '                End If


    '            ElseIf mblnIsPaid = True Then 'FOR PAID LEAVE

    '                Dim objbalance As New clsleavebalance_tran
    '                Dim dsBalance As DataSet = Nothing

    '                If objLeaveType._DeductFromLeaveTypeunkid > 0 Then
    '                    If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                        dsBalance = objbalance.GetEmployeeBalanceData(objLeaveType._DeductFromLeaveTypeunkid, intEmployeeunkid, True, intYearunkid)
    '                    ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                        dsBalance = objbalance.GetEmployeeBalanceData(objLeaveType._DeductFromLeaveTypeunkid, intEmployeeunkid, True, intYearunkid, True, True)
    '                    End If
    '                Else
    '                    If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                        dsBalance = objbalance.GetEmployeeBalanceData(intLeaveTypeunkid, intEmployeeunkid, True, intYearunkid)
    '                    ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                        dsBalance = objbalance.GetEmployeeBalanceData(intLeaveTypeunkid, intEmployeeunkid, True, intYearunkid, True, True)
    '                    End If
    '                End If

    '                If dsBalance.Tables(0).Rows.Count > 0 Then

    '                    strQ = "Update lvleaveIssue_tran set ispaid = @ispaid where leaveissueunkid = @leaveissueunkid"

    '                    strAtQ = "Update atlvleaveIssue_tran set ispaid = @ispaid where leaveissueunkid = @leaveissueunkid"


    '                    If CInt(dsBalance.Tables(0).Rows(0)("accruesetting")) = enAccrueSetting.Exceeding_balance_Unpaid Then
    '                        Dim mintLeavedays As Integer = 0

    '                        strQ &= " and leavedate = @leavedate "
    '                        strAtQ &= " and leavedate = @leavedate "

    '                        For i As Integer = 0 To strLeavedate.Length - 1

    '                            objDataOperation.ClearParameters()
    '                            objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
    '                            objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, strLeavedate(i).ToString)

    '                            If Math.Round(dsBalance.Tables(0).Rows(0)("remaining_bal") - (i + 1)) >= 0 Then
    '                                mintLeavedays += 1
    '                            Else
    '                                ispaid = False
    '                            End If
    '                            objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, ispaid)
    '                            objDataOperation.ExecNonQuery(strQ)
    '                            objDataOperation.ExecNonQuery(strAtQ)

    '                            If objDataOperation.ErrorMessage <> "" Then
    '                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                Throw exForce
    '                            End If

    '                        Next

    '                        If objLeaveType._DeductFromLeaveTypeunkid > 0 Then
    '                            UpdateEmployeeAccrueBalance(objDataOperation, mintLeavedays - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing), IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, objLeaveType._DeductFromLeaveTypeunkid, False, , mintUserunkid, , , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID)
    '                        Else
    '                            UpdateEmployeeAccrueBalance(objDataOperation, mintLeavedays - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing), IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, intLeaveTypeunkid, False, , mintUserunkid, , , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID)
    '                        End If

    '                    ElseIf CInt(dsBalance.Tables(0).Rows(0)("accruesetting")) = enAccrueSetting.Issue_Balance OrElse CInt(dsBalance.Tables(0).Rows(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance Then

    '                        objDataOperation.ClearParameters()
    '                        objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
    '                        objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, ispaid)
    '                        objDataOperation.ExecNonQuery(strQ)
    '                        objDataOperation.ExecNonQuery(strAtQ)

    '                        If objDataOperation.ErrorMessage <> "" Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If

    '                        If objLeaveType._DeductFromLeaveTypeunkid > 0 Then
    '                            UpdateEmployeeAccrueBalance(objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing), IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, objLeaveType._DeductFromLeaveTypeunkid, False, , mintUserunkid, , , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID)
    '                        Else
    '                            UpdateEmployeeAccrueBalance(objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing), IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, intLeaveTypeunkid, False, , mintUserunkid, , , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID)
    '                        End If

    '                    End If


    '                    'Pinkal (06-Mar-2014) -- Start
    '                    'Enhancement : Oman Changes   [IT IS USED WHEN LEAVE TYPE IS PAID LEAVE WITH NO ACCRUE AMOUNT]

    '                Else

    '                    strQ = "Update lvleaveIssue_tran set ispaid = @ispaid where leaveissueunkid = @leaveissueunkid  and leavedate = @leavedate "
    '                    strAtQ = "Update atlvleaveIssue_tran set ispaid = @ispaid where leaveissueunkid = @leaveissueunkid and leavedate = @leavedate "

    '                    For i As Integer = 0 To strLeavedate.Length - 1

    '                        objDataOperation.ClearParameters()
    '                        objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
    '                        objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, strLeavedate(i).ToString)
    '                        objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, ispaid)
    '                        objDataOperation.ExecNonQuery(strQ)
    '                        objDataOperation.ExecNonQuery(strAtQ)

    '                        If objDataOperation.ErrorMessage <> "" Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If

    '                    Next

    '                End If

    '                'Pinkal (06-Mar-2014) -- End

    '            End If

    '            'END FOR CHECK ACCRUE SETTING

    '        End If

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveIssue_tran) </purpose>
    Public Function InsertDelete(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                             , ByVal xCompanyUnkid As Integer _
                                             , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                             , ByVal xIncludeIn_ActiveEmployee As Boolean, ByVal objDataOperation As clsDataOperation, ByVal strLeavedate() As String, ByVal intYearunkid As Integer, ByVal intEmployeeunkid As Integer _
                                          , ByVal intLeaveTypeunkid As Integer, Optional ByVal intLeaveBalanceSetting As Integer = -1 _
                                             , Optional ByVal blnLeaveApproverForLeaveType As String = "", Optional ByVal intApproverID As Integer = -1 _
                                             , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                             , Optional ByVal mblnIsExternalApprover As Boolean = False) As Boolean


        'Pinkal (01-Mar-2016) -- Implementing External Approver in Claim Request & Leave Module.[Optional ByVal mblnIsExternalApprover As Boolean = False]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            'START FOR DELETE RECORDS FROM LEAVE ISSUE TRAN TABLE FOR SPECIFIC LEAVE ISSUE ID

            strQ = " SELECT  leaveissuetranunkid " & _
                      ", lvleaveissue_tran.leaveissueunkid " & _
                      ", lvleaveissue_tran.leavedate " & _
                      ", lvleaveday_fraction.dayfraction " & _
                      " FROM lvleaveissue_tran " & _
                      " JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
                      " JOIN lvleaveday_fraction ON lvleaveIssue_master.formunkid = lvleaveday_fraction.formunkid AND lvleaveissue_tran.leavedate = lvleaveday_fraction.leavedate " & _
                      " WHERE lvleaveissue_tran.leaveissueunkid = @leaveissue_unkid"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leaveissue_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            strQ = "delete from lvleaveissue_tran where leaveissuetranunkid = @leaveissuetranunkid "

            For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@leaveissuetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("List").Rows(i)("leaveissuetranunkid").ToString)
                objDataOperation.ExecNonQuery(strQ)

                'INSERT FOR AUDIT TRAIL
                mintLeaveissuetranunkid = CInt(dsList.Tables("List").Rows(i)("leaveissuetranunkid").ToString)

                If InsertAuditTrailForLeaveIssue(objDataOperation, 3, dsList.Tables("List").Rows(i)("leavedate").ToString, mintUserunkid) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            Next

            If dsList.Tables("List").Rows.Count > 0 Then
                mintTotalIssue = CDec(dsList.Tables("List").Compute("sum(dayfraction)", "1=1"))
            End If

            Dim objLeaveType As New clsleavetype_master
            'Pinkal (02-Dec-2015) -- Start
            'Enhancement - Solving Leave bug in Self Service.
            objLeaveType._DoOperation = objDataOperation
            'Pinkal (02-Dec-2015) -- End
            objLeaveType._Leavetypeunkid = intLeaveTypeunkid


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

            'If objLeaveType._DeductFromLeaveTypeunkid > 0 Then
            '    UpdateEmployeeAccrueBalance(objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing), IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, objLeaveType._DeductFromLeaveTypeunkid, True, , mintUserunkid, , , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID)
            'Else
            '    UpdateEmployeeAccrueBalance(objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing), IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, intLeaveTypeunkid, True, , mintUserunkid, , , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID)
            'End If

            If objLeaveType._DeductFromLeaveTypeunkid > 0 Then


                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                'UpdateEmployeeAccrueBalance(xDatabaseName, xCompanyUnkid, objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing) _
                '                                                , IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, objLeaveType._DeductFromLeaveTypeunkid _
                '                                                , True, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, 0, mintUserunkid, Nothing, Nothing _
                '                                                , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID, blnApplyUserAccessFilter)

                UpdateEmployeeAccrueBalance(xDatabaseName, xCompanyUnkid, objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing) _
                                                                , IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, objLeaveType._DeductFromLeaveTypeunkid _
                                                                , True, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, 0, mintUserunkid, Nothing, Nothing _
                                                               , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID, blnApplyUserAccessFilter, mblnIsExternalApprover)

                'Pinkal (01-Mar-2016) -- End

            Else


                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                'UpdateEmployeeAccrueBalance(xDatabaseName, xCompanyUnkid, objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing) _
                '                                             , IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, intLeaveTypeunkid _
                '                                             , True, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, 0, mintUserunkid, Nothing, Nothing _
                '                                             , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID, blnApplyUserAccessFilter)

                UpdateEmployeeAccrueBalance(xDatabaseName, xCompanyUnkid, objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing) _
                                                             , IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, intLeaveTypeunkid _
                                                             , True, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, 0, mintUserunkid, Nothing, Nothing _
                                                            , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID, blnApplyUserAccessFilter, mblnIsExternalApprover)

                'Pinkal (01-Mar-2016) -- End

            End If


            'Pinkal (24-Aug-2015) -- End

            'END FOR DELETE RECORDS FROM LEAVE ISSUE TRAN TABLE FOR SPECIFIC LEAVE ISSUE ID


            strQ = "INSERT INTO lvleaveIssue_tran ( " & _
             "  leaveissueunkid " & _
             ", leavedate " & _
             ", leaveadjustmentunkid " & _
             ", userunkid " & _
             ", isvoid " & _
             ", voiddatetime " & _
             ", voiduserunkid" & _
             ", voidreason" & _
                       ", dayfraction " & _
                     ", isprocess " & _
                         ", istnaperiodlinked " & _
           ") VALUES (" & _
             "  @leaveissueunkid " & _
             ", @leavedate " & _
             ", @leaveadjustmentunkid " & _
             ", @userunkid " & _
             ", @isvoid " & _
             ", @voiddatetime " & _
             ", @voiduserunkid" & _
             ", @voidreason " & _
                       ", @dayfraction " & _
                     ", @isprocess " & _
                         ", @istnaperiodlinked " & _
           ") select @@identity; "

            'Pinkal (01-Jan-2019) -- 'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[ ", @istnaperiodlinked " & _]

            If strLeavedate.Length > 0 Then

                For i As Integer = 0 To strLeavedate.Length - 1

                    Dim objIssue As New clsleaveissue_master
                    objIssue._ObjDataOperation = objDataOperation
                    objIssue._Leaveissueunkid = mintLeaveissueunkid
                    Dim objFraction As New clsleaveday_fraction

                    'Pinkal (01-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.

                    If mblnSkipApproverFlow Then
                        mdclDayfraction = CDec(objFraction.GetEmpLeaveDayFractionForSkipApproverFlow(eZeeDate.convertDate(CDate(strLeavedate(i))), objIssue._Formunkid, intEmployeeunkid, objDataOperation))
                    Else
                    mdclDayfraction = CDec(objFraction.GetEmployeeLeaveDay_Fraction(eZeeDate.convertDate(CDate(strLeavedate(i))), objIssue._Formunkid, intEmployeeunkid, blnLeaveApproverForLeaveType, intApproverID, objDataOperation))
                    End If

                    objFraction = Nothing
                    'Pinkal (01-Oct-2018) -- End

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdclDayfraction.ToString)
                    objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
                    objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, strLeavedate(i).ToString)
                    objDataOperation.AddParameter("@leaveadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveAdjustmentunkid)
                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, IIf(mstrVoidreason <> Nothing, mstrVoidreason.ToString, ""))
                    objDataOperation.AddParameter("@isprocess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsProcess.ToString)

                    'Pinkal (01-Jan-2019) -- Start
                    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                    objDataOperation.AddParameter("@istnaperiodlinked", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnConsiderLeaveOnTnAPeriod)
                    'Pinkal (01-Jan-2019) -- End

                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    'Pinkal (01-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.
                    'mintTotalIssue += CDec(objFraction.GetEmployeeLeaveDay_Fraction(eZeeDate.convertDate(CDate(strLeavedate(i))), -1, intEmployeeunkid, blnLeaveApproverForLeaveType, intApproverID, objDataOperation))
                    mintTotalIssue += mdclDayfraction
                    'Pinkal (01-Oct-2018) -- End

                    If dsList.Tables("List").Rows.Count > 0 Then
                        mintLeaveissuetranunkid = CInt(dsList.Tables("List").Rows(0).Item(0))
                    End If

                    If InsertAuditTrailForLeaveIssue(objDataOperation, 1, strLeavedate(i).ToString, mintUserunkid) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If

                Next


                'START FOR CHECK ACCRUE SETTING

                Dim strAtQ As String = ""
                Dim ispaid As Boolean = mblnIsPaid

                If mblnIsPaid = False Then   'FOR UNPAID LEAVE

                    strQ = "Update lvleaveIssue_tran set ispaid = @ispaid where leaveissueunkid = @leaveissueunkid"
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
                    objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, ispaid)
                    objDataOperation.ExecNonQuery(strQ)


                    strAtQ = "Update atlvleaveIssue_tran set ispaid = @ispaid where leaveissueunkid = @leaveissueunkid"
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
                    objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, ispaid)
                    objDataOperation.ExecNonQuery(strAtQ)


                    'Pinkal (24-Aug-2015) -- Start
                    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

                    'If objLeaveType._DeductFromLeaveTypeunkid > 0 Then
                    '    UpdateEmployeeAccrueBalance(objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing), IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, objLeaveType._DeductFromLeaveTypeunkid, False, , mintUserunkid, , , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID)
                    'Else
                    '    UpdateEmployeeAccrueBalance(objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing), IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, intLeaveTypeunkid, False, , mintUserunkid, , , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID)
                    'End If

                    If objLeaveType._DeductFromLeaveTypeunkid > 0 Then

                        'Pinkal (01-Mar-2016) -- Start
                        'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                        'UpdateEmployeeAccrueBalance(xDatabaseName, xCompanyUnkid, objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing) _
                        '                                               , IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, objLeaveType._DeductFromLeaveTypeunkid _
                        '                                               , False, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, 0, mintUserunkid, Nothing, Nothing _
                        '                                               , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID, blnApplyUserAccessFilter)

                        UpdateEmployeeAccrueBalance(xDatabaseName, xCompanyUnkid, objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing) _
                                                                       , IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, objLeaveType._DeductFromLeaveTypeunkid _
                                                                       , False, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, 0, mintUserunkid, Nothing, Nothing _
                                                                       , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID, blnApplyUserAccessFilter, mblnIsExternalApprover)

                        'Pinkal (01-Mar-2016) -- End

                    Else


                        'Pinkal (01-Mar-2016) -- Start
                        'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                        'UpdateEmployeeAccrueBalance(xDatabaseName, xCompanyUnkid, objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing) _
                        '                                              , IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, intLeaveTypeunkid _
                        '                                              , False, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, 0, mintUserunkid, Nothing, Nothing _
                        '                                              , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID, blnApplyUserAccessFilter)


                        UpdateEmployeeAccrueBalance(xDatabaseName, xCompanyUnkid, objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing) _
                                                                      , IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, intLeaveTypeunkid _
                                                                      , False, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, 0, mintUserunkid, Nothing, Nothing _
                                                                      , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID, blnApplyUserAccessFilter, mblnIsExternalApprover)

                        'Pinkal (01-Mar-2016) -- End

                    End If

                    'Pinkal (24-Aug-2015) -- End


                ElseIf mblnIsPaid = True Then 'FOR PAID LEAVE

                    Dim objbalance As New clsleavebalance_tran
                    Dim dsBalance As DataSet = Nothing


                    If objLeaveType._DeductFromLeaveTypeunkid > 0 Then
                        If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                            dsBalance = objbalance.GetEmployeeBalanceData(objLeaveType._DeductFromLeaveTypeunkid, intEmployeeunkid, True, intYearunkid, False, False, False, objDataOperation)
                        ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                            dsBalance = objbalance.GetEmployeeBalanceData(objLeaveType._DeductFromLeaveTypeunkid, intEmployeeunkid, True, intYearunkid, True, True, False, objDataOperation)
                        End If
                    Else
                        If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                            dsBalance = objbalance.GetEmployeeBalanceData(intLeaveTypeunkid, intEmployeeunkid, True, intYearunkid, False, False, False, objDataOperation)
                        ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                            dsBalance = objbalance.GetEmployeeBalanceData(intLeaveTypeunkid, intEmployeeunkid, True, intYearunkid, True, True, False, objDataOperation)
                        End If
                    End If

                    'Pinkal (02-Dec-2015) -- End


                    If dsBalance.Tables(0).Rows.Count > 0 Then

                        strQ = "Update lvleaveIssue_tran set ispaid = @ispaid where leaveissueunkid = @leaveissueunkid"

                        strAtQ = "Update atlvleaveIssue_tran set ispaid = @ispaid where leaveissueunkid = @leaveissueunkid"


                        If CInt(dsBalance.Tables(0).Rows(0)("accruesetting")) = enAccrueSetting.Exceeding_balance_Unpaid Then
                            Dim mintLeavedays As Integer = 0

                            strQ &= " and leavedate = @leavedate "
                            strAtQ &= " and leavedate = @leavedate "

                            For i As Integer = 0 To strLeavedate.Length - 1

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
                                objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, strLeavedate(i).ToString)

                                If Math.Round(dsBalance.Tables(0).Rows(0)("remaining_bal") - (i + 1)) >= 0 Then
                                    mintLeavedays += 1
                                Else
                                    ispaid = False
                                End If
                                objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, ispaid)
                                objDataOperation.ExecNonQuery(strQ)
                                objDataOperation.ExecNonQuery(strAtQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Next


                            If objLeaveType._DeductFromLeaveTypeunkid > 0 Then

                                'Pinkal (01-Mar-2016) -- Start
                                'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                                'UpdateEmployeeAccrueBalance(xDatabaseName, xCompanyUnkid, objDataOperation, mintLeavedays - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing) _
                                '                                              , IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, objLeaveType._DeductFromLeaveTypeunkid _
                                '                                              , False, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, 0, mintUserunkid, Nothing, Nothing _
                                '                                              , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID, blnApplyUserAccessFilter)
                                UpdateEmployeeAccrueBalance(xDatabaseName, xCompanyUnkid, objDataOperation, mintLeavedays - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing) _
                                                                              , IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, objLeaveType._DeductFromLeaveTypeunkid _
                                                                              , False, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, 0, mintUserunkid, Nothing, Nothing _
                                                                           , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID, blnApplyUserAccessFilter, mblnIsExternalApprover)

                                'Pinkal (01-Mar-2016) -- End

                            Else

                                'Pinkal (01-Mar-2016) -- Start
                                'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                                'UpdateEmployeeAccrueBalance(xDatabaseName, xCompanyUnkid, objDataOperation, mintLeavedays - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing) _
                                '                                               , IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, intLeaveTypeunkid _
                                '                                               , False, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, 0, mintUserunkid, Nothing, Nothing _
                                '                                               , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID, blnApplyUserAccessFilter)

                                UpdateEmployeeAccrueBalance(xDatabaseName, xCompanyUnkid, objDataOperation, mintLeavedays - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing) _
                                                                               , IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, intLeaveTypeunkid _
                                                                               , False, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, 0, mintUserunkid, Nothing, Nothing _
                                                                            , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID, blnApplyUserAccessFilter, mblnIsExternalApprover)

                                'Pinkal (01-Mar-2016) -- End

                            End If

                            'Pinkal (24-Aug-2015) -- End


                            'Pinkal (18-Nov-2016) -- Start
                            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

                        ElseIf CInt(dsBalance.Tables(0).Rows(0)("accruesetting")) = enAccrueSetting.Issue_Balance OrElse CInt(dsBalance.Tables(0).Rows(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance OrElse CInt(dsBalance.Tables(0).Rows(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date Then

                            'Pinkal (18-Nov-2016) -- End

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
                            objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, ispaid)
                            objDataOperation.ExecNonQuery(strQ)
                            objDataOperation.ExecNonQuery(strAtQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If


                            'Pinkal (24-Aug-2015) -- Start
                            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

                            'If objLeaveType._DeductFromLeaveTypeunkid > 0 Then
                            '    UpdateEmployeeAccrueBalance(objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing), IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, objLeaveType._DeductFromLeaveTypeunkid, False, , mintUserunkid, , , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID)
                            'Else
                            '    UpdateEmployeeAccrueBalance(objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing), IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, intLeaveTypeunkid, False, , mintUserunkid, , , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID)
                            'End If

                            If objLeaveType._DeductFromLeaveTypeunkid > 0 Then

                                'Pinkal (01-Mar-2016) -- Start
                                'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                                'UpdateEmployeeAccrueBalance(xDatabaseName, xCompanyUnkid, objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing) _
                                '                                                , IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, objLeaveType._DeductFromLeaveTypeunkid _
                                '                                                , False, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, 0, mintUserunkid, Nothing, Nothing _
                                '                                                , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID, blnApplyUserAccessFilter)

                                UpdateEmployeeAccrueBalance(xDatabaseName, xCompanyUnkid, objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing) _
                                                                                , IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, objLeaveType._DeductFromLeaveTypeunkid _
                                                                                , False, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, 0, mintUserunkid, Nothing, Nothing _
                                                                             , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID, blnApplyUserAccessFilter, mblnIsExternalApprover)

                                'Pinkal (01-Mar-2016) -- End

                            Else

                                'Pinkal (01-Mar-2016) -- Start
                                'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                                'UpdateEmployeeAccrueBalance(xDatabaseName, xCompanyUnkid, objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing) _
                                '                                              , IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, intLeaveTypeunkid _
                                '                                              , False, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, 0, mintUserunkid, Nothing, Nothing _
                                '                                              , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID, blnApplyUserAccessFilter)

                                UpdateEmployeeAccrueBalance(xDatabaseName, xCompanyUnkid, objDataOperation, strLeavedate.Length - 1, IIf(strLeavedate.Length > 0, strLeavedate(0), Nothing) _
                                                                              , IIf(strLeavedate.Length > 0, strLeavedate(strLeavedate.Length - 1), Nothing), intYearunkid, intEmployeeunkid, intLeaveTypeunkid _
                                                                              , False, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, True, 0, mintUserunkid, Nothing, Nothing _
                                                                       , blnLeaveApproverForLeaveType, intLeaveBalanceSetting, intApproverID, blnApplyUserAccessFilter, mblnIsExternalApprover)

                                'Pinkal (01-Mar-2016) -- End

                            End If

                            'Pinkal (24-Aug-2015) -- End

                        End If


                        'Pinkal (06-Mar-2014) -- Start
                        'Enhancement : Oman Changes   [IT IS USED WHEN LEAVE TYPE IS PAID LEAVE WITH NO ACCRUE AMOUNT]

                    Else

                        strQ = "Update lvleaveIssue_tran set ispaid = @ispaid where leaveissueunkid = @leaveissueunkid  and leavedate = @leavedate "
                        strAtQ = "Update atlvleaveIssue_tran set ispaid = @ispaid where leaveissueunkid = @leaveissueunkid and leavedate = @leavedate "

                        For i As Integer = 0 To strLeavedate.Length - 1

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
                            objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, strLeavedate(i).ToString)
                            objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, ispaid)
                            objDataOperation.ExecNonQuery(strQ)
                            objDataOperation.ExecNonQuery(strAtQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                    End If

                        Next

                    End If

                    'Pinkal (06-Mar-2014) -- End

                End If

                'END FOR CHECK ACCRUE SETTING

            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvleaveIssue_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            'START FOR UPDATE ACCRUE LEAVE TABLES

            strQ = " Select isnull(leaveyearunkid,0) as leaveyearunkid,isnull(leavetypeunkid,0) as leavetypeunkid,isnull(employeeunkid,0) as employeeunkid " & _
                   " From lvleaveIssue_master " & _
                   " WHERE leaveissueunkid in " & _
                   " (Select isnull(leaveissueunkid,0) from lvleaveIssue_tran WHERE leaveissuetranunkid = @leaveissuetranunkid) "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leaveissuetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList.Tables("List").Rows.Count > 0 Then

                'START FOR GET LEAVE ACCRUE UNK ID

                'END FOR GET LEAVE ACCRUE UNK ID

            End If

            'END FOR UPDATE ACCRUE LEAVE TABLES

            ' START FOR VOID IN LEAVE ISSUE TRAN TABLE

            strQ = "Update lvleaveIssue_tran set isvoid=1,voiddatetime=getdate(),voidreason=@voidreason" & _
            " WHERE leaveissuetranunkid = @leaveissuetranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@leaveissuetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            objDataOperation.ExecNonQuery(strQ)

            ' END FOR VOID IN LEAVE ISSUE TRAN TABLE

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  leaveissuetranunkid " & _
              ", leaveissueunkid " & _
              ", leavedate " & _
              ", ispaid " & _
              ", leaveadjustmentunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
             "FROM lvleaveIssue_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND leaveissuetranunkid <> @leaveissuetranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@leaveissuetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function



    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function UpdateEmployeeAccrueBalance(ByVal objDataOperation As clsDataOperation, ByVal noofDays As Integer, ByVal Startdate As DateTime, ByVal Enddate As DateTime, _
    '                                            ByVal intYearunkid As Integer, ByVal intEmployeeunkid As Integer, ByVal intLeaveTypeunkid As Integer, ByVal isDelete As Boolean, _
    '                                            Optional ByVal intBatchunkid As Integer = 0, Optional ByVal intUserId As Integer = 0, Optional ByVal mdtDbStartdate As Date = Nothing, _
    '                                            Optional ByVal mdtDbEndDate As Date = Nothing, Optional ByVal blnLeaveApproverForLeaveType As String = "", _
    '                                            Optional ByVal intLeaveBalanceSetting As Integer = -1, Optional ByVal intApproverID As Integer = -1) As Boolean


    '    Dim dsList As DataSet = Nothing
    '    Dim days As Integer = 0
    '    Dim Remaining_bal As Double = 0
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim intleaveaccrueunkid As Integer = 0
    '    Dim iRemaining_Occurrence As Double = 0
    '    Try

    '        If mintTotalIssue = 0 Then Return True

    '        Dim objAccure As New clsleavebalance_tran

    '        If intLeaveBalanceSetting <= 0 Then
    '            intLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
    '        End If

    '        If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '            dsList = objAccure.GetList("AccrueList", True, , intEmployeeunkid)
    '        ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '            dsList = objAccure.GetList("AccrueList", True, False, intEmployeeunkid, True, True)
    '        End If


    '        If dsList IsNot Nothing Then          '----------------------(1)

    '            If dsList.Tables(0).Rows.Count > 0 Then            '----------------------(2)

    '                Dim drRow As DataRow() = dsList.Tables(0).Select("yearunkid =" & intYearunkid & " AND leavetypeunkid=" & intLeaveTypeunkid)

    '                'START FOR UPDATE ISSUE AND BALANCE AMOUNT IN LEAVE ACCRUE TRAN TABLE
    '                Dim GetIssueAmt As Integer = 0
    '                Dim Accruebal As Double = 0
    '                Dim balance As Double = 0
    '                Dim isAdvance As Boolean = False
    '                Dim isFirstDay As Boolean = True



    '                If drRow.Length > 0 Then    '----------------------(3)

    '                    If isDelete Then       '----------------------(4)

    '                        'START FOR UPDATE LAST STATUS OF LEAVE ISSUE TRAN

    '                        Dim dsBalance As DataSet = objAccure.GetAuditDataforLeaveBalance(CInt(drRow(0)("leavebalanceunkid")), intYearunkid, intLeaveTypeunkid, intEmployeeunkid)

    '                        If dsBalance.Tables(0).Rows.Count > 0 Then
    '                            strQ = "delete from atlvleavebalance_tran where atunkid = @atunkid "
    '                            objDataOperation.ClearParameters()
    '                            objDataOperation.AddParameter("@atunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsBalance.Tables(0).Rows(0)("atunkid"))
    '                            objDataOperation.ExecNonQuery(strQ)


    '                            If objDataOperation.ErrorMessage <> "" Then
    '                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                Throw exForce
    '                            End If

    '                        End If

    '                        dsBalance = objAccure.GetAuditDataforLeaveBalance(CInt(drRow(0)("leavebalanceunkid")), intYearunkid, intLeaveTypeunkid, intEmployeeunkid)
    '                        If dsBalance.Tables(0).Rows.Count > 0 Then

    '                            strQ = " Update lvleavebalance_tran set " & _
    '                              "  issue_amount =  @issue_amount " & _
    '                              ", balance = @balance  " & _
    '                              ", uptolstyr_issueamt =  @uptolstyr_issueamt " & _
    '                              ", remaining_bal = @remaining_bal " & _
    '                              ", days = @days " & _
    '                              ", remaining_occurrence = @remaining_occurrence " & _
    '                              " where leavebalanceunkid = @leavebalanceunkid  AND isvoid = 0 "


    '                            If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                                strQ &= " AND isopenelc = 1 AND iselc = 1"
    '                            End If

    '                            objDataOperation.ClearParameters()
    '                            objDataOperation.AddParameter("@issue_amount", SqlDbType.Int, eZeeDataType.INT_SIZE, dsBalance.Tables(0).Rows(0)("issue_amount"))
    '                            objDataOperation.AddParameter("@balance", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dsBalance.Tables(0).Rows(0)("balance"))
    '                            objDataOperation.AddParameter("@remaining_bal", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dsBalance.Tables(0).Rows(0)("remaining_bal"))
    '                            objDataOperation.AddParameter("@uptolstyr_issueamt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dsBalance.Tables(0).Rows(0)("uptolstyr_issueamt"))
    '                            objDataOperation.AddParameter("@days", SqlDbType.Int, eZeeDataType.INT_SIZE, dsBalance.Tables(0).Rows(0)("days"))
    '                            objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow(0)("leavebalanceunkid").ToString())
    '                            objDataOperation.AddParameter("@remaining_occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow(0)("remaining_occurrence").ToString())
    '                            objDataOperation.ExecNonQuery(strQ)

    '                            If objDataOperation.ErrorMessage <> "" Then
    '                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                Throw exForce
    '                            End If

    '                        End If

    '                        ' END FOR UPDATE LAST STATUS OF LEAVE ISSUE TRAN

    '                    Else

    '                        'START FOR CALCULATE BALANCE

    '                        objAccure._LeaveBalanceunkid = CInt(drRow(0)("leavebalanceunkid").ToString())
    '                        intBatchunkid = objAccure._Batchunkid
    '                        mdblTotalAccrue = objAccure._AccrueAmount
    '                        If objAccure._IsshortLeave = False Then


    '                            'Pinkal (09-Jul-2014) -- Start
    '                            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2

    '                            'For i As Integer = 0 To noofDays

    '                            '    If objAccure._Startdate.Date > Startdate.Date.AddDays(i) Then
    '                            '        Accruebal = 0
    '                            '        isAdvance = True
    '                            '    Else

    '                            '        If objAccure._Days = 0 Then



    '                            '            'Pinkal (06-Feb-2013) -- Start
    '                            '            'Enhancement : TRA Changes

    '                            '            If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then  'FINANCIAL YEAR

    '                            '                If mdtDbStartdate = Nothing Then
    '                            '                    If objAccure._Startdate.Date > FinancialYear._Object._Database_Start_Date.Date Then
    '                            '                        Accruebal = objAccure._Daily_Amount * DateDiff(DateInterval.Day, objAccure._Startdate.Date, Enddate.Date)
    '                            '                    Else
    '                            '                        Accruebal = objAccure._Daily_Amount * DateDiff(DateInterval.Day, FinancialYear._Object._Database_Start_Date.Date, Enddate.Date)
    '                            '                    End If
    '                            '                Else
    '                            '                    If objAccure._Startdate.Date > mdtDbStartdate.Date Then
    '                            '                        Accruebal = objAccure._Daily_Amount * DateDiff(DateInterval.Day, objAccure._Startdate.Date, Enddate.Date)
    '                            '                    Else
    '                            '                        Accruebal = objAccure._Daily_Amount * DateDiff(DateInterval.Day, mdtDbStartdate.Date, Enddate.Date)
    '                            '                    End If
    '                            '                End If

    '                            '            ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then  'ELC

    '                            '                If mdtDbStartdate = Nothing Then
    '                            '                    Accruebal = objAccure._Daily_Amount * DateDiff(DateInterval.Day, objAccure._Startdate.Date, Enddate.Date)

    '                            '                Else
    '                            '                    If objAccure._Startdate.Date > mdtDbStartdate.Date Then
    '                            '                        Accruebal = objAccure._Daily_Amount * DateDiff(DateInterval.Day, objAccure._Startdate.Date, Enddate.Date)
    '                            '                    Else
    '                            '                        Accruebal = objAccure._Daily_Amount * DateDiff(DateInterval.Day, mdtDbStartdate.Date, Enddate.Date)
    '                            '                    End If

    '                            '                End If

    '                            '            End If

    '                            '            'Pinkal (06-Feb-2013) -- End


    '                            '        ElseIf objAccure._Days > 0 Then



    '                            '            'Pinkal (06-Feb-2013) -- Start
    '                            '            'Enhancement : TRA Changes

    '                            '            If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then  'FINANCIAL YEAR

    '                            '                If mdtDbStartdate = Nothing Then
    '                            '                    If objAccure._Startdate.Date > FinancialYear._Object._Database_Start_Date.Date Then
    '                            '                        Accruebal = objAccure._Daily_Amount * (DateDiff(DateInterval.Day, objAccure._Startdate.Date, Enddate.Date) - objAccure._Days)
    '                            '                    Else
    '                            '                        Accruebal = objAccure._Daily_Amount * (DateDiff(DateInterval.Day, FinancialYear._Object._Database_Start_Date.Date, Enddate.Date) - objAccure._Days)
    '                            '                    End If
    '                            '                Else
    '                            '                    If objAccure._Startdate.Date > mdtDbStartdate.Date Then
    '                            '                        Accruebal = objAccure._Daily_Amount * (DateDiff(DateInterval.Day, objAccure._Startdate.Date, Enddate.Date) - objAccure._Days)
    '                            '                    Else
    '                            '                        Accruebal = objAccure._Daily_Amount * (DateDiff(DateInterval.Day, mdtDbStartdate.Date, Enddate.Date) - objAccure._Days)
    '                            '                    End If
    '                            '                End If

    '                            '            ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then  'ELC

    '                            '                If mdtDbStartdate = Nothing Then
    '                            '                    Accruebal = objAccure._Daily_Amount * (DateDiff(DateInterval.Day, objAccure._Startdate.Date, Enddate.Date) - objAccure._Days)
    '                            '                Else

    '                            '                    If objAccure._Startdate.Date > mdtDbStartdate.Date Then
    '                            '                        Accruebal = objAccure._Daily_Amount * (DateDiff(DateInterval.Day, objAccure._Startdate.Date, Enddate.Date) - objAccure._Days)
    '                            '                    Else
    '                            '                        Accruebal = objAccure._Daily_Amount * (DateDiff(DateInterval.Day, mdtDbStartdate.Date, Enddate.Date) - objAccure._Days)
    '                            '                    End If

    '                            '                End If

    '                            '            End If

    '                            '            'Pinkal (06-Feb-2013) -- End

    '                            '        End If

    '                            '    End If


    '                            '    Dim objLeaveFraction As New clsleaveday_fraction


    '                            '    'Pinkal (15-Jul-2013) -- Start
    '                            '    'Enhancement : TRA Changes
    '                            '    'Dim mdclFraction As Decimal = objLeaveFraction.GetEmployeeLeaveDay_Fraction(eZeeDate.convertDate(Startdate.Date.AddDays(i)), -1, intEmployeeunkid, blnLeaveApproverForLeaveType)
    '                            '    Dim mdclFraction As Decimal = objLeaveFraction.GetEmployeeLeaveDay_Fraction(eZeeDate.convertDate(Startdate.Date.AddDays(i)), -1, intEmployeeunkid, blnLeaveApproverForLeaveType, intApproverID)
    '                            '    'Pinkal (15-Jul-2013) -- End


    '                            '    If Accruebal = 0 And isAdvance = True Then
    '                            '        balance = Accruebal + objAccure._Balance - mdclFraction
    '                            '        isAdvance = False
    '                            '    ElseIf isAdvance = False And isFirstDay = True Then
    '                            '        balance = Accruebal + objAccure._Balance - mdclFraction
    '                            '        isFirstDay = False
    '                            '    Else
    '                            '        balance = objAccure._Daily_Amount + objAccure._Balance - mdclFraction
    '                            '    End If

    '                            '    objAccure._Balance = balance

    '                            'Next


    '                            ''Pinkal (06-Feb-2013) -- Start
    '                            ''Enhancement : TRA Changes

    '                            'If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then  'FINANCIAL YEAR

    '                            '    'START FOR CALCULATE REMAINING BALANCE

    '                            '    If mdtDbEndDate = Nothing Then

    '                            '        If objAccure._Enddate <> Nothing And objAccure._Enddate < FinancialYear._Object._Database_End_Date.Date Then
    '                            '            Remaining_bal = balance + (objAccure._Daily_Amount * DateDiff(DateInterval.Day, Enddate.Date.AddDays(1), objAccure._Enddate))
    '                            '        Else
    '                            '            Remaining_bal = balance + (objAccure._Daily_Amount * DateDiff(DateInterval.Day, Enddate.Date.AddDays(1), FinancialYear._Object._Database_End_Date.Date))
    '                            '        End If

    '                            '    Else
    '                            '        If objAccure._Enddate <> Nothing And objAccure._Enddate < mdtDbEndDate.Date Then
    '                            '            Remaining_bal = balance + (objAccure._Daily_Amount * DateDiff(DateInterval.Day, Enddate.Date.AddDays(1), objAccure._Enddate))
    '                            '        Else
    '                            '            Remaining_bal = balance + (objAccure._Daily_Amount * DateDiff(DateInterval.Day, Enddate.Date.AddDays(1), mdtDbEndDate.Date))
    '                            '        End If
    '                            '    End If

    '                            '    'END FOR CALCULATE REMAINING BALANCE

    '                            '    'START FOR CALCULATE DAYS

    '                            '    If mdtDbStartdate = Nothing Then

    '                            '        If objAccure._Startdate.Date > FinancialYear._Object._Database_Start_Date.Date Then
    '                            '            days = IIf(DateDiff(DateInterval.Day, objAccure._Startdate.Date, Enddate.Date) > 0, DateDiff(DateInterval.Day, objAccure._Startdate.Date, Enddate.Date), 0)
    '                            '        Else
    '                            '            days = IIf(DateDiff(DateInterval.Day, FinancialYear._Object._Database_Start_Date.Date, Enddate.Date) > 0, DateDiff(DateInterval.Day, FinancialYear._Object._Database_Start_Date.Date, Enddate.Date), 0)
    '                            '        End If

    '                            '    Else

    '                            '        If objAccure._Startdate.Date > mdtDbStartdate.Date Then
    '                            '            days = IIf(DateDiff(DateInterval.Day, objAccure._Startdate.Date, Enddate.Date) > 0, DateDiff(DateInterval.Day, objAccure._Startdate.Date, Enddate.Date), 0)
    '                            '        Else
    '                            '            days = IIf(DateDiff(DateInterval.Day, mdtDbStartdate.Date, Enddate.Date) > 0, DateDiff(DateInterval.Day, mdtDbStartdate.Date, Enddate.Date), 0)
    '                            '        End If

    '                            '    End If

    '                            '    'END FOR CALCULATE DAYS

    '                            'ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then   'ELC

    '                            '    'START FOR CALCULATE REMAINING BALANCE

    '                            '    If mdtDbEndDate = Nothing Then

    '                            '        If objAccure._Enddate <> Nothing Then
    '                            '            Remaining_bal = balance + (objAccure._Daily_Amount * DateDiff(DateInterval.Day, Enddate.Date.AddDays(1), objAccure._Enddate))
    '                            '        End If

    '                            '    Else

    '                            '        If objAccure._Enddate <> Nothing And objAccure._Enddate < mdtDbEndDate.Date Then
    '                            '            Remaining_bal = balance + (objAccure._Daily_Amount * DateDiff(DateInterval.Day, Enddate.Date.AddDays(1), objAccure._Enddate))
    '                            '        Else
    '                            '            Remaining_bal = balance + (objAccure._Daily_Amount * DateDiff(DateInterval.Day, Enddate.Date.AddDays(1), mdtDbEndDate.Date))
    '                            '        End If

    '                            '    End If

    '                            '    'END FOR CALCULATE REMAINING BALANCE

    '                            '    'START FOR CALCULATE DAYS

    '                            '    If mdtDbStartdate = Nothing Then
    '                            '        days = IIf(DateDiff(DateInterval.Day, objAccure._Startdate.Date, Enddate.Date) > 0, DateDiff(DateInterval.Day, objAccure._Startdate.Date, Enddate.Date), 0)
    '                            '    Else

    '                            '        If objAccure._Startdate.Date > mdtDbStartdate.Date Then
    '                            '            days = IIf(DateDiff(DateInterval.Day, objAccure._Startdate.Date, Enddate.Date) > 0, DateDiff(DateInterval.Day, objAccure._Startdate.Date, Enddate.Date), 0)
    '                            '        Else
    '                            '            days = IIf(DateDiff(DateInterval.Day, mdtDbStartdate.Date, Enddate.Date) > 0, DateDiff(DateInterval.Day, mdtDbStartdate.Date, Enddate.Date), 0)
    '                            '        End If

    '                            '    End If

    '                            '    'END FOR CALCULATE DAYS


    '                            'End If

    '                            'Pinkal (06-Feb-2013) -- End

    '                            Remaining_bal = objAccure._AccrueAmount - (objAccure._IssueAmount + mintTotalIssue)

    '                            'Pinkal (09-Jul-2014) -- End


    '                        Else
    '                            Remaining_bal = objAccure._AccrueAmount - (objAccure._IssueAmount + mintTotalIssue)  ' FOR SHORT LEAVE IS TRUE 
    '                            iRemaining_Occurrence = objAccure._Remaining_Occurrence - 1
    '                        End If

    '                        'END FOR CALCULATE BALANCE

    '                        'Pinkal (09-Jul-2014) -- Start
    '                        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2

    '                        'strQ = " Update lvleavebalance_tran set " & _
    '                        '       "  issue_amount = issue_amount  + @issue_amount " & _
    '                        '       ", balance = @balance  " & _
    '                        '       ", uptolstyr_issueamt =  uptolstyr_issueamt + @issue_amount  " & _
    '                        '       ", remaining_bal = @remaining_bal " & _
    '                        '       ", days = @days " & _
    '                        '       ", remaining_occurrence = @remaining_occurrence " & _
    '                        '       " where leavebalanceunkid = @leavebalanceunkid  AND isvoid = 0"

    '                        strQ = " Update lvleavebalance_tran set " & _
    '                               "  issue_amount = issue_amount  + @issue_amount " & _
    '                               ", uptolstyr_issueamt =  uptolstyr_issueamt + @issue_amount  " & _
    '                               ", remaining_bal = @remaining_bal " & _
    '                               ", remaining_occurrence = @remaining_occurrence " & _
    '                               " where leavebalanceunkid = @leavebalanceunkid  AND isvoid = 0" '

    '                        'Pinkal (09-Jul-2014) -- End


    '                        If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                            strQ &= " AND isopenelc = 1 AND iselc = 1 "
    '                        End If

    '                        objDataOperation.ClearParameters()
    '                        objDataOperation.AddParameter("@issue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mintTotalIssue)

    '                        'Pinkal (09-Jul-2014) -- Start
    '                        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2
    '                        'objDataOperation.AddParameter("@balance", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, balance)
    '                        'objDataOperation.AddParameter("@days", SqlDbType.Int, eZeeDataType.INT_SIZE, days)
    '                        'Pinkal (09-Jul-2014) -- End

    '                        objDataOperation.AddParameter("@remaining_bal", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, Remaining_bal)
    '                        objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow(0)("leavebalanceunkid").ToString())
    '                        objDataOperation.AddParameter("@remaining_occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, iRemaining_Occurrence)
    '                        objDataOperation.ExecNonQuery(strQ)

    '                        If objDataOperation.ErrorMessage <> "" Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If

    '                    End If   '----------------------(4)


    '                    'END FOR UPDATE ISSUE AND BALANCE AMOUNT IN LEAVE ACCRUE TRAN TABLE


    '                    'INSERT FOR LEAVE BALANCE AUDIT TRAIL 

    '                    If isDelete = False Then

    '                        mintTotalIssue = mintTotalIssue + objAccure._IssueAmount
    '                        If InsertAudiTrailForLeaveBalance(objDataOperation, 2, CInt(drRow(0)("leavebalanceunkid").ToString()), intYearunkid, intEmployeeunkid, _
    '                                                       intLeaveTypeunkid, intBatchunkid, objAccure._Daily_Amount, objAccure._Startdate, objAccure._Enddate, _
    '                                                       balance, Remaining_bal, objAccure._UptoLstYr_AccrueAmout, objAccure._UptoLstYr_IssueAmout + (mintTotalIssue - objAccure._IssueAmount) _
    '                                                   , objAccure._AccrueSetting, days, objAccure._LeaveBF, objAccure._IsshortLeave, objAccure._ActualAmount, -1, objAccure._IsOpenELC, objAccure._IsELC, intUserId) = False Then

    '                            objDataOperation.ReleaseTransaction(False)
    '                            Return False
    '                        End If

    '                    End If

    '                    'FOR WHEN THERE ARE ROW(S) FOR UNPIAD LEAVE

    '                ElseIf isDelete = False Then
    '                    Dim objLeaveType As New clsleavetype_master
    '                    objLeaveType._Leavetypeunkid = intLeaveTypeunkid
    '                    If objLeaveType._IsPaid = False Then

    '                        'Pinkal (24-Jan-2014) -- Start
    '                        'Enhancement : Oman Changes

    '                        If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                            objAccure._IsOpenELC = True
    '                            objAccure._IsELC = True
    '                            objAccure._IsCloseFy = False
    '                        ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                            objAccure._IsOpenELC = False
    '                            objAccure._IsELC = False
    '                            objAccure._IsCloseFy = False
    '                        End If

    '                        objAccure._Startdate = FinancialYear._Object._Database_Start_Date.Date
    '                        objAccure._Enddate = FinancialYear._Object._Database_End_Date.Date

    '                        'Pinkal (24-Jan-2014) -- End


    '                        InsertIssueAccrueBalance(objDataOperation, intYearunkid, intEmployeeunkid, intLeaveTypeunkid, intBatchunkid, _
    '                                                 objAccure._Daily_Amount, objAccure._Startdate, objAccure._Enddate, balance, Remaining_bal, _
    '                                         objAccure._UptoLstYr_AccrueAmout, objAccure._UptoLstYr_IssueAmout, objAccure._AccrueSetting, days, _
    '                                         objAccure._LeaveBF, objAccure._IsshortLeave, objAccure._ActualAmount, -1, objAccure._IsOpenELC, objAccure._IsELC, intUserId)


    '                    End If

    '                End If  '----------------------(3)


    '            Else
    '                'FOR WHEN THERE IS NO ROW FOR UNPIAD LEAVE
    '                If isDelete = False Then
    '                    Dim objLeaveType As New clsleavetype_master
    '                    objLeaveType._Leavetypeunkid = intLeaveTypeunkid
    '                    If objLeaveType._IsPaid = False Then

    '                        'Pinkal (24-Jan-2014) -- Start
    '                        'Enhancement : Oman Changes

    '                        If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                            objAccure._IsOpenELC = True
    '                            objAccure._IsELC = True
    '                            objAccure._IsCloseFy = False
    '                        ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                            objAccure._IsOpenELC = False
    '                            objAccure._IsELC = False
    '                            objAccure._IsCloseFy = False
    '                        End If

    '                        objAccure._Startdate = FinancialYear._Object._Database_Start_Date.Date
    '                        objAccure._Enddate = FinancialYear._Object._Database_End_Date.Date

    '                        'Pinkal (24-Jan-2014) -- End

    '                        InsertIssueAccrueBalance(objDataOperation, intYearunkid, intEmployeeunkid, intLeaveTypeunkid, intBatchunkid, objAccure._Daily_Amount, _
    '                                                 Nothing, Nothing, mdblTotalAccrue - mintTotalIssue, Remaining_bal, objAccure._UptoLstYr_AccrueAmout, _
    '                                    objAccure._UptoLstYr_IssueAmout + mintTotalIssue, objAccure._AccrueSetting, days, _
    '                         objAccure._LeaveBF, objAccure._IsshortLeave, objAccure._ActualAmount, -1, objAccure._IsOpenELC, objAccure._IsELC, intUserId)

    '                    End If
    '                End If

    '            End If  '----------------------(2)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '        End If  '----------------------(1)

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: UpdateEmployeeAccrueBalance; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '    End Try
    '    Return False
    'End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function UpdateEmployeeAccrueBalance(ByVal xDatabaseName As String, ByVal xCompanyUnkid As Integer, ByVal objDataOperation As clsDataOperation, ByVal noofDays As Integer _
                                                                           , ByVal Startdate As DateTime, ByVal Enddate As DateTime, ByVal intYearunkid As Integer, ByVal intEmployeeunkid As Integer _
                                                                           , ByVal intLeaveTypeunkid As Integer, ByVal isDelete As Boolean _
                                                                           , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                                           , ByVal xIncludeIn_ActiveEmployee As Boolean, Optional ByVal blnOnlyActive As Boolean = True _
                                                                           , Optional ByVal intBatchunkid As Integer = 0, Optional ByVal intUserId As Integer = 0, Optional ByVal mdtDbStartdate As Date = Nothing _
                                                                           , Optional ByVal mdtDbEndDate As Date = Nothing, Optional ByVal blnLeaveApproverForLeaveType As String = "" _
                                                                           , Optional ByVal intLeaveBalanceSetting As Integer = -1, Optional ByVal intApproverID As Integer = -1 _
                                                                           , Optional ByVal blnApplyUserAccessFilter As Boolean = True, Optional ByVal mblnIsExternalApprover As Boolean = False) As Boolean


        'Pinkal (01-Mar-2016) --   'Enhancement - Implementing External Approver in Claim Request & Leave Module.[Optional ByVal mblnIsExternalApprover As Boolean = False]

        Dim dsList As DataSet = Nothing
        Dim days As Integer = 0
        Dim Remaining_bal As Double = 0
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intleaveaccrueunkid As Integer = 0
        Dim iRemaining_Occurrence As Double = 0
        Try

            If mintTotalIssue = 0 Then Return True

            Dim objAccure As New clsleavebalance_tran

            If intLeaveBalanceSetting <= 0 Then
                intLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            End If


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

            'If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
            '    dsList = objAccure.GetList("AccrueList", True, , intEmployeeunkid)
            'ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
            '    dsList = objAccure.GetList("AccrueList", True, False, intEmployeeunkid, True, True)
            'End If


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
            '    dsList = objAccure.GetList("AccrueList", xDatabaseName, intUserId, intYearunkid, xCompanyUnkid _
            '                                            , strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee _
            '                                            , blnOnlyActive, blnApplyUserAccessFilter, False, intEmployeeunkid, False, False, False, "", objDataOperation)
            'ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
            '    dsList = objAccure.GetList("AccrueList", xDatabaseName, intUserId, intYearunkid, xCompanyUnkid _
            '                                               , strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee _
            '                                               , blnOnlyActive, blnApplyUserAccessFilter, False, intEmployeeunkid, True, True, False, "", objDataOperation)
            'End If

            If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                dsList = objAccure.GetList("AccrueList", xDatabaseName, intUserId, intYearunkid, xCompanyUnkid _
                                                        , strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee _
                                                        , blnOnlyActive, blnApplyUserAccessFilter, False, intEmployeeunkid, False, False, False, "", objDataOperation, mblnIsExternalApprover)
            ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                dsList = objAccure.GetList("AccrueList", xDatabaseName, intUserId, intYearunkid, xCompanyUnkid _
                                                           , strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee _
                                                           , blnOnlyActive, blnApplyUserAccessFilter, False, intEmployeeunkid, True, True, False, "", objDataOperation, mblnIsExternalApprover)
            End If

            'Pinkal (01-Mar-2016) -- End

            'Pinkal (24-Aug-2015) -- End


            If dsList IsNot Nothing Then          '----------------------(1)

                If dsList.Tables(0).Rows.Count > 0 Then            '----------------------(2)

                    Dim drRow As DataRow() = dsList.Tables(0).Select("yearunkid =" & intYearunkid & " AND leavetypeunkid=" & intLeaveTypeunkid)

                    'START FOR UPDATE ISSUE AND BALANCE AMOUNT IN LEAVE ACCRUE TRAN TABLE
                    Dim GetIssueAmt As Integer = 0
                    Dim Accruebal As Double = 0
                    Dim balance As Double = 0
                    Dim isAdvance As Boolean = False
                    Dim isFirstDay As Boolean = True


                    'Pinkal (25-Mar-2019) -- Start
                    'Enhancement - Working on Leave Changes for NMB.
                    If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                        objAccure._LeaveBalanceunkid = CInt(drRow(0)("leavebalanceunkid"))
                    End If
                    'Pinkal (25-Mar-2019) -- End




                    If drRow.Length > 0 Then    '----------------------(3)

                        If isDelete Then       '----------------------(4)

                            'START FOR UPDATE LAST STATUS OF LEAVE ISSUE TRAN

                            Dim dsBalance As DataSet = objAccure.GetAuditDataforLeaveBalance(CInt(drRow(0)("leavebalanceunkid")), intYearunkid, intLeaveTypeunkid, intEmployeeunkid)

                            If dsBalance.Tables(0).Rows.Count > 0 Then
                                strQ = "delete from atlvleavebalance_tran where atunkid = @atunkid "
                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@atunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsBalance.Tables(0).Rows(0)("atunkid"))
                                objDataOperation.ExecNonQuery(strQ)


                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            End If

                            dsBalance = objAccure.GetAuditDataforLeaveBalance(CInt(drRow(0)("leavebalanceunkid")), intYearunkid, intLeaveTypeunkid, intEmployeeunkid)
                            If dsBalance.Tables(0).Rows.Count > 0 Then

                                strQ = " Update lvleavebalance_tran set " & _
                                  "  issue_amount =  @issue_amount " & _
                                  ", balance = @balance  " & _
                                  ", uptolstyr_issueamt =  @uptolstyr_issueamt " & _
                                  ", remaining_bal = @remaining_bal " & _
                                  ", days = @days " & _
                                  ", remaining_occurrence = @remaining_occurrence " & _
                                  " where leavebalanceunkid = @leavebalanceunkid  AND isvoid = 0 "


                                If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                                    strQ &= " AND isopenelc = 1 AND iselc = 1"
                                End If

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@issue_amount", SqlDbType.Int, eZeeDataType.INT_SIZE, dsBalance.Tables(0).Rows(0)("issue_amount"))
                                objDataOperation.AddParameter("@balance", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dsBalance.Tables(0).Rows(0)("balance"))
                                objDataOperation.AddParameter("@remaining_bal", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dsBalance.Tables(0).Rows(0)("remaining_bal"))
                                objDataOperation.AddParameter("@uptolstyr_issueamt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dsBalance.Tables(0).Rows(0)("uptolstyr_issueamt"))
                                objDataOperation.AddParameter("@days", SqlDbType.Int, eZeeDataType.INT_SIZE, dsBalance.Tables(0).Rows(0)("days"))
                                objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow(0)("leavebalanceunkid").ToString())
                                objDataOperation.AddParameter("@remaining_occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow(0)("remaining_occurrence").ToString())
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            End If

                            ' END FOR UPDATE LAST STATUS OF LEAVE ISSUE TRAN

                        Else

                            'START FOR CALCULATE BALANCE
                            objAccure._mObjDataoperation = objDataOperation
                            objAccure._LeaveBalanceunkid = CInt(drRow(0)("leavebalanceunkid").ToString())
                            intBatchunkid = objAccure._Batchunkid
                            mdblTotalAccrue = objAccure._AccrueAmount
                            If objAccure._IsshortLeave = False Then
                                Remaining_bal = objAccure._AccrueAmount - (objAccure._IssueAmount + mintTotalIssue)
                            Else
                                Remaining_bal = objAccure._AccrueAmount - (objAccure._IssueAmount + mintTotalIssue)  ' FOR SHORT LEAVE IS TRUE 
                                iRemaining_Occurrence = objAccure._Remaining_Occurrence - 1
                            End If

                            'END FOR CALCULATE BALANCE


                            strQ = " Update lvleavebalance_tran set " & _
                                   "  issue_amount = issue_amount  + @issue_amount " & _
                                   ", uptolstyr_issueamt =  uptolstyr_issueamt + @issue_amount  " & _
                                   ", remaining_bal = @remaining_bal " & _
                                   ", remaining_occurrence = @remaining_occurrence " & _
                                   " where leavebalanceunkid = @leavebalanceunkid  AND isvoid = 0" '

                            'Pinkal (09-Jul-2014) -- End


                            If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                                strQ &= " AND isopenelc = 1 AND iselc = 1 "
                            End If

                            objDataOperation.ClearParameters()
                            objDataOperation.AddParameter("@issue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mintTotalIssue)

                            objDataOperation.AddParameter("@remaining_bal", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, Remaining_bal)
                            objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow(0)("leavebalanceunkid").ToString())
                            objDataOperation.AddParameter("@remaining_occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, iRemaining_Occurrence)
                            objDataOperation.ExecNonQuery(strQ)

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        End If   '----------------------(4)


                        'END FOR UPDATE ISSUE AND BALANCE AMOUNT IN LEAVE ACCRUE TRAN TABLE


                        'INSERT FOR LEAVE BALANCE AUDIT TRAIL 

                        If isDelete = False Then

                            mintTotalIssue = mintTotalIssue + objAccure._IssueAmount

                            'Pinkal (25-Mar-2019) -- Start
                            'Enhancement - Working on Leave Changes for NMB.

                            'If InsertAudiTrailForLeaveBalance(objDataOperation, 2, CInt(drRow(0)("leavebalanceunkid").ToString()), intYearunkid, intEmployeeunkid, _
                            '                               intLeaveTypeunkid, intBatchunkid, objAccure._Daily_Amount, objAccure._Startdate, objAccure._Enddate, _
                            '                               balance, Remaining_bal, objAccure._UptoLstYr_AccrueAmout, objAccure._UptoLstYr_IssueAmout + (mintTotalIssue - objAccure._IssueAmount) _
                            '                           , objAccure._AccrueSetting, days, objAccure._LeaveBF, objAccure._IsshortLeave, objAccure._ActualAmount, -1, objAccure._IsOpenELC, objAccure._IsELC, intUserId) = False Then

                            If InsertAudiTrailForLeaveBalance(objDataOperation, 2, CInt(drRow(0)("leavebalanceunkid").ToString()), intYearunkid, intEmployeeunkid, _
                                                           intLeaveTypeunkid, intBatchunkid, objAccure._Daily_Amount, objAccure._Startdate, objAccure._Enddate, _
                                                           balance, Remaining_bal, objAccure._UptoLstYr_AccrueAmout, objAccure._UptoLstYr_IssueAmout + (mintTotalIssue - objAccure._IssueAmount) _
                                                                          , objAccure._AccrueSetting, days, objAccure._LeaveBF, objAccure._IsshortLeave, objAccure._ActualAmount, -1, objAccure._IsOpenELC, objAccure._IsELC _
                                                                          , objAccure._IsCloseFy, objAccure._CFlvAmount, objAccure._EligibilityAfter, objAccure._IsNoAction, objAccure._Consicutivedays, objAccure._Occurance _
                                                                          , objAccure._Optiondid, iRemaining_Occurrence, objAccure._AdjustmentAmt, objAccure._Monthly_Accrue, objAccure._MaxNegativeDaysLimit _
                                                                          , objAccure._OccurrenceTenure, intUserId) = False Then

                                'Pinkal (25-Mar-2019) -- End

                                objDataOperation.ReleaseTransaction(False)
                                Return False
                            End If

                        End If

                        'FOR WHEN THERE ARE ROW(S) FOR UNPIAD LEAVE

                    ElseIf isDelete = False Then
                        Dim objLeaveType As New clsleavetype_master
                        objLeaveType._DoOperation = objDataOperation
                        objLeaveType._Leavetypeunkid = intLeaveTypeunkid
                        If objLeaveType._IsPaid = False Then

                            'Pinkal (24-Jan-2014) -- Start
                            'Enhancement : Oman Changes

                            If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                                objAccure._IsOpenELC = True
                                objAccure._IsELC = True
                                objAccure._IsCloseFy = False
                            ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                                objAccure._IsOpenELC = False
                                objAccure._IsELC = False
                                objAccure._IsCloseFy = False
                            End If

                            objAccure._Startdate = FinancialYear._Object._Database_Start_Date.Date
                            objAccure._Enddate = FinancialYear._Object._Database_End_Date.Date

                            'Pinkal (24-Jan-2014) -- End


                            'Pinkal (25-Mar-2019) -- Start
                            'Enhancement - Working on Leave Changes for NMB.

                            'InsertIssueAccrueBalance(objDataOperation, intYearunkid, intEmployeeunkid, intLeaveTypeunkid, intBatchunkid, _
                            '                         objAccure._Daily_Amount, objAccure._Startdate, objAccure._Enddate, balance, Remaining_bal, _
                            '                 objAccure._UptoLstYr_AccrueAmout, objAccure._UptoLstYr_IssueAmout, objAccure._AccrueSetting, days, _
                            '                 objAccure._LeaveBF, objAccure._IsshortLeave, objAccure._ActualAmount, -1, objAccure._IsOpenELC, objAccure._IsELC, intUserId)

                            InsertIssueAccrueBalance(objDataOperation, intYearunkid, intEmployeeunkid, intLeaveTypeunkid, intBatchunkid _
                                                                , objAccure._Daily_Amount, objAccure._Startdate, objAccure._Enddate, balance, Remaining_bal _
                                                                , objAccure._UptoLstYr_AccrueAmout, objAccure._UptoLstYr_IssueAmout, objAccure._AccrueSetting, days _
                                                                , objAccure._LeaveBF, objAccure._IsshortLeave, objAccure._ActualAmount, -1, objAccure._IsOpenELC, objAccure._IsELC _
                                                                , objAccure._IsCloseFy, objAccure._CFlvAmount, objAccure._EligibilityAfter, objAccure._IsNoAction, objAccure._Consicutivedays, objAccure._Occurance _
                                                                , objAccure._Optiondid, iRemaining_Occurrence, objAccure._AdjustmentAmt, objAccure._Monthly_Accrue, objAccure._MaxNegativeDaysLimit _
                                                                , objAccure._OccurrenceTenure, intUserId)

                            'Pinkal (25-Mar-2019) -- End


                        End If

                    End If  '----------------------(3)


                Else
                    'FOR WHEN THERE IS NO ROW FOR UNPIAD LEAVE
                    If isDelete = False Then
                        Dim objLeaveType As New clsleavetype_master
                        'Pinkal (02-Dec-2015) -- Start
                        'Enhancement - Solving Leave bug in Self Service.
                        objLeaveType._DoOperation = objDataOperation
                        'Pinkal (02-Dec-2015) -- End
                        objLeaveType._Leavetypeunkid = intLeaveTypeunkid
                        If objLeaveType._IsPaid = False Then

                            'Pinkal (24-Jan-2014) -- Start
                            'Enhancement : Oman Changes

                            If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                                objAccure._IsOpenELC = True
                                objAccure._IsELC = True
                                objAccure._IsCloseFy = False
                            ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                                objAccure._IsOpenELC = False
                                objAccure._IsELC = False
                                objAccure._IsCloseFy = False
                            End If

                            objAccure._Startdate = FinancialYear._Object._Database_Start_Date.Date
                            objAccure._Enddate = FinancialYear._Object._Database_End_Date.Date

                            'Pinkal (24-Jan-2014) -- End


                            'Pinkal (25-Mar-2019) -- Start
                            'Enhancement - Working on Leave Changes for NMB.

                            'InsertIssueAccrueBalance(objDataOperation, intYearunkid, intEmployeeunkid, intLeaveTypeunkid, intBatchunkid, objAccure._Daily_Amount, _
                            '                         Nothing, Nothing, mdblTotalAccrue - mintTotalIssue, Remaining_bal, objAccure._UptoLstYr_AccrueAmout, _
                            '            objAccure._UptoLstYr_IssueAmout + mintTotalIssue, objAccure._AccrueSetting, days, _
                            ' objAccure._LeaveBF, objAccure._IsshortLeave, objAccure._ActualAmount, -1, objAccure._IsOpenELC, objAccure._IsELC, intUserId)

                            InsertIssueAccrueBalance(objDataOperation, intYearunkid, intEmployeeunkid, intLeaveTypeunkid, intBatchunkid _
                                                              , objAccure._Daily_Amount, Nothing, Nothing, mdblTotalAccrue - mintTotalIssue, Remaining_bal _
                                                              , objAccure._UptoLstYr_AccrueAmout, objAccure._UptoLstYr_IssueAmout + mintTotalIssue, objAccure._AccrueSetting, days _
                                                              , objAccure._LeaveBF, objAccure._IsshortLeave, objAccure._ActualAmount, -1, objAccure._IsOpenELC, objAccure._IsELC _
                                                              , objAccure._IsCloseFy, objAccure._CFlvAmount, objAccure._EligibilityAfter, objAccure._IsNoAction, objAccure._Consicutivedays, objAccure._Occurance _
                                                              , objAccure._Optiondid, objAccure._Remaining_Occurrence, objAccure._AdjustmentAmt, objAccure._Monthly_Accrue, objAccure._MaxNegativeDaysLimit _
                                                              , objAccure._OccurrenceTenure, intUserId)

                            'Pinkal (25-Mar-2019) -- End


                        End If
                    End If

                End If  '----------------------(2)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If  '----------------------(1)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateEmployeeAccrueBalance; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return False
    End Function



    'Pinkal (25-Mar-2019) -- Start
    'Enhancement - Working on Leave Changes for NMB.

    Private Sub InsertIssueAccrueBalance(ByVal objDataOperation As clsDataOperation, ByVal intYearunkid As Integer, ByVal intEmployeeunkid As Integer, _
                                         ByVal intLeaveTypeunkid As Integer, ByVal intBatchunkid As Integer, ByVal daily_amount As Double, ByVal mdtStartdate As DateTime, _
                                         ByVal mdtenddate As DateTime, ByVal mdblbalance As Double, ByVal mdblRemaining_bal As Double, ByVal mdblUptoLstyrAccrue_Amount As Double, _
                                         ByVal mdblUptoLstyrIssue_Amount As Double, ByVal mintAccrueSetting As Integer, ByVal intDays As Integer, ByVal mdclLeaveBF As Decimal, ByVal mblnShortLeave As Boolean, _
                                          ByVal mdclactualamt As Decimal, ByVal mintLoginempId As Integer, ByVal mblnIsOpenELC As Boolean, ByVal mblnIsELC As Boolean, _
                                          ByVal mblnIsCloseFy As Boolean, ByVal mdecCFlvAmount As Decimal, ByVal mintEligibilityAfter As Integer, ByVal mblnIsNoAction As Boolean, ByVal mintConsicutivedays As Integer, _
                                          ByVal mintOccurance As Integer, ByVal mintOptiondid As Integer, ByVal mintRemaining_Occurrence As Integer, ByVal mdecAdjustmentAmt As Decimal, _
                                          ByVal mdecMonthly_AccrueAmt As Decimal, ByVal mintMaxNegativeDaysLimit As Integer, ByVal mintOccurrenceTenure As Integer, Optional ByVal intUserId As Integer = 0)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mintBalanceunkid As Integer = -1
        Try
            objDataOperation.ClearParameters()

        

            strQ = "INSERT INTO lvleavebalance_tran ( " & _
                   "  yearunkid " & _
                   ", employeeunkid " & _
                   ", leavetypeunkid " & _
                   ", batchunkid " & _
                   ", startdate " & _
                   ", enddate " & _
                   ", accrue_amount " & _
                   ", issue_amount " & _
                   ", balance " & _
                   ", remaining_bal " & _
                   ", uptolstyr_accrueamt " & _
                   ", uptolstyr_issueamt " & _
                   ", daily_amount " & _
                   ", ispaid " & _
                   ", accruesetting " & _
                   ", days " & _
                    ",leavebf " & _
                    ",isshortleave " & _
                    ",actualamount " & _
                    ",isopenelc " & _
                    ",iselc " & _
                   ", userunkid " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", voidreason " & _
                ") VALUES (" & _
                   "  @yearunkid " & _
                   ", @employeeunkid " & _
                   ", @leavetypeunkid " & _
                   ", @batchunkid " & _
                   ", @startdate " & _
                   ", @enddate " & _
                   ", @accrue_amount " & _
                   ", @issue_amount " & _
                   ", @balance " & _
                   ", @remaining_bal " & _
                   ", @uptolstyr_accrueamt " & _
                   ", @uptolstyr_issueamt " & _
                   ", @daily_amount " & _
                   ", @ispaid " & _
                   ", @accruesetting " & _
                   ", @days " & _
                   ", @leavebf " & _
                   ", @isshortleave " & _
                   ", @actualamount " & _
                   ", @isopenelc " & _
                   ", @iselc " & _
                   ", @userunkid " & _
                   ", @isvoid " & _
                   ", @voiduserunkid " & _
                   ", @voiddatetime " & _
                   ", @voidreason " & _
                "); SELECT @@identity"

            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid.ToString)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeunkid.ToString)
            objDataOperation.AddParameter("@batchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBatchunkid.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtStartdate <> Nothing, mdtStartdate, DBNull.Value))
            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtenddate <> Nothing, mdtenddate, DBNull.Value))
            objDataOperation.AddParameter("@accrue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblTotalAccrue)
            objDataOperation.AddParameter("@issue_amount", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotalIssue)
            objDataOperation.AddParameter("@balance", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblbalance)
            objDataOperation.AddParameter("@remaining_bal", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblTotalAccrue - mintTotalIssue)
            objDataOperation.AddParameter("@uptolstyr_accrueamt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblUptoLstyrAccrue_Amount)
            objDataOperation.AddParameter("@uptolstyr_issueamt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblUptoLstyrIssue_Amount)
            objDataOperation.AddParameter("@daily_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, daily_amount)
            objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPaid.ToString)
            objDataOperation.AddParameter("@accruesetting", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccrueSetting.ToString)
            objDataOperation.AddParameter("@days", SqlDbType.Int, eZeeDataType.INT_SIZE, intDays.ToString)

            If intUserId <= 0 Then
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            Else
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId.ToString)
            End If

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@leavebf", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdclLeaveBF)
            objDataOperation.AddParameter("@isshortleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnShortLeave)
            objDataOperation.AddParameter("@actualamount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdclactualamt)
            objDataOperation.AddParameter("@isopenelc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsOpenELC)
            objDataOperation.AddParameter("@iselc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsELC)

            'Pinkal (06-Feb-2013) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                mintBalanceunkid = CInt(dsList.Tables("List").Rows(0).Item(0))
            End If



            'Pinkal (25-Mar-2019) -- Start
            'Enhancement - Working on Leave Changes for NMB.

            'If InsertAudiTrailForLeaveBalance(objDataOperation, 1, mintBalanceunkid, intYearunkid, intEmployeeunkid, intLeaveTypeunkid, intBatchunkid, _
            '                              daily_amount, mdtStartdate, mdtenddate, mdblbalance, mdblRemaining_bal, mdblUptoLstyrAccrue_Amount, mdblUptoLstyrIssue_Amount, mintAccrueSetting, intDays _
            '                              , mdclLeaveBF, mblnShortLeave, mdclactualamt, -1, mblnIsOpenELC, mblnIsELC, IIf(intUserId <= 0, mintUserunkid, intUserId)) = False Then


            If InsertAudiTrailForLeaveBalance(objDataOperation, 1, mintBalanceunkid, intYearunkid, intEmployeeunkid, intLeaveTypeunkid, intBatchunkid, _
                                          daily_amount, mdtStartdate, mdtenddate, mdblbalance, mdblRemaining_bal, mdblUptoLstyrAccrue_Amount, mdblUptoLstyrIssue_Amount, mintAccrueSetting, intDays _
                                       , mdclLeaveBF, mblnShortLeave, mdclactualamt, -1, mblnIsOpenELC, mblnIsELC, mblnIsCloseFy, mdecCFlvAmount, mintEligibilityAfter, mblnIsNoAction, mintConsicutivedays _
                                       , mintOccurance, mintOptiondid, mintRemaining_Occurrence, mdecAdjustmentAmt, mdecMonthly_AccrueAmt, mintMaxNegativeDaysLimit, mintOccurrenceTenure _
                                       , IIf(intUserId <= 0, mintUserunkid, intUserId)) = False Then

                'Pinkal (25-Mar-2019) -- End


                objDataOperation.ReleaseTransaction(False)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertIssueAccrueLeave", mstrModuleName)
        End Try
    End Sub

    'Pinkal (25-Mar-2019) -- End

    Public Function isIssueLeaveOnHoliday(ByVal intYearunkid As Integer, ByVal intFormunkid As Integer, ByVal intLeaveTypeunkid As Integer, ByVal intEmployeeunkid As Integer, ByVal dtIssuedate As DateTime) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = " Select isnull(leaveissuetranunkid,0) as leaveissuetranunkid " & _
                   " from lvleaveissue_tran " & _
                   " WHERE leaveissueunkid in " & _
                   " (Select isnull(leaveissueunkid,0) from lvleaveissue_master  " & _
                   "  WHERE leaveyearunkid = @leaveyearunkid and formunkid = @formunkid  " & _
                   "  and leavetypeunkid = @leavetypeunkid and employeeunkid = @employeeunkid) " & _
                   " AND convert(char(8),leavedate,112) = @leaveDate"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leaveyearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearunkid)
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@leavedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtIssuedate))
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList.Tables("List").Rows.Count > 0 Then
                If CInt(dsList.Tables("List").Rows(0)("leaveissuetranunkid")) > 0 Then
                    Return True
                Else
                    Return False
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isIssueLeaveOnHoliday; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrailForLeaveIssue(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal LeaveDate As String, Optional ByVal intUserID As Integer = 0) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO atlvleaveIssue_tran ( " & _
            "  leaveissuetranunkid " & _
            ", leaveissueunkid " & _
            ", leavedate " & _
            ", ispaid " & _
            ", leaveadjustmentunkid " & _
            ", audittype " & _
            ", audituserunkid " & _
            ", auditdatetime " & _
            ", ip" & _
            ", machine_name" & _
                     ", form_name " & _
                     ", module_name1 " & _
                     ", module_name2 " & _
                     ", module_name3 " & _
                     ", module_name4 " & _
                     ", module_name5 " & _
                     ", isweb " & _
                        ", dayfraction " & _
                    ", isprocess " & _
                        ", istnaperiodlinked " & _
          ") VALUES (" & _
            "  @leaveissuetranunkid " & _
            ", @leaveissueunkid " & _
            ", @leavedate " & _
            ", @ispaid " & _
            ", @leaveadjustmentunkid " & _
            ", @audittype " & _
            ", @audituserunkid " & _
            ", @auditdatetime " & _
            ", @ip" & _
            ", @machine_name " & _
                     ", @form_name " & _
                     ", @module_name1 " & _
                     ", @module_name2 " & _
                     ", @module_name3 " & _
                     ", @module_name4 " & _
                     ", @module_name5 " & _
                     ", @isweb " & _
                        ", @dayfraction " & _
                    ", @isprocess " & _
                        ", @istnaperiodlinked" & _
                        " ); "

            'Pinkal (01-Jan-2019) --  'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[", @istnaperiodlinked" & _]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leaveissuetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissuetranunkid.ToString)
            objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
            objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, LeaveDate.ToString())
            objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPaid.ToString)
            objDataOperation.AddParameter("@leaveadjustmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveAdjustmentunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            If intUserID <= 0 Then
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            Else
                objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserID)
            End If

            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            If mstrWebClientIP.ToString().Length <= 0 Then
                mstrWebClientIP = getIP()
            End If

            If mstrWebHostName.ToString().Length <= 0 Then
                mstrWebHostName = getHostName()
            End If
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdclDayfraction.ToString)
            objDataOperation.AddParameter("@isprocess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsProcess)

            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            objDataOperation.AddParameter("@istnaperiodlinked", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnConsiderLeaveOnTnAPeriod)
            'Pinkal (01-Jan-2019) -- End


            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForLeaveIssue", mstrMessage)
            Return False
        End Try
    End Function


    'Pinkal (25-Mar-2019) -- Start
    'Enhancement - Working on Leave Changes for NMB.

    Public Function InsertAudiTrailForLeaveBalance(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal mintBalanceunkid As Integer, _
                                          ByVal intYearunkid As Integer, ByVal intEmployeeunkid As Integer, ByVal intLeaveTypeunkid As Integer, ByVal intBatchunkid As Integer, _
                                          ByVal daily_amount As Double, ByVal mdtStartdate As DateTime, ByVal mdtenddate As DateTime, ByVal mdblbalance As Double, _
                                          ByVal mdblRemaining_bal As Double, ByVal mdblUptoLstyrAccrue_Amount As Double, ByVal mdblUptoLstyrIssue_Amount As Double, _
                                          ByVal mintAccrueSetting As Integer, ByVal intdays As Integer, ByVal mdclLeaveBF As Decimal, ByVal mblnShortLeave As Boolean, _
                                          ByVal mdclactualamt As Decimal, ByVal mintLoginempId As Integer, ByVal mblnIsOpenELC As Boolean, ByVal mblnIsELC As Boolean, _
                                          ByVal mblnIsCloseFy As Boolean, ByVal mdecCFlvAmount As Decimal, ByVal mintEligibilityAfter As Integer, ByVal mblnIsNoAction As Boolean, ByVal mintConsicutivedays As Integer, _
                                          ByVal mintOccurance As Integer, ByVal mintOptiondid As Integer, ByVal mintRemaining_Occurrence As Integer, ByVal mdecAdjustmentAmt As Decimal, _
                                          ByVal mdecMonthly_AccrueAmt As Decimal, ByVal mintMaxNegativeDaysLimit As Integer, ByVal mintOccurrenceTenure As Integer, Optional ByVal intUserID As Integer = 0) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation.ClearParameters()

            strQ = "INSERT INTO atlvleavebalance_tran ( " & _
                    "  leavebalanceunkid " & _
                    ", yearunkid " & _
                    ", employeeunkid " & _
                    ", leavetypeunkid " & _
                    ", batchunkid " & _
                    ", startdate " & _
                    ", enddate " & _
                    ", accrue_amount " & _
                    ", issue_amount " & _
                    ", balance " & _
                    ", remaining_bal " & _
                    ", uptolstyr_accrueamt " & _
                    ", uptolstyr_issueamt " & _
                    ", daily_amount " & _
                    ", ispaid " & _
                    ", accruesetting " & _
                    ", days " & _
                    ",leavebf " & _
                    ",isshortleave " & _
                    ",actualamount " & _
                    ",isopenelc " & _
                    ",iselc " & _
                    ",isclose_fy " & _
                    ",cfamount" & _
                    ",eligibilityafter" & _
                    ",isnoaction" & _
                    ",consecutivedays" & _
                    ",occurrence" & _
                    ",optiondid" & _
                    ",remaining_occurrence" & _
                    ",adj_remaining_bal" & _
                    ",monthly_accrue" & _
                    ",maxnegative_limit" & _
                    ",occ_tenure" & _
                    ",loginemployeeunkid " & _
                    ", audittype " & _
                    ", audituserunkid " & _
                    ", auditdatetime " & _
                    ", ip" & _
                    ", machine_name " & _
                  ", form_name " & _
                  ", module_name1 " & _
                  ", module_name2 " & _
                  ", module_name3 " & _
                  ", module_name4 " & _
                  ", module_name5 " & _
                  ", isweb " & _
                    ") VALUES (" & _
                    "  @leavebalanceunkid " & _
                    ", @yearunkid " & _
                    ", @employeeunkid " & _
                    ", @leavetypeunkid " & _
                    ", @batchunkid " & _
                    ", @startdate " & _
                    ", @enddate " & _
                    ", @accrue_amount " & _
                    ", @issue_amount " & _
                    ", @balance " & _
                    ", @remaining_bal " & _
                    ", @uptolstyr_accrueamt " & _
                    ", @uptolstyr_issueamt " & _
                    ", @daily_amount " & _
                    ", @ispaid " & _
                    ", @accruesetting " & _
                    ", @days " & _
                    ", @leavebf " & _
                    ", @isshortleave " & _
                    ", @actualamount " & _
                    ", @isopenelc " & _
                    ", @iselc " & _
                    ", @isclose_fy " & _
                    ", @cfamount" & _
                    ", @eligibilityafter" & _
                    ", @isnoaction" & _
                    ", @consecutivedays" & _
                    ", @occurrence" & _
                    ", @optiondid" & _
                    ", @remaining_occurrence" & _
                    ", @adj_remaining_bal" & _
                    ", @monthly_accrue" & _
                    ", @maxnegative_limit" & _
                    ", @occ_tenure" & _
                    ", @loginemployeeunkid " & _
                    ", @audittype " & _
                    ", @audituserunkid " & _
                    ", @auditdatetime " & _
                    ", @ip" & _
                  ", @machine_name" & _
                  ", @form_name " & _
                  ", @module_name1 " & _
                  ", @module_name2 " & _
                  ", @module_name3 " & _
                  ", @module_name4 " & _
                  ", @module_name5 " & _
                  ", @isweb " & _
                  " )"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavebalanceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBalanceunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid.ToString)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeunkid.ToString)
            objDataOperation.AddParameter("@batchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBatchunkid.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtStartdate <> Nothing, mdtStartdate, DBNull.Value))
            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtenddate <> Nothing, mdtenddate, DBNull.Value))
            objDataOperation.AddParameter("@accrue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblTotalAccrue)
            objDataOperation.AddParameter("@issue_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mintTotalIssue)
            objDataOperation.AddParameter("@balance", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblbalance)
            objDataOperation.AddParameter("@remaining_bal", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblRemaining_bal)
            objDataOperation.AddParameter("@uptolstyr_accrueamt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblUptoLstyrAccrue_Amount)
            objDataOperation.AddParameter("@uptolstyr_issueamt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblUptoLstyrIssue_Amount)
            objDataOperation.AddParameter("@daily_amount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, daily_amount)
            objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPaid.ToString)
            objDataOperation.AddParameter("@accruesetting", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccrueSetting.ToString)
            objDataOperation.AddParameter("@days", SqlDbType.Int, eZeeDataType.INT_SIZE, intdays.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            If intUserID <= 0 Then
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            Else
                objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserID)
            End If

            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            If mstrWebClientIP.ToString().Length <= 0 Then
                mstrWebClientIP = getIP()
            End If

            If mstrWebHostName.ToString().Length <= 0 Then
                mstrWebHostName = getHostName()
            End If

            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            objDataOperation.AddParameter("@leavebf", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdclLeaveBF)
            objDataOperation.AddParameter("@isshortleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnShortLeave)
            objDataOperation.AddParameter("@actualamount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdclactualamt)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginempId)
            objDataOperation.AddParameter("@isopenelc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsOpenELC)
            objDataOperation.AddParameter("@iselc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsELC)



            objDataOperation.AddParameter("@isclose_fy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCloseFy.ToString)
            objDataOperation.AddParameter("@cfamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCFlvAmount.ToString)
            objDataOperation.AddParameter("@eligibilityafter", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEligibilityAfter.ToString)
            objDataOperation.AddParameter("@isnoaction", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsNoAction.ToString)
            objDataOperation.AddParameter("@consecutivedays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintConsicutivedays.ToString)
            objDataOperation.AddParameter("@occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOccurance.ToString)
            objDataOperation.AddParameter("@optiondid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOptiondid.ToString)
            objDataOperation.AddParameter("@remaining_occurrence", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRemaining_Occurrence.ToString)
            objDataOperation.AddParameter("@adj_remaining_bal", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdecAdjustmentAmt)
            objDataOperation.AddParameter("@monthly_accrue", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdecMonthly_AccrueAmt)
            objDataOperation.AddParameter("@maxnegative_limit", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxNegativeDaysLimit.ToString)
            objDataOperation.AddParameter("@occ_tenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOccurrenceTenure.ToString)



            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAudiTrailForLeaveBalance", mstrModuleName)
        End Try
        Return True
    End Function

    'Pinkal (25-Mar-2019) -- End

    Public Function GetEmployeeIssueDaysCount(ByVal Employeeunkid As Integer, ByVal LeaveTypeunkid As Integer, _
                                              ByVal intYearId As Integer, Optional ByVal mdtdate As DateTime = Nothing, _
                                              Optional ByVal mdtStartDate As Date = Nothing, Optional ByVal mdtEndDate As Date = Nothing, _
                                              Optional ByVal mdtdbStartDate As DateTime = Nothing, Optional ByVal xDataOp As clsDataOperation = Nothing _
                                              , Optional ByVal strDatabaseName As String = "") As Decimal
        'Sohail (15 Jan 2019) - [strDatabaseName]
        'Sohail (03 May 2018) - [xDataOp]

        'Pinkal (25-Jul-2015) -- Getting Problem in showing Leave Balance in Payslip in ELC Setting For (CIT) [ Optional ByVal mdtdbStartDate As DateTime = Nothing]

        'Pinkal (24-May-2014) -- Start
        'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT] [ ByVal intYearId As Integer]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim IssuedaysCount As Decimal = 0
        Dim mdecPreIssuedaysCount As Decimal = 0
        Dim mstrFormIDs As String = ""
        'Sohail (15 Jan 2019) -- Start
        'PACRA Issue - 76.1 - Leave details were not coming on Payslip report for Previous years in Aruti Self Service.
        Dim strDB As String = ""
        If strDatabaseName.Trim <> "" Then
            strDB = strDatabaseName & ".."
        End If
        'Sohail (15 Jan 2019) -- End
        Try
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = New clsDataOperation
            Dim objDataOperation As clsDataOperation
            If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOp
            End If
            objDataOperation.ClearParameters()
            'Sohail (03 May 2018) -- End

            'Pinkal (21-Oct-2014) -- Start
            'Enhancement -  CHANGES GETTING PREVIOUS YEAR ISSUE DAYS COUNT FOR ALL COMPANY WHICH USES ELC SETTINGS.

            'Pinkal (25-Jul-2015) -- Start
            'Getting Problem in showing Leave Balance in Payslip in ELC Setting For (CIT) [ Optional ByVal mdtdbStartDate As DateTime = Nothing]
            ' If mdtStartDate <> Nothing AndAlso mdtStartDate < FinancialYear._Object._Database_Start_Date AndAlso mdtdate = Nothing Then
            If mdtStartDate <> Nothing AndAlso mdtStartDate < mdtdbStartDate.Date AndAlso mdtdate = Nothing Then
                'Pinkal (25-Jul-2015) -- End
                Dim mintCompany As Integer = 0
                Dim mstrPreviousDBName As String = ""
                Dim mdtFYStartDate As Date = Nothing
                Dim mdtFYEndDate As Date = Nothing
                Dim mintLeaveBalanceSetting As Integer = enLeaveBalanceSetting.Financial_Year

                strQ = "SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE yearunkid = " & intYearId & "  "
                Dim dsCompany As DataSet = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsCompany IsNot Nothing AndAlso dsCompany.Tables(0).Rows.Count > 0 Then
                    mintCompany = CInt(dsCompany.Tables(0).Rows(0)("companyunkid"))
                End If

                If mintCompany > 0 Then

                    strQ = "SELECT ISNULL(key_value,1) AS key_value  from hrmsConfiguration..cfconfiguration WHERE key_name = 'LeaveBalanceSetting' and companyunkid = " & mintCompany
                    Dim dsLeaveBalanceSetting As DataSet = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsLeaveBalanceSetting IsNot Nothing AndAlso dsLeaveBalanceSetting.Tables(0).Rows.Count > 0 Then
                        mintLeaveBalanceSetting = CInt(dsLeaveBalanceSetting.Tables(0).Rows(0)("key_value"))
                    End If

                    If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                        strQ = "SELECT yearunkid,database_name,start_date,end_date FROM hrmsConfiguration..cffinancial_year_tran WHERE companyunkid = " & mintCompany & " AND isclosed = 1 AND @stdate BETWEEN start_date AND end_date "
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@stdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
                        Dim dsDBName As DataSet = objDataOperation.ExecQuery(strQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If dsDBName IsNot Nothing AndAlso dsDBName.Tables(0).Rows.Count > 0 Then
                            mstrPreviousDBName = dsDBName.Tables(0).Rows(0)("database_name").ToString()
                            mdtFYStartDate = CDate(dsDBName.Tables(0).Rows(0)("start_date"))
                            mdtFYEndDate = CDate(dsDBName.Tables(0).Rows(0)("end_date"))
                        End If

                        objDataOperation.ClearParameters()
                        strQ = "SELECT ISNULL(SUM(lvleaveIssue_tran.dayfraction),0) as Issuedays FROM " & mstrPreviousDBName & "..lvleaveIssue_master " & _
                                  " JOIN " & mstrPreviousDBName & "..lvleaveIssue_tran ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvleaveIssue_tran.isvoid = 0 "

                        'Pinkal (01-Dec-2014) -- Start
                        'Enhancement - ENHANCEMENT GIVEN BY TRA - EMPLOYEE CAN APPLY LEAVE BEYOND ELC END DATE, IF EMPLOYEE HAVE LEAVE BALANCE .

                        If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                            strQ &= " JOIN  " & mstrPreviousDBName & "..lvleaveform ON lvleaveform.formunkid = lvleaveIssue_master.formunkid AND lvleaveform.isvoid = 0 " & _
                                        " AND  CONVERT(char(8),lvleaveform.approve_stdate,112) BETWEEN @startdate AND @enddate "
                        End If

                        strQ &= " WHERE lvleaveIssue_master.Employeeunkid = @Employeeunkid And lvleaveIssue_master.LeaveTypeunkid = @LeaveTypeunkid and lvleaveIssue_master.isvoid = 0 "

                        If mdtStartDate <> Nothing Then
                            '    strQ &= " AND CONVERT(CHAR(8),lvleaveIssue_tran.leavedate,112) >= @startdate"
                            objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
                        End If

                        If mdtEndDate <> Nothing Then
                            '    strQ &= " AND CONVERT(CHAR(8),lvleaveIssue_tran.leavedate,112) <= @enddate"
                            objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFYEndDate))
                        End If

                        'Pinkal (01-Dec-2014) -- End

                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Employeeunkid)
                        objDataOperation.AddParameter("@LeaveTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, LeaveTypeunkid)
                        dsList = objDataOperation.ExecQuery(strQ, "List")


                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                            mdecPreIssuedaysCount = CDec(dsList.Tables("List").Rows(0)("Issuedays"))
                        End If


                        'Pinkal (01-Dec-2014) -- Start
                        'Enhancement - ENHANCEMENT GIVEN BY TRA - EMPLOYEE CAN APPLY LEAVE BEYOND ELC END DATE, IF EMPLOYEE HAVE LEAVE BALANCE .
                        objDataOperation.ClearParameters()

                        strQ = " SELECT ISNULL(STUFF((SELECT DISTINCT  ',' + CONVERT(NVARCHAR(max), lvleaveform.formunkid) FROM " & mstrPreviousDBName & "..lvleaveIssue_master " & _
                                 " JOIN " & mstrPreviousDBName & "..lvleaveIssue_tran ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvleaveIssue_tran.isvoid = 0 "

                        If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                            strQ &= " JOIN  " & mstrPreviousDBName & "..lvleaveform ON lvleaveform.formunkid = lvleaveIssue_master.formunkid AND lvleaveform.isvoid = 0 " & _
                                        " AND  CONVERT(char(8),lvleaveform.approve_stdate,112) BETWEEN @startdate AND @enddate "
                        End If

                        strQ &= " WHERE lvleaveIssue_master.Employeeunkid = @Employeeunkid And lvleaveIssue_master.LeaveTypeunkid = @LeaveTypeunkid and lvleaveIssue_master.isvoid = 0  FOR XML PATH('')),1,1,''),'') AS formunkid"

                        If mdtStartDate <> Nothing Then
                            objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
                        End If

                        If mdtEndDate <> Nothing Then
                            objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFYEndDate))
                        End If

                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Employeeunkid)
                        objDataOperation.AddParameter("@LeaveTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, LeaveTypeunkid)
                        dsList = objDataOperation.ExecQuery(strQ, "List")


                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                    End If

                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                            mstrFormIDs = dsList.Tables("List").Rows(0)("formunkid").ToString()
                End If

                    End If

                End If

            End If

            'Pinkal (21-Oct-2014) -- End

            strQ = " SELECT ISNULL(SUM(lvleaveIssue_tran.dayfraction),0) as Issuedays FROM " & strDB & "lvleaveIssue_master " & _
                      " JOIN " & strDB & "lvleaveIssue_tran ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvleaveIssue_tran.isvoid = 0 "
            'Sohail (15 Jan 2019) - [strDB]

            'Pinkal (01-Dec-2014) -- Start
            'Enhancement - ENHANCEMENT GIVEN BY TRA - EMPLOYEE CAN APPLY LEAVE BEYOND ELC END DATE, IF EMPLOYEE HAVE LEAVE BALANCE .

            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                strQ &= " JOIN " & strDB & "lvleaveform ON lvleaveform.formunkid = lvleaveIssue_master.formunkid AND lvleaveform.isvoid = 0 " & _
                            " AND  CONVERT(char(8),lvleaveform.approve_stdate,112) BETWEEN @startdate AND @enddate "
                'Sohail (15 Jan 2019) - [strDB]
            End If

            strQ &= " WHERE lvleaveIssue_master.Employeeunkid = @Employeeunkid And lvleaveIssue_master.LeaveTypeunkid = @LeaveTypeunkid and lvleaveIssue_master.isvoid = 0"
            'Pinkal (01-Dec-2014) -- End

            objDataOperation.ClearParameters()

            If mdtdate <> Nothing Then
                strQ &= " AND lvleaveIssue_tran.leavedate <= @leavedate"
                objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtdate)
            End If


            'Pinkal (01-Dec-2014) -- Start
            'Enhancement - ENHANCEMENT GIVEN BY TRA - EMPLOYEE CAN APPLY LEAVE BEYOND ELC END DATE, IF EMPLOYEE HAVE LEAVE BALANCE .

            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            If mdtStartDate <> Nothing Then
                '    strQ &= " AND CONVERT(CHAR(8),lvleaveIssue_tran.leavedate,112) >= @startdate"
                objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
            End If

            If mdtEndDate <> Nothing Then
                '    strQ &= " AND CONVERT(CHAR(8),lvleaveIssue_tran.leavedate,112) <= @enddate"
                objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
            End If
            'Pinkal (21-Jul-2014) -- End

            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing AndAlso mstrFormIDs.Trim.Length > 0 Then
                strQ &= " AND lvleaveform.formunkid NOT in (" & mstrFormIDs & ")"
            End If

            'Pinkal (01-Dec-2014) -- End


            'Pinkal (24-May-2014) -- Start
            'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]

            If intYearId > 0 Then
                strQ &= " AND lvleaveIssue_master.leaveyearunkid = @leaveyearunkid"
                objDataOperation.AddParameter("@leaveyearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            End If

            'Pinkal (24-May-2014) -- End

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Employeeunkid)
            objDataOperation.AddParameter("@LeaveTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, LeaveTypeunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            'Pinkal (01-Dec-2014) -- Start
            'Enhancement - ENHANCEMENT GIVEN BY TRA - EMPLOYEE CAN APPLY LEAVE BEYOND ELC END DATE, IF EMPLOYEE HAVE LEAVE BALANCE .
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Pinkal (01-Dec-2014) -- End


            If dsList.Tables("List").Rows.Count > 0 Then
                IssuedaysCount = CDec(dsList.Tables("List").Rows(0)("Issuedays"))
            End If


            'Pinkal (21-Oct-2014) -- Start
            'Enhancement -  CHANGES GETTING PREVIOUS YEAR ISSUE DAYS COUNT FOR ALL COMPANY WHICH USES ELC SETTINGS.
            IssuedaysCount += mdecPreIssuedaysCount
            'Pinkal (21-Oct-2014) -- End


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeIssueDaysCount", mstrModuleName)
        Finally
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return IssuedaysCount
    End Function

    Public Function GetTerminatedEmployeeIssueDaysCount(ByVal Employeeunkid As Integer, ByVal LeaveTypeunkid As Integer, ByVal mdtTerminatedDate As Date) As Decimal
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim IssuedaysCount As Decimal = 0
        Try
            objDataOperation = New clsDataOperation


            strQ = " SELECT ISNULL(SUM(lvleaveday_fraction.dayfraction),0) as Issuedays FROM lvleaveIssue_master " & _
                      " JOIN lvleaveday_fraction ON lvleaveday_fraction.formunkid = lvleaveIssue_master.formunkid " & _
                   " JOIN lvleaveIssue_tran ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvleaveday_fraction.leavedate =lvleaveIssue_tran.leavedate AND lvleaveIssue_tran.isvoid = 0 " & _
                   " WHERE Employeeunkid = @Employeeunkid And LeaveTypeunkid = @LeaveTypeunkid and lvleaveIssue_master.isvoid = 0  AND Convert(char(8),lvleaveIssue_tran.leavedate,112) >= @leavedate"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtTerminatedDate.Date))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Employeeunkid)
            objDataOperation.AddParameter("@LeaveTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, LeaveTypeunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList.Tables("List").Rows.Count > 0 Then
                IssuedaysCount = CDec(dsList.Tables("List").Rows(0)("Issuedays"))
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetTerminatedEmployeeIssueDaysCount", mstrModuleName)
        End Try
        Return IssuedaysCount
    End Function

    Public Function GetIssuedDayFractionForViewer(ByVal intEmployeeId As Integer, ByVal strDate As String) As Decimal
        Dim mdecDayFraction As Decimal = 0
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation


            strQ = " SELECT ISNULL(dayfraction,0.00) AS dayfraction FROM lvleaveIssue_tran " & _
                      " JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
                      " AND lvleaveIssue_master.employeeunkid = @employeeunkid AND lvleaveIssue_master.isvoid= 0 " & _
                      " WHERE  CONVERT(CHAR(8), lvleaveIssue_tran.leavedate, 112) = @leavedate AND lvleaveIssue_tran.isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strDate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList.Tables("List").Rows.Count > 0 Then
                mdecDayFraction = CDec(dsList.Tables("List").Rows(0)("dayfraction"))
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetIssuedDayFractionForViewer; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
        Return mdecDayFraction
    End Function


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class