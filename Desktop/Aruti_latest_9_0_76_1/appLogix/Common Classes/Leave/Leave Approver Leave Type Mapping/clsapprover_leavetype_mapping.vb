﻿'************************************************************************************************************************************
'Class Name : clsapprover_leavetype_mapping.vb
'Purpose    :
'Date       :07/02/2012
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System

''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsapprover_leavetype_mapping
    Private Const mstrModuleName = "clsapprover_leavetype_mapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintLeavetypemappingunkid As Integer
    Private mintApproverunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdtLeaveType As DataTable

#End Region

#Region "Constructor"

    Public Sub New()

        mdtLeaveType = New DataTable("List")
        mdtLeaveType.Columns.Add("ischecked", Type.GetType("System.Boolean"))
        mdtLeaveType.Columns.Add("leavetypemappingunkid", Type.GetType("System.Int32"))
        mdtLeaveType.Columns.Add("approverunkid", Type.GetType("System.Int32"))
        mdtLeaveType.Columns.Add("leavetypeunkid", Type.GetType("System.Int32"))
        mdtLeaveType.Columns.Add("leavetypecode", Type.GetType("System.String"))
        mdtLeaveType.Columns.Add("leavename", Type.GetType("System.String"))
        mdtLeaveType.Columns.Add("ispaid", Type.GetType("System.Boolean"))

    End Sub

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavetypemappingunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Leavetypemappingunkid() As Integer
        Get
            Return mintLeavetypemappingunkid
        End Get
        Set(ByVal value As Integer)
            mintLeavetypemappingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approverunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Approverunkid() As Integer
        Get
            Return mintApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdtLeaveType
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _dtLeaveType() As DataTable
        Get
            Return mdtLeaveType
        End Get
        Set(ByVal value As DataTable)
            mdtLeaveType = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intApproverunkid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  lvapprover_leavetype_mapping.leavetypemappingunkid " & _
              ", lvapprover_leavetype_mapping.approverunkid " & _
              ", lvapprover_leavetype_mapping.leavetypeunkid " & _
              ", ISNULL(lvleavetype_master.leavetypecode,'') as leavetypecode " & _
              ", ISNULL(lvleavetype_master.leavename,'') as leavename " & _
              ", ISNULL(lvleavetype_master.ispaid,0) as ispaid " & _
              ", lvapprover_leavetype_mapping.userunkid " & _
              ", lvapprover_leavetype_mapping.isvoid " & _
              ", lvapprover_leavetype_mapping.voiduserunkid " & _
              ", lvapprover_leavetype_mapping.voiddatetime " & _
              ", lvapprover_leavetype_mapping.voidreason " & _
             " FROM lvapprover_leavetype_mapping " & _
             " JOIN lvleavetype_master on lvleavetype_master.leavetypeunkid = lvapprover_leavetype_mapping.leavetypeunkid AND lvleavetype_master.isactive = 1 "

            If blnOnlyActive Then
                strQ &= " WHERE lvapprover_leavetype_mapping.isvoid = 0 "
            End If

            If intApproverunkid > 0 Then
                strQ &= " AND lvapprover_leavetype_mapping.approverunkid = " & intApproverunkid
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvapprover_leavetype_mapping) </purpose>
    Public Function Insert(Optional ByVal objDataOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mblnIsFromApproveMst As Boolean = False

        If objDataOperation Is Nothing Then
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            mblnIsFromApproveMst = True
        End If


        Try

            If _dtLeaveType IsNot Nothing AndAlso _dtLeaveType.Rows.Count > 0 Then

                For Each dr As DataRow In _dtLeaveType.Rows
                    mblnIsvoid = False
                    mintVoiduserunkid = -1
                    mdtVoiddatetime = Nothing
                    If isExist(mintApproverunkid, CInt(dr("leavetypeunkid")), objDataOperation) Then
                        If CBool(dr("ischecked")) = False Then
                            mblnIsvoid = True
                            mintVoiduserunkid = mintUserunkid
                            mdtVoiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            Delete(objDataOperation, dr)
                        End If
                        Continue For
                    End If

                    If CBool(dr("ischecked")) Then

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
                        objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("leavetypeunkid").ToString()))
                        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                        strQ = "INSERT INTO lvapprover_leavetype_mapping ( " & _
                                  "  approverunkid " & _
                                  ", leavetypeunkid " & _
                                  ", userunkid " & _
                                  ", isvoid " & _
                                  ", voiduserunkid " & _
                                  ", voiddatetime " & _
                                  ", voidreason" & _
                                ") VALUES (" & _
                                  "  @approverunkid " & _
                                  ", @leavetypeunkid " & _
                                  ", @userunkid " & _
                                  ", @isvoid " & _
                                  ", @voiduserunkid " & _
                                  ", @voiddatetime " & _
                                  ", @voidreason" & _
                                "); SELECT @@identity"

                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        mintLeavetypemappingunkid = dsList.Tables(0).Rows(0).Item(0)
                        If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "lvapprover_leavetype_mapping", "leavetypemappingunkid", mintLeavetypemappingunkid, False, mintUserunkid) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    End If

                Next

            End If
            If mblnIsFromApproveMst = False Then
            objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            ' objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (mdprovider_mapping) </purpose>
    Public Function Delete(ByVal objDataOperation As clsDataOperation, ByVal dr As DataRow) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            If dr IsNot Nothing Then

                strQ = " UPDATE lvapprover_leavetype_mapping set " & _
                          "  isvoid = @isvoid " & _
                          ", voiduserunkid = @voiduserunkid " & _
                          ", voiddatetime=@voiddatetime  " & _
                          ", voidreason=@voidreason  " & _
                          " WHERE leavetypeunkid = @leavetypeunkid  AND approverunkid = @approverunkid AND isvoid = 0"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("leavetypeunkid").ToString)
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lvapprover_leavetype_mapping", "leavetypemappingunkid", CInt(dr("leavetypemappingunkid")), False, mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (mdprovider_mapping) </purpose>
    Public Function Delete(ByVal objDataOperation As clsDataOperation, ByVal intApproverunkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "Select ISNULL(leavetypemappingunkid,0) leavetypemappingunkid From lvapprover_leavetype_mapping where approverunkid = @approverunkid and isvoid = 0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")



            strQ = " UPDATE lvapprover_leavetype_mapping set " & _
                      "  isvoid = @isvoid " & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", voiddatetime=@voiddatetime  " & _
                      ", voidreason=@voidreason  " & _
                      " WHERE approverunkid = @approverunkid AND isvoid = 0"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lvapprover_leavetype_mapping", "leavetypemappingunkid", CInt(dsList.Tables(0).Rows(i)("leavetypemappingunkid")), False, mintVoiduserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function isExist(ByVal intappreoverunkid As Integer, ByVal intleavetypeunkid As Integer, Optional ByVal mobjDataOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If mobjDataOperation IsNot Nothing Then
            objDataOperation = mobjDataOperation
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  leavetypemappingunkid " & _
              ", approverunkid " & _
              ", leavetypeunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM lvapprover_leavetype_mapping " & _
             "WHERE approverunkid = @approverunkid " & _
             "AND leavetypeunkid = @leavetypeunkid AND isvoid = 0 "


            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intappreoverunkid)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intleavetypeunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLeaveTypeMappingUnkId(ByVal intappreoverunkid As Integer, ByVal intleavetypeunkid As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  leavetypemappingunkid " & _
              ", approverunkid " & _
              ", leavetypeunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM lvapprover_leavetype_mapping " & _
             "WHERE approverunkid = @approverunkid " & _
             "AND leavetypeunkid = @leavetypeunkid AND isvoid = 0 "


            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intappreoverunkid)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intleavetypeunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0)("leavetypemappingunkid"))
            Else
                Return -1
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLeaveTypeMappingUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLeaveTypeForMapping(ByVal intApproveID As Integer) As DataTable
        Dim dsFill As DataSet = Nothing
        Try
            Dim objLeaveType As New clsleavetype_master
            dsFill = objLeaveType.GetList("List", True, True)
            For Each dr As DataRow In dsFill.Tables(0).Rows

                Dim drRow As DataRow = mdtLeaveType.NewRow
                drRow("leavetypemappingunkid") = GetLeaveTypeMappingUnkId(intApproveID, CInt(dr("leavetypeunkid")))
                drRow("ischecked") = isExist(intApproveID, CInt(dr("leavetypeunkid")))
                drRow("approverunkid") = intApproveID
                drRow("leavetypeunkid") = dr("leavetypeunkid").ToString()
                drRow("leavetypecode") = dr("leavetypecode").ToString()
                drRow("leavename") = dr("leavename").ToString()
                drRow("ispaid") = dr("ispaid").ToString()
                mdtLeaveType.Rows.Add(drRow)
            Next
            Return mdtLeaveType
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLeaveTypeForMapping", mstrModuleName)
        End Try
        Return Nothing
    End Function



End Class