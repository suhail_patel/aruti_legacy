﻿'************************************************************************************************************************************
'Class Name : clsleaveform.vb
'Purpose    :
'Date       :12/07/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Web
Imports System.Xml
Imports System.Net
Imports System.IO

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsleaveform
    Private Shared ReadOnly mstrModuleName As String = "clsleaveform"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mintFormunkid As Integer
    Private mstrFormno As String = String.Empty
    Private mintEmployeeunkid As Integer
    Private mintLeavetypeunkid As Integer
    Private mdtApplydate As Date
    Private mdtStartdate As Date
    Private mdtReturndate As Date
    Private mstrAddressonleave As String = String.Empty
    Private mstrRemark As String = String.Empty
    Private mintStatusunkid As Integer = 2
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintUserunkid As Integer
    Private mintVoiduserunkid As Integer
    Private mintLoginEmployeeunkid As Integer = -1
    Dim objFraction As New clsleaveday_fraction
    Private mdtExpense As DataTable
    Dim objExpense As New clsleaveexpense
    Private mintVoidlogingemployeeunkid As Integer = -1
    Private mstrEmpFirstName As String = ""
    Private mstrEmpMiddleName As String = ""
    Private mstrEmpSurName As String = ""
    Private mstrEmpCode As String = ""
    Private mstrEmpMail As String = ""

    'S.SANDEEP [ 28 JAN 2014 ] -- START
    Private mstrWebFrmName As String = String.Empty
    'S.SANDEEP [ 28 JAN 2014 ] -- END

    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes
    Private mblnPaymentApprovalwithLeaveApproval As Boolean = False
    Private objClaimRequestMst As clsclaim_request_master = Nothing
    'Pinkal (06-Mar-2014) -- End

    'Pinkal (07-May-2015) -- Start
    'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.
    Private mdtAttachDocument As DataTable = Nothing
    'Pinkal (07-May-2015) -- End

    'Pinkal (22-Oct-2015) -- Start
    'Enhancement - WORKING ON AKFTZ LEAVE ISSUE ON WEB.
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    'Pinkal (22-Oct-2015) -- End

    'Pinkal (28-Oct-2015) -- Start
    'Enhancement - WORKING ON AKFTZ LEAVE ISSUE.LEAVE WAS NOT ISSUE FROM PROCESS PENDING FORM IN WEB.
    Dim objDoOperation As clsDataOperation
    'Pinkal (28-Oct-2015) -- End

    'Shani (20-Aug-2016) -- Start
    'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
    Private mdtClaimAttchment As DataTable = Nothing
    'Shani (20-Aug-2016) -- End


    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mintRelieverEmpunkid As Integer = 0
    'Pinkal (01-Oct-2018) -- End


    'Pinkal (01-Jan-2019) -- Start
    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
    Private mblnConsiderLeaveOnTnAPeriod As Boolean = False
    'Pinkal (01-Jan-2019) -- End


#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Formunkid() As Integer
        Get
            Return mintFormunkid
        End Get
        Set(ByVal value As Integer)
            mintFormunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formno
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Formno() As String
        Get
            Return mstrFormno
        End Get
        Set(ByVal value As String)
            mstrFormno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavetypeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leavetypeunkid() As Integer
        Get
            Return mintLeavetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintLeavetypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set applydate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Applydate() As Date
        Get
            Return mdtApplydate
        End Get
        Set(ByVal value As Date)
            mdtApplydate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Startdate() As Date
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As Date)
            mdtStartdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set returndate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Returndate() As Date
        Get
            Return mdtReturndate
        End Get
        Set(ByVal value As Date)
            mdtReturndate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set addressonleave
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Addressonleave() As String
        Get
            Return mstrAddressonleave
        End Get
        Set(ByVal value As String)
            mstrAddressonleave = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dtExpense
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _dtExpense() As DataTable
        Get
            Return mdtExpense
        End Get
        Set(ByVal value As DataTable)
            mdtExpense = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property

    Public Property _EmployeeCode() As String
        Get
            Return mstrEmpCode
        End Get
        Set(ByVal value As String)
            mstrEmpCode = value
        End Set
    End Property

    Public Property _EmployeeFirstName() As String
        Get
            Return mstrEmpFirstName
        End Get
        Set(ByVal value As String)
            mstrEmpFirstName = value
        End Set
    End Property

    Public Property _EmployeeMiddleName() As String
        Get
            Return mstrEmpMiddleName
        End Get
        Set(ByVal value As String)
            mstrEmpMiddleName = value
        End Set
    End Property

    Public Property _EmployeeSurName() As String
        Get
            Return mstrEmpSurName
        End Get
        Set(ByVal value As String)
            mstrEmpSurName = value
        End Set
    End Property

    Public Property _EmpMail() As String
        Get
            Return mstrEmpMail
        End Get
        Set(ByVal value As String)
            mstrEmpMail = value
        End Set
    End Property


    'S.SANDEEP [ 28 JAN 2014 ] -- START
    Public WriteOnly Property _WebFrmName() As String
        Set(ByVal value As String)
            mstrWebFrmName = value
        End Set
    End Property
    'S.SANDEEP [ 28 JAN 2014 ] -- END

    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Set IsPaymentApprovalwithLeaveApproval
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _IsPaymentApprovalwithLeaveApproval() As Boolean
        Set(ByVal value As Boolean)
            mblnPaymentApprovalwithLeaveApproval = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set ObjClaimRequestMst
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _ObjClaimRequestMst() As clsclaim_request_master
        Set(ByVal value As clsclaim_request_master)
            objClaimRequestMst = value
        End Set
    End Property

    'Pinkal (06-Mar-2014) -- End


    'Pinkal (07-May-2015) -- Start
    'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.

    ''' <summary>
    ''' Purpose: Get or Set mdtAttachDocument
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _dtAttachDocument() As DataTable
        Get
            Return mdtAttachDocument
        End Get
        Set(ByVal value As DataTable)
            mdtAttachDocument = value
        End Set
    End Property

    'Pinkal (07-May-2015) -- End

    'Shani (20-Aug-2016) -- Start
    'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
    Public Property _dtClaimAttchment() As DataTable
        Get
            Return mdtClaimAttchment
        End Get
        Set(ByVal value As DataTable)
            mdtClaimAttchment = value
        End Set
    End Property

    'Shani (20-Aug-2016) -- End



    'Pinkal (22-Oct-2015) -- Start
    'Enhancement - WORKING ON AKFTZ LEAVE ISSUE ON WEB.

    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    'Pinkal (22-Oct-2015) -- End

    'Pinkal (28-Oct-2015) -- Start
    'Enhancement - WORKING ON AKFTZ LEAVE ISSUE.LEAVE WAS NOT ISSUE FROM PROCESS PENDING FORM IN WEB.
    Public WriteOnly Property _DoOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDoOperation = value
        End Set
    End Property
    'Pinkal (28-Oct-2015) -- End


    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    ''' <summary>
    ''' Purpose: Set ReliverEmpId
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _RelieverEmpId() As Integer
        Get
            Return mintRelieverEmpunkid
        End Get
        Set(ByVal value As Integer)
            mintRelieverEmpunkid = value
        End Set
    End Property

    'Pinkal (01-Oct-2018) -- End

    'Pinkal (01-Jan-2019) -- Start
    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.

    ''' <summary>
    ''' Purpose: Get or Set ConsiderLeaveOnTnAPeriod
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _ConsiderLeaveOnTnAPeriod() As Boolean
        Get
            Return mblnConsiderLeaveOnTnAPeriod
        End Get
        Set(ByVal value As Boolean)
            mblnConsiderLeaveOnTnAPeriod = value
        End Set
    End Property

    'Pinkal (01-Jan-2019) -- End

#End Region

    ''' <summary>
    ''' This Method sets all properties of leave form class by passing the Unique Id (Leaveformunkid) of specific Leave form
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()
        Try


            strQ = "SELECT " & _
              "  formunkid " & _
              ", formno " & _
              ", employeeunkid " & _
              ", leavetypeunkid " & _
              ", applydate " & _
              ", startdate " & _
              ", returndate " & _
              ", addressonleave " & _
              ", remark " & _
              ", statusunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
            ", ISNULL(voidloginemployeeunkid,0) voidloginemployeeunkid " & _
              ", ISNULL(relieverempunkid,0) relieverempunkid " & _
             ", ISNULL(istnaperiodlinked,0)  AS istnaperiodlinked " & _
             "FROM lvleaveform " & _
             "WHERE formunkid = @formunkid "

            'Pinkal (01-Jan-2019) -- 'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[", ISNULL(istnaperiodlinked,0)  AS istnaperiodlinked " & _]

            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintFormunkid = CInt(dtRow.Item("formunkid"))
                mstrFormno = dtRow.Item("formno").ToString
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintLeavetypeunkid = CInt(dtRow.Item("leavetypeunkid"))
                mdtApplydate = dtRow.Item("applydate")
                mdtStartdate = dtRow.Item("startdate")
                If IsDBNull(dtRow.Item("returndate")) Then
                    mdtReturndate = Nothing
                Else
                    mdtReturndate = dtRow.Item("returndate")
                End If
                mstrAddressonleave = dtRow.Item("addressonleave").ToString
                mstrRemark = dtRow.Item("remark").ToString
                mintStatusunkid = CInt(dtRow.Item("statusunkid").ToString)
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If mdtVoiddatetime <> Nothing Then mdtVoiddatetime = dtRow.Item("voiddatetime")
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintVoiduserunkid = dtRow.Item("voiduserunkid")

                If dtRow.Item("loginemployeeunkid").ToString <> "" AndAlso IsDBNull(dtRow.Item("loginemployeeunkid")) = False Then
                    mintLoginEmployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                End If

                If IsDBNull(dtRow.Item("voidloginemployeeunkid")) = False Then
                    mintVoidlogingemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                End If

                'Pinkal (01-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                If IsDBNull(dtRow.Item("relieverempunkid")) = False Then
                    mintRelieverEmpunkid = CInt(dtRow.Item("relieverempunkid"))
                End If
                'Pinkal (01-Oct-2018) -- End


                'Pinkal (01-Jan-2019) -- Start
                'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                mblnConsiderLeaveOnTnAPeriod = CBool(dtRow.Item("istnaperiodlinked"))
                'Pinkal (01-Jan-2019) -- End


                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intApproverunkid As Integer = -1, _
    '                                Optional ByVal blnIncludeInactiveEmployee As String = "", Optional ByVal strEmployeeAsOnDate As String = "", _
    '                                Optional ByVal strUserAccessLevelFilterString As String = "", Optional ByVal intLoginEmployeeID As Integer = -1, _
    '                                Optional ByVal mstrFilterString As String = "") As DataSet  'Pinkal (06-May-2014) -- Start [Optional ByVal mstrFilterString As String = ""]


    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation

    '    Try

    'strQ = "SELECT " & _
    '       "  lvleaveform.formunkid " & _
    '       ", isnull(lvleaveIssue_master.leaveissueunkid,-1) as leaveissueunkid " & _
    '       ", formno " & _
    '       ", lvleaveform.employeeunkid " & _
    '       ", h1.employeecode " & _
    '       ", isnull(h1.firstname,'') + ' ' + isnull(h1.surname,'') as employeename" & _
    '       ", lvleaveform.leavetypeunkid " & _
    '       ", lvLeaveType_master.leavename " & _
    '       ", convert(char(8),applydate,112) as  applydate" & _
    '       ", convert(char(8),lvleaveform.startdate,112) as startdate " & _
    '       ", convert(char(8),lvleaveform.returndate,112) as returndate " & _
    '       ", DATEPART(MM,lvleaveform.startdate) as startmonth  " & _
    '       ", DATEPART(MM,lvleaveform.returndate) as endmonth  " & _
    '       ", ISNULL((SELECT SUM(dayfraction) FROM lvleaveday_fraction WHERE lvleaveday_fraction.formunkid = lvleaveform.formunkid  " & _
    '                       "    AND lvleaveform.isvoid = 0 AND lvleaveday_fraction.isvoid = 0 AND leavedate BETWEEN lvleaveform.startdate AND ISNULL(lvleaveform.returndate,lvleaveform.startdate) AND ISNULL(lvleaveday_fraction.approverunkid,0) <=0),0.00) days " & _
    '       ", addressonleave " & _
    '       ", lvleaveform.statusunkid " & _
    '       ", case when lvleaveform.statusunkid = 1 then @Approve " & _
    '       "         when lvleaveform.statusunkid = 2 then @Pending  " & _
    '       "         when lvleaveform.statusunkid = 3 then @Reject  " & _
    '       "         when lvleaveform.statusunkid = 4 then @ReSchedule " & _
    '       "         when lvleaveform.statusunkid = 6 then @Cancel " & _
    '       "         when lvleaveform.statusunkid = 7 then @Issued " & _
    '       "        end as status" & _
    '       ", lvleaveform.remark " & _
    '       ", lvleaveform.isvoid " & _
    '       ", lvleaveform.voiddatetime " & _
    '       ", lvleaveform.userunkid " & _
    '       ", lvleaveform.voiduserunkid " & _
    '       ", lvleaveform.voidreason " & _
    '       ", lvleaveform.loginemployeeunkid " & _
    '       ", ISNULL(lvleaveform.voidloginemployeeunkid,0) voidloginemployeeunkid " & _
    '       ", lvLeaveType_master.color " & _
    '       ", convert(char(8),lvleaveform.approve_stdate,112) As Approved_StartDate " & _
    '       ", convert(char(8),lvleaveform.approve_eddate,112) As Approved_EndDate " & _
    '       ", convert(char(8),lvleaveform.approve_days,112) As Approved_Days "

    '        strQ &= ",ISNULL(h1.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                 ",ISNULL(h1.unitgroupunkid,0) AS unitgroupunkid " & _
    '                 ",ISNULL(h1.teamunkid,0) AS teamunkid " & _
    '                 ",ISNULL(h1.stationunkid,0) AS stationunkid " & _
    '                 ",ISNULL(h1.deptgroupunkid,0) AS deptgroupunkid " & _
    '                 ",ISNULL(h1.departmentunkid,0) AS departmentunkid " & _
    '                 ",ISNULL(h1.sectionunkid,0) AS sectionunkid " & _
    '                 ",ISNULL(h1.unitunkid,0) AS unitunkid " & _
    '                 ",ISNULL(h1.jobunkid,0) AS jobunkid " & _
    '                 ",ISNULL(h1.classgroupunkid,0) AS classgroupunkid " & _
    '                 ",ISNULL(h1.classunkid,0) AS classunkid " & _
    '                 ",ISNULL(h1.jobgroupunkid,0) AS jobgroupunkid " & _
    '                 ",ISNULL(h1.gradegroupunkid,0) AS gradegroupunkid " & _
    '                 ",ISNULL(h1.gradeunkid,0) AS gradeunkid " & _
    '                 ",ISNULL(h1.gradelevelunkid,0) AS gradelevelunkid "

    '        strQ &= " FROM lvleaveform " & _
    '         " LEFT JOIN hremployee_master h1 on h1.employeeunkid = lvleaveform.employeeunkid " & _
    '         " LEFT JOIN lvLeaveType_master on lvLeaveType_master.leavetypeunkid = lvleaveform.leavetypeunkid " & _
    '         " LEFT JOIN lvleaveIssue_master on lvleaveIssue_master.formunkid = lvleaveform.formunkid and lvleaveIssue_master.isvoid = 0"

    '        strQ &= " WHERE 1 = 1 "
    '        If blnOnlyActive Then
    '            strQ &= " AND lvleaveform.isvoid = 0 "
    '        End If

    '        If intLoginEmployeeID <= 0 Then

    '        If strUserAccessLevelFilterString = "" Then
    '            strQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "h1")
    '        Else
    '            strQ &= strUserAccessLevelFilterString.Replace("hremployee_master", "h1")
    '        End If

    '        End If

    '        objDataOperation.ClearParameters()


    '        If blnIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            blnIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString()
    '        End If

    '        If CBool(blnIncludeInactiveEmployee) = False Then

    '            strQ &= " AND CONVERT(CHAR(8),h1.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),h1.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),h1.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),h1.empl_enddate,112), @startdate) >= @startdate "

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))

    '        End If

    '        strQ &= " AND h1.isapproved = 1 "


    '        'Pinkal (06-May-2014) -- Start
    '        'Enhancement : TRA Changes
    '        If mstrFilterString.Trim.Length > 0 Then
    '            strQ &= " AND " & mstrFilterString
    '        End If
    '        'Pinkal (06-May-2014) -- End


    '        objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
    '        objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
    '        objDataOperation.AddParameter("@ReSchedule", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 113, "Re-Scheduled"))
    '        objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
    '        objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 276, "Issued"))


    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)


    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (lvleaveform) </purpose>
    'Public Function Insert(Optional ByVal dtFraction As DataTable = Nothing, Optional ByVal intCompanyUnkId As Integer = 0, Optional ByVal blnLeaveApproverForLeaveType As String = "") As Boolean

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation

    '    Try
    '        objDataOperation.BindTransaction()
    '        'START FOR AUTO INCREMENT NO 

    '    Dim intFormNoType As Integer = 0
    '        Dim objConfig As New clsConfigOptions
    '        objConfig._Companyunkid = IIf(intCompanyUnkId = 0, Company._Object._Companyunkid, intCompanyUnkId)
    '        intFormNoType = objConfig._LeaveFormNoType

    '        If intFormNoType = 0 Then
    '            If isExist(mstrFormno) Then
    '                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Form No is already defined. Please define new Form No.")
    '                objDataOperation.ReleaseTransaction(False)
    '                Return False
    '            End If
    '    End If

    '        'Pinkal (15-Jul-2013) -- Start
    '        'Enhancement : TRA Changes


    '        'S.SANDEEP [ 26 SEPT 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        If mdtReturndate <> Nothing Then
    '        If isDayExist(False, mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date) Then
    '            mstrMessage = Language.getMessage(mstrModuleName, 3, "This tenure is already defined to other form. Please define new tenure for this form.")
    '            objDataOperation.ReleaseTransaction(False)
    '        Return False
    '    End If

    '        If isDayExist(True, mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date) Then
    '            mstrMessage = Language.getMessage(mstrModuleName, 26, "This tenure is already approved by approver.Please define new tenure for this form.")
    '            objDataOperation.ReleaseTransaction(False)
    '            Return False
    '        End If
    '        End If
    '        'S.SANDEEP [ 26 SEPT 2013 ] -- END



    '        'Pinkal (15-Jul-2013) -- End

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@formno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormno.ToString)
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
    '        objDataOperation.AddParameter("@applydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplydate)
    '        objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)

    '        'S.SANDEEP [ 26 SEPT 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'objDataOperation.AddParameter("@returndate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReturndate)
    '        If mdtReturndate <> Nothing Then
    '        objDataOperation.AddParameter("@returndate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReturndate)
    '        Else
    '            objDataOperation.AddParameter("@returndate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        End If
    '        'S.SANDEEP [ 26 SEPT 2013 ] -- END
    '        objDataOperation.AddParameter("@addressonleave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddressonleave.ToString)
    '        objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
    '        objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '        objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

    '        strQ = "INSERT INTO lvleaveform ( " & _
    '          "  formno " & _
    '          ", employeeunkid " & _
    '          ", leavetypeunkid " & _
    '          ", applydate " & _
    '          ", startdate " & _
    '          ", returndate " & _
    '          ", addressonleave " & _
    '          ", remark " & _
    '          ", statusunkid" & _
    '          ", isvoid " & _
    '          ", voiddatetime " & _
    '          ", userunkid " & _
    '          ", loginemployeeunkid " & _
    '          ", voiduserunkid" & _
    '          ", voidreason " & _
    '        ") VALUES (" & _
    '          "  @formno " & _
    '          ", @employeeunkid " & _
    '          ", @leavetypeunkid " & _
    '          ", @applydate " & _
    '          ", @startdate " & _
    '          ", @returndate " & _
    '          ", @addressonleave " & _
    '          ", @remark " & _
    '          ", @statusunkid " & _
    '          ", @isvoid " & _
    '          ", @voiddatetime " & _
    '          ", @userunkid " & _
    '          ", @loginemployeeunkid " & _
    '          ", @voiduserunkid" & _
    '          ", @voidreason " & _
    '        "); SELECT @@identity"



    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintFormunkid = dsList.Tables(0).Rows(0).Item(0)
    '        If intFormNoType = 1 Then
    '            If Set_AutoNumber(objDataOperation, mintFormunkid, "lvleaveform", "formno", "formunkid", "NextLeaveFormNo", objConfig._LeaveFormNoPrifix, objConfig._Companyunkid) = False Then
    '                If objDataOperation.ErrorMessage <> "" Then
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If
    '            End If

    '        End If

    '        objFraction._Formunkid = mintFormunkid
    '        objFraction._dtfraction = dtFraction
    '        objFraction._Userunkid = mintUserunkid
    '        objFraction._Loginemployeeunkid = mintLoginEmployeeunkid
    '        objFraction._Voidloginemployeeunkid = mintVoidlogingemployeeunkid
    '        If objFraction.InsertUpdateDelete_Fraction(objDataOperation) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If



    '        'Pinkal (06-Mar-2014) -- Start 
    '        'Enhancement : TRA Changes

    '        'objExpense._Formunkid = mintFormunkid
    '        'objExpense._dtExpense = mdtExpense
    '        'objExpense._Userunkid = mintUserunkid
    '        'objExpense._Loginemployeeunkid = mintVoidlogingemployeeunkid

    '        'If objExpense.InsertUpdateDelete_Expense(objDataOperation) = False Then
    '        '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        '    Throw exForce
    '        'End If

    '        If objClaimRequestMst IsNot Nothing AndAlso mdtExpense IsNot Nothing Then

    '            If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
    '                blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
    '            End If

    '            objClaimRequestMst._LeaveApproverForLeaveType = CBool(blnLeaveApproverForLeaveType)
    '            objClaimRequestMst._LeaveTypeId = mintLeavetypeunkid
    '            objClaimRequestMst._Referenceunkid = mintFormunkid
    '            objClaimRequestMst._FromModuleId = enExpFromModuleID.FROM_LEAVE
    '            If objClaimRequestMst.Insert(mdtExpense, objConfig._Companyunkid, objDataOperation, mblnPaymentApprovalwithLeaveApproval) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        End If

    '        'Pinkal (06-Mar-2014) -- End





    '        'S.SANDEEP [ 26 SEPT 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'Dim objapprover As New clsleaveapprover_master

    '        ''Pinkal (06-Feb-2012) -- Start
    '        ''Enhancement : TRA Changes
    '        ''Dim dtList As DataTable = objapprover.GetEmployeeApprover(, mintEmployeeunkid)

    '        'Dim dtList As DataTable = Nothing


    '        ''Pinkal (25-APR-2012) -- Start
    '        ''Enhancement : TRA Changes

    '        ''If ConfigParameter._Object._IsLeaveApprover_ForLeaveType Then
    '        ''    dtList = objapprover.GetEmployeeApprover(-1, mintEmployeeunkid, -1, mintLeavetypeunkid)
    '        ''Else
    '        ''    dtList = objapprover.GetEmployeeApprover(-1, mintEmployeeunkid, -1, -1)
    '        ''End If

    '        'If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
    '        '    blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
    '        'End If

    '        'If CBool(blnLeaveApproverForLeaveType) = True Then
    '        '    dtList = objapprover.GetEmployeeApprover(-1, mintEmployeeunkid, -1, mintLeavetypeunkid, blnLeaveApproverForLeaveType)
    '        'Else
    '        '    dtList = objapprover.GetEmployeeApprover(-1, mintEmployeeunkid, -1, -1)
    '        'End If


    '        ''Pinkal (25-APR-2012) -- End

    '        ''Pinkal (06-Feb-2012) -- End



    '        'If dtList.Rows.Count > 0 Then
    '        '    Dim objpending As New clspendingleave_Tran
    '        '    For Each dr As DataRow In dtList.Rows
    '        '        objpending._Approvaldate = mdtApplydate
    '        '        objpending._Startdate = mdtStartdate
    '        '        objpending._Enddate = mdtReturndate
    '        '        objpending._Approverunkid = CInt(dr("employeeunkid"))

    '        '        'Pinkal (03-APR-2012) -- Start
    '        '        'Enhancement : TRA Changes
    '        '        objpending._Approvertranunkid = CInt(dr("approverunkid"))
    '        '        'Pinkal (03-APR-2012) -- End

    '        '        objpending._Employeeunkid = mintEmployeeunkid
    '        '        objpending._Formunkid = mintFormunkid
    '        '        objpending._Statusunkid = mintStatusunkid
    '        '        objpending._Userunkid = mintUserunkid


    '        '        'Pinkal (15-Jul-2013) -- Start
    '        '        'Enhancement : TRA Changes

    '        '        If dtFraction IsNot Nothing AndAlso dtFraction.Rows.Count > 0 Then
    '        '            objpending._DayFraction = dtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'")
    '        '        Else
    '        '            objpending._DayFraction = 0
    '        '        End If

    '        '        'Pinkal (15-Jul-2013) -- End

    '        '        Dim blnFlag As Boolean = objpending.Insert(objDataOperation)

    '        '        If blnFlag = False And objpending._Message <> "" Then
    '        '            Throw New Exception(objpending._Message)
    '        '            Exit For
    '        '        End If
    '        '    Next

    '        'End If
    '        If mdtReturndate <> Nothing Then
    '            Dim objapprover As New clsleaveapprover_master
    '            Dim dtList As DataTable = Nothing

    '        If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
    '            blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
    '        End If

    '        If CBool(blnLeaveApproverForLeaveType) = True Then
    '            dtList = objapprover.GetEmployeeApprover(-1, mintEmployeeunkid, -1, mintLeavetypeunkid, blnLeaveApproverForLeaveType)
    '        Else
    '            dtList = objapprover.GetEmployeeApprover(-1, mintEmployeeunkid, -1, -1)
    '        End If

    '        If dtList.Rows.Count > 0 Then
    '            Dim objpending As New clspendingleave_Tran
    '            For Each dr As DataRow In dtList.Rows
    '                objpending._Approvaldate = mdtApplydate
    '                objpending._Startdate = mdtStartdate
    '                objpending._Enddate = mdtReturndate
    '                objpending._Approverunkid = CInt(dr("employeeunkid"))
    '                objpending._Approvertranunkid = CInt(dr("approverunkid"))
    '                objpending._Employeeunkid = mintEmployeeunkid
    '                objpending._Formunkid = mintFormunkid
    '                objpending._Statusunkid = mintStatusunkid
    '                objpending._Userunkid = mintUserunkid



    '                    'Pinkal (09-Jul-2014) -- Start
    '                    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 2

    '                    Dim intMinPriority As Integer = CInt(dtList.Compute("MIN(priority)", "1=1"))

    '                    If intMinPriority = CInt(dr("priority")) Then
    '                        objpending._VisiblelId = mintStatusunkid
    '                    Else
    '                        objpending._VisiblelId = -1
    '                    End If
    '                    'Pinkal (09-Jul-2014) -- End



    '                If dtFraction IsNot Nothing AndAlso dtFraction.Rows.Count > 0 Then
    '                    objpending._DayFraction = dtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'")
    '                Else
    '                    objpending._DayFraction = 0
    '                End If

    '                Dim blnFlag As Boolean = objpending.Insert(objDataOperation)

    '                If blnFlag = False And objpending._Message <> "" Then
    '                    Throw New Exception(objpending._Message)
    '                    Exit For
    '                End If
    '            Next
    '            End If
    '        End If
    '        'S.SANDEEP [ 26 SEPT 2013 ] -- END


    '        'Pinkal (07-May-2015) -- Start
    '        'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.

    '        If mdtAttachDocument IsNot Nothing Then
    '            Dim drRow() As DataRow = mdtAttachDocument.Select("AUD = 'A'")
    '            If drRow.Length > 0 Then
    '                For i As Integer = 0 To drRow.Length - 1
    '                    drRow(i)("transactionunkid") = mintFormunkid
    '                    drRow(i).AcceptChanges()
    '                Next
    '            End If
    '            Dim objDocument As New clsScan_Attach_Documents
    '            objDocument._Datatable = mdtAttachDocument
    '            If objDocument.InsertUpdateDelete_Documents(objDataOperation) = False Then
    '                Throw New Exception(objDocument._Message)
    '            End If
    '            objDocument = Nothing
    '        End If

    '        'Pinkal (07-May-2015) -- End

    '        objDataOperation.ReleaseTransaction(True)
    '        Return True

    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '    ''' <summary>
    '    ''' Modify By: Pinkal
    '    ''' </summary>
    '    ''' <returns>Boolean</returns>
    '    ''' <purpose> Update Database Table (lvleaveform) </purpose>
    '    Public Function Update(ByVal mblnIssued As Boolean, ByVal dtFraction As DataTable, Optional ByVal intYearID As Integer = -1, _
    '                                     Optional ByVal blnLeaveApproverForLeaveType As String = "", Optional ByVal intLeaveBalanceSetting As Integer = -1, Optional ByVal intCompanyID As Integer = -1) As Boolean

    '        Dim dsList As DataSet = Nothing
    '        Dim strQ As String = ""
    '        Dim exForce As Exception
    '        Dim objDataOperation As New clsDataOperation
    '        Try
    '            objDataOperation.BindTransaction()

    '            'Pinkal (06-Feb-2013) -- Start
    '            'Enhancement : TRA Changes
    '            If intLeaveBalanceSetting <= 0 Then
    '                intLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
    '            End If
    '            'Pinkal (06-Feb-2013) -- End


    '            If isExist(mstrFormno, mintFormunkid) Then
    '                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Form No is already defined. Please define new Form No.")
    '                objDataOperation.ReleaseTransaction(False)
    '                Return False


    '                'Pinkal (15-Jul-2013) -- Start
    '                'Enhancement : TRA Changes

    '                'S.SANDEEP [ 26 SEPT 2013 ] -- START
    '                'ENHANCEMENT : TRA CHANGES
    '            End If

    '            If mdtReturndate <> Nothing Then
    '                If isDayExist(False, mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date, mintFormunkid) Then
    '                    mstrMessage = Language.getMessage(mstrModuleName, 3, "This tenure is already defined to other form. Please define new tenure for this form.")
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If

    '                If isDayExist(True, mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date, mintFormunkid) Then
    '                    mstrMessage = Language.getMessage(mstrModuleName, 26, "This tenure is already approved by approver.Please define new tenure for this form.")
    '                    objDataOperation.ReleaseTransaction(False)
    '                    Return False
    '                End If
    '            End If
    '            'S.SANDEEP [ 26 SEPT 2013 ] -- END



    '            'Pinkal (15-Jul-2013) -- End



    '            If mblnIssued Then

    '                'START FOR GET LEAVE ISSUE AMOUNT FOR THAT LEAVE FORM

    '                strQ = " Select count(*) issue_Amount From lvleaveissue_tran " & _
    '                     " WHERE leaveissueunkid in (select leaveissueunkid from lvleaveissue_master where formunkid = @formunkid and isvoid = 0) "
    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid)
    '                Dim dsIssueAmount As DataSet = objDataOperation.ExecQuery(strQ, "IssueAmount")

    '                'END FOR GET LEAVE ISSUE AMOUNT FOR THAT LEAVE FORM

    '                Dim objBalance As New clsleavebalance_tran


    '                'Pinkal (06-Feb-2013) -- Start
    '                'Enhancement : TRA Changes

    '                Dim dsBalList As DataSet = Nothing
    '                If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                    dsBalList = objBalance.GetList("BalanceList", True)
    '                ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                    dsBalList = objBalance.GetList("BalanceList", True, False, -1, True, True)
    '                End If

    '                'Pinkal (06-Feb-2013) -- End

    '                If dsBalList IsNot Nothing And dsBalList.Tables(0).Rows.Count > 0 Then
    '                    If intYearID <= 0 Then
    '                        intYearID = FinancialYear._Object._YearUnkid
    '                    End If
    '                    Dim drRow As DataRow() = dsBalList.Tables(0).Select("yearunkid =" & intYearID & " AND employeeunkid= " & mintEmployeeunkid & " AND leavetypeunkid=" & mintLeavetypeunkid)
    '                    If drRow.Length = 0 Then GoTo UpdateForm
    '                    Dim dsBalance As DataSet = objBalance.GetAuditDataforLeaveBalance(CInt(drRow(0)("leavebalanceunkid")), intYearID, mintLeavetypeunkid, mintEmployeeunkid)

    '                    If dsBalance IsNot Nothing AndAlso dsBalance.Tables(0).Rows.Count > 0 Then
    '                        strQ = "delete from atlvleavebalance_tran where atunkid = @atunkid "
    '                        objDataOperation.ClearParameters()
    '                        objDataOperation.AddParameter("@atunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsBalance.Tables(0).Rows(0)("atunkid"))
    '                        objDataOperation.ExecNonQuery(strQ)
    '                    End If
    '                    dsBalance = objBalance.GetAuditDataforLeaveBalance(CInt(drRow(0)("leavebalanceunkid")), intYearID, mintLeavetypeunkid, mintEmployeeunkid)
    '                    If dsBalance.Tables(0).Rows.Count > 0 Then

    '                        'START FOR UPDATE LEAVE BALANCE TRAN
    '                        strQ = " Update lvleavebalance_tran set " & _
    '                          "  issue_amount =  @issue_amount " & _
    '                          ", balance = @balance  " & _
    '                          ", uptolstyr_issueamt =  @uptolstyr_issueamt " & _
    '                          ", remaining_bal = @remaining_bal " & _
    '                          ", days = @days " & _
    '                          " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid  and isvoid = 0  "


    '                        'Pinkal (06-Feb-2013) -- Start
    '                        'Enhancement : TRA Changes
    '                        If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                            strQ &= " AND isopenelc  = 1 AND iselc = 1 "
    '                        End If
    '                        'Pinkal (06-Feb-2013) -- End


    '                        objDataOperation.ClearParameters()
    '                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
    '                        objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid)
    '                        objDataOperation.AddParameter("@issue_amount", SqlDbType.Int, eZeeDataType.INT_SIZE, dsBalance.Tables(0).Rows(0)("issue_amount"))
    '                        objDataOperation.AddParameter("@balance", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dsBalance.Tables(0).Rows(0)("balance"))
    '                        objDataOperation.AddParameter("@remaining_bal", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dsBalance.Tables(0).Rows(0)("remaining_bal"))
    '                        objDataOperation.AddParameter("@uptolstyr_issueamt", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dsBalance.Tables(0).Rows(0)("uptolstyr_issueamt"))
    '                        objDataOperation.AddParameter("@days", SqlDbType.Int, eZeeDataType.INT_SIZE, dsBalance.Tables(0).Rows(0)("days"))
    '                        objDataOperation.ExecNonQuery(strQ)

    '                        'END FOR UPDATE LEAVE BALANCE TRAN

    '                    End If


    '                    'START FOR INSERT LEAVE BALANCE AUDIT TRAIL

    '                    If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                        dsBalList = objBalance.GetList("BalanceList", True)
    '                    ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                        dsBalList = objBalance.GetList("BalanceList", True, False, -1, True, True)
    '                    End If

    '                    Dim dtBalance As DataTable = New DataView(dsBalList.Tables("BalanceList"), "employeeunkid=" & mintEmployeeunkid & " AND leavetypeunkid = " & mintLeavetypeunkid, "", DataViewRowState.CurrentRows).ToTable

    '                    If dtBalance.Rows.Count > 0 Then
    '                        For i As Integer = 0 To dtBalance.Rows.Count - 1
    '                            objBalance._LeaveBalanceunkid = CInt(dtBalance.Rows(i)("leavebalanceunkid"))
    '                            If objBalance.InsertAudiTrailForLeaveBalance(objDataOperation, 2) = False Then
    '                                objDataOperation.ReleaseTransaction(False)
    '                                Return False
    '                            End If
    '                        Next
    '                    End If

    '                    If objBalance._Message <> "" Then
    '                        Throw New Exception(objBalance._Message)
    '                    End If
    '                    'END FOR INSERT LEAVE BALANCE AUDIT TRAIL

    '                    'START FOR GET LEAVE ISSUE UNKID 
    '                    strQ = "select isnull(leaveissueunkid,0) as leaveissueunkid from lvleaveissue_master where formunkid = @formid"
    '                    objDataOperation.ClearParameters()
    '                    objDataOperation.AddParameter("@formid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid)
    '                    Dim dsIssueUnkid As DataSet = objDataOperation.ExecQuery(strQ, "leaveissueunkid")
    '                    'END FOR GET LEAVE ISSUE UNKID 

    '                    If dsIssueUnkid.Tables("leaveissueunkid").Rows.Count > 0 Then

    '                        'START FOR INSERT LEAVE ISSUE AUDIT TRAIL
    '                        Dim objIssue As New clsleaveissue_Tran
    '                        Dim dsIssue As DataSet = objIssue.GetList("List", True)
    '                        Dim dtIssue As DataTable = New DataView(dsIssue.Tables("List"), "leaveissueunkid=" & CInt(dsIssueUnkid.Tables("leaveissueunkid").Rows(0)("leaveissueunkid")) _
    '                                                                  & " AND formunkid = " & mintFormunkid, "", DataViewRowState.CurrentRows).ToTable

    '                        If dtIssue.Rows.Count > 0 Then
    '                            For i As Integer = 0 To dtIssue.Rows.Count - 1
    '                                objIssue._Leaveissuetranunkid = CInt(dtIssue.Rows(i)("leaveissuetranunkid"))
    '                                If objIssue.InsertAuditTrailForLeaveIssue(objDataOperation, 3, eZeeDate.convertDate(dtIssue.Rows(i)("leavedate").ToString).ToShortDateString) = False Then
    '                                    objDataOperation.ReleaseTransaction(False)
    '                                    Return False
    '                                End If
    '                            Next
    '                        End If

    '                        If objIssue._Message <> "" Then
    '                            Throw New Exception(objIssue._Message)
    '                        End If
    '                        'END FOR INSERT LEAVE ISSUE AUDIT TRAIL

    '                    End If

    '                    strQ = "Update lvleaveissue_tran set" & _
    '                           " isvoid =1,voiddatetime= @voiddatetime, " & _
    '                           " voidreason = @voidreason,  " & _
    '                           " voiduserunkid = @voiduserunkid " & _
    '                           " WHERE leaveissueunkid = (select leaveissueunkid from lvleaveissue_master where formunkid = @formid and isvoid = 0) "

    '                    objDataOperation.ClearParameters()
    '                    objDataOperation.AddParameter("@formid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid)
    '                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
    '                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
    '                    objDataOperation.ExecNonQuery(strQ)


    '                    strQ = "Update lvleaveissue_master set" & _
    '                                       " isvoid =1,voiddatetime= @voiddatetime,voidreason = @void_reason, " & _
    '                                       " voiduserunkid = @voiduserunkid " & _
    '                                       " WHERE formunkid = @form_id"
    '                    objDataOperation.ClearParameters()
    '                    objDataOperation.AddParameter("@form_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid)
    '                    objDataOperation.AddParameter("@void_reason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
    '                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
    '                    objDataOperation.ExecNonQuery(strQ)

    '                End If

    '            End If

    'UpdateForm:

    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
    '            objDataOperation.AddParameter("@formno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormno.ToString)
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
    '            objDataOperation.AddParameter("@applydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplydate)
    '            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)

    '            'S.SANDEEP [ 26 SEPT 2013 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            'objDataOperation.AddParameter("@returndate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReturndate)
    '            If mdtReturndate <> Nothing Then
    '                objDataOperation.AddParameter("@returndate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReturndate)
    '            Else
    '                objDataOperation.AddParameter("@returndate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '            End If
    '            'S.SANDEEP [ 26 SEPT 2013 ] -- END

    '            objDataOperation.AddParameter("@addressonleave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddressonleave.ToString)
    '            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
    '            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
    '            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
    '            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
    '            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

    '            strQ = "UPDATE lvleaveform SET " & _
    '              "  formno = @formno" & _
    '              ", employeeunkid = @employeeunkid" & _
    '              ", leavetypeunkid = @leavetypeunkid" & _
    '              ", applydate = @applydate" & _
    '              ", startdate = @startdate" & _
    '              ", returndate = @returndate" & _
    '              ", addressonleave = @addressonleave" & _
    '              ", remark = @remark" & _
    '              ", statusunkid = @statusunkid" & _
    '              ", isvoid = @isvoid" & _
    '              ", voiddatetime = @voiddatetime" & _
    '              ", userunkid = @userunkid" & _
    '              ", loginemployeeunkid = @loginemployeeunkid" & _
    '              ", voiduserunkid = @voiduserunkid " & _
    '              ", voidreason = @voidreason " & _
    '            "WHERE formunkid = @formunkid "

    '            Call objDataOperation.ExecNonQuery(strQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            objFraction._Formunkid = mintFormunkid
    '            objFraction._dtfraction = dtFraction
    '            objFraction._Userunkid = mintUserunkid
    '            objFraction._Loginemployeeunkid = mintLoginEmployeeunkid
    '            objFraction._Voidloginemployeeunkid = mintVoidlogingemployeeunkid

    '            Dim dt() As DataRow = objFraction._dtfraction.Select("AUD=''")
    '            If dt.Length = objFraction._dtfraction.Rows.Count Then
    '                If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "lvleaveform", mintFormunkid, "formunkid", 2, objDataOperation) Then
    '                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", mintFormunkid, "lvleaveday_fraction", "dayfractionunkid", -1, 2, 0, False, mintUserunkid) = False Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                End If

    '            Else

    '                'Pinkal (15-Jul-2013) -- Start
    '                'Enhancement : TRA Changes

    '                objFraction.VoidApproverLeaveDayFraction(objDataOperation, mintFormunkid, mintUserunkid, mintLoginEmployeeunkid)

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If

    '                'Pinkal (15-Jul-2013) -- End

    '                If objFraction.InsertUpdateDelete_Fraction(objDataOperation) = False Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If

    '            End If

    '            'objExpense._Formunkid = mintFormunkid
    '            'objExpense._dtExpense = mdtExpense
    '            'objExpense._Userunkid = mintUserunkid
    '            'objExpense._Loginemployeeunkid = mintLoginEmployeeunkid
    '            'objExpense._Voidloginemployeeunkid = mintVoidlogingemployeeunkid

    '            'If objExpense.InsertUpdateDelete_Expense(objDataOperation) = False Then
    '            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            '    Throw exForce
    '            'End If


    '            'Pinkal (06-Mar-2014) -- Start
    '            'Enhancement : TRA Changes

    '            'objExpense._Formunkid = mintFormunkid
    '            'objExpense._dtExpense = mdtExpense
    '            'objExpense._Userunkid = mintUserunkid
    '            'objExpense._Loginemployeeunkid = mintLoginEmployeeunkid
    '            'objExpense._Voidloginemployeeunkid = mintVoidlogingemployeeunkid
    '            'If objExpense.InsertUpdateDelete_Expense(objDataOperation) = False Then
    '            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            '    Throw exForce
    '            'End If


    '            If objClaimRequestMst IsNot Nothing Then

    '                If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
    '                    blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
    '                End If

    '                objClaimRequestMst._LeaveApproverForLeaveType = CBool(blnLeaveApproverForLeaveType)
    '                objClaimRequestMst._LeaveTypeId = mintLeavetypeunkid
    '                objClaimRequestMst._Referenceunkid = mintFormunkid

    '                If mblnPaymentApprovalwithLeaveApproval Then
    '                    objClaimRequestMst._FromModuleId = enExpFromModuleID.FROM_LEAVE
    '                Else
    '                    objClaimRequestMst._FromModuleId = enExpFromModuleID.FROM_EXPENSE
    '                End If

    '                If objClaimRequestMst._Crmasterunkid > 0 Then
    '                    If objClaimRequestMst.Update(mdtExpense, objDataOperation, mblnPaymentApprovalwithLeaveApproval) = False Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                Else

    '                    If intCompanyID <= 0 Then
    '                        intCompanyID = Company._Object._Companyunkid
    '                    End If

    '                    If objClaimRequestMst.Insert(mdtExpense, intCompanyID, objDataOperation, mblnPaymentApprovalwithLeaveApproval) = False Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If

    '                End If

    '            End If

    '            'Pinkal (06-Mar-2014) -- End




    '            'S.SANDEEP [ 26 SEPT 2013 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            'Dim objapprover As New clsleaveapprover_master


    '            ''Pinkal (06-Feb-2012) -- Start
    '            ''Enhancement : TRA Changes


    '            ''Pinkal (25-APR-2012) -- Start
    '            ''Enhancement : TRA Changes

    '            ''Dim dtList As DataTable = Nothing
    '            ''If ConfigParameter._Object._IsLeaveApprover_ForLeaveType Then
    '            ''    dtList = objapprover.GetEmployeeApprover(-1, mintEmployeeunkid, mintLeavetypeunkid)
    '            ''Else
    '            ''    dtList = objapprover.GetEmployeeApprover(-1, mintEmployeeunkid, -1)
    '            ''End If

    '            'If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
    '            '    blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
    '            'End If

    '            'Dim dtList As DataTable = Nothing
    '            'If CBool(blnLeaveApproverForLeaveType) = True Then
    '            '    dtList = objapprover.GetEmployeeApprover(-1, mintEmployeeunkid, -1, mintLeavetypeunkid, blnLeaveApproverForLeaveType)
    '            'Else
    '            '    dtList = objapprover.GetEmployeeApprover(-1, mintEmployeeunkid, -1)
    '            'End If

    '            ''Pinkal (25-APR-2012) -- End

    '            ''Pinkal (06-Feb-2012) -- End

    '            'If dtList.Rows.Count > 0 Then
    '            '    Dim objpending As New clspendingleave_Tran
    '            '    For Each dr As DataRow In dtList.Rows
    '            '        objpending._Approvaldate = mdtApplydate
    '            '        objpending._Startdate = mdtStartdate
    '            '        objpending._Enddate = mdtReturndate
    '            '        objpending._Approverunkid = CInt(dr("employeeunkid"))


    '            '        'Pinkal (03-APR-2012) -- Start
    '            '        'Enhancement : TRA Changes
    '            '        objpending._Approvertranunkid = CInt(dr("approverunkid"))
    '            '        'Pinkal (03-APR-2012) -- End

    '            '        objpending._Employeeunkid = mintEmployeeunkid
    '            '        objpending._Formunkid = mintFormunkid
    '            '        objpending._Statusunkid = mintStatusunkid
    '            '        objpending._Userunkid = mintUserunkid

    '            '        'Pinkal (15-Jul-2013) -- Start
    '            '        'Enhancement : TRA Changes

    '            '        If dtFraction IsNot Nothing AndAlso dtFraction.Rows.Count > 0 Then
    '            '            objpending._DayFraction = dtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'")
    '            '        Else
    '            '            objpending._DayFraction = 0
    '            '        End If

    '            '        'Pinkal (15-Jul-2013) -- End


    '            '        Dim blnFlag As Boolean = objpending.Update(objDataOperation)

    '            '        If blnFlag = False And objpending._Message <> "" Then
    '            '            Throw New Exception(objpending._Message)
    '            '            Exit For
    '            '        End If
    '            '    Next

    '            'End If
    '            If mdtReturndate <> Nothing Then
    '                Dim objapprover As New clsleaveapprover_master

    '                If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
    '                    blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
    '                End If

    '                Dim dtList As DataTable = Nothing
    '                If CBool(blnLeaveApproverForLeaveType) = True Then
    '                    dtList = objapprover.GetEmployeeApprover(-1, mintEmployeeunkid, -1, mintLeavetypeunkid, blnLeaveApproverForLeaveType)
    '                Else
    '                    dtList = objapprover.GetEmployeeApprover(-1, mintEmployeeunkid, -1)
    '                End If

    '                If dtList.Rows.Count > 0 Then
    '                    'S.SANDEEP [ 26 SEPT 2013 ] -- START
    '                    'ENHANCEMENT : TRA CHANGES
    '                    Dim iPendingId As Integer = 0
    '                    'S.SANDEEP [ 26 SEPT 2013 ] -- END
    '                    Dim objpending As New clspendingleave_Tran
    '                    For Each dr As DataRow In dtList.Rows
    '                        'S.SANDEEP [ 26 SEPT 2013 ] -- START
    '                        'ENHANCEMENT : TRA CHANGES
    '                        iPendingId = objpending.IsExist(objDataOperation, mintEmployeeunkid, mintFormunkid, CInt(dr.Item("employeeunkid")))
    '                        If iPendingId > 0 Then
    '                            objpending._Pendingleavetranunkid = iPendingId
    '                        End If
    '                        'S.SANDEEP [ 26 SEPT 2013 ] -- END
    '                        objpending._Approvaldate = mdtApplydate
    '                        objpending._Startdate = mdtStartdate
    '                        objpending._Enddate = mdtReturndate
    '                        objpending._Approverunkid = CInt(dr("employeeunkid"))
    '                        objpending._Approvertranunkid = CInt(dr("approverunkid"))
    '                        objpending._Employeeunkid = mintEmployeeunkid
    '                        objpending._Formunkid = mintFormunkid
    '                        objpending._Statusunkid = mintStatusunkid
    '                        objpending._Userunkid = mintUserunkid

    '                        If dtFraction IsNot Nothing AndAlso dtFraction.Rows.Count > 0 Then
    '                            objpending._DayFraction = dtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'")
    '                        Else
    '                            objpending._DayFraction = 0
    '                        End If

    '                        'S.SANDEEP [ 26 SEPT 2013 ] -- START
    '                        'ENHANCEMENT : TRA CHANGES
    '                        'Dim blnFlag As Boolean = objpending.Update(objDataOperation)
    '                        Dim blnFlag As Boolean = False
    '                        If iPendingId > 0 Then
    '                            blnFlag = objpending.Update(objDataOperation)
    '                        Else
    '                            blnFlag = objpending.Insert(objDataOperation)
    '                        End If
    '                        'S.SANDEEP [ 26 SEPT 2013 ] -- END

    '                        If blnFlag = False And objpending._Message <> "" Then
    '                            Throw New Exception(objpending._Message)
    '                            Exit For
    '                        End If
    '                    Next
    '                End If
    '            End If
    '            'S.SANDEEP [ 26 SEPT 2013 ] -- END

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If



    '            'Pinkal (07-May-2015) -- Start
    '            'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.

    '            If mdtAttachDocument IsNot Nothing Then
    '                Dim drRow() As DataRow = mdtAttachDocument.Select("AUD = 'A'")
    '                If drRow.Length > 0 Then
    '                    For i As Integer = 0 To drRow.Length - 1
    '                        drRow(i)("transactionunkid") = mintFormunkid
    '                        drRow(i).AcceptChanges()
    '                    Next
    '                End If
    '                Dim objDocument As New clsScan_Attach_Documents
    '                objDocument._Datatable = mdtAttachDocument
    '                If objDocument.InsertUpdateDelete_Documents(objDataOperation) = False Then
    '                    Throw New Exception(objDocument._Message)
    '                End If
    '                objDocument = Nothing
    '            End If

    '            'Pinkal (07-May-2015) -- End



    '            objDataOperation.ReleaseTransaction(True)
    '            Return True
    '        Catch ex As Exception
    '            objDataOperation.ReleaseTransaction(False)
    '            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '            Return False
    '        Finally
    '            exForce = Nothing
    '            If dsList IsNot Nothing Then dsList.Dispose()
    '            objDataOperation = Nothing
    '        End Try
    '    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                      , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                      , ByVal mdtEmployeeAsOnDate As Date _
                                      , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                      , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                      , Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intApproverunkid As Integer = -1 _
                                      , Optional ByVal blnApplyUserAccessFilter As Boolean = True, Optional ByVal intLoginEmployeeID As Integer = -1 _
                                      , Optional ByVal mstrFilterString As String = "" _
                                      , Optional ByVal blnIsExternalApprover As Boolean = False) As DataSet


        'Pinkal (01-Mar-2016) -- Implementing External Approver in Claim Request & Leave Module.[Optional ByVal blnIsExternalApprover As Boolean = False]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsOnDate, mdtEmployeeAsOnDate, , , xDatabaseName)

            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEmployeeAsOnDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            If blnIsExternalApprover = False Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEmployeeAsOnDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            'Pinkal (01-Mar-2016) -- End

            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsOnDate, xDatabaseName)


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

            'strQ = "SELECT " & _
            '         "  lvleaveform.formunkid " & _
            '         ", isnull(lvleaveIssue_master.leaveissueunkid,-1) as leaveissueunkid " & _
            '         ", formno " & _
            '         ", lvleaveform.employeeunkid " & _
            '         ", h1.employeecode " & _
            '         ", isnull(h1.firstname,'') + ' ' + isnull(h1.surname,'') as employeename" & _
            '         ", lvleaveform.leavetypeunkid " & _
            '         ", lvLeaveType_master.leavename " & _
            '         ", ISNULL(lvLeaveType_master.skipapproverflow,0) AS skipapproverflow " & _
            '         ", convert(char(8),applydate,112) as  applydate" & _
            '         ", convert(char(8),lvleaveform.startdate,112) as startdate " & _
            '         ", convert(char(8),lvleaveform.returndate,112) as returndate " & _
            '         ", DATEPART(MM,lvleaveform.startdate) as startmonth  " & _
            '         ", DATEPART(MM,lvleaveform.returndate) as endmonth  " & _
            '        ", ISNULL((SELECT SUM(dayfraction) FROM lvleaveday_fraction WHERE lvleaveday_fraction.formunkid = lvleaveform.formunkid  " & _
            '               "    AND lvleaveform.isvoid = 0 AND lvleaveday_fraction.isvoid = 0 AND leavedate BETWEEN lvleaveform.startdate AND ISNULL(lvleaveform.returndate,lvleaveform.startdate) AND ISNULL(lvleaveday_fraction.approverunkid,0) <=0),0.00) days " & _
            '         ", addressonleave " & _
            '         ", lvleaveform.statusunkid " & _
            '         ", case when lvleaveform.statusunkid = 1 then @Approve " & _
            '         "         when lvleaveform.statusunkid = 2 then @Pending  " & _
            '         "         when lvleaveform.statusunkid = 3 then @Reject  " & _
            '         "         when lvleaveform.statusunkid = 4 then @ReSchedule " & _
            '         "         when lvleaveform.statusunkid = 6 then @Cancel " & _
            '         "         when lvleaveform.statusunkid = 7 then @Issued " & _
            '         "        end as status" & _
            '         ", lvleaveform.remark " & _
            '         ", lvleaveform.isvoid " & _
            '         ", lvleaveform.voiddatetime " & _
            '         ", lvleaveform.userunkid " & _
            '         ", lvleaveform.voiduserunkid " & _
            '         ", lvleaveform.voidreason " & _
            '         ", lvleaveform.loginemployeeunkid " & _
            '         ", ISNULL(lvleaveform.voidloginemployeeunkid,0) voidloginemployeeunkid " & _
            '        ", lvLeaveType_master.color " & _
            '        ", convert(char(8),lvleaveform.approve_stdate,112) As Approved_StartDate " & _
            '        ", convert(char(8),lvleaveform.approve_eddate,112) As Approved_EndDate " & _
            '                 ", lvleaveform.approve_days As Approved_Days " & _
            '         ", ISNULL(lvleaveform.relieverempunkid,0) As relieverempunkid " & _
            '                 ",ISNULL(Alloc.sectiongroupunkid,0) AS sectiongroupunkid " & _
            '                 ",ISNULL(Alloc.unitgroupunkid,0) AS unitgroupunkid " & _
            '                 ",ISNULL(Alloc.teamunkid,0) AS teamunkid " & _
            '                 ",ISNULL(Alloc.stationunkid,0) AS stationunkid " & _
            '                 ",ISNULL(Alloc.deptgroupunkid,0) AS deptgroupunkid " & _
            '                 ",ISNULL(Alloc.departmentunkid,0) AS departmentunkid " & _
            '                 ",ISNULL(Alloc.sectionunkid,0) AS sectionunkid " & _
            '                 ",ISNULL(Alloc.unitunkid,0) AS unitunkid " & _
            '                 ",ISNULL(Alloc.classgroupunkid,0) AS classgroupunkid " & _
            '                 ",ISNULL(Alloc.classunkid,0) AS classunkid " & _
            '                 ",ISNULL(Jobs.jobunkid,0) AS jobunkid " & _
            '                 ",ISNULL(Jobs.jobgroupunkid,0) AS jobgroupunkid " & _
            '                 ",ISNULL(Grds.gradegroupunkid,0) AS gradegroupunkid " & _
            '                 ",ISNULL(Grds.gradeunkid,0) AS gradeunkid " & _
            '                 ",ISNULL(Grds.gradelevelunkid,0) AS gradelevelunkid " & _
            '                   ",ISNULL(lvleaveform.istnaperiodlinked,0) AS istnaperiodlinked " & _
            '                 " FROM lvleaveform " & _
            '         " LEFT JOIN hremployee_master h1 on h1.employeeunkid = lvleaveform.employeeunkid " & _
            '         " LEFT JOIN lvLeaveType_master on lvLeaveType_master.leavetypeunkid = lvleaveform.leavetypeunkid " & _
            '                 " LEFT JOIN lvleaveIssue_master on lvleaveIssue_master.formunkid = lvleaveform.formunkid and lvleaveIssue_master.isvoid = 0 " & _
            '                 " LEFT JOIN " & _
            '                 "( " & _
            '                 "    SELECT " & _
            '                 "         stationunkid " & _
            '                 "        ,deptgroupunkid " & _
            '                 "        ,departmentunkid " & _
            '                 "        ,sectiongroupunkid " & _
            '                 "        ,sectionunkid " & _
            '                 "        ,unitgroupunkid " & _
            '                 "        ,unitunkid " & _
            '                 "        ,teamunkid " & _
            '                 "        ,classgroupunkid " & _
            '                 "        ,classunkid " & _
            '                 "        ,employeeunkid " & _
            '                 "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '                 "    FROM hremployee_transfer_tran " & _
            '                 "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsOnDate) & "' " & _
            '                 " ) AS Alloc ON Alloc.employeeunkid = h1.employeeunkid AND Alloc.rno = 1 " & _
            '                 " LEFT JOIN " & _
            '                 "( " & _
            '                 "    SELECT " & _
            '                 "         jobunkid " & _
            '                 "        ,jobgroupunkid " & _
            '                 "        ,employeeunkid " & _
            '                 "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '                 "    FROM hremployee_categorization_tran " & _
            '                 "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsOnDate) & "' " & _
            '                 " ) AS Jobs ON Jobs.employeeunkid = h1.employeeunkid AND Jobs.rno = 1 " & _
            '                 " LEFT JOIN " & _
            '                 "( " & _
            '                 "    SELECT " & _
            '                 "         gradegroupunkid " & _
            '                 "        ,gradeunkid " & _
            '                 "        ,gradelevelunkid " & _
            '                 "        ,employeeunkid " & _
            '                 "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
            '                 "    FROM prsalaryincrement_tran " & _
            '                 "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsOnDate) & "' " & _
            '                 ") AS Grds ON Grds.employeeunkid = h1.employeeunkid AND Grds.rno = 1 "


            strQ = "SELECT " & _
             "  lvleaveform.formunkid " & _
             ", isnull(lvleaveIssue_master.leaveissueunkid,-1) as leaveissueunkid " & _
             ", formno " & _
             ", lvleaveform.employeeunkid " & _
             ", h1.employeecode " & _
             ", isnull(h1.firstname,'') + ' ' + isnull(h1.surname,'') as employeename" & _
             ", lvleaveform.leavetypeunkid " & _
             ", lvLeaveType_master.leavename " & _
             ", ISNULL(lvLeaveType_master.skipapproverflow,0) AS skipapproverflow " & _
             ", convert(char(8),applydate,112) as  applydate" & _
             ", convert(char(8),lvleaveform.startdate,112) as startdate " & _
             ", convert(char(8),lvleaveform.returndate,112) as returndate " & _
             ", DATEPART(MM,lvleaveform.startdate) as startmonth  " & _
             ", DATEPART(MM,lvleaveform.returndate) as endmonth  " & _
                       ", ISNULL(dayfraction.days, 0.00) AS days " & _
             ", addressonleave " & _
             ", lvleaveform.statusunkid " & _
             ", case when lvleaveform.statusunkid = 1 then @Approve " & _
             "         when lvleaveform.statusunkid = 2 then @Pending  " & _
             "         when lvleaveform.statusunkid = 3 then @Reject  " & _
             "         when lvleaveform.statusunkid = 4 then @ReSchedule " & _
             "         when lvleaveform.statusunkid = 6 then @Cancel " & _
             "         when lvleaveform.statusunkid = 7 then @Issued " & _
             "        end as status" & _
             ", lvleaveform.remark " & _
             ", lvleaveform.isvoid " & _
             ", lvleaveform.voiddatetime " & _
             ", lvleaveform.userunkid " & _
             ", lvleaveform.voiduserunkid " & _
             ", lvleaveform.voidreason " & _
             ", lvleaveform.loginemployeeunkid " & _
             ", ISNULL(lvleaveform.voidloginemployeeunkid,0) voidloginemployeeunkid " & _
            ", lvLeaveType_master.color " & _
            ", convert(char(8),lvleaveform.approve_stdate,112) As Approved_StartDate " & _
            ", convert(char(8),lvleaveform.approve_eddate,112) As Approved_EndDate " & _
                     ", lvleaveform.approve_days As Approved_Days " & _
             ", ISNULL(lvleaveform.relieverempunkid,0) As relieverempunkid " & _
                     ",ISNULL(Alloc.sectiongroupunkid,0) AS sectiongroupunkid " & _
                     ",ISNULL(Alloc.unitgroupunkid,0) AS unitgroupunkid " & _
                     ",ISNULL(Alloc.teamunkid,0) AS teamunkid " & _
                     ",ISNULL(Alloc.stationunkid,0) AS stationunkid " & _
                     ",ISNULL(Alloc.deptgroupunkid,0) AS deptgroupunkid " & _
                     ",ISNULL(Alloc.departmentunkid,0) AS departmentunkid " & _
                     ",ISNULL(Alloc.sectionunkid,0) AS sectionunkid " & _
                     ",ISNULL(Alloc.unitunkid,0) AS unitunkid " & _
                     ",ISNULL(Alloc.classgroupunkid,0) AS classgroupunkid " & _
                     ",ISNULL(Alloc.classunkid,0) AS classunkid " & _
                     ",ISNULL(Jobs.jobunkid,0) AS jobunkid " & _
                     ",ISNULL(Jobs.jobgroupunkid,0) AS jobgroupunkid " & _
                     ",ISNULL(Grds.gradegroupunkid,0) AS gradegroupunkid " & _
                     ",ISNULL(Grds.gradeunkid,0) AS gradeunkid " & _
                     ",ISNULL(Grds.gradelevelunkid,0) AS gradelevelunkid " & _
                       ",ISNULL(lvleaveform.istnaperiodlinked,0) AS istnaperiodlinked " & _
                     " FROM lvleaveform " & _
                         " LEFT JOIN ( " & _
                         "                      SELECT " & _
                         "                               lvleaveday_fraction.formunkid " & _
                         "                             ,ISNULL(SUM(dayfraction), 0.00) AS days " & _
                         "                        FROM lvleaveday_fraction " & _
                         "                        WHERE isvoid = 0  AND ISNULL(lvleaveday_fraction.approverunkid, 0) <= 0 " & _
                         "                      GROUP BY lvleaveday_fraction.formunkid " & _
                         "                 ) AS dayfraction	ON dayfraction.formunkid = lvleaveform.formunkid " & _
                         " LEFT JOIN hremployee_master h1 on h1.employeeunkid = lvleaveform.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry.Replace("hremployee_master", "h1") & " "
            End If

            strQ &= " LEFT JOIN lvLeaveType_master on lvLeaveType_master.leavetypeunkid = lvleaveform.leavetypeunkid " & _
                     " LEFT JOIN lvleaveIssue_master on lvleaveIssue_master.formunkid = lvleaveform.formunkid and lvleaveIssue_master.isvoid = 0 " & _
                     " LEFT JOIN " & _
                     "( " & _
                     "    SELECT " & _
                     "         stationunkid " & _
                     "        ,deptgroupunkid " & _
                     "        ,departmentunkid " & _
                     "        ,sectiongroupunkid " & _
                     "        ,sectionunkid " & _
                     "        ,unitgroupunkid " & _
                     "        ,unitunkid " & _
                     "        ,teamunkid " & _
                     "        ,classgroupunkid " & _
                     "        ,classunkid " & _
                     "        ,employeeunkid " & _
                     "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                     "    FROM hremployee_transfer_tran " & _
                     "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsOnDate) & "' " & _
                     " ) AS Alloc ON Alloc.employeeunkid = h1.employeeunkid AND Alloc.rno = 1 " & _
                     " LEFT JOIN " & _
                     "( " & _
                     "    SELECT " & _
                     "         jobunkid " & _
                     "        ,jobgroupunkid " & _
                     "        ,employeeunkid " & _
                     "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                     "    FROM hremployee_categorization_tran " & _
                     "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsOnDate) & "' " & _
                     " ) AS Jobs ON Jobs.employeeunkid = h1.employeeunkid AND Jobs.rno = 1 " & _
                     " LEFT JOIN " & _
                     "( " & _
                     "    SELECT " & _
                     "         gradegroupunkid " & _
                     "        ,gradeunkid " & _
                     "        ,gradelevelunkid " & _
                     "        ,employeeunkid " & _
                     "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                     "    FROM prsalaryincrement_tran " & _
                     "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsOnDate) & "' " & _
                     ") AS Grds ON Grds.employeeunkid = h1.employeeunkid AND Grds.rno = 1 "


            'Pinkal (11-Sep-2020) -- End


            If intLoginEmployeeID <= 0 AndAlso blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry.Replace("hremployee_master", "h1") & " "
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry.Replace("hremployee_master", "h1") & " "
            End If

            strQ &= " WHERE 1 = 1 "

            If blnOnlyActive Then
                strQ &= " AND lvleaveform.isvoid = 0 "
            End If


            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry.Replace("hremployee_master", "h1") & " "
                End If
            End If

            'strQ &= " AND h1.isapproved = 1 "
            If mstrFilterString.Trim.Length > 0 Then
                strQ &= mstrFilterString
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@ReSchedule", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 113, "Re-Scheduled"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
            objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 276, "Issued"))


            dsList = objDataOperation.ExecQuery(strQ, strTableName)


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveform) </purpose>
    Public Function Insert(ByVal xDatabaseName As String, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                             , ByVal mdtEmployeeAsonDate As Date, ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                             , ByVal dtCurrentDateAndTime As DateTime _
                                                             , ByVal mblnSkipForApproverFlow As Boolean _
                                                             , ByVal xLeaveBalanceSetting As Integer _
                                                             , ByVal xUserModeSettings As String _
                                                             , Optional ByVal dtFraction As DataTable = Nothing _
                                                             , Optional ByVal blnLeaveApproverForLeaveType As String = "" _
                                                             , Optional ByVal blnSkipEmployeeMovementApprovalFlow As Boolean = True _
                                                             , Optional ByVal blnCreateADUserFromEmpMst As Boolean = False _
                                                             ) As Boolean
        'Sohail (21 Oct 2019) - [blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst]
        'Pinkal (01-Oct-2018) --  'Enhancement - Leave Enhancement for NMB.[, ByVal mblnSkipForApproverFlow As Boolean, ByVal xLeaveBalanceSetting As Integer , ByVal xUserModeSettings As String _]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            objDataOperation.BindTransaction()
            'START FOR AUTO INCREMENT NO 

            Dim intFormNoType As Integer = 0
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = IIf(xCompanyUnkid = 0, Company._Object._Companyunkid, xCompanyUnkid)
            intFormNoType = objConfig._LeaveFormNoType

            If intFormNoType = 0 Then
                If isExist(mstrFormno) Then
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Form No is already defined. Please define new Form No.")
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If


            'Pinkal (18-Mar-2021) -- Start 
            'NMB Enhancmenet : AD Enhancement for NMB.

            'If mdtReturndate <> Nothing Then
            '    If isDayExist(False, mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date, mintFormunkid) Then

            '        'Gajanan [20-Dec-2019] -- Start   
            '        'Enhancement:Voltamp Transformers-Oman [0003275] : Unable to apply leave on cancelled days
            '        If IsIssuedDays(mintEmployeeunkid, mdtStartdate, mdtReturndate, objDataOperation) = True Then
            '            mstrMessage = Language.getMessage(mstrModuleName, 3, "This tenure is already defined to another form. Please define new tenure for this form.")
            '            objDataOperation.ReleaseTransaction(False)
            '            Return False
            '            'Pinkal (09-Mar-2020) -- Start
            '            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            '        Else
            '            mstrMessage = Language.getMessage(mstrModuleName, 3, "This tenure is already defined to another form. Please define new tenure for this form.")
            '            objDataOperation.ReleaseTransaction(False)
            '            Return False
            '            'Pinkal (09-Mar-2020) -- End
            '        End If
            '        'Gajanan [20-Dec-2019] -- End


            '    End If

            '    If isDayExist(True, mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date, mintFormunkid) Then
            '        'Gajanan [20-Dec-2019] -- Start   
            '        'Enhancement:Voltamp Transformers-Oman [0003275] : Unable to apply leave on cancelled days
            '        If IsIssuedDays(mintEmployeeunkid, mdtStartdate, mdtReturndate, objDataOperation) = True Then
            '            mstrMessage = Language.getMessage(mstrModuleName, 26, "This tenure is already approved by approver.Please define new tenure for this form.")
            '            objDataOperation.ReleaseTransaction(False)
            '            Return False
            '            'Pinkal (09-Mar-2020) -- Start
            '            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            '        Else
            '            mstrMessage = Language.getMessage(mstrModuleName, 26, "This tenure is already approved by approver.Please define new tenure for this form.")
            '            objDataOperation.ReleaseTransaction(False)
            '            Return False
            '            'Pinkal (09-Mar-2020) -- End
            '        End If
            '        'Gajanan [20-Dec-2019] -- End
            '    End If
            'End If


            If mdtReturndate <> Nothing Then
                    If IsIssuedDays(mintEmployeeunkid, mdtStartdate, mdtReturndate, objDataOperation) = True Then
                    mstrMessage = Language.getMessage(mstrModuleName, 3, "This tenure is already defined to another form. Please define new tenure for this form.")
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                    Else
                    'If IsCancelledDays(mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date, Nothing) = False Then
                    'If isDayExist(False, mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date, -1) Then
                    '    mstrMessage = Language.getMessage(mstrModuleName, 3, "This tenure is already defined to another form. Please define new tenure for this form.")
                    '    objDataOperation.ReleaseTransaction(False)
                    '    Return False
                    'ElseIf isDayExist(True, mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date, -1) Then
                    '    mstrMessage = Language.getMessage(mstrModuleName, 3, "This tenure is already defined to another form. Please define new tenure for this form.")
                    '    objDataOperation.ReleaseTransaction(False)
                    '    Return False
                    'End If
                    'Else
                        If isDayExist(False, mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date, -1, "lvleaveform.statusunkid = 2") Then
                            mstrMessage = Language.getMessage(mstrModuleName, 3, "This tenure is already defined to another form. Please define new tenure for this form.")
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                        ElseIf isDayExist(True, mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date, -1, "lvleaveform.statusunkid = 1") Then
                            mstrMessage = Language.getMessage(mstrModuleName, 3, "This tenure is already defined to another form. Please define new tenure for this form.")
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                End If
                    'End If
            End If
            End If
            'Pinkal (18-Mar-2021) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormno.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
            objDataOperation.AddParameter("@applydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplydate)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            If mdtReturndate <> Nothing Then
                objDataOperation.AddParameter("@returndate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReturndate)
            Else
                objDataOperation.AddParameter("@returndate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@addressonleave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddressonleave.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@relieverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelieverEmpunkid.ToString)
            'Pinkal (01-Oct-2018) -- End


            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            objDataOperation.AddParameter("@istnaperiodlinked", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnConsiderLeaveOnTnAPeriod)
            'Pinkal (01-Jan-2019) -- End


            strQ = "INSERT INTO lvleaveform ( " & _
              "  formno " & _
              ", employeeunkid " & _
              ", leavetypeunkid " & _
              ", applydate " & _
              ", startdate " & _
              ", returndate " & _
              ", addressonleave " & _
              ", remark " & _
              ", statusunkid" & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", voiduserunkid" & _
              ", voidreason " & _
                      ", relieverempunkid " & _
                          ", istnaperiodlinked " & _
            ") VALUES (" & _
              "  @formno " & _
              ", @employeeunkid " & _
              ", @leavetypeunkid " & _
              ", @applydate " & _
              ", @startdate " & _
              ", @returndate " & _
              ", @addressonleave " & _
              ", @remark " & _
              ", @statusunkid " & _
              ", @isvoid " & _
              ", @voiddatetime " & _
              ", @userunkid " & _
              ", @loginemployeeunkid " & _
              ", @voiduserunkid" & _
              ", @voidreason " & _
                      ", @relieverempunkid " & _
                          ", @istnaperiodlinked " & _
            "); SELECT @@identity"

            'Pinkal (01-Jan-2019) --  'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[", @istnaperiodlinked " & _]

            'Pinkal (01-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB. [relieverempunkid] 

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintFormunkid = dsList.Tables(0).Rows(0).Item(0)
            If intFormNoType = 1 Then
                If Set_AutoNumber(objDataOperation, mintFormunkid, "lvleaveform", "formno", "formunkid", "NextLeaveFormNo", objConfig._LeaveFormNoPrifix, objConfig._Companyunkid) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                'S.SANDEEP [18-APR-2019] -- START

                'Pinkal (03-May-2019) -- Start
                'Enhancement - Working on Leave UAT Changes for NMB.
                'If Get_Saved_Number(objDataOperation, mintEmployeeunkid, "lvleaveform", "formno", "formunkid", mstrFormno) = False Then
                If Get_Saved_Number(objDataOperation, mintFormunkid, "lvleaveform", "formno", "formunkid", mstrFormno) = False Then
                    'Pinkal (03-May-2019) -- End
                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                'S.SANDEEP [18-APR-2019] -- END
            End If

            objFraction._Formunkid = mintFormunkid
            objFraction._dtfraction = dtFraction
            objFraction._Userunkid = mintUserunkid
            objFraction._Loginemployeeunkid = mintLoginEmployeeunkid
            objFraction._Voidloginemployeeunkid = mintVoidlogingemployeeunkid
            If objFraction.InsertUpdateDelete_Fraction(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            'Pinkal (19-Oct-2016) -- Start
            'Enhancement - Solving Bug In Leave & Claim Approver for specific Leave application.


            'If objClaimRequestMst IsNot Nothing AndAlso mdtExpense IsNot Nothing Then

            '    If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
            '        blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
            '    End If

            '    objClaimRequestMst._LeaveApproverForLeaveType = CBool(blnLeaveApproverForLeaveType)
            '    objClaimRequestMst._LeaveTypeId = mintLeavetypeunkid
            '    objClaimRequestMst._Referenceunkid = mintFormunkid
            '    objClaimRequestMst._FromModuleId = enExpFromModuleID.FROM_LEAVE

            '    If objClaimRequestMst.Insert(xDatabaseName, xYearUnkid, xCompanyUnkid _
            '                                            , mdtEmployeeAsonDate, xIncludeIn_ActiveEmployee, mdtExpense _
            '                                 , dtCurrentDateAndTime,mdtClaimAttchment, objDataOperation _
            '                                 , mblnPaymentApprovalwithLeaveApproval) = False Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If


            If mdtReturndate <> Nothing Then

                'Pinkal (01-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.

                If mblnSkipForApproverFlow = False Then

                Dim objapprover As New clsleaveapprover_master
                Dim dtList As DataTable = Nothing

                If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
                    blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
                End If

                If CBool(blnLeaveApproverForLeaveType) = True Then
                    dtList = objapprover.GetEmployeeApprover(xDatabaseName, xYearUnkid, xCompanyUnkid, mdtEmployeeAsonDate _
                                                                                 , xIncludeIn_ActiveEmployee, -1, mintEmployeeunkid, -1, mintLeavetypeunkid, blnLeaveApproverForLeaveType, objDataOperation)
                Else
                    dtList = objapprover.GetEmployeeApprover(xDatabaseName, xYearUnkid, xCompanyUnkid, mdtEmployeeAsonDate _
                                                                                 , xIncludeIn_ActiveEmployee, -1, mintEmployeeunkid, -1, -1, blnLeaveApproverForLeaveType, objDataOperation)
                End If


                If dtList.Rows.Count > 0 Then
                    Dim objpending As New clspendingleave_Tran
                    For Each dr As DataRow In dtList.Rows
                        objpending._Approvaldate = mdtApplydate
                        objpending._Startdate = mdtStartdate
                        objpending._Enddate = mdtReturndate
                        objpending._Approverunkid = CInt(dr("employeeunkid"))
                        objpending._Approvertranunkid = CInt(dr("approverunkid"))
                        objpending._Employeeunkid = mintEmployeeunkid
                        objpending._Formunkid = mintFormunkid
                        objpending._Statusunkid = mintStatusunkid
                        objpending._Userunkid = mintUserunkid

                        Dim intMinPriority As Integer = CInt(dtList.Compute("MIN(priority)", "1=1"))

                        If intMinPriority = CInt(dr("priority")) Then
                            objpending._VisiblelId = mintStatusunkid
                                'Pinkal (01-Apr-2019) -- Start
                                'Enhancement - Working on Leave Changes for NMB.
                                If objConfig._LeaveApplicationEscalationSettings > 0 Then
                                    objpending._EscalationDate = mdtApplydate.AddDays(CInt(dr("escalation_days"))).AddDays(1).Date
                                Else
                                    objpending._EscalationDate = Nothing
                                End If
                                'Pinkal (01-Apr-2019) -- End
                        Else
                            objpending._VisiblelId = -1
                                'Pinkal (01-Apr-2019) -- Start
                                'Enhancement - Working on Leave Changes for NMB.
                                objpending._EscalationDate = Nothing
                                'Pinkal (01-Apr-2019) -- End
                        End If

                        If dtFraction IsNot Nothing AndAlso dtFraction.Rows.Count > 0 Then
                            objpending._DayFraction = dtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'")
                        Else
                            objpending._DayFraction = 0
                        End If

                            'Pinkal (01-Oct-2018) -- Start
                            'Enhancement - Leave Enhancement for NMB.
                            If mintRelieverEmpunkid > 0 Then objpending._RelieverEmpId = mintRelieverEmpunkid
                            'Pinkal (01-Oct-2018) -- End

                            Dim blnFlag As Boolean = objpending.Insert(objDataOperation)

                            If blnFlag = False And objpending._Message <> "" Then
                                Throw New Exception(objpending._Message)
                                Exit For
                            End If
                        Next

                        If objClaimRequestMst IsNot Nothing AndAlso mdtExpense IsNot Nothing Then

                            If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
                                blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
                            End If

                            objClaimRequestMst._LeaveApproverForLeaveType = CBool(blnLeaveApproverForLeaveType)
                            objClaimRequestMst._LeaveTypeId = mintLeavetypeunkid
                            objClaimRequestMst._Referenceunkid = mintFormunkid
                            objClaimRequestMst._FromModuleId = enExpFromModuleID.FROM_LEAVE

                            objClaimRequestMst._dtLeaveApprover = dtList

                            If objClaimRequestMst.Insert(xDatabaseName, xYearUnkid, xCompanyUnkid _
                                                                    , mdtEmployeeAsonDate, xIncludeIn_ActiveEmployee, mdtExpense _
                                                         , dtCurrentDateAndTime, mdtClaimAttchment, objDataOperation _
                                                         , mblnPaymentApprovalwithLeaveApproval) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        End If 'If objClaimRequestMst IsNot Nothing AndAlso mdtExpense IsNot Nothing Then

                    End If  'If dtList.Rows.Count > 0 Then

                End If  ' If mblnSkipForApproverFlow = False Then

                'Pinkal (01-Oct-2018) -- End

            End If  'If mdtReturndate <> Nothing Then

            If mdtAttachDocument IsNot Nothing AndAlso mdtAttachDocument.Rows.Count > 0 Then
                Dim drRow() As DataRow = mdtAttachDocument.Select("AUD = 'A'")
                If drRow.Length > 0 Then
                    For i As Integer = 0 To drRow.Length - 1
                        drRow(i)("transactionunkid") = mintFormunkid
                        drRow(i).AcceptChanges()
                    Next
                End If
                Dim objDocument As New clsScan_Attach_Documents
                objDocument._Datatable = mdtAttachDocument
                If objDocument.InsertUpdateDelete_Documents(objDataOperation) = False Then
                    Throw New Exception(objDocument._Message)
                End If
                objDocument = Nothing
            End If


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            If mblnSkipForApproverFlow AndAlso mdtReturndate <> Nothing Then   'SKIP APPROVER FLOW AS PER LEAVE TYPE SETTINGS.
                Dim mdecTotalIssue As Decimal = 0
                Dim mdblLeaveAmout As Decimal = 0
                Dim arGetLeaveDate As ArrayList = Nothing
                Dim arGetdate() As String = Nothing
                Dim objleaveIssue As New clsleaveissue_master

                Dim objLeaveType As New clsleavetype_master
                objLeaveType._DoOperation = objDataOperation
                objLeaveType._Leavetypeunkid = mintLeavetypeunkid


                If objLeaveType._IsPaid Then
                    Dim objAcccure As New clsleavebalance_tran
                    Dim dsAccrueList As DataSet = Nothing
                    Dim mstrFilter As String = "lvleavebalance_tran.yearunkid = " & xYearUnkid & " AND lvleavebalance_tran.leavetypeunkid = " & mintLeavetypeunkid


                    If xLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        dsAccrueList = objAcccure.GetList("List", xDatabaseName, mintUserunkid, xYearUnkid _
                                                               , xCompanyUnkid, eZeeDate.convertDate(mdtEmployeeAsonDate.Date), xUserModeSettings _
                                                               , True, xIncludeIn_ActiveEmployee, True, True, False _
                                                               , mintEmployeeunkid, False, False, False, mstrFilter, objDataOperation, False)

                    ElseIf xLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        dsAccrueList = objAcccure.GetList("List", xDatabaseName, mintUserunkid, xYearUnkid _
                                                               , xCompanyUnkid, eZeeDate.convertDate(mdtEmployeeAsonDate.Date), xUserModeSettings _
                                                               , True, xIncludeIn_ActiveEmployee, True, True, False _
                                                               , mintEmployeeunkid, True, True, False, mstrFilter, objDataOperation, False)
                    End If

                    If dsAccrueList IsNot Nothing Then
                        If dsAccrueList.Tables(0).Rows.Count > 0 Then
                            mdblLeaveAmout = CDec(dsAccrueList.Tables(0).Rows(0)("accrue_amount"))
                        End If
                    End If

                    objAcccure = Nothing
                Else
                    mdblLeaveAmout = objLeaveType._MaxAmount
                End If


                arGetLeaveDate = New ArrayList()
                For i As Integer = 0 To dtFraction.Rows.Count - 1
                    arGetLeaveDate.Add(CDate(dtFraction.Rows(i)("leavedate")).ToShortDateString())
                    mdecTotalIssue += CDec(dtFraction.Rows(i)("dayfraction"))
                Next

                If arGetLeaveDate IsNot Nothing Then
                    arGetdate = CType(arGetLeaveDate.ToArray(GetType(String)), String())
                End If

                objleaveIssue._Formunkid = mintFormunkid
                objleaveIssue._Employeeunkid = mintEmployeeunkid
                objleaveIssue._Leavetypeunkid = mintLeavetypeunkid
                objleaveIssue._StartDate = mdtStartdate.Date
                objleaveIssue._EndDate = mdtReturndate.Date
                objleaveIssue._TotalIssue = mdecTotalIssue
                objleaveIssue._Leaveyearunkid = xYearUnkid
                objleaveIssue._SkipApproverFlow = mblnSkipForApproverFlow
                objleaveIssue._Userunkid = mintUserunkid
                objleaveIssue._WebClientIP = mstrWebClientIP
                objleaveIssue._WebFormName = mstrWebFrmName
                objleaveIssue._WebHostName = mstrWebHostName

                'Pinkal (01-Jan-2019) -- Start
                'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                objleaveIssue._ConsiderLeaveOnTnAPeriod = mblnConsiderLeaveOnTnAPeriod
                'Pinkal (01-Jan-2019) -- End

                'Pinkal (25-Mar-2019) -- Start
                'Enhancement - Working on Leave Changes for NMB.

                'If objleaveIssue.Insert(FinancialYear._Object._DatabaseName, IIf(mintUserunkid <= 0, mintLoginEmployeeunkid, mintUserunkid), xYearUnkid, xCompanyUnkid _
                '                              , eZeeDate.convertDate(mdtEmployeeAsonDate.Date), xUserModeSettings, True, xIncludeIn_ActiveEmployee _
                '                              , arGetdate, objLeaveType._IsPaid, mdblLeaveAmout, xLeaveBalanceSetting, blnLeaveApproverForLeaveType, -1, objDataOperation, True, False) = False Then

                If objleaveIssue.Insert(FinancialYear._Object._DatabaseName, IIf(mintUserunkid <= 0, mintLoginEmployeeunkid, mintUserunkid), xYearUnkid, xCompanyUnkid _
                                              , eZeeDate.convertDate(mdtEmployeeAsonDate.Date), xUserModeSettings, True, xIncludeIn_ActiveEmployee _
                                             , arGetdate, objLeaveType._IsPaid, mdblLeaveAmout, xLeaveBalanceSetting, blnLeaveApproverForLeaveType, -1, objDataOperation, False, False, objLeaveType._Isexempt_from_payroll, blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst) = False Then
                    'Sohail (21 Oct 2019) - [blnExemptFromPayroll, blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst]
                    'Pinkal (25-Mar-2019) -- End

                    If objleaveIssue._Message <> "" Then
                        mstrMessage = objleaveIssue._Message
                        exForce = New Exception(objleaveIssue._Message)
                    Else
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

                objLeaveType = Nothing

                If ApprovedLeaveFormtenure(objDataOperation, mintFormunkid, mdtStartdate.Date, mdtReturndate.Date, mdecTotalIssue, True, mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                UpdateLeaveFormStatus(objDataOperation, mintFormunkid, 7, mintUserunkid)  'UPDATE LEAVE FORM STATUS TO ISSUED.

            End If

            'Pinkal (01-Oct-2018) -- End

            objDataOperation.ReleaseTransaction(True)

            'S.SANDEEP |08-APR-2019| -- START

            'S.SANDEEP |25-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : FlexCube
            'Call BlockEmployeeInCBS(mintEmployeeunkid, mdtStartdate, mdtReturndate, mstrFormno, dtCurrentDateAndTime, xCompanyUnkid, "D", objDataOperation)
            Call BlockEmployeeInCBS(mintEmployeeunkid, mdtStartdate, mdtReturndate, mstrFormno, dtCurrentDateAndTime, xCompanyUnkid, "D", objDataOperation, mintUserunkid, IIf(mstrWebClientIP.Trim.Length > 0, mintEmployeeunkid, 0))
            'S.SANDEEP |25-NOV-2019| -- END


            'S.SANDEEP |08-APR-2019| -- END

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lvleaveform) </purpose>
    Public Function Update(ByVal xDatabaseName As String, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                     , ByVal mdtEmployeeAsonDate As Date, ByVal xIncludeIn_ActiveEmployee As Boolean _
                                     , ByVal mblnIssued As Boolean, ByVal dtFraction As DataTable _
                                     , ByVal dtCurrentDateAndTime As DateTime _
                                     , ByVal mblnSkipForApproverFlow As Boolean _
                                     , ByVal xUserModeSettings As String _
                                     , Optional ByVal blnLeaveApproverForLeaveType As String = "" _
                                     , Optional ByVal intLeaveBalanceSetting As Integer = -1 _
                                     , Optional ByVal blnSkipEmployeeMovementApprovalFlow As Boolean = True _
                                     , Optional ByVal blnCreateADUserFromEmpMst As Boolean = False _
                                     ) As Boolean
        'Sohail (21 Oct 2019) - [blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst]
        'Pinkal (01-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[, ByVal mblnSkipForApproverFlow As Boolean, ByVal xLeaveBalanceSetting As Integer , ByVal xUserModeSettings As String _]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try



            'Pinkal (01-Apr-2019) -- Start
            'Enhancement - Working on Leave Changes for NMB.

            'If intLeaveBalanceSetting <= 0 Then
            '    intLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            'End If

            Dim objConfig As New clsConfigOptions
            intLeaveBalanceSetting = objConfig.GetKeyValue(xCompanyUnkid, "LeaveBalanceSetting")
            Dim xLeaveApplicationEscalationSettings As Integer = 0
            Dim mstrLeaveApplicationEscalationSettings As String = objConfig.GetKeyValue(xCompanyUnkid, "LeaveApplicationEscalationSettings").ToString().Trim()
            If mstrLeaveApplicationEscalationSettings.Trim.Length > 0 Then xLeaveApplicationEscalationSettings = mstrLeaveApplicationEscalationSettings.Trim()
            objConfig = Nothing
            'Pinkal (01-Apr-2019) -- End

            If isExist(mstrFormno, mintFormunkid) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Form No is already defined. Please define new Form No.")
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If


            'Pinkal (18-Mar-2021) -- Start 
            'NMB Enhancmenet : AD Enhancement for NMB.

            'If mdtReturndate <> Nothing Then
            '    If isDayExist(False, mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date, mintFormunkid) Then

            '        'Gajanan [20-Dec-2019] -- Start   
            '        'Enhancement:Voltamp Transformers-Oman [0003275] : Unable to apply leave on cancelled days
            '        If IsIssuedDays(mintEmployeeunkid, mdtStartdate, mdtReturndate, objDataOperation) = True Then
            '        mstrMessage = Language.getMessage(mstrModuleName, 3, "This tenure is already defined to another form. Please define new tenure for this form.")
            '        objDataOperation.ReleaseTransaction(False)
            '        Return False
            '            'Pinkal (09-Mar-2020) -- Start
            '            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            '        Else
            '            mstrMessage = Language.getMessage(mstrModuleName, 3, "This tenure is already defined to another form. Please define new tenure for this form.")
            '            objDataOperation.ReleaseTransaction(False)
            '            Return False
            '            'Pinkal (09-Mar-2020) -- End
            '    End If
            '        'Gajanan [20-Dec-2019] -- End


            '    End If

            '    If isDayExist(True, mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date, mintFormunkid) Then
            '        'Gajanan [20-Dec-2019] -- Start   
            '        'Enhancement:Voltamp Transformers-Oman [0003275] : Unable to apply leave on cancelled days
            '        If IsIssuedDays(mintEmployeeunkid, mdtStartdate, mdtReturndate, objDataOperation) = True Then
            '        mstrMessage = Language.getMessage(mstrModuleName, 26, "This tenure is already approved by approver.Please define new tenure for this form.")
            '        objDataOperation.ReleaseTransaction(False)
            '        Return False
            '            'Pinkal (09-Mar-2020) -- Start
            '            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            '        Else
            '            mstrMessage = Language.getMessage(mstrModuleName, 26, "This tenure is already approved by approver.Please define new tenure for this form.")
            '            objDataOperation.ReleaseTransaction(False)
            '            Return False
            '            'Pinkal (09-Mar-2020) -- End
            '    End If
            '        'Gajanan [20-Dec-2019] -- End
            '    End If
            'End If

            If mdtReturndate <> Nothing Then
                    If IsIssuedDays(mintEmployeeunkid, mdtStartdate, mdtReturndate, objDataOperation) = True Then
                    mstrMessage = Language.getMessage(mstrModuleName, 3, "This tenure is already defined to another form. Please define new tenure for this form.")
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                    Else
                    'If IsCancelledDays(mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date, Nothing) = False Then
                    '    If isDayExist(False, mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date, mintFormunkid) Then
                    '        mstrMessage = Language.getMessage(mstrModuleName, 3, "This tenure is already defined to another form. Please define new tenure for this form.")
                    '        objDataOperation.ReleaseTransaction(False)
                    '        Return False
                    '    ElseIf isDayExist(True, mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date, mintFormunkid) Then
                    '        mstrMessage = Language.getMessage(mstrModuleName, 3, "This tenure is already defined to another form. Please define new tenure for this form.")
                    '        objDataOperation.ReleaseTransaction(False)
                    '        Return False
                    '    End If
                    'Else
                        If isDayExist(False, mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date, mintFormunkid, "lvleaveform.statusunkid = 2") Then
                            mstrMessage = Language.getMessage(mstrModuleName, 3, "This tenure is already defined to another form. Please define new tenure for this form.")
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                        ElseIf isDayExist(True, mintEmployeeunkid, mdtStartdate.Date, mdtReturndate.Date, mintFormunkid, "lvleaveform.statusunkid = 1") Then
                            mstrMessage = Language.getMessage(mstrModuleName, 3, "This tenure is already defined to another form. Please define new tenure for this form.")
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                End If
                    'End If
                End If
            End If

            'Pinkal (18-Mar-2021) -- End

            objDataOperation.BindTransaction()
            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
            objDataOperation.AddParameter("@formno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormno.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
            objDataOperation.AddParameter("@applydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplydate)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            If mdtReturndate <> Nothing Then
                objDataOperation.AddParameter("@returndate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReturndate)
            Else
                objDataOperation.AddParameter("@returndate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@addressonleave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddressonleave.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@relieverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelieverEmpunkid.ToString)
            'Pinkal (01-Oct-2018) -- End

            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            objDataOperation.AddParameter("@istnaperiodlinked", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnConsiderLeaveOnTnAPeriod.ToString)
            'Pinkal (01-Jan-2019) -- End



            strQ = "UPDATE lvleaveform SET " & _
              "  formno = @formno" & _
              ", employeeunkid = @employeeunkid" & _
              ", leavetypeunkid = @leavetypeunkid" & _
              ", applydate = @applydate" & _
              ", startdate = @startdate" & _
              ", returndate = @returndate" & _
              ", addressonleave = @addressonleave" & _
              ", remark = @remark" & _
              ", statusunkid = @statusunkid" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", userunkid = @userunkid" & _
              ", loginemployeeunkid = @loginemployeeunkid" & _
              ", voiduserunkid = @voiduserunkid " & _
              ", voidreason = @voidreason " & _
                      ", relieverempunkid = @relieverempunkid " & _
                         ", istnaperiodlinked = @istnaperiodlinked " & _
                         " WHERE formunkid = @formunkid "


            'Pinkal (01-Jan-2019) -- 'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[", istnaperiodlinked = @istnaperiodlinked " & _]

            'Pinkal (01-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", relieverempunkid = @relieverempunkid " & _]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objFraction._Formunkid = mintFormunkid
            objFraction._dtfraction = dtFraction
            objFraction._Userunkid = mintUserunkid
            objFraction._Loginemployeeunkid = mintLoginEmployeeunkid
            objFraction._Voidloginemployeeunkid = mintVoidlogingemployeeunkid

            Dim dt() As DataRow = objFraction._dtfraction.Select("AUD=''")
            If dt.Length = objFraction._dtfraction.Rows.Count Then
                If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "lvleaveform", mintFormunkid, "formunkid", 2, objDataOperation) Then
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", mintFormunkid, "lvleaveday_fraction", "dayfractionunkid", -1, 2, 0, False, mintUserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

            Else

                objFraction.VoidApproverLeaveDayFraction(objDataOperation, mintFormunkid, mintUserunkid, mintLoginEmployeeunkid)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If objFraction.InsertUpdateDelete_Fraction(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            If mdtReturndate <> Nothing Then

                'Pinkal (01-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                If mblnSkipForApproverFlow = False Then

                    Dim objapprover As New clsleaveapprover_master

                    If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
                        blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
                    End If

                    Dim dtList As DataTable = Nothing
                    If CBool(blnLeaveApproverForLeaveType) = True Then
                        dtList = objapprover.GetEmployeeApprover(xDatabaseName, xYearUnkid, xCompanyUnkid, mdtEmployeeAsonDate _
                                                                 , xIncludeIn_ActiveEmployee, -1, mintEmployeeunkid, -1, mintLeavetypeunkid, blnLeaveApproverForLeaveType, objDataOperation)
                    Else
                        dtList = objapprover.GetEmployeeApprover(xDatabaseName, xYearUnkid, xCompanyUnkid, mdtEmployeeAsonDate _
                                                                                     , xIncludeIn_ActiveEmployee, -1, mintEmployeeunkid, -1, -1, blnLeaveApproverForLeaveType, objDataOperation)
                    End If

                    If dtList.Rows.Count > 0 Then
                        Dim iPendingId As Integer = 0
                        Dim objpending As New clspendingleave_Tran

                        'Pinkal (03-Apr-2017) -- Start
                        'Enhancement - SOLVED BUG FOR FINCA TANZANIA WHEN SICK LEAVE APPLIED END END DATE IS NOT COMPULSORY.
                        Dim mintMinPriority As Integer = dtList.Compute("MIN(priority)", "1=1")
                        'Pinkal (03-Apr-2017) -- End

                        For Each dr As DataRow In dtList.Rows
                            iPendingId = objpending.IsExist(objDataOperation, mintEmployeeunkid, mintFormunkid, CInt(dr.Item("employeeunkid")))
                            If iPendingId > 0 Then
                                objpending._Pendingleavetranunkid = iPendingId
                            End If
                            objpending._Approvaldate = mdtApplydate
                            objpending._Startdate = mdtStartdate
                            objpending._Enddate = mdtReturndate
                            objpending._Approverunkid = CInt(dr("employeeunkid"))
                            objpending._Approvertranunkid = CInt(dr("approverunkid"))
                            objpending._Employeeunkid = mintEmployeeunkid
                            objpending._Formunkid = mintFormunkid
                            objpending._Statusunkid = mintStatusunkid
                            objpending._Userunkid = mintUserunkid


                            If mintMinPriority = CInt(dr("priority")) Then
                                objpending._VisiblelId = 2
                                'Pinkal (01-Apr-2019) -- Start
                                'Enhancement - Working on Leave Changes for NMB.
                                If xLeaveApplicationEscalationSettings > 0 Then
                                    objpending._EscalationDate = mdtApplydate.AddDays(CInt(dr("escalation_days"))).AddDays(1).Date
                                Else
                                    objpending._EscalationDate = Nothing
                                End If
                                'Pinkal (01-Apr-2019) -- End
                            Else
                                objpending._VisiblelId = -1
                                'Pinkal (01-Apr-2019) -- Start
                                'Enhancement - Working on Leave Changes for NMB.
                                objpending._EscalationDate = Nothing
                                'Pinkal (01-Apr-2019) -- End
                            End If

                            If dtFraction IsNot Nothing AndAlso dtFraction.Rows.Count > 0 Then
                                objpending._DayFraction = dtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'")
                            Else
                                objpending._DayFraction = 0
                            End If


                            'Pinkal (01-Oct-2018) -- Start
                            'Enhancement - Leave Enhancement for NMB.
                            If mintRelieverEmpunkid > 0 Then
                                objpending._RelieverEmpId = mintRelieverEmpunkid
                            End If
                            'Pinkal (01-Oct-2018) -- End


                            Dim blnFlag As Boolean = False
                            If iPendingId > 0 Then
                                blnFlag = objpending.Update(objDataOperation)
                            Else
                                blnFlag = objpending.Insert(objDataOperation)
                            End If

                            If blnFlag = False And objpending._Message <> "" Then
                                Throw New Exception(objpending._Message)
                                Exit For
                            End If
                        Next

                        'Pinkal (03-Apr-2017) -- Start
                        'Enhancement - SOLVED BUG FOR FINCA TANZANIA WHEN SICK LEAVE APPLIED END END DATE IS NOT COMPULSORY.
                        'If objClaimRequestMst IsNot Nothing Then
                        If objClaimRequestMst IsNot Nothing AndAlso (mdtExpense IsNot Nothing AndAlso mdtExpense.Rows.Count > 0) Then
                            'Pinkal (03-Apr-2017) -- End
                            If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
                                blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
                            End If

                            objClaimRequestMst._LeaveApproverForLeaveType = CBool(blnLeaveApproverForLeaveType)
                            objClaimRequestMst._LeaveTypeId = mintLeavetypeunkid
                            objClaimRequestMst._Referenceunkid = mintFormunkid


                            'Pinkal (04-Feb-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                            'If mblnPaymentApprovalwithLeaveApproval Then
                            '    objClaimRequestMst._FromModuleId = enExpFromModuleID.FROM_LEAVE
                            '    objClaimRequestMst._dtLeaveApprover = dtList
                            'Else
                            '    objClaimRequestMst._FromModuleId = enExpFromModuleID.FROM_EXPENSE
                            'End If
                            If mblnPaymentApprovalwithLeaveApproval Then
                                objClaimRequestMst._dtLeaveApprover = dtList
                            End If
                            objClaimRequestMst._FromModuleId = enExpFromModuleID.FROM_LEAVE
                            'Pinkal (04-Feb-2019) -- End


                            'Pinkal (20-Nov-2018) -- Start
                            'Enhancement - Working on P2P Integration for NMB.
                            Dim mblnIsClaimFormBudgetMandatory As Boolean = False
                            If mdtExpense IsNot Nothing AndAlso mdtExpense.Rows.Count > 0 Then
                                If mdtExpense.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isbudgetmandatory") = True AndAlso x.Field(Of String)("AUD") <> "D").Count > 0 Then
                                    mblnIsClaimFormBudgetMandatory = True
                                End If
                            End If
                            'Pinkal (20-Nov-2018) -- End


                            If objClaimRequestMst._Crmasterunkid > 0 Then


                                If objClaimRequestMst.Update(xDatabaseName, xYearUnkid, xCompanyUnkid _
                                                                         , mdtEmployeeAsonDate, xIncludeIn_ActiveEmployee, mdtExpense, dtCurrentDateAndTime _
                                                                         , mdtClaimAttchment, objDataOperation, mblnPaymentApprovalwithLeaveApproval) = False Then

                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce

                                End If

                            Else

                                If xCompanyUnkid <= 0 Then
                                    xCompanyUnkid = Company._Object._Companyunkid
                                End If


                                If objClaimRequestMst.Insert(xDatabaseName, xYearUnkid, xCompanyUnkid, mdtEmployeeAsonDate _
                                                                        , xIncludeIn_ActiveEmployee, mdtExpense, dtCurrentDateAndTime, mdtClaimAttchment, objDataOperation _
                                                                        , mblnPaymentApprovalwithLeaveApproval) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce

                                End If  'objClaimRequestMst.Insert

                            End If  '   If objClaimRequestMst._Crmasterunkid > 0 Then

                        End If  'If objClaimRequestMst IsNot Nothing AndAlso (mdtExpense IsNot Nothing AndAlso mdtExpense.Rows.Count > 0) Then

                    End If  'If dtList.Rows.Count > 0 Then

                End If  ' If mblnSkipForApproverFlow = False Then

            End If '  If mdtReturndate <> Nothing Then


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtAttachDocument IsNot Nothing AndAlso mdtAttachDocument.Rows.Count > 0 Then
                Dim drRow() As DataRow = mdtAttachDocument.Select("AUD = 'A'")
                If drRow.Length > 0 Then
                    For i As Integer = 0 To drRow.Length - 1
                        drRow(i)("transactionunkid") = mintFormunkid
                        drRow(i).AcceptChanges()
                    Next
                End If
                Dim objDocument As New clsScan_Attach_Documents
                objDocument._Datatable = mdtAttachDocument
                If objDocument.InsertUpdateDelete_Documents(objDataOperation) = False Then
                    Throw New Exception(objDocument._Message)
                End If
                objDocument = Nothing
            End If


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            If mblnSkipForApproverFlow AndAlso mdtReturndate <> Nothing Then   'SKIP APPROVER FLOW AS PER LEAVE TYPE SETTINGS.
                Dim mdecTotalIssue As Decimal = 0
                Dim mdblLeaveAmout As Decimal = 0
                Dim arGetLeaveDate As ArrayList = Nothing
                Dim arGetdate() As String = Nothing
                Dim objleaveIssue As New clsleaveissue_master

                Dim objLeaveType As New clsleavetype_master
                objLeaveType._DoOperation = objDataOperation
                objLeaveType._Leavetypeunkid = mintLeavetypeunkid


                If objLeaveType._IsPaid Then
                    Dim objAcccure As New clsleavebalance_tran
                    Dim dsAccrueList As DataSet = Nothing
                    Dim mstrFilter As String = "lvleavebalance_tran.yearunkid = " & xYearUnkid & " AND lvleavebalance_tran.leavetypeunkid = " & mintLeavetypeunkid


                    If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        dsAccrueList = objAcccure.GetList("List", xDatabaseName, mintUserunkid, xYearUnkid _
                                                               , xCompanyUnkid, eZeeDate.convertDate(mdtEmployeeAsonDate.Date), xUserModeSettings _
                                                               , True, xIncludeIn_ActiveEmployee, True, True, False _
                                                               , mintEmployeeunkid, False, False, False, mstrFilter, objDataOperation, False)

                    ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        dsAccrueList = objAcccure.GetList("List", xDatabaseName, mintUserunkid, xYearUnkid _
                                                               , xCompanyUnkid, eZeeDate.convertDate(mdtEmployeeAsonDate.Date), xUserModeSettings _
                                                               , True, xIncludeIn_ActiveEmployee, True, True, False _
                                                               , mintEmployeeunkid, True, True, False, mstrFilter, objDataOperation, False)
                    End If

                    If dsAccrueList IsNot Nothing Then
                        If dsAccrueList.Tables(0).Rows.Count > 0 Then
                            mdblLeaveAmout = CDec(dsAccrueList.Tables(0).Rows(0)("accrue_amount"))
                        End If
                    End If

                    objAcccure = Nothing
                Else
                    mdblLeaveAmout = objLeaveType._MaxAmount
                End If


                arGetLeaveDate = New ArrayList()
                For i As Integer = 0 To dtFraction.Rows.Count - 1
                    arGetLeaveDate.Add(CDate(dtFraction.Rows(i)("leavedate")).ToShortDateString())
                    mdecTotalIssue += CDec(dtFraction.Rows(i)("dayfraction"))
                Next

                If arGetLeaveDate IsNot Nothing Then
                    arGetdate = CType(arGetLeaveDate.ToArray(GetType(String)), String())
                End If

                objleaveIssue._Formunkid = mintFormunkid
                objleaveIssue._Employeeunkid = mintEmployeeunkid
                objleaveIssue._Leavetypeunkid = mintLeavetypeunkid
                objleaveIssue._StartDate = mdtStartdate.Date
                objleaveIssue._EndDate = mdtReturndate.Date
                objleaveIssue._TotalIssue = mdecTotalIssue
                objleaveIssue._Leaveyearunkid = xYearUnkid
                objleaveIssue._SkipApproverFlow = mblnSkipForApproverFlow
                objleaveIssue._Userunkid = mintUserunkid
                objleaveIssue._WebClientIP = mstrWebClientIP
                objleaveIssue._WebFormName = mstrWebFrmName
                objleaveIssue._WebHostName = mstrWebHostName

                'Pinkal (01-Jan-2019) -- Start
                'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                objleaveIssue._ConsiderLeaveOnTnAPeriod = mblnConsiderLeaveOnTnAPeriod
                'Pinkal (01-Jan-2019) -- End

                If objleaveIssue.Insert(FinancialYear._Object._DatabaseName, IIf(mintUserunkid <= 0, mintLoginEmployeeunkid, mintUserunkid), xYearUnkid, xCompanyUnkid _
                                              , eZeeDate.convertDate(mdtEmployeeAsonDate.Date), xUserModeSettings, True, xIncludeIn_ActiveEmployee _
                                              , arGetdate, objLeaveType._IsPaid, mdblLeaveAmout, intLeaveBalanceSetting, blnLeaveApproverForLeaveType, -1, objDataOperation, True, False, objLeaveType._Isexempt_from_payroll, blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst) = False Then
                    'Sohail (21 Oct 2019) - [blnExemptFromPayroll, blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst]

                    If objleaveIssue._Message <> "" Then
                        mstrMessage = objleaveIssue._Message
                        exForce = New Exception(objleaveIssue._Message)
                    Else
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

                objLeaveType = Nothing

                If ApprovedLeaveFormtenure(objDataOperation, mintFormunkid, mdtStartdate.Date, mdtReturndate.Date, mdecTotalIssue, True, mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                UpdateLeaveFormStatus(objDataOperation, mintFormunkid, 7, mintUserunkid)  'UPDATE LEAVE FORM STATUS TO ISSUED.

            End If

            'Pinkal (01-Oct-2018) -- End


            objDataOperation.ReleaseTransaction(True)

            'S.SANDEEP |08-APR-2019| -- START

            'S.SANDEEP |25-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : FlexCube
            'Call BlockEmployeeInCBS(mintEmployeeunkid, mdtStartdate, mdtReturndate, mstrFormno, dtCurrentDateAndTime, xCompanyUnkid, "D", objDataOperation)
            Call BlockEmployeeInCBS(mintEmployeeunkid, mdtStartdate, mdtReturndate, mstrFormno, dtCurrentDateAndTime, xCompanyUnkid, "D", objDataOperation, mintUserunkid, IIf(mstrWebClientIP.Trim.Length > 0, mintEmployeeunkid, 0))
            'S.SANDEEP |25-NOV-2019| -- END

            'S.SANDEEP |08-APR-2019| -- END

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (lvleaveform) </purpose>
    'Public Function Delete(ByVal intUnkid As Integer, Optional ByVal intLeaveBalanceSetting As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation
    '    objDataOperation.BindTransaction()

    '    Try

    '        If intLeaveBalanceSetting <= 0 Then
    '            intLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
    '        End If

    '        'START FOR GET LEAVE TYPE AND EMPLOYEE OF THAT LEAVE FORM
    '        strQ = " Select isnull(leavetypeunkid,0) as leavetypeunkid,isnull(employeeunkid,0) as employeeunkid " & _
    '               " from lvleaveform WHERE formunkid=@formunkid"

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        dsList = objDataOperation.ExecQuery(strQ, "GetFormData")

    '        'END FOR GET LEAVE TYPE AND EMPLOYEE OF THAT LEAVE FORM

    '        If dsList.Tables("GetFormData").Rows.Count > 0 Then

    '            'START FOR GET LEAVE FORM FOR THAT EMPLOYEE AND LEAVE TYPE 
    '            strQ = " Select isnull(formunkid,0) as formunkid " & _
    '                   " FROM lvleaveform " & _
    '                   " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid " & _
    '                   " AND formunkid <> @formunkid AND isvoid = 0 "
    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("employeeunkid"))
    '            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("leavetypeunkid"))
    '            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '            Dim dsForm As DataSet = objDataOperation.ExecQuery(strQ, "FormData")

    '            'END FOR GET LEAVE FORM FOR THAT EMPLOYEE AND LEAVE TYPE 

    '            If dsForm.Tables("FormData").Rows.Count > 0 Then

    '                'START FOR GET LEAVE ISSUE AMOUNT FOR THAT LEAVE FORM

    '                strQ = " SELECT  ISNULL(SUM(lvleaveday_fraction.dayfraction),0) issue_Amount " & _
    '                          " FROM lvleaveissue_tran " & _
    '                          " JOIN lvleaveday_fraction ON lvleaveissue_tran.leavedate = lvleaveday_fraction.leavedate AND lvleaveday_fraction.formunkid  = @formunkid " & _
    '                          " WHERE  lvleaveissue_tran.leaveissueunkid IN (SELECT leaveissueunkid FROM lvleaveIssue_master WHERE formunkid = @formunkid AND isvoid = 0) "

    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '                Dim dsIssueAmount As DataSet = objDataOperation.ExecQuery(strQ, "IssueAmount")

    '                'END FOR GET LEAVE ISSUE AMOUNT FOR THAT LEAVE FORM

    '                'START FOR INSERT LEAVE BALANCE AUDIT TRAIL
    '                Dim objBalance As New clsleavebalance_tran
    '                Dim dsBalance As DataSet = Nothing

    '                If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                    dsBalance = objBalance.GetList("BalanceList", True)
    '                ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                    dsBalance = objBalance.GetList("BalanceList", True, False, -1, True, True)
    '                End If

    '                Dim dtBalance As DataTable = New DataView(dsBalance.Tables("BalanceList"), "employeeunkid=" & CInt(dsList.Tables("GetFormData").Rows(0)("employeeunkid")) _
    '                                                           & " AND leavetypeunkid = " & CInt(dsList.Tables("GetFormData").Rows(0)("leavetypeunkid")), "", DataViewRowState.CurrentRows).ToTable

    '                If dtBalance.Rows.Count > 0 Then
    '                    For i As Integer = 0 To dtBalance.Rows.Count - 1
    '                        objBalance._LeaveBalanceunkid = CInt(dtBalance.Rows(i)("leavebalanceunkid"))
    '                        objBalance._IssueAmount = objBalance._IssueAmount - CInt(dsIssueAmount.Tables("IssueAmount").Rows(0)("issue_Amount"))
    '                        If objBalance.InsertAudiTrailForLeaveBalance(objDataOperation, 2) = False Then
    '                            objDataOperation.ReleaseTransaction(False)
    '                            Return False
    '                        End If
    '                    Next
    '                End If
    '                'END FOR INSERT LEAVE BALANCE AUDIT TRAIL

    '                'START FOR UPDATE LEAVE BALANCE TRAN
    '                strQ = " UPDATE lvleavebalance_tran set " & _
    '                       " issue_amount = issue_amount - @issueamount " & _
    '                       ", balance = balance + @issueamount" & _
    '                       " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid   "

    '                strQ &= " AND isvoid  = 0 "
    '                If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                    strQ &= " AND isopenelc = 1 AND iselc = 1 "
    '                End If


    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@issueamount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dsIssueAmount.Tables("IssueAmount").Rows(0)("issue_Amount"))
    '                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("employeeunkid"))
    '                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("leavetypeunkid"))
    '                objDataOperation.ExecNonQuery(strQ)

    '                ''END FOR UPDATE LEAVE BALANCE TRAN

    '                'Pinkal (01-Feb-2014) -- Start
    '                'Enhancement : TRA Changes

    '                'Else

    '                '    'START FOR INSERT LEAVE BALANCE AUDIT TRAIL
    '                '    Dim objBalance As New clsleavebalance_tran

    '                '    Dim dtBalance As DataTable = New DataView(dsBalance.Tables("BalanceList"), "employeeunkid=" & CInt(dsList.Tables("GetFormData").Rows(0)("employeeunkid")) _
    '                '                                               & " AND leavetypeunkid = " & CInt(dsList.Tables("GetFormData").Rows(0)("leavetypeunkid")), "", DataViewRowState.CurrentRows).ToTable

    '                '    If dtBalance.Rows.Count > 0 Then
    '                '        For i As Integer = 0 To dtBalance.Rows.Count - 1
    '                '            objBalance._LeaveBalanceunkid = CInt(dtBalance.Rows(i)("leavebalanceunkid"))
    '                '            If objBalance.InsertAudiTrailForLeaveBalance(objDataOperation, 3) = False Then
    '                '                objDataOperation.ReleaseTransaction(False)
    '                '                Return False
    '                '            End If
    '                '        Next
    '                '    End If
    '                '    'END FOR INSERT LEAVE BALANCE AUDIT TRAIL

    '                '    strQ = " UPDATE lvleavebalance_tran set " & _
    '                '           " isvoid = 1" & _
    '                '           ",voiddatetime = @voiddatetime  " & _
    '                '           ", voidreason = @voidreason " & _
    '                '           ", voiduserunkid = @voiduserunkid " & _
    '                '           " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid   "



    '                '    objDataOperation.ClearParameters()
    '                '    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
    '                '    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '                '    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)

    '                'Pinkal (01-Feb-2014) -- End

    '            End If




    '        End If

    '        'START FOR VOID ISSUE LEAVE

    '        'START FOR GET LEAVE ISSUE UNKID 
    '        strQ = "select isnull(leaveissueunkid,0) as leaveissueunkid from lvleaveissue_master where formunkid = @formid"
    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@formid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        Dim dsIssueUnkid As DataSet = objDataOperation.ExecQuery(strQ, "leaveissueunkid")
    '        'END FOR GET LEAVE ISSUE UNKID 

    '        If dsIssueUnkid.Tables("leaveissueunkid").Rows.Count > 0 Then

    '            'START FOR INSERT LEAVE ISSUE AUDIT TRAIL
    '            Dim objIssue As New clsleaveissue_Tran
    '            Dim dsIssue As DataSet = objIssue.GetList("List", True)
    '            Dim dtIssue As DataTable = New DataView(dsIssue.Tables("List"), "leaveissueunkid=" & CInt(dsIssueUnkid.Tables("leaveissueunkid").Rows(0)("leaveissueunkid")) _
    '                                                      & " AND formunkid = " & intUnkid, "", DataViewRowState.CurrentRows).ToTable

    '            If dtIssue.Rows.Count > 0 Then
    '                For i As Integer = 0 To dtIssue.Rows.Count - 1
    '                    objIssue._Leaveissuetranunkid = CInt(dtIssue.Rows(i)("leaveissuetranunkid"))
    '                    If objIssue.InsertAuditTrailForLeaveIssue(objDataOperation, 3, eZeeDate.convertDate(dtIssue.Rows(i)("leavedate").ToString).ToShortDateString) = False Then
    '                        objDataOperation.ReleaseTransaction(False)
    '                        Return False
    '                    End If
    '                Next
    '            End If
    '            'END FOR INSERT LEAVE ISSUE AUDIT TRAIL
    '        End If

    '        strQ = "Update lvleaveissue_tran set" & _
    '                   " isvoid =1,voiddatetime= @voiddatetime, " & _
    '                   " voidreason = @voidreason,  " & _
    '                   " voiduserunkid = @voiduserunkid " & _
    '                   " WHERE leaveissueunkid = (select leaveissueunkid from lvleaveissue_master where formunkid = @formid and isvoid = 0)"

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@formid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
    '        objDataOperation.ExecNonQuery(strQ)


    '        strQ = "Update lvleaveissue_master set" & _
    '                   " isvoid =1,voiddatetime= @voiddatetime,voidreason = @void_reason, " & _
    '                   " voiduserunkid = @voiduserunkid " & _
    '                   " WHERE formunkid = @form_id"

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@form_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        objDataOperation.AddParameter("@void_reason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
    '        objDataOperation.ExecNonQuery(strQ)

    '        'END FOR VOID ISSUE LEAVE

    '        strQ = "Select  Isnull(pendingleavetranunkid,0) pendingleavetranunkid from lvpendingleave_tran " & _
    '                  " WHERE formunkid = @form_unkid "

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@form_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
    '        Dim dtList As DataSet = objDataOperation.ExecQuery(strQ, "List")

    '        'START FOR VOID PROCESS LEAVE
    '        strQ = "Update lvpendingleave_tran set" & _
    '               " isvoid =1,voiddatetime= @voiddatetime,voidreason = @vreason, " & _
    '               " voiduserunkid = @voiduserunkid " & _
    '               " WHERE formunkid = @form_unkid "

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@form_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        objDataOperation.AddParameter("@vreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
    '        objDataOperation.ExecNonQuery(strQ)
    '        'END FOR VOID PROCESS LEAVE

    '        If dtList.Tables.Count > 0 AndAlso dtList.Tables(0).Rows.Count > 0 Then

    '            For Each dr As DataRow In dtList.Tables(0).Rows
    '                If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lvpendingleave_tran", "pendingleavetranunkid", CInt(dr("pendingleavetranunkid"))) = False Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If

    '            Next

    '        End If


    '        'START FOR VOID LEAVE FORM

    '        strQ = "Update lvleaveform set" & _
    '               " isvoid =1,voiddatetime= @voiddatetime,voidreason = @vdreason " & _
    '               ", voiduserunkid = @voiduserunkid " & _
    '               ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
    '               " WHERE formunkid = @formunkid "


    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        objDataOperation.AddParameter("@vdreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        If mintVoidlogingemployeeunkid > 0 Then
    '            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
    '        Else
    '            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
    '        End If
    '        objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidlogingemployeeunkid)
    '        objDataOperation.ExecNonQuery(strQ)


    '        'END FOR VOID LEAVE FORM


    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If



    '        'START FOR VOID LEAVE FRACTION
    '        Dim objFraction As New clsleaveday_fraction
    '        objFraction._Isvoid = True

    '        'Pinkal (06-Mar-2014) -- Start
    '        'Enhancement : TRA Changes
    '        'objFraction._Voiduserunkid = User._Object._Userunkid
    '        objFraction._Voiduserunkid = mintVoiduserunkid
    '        'Pinkal (06-Mar-2014) -- End

    '        objFraction._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '        objFraction._Voidreason = mstrVoidreason
    '        objFraction._Voidloginemployeeunkid = mintVoidlogingemployeeunkid
    '        If objFraction.Delete(intUnkid, objDataOperation) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'objExpense._Isvoid = True
    '        'objExpense._Voiduserunkid = User._Object._Userunkid
    '        'objExpense._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '        'objExpense._Voidreason = mstrVoidreason
    '        'objExpense._Voidloginemployeeunkid = mintVoidlogingemployeeunkid
    '        'If objExpense.Delete(intUnkid, objDataOperation) = False Then
    '        '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        '    Throw exForce
    '        'End If

    '        'Pinkal (06-Mar-2014) -- Start
    '        'Enhancement : TRA Changes

    '        'objExpense._Isvoid = True
    '        'objExpense._Voiduserunkid = User._Object._Userunkid
    '        'objExpense._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '        'objExpense._Voidreason = mstrVoidreason
    '        'objExpense._Voidloginemployeeunkid = mintVoidlogingemployeeunkid
    '        'If objExpense.Delete(intUnkid, objDataOperation) = False Then
    '        '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        '    Throw exForce
    '        'End If

    '        If objClaimRequestMst._Crmasterunkid > 0 Then
    '            objClaimRequestMst._Isvoid = True
    '            objClaimRequestMst._Voiduserunkid = mintVoiduserunkid
    '            objClaimRequestMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '            objClaimRequestMst._Voidreason = mstrVoidreason
    '            objClaimRequestMst._Loginemployeeunkid = mintVoidlogingemployeeunkid
    '            If objClaimRequestMst.Delete(objClaimRequestMst._Crmasterunkid, mintVoiduserunkid, objDataOperation) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If
    '        End If

    '        'Pinkal (06-Mar-2014) -- End

    '        'END FOR VOID LEAVE FRACTION



    '        'Pinkal (07-May-2015) -- Start
    '        'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.

    '        If mdtAttachDocument IsNot Nothing Then
    '            Dim objDocument As New clsScan_Attach_Documents
    '            Dim mstrIds As String = ""
    '            For Each drRow As DataRow In mdtAttachDocument.Rows
    '                mstrIds &= drRow("scanattachtranunkid").ToString() & ","
    '            Next
    '            If mstrIds.Trim.Length > 0 Then
    '                mstrIds = mstrIds.Trim.Substring(0, mstrIds.Trim.Length - 1)
    '            End If
    '            If objDocument.Delete(mstrIds, objDataOperation) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If
    '            objDocument = Nothing
    '        End If

    '        'Pinkal (07-May-2015) -- End


    '        objDataOperation.ReleaseTransaction(True)
    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvleaveform) </purpose>
    ''' 'Shani (20-Aug-2016) -- Start
    ''' 'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
    ''' 'Public Function Delete(ByVal intUnkid As Integer, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
    ''' '                                 , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
    ''' '                                 , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
    ''' '                                 , ByVal xIncludeIn_ActiveEmployee As Boolean, Optional ByVal blnOnlyActive As Boolean = True _
    ''' '                                 , Optional ByVal blnApplyUserAccessFilter As Boolean = True, Optional ByVal intLeaveBalanceSetting As Integer = -1) As Boolean
    Public Function Delete(ByVal intUnkid As Integer, _
                           ByVal xDatabaseName As String, _
                           ByVal xUserUnkid As Integer, _
                           ByVal xYearUnkid As Integer, _
                           ByVal xCompanyUnkid As Integer, _
                           ByVal strEmployeeAsOnDate As String, _
                           ByVal xUserModeSetting As String, _
                           ByVal xOnlyApproved As Boolean, _
                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                           ByVal dtAttachment As DataTable, _
                           Optional ByVal blnOnlyActive As Boolean = True, _
                           Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                           Optional ByVal intLeaveBalanceSetting As Integer = -1) As Boolean
        'Shani (20-Aug-2016) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If intLeaveBalanceSetting <= 0 Then
                intLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            End If

            'START FOR GET LEAVE TYPE AND EMPLOYEE OF THAT LEAVE FORM
            strQ = " Select isnull(leavetypeunkid,0) as leavetypeunkid,isnull(employeeunkid,0) as employeeunkid " & _
                   " from lvleaveform WHERE formunkid=@formunkid"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "GetFormData")

            'END FOR GET LEAVE TYPE AND EMPLOYEE OF THAT LEAVE FORM

            If dsList.Tables("GetFormData").Rows.Count > 0 Then

                'START FOR GET LEAVE FORM FOR THAT EMPLOYEE AND LEAVE TYPE 
                strQ = " Select isnull(formunkid,0) as formunkid " & _
                       " FROM lvleaveform " & _
                       " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid " & _
                       " AND formunkid <> @formunkid AND isvoid = 0 "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("employeeunkid"))
                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("leavetypeunkid"))
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                Dim dsForm As DataSet = objDataOperation.ExecQuery(strQ, "FormData")

                'END FOR GET LEAVE FORM FOR THAT EMPLOYEE AND LEAVE TYPE 

                If dsForm.Tables("FormData").Rows.Count > 0 Then

                    'START FOR GET LEAVE ISSUE AMOUNT FOR THAT LEAVE FORM

                    strQ = " SELECT  ISNULL(SUM(lvleaveday_fraction.dayfraction),0) issue_Amount " & _
                              " FROM lvleaveissue_tran " & _
                              " JOIN lvleaveday_fraction ON lvleaveissue_tran.leavedate = lvleaveday_fraction.leavedate AND lvleaveday_fraction.formunkid  = @formunkid " & _
                              " WHERE  lvleaveissue_tran.leaveissueunkid IN (SELECT leaveissueunkid FROM lvleaveIssue_master WHERE formunkid = @formunkid AND isvoid = 0) "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                    Dim dsIssueAmount As DataSet = objDataOperation.ExecQuery(strQ, "IssueAmount")

                    'END FOR GET LEAVE ISSUE AMOUNT FOR THAT LEAVE FORM

                    'START FOR INSERT LEAVE BALANCE AUDIT TRAIL
                    Dim objBalance As New clsleavebalance_tran
                    Dim dsBalance As DataSet = Nothing



                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

                    Dim mstrFilter As String = "lvleavebalance_tran.leavetypeunkid = " & CInt(dsList.Tables("GetFormData").Rows(0)("leavetypeunkid"))

                    'If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    '    dsBalance = objBalance.GetList("BalanceList", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                    '                                                    , strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnOnlyActive _
                    '                                                    , blnApplyUserAccessFilter, False, -1, False, False, False, "", objDataOperation)
                    'ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    '    dsBalance = objBalance.GetList("BalanceList", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                    '                                                    , strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnOnlyActive _
                    '                                                    , blnApplyUserAccessFilter, False, -1, True, True, False, "", objDataOperation)
                    'End If


                    If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                        dsBalance = objBalance.GetList("BalanceList", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                                        , strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnOnlyActive _
                                                                        , blnApplyUserAccessFilter, False, CInt(dsList.Tables("GetFormData").Rows(0)("employeeunkid")), False, False, False, mstrFilter, objDataOperation)
                    ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        dsBalance = objBalance.GetList("BalanceList", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                                        , strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnOnlyActive _
                                                                        , blnApplyUserAccessFilter, False, CInt(dsList.Tables("GetFormData").Rows(0)("employeeunkid")), True, True, False, mstrFilter, objDataOperation)
                    End If


                    'Dim dtBalance As DataTable = New DataView(dsBalance.Tables("BalanceList"), "employeeunkid=" & CInt(dsList.Tables("GetFormData").Rows(0)("employeeunkid")) _
                    '                                            & " AND leavetypeunkid = " & CInt(dsList.Tables("GetFormData").Rows(0)("leavetypeunkid")), "", DataViewRowState.CurrentRows).ToTable

                    Dim dtBalance As DataTable = dsBalance.Tables(0)

                    'Pinkal (11-Sep-2020) -- End

                    If dtBalance.Rows.Count > 0 Then
                        For i As Integer = 0 To dtBalance.Rows.Count - 1
                            objBalance._LeaveBalanceunkid = CInt(dtBalance.Rows(i)("leavebalanceunkid"))
                            objBalance._IssueAmount = objBalance._IssueAmount - CInt(dsIssueAmount.Tables("IssueAmount").Rows(0)("issue_Amount"))
                            If objBalance.InsertAudiTrailForLeaveBalance(objDataOperation, 2) = False Then
                                objDataOperation.ReleaseTransaction(False)
                                Return False
                            End If
                        Next
                    End If
                    'END FOR INSERT LEAVE BALANCE AUDIT TRAIL

                    'START FOR UPDATE LEAVE BALANCE TRAN
                    strQ = " UPDATE lvleavebalance_tran set " & _
                           " issue_amount = issue_amount - @issueamount " & _
                           ", balance = balance + @issueamount" & _
                           " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid   "

                    strQ &= " AND isvoid  = 0 "
                    If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                        strQ &= " AND isopenelc = 1 AND iselc = 1 "
                    End If


                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@issueamount", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dsIssueAmount.Tables("IssueAmount").Rows(0)("issue_Amount"))
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("employeeunkid"))
                    objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("leavetypeunkid"))
                    objDataOperation.ExecNonQuery(strQ)

                    ''END FOR UPDATE LEAVE BALANCE TRAN

                    'Pinkal (01-Feb-2014) -- Start
                    'Enhancement : TRA Changes

                    'Else

                    '    'START FOR INSERT LEAVE BALANCE AUDIT TRAIL
                    '    Dim objBalance As New clsleavebalance_tran

                    '    Dim dtBalance As DataTable = New DataView(dsBalance.Tables("BalanceList"), "employeeunkid=" & CInt(dsList.Tables("GetFormData").Rows(0)("employeeunkid")) _
                    '                                               & " AND leavetypeunkid = " & CInt(dsList.Tables("GetFormData").Rows(0)("leavetypeunkid")), "", DataViewRowState.CurrentRows).ToTable

                    '    If dtBalance.Rows.Count > 0 Then
                    '        For i As Integer = 0 To dtBalance.Rows.Count - 1
                    '            objBalance._LeaveBalanceunkid = CInt(dtBalance.Rows(i)("leavebalanceunkid"))
                    '            If objBalance.InsertAudiTrailForLeaveBalance(objDataOperation, 3) = False Then
                    '                objDataOperation.ReleaseTransaction(False)
                    '                Return False
                    '            End If
                    '        Next
                    '    End If
                    '    'END FOR INSERT LEAVE BALANCE AUDIT TRAIL

                    '    strQ = " UPDATE lvleavebalance_tran set " & _
                    '           " isvoid = 1" & _
                    '           ",voiddatetime = @voiddatetime  " & _
                    '           ", voidreason = @voidreason " & _
                    '           ", voiduserunkid = @voiduserunkid " & _
                    '           " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid   "



                    '    objDataOperation.ClearParameters()
                    '    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
                    '    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    '    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)

                    'Pinkal (01-Feb-2014) -- End

                End If




            End If

            'START FOR VOID ISSUE LEAVE

            'START FOR GET LEAVE ISSUE UNKID 
            strQ = "select isnull(leaveissueunkid,0) as leaveissueunkid from lvleaveissue_master where formunkid = @formid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            Dim dsIssueUnkid As DataSet = objDataOperation.ExecQuery(strQ, "leaveissueunkid")
            'END FOR GET LEAVE ISSUE UNKID 

            If dsIssueUnkid.Tables("leaveissueunkid").Rows.Count > 0 Then

                'START FOR INSERT LEAVE ISSUE AUDIT TRAIL
                Dim objIssue As New clsleaveissue_Tran
                Dim dsIssue As DataSet = objIssue.GetList("List", True)
                Dim dtIssue As DataTable = New DataView(dsIssue.Tables("List"), "leaveissueunkid=" & CInt(dsIssueUnkid.Tables("leaveissueunkid").Rows(0)("leaveissueunkid")) _
                                                          & " AND formunkid = " & intUnkid, "", DataViewRowState.CurrentRows).ToTable

                If dtIssue.Rows.Count > 0 Then
                    For i As Integer = 0 To dtIssue.Rows.Count - 1
                        objIssue._Leaveissuetranunkid = CInt(dtIssue.Rows(i)("leaveissuetranunkid"))
                        If objIssue.InsertAuditTrailForLeaveIssue(objDataOperation, 3, eZeeDate.convertDate(dtIssue.Rows(i)("leavedate").ToString).ToShortDateString) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    Next
                End If
                'END FOR INSERT LEAVE ISSUE AUDIT TRAIL
            End If

            strQ = "Update lvleaveissue_tran set" & _
                   " isvoid =1,voiddatetime= @voiddatetime, " & _
                   " voidreason = @voidreason,  " & _
                   " voiduserunkid = @voiduserunkid " & _
                   " WHERE leaveissueunkid = (select leaveissueunkid from lvleaveissue_master where formunkid = @formid and isvoid = 0)"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.ExecNonQuery(strQ)


            strQ = "Update lvleaveissue_master set" & _
                               " isvoid =1,voiddatetime= @voiddatetime,voidreason = @void_reason, " & _
                               " voiduserunkid = @voiduserunkid " & _
                               " WHERE formunkid = @form_id"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@form_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@void_reason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.ExecNonQuery(strQ)

            'END FOR VOID ISSUE LEAVE

            strQ = "Select  Isnull(pendingleavetranunkid,0) pendingleavetranunkid from lvpendingleave_tran " & _
                      " WHERE formunkid = @form_unkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@form_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            Dim dtList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            'START FOR VOID PROCESS LEAVE
            strQ = "Update lvpendingleave_tran set" & _
                   " isvoid =1,voiddatetime= @voiddatetime,voidreason = @vreason, " & _
                   " voiduserunkid = @voiduserunkid " & _
                   " WHERE formunkid = @form_unkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@form_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@vreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.ExecNonQuery(strQ)
            'END FOR VOID PROCESS LEAVE

            If dtList.Tables.Count > 0 AndAlso dtList.Tables(0).Rows.Count > 0 Then

                For Each dr As DataRow In dtList.Tables(0).Rows
                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lvpendingleave_tran", "pendingleavetranunkid", CInt(dr("pendingleavetranunkid"))) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            End If


            'START FOR VOID LEAVE FORM

            strQ = "Update lvleaveform set" & _
                   " isvoid =1,voiddatetime= @voiddatetime,voidreason = @vdreason " & _
                   ", voiduserunkid = @voiduserunkid " & _
                   ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                   " WHERE formunkid = @formunkid "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@vdreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            If mintVoidlogingemployeeunkid > 0 Then
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            Else
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            End If
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidlogingemployeeunkid)
            objDataOperation.ExecNonQuery(strQ)


            'END FOR VOID LEAVE FORM


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            'START FOR VOID LEAVE FRACTION
            Dim objFraction As New clsleaveday_fraction
            objFraction._Isvoid = True

            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes
            'objFraction._Voiduserunkid = User._Object._Userunkid
            objFraction._Voiduserunkid = mintVoiduserunkid
            'Pinkal (06-Mar-2014) -- End

            objFraction._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objFraction._Voidreason = mstrVoidreason
            objFraction._Voidloginemployeeunkid = mintVoidlogingemployeeunkid
            If objFraction.Delete(intUnkid, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'objExpense._Isvoid = True
            'objExpense._Voiduserunkid = User._Object._Userunkid
            'objExpense._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            'objExpense._Voidreason = mstrVoidreason
            'objExpense._Voidloginemployeeunkid = mintVoidlogingemployeeunkid
            'If objExpense.Delete(intUnkid, objDataOperation) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes

            'objExpense._Isvoid = True
            'objExpense._Voiduserunkid = User._Object._Userunkid
            'objExpense._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            'objExpense._Voidreason = mstrVoidreason
            'objExpense._Voidloginemployeeunkid = mintVoidlogingemployeeunkid
            'If objExpense.Delete(intUnkid, objDataOperation) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            If objClaimRequestMst._Crmasterunkid > 0 Then
                objClaimRequestMst._Isvoid = True
                objClaimRequestMst._Voiduserunkid = mintVoiduserunkid
                objClaimRequestMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objClaimRequestMst._Voidreason = mstrVoidreason
                objClaimRequestMst._Loginemployeeunkid = mintVoidlogingemployeeunkid

                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'If objClaimRequestMst.Delete(objClaimRequestMst._Crmasterunkid, mintVoiduserunkid, objDataOperation) = False Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If

                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)



                'If objClaimRequestMst.Delete(objClaimRequestMst._Crmasterunkid, mintVoiduserunkid, xDatabaseName, xYearUnkid _
                '                                            , xCompanyUnkid, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved _
                '                                             , blnApplyUserAccessFilter, objDataOperation) = False Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If

                If objClaimRequestMst.Delete(objClaimRequestMst._Crmasterunkid, mintVoiduserunkid, xDatabaseName, xYearUnkid _
                                                            , xCompanyUnkid, strEmployeeAsOnDate, xUserModeSetting, xOnlyApproved _
                                                              , dtAttachment, blnApplyUserAccessFilter, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'Shani (26-Sep-2016) -- End

                'Pinkal (24-Aug-2015) -- End


            End If

            'Pinkal (06-Mar-2014) -- End

            'END FOR VOID LEAVE FRACTION



            'Pinkal (07-May-2015) -- Start
            'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.

            If mdtAttachDocument IsNot Nothing Then
                Dim objDocument As New clsScan_Attach_Documents
                Dim mstrIds As String = ""
                For Each drRow As DataRow In mdtAttachDocument.Rows
                    mstrIds &= drRow("scanattachtranunkid").ToString() & ","
                Next
                If mstrIds.Trim.Length > 0 Then
                    mstrIds = mstrIds.Trim.Substring(0, mstrIds.Trim.Length - 1)
                End If
                If objDocument.Delete(mstrIds, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objDocument = Nothing
            End If

            'Pinkal (07-May-2015) -- End


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            'strQ = "Select isnull(formunkid,0) from lvleaveIssue_master where formunkid = @formunkid"
            'objDataOperation.ClearParameters()
            'objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            'If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            'strQ = "Select isnull(formunkid,0) from lvpendingleave_tran where formunkid = @formunkid"
            'objDataOperation.ClearParameters()
            'objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            'If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strFormNo As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  formunkid " & _
              ", formno " & _
              ", employeeunkid " & _
              ", leavetypeunkid " & _
              ", applydate " & _
              ", startdate " & _
              ", returndate " & _
              ", addressonleave " & _
              ", remark " & _
              ", statusunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", voiduserunkid " & _
             ", voidloginemployeeunkid " & _
                      ", ISNULL(relieverempunkid,0) AS relieverempunkid " & _
                         ", ISNULL(istnaperiodlinked,0)  AS istnaperiodlinked " & _
                         " FROM lvleaveform " & _
                         " WHERE formno = @formno AND isvoid = 0 "


            'Pinkal (01-Jan-2019) --  'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[ ", ISNULL(istnaperiodlinked,0)  AS istnaperiodlinked " & _]

            'Pinkal (01-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", ISNULL(relieverempunkid,0) AS relieverempunkid " & _]

            If intUnkid > 0 Then
                strQ &= " AND formunkid <> @formunkid "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strFormNo)
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            '     objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isDayExist(ByVal blnIsApprovedDate As Boolean, ByVal intEmployeeunkid As Integer, ByVal dtstartdate As DateTime, ByVal dtreturndate As DateTime _
                                        , Optional ByVal intunkid As Integer = -1, Optional ByVal mstrFilter As String = "") As Boolean

        'Pinkal (18-Mar-2021) -- NMB Enhancmenet : AD Enhancement for NMB.[Optional ByVal mstrFilter As String = ""]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try

            strQ = "SELECT " & _
              "  formunkid " & _
              ", formno " & _
              ", employeeunkid " & _
              ", leavetypeunkid " & _
              ", applydate " & _
              ", startdate " & _
              ", returndate " & _
              ", addressonleave " & _
              ", remark " & _
              ", statusunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", voiduserunkid " & _
                      ", ISNULL(relieverempunkid,0) AS relieverempunkid " & _
              ", ISNULL(istnaperiodlinked,0) AS istnaperiodlinked " & _
              "  FROM lvleaveform " & _
              "  WHERE employeeunkid = @employeeunkid AND isvoid = 0 "

            'Pinkal (01-Jan-2019) -- 'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[", ISNULL(istnaperiodlinked,0) AS istnaperiodlinked " & _]

            'Pinkal (01-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", ISNULL(relieverempunkid,0) AS relieverempunkid " & _]


            'Pinkal (20-Jan-2014) -- Start
            'Enhancement : Oman Changes

            'If blnIsApprovedDate = False Then
            '    strQ &= "  AND (convert(char(8),startdate,112) BETWEEN @startdate and @returndate " & _
            '                "  OR convert(char(8),returndate,112) BETWEEN @startdate and @returndate) "
            'Else
            '    strQ &= "  AND (convert(char(8),approve_stdate,112) BETWEEN @startdate and @returndate " & _
            '                "  OR convert(char(8),approve_eddate,112) BETWEEN @startdate and @returndate) "
            'End If


            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : Oman Changes  [CHANGE BECOZ USER CAN APPLY LEAVE FORM BETWEEN APPLIED LEAVE FORM PERIOD]

            If blnIsApprovedDate = False Then
                strQ &= "  AND ( " & _
                            "               (@startdate BETWEEN convert(char(8),startdate,112) AND convert(char(8),returndate,112) " & _
                            "               OR @returndate BETWEEN convert(char(8),startdate,112) AND convert(char(8),returndate,112)) " & _
                            "               OR " & _
                            "               (convert(char(8),startdate,112) BETWEEN @startdate and @returndate " & _
                            "               AND convert(char(8),returndate,112) BETWEEN @startdate and @returndate) " & _
                            "          ) "
            Else
                strQ &= "  AND ( " & _
                            "               (@startdate BETWEEN convert(char(8),approve_stdate,112) AND convert(char(8),approve_eddate,112) " & _
                            "                OR @returndate BETWEEN convert(char(8),approve_stdate,112) AND convert(char(8),approve_eddate,112)) " & _
                            "               OR " & _
                            "               (convert(char(8),approve_stdate,112) BETWEEN @startdate and @returndate " & _
                            "               AND convert(char(8),approve_eddate,112) BETWEEN @startdate and @returndate) " & _
                            "          ) "
            End If

            'Pinkal (06-Mar-2014) -- End

            'Pinkal (20-Jan-2014) -- End

            strQ &= " AND statusunkid IN (1,2,7)"

            If mstrFilter.Trim.Length > 0 Then
                strQ &= "AND " & mstrFilter
            End If

            objDataOperation.ClearParameters()

            If intunkid > 0 Then
                strQ &= " AND formunkid <> @formunkid "
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intunkid)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtstartdate))
            objDataOperation.AddParameter("@returndate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtreturndate))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isDayExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            ' objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(ByVal strListName As String, Optional ByVal intStatusunkid As Integer = -1, Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as formunkid, ' ' +  @name as name   UNION "
            End If
            strQ &= "SELECT formunkid, formno as name FROM lvleaveform where (statusunkid = @statusunkid or @statusunkid = -1)  and isvoid = 0  ORDER BY name "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusunkid)

            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveform) </purpose>
    Public Function GetLeaveBalanceForForm(ByVal objDataOperation As clsDataOperation, ByVal intEmployeeunkid As Integer, ByVal intLeaveTypeunkid As Integer) As DataSet
        'Dim strQ As String = ""
        'Try

        '    strQ = "SELECT " & _
        '      "  leavebalanceunkid " & _
        '      ", yearunkid " & _
        '      ", employeeunkid " & _
        '      ", leavetypeunkid " & _
        '      ", batchunkid " & _
        '      ", accrue_amount " & _
        '      ", issue_amount " & _
        '      ", balance " & _
        '      ", ispaid " & _
        '      ", userunkid " & _
        '      ", isvoid " & _
        '      ", voiduserunkid " & _
        '      ", voiddatetime " & _
        '      ", voidreason " & _
        '     " FROM lvleavebalance_tran " & _
        '     " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid AND lvleavebalance_tran.Isvoid = 0 "

        '    objDataOperation.ClearParameters()
        '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
        '    objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeunkid)
        '    Dim dsBalance As DataSet = objDataOperation.ExecQuery(strQ, "BalanceList")

        '    Return dsBalance

        'Catch ex As Exception
        '    DisplayError.Show("-1", ex.Message, "GetLeaveBalanceForForm", mstrModuleName)
        'End Try
        Return Nothing
    End Function

    'START USED FOR IMPORT ONLY OTHERWISE NOT TO USE


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION. THIS METHOD NOT IN USE.

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <returns>Boolean</returns>
    '' <purpose> Delete Database Table (lvleaveform) </purpose>
    'Public Function ImportDelete(ByVal strFormno As String, Optional ByVal intLeaveBalanceSetting As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation
    '    objDataOperation.BindTransaction()

    '    Try

    '        If intLeaveBalanceSetting <= 0 Then
    '            intLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
    '        End If

    '        'START FOR GET LEAVE TYPE AND EMPLOYEE OF THAT LEAVE FORM
    '        strQ = " Select isnull(formunkid,0) as formunkid, isnull(leavetypeunkid,0) as leavetypeunkid,isnull(employeeunkid,0) as employeeunkid " & _
    '               " from lvleaveform WHERE formno=@formno"

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@formno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strFormno)
    '        dsList = objDataOperation.ExecQuery(strQ, "GetFormData")

    '        'END FOR GET LEAVE TYPE AND EMPLOYEE OF THAT LEAVE FORM

    '        If dsList.Tables("GetFormData").Rows.Count > 0 Then

    '            'START FOR GET LEAVE FORM FOR THAT EMPLOYEE AND LEAVE TYPE 
    '            strQ = " Select isnull(formunkid,0) as formunkid " & _
    '                   " FROM lvleaveform " & _
    '                   " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid " & _
    '                   " AND formunkid <> @formunkid AND isvoid = 0 "
    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("employeeunkid"))
    '            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("leavetypeunkid"))
    '            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("formunkid"))
    '            Dim dsForm As DataSet = objDataOperation.ExecQuery(strQ, "FormData")

    '            'END FOR GET LEAVE FORM FOR THAT EMPLOYEE AND LEAVE TYPE 

    '            If dsForm.Tables("FormData").Rows.Count > 0 Then

    '                'START FOR GET LEAVE ISSUE AMOUNT FOR THAT LEAVE FORM

    '                strQ = " Select count(*) issue_Amount From lvleaveissue_tran " & _
    '                       " WHERE leaveissueunkid in (select leaveissueunkid from lvleaveissue_master where formunkid = @formunkid) "
    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("formunkid"))
    '                Dim dsIssueAmount As DataSet = objDataOperation.ExecQuery(strQ, "IssueAmount")

    '                'END FOR GET LEAVE ISSUE AMOUNT FOR THAT LEAVE FORM

    '                'START FOR INSERT LEAVE BALANCE AUDIT TRAIL
    '                Dim objBalance As New clsleavebalance_tran
    '                'Dim dsBalance As DataSet = GetLeaveBalanceForForm(objDataOperation, CInt(dsList.Tables("GetFormData").Rows(0)("employeeunkid")), CInt(dsList.Tables("GetFormData").Rows(0)("leavetypeunkid")))
    '                Dim dsBalance As DataSet = Nothing
    '                If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                    dsBalance = objBalance.GetList("BalanceList", True)
    '                ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                    dsBalance = objBalance.GetList("BalanceList", True, False, -1, True, True)
    '                End If

    '                Dim dtBalance As DataTable = New DataView(dsBalance.Tables("BalanceList"), "employeeunkid=" & CInt(dsList.Tables("GetFormData").Rows(0)("employeeunkid")) _
    '                                                           & " AND leavetypeunkid = " & CInt(dsList.Tables("GetFormData").Rows(0)("leavetypeunkid")), "", DataViewRowState.CurrentRows).ToTable

    '                If dtBalance.Rows.Count > 0 Then
    '                    For i As Integer = 0 To dtBalance.Rows.Count - 1
    '                        objBalance._LeaveBalanceunkid = CInt(dtBalance.Rows(i)("leavebalanceunkid"))
    '                        objBalance._IssueAmount = objBalance._IssueAmount - CInt(dsIssueAmount.Tables("IssueAmount").Rows(0)("issue_Amount"))
    '                        If objBalance.InsertAudiTrailForLeaveBalance(objDataOperation, 2) = False Then
    '                            objDataOperation.ReleaseTransaction(False)
    '                            Return False
    '                        End If
    '                    Next
    '                End If
    '                'END FOR INSERT LEAVE BALANCE AUDIT TRAIL

    '                'START FOR UPDATE LEAVE BALANCE TRAN
    '                strQ = " UPDATE lvleavebalance_tran set " & _
    '                       " issue_amount = issue_amount - @issueamount " & _
    '                       ", balance = balance + @issueamount" & _
    '                       " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid   "

    '                strQ &= " AND isvoid =0 "
    '                If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                    strQ &= " AND isopenelc = 1 AND iselc = 1 "
    '                End If

    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@issueamount", SqlDbType.Int, eZeeDataType.INT_SIZE, dsIssueAmount.Tables("IssueAmount").Rows(0)("issue_Amount"))
    '            Else

    '                'START FOR INSERT LEAVE BALANCE AUDIT TRAIL
    '                Dim objBalance As New clsleavebalance_tran
    '                '  Dim dsBalance As DataSet = GetLeaveBalanceForForm(objDataOperation, CInt(dsList.Tables("GetFormData").Rows(0)("employeeunkid")), CInt(dsList.Tables("GetFormData").Rows(0)("leavetypeunkid")))

    '                Dim dsBalance As DataSet = Nothing

    '                If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                    dsBalance = objBalance.GetList("BalanceList", True)
    '                ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                    dsBalance = objBalance.GetList("BalanceList", True, False, -1, True, True)
    '                End If

    '                Dim dtBalance As DataTable = New DataView(dsBalance.Tables("BalanceList"), "employeeunkid=" & CInt(dsList.Tables("GetFormData").Rows(0)("employeeunkid")) _
    '                                                           & " AND leavetypeunkid = " & CInt(dsList.Tables("GetFormData").Rows(0)("leavetypeunkid")), "", DataViewRowState.CurrentRows).ToTable

    '                If dtBalance.Rows.Count > 0 Then
    '                    For i As Integer = 0 To dtBalance.Rows.Count - 1
    '                        objBalance._LeaveBalanceunkid = CInt(dtBalance.Rows(i)("leavebalanceunkid"))
    '                        If objBalance.InsertAudiTrailForLeaveBalance(objDataOperation, 3) = False Then
    '                            objDataOperation.ReleaseTransaction(False)
    '                            Return False
    '                        End If
    '                    Next
    '                End If
    '                'END FOR INSERT LEAVE BALANCE AUDIT TRAIL

    '                strQ = "delete from lvleavebalance_tran WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid   "
    '                strQ &= " AND isvoid = 0 "
    '                If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                    strQ &= " AND isopenelc = 1 AND iselc = 1 "
    '                End If

    '                objDataOperation.ClearParameters()
    '            End If
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("employeeunkid"))
    '            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("leavetypeunkid"))
    '            objDataOperation.ExecNonQuery(strQ)

    '            'END FOR UPDATE LEAVE BALANCE TRAN

    '        End If

    '        'START FOR VOID ISSUE LEAVE

    '        'START FOR GET LEAVE ISSUE UNKID 
    '        strQ = "select isnull(leaveissueunkid,0) as leaveissueunkid from lvleaveissue_master where formunkid = @formid"
    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@formid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("formunkid"))
    '        Dim dsIssueUnkid As DataSet = objDataOperation.ExecQuery(strQ, "leaveissueunkid")
    '        'END FOR GET LEAVE ISSUE UNKID 

    '        If dsIssueUnkid.Tables("leaveissueunkid").Rows.Count > 0 Then

    '            'START FOR INSERT LEAVE ISSUE AUDIT TRAIL
    '            Dim objIssue As New clsleaveissue_Tran
    '            Dim dsIssue As DataSet = objIssue.GetList("List", True)
    '            Dim dtIssue As DataTable = New DataView(dsIssue.Tables("List"), "leaveissueunkid=" & CInt(dsIssueUnkid.Tables("leaveissueunkid").Rows(0)("leaveissueunkid")) _
    '                                                      & " AND formunkid = " & dsList.Tables("GetFormData").Rows(0)("formunkid"), "", DataViewRowState.CurrentRows).ToTable

    '            If dtIssue.Rows.Count > 0 Then
    '                For i As Integer = 0 To dtIssue.Rows.Count - 1
    '                    objIssue._Leaveissuetranunkid = CInt(dtIssue.Rows(i)("leaveissuetranunkid"))
    '                    If objIssue.InsertAuditTrailForLeaveIssue(objDataOperation, 3, eZeeDate.convertDate(dtIssue.Rows(i)("leavedate").ToString).ToShortDateString) = False Then
    '                        objDataOperation.ReleaseTransaction(False)
    '                        Return False
    '                    End If
    '                Next
    '            End If
    '            'END FOR INSERT LEAVE ISSUE AUDIT TRAIL
    '        End If

    '        strQ = "delete from lvleaveissue_tran  WHERE leaveissueunkid = (select leaveissueunkid from lvleaveissue_master where formunkid = @formid)"

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@formid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("formunkid"))
    '        objDataOperation.ExecNonQuery(strQ)

    '        strQ = "delete from lvleaveissue_master WHERE formunkid = @form_id"
    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@form_id", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("formunkid"))
    '        objDataOperation.ExecNonQuery(strQ)

    '        'END FOR VOID ISSUE LEAVE

    '        strQ = "Select  Isnull(pendingleavetranunkid,0) pendingleavetranunkid from lvpendingleave_tran " & _
    '               " WHERE formunkid = @form_unkid "

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@form_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("formunkid").ToString)
    '        Dim dtList As DataSet = objDataOperation.ExecQuery(strQ, "List")


    '        'START FOR VOID PROCESS LEAVE
    '        strQ = "delete from lvpendingleave_tran WHERE formunkid = @form_unkid "

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@form_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("formunkid"))
    '        objDataOperation.ExecNonQuery(strQ)
    '        'END FOR VOID PROCESS LEAVE


    '        If dtList.Tables.Count > 0 AndAlso dtList.Tables(0).Rows.Count > 0 Then

    '            For Each dr As DataRow In dtList.Tables(0).Rows
    '                If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lvpendingleave_tran", "pendingleavetranunkid", CInt(dr("pendingleavetranunkid"))) = False Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If

    '            Next

    '        End If


    '        'START FOR VOID LEAVE FORM
    '        strQ = "delete from lvleaveform WHERE formunkid = @formunkid "

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dsList.Tables("GetFormData").Rows(0)("formunkid"))
    '        objDataOperation.ExecNonQuery(strQ)
    '        'END FOR VOID LEAVE FORM

    '        If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lvleaveform", "formunkid", dsList.Tables("GetFormData").Rows(0)("formunkid")) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        objDataOperation.ReleaseTransaction(True)
    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    'Pinkal (24-Aug-2015) -- End

    'END  USED FOR IMPORT ONLY OTHERWISE NOT TO USE

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Shared Function GetLeaveForms(ByVal intEmpId As Integer, Optional ByVal blnFlag As Boolean = False, Optional ByVal xFormId As Integer = 0) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            Dim objDataOperation As New clsDataOperation


            'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .

            'StrQ &= "SELECT " & _
            '       "	 lvleaveform.formunkid " & _
            '       "	,formno " & _
            '       "	,ISNULL(cancelunkid,0) AS  cancelunkid " & _
            '       " FROM lvleaveform " & _
            '       " LEFT JOIN lvcancelform ON lvcancelform.formunkid = lvleaveform.formunkid " & _
            '       " Where lvleaveform.employeeunkid = @EmpId " & _
            '       " AND ISNULL(lvleaveform.isvoid,0) = 0  AND ISNULL(lvcancelform.isvoid,0) = 0"

            If blnFlag = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name, '0' AS cancelunkid UNION "
            End If

            StrQ &= "SELECT " & _
                   "	 lvleaveform.formunkid " & _
                   "	,formno " & _
                   "    ,ISNULL(STUFF((SELECT  ',' + CAST(ISNULL(cancelunkid, 0) AS NVARCHAR(MAX)) " & _
                   "                FROM lvcancelform " & _
                   "                WHERE(ISNULL(lvcancelform.isvoid, 0) = 0) AND lvleaveform.formunkid = lvcancelform.formunkid 	FOR XML PATH (''))  , 1, 1, ''),'0') AS cancelunkid " & _
                   " FROM lvleaveform " & _
                   " Where lvleaveform.employeeunkid = @EmpId " & _
                   " AND ISNULL(lvleaveform.isvoid,0) = 0  "

            If xFormId <> 0 Then
                StrQ &= " AND lvleaveform.formunkid = @formunkid"
            End If


            'Pinkal (28-Nov-2017) -- End


            'Pinkal (03-Jan-2014) -- Start
            'Enhancement : Oman Changes

            'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            'StrQ &= " AND statusunkid IN (1,2) "
            StrQ &= " AND statusunkid IN (1,2,7)"
            'Pinkal (28-Nov-2017) -- End



            'Pinkal (03-Jan-2014) -- End


            'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xFormId)
            'Pinkal (28-Nov-2017) -- End


            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select Leave Form"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLeaveForms", mstrModuleName)
            Return Nothing
        Finally
            dsList.Dispose() : StrQ = String.Empty
        End Try
    End Function

    'Pinkal (02-Dec-2015) -- Start
    'Enhancement - Solving Leave bug in Self Service.

    'Public Shared Sub UpdateLeaveFormStatus(ByVal objDataOperation As clsDataOperation, ByVal intFormunkid As Integer, ByVal intStatusId As Integer, Optional ByVal intUserID As Integer = 0)
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Try
    '        strQ = "Update lvleaveform set statusunkid = @status_unkid where formunkid = @form_unkid"

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@form_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid.ToString)
    '        objDataOperation.AddParameter("@status_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusId.ToString)

    '        objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lvleaveform", "formunkid", intFormunkid, False, intUserID) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Public Sub UpdateLeaveFormStatus(ByVal objDataOperation As clsDataOperation, ByVal intFormunkid As Integer, ByVal intStatusId As Integer, Optional ByVal intUserID As Integer = 0)
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "Update lvleaveform set statusunkid = @status_unkid where formunkid = @form_unkid"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@form_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid.ToString)
            objDataOperation.AddParameter("@status_unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusId.ToString)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lvleaveform", "formunkid", intFormunkid, False, intUserID) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateLeaveFormStatus; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (02-Dec-2015) -- End


    'Pinkal (01-Feb-2014) -- Start
    'Enhancement : TRA Changes

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <returns>Boolean</returns>
    Public Function GetNoOfHolidays(ByVal intEmpId As Integer, ByVal mdtStartdate As Date, Optional ByVal mdtEndDate As Date = Nothing _
                                                  , Optional ByRef arHolidayDate() As String = Nothing, Optional ByVal blnIsssueonWeekend As Boolean = False, Optional ByVal blnConsiderLVHlOnWk As Boolean = False) As Decimal
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mdblCount As Decimal = 0
        Dim arHolidayList As ArrayList = Nothing
        Try

            arHolidayList = New ArrayList

            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()


            strQ = " SELECT COUNT(*) holiday FROM lvemployee_holiday " & _
                      " JOIN lvholiday_master ON  lvemployee_holiday.holidayunkid = lvholiday_master.holidayunkid AND employeeunkid = " & intEmpId & _
                      " WHERE convert(char(8),holidaydate,112) >= '" & eZeeDate.convertDate(mdtStartdate) & "'"

            If mdtEndDate <> Nothing Then
                strQ &= " AND  convert(char(8),holidaydate,112) <= '" & eZeeDate.convertDate(mdtEndDate) & "'"
            End If

            dsList = objDataOperation.ExecQuery(strQ, "Holidaycnt")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then mdblCount = CDec(dsList.Tables(0).Rows(0)("holiday"))

            strQ = " SELECT convert(char(8),holidaydate,112)  holidaydate FROM lvemployee_holiday " & _
               " JOIN lvholiday_master ON  lvemployee_holiday.holidayunkid = lvholiday_master.holidayunkid AND employeeunkid = " & intEmpId & _
               " WHERE convert(char(8),holidaydate,112) >= '" & eZeeDate.convertDate(mdtStartdate) & "'"

            If mdtEndDate <> Nothing Then
                strQ &= " AND  convert(char(8),holidaydate,112) <= '" & eZeeDate.convertDate(mdtEndDate) & "'"
            End If

            dsList = objDataOperation.ExecQuery(strQ, "Holidaycnt")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (01-Feb-2014) -- Start
            'Enhancement : TRA Changes

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                'ReDim arHolidayDate(dsList.Tables(0).Rows.Count - 1)
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    'Pinkal (21-Jul-2014) -- Start
                    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                    If arHolidayList.Contains(dsList.Tables(0).Rows(i)("holidaydate").ToString()) = True Then Continue For
                    'Pinkal (21-Jul-2014) -- End
                    arHolidayList.Add(dsList.Tables(0).Rows(i)("holidaydate").ToString())
                Next
            End If


            If blnIsssueonWeekend AndAlso blnConsiderLVHlOnWk = False Then

                Dim objShift As New clsNewshift_master
                Dim objShifttran As New clsshift_tran

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim objEmp As New clsEmployee_Master
                'objEmp._Employeeunkid = intEmpId

                'objShift._Shiftunkid = objEmp._Shiftunkid
                'S.SANDEEP [04 JUN 2015] -- END

                Dim objEmpShiftTran As New clsEmployee_Shift_Tran

                If mdtStartdate <> Nothing AndAlso mdtEndDate <> Nothing Then
                    Dim intDays As Integer = DateDiff(DateInterval.Day, mdtStartdate.Date, mdtEndDate.AddDays(1).Date)
                    For i As Integer = 0 To intDays - 1

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objShift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(mdtStartdate.AddDays(i).Date, objEmp._Employeeunkid)
                        objShift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(mdtStartdate.AddDays(i).Date, intEmpId)
                        'S.SANDEEP [04 JUN 2015] -- END

                        objShifttran.GetShiftTran(objShift._Shiftunkid)

                        'Pinkal (02-Dec-2015) -- Start
                        'Enhancement - Solving FDRC Leave bug in Self Service.

                        'Pinkal (29-Sep-2016) -- Start
                        'Enhancement - Implementing ACB Changes in TnA Module.
                        'Dim strWeekName As String = Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.GetDayName(Weekday(mdtStartdate.AddDays(i).Date, FirstDayOfWeek.Sunday))
                        Dim strWeekName As String = WeekdayName(Weekday(mdtStartdate.AddDays(i).Date), False, FirstDayOfWeek.Sunday)
                        'Pinkal (29-Sep-2016) -- End


                        'Pinkal (02-Dec-2015) -- End
                        If objShifttran._dtShiftday IsNot Nothing Then
                            'Pinkal (02-Dec-2015) -- Start
                            'Enhancement - Solving FDRC Leave bug in Self Service.
                            'Dim drrow() As DataRow = objShifttran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtStartdate.AddDays(i).DayOfWeek.ToString()) & " AND isweekend = 1 ")
                            Dim drrow() As DataRow = objShifttran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1 ")
                            'Pinkal (02-Dec-2015) -- End
                            If drrow.Length > 0 Then
                                'If mdblCount > 0 Then mdblCount -= 1
                                If mdblCount > 0 Then
                                    arHolidayList.Remove(eZeeDate.convertDate(mdtStartdate.AddDays(i).Date))
                                End If
                            End If
                        End If
                    Next
                End If
            End If

            If arHolidayList IsNot Nothing Then
                arHolidayDate = CType(arHolidayList.ToArray(GetType(String)), String())
            End If

            mdblCount = arHolidayDate.Length

            'Pinkal (01-Feb-2014) -- End

            Return mdblCount
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNoOfHolidays", mstrModuleName)
        End Try
    End Function

    'Pinkal (01-Feb-2014) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function GetApproverPendingLeaveFormCount(ByVal intApproverID As Integer, ByVal mstrEmpID As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mintCount As Integer = 0
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()


            'Pinkal (17-Jun-2015) -- Start
            'Enhancement - WORKING ON TRA LEAVE APPROVER ACTIVE AND DEACTIVATE FEATURE.

            'strQ = " SELECT COUNT(*) PendingFormCount FROM lvleaveform  " & _
            '          " JOIN lvpendingleave_tran ON lvleaveform.formunkid = lvpendingleave_tran.formunkid AND approvertranunkid  = " & intApproverID & _
            '          " WHERE lvleaveform.statusunkid = 2 AND lvleaveform.employeeunkid IN (" & mstrEmpID & " ) AND lvleaveform.isvoid = 0 "


            strQ = " SELECT COUNT(*) PendingFormCount FROM lvleaveform  " & _
                      " JOIN lvpendingleave_tran ON lvleaveform.formunkid = lvpendingleave_tran.formunkid AND approvertranunkid  = " & intApproverID & _
                      " WHERE lvleaveform.statusunkid = 2  AND lvleaveform.isvoid = 0  AND lvpendingleave_tran.isvoid = 0 "

            If mstrEmpID.Trim.Length > 0 Then
                strQ &= " AND lvleaveform.employeeunkid IN (" & mstrEmpID & " )"
            End If

            'Pinkal (17-Jun-2015) -- End

            dsList = objDataOperation.ExecQuery(strQ, "PedingCount")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCount = CInt(dsList.Tables(0).Rows(0)("PendingFormCount"))

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverPendingLeaveFormCount", mstrModuleName)
        End Try
        Return mintCount
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function GetApproverPendingLeaveForm(ByVal intApproverID As Integer, ByVal mstrEmpID As String, Optional ByVal mobjDataOperation As clsDataOperation = Nothing) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrFormID As String = String.Empty
        Try
            Dim objDataOperation As clsDataOperation = Nothing

            If mobjDataOperation IsNot Nothing Then
                objDataOperation = mobjDataOperation
            Else
                objDataOperation = New clsDataOperation
            End If

            objDataOperation.ClearParameters()



            'Pinkal (01-Nov-2014) -- Start
            'Enhancement -  CHANGES FOR FINCA TZ FOR LEAVE BUGS.

            'strQ = " SELECT STUFF( " & _
            '           " (SELECT  ',' +  CAST(lvleaveform.formunkid AS NVARCHAR(max)) " & _
            '           "   FROM lvleaveform " & _
            '           "   JOIN lvpendingleave_tran ON lvleaveform.formunkid = lvpendingleave_tran.formunkid AND approvertranunkid  = " & intApproverID & _
            '           " WHERE lvleaveform.statusunkid = 2 AND lvleaveform.employeeunkid IN (" & mstrEmpID & ") AND lvleaveform.isvoid = 0  AND lvpendingleave_tran.isvoid = 0 " & _
            '           " ORDER BY lvleaveform.formunkid  FOR XML PATH('')),1,1,'') AS CSV"

            strQ = " SELECT STUFF( " & _
                      " (SELECT  ',' +  CAST(lvleaveform.formunkid AS NVARCHAR(max)) " & _
                      "   FROM lvleaveform " & _
                      "   JOIN lvpendingleave_tran ON lvleaveform.formunkid = lvpendingleave_tran.formunkid AND approvertranunkid  = " & intApproverID & _
             " WHERE lvleaveform.statusunkid in (1,2) AND lvleaveform.employeeunkid IN (" & mstrEmpID & ") AND lvleaveform.isvoid = 0  AND lvpendingleave_tran.isvoid = 0 " & _
                      " ORDER BY lvleaveform.formunkid  FOR XML PATH('')),1,1,'') AS CSV"

            'Pinkal (01-Nov-2014) -- End

            dsList = objDataOperation.ExecQuery(strQ, "FormList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrFormID = dsList.Tables(0).Rows(0)("CSV").ToString()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverPendingLeaveForm", mstrModuleName)
        End Try
        Return mstrFormID
    End Function

    'S.SANDEEP [30 JAN 2016] -- START
    'THIS METHOD COMMENTED HERE DUE TO THIS METHOD IS ADJUSTED IN APPROVER MASTER CLASS IN [GetEmployeeApprover]
    'AND LEFT JOIN IS INCLUDED.
    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <returns>Boolean</returns>
    'Public Function GetEmployeeApproverPriority(ByVal intEmployeeunkid As Integer, ByVal intLeaveTypeID As Integer, Optional ByVal blnLeaveApproverForLeaveType As String = "") As DataTable
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation
    '    Try

    '        strQ = " SELECT lvleaveapprover_master.approverunkid, ISNULL(leaveapproverunkid, 0) leaveapproverunkid , ISNULL(lvleaveapprover_tran.employeeunkid,0) employeeunkid, " & _
    '                   " ISNULL(h1.firstname, '') + ' ' + ISNULL(h1.surname, '') AS approvername," & _
    '                   " ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.surname, '') AS employeename,lvapproverlevel_master.priority " & _
    '                   " FROM lvleaveapprover_master " & _
    '                   " JOIN lvapproverlevel_master ON lvleaveapprover_master.levelunkid = lvapproverlevel_master.levelunkid " & _
    '                   " JOIN lvleaveapprover_tran ON lvleaveapprover_tran.approverunkid = lvleaveapprover_master.approverunkid AND lvleaveapprover_tran.isvoid = 0 " & _
    '                   " LEFT JOIN hremployee_master h1 ON h1.employeeunkid = lvleaveapprover_master.leaveapproverunkid " & _
    '                   " LEFT JOIN hremployee_master h2 ON h2.employeeunkid = lvleaveapprover_tran.employeeunkid "



    '        If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
    '            blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
    '        End If


    '        If CBool(blnLeaveApproverForLeaveType) = True And intLeaveTypeID > 0 Then

    '            strQ &= "  JOIN lvapprover_leavetype_mapping ON lvleaveapprover_master.approverunkid = lvapprover_leavetype_mapping.approverunkid AND lvapprover_leavetype_mapping.isvoid = 0 " & _
    '                        "  JOIN lvleavetype_master ON lvapprover_leavetype_mapping.leavetypeunkid = lvleavetype_master.leavetypeunkid AND lvapprover_leavetype_mapping.leavetypeunkid = @leavetypeId "

    '            objDataOperation.AddParameter("@leavetypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeID.ToString)

    '        End If

    '        strQ &= " WHERE lvleaveapprover_master.isvoid = 0  AND lvleaveapprover_tran.employeeunkid = @employeeunkid"

    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid.ToString)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dsList.Tables.Count > 0 Then
    '            Return dsList.Tables("List")
    '        Else
    '            Return Nothing
    '        End If

    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApproverPriority; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function
    'S.SANDEEP [30 JAN 2016] -- END




    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Sub SendMailToApprover(ByVal intEmpId As Integer, ByVal intLeaveTypeID As Integer, _
    '                              ByVal intFormUnkId As Integer, _
    '                              ByVal strFormNo As String, _
    '                              ByVal blnDelete As Boolean, _
    '                              Optional ByVal blnLeaveApproverForLeaveType As String = "", _
    '                              Optional ByVal intCompanyUnkId As Integer = 0, _
    '                              Optional ByVal strArutiSelfServiceURL As String = "", _
    '                              Optional ByVal iLoginTypeId As Integer = 0, _
    '                              Optional ByVal iLoginEmployeeId As Integer = 0, _
    '                              Optional ByVal iUserId As Integer = 0) 'S.SANDEEP [ 28 JAN 2014 ] -- START {iLoginTypeId,iLoginEmployeeId,iUserId} -- END
    '    '                         'Sohail (01 Jan 2013) - [intCompanyUnkId, strArutiSelfServiceURL]

    '    'Sohail (01 Jan 2013) -- Start
    '    'TRA - ENHANCEMENT
    '    Dim objUserMap As New clsapprover_Usermapping
    '    Dim objLeavePending As New clspendingleave_Tran
    '    Dim dsList As DataSet = Nothing
    '    Dim strLink As String
    '    'Sohail (01 Jan 2013) -- End

    '    Try

    '        'Sohail (01 Jan 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        If intCompanyUnkId <= 0 Then intCompanyUnkId = Company._Object._Companyunkid
    '        If strArutiSelfServiceURL = "" Then strArutiSelfServiceURL = ConfigParameter._Object._ArutiSelfServiceURL
    '        'Sohail (01 Jan 2013) -- End

    '        Dim objNet As New clsNetConnectivity
    '        If objNet._Conected = False Then Exit Sub

    '        If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
    '            blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
    '        End If
    '        Dim dtApprover As DataTable = GetEmployeeApproverPriority(intEmpId, intLeaveTypeID, blnLeaveApproverForLeaveType)

    '        If dtApprover.Rows.Count <= 0 Then Exit Sub

    '        Dim objEmp As New clsEmployee_Master
    '        Dim objMail As New clsSendMail
    '        Dim drRow() As DataRow = dtApprover.Select("priority = Min(priority)")

    '        If drRow.Length > 0 Then


    '            'Pinkal (06-Mar-2014) -- Start
    '            'Enhancement : Oman Changes Add Leave Type Name in Email Notification for leave application as per Mr.gogi's Request.

    '            Dim objLeaveType As New clsleavetype_master
    '            objLeaveType._Leavetypeunkid = intLeaveTypeID


    '            For i As Integer = 0 To drRow.Length - 1

    '                objEmp._Employeeunkid = CInt(drRow(i)("leaveapproverunkid"))

    '                Dim strMessage As String = ""

    '                strMessage = "<HTML> <BODY>"

    '                strMessage &= Language.getMessage(mstrModuleName, 14, "Dear") & " " & drRow(i)("approvername").ToString() & ", <BR><BR>"

    '                If blnDelete Then

    '                    objMail._Subject = Language.getMessage(mstrModuleName, 5, "Leave Application form deleted")

    '                    strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 6, "This is to inform you that leave application no") & " " & strFormNo.Trim & _
    '                                          Language.getMessage(mstrModuleName, 7, " with ") & objLeaveType._Leavename & Language.getMessage(mstrModuleName, 44, " for ") & drRow(i)("employeename").ToString() & Language.getMessage(mstrModuleName, 8, " who was seeking your approval has been deleted. Kindly take a note of it.")
    '                Else

    '                    objMail._Subject = Language.getMessage(mstrModuleName, 9, "Notification for approving Leave Application form")

    '                    strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 10, "This is the notification for approving leave application no") & " " & strFormNo.Trim & _
    '                                     Language.getMessage(mstrModuleName, 7, " with ") & objLeaveType._Leavename & Language.getMessage(mstrModuleName, 45, " of ") & drRow(i)("employeename").ToString() & Language.getMessage(mstrModuleName, 12, " having Start date : ") & _
    '                                       mdtStartdate.Date & Language.getMessage(mstrModuleName, 13, " and End Date : ") & mdtReturndate.Date & "."

    '                    'Sohail (01 Jan 2013) -- Start
    '                    'TRA - ENHANCEMENT
    '                    objUserMap.GetData(enUserType.Approver, CInt(drRow(i)("approverunkid")))
    '                    dsList = objLeavePending.GetList("PendingLeave", True, , , , , "lvpendingleave_tran.approverunkid = " & CInt(drRow(i)("leaveapproverunkid")) & " AND lvpendingleave_tran.formunkid = " & intFormUnkId & " ")
    '                    Dim intPendingleavetranunkid As Integer = 0
    '                    If dsList.Tables(0).Rows.Count > 0 Then
    '                        intPendingleavetranunkid = CInt(dsList.Tables(0).Rows(0)("pendingleavetranunkid"))
    '                    End If
    '                    strLink = strArutiSelfServiceURL & "/Leave/ProcessPendingLeave.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyUnkId.ToString & "|" & objUserMap._Userunkid & "|" & intEmpId.ToString & "|" & drRow(i)("leaveapproverunkid").ToString & "|" & intFormUnkId.ToString & "|" & intPendingleavetranunkid.ToString))
    '                    'Sohail (01 Jan 2013) -- End

    '                    strMessage &= "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 27, "Please click on the following link to approve leave.")
    '                    strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
    '                End If

    '                strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"

    '                strMessage &= "</BODY></HTML>"

    '                objMail._Message = strMessage
    '                objMail._ToEmail = objEmp._Email
    '                'S.SANDEEP [ 28 JAN 2014 ] -- START
    '                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
    '                If mstrWebFrmName.Trim.Length > 0 Then
    '                    objMail._Form_Name = mstrWebFrmName
    '                End If
    '                objMail._LogEmployeeUnkid = iLoginEmployeeId
    '                objMail._OperationModeId = iLoginTypeId
    '                objMail._UserUnkid = iUserId
    '                objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
    '                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT
    '                'S.SANDEEP [ 28 JAN 2014 ] -- END
    '                objMail.SendMail()

    '            Next

    '            'Pinkal (06-Mar-2014) -- End

    '        End If


    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "SendMailToApprover", mstrModuleName)
    '    End Try
    'End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    Public Sub SendMailToApprover(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                                  , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                  , ByVal mdtEmployeeAsOnDate As Date _
                                                  , ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                                  , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                  , ByVal intEmpId As Integer, ByVal intLeaveTypeID As Integer _
                                                  , ByVal intFormUnkId As Integer _
                                                  , ByVal strFormNo As String _
                                                  , ByVal blnDelete As Boolean _
                                                  , Optional ByVal blnLeaveApproverForLeaveType As String = "" _
                                                  , Optional ByVal strArutiSelfServiceURL As String = "" _
                                                  , Optional ByVal iLoginTypeId As Integer = 0 _
                                                  , Optional ByVal iLoginEmployeeId As Integer = 0 _
                                                  , Optional ByVal mblnSkipApproverFlow As Boolean = False _
                                                  , Optional ByVal mstrDeleterName As String = "")


        'Pinkal (01-Apr-2019) --  'Enhancement - Working on Leave Changes for NMB.[, Optional ByVal mstrDeleterName As String = ""]

        'Pinkal (01-Oct-2018) --  'Enhancement - Leave Enhancement for NMB.[ , Optional ByVal mblnSkipApproverFlow As Boolean = False]


        Dim objUserMap As New clsapprover_Usermapping
        Dim objLeavePending As New clspendingleave_Tran
        Dim dsList As DataSet = Nothing
        Dim strLink As String
        Dim objLeaveApprover As New clsleaveapprover_master

        Try
            If xCompanyUnkid <= 0 Then xCompanyUnkid = Company._Object._Companyunkid
            If strArutiSelfServiceURL = "" Then strArutiSelfServiceURL = ConfigParameter._Object._ArutiSelfServiceURL

            Dim objNet As New clsNetConnectivity
            If objNet._Conected = False Then Exit Sub

            If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
                blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
            End If


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            Dim dtApprover As DataTable = Nothing
            If mblnSkipApproverFlow = False Then
                dtApprover = objLeaveApprover.GetEmployeeApprover(xDatabaseName, xYearUnkid, xCompanyUnkid, mdtEmployeeAsOnDate, xIncludeIn_ActiveEmployee, -1, intEmpId, , intLeaveTypeID, blnLeaveApproverForLeaveType)
            Else
                Dim objReportingTo As New clsReportingToEmployee
                objReportingTo._EmployeeUnkid(mdtEmployeeAsOnDate) = mintEmployeeunkid
                dtApprover = objReportingTo._RDataTable.Copy()
                objReportingTo = Nothing
            End If
            'Pinkal (01-Oct-2018) -- End

            If dtApprover.Rows.Count <= 0 Then Exit Sub

            Dim objMail As New clsSendMail

            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            Dim drRow() As DataRow = Nothing
            If mblnSkipApproverFlow = False Then
                drRow = dtApprover.Select("priority = Min(priority)")
            Else
                drRow = dtApprover.Select("ishierarchy = 1")
            End If
            'Pinkal (01-Oct-2018) -- End



            If drRow.Length > 0 Then
                Dim objLeaveType As New clsleavetype_master
                objLeaveType._Leavetypeunkid = intLeaveTypeID

                For i As Integer = 0 To drRow.Length - 1

                    Dim strMessage As String = ""

                    strMessage = "<HTML> <BODY>"


                    'Pinkal (13-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                    'If mblnSkipApproverFlow = False Then
                    '    strMessage &= Language.getMessage(mstrModuleName, 14, "Dear") & " " & drRow(i)("employeename").ToString() & ", <BR><BR>"
                    'Else
                    '    strMessage &= Language.getMessage(mstrModuleName, 14, "Dear") & " " & drRow(i)("ename").ToString() & ", <BR><BR>"
                    'End If
                    Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                    If mblnSkipApproverFlow = False Then
                        strMessage &= Language.getMessage(mstrModuleName, 14, "Dear") & " " & info1.ToTitleCase(drRow(i)("employeename").ToString().ToLower()) & ", <BR><BR>"
                    Else
                        strMessage &= Language.getMessage(mstrModuleName, 14, "Dear") & " " & info1.ToTitleCase(drRow(i)("ename").ToString().ToLower()) & ", <BR><BR>"
                    End If
                    'Pinkal (13-Mar-2019) -- End



                    If blnDelete Then

                        objMail._Subject = Language.getMessage(mstrModuleName, 5, "Leave Application form deleted")


                        'Pinkal (13-Mar-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 6, "This is to inform you that leave application no") & " " & strFormNo.Trim & _
                        '                      Language.getMessage(mstrModuleName, 7, " with ") & objLeaveType._Leavename & Language.getMessage(mstrModuleName, 44, " for ") & drRow(i)("assigndemployeename").ToString() & Language.getMessage(mstrModuleName, 8, " who was seeking your approval has been deleted. Kindly take a note of it.")


                        'Pinkal (15-Mar-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                        'strMessage &= Language.getMessage(mstrModuleName, 6, "This is to inform you that leave application no") & " " & strFormNo.Trim & " " & _
                        '                      Language.getMessage(mstrModuleName, 7, "with") & " " & objLeaveType._Leavename & " " & Language.getMessage(mstrModuleName, 44, "for") & " " & drRow(i)("assigndemployeename").ToString() & Language.getMessage(mstrModuleName, 8, " who was seeking your approval has been deleted. Kindly take a note of it.")


                        'Pinkal (01-Apr-2019) -- Start
                        'Enhancement - Working on Leave Changes for NMB.

                        strMessage &= Language.getMessage(mstrModuleName, 6, "This is to inform you that leave application no") & " <B>(" & strFormNo.Trim & ")</B> " & _
                                             Language.getMessage(mstrModuleName, 7, "with") & " <B>(" & objLeaveType._Leavename & ")</B> " & Language.getMessage(mstrModuleName, 44, "for") & " " & info1.ToTitleCase(drRow(i)("assigndemployeename").ToString().ToLower()) & " "

                        If mstrDeleterName.Trim.Length > 0 Then
                            strMessage &= Language.getMessage(mstrModuleName, 58, "who was seeking your approval has been deleted by") & " <B>(" & info1.ToTitleCase(mstrDeleterName.ToString().ToLower()) & ")</B>. " & Language.getMessage(mstrModuleName, 59, "Kindly take a note of it.")
                        Else
                            strMessage &= Language.getMessage(mstrModuleName, 8, " who was seeking your approval has been deleted. Kindly take a note of it.")
                        End If


                        'Pinkal (01-Apr-2019) -- End



                        'Pinkal (15-Mar-2019) -- End

                        'Pinkal (13-Mar-2019) -- End
                    Else

                        'Pinkal (01-Oct-2018) -- Start
                        'Enhancement - Leave Enhancement for NMB.

                        If mblnSkipApproverFlow = False Then

                        objMail._Subject = Language.getMessage(mstrModuleName, 9, "Notification for approving Leave Application form")


                            'Pinkal (13-Mar-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                            'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 10, "This is a notification for approving leave application no") & " " & strFormNo.Trim & _
                            '                 Language.getMessage(mstrModuleName, 7, " with ") & objLeaveType._Leavename & Language.getMessage(mstrModuleName, 45, " of ") & drRow(i)("assigndemployeename").ToString() & Language.getMessage(mstrModuleName, 12, " having Start date : ") & _
                            '                   mdtStartdate.Date & Language.getMessage(mstrModuleName, 13, " and End Date : ") & mdtReturndate.Date & "."


                            'Pinkal (15-Mar-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                            'strMessage &= Language.getMessage(mstrModuleName, 10, "This is a notification for approving leave application no") & " " & strFormNo.Trim() & " " & _
                            '                Language.getMessage(mstrModuleName, 7, "with") & " " & objLeaveType._Leavename & " " & Language.getMessage(mstrModuleName, 45, "of") & " " & drRow(i)("assigndemployeename").ToString() & Language.getMessage(mstrModuleName, 12, " having Start date : ") & _
                            '                  mdtStartdate.Date & Language.getMessage(mstrModuleName, 13, "and End Date : ") & mdtReturndate.Date & "."


                            'Pinkal (01-Apr-2019) -- Start
                            'Enhancement - Working on Leave Changes for NMB.

                            strMessage &= Language.getMessage(mstrModuleName, 10, "This is a notification for approving leave application no") & " <B>(" & strFormNo.Trim() & ")</B> " & _
                                                   Language.getMessage(mstrModuleName, 7, "with") & " <B>(" & objLeaveType._Leavename & ")</B> " & Language.getMessage(mstrModuleName, 45, "of") & " " & info1.ToTitleCase(drRow(i)("assigndemployeename").ToString().ToLower()) & " " & Language.getMessage(mstrModuleName, 12, " having Start date : ") & _
                                                   " " & mdtStartdate.Date & " " & Language.getMessage(mstrModuleName, 13, "and End Date : ") & " " & mdtReturndate.Date & "."

                            'Pinkal (01-Apr-2019) -- End

                            'Pinkal (15-Mar-2019) -- End

                            'Pinkal (13-Mar-2019) -- End

                        objUserMap.GetData(enUserType.Approver, CInt(drRow(i)("approverunkid")))
                        dsList = objLeavePending.GetList("PendingLeave", xDatabaseName, xUserUnkid, xYearUnkid _
                                                                         , xCompanyUnkid, mdtEmployeeAsOnDate, xOnlyApproved, xIncludeIn_ActiveEmployee _
                                                                         , True, "lvpendingleave_tran.approverunkid = " & CInt(drRow(i)("leaveapproverunkid")) & " AND lvpendingleave_tran.formunkid = " & intFormUnkId & " ")

                        Dim intPendingleavetranunkid As Integer = 0
                        If dsList.Tables(0).Rows.Count > 0 Then
                            intPendingleavetranunkid = CInt(dsList.Tables(0).Rows(0)("pendingleavetranunkid"))
                        End If
                        strLink = strArutiSelfServiceURL & "/Leave/wPg_ProcessLeaveAddEdit.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(xCompanyUnkid.ToString & "|" & objUserMap._Userunkid & "|" & intEmpId.ToString & "|" & drRow(i)("leaveapproverunkid").ToString & "|" & intFormUnkId.ToString & "|" & intPendingleavetranunkid.ToString & "|" & drRow(i)("isexternalapprover").ToString))


                            'Pinkal (13-Mar-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                            'strMessage &= "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 27, "Please click on the following link to approve leave.")
                            'strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"

                            strMessage &= "<BR></BR><BR></BR>" & Language.getMessage(mstrModuleName, 27, "Please click on the following link to approve leave.")
                            strMessage &= "<BR></BR><a href='" & strLink & "'>" & strLink & "</a>"

                            'Pinkal (13-Mar-2019) -- End

                        Else

                            objMail._Subject = Language.getMessage(mstrModuleName, 51, "Issued Leave Application form notification")


                            'Pinkal (13-Mar-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                            'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 52, " This is to notify that leave application no") & " " & strFormNo.Trim & _
                            '                           Language.getMessage(mstrModuleName, 7, " with ") & " " & objLeaveType._Leavename & Language.getMessage(mstrModuleName, 45, " of ") & " " & drRow(i)("EmployeeName") & Language.getMessage(mstrModuleName, 12, " having Start date : ")

                            'strMessage &= mdtStartdate.Date & Language.getMessage(mstrModuleName, 13, " and End Date : ") & mdtReturndate.Date & Language.getMessage(mstrModuleName, 53, " has been issued.")


                            'Pinkal (15-Mar-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                            'strMessage &= Language.getMessage(mstrModuleName, 52, " This is to notify that leave application no") & " " & strFormNo.Trim & " " & _
                            '                           Language.getMessage(mstrModuleName, 7, "with") & " " & objLeaveType._Leavename & " " & Language.getMessage(mstrModuleName, 45, "of") & " " & drRow(i)("EmployeeName") & Language.getMessage(mstrModuleName, 12, " having Start date : ")


                            'Pinkal (01-Apr-2019) -- Start
                            'Enhancement - Working on Leave Changes for NMB.

                            'strMessage &= Language.getMessage(mstrModuleName, 52, " This is to notify that leave application no") & " " & strFormNo.Trim & " " & _
                            '                       Language.getMessage(mstrModuleName, 7, "with") & " " & objLeaveType._Leavename & " " & Language.getMessage(mstrModuleName, 45, "of") & " " & info1.ToTitleCase(drRow(i)("EmployeeName").ToString().ToLower()) & " " & Language.getMessage(mstrModuleName, 12, " having Start date : ")

                            strMessage &= Language.getMessage(mstrModuleName, 52, " This is to notify that leave application no") & " <B>(" & strFormNo.Trim & ")</B> " & _
                                                Language.getMessage(mstrModuleName, 7, "with") & " <B>(" & objLeaveType._Leavename & ")</B> " & Language.getMessage(mstrModuleName, 45, "of") & " " & info1.ToTitleCase(drRow(i)("EmployeeName").ToString().ToLower()) & " " & Language.getMessage(mstrModuleName, 12, " having Start date : ")

                            'Pinkal (01-Apr-2019) -- End


                            'Pinkal (15-Mar-2019) -- End




                            strMessage &= mdtStartdate.Date & " " & Language.getMessage(mstrModuleName, 13, "and End Date : ") & mdtReturndate.Date & " " & Language.getMessage(mstrModuleName, 53, "has been issued.")

                            'Pinkal (13-Mar-2019) -- End

                        End If

                        'Pinkal (01-Oct-2018) -- End

                    End If

                    strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"

                    strMessage &= "</BODY></HTML>"

                    objMail._Message = strMessage

                    'Pinkal (01-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.
                    If mblnSkipApproverFlow = False Then
                    objMail._ToEmail = drRow(i)("approveremail")
                    Else
                        objMail._ToEmail = drRow(i)("ReportingToEmail")
                    End If
                    'Pinkal (01-Oct-2018) -- End


                    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                    If mstrWebFrmName.Trim.Length > 0 Then
                        objMail._Form_Name = mstrWebFrmName
                        objMail._WebClientIP = mstrWebClientIP
                        objMail._WebHostName = mstrWebHostName
                    End If
                    objMail._LogEmployeeUnkid = iLoginEmployeeId
                    objMail._OperationModeId = iLoginTypeId
                    objMail._UserUnkid = xUserUnkid

                    'Pinkal (01-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.
                    If mblnSkipApproverFlow = False Then
                    objMail._SenderAddress = IIf(drRow(i)("approveremail") = "", drRow(i)("employeename"), drRow(i)("approveremail"))
                    Else
                        objMail._SenderAddress = IIf(drRow(i)("ReportingToEmail") = "", drRow(i)("ename"), drRow(i)("ReportingToEmail"))
                    End If
                    'Pinkal (01-Oct-2018) -- End


                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT
                    objMail.SendMail(xCompanyUnkid)
                Next
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SendMailToApprover", mstrModuleName)
        End Try
    End Sub

    'Pinkal (03-May-2019) -- Start
    'Enhancement - Working on Leave UAT Changes for NMB.


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    Public Sub SendMailToReliever(ByVal xCompanyUnkid As Integer, ByVal intRelieverEmpId As Integer, ByVal intLeaveTypeID As Integer _
                                                  , ByVal intFormUnkId As Integer, ByVal strFormNo As String, ByVal xEmployeeName As String _
                                                  , ByVal mdtStartDate As Date, ByVal mdtEndDate As Date _
                                                  , ByVal xUserUnkid As Integer, ByVal xStatusId As Integer, Optional ByVal strCancelDates As String = "" _
                                                  , Optional ByVal iLoginTypeId As Integer = 0)


        'Pinkal (25-May-2019) -- Start 'Enhancement - NMB FINAL LEAVE UAT CHANGES.[ByVal xStatusId As Integer, Optional ByVal strCancelDates As String = ""]

        Dim objLeavePending As New clspendingleave_Tran
        Dim dsList As DataSet = Nothing

        Try

            Dim objNet As New clsNetConnectivity
            If objNet._Conected = False Then Exit Sub


            Dim mstrLeaveName As String = ""
            Dim objLeaveType As New clsleavetype_master
            objLeaveType._Leavetypeunkid = intLeaveTypeID
            mstrLeaveName = objLeaveType._Leavename
            objLeaveType = Nothing

            Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo

            Dim objEmp As New clsEmployee_Master
            objEmp._Employeeunkid(mdtEndDate) = intRelieverEmpId

            Dim objMail As New clsSendMail
            objMail._Subject = Language.getMessage(mstrModuleName, 63, "Relieving Notification for") & " " & info1.ToTitleCase(xEmployeeName.ToLower())

            Dim strMessage As String = ""
            strMessage = "<HTML> <BODY>"

            strMessage &= Language.getMessage(mstrModuleName, 14, "Dear") & " " & info1.ToTitleCase(objEmp._Firstname.ToLower() & " " & objEmp._Surname.ToLower()) & ", <BR><BR>"


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.

            If xStatusId = 1 OrElse xStatusId = 7 Then  'APPROVED OR ISSUED
            strMessage &= Language.getMessage(mstrModuleName, 60, "This is to inform you that you will be relieving") & " <B>(" & info1.ToTitleCase(xEmployeeName.ToLower()) & ")</B> " & _
                                 Language.getMessage(mstrModuleName, 61, "who will be on leave from") & " " & mdtStartDate.Date & " " & Language.getMessage(mstrModuleName, 19, "To") & " " & mdtEndDate.Date & "."
            ElseIf xStatusId = 6 Then  'CANCELLED

                If strCancelDates.Length > 0 Then
                    Dim arStrCancelDate As String() = strCancelDates.Split(CChar(","))

                    If arStrCancelDate.Length > 0 Then
                        strCancelDates = ""
                        For i As Integer = 0 To arStrCancelDate.Length - 1
                            strCancelDates &= eZeeDate.convertDate(arStrCancelDate(i)).ToShortDateString & ","
                        Next

                        If strCancelDates.Length > 0 Then
                            strCancelDates = strCancelDates.Substring(0, strCancelDates.Length - 1)
                        End If
                    End If

                    strMessage &= Language.getMessage(mstrModuleName, 67, "This is to inform you that , the leave for") & " <B>(" & info1.ToTitleCase(xEmployeeName.ToLower()) & ")</B> " & _
                                           Language.getMessage(mstrModuleName, 68, "has been Cancelled for these date(s):") & " " & strCancelDates & "." & Language.getMessage(mstrModuleName, 59, "Kindly take a note of it.")

                End If

            End If
            'Pinkal (25-May-2019) -- End


            strMessage &= "<BR></BR>" & Language.getMessage(mstrModuleName, 62, "Regards") & "."

            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"

            strMessage &= "</BODY></HTML>"

            objMail._Message = strMessage
            objMail._ToEmail = objEmp._Email

            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
            If mstrWebFrmName.Trim.Length > 0 Then
                objMail._Form_Name = mstrWebFrmName
                objMail._WebClientIP = mstrWebClientIP
                objMail._WebHostName = mstrWebHostName
            End If
            objMail._LogEmployeeUnkid = -1
            objMail._OperationModeId = iLoginTypeId
            objMail._UserUnkid = xUserUnkid

            objMail._SenderAddress = objEmp._Email

            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT
            objMail.SendMail(xCompanyUnkid)

            objEmp = Nothing

            '************************* START TO SET LANGUAGE FOR RELIEVER LEAVE END NOTIFICATION WHICH IS NOT USED IN ABOVE SECTION.IT IS USED IN ARUTI REFLEX FOR RELIEVER ONLY.
            strMessage = ""
            strMessage = Language.getMessage(mstrModuleName, 64, "This is to notify you that")
            strMessage = Language.getMessage(mstrModuleName, 65, "will be ending on")
            strMessage = Language.getMessage(mstrModuleName, 66, "hence He/She is schedule to report tomorrow.")
            strMessage = ""

            '************************* END TO SET LANGUAGE FOR RELIEVER LEAVE END NOTIFICATION WHICH IS NOT USED IN ABOVE SECTION.IT IS USED IN ARUTI REFLEX FOR RELIEVER ONLY.

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendMailToReliever; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (03-May-2019) -- End



    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    Public Sub SendMailToEmployee(ByVal intEmpId As Integer, _
                                  ByVal mstrLeaveType As String, _
                                  ByVal dtStartdate As Date, _
                                  ByVal dtEndDate As Date, _
                                  ByVal intStatusID As Integer, _
                                  ByVal intCompanyUnkId As Integer, _
                                  Optional ByVal strCancelDate As String = "", _
                                  Optional ByVal strPath As String = "", _
                                  Optional ByVal strFileName As String = "", _
                                  Optional ByVal iLoginTypeId As Integer = 0, _
                                  Optional ByVal iLoginEmployeeId As Integer = 0, _
                                  Optional ByVal iUserId As Integer = 0, _
                                  Optional ByVal mstrRejectionRemark As String = "", _
                                  Optional ByVal intClaimMstId As Integer = 0, _
                                  Optional ByVal mstrLeaveFormNo As String = "")

        'Pinkal (01-Apr-2019) -- 'Enhancement - Working on Leave Changes for NMB.[ Optional ByVal mstrLeaveFormNo As String = ""]


        Try

            Dim objNet As New clsNetConnectivity
            If objNet._Conected = False Then Exit Sub

            If intStatusID = 2 Then Exit Sub

            Dim mstrStatus As String = ""


            Select Case intStatusID
                Case 1
                    mstrStatus = Language.getMessage("clsMasterData", 110, "Approved")
                Case 3
                    mstrStatus = Language.getMessage("clsMasterData", 112, "Rejected")
                Case 4
                    mstrStatus = Language.getMessage("clsMasterData", 113, "Re-Scheduled")
                Case 6
                    mstrStatus = Language.getMessage("clsMasterData", 115, "Cancelled")

                    'Pinkal (01-Feb-2014) -- Start
                    'Enhancement : TRA Changes
                Case 7
                    mstrStatus = Language.getMessage("clsMasterData", 276, "Issued")
                    'Pinkal (01-Feb-2014) -- End


            End Select


            'If intStatusID <> 6 Then

            If strCancelDate.Length > 0 Then
                Dim arStrCancelDate As String() = strCancelDate.Split(CChar(","))

                If arStrCancelDate.Length > 0 Then
                    strCancelDate = ""
                    For i As Integer = 0 To arStrCancelDate.Length - 1
                        strCancelDate &= eZeeDate.convertDate(arStrCancelDate(i)).ToShortDateString & ","
                    Next

                    If strCancelDate.Length > 0 Then
                        strCancelDate = strCancelDate.Substring(0, strCancelDate.Length - 1)
                    End If
                End If

            End If

            ' End If


            Dim objMail As New clsSendMail

            objMail._Subject = Language.getMessage(mstrModuleName, 15, "Leave Status Notification")

            Dim strMessage As String = ""

            strMessage = "<HTML> <BODY>"


            'Pinkal (13-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'strMessage &= Language.getMessage(mstrModuleName, 14, "Dear") & " " & mstrEmpFirstName & "  " & mstrEmpSurName & ", <BR><BR>"
            Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
            strMessage &= Language.getMessage(mstrModuleName, 14, "Dear") & " " & info1.ToTitleCase(mstrEmpFirstName.ToLower()) & "  " & info1.ToTitleCase(mstrEmpSurName.ToLower()) & ", <BR><BR>"
            'Pinkal (13-Mar-2019) -- End



            Dim mblnIsClaimPending As Boolean = False
            Dim mintClaimStatusID As Integer = -1
            If intClaimMstId > 0 Then
                Dim objclaim As New clsclaim_request_master
                objclaim._Crmasterunkid = intClaimMstId
                mintClaimStatusID = objclaim._Statusunkid
                If objclaim._Statusunkid = 2 Then
                    mblnIsClaimPending = True
                End If
                objclaim = Nothing
            End If


            If intStatusID <> 6 Then

                'Pinkal (13-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 16, " This is to inform you that your application for ") & mstrLeaveType & Language.getMessage(mstrModuleName, 17, " has been ") & mstrStatus & Language.getMessage(mstrModuleName, 18, " From ")
                'strMessage &= dtStartdate.Date & Language.getMessage(mstrModuleName, 19, "  To ") & dtEndDate.Date & " "

                If intStatusID <> 3 Then  'NOT REJECTED

                    'Pinkal (01-Apr-2019) -- Start
                    'Enhancement - Working on Leave Changes for NMB.
                    'strMessage &= Language.getMessage(mstrModuleName, 16, " This is to inform you that your application for ") & " " & mstrLeaveType & " " & Language.getMessage(mstrModuleName, 17, "has been ") & " " & mstrStatus & " " & Language.getMessage(mstrModuleName, 18, " From ") & " "
                    'strMessage &= dtStartdate.Date & " " & Language.getMessage(mstrModuleName, 19, "To") & " " & dtEndDate.Date & " "

                    strMessage &= Language.getMessage(mstrModuleName, 16, " This is to inform you that your application for ") & " <B>(" & mstrLeaveType & ")</B> " & Language.getMessage(mstrModuleName, 57, "with application no") & " <B>(" & mstrLeaveFormNo & ")</B> " & Language.getMessage(mstrModuleName, 17, "has been ") & " " & mstrStatus & " " & Language.getMessage(mstrModuleName, 18, " From ") & " "
                    strMessage &= dtStartdate.Date & " " & Language.getMessage(mstrModuleName, 19, "To") & " " & dtEndDate.Date

                    'Pinkal (01-Apr-2019) -- End


                ElseIf intStatusID = 3 Then  'REJECTED

                    'Pinkal (01-Apr-2019) -- Start
                    'Enhancement - Working on Leave Changes for NMB.
                    'strMessage &= Language.getMessage(mstrModuleName, 16, " This is to inform you that your application for ") & " " & mstrLeaveType & " " & Language.getMessage(mstrModuleName, 18, " From ") & " "
                    'strMessage &= dtStartdate.Date & " " & Language.getMessage(mstrModuleName, 19, "To") & " " & dtEndDate.Date & " " & Language.getMessage(mstrModuleName, 17, "has been ") & " " & mstrStatus & " "

                    strMessage &= Language.getMessage(mstrModuleName, 16, " This is to inform you that your application for ") & " <B>(" & mstrLeaveType & ") </B> " & Language.getMessage(mstrModuleName, 57, "with application no") & " <B>(" & mstrLeaveFormNo & ")</B> " & Language.getMessage(mstrModuleName, 18, " From ") & " "
                    strMessage &= dtStartdate.Date & " " & Language.getMessage(mstrModuleName, 19, "To") & " " & dtEndDate.Date & " " & Language.getMessage(mstrModuleName, 17, "has been ") & " " & mstrStatus
                    'Pinkal (01-Apr-2019) -- End

                    
                End If
                'Pinkal (13-Mar-2019) -- End

                If intStatusID = 3 OrElse intStatusID = 4 Then
                    If mintClaimStatusID = 3 Then
                        strMessage &= " " & Language.getMessage(mstrModuleName, 48, "and") & " " & Language.getMessage(mstrModuleName, 50, "your request for leave expense(s) associated with this leave application is rejected.")
                    End If
                    'Pinkal (13-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'strMessage &= "<BR></BR>"
                    'strMessage &= " " & Language.getMessage(mstrModuleName, 46, "Remarks/Comments:") & " " & ChrW(34) & mstrRejectionRemark & ChrW(34)
                    strMessage &= " " & Language.getMessage(mstrModuleName, 54, "with below") & " " & Language.getMessage(mstrModuleName, 46, "Remarks/Comments:") & "<BR></BR> " & ChrW(34) & mstrRejectionRemark & ChrW(34)
                    'Pinkal (13-Mar-2019) -- End

                End If

            Else

                'Pinkal (13-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 20, " This is to inform you that your leave has been Cancelled for these date(s): ")
                strMessage &= Language.getMessage(mstrModuleName, 20, " This is to inform you that your leave has been Cancelled for these date(s): ")
                'Pinkal (13-Mar-2019) -- End

                strMessage &= strCancelDate

                'Pinkal (13-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'strMessage &= "<BR></BR>"
                'Pinkal (13-Mar-2019) -- End

                Dim objCancelform As New clsCancel_Leaveform
                Dim dsList As DataSet = objCancelform.GetList("List", mintFormunkid, "", True)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    objCancelform._Cancelunkid = CInt(dsList.Tables(0).Rows(0)("cancelunkid"))
                    'Pinkal (13-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'strMessage &= " " & Language.getMessage(mstrModuleName, 46, "Remarks/Comments:") & " " & ChrW(34) & objCancelform._Cancel_Reason & ChrW(34)
                    strMessage &= " " & Language.getMessage(mstrModuleName, 54, "with below") & " " & Language.getMessage(mstrModuleName, 46, "Remarks/Comments:") & "<BR></BR> " & ChrW(34) & objCancelform._Cancel_Reason & ChrW(34)
                    'Pinkal (13-Mar-2019) -- End
                End If
                objCancelform = Nothing
            End If


            If intStatusID = 1 Then
                strMessage &= Language.getMessage(mstrModuleName, 21, "but still has not been issued")
                If mblnIsClaimPending AndAlso mintClaimStatusID = 2 Then
                    strMessage &= " " & Language.getMessage(mstrModuleName, 47, "Approval for your request for leave expense(s) is in progress, you will be notified when it is ready.")
                ElseIf mintClaimStatusID = 1 Then
                    strMessage &= " " & Language.getMessage(mstrModuleName, 48, "and") & " " & Language.getMessage(mstrModuleName, 49, "your request for leave expense(s) associated with this leave application is approved.")
                ElseIf mintClaimStatusID = 3 Then
                    strMessage &= " " & Language.getMessage(mstrModuleName, 48, "and") & " " & Language.getMessage(mstrModuleName, 50, "your request for leave expense(s) associated with this leave application is rejected.")
                End If

                'Pinkal (13-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'strMessage &= "." & Language.getMessage(mstrModuleName, 43, "Kindly take note of it.Please find the leave form attached for your reference.")
                strMessage &= "." & Language.getMessage(mstrModuleName, 55, "Kindly take note of it.") & "<br></br>" & Language.getMessage(mstrModuleName, 56, "Please find the leave form attached for your reference.")
                'Pinkal (13-Mar-2019) -- End


            ElseIf intStatusID = 7 Then
                If mblnIsClaimPending AndAlso mintClaimStatusID = 2 Then
                    strMessage &= " " & Language.getMessage(mstrModuleName, 47, "Approval for your request for leave expense(s) is in progress, you will be notified when it is ready.")
                ElseIf mintClaimStatusID = 1 Then
                    strMessage &= " " & Language.getMessage(mstrModuleName, 48, "and") & " " & Language.getMessage(mstrModuleName, 49, "your request for leave expense(s) associated with this leave application is approved.")
                ElseIf mintClaimStatusID = 3 Then
                    strMessage &= " " & Language.getMessage(mstrModuleName, 48, "and") & " " & Language.getMessage(mstrModuleName, 50, "your request for leave expense(s) associated with this leave application is rejected.")
                End If


                'Pinkal (13-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'strMessage &= Language.getMessage(mstrModuleName, 43, "Kindly take note of it.Please find the leave form attached for your reference.")

                'Pinkal (01-Apr-2019) -- Start
                'Enhancement - Working on Leave Changes for NMB.
                'strMessage &= Language.getMessage(mstrModuleName, 55, "Kindly take note of it.") & "<br></br>" & Language.getMessage(mstrModuleName, 56, "Please find the leave form attached for your reference.")
                strMessage &= "." & Language.getMessage(mstrModuleName, 55, "Kindly take note of it.") & "<br></br>" & Language.getMessage(mstrModuleName, 56, "Please find the leave form attached for your reference.")
                'Pinkal (01-Apr-2019) -- End


                'Pinkal (13-Mar-2019) -- End

            Else
                strMessage &= "."
            End If

            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strMessage &= "</BODY></HTML>"


            objMail._Message = strMessage
            objMail._ToEmail = mstrEmpMail

            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
            If mstrWebFrmName.Trim.Length > 0 Then
                objMail._Form_Name = mstrWebFrmName
                objMail._WebClientIP = mstrWebClientIP
                objMail._WebHostName = mstrWebHostName
            End If
            objMail._LogEmployeeUnkid = iLoginEmployeeId
            objMail._OperationModeId = iLoginTypeId
            objMail._UserUnkid = iUserId
            objMail._SenderAddress = mstrEmpFirstName & " " & mstrEmpSurName
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT

            If (intStatusID = 1 OrElse intStatusID = 7) And strPath <> "" Then
                objMail._AttachedFiles = strFileName
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objMail.SendMail(True, strPath)
                objMail.SendMail(intCompanyUnkId, True, strPath)
                'Sohail (30 Nov 2017) -- End
            Else
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objMail.SendMail()
                objMail.SendMail(intCompanyUnkId)
                'Sohail (30 Nov 2017) -- End
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SendMailToEmployee", mstrModuleName)
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    Public Sub SendMailToUser(ByVal intEmployeeID As Integer, _
                              ByVal strFormNo As String, _
                              ByVal mstrLeaveType As String, _
                              ByVal dtStartdate As Date, _
                              ByVal dtEndDate As Date, _
                              ByVal intCompanyUnkId As Integer, _
                              Optional ByVal mstrUserName As String = "", _
                              Optional ByVal strPath As String = "", _
                              Optional ByVal strFileName As String = "", _
                              Optional ByVal iLoginTypeId As Integer = 0, _
                              Optional ByVal iLoginEmployeeId As Integer = 0, _
                              Optional ByVal iUserId As Integer = 0)
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        'S.SANDEEP [ 28 JAN 2014 ] -- START {iLoginTypeId,iLoginEmployeeId,iUserId} -- END
        Try

            Dim objNet As New clsNetConnectivity
            If objNet._Conected = False Then Exit Sub

            Dim objMail As New clsSendMail


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            '' 194 IS PRIVIEGE NO FOR ALLOW TO ISSUE LEAVE.
            'Dim objUser As New clsUserAddEdit
            'Dim dsList As DataSet = objUser.Get_UserBy_PrivilegeId(194)

            'If dsList.Tables(0).Rows.Count <= 0 Then Exit Sub

            'For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

            '    If dsList.Tables(0).Rows(i)("UEmail").ToString().Trim.Length <= 0 Then Continue For

            '    objMail._Subject = Language.getMessage(mstrModuleName, 22, "Final Reminder to issue Leave Application form")
            '    Dim strMessage As String = ""
            '    strMessage = "<HTML> <BODY>"
            '    strMessage &= Language.getMessage(mstrModuleName, 14, "Dear") & " " & dsList.Tables(0).Rows(i)("UName").ToString() & ", <BR><BR>"

            '    strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 23, "This is the final reminder for issuing leave application no") & " " & strFormNo.Trim & _
            '                                Language.getMessage(mstrModuleName, 11, " of ") & mstrEmpFirstName & " " & mstrEmpSurName & Language.getMessage(mstrModuleName, 12, " having Start date : ")

            '    If mstrUserName.Trim.Length > 0 Then
            '        strMessage &= mdtStartdate.Date & Language.getMessage(mstrModuleName, 13, " and End Date : ") & mdtReturndate.Date & "." & Language.getMessage(mstrModuleName, 24, " The leave has been finally approved by ") & mstrUserName & "."
            '    Else
            '        strMessage &= mdtStartdate.Date & Language.getMessage(mstrModuleName, 13, " and End Date : ") & mdtReturndate.Date & "." & Language.getMessage(mstrModuleName, 24, " The leave has been finally approved by ") & User._Object._Firstname & " " & User._Object._Lastname & "."
            '    End If

            '    strMessage &= Language.getMessage(mstrModuleName, 25, "so please login to Aruti HRMS System to Issue this leave.")

            '    strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            '    strMessage &= "</BODY></HTML>"

            '    objMail._Message = strMessage
            '    objMail._ToEmail = dsList.Tables(0).Rows(i)("UEmail").ToString()
            '    If strPath <> "" Then
            '        objMail._AttachedFiles = strFileName
            '        objMail.SendMail(True, strPath)
            '    Else
            '        objMail.SendMail()
            '    End If

            'Next

            Dim objIssueUser As New clsissueUser_Tran
            Dim dsList As DataSet = objIssueUser.GetList("List", -1, intEmployeeID)
            If dsList.Tables(0).Rows.Count > 0 Then

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = CInt(dsList.Tables(0).Rows(0)("issueuserunkid"))

                objMail._Subject = Language.getMessage(mstrModuleName, 22, "Final Reminder to issue Leave Application form")
                Dim strMessage As String = ""
                strMessage = "<HTML> <BODY>"


                'Pinkal (13-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                'strMessage &= Language.getMessage(mstrModuleName, 14, "Dear") & " " & objUser._Firstname & " " & objUser._Lastname & ", <BR><BR>"

                'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 23, "This is the final reminder for issuing leave application no") & " " & strFormNo.Trim & _
                '                           Language.getMessage(mstrModuleName, 7, " with ") & mstrLeaveType & Language.getMessage(mstrModuleName, 45, " of ") & mstrEmpFirstName & " " & mstrEmpSurName & Language.getMessage(mstrModuleName, 12, " having Start date : ")

                'If mstrUserName.Trim.Length > 0 Then
                '    strMessage &= mdtStartdate.Date & Language.getMessage(mstrModuleName, 13, " and End Date : ") & mdtReturndate.Date & "." & Language.getMessage(mstrModuleName, 24, " The leave has been finally approved by ") & mstrUserName & "."
                'Else
                '    strMessage &= mdtStartdate.Date & Language.getMessage(mstrModuleName, 13, " and End Date : ") & mdtReturndate.Date & "." & Language.getMessage(mstrModuleName, 24, " The leave has been finally approved by ") & User._Object._Firstname & " " & User._Object._Lastname & "."
                'End If

                Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                strMessage &= Language.getMessage(mstrModuleName, 14, "Dear") & " " & info1.ToTitleCase(objUser._Firstname.ToLower()) & " " & info1.ToTitleCase(objUser._Lastname.ToLower()) & ", <BR><BR>"


                'Pinkal (15-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'strMessage &= Language.getMessage(mstrModuleName, 23, "This is the final reminder for issuing leave application no") & " " & strFormNo.Trim & " " & _
                '                          Language.getMessage(mstrModuleName, 7, "with") & " " & mstrLeaveType & " " & Language.getMessage(mstrModuleName, 45, "of") & " " & mstrEmpFirstName & " " & mstrEmpSurName & " " & Language.getMessage(mstrModuleName, 12, " having Start date : ")


                'Pinkal (01-Apr-2019) -- Start
                'Enhancement - Working on Leave Changes for NMB.

                'strMessage &= Language.getMessage(mstrModuleName, 23, "This is the final reminder for issuing leave application no") & " " & strFormNo.Trim & " " & _
                '                          Language.getMessage(mstrModuleName, 7, "with") & " " & mstrLeaveType & " " & Language.getMessage(mstrModuleName, 45, "of") & " " & info1.ToTitleCase(mstrEmpFirstName.ToLower()) & " " & info1.ToTitleCase(mstrEmpSurName.ToLower()) & " " & Language.getMessage(mstrModuleName, 12, " having Start date : ")

                strMessage &= Language.getMessage(mstrModuleName, 23, "This is the final reminder for issuing leave application no") & " <B>(" & strFormNo.Trim & ")</B> " & _
                                          Language.getMessage(mstrModuleName, 7, "with") & " <B>(" & mstrLeaveType & ")</B> " & Language.getMessage(mstrModuleName, 45, "of") & " " & info1.ToTitleCase(mstrEmpFirstName.ToLower()) & " " & info1.ToTitleCase(mstrEmpSurName.ToLower()) & " " & Language.getMessage(mstrModuleName, 12, " having Start date : ")

                'Pinkal (01-Apr-2019) -- End


                'Pinkal (15-Mar-2019) -- End

                
                'Pinkal (15-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If mstrUserName.Trim.Length > 0 Then
                    'strMessage &= mdtStartdate.Date & " " & Language.getMessage(mstrModuleName, 13, "and End Date : ") & mdtReturndate.Date & "." & Language.getMessage(mstrModuleName, 24, " The leave has been finally approved by ") & " " & mstrUserName & "."

                    'Pinkal (01-Apr-2019) -- Start
                    'Enhancement - Working on Leave Changes for NMB.
                    'strMessage &= mdtStartdate.Date & " " & Language.getMessage(mstrModuleName, 13, "and End Date : ") & mdtReturndate.Date & "." & Language.getMessage(mstrModuleName, 24, " The leave has been finally approved by ") & " " & info1.ToTitleCase(mstrUserName.ToLower()) & "."
                    strMessage &= " " & mdtStartdate.Date & " " & Language.getMessage(mstrModuleName, 13, "and End Date : ") & mdtReturndate.Date & "." & Language.getMessage(mstrModuleName, 24, " The leave has been finally approved by ") & " " & info1.ToTitleCase(mstrUserName.ToLower()) & "."
                    'Pinkal (01-Apr-2019) -- End
                Else
                    'strMessage &= mdtStartdate.Date & " " & Language.getMessage(mstrModuleName, 13, "and End Date : ") & mdtReturndate.Date & "." & Language.getMessage(mstrModuleName, 24, " The leave has been finally approved by ") & " " & User._Object._Firstname & " " & User._Object._Lastname & "."

                    'Pinkal (01-Apr-2019) -- Start
                    'Enhancement - Working on Leave Changes for NMB.
                    'strMessage &= mdtStartdate.Date & " " & Language.getMessage(mstrModuleName, 13, "and End Date : ") & mdtReturndate.Date & "." & Language.getMessage(mstrModuleName, 24, " The leave has been finally approved by ") & " " & info1.ToTitleCase(User._Object._Firstname.ToLower()) & " " & info1.ToTitleCase(User._Object._Lastname.ToLower()) & "."
                    strMessage &= " " & mdtStartdate.Date & " " & Language.getMessage(mstrModuleName, 13, "and End Date : ") & mdtReturndate.Date & "." & Language.getMessage(mstrModuleName, 24, " The leave has been finally approved by ") & " " & info1.ToTitleCase(User._Object._Firstname.ToLower()) & " " & info1.ToTitleCase(User._Object._Lastname.ToLower()) & "."
                    'Pinkal (01-Apr-2019) -- End

                End If

                'Pinkal (15-Mar-2019) -- End

                'Pinkal (13-Mar-2019) -- End



                strMessage &= Language.getMessage(mstrModuleName, 25, "so please login to Aruti HRMS System to Issue this leave.")

                strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                strMessage &= "</BODY></HTML>"

                objMail._Message = strMessage
                objMail._ToEmail = objUser._Email

                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                If mstrWebFrmName.Trim.Length > 0 Then
                    objMail._Form_Name = mstrWebFrmName
                    objMail._WebClientIP = mstrWebClientIP
                    objMail._WebHostName = mstrWebHostName
                End If
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = iLoginTypeId
                objMail._UserUnkid = iUserId
                objMail._SenderAddress = mstrEmpFirstName & " " & mstrEmpSurName
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT

                If strPath <> "" Then
                    objMail._AttachedFiles = strFileName
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objMail.SendMail(True, strPath)
                    objMail.SendMail(intCompanyUnkId, True, strPath)
                    'Sohail (30 Nov 2017) -- End
                Else
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objMail.SendMail()
                    objMail.SendMail(intCompanyUnkId)
                    'Sohail (30 Nov 2017) -- End
                End If

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SendMailToUser", mstrModuleName)
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function GetEmployeeLastIssuedLeave(ByVal intFormID As Integer, ByVal intEmployeeID As Integer, ByVal intLeaveTypeID As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        Try

            strQ = " SELECT  formunkid, startdate,enddate,priority,lvleaveapprover_master.approverunkid   FROM lvpendingleave_tran " & _
                     " JOIN lvleaveapprover_master ON lvpendingleave_tran.approvertranunkid = lvleaveapprover_master.approverunkid " & _
                     " JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid = lvleaveapprover_master.levelunkid " & _
                     " WHERE lvpendingleave_tran.formunkid IN " & _
                     " ( " & _
                     "      SELECT TOP 1 formunkid FROM lvleaveform WHERE statusunkid=7  AND lvleaveform.employeeunkid = " & intEmployeeID & _
                     "      AND lvleaveform.leavetypeunkid = " & intLeaveTypeID

            'Pinkal (10-Jul-2018) -- Start
            'Enhancement - Changed for Abood Employee Claim Form.

            If intFormID > 0 Then
                strQ &= " AND lvleaveform.formunkid <> " & intFormID
            End If

            strQ &= "  ORDER BY returndate DESC) " & _
                     " AND lvpendingleave_tran.isvoid = 0 " & _
                     " ORDER BY priority DESC "

            'Pinkal (10-Jul-2018) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeLastIssuedLeave", mstrModuleName)
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function GetPendingLeaveFormForPeriod(ByVal mdtStartdate As DateTime, ByVal mdtEndTime As DateTime, Optional ByVal IscheckForCancel As Boolean = False, Optional ByVal mstrEmployeeIDs As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            objDataOperation.ClearParameters()

            If IscheckForCancel Then
                strQ = "SELECT * FROM lvleaveform WHERE CONVERT(char(8),startdate,112) >= '" & eZeeDate.convertDate(mdtStartdate) & "' AND CONVERT(char(8),startdate,112) <=  '" & eZeeDate.convertDate(mdtEndTime) & "'  AND statusunkid IN (6) AND isvoid = 0 "    'CHECK FOR ISSUE STATUS
            Else
                strQ = "SELECT * FROM lvleaveform WHERE CONVERT(char(8),startdate,112) >= '" & eZeeDate.convertDate(mdtStartdate) & "' AND CONVERT(char(8),startdate,112) <=  '" & eZeeDate.convertDate(mdtEndTime) & "'  AND statusunkid IN (1,2) AND isvoid = 0 "   'CHECK FOR APPROVE AND PENDING STATUS
            End If

            If mstrEmployeeIDs.Trim.Length > 0 Then
                strQ &= " AND employeeunkid IN (" & mstrEmployeeIDs & ")"
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetPendingLeaveFormForPeriod", mstrModuleName)
        End Try
        Return dsList
    End Function

    'Pinkal (15-Sep-2013) -- End

    'Pinkal (02-Dec-2015) -- Start
    'Enhancement - Solving Leave bug in Self Service.

    'Public Shared Function ApprovedLeaveFormtenure(ByVal objDataOperation As clsDataOperation, ByVal intFormunkid As Integer _
    '                                                                         , ByVal mdtStartdate As DateTime, ByVal mdtEnddate As DateTime _
    '                                                                         , ByVal mdecDayFraction As Decimal, ByVal mblnApproved As Boolean, Optional ByVal intUserID As Integer = 0) As Boolean
    '    Dim strQ As String = ""
    '    Dim exForce As Exception = Nothing
    '    Try

    '        'START FOR UPDATE LEAVE FORM APPROVED START DATE,END DATE AND APPROVED DAYS
    '        objDataOperation.ClearParameters()
    '        If mblnApproved Then

    '            strQ = " UPDATE lvleaveform SET approve_stdate = @startdate,approve_eddate = @enddate,approve_days = @days WHERE formunkid = @formunkid AND isvoid = 0 "
    '            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
    '            objDataOperation.AddParameter("@days", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDayFraction)
    '        Else
    '            strQ = " UPDATE lvleaveform SET approve_stdate = null,approve_eddate = null,approve_days = 0 WHERE formunkid = @formunkid AND isvoid = 0 "
    '        End If
    '        objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
    '        objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lvleaveform", "formunkid", intFormunkid, False, intUserID) = False Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'END FOR UPDATE LEAVE FORM APPROVED START DATE,END DATE AND APPROVED DAYS

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    '    Return True
    'End Function

    Public Function ApprovedLeaveFormtenure(ByVal objDataOperation As clsDataOperation, ByVal intFormunkid As Integer _
                                                                             , ByVal mdtStartdate As DateTime, ByVal mdtEnddate As DateTime _
                                                                             , ByVal mdecDayFraction As Decimal, ByVal mblnApproved As Boolean, Optional ByVal intUserID As Integer = 0) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Try

            'START FOR UPDATE LEAVE FORM APPROVED START DATE,END DATE AND APPROVED DAYS
            objDataOperation.ClearParameters()
            If mblnApproved Then

                strQ = " UPDATE lvleaveform SET approve_stdate = @startdate,approve_eddate = @enddate,approve_days = @days WHERE formunkid = @formunkid AND isvoid = 0 "
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
                objDataOperation.AddParameter("@days", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDayFraction)
            Else
                strQ = " UPDATE lvleaveform SET approve_stdate = null,approve_eddate = null,approve_days = 0 WHERE formunkid = @formunkid AND isvoid = 0 "
            End If
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lvleaveform", "formunkid", intFormunkid, False, intUserID) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'END FOR UPDATE LEAVE FORM APPROVED START DATE,END DATE AND APPROVED DAYS

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ApprovedLeaveFormtenure; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function
    'Pinkal (02-Dec-2015) -- End
    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function IsValid_Form(ByVal iEmpId As Integer, ByVal iLeaveTypeId As Integer, ByVal iSDate As DateTime, ByVal iEDate As DateTime, ByVal iDaysApplied As Integer) As String
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim sMessage As String = ""
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Try

            'Pinkal (17-May-2017) -- Start
            'Enhancement - Problem solved for Consecutive Days issue Reported By PACRA.

            'StrQ = "SELECT TOP 1 consecutivedays AS cDays,occurrence AS iOcc,optiondid AS iOptId,remaining_occurrence AS rOcc,CONVERT(CHAR(8),lvleaveIssue_tran.leavedate,112) AS iDate " & _
            '       "FROM lvleavebalance_tran " & _
            '           " LEFT JOIN lvleaveIssue_master ON lvleavebalance_tran.employeeunkid = lvleaveIssue_master.employeeunkid AND lvleavebalance_tran.leavetypeunkid = lvleaveIssue_master.leavetypeunkid AND lvleaveIssue_master.isvoid = 0 " & _
            '           " LEFT JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid AND lvleaveIssue_tran.isvoid = 0 " & _
            '       " WHERE lvleavebalance_tran.isvoid = 0 AND lvleavebalance_tran.employeeunkid = '" & iEmpId & "' AND lvleavebalance_tran.leavetypeunkid = '" & iLeaveTypeId & "' AND isshortleave = 1 AND occurrence > 0 AND consecutivedays > 0 " & _
            '       " ORDER BY leavedate DESC "

            StrQ = "SELECT TOP 1 consecutivedays AS cDays " & _
                     " ,occurrence AS iOcc " & _
                     ",optiondid AS iOptId " & _
                     ",remaining_occurrence AS rOcc " & _
                     ",CONVERT(CHAR(8), isnull(lvleaveIssue_tran.leavedate, lv.EndDate), 112) AS iDate " & _
                     " FROM lvleavebalance_tran " & _
                     " LEFT JOIN lvleaveIssue_master ON lvleavebalance_tran.employeeunkid = lvleaveIssue_master.employeeunkid " & _
                     " AND lvleavebalance_tran.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
                     " AND lvleaveIssue_master.isvoid = 0 " & _
                     " LEFT JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                     " AND lvleaveIssue_tran.isvoid = 0 " & _
                     " LEFT JOIN ( " & _
                     " SELECT TOP 1 ISNULL(approve_eddate, returndate) AS EndDate " & _
                          ",leavetypeunkid " & _
                          ",employeeunkid " & _
                     " FROM lvleaveform " & _
                     " WHERE lvleaveform.employeeunkid = @employeeunkid AND leavetypeunkid =  @leavetypeunkid  AND isvoid = 0 " & _
                     " AND lvleaveform.statusunkid in (1,2,7) " & _
                     " ORDER BY startdate DESC " & _
                     " ) AS lv ON lv.leavetypeunkid = lvleavebalance_tran.leavetypeunkid " & _
                     " AND lv.employeeunkid = lvleavebalance_tran.employeeunkid " & _
                     " WHERE lvleavebalance_tran.isvoid = 0 AND lvleavebalance_tran.employeeunkid = @employeeunkid " & _
                     " AND lvleavebalance_tran.leavetypeunkid = @leavetypeunkid  AND isshortleave = 1 AND occurrence > 0 " & _
                     " AND consecutivedays >= 0  ORDER BY leavedate DESC "


            'Pinkal (21-May-2019) -- 'Enhancement - Global Approval for leave ,system should provide option to approve leave globally.[ " AND lvleaveform.statusunkid in (1,2,7) " & _]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpId)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iLeaveTypeId)
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then

                'Pinkal (13-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.

                If CInt(dsList.Tables(0).Rows(0).Item("cDays")) > 0 Then

                If iDaysApplied > CInt(dsList.Tables(0).Rows(0).Item("cDays")) Then
                    sMessage = Language.getMessage(mstrModuleName, 28, "Sorry, you cannot apply for [ ") & iDaysApplied & _
                               Language.getMessage(mstrModuleName, 29, " ] day(s) for this leave type. Reason you can only apply for [ ") & CInt(dsList.Tables(0).Rows(0).Item("cDays")) & _
                               Language.getMessage(mstrModuleName, 30, " ] consecutive day(s) for this leave type.")
                    GoTo lFound
                End If



                    'Pinkal (13-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.

                    'Dim mintDays As Integer = 0
                    'If IsDBNull(dsList.Tables(0).Rows(0).Item("iDate")) Then
                    '    dsList.Tables(0).Rows(0).Item("iDate") = eZeeDate.convertDate(iEDate.Date).ToString()
                    '    mintDays = DateDiff(DateInterval.Day, iSDate, eZeeDate.convertDate(dsList.Tables(0).Rows(0).Item("iDate")))
                    'Else
                    '    mintDays = DateDiff(DateInterval.Day, eZeeDate.convertDate(dsList.Tables(0).Rows(0).Item("iDate")), iSDate)
                    'End If

                    'If mintDays <= 1 Then
                    '    sMessage = Language.getMessage(mstrModuleName, 31, "Sorry, you cannot apply from [ ") & iSDate.Date & _
                    '              Language.getMessage(mstrModuleName, 32, " ] date for this leave type. Reason, this will violate the consecutive days rule set for this leave type.")
                    '    GoTo lFound
                    'End If

                    If iDaysApplied < CInt(dsList.Tables(0).Rows(0).Item("cDays")) Then
                    sMessage = Language.getMessage(mstrModuleName, 31, "Sorry, you cannot apply from [ ") & iSDate.Date & _
                              Language.getMessage(mstrModuleName, 32, " ] date for this leave type. Reason, this will violate the consecutive days rule set for this leave type.")
                    GoTo lFound
                End If

                    'Pinkal (13-Oct-2018) -- End


                End If

                'Pinkal (13-Oct-2018) -- End

                If CInt(dsList.Tables(0).Rows(0).Item("rOcc")) <= 0 Then
                    sMessage = Language.getMessage(mstrModuleName, 33, "Sorry, you cannot apply for this leave type as you can only apply for [") & CInt(dsList.Tables(0).Rows(0).Item("iOcc")) & _
                               Language.getMessage(mstrModuleName, 34, " ] time(s)")
                    Select Case CInt(dsList.Tables(0).Rows(0).Item("iOptId"))
                        Case enLeaveFreq_Factors.LVF_LIFE_TIME
                            sMessage &= Language.getMessage(mstrModuleName, 35, " in your life time.")
                        Case enLeaveFreq_Factors.LVF_FINANCIAL_YEAR
                            'Pinkal (13-Oct-2018) -- Start
                            'Enhancement - Leave Enhancement for NMB.
                            'sMessage &= Language.getMessage(mstrModuleName, 36, " during financial year.")
                            sMessage &= Language.getMessage(mstrModuleName, 36, " during your leave tenure.")
                            'Pinkal (13-Oct-2018) -- End
                        Case enLeaveFreq_Factors.LVF_ELC
                            sMessage &= Language.getMessage(mstrModuleName, 37, " during your employee leave cycle.")
                    End Select
                    GoTo lFound
                End If
            End If

lFound:     Return sMessage

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValid_Form; Module Name: " & mstrModuleName)
            Return ex.Message
        Finally

        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function IsValid_SickLeaveForm(ByVal iEmployeeId As Integer, ByVal iLeaveTypeId As Integer, ByVal iDateValue As Date, Optional ByVal iEndDate As Date = Nothing, Optional ByVal FormId As Integer = -1) As String  'Pinkal [21-Oct-2013] iEndDate,FormId
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim sMsg As String = String.Empty
        Try
            Dim objDataOperation As New clsDataOperation

            StrQ = "SELECT TOP 1 startdate,returndate,statusunkid " & _
                   "FROM lvleaveform " & _
                   "    JOIN lvleavetype_master ON lvleaveform.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
                   "WHERE lvleaveform.isvoid = 0 AND lvleaveform.employeeunkid = '" & iEmployeeId & "' AND lvleaveform.leavetypeunkid = '" & iLeaveTypeId & "' " & _
                   "    AND lvleavetype_master.issickleave = 1 AND lvleaveform.statusunkid IN (1,2,7) "

            If FormId > 0 Then
                StrQ &= " AND lvleaveform.formunkid <> " & FormId
            End If

            StrQ &= "ORDER BY startdate DESC "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                If IsDBNull(dsList.Tables("List").Rows(0).Item("returndate")) = True Then
                    sMsg = Language.getMessage(mstrModuleName, 38, "Sorry, you cannot apply for leave. Reason : There is already another leave form whose end date is not set.")
                    GoTo mFound
                End If

                Select Case CInt(dsList.Tables("List").Rows(0).Item("statusunkid"))

                    Case 1, 2
                        If IsDBNull(dsList.Tables("List").Rows(0).Item("returndate")) = False Then
                            sMsg = Language.getMessage(mstrModuleName, 39, "Sorry, you cannot apply for leave. Reason : There is already another leave form which is not yet issued.")
                            GoTo mFound
                        End If

                End Select

                Select Case CInt(dsList.Tables("List").Rows(0).Item("statusunkid"))
                    Case 1, 2, 7

                        If iEndDate = Nothing Then
                            If iDateValue <= CDate(dsList.Tables("List").Rows(0).Item("returndate")).Date Then
                                If DateDiff(DateInterval.Day, iDateValue, CDate(dsList.Tables("List").Rows(0).Item("startdate")).Date) <= 1 Then
                                    sMsg = Language.getMessage(mstrModuleName, 40, "Sorry, you cannot apply for leave. Reason : You cannot apply for same leave for consecutive days.")
                                    GoTo mFound
                                End If
                            Else
                                If DateDiff(DateInterval.Day, CDate(dsList.Tables("List").Rows(0).Item("returndate")).Date, iDateValue) <= 1 Then
                                    sMsg = Language.getMessage(mstrModuleName, 40, "Sorry, you cannot apply for leave. Reason : You cannot apply for same leave for consecutive days.")
                                    GoTo mFound
                                End If
                            End If

                        Else

                            If iEndDate <= CDate(dsList.Tables("List").Rows(0).Item("returndate")).Date Then
                                If DateDiff(DateInterval.Day, iEndDate, CDate(dsList.Tables("List").Rows(0).Item("startdate")).Date) <= 1 Then
                                    sMsg = Language.getMessage(mstrModuleName, 40, "Sorry, you cannot apply for leave. Reason : You cannot apply for same leave for consecutive days.")
                                    GoTo mFound
                                End If
                            Else
                                If DateDiff(DateInterval.Day, CDate(dsList.Tables("List").Rows(0).Item("returndate")).Date, iEndDate) <= 1 Then
                                    sMsg = Language.getMessage(mstrModuleName, 40, "Sorry, you cannot apply for leave. Reason : You cannot apply for same leave for consecutive days.")
                                    GoTo mFound
                                End If
                            End If

                        End If

                End Select
            End If

mFound:     objDataOperation = Nothing
            Return sMsg
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid_SickLeaveForm", mstrModuleName)
            Return ex.Message
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function LeaveForm_Alert(ByVal iEmpId As Integer, ByVal iStDate As DateTime, ByVal iEndDate As DateTime, ByVal mintFormId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            mstrMessage = ""
            Dim objDataOperation As New clsDataOperation

            StrQ = "SELECT TOP 1 startdate,returndate,statusunkid " & _
                   "FROM lvleaveform " & _
                   " JOIN lvleavetype_master ON lvleaveform.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
                   "WHERE lvleaveform.isvoid = 0 AND lvleaveform.employeeunkid = '" & iEmpId & "' " & _
                   " AND lvleavetype_master.issickleave = 1 AND returndate IS NULL "
            If mintFormId > 0 Then
                StrQ &= " AND lvleaveform.formunkid <> '" & mintFormId & "' "
            End If
            StrQ &= "ORDER BY startdate DESC "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                If iStDate.Date >= CDate(dsList.Tables("List").Rows(0).Item("startdate")) Then
                    mstrMessage = Language.getMessage(mstrModuleName, 41, "Sorry, you cannot apply for leave." & vbCrLf & _
                                                      "Reason : You have some pending sick leave form in which return date is not set.")
                    GoTo iMsg
                End If

                If iStDate.Date <= CDate(dsList.Tables("List").Rows(0).Item("startdate")) Then
                    If iEndDate.Date >= CDate(dsList.Tables("List").Rows(0).Item("startdate")) Then
                        mstrMessage = Language.getMessage(mstrModuleName, 42, "Sorry, you cannot apply for leave." & vbCrLf & _
                                                       "Reason : Leave end date should be less than start date of sick leave form.")
                        GoTo iMsg
                    End If
                End If
            End If

iMsg:       Return mstrMessage

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "LeaveForm_Alert", mstrModuleName)
            Return ex.Message
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 

    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    'Public Function IsLeaveForm_Exists(ByVal iEmployeeId As Integer, ByVal iLeaveTypeId As Integer) As Boolean
    Public Function IsLeaveForm_Exists(ByVal iEmployeeId As Integer, ByVal iLeaveTypeId As Integer, Optional ByVal mblnIncludeStatusFilter As Boolean = False) As Boolean
        'Pinkal (26-Feb-2019) -- End
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim blnFlag As Boolean = False
        Dim iCnt As Integer = 0
        Try
            Using objDataOperation As New clsDataOperation
                StrQ = "SELECT 1 FROM lvleaveform WHERE isvoid = 0 AND employeeunkid = '" & iEmployeeId & "' AND leavetypeunkid = '" & iLeaveTypeId & "' "

                'Pinkal (26-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If mblnIncludeStatusFilter Then
                    StrQ &= " AND statusunkid in (1,2,7)"
                End If
                'Pinkal (26-Feb-2019) -- End

                iCnt = objDataOperation.RecordCount(StrQ)
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If iCnt > 0 Then
                    blnFlag = True
                End If
            End Using
            Return blnFlag
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsLeaveForm_Exists", mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function ValidShiftForEmployee(ByVal intEmpId As Integer, ByVal mdtDate As DateTime) As Boolean
        Dim blnFlag As Boolean = False
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Try
            StrQ = " Select Isnull(formunkid,0) formunkid From lvleaveform where employeeunkid = @employeeunkid  " & _
                      " and @date between convert(CHAR(8),startdate,112) and convert(CHAR(8),isnull(returndate,startdate),112) and statusunkid In (1,2,7) and isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate))
            dsList = objDataOperation.ExecQuery(StrQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return (dsList.Tables(0).Rows.Count) > 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ValidShiftForEmployee", mstrModuleName)
        End Try
        Return blnFlag
    End Function

    'Pinkal (03-Jan-2014) -- Start
    'Enhancement : Oman Changes

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function GetEmployeeLastLeaveFormStatus(ByVal intEmployeeID As Integer, ByVal intLeaveTypeId As Integer, Optional ByVal intFormunkid As Integer = 0) As Integer
        Dim mintStatusId As Integer = -1
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = String.Empty
        Dim exForce As Exception = Nothing
        Dim objDataOperation As New clsDataOperation
        Try
            objDataOperation.ClearParameters()

            'Pinkal (16-Dec-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            'StrQ = "SELECT Top 1 ISNULL(statusunkid,0) AS statusunkid FROM lvleaveform WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid AND isvoid = 0  "
            StrQ = "SELECT Top 1 ISNULL(statusunkid,0) AS statusunkid FROM lvleaveform WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid AND isvoid = 0 AND statusunkid in (1,2)  "
            'Pinkal (16-Dec-2016) -- End

            If intFormunkid > 0 Then
                StrQ &= " AND lvleaveform.formunkid <> @formunkid "
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
            End If

            'Pinkal (16-Dec-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            'StrQ &= " ORDER BY startdate DESC "
            'Pinkal (16-Dec-2016) -- End

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeId)
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintStatusId = CInt(dsList.Tables(0).Rows(0)("statusunkid"))
            Else
                mintStatusId = -1
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeLastLeaveFormStatus", mstrModuleName)
        End Try
        Return mintStatusId
    End Function

    'Pinkal (03-Jan-2014) -- End

    'Pinkal (01-Feb-2014) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function GetNoOfWeekend(ByVal intEmpId As Integer, ByVal mdtStartdate As Date, ByVal arHolidayDate() As String, Optional ByVal mdtEndDate As Date = Nothing, _
                                                    Optional ByVal blnIsssueonHoliday As Boolean = False, Optional ByVal blnConsiderLVHlOnWk As Boolean = False) As Decimal
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim mdblCount As Decimal = 0
        Try

            Dim objShift As New clsNewshift_master
            Dim objShifttran As New clsshift_tran

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim objEmp As New clsEmployee_Master
            'objEmp._Employeeunkid = intEmpId
            'S.SANDEEP [04 JUN 2015] -- END

            Dim objEmpShiftTran As New clsEmployee_Shift_Tran
            If mdtStartdate <> Nothing AndAlso mdtEndDate <> Nothing Then
                Dim intDays As Integer = DateDiff(DateInterval.Day, mdtStartdate.Date, mdtEndDate.AddDays(1).Date)
                For i As Integer = 0 To intDays - 1
                    'START CHECK WHETHER HOLIDAY IS COMING ON WEEKEND SO WE CAN SKIP THAT DAY IN WEEKEND BECAUSE IT WAS ALREADY COUNTED IN HOLIDAY
                    Dim index As Integer = -1
                    If arHolidayDate IsNot Nothing Then
                        index = Array.IndexOf(arHolidayDate, eZeeDate.convertDate(mdtStartdate.AddDays(i).Date))
                    End If

                    If index >= 0 Then Continue For

                    'END CHECK WHETHER HOLIDAY IS COMING ON WEEKEND SO WE CAN SKIP THAT DAY IN WEEKEND BECAUSE IT WAS ALREADY COUNTED IN HOLIDAY

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objShift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(mdtStartdate.AddDays(i).Date, objEmp._Employeeunkid)
                    objShift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(mdtStartdate.AddDays(i).Date, intEmpId)
                    'S.SANDEEP [04 JUN 2015] -- END

                    objShifttran.GetShiftTran(objShift._Shiftunkid)

                    'Pinkal (02-Dec-2015) -- Start
                    'Enhancement - Solving FDRC Leave bug in Self Service.
                    Dim strWeekName As String = Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.GetDayName(mdtStartdate.AddDays(i).DayOfWeek)
                    'Pinkal (02-Dec-2015) -- End

                    If objShifttran._dtShiftday IsNot Nothing Then

                        'Pinkal (02-Dec-2015) -- Start
                        'Enhancement - Solving FDRC Leave bug in Self Service.
                        'Dim drrow() As DataRow = objShifttran._dtShiftday.Select("dayid = " & GetWeekDayNumber(mdtStartdate.AddDays(i).DayOfWeek.ToString()) & " AND isweekend = 1 ")
                        Dim drrow() As DataRow = objShifttran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1 ")
                        'Pinkal (02-Dec-2015) -- End


                        If drrow.Length > 0 Then
                            If blnIsssueonHoliday AndAlso blnConsiderLVHlOnWk = False Then
                                Dim objEmpHoliday As New clsemployee_holiday
                                If objEmpHoliday.GetEmployeeHoliday(intEmpId, mdtStartdate.AddDays(i).Date).Rows.Count > 0 Then
                                    Continue For
                                End If
                            End If
                            mdblCount += 1
                        End If
                    End If
                Next
            End If

            Return mdblCount
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNoOfWeekend", mstrModuleName)
        End Try
    End Function

    'Pinkal (01-Feb-2014) -- End

    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function getListForCombo(ByVal iLeaveTypeId As Integer, _
                                    Optional ByVal iStatusIds As String = "", _
                                    Optional ByVal iEmpId As Integer = -1, _
                                    Optional ByVal iConcateDates As Boolean = False, _
                                    Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as formunkid, ' ' +  @name as name,'' AS sDate, '' AS eDate   UNION "
            End If

            strQ &= "SELECT formunkid, formno as name, Convert(Char(8),startdate,112) AS sDate, Convert(Char(8),returndate,112) AS eDate FROM lvleaveform  WHERE isvoid = 0 "

            If iLeaveTypeId > 0 Then
                strQ &= " AND leavetypeunkid = '" & iLeaveTypeId & "'"
            End If

            If iStatusIds.Trim.Length > 0 Then
                strQ &= " AND statusunkid in (" & iStatusIds & ")"
            End If

            If iEmpId > 0 Then
                strQ &= " AND employeeunkid = '" & iEmpId & "'"
            End If

            strQ &= " ORDER BY name "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iConcateDates = True Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    If dRow.Item("formunkid") <= 0 Then Continue For
                    dRow.Item("name") = dRow.Item("name") & " [ " & eZeeDate.convertDate(dRow.Item("sDate")) & " - " & eZeeDate.convertDate(dRow.Item("eDate")) & " ]"
                Next
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    'Pinkal (06-Mar-2014) -- End

    'Pinkal (24-May-2014) -- Start
    'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function getListForCurrentYearLeaveForm(ByVal intYearId As Integer, ByVal iLeaveTypeId As Integer, _
                                    Optional ByVal iStatusIds As String = "", _
                                    Optional ByVal iEmpId As Integer = -1, _
                                    Optional ByVal iConcateDates As Boolean = False, _
                                    Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as formunkid, ' ' +  @name as name,'' AS sDate, '' AS eDate, 0 AS yearunkid  UNION "
            End If

            strQ &= " SELECT lvleaveform.formunkid, formno as name, Convert(Char(8),lvleaveform.startdate,112) AS sDate, " & _
                        " Convert(Char(8),lvleaveform.returndate,112) AS eDate,ISNULL(lvleaveIssue_master.leaveyearunkid,0) AS yearunkid FROM lvleaveform  " & _
                         " LEFT JOIN lvleaveIssue_master ON lvleaveIssue_master.formunkid  = lvleaveform.formunkid AND lvleaveIssue_master.isvoid = 0  " & _
                         " AND lvleaveIssue_master.leavetypeunkid =  lvleaveform.leavetypeunkid " & _
                         " WHERE lvleaveform.isvoid = 0 "

            If iLeaveTypeId > 0 Then
                strQ &= " AND lvleaveform.leavetypeunkid = '" & iLeaveTypeId & "'"
            End If

            If iStatusIds.Trim.Length > 0 Then
                strQ &= " AND statusunkid in (" & iStatusIds & ")"
            End If

            If iEmpId > 0 Then
                strQ &= " AND lvleaveform.employeeunkid = '" & iEmpId & "'"
            End If

            strQ &= " ORDER BY name "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iConcateDates = True Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    If dRow.Item("formunkid") <= 0 Then Continue For

                    'Pinkal (17-Jun-2016) -- Start
                    'Enhancement - Solved Bug when end date is NULL for Sick Leave For Applied 
                    If IsDBNull(dRow.Item("eDate")) OrElse IsDBNull(dRow.Item("sDate")) Then Continue For
                    'Pinkal (17-Jun-2016) -- End

                    dRow.Item("name") = dRow.Item("name") & " [ " & eZeeDate.convertDate(dRow.Item("sDate")) & " - " & eZeeDate.convertDate(dRow.Item("eDate")) & " ]"
                Next
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCurrentYearLeaveForm", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    'Pinkal (24-May-2014) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function GetLeaveFormForExpense(ByVal iLeaveTypeId As Integer, _
                                       Optional ByVal iStatusIds As String = "", _
                                       Optional ByVal iEmpId As Integer = -1, _
                                       Optional ByVal iConcateDates As Boolean = False, _
                                       Optional ByVal mblFlag As Boolean = False, _
                                       Optional ByVal iLeaveFormID As Integer = -1) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as formunkid, ' ' +  @name as name,'' AS sDate, '' AS eDate   UNION "
            End If

            strQ &= " SELECT formunkid, formno as name, Convert(Char(8),startdate,112) AS sDate, Convert(Char(8),returndate,112) AS eDate " & _
                        "  FROM lvleaveform  " & _
                        "  WHERE formunkid NOT IN ( " & _
                        "  SELECT referenceunkid " & _
                        "  FROM cmclaim_request_master " & _
                        "  WHERE cmclaim_request_master.isvoid = 0 AND cmclaim_request_master.modulerefunkid = " & enModuleReference.Leave & " AND referenceunkid <> " & iLeaveFormID


            'Pinkal (22-Jun-2015) -- Start
            'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.
            strQ &= " AND cmclaim_request_master.statusunkid IN (1,2))"
            'Pinkal (22-Jun-2015) -- End

            strQ &= "  AND  lvleaveform.isvoid = 0 AND lvleaveform.leavetypeunkid = '" & iLeaveTypeId & "' "

            If iStatusIds.Trim.Length > 0 Then
                strQ &= " AND lvleaveform.statusunkid in (" & iStatusIds & ")"
            End If

            If iEmpId > 0 Then
                strQ &= " AND lvleaveform.employeeunkid = '" & iEmpId & "'"
            End If

            strQ &= " ORDER BY name "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iConcateDates = True Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    If dRow.Item("formunkid") <= 0 Then Continue For
                    dRow.Item("name") = dRow.Item("name") & " [ " & eZeeDate.convertDate(dRow.Item("sDate")) & " - " & eZeeDate.convertDate(dRow.Item("eDate")) & " ]"
                Next
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "GetLeaveFormForExpense", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    'Pinkal (06-Mar-2014) -- End

    'Pinkal (14-Dec-2017) -- Start
    'Bug - SUPPORT B5 1738 | EXPENSE ASSIGNMENT TAKES TOO LONG.

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <returns>Boolean</returns>
    'Public Function GetEmployeeTotalAppliedDays(ByVal intYearID As Integer, ByVal intEmpID As Integer, ByVal intLeaveTypeId As Integer, ByVal mdtStartDate As Date, ByVal mdtEndDate As Date) As Decimal
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim mdecAppliedDaysCount As Decimal = 0
    '    Dim mdecPreApplieddaysCount As Decimal = 0
    '    Dim mstrFormIDs As String = ""
    '    Try
    '        Dim objDataOperation As New clsDataOperation
    '        objDataOperation.ClearParameters()
    '        If mdtStartDate <> Nothing AndAlso mdtStartDate < FinancialYear._Object._Database_Start_Date.Date Then
    '            Dim mintCompany As Integer = 0
    '            Dim mstrPreviousDBName As String = ""
    '            Dim mdtFYStartDate As Date = Nothing
    '            Dim mdtFYEndDate As Date = Nothing
    '            Dim mintLeaveBalanceSetting As Integer = enLeaveBalanceSetting.Financial_Year

    '            strQ = "SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE yearunkid = " & intYearID & "  "
    '            Dim dsCompany As DataSet = objDataOperation.ExecQuery(strQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If dsCompany IsNot Nothing AndAlso dsCompany.Tables(0).Rows.Count > 0 Then
    '                mintCompany = CInt(dsCompany.Tables(0).Rows(0)("companyunkid"))
    '            End If

    '            If mintCompany > 0 Then

    '                strQ = "SELECT ISNULL(key_value,1) AS key_value  from hrmsConfiguration..cfconfiguration WHERE key_name = 'LeaveBalanceSetting' and companyunkid = " & mintCompany
    '                Dim dsLeaveBalanceSetting As DataSet = objDataOperation.ExecQuery(strQ, "List")

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If

    '                If dsLeaveBalanceSetting IsNot Nothing AndAlso dsLeaveBalanceSetting.Tables(0).Rows.Count > 0 Then
    '                    mintLeaveBalanceSetting = CInt(dsLeaveBalanceSetting.Tables(0).Rows(0)("key_value"))
    '                End If

    '                If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

    '                    strQ = "SELECT yearunkid,database_name,start_date,end_date FROM hrmsConfiguration..cffinancial_year_tran WHERE companyunkid = " & mintCompany & " AND isclosed = 1 AND @stdate BETWEEN start_date AND end_date "
    '                    objDataOperation.ClearParameters()
    '                    objDataOperation.AddParameter("@stdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
    '                    Dim dsDBName As DataSet = objDataOperation.ExecQuery(strQ, "List")

    '                    If objDataOperation.ErrorMessage <> "" Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If

    '                    If dsDBName IsNot Nothing AndAlso dsDBName.Tables(0).Rows.Count > 0 Then
    '                        mstrPreviousDBName = dsDBName.Tables(0).Rows(0)("database_name").ToString()
    '                        mdtFYStartDate = CDate(dsDBName.Tables(0).Rows(0)("start_date"))
    '                        mdtFYEndDate = CDate(dsDBName.Tables(0).Rows(0)("end_date"))
    '                    End If

    '                    If mstrPreviousDBName.Trim.Length > 0 Then

    '                        objDataOperation.ClearParameters()

    '                        strQ = " SELECT ISNULL(SUM(lvleaveday_fraction.dayfraction),0) AS applieddays from " & mstrPreviousDBName & "..lvleaveform " & _
    '                                  " JOIN " & mstrPreviousDBName & "..lvleaveday_fraction on lvleaveday_fraction.formunkid = lvleaveform.formunkid and lvleaveday_fraction.isvoid = 0 and ISNULL(lvleaveday_fraction.approverunkid,0) <=0 " & _
    '                                  " WHERE lvleaveform.employeeunkid = @Employeeunkid and lvleaveform.leavetypeunkid = @LeaveTypeunkid and lvleaveform.isvoid = 0"


    '                        'Pinkal (01-Dec-2014) -- Start
    '                        'Enhancement - ENHANCEMENT GIVEN BY TRA - EMPLOYEE CAN APPLY LEAVE BEYOND ELC END DATE, IF EMPLOYEE HAVE LEAVE BALANCE .

    '                        'If mdtStartDate <> Nothing Then
    '                        '    strQ &= " AND CONVERT(CHAR(8),lvleaveday_fraction.leavedate,112) >= @startdate"
    '                        '    objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
    '                        'End If

    '                        'If mdtEndDate <> Nothing Then
    '                        '    strQ &= " AND CONVERT(CHAR(8),lvleaveday_fraction.leavedate,112) <= @enddate"
    '                        '    objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFYEndDate))
    '                        'End If

    '                        If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
    '                            strQ &= " AND CONVERT(CHAR(8),lvleaveform.startdate,112) BETWEEN  @startdate AND @enddate "
    '                            objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
    '                            objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFYEndDate))
    '                        End If

    '                        'Pinkal (01-Dec-2014) -- End

    '                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
    '                        objDataOperation.AddParameter("@LeaveTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeId)
    '                        dsList = objDataOperation.ExecQuery(strQ, "List")


    '                        If objDataOperation.ErrorMessage <> "" Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If

    '                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
    '                            mdecPreApplieddaysCount = CDec(dsList.Tables("List").Rows(0)("applieddays"))
    '                        End If


    '                        'Pinkal (01-Dec-2014) -- Start
    '                        'Enhancement - ENHANCEMENT GIVEN BY TRA - EMPLOYEE CAN APPLY LEAVE BEYOND ELC END DATE, IF EMPLOYEE HAVE LEAVE BALANCE .
    '                        objDataOperation.ClearParameters()

    '                        strQ = " SELECT ISNULL(STUFF((SELECT DISTINCT  ',' + CONVERT(NVARCHAR(max), lvleaveform.formunkid) FROM " & mstrPreviousDBName & "..lvleaveIssue_master " & _
    '                                 " JOIN " & mstrPreviousDBName & "..lvleaveIssue_tran ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvleaveIssue_tran.isvoid = 0 "

    '                        If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
    '                            strQ &= " JOIN  " & mstrPreviousDBName & "..lvleaveform ON lvleaveform.formunkid = lvleaveIssue_master.formunkid AND lvleaveform.isvoid = 0 " & _
    '                                        " AND  CONVERT(char(8),lvleaveform.approve_stdate,112) BETWEEN @startdate AND @enddate "
    '                        End If

    '                        strQ &= " WHERE lvleaveIssue_master.Employeeunkid = @Employeeunkid And lvleaveIssue_master.LeaveTypeunkid = @LeaveTypeunkid and lvleaveIssue_master.isvoid = 0  FOR XML PATH('')),1,1,''),'') AS formunkid"

    '                        If mdtStartDate <> Nothing Then
    '                            objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
    '                        End If

    '                        If mdtEndDate <> Nothing Then
    '                            objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFYEndDate))
    '                        End If

    '                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
    '                        objDataOperation.AddParameter("@LeaveTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeId)
    '                        dsList = objDataOperation.ExecQuery(strQ, "List")


    '                        If objDataOperation.ErrorMessage <> "" Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If

    '                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
    '                            mstrFormIDs = dsList.Tables("List").Rows(0)("formunkid").ToString()
    '                        End If

    '                        'Pinkal (01-Dec-2014) -- End


    '                    End If

    '                End If

    '            End If

    '            objDataOperation.ClearParameters()

    '            strQ = " SELECT ISNULL(SUM(lvleaveday_fraction.dayfraction),0) AS applieddays from lvleaveform " & _
    '                      " JOIN lvleaveday_fraction on lvleaveday_fraction.formunkid = lvleaveform.formunkid and lvleaveday_fraction.isvoid = 0 and ISNULL(lvleaveday_fraction.approverunkid,0) <=0 " & _
    '                      " WHERE lvleaveform.employeeunkid = @Employeeunkid and lvleaveform.leavetypeunkid = @LeaveTypeunkid and lvleaveform.isvoid = 0"


    '            'Pinkal (01-Dec-2014) -- Start
    '            'Enhancement - ENHANCEMENT GIVEN BY TRA - EMPLOYEE CAN APPLY LEAVE BEYOND ELC END DATE, IF EMPLOYEE HAVE LEAVE BALANCE .

    '            'If mdtStartDate <> Nothing Then
    '            '    strQ &= " AND CONVERT(CHAR(8),lvleaveday_fraction.leavedate,112) >= @startdate"
    '            '    objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
    '            'End If

    '            'If mdtEndDate <> Nothing Then
    '            '    strQ &= " AND CONVERT(CHAR(8),lvleaveday_fraction.leavedate,112) <= @enddate"
    '            '    objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
    '            'End If

    '            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
    '                strQ &= " AND CONVERT(CHAR(8),lvleaveform.startdate,112) BETWEEN @startdate AND @enddate"
    '                objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
    '                objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))

    '                If mstrFormIDs.Trim.Length > 0 Then
    '                    strQ &= " AND lvleaveform.formunkid NOT in (" & mstrFormIDs & ")"
    '                End If

    '            End If

    '            'Pinkal (01-Dec-2014) -- End

    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
    '            objDataOperation.AddParameter("@LeaveTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeId)
    '            dsList = objDataOperation.ExecQuery(strQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If dsList IsNot Nothing AndAlso dsList.Tables("List").Rows.Count > 0 Then
    '                mdecAppliedDaysCount = CDec(dsList.Tables("List").Rows(0)("applieddays"))
    '            End If

    '            mdecAppliedDaysCount += mdecPreApplieddaysCount

    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeTotalAppliedDays; Module Name: " & mstrModuleName)
    '    End Try
    '    Return mdecAppliedDaysCount
    'End Function

    Public Function GetEmployeeTotalAppliedDays(ByVal intYearID As Integer, ByVal intEmpID As Integer, ByVal intLeaveTypeId As Integer, ByVal mdtStartDate As Date _
                                                                       , ByVal mdtEndDate As Date, ByVal mintCompany As Integer, ByVal mdtDatabaseStartDate As Date, ByVal mintLeaveBalanceSetting As Integer _
                                                                       , ByVal mstrPreviousDBName As String, ByVal mdtDBPreviousStartDate As Date, ByVal mdtDBPreviousEndDate As Date) As Decimal
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mdecAppliedDaysCount As Decimal = 0
        Dim mdecPreApplieddaysCount As Decimal = 0
        Dim mstrFormIDs As String = ""
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()
            If mdtStartDate <> Nothing AndAlso mdtStartDate < mdtDatabaseStartDate Then
                'Dim mintCompany As Integer = 0
                'Dim mstrPreviousDBName As String = ""
                'Dim mdtFYStartDate As Date = Nothing
                'Dim mdtFYEndDate As Date = Nothing
                'Dim mintLeaveBalanceSetting As Integer = enLeaveBalanceSetting.Financial_Year

                'strQ = "SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE yearunkid = " & intYearID & "  "
                'Dim dsCompany As DataSet = objDataOperation.ExecQuery(strQ, "List")

                'If objDataOperation.ErrorMessage <> "" Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If

                'If dsCompany IsNot Nothing AndAlso dsCompany.Tables(0).Rows.Count > 0 Then
                '    mintCompany = CInt(dsCompany.Tables(0).Rows(0)("companyunkid"))
                'End If

                If mintCompany > 0 Then

                    'strQ = "SELECT ISNULL(key_value,1) AS key_value  from hrmsConfiguration..cfconfiguration WHERE key_name = 'LeaveBalanceSetting' and companyunkid = " & mintCompany
                    'Dim dsLeaveBalanceSetting As DataSet = objDataOperation.ExecQuery(strQ, "List")

                    'If objDataOperation.ErrorMessage <> "" Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If

                    'If dsLeaveBalanceSetting IsNot Nothing AndAlso dsLeaveBalanceSetting.Tables(0).Rows.Count > 0 Then
                    '    mintLeaveBalanceSetting = CInt(dsLeaveBalanceSetting.Tables(0).Rows(0)("key_value"))
                    'End If

                    If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                        'strQ = "SELECT yearunkid,database_name,start_date,end_date FROM hrmsConfiguration..cffinancial_year_tran WHERE companyunkid = " & mintCompany & " AND isclosed = 1 AND @stdate BETWEEN start_date AND end_date "
                        'objDataOperation.ClearParameters()
                        'objDataOperation.AddParameter("@stdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
                        'Dim dsDBName As DataSet = objDataOperation.ExecQuery(strQ, "List")

                        'If objDataOperation.ErrorMessage <> "" Then
                        '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        '    Throw exForce
                        'End If

                        'If dsDBName IsNot Nothing AndAlso dsDBName.Tables(0).Rows.Count > 0 Then
                        '    mstrPreviousDBName = dsDBName.Tables(0).Rows(0)("database_name").ToString()
                        '    mdtFYStartDate = CDate(dsDBName.Tables(0).Rows(0)("start_date"))
                        '    mdtFYEndDate = CDate(dsDBName.Tables(0).Rows(0)("end_date"))
                        'End If

                        If mstrPreviousDBName.Trim.Length > 0 Then

                            objDataOperation.ClearParameters()

                            strQ = " SELECT ISNULL(SUM(lvleaveday_fraction.dayfraction),0) AS applieddays from " & mstrPreviousDBName & "..lvleaveform " & _
                                      " JOIN " & mstrPreviousDBName & "..lvleaveday_fraction on lvleaveday_fraction.formunkid = lvleaveform.formunkid and lvleaveday_fraction.isvoid = 0 and ISNULL(lvleaveday_fraction.approverunkid,0) <=0 " & _
                                      " WHERE lvleaveform.employeeunkid = @Employeeunkid and lvleaveform.leavetypeunkid = @LeaveTypeunkid and lvleaveform.isvoid = 0"


                            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                                strQ &= " AND CONVERT(CHAR(8),lvleaveform.startdate,112) BETWEEN  @startdate AND @enddate "
                                objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
                                objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDBPreviousEndDate))
                            End If
                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
                            objDataOperation.AddParameter("@LeaveTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeId)
                            dsList = objDataOperation.ExecQuery(strQ, "List")


                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                                mdecPreApplieddaysCount = CDec(dsList.Tables("List").Rows(0)("applieddays"))
                            End If


                            'Pinkal (01-Dec-2014) -- Start
                            'Enhancement - ENHANCEMENT GIVEN BY TRA - EMPLOYEE CAN APPLY LEAVE BEYOND ELC END DATE, IF EMPLOYEE HAVE LEAVE BALANCE .
                            objDataOperation.ClearParameters()

                            strQ = " SELECT ISNULL(STUFF((SELECT DISTINCT  ',' + CONVERT(NVARCHAR(max), lvleaveform.formunkid) FROM " & mstrPreviousDBName & "..lvleaveIssue_master " & _
                                     " JOIN " & mstrPreviousDBName & "..lvleaveIssue_tran ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvleaveIssue_tran.isvoid = 0 "

                            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                                strQ &= " JOIN  " & mstrPreviousDBName & "..lvleaveform ON lvleaveform.formunkid = lvleaveIssue_master.formunkid AND lvleaveform.isvoid = 0 " & _
                                            " AND  CONVERT(char(8),lvleaveform.approve_stdate,112) BETWEEN @startdate AND @enddate "
                            End If

                            strQ &= " WHERE lvleaveIssue_master.Employeeunkid = @Employeeunkid And lvleaveIssue_master.LeaveTypeunkid = @LeaveTypeunkid and lvleaveIssue_master.isvoid = 0  FOR XML PATH('')),1,1,''),'') AS formunkid"

                            If mdtStartDate <> Nothing Then
                                objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
                            End If

                            If mdtEndDate <> Nothing Then
                                objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDBPreviousEndDate))
                            End If

                            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
                            objDataOperation.AddParameter("@LeaveTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeId)
                            dsList = objDataOperation.ExecQuery(strQ, "List")


                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                                mstrFormIDs = dsList.Tables("List").Rows(0)("formunkid").ToString()
                            End If


                        End If

                    End If

                End If

                objDataOperation.ClearParameters()

                strQ = " SELECT ISNULL(SUM(lvleaveday_fraction.dayfraction),0) AS applieddays from lvleaveform " & _
                          " JOIN lvleaveday_fraction on lvleaveday_fraction.formunkid = lvleaveform.formunkid and lvleaveday_fraction.isvoid = 0 and ISNULL(lvleaveday_fraction.approverunkid,0) <=0 " & _
                          " WHERE lvleaveform.employeeunkid = @Employeeunkid and lvleaveform.leavetypeunkid = @LeaveTypeunkid and lvleaveform.isvoid = 0"


                If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                    strQ &= " AND CONVERT(CHAR(8),lvleaveform.startdate,112) BETWEEN @startdate AND @enddate"
                    objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
                    objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))

                    If mstrFormIDs.Trim.Length > 0 Then
                        strQ &= " AND lvleaveform.formunkid NOT in (" & mstrFormIDs & ")"
                    End If

                End If

                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
                objDataOperation.AddParameter("@LeaveTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeId)
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList IsNot Nothing AndAlso dsList.Tables("List").Rows.Count > 0 Then
                    mdecAppliedDaysCount = CDec(dsList.Tables("List").Rows(0)("applieddays"))
                End If

                mdecAppliedDaysCount += mdecPreApplieddaysCount

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeTotalAppliedDays; Module Name: " & mstrModuleName)
        End Try
        Return mdecAppliedDaysCount
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    'Public Function GetEmployeeTotalApprovedDays(ByVal intYearID As Integer, ByVal intEmpID As Integer, ByVal intLeaveTypeId As Integer, ByVal mdtStartDate As Date, ByVal mdtEndDate As Date) As Decimal
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim mdecApprovedDaysCount As Decimal = 0
    '    Dim mdecPreApproveddaysCount As Decimal = 0
    '    Dim mstrFormIDs As String = ""
    '    Try
    '        Dim objDataOperation As New clsDataOperation
    '        objDataOperation.ClearParameters()
    '        If mdtStartDate <> Nothing AndAlso mdtStartDate < FinancialYear._Object._Database_Start_Date.Date Then
    '            Dim mintCompany As Integer = 0
    '            Dim mstrPreviousDBName As String = ""
    '            Dim mdtFYStartDate As Date = Nothing
    '            Dim mdtFYEndDate As Date = Nothing
    '            Dim mintLeaveBalanceSetting As Integer = enLeaveBalanceSetting.Financial_Year

    '            strQ = "SELECT companyunkid FROM hrmsConfiguration..cffinancial_year_tran WHERE yearunkid = " & intYearID & "  "
    '            Dim dsCompany As DataSet = objDataOperation.ExecQuery(strQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If dsCompany IsNot Nothing AndAlso dsCompany.Tables(0).Rows.Count > 0 Then
    '                mintCompany = CInt(dsCompany.Tables(0).Rows(0)("companyunkid"))
    '            End If

    '            If mintCompany > 0 Then

    '                strQ = "SELECT ISNULL(key_value,1) AS key_value  from hrmsConfiguration..cfconfiguration WHERE key_name = 'LeaveBalanceSetting' and companyunkid = " & mintCompany
    '                Dim dsLeaveBalanceSetting As DataSet = objDataOperation.ExecQuery(strQ, "List")

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If

    '                If dsLeaveBalanceSetting IsNot Nothing AndAlso dsLeaveBalanceSetting.Tables(0).Rows.Count > 0 Then
    '                    mintLeaveBalanceSetting = CInt(dsLeaveBalanceSetting.Tables(0).Rows(0)("key_value"))
    '                End If

    '                If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

    '                    strQ = "SELECT yearunkid,database_name,start_date,end_date FROM hrmsConfiguration..cffinancial_year_tran WHERE companyunkid = " & mintCompany & " AND isclosed = 1 AND @stdate BETWEEN start_date AND end_date "
    '                    objDataOperation.ClearParameters()
    '                    objDataOperation.AddParameter("@stdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
    '                    Dim dsDBName As DataSet = objDataOperation.ExecQuery(strQ, "List")

    '                    If objDataOperation.ErrorMessage <> "" Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If

    '                    If dsDBName IsNot Nothing AndAlso dsDBName.Tables(0).Rows.Count > 0 Then
    '                        mstrPreviousDBName = dsDBName.Tables(0).Rows(0)("database_name").ToString()
    '                        mdtFYStartDate = CDate(dsDBName.Tables(0).Rows(0)("start_date"))
    '                        mdtFYEndDate = CDate(dsDBName.Tables(0).Rows(0)("end_date"))
    '                    End If

    '                    If mstrPreviousDBName.Trim.Length > 0 Then

    '                        objDataOperation.ClearParameters()
    '                        strQ = " SELECT ISNULL(SUM(approve_days),0) AS approveddays from " & mstrPreviousDBName & "..lvleaveform " & _
    '                                  " WHERE lvleaveform.employeeunkid = @Employeeunkid and lvleaveform.leavetypeunkid = @LeaveTypeunkid and lvleaveform.isvoid = 0"


    '                        'Pinkal (01-Dec-2014) -- Start
    '                        'Enhancement - ENHANCEMENT GIVEN BY TRA - EMPLOYEE CAN APPLY LEAVE BEYOND ELC END DATE, IF EMPLOYEE HAVE LEAVE BALANCE .

    '                        If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
    '                            strQ &= " AND CONVERT(CHAR(8),lvleaveform.approve_stdate,112) Between @startdate AND @enddate"
    '                            objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
    '                            objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFYEndDate))
    '                        End If

    '                        'If mdtEndDate <> Nothing Then
    '                        '    strQ &= " AND CONVERT(CHAR(8),lvleaveform.returndate,112) <= "
    '                        '    objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFYEndDate))
    '                        'End If

    '                        'Pinkal (01-Dec-2014) -- End


    '                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
    '                        objDataOperation.AddParameter("@LeaveTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeId)
    '                        dsList = objDataOperation.ExecQuery(strQ, "List")


    '                        If objDataOperation.ErrorMessage <> "" Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If

    '                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
    '                            mdecPreApproveddaysCount = CDec(dsList.Tables("List").Rows(0)("approveddays"))
    '                        End If

    '                        'Pinkal (01-Dec-2014) -- Start
    '                        'Enhancement - ENHANCEMENT GIVEN BY TRA - EMPLOYEE CAN APPLY LEAVE BEYOND ELC END DATE, IF EMPLOYEE HAVE LEAVE BALANCE .
    '                        objDataOperation.ClearParameters()

    '                        strQ = " SELECT ISNULL(STUFF((SELECT DISTINCT  ',' + CONVERT(NVARCHAR(max), lvleaveform.formunkid) FROM " & mstrPreviousDBName & "..lvleaveIssue_master " & _
    '                                 " JOIN " & mstrPreviousDBName & "..lvleaveIssue_tran ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvleaveIssue_tran.isvoid = 0 "

    '                        If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
    '                            strQ &= " JOIN  " & mstrPreviousDBName & "..lvleaveform ON lvleaveform.formunkid = lvleaveIssue_master.formunkid AND lvleaveform.isvoid = 0 " & _
    '                                        " AND  CONVERT(char(8),lvleaveform.approve_stdate,112) BETWEEN @startdate AND @enddate "
    '                        End If

    '                        strQ &= " WHERE lvleaveIssue_master.Employeeunkid = @Employeeunkid And lvleaveIssue_master.LeaveTypeunkid = @LeaveTypeunkid and lvleaveIssue_master.isvoid = 0  FOR XML PATH('')),1,1,''),'') AS formunkid"

    '                        If mdtStartDate <> Nothing Then
    '                            objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
    '                        End If

    '                        If mdtEndDate <> Nothing Then
    '                            objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFYEndDate))
    '                        End If

    '                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
    '                        objDataOperation.AddParameter("@LeaveTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeId)
    '                        dsList = objDataOperation.ExecQuery(strQ, "List")


    '                        If objDataOperation.ErrorMessage <> "" Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If

    '                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
    '                            mstrFormIDs = dsList.Tables("List").Rows(0)("formunkid").ToString()
    '                        End If

    '                        'Pinkal (01-Dec-2014) -- End


    '                    End If

    '                End If

    '            End If

    '            objDataOperation.ClearParameters()

    '            strQ = " SELECT ISNULL(SUM(approve_days),0) AS approveddays from lvleaveform " & _
    '                      " WHERE lvleaveform.employeeunkid = @Employeeunkid and lvleaveform.leavetypeunkid = @LeaveTypeunkid and lvleaveform.isvoid = 0"


    '            'Pinkal (01-Dec-2014) -- Start
    '            'Enhancement - ENHANCEMENT GIVEN BY TRA - EMPLOYEE CAN APPLY LEAVE BEYOND ELC END DATE, IF EMPLOYEE HAVE LEAVE BALANCE .

    '            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
    '                strQ &= " AND CONVERT(CHAR(8),lvleaveform.approve_stdate,112) Between @startdate AND @enddate"
    '                objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
    '                objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))

    '                If mstrFormIDs.Trim.Length > 0 Then
    '                    strQ &= " AND lvleaveform.formunkid NOT in (" & mstrFormIDs & ")"
    '                End If

    '            End If

    '            'If mdtEndDate <> Nothing Then
    '            '    strQ &= " AND CONVERT(CHAR(8),lvleaveform.returndate,112) <= @enddate"
    '            '    objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
    '            'End If

    '            'Pinkal (01-Dec-2014) -- End

    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
    '            objDataOperation.AddParameter("@LeaveTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeId)
    '            dsList = objDataOperation.ExecQuery(strQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If dsList IsNot Nothing AndAlso dsList.Tables("List").Rows.Count > 0 Then
    '                mdecApprovedDaysCount = CDec(dsList.Tables("List").Rows(0)("approveddays"))
    '            End If

    '            mdecApprovedDaysCount += mdecPreApproveddaysCount

    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeTotalApprovedDays; Module Name: " & mstrModuleName)
    '    End Try
    '    Return mdecApprovedDaysCount
    'End Function

    Public Function GetEmployeeTotalApprovedDays(ByVal intYearID As Integer, ByVal intEmpID As Integer, ByVal intLeaveTypeId As Integer, ByVal mdtStartDate As Date, ByVal mdtEndDate As Date _
                                                                          , ByVal mintCompany As Integer, ByVal mdtDatabaseStartDate As Date, ByVal mintLeaveBalanceSetting As Integer _
                                                                          , ByVal mstrPreviousDBName As String, ByVal mdtDBPreviousStartDate As Date, ByVal mdtDBPreviousEndDate As Date) As Decimal
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mdecApprovedDaysCount As Decimal = 0
        Dim mdecPreApproveddaysCount As Decimal = 0
        Dim mstrFormIDs As String = ""
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()
            If mdtStartDate <> Nothing AndAlso mdtStartDate < mdtDatabaseStartDate Then
                If mintCompany > 0 AndAlso mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC AndAlso mstrPreviousDBName.Trim.Length > 0 Then

                    objDataOperation.ClearParameters()
                    strQ = " SELECT ISNULL(SUM(approve_days),0) AS approveddays from " & mstrPreviousDBName & "..lvleaveform " & _
                              " WHERE lvleaveform.employeeunkid = @Employeeunkid and lvleaveform.leavetypeunkid = @LeaveTypeunkid and lvleaveform.isvoid = 0"

                    If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                        strQ &= " AND CONVERT(CHAR(8),lvleaveform.approve_stdate,112) Between @startdate AND @enddate"
                        objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
                        objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDBPreviousEndDate))
                    End If

                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
                    objDataOperation.AddParameter("@LeaveTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeId)
                    dsList = objDataOperation.ExecQuery(strQ, "List")


                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        mdecPreApproveddaysCount = CDec(dsList.Tables("List").Rows(0)("approveddays"))
                    End If


                    objDataOperation.ClearParameters()

                    strQ = " SELECT ISNULL(STUFF((SELECT DISTINCT  ',' + CONVERT(NVARCHAR(max), lvleaveform.formunkid) FROM " & mstrPreviousDBName & "..lvleaveIssue_master " & _
                             " JOIN " & mstrPreviousDBName & "..lvleaveIssue_tran ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid AND lvleaveIssue_tran.isvoid = 0 "

                    If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                        strQ &= " JOIN  " & mstrPreviousDBName & "..lvleaveform ON lvleaveform.formunkid = lvleaveIssue_master.formunkid AND lvleaveform.isvoid = 0 " & _
                                    " AND  CONVERT(char(8),lvleaveform.approve_stdate,112) BETWEEN @startdate AND @enddate "
                    End If

                    strQ &= " WHERE lvleaveIssue_master.Employeeunkid = @Employeeunkid And lvleaveIssue_master.LeaveTypeunkid = @LeaveTypeunkid and lvleaveIssue_master.isvoid = 0  FOR XML PATH('')),1,1,''),'') AS formunkid"

                    If mdtStartDate <> Nothing Then
                        objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
                    End If

                    If mdtEndDate <> Nothing Then
                        objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDBPreviousEndDate))
                    End If

                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
                    objDataOperation.AddParameter("@LeaveTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeId)
                    dsList = objDataOperation.ExecQuery(strQ, "List")


                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        mstrFormIDs = dsList.Tables("List").Rows(0)("formunkid").ToString()
                    End If


                End If

                objDataOperation.ClearParameters()

                strQ = " SELECT ISNULL(SUM(approve_days),0) AS approveddays from lvleaveform " & _
                          " WHERE lvleaveform.employeeunkid = @Employeeunkid and lvleaveform.leavetypeunkid = @LeaveTypeunkid and lvleaveform.isvoid = 0"


                If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                    strQ &= " AND CONVERT(CHAR(8),lvleaveform.approve_stdate,112) Between @startdate AND @enddate"
                    objDataOperation.AddParameter("@startdate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
                    objDataOperation.AddParameter("@enddate", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))

                    If mstrFormIDs.Trim.Length > 0 Then
                        strQ &= " AND lvleaveform.formunkid NOT in (" & mstrFormIDs & ")"
                    End If

                End If

                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
                objDataOperation.AddParameter("@LeaveTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeId)
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList IsNot Nothing AndAlso dsList.Tables("List").Rows.Count > 0 Then
                    mdecApprovedDaysCount = CDec(dsList.Tables("List").Rows(0)("approveddays"))
                End If

                mdecApprovedDaysCount += mdecPreApproveddaysCount

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeTotalApprovedDays; Module Name: " & mstrModuleName)
        End Try
        Return mdecApprovedDaysCount
    End Function

    'Pinkal (14-Dec-2017) -- End
    
    'Pinkal (01-Jan-2019) -- Start
    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function GetEmployeeIdsFromIssuedForm(ByVal xLeaveTypeId As Integer, ByVal dtStartDate As Date, ByVal dtEnddate As Date) As String
        Dim mstrEmployeeIDs As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation


            Dim strQ As String = " SELECT " & _
                                                     "      ISNULL(STUFF((SELECT DISTINCT ',' + CONVERT(NVARCHAR(MAX), lvleaveform.employeeunkid) " & _
                                                     " FROM lvleaveform " & _
                                                     " WHERE lvleaveform.isvoid = 0 " & _
                                                     " AND (@startdate BETWEEN CONVERT(CHAR(8), lvleaveform.approve_stdate, 112) AND CONVERT(CHAR(8), lvleaveform.approve_eddate, 112) " & _
                                                     "             OR @enddate BETWEEN CONVERT(CHAR(8), lvleaveform.approve_stdate, 112) AND CONVERT(CHAR(8), lvleaveform.approve_eddate, 112)) " & _
                                                     "             OR (CONVERT(CHAR(8), lvleaveform.approve_stdate, 112) BETWEEN @startdate AND @enddate " & _
                                                     "              AND CONVERT(CHAR(8), lvleaveform.approve_eddate, 112) BETWEEN @startdate AND @enddate " & _
                                                     " )  AND lvleaveform.statusunkid = 7 AND lvleaveform.leavetypeunkid = @leavetypeunkid 	FOR XML PATH ('')) , 1, 1, ''), '') AS EmployeeIds "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtStartDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtEnddate))
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xLeaveTypeId)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mstrEmployeeIDs = dsList.Tables("List").Rows(0)("EmployeeIds").ToString()
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeIdsFromIssuedForm; Module Name: " & mstrModuleName)
        End Try
        Return mstrEmployeeIDs
    End Function

    'Pinkal (01-Jan-2019) -- End

    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Public Function GetLeaveFormForBlockLeaveStatus(ByVal xEmployeeId As Integer, ByVal xLeaveTypeId As Integer) As Integer
        Dim mintLeaveStatusId As Integer = 0
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            Dim strQ As String = " SELECT ISNULL(statusunkid,0) AS statusunkid  " & _
                                                      " FROM lvleaveform  " & _
                                                     " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid AND isvoid = 0 AND statusunkid = 7 " & _
                                                     " Order By approve_eddate DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xLeaveTypeId)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintLeaveStatusId = CInt(dsList.Tables(0).Rows(0)("statusunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLeaveFormForBlockLeaveStatus; Module Name: " & mstrModuleName)
        End Try
        Return mintLeaveStatusId
    End Function
    'Pinkal (26-Feb-2019) -- End

    'S.SANDEEP |08-APR-2019| -- START
#Region " Flex_CBS_Block_Employee"

    'S.SANDEEP |11-APR-2019| -- START
    'Public Function BlockEmployeeInCBS(ByVal intEmpId As Integer, _
    '                                   ByVal dtStartDate As Date, _
    '                                   ByVal dtEndDate As Date, _
    '                                   ByVal strFormNumber As String, _
    '                                   ByVal dtCurrentDate As Date, _
    '                                   ByVal intCompanyId As Integer, _
    '                                   ByVal strUserState As String, _
    '                                   ByVal objDataOper As clsDataOperation) As Boolean
    '    Dim StrQ As String = String.Empty
    '    Dim dsData As New DataSet
    '    Dim objConfig As New clsConfigOptions
    '    Dim strServiceURL As String = ""
    '    Dim straccno As String = String.Empty
    '    Dim strcccno As String = String.Empty
    '    Try
    '        Dim objEmp As New clsEmployee_Master
    '        objDataOper.ClearParameters()
    '        objConfig._Companyunkid = intCompanyId
    '        If objConfig._IsHRFlexcubeIntegrated Then

    '            If dtCurrentDate.Date < dtStartDate.Date AndAlso dtEndDate.Date < dtCurrentDate.Date Then Return True 'Less than today date for both start and end date -No action 

    '            'If dtCurrentDate.Date >= dtStartDate.Date And dtEndDate.Date >= dtCurrentDate.Date Then

    '            'S.SANDEEP |11-APR-2019| -- START
    '            'If objConfig._FlexcubeServiceCollection.ContainsKey(enFlexcubeServiceInfo.FLX_USER_SRV) Then
    '            '    strServiceURL = objConfig._FlexcubeServiceCollection(enFlexcubeServiceInfo.FLX_USER_SRV)
    '            If objConfig._FlexcubeServiceCollection.ContainsKey(enFlexcubeServiceInfo.FLX_BLOCK_USER) Then
    '                strServiceURL = objConfig._FlexcubeServiceCollection(enFlexcubeServiceInfo.FLX_BLOCK_USER)
    '                'S.SANDEEP |11-APR-2019| -- END

    '                If strServiceURL IsNot Nothing AndAlso strServiceURL.Trim.Length > 0 Then

    '                    StrQ = "SELECT 1 FROM hrflexcube_request_tran WHERE reqmst_number = '" & strFormNumber & "' AND iserror = 0 "

    '                    Dim intExists As Integer = objDataOper.RecordCount(StrQ)

    '                    If objDataOper.ErrorMessage <> "" Then
    '                        Throw New Exception(objDataOper.ErrorNumber & " : " & objDataOper.ErrorMessage)
    '                    End If

    '                    If intExists > 0 Then
    '                        Return True
    '                    End If

    '                    StrQ = "SELECT " & _
    '                           "    hremployee_master.employeeunkid " & _
    '                           "   ,EA.customcode " & _
    '                           "   ,EB.accn " & _
    '                           "   FROM hremployee_master " & _
    '                           "   LEFT JOIN " & _
    '                           "   ( " & _
    '                           "       SELECT " & _
    '                           "            A.employeeunkid " & _
    '                           "           ,A.customcode " & _
    '                           "       FROM " & _
    '                           "       ( " & _
    '                           "           SELECT " & _
    '                           "                employeeunkid " & _
    '                           "               ,customcode " & _
    '                           "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
    '                           "           FROM hremployee_cctranhead_tran " & _
    '                           "               JOIN prcostcenter_master ON cctranheadvalueid = prcostcenter_master.costcenterunkid " & _
    '                           "           WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @end_date " & _
    '                           "           AND employeeunkid = @employeeunkid AND istransactionhead = 0 " & _
    '                           "       ) AS A WHERE A.xNo = 1 " & _
    '                           "   ) AS EA ON EA.employeeunkid = hremployee_master.employeeunkid " & _
    '                           "   LEFT JOIN " & _
    '                           "   ( " & _
    '                           "       SELECT " & _
    '                           "            B.employeeunkid " & _
    '                           "           ,'''' + B.accountno + '''' AS accn " & _
    '                           "       FROM " & _
    '                           "       ( " & _
    '                           "           SELECT " & _
    '                           "                premployee_bank_tran.employeeunkid " & _
    '                           "               ,premployee_bank_tran.accountno " & _
    '                           "               ,DENSE_RANK() OVER (PARTITION  BY premployee_bank_tran.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
    '                           "           FROM premployee_bank_tran " & _
    '                           "               LEFT JOIN cfcommon_period_tran ON premployee_bank_tran.periodunkid = cfcommon_period_tran.periodunkid AND isactive = 1 " & _
    '                           "           WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date " & _
    '                           "       )AS B WHERE B.ROWNO = 1 " & _
    '                           "   ) AS EB ON EB.employeeunkid = hremployee_master.employeeunkid " & _
    '                           "WHERE hremployee_master.employeeunkid = @employeeunkid "

    '                    'StrQ = "SELECT " & _
    '                    '       "    '''' + A.accountno + '''' AS accn " & _
    '                    '       "FROM " & _
    '                    '       "( " & _
    '                    '       "    SELECT " & _
    '                    '       "         premployee_bank_tran.accountno " & _
    '                    '       "        ,DENSE_RANK() OVER (PARTITION  BY premployee_bank_tran.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
    '                    '       "    FROM premployee_bank_tran " & _
    '                    '       "        LEFT JOIN cfcommon_period_tran ON premployee_bank_tran.periodunkid = cfcommon_period_tran.periodunkid AND isactive = 1 " & _
    '                    '       "    WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date " & _
    '                    '       ")AS A WHERE A.ROWNO = 1 "

    '                    objDataOper.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
    '                    objDataOper.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtCurrentDate))

    '                    dsData = objDataOper.ExecQuery(StrQ, "List")

    '                    If objDataOper.ErrorMessage <> "" Then
    '                        Throw New Exception(objDataOper.ErrorNumber & " : " & objDataOper.ErrorMessage)
    '                    End If

    '                    If dsData.Tables(0).Rows.Count > 0 Then
    '                        straccno = String.Join(",", dsData.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of String)("accn")).ToArray())
    '                        strcccno = dsData.Tables(0).Rows(0)("customcode").ToString()
    '                    End If

    '                    If straccno.Trim.Length > 0 Then
    '                        dsData = New DataSet
    '                        Using cnnOracle As New OracleClient.OracleConnection
    '                            cnnOracle.ConnectionString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" & objConfig._OracleHostName & ")(PORT=" & objConfig._OraclePortNo & ")))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" & objConfig._OracleServiceName & "))); User Id=" & objConfig._OracleUserName & ";Password=" & objConfig._OracleUserPassword.ToString() & "; "
    '                            Try
    '                                cnnOracle.Open()

    '                                StrQ = "SELECT " & _
    '                                   "     USER_ID " & _
    '                                   "    ,USER_NAME " & _
    '                                   "FROM fcubs.aruti_user " & _
    '                                   "WHERE CUST_AC_NO IN (" & straccno & ") "
    '                                Using cmdOracle As New OracleClient.OracleCommand
    '                                    If cnnOracle.State = ConnectionState.Closed Or cnnOracle.State = ConnectionState.Broken Then
    '                                        cnnOracle.Open()
    '                                    End If
    '                                    cmdOracle.Connection = cnnOracle
    '                                    cmdOracle.CommandType = CommandType.Text
    '                                    cmdOracle.CommandText = StrQ
    '                                    Dim oda As New OracleClient.OracleDataAdapter(cmdOracle)
    '                                    oda.Fill(dsData)
    '                                End Using

    '                            Catch ex As Exception

    '                            End Try
    '                        End Using
    '                        If dsData.Tables.Count > 0 AndAlso dsData.Tables(0).Rows.Count > 0 Then
    '                            Dim strBuilder, strPrefix As String
    '                            strPrefix = "UBLK_" : strBuilder = ""
    '                            Dim oStrMsgId As String = ""
    '                            Dim strFileName As String = ""
    '                            Dim blnerror As Boolean = False
    '                            Dim oResponseData As String = ""
    '                            Dim strErrorDesc As String = ""

    '                            ''''''''''''''''''''''''''''' GENERATE UNIQUE MESSAGE ID FOR EACH REQUEST ---- START
    '                            Dim iCount As Integer = 1
    '                            Dim dsList As New DataSet
    '                            While iCount > 0
    '                                StrQ = "SELECT CONVERT(NVarChar(5), right(replace(convert(varchar, getdate(),114),':',''),5)) + CONVERT(NVarChar(5), right(newid(),5)) "
    '                                dsList = objDataOper.ExecQuery(StrQ, "List")
    '                                If objDataOper.ErrorMessage <> "" Then
    '                                    Throw New Exception(objDataOper.ErrorNumber & " : " & objDataOper.ErrorMessage)
    '                                End If
    '                                oStrMsgId = CStr(dsList.Tables(0).Rows(0)(0))
    '                                StrQ = "SELECT 1 FROM hrflexcube_request_tran WHERE fmsgeid = '" & oStrMsgId & "' "
    '                                iCount = objDataOper.RecordCount(StrQ)
    '                                If objDataOper.ErrorMessage <> "" Then
    '                                    Throw New Exception(objDataOper.ErrorNumber & " : " & objDataOper.ErrorMessage)
    '                                End If
    '                                If iCount <= 0 Then Exit While
    '                            End While
    '                            ''''''''''''''''''''''''''''' GENERATE UNIQUE MESSAGE ID FOR EACH REQUEST ---- END

    '                            StrQ = "SELECT TOP 1 " & _
    '                                   "  '<?xml version=""1.0"" encoding=""UTF-8""?>' " & _
    '                                   ", '<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns=""http://fcubs.ofss.com/service/FCUBSACService"">' " & _
    '                                   ", '<soapenv:Header/>' " & _
    '                                   ", '<soapenv:Body>'" & _
    '                                   ", '<MODIFYUSERMAINT_FSFS_REQ xmlns=""http://fcubs.ofss.com/service/FCUBSSMService"">' " & _
    '                                   ", '<FCUBS_HEADER>' " & _
    '                                   ", '<SOURCE>ARUTI</SOURCE>' " & _
    '                                   ", '<UBSCOMP>FCUBS</UBSCOMP>' " & _
    '                                   ", '<MSGID>' + '" & oStrMsgId & "' + '</MSGID>' " & _
    '                                   ", '<CORRELID>' + '" & oStrMsgId & "' + '</CORRELID>' " & _
    '                                   ", '<USERID>ARUTI</USERID>' " & _
    '                                   ", '<BRANCH>'+'" & strcccno & "'+'</BRANCH>' " & _
    '                                   ", '<MODULEID>SM</MODULEID>' " & _
    '                                   ", '<SERVICE>FCUBSSMService</SERVICE>' " & _
    '                                   ", '<OPERATION>ModifyUserMaint</OPERATION>' " & _
    '                                   ", '<SOURCE_OPERATION>ModifyUserMaint</SOURCE_OPERATION>' " & _
    '                                   ", '<SOURCE_USERID/>' " & _
    '                                   ", '<DESTINATION/>' " & _
    '                                   ", '<MULTITRIPID/>' " & _
    '                                   ", '<FUNCTIONID/>' " & _
    '                                   ", '<ACTION></ACTION>' " & _
    '                                   ", '</FCUBS_HEADER>' " & _
    '                                   ", '<FCUBS_BODY>' " & _
    '                                   ", '<USR-Full>' " & _
    '                                   ", '<USRID>'+ '" & dsData.Tables(0).Rows(0)("USER_ID") & "' +'</USRID>' " & _
    '                                   ", '<USRNAME>'+ '" & dsData.Tables(0).Rows(0)("USER_NAME").Replace("'", "''") & "' +'</USRNAME>' " & _
    '                                   ", '<HOMEBRN>'+'" & strcccno & "'+'</HOMEBRN>' " & _
    '                                   ", '<USRSTAT>'+'" & strUserState & "'+'</USRSTAT>' " & _
    '                                   ", '<USRLANG>ENG</USRLANG>' " & _
    '                                   ", '<TIMELEVEL>9</TIMELEVEL>' " & _
    '                                   ", '<STRTDATE>'+ '" & Format(dtStartDate.Date, "dd-MMM-yyyy") & "' +'</STRTDATE>' " & _
    '                                   ", '</USR-Full>' " & _
    '                                   ", '</FCUBS_BODY>' " & _
    '                                   ", '</MODIFYUSERMAINT_FSFS_REQ>' " & _
    '                                   ", '</soapenv:Body>' " & _
    '                                   ", '</soapenv:Envelope>' "

    '                            dsList = objDataOper.ExecQuery(StrQ, "List")

    '                            If objDataOper.ErrorMessage <> "" Then
    '                                Throw New Exception(objDataOper.ErrorNumber & " : " & objDataOper.ErrorMessage)
    '                            End If

    '                            If dsList.Tables("List").Rows.Count > 0 Then
    '                                Dim sb As New System.Text.StringBuilder
    '                                For Each iRow As DataRow In dsList.Tables("List").Rows
    '                                    For Each iCol As DataColumn In dsList.Tables("List").Columns
    '                                        Dim strData As String = "" : strData = iRow(iCol).ToString()
    '                                        SetXMLFormat(strData)
    '                                        sb.Append(strData & vbCrLf)
    '                                    Next
    '                                Next
    '                                strBuilder = sb.ToString()
    '                                If strBuilder.Trim.Length > 0 Then
    '                                    oResponseData = PostData(strBuilder, strServiceURL, strErrorDesc)
    '                                    If strErrorDesc.Trim().Length > 0 Then
    '                                        blnerror = True
    '                                    End If
    '                                    strFileName = strPrefix & strFormNumber.ToString() & "_" & Date.Now.ToString("yyyymmdd") & ".xml"
    '                                    If objEmp.InsertFlexcubeRequest(intEmpId, enRequestType.DEACTIVATE, enFiletype.USER, strFileName, strBuilder, "", oStrMsgId, oResponseData, blnerror, strErrorDesc, enRequestForm.LEAVE, strFormNumber, objDataOperation) = False Then
    '                                        Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                                    End If
    '                                End If
    '                            End If
    '                        Else
    '                            If objEmp.InsertFlexcubeRequest(intEmpId, enRequestType.DEACTIVATE, enFiletype.USER, "", "", "", "", "", True, "Flexcube user not found", enRequestForm.LEAVE, strFormNumber, objDataOperation) = False Then
    '                                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            End If
    '                        End If
    '                    Else
    '                        If objEmp.InsertFlexcubeRequest(intEmpId, enRequestType.DEACTIVATE, enFiletype.USER, "", "", "", "", "", True, "Account Number Not Assigned", enRequestForm.LEAVE, strFormNumber, objDataOperation) = False Then
    '                            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                        End If
    '                    End If
    '                End If
    '            End If
    '        End If
    '        'End If
    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: BlockEmployeeInCBS; Module Name: " & mstrModuleName)
    '    Finally
    '        objConfig = Nothing
    '    End Try
    'End Function
    Public Function BlockEmployeeInCBS(ByVal intEmpId As Integer, _
                                       ByVal dtStartDate As Date, _
                                       ByVal dtEndDate As Date, _
                                       ByVal strFormNumber As String, _
                                       ByVal dtCurrentDate As Date, _
                                       ByVal intCompanyId As Integer, _
                                       ByVal strUserState As String, _
                                       ByVal objDataOper As clsDataOperation, _
                                       ByVal xUserUnkid As Integer, _
                                       ByVal iLoginEmployeeId As Integer) As Boolean 'S.SANDEEP |25-NOV-2019| -- START {xUserUnkid,iLoginEmployeeId} -- END
        Dim StrQ As String = String.Empty
        Dim dsData As New DataSet
        Dim objCParam As New clsConfigOptions
        Dim strServiceURL As String = ""
        Dim straccno As String = String.Empty
        Dim strcccno As String = String.Empty
        'S.SANDEEP |25-NOV-2019| -- START
        'ISSUE/ENHANCEMENT : FlexCube
        Dim strFailedEmailNotification As String = String.Empty
        Dim strOracleExecption As String = String.Empty
        'S.SANDEEP |25-NOV-2019| -- END
        Try
            Dim objEmp As New clsEmployee_Master
            objDataOper.ClearParameters()
            Dim mDicKeyValues As New Dictionary(Of String, String)
            Dim strParamKeys() As String = { _
                                            "IsHRFlexcubeIntegrated", _
                                            "_FlxSrv_" & CInt(enFlexcubeServiceInfo.FLX_BLOCK_USER), _
                                            "OracleHostName", _
                                            "OraclePortNo", _
                                            "OracleServiceName", _
                                            "OracleUserName", _
                                            "OracleUserPassword" _
                                            }
            mDicKeyValues = objCParam.GetKeyValue(intCompanyId, strParamKeys)

            'S.SANDEEP |25-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : FlexCube
            strFailedEmailNotification = objCParam.GetKeyValue(intCompanyId, "FailedRequestNotificationEmails")
            'S.SANDEEP |25-NOV-2019| -- END
            If mDicKeyValues.Keys.Count > 0 Then
                If CBool(mDicKeyValues("IsHRFlexcubeIntegrated")) Then
                    If dtStartDate.Date < dtCurrentDate.Date AndAlso dtEndDate.Date < dtCurrentDate.Date Then Return True 'Less than today date for both start and end date -No action 
                    'S.SANDEEP |04-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : {Extra Validation Kept For Future Date}
                    If dtStartDate.Date > dtCurrentDate.Date Then Return True 'SKIP Block Logic for Future Date
                    'S.SANDEEP |04-JUL-2019| -- END
                    strServiceURL = mDicKeyValues("_FlxSrv_" & CInt(enFlexcubeServiceInfo.FLX_BLOCK_USER))
                    If strServiceURL IsNot Nothing AndAlso strServiceURL.Trim.Length > 0 Then
                        StrQ = "SELECT 1 FROM hrflexcube_request_tran WHERE reqmst_number = '" & strFormNumber & "' AND iserror = 0 "
                        Dim intExists As Integer = objDataOper.RecordCount(StrQ)
                        If objDataOper.ErrorMessage <> "" Then
                            Throw New Exception(objDataOper.ErrorNumber & " : " & objDataOper.ErrorMessage)
                        End If
                        If intExists > 0 Then Return True

                        StrQ = "SELECT " & _
                               "    hremployee_master.employeeunkid " & _
                               "   ,EA.customcode " & _
                               "   ,EB.accn " & _
                               "   FROM hremployee_master " & _
                               "   LEFT JOIN " & _
                               "   ( " & _
                               "       SELECT " & _
                               "            A.employeeunkid " & _
                               "           ,A.customcode " & _
                               "       FROM " & _
                               "       ( " & _
                               "           SELECT " & _
                               "                employeeunkid " & _
                               "               ,customcode " & _
                               "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                               "           FROM hremployee_cctranhead_tran " & _
                               "               JOIN prcostcenter_master ON cctranheadvalueid = prcostcenter_master.costcenterunkid " & _
                               "           WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @end_date " & _
                               "           AND employeeunkid = @employeeunkid AND istransactionhead = 0 " & _
                               "       ) AS A WHERE A.xNo = 1 " & _
                               "   ) AS EA ON EA.employeeunkid = hremployee_master.employeeunkid " & _
                               "   LEFT JOIN " & _
                               "   ( " & _
                               "       SELECT " & _
                               "            B.employeeunkid " & _
                               "           ,'''' + B.accountno + '''' AS accn " & _
                               "       FROM " & _
                               "       ( " & _
                               "           SELECT " & _
                               "                premployee_bank_tran.employeeunkid " & _
                               "               ,premployee_bank_tran.accountno " & _
                               "               ,DENSE_RANK() OVER (PARTITION  BY premployee_bank_tran.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                               "           FROM premployee_bank_tran " & _
                               "               LEFT JOIN cfcommon_period_tran ON premployee_bank_tran.periodunkid = cfcommon_period_tran.periodunkid AND isactive = 1 " & _
                               "           WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date " & _
                               "       )AS B WHERE B.ROWNO = 1 " & _
                               "   ) AS EB ON EB.employeeunkid = hremployee_master.employeeunkid " & _
                               "WHERE hremployee_master.employeeunkid = @employeeunkid "

                        objDataOper.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
                        objDataOper.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtCurrentDate))

                        dsData = objDataOper.ExecQuery(StrQ, "List")

                        If objDataOper.ErrorMessage <> "" Then
                            Throw New Exception(objDataOper.ErrorNumber & " : " & objDataOper.ErrorMessage)
                        End If

                        If dsData.Tables(0).Rows.Count > 0 Then
                            straccno = String.Join(",", dsData.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of String)("accn")).ToArray())
                            strcccno = dsData.Tables(0).Rows(0)("customcode").ToString()
                        End If

                        If straccno.Trim.Length > 0 Then
                            dsData = New DataSet
                            If straccno.Trim.Length > 0 Then
                                dsData = New DataSet
                            Using cnnOracle As New OracleClient.OracleConnection
                                    cnnOracle.ConnectionString = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" & mDicKeyValues("OracleHostName").ToString() & ")(PORT=" & mDicKeyValues("OraclePortNo").ToString() & ")))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" & mDicKeyValues("OracleServiceName").ToString() & "))); User Id=" & mDicKeyValues("OracleUserName") & ";Password=" & clsSecurity.Decrypt(mDicKeyValues("OracleUserPassword").ToString(), "ezee") & "; "
                                    'S.SANDEEP |25-NOV-2019| -- START
                                    'ISSUE/ENHANCEMENT : FlexCube
                                    'cnnOracle.Open()
                                    Try
                                    cnnOracle.Open()
                                    Catch ex As Exception
                                        strOracleExecption = ex.Message
                                        Throw ex
                                    End Try
                                    'S.SANDEEP |25-NOV-2019| -- END
                                    StrQ = "SELECT " & _
                                       "     USER_ID " & _
                                       "    ,USER_NAME " & _
                                           "    ,BRANCH " & _
                                       "    ,time_level " & _
                                       "    ,start_date " & _
                                           "    ,USER_STATUS " & _
                                       "FROM fcubs.aruti_user " & _
                                       "WHERE CUST_AC_NO IN (" & straccno & ") " 'ALWAYS TAKE FIRST ROW
                                    Using cmdOracle As New OracleClient.OracleCommand
                                        If cnnOracle.State = ConnectionState.Closed Or cnnOracle.State = ConnectionState.Broken Then
                                            cnnOracle.Open()
                                        End If
                                        cmdOracle.Connection = cnnOracle
                                        cmdOracle.CommandType = CommandType.Text
                                        cmdOracle.CommandText = StrQ
                                        Dim oda As New OracleClient.OracleDataAdapter(cmdOracle)
                                        oda.Fill(dsData)
                                    End Using
                            End Using

                            If dsData.Tables.Count > 0 AndAlso dsData.Tables(0).Rows.Count > 0 Then
                                    If dsData.Tables(0).Rows(0)("USER_STATUS").ToString().ToUpper() = "E" Then
                                Dim strBuilder, strPrefix As String
                                strPrefix = "UBLK_" : strBuilder = ""
                                Dim oStrMsgId As String = ""
                                Dim strFileName As String = ""
                                Dim blnerror As Boolean = False
                                Dim oResponseData As String = ""
                                Dim strErrorDesc As String = ""

                                ''''''''''''''''''''''''''''' GENERATE UNIQUE MESSAGE ID FOR EACH REQUEST ---- START
                                Dim iCount As Integer = 1
                                Dim dsList As New DataSet
                                While iCount > 0
                                    StrQ = "SELECT CONVERT(NVarChar(5), right(replace(convert(varchar, getdate(),114),':',''),5)) + CONVERT(NVarChar(5), right(newid(),5)) "
                                    dsList = objDataOper.ExecQuery(StrQ, "List")
                                    If objDataOper.ErrorMessage <> "" Then
                                        Throw New Exception(objDataOper.ErrorNumber & " : " & objDataOper.ErrorMessage)
                                    End If
                                    oStrMsgId = CStr(dsList.Tables(0).Rows(0)(0))
                                    StrQ = "SELECT 1 FROM hrflexcube_request_tran WHERE fmsgeid = '" & oStrMsgId & "' "
                                    iCount = objDataOper.RecordCount(StrQ)
                                    If objDataOper.ErrorMessage <> "" Then
                                        Throw New Exception(objDataOper.ErrorNumber & " : " & objDataOper.ErrorMessage)
                                    End If
                                    If iCount <= 0 Then Exit While
                                End While
                                ''''''''''''''''''''''''''''' GENERATE UNIQUE MESSAGE ID FOR EACH REQUEST ---- END

                                StrQ = "SELECT TOP 1 " & _
                                       "  '<?xml version=""1.0"" encoding=""UTF-8""?>' " & _
                                       ", '<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns=""http://fcubs.ofss.com/service/FCUBSACService"">' " & _
                                       ", '<soapenv:Header/>' " & _
                                       ", '<soapenv:Body>'" & _
                                       ", '<MODIFYUSERMAINT_FSFS_REQ xmlns=""http://fcubs.ofss.com/service/FCUBSSMService"">' " & _
                                       ", '<FCUBS_HEADER>' " & _
                                       ", '<SOURCE>ARUTI</SOURCE>' " & _
                                       ", '<UBSCOMP>FCUBS</UBSCOMP>' " & _
                                       ", '<MSGID>' + '" & oStrMsgId & "' + '</MSGID>' " & _
                                       ", '<CORRELID>' + '" & oStrMsgId & "' + '</CORRELID>' " & _
                                       ", '<USERID>ARUTI</USERID>' " & _
                                           ", '<BRANCH>101</BRANCH>' " & _
                                       ", '<MODULEID>SM</MODULEID>' " & _
                                       ", '<SERVICE>FCUBSSMService</SERVICE>' " & _
                                       ", '<OPERATION>ModifyUserMaint</OPERATION>' " & _
                                       ", '<SOURCE_OPERATION>ModifyUserMaint</SOURCE_OPERATION>' " & _
                                       ", '<SOURCE_USERID/>' " & _
                                       ", '<DESTINATION/>' " & _
                                       ", '<MULTITRIPID/>' " & _
                                       ", '<FUNCTIONID/>' " & _
                                       ", '<ACTION></ACTION>' " & _
                                       ", '</FCUBS_HEADER>' " & _
                                       ", '<FCUBS_BODY>' " & _
                                       ", '<USR-Full>' " & _
                                       ", '<USRID>'+ '" & dsData.Tables(0).Rows(0)("USER_ID") & "' +'</USRID>' " & _
                                       ", '<USRNAME>'+ '" & dsData.Tables(0).Rows(0)("USER_NAME").Replace("'", "''") & "' +'</USRNAME>' " & _
                                           ", '<HOMEBRN>'+'" & dsData.Tables(0).Rows(0)("BRANCH") & "'+'</HOMEBRN>' " & _
                                       ", '<USRSTAT>'+'" & strUserState & "'+'</USRSTAT>' " & _
                                       ", '<USRLANG>ENG</USRLANG>' " & _
                                           ", '<TIMELEVEL>'+'" & dsData.Tables(0).Rows(0)("time_level") & "'+'</TIMELEVEL>' " & _
                                           ", '<STRTDATE>'+ '" & Format(CDate(dsData.Tables(0).Rows(0)("start_date")).Date, "yyyy-MM-dd") & "' +'</STRTDATE>' " & _
                                       ", '</USR-Full>' " & _
                                       ", '</FCUBS_BODY>' " & _
                                       ", '</MODIFYUSERMAINT_FSFS_REQ>' " & _
                                       ", '</soapenv:Body>' " & _
                                       ", '</soapenv:Envelope>' "

                                    'S.SANDEEP |29-APR-2019| -- START
                                    'CHANGES MADE AS PER ZEESHAN'S COMMENTS FOR 2 TAGS
                                    'TIMELEVEL -->  TO PICK FROM FLX USER TABLE VIEW COL.NAME [time_level]
                                    'STRTDATE  -->  TO CHANGE THE FORMAT OLD [DD-MMM-YYYY] TO NEW [YYYY-MM-DD]
                                    'S.SANDEEP |29-APR-2019| -- END

                                    'S.SANDEEP [18-APR-2019] -- START
                                    '' AS PER SKYPE CHAT WITH ZEESHAN HE SUGGESTED THAT, PASS 101 IN BRANCH & TAKE 'BRANCH' COLUMN VALUE FOR HOMEBRN FROM ORACLE DATABASE
                                    '", '<BRANCH>'+'" & strcccno & "'+'</BRANCH>' " & _
                                    '", '<HOMEBRN>'+'" & strcccno & "'+'</HOMEBRN>' " & _
                                    'S.SANDEEP [18-APR-2019] -- END

                                dsList = objDataOper.ExecQuery(StrQ, "List")

                                If objDataOper.ErrorMessage <> "" Then
                                    Throw New Exception(objDataOper.ErrorNumber & " : " & objDataOper.ErrorMessage)
                                End If

                                If dsList.Tables("List").Rows.Count > 0 Then
                                    Dim sb As New System.Text.StringBuilder
                                    For Each iRow As DataRow In dsList.Tables("List").Rows
                                        For Each iCol As DataColumn In dsList.Tables("List").Columns
                                            Dim strData As String = "" : strData = iRow(iCol).ToString()
                                            SetXMLFormat(strData)
                                            sb.Append(strData & vbCrLf)
                                        Next
                                    Next
                                    strBuilder = sb.ToString()
                                    If strBuilder.Trim.Length > 0 Then
                                        oResponseData = PostData(strBuilder, strServiceURL, strErrorDesc)

                                            'S.SANDEEP |24-APR-2019| -- START
                                            If oResponseData.Trim.Length > 0 Then
                                                Dim dsErr As New DataSet
                                                dsErr.ReadXml(New System.IO.StringReader(oResponseData))
                                                'S.SANDEEP |08-JUN-2019| -- START
                                                'ISSUE/ENHANCEMENT : Object Reference Error
                                                'If dsErr.Tables("FCUBS_HEADER").Rows.Count > 0 Then
                                                '    If dsErr.Tables("FCUBS_HEADER").Rows(0).Item("MSGSTAT").ToString.ToUpper <> "SUCCESS" Then
                                                '        If dsErr.Tables.Contains("ERROR") = True Then
                                                '            If dsErr.Tables("ERROR").Rows.Count > 0 Then
                                                '                strErrorDesc = String.Join(",", dsErr.Tables("ERROR").AsEnumerable().Select(Function(x) x.Field(Of String)("ECODE").ToString() & " -> " & x.Field(Of String)("EDESC")).ToArray())
                                                '            End If
                                                '        End If
                                                '    End If
                                                'End If
                                                If dsErr.Tables.Contains("FCUBS_HEADER") = True Then
                                                If dsErr.Tables("FCUBS_HEADER").Rows.Count > 0 Then
                                                    If dsErr.Tables("FCUBS_HEADER").Rows(0).Item("MSGSTAT").ToString.ToUpper <> "SUCCESS" Then
                                                        If dsErr.Tables.Contains("ERROR") = True Then
                                                            If dsErr.Tables("ERROR").Rows.Count > 0 Then
                                                                strErrorDesc = String.Join(",", dsErr.Tables("ERROR").AsEnumerable().Select(Function(x) x.Field(Of String)("ECODE").ToString() & " -> " & x.Field(Of String)("EDESC")).ToArray())
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                                        'S.SANDEEP |25-NOV-2019| -- START
                                                        'ISSUE/ENHANCEMENT : FlexCube
                                                    Else
                                                        strErrorDesc = "Invalid Response Data, [MSGSTAT] Not Found in the given response."
                                                        'S.SANDEEP |25-NOV-2019| -- END
                                            End If
                                                'S.SANDEEP |08-JUN-2019| -- END
                                            End If
                                            'S.SANDEEP |24-APR-2019| -- END

                                        If strErrorDesc.Trim().Length > 0 Then
                                            blnerror = True
                                        End If
                                        strFileName = strPrefix & strFormNumber.ToString() & "_" & Date.Now.ToString("yyyymmdd") & ".xml"

                                            'S.SANDEEP |08-JUN-2019| -- START
                                            'ISSUE/ENHANCEMENT : Object Reference Error
                                            'If objEmp.InsertFlexcubeRequest(intEmpId, enRequestType.DEACTIVATE, enFiletype.USER, strFileName, strBuilder, "", oStrMsgId, oResponseData, blnerror, strErrorDesc, enRequestForm.LEAVE, strFormNumber, objDataOperation) = False Then
                                            '    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                            'End If

                                                'S.SANDEEP |25-NOV-2019| -- START
                                                'ISSUE/ENHANCEMENT : FlexCube
                                                'If objEmp.InsertFlexcubeRequest(intEmpId, enRequestType.DEACTIVATE, enFiletype.USER, strFileName, strBuilder, "", oStrMsgId, oResponseData, blnerror, strErrorDesc, enRequestForm.LEAVE, strFormNumber, objDataOper) = False Then

                                                If strErrorDesc.Trim().Length > 0 Then
                                                    If strFailedEmailNotification.Trim.Length > 0 Then
                                                        Dim mDicError As New Dictionary(Of String, String)
                                                        mDicError.Add("USERID", dsData.Tables(0).Rows(0)("USER_ID").ToString)
                                                        mDicError.Add("MSGID/CORRELID", oStrMsgId)
                                                        mDicError.Add("REQUESTDATE", Now)
                                                        mDicError.Add("ERROR", strErrorDesc)
                                                        Call SendCBS_Notification(strFailedEmailNotification, mDicError, "", intCompanyId, "Block User(s) Failed Request [" + Now + "]", xUserUnkid, iLoginEmployeeId)
                                                    End If
                                                End If
                                                If objEmp.InsertFlexcubeRequest(intEmpId, enRequestType.DEACTIVATE, enFiletype.USER, strFileName, strBuilder, "", oStrMsgId, oResponseData, blnerror, strErrorDesc, enRequestForm.LEAVE, strFormNumber, dsData.Tables(0).Rows(0)("USER_ID").ToString, IIf(mstrWebClientIP.Trim.Length <= 0, getIP(), mstrWebClientIP), objDataOper) = False Then
                                                    'S.SANDEEP |25-NOV-2019| -- END
                                                Throw New Exception(objDataOper.ErrorNumber & ": " & objDataOper.ErrorMessage)
                                        End If
                                            'S.SANDEEP |08-JUN-2019| -- END
                                            
                                    End If
                                End If
                            Else
                                        If dsData.Tables(0).Rows(0)("USER_STATUS").ToString() <> "E" Then
                                            If strFailedEmailNotification.Trim.Length > 0 Then
                                                Dim mDicError As New Dictionary(Of String, String)
                                                mDicError.Add("USERID", dsData.Tables(0).Rows(0)("USER_ID").ToString)
                                                mDicError.Add("REQUESTDATE", Now)
                                                mDicError.Add("ERROR", "User Status : " & dsData.Tables(0).Rows(0)("USER_STATUS").ToString())
                                                Call SendCBS_Notification(strFailedEmailNotification, mDicError, "", intCompanyId, "Block User(s) Failed Request [" + Now + "]", xUserUnkid, iLoginEmployeeId)
                                            End If
                                        End If
                                        If objEmp.InsertFlexcubeRequest(intEmpId, enRequestType.DEACTIVATE, enFiletype.USER, "", "", "", "", "", True, "User Status : " + dsData.Tables(0).Rows(0)("USER_STATUS").ToString(), enRequestForm.LEAVE, strFormNumber, dsData.Tables(0).Rows(0)("USER_ID").ToString, IIf(mstrWebClientIP.Trim.Length <= 0, getIP(), mstrWebClientIP), objDataOper) = False Then
                                            Throw New Exception(objDataOper.ErrorNumber & ": " & objDataOper.ErrorMessage)
                                        End If
                                    End If
                                Else
                                    'S.SANDEEP |08-JUN-2019| -- START
                                    'ISSUE/ENHANCEMENT : Object Reference Error
                                    'If objEmp.InsertFlexcubeRequest(intEmpId, enRequestType.DEACTIVATE, enFiletype.USER, "", "", "", "", "", True, "Flexcube user not found", enRequestForm.LEAVE, strFormNumber, objDataOperation) = False Then
                                    '    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    'End If

                                    'S.SANDEEP |25-NOV-2019| -- START
                                    'ISSUE/ENHANCEMENT : FlexCube
                                    'If objEmp.InsertFlexcubeRequest(intEmpId, enRequestType.DEACTIVATE, enFiletype.USER, "", "", "", "", "", True, "Flexcube user not found", enRequestForm.LEAVE, strFormNumber, objDataOper) = False Then
                                    If objEmp.InsertFlexcubeRequest(intEmpId, enRequestType.DEACTIVATE, enFiletype.USER, "", "", "", "", "", True, "Flexcube user not found", enRequestForm.LEAVE, strFormNumber, "", IIf(mstrWebClientIP.Trim.Length <= 0, getIP(), mstrWebClientIP), objDataOper) = False Then
                                        'S.SANDEEP |25-NOV-2019| -- END
                                        Throw New Exception(objDataOper.ErrorNumber & ": " & objDataOper.ErrorMessage)
                                End If
                                    'S.SANDEEP |08-JUN-2019| -- END
                            End If
                        Else
                                'S.SANDEEP |08-JUN-2019| -- START
                                'ISSUE/ENHANCEMENT : Object Reference Error
                                'If objEmp.InsertFlexcubeRequest(intEmpId, enRequestType.DEACTIVATE, enFiletype.USER, "", "", "", "", "", True, "Account Number Not Assigned", enRequestForm.LEAVE, strFormNumber, objDataOperation) = False Then
                                '    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                'End If

                                'S.SANDEEP |25-NOV-2019| -- START
                                'ISSUE/ENHANCEMENT : FlexCube
                                'If objEmp.InsertFlexcubeRequest(intEmpId, enRequestType.DEACTIVATE, enFiletype.USER, "", "", "", "", "", True, "Account Number Not Assigned", enRequestForm.LEAVE, strFormNumber, objDataOper) = False Then
                                If objEmp.InsertFlexcubeRequest(intEmpId, enRequestType.DEACTIVATE, enFiletype.USER, "", "", "", "", "", True, "Account Number Not Assigned", enRequestForm.LEAVE, strFormNumber, "", IIf(mstrWebClientIP.Trim.Length <= 0, getIP(), mstrWebClientIP), objDataOper) = False Then
                                    'S.SANDEEP |25-NOV-2019| -- END

                                    Throw New Exception(objDataOper.ErrorNumber & ": " & objDataOper.ErrorMessage)
                            End If
                                'S.SANDEEP |08-JUN-2019| -- END
                        End If
                    End If
                End If
            End If
            End If

            Return True
        Catch ex As Exception
            'S.SANDEEP |25-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : FlexCube
            If strOracleExecption.Trim.Length > 0 Then
                If strFailedEmailNotification.Trim.Length > 0 Then
                    Call SendCBS_Notification(strFailedEmailNotification, Nothing, ex.Message, intCompanyId, "Block User Oracle Connection Issue [" + Now + "]", xUserUnkid, iLoginEmployeeId)
                End If
            End If
            'S.SANDEEP |25-NOV-2019| -- END
            Throw New Exception(ex.Message & "; Procedure Name: BlockEmployeeInCBS; Module Name: " & mstrModuleName)
        Finally
            objCParam = Nothing
        End Try
    End Function
    'S.SANDEEP |11-APR-2019| -- END

    'S.SANDEEP |25-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : FlexCube
    Private Sub SendCBS_Notification(ByVal strFailedEmailNotification As String, _
                                     ByVal mDicError As Dictionary(Of String, String), _
                                     ByVal strExecption As String, _
                                     ByVal intCompanyId As Integer, _
                                     ByVal strSubject As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal iLoginEmployeeId As Integer)
        Try
            Dim arrEmail() As String = Nothing
            If strFailedEmailNotification.Trim.Contains(",") Then
                arrEmail = strFailedEmailNotification.Split(",")
            ElseIf strFailedEmailNotification.Trim.Contains(";") Then
                arrEmail = strFailedEmailNotification.Split(";")
            End If
            If arrEmail.Length > 0 Then
                Dim objComp As New clsCompany_Master
                objComp._Companyunkid = intCompanyId
                Dim strSenderAddress, strSendName As String
                strSenderAddress = "" : strSendName = ""
                strSenderAddress = objComp._Senderaddress
                strSendName = objComp._Sendername

                Dim strMailBody As String = String.Empty
                If strExecption.Trim.Length > 0 Then
                    strMailBody = "Aruti Leave Application Error in Blocking In Flexcube :  <b>" + strExecption.ToString() + "</b>"
                ElseIf mDicError IsNot Nothing AndAlso mDicError.Keys.Count > 0 Then
                    strMailBody = "<table style='width: 100%;' border='1'>" & vbCrLf
                    strMailBody &= "<tr style='width: 100%;'>" & vbCrLf
                    For Each iKey As String In mDicError.Keys
                        strMailBody &= "<td bgcolor = '#5D7B9D' border='1'><b><font color='#fff'>" & iKey & "</font></b></td>" & vbCrLf
                    Next
                    strMailBody &= "</tr>" & vbCrLf
                    strMailBody &= "<tr style='width: 100%;'>" & vbCrLf
                    For Each iKey As String In mDicError.Keys
                        strMailBody &= "<td border='1'>" & mDicError(iKey) & "</td>" & vbCrLf
                    Next
                    strMailBody &= "</tr>" & vbCrLf
                    strMailBody &= "</table>"
                End If
                Dim objMail As New clsSendMail
                If mstrWebFrmName.Trim.Length > 0 Then
                    objMail._Form_Name = mstrWebFrmName
                    objMail._WebClientIP = mstrWebClientIP
                    objMail._WebHostName = mstrWebHostName
                End If
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = enLogin_Mode.DESKTOP
                objMail._UserUnkid = xUserUnkid
                objMail._Subject = strSubject
                objMail._Message = strMailBody
                objMail._SenderAddress = IIf(strSenderAddress.Trim.Length <= 0, strSendName, strSenderAddress)
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT
                For i As Integer = 0 To arrEmail.Length - 1
                    objMail._ToEmail = arrEmail(i)
                    objMail.SendMail(intCompanyId)
                Next
                objMail = Nothing
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendCBS_Notification; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |25-NOV-2019| -- END


    Private Sub SetXMLFormat(ByRef strData As String)
        Try
            strData = strData.Replace("&", "&amp;")
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetXMLFormat; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function PostData(ByVal strxmldata As String, ByVal strSrvURL As String, ByRef strError As String) As String
        Dim strResponseData As String = String.Empty
        Try
            Dim xmlDoc As XmlDocument = New XmlDocument()
            xmlDoc.LoadXml(strxmldata)
            Dim request As HttpWebRequest = CType(WebRequest.Create(strSrvURL), HttpWebRequest)
            Dim bytes As Byte()
            bytes = System.Text.Encoding.ASCII.GetBytes(xmlDoc.InnerXml)
            request.ContentType = "text/xml"
            request.ContentLength = bytes.Length
            request.Method = "POST"
            Dim requestStream As Stream = request.GetRequestStream()
            requestStream.Write(bytes, 0, bytes.Length)
            requestStream.Close()
            Using responseReader As New StreamReader(request.GetResponse().GetResponseStream())
                Dim result As String = responseReader.ReadToEnd()
                Dim ResultXML As XDocument = XDocument.Parse(result)
                strResponseData = ResultXML.ToString()
            End Using
        Catch ex As WebException
            If ex.InnerException IsNot Nothing Then
                strError = ex.InnerException.ToString() & Environment.NewLine & ex.Message
            Else
                strError = ex.Message
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: PostData; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strResponseData
    End Function

#End Region
    'S.SANDEEP |08-APR-2019| -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.

    'Gajanan [20-Dec-2019] -- Start   
    'Enhancement:Voltamp Transformers-Oman [0003275] : Unable to apply leave on cancelled days
    Public Function IsIssuedDays(ByVal intEmployeeunkid As Integer, _
                                 ByVal dtstartdate As DateTime, ByVal dtreturndate As DateTime, _
                                 Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            Dim objLeaveissue_master = New clsleaveissue_master
            Dim mstrFilter As String = ""
            mstrFilter = " convert(char(8),lvleaveIssue_tran.leavedate,112)  BETWEEN '" & eZeeDate.convertDate(dtstartdate) & "'  AND '" & eZeeDate.convertDate(dtreturndate) & "' "
            dsList = objLeaveissue_master.GetEmployeeIssueData(intEmployeeunkid, 0, xDataOpr, mstrFilter)

            Return (dsList.Tables(0).Rows.Count > 0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsIssuedDays; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [20-Dec-2019] -- Start   
            'Enhancement:Voltamp Transformers-Oman [0003275] : Unable to apply leave on cancelled days
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
            'Gajanan [20-Dec-2019] -- End

        End Try
    End Function
    'Gajanan [20-Dec-2019] -- End

    'Pinkal (18-Mar-2021) -- Start 
    'NMB Enhancmenet : AD Enhancement for NMB.
    Public Function IsCancelledDays(ByVal intEmployeeunkid As Integer, _
                                 ByVal dtstartdate As DateTime, ByVal dtreturndate As DateTime, _
                                 Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objCancel As New clsCancel_Leaveform
        Try
            Dim mstrFilter As String = ""
            mstrFilter = " lvcancelform.employeeunkid = " & intEmployeeunkid & " AND convert(char(8),lvcancelform.cancel_leavedate,112)  BETWEEN '" & eZeeDate.convertDate(dtstartdate) & _
                              "'  AND '" & eZeeDate.convertDate(dtreturndate) & "' "

            dsList = objCancel.GetList("List", -1, "", True, mstrFilter, xDataOpr)

            Return (dsList.Tables(0).Rows.Count > 0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsCancelledDays; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
            objCancel = Nothing
        End Try
    End Function
    'Pinkal (18-Mar-2021) -- End

#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Form No is already defined. Please define new Form No.")
            Language.setMessage(mstrModuleName, 2, "Select")
            Language.setMessage(mstrModuleName, 3, "This tenure is already defined to another form. Please define new tenure for this form.")
            Language.setMessage(mstrModuleName, 4, "Select Leave Form")
            Language.setMessage(mstrModuleName, 5, "Leave Application form deleted")
            Language.setMessage(mstrModuleName, 6, "This is to inform you that leave application no")
			Language.setMessage(mstrModuleName, 7, "with")
            Language.setMessage(mstrModuleName, 8, " who was seeking your approval has been deleted. Kindly take a note of it.")
            Language.setMessage(mstrModuleName, 9, "Notification for approving Leave Application form")
            Language.setMessage(mstrModuleName, 10, "This is a notification for approving leave application no")
            Language.setMessage(mstrModuleName, 12, " having Start date :")
			Language.setMessage(mstrModuleName, 13, "and End Date :")
            Language.setMessage(mstrModuleName, 14, "Dear")
            Language.setMessage(mstrModuleName, 15, "Leave Status Notification")
            Language.setMessage(mstrModuleName, 16, " This is to inform you that your application for")
			Language.setMessage(mstrModuleName, 17, "has been")
            Language.setMessage(mstrModuleName, 18, " From")
			Language.setMessage(mstrModuleName, 19, "To")
            Language.setMessage(mstrModuleName, 20, " This is to inform you that your leave has been Cancelled for these date(s):")
            Language.setMessage(mstrModuleName, 21, "but still has not been issued")
            Language.setMessage(mstrModuleName, 22, "Final Reminder to issue Leave Application form")
            Language.setMessage(mstrModuleName, 23, "This is the final reminder for issuing leave application no")
            Language.setMessage(mstrModuleName, 24, " The leave has been finally approved by")
            Language.setMessage(mstrModuleName, 25, "so please login to Aruti HRMS System to Issue this leave.")
            Language.setMessage(mstrModuleName, 26, "This tenure is already approved by approver.Please define new tenure for this form.")
            Language.setMessage(mstrModuleName, 27, "Please click on the following link to approve leave.")
            Language.setMessage(mstrModuleName, 28, "Sorry, you cannot apply for [")
            Language.setMessage(mstrModuleName, 29, " ] day(s) for this leave type. Reason you can only apply for [")
            Language.setMessage(mstrModuleName, 30, " ] consecutive day(s) for this leave type.")
            Language.setMessage(mstrModuleName, 31, "Sorry, you cannot apply from [")
            Language.setMessage(mstrModuleName, 32, " ] date for this leave type. Reason, this will violate the consecutive days rule set for this leave type.")
            Language.setMessage(mstrModuleName, 33, "Sorry, you cannot apply for this leave type as you can only apply for [")
            Language.setMessage(mstrModuleName, 34, " ] time(s)")
            Language.setMessage(mstrModuleName, 35, " in your life time.")
	    Language.setMessage(mstrModuleName, 36, " during your leave tenure.")
            Language.setMessage(mstrModuleName, 37, " during your employee leave cycle.")
            Language.setMessage(mstrModuleName, 38, "Sorry, you cannot apply for leave. Reason : There is already another leave form whose end date is not set.")
            Language.setMessage(mstrModuleName, 39, "Sorry, you cannot apply for leave. Reason : There is already another leave form which is not yet issued.")
            Language.setMessage(mstrModuleName, 40, "Sorry, you cannot apply for leave. Reason : You cannot apply for same leave for consecutive days.")
            Language.setMessage(mstrModuleName, 41, "Sorry, you cannot apply for leave." & vbCrLf & _
                                                               "Reason : You have some pending sick leave form in which return date is not set.")
            Language.setMessage(mstrModuleName, 42, "Sorry, you cannot apply for leave." & vbCrLf & _
                                                                "Reason : Leave end date should be less than start date of sick leave form.")
			Language.setMessage(mstrModuleName, 44, "for")
			Language.setMessage(mstrModuleName, 45, "of")
            Language.setMessage(mstrModuleName, 46, "Remarks/Comments:")
            Language.setMessage(mstrModuleName, 47, "Approval for your request for leave expense(s) is in progress, you will be notified when it is ready.")
            Language.setMessage(mstrModuleName, 48, "and")
            Language.setMessage(mstrModuleName, 49, "your request for leave expense(s) associated with this leave application is approved.")
            Language.setMessage(mstrModuleName, 50, "your request for leave expense(s) associated with this leave application is rejected.")
            Language.setMessage(mstrModuleName, 51, "Issued Leave Application form notification")
            Language.setMessage(mstrModuleName, 52, " This is to notify that leave application no")
			Language.setMessage(mstrModuleName, 53, "has been issued.")
			Language.setMessage(mstrModuleName, 54, "with below")
			Language.setMessage(mstrModuleName, 55, "Kindly take note of it.")
			Language.setMessage(mstrModuleName, 56, "Please find the leave form attached for your reference.")
			Language.setMessage(mstrModuleName, 57, "with application no")
			Language.setMessage(mstrModuleName, 58, "who was seeking your approval has been deleted by")
			Language.setMessage(mstrModuleName, 59, "Kindly take a note of it.")
			Language.setMessage(mstrModuleName, 60, "This is to inform you that you will be relieving")
			Language.setMessage(mstrModuleName, 61, "who will be on leave from")
			Language.setMessage(mstrModuleName, 62, "Regards")
			Language.setMessage(mstrModuleName, 63, "Relieving Notification for")
			Language.setMessage(mstrModuleName, 64, "This is to notify you that")
			Language.setMessage(mstrModuleName, 65, "will be ending on")
			Language.setMessage(mstrModuleName, 66, "hence He/She is schedule to report tomorrow.")
			Language.setMessage(mstrModuleName, 67, "This is to inform you that , the leave for")
			Language.setMessage(mstrModuleName, 68, "has been Cancelled for these date(s):")
            Language.setMessage("clsMasterData", 110, "Approved")
            Language.setMessage("clsMasterData", 111, "Pending")
            Language.setMessage("clsMasterData", 112, "Rejected")
            Language.setMessage("clsMasterData", 113, "Re-Scheduled")
            Language.setMessage("clsMasterData", 115, "Cancelled")
            Language.setMessage("clsMasterData", 276, "Issued")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class