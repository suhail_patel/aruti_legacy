﻿'************************************************************************************************************************************
'Class Name : clsleaveday_fraction.vb
'Purpose    :
'Date       :12/30/2011
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Globalization

''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsleaveday_fraction
    Private Const mstrModuleName = "clsleaveday_fraction"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintDayfractionunkid As Integer
    Private mintFormunkid As Integer
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer = -1
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdtFraction As DataTable

    'Pinkal (07-APR-2012) -- Start
    'Enhancement : TRA Changes
    Private mintVoidlogingemployeeunkid As Integer = -1
    'Pinkal (07-APR-2012) -- End


    'Pinkal (15-Jul-2013) -- Start
    'Enhancement : TRA Changes
    Private mintApproverunkid As Integer = -1
    Private mintPendingLeaveTranId As Integer = -1
    'Pinkal (15-Jul-2013) -- End


#End Region

#Region "Constructor"

    Public Sub New()
        mdtFraction = New DataTable("Fraction")
        mdtFraction.Columns.Add("dayfractionunkid", Type.GetType("System.Int32"))
        mdtFraction.Columns.Add("formunkid", Type.GetType("System.Int32"))
        mdtFraction.Columns.Add("leavedate", Type.GetType("System.DateTime"))
        mdtFraction.Columns.Add("dayfraction", Type.GetType("System.Decimal"))
        mdtFraction.Columns.Add("AUD", Type.GetType("System.String"))
        mdtFraction.Columns.Add("GUID", Type.GetType("System.String"))

        'Pinkal (15-Nov-2013) -- Start
        'Enhancement : Oman Changes
        mdtFraction.Columns.Add("approverunkid", Type.GetType("System.Int32"))
        'Pinkal (15-Nov-2013) -- End

    End Sub

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dayfractionunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Dayfractionunkid() As Integer
        Get
            Return mintDayfractionunkid
        End Get
        Set(ByVal value As Integer)
            mintDayfractionunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Formunkid() As Integer
        Get
            Return mintFormunkid
        End Get
        Set(ByVal value As Integer)
            mintFormunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdtFraction
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _dtfraction() As DataTable
        Get
            Return mdtFraction
        End Get
        Set(ByVal value As DataTable)
            mdtFraction = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property


    'Pinkal (07-APR-2012) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property

    'Pinkal (07-APR-2012) -- End


    'Pinkal (15-Jul-2013) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set approverunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Approverunkid() As Integer
        Get
            Return mintApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set PendingLeaveTranId
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _PendingLeaveTranId() As Integer
        Get
            Return mintPendingLeaveTranId
        End Get
        Set(ByVal value As Integer)
            mintPendingLeaveTranId = value
        End Set
    End Property

    'Pinkal (15-Jul-2013) -- End

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intFormunkid As Integer = -1, Optional ByVal blnOnlyActive As Boolean = True _
                                    , Optional ByVal intEmployeeId As Integer = -1, Optional ByVal intApproverId As Integer = -1, Optional ByVal strFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()
        Try

            strQ = "SELECT " & _
              "  lvleaveday_fraction.dayfractionunkid " & _
              ", lvleaveday_fraction.formunkid " & _
              ", lvleaveday_fraction.leavedate " & _
              ", lvleaveday_fraction.dayfraction " & _
              ", lvleaveday_fraction.userunkid " & _
              ", lvleaveday_fraction.loginemployeeunkid " & _
              ", lvleaveday_fraction.isvoid " & _
              ", lvleaveday_fraction.voiduserunkid " & _
              ", lvleaveday_fraction.voiddatetime " & _
              ", lvleaveday_fraction.voidreason " & _
              ", '' as  AUD " & _
              ", '' as  GUID " & _
           ", ISNULL(lvleaveday_fraction.voidloginemployeeunkid,0) voidloginemployeeunkid " & _
              ", ISNULL(lvleaveday_fraction.approverunkid,0) approverunkid "

            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            If intEmployeeId > 0 Then
                strQ &= ", ISNULL(lvleavetype_master.leavename,'') AS leavename "
            End If
            'Pinkal (13-Oct-2017) -- End

            strQ &= " FROM lvleaveday_fraction "

            If intEmployeeId > 0 Then
                strQ &= " JOIN lvleaveform ON lvleaveday_fraction.formunkid = lvleaveform.formunkid AND employeeunkid = @employeeid "

                'Pinkal (13-Oct-2017) -- Start
                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                strQ &= " JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid "
                'Pinkal (13-Oct-2017) -- End

                objDataOperation.AddParameter("@employeeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            End If

            strQ &= " WHERE 1 = 1 "

            If blnOnlyActive Then
                strQ &= " AND lvleaveday_fraction.isvoid = 0 "
            End If

            If intFormunkid > 0 Then
                strQ &= " AND lvleaveday_fraction.formunkid = @formunkid"
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
            End If

            If intApproverId > 0 Then
                strQ &= " AND lvleaveday_fraction.approverunkid = @approverunkid"
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverId)
            Else
                strQ &= " AND ISNULL(lvleaveday_fraction.approverunkid,-1) <= -1 "
            End If

            If strFilter.Trim.Length > 0 Then
                strQ &= strFilter
            End If

            strQ &= " Order by lvleaveday_fraction.leavedate "
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveday_fraction) </purpose>
    Public Function InsertUpdateDelete_Fraction(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim strErrorMessage As String = ""
        Dim exForce As Exception

        Try

            If mdtFraction Is Nothing Then Return True

            For i = 0 To mdtFraction.Rows.Count - 1
                With mdtFraction.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then

                        Select Case .Item("AUD")
                            Case "A"

                                'Pinkal (06-Nov-2015) -- Start
                                'Enhancement - WORKING ON FIXING BUG ON DUPLICATION OF LEAVE DAY FRACTION FROM WEB.
                                If IsDayFractionExist(objDataOperation, mintFormunkid, CDate(.Item("leavedate")), mintApproverunkid) Then
                                    Continue For
                                End If
                                objDataOperation.ClearParameters()
                                'Pinkal (06-Nov-2015) -- End


                                strQ = "INSERT INTO lvleaveday_fraction ( " & _
                                  "  formunkid " & _
                                  ", leavedate " & _
                                  ", dayfraction " & _
                                  ", userunkid " & _
                                  ", loginemployeeunkid " & _
                                  ", isvoid " & _
                                  ", voiduserunkid " & _
                                  ", voiddatetime " & _
                                  ", voidreason" & _
                                  ", approverunkid " & _
                                ") VALUES (" & _
                                  "  @formunkid " & _
                                  ", @leavedate " & _
                                  ", @dayfraction " & _
                                  ", @userunkid " & _
                                  ", @loginemployeeunkid " & _
                                  ", @isvoid " & _
                                  ", @voiduserunkid " & _
                                  ", @voiddatetime " & _
                                  ", @voidreason" & _
                                  ", @approverunkid " & _
                                "); SELECT @@identity"
                                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
                                objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("leavedate").ToString)
                                objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, .Item("dayfraction").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintDayfractionunkid = dsList.Tables(0).Rows(0).Item(0)
                                If mintPendingLeaveTranId <= 0 Then
                                If CInt(.Item("formunkid")) > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", CInt(.Item("formunkid")), "lvleaveday_fraction", "dayfractionunkid", mintDayfractionunkid, 2, 1, False, mintUserunkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", mintFormunkid, "lvleaveday_fraction", "dayfractionunkid", mintDayfractionunkid, 1, 1, False, mintUserunkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                ElseIf mintPendingLeaveTranId > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvpendingleave_tran", "pendingleavetranunkid", mintPendingLeaveTranId, "lvleaveday_fraction", "dayfractionunkid", mintDayfractionunkid, 2, 1, False, mintUserunkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                            Case "U"

                                strQ = "UPDATE lvleaveday_fraction SET " & _
                                            "  formunkid = @formunkid" & _
                                            ", leavedate = @leavedate" & _
                                            ", dayfraction = @dayfraction" & _
                                            ", userunkid = @userunkid" & _
                                            ", loginemployeeunkid = @loginemployeeunkid" & _
                                            ", isvoid = @isvoid" & _
                                            ", voiduserunkid = @voiduserunkid" & _
                                            ", voiddatetime = @voiddatetime" & _
                                            ", voidreason = @voidreason " & _
                                            ", approverunkid = @approverunkid " & _
                                          "WHERE dayfractionunkid = @dayfractionunkid "

                                If mintApproverunkid > 0 Then
                                    strQ &= " AND approverunkid = @approverunkid "
                                End If

                                objDataOperation.AddParameter("@dayfractionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dayfractionunkid").ToString)
                                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
                                objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("leavedate").ToString)
                                objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, .Item("dayfraction").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If mintPendingLeaveTranId <= 0 Then
                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", CInt(.Item("formunkid")), "lvleaveday_fraction", "dayfractionunkid", CInt(.Item("dayfractionunkid")), 2, 2, False, mintUserunkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                ElseIf mintPendingLeaveTranId > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvpendingleave_tran", "pendingleavetranunkid", mintPendingLeaveTranId, "lvleaveday_fraction", "dayfractionunkid", CInt(.Item("dayfractionunkid")), 2, 2, False, mintUserunkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                            Case "D"

                                strQ = "UPDATE lvleaveday_fraction SET " & _
                                          " isvoid = @isvoid" & _
                                          ", voiduserunkid = @voiduserunkid" & _
                                          ", voiddatetime = @voiddatetime" & _
                                          ", voidreason = @voidreason " & _
                                      ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                                        "WHERE dayfractionunkid = @dayfractionunkid "

                                If mintApproverunkid > 0 Then
                                    strQ &= " AND approverunkid = @approverunkid "
                                    objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
                                End If

                                objDataOperation.AddParameter("@dayfractionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("dayfractionunkid")).ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True.ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                                If mintVoidlogingemployeeunkid > 0 Then
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                Else
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                End If
                                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidlogingemployeeunkid.ToString)
                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                If mintPendingLeaveTranId <= 0 Then
                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", CInt(.Item("formunkid")), "lvleaveday_fraction", "dayfractionunkid", CInt(.Item("dayfractionunkid")), 2, 3, False, mintUserunkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                ElseIf mintPendingLeaveTranId > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvpendingleave_tran", "pendingleavetranunkid", mintPendingLeaveTranId, "lvleaveday_fraction", "dayfractionunkid", CInt(.Item("dayfractionunkid")), 2, 3, False, mintUserunkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                        End Select

                    End If

                End With

            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    '''' <summary>
    '''' Modify By: Pinkal Jariwala
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (lvleaveday_fraction) </purpose>
    'Public Function Update(ByVal objDataOperation As clsDataOperation) As Boolean
    '    'If isExist(mstrName, mintDayfractionunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Try

    '        strQ = "UPDATE lvleaveday_fraction SET " & _
    '       "  formunkid = @formunkid" & _
    '       ", leavedate = @leavedate" & _
    '       ", dayfraction = @dayfraction" & _
    '       ", userunkid = @userunkid" & _
    '       ", loginemployeeunkid = @loginemployeeunkid" & _
    '       ", isvoid = @isvoid" & _
    '       ", voiduserunkid = @voiduserunkid" & _
    '       ", voiddatetime = @voiddatetime" & _
    '       ", voidreason = @voidreason " & _
    '     "WHERE dayfractionunkid = @dayfractionunkid "


    '        If mdtFraction Is Nothing Or mdtFraction.Rows.Count = 0 Then Return True

    '        For Each dr As DataRow In mdtFraction.Rows

    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@dayfractionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("dayfractionunkid").ToString)
    '            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
    '            objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dr("leavedate").ToString)
    '            objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, dr("dayfraction").ToString)
    '            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
    '            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
    '            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

    '            Call objDataOperation.ExecNonQuery(strQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "lvleaveform", mintFormunkid, "formunkid", 2) Then
    '                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", mintFormunkid, "lvleaveday_fraction", "dayfractionunkid", CInt(dr("dayfractionunkid").ToString), 2, 2) = False Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If
    '            End If

    '        Next

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvleaveday_fraction) </purpose>
    Public Function Delete(ByVal intFormunkid As Integer, ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "Select * from lvleaveday_fraction where formunkid = @formunkid and isvoid = 0"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                strQ = "UPDATE lvleaveday_fraction SET " & _
                         " isvoid = @isvoid" & _
                         ", voiduserunkid = @voiduserunkid" & _
                         ", voiddatetime = @voiddatetime" & _
                         ", voidreason = @voidreason " & _
                        ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                        " WHERE dayfractionunkid = @dayfractionunkid "

                For Each dr As DataRow In dsList.Tables(0).Rows
                    objDataOperation.ClearParameters()

                    objDataOperation.AddParameter("@dayfractionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("dayfractionunkid")).ToString)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                    If mintVoidlogingemployeeunkid > 0 Then
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                    Else
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    End If

                    objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidlogingemployeeunkid.ToString)

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", CInt(dr("formunkid")), "lvleaveday_fraction", "dayfractionunkid", CInt(dr("dayfractionunkid")), 3, 3, False, mintUserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@dayfractionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
              "  dayfractionunkid " & _
              ", formunkid " & _
              ", leavedate " & _
              ", dayfraction " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", voidloginemployeeunkid " & _
                      ", approverunkid " & _
             "FROM lvleaveday_fraction " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND dayfractionunkid <> @dayfractionunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@dayfractionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeLeaveDay_Fraction(ByVal strDate As String, ByVal intFormunkid As Integer, ByVal intEmployeeunkid As Integer _
                                                                         , Optional ByVal blnLeaveApproverForLeaveType As String = "", Optional ByVal intApproverID As Integer = -1, Optional ByVal objDoOperation As clsDataOperation = Nothing) As Decimal
        'Pinkal (02-Dec-2015) -- Start      'Enhancement - Solving Leave bug in Self Service.[Optional ByVal objDoOperation As clsDataOperation = Nothing]
        Dim mdclFraction As Decimal = 0
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Try

            'Pinkal (02-Dec-2015) -- Start
            'Enhancement - Solving Leave bug in Self Service.

            If objDoOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDoOperation
            End If

            'Pinkal (02-Dec-2015) -- End

            objDataOperation.ClearParameters()

            strQ = " SELECT ISNULL(lvleaveday_fraction.dayfraction,0.00) dayfraction FROM lvleaveday_fraction " & _
                      " JOIN lvpendingleave_tran ON lvleaveday_fraction.formunkid = lvpendingleave_tran.formunkid " & _
                      " AND (lvpendingleave_tran.statusunkid = 1 or lvpendingleave_tran.statusunkid = 7)" & _
                " AND approvertranunkid IN  " & _
                      " ( " & _
                      " SELECT TOP 1 lvleaveapprover_master.approverunkid FROM lvleaveapprover_master " & _
                      " JOIN lvleaveapprover_tran ON lvleaveapprover_master.approverunkid = lvleaveapprover_tran.approverunkid AND lvleaveapprover_tran.employeeunkid = @employeeunkid AND lvleaveapprover_master.approverunkid = lvpendingleave_tran.approvertranunkid" & _
                            " JOIN lvapproverlevel_master ON lvapproverlevel_master.levelunkid =lvleaveapprover_master.levelunkid "

            If blnLeaveApproverForLeaveType.Trim.Length <= 0 Then
                blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType.ToString()
            End If

            If CBool(blnLeaveApproverForLeaveType) = True Then
                strQ &= " JOIN lvapprover_leavetype_mapping ON lvleaveapprover_master.approverunkid = lvapprover_leavetype_mapping.approverunkid "
            End If

            strQ &= " ORDER BY lvapproverlevel_master.priority DESC )"

            If intApproverID > 0 Then
                strQ &= "  AND lvleaveday_fraction.approverunkid = lvpendingleave_tran.approverunkid "
            End If

            strQ &= " WHERE CONVERT(CHAR(8),lvleaveday_fraction.leavedate,112)= @leavedate AND lvleaveday_fraction.isvoid = 0 "


            If intFormunkid > 0 Then
                strQ &= " AND lvleaveday_fraction.formunkid  = @formunkid "
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
            End If


            If intApproverID > 0 Then
                strQ &= " AND lvleaveday_fraction.approverunkid = @approverunkid "
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverID)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@leavedate", SqlDbType.NChar, eZeeDataType.NAME_SIZE, strDate)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdclFraction = CDec(dsList.Tables(0).Rows(0)("dayfraction"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeLeaveDay_Fraction; Module Name: " & mstrModuleName)
        End Try
        Return mdclFraction
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetPendingLeaveDay_Fraction(ByVal strStartDate As String, ByVal strEnddate As String, ByVal intleavependingtranunkid As Integer) As Decimal
        Dim mdclFraction As Decimal = 0
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Try

            Dim objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            strQ = " SELECT ISNULL(SUM(dayfraction),0.00) dayfraction FROM lvleaveday_fraction " & _
                      " JOIN lvpendingleave_tran ON lvleaveday_fraction.formunkid = lvpendingleave_tran.formunkid " & _
                       " WHERE lvleaveday_fraction.isvoid = 0 AND CONVERT(CHAR(8),lvleaveday_fraction.leavedate,112) BETWEEN  @startdate AND  @enddate " & _
                       " AND lvpendingleave_tran.pendingleavetranunkid = @pendingleavetranunkid AND lvpendingleave_tran.isvoid = 0 "

            objDataOperation.AddParameter("@pendingleavetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intleavependingtranunkid)
            objDataOperation.AddParameter("@startdate", SqlDbType.NChar, eZeeDataType.NAME_SIZE, strStartDate)
            objDataOperation.AddParameter("@enddate", SqlDbType.NChar, eZeeDataType.NAME_SIZE, strEnddate)
            dsList = objDataOperation.ExecQuery(strQ, "List")


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdclFraction = CDec(dsList.Tables(0).Rows(0)("dayfraction"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeLeaveDay_Fraction", mstrModuleName)
        End Try
        Return mdclFraction
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function VoidApproverLeaveDayFraction(ByVal objDataOperation As clsDataOperation, ByVal intFormId As Integer, Optional ByVal intUserId As Integer = -1 _
                                                                    , Optional ByVal intEmployeeLoginid As Integer = -1) As Boolean
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim blnFlag As Boolean = False
        Dim exForce As Exception
        Try

            strQ = " SELECT dayfractionunkid FROM lvleaveday_fraction WHERE formunkid = @formunkid AND approverunkid > 0 and isvoid = 0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormId)
            dsList = objDataOperation.ExecQuery(strQ, "List")


            objDataOperation.ClearParameters()

            strQ = " Update lvleaveday_fraction set isvoid = 1 " & _
                      ", voiddatetime = @voiddatetime " & _
                      ", voidreason = @voidreason "

            If intUserId > 0 Then
                strQ &= ", voiduserunkid = @voiduserunkid  "
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(intUserId > 0, intUserId, User._Object._Userunkid))
                mintUserunkid = intUserId
            ElseIf intEmployeeLoginid > 0 Then
                strQ &= ", voidloginemployeeunkid = @voidloginemployeeunkid  "
                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeLoginid)
                mintUserunkid = intEmployeeLoginid
            End If

            strQ &= " WHERE formunkid = @formunkid AND approverunkid > 0 and isvoid = 0"

            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormId)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For Each dr As DataRow In dsList.Tables(0).Rows

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", intFormId, "lvleaveday_fraction", "dayfractionunkid", CInt(dr("dayfractionunkid")), 2, 3, False, mintUserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            End If
            blnFlag = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "VoidApproverLeaveDayFraction", mstrModuleName)
        End Try
        Return blnFlag
    End Function

    'Pinkal (06-Nov-2015) -- Start
    'Enhancement - WORKING ON FIXING BUG ON DUPLICATION OF LEAVE DAY FRACTION FROM WEB.

    '' <summary>
    '' Modify By: Pinkal Jariwala
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    Public Function IsDayFractionExist(ByVal objDataOperation As clsDataOperation, ByVal intFormID As Integer, ByVal mdtLeaveDate As Date, ByVal intApproverID As Integer) As Boolean
        Try
            Dim strQ As String = ""
            Dim dsList As DataSet = Nothing
            Dim exForce As Exception

            objDataOperation.ClearParameters()

            strQ = " SELECT  formunkid " & _
                      ", leavedate " & _
                      ", dayfraction " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                      ", approverunkid  " & _
                      " FROM lvleaveday_fraction  " & _
                      " WHERE formunkid = @formunkid AND convert(Char(8),leavedate,112) = @leavedate AND isvoid = 0"

            If intApproverID > 0 Then
                strQ &= " AND approverunkid = @approverunkid "
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverID)
            End If

            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormID)
            objDataOperation.AddParameter("@leavedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtLeaveDate))
            dsList = objDataOperation.ExecQuery(strQ, "DayFraction")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsDayFractionExist; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    'Pinkal (06-Nov-2015) -- End



    '''' <summary>
    '''' Modify By: Pinkal Jariwala
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (lvleaveday_fraction) </purpose>
    'Public Function Update(ByVal objDataOperation As clsDataOperation) As Boolean
    '    'If isExist(mstrName, mintDayfractionunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Try

    '        strQ = "UPDATE lvleaveday_fraction SET " & _
    '       "  formunkid = @formunkid" & _
    '       ", leavedate = @leavedate" & _
    '       ", dayfraction = @dayfraction" & _
    '       ", userunkid = @userunkid" & _
    '       ", loginemployeeunkid = @loginemployeeunkid" & _
    '       ", isvoid = @isvoid" & _
    '       ", voiduserunkid = @voiduserunkid" & _
    '       ", voiddatetime = @voiddatetime" & _
    '       ", voidreason = @voidreason " & _
    '     "WHERE dayfractionunkid = @dayfractionunkid "


    '        If mdtFraction Is Nothing Or mdtFraction.Rows.Count = 0 Then Return True

    '        For Each dr As DataRow In mdtFraction.Rows

    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@dayfractionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("dayfractionunkid").ToString)
    '            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
    '            objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dr("leavedate").ToString)
    '            objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, dr("dayfraction").ToString)
    '            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
    '            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
    '            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

    '            Call objDataOperation.ExecNonQuery(strQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "lvleaveform", mintFormunkid, "formunkid", 2) Then
    '                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", mintFormunkid, "lvleaveday_fraction", "dayfractionunkid", CInt(dr("dayfractionunkid").ToString), 2, 2) = False Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If
    '            End If

    '        Next

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '    End Try
    'End Function


    ''' <summary>
    ''' Create By: Shani Sheladiya
    ''' Date : 23 - JAN -2016
    ''' </summary>
    ''' <purpose> </purpose>
    Public Function GetDayFraction(ByRef dttran As DataTable, ByVal intLeaveFormUnkId As Integer, ByVal intEmpUnkId As Integer, ByVal dtStartDate As Date, ByVal intLeaveTypeID As Integer, ByVal strEmployeeAsOnDate As String, Optional ByVal dtEndDate As Date = Nothing) As Boolean
        Dim objLeaveType As New clsleavetype_master
        Dim objEmpHoliday As New clsemployee_holiday
        Dim objShift As New clsNewshift_master
        Dim blnIssueonHoliday As Boolean = False
        Try
            'Dim mdtStartDate As Date = Nothing
            'Dim mdtEndDate As Date = Nothing

            If dtEndDate = Nothing Then
                dtEndDate = dtStartDate
            End If

            Dim mintTotalDays As Integer = CInt(DateDiff(DateInterval.Day, dtStartDate, dtEndDate.AddDays(1)))

            objLeaveType._Leavetypeunkid = intLeaveTypeID
            blnIssueonHoliday = objLeaveType._Isissueonholiday

            Dim objEmpShiftTran As New clsEmployee_Shift_Tran
            Dim objShiftTran As New clsshift_tran
            Dim blnIssueonweekend As Boolean = False
            Dim blnConsiderLVHlOnWk As Boolean = False
            blnIssueonweekend = objLeaveType._Isissueonweekend
            blnConsiderLVHlOnWk = objLeaveType._IsLeaveHL_onWK

            Dim objEmp As New clsEmployee_Master

            objEmp._Employeeunkid(eZeeDate.convertDate(strEmployeeAsOnDate)) = intEmpUnkId
            If dttran IsNot Nothing AndAlso dttran.Rows.Count > 0 Then

                Dim drRow As DataRow() = dttran.Select("leavedate < '" & dtStartDate & "' AND AUD <> 'D'")
                If drRow.Length > 0 Then
                    For i As Integer = 0 To drRow.Length - 1
                        drRow(i)("AUD") = "D"
                    Next
                    dttran.AcceptChanges()
                End If

                drRow = dttran.Select("leavedate >'" & dtEndDate & "' AND AUD <> 'D'")
                If drRow.Length > 0 Then
                    For i As Integer = 0 To drRow.Length - 1
                        drRow(i)("AUD") = "D"
                    Next
                    dttran.AcceptChanges()
                End If

                Dim mintDays As Integer = 0
                Dim mdtdate As DateTime = Nothing

                mintDays = CInt(DateDiff(DateInterval.Day, dtStartDate, dtEndDate))
                mdtdate = dtStartDate
                For i As Integer = 0 To mintDays
                    objShift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(dtStartDate.AddDays(i).Date, objEmp._Employeeunkid(eZeeDate.convertDate(strEmployeeAsOnDate)))
                    objShiftTran.GetShiftTran(objShift._Shiftunkid)
                    Dim strWeekName As String = CultureInfo.CurrentUICulture.DateTimeFormat.GetDayName(dtStartDate.AddDays(i).DayOfWeek)
                    If blnIssueonHoliday = False AndAlso blnIssueonweekend = True Then
                        If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(strEmployeeAsOnDate)), dtStartDate.AddDays(i).Date).Rows.Count > 0 Then
                            Dim dRow() As DataRow = dttran.Select("leavedate = '" & dtStartDate.AddDays(i).Date & "' AND AUD <> 'D'")
                            If dRow.Length > 0 Then
                                dRow(0)("AUD") = "D"
                                dttran.AcceptChanges()
                            End If

                            If blnIssueonweekend AndAlso blnConsiderLVHlOnWk = False Then

                                If objShiftTran._dtShiftday IsNot Nothing Then
                                    Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")
                                    If drShift.Length > 0 Then
                                        GoTo AddRecord
                                    End If

                                End If

                            End If
                            Continue For
                        End If

                        Dim objReccurent As New clsemployee_holiday
                        If objReccurent.GetEmployeeReCurrentHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(strEmployeeAsOnDate)), dtStartDate.AddDays(i).Date) Then
                            Continue For
                        End If
                    End If

                    If blnIssueonweekend = False AndAlso blnIssueonHoliday = True Then
                        If objShiftTran._dtShiftday IsNot Nothing Then
                            Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")
                            If drShift.Length > 0 Then
                                Dim dRow() As DataRow = dttran.Select("leavedate = '" & dtStartDate.AddDays(i).Date & "' AND AUD <> 'D'")
                                If dRow.Length > 0 Then
                                    dRow(0)("AUD") = "D"
                                    dttran.AcceptChanges()
                                End If

                                If blnIssueonHoliday AndAlso blnConsiderLVHlOnWk = False Then
                                    If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(strEmployeeAsOnDate)), dtStartDate.AddDays(i).Date).Rows.Count > 0 Then
                                        GoTo AddRecord
                                    End If

                                End If
                                Continue For

                            End If

                        End If

                    End If

                    If blnIssueonweekend = False AndAlso blnIssueonHoliday = False Then

                        If objShiftTran._dtShiftday IsNot Nothing Then
                            Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")
                            If drShift.Length > 0 Then
                                Dim dRow() As DataRow = dttran.Select("leavedate = '" & dtStartDate.AddDays(i).Date & "' AND AUD <> 'D'")
                                If dRow.Length > 0 Then
                                    dRow(0)("AUD") = "D"
                                    dttran.AcceptChanges()
                                End If
                                Continue For
                            Else
                                If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(strEmployeeAsOnDate)), dtStartDate.AddDays(i).Date).Rows.Count > 0 Then
                                    Continue For
                                End If
                                Dim objReccurent As New clsemployee_holiday
                                If objReccurent.GetEmployeeReCurrentHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(strEmployeeAsOnDate)), dtStartDate.AddDays(i).Date) Then
                                    Continue For
                                End If
                            End If
                        End If
                    End If
AddRecord:

                    Dim dtRow As DataRow() = dttran.Select("leavedate = '" & mdtdate.AddDays(i).Date & "'  AND AUD <> 'D' ")
                    If dtRow.Length = 0 Then
                        Dim dr As DataRow = dttran.NewRow()
                        dr("dayfractionunkid") = -1

                        If intLeaveFormUnkId > 0 Then
                            dr("formunkid") = intLeaveFormUnkId
                        Else
                            dr("formunkid") = -1
                        End If
                        dr("leavedate") = mdtdate.AddDays(i)
                        dr("dayfraction") = 1.0
                        dr("GUID") = Guid.NewGuid.ToString()
                        dr("AUD") = "A"
                        dttran.Rows.InsertAt(dr, i)
                    End If
                Next

            Else

                If intLeaveFormUnkId > 0 Then
                    dttran = GetList("List", intLeaveFormUnkId, True, intEmpUnkId).Tables(0)
                Else
                    dttran = GetList("List", intLeaveFormUnkId, True, intEmpUnkId).Tables(0).Clone
                End If


                Dim drRow As DataRow() = dttran.Select("leavedate > '" & dtEndDate & "'")
                If drRow.Length > 0 Then
                    For i As Integer = 0 To drRow.Length - 1
                        drRow(i)("AUD") = "D"
                    Next
                    dttran.AcceptChanges()
                End If

                For i As Integer = 0 To mintTotalDays - 1
                    objShift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(dtStartDate.AddDays(i).Date, objEmp._Employeeunkid(eZeeDate.convertDate(strEmployeeAsOnDate)))
                    objShiftTran.GetShiftTran(objShift._Shiftunkid)
                    Dim strWeekName As String = CultureInfo.CurrentUICulture.DateTimeFormat.GetDayName(dtStartDate.AddDays(i).DayOfWeek)
                    If blnIssueonHoliday = False AndAlso blnIssueonweekend = True Then
                        If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(strEmployeeAsOnDate)), dtStartDate.AddDays(i).Date).Rows.Count > 0 Then
                            Dim dRow() As DataRow = dttran.Select("leavedate = '" & dtStartDate.AddDays(i).Date & "' AND AUD <> 'D'")
                            If dRow.Length > 0 Then
                                dRow(0)("AUD") = "D"
                                dttran.AcceptChanges()
                            End If

                            If blnIssueonweekend AndAlso blnConsiderLVHlOnWk = False Then

                                If objShiftTran._dtShiftday IsNot Nothing Then
                                    Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")
                                    If drShift.Length > 0 Then
                                        GoTo EditRecord
                                    End If

                                End If

                            End If
                            Continue For
                        End If

                        Dim objReccurent As New clsemployee_holiday
                        If objReccurent.GetEmployeeReCurrentHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(strEmployeeAsOnDate)), dtStartDate.AddDays(i).Date) Then
                            Continue For
                        End If
                    End If

                    If blnIssueonweekend = False AndAlso blnIssueonHoliday = True Then
                        If objShiftTran._dtShiftday IsNot Nothing Then
                            Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")
                            If drShift.Length > 0 Then
                                Dim dRow() As DataRow = dttran.Select("leavedate = '" & dtStartDate.AddDays(i).Date & "' AND AUD <> 'D'")
                                If dRow.Length > 0 Then
                                    dRow(0)("AUD") = "D"
                                    dttran.AcceptChanges()
                                End If

                                If blnIssueonHoliday AndAlso blnConsiderLVHlOnWk = False Then
                                    If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(strEmployeeAsOnDate)), dtStartDate.AddDays(i).Date).Rows.Count > 0 Then
                                        GoTo EditRecord
                                    End If

                                End If
                                Continue For
                            End If
                        End If
                    End If
                    If blnIssueonweekend = False AndAlso blnIssueonHoliday = False Then
                        If objShiftTran._dtShiftday IsNot Nothing Then
                            Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")
                            If drShift.Length > 0 Then
                                Dim dRow() As DataRow = dttran.Select("leavedate = '" & dtStartDate.AddDays(i).Date & "' AND AUD <> 'D'")
                                If dRow.Length > 0 Then
                                    dRow(0)("AUD") = "D"
                                    dttran.AcceptChanges()
                                End If
                                Continue For
                            Else
                                If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(strEmployeeAsOnDate)), dtStartDate.AddDays(i).Date).Rows.Count > 0 Then
                                    Continue For
                                End If
                                Dim objReccurent As New clsemployee_holiday
                                If objReccurent.GetEmployeeReCurrentHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(strEmployeeAsOnDate)), dtStartDate.AddDays(i).Date) Then
                                    Continue For
                                End If
                            End If
                        End If
                    End If
EditRecord:
                    Dim dtRow As DataRow() = dttran.Select("leavedate = '" & dtStartDate.Date.AddDays(i).Date & "' AND AUD = ''")
                    If dtRow.Length = 0 Then
                        Dim dr As DataRow = dttran.NewRow
                        dr("dayfractionunkid") = -1
                        dr("formunkid") = intLeaveFormUnkId
                        dr("leavedate") = dtStartDate.Date.AddDays(i).ToShortDateString
                        dr("dayfraction") = 1.0
                        If dr("AUD").ToString() = "" Then
                            dr("AUD") = "A"
                        End If
                        dr("GUID") = Guid.NewGuid.ToString()
                        dttran.Rows.Add(dr)
                    End If
                Next

            End If

            Dim dtDelete As DataTable = New DataView(dttran, "AUD = 'D'", "leavedate asc", DataViewRowState.CurrentRows).ToTable
            dttran = New DataView(dttran, "", "leavedate asc", DataViewRowState.CurrentRows).ToTable
            'Me.ViewState("DeletedFraction") = dtDelete

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsDayFractionExist; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function


    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmpLeaveDayFractionForSkipApproverFlow(ByVal strDate As String, ByVal intFormunkid As Integer, ByVal intEmployeeunkid As Integer, Optional ByVal objDoOperation As clsDataOperation = Nothing) As Decimal
        Dim mdclFraction As Decimal = 0
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Try
            If objDoOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDoOperation
            End If

            objDataOperation.ClearParameters()

            strQ = " SELECT ISNULL(lvleaveday_fraction.dayfraction,0.00) dayfraction " & _
                      " FROM lvleaveday_fraction " & _
                      " WHERE CONVERT(CHAR(8),lvleaveday_fraction.leavedate,112)= @leavedate AND lvleaveday_fraction.isvoid = 0 AND approverunkid <= 0 "

            If intFormunkid > 0 Then
                strQ &= " AND lvleaveday_fraction.formunkid  = @formunkid "
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@leavedate", SqlDbType.NChar, eZeeDataType.NAME_SIZE, strDate)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdclFraction = CDec(dsList.Tables(0).Rows(0)("dayfraction"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmpLeaveDayFractionForSkipApproverFlow; Module Name: " & mstrModuleName)
        End Try
        Return mdclFraction
    End Function

    'Pinkal (01-Oct-2018) -- End


End Class