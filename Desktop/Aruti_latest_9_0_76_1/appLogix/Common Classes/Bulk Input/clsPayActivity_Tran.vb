﻿'************************************************************************************************************************************
'Class Name :clsPayActivity_Tran.vb
'Purpose    :
'Date       :12-Jun-2013
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma 
''' </summary>

Public Class clsPayActivity_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsPayActivity_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintPayactivitytranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintCostcenterunkid As Integer
    Private mintActivityunkid As Integer
    Private mdtActivity_Date As Date
    Private mdecActivity_Value As Decimal
    Private mintUserunkid As Integer
    Private mblnIsapproved As Boolean
    Private mintApproveruserunkid As Integer
    Private mblnIsposted As Boolean
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintVoiduserunkid As Integer
    Private mblnIsformula_Based As Boolean
    Private mintTranheadunkid As Integer
    Private mdecActivity_Rate As Decimal
    Private mdecAmount As Decimal
    Private ObjOperation As clsDataOperation
    Private mdecNormal_Factor As Decimal
    Private mdecPH_Factor As Decimal
    Private mdecNormal_Hrs As Decimal
    Private mdecOt_Hrs As Decimal
    Private mdecNormal_Amount As Decimal
    Private mdecOt_Amount As Decimal
    Private mstrWebFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty


    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvance_Filter As String = String.Empty
    'Pinkal (06-Mar-2014) -- End


    'Pinkal (4-Sep-2014) -- Start
    'Enhancement - PAY_A CHANGES IN PPA
    Private mintOT_Tranheadunkid As Integer
    Private mintPH_Tranheadunkid As Integer
    'Pinkal (4-Sep-2014) -- End


#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set payactivitytranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Payactivitytranunkid() As Integer
        Get
            Return mintPayactivitytranunkid
        End Get
        Set(ByVal value As Integer)
            mintPayactivitytranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set costcenterunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Costcenterunkid() As Integer
        Get
            Return mintCostcenterunkid
        End Get
        Set(ByVal value As Integer)
            mintCostcenterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set activityunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Activityunkid() As Integer
        Get
            Return mintActivityunkid
        End Get
        Set(ByVal value As Integer)
            mintActivityunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set activity_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Activity_Date() As Date
        Get
            Return mdtActivity_Date
        End Get
        Set(ByVal value As Date)
            mdtActivity_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set activity_value
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Activity_Value() As Decimal
        Get
            Return mdecActivity_Value
        End Get
        Set(ByVal value As Decimal)
            mdecActivity_Value = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isapproved
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isapproved() As Boolean
        Get
            Return mblnIsapproved
        End Get
        Set(ByVal value As Boolean)
            mblnIsapproved = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approveruserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Approveruserunkid() As Integer
        Get
            Return mintApproveruserunkid
        End Get
        Set(ByVal value As Integer)
            mintApproveruserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isposted
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isposted() As Boolean
        Get
            Return mblnIsposted
        End Get
        Set(ByVal value As Boolean)
            mblnIsposted = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isformula_based
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isformula_Based() As Boolean
        Get
            Return mblnIsformula_Based
        End Get
        Set(ByVal value As Boolean)
            mblnIsformula_Based = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set activity_rate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Activity_Rate() As Decimal
        Get
            Return mdecActivity_Rate
        End Get
        Set(ByVal value As Decimal)
            mdecActivity_Rate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set amount
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Amount() As Decimal
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal)
            mdecAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set normal_factor
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Normal_Factor() As Decimal
        Get
            Return mdecNormal_Factor
        End Get
        Set(ByVal value As Decimal)
            mdecNormal_Factor = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ph_factor
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _PH_Factor() As Decimal
        Get
            Return mdecPH_Factor
        End Get
        Set(ByVal value As Decimal)
            mdecPH_Factor = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set normal_hrs
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Normal_Hrs() As Decimal
        Get
            Return mdecNormal_Hrs
        End Get
        Set(ByVal value As Decimal)
            mdecNormal_Hrs = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ot_hrs
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ot_Hrs() As Decimal
        Get
            Return mdecOt_Hrs
        End Get
        Set(ByVal value As Decimal)
            mdecOt_Hrs = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set normal_amount
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Normal_Amount() As Decimal
        Get
            Return mdecNormal_Amount
        End Get
        Set(ByVal value As Decimal)
            mdecNormal_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ot_amount
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ot_Amount() As Decimal
        Get
            Return mdecOt_Amount
        End Get
        Set(ByVal value As Decimal)
            mdecOt_Amount = value
        End Set
    End Property

    Public WriteOnly Property _ObjData() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            ObjOperation = value
        End Set
    End Property

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property



    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _Advance_Filter() As String
        Set(ByVal value As String)
            mstrAdvance_Filter = value
        End Set
    End Property

    'Pinkal (06-Mar-2014) -- End


    'Pinkal (4-Sep-2014) -- Start
    'Enhancement - PAY_A CHANGES IN PPA

    ''' <summary>
    ''' Purpose: Get or Set ottranheadunkid
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _OT_Tranheadunkid() As Integer
        Get
            Return mintOT_Tranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintOT_Tranheadunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set phtranheadunkid
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _PH_Tranheadunkid() As Integer
        Get
            Return mintPH_Tranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintPH_Tranheadunkid = value
        End Set
    End Property

    'Pinkal (4-Sep-2014) -- End



#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If ObjOperation IsNot Nothing Then
            objDataOperation = ObjOperation
        Else
            objDataOperation = New clsDataOperation
        End If
        Try
            strQ = "SELECT " & _
                   "  payactivitytranunkid " & _
                   ", employeeunkid " & _
                   ", periodunkid " & _
                   ", costcenterunkid " & _
                   ", activityunkid " & _
                   ", activity_date " & _
                   ", activity_value " & _
                   ", userunkid " & _
                   ", isapproved " & _
                   ", approveruserunkid " & _
                   ", isposted " & _
                   ", isvoid " & _
                   ", voiddatetime " & _
                   ", voidreason " & _
                   ", voiduserunkid " & _
                   ", isformula_based " & _
                   ", tranheadunkid " & _
                   ", activity_rate " & _
                   ", amount " & _
                   ", normal_factor " & _
                   ", ph_factor " & _
                   ", normal_hrs " & _
                   ", ot_hrs " & _
                   ", normal_amount " & _
                   ", ot_amount " & _
                   ", ot_tranheadunkid " & _
                   ", ph_tranheadunkid " & _
                   "FROM prpayactivity_tran " & _
                   "WHERE payactivitytranunkid = @payactivitytranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@payactivitytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayactivitytranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPayactivitytranunkid = CInt(dtRow.Item("payactivitytranunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintCostcenterunkid = CInt(dtRow.Item("costcenterunkid"))
                mintActivityunkid = CInt(dtRow.Item("activityunkid"))
                mdtActivity_Date = dtRow.Item("activity_date")
                mdecActivity_Value = dtRow.Item("activity_value")
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsapproved = CBool(dtRow.Item("isapproved"))
                mintApproveruserunkid = CInt(dtRow.Item("approveruserunkid"))
                mblnIsposted = CBool(dtRow.Item("isposted"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mblnIsformula_Based = CBool(dtRow.Item("isformula_based"))
                mintTranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                mdecActivity_Rate = dtRow.Item("activity_rate")
                mdecAmount = dtRow.Item("amount")
                mdecNormal_Factor = dtRow.Item("normal_factor")
                mdecPH_Factor = dtRow.Item("ph_factor")
                mdecNormal_Hrs = dtRow.Item("normal_hrs")
                mdecOt_Hrs = dtRow.Item("ot_hrs")
                mdecNormal_Amount = dtRow.Item("normal_amount")
                mdecOt_Amount = dtRow.Item("ot_amount")

                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA
                mintOT_Tranheadunkid = CInt(dtRow.Item("ot_tranheadunkid"))
                mintPH_Tranheadunkid = CInt(dtRow.Item("ph_tranheadunkid"))
                'Pinkal (4-Sep-2014) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, _
                            Optional ByVal iEmployeeId As Integer = 0, _
                            Optional ByVal iPeriodId As Integer = 0, _
                            Optional ByVal iActivitys As String = "", _
                            Optional ByVal dtDate1 As Date = Nothing, _
                            Optional ByVal dtDate2 As Date = Nothing, _
                            Optional ByVal OnlyPosted As Boolean = False, Optional ByVal IsExport As Boolean = False, _
                            Optional ByVal strFilter As String = "", _
                            Optional ByVal xDataOp As clsDataOperation = Nothing) As DataSet
        'Sohail (03 May 2018) - [xDataOp]
        'Sohail (22 Aug 2014) - [strFilter]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT " & _
                   " Cast(0 AS Bit) As [Select] " & _
                   ",  prpayactivity_tran.payactivitytranunkid " & _
                   ", prpayactivity_tran.employeeunkid " & _
                   ", prpayactivity_tran.periodunkid " & _
                   ", prpayactivity_tran.costcenterunkid " & _
                   ", prpayactivity_tran.activityunkid " & _
                   ", prpayactivity_tran.activity_date " & _
                   ", prpayactivity_tran.activity_value " & _
                   ", prpayactivity_tran.userunkid " & _
                   ", prpayactivity_tran.isapproved " & _
                   ", prpayactivity_tran.approveruserunkid " & _
                   ", prpayactivity_tran.isposted " & _
                   ", prpayactivity_tran.isvoid " & _
                   ", prpayactivity_tran.voiddatetime " & _
                   ", prpayactivity_tran.voidreason " & _
                   ", prpayactivity_tran.voiduserunkid " & _
                   ", ISNULL(prcostcenter_master.costcentername, '') AS costcentername " & _
                   ", hremployee_master.firstname+' '+hremployee_master.surname AS employeename " & _
                   ", practivity_master.name AS activityname " & _
                   ", practivity_master.code AS activitycode " & _
                   ", practivity_master.trnheadtype_id " & _
                   ", prunitmeasure_master.name AS measure " & _
                   ", CONVERT(CHAR(8),prpayactivity_tran.activity_date,112) AS ADate " & _
                   ", prpayactivity_tran.isformula_based " & _
                   ", prpayactivity_tran.tranheadunkid " & _
                   ", prpayactivity_tran.activity_rate " & _
                   ", prpayactivity_tran.amount " & _
                   ", ISNULL(prtranhead_master.trnheadname,'') AS tname " & _
                   ", prpayactivity_tran.normal_factor " & _
                   ", prpayactivity_tran.ph_factor " & _
                   ", prpayactivity_tran.normal_hrs " & _
                   ", prpayactivity_tran.ot_hrs " & _
                   ", prpayactivity_tran.normal_amount " & _
                   ", prpayactivity_tran.ot_amount " & _
                   ", prpayactivity_tran.ot_tranheadunkid " & _
                   ", prpayactivity_tran.ph_tranheadunkid " & _
                   ", ISNULL(prtranhead_master.roundofftypeid, 200) AS roundofftypeid " & _
                   ", ISNULL(prtranhead_master.roundoffmodeid, 1) AS roundoffmodeid " & _
                   "FROM prpayactivity_tran " & _
                   "  LEFT JOIN prtranhead_master ON prpayactivity_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                   "  LEFT JOIN prcostcenter_master ON prpayactivity_tran.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                   "  JOIN hremployee_master ON prpayactivity_tran.employeeunkid = hremployee_master.employeeunkid " & _
                   "  JOIN practivity_master ON prpayactivity_tran.activityunkid = practivity_master.activityunkid " & _
                   "  JOIN prunitmeasure_master ON practivity_master.measureunkid = prunitmeasure_master.measureunkid " & _
                   " WHERE prpayactivity_tran.isvoid = 0 "
            'Sohail (11 Dec 2020) - [roundoffmodeid]
            'Sohail (03 Dec 2020) - [roundofftypeid]
            'Sohail (06 Nov 2014) - issue of activities without cost center are not coming. [JOIN prcostcenter_master = LEFT JOIN prcostcenter_master]
            'Sohail (21 Jun 2013) - [trnheadtype_id]

            If iEmployeeId > 0 Then strQ &= " AND prpayactivity_tran.employeeunkid = '" & iEmployeeId & "' "
            If iPeriodId > 0 Then strQ &= " AND prpayactivity_tran.periodunkid = '" & iPeriodId & "' "
            If iActivitys.Length > 0 Then strQ &= " AND prpayactivity_tran.activityunkid IN (" & iActivitys & ") "

            If dtDate1 <> Nothing AndAlso dtDate2 <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),prpayactivity_tran.activity_date,112) BETWEEN @Date1 AND @Date2 "
                objDataOperation.AddParameter("@Date1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDate1).ToString)
                objDataOperation.AddParameter("@Date2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDate2).ToString)
            End If

            If IsExport = False Then
                If OnlyPosted = True Then
                    strQ &= " AND prpayactivity_tran.isposted = 1 "
                Else
                    strQ &= " AND prpayactivity_tran.isposted = 0 "
                End If
            End If

            'Sohail (22 Aug 2014) -- Start
            'Enhancement - Update OT Rates and PH Rates to UnPosted Activities.
            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If
            'Sohail (22 Aug 2014) -- End

            'S.SANDEEP [ 04 DEC 2014 ] -- START
            'strQ &= " ORDER BY prpayactivity_tran.activity_date ASC " & _
            '                    ", hremployee_master.firstname+' '+hremployee_master.surname ASC " & _
            '                    ", practivity_master.name ASC " & _
            '                    ", prunitmeasure_master.name ASC " & _
            '                    ", prpayactivity_tran.activity_value ASC "

            strQ &= " ORDER BY prpayactivity_tran.activity_date DESC " & _
                    ", hremployee_master.firstname+' '+hremployee_master.surname ASC " & _
                    ", practivity_master.name ASC " & _
                    ", prunitmeasure_master.name ASC " & _
                    ", prpayactivity_tran.activity_value ASC "
            'S.SANDEEP [ 04 DEC 2014 ] -- END


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prpayactivity_tran) </purpose>
    Public Function Insert(ByVal dtCurrentDateAndTime As DateTime,Optional ByVal iUserId As Integer = 0) As Boolean
        'Shani(24-Aug-2015) - [dtCurrentDateAndTime]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivityunkid.ToString)
            objDataOperation.AddParameter("@activity_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActivity_Date.ToString)
            objDataOperation.AddParameter("@activity_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecActivity_Value)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsposted.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@isformula_based", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsformula_Based.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@activity_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecActivity_Rate)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount)
            objDataOperation.AddParameter("@normal_factor", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNormal_Factor)
            objDataOperation.AddParameter("@ph_factor", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPH_Factor)
            objDataOperation.AddParameter("@normal_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNormal_Hrs)
            objDataOperation.AddParameter("@ot_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOt_Hrs)
            objDataOperation.AddParameter("@normal_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNormal_Amount)
            objDataOperation.AddParameter("@ot_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOt_Amount)


            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA
            objDataOperation.AddParameter("@ot_tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOT_Tranheadunkid.ToString)
            objDataOperation.AddParameter("@ph_tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPH_Tranheadunkid.ToString)
            'Pinkal (4-Sep-2014) -- End


            strQ = "INSERT INTO prpayactivity_tran ( " & _
                       "  employeeunkid " & _
                       ", periodunkid " & _
                       ", costcenterunkid " & _
                       ", activityunkid " & _
                       ", isformula_based " & _
                       ", tranheadunkid " & _
                       ", activity_date " & _
                       ", activity_value " & _
                       ", activity_rate " & _
                       ", amount " & _
                       ", userunkid " & _
                       ", isapproved " & _
                       ", approveruserunkid " & _
                       ", isposted " & _
                       ", isvoid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                       ", voiduserunkid " & _
                       ", normal_factor " & _
                       ", ph_factor " & _
                       ", normal_hrs " & _
                       ", ot_hrs " & _
                       ", normal_amount " & _
                       ", ot_amount " & _
                       ", ot_tranheadunkid " & _
                       ", ph_tranheadunkid " & _
                   ") VALUES (" & _
                       "  @employeeunkid " & _
                       ", @periodunkid " & _
                       ", @costcenterunkid " & _
                       ", @activityunkid " & _
                       ", @isformula_based " & _
                       ", @tranheadunkid " & _
                       ", @activity_date " & _
                       ", @activity_value " & _
                       ", @activity_rate " & _
                       ", @amount " & _
                       ", @userunkid " & _
                       ", @isapproved " & _
                       ", @approveruserunkid " & _
                       ", @isposted " & _
                       ", @isvoid " & _
                       ", @voiddatetime " & _
                       ", @voidreason " & _
                       ", @voiduserunkid " & _
                       ", @normal_factor " & _
                       ", @ph_factor " & _
                       ", @normal_hrs " & _
                       ", @ot_hrs " & _
                       ", @normal_amount " & _
                       ", @ot_amount " & _
                       ", @ot_tranheadunkid " & _
                       ", @ph_tranheadunkid " & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPayactivitytranunkid = dsList.Tables(0).Rows(0).Item(0)


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If Insert_AT_Log(objDataOperation, 1, iUserId) = False Then
            If Insert_AT_Log(objDataOperation, 1, iUserId, dtCurrentDateAndTime) = False Then
                'Shani(24-Aug-2015) -- End

                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prpayactivity_tran) </purpose>
    Public Function Update(ByVal dtCurrentDateAndTime As DateTime,Optional ByVal ObjData As clsDataOperation = Nothing, Optional ByVal iUserId As Integer = 0) As Boolean
        'Shani(24-Aug-2015) - [dtCurrentDateAndTime]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If ObjData IsNot Nothing Then
            objDataOperation = ObjData
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@payactivitytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayactivitytranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivityunkid.ToString)
            objDataOperation.AddParameter("@activity_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActivity_Date.ToString)
            objDataOperation.AddParameter("@activity_value", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecActivity_Value.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsposted.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@isformula_based", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsformula_Based.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@activity_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecActivity_Rate)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount)
            objDataOperation.AddParameter("@normal_factor", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNormal_Factor)
            objDataOperation.AddParameter("@ph_factor", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPH_Factor)
            objDataOperation.AddParameter("@normal_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNormal_Hrs)
            objDataOperation.AddParameter("@ot_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOt_Hrs)
            objDataOperation.AddParameter("@normal_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNormal_Amount)
            objDataOperation.AddParameter("@ot_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOt_Amount)


            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA
            objDataOperation.AddParameter("@ot_tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOT_Tranheadunkid.ToString)
            objDataOperation.AddParameter("@ph_tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPH_Tranheadunkid.ToString)
            'Pinkal (4-Sep-2014) -- End


            strQ = "UPDATE prpayactivity_tran SET " & _
                   "  employeeunkid = @employeeunkid" & _
                   ", periodunkid = @periodunkid" & _
                   ", costcenterunkid = @costcenterunkid" & _
                   ", activityunkid = @activityunkid" & _
                   ", isformula_based = @isformula_based" & _
                   ", tranheadunkid = @tranheadunkid" & _
                   ", activity_date = @activity_date" & _
                   ", activity_value = @activity_value" & _
                   ", activity_rate = @activity_rate" & _
                   ", amount = @amount" & _
                   ", userunkid = @userunkid" & _
                   ", isapproved = @isapproved" & _
                   ", approveruserunkid = @approveruserunkid" & _
                   ", isposted = @isposted" & _
                   ", isvoid = @isvoid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason" & _
                   ", voiduserunkid = @voiduserunkid " & _
                   ", normal_factor = @normal_factor" & _
                   ", ph_factor = @ph_factor " & _
                   ", normal_hrs = @normal_hrs" & _
                   ", ot_hrs = @ot_hrs" & _
                   ", normal_amount = @normal_amount" & _
                   ", ot_amount = @ot_amount " & _
                   ", ot_tranheadunkid = @ot_tranheadunkid " & _
                   ", ph_tranheadunkid = @ph_tranheadunkid " & _
                   "WHERE payactivitytranunkid = @payactivitytranunkid "


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _ObjData = objDataOperation
            _Payactivitytranunkid = mintPayactivitytranunkid


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If Insert_AT_Log(objDataOperation, 2, iUserId) = False Then
            If Insert_AT_Log(objDataOperation, 2, iUserId, dtCurrentDateAndTime) = False Then
                'Shani(24-Aug-2015) -- End

                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    'Pinkal (4-Sep-2014) -- Start
    'Enhancement - PAY_A CHANGES IN PPA

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prpayactivity_tran) </purpose>
    ''' 
    Public Function Delete(ByVal intUnkid As Integer, ByVal dtCurrentDateAndTime As DateTime, Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        'Shani(24-Aug-2015) - [dtCurrentDateAndTime]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try
            strQ = "UPDATE prpayactivity_tran SET " & _
                        "  isvoid = @isvoid" & _
                        ", voiddatetime = @voiddatetime" & _
                        ", voidreason = @voidreason" & _
                        ", voiduserunkid = @voiduserunkid " & _
                   "WHERE payactivitytranunkid = @payactivitytranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@payactivitytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _ObjData = objDataOperation
            _Payactivitytranunkid = intUnkid


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If Insert_AT_Log(objDataOperation, 3, mintVoiduserunkid) = False Then
            If Insert_AT_Log(objDataOperation, 3, mintVoiduserunkid, dtCurrentDateAndTime) = False Then
                'Shani(24-Aug-2015) -- End

                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDOperation Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then
            objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iEmployeeId As Integer, ByVal iDate As Date, ByVal iActivityId As Integer, ByVal iCostCenterId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim iTranUnkid As Integer = 0
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                   "   payactivitytranunkid " & _
                   "FROM prpayactivity_tran " & _
                   "WHERE isvoid = 0 " & _
                   "   AND employeeunkid = @iEmployeeId " & _
                   "   AND CONVERT(CHAR(8),activity_date,112) = @iDate " & _
                   "   AND activityunkid = @iActivityId " & _
                   "   AND costcenterunkid = @iCostCenterId " & _
                   "   AND isposted = 0 "

            objDataOperation.AddParameter("@iEmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeId)
            objDataOperation.AddParameter("@iDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(iDate).ToString)
            objDataOperation.AddParameter("@iActivityId", SqlDbType.Int, eZeeDataType.INT_SIZE, iActivityId)
            objDataOperation.AddParameter("@iCostCenterId", SqlDbType.Int, eZeeDataType.INT_SIZE, iCostCenterId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                iTranUnkid = dsList.Tables(0).Rows(0).Item(0)
            End If

            Return iTranUnkid
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Get Total Of Activity/Measure By Employee.
    ''' </summary>
    ''' <param name="iTotalBy">1 = By Activity, 2 = By Measure</param>
    ''' <param name="iVType">1 = By Date, 2 = By Period</param>
    ''' <remarks></remarks>
    Public Function Employee_Based_Total(ByVal iTotalBy As Integer, _
                                         ByVal iVType As Integer, _
                                         Optional ByVal iEmployeeId As Integer = 0, _
                                         Optional ByVal iDate As Date = Nothing, _
                                         Optional ByVal iPeriodId As Integer = 0) As DataSet 'S.SANDEEP [ 10 DEC 2014 ] -- START {iVType,iDate,iPeriodId} -- END
        'Public Function Employee_Based_Total(ByVal iTotalBy As Integer, ByVal iDate As Date, Optional ByVal iEmployeeId As Integer = 0) As DataSet
        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT "
            Select Case iTotalBy
                Case 2
                    strQ &= "prunitmeasure_master.name AS TotalBy " & _
                            ", hremployee_master.firstname+' '+hremployee_master.surname AS employeename "
                Case Else
                    strQ &= "practivity_master.name AS TotalBy " & _
                            ", hremployee_master.firstname+' '+hremployee_master.surname AS employeename "
            End Select

            strQ &= ",ISNULL(SUM(activity_value),0) AS Total " & _
                    "FROM prpayactivity_tran "

            Select Case iTotalBy
                Case 2
                    strQ &= " JOIN practivity_master ON prpayactivity_tran.activityunkid = practivity_master.activityunkid " & _
                            " JOIN prunitmeasure_master ON practivity_master.measureunkid = prunitmeasure_master.measureunkid "
                Case Else
                    strQ &= " JOIN practivity_master ON prpayactivity_tran.activityunkid = practivity_master.activityunkid "
            End Select
            strQ &= "  JOIN hremployee_master ON prpayactivity_tran.employeeunkid = hremployee_master.employeeunkid "

            'S.SANDEEP [ 10 DEC 2014 ] -- START
            'strQ &= "WHERE isvoid = 0 AND CONVERT(CHAR(8),prpayactivity_tran.activity_date,112) = '" & eZeeDate.convertDate(iDate).ToString & "' "
            strQ &= "WHERE isvoid = 0 "

            If iVType = 1 Then
                strQ &= " AND CONVERT(CHAR(8),prpayactivity_tran.activity_date,112) = '" & eZeeDate.convertDate(iDate).ToString & "' "
            Else
                strQ &= " AND prpayactivity_tran.periodunkid = '" & iPeriodId & "' "
            End If
            'S.SANDEEP [ 10 DEC 2014 ] -- END

            If iEmployeeId > 0 Then
                strQ &= " AND prpayactivity_tran.employeeunkid = '" & iEmployeeId & "' "
            End If

            Select Case iTotalBy
                Case 2
                    strQ &= "GROUP BY prunitmeasure_master.name, hremployee_master.firstname+' '+hremployee_master.surname"
                Case Else
                    strQ &= "GROUP BY practivity_master.name, hremployee_master.firstname+' '+hremployee_master.surname"
            End Select

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Employee_Based_Total; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function


    'Pinkal (4-Sep-2014) -- Start
    'Enhancement - PAY_A CHANGES IN PPA

    ''' <summary>
    ''' Get the Rate/Transaction Head Set for the Given Activity
    ''' </summary>
    ''' <param name="iActivityId">The Selected Activityunkid</param>
    ''' <param name="iDate">Date of Rate Defined</param>
    ''' <remarks></remarks>
    Public Function Get_Activity_Rate(ByVal iActivityId As Integer, ByVal iDate As Date) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                   "  activity_rate " & _
                   " ,practivityrate_master.tranheadunkid " & _
                   " ,ISNULL(prtranhead_master.trnheadname,'') AS t_name " & _
                   " , modeid " & _
                   " ,isformula_based " & _
                   " ,practivity_master.is_ot_activity " & _
                   "FROM practivity_master " & _
                   " JOIN practivityrate_master ON practivity_master.activityunkid = practivityrate_master.activityunkid AND practivityrate_master.isactive = 1  " & _
                   " JOIN cfcommon_period_tran on cfcommon_period_tran.periodunkid = practivityrate_master.periodunkid " & _
                   " LEFT JOIN prtranhead_master ON practivityrate_master.tranheadunkid = prtranhead_master.tranheadunkid " & _
                   "WHERE practivity_master.isactive = 1 AND practivity_master.activityunkid = '" & iActivityId & "' " & _
                   " AND CONVERT(CHAR(8),rate_date,112) = @Date AND cfcommon_period_tran.statusid = 1 "


            'Pinkal (23-Sep-2014) -- Start
            'Enhancement -  PAY_A CHANGES IN PPA  [practivity_master.is_ot_activity ]


            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(iDate).ToString)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                If CBool(dsList.Tables(0).Rows(0).Item("isformula_based")) = True Then
                    If CInt(dsList.Tables(0).Rows(0).Item("tranheadunkid")) <= 0 Or dsList.Tables(0).Rows(0).Item("t_name").ToString = "" Then
                        dsList.Tables(0).Rows.Clear()
                    End If
                Else
                    If CDec(dsList.Tables(0).Rows(0).Item("activity_rate")) = 0 Then
                        dsList.Tables(0).Rows.Clear()
                    End If
                End If
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Activity_Rate; Module Name: " & mstrModuleName)
        End Try
    End Function

    'Pinkal (4-Sep-2014) -- End

    ''' <summary>
    ''' Get the Working Hours of Employee on particular date.
    ''' </summary>
    ''' <param name="mDicDays">DayofWeeks Dictionary</param>
    ''' <param name="iShiftId">Employee Shift Id</param>
    ''' <param name="iDate">Date of Rate Defined</param>
    ''' <remarks></remarks>
    Public Function Get_Sft_WorkHrs(ByVal mDicDays As Dictionary(Of String, Integer), ByVal iShiftId As Integer, ByVal iDate As Date, ByRef IsWeekEnd As Boolean) As Double
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim dblWorkingHrs As Double = 0
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                   "  tnashift_master.shiftunkid " & _
                   " ,dayid " & _
                   " ,isweekend " & _
                   " ,CONVERT(DECIMAL(5,2),FLOOR(((tnashift_tran.workinghrs)/60)/60)+(CONVERT(DECIMAL(5,2),(tnashift_tran.workinghrs/60)%60)/100)) AS WHrs " & _
                   "FROM tnashift_master " & _
                   " JOIN tnashift_tran ON tnashift_master.shiftunkid = tnashift_tran.shiftunkid " & _
                   "WHERE tnashift_master.shiftunkid = '" & iShiftId & "' AND tnashift_master.isactive = 1 "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                If mDicDays.Keys.Count > 0 Then
                    Dim dictval = From x In mDicDays Where x.Key.Contains(iDate.DayOfWeek.ToString) Select x
                    Dim dRow() = dsList.Tables(0).Select("dayid = '" & dictval.First.Value & "'")
                    If dRow.Length > 0 Then
                        dblWorkingHrs = CDbl(dRow(0).Item("WHrs"))
                        IsWeekEnd = CBool(dRow(0).Item("isweekend"))
                    End If
                End If
            End If

            Return dblWorkingHrs

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Sft_WorkHrs; Module Name: " & mstrModuleName)
        End Try
    End Function

    '' <summary>
    '' Get Total Of Activity/Measure By Company.
    '' </summary>
    '' <param name="iVType">1 = By Date, 2 = By Period</param>
    '' <param name="iPeriod">Get Total By Selected Period for All Employee</param>
    '' <param name="iDate">Get Total of Particular Date for All Employee</param>
    '' <param name="iTotalBy">1 = By Activity, 2 = By Measure</param>
    '' <remarks></remarks>

    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
    'Public Function Company_Based_Total(ByVal iVType As Integer, _
    '                                    ByVal iTotalBy As Integer, _
    '                                    Optional ByVal iPeriod As Integer = 0, _
    '                                    Optional ByVal iDate As Date = Nothing, _
    '                                    Optional ByVal iAllocations As String = "", _
    '                                    Optional ByVal iUserAccess As String = "") As DataSet
    '    Dim dsList As New DataSet
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    objDataOperation = New clsDataOperation
    '    Try
    '        strQ = "SELECT "
    '        Select Case iTotalBy
    '            Case 2
    '                strQ &= "prunitmeasure_master.name AS TotalBy "
    '            Case Else
    '                strQ &= "practivity_master.name AS TotalBy "
    '        End Select

    '        strQ &= ",ISNULL(SUM(activity_value),0) AS Total " & _
    '                "FROM prpayactivity_tran "

    '        Select Case iTotalBy
    '            Case 2
    '                strQ &= " JOIN practivity_master ON prpayactivity_tran.activityunkid = practivity_master.activityunkid " & _
    '                        " JOIN prunitmeasure_master ON practivity_master.measureunkid = prunitmeasure_master.measureunkid "
    '            Case Else
    '                strQ &= " JOIN practivity_master ON prpayactivity_tran.activityunkid = practivity_master.activityunkid "
    '        End Select

    '        strQ &= "  JOIN hremployee_master ON prpayactivity_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                " WHERE isvoid = 0 "

    '        If iVType = 1 Then
    '            strQ &= " AND CONVERT(CHAR(8),prpayactivity_tran.activity_date,112) = '" & eZeeDate.convertDate(iDate).ToString & "' "
    '        Else
    '            strQ &= " AND prpayactivity_tran.periodunkid = '" & iPeriod & "' "
    '        End If

    '        If iAllocations.Trim.Length > 0 Then
    '            strQ &= iAllocations
    '        End If

    '        If iUserAccess.Trim.Length <= 0 Then iUserAccess = UserAccessLevel._AccessLevelFilterString
    '        strQ &= iUserAccess

    '        Select Case iTotalBy
    '            Case 2
    '                strQ &= "GROUP BY prunitmeasure_master.name"
    '            Case Else
    '                strQ &= "GROUP BY practivity_master.name"
    '        End Select

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Employee_Based_Total; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    Public Function Company_Based_Total(ByVal iVType As Integer, _
                                        ByVal iTotalBy As Integer, _
                                                              ByVal xDatabaseName As String, _
                                                              ByVal xUserUnkid As Integer, _
                                                              ByVal xYearUnkid As Integer, _
                                                              ByVal xCompanyUnkid As Integer, _
                                                              ByVal mdtEmployeeAsonDate As Date, _
                                                              ByVal xUserModeSetting As String, _
                                                              ByVal xOnlyApproved As Boolean, _
                                        Optional ByVal iPeriod As Integer = 0, _
                                        Optional ByVal iDate As Date = Nothing, _
                                                              Optional ByVal iAllocations As String = "") As DataSet
        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try

            Dim xUACQry, xUACFiltrQry
            xUACQry = "" : xUACFiltrQry = ""
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEmployeeAsonDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)


            'S.SANDEEP [05 SEP 2016] -- START
            'ENHANCEMENT : ISSUE FOR QUERY TAKING TOO MUCH OF TIME TO EXECUTE
            'strQ = "SELECT "
            'Select Case iTotalBy
            '    Case 2
            '        strQ &= "prunitmeasure_master.name AS TotalBy "
            '    Case Else
            '        strQ &= "practivity_master.name AS TotalBy "
            'End Select

            'strQ &= ",ISNULL(SUM(activity_value),0) AS Total " & _
            '        "FROM prpayactivity_tran "

            'Select Case iTotalBy
            '    Case 2
            '        strQ &= " JOIN practivity_master ON prpayactivity_tran.activityunkid = practivity_master.activityunkid " & _
            '                " JOIN prunitmeasure_master ON practivity_master.measureunkid = prunitmeasure_master.measureunkid "
            '    Case Else
            '        strQ &= " JOIN practivity_master ON prpayactivity_tran.activityunkid = practivity_master.activityunkid "
            'End Select

            'strQ &= "  JOIN hremployee_master ON prpayactivity_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '            "   LEFT JOIN " & _
            '            "  ( " & _
            '            "    SELECT " & _
            '            "         stationunkid " & _
            '            "        ,deptgroupunkid " & _
            '            "        ,departmentunkid " & _
            '            "        ,sectiongroupunkid " & _
            '            "        ,sectionunkid " & _
            '            "        ,unitgroupunkid " & _
            '            "        ,unitunkid " & _
            '            "        ,teamunkid " & _
            '            "        ,classgroupunkid " & _
            '            "        ,classunkid " & _
            '            "        ,employeeunkid " & _
            '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '            "    FROM hremployee_transfer_tran " & _
            '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
            '            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
            '            "   LEFT JOIN " & _
            '            "( " & _
            '            "    SELECT " & _
            '            "         jobunkid " & _
            '            "        ,jobgroupunkid " & _
            '            "        ,employeeunkid " & _
            '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '            "    FROM hremployee_categorization_tran " & _
            '            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
            '            ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
            '            "LEFT JOIN " & _
            '            "( " & _
            '            "    SELECT " & _
            '            "         cctranheadvalueid as costcenterunkid " & _
            '            "        ,employeeunkid " & _
            '            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '            "    FROM hremployee_cctranhead_tran " & _
            '            "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
            '            "    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
            '            ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 "


            'strQ &= " WHERE isvoid = 0 "

            'If iVType = 1 Then
            '    strQ &= " AND CONVERT(CHAR(8),prpayactivity_tran.activity_date,112) = '" & eZeeDate.convertDate(iDate).ToString & "' "
            'Else
            '    strQ &= " AND prpayactivity_tran.periodunkid = '" & iPeriod & "' "
            'End If

            'If iAllocations.Trim.Length > 0 Then
            '    strQ &= iAllocations
            'End If


            ''Pinkal (24-Aug-2015) -- Start
            ''Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            ''If iUserAccess.Trim.Length <= 0 Then iUserAccess = UserAccessLevel._AccessLevelFilterString
            ''strQ &= iUserAccess

            'If xUACFiltrQry.Trim.Length > 0 Then
            '    strQ &= " AND " & xUACFiltrQry & " "
            'End If

            ''Pinkal (24-Aug-2015) -- End



            'Select Case iTotalBy
            '    Case 2
            '        strQ &= "GROUP BY prunitmeasure_master.name"
            '    Case Else
            '        strQ &= "GROUP BY practivity_master.name"
            'End Select


            strQ = "DECLARE @table AS TABLE (employeeunkid INT) " & _
                   "INSERT INTO @table (employeeunkid) " & _
                   "SELECT " & _
                   "    hremployee_master.employeeunkid " & _
                   "FROM hremployee_master "
            'S.SANDEEP [15 NOV 2016] -- START
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            'S.SANDEEP [15 NOV 2016] -- END
            strQ &= "   LEFT JOIN " & _
                        "  ( " & _
                        "    SELECT " & _
                        "         stationunkid " & _
                        "        ,deptgroupunkid " & _
                        "        ,departmentunkid " & _
                        "        ,sectiongroupunkid " & _
                        "        ,sectionunkid " & _
                        "        ,unitgroupunkid " & _
                        "        ,unitunkid " & _
                        "        ,teamunkid " & _
                        "        ,classgroupunkid " & _
                        "        ,classunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "    FROM hremployee_transfer_tran " & _
                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                        ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                        "   LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         jobunkid " & _
                        "        ,jobgroupunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "    FROM hremployee_categorization_tran " & _
                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                        ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         cctranheadvalueid as costcenterunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "    FROM hremployee_cctranhead_tran " & _
                        "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                        "    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                        ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 "

            If iAllocations.Trim.Length > 0 Then
                strQ &= iAllocations
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    strQ &= " AND " & xUACFiltrQry & " "
            'End If
            'S.SANDEEP [15 NOV 2016] -- END
            

            strQ &= "SELECT "
            Select iTotalBy
                Case 2
                    strQ &= "prunitmeasure_master.name AS TotalBy "
                Case Else
                    strQ &= "practivity_master.name AS TotalBy "
            End Select

            strQ &= ",ISNULL(SUM(activity_value),0) AS Total " & _
                    "FROM prpayactivity_tran "

            Select Case iTotalBy
                Case 2
                    strQ &= " JOIN practivity_master ON prpayactivity_tran.activityunkid = practivity_master.activityunkid " & _
                            " JOIN prunitmeasure_master ON practivity_master.measureunkid = prunitmeasure_master.measureunkid "
                Case Else
                    strQ &= " JOIN practivity_master ON prpayactivity_tran.activityunkid = practivity_master.activityunkid "
            End Select

            strQ &= " WHERE isvoid = 0 "

            If iVType = 1 Then
                strQ &= " AND CONVERT(CHAR(8),prpayactivity_tran.activity_date,112) = '" & eZeeDate.convertDate(iDate).ToString & "' "
            Else
                strQ &= " AND prpayactivity_tran.periodunkid = '" & iPeriod & "' "
            End If

            Select iTotalBy
                Case 2
                    strQ &= "GROUP BY prunitmeasure_master.name"
                Case Else
                    strQ &= "GROUP BY practivity_master.name"
            End Select

            'S.SANDEEP [05 SEP 2016] -- START

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Employee_Based_Total; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- End

    ''' <summary>
    ''' SET POSTING/VOID OPERATION ON prpayactivity_tran Table.
    ''' </summary>
    ''' <param name="IsPosting">1 = By Date, 2 = By Period</param>
    ''' <param name="iPeriod">Get Total By Selected Period for All Employee</param>    
    ''' <remarks></remarks>
    Public Function Posting_Activity(ByVal IsPosting As Boolean, ByVal iPeriod As Integer, ByVal iUserId As Integer _
                                     , ByVal dtCurrentDateAndTime As DateTime _
                                     , Optional ByVal objDataOpr As clsDataOperation = Nothing _
                                     , Optional ByVal intEmpID As Integer = 0 _
                                     , Optional ByVal intActivityId As Integer = 0 _
                                     , Optional ByVal intPayactivityTranID As Integer = 0 _
                                     , Optional ByVal blnFromProcessPayroll As Boolean = False _
                                     ) As Boolean
        'Sohail (10 Jul 2018) - [blnFromProcessPayroll]
        'Shani(24-Aug-2015) -- [dtCurrentDateAndTime]--'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        'Sohail (21 Jun 2013) - [objDataOperation, intEmpID]

        Dim StrQ As String = String.Empty
        Dim uStrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            'Sohail (21 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation = New clsDataOperation
            'objDataOperation.BindTransaction()
            If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            Else
                objDataOperation = objDataOpr
            End If
            objDataOperation.ClearParameters()
            'Sohail (21 Jun 2013) -- End

            If IsPosting = True Then
                StrQ = "SELECT " & _
                       "  periodunkid " & _
                       " ,employeeunkid " & _
                       " ,activityunkid " & _
                       " ,ISNULL(SUM(activity_value),0) AS sum_activity_value " & _
                       " ,ISNULL(SUM(activity_rate),0) AS sum_activity_rate " & _
                       " ,ISNULL(SUM(amount),0) AS sum_activity_amount " & _
                       "FROM prpayactivity_tran " & _
                       "WHERE isvoid = 0 AND periodunkid = '" & iPeriod & "' "

                'Sohail (21 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                If intEmpID > 0 Then
                    StrQ &= " AND employeeunkid = " & intEmpID & " "
                End If
                'Sohail (21 Jun 2013) -- End


                'Pinkal (4-Sep-2014) -- Start
                'Enhancement - PAY_A CHANGES IN PPA
                If intActivityId > 0 Then
                    StrQ &= " AND activityunkid = " & intActivityId & " "
                End If

                If intPayactivityTranID > 0 Then
                    StrQ &= " AND payactivitytranunkid = " & intPayactivityTranID & " "
                End If
                'Pinkal (4-Sep-2014) -- End

                'Sohail (10 Jul 2018) -- Start
                'MTC Support Issue Id # 2376 : Payments change once payroll is voided and reprocessed. (Unposted transactions are getting posted while processing payroll. So it creates an issue when process payroll is done while any PPA transactions are not posted to payroll) in 72.1.
                If blnFromProcessPayroll = True Then
                    StrQ &= " AND isposted = 1 "
                End If
                'Sohail (10 Jul 2018) -- End

                StrQ &= " GROUP BY periodunkid,employeeunkid,activityunkid " & _
                       "ORDER BY employeeunkid "
            Else
                StrQ = "SELECT paysummunkid FROM prpayactivity_summary WHERE periodunkid = '" & iPeriod & "' "
                'Sohail (21 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                'Sohail (21 Jun 2013) -- Start
                'TRA - ENHANCEMENT
                If intEmpID > 0 Then
                    StrQ &= " AND employeeunkid = " & intEmpID & " "
                End If
                'Sohail (21 Jun 2013) -- End
                'Sohail (21 Jun 2013) -- End
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsPosting = True Then
                StrQ = "INSERT INTO prpayactivity_summary ( " & _
                           "  periodunkid " & _
                           ", employeeunkid " & _
                           ", activityunkid " & _
                           ", sum_activity_value " & _
                           ", sum_activity_rate " & _
                           ", sum_activity_amount" & _
                       ") VALUES (" & _
                           "  @periodunkid " & _
                           ", @employeeunkid " & _
                           ", @activityunkid " & _
                           ", @sum_activity_value " & _
                           ", @sum_activity_rate " & _
                           ", @sum_activity_amount" & _
                       "); SELECT @@identity"

                'Pinkal (01-Dec-2015) -- Start
                'Enhancement - Working on PPA Changes as Per Sohail Guidance.

                'Pinkal (03-May-2016) -- Start
                'Enhancement - Working on PPA Changes as Per Sohail Guidance. [Kibena Issue :  PPA activity head value and amount getting n times doubled if process payroll is done more than one time]

                If intPayactivityTranID <= 0 Then
                    uStrQ = "UPDATE prpayactivity_summary " & _
                            "SET sum_activity_value =  @sum_activity_value " & _
                            "   ,sum_activity_rate = @sum_activity_rate " & _
                            "   ,sum_activity_amount = @sum_activity_amount " & _
                            "WHERE paysummunkid = @paysummunkid "
                Else
                uStrQ = "UPDATE prpayactivity_summary " & _
                  "SET sum_activity_value =  sum_activity_value + @sum_activity_value " & _
                            ",sum_activity_rate = sum_activity_rate + @sum_activity_rate " & _
                            ",sum_activity_amount = sum_activity_amount + @sum_activity_amount " & _
                        "WHERE paysummunkid = @paysummunkid "
                End If
                'Pinkal (03-May-2016) -- End

                'Pinkal (01-Dec-2015) -- End

            Else
                StrQ = "DELETE FROM prpayactivity_summary WHERE paysummunkid = @paysummunkid"
            End If

            For Each dRow As DataRow In dsList.Tables("List").Rows
                If IsPosting = True Then 'FOR POSTING
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriod.ToString)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dRow.Item("employeeunkid"))
                    objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dRow.Item("activityunkid"))
                    objDataOperation.AddParameter("@sum_activity_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dRow.Item("sum_activity_value"))
                    objDataOperation.AddParameter("@sum_activity_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dRow.Item("sum_activity_rate"))
                    objDataOperation.AddParameter("@sum_activity_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dRow.Item("sum_activity_amount"))

                    Dim dLst As New DataSet
                    dLst = objDataOperation.ExecQuery("SELECT * FROM prpayactivity_summary WHERE employeeunkid = '" & dRow.Item("employeeunkid") & "' AND periodunkid = '" & iPeriod & "' AND activityunkid = '" & dRow.Item("activityunkid") & "' ", "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    Dim iPaySummUnkid As Integer = 0

                    If dLst.Tables(0).Rows.Count > 0 Then iPaySummUnkid = dLst.Tables(0).Rows(0).Item("paysummunkid")

                    If iPaySummUnkid <= 0 Then
                        Dim dsData As New DataSet : Dim iSummUnkid As Integer = 0
                        dsData = objDataOperation.ExecQuery(StrQ, "List")
                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                        iSummUnkid = dsData.Tables(0).Rows(0).Item(0)

                        If Insert_AT_Summary_Log(objDataOperation, 1, iUserId, iSummUnkid) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Else
                        objDataOperation.AddParameter("@paysummunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPaySummUnkid.ToString)

                        objDataOperation.ExecNonQuery(uStrQ)
                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If


                        If Insert_AT_Summary_Log(objDataOperation, 2, iUserId, iPaySummUnkid) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        '<FUTURE USE FOR COMPARE DATAROW WITHOUT LOOP>
                        'Dim dtemp() As DataRow = dLst.Tables(0).Select("paysummunkid = '" & iPaySummUnkid & "'")
                        'Dim iEqual As Boolean = False
                        'If dtemp.Length > 0 Then
                        '    Dim iCompare As IEqualityComparer(Of DataRow) = DataRowComparer.Default
                        '    iEqual = iCompare.Equals(dRow, dtemp(0))
                        'End If

                        ''''''AUDIT TRAILS
                    End If
                Else 'FOR VOID POSTING

                    If Insert_AT_Summary_Log(objDataOperation, 3, iUserId, CInt(dRow.Item("paysummunkid"))) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@paysummunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dRow.Item("paysummunkid").ToString)

                    objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                End If
            Next


            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA
            'If intEmpID <= 0 Then 'Sohail (21 Jun 2013)
            'Pinkal (4-Sep-2014) -- End


            StrQ = "SELECT payactivitytranunkid FROM prpayactivity_tran WHERE isvoid = 0 AND periodunkid = '" & iPeriod & "' "

            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA
            If intEmpID > 0 Then
                StrQ &= " AND employeeunkid = " & intEmpID
            End If

            If intActivityId > 0 Then
                StrQ &= " AND activityunkid = " & intActivityId
            End If

            If intPayactivityTranID > 0 Then
                StrQ &= " AND payactivitytranunkid = " & intPayactivityTranID & " "
            End If

            'Pinkal (4-Sep-2014) -- End


            If IsPosting = True Then
                StrQ &= " AND isposted = 0 "
            Else
                StrQ &= " AND isposted = 1 "
            End If

            'Sohail (10 Jul 2018) -- Start
            'MTC Support Issue Id # 2376 : Payments change once payroll is voided and reprocessed. (Unposted transactions are getting posted while processing payroll. So it creates an issue when process payroll is done while any PPA transactions are not posted to payroll) in 72.1.
            If blnFromProcessPayroll = True Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (10 Jul 2018) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables("List").Rows
                _ObjData = objDataOperation
                _Payactivitytranunkid = CInt(dRow.Item("payactivitytranunkid"))
                _Isposted = IsPosting

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If Update(objDataOperation) = False Then
                If Update(dtCurrentDateAndTime, objDataOperation) = False Then
                    'Shani(24-Aug-2015) -- End

                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next


            'Sohail (21 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.ReleaseTransaction(True)
            If objDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'Sohail (21 Jun 2013) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Posting_Activity; Module Name: " & mstrModuleName)
        Finally
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            If objDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
    End Function

    ''' <summary>
    ''' SET AUDIT TRAIL ON atprpayactivity_tran Table.
    ''' </summary>
    ''' <param name="objDataOperation">Transaction Object</param>
    ''' <param name="iAuditTyp">Audit Type Operaion (1 = Add, 2 = Edit, 3 = Delete)</param>
    ''' <param name="iUserId">User Id Who Perform's the Operation</param>    
    ''' <remarks></remarks>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function Insert_AT_Log(ByVal objDataOperation As clsDataOperation, ByVal iAuditTyp As Integer, ByVal iUserId As Integer) As Boolean
    Public Function Insert_AT_Log(ByVal objDataOperation As clsDataOperation, ByVal iAuditTyp As Integer, ByVal iUserId As Integer, ByVal dtCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@payactivitytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayactivitytranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivityunkid.ToString)
            objDataOperation.AddParameter("@activity_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActivity_Date.ToString)
            objDataOperation.AddParameter("@activity_value", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecActivity_Value.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsposted.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@isformula_based", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsformula_Based.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@activity_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecActivity_Rate)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount)
            objDataOperation.AddParameter("@normal_factor", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNormal_Factor)
            objDataOperation.AddParameter("@ph_factor", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPH_Factor)
            objDataOperation.AddParameter("@normal_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNormal_Hrs)
            objDataOperation.AddParameter("@ot_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOt_Hrs)
            objDataOperation.AddParameter("@normal_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecNormal_Amount)
            objDataOperation.AddParameter("@ot_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecOt_Amount)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, iAuditTyp.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(iUserId <= 0, User._Object._Userunkid, iUserId))

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            'Shani(24-Aug-2015) -- End

            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)



            'Pinkal (4-Sep-2014) -- Start
            'Enhancement - PAY_A CHANGES IN PPA
            objDataOperation.AddParameter("@ot_tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOT_Tranheadunkid.ToString)
            objDataOperation.AddParameter("@ph_tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPH_Tranheadunkid.ToString)
            'Pinkal (4-Sep-2014) -- End


            StrQ = "INSERT INTO atprpayactivity_tran ( " & _
                       "  payactivitytranunkid " & _
                       ", employeeunkid " & _
                       ", periodunkid " & _
                       ", costcenterunkid " & _
                       ", activityunkid " & _
                       ", isformula_based " & _
                       ", tranheadunkid " & _
                       ", activity_date " & _
                       ", activity_value " & _
                       ", activity_rate " & _
                       ", amount " & _
                       ", userunkid " & _
                       ", isapproved " & _
                       ", approveruserunkid " & _
                       ", isposted " & _
                       ", isvoid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                       ", voiduserunkid " & _
                       ", normal_factor " & _
                       ", ph_factor " & _
                       ", normal_hrs " & _
                       ", ot_hrs " & _
                       ", normal_amount " & _
                       ", ot_amount " & _
                       ", audittype " & _
                       ", audituserunkid " & _
                       ", auditdatetime " & _
                       ", ip " & _
                       ", host " & _
                       ", form_name " & _
                       ", module_name1 " & _
                       ", module_name2 " & _
                       ", module_name3 " & _
                       ", module_name4 " & _
                       ", module_name5 " & _
                       ", isweb " & _
                       ", loginemployeeunkid " & _
                       ", ot_tranheadunkid " & _
                       ", ph_tranheadunkid " & _
                   ") VALUES (" & _
                       "  @payactivitytranunkid " & _
                       ", @employeeunkid " & _
                       ", @periodunkid " & _
                       ", @costcenterunkid " & _
                       ", @activityunkid " & _
                       ", @isformula_based " & _
                       ", @tranheadunkid " & _
                       ", @activity_date " & _
                       ", @activity_value " & _
                       ", @activity_rate " & _
                       ", @amount " & _
                       ", @userunkid " & _
                       ", @isapproved " & _
                       ", @approveruserunkid " & _
                       ", @isposted " & _
                       ", @isvoid " & _
                       ", @voiddatetime " & _
                       ", @voidreason " & _
                       ", @voiduserunkid " & _
                       ", @normal_factor " & _
                       ", @ph_factor " & _
                       ", @normal_hrs " & _
                       ", @ot_hrs " & _
                       ", @normal_amount " & _
                       ", @ot_amount " & _
                       ", @audittype " & _
                       ", @audituserunkid " & _
                       ", @auditdatetime " & _
                       ", @ip " & _
                       ", @host " & _
                       ", @form_name " & _
                       ", @module_name1 " & _
                       ", @module_name2 " & _
                       ", @module_name3 " & _
                       ", @module_name4 " & _
                       ", @module_name5 " & _
                       ", @isweb " & _
                       ", @loginemployeeunkid " & _
                       ", @ot_tranheadunkid " & _
                       ", @ph_tranheadunkid " & _
                   "); SELECT @@identity"


            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AT_Log; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' SET AUDIT TRAIL ON atprpayactivity_summary Table.
    ''' </summary>
    ''' <param name="objDataOperation">Transaction Object</param>
    ''' <param name="iAuditTyp">Audit Type Operaion (1 = Add, 2 = Edit, 3 = Delete)</param>
    ''' <param name="iUserId">User Id Who Perform's the Operation</param>    
    ''' <param name="iSummaryUnkid">The Summary Table Primary Key Unkid</param>
    ''' <remarks></remarks>
    Private Function Insert_AT_Summary_Log(ByVal objDataOperation As clsDataOperation, ByVal iAuditTyp As Integer, ByVal iUserId As Integer, ByVal iSummaryUnkid As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            StrQ = "INSERT INTO atprpayactivity_summary " & _
                   " (paysummunkid,periodunkid,employeeunkid,activityunkid " & _
                   " ,sum_activity_value,sum_activity_rate,sum_activity_amount " & _
                   " ,audittype,audituserunkid,auditdatetime,ip,host,form_name,module_name1 " & _
                   " ,module_name2,module_name3,module_name4,module_name5,isweb,loginemployeeunkid) " & _
                   "SELECT " & _
                   "  paysummunkid,periodunkid,employeeunkid,activityunkid " & _
                   " ,sum_activity_value,sum_activity_rate,sum_activity_amount " & _
                   " ,'" & iAuditTyp & "','" & IIf(iUserId <= 0, User._Object._Userunkid, iUserId) & "',GETDATE() " & _
                   " ,'" & IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP) & "','" & IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName) & "' " & _
                   " ,'" & IIf(mstrWebFormName.Trim.Length <= 0, mstrForm_Name, mstrWebFormName) & "','" & IIf(mstrWebFormName.Trim.Length <= 0, StrModuleName1, Language.getMessage(mstrModuleName, 1, "WEB")) & "' " & _
                   " ,'" & StrModuleName2 & "','" & StrModuleName3 & "','" & StrModuleName4 & "','" & StrModuleName5 & "' " & _
                   " ,'" & IIf(mstrWebFormName.Trim.Length <= 0, False, True) & "','" & mintLogEmployeeUnkid & "' " & _
                   "FROM prpayactivity_summary WHERE paysummunkid = '" & iSummaryUnkid & "' "

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AT_Summary_Log; Module Name: " & mstrModuleName)
        End Try
    End Function

    '' <summary>
    '' Export Pay Per Activity Based On Selected Period for All Employees
    '' </summary>
    '' <param name="sDate1">Start Date in [yyyymmdd] Format</param>
    '' <param name="sDate2">End Date in [yyyymmdd] Format</param>
    '' <param name="sAllocation">Allocation Selected</param>
    '' <param name="iUserAccess">User Access Setting</param>
    '' <returns></returns>
    '' <remarks></remarks>
    '' 

    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Public Function Export_PayPerActivity(ByVal sDate1 As String, ByVal sDate2 As String, ByVal sAllocation As String, Optional ByVal iUserAccess As String = "", Optional ByVal iCurrFormat As String = "") As DataTable

    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Dim dtFinal As DataTable = Nothing
    '    Dim dtTemp As DataTable = Nothing
    '    Dim mDicUoM As New Dictionary(Of String, String)
    '    Try
    '        objDataOperation = New clsDataOperation


    '        'Pinkal (06-Mar-2014) -- Start
    '        'Enhancement : WTCL Changes PUTTING ANALYSIS BY AND ADVANCE FILTER AS PER MR.RUTTA'S COMMENT.

    '        'StrQ = "SELECT " & _
    '        '       "	 employeecode AS ECode " & _
    '        '       "	,firstname+' '+ surname AS EName " & _
    '        '       "	,hrdepartment_master.name AS Dept " & _
    '        '       "    ,hremployee_master.employeeunkid AS EmpId " & _
    '        '       "FROM hremployee_master " & _
    '        '       "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
    '        '       "WHERE isapproved = 1 " & _
    '        '       "	AND CONVERT(CHAR(8),appointeddate,112) <= @Date2 AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@Date1) >= @Date1 " & _
    '        '       "	AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@Date1) >= @Date1 AND ISNULL(CONVERT(CHAR(8),empl_enddate,112), @Date1) >= @Date1 " & _
    '        '       "	AND ISNULL(CONVERT(CHAR(8),reinstatement_date,112),@Date1) <= @Date1 "


    '        StrQ = "SELECT " & _
    '               "	 employeecode AS ECode " & _
    '               "	,firstname+' '+ surname AS EName " & _
    '                  "	,hd.name AS Dept " & _
    '                  " ,hremployee_master.employeeunkid AS EmpId "

    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        End If

    '        StrQ &= "FROM hremployee_master " & _
    '                     "	JOIN hrdepartment_master hd ON hremployee_master.departmentunkid = hd.departmentunkid "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= "WHERE isapproved = 1 " & _
    '               "	AND CONVERT(CHAR(8),appointeddate,112) <= @Date2 AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@Date1) >= @Date1 " & _
    '               "	AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@Date1) >= @Date1 AND ISNULL(CONVERT(CHAR(8),empl_enddate,112), @Date1) >= @Date1 " & _
    '               "	AND ISNULL(CONVERT(CHAR(8),reinstatement_date,112),@Date1) <= @Date1 "


    '        If mstrAdvance_Filter.Trim.Length > 0 Then
    '            StrQ &= " AND " & mstrAdvance_Filter
    '        End If

    '        'Pinkal (06-Mar-2014) -- End

    '        If sAllocation.Trim.Length > 0 Then
    '            StrQ &= sAllocation
    '        End If

    '        If iUserAccess.Trim.Length <= 0 Then iUserAccess = UserAccessLevel._AccessLevelFilterString

    '        StrQ &= iUserAccess

    '        StrQ &= " ORDER BY firstname+' '+ surname "

    '        objDataOperation.AddParameter("@Date1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, sDate1)
    '        objDataOperation.AddParameter("@Date2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, sDate2)

    '        dtFinal = objDataOperation.ExecQuery(StrQ, "List").Tables(0)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Dim dCol As DataColumn
    '        Dim objARates As New clsActivity_Rates
    '        dtTemp = objARates.Get_Date_Range(sDate1, sDate2)
    '        If dtTemp.Rows.Count > 0 Then
    '            For Each dRow As DataRow In dtTemp.Rows
    '                dCol = New DataColumn("Column" & dRow.Item("iDay"))
    '                dCol.DefaultValue = "" : dCol.Caption = dRow.Item("iName") & "#10;" & eZeeDate.convertDate(dRow.Item("iDate").ToString).ToShortDateString
    '                dCol.DataType = System.Type.GetType("System.String")
    '                dtFinal.Columns.Add(dCol)
    '            Next
    '        End If
    '        objARates = Nothing
    '        Dim objMeasure As New clsUnitMeasure_Master
    '        dtTemp = objMeasure.GetList("List", True).Tables(0)
    '        If dtTemp.Rows.Count > 0 Then
    '            For Each dRow As DataRow In dtTemp.Rows
    '                dCol = New DataColumn("UoM_" & dRow.Item("name"))
    '                dCol.DefaultValue = 0 : dCol.Caption = dRow.Item("name")
    '                dCol.DataType = System.Type.GetType("System.Decimal")
    '                dtFinal.Columns.Add(dCol)
    '                If mDicUoM.ContainsKey(dRow.Item("name")) = False Then
    '                    mDicUoM.Add(dRow.Item("name"), "UoM_" & dRow.Item("name"))
    '                End If
    '            Next
    '        End If

    '        dtFinal.Rows.Add("")

    '        For Each dRow As DataRow In dtTemp.Rows
    '            Dim tRow As DataRow = dtFinal.NewRow

    '            tRow.Item("ECode") = dRow.Item("code")
    '            tRow.Item("EName") = dRow.Item("name")
    '            tRow.Item("Dept") = ""
    '            tRow.Item("EmpId") = 0

    '            dtFinal.Rows.Add(tRow)
    '        Next

    '        objMeasure = Nothing

    '        dtTemp = GetList("List", , , , eZeeDate.convertDate(sDate1), eZeeDate.convertDate(sDate2), , True).Tables(0)
    '        Dim iDay As String = String.Empty
    '        If dtTemp.Rows.Count > 0 Then
    '            For Each dRow As DataRow In dtTemp.Rows
    '                iDay = CDate(dRow.Item("activity_date")).Day
    '                If iCurrFormat.Trim.Length <= 0 Then iCurrFormat = GUI.fmtCurrency
    '                Dim tmpRow() As DataRow = dtFinal.Select("EmpId = '" & dRow.Item("employeeunkid") & "'")
    '                If tmpRow.Length > 0 Then
    '                    tmpRow(0).Item("Column" & iDay) &= dRow.Item("activitycode") & "/" & Format(CDec(dRow.Item("activity_value")), iCurrFormat) & "#10;"
    '                    If mDicUoM.ContainsKey(dRow.Item("measure")) Then
    '                        Dim decTotal As Decimal = tmpRow(0).Item("UoM_" & dRow.Item("measure"))
    '                        tmpRow(0).Item("UoM_" & dRow.Item("measure")) = Format(CDec(decTotal) + CDec(dRow.Item("activity_value")), iCurrFormat)
    '                    End If
    '                End If
    '                dtFinal.AcceptChanges()
    '            Next
    '        End If

    '        'S.SANDEEP [ 25 JULY 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'StrQ = "SELECT " & _
    '        '       "	 name " & _
    '        '       "	,ISNULL(iValue,0) AS iValue " & _
    '        '       "	,iDay " & _
    '        '       "FROM prunitmeasure_master " & _
    '        '       "LEFT JOIN " & _
    '        '       "( " & _
    '        '       "	SELECT " & _
    '        '       "	  practivity_master.measureunkid " & _
    '        '       "	 ,SUM(ISNULL(activity_value,0)) AS iValue " & _
    '        '       "	 ,DAY(activity_date) AS iDay " & _
    '        '       "	FROM prpayactivity_tran " & _
    '        '       "	 JOIN practivity_master ON prpayactivity_tran.activityunkid = practivity_master.activityunkid " & _
    '        '       "	 JOIN prunitmeasure_master ON practivity_master.measureunkid = prunitmeasure_master.measureunkid " & _
    '        '       "	WHERE isvoid = 0 GROUP BY practivity_master.measureunkid,DAY(activity_date) " & _
    '        '       ")AS UoM ON prunitmeasure_master.measureunkid = UoM.measureunkid " & _
    '        '       "WHERE prunitmeasure_master.isactive = 1 "
    '        StrQ = "SELECT " & _
    '               "	 name " & _
    '               "	,ISNULL(iValue,0) AS iValue " & _
    '               "	,ISNULL(iDay,0) AS iDay " & _
    '               "FROM prunitmeasure_master " & _
    '               "LEFT JOIN " & _
    '               "( " & _
    '               "	SELECT " & _
    '               "	  practivity_master.measureunkid " & _
    '               "	 ,SUM(ISNULL(activity_value,0)) AS iValue " & _
    '               "	 ,DAY(activity_date) AS iDay " & _
    '               "	FROM prpayactivity_tran " & _
    '               "	 JOIN practivity_master ON prpayactivity_tran.activityunkid = practivity_master.activityunkid " & _
    '               "	 JOIN prunitmeasure_master ON practivity_master.measureunkid = prunitmeasure_master.measureunkid "


    '        'Pinkal (06-Mar-2014) -- Start
    '        'Enhancement : TRA Changes

    '        If mintViewIndex > 0 Then
    '            StrQ &= " JOIN hremployee_master ON hremployee_master.employeeunkid = prpayactivity_tran.employeeunkid "
    '            StrQ &= mstrAnalysis_Join
    '        End If

    '        'Pinkal (06-Mar-2014) -- End

    '        StrQ &= "	WHERE isvoid = 0 AND CONVERT(CHAR(8),activity_date,112) BETWEEN '" & sDate1 & "' AND '" & sDate2 & "' " & _
    '               "    GROUP BY practivity_master.measureunkid,DAY(activity_date) " & _
    '               ")AS UoM ON prunitmeasure_master.measureunkid = UoM.measureunkid " & _
    '               "WHERE prunitmeasure_master.isactive = 1 "
    '        'S.SANDEEP [ 25 JULY 2013 ] -- END

    '        dtTemp = objDataOperation.ExecQuery(StrQ, "List").Tables(0)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dtTemp.Rows.Count > 0 Then
    '            For Each dRow As DataRow In dtTemp.Rows
    '                'S.SANDEEP [ 25 JULY 2013 ] -- START
    '                'ENHANCEMENT : TRA CHANGES
    '                'Dim tmpRow() As DataRow = dtFinal.Select("EName = '" & dRow.Item("name") & "'")
    '                'tmpRow(0).Item("Column" & dRow.Item("iDay")) = Format(CDec(dRow.Item("iValue")), iCurrFormat)
    '                If dRow.Item("iDay") > 0 Then
    '                    Dim tmpRow() As DataRow = dtFinal.Select("EName = '" & dRow.Item("name") & "'")
    '                    tmpRow(0).Item("Column" & dRow.Item("iDay")) = Format(CDec(dRow.Item("iValue")), iCurrFormat)
    '                End If
    '                'S.SANDEEP [ 25 JULY 2013 ] -- END
    '            Next
    '            dtFinal.AcceptChanges()
    '        End If



    '        'Pinkal (06-Mar-2014) -- Start
    '        'Enhancement : TRA Changes

    '        If mintViewIndex > 0 AndAlso dtFinal.Rows.Count > 0 Then
    '            dtFinal.Columns("Id").SetOrdinal(dtFinal.Columns.Count - 1)
    '            dtFinal.Columns("GName").SetOrdinal(dtFinal.Columns.Count - 1)
    '        End If

    '        dtFinal.Columns("ECode").Caption = "Employee Code"
    '        dtFinal.Columns("EName").Caption = "Employee Name"
    '        dtFinal.Columns("Dept").Caption = "Department"

    '        'Pinkal (06-Mar-2014) -- End


    '        Return dtFinal

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Export_PayPerActivity; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Function

    Public Function Export_PayPerActivity(ByVal sDate1 As String, ByVal sDate2 As String _
                                                            , ByVal sAllocation As String _
                                                            , ByVal xDatabaseName As String _
                                                            , ByVal xUserUnkid As Integer _
                                                            , ByVal xYearUnkid As Integer _
                                                            , ByVal xCompanyUnkid As Integer _
                                                            , ByVal mdtEmployeeAsonDate As Date _
                                                            , ByVal xUserModeSetting As String _
                                                            , ByVal xOnlyApproved As Boolean _
                                                            , Optional ByVal iUserAccess As String = "" _
                                                            , Optional ByVal iCurrFormat As String = "") As DataTable

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dtFinal As DataTable = Nothing
        Dim dtTemp As DataTable = Nothing
        Dim mDicUoM As New Dictionary(Of String, String)
        Try
            objDataOperation = New clsDataOperation


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsonDate, mdtEmployeeAsonDate, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtEmployeeAsonDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsonDate, xDatabaseName)


            StrQ = "SELECT " & _
                   "	 employeecode AS ECode " & _
                   "	,firstname+' '+ surname AS EName " & _
                      "	,hd.name AS Dept " & _
                      " ,hremployee_master.employeeunkid AS EmpId "

            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            End If

            StrQ &= "FROM hremployee_master " & _
                         "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         stationunkid " & _
                        "        ,deptgroupunkid " & _
                        "        ,departmentunkid " & _
                        "        ,sectiongroupunkid " & _
                        "        ,sectionunkid " & _
                        "        ,unitgroupunkid " & _
                        "        ,unitunkid " & _
                        "        ,teamunkid " & _
                        "        ,classgroupunkid " & _
                        "        ,classunkid " & _
                        "        ,employeeunkid " & _
                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "    FROM hremployee_transfer_tran " & _
                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                        "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                         "	JOIN hrdepartment_master hd ON Alloc.departmentunkid = hd.departmentunkid "

            StrQ &= mstrAnalysis_Join


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            'StrQ &= "WHERE isapproved = 1 " & _
            '       "	AND CONVERT(CHAR(8),appointeddate,112) <= @Date2 AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@Date1) >= @Date1 " & _
            '       "	AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@Date1) >= @Date1 AND ISNULL(CONVERT(CHAR(8),empl_enddate,112), @Date1) >= @Date1 " & _
            '       "	AND ISNULL(CONVERT(CHAR(8),reinstatement_date,112),@Date1) <= @Date1 "



            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry & " "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            If mstrAdvance_Filter.Trim.Length > 0 Then
                StrQ &= " AND " & mstrAdvance_Filter
            End If

            If sAllocation.Trim.Length > 0 Then
                StrQ &= sAllocation
            End If

            'If iUserAccess.Trim.Length <= 0 Then iUserAccess = UserAccessLevel._AccessLevelFilterString
            'StrQ &= iUserAccess

            StrQ &= " ORDER BY firstname+' '+ surname "

            'Nilay (25-May-2016) -- Start
            'objDataOperation.AddParameter("@Date1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, sDate1)
            'objDataOperation.AddParameter("@Date2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, sDate2)
            'Nilay (25-May-2016) -- End

            dtFinal = objDataOperation.ExecQuery(StrQ, "List").Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dCol As DataColumn
            Dim objARates As New clsActivity_Rates


            'Pinkal (21-Jun-2016) -- Start
            'Enhancement - Adding Close Period Boolean for Pay Per Activity Report.
            'dtTemp = objARates.Get_Date_Range(sDate1, sDate2)
            dtTemp = objARates.Get_Date_Range(sDate1, sDate2, False)
            'Pinkal (21-Jun-2016) -- End


            If dtTemp.Rows.Count > 0 Then
                For Each dRow As DataRow In dtTemp.Rows
                    'Nilay (25-May-2016) -- Start
                    'dCol = New DataColumn("Column" & dRow.Item("iDay"))
                    dCol = New DataColumn("Column" & dRow.Item("iDate"))
                    'Nilay (25-May-2016) -- End
                    dCol.DefaultValue = "" : dCol.Caption = dRow.Item("iName") & "#10;" & eZeeDate.convertDate(dRow.Item("iDate").ToString).ToShortDateString
                    dCol.DataType = System.Type.GetType("System.String")
                    dtFinal.Columns.Add(dCol)
                Next
            End If
            objARates = Nothing
            Dim objMeasure As New clsUnitMeasure_Master
            dtTemp = objMeasure.GetList("List", True).Tables(0)
            If dtTemp.Rows.Count > 0 Then
                For Each dRow As DataRow In dtTemp.Rows
                    dCol = New DataColumn("UoM_" & dRow.Item("name"))
                    dCol.DefaultValue = 0 : dCol.Caption = dRow.Item("name")
                    dCol.DataType = System.Type.GetType("System.Decimal")
                    dtFinal.Columns.Add(dCol)
                    If mDicUoM.ContainsKey(dRow.Item("name")) = False Then
                        mDicUoM.Add(dRow.Item("name"), "UoM_" & dRow.Item("name"))                        
                    End If
                Next
            End If

            dtFinal.Rows.Add("")

            For Each dRow As DataRow In dtTemp.Rows
                Dim tRow As DataRow = dtFinal.NewRow

                tRow.Item("ECode") = dRow.Item("code")
                tRow.Item("EName") = dRow.Item("name")
                tRow.Item("Dept") = ""
                tRow.Item("EmpId") = 0

                dtFinal.Rows.Add(tRow)
            Next

            objMeasure = Nothing

            'Sohail (05 Jun 2020) -- Start
            'Internal Issue # : Object referencce error on Pay Per Activity report.
            'dtTemp = GetList("List", , , , eZeeDate.convertDate(sDate1), eZeeDate.convertDate(sDate2), , True).Tables(0)
            dtTemp = GetList("List", , , , eZeeDate.convertDate(sDate1), eZeeDate.convertDate(sDate2), , True, , objDataOperation).Tables(0)
            'Sohail (05 Jun 2020) -- End
            Dim iDay As String = String.Empty
            If dtTemp.Rows.Count > 0 Then
                For Each dRow As DataRow In dtTemp.Rows
                    'Pinkal (21-Jun-2016) -- Start
                    'Enhancement - Adding Close Period Boolean for Pay Per Activity Report.
                    'iDay = CDate(dRow.Item("activity_date")).Day
                    iDay = eZeeDate.convertDate(CDate(dRow.Item("activity_date")).Date)
                    'Pinkal (21-Jun-2016) -- End
                    If iCurrFormat.Trim.Length <= 0 Then iCurrFormat = GUI.fmtCurrency
                    Dim tmpRow() As DataRow = dtFinal.Select("EmpId = '" & dRow.Item("employeeunkid") & "'")
                    If tmpRow.Length > 0 Then
                        tmpRow(0).Item("Column" & iDay) &= dRow.Item("activitycode") & "/" & Format(CDec(dRow.Item("activity_value")), iCurrFormat) & "#10;"
                        If mDicUoM.ContainsKey(dRow.Item("measure")) Then
                            Dim decTotal As Decimal = tmpRow(0).Item("UoM_" & dRow.Item("measure"))
                            tmpRow(0).Item("UoM_" & dRow.Item("measure")) = Format(CDec(decTotal) + CDec(dRow.Item("activity_value")), iCurrFormat)
                        End If
                    End If
                    dtFinal.AcceptChanges()
                Next
            End If


            StrQ = "SELECT " & _
                   "	 name " & _
                   "	,ISNULL(iValue,0) AS iValue " & _
                   "	,ISNULL(iDay,0) AS iDay " & _
                   "    ,ISNULL(iDate,'') AS iDate " & _
                   "FROM prunitmeasure_master " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "	SELECT " & _
                   "	  practivity_master.measureunkid " & _
                   "	 ,SUM(ISNULL(activity_value,0)) AS iValue " & _
                   "	 ,DAY(activity_date) AS iDay " & _
                   "	 ,CONVERT (CHAR(8),activity_date, 112) AS iDate " & _
                   "	FROM prpayactivity_tran " & _
                   "	 JOIN practivity_master ON prpayactivity_tran.activityunkid = practivity_master.activityunkid " & _
                   "	 JOIN prunitmeasure_master ON practivity_master.measureunkid = prunitmeasure_master.measureunkid "
            'Nilay (25-May-2016) -- [iDate]

            If mintViewIndex > 0 Then
                StrQ &= " JOIN hremployee_master ON hremployee_master.employeeunkid = prpayactivity_tran.employeeunkid "
                StrQ &= mstrAnalysis_Join
            End If


            StrQ &= "	WHERE isvoid = 0 AND CONVERT(CHAR(8),activity_date,112) BETWEEN '" & sDate1 & "' AND '" & sDate2 & "' " & _
                   "    GROUP BY practivity_master.measureunkid, activity_date " & _
                   ")AS UoM ON prunitmeasure_master.measureunkid = UoM.measureunkid " & _
                   "WHERE prunitmeasure_master.isactive = 1 "
            'Nilay (25-May-2016) -- Start
            'DAY(activity_date) REPLACED BY activity_date
            'Nilay (25-May-2016) -- End 

            dtTemp = objDataOperation.ExecQuery(StrQ, "List").Tables(0)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dtTemp.Rows.Count > 0 Then
                For Each dRow As DataRow In dtTemp.Rows
                    If dRow.Item("iDay") > 0 Then
                    Dim tmpRow() As DataRow = dtFinal.Select("EName = '" & dRow.Item("name") & "'")
                        'Nilay (25-May-2016) -- Start
                        'tmpRow(0).Item("Column" & dRow.Item("iDay")) = Format(CDec(dRow.Item("iValue")), iCurrFormat)
                        tmpRow(0).Item("Column" & dRow.Item("iDate")) = Format(CDec(dRow.Item("iValue")), iCurrFormat)
                        'Nilay (25-May-2016) -- End
                    End If
                Next
                dtFinal.AcceptChanges()
            End If

            If mintViewIndex > 0 AndAlso dtFinal.Rows.Count > 0 Then
                dtFinal.Columns("Id").SetOrdinal(dtFinal.Columns.Count - 1)
                dtFinal.Columns("GName").SetOrdinal(dtFinal.Columns.Count - 1)
            End If

            dtFinal.Columns("ECode").Caption = "Employee Code"
            dtFinal.Columns("EName").Caption = "Employee Name"
            dtFinal.Columns("Dept").Caption = "Department"

            Return dtFinal

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Export_PayPerActivity; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- End

   

'Sohail (25 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    Public Function GetUnitTotalMeasurementWise(ByVal intMeasurementUnkId As Integer _
                                             , Optional ByVal intPeriodUnkId As Integer = 0 _
                                             , Optional ByVal intEmployeeUnkId As Integer = 0 _
                                             , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                             ) As Decimal
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim decTotal As Decimal = 0

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT  ISNULL(SUM(prpayactivity_summary.sum_activity_value), 0) AS sum_activity_value " & _
                    "FROM    prpayactivity_summary " & _
                            "LEFT JOIN practivity_master ON prpayactivity_summary.activityunkid = practivity_master.activityunkid " & _
                    "WHERE   practivity_master.isactive = 1 " & _
                            "AND practivity_master.measureunkid = @measureunkid "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            objDataOperation.AddParameter("@measureunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMeasurementUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                decTotal = CDec(dsList.Tables("List").Rows(0).Item("sum_activity_value"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUnitTotalMeasurementWise; Module Name: " & mstrModuleName)
            Return Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return decTotal
    End Function

    Public Function GetUnitTotalActivityWise(ByVal intActivityUnkId As Integer _
                                             , Optional ByVal intPeriodUnkId As Integer = 0 _
                                             , Optional ByVal intEmployeeUnkId As Integer = 0 _
                                             , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                             ) As Decimal
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim decTotal As Decimal = 0

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT  ISNULL(SUM(prpayactivity_summary.sum_activity_value), 0) AS sum_activity_value " & _
                    "FROM    prpayactivity_summary " & _
                    "WHERE   1 = 1 " & _
                            "AND activityunkid = @activityunkid "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intActivityUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                decTotal = CDec(dsList.Tables("List").Rows(0).Item("sum_activity_value"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUnitTotalActivityWise; Module Name: " & mstrModuleName)
            Return Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return decTotal
    End Function

    Public Function GetAmountTotalMeasurementWise(ByVal intMeasurementUnkId As Integer _
                                                  , Optional ByVal intPeriodUnkId As Integer = 0 _
                                                  , Optional ByVal intEmployeeUnkId As Integer = 0 _
                                                  , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                                  ) As Decimal
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim decTotal As Decimal = 0

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT  ISNULL(SUM(prpayactivity_summary.sum_activity_amount), 0) AS sum_activity_amount " & _
                    "FROM    prpayactivity_summary " & _
                            "LEFT JOIN practivity_master ON prpayactivity_summary.activityunkid = practivity_master.activityunkid " & _
                    "WHERE   practivity_master.isactive = 1 " & _
                            "AND practivity_master.measureunkid = @measureunkid "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            objDataOperation.AddParameter("@measureunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMeasurementUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                decTotal = CDec(dsList.Tables("List").Rows(0).Item("sum_activity_amount"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAmountTotalMeasurementWise; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
        Return decTotal
    End Function

    Public Function GetAmountTotalActivityWise(ByVal intActivityUnkId As Integer _
                                               , Optional ByVal intPeriodUnkId As Integer = 0 _
                                               , Optional ByVal intEmployeeUnkId As Integer = 0 _
                                               , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                               ) As Decimal
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim decTotal As Decimal = 0

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT  ISNULL(SUM(prpayactivity_summary.sum_activity_amount), 0) AS sum_activity_amount " & _
                    "FROM    prpayactivity_summary " & _
                    "WHERE   1 = 1 " & _
                            "AND activityunkid = @activityunkid "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intActivityUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                decTotal = CDec(dsList.Tables("List").Rows(0).Item("sum_activity_amount"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAmountTotalActivityWise; Module Name: " & mstrModuleName)
            Return Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return decTotal
    End Function
    'Sohail (21 Jun 2013) -- End

    'Sohail (09 Sep 2014) -- Start
    'Enhancement - OT and PH segregate function for PPA in formula.
    Public Function GetTotalBaseUnitActivityWise(ByVal intActivityUnkId As Integer _
                                                 , Optional ByVal intPeriodUnkId As Integer = 0 _
                                                 , Optional ByVal intEmployeeUnkId As Integer = 0 _
                                                 , Optional ByVal blnOnlyPosted As Boolean = True _
                                                 , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                                 ) As Decimal
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim decTotal As Decimal = 0

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT  ISNULL(SUM(A.Unit), 0) AS TotalUnit " & _
                    "FROM    ( SELECT    ISNULL(activity_value, 0) AS Unit " & _
                              "FROM      prpayactivity_tran " & _
                              "WHERE     ISNULL(isvoid, 0) = 0 " & _
                                        "AND normal_factor = 0 " & _
                                        "AND ph_factor = 0 " & _
                                        "AND activityunkid = @activityunkid "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            If blnOnlyPosted = True Then
                strQ &= " AND isposted = 1 "
            End If

            strQ &= "          UNION ALL " & _
                              "SELECT    ISNULL(normal_hrs, 0) AS Unit " & _
                              "FROM      prpayactivity_tran " & _
                              "WHERE     ISNULL(isvoid, 0) = 0 " & _
                                        "AND normal_factor > 0 " & _
                                        "AND activityunkid = @activityunkid "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            If blnOnlyPosted = True Then
                strQ &= " AND isposted = 1 "
            End If

            strQ &= "        ) AS A "


            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intActivityUnkId)

            If intPeriodUnkId > 0 Then
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                decTotal = CDec(dsList.Tables("List").Rows(0).Item("TotalUnit"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalBaseUnitActivityWise; Module Name: " & mstrModuleName)
            Return Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return decTotal
    End Function

    Public Function GetTotalBaseAmountActivityWise(ByVal intActivityUnkId As Integer _
                                                   , Optional ByVal intPeriodUnkId As Integer = 0 _
                                                   , Optional ByVal intEmployeeUnkId As Integer = 0 _
                                                   , Optional ByVal blnOnlyPosted As Boolean = True _
                                                   , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                                   ) As Decimal
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim decTotal As Decimal = 0

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT  ISNULL(SUM(A.Amount), 0) AS TotalAmount " & _
                    "FROM    ( SELECT    ISNULL(amount, 0) AS Amount " & _
                              "FROM      prpayactivity_tran " & _
                              "WHERE     ISNULL(isvoid, 0) = 0 " & _
                                        "AND normal_factor = 0 " & _
                                        "AND ph_factor = 0 " & _
                                        "AND activityunkid = @activityunkid "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            If blnOnlyPosted = True Then
                strQ &= " AND isposted = 1 "
            End If

            strQ &= "          UNION ALL " & _
                              "SELECT    ISNULL(normal_amount, 0) AS Amount " & _
                              "FROM      prpayactivity_tran " & _
                              "WHERE     ISNULL(isvoid, 0) = 0 " & _
                                        "AND normal_factor > 0 " & _
                                        "AND activityunkid = @activityunkid "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            If blnOnlyPosted = True Then
                strQ &= " AND isposted = 1 "
            End If

            strQ &= "        ) AS A "


            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intActivityUnkId)

            If intPeriodUnkId > 0 Then
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                decTotal = CDec(dsList.Tables("List").Rows(0).Item("TotalAmount"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalBaseAmountActivityWise; Module Name: " & mstrModuleName)
            Return Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return decTotal
    End Function

    'Sohail (13 Sep 2014) -- Start
    'Enhancement - Make Activity selection Optional for OT and PH segregate function for PPA in formula.
    Public Function GetTotalBaseUnitHeadTypeWise(ByVal intHeadTypeID As Integer _
                                                 , Optional ByVal intPeriodUnkId As Integer = 0 _
                                                 , Optional ByVal intEmployeeUnkId As Integer = 0 _
                                                 , Optional ByVal blnOnlyPosted As Boolean = True _
                                                 , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                                 ) As Decimal
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim decTotal As Decimal = 0


        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT  ISNULL(SUM(A.Unit), 0) AS TotalUnit " & _
                    "FROM    ( SELECT    ISNULL(activity_value, 0) AS Unit " & _
                              "FROM      prpayactivity_tran " & _
                                        "LEFT JOIN practivity_master ON practivity_master.activityunkid = prpayactivity_tran.activityunkid " & _
                              "WHERE     ISNULL(isvoid, 0) = 0 " & _
                                        "AND normal_factor = 0 " & _
                                        "AND ph_factor = 0 " & _
                                        "AND trnheadtype_id = @trnheadtype_id "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            If blnOnlyPosted = True Then
                strQ &= " AND isposted = 1 "
            End If

            strQ &= "          UNION ALL " & _
                              "SELECT    ISNULL(normal_hrs, 0) AS Unit " & _
                              "FROM      prpayactivity_tran " & _
                                        "LEFT JOIN practivity_master ON practivity_master.activityunkid = prpayactivity_tran.activityunkid " & _
                              "WHERE     ISNULL(isvoid, 0) = 0 " & _
                                        "AND normal_factor > 0 " & _
                                        "AND trnheadtype_id = @trnheadtype_id "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            If blnOnlyPosted = True Then
                strQ &= " AND isposted = 1 "
            End If

            strQ &= "        ) AS A "


            objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intHeadTypeID)

            If intPeriodUnkId > 0 Then
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                decTotal = CDec(dsList.Tables("List").Rows(0).Item("TotalUnit"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalBaseUnitHeadTypeWise; Module Name: " & mstrModuleName)
            Return Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return decTotal
    End Function

    Public Function GetTotalBaseAmountHeadTypeWise(ByVal intHeadTypeID As Integer _
                                                   , Optional ByVal intPeriodUnkId As Integer = 0 _
                                                   , Optional ByVal intEmployeeUnkId As Integer = 0 _
                                                   , Optional ByVal blnOnlyPosted As Boolean = True _
                                                   , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                                   ) As Decimal
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim decTotal As Decimal = 0

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT  ISNULL(SUM(A.Amount), 0) AS TotalAmount " & _
                    "FROM    ( SELECT    ISNULL(amount, 0) AS Amount " & _
                              "FROM      prpayactivity_tran " & _
                                        "LEFT JOIN practivity_master ON practivity_master.activityunkid = prpayactivity_tran.activityunkid " & _
                              "WHERE     ISNULL(isvoid, 0) = 0 " & _
                                        "AND normal_factor = 0 " & _
                                        "AND ph_factor = 0 " & _
                                        "AND trnheadtype_id = @trnheadtype_id "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            If blnOnlyPosted = True Then
                strQ &= " AND isposted = 1 "
            End If

            strQ &= "          UNION ALL " & _
                              "SELECT    ISNULL(normal_amount, 0) AS Amount " & _
                              "FROM      prpayactivity_tran " & _
                                        "LEFT JOIN practivity_master ON practivity_master.activityunkid = prpayactivity_tran.activityunkid " & _
                              "WHERE     ISNULL(isvoid, 0) = 0 " & _
                                        "AND normal_factor > 0 " & _
                                        "AND trnheadtype_id = @trnheadtype_id "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            If blnOnlyPosted = True Then
                strQ &= " AND isposted = 1 "
            End If

            strQ &= "        ) AS A "


            objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intHeadTypeID)

            If intPeriodUnkId > 0 Then
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                decTotal = CDec(dsList.Tables("List").Rows(0).Item("TotalAmount"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalBaseAmountHeadTypeWise; Module Name: " & mstrModuleName)
            Return Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return decTotal
    End Function
    'Sohail (13 Sep 2014) -- End

    Public Function GetTotalOTUnitActivityWise(ByVal intActivityUnkId As Integer _
                                               , Optional ByVal intPeriodUnkId As Integer = 0 _
                                               , Optional ByVal intEmployeeUnkId As Integer = 0 _
                                               , Optional ByVal blnOnlyPosted As Boolean = True _
                                               , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                               ) As Decimal
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim decTotal As Decimal = 0

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT  ISNULL(SUM(ot_hrs), 0) AS TotalUnit " & _
                    "FROM    prpayactivity_tran " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND normal_factor > 0 " & _
                            "AND activityunkid = @activityunkid "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            If blnOnlyPosted = True Then
                strQ &= " AND isposted = 1 "
            End If

            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intActivityUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                decTotal = CDec(dsList.Tables("List").Rows(0).Item("TotalUnit"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalOTUnitActivityWise; Module Name: " & mstrModuleName)
            Return Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return decTotal
    End Function

    Public Function GetTotalOTAmountActivityWise(ByVal intActivityUnkId As Integer _
                                                 , Optional ByVal intPeriodUnkId As Integer = 0 _
                                                 , Optional ByVal intEmployeeUnkId As Integer = 0 _
                                                 , Optional ByVal blnOnlyPosted As Boolean = True _
                                                 , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                                 ) As Decimal
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim decTotal As Decimal = 0

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT  ISNULL(SUM(ot_amount), 0) AS TotalAmount " & _
                    "FROM    prpayactivity_tran " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND normal_factor > 0 " & _
                            "AND activityunkid = @activityunkid "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            If blnOnlyPosted = True Then
                strQ &= " AND isposted = 1 "
            End If

            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intActivityUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                decTotal = CDec(dsList.Tables("List").Rows(0).Item("TotalAmount"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalOTAmountActivityWise; Module Name: " & mstrModuleName)
            Return Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return decTotal
    End Function

    'Sohail (13 Sep 2014) -- Start
    'Enhancement - Make Activity selection Optional for OT and PH segregate function for PPA in formula.
    Public Function GetTotalOTUnitHeadTypeWise(ByVal intHeadTypeID As Integer _
                                               , Optional ByVal intPeriodUnkId As Integer = 0 _
                                               , Optional ByVal intEmployeeUnkId As Integer = 0 _
                                               , Optional ByVal blnOnlyPosted As Boolean = True _
                                               , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                               ) As Decimal
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim decTotal As Decimal = 0

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT  ISNULL(SUM(ot_hrs), 0) AS TotalUnit " & _
                    "FROM    prpayactivity_tran " & _
                            "LEFT JOIN practivity_master ON practivity_master.activityunkid = prpayactivity_tran.activityunkid " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND normal_factor > 0 " & _
                            "AND trnheadtype_id = @trnheadtype_id "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            If blnOnlyPosted = True Then
                strQ &= " AND isposted = 1 "
            End If

            objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intHeadTypeID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                decTotal = CDec(dsList.Tables("List").Rows(0).Item("TotalUnit"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalOTUnitHeadTypeWise; Module Name: " & mstrModuleName)
            Return Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return decTotal
    End Function

    Public Function GetTotalOTAmountHeadTypeWise(ByVal intHeadTypeID As Integer _
                                                 , Optional ByVal intPeriodUnkId As Integer = 0 _
                                                 , Optional ByVal intEmployeeUnkId As Integer = 0 _
                                                 , Optional ByVal blnOnlyPosted As Boolean = True _
                                                 , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                                 ) As Decimal
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim decTotal As Decimal = 0

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT  ISNULL(SUM(ot_amount), 0) AS TotalAmount " & _
                    "FROM    prpayactivity_tran " & _
                            "LEFT JOIN practivity_master ON practivity_master.activityunkid = prpayactivity_tran.activityunkid " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND normal_factor > 0 " & _
                            "AND trnheadtype_id = @trnheadtype_id "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            If blnOnlyPosted = True Then
                strQ &= " AND isposted = 1 "
            End If

            objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intHeadTypeID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                decTotal = CDec(dsList.Tables("List").Rows(0).Item("TotalAmount"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalOTAmountHeadTypeWise; Module Name: " & mstrModuleName)
            Return Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return decTotal
    End Function
    'Sohail (13 Sep 2014) -- End

    Public Function GetTotalPHUnitActivityWise(ByVal intActivityUnkId As Integer _
                                              , Optional ByVal intPeriodUnkId As Integer = 0 _
                                              , Optional ByVal intEmployeeUnkId As Integer = 0 _
                                              , Optional ByVal blnOnlyPosted As Boolean = True _
                                              , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                              ) As Decimal
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim decTotal As Decimal = 0

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT  ISNULL(SUM(ot_hrs), 0) AS TotalUnit " & _
                    "FROM    prpayactivity_tran " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND ph_factor > 0 " & _
                            "AND activityunkid = @activityunkid "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            If blnOnlyPosted = True Then
                strQ &= " AND isposted = 1 "
            End If

            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intActivityUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                decTotal = CDec(dsList.Tables("List").Rows(0).Item("TotalUnit"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalPHUnitActivityWise; Module Name: " & mstrModuleName)
            Return Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return decTotal
    End Function

    Public Function GetTotalPHAmountActivityWise(ByVal intActivityUnkId As Integer _
                                                 , Optional ByVal intPeriodUnkId As Integer = 0 _
                                                 , Optional ByVal intEmployeeUnkId As Integer = 0 _
                                                 , Optional ByVal blnOnlyPosted As Boolean = True _
                                                 , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                                 ) As Decimal
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim decTotal As Decimal = 0

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT  ISNULL(SUM(ot_amount), 0) AS TotalAmount " & _
                    "FROM    prpayactivity_tran " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND ph_factor > 0 " & _
                            "AND activityunkid = @activityunkid "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            If blnOnlyPosted = True Then
                strQ &= " AND isposted = 1 "
            End If

            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intActivityUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                decTotal = CDec(dsList.Tables("List").Rows(0).Item("TotalAmount"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalPHAmountActivityWise; Module Name: " & mstrModuleName)
            Return Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return decTotal
    End Function
    'Sohail (09 Sep 2014) -- End

    'Sohail (13 Sep 2014) -- Start
    'Enhancement - Make Activity selection Optional for OT and PH segregate function for PPA in formula.
    Public Function GetTotalPHUnitHeadTypeWise(ByVal intHeadTypeID As Integer _
                                              , Optional ByVal intPeriodUnkId As Integer = 0 _
                                              , Optional ByVal intEmployeeUnkId As Integer = 0 _
                                              , Optional ByVal blnOnlyPosted As Boolean = True _
                                              , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                              ) As Decimal
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim decTotal As Decimal = 0

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT  ISNULL(SUM(ot_hrs), 0) AS TotalUnit " & _
                    "FROM    prpayactivity_tran " & _
                            "LEFT JOIN practivity_master ON practivity_master.activityunkid = prpayactivity_tran.activityunkid " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND ph_factor > 0 " & _
                            "AND trnheadtype_id = @trnheadtype_id "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            If blnOnlyPosted = True Then
                strQ &= " AND isposted = 1 "
            End If

            objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intHeadTypeID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                decTotal = CDec(dsList.Tables("List").Rows(0).Item("TotalUnit"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalPHUnitHeadTypeWise; Module Name: " & mstrModuleName)
            Return Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return decTotal
    End Function

    Public Function GetTotalPHAmountHeadTypeWise(ByVal intHeadTypeID As Integer _
                                                 , Optional ByVal intPeriodUnkId As Integer = 0 _
                                                 , Optional ByVal intEmployeeUnkId As Integer = 0 _
                                                 , Optional ByVal blnOnlyPosted As Boolean = True _
                                                 , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                                 ) As Decimal
        'Sohail (03 May 2018) - [xDataOp]

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim decTotal As Decimal = 0

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT  ISNULL(SUM(ot_amount), 0) AS TotalAmount " & _
                    "FROM    prpayactivity_tran " & _
                            "LEFT JOIN practivity_master ON practivity_master.activityunkid = prpayactivity_tran.activityunkid " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND ph_factor > 0 " & _
                            "AND trnheadtype_id = @trnheadtype_id "

            If intPeriodUnkId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intEmployeeUnkId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            If blnOnlyPosted = True Then
                strQ &= " AND isposted = 1 "
            End If

            objDataOperation.AddParameter("@trnheadtype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intHeadTypeID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                decTotal = CDec(dsList.Tables("List").Rows(0).Item("TotalAmount"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTotalPHAmountHeadTypeWise; Module Name: " & mstrModuleName)
            Return Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return decTotal
    End Function
    'Sohail (13 Sep 2014) -- End



    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes


    '' <summary>
    '' VOID OPERATION ON prpayactivity_tran Table.
    '' </summary>
    '' <param name="IsPosting">1 = By Date, 2 = By Period</param>
    '' <param name="iPeriod">Get Total By Selected Period for All Employee</param>    
    '' <remarks></remarks>


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function VoidPosting_Activity(ByVal iPeriod As Integer, ByVal iUserId As Integer, ByVal intEmpID As Integer, ByVal intTranunkid As Integer, ByVal intActivityId As Integer) As Boolean
    Public Function VoidPosting_Activity(ByVal iPeriod As Integer, ByVal iUserId As Integer, ByVal intEmpID As Integer, ByVal intTranunkid As Integer, ByVal intActivityId As Integer, ByVal dtCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End


        Dim StrQ As String = String.Empty
        Dim uStrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            objDataOperation.ClearParameters()

            StrQ = "SELECT paysummunkid FROM prpayactivity_summary WHERE periodunkid = '" & iPeriod & "' "

            If intEmpID > 0 Then
                StrQ &= " AND employeeunkid = " & intEmpID & " "
            End If

            If intActivityId > 0 Then
                StrQ &= " AND activityunkid = " & intActivityId & " "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables("List").Rows

                StrQ = "DELETE FROM prpayactivity_summary WHERE paysummunkid = @paysummunkid "

                If Insert_AT_Summary_Log(objDataOperation, 3, iUserId, CInt(dRow.Item("paysummunkid"))) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@paysummunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dRow.Item("paysummunkid").ToString)
                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

            StrQ = "SELECT payactivitytranunkid FROM prpayactivity_tran WHERE isvoid = 0 AND isposted = 1 "
            If iPeriod > 0 Then
                StrQ &= " AND periodunkid = " & iPeriod
            End If

            If intEmpID > 0 Then
                StrQ &= " AND employeeunkid = " & intEmpID
            End If

            If intTranunkid > 0 Then
                StrQ &= " AND payactivitytranunkid = " & intTranunkid
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables("List").Rows
                _ObjData = objDataOperation
                _Payactivitytranunkid = CInt(dRow.Item("payactivitytranunkid"))
                _Isposted = False

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If Update(objDataOperation) = False Then
                If Update(dtCurrentDateAndTime, objDataOperation) = False Then
                    'Shani(24-Aug-2015) -- End

                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If Insert_AT_Log(objDataOperation, 2, iUserId) = False Then
                If Insert_AT_Log(objDataOperation, 2, iUserId, dtCurrentDateAndTime) = False Then
                    'Shani(24-Aug-2015) -- End

                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

  'Pinkal (01-Dec-2015) -- Start
            'Enhancement - Working on PPA Changes.

            StrQ = "SELECT " & _
                         "  periodunkid " & _
                         " ,employeeunkid " & _
                         " ,activityunkid " & _
                         " ,ISNULL(SUM(activity_value),0) AS sum_activity_value " & _
                         " ,ISNULL(SUM(activity_rate),0) AS sum_activity_rate " & _
                         " ,ISNULL(SUM(amount),0) AS sum_activity_amount " & _
                         "FROM prpayactivity_tran " & _
                         "WHERE isvoid = 0 AND isposted = 1 AND periodunkid = '" & iPeriod & "' "

            If intEmpID > 0 Then
                StrQ &= " AND employeeunkid = " & intEmpID & " "
            End If

            If intActivityId > 0 Then
                StrQ &= " AND activityunkid = " & intActivityId & " "
            End If

            StrQ &= " GROUP BY periodunkid,employeeunkid,activityunkid ORDER BY employeeunkid "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                StrQ = "INSERT INTO prpayactivity_summary ( " & _
                           "  periodunkid " & _
                           ", employeeunkid " & _
                           ", activityunkid " & _
                           ", sum_activity_value " & _
                           ", sum_activity_rate " & _
                           ", sum_activity_amount" & _
                       ") VALUES (" & _
                           "  @periodunkid " & _
                           ", @employeeunkid " & _
                           ", @activityunkid " & _
                           ", @sum_activity_value " & _
                           ", @sum_activity_rate " & _
                           ", @sum_activity_amount" & _
                       "); SELECT @@identity"

                For Each dRow As DataRow In dsList.Tables("List").Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriod.ToString)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dRow.Item("employeeunkid"))
                    objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dRow.Item("activityunkid"))
                    objDataOperation.AddParameter("@sum_activity_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dRow.Item("sum_activity_value"))
                    objDataOperation.AddParameter("@sum_activity_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dRow.Item("sum_activity_rate"))
                    objDataOperation.AddParameter("@sum_activity_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dRow.Item("sum_activity_amount"))
                    Dim dsData As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    Dim iPaySummUnkid As Integer = dsData.Tables(0).Rows(0).Item(0)

                    If Insert_AT_Summary_Log(objDataOperation, 1, iUserId, iPaySummUnkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            End If


            'Pinkal (01-Dec-2015) -- End

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoidPosting_Activity; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function


    '' <summary>
    '' VOID OPERATION ON prpayactivity_tran Table.
    '' </summary>
    '' <param name="IsPosting">1 = By Date, 2 = By Period</param>
    '' <param name="iPeriod">Get Total By Selected Period for All Employee</param>    
    '' <remarks></remarks>
    '' 

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function VoidUnPosted_Activity(ByVal iPeriod As Integer, ByVal iUserId As Integer, ByVal intEmpID As Integer, ByVal intTranunkid As Integer, ByVal intActivityId As Integer) As Boolean
    Public Function VoidUnPosted_Activity(ByVal iPeriod As Integer, ByVal iUserId As Integer, ByVal intEmpID As Integer, ByVal intTranunkid As Integer, ByVal intActivityId As Integer, ByVal dtCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End


        Dim StrQ As String = String.Empty
        Dim uStrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            objDataOperation.ClearParameters()

            StrQ = "SELECT payactivitytranunkid FROM prpayactivity_tran WHERE isvoid = 0 AND isposted = 0 "
            If iPeriod > 0 Then
                StrQ &= " AND periodunkid = " & iPeriod
            End If

            If intEmpID > 0 Then
                StrQ &= " AND employeeunkid = " & intEmpID
            End If

            If intTranunkid > 0 Then
                StrQ &= " AND payactivitytranunkid = " & intTranunkid
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables("List").Rows
                _Isvoid = True

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                '_Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                _Voiddatetime = dtCurrentDateAndTime
                'Shani(24-Aug-2015) -- End

                _Voiduserunkid = iUserId
                _Voidreason = ""

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If Delete(CInt(dRow.Item("payactivitytranunkid")), dtCurrentDateAndTime, objDataOperation) = False Then
                    'Shani(24-Aug-2015) -- End

                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoidUnPosted_Activity; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'Pinkal (06-Mar-2014) -- End


    'Pinkal (4-Sep-2014) -- Start
    'Enhancement - PAY_A CHANGES IN PPA

    'S.SANDEEP [ 28 AUG 2014 ] -- START
    'Public Function ReProcess_Posted_Activity(ByVal iDataSet As DataSet, ByVal iUserId As Integer) As Boolean
    '    Try
    '        For Each xRow As DataRow In iDataSet.Tables(0).Rows
    '            If xRow.Item("ph_factor") <= 0 Then
    '                Dim objEHoliday As New clsemployee_holiday
    '                If CType(objEHoliday.GetEmployeeHoliday(CInt(xRow.Item("employeeunkid")), eZeeDate.convertDate(xRow.Item("ADate").ToString).Date), DataTable).Rows.Count > 0 Then
    '                    Dim objActivity As New clsActivity_Master
    '                    objActivity._Activityunkid = xRow.Item("activityunkid")
    '                    If objActivity._Ph_Otfactor > 0 Then
    '                        Dim mdecFinalAmt As Decimal = 0
    '                        mdecFinalAmt = (xRow.Item("activity_rate") * objActivity._Ph_Otfactor) * xRow.Item("activity_value")
    '                        _Payactivitytranunkid = xRow("payactivitytranunkid")
    '                        _Normal_Factor = 0 : _PH_Factor = objActivity._Ph_Otfactor
    '                        _Normal_Hrs = 0 : _Ot_Hrs = xRow.Item("activity_value")
    '                        _Normal_Amount = 0 : _Ot_Amount = mdecFinalAmt
    '                        _Amount = mdecFinalAmt
    '                        If Update(, iUserId) = False Then
    '                            Return False
    '                        End If
    '                    End If
    '                    objActivity = Nothing
    '                End If
    '                objEHoliday = Nothing
    '            End If
    '        Next
    '        Return True
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "ReProcess_Posted_Activity", mstrModuleName)
    '    Finally
    '    End Try
    'End Function
    'S.SANDEEP [ 28 AUG 2014 ] -- END


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function ReProcess_Posted_Activity(ByVal drRow As DataRow, ByVal iUserId As Integer) As Boolean
    Public Function ReProcess_Posted_Activity(ByVal drRow As DataRow, ByVal iUserId As Integer, ByVal dtCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End

        Try
            Dim mDblSftHrs As Double = 0
            Dim mblnIsWeekEnd As Boolean = False
            Dim mblnIsHoliday As Boolean = False
            Dim objActTran As New clsPayActivity_Tran
            Dim objEmpShift As New clsEmployee_Shift_Tran
                    Dim objEHoliday As New clsemployee_holiday
            Dim mintShiftUnkid As Integer = 0


            'Pinkal (23-Sep-2014) -- Start
            'Enhancement -  PAY_A CHANGES IN PPA
            Dim mblnIsOtActivity As Boolean = False
            'Pinkal (23-Sep-2014) -- End


            Dim days() As String = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.DayNames
            Dim mDicWeekName As New Dictionary(Of String, Integer)
            For i As Integer = 0 To 6
                mDicWeekName.Add(days(i).ToString, i)
            Next

            mintShiftUnkid = objEmpShift.GetEmployee_Current_ShiftId(eZeeDate.convertDate(drRow.Item("ADate").ToString).Date, CInt(drRow.Item("employeeunkid")))
            mDblSftHrs = objActTran.Get_Sft_WorkHrs(mDicWeekName, mintShiftUnkid, eZeeDate.convertDate(drRow.Item("ADate").ToString).Date, mblnIsWeekEnd)

                If CType(objEHoliday.GetEmployeeHoliday(CInt(drRow.Item("employeeunkid")), eZeeDate.convertDate(drRow.Item("ADate").ToString).Date), DataTable).Rows.Count > 0 Then
                mblnIsHoliday = True
            Else
                mblnIsHoliday = False
            End If

            Dim dLs As DataSet = objActTran.Get_Activity_Rate(drRow.Item("activityunkid"), eZeeDate.convertDate(drRow.Item("ADate").ToString).Date)

            If dLs.Tables(0).Rows.Count > 0 Then
                Dim dRow() As DataRow = Nothing
                            Dim mdecFinalAmt As Decimal = 0

                'Pinkal (23-Sep-2014) -- Start
                'Enhancement -  PAY_A CHANGES IN PPA
                mblnIsOtActivity = CBool(dLs.Tables(0).Rows(0)("is_ot_activity"))
                'Pinkal (23-Sep-2014) -- End

                _Payactivitytranunkid = CInt(drRow.Item("payactivitytranunkid"))

                dRow = dLs.Tables(0).Select("modeid = " & enPPAMode.RATE)
                If dRow.Length > 0 Then
                    _Activity_Rate = CDec(dRow(0)("activity_rate"))
                    _Tranheadunkid = CInt(dRow(0)("tranheadunkid"))
                    _OT_Tranheadunkid = 0
                    _PH_Tranheadunkid = 0
                End If
                If mblnIsWeekEnd = False AndAlso mblnIsHoliday = False Then
                    dRow = dLs.Tables(0).Select("modeid = " & enPPAMode.OT)
                    If dRow.Length > 0 Then
                        _Normal_Factor = CDec(dRow(0)("activity_rate"))
                        _OT_Tranheadunkid = CInt(dRow(0)("tranheadunkid"))
                        _PH_Tranheadunkid = 0
                    End If
                ElseIf mblnIsWeekEnd OrElse mblnIsHoliday Then
                    dRow = dLs.Tables(0).Select("modeid = " & enPPAMode.PH)
                    If dRow.Length > 0 Then
                        _PH_Factor = CDec(dRow(0)("activity_rate"))
                        _PH_Tranheadunkid = CInt(dRow(0)("tranheadunkid"))
                        _OT_Tranheadunkid = 0
                    End If
                End If


                'Pinkal (23-Sep-2014) -- Start
                'Enhancement -  PAY_A CHANGES IN PPA


                If _Isformula_Based = False Then
                    If _Activity_Rate > 0 Then
                        If mblnIsWeekEnd = True Or mblnIsHoliday = True Then
                            If mblnIsOtActivity Then
                            _Ot_Hrs = drRow.Item("activity_value")
                            _Amount = ((_Activity_Rate * _PH_Factor) * drRow.Item("activity_value"))
                            _Ot_Amount = _Amount
                            _Normal_Amount = 0
                        Else
                                _Normal_Hrs = CDec(mDblSftHrs)
                                _Normal_Amount = 0
                                _Ot_Hrs = 0
                                _Ot_Amount = 0
                                _PH_Factor = 0
                                _Normal_Factor = 0
                                _Amount = _Activity_Rate * CDec(drRow.Item("activity_value"))
                            End If
                        Else
                            If mDblSftHrs > 0 Then
                                If mblnIsOtActivity Then
                                _Ot_Hrs = CDec(drRow.Item("activity_value")) - mDblSftHrs
                                If _Ot_Hrs <= 0 Then
                                    _Amount = _Activity_Rate * CDec(drRow.Item("activity_value"))
                                    _Normal_Amount = _Amount
                                    _Ot_Amount = 0
                                    _Ot_Hrs = 0
                                Else
                                    _Normal_Hrs = mDblSftHrs
                                    _Normal_Amount = (CDec(mDblSftHrs * _Activity_Rate))
                                    _Ot_Amount = CDec(_Ot_Hrs * (_Activity_Rate * _Normal_Factor))
                                    _Amount = _Normal_Amount + _Ot_Amount
                                End If
                            Else
                                    _Normal_Hrs = CDec(mDblSftHrs)
                                    _Normal_Amount = 0
                                    _Ot_Hrs = 0
                                    _Ot_Amount = 0
                                    _PH_Factor = 0
                                    _Normal_Factor = 0
                                _Amount = _Activity_Rate * CDec(drRow.Item("activity_value"))
                            End If
                            Else
                                _Amount = _Activity_Rate * CDec(drRow.Item("activity_value"))
                            End If

                        End If

                    End If

                Else
                    _Activity_Rate = 0
                    _Amount = 0
                    _Ot_Amount = 0
                    _Normal_Amount = 0
                    If mblnIsOtActivity Then
                    If mblnIsWeekEnd = True Or mblnIsHoliday = True Then
                        _Ot_Hrs = drRow.Item("activity_value")
                    Else
                        If mDblSftHrs > 0 Then
                            _Ot_Hrs = CDec(drRow.Item("activity_value")) - mDblSftHrs
                            If _Ot_Hrs <= 0 Then
                                _Ot_Hrs = 0
                                _Normal_Hrs = mDblSftHrs
                                    _PH_Tranheadunkid = 0
                                    _OT_Tranheadunkid = 0
                            Else
                                _Normal_Hrs = mDblSftHrs
                            End If
                        Else
                            _Normal_Hrs = mDblSftHrs
                        End If
                    End If
                    Else
                        _PH_Tranheadunkid = 0
                        _OT_Tranheadunkid = 0
                        _Ot_Hrs = 0
                        _PH_Factor = 0
                        _Normal_Factor = 0
                    End If

                End If

                'Pinkal (23-Sep-2014) -- End


                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If Update(, iUserId) = False Then
                If Update(dtCurrentDateAndTime, , iUserId) = False Then
                    'Shani(24-Aug-2015) -- End

                                Return False
                            End If

                        End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ReProcess_Posted_Activity", mstrModuleName)
        Finally
        End Try
    End Function

    'Pinkal (4-Sep-2014) -- End


    'S.SANDEEP [ 17 OCT 2014 ] -- START
    ''' <summary>
    ''' REMOVE DATA FROM AUDITTRAIL TABLES OF PAY PER ACTIVTY
    ''' </summary>
    ''' <param name="intDeleteFromId">0 = POSTED ACTIVITY, 1 = POSTED RATES</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function RemoveDataFrom_AuditTrails(ByVal intDeleteFromId As Integer, ByVal dtDate1 As Date, ByVal dtDate2 As Date) As Boolean
        Dim StrQ As String = String.Empty
        Dim objDataOpr As New clsDataOperation
        Try
            objDataOpr.BindTransaction()
            Select Case intDeleteFromId
                Case 0  'POSTED ACTIVITY
                    StrQ = "DELETE FROM atprpayactivity_tran WHERE CONVERT(CHAR(8),auditdatetime,112) BETWEEN '" & eZeeDate.convertDate(dtDate1) & "' AND '" & eZeeDate.convertDate(dtDate2) & "';" & _
                           "DELETE FROM atprpayactivity_summary WHERE CONVERT(CHAR(8),auditdatetime,112) BETWEEN '" & eZeeDate.convertDate(dtDate1) & "' AND '" & eZeeDate.convertDate(dtDate2) & "' "
                Case 1  'POSTED RATES
                    StrQ = "DELETE FROM atpractivityrate_master WHERE CONVERT(CHAR(8),auditdatetime,112) BETWEEN '" & eZeeDate.convertDate(dtDate1) & "' AND '" & eZeeDate.convertDate(dtDate2) & "'"
            End Select
            objDataOpr.ExecNonQuery(StrQ)
            If objDataOpr.ErrorMessage <> "" Then
                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
            End If

            objDataOpr.ReleaseTransaction(True)

        Catch ex As Exception
            objDataOpr.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "RemoveDataFrom_AuditTrails", mstrModuleName)
        Finally
        End Try
        Return True
    End Function
    'S.SANDEEP [ 17 OCT 2014 ] -- END


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
