﻿'************************************************************************************************************************************
'Class Name : clsUnitMeasure_Master.vb
'Purpose    :
'Date       :14-Jun-2013
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clsUnitMeasure_Master
    Private Shared ReadOnly mstrModuleName As String = "clsUnitMeasure_Master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintMeasureunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
    Private mblnIsactive As Boolean = True
    'Hemant (26-07-2018)  -- Start
    'Private mchrIsdeleted As Char
    Private mchrIsdeleted As Char = "0"
    'Hemant (26-07-2018)  -- End
    Private mblnIsformula_Based As Boolean

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set measureunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Measureunkid() As Integer
        Get
            Return mintMeasureunkid
        End Get
        Set(ByVal value As Integer)
            mintMeasureunkid = value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isdeleted
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isdeleted() As Char
        Get
            Return mchrIsdeleted
        End Get
        Set(ByVal value As Char)
            mchrIsdeleted = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isformula_based
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isformula_Based() As Boolean
        Get
            Return mblnIsformula_Based
        End Get
        Set(ByVal value As Boolean)
            mblnIsformula_Based = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  measureunkid " & _
                      ", code " & _
                      ", name " & _
                      ", description " & _
                      ", name1 " & _
                      ", name2 " & _
                      ", isformula_based " & _
                      ", isactive " & _
                      ", isdeleted " & _
                    "FROM prunitmeasure_master " & _
                    "WHERE measureunkid = @measureunkid "

            objDataOperation.AddParameter("@measureunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintMeasureUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintmeasureunkid = CInt(dtRow.Item("measureunkid"))
                mstrcode = dtRow.Item("code").ToString
                mstrname = dtRow.Item("name").ToString
                mstrdescription = dtRow.Item("description").ToString
                mstrname1 = dtRow.Item("name1").ToString
                mstrname2 = dtRow.Item("name2").ToString
                mblnisactive = CBool(dtRow.Item("isactive"))
                mchrIsdeleted = dtRow.Item("isdeleted")
                mblnIsformula_Based = CBool(dtRow.Item("isformula_based"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                        "  measureunkid " & _
                        ", code " & _
                        ", name " & _
                        ", description " & _
                        ", name1 " & _
                        ", name2 " & _
                        ", isformula_based " & _
                        ", isactive " & _
                        ", isdeleted " & _
                        ", CASE WHEN isformula_based = 1 THEN @FB ELSE @NF END AS uom " & _
                     "FROM prunitmeasure_master "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            objDataOperation.AddParameter("@FB", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Formula Based"))
            objDataOperation.AddParameter("@NF", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Non Formula"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prunitmeasure_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCode, "") Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
            Return False
        End If

        If isExist(, mstrName) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname2.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@isdeleted", SqlDbType.Char, eZeeDataType.BIT_SIZE, mchrIsdeleted.ToString)
            objDataOperation.AddParameter("@isformula_based", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsformula_Based.ToString)

            strQ = "INSERT INTO prunitmeasure_master ( " & _
                         "  code " & _
                         ", name " & _
                         ", description " & _
                         ", name1 " & _
                         ", name2 " & _
                         ", isformula_based " & _
                         ", isactive " & _
                         ", isdeleted" & _
                     ") VALUES (" & _
                         "  @code " & _
                         ", @name " & _
                         ", @description " & _
                         ", @name1 " & _
                         ", @name2 " & _
                         ", @isformula_based " & _
                         ", @isactive " & _
                         ", @isdeleted" & _
                     "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintMeasureunkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "prunitmeasure_master", "measureunkid", mintMeasureunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prunitmeasure_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrCode, "", mintMeasureunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
            Return False
        End If

        If isExist(, mstrName, mintMeasureunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@measureunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintmeasureunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname2.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@isdeleted", SqlDbType.Char, eZeeDataType.BIT_SIZE, mchrIsdeleted.ToString)
            objDataOperation.AddParameter("@isformula_based", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsformula_Based.ToString)

            strQ = "UPDATE prunitmeasure_master SET " & _
                        "  code = @code" & _
                        ", name = @name" & _
                        ", description = @description" & _
                        ", name1 = @name1" & _
                        ", name2 = @name2" & _
                        ", isformula_based = @isformula_based " & _
                        ", isactive = @isactive" & _
                        ", isdeleted = @isdeleted " & _
                    "WHERE measureunkid = @measureunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "prunitmeasure_master", mintMeasureunkid, "measureunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "prunitmeasure_master", "measureunkid", mintMeasureunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prunitmeasure_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        mstrMessage = ""
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, You cannot delete this Measurement. Reason : This Measurement is already linked with some of the activity.", )
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE prunitmeasure_master SET isactive = 0 " & _
            "WHERE measureunkid = @measureunkid "

            objDataOperation.AddParameter("@measureunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "prunitmeasure_master", "measureunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT 1 FROM practivity_master WHERE measureunkid = '" & intUnkid & "'  AND isactive = 1"

            objDataOperation.AddParameter("@measureunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1, Optional ByRef Ret_intUnkId As Integer = 0) As Boolean
        'Sohail (30 Jun 2017) - [Ret_intUnkId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Ret_intUnkId = 0 'Sohail (30 Jun 2017)

        Try
            strQ = "SELECT " & _
                         "  measureunkid " & _
                         ", code " & _
                         ", name " & _
                         ", description " & _
                         ", name1 " & _
                         ", name2 " & _
                         ", isactive " & _
                         ", isdeleted " & _
                     "FROM prunitmeasure_master " & _
                     "WHERE isactive = 1 "

            If strCode.Trim.Length > 0 Then
                strQ &= " AND code = @code "
            End If

            If strName.Trim.Length > 0 Then
                strQ &= " AND name = @name "
            End If

            If intUnkid > 0 Then
                strQ &= " AND measureunkid <> @measureunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@measureunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (11-Feb-2015) -- Start
            'Enhancement - Included NEW ACTIVITY MASTER IMPORTATION.
            If dsList.Tables(0).Rows.Count > 0 Then
                mintMeasureunkid = CInt(dsList.Tables(0).Rows(0)("measureunkid"))
                'Sohail (30 Jun 2017) -- Start
                'Issue - not able to edit measurement.
                'Else
                '    mintMeasureunkid = 0
                Ret_intUnkId = CInt(dsList.Tables(0).Rows(0)("measureunkid"))
                'Sohail (30 Jun 2017) -- End
            End If
            'Pinkal (11-Feb-2015) -- End

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <param name="StrLst">Name Of the List</param>
    ''' <param name="blnFlag">Include the Select in the List</param>
    ''' <param name="iUoMTypeId">0 = All Measure, 1 = Non Formula Based, 2 = Formula Based</param>
    ''' <purpose>Get List Of Data For Combo</purpose>
    Public Function getComboList(ByVal StrLst As String, Optional ByVal blnFlag As Boolean = False, Optional ByVal iUoMTypeId As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            If blnFlag = True Then
                strQ = "SELECT ' ' AS code, @Select AS name, 0 AS measureunkid UNION "
            End If
            strQ &= "SELECT code, name, measureunkid FROM prunitmeasure_master WHERE isactive = 1 "

            Select Case iUoMTypeId
                Case 1 'Non Formula Based
                    strQ &= " AND isformula_based = 0 "
                Case 2 'Formula Based
                    strQ &= " AND isformula_based = 1 "
            End Select

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, StrLst)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (21 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    Public Function Is_FormulaBased(ByVal intMeasureUnkId As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Sohail (03 May 2018) - [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try

            strQ = "SELECT  ISNULL(isformula_based, 0) AS isformula_based " & _
                    "FROM    prunitmeasure_master " & _
                    "WHERE   prunitmeasure_master.measureunkid = @measureunkid "

            objDataOperation.AddParameter("@measureunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMeasureUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CBool(dsList.Tables(0).Rows(0).Item("isformula_based"))
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Is_FormulaBased; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
    End Function
    'Sohail (21 Jun 2013) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
			Language.setMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "Sorry, You cannot delete this Measurement. Reason : This Measurement is already linked with some of the activity.")
			Language.setMessage(mstrModuleName, 5, "Formula Based")
			Language.setMessage(mstrModuleName, 6, "Non Formula")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
