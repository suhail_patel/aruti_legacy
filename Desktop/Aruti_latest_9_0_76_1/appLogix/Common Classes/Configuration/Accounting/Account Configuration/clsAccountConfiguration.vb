﻿'************************************************************************************************************************************
'Class Name : clsAccountConfiguration.vb
'Purpose    :
'Date       :31/08/2010
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsAccountConfiguration
    Private Shared ReadOnly mstrModuleName As String = "clsAccountConfiguration"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAccountconfigunkid As Integer
    Private mintTranheadunkid As Integer
    Private mintAccountunkid As Integer
    Private mintAccountGroupunkid As Integer
    Private mblnIsactive As Boolean = True
    Private mintTransactiontype_Id As Integer
    'Sohail (14 Nov 2011) -- Start
    Private mintReferencecodeid As Integer
    Private mintReferencenameid As Integer
    Private mintReferencetypeid As Integer = -1
    'Sohail (14 Nov 2011) -- End
    Private mstrShortname As String = String.Empty 'Sohail (13 Mar 2013)
    Private mstrShortname2 As String = String.Empty 'Sohail (08 Jul 2017)
    Private mstrShortname3 As String = String.Empty 'Sohail (03 Mar 2020)


    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Private mblnIsInactive As Boolean = False
    'Gajanan [24-Aug-2020] -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accountconfigunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Accountconfigunkid() As Integer
        Get
            Return mintAccountconfigunkid
        End Get
        Set(ByVal value As Integer)
            mintAccountconfigunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accountunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Accountunkid() As Integer
        Get
            Return mintAccountunkid
        End Get
        Set(ByVal value As Integer)
            mintAccountunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Transactiontype_Id
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Transactiontype_Id() As Integer
        Get
            Return mintTransactiontype_Id
        End Get
        Set(ByVal value As Integer)
            mintTransactiontype_Id = value
        End Set
    End Property

    Public ReadOnly Property _AccountGroupunkid() As Integer
        Get
            Return mintAccountGroupunkid
        End Get
    End Property

    'Sohail (14 Nov 2011) -- Start
    ''' <summary>
    ''' Purpose: Get or Set referencecodeid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Referencecodeid() As Integer
        Get
            Return mintReferencecodeid
        End Get
        Set(ByVal value As Integer)
            mintReferencecodeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set referencenameid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Referencenameid() As Integer
        Get
            Return mintReferencenameid
        End Get
        Set(ByVal value As Integer)
            mintReferencenameid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set referencetypeid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Referencetypeid() As Integer
        Get
            Return mintReferencetypeid
        End Get
        Set(ByVal value As Integer)
            mintReferencetypeid = value
        End Set
    End Property
    'Sohail (14 Nov 2011) -- End

    'Sohail (13 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set shortname
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Shortname() As String
        Get
            Return mstrShortname
        End Get
        Set(ByVal value As String)
            mstrShortname = value
        End Set
    End Property
    'Sohail (13 Mar 2013) -- End

    'Sohail (08 Jul 2017) -- Start
    'TANAPA Enhancement - 68.1 - New JV Integration with Dynamics Nav.
    Public Property _Shortname2() As String
        Get
            Return mstrShortname2
        End Get
        Set(ByVal value As String)
            mstrShortname2 = value
        End Set
    End Property
    'Sohail (08 Jul 2017) -- End

    'Sohail (03 Mar 2020) -- Start
    'NMB Enhancement # : Need another option like short name on account configuration screens to set keywords to get concern value on JV.
    Public Property _Shortname3() As String
        Get
            Return mstrShortname3
        End Get
        Set(ByVal value As String)
            mstrShortname3 = value
        End Set
    End Property
    'Sohail (03 Mar 2020) -- End

    'Sohail (03 Jul 2020) -- Start
    'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
    Private mintPeriodunkid As Integer
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property
    'Sohail (03 Jul 2020) -- End


    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Public Property _IsInactive() As Boolean
        Get
            Return mblnIsInactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsInactive = value
        End Set
    End Property
    'Gajanan [24-Aug-2020] -- End

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  praccount_configuration.accountconfigunkid " & _
                      ", praccount_configuration.tranheadunkid " & _
                      ", praccount_configuration.accountunkid " & _
                      ", praccount_master.accountgroup_id " & _
                      ", praccount_configuration.isactive " & _
                      ", praccount_configuration.Transactiontype_Id " & _
                      ", praccount_configuration.referencecodeid " & _
                      ", praccount_configuration.referencenameid " & _
                      ", praccount_configuration.referencetypeid " & _
                      ", ISNULL(praccount_configuration.shortname, '') AS shortname " & _
                      ", ISNULL(praccount_configuration.shortname2, '') AS shortname2 " & _
                      ", ISNULL(praccount_configuration.shortname3, '') AS shortname3 " & _
                      ", ISNULL(praccount_configuration.periodunkid, 0) AS periodunkid " & _
                      ", ISNULL(praccount_configuration.isinactive, 0) AS isinactive " & _
                 "FROM praccount_configuration " & _
                 "LEFT JOIN praccount_master ON praccount_configuration.accountunkid = praccount_master.accountunkid " & _
                 "WHERE praccount_configuration.accountconfigunkid = @accountconfigunkid "
            'Sohail (03 Jul 2020) - [periodunkid]
            'Sohail (03 Mar 2020) - [shortname3]
            'Sohail (14 Nov 2011) - [referencecodeid, referencenameid, referencetypeid]
            '    'Sohail (08 Jul 2017) - [shortname2]
            '    'Sohail (13 Mar 2013) - [shortname]
            'Gajanan [24-Aug-2020] -- [isinactive]


            objDataOperation.AddParameter("@accountconfigunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintAccountconfigUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintaccountconfigunkid = CInt(dtRow.Item("accountconfigunkid"))
                minttranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                mintAccountunkid = CInt(dtRow.Item("accountunkid"))
                mintAccountGroupunkid = CInt(dtRow.Item("accountgroup_id"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mintTransactiontype_Id = CInt(dtRow.Item("Transactiontype_Id"))
                'Sohail (14 Nov 2011) -- Start
                mintReferencecodeid = CInt(dtRow.Item("referencecodeid"))
                mintReferencenameid = CInt(dtRow.Item("referencenameid"))
                mintReferencetypeid = CInt(dtRow.Item("referencetypeid"))
                'Sohail (14 Nov 2011) -- End
                mstrShortname = dtRow.Item("shortname").ToString 'Sohail (13 Mar 2013)
                mstrShortname2 = dtRow.Item("shortname2").ToString 'Sohail (08 Jul 2017)
                mstrShortname3 = dtRow.Item("shortname3").ToString 'Sohail (03 Mar 2020)
                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                'Sohail (03 Jul 2020) -- End


                'Gajanan [24-Aug-2020] -- Start
                'NMB Enhancement : Allow to set account configuration mapping 
                'inactive for closed period to allow to map head on other account 
                'configuration screen from new period
                mblnIsInactive = CBool(dtRow.Item("isinactive"))
                'Gajanan [24-Aug-2020] -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                            , Optional ByVal blnOnlyActive As Boolean = True _
                            , Optional ByVal intTransactionTypeID As Integer = 0 _
                            , Optional ByVal intTranHeadID As Integer = 0 _
                            , Optional ByVal intAccountID As Integer = 0 _
                            , Optional ByVal intAccountGroupID As Integer = 0 _
                            , Optional ByVal dtAsOnDate As Date = Nothing _
                            , Optional ByVal strFilterOuter As String = "" _
                            , Optional ByVal mintOnlyInActive As Integer = enTranHeadActiveInActive.ACTIVE _
                            ) As DataSet
        'Sohail (03 Jul 2020) - [dtAsOnDate, strFilterOuter]
        'Gajanan [24-Aug-2020] -- [mintOnlyInActive]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            'Sohail (03 Jul 2020) -- End

            'Sohail (06 Aug 2016) -- Start
            'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
            Dim objMaster As New clsMasterData
            Dim ds As DataSet = objMaster.getComboListJVTransactionType("TranType")
            Dim dicTranType As Dictionary(Of Integer, String) = (From p In ds.Tables("TranType") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            'Sohail (06 Aug 2016) -- End

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            strQ = "SELECT * FROM ( "
            'Sohail (03 Jul 2020) -- End

            'Sohail (14 Nov 2011) -- Start
            'Changes : Loan Schme name and Saving Scheme name added.
            'strQ = "SELECT  praccount_configuration.accountconfigunkid , " & _
            '                "praccount_configuration.tranheadunkid , " & _
            '                "prtranhead_master.trnheadname , " & _
            '                "praccount_configuration.accountunkid , " & _
            '                "praccount_master.account_code , " & _
            '                "praccount_master.account_name , " & _
            '                "praccount_master.accountgroup_id , " & _
            '                "praccount_configuration.isactive , " & _
            '                "praccount_configuration.transactiontype_Id " & _
            '        "FROM    praccount_configuration " & _
            '                "LEFT JOIN prtranhead_master ON praccount_configuration.tranheadunkid = prtranhead_master.tranheadunkid " & _
            '                "LEFT JOIN praccount_master ON praccount_configuration.accountunkid = praccount_master.accountunkid " & _
            '        "WHERE   1 = 1 " 'Sohail (08 Nov 2011) - [account_code]
            strQ &= "SELECT  praccount_configuration.accountconfigunkid " & _
                          ", praccount_configuration.tranheadunkid " & _
                          ", ISNULL(CASE praccount_configuration.transactiontype_Id " & _
                                 "WHEN " & enJVTransactionType.TRANSACTION_HEAD & " THEN prtranhead_master.trnheadcode " & _
                                 "WHEN " & enJVTransactionType.LOAN & " THEN lnloan_scheme_master.code " & _
                                 "WHEN " & enJVTransactionType.SAVINGS & " THEN svsavingscheme_master.savingschemecode " & _
                                 "WHEN " & enJVTransactionType.CASH & " THEN '" & If(dicTranType.ContainsKey(enJVTransactionType.CASH) = True, dicTranType.Item(enJVTransactionType.CASH), "CASH").ToString & "' " & _
                                 "WHEN " & enJVTransactionType.BANK & " THEN cfbankbranch_master.branchcode " & _
                                 "WHEN " & enJVTransactionType.COST_CENTER & " THEN '" & If(dicTranType.ContainsKey(enJVTransactionType.COST_CENTER) = True, dicTranType.Item(enJVTransactionType.COST_CENTER), "COST CENTER").ToString & "' " & _
                                 "WHEN " & enJVTransactionType.PAY_PER_ACTIVITY & " THEN practivity_master.code " & _
                                 "WHEN " & enJVTransactionType.CR_EXPENSE & " THEN cmexpense_master.code " & _
                                 "WHEN " & enJVTransactionType.COMPANY_BANK & " THEN CompBranch.branchcode " & _
                                     "ELSE '' " & _
                                 "END, '') AS trnheadcode " & _
                          ", ISNULL(CASE praccount_configuration.transactiontype_Id " & _
                                 "WHEN " & enJVTransactionType.TRANSACTION_HEAD & " THEN prtranhead_master.trnheadname " & _
                                 "WHEN " & enJVTransactionType.LOAN & " THEN lnloan_scheme_master.name " & _
                                 "WHEN " & enJVTransactionType.SAVINGS & " THEN svsavingscheme_master.savingschemename " & _
                                 "WHEN " & enJVTransactionType.CASH & " THEN '" & If(dicTranType.ContainsKey(enJVTransactionType.CASH) = True, dicTranType.Item(enJVTransactionType.CASH), "CASH").ToString & "' " & _
                                 "WHEN " & enJVTransactionType.BANK & " THEN cfbankbranch_master.branchname " & _
                                 "WHEN " & enJVTransactionType.COST_CENTER & " THEN '" & If(dicTranType.ContainsKey(enJVTransactionType.COST_CENTER) = True, dicTranType.Item(enJVTransactionType.COST_CENTER), "COST CENTER").ToString & "' " & _
                                 "WHEN " & enJVTransactionType.PAY_PER_ACTIVITY & " THEN practivity_master.name " & _
                                     "WHEN " & enJVTransactionType.CR_EXPENSE & " THEN cmexpense_master.name " & _
                                 "WHEN " & enJVTransactionType.COMPANY_BANK & " THEN CompBranch.branchname " & _
                                     "ELSE '' " & _
                                   "END, '') AS trnheadname " & _
                          ", praccount_configuration.accountunkid " & _
                          ", praccount_master.account_code " & _
                          ", praccount_master.account_name " & _
                          ", praccount_master.accountgroup_id " & _
                          ", praccount_configuration.isactive " & _
                          ", ISNULL(praccount_configuration.isinactive, 0) AS isinactive " & _
                          ", praccount_configuration.transactiontype_Id " & _
                          ", praccount_configuration.referencecodeid " & _
                          ", praccount_configuration.referencenameid " & _
                          ", praccount_configuration.referencetypeid " & _
                          ", ISNULL(praccount_configuration.shortname, '') AS shortname " & _
                          ", ISNULL(praccount_configuration.shortname2, '') AS shortname2 " & _
                          ", ISNULL(praccount_configuration.shortname3, '') AS shortname3 " & _
                      ", ISNULL(praccount_configuration.periodunkid, 0) AS periodunkid " & _
                      ", ISNULL(cfcommon_period_tran.period_code, '') AS period_code " & _
                      ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
                      ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO "
            'Sohail (03 Jul 2020) - [periodunkid, period_code, period_name, ROWNO]
            'Sohail (04 Jun 2020) - [trnheadcode]
            'Sohail (03 Mar 2020) - [shortname3]
            'Sohail (03 Jan 2019) - [CompBranch.branchname]
            'Sohail (08 Jul 2017) - [shortname2]
            'Gajanan [24-Aug-2020] -- [isinactive]


            'Sohail (06 Aug 2016) -- Start
            'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
            '"WHEN 5 THEN 'CASH' " & _
            '"WHEN 6 THEN 'BANK' " & _
            '"WHEN 7 THEN 'COST CENTER' " & _
            strQ &= ", CASE praccount_configuration.transactiontype_Id "
            For Each pair In dicTranType
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS transactiontype_name "
            'Sohail (06 Aug 2016) -- End

            strQ &= "FROM    praccount_configuration " & _
                            "LEFT JOIN prtranhead_master ON praccount_configuration.tranheadunkid = prtranhead_master.tranheadunkid " & _
                            "LEFT JOIN praccount_master ON praccount_configuration.accountunkid = praccount_master.accountunkid " & _
                            "LEFT JOIN lnloan_scheme_master ON praccount_configuration.tranheadunkid = lnloan_scheme_master.loanschemeunkid " & _
                            "LEFT JOIN svsavingscheme_master ON praccount_configuration.tranheadunkid = svsavingscheme_master.savingschemeunkid " & _
                            "LEFT JOIN practivity_master ON praccount_configuration.tranheadunkid = practivity_master.activityunkid " & _
                            "LEFT JOIN cmexpense_master ON praccount_configuration.tranheadunkid = cmexpense_master.expenseunkid " & _
                            "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON praccount_configuration.tranheadunkid = cfbankbranch_master.branchunkid " & _
                            "LEFT JOIN hrmsConfiguration..cfcompanybank_tran ON cfcompanybank_tran.companybanktranunkid = praccount_configuration.tranheadunkid " & _
                            "LEFT JOIN hrmsConfiguration..cfbankbranch_master AS CompBranch ON cfcompanybank_tran.branchunkid = CompBranch.branchunkid " & _
                            "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                    "WHERE   1 = 1 "
            '       'Sohail (21 May 2020) - [LEFT JOIN hrmsConfiguration..cfcompanybank_tran ON cfcompanybank_tran.companybanktranunkid = praccount_configuration.tranheadunkid]
            '       'Sohail (03 Jan 2019) - [LEFT JOIN hrmsConfiguration..cfbankbranch_master AS CompBranch ON praccount_configuration.tranheadunkid = CompBranch.branchunkid]
            '       'Sohail (06 Aug 2016) - [LEFT JOIN cfbankbranch_master ON praccount_configuration.tranheadunkid = cfbankbranch_master.branchunkid]
            '       'Sohail (12 Nov 2014) - [cmexpense_master.name]
            '       'Sohail (21 Jun 2013) - [CASH, BANK, COST CENTER, practivity_master.name]
            '       'Sohail (13 Mar 2013) - [shortname]
            'Sohail (14 Nov 2011) -- End
           
            If blnOnlyActive Then
                strQ &= "AND praccount_configuration.isactive = 1 "
            End If

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            If dtAsOnDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
                objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            End If
            'Sohail (03 Jul 2020) -- End

            If intTransactionTypeID > 0 Then
                strQ &= "AND praccount_configuration.transactiontype_Id = @transactiontype_Id "
                objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, intTransactionTypeID.ToString)
            End If

            If intTranHeadID > 0 Then
                strQ &= "AND praccount_configuration.tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadID.ToString)
            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'If intAccountID > 0 Then
            '    strQ &= "AND praccount_configuration.accountunkid = @accountunkid "
            '    objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAccountID.ToString)
            'End If

            'If intAccountGroupID > 0 Then
            '    strQ &= "AND praccount_master.accountgroup_id = @accountgroup_id "
            '    objDataOperation.AddParameter("@accountgroup_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intAccountGroupID.ToString)
            'End If
            'Sohail (25 Jul 2020) -- End

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            strQ &= " ) AS A " & _
                   " WHERE 1 = 1 "

            If dtAsOnDate <> Nothing Then
                strQ &= " AND A.ROWNO = 1 "
            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            If intAccountID > 0 Then
                strQ &= "AND A.accountunkid = @accountunkid "
                objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAccountID.ToString)
            End If

            If intAccountGroupID > 0 Then
                strQ &= "AND A.accountgroup_id = @accountgroup_id "
                objDataOperation.AddParameter("@accountgroup_id", SqlDbType.Int, eZeeDataType.INT_SIZE, intAccountGroupID.ToString)
            End If
            'Sohail (25 Jul 2020) -- End

            'Gajanan [24-Aug-2020] -- Start
            'NMB Enhancement : Allow to set account configuration mapping 
            'inactive for closed period to allow to map head on other account 
            'configuration screen from new period
            If mintOnlyInActive = enTranHeadActiveInActive.INACTIVE Then
                strQ &= "AND A.isinactive = 1 "
            Else
                strQ &= "AND A.isinactive = 0 "
            End If
            'Gajanan [24-Aug-2020] -- End

            If strFilterOuter.Trim <> "" Then
                strQ &= " " & strFilterOuter & " "
            End If
            'Sohail (03 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (praccount_configuration) </purpose>
    Public Function Insert(ByVal blnIsOverWrite As Boolean, ByVal intTranHeadTypeID As Integer, Optional ByVal mstrTranheadunkid As String = "") As Boolean 'Sohail (12 Oct 2011)
        'Sohail (12 Oct 2011) -- Start
        'If isExist(mintTransactiontype_Id, mintTranheadunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Account Configuration for this Transaction Head is already defined. Please define new Account Configuration.")
        '    Return False
        'End If
        'Sohail (12 Oct 2011) -- End

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            'Sohail (12 Oct 2011) -- Start
            'objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            'objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountunkid.ToString)
            'objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            'objDataOperation.AddParameter("@Transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactiontype_Id.ToString)
            'Sohail (12 Oct 2011) -- End

            strQ = "INSERT INTO praccount_configuration ( " & _
              "  tranheadunkid " & _
              ", accountunkid " & _
              ", isactive" & _
              ", Transactiontype_Id" & _
              ", referencecodeid " & _
              ", referencenameid " & _
              ", referencetypeid " & _
              ", shortname " & _
              ", shortname2 " & _
              ", shortname3 " & _
              ", periodunkid " & _
              ", isinactive " & _
            ") VALUES (" & _
              "  @tranheadunkid " & _
              ", @accountunkid " & _
              ", @isactive" & _
              ", @Transactiontype_Id" & _
              ", @referencecodeid " & _
              ", @referencenameid " & _
              ", @referencetypeid " & _
              ", @shortname " & _
              ", @shortname2 " & _
              ", @shortname3 " & _
              ", @periodunkid " & _
              ", @isinactive " & _
            "); SELECT @@identity"
            'Sohail (03 Jul 2020) = [periodunkid]
            'Sohail (03 Mar 2020) - [shortname3]
            'Sohail (14 Nov 2011) - [referencecodeid, referencenameid, referencetypeid]
            'Sohail (08 Jul 2017) - [shortname2]
            'Sohail (13 Mar 2013) - [shortname]

            'Sohail (12 Oct 2011) -- Start
            'dsList = objDataOperation.ExecQuery(strQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'mintAccountconfigunkid = dsList.Tables(0).Rows(0).Item(0)
            If mstrTranheadunkid <> "" Then

                Dim arTranheadunkid As String() = mstrTranheadunkid.Split(",")

                If arTranheadunkid.Length <= 0 Then Return False

                For i As Integer = 0 To arTranheadunkid.Length - 1

                    'Sohail (03 Jul 2020) -- Start
                    'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                    'If isExist(intTranHeadTypeID, CInt(arTranheadunkid(i))) Then
                    If isExist(intTranHeadTypeID, CInt(arTranheadunkid(i)), mintPeriodunkid) Then
                        'Sohail (03 Jul 2020) -- End
                        If blnIsOverWrite Then
                            'Sohail (03 Jul 2020) -- Start
                            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                            'mintAccountconfigunkid = GetAccountConfigUnkId(intTranHeadTypeID, CInt(arTranheadunkid(i)))
                            mintAccountconfigunkid = GetAccountConfigUnkId(intTranHeadTypeID, CInt(arTranheadunkid(i)), mintPeriodunkid)
                            'Sohail (03 Jul 2020) -- End
                            mintTranheadunkid = CInt(arTranheadunkid(i))
                            Update(objDataOperation)
                            mintTranheadunkid = -1
                            mintAccountconfigunkid = -1
                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                            Continue For
                        Else
                            Continue For
                        End If
                    End If

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountunkid.ToString)
                    objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                    objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactiontype_Id.ToString)
                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, arTranheadunkid(i).ToString)
                    'Sohail (14 Nov 2011) -- Start
                    objDataOperation.AddParameter("@referencecodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencecodeid.ToString)
                    objDataOperation.AddParameter("@referencenameid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencenameid.ToString)
                    objDataOperation.AddParameter("@referencetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencetypeid.ToString)
                    'Sohail (14 Nov 2011) -- End
                    objDataOperation.AddParameter("@shortname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname.ToString) 'Sohail (13 Mar 2013)
                    objDataOperation.AddParameter("@shortname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname2.ToString) 'Sohail (08 Jul 2017)
                    objDataOperation.AddParameter("@shortname3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname3.ToString) 'Sohail (03 Mar 2020)
                    'Sohail (03 Jul 2020) -- Start
                    'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
                    'Sohail (03 Jul 2020) -- End


                    'Gajanan [24-Aug-2020] -- Start
                    'NMB Enhancement : Allow to set account configuration mapping 
                    'inactive for closed period to allow to map head on other account 
                    'configuration screen from new period
                    objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                    'Gajanan [24-Aug-2020] -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                    mintAccountconfigunkid = dsList.Tables(0).Rows(0).Item(0)
                    If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "praccount_configuration", "accountconfigunkid", mintAccountconfigunkid) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                Next

            Else
                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                'If isExist(intTranHeadTypeID, 0) = True Then
                If isExist(intTranHeadTypeID, 0, mintPeriodunkid) = True Then
                    'Sohail (03 Jul 2020) -- End
                    If blnIsOverWrite Then
                        'Sohail (03 Jul 2020) -- Start
                        'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                        'mintAccountconfigunkid = GetAccountConfigUnkId(intTranHeadTypeID, 0)
                        mintAccountconfigunkid = GetAccountConfigUnkId(intTranHeadTypeID, 0, mintPeriodunkid)
                        'Sohail (03 Jul 2020) -- End
                        If Update(True) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    End If
                Else
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountunkid.ToString)
                    objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                    objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactiontype_Id.ToString)
                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid)
                    'Sohail (14 Nov 2011) -- Start
                    objDataOperation.AddParameter("@referencecodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencecodeid.ToString)
                    objDataOperation.AddParameter("@referencenameid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencenameid.ToString)
                    objDataOperation.AddParameter("@referencetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencetypeid.ToString)
                    'Sohail (14 Nov 2011) -- End
                    objDataOperation.AddParameter("@shortname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname.ToString) 'Sohail (13 Mar 2013)
                    objDataOperation.AddParameter("@shortname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname2.ToString) 'Sohail (08 Jul 2017)
                    objDataOperation.AddParameter("@shortname3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname3.ToString) 'Sohail (03 Mar 2020)
                    'Sohail (03 Jul 2020) -- Start
                    'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
                    'Sohail (03 Jul 2020) -- End


                    'Gajanan [24-Aug-2020] -- Start
                    'NMB Enhancement : Allow to set account configuration mapping 
                    'inactive for closed period to allow to map head on other account 
                    'configuration screen from new period
                    objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                    'Gajanan [24-Aug-2020] -- End

                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    mintAccountconfigunkid = dsList.Tables(0).Rows(0).Item(0)
                    If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "praccount_configuration", "accountconfigunkid", mintAccountconfigunkid) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
            'Sohail (12 Oct 2011) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (praccount_configuration) </purpose>
    Public Function Update(ByVal blnIsOverWrite As Boolean) As Boolean 'Sohail (12 Oct 2011)
        'Sohail (12 Oct 2011) -- Start
        'If isExist(mintTransactiontype_Id, mintTranheadunkid, mintAccountconfigunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Account Configuration is already defined. Please define new Account Configuration.")
        '    Return False
        'End If
        'Sohail (12 Oct 2011) -- End

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            'Sohail (12 Oct 2011) -- Start

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            'If isExist(mintTransactiontype_Id, mintTranheadunkid, mintAccountconfigunkid) Then
            If isExist(mintTransactiontype_Id, mintTranheadunkid, mintPeriodunkid, mintAccountconfigunkid) Then
                'Sohail (03 Jul 2020) -- End
                If blnIsOverWrite Then
                    GoTo OverWrite
                Else
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Account Configuration is already defined. Please define new Account Configuration.")
                    objDataOperation.ReleaseTransaction(False)
                    Return False
        End If
            End If
OverWrite:
            'Sohail (12 Oct 2011) -- End

            objDataOperation.AddParameter("@accountconfigunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountconfigunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@Transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactiontype_Id.ToString)
            'Sohail (14 Nov 2011) -- Start
            objDataOperation.AddParameter("@referencecodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencecodeid.ToString)
            objDataOperation.AddParameter("@referencenameid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencenameid.ToString)
            objDataOperation.AddParameter("@referencetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencetypeid.ToString)
            'Sohail (14 Nov 2011) -- End
            objDataOperation.AddParameter("@shortname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname.ToString) 'Sohail (13 Mar 2013)
            objDataOperation.AddParameter("@shortname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname2.ToString) 'Sohail (08 Jul 2017)
            objDataOperation.AddParameter("@shortname3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname3.ToString) 'Sohail (03 Mar 2020)
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
            'Sohail (03 Jul 2020) -- End


            'Gajanan [24-Aug-2020] -- Start
            'NMB Enhancement : Allow to set account configuration mapping 
            'inactive for closed period to allow to map head on other account 
            'configuration screen from new period
            objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsInactive.ToString)
            'Gajanan [24-Aug-2020] -- End


            strQ = "UPDATE praccount_configuration SET " & _
              "  tranheadunkid = @tranheadunkid" & _
              ", accountunkid = @accountunkid" & _
              ", isactive = @isactive " & _
              ", Transactiontype_Id = @Transactiontype_Id " & _
              ", referencecodeid = @referencecodeid" & _
              ", referencenameid = @referencenameid" & _
              ", referencetypeid = @referencetypeid " & _
              ", shortname = @shortname " & _
              ", shortname2 = @shortname2 " & _
              ", shortname3 = @shortname3 " & _
              ", periodunkid = @periodunkid " & _
              ", isinactive = @isinactive " & _
            "WHERE accountconfigunkid = @accountconfigunkid "
            'Sohail (03 Jul 2020) - [periodunkid]
            'Sohail (03 Mar 2020) - [shortname3]
            'Sohail (14 Nov 2011) - [referencecodeid, referencenameid, referencetypeid]
            'Sohail (08 Jul 2017) - [shortname2]
            'Sohail (13 Mar 2013) - [shortname]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (12 Oct 2011) -- Start
            Dim blnToInsert As Boolean
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "praccount_configuration", mintAccountconfigunkid, "accountconfigunkid", 2) Then
                blnToInsert = True
            End If
            If blnToInsert = True AndAlso clsCommonATLog.Insert_AtLog(objDataOperation, 2, "praccount_configuration", "accountconfigunkid", mintAccountconfigunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
                Return True
            End If
            'Sohail (12 Oct 2011) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (12 Oct 2011) -- Start
    Public Function Update(ByVal objDataOperation As clsDataOperation) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            objDataOperation.AddParameter("@accountconfigunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountconfigunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@Transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactiontype_Id.ToString)
            'Sohail (14 Nov 2011) -- Start
            objDataOperation.AddParameter("@referencecodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencecodeid.ToString)
            objDataOperation.AddParameter("@referencenameid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencenameid.ToString)
            objDataOperation.AddParameter("@referencetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencetypeid.ToString)
            'Sohail (14 Nov 2011) -- End
            objDataOperation.AddParameter("@shortname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname.ToString) 'Sohail (13 Mar 2013)
            objDataOperation.AddParameter("@shortname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname2.ToString) 'Sohail (08 Jul 2017)
            objDataOperation.AddParameter("@shortname3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname3.ToString) 'Sohail (03 Mar 2020)
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
            'Sohail (03 Jul 2020) -- End

            'Gajanan [24-Aug-2020] -- Start
            'NMB Enhancement : Allow to set account configuration mapping 
            'inactive for closed period to allow to map head on other account 
            'configuration screen from new period
            objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsInactive.ToString)
            'Gajanan [24-Aug-2020] -- End

            strQ = "UPDATE praccount_configuration SET " & _
              "  tranheadunkid = @tranheadunkid" & _
              ", accountunkid = @accountunkid" & _
              ", isactive = @isactive " & _
              ", Transactiontype_Id = @Transactiontype_Id " & _
              ", referencecodeid = @referencecodeid" & _
              ", referencenameid = @referencenameid" & _
              ", referencetypeid = @referencetypeid " & _
              ", shortname = @shortname " & _
              ", shortname2 = @shortname2 " & _
              ", shortname3 = @shortname3 " & _
              ", periodunkid = @periodunkid " & _
              ", isinactive = @isinactive " & _
            "WHERE accountconfigunkid = @accountconfigunkid "
            'Sohail (03 Jul 2020) - [periodunkid]
            'Sohail (03 Mar 2020) - [shortname3]
            'Sohail (14 Nov 2011) - [referencecodeid, referencenameid, referencetypeid]
            'Sohail (08 Jul 2017) - [shortname2]
            'Sohail (13 Mar 2013) - [shortname]

            'Gajanan [24-Aug-2020] -- [isinactive]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnToInsert As Boolean
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "praccount_configuration", mintAccountconfigunkid, "accountconfigunkid", 2) Then
                blnToInsert = True
            End If
            If blnToInsert = True AndAlso clsCommonATLog.Insert_AtLog(objDataOperation, 2, "praccount_configuration", "accountconfigunkid", mintAccountconfigunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function
    'Sohail (12 Oct 2011) -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (praccount_configuration) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If
        '<TODO make isused query>

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            'StrQ = "DELETE FROM praccount_configuration " & _
            '"WHERE accountconfigunkid = @accountconfigunkid "
            strQ = "UPDATE praccount_configuration SET " & _
             " isactive = 0 " & _
           "WHERE accountconfigunkid = @accountconfigunkid "

            objDataOperation.AddParameter("@accountconfigunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (12 Oct 2011) -- Start
            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "praccount_configuration", "accountconfigunkid", intUnkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If
            'Sohail (12 Oct 2011) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (30 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    Public Function VoidAll(ByVal strUnkIDs As String) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE  praccount_configuration " & _
                   "SET     isactive = 0 " & _
                   "WHERE   accountconfigunkid IN ( " & strUnkIDs & " ) "


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim arrID() As String = strUnkIDs.Split(",")
            For i = 0 To arrID.Count - 1
                If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "praccount_configuration", "accountconfigunkid", CInt(arrID(i))) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Next
            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoidAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (30 Aug 2013) -- End


    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Public Function InactiveAll(ByVal strUnkIDs As String, ByVal intPeriodid As Integer) As Boolean

        If isInactive(strUnkIDs) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Some of the account of selected transactions are already inactive.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim newIds As String = ""

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = "INSERT INTO praccount_configuration( " & _
                   " tranheadunkid " & _
                   ",accountunkid  " & _
                   ",isactive " & _
                   ",transactiontype_Id " & _
                   ",referencecodeid " & _
                   ",referencenameid " & _
                   ",referencetypeid " & _
                   ",shortname " & _
                   ",shortname2 " & _
                   ",shortname3 " & _
                   ",periodunkid " & _
                   ",isInactive " & _
                   ") select  " & _
                   " tranheadunkid " & _
                   ",accountunkid " & _
                   ",@isactive " & _
                   ",transactiontype_Id " & _
                   ",referencecodeid " & _
                   ",referencenameid " & _
                   ",referencetypeid " & _
                   ",shortname " & _
                   ",shortname2 " & _
                   ",shortname3 " & _
                   ",@periodunkid " & _
                   ",@isInactive " & _
                   "from praccount_configuration " & _
                   "WHERE accountconfigunkid = @accountconfigunkid " & _
                   "; SELECT @@identity"

            For Each id As String In strUnkIDs.Split(",")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodid)
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@accountconfigunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(id))

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If newIds.Trim.Length <= 0 Then
                    newIds = dsList.Tables(0).Rows(0).Item(0)
                Else
                    newIds &= "," & dsList.Tables(0).Rows(0).Item(0)
                End If
                dsList = Nothing
            Next

            Dim arrID() As String = newIds.Split(",")
            For i = 0 To arrID.Count - 1
                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "praccount_configuration", "accountconfigunkid", CInt(arrID(i))) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Next
            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InactiveAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isInactive(ByVal strUnkIDs As String) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              " accountconfigunkid " & _
              "FROM praccount_configuration " & _
              "WHERE isactive = 1 and isinactive = 1 and accountconfigunkid IN ( " & strUnkIDs & " ) "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isInactive; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Gajanan [24-Aug-2020] -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@accountconfigunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intTransactionTypeID As Integer, ByVal intTranHeadID As Integer, ByVal intPeriodUnkId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        'Sohail (03 Jul 2020) - [intPeriodUnkId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  accountconfigunkid " & _
              ", tranheadunkid " & _
              ", accountunkid " & _
              ", isactive " & _
              ", transactiontype_Id " & _
              ", referencecodeid " & _
              ", referencenameid " & _
              ", referencetypeid " & _
             "FROM praccount_configuration " & _
             "WHERE isactive = 1 " & _
             "AND transactiontype_Id = @transactiontype_Id " & _
             "AND tranheadunkid = @tranheadunkid " 'Sohail (14 Nov 2011) - [referencecodeid, referencenameid, referencetypeid]

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            If intPeriodUnkId > 0 Then
                strQ &= " AND praccount_configuration.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If
            'Sohail (03 Jul 2020) -- End

            If intUnkid > 0 Then
                strQ &= " AND accountconfigunkid <> @accountconfigunkid"
                objDataOperation.AddParameter("@accountconfigunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            End If

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, intTransactionTypeID.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (12 Oct 2011) -- Start
    Public Function GetAccountConfigUnkId(ByVal intTranHeadTypeID As Integer, ByVal TranHeadunkid As Integer, ByVal intPeriodUnkId As Integer) As Integer
        'Sohail (03 Jul 2020) - [intPeriodUnkId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intUnkId As Integer = -1

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  accountconfigunkid " & _
             "FROM praccount_configuration " & _
             "WHERE transactiontype_Id = @transactiontype_Id  " & _
             "AND isactive = 1"

            If TranHeadunkid > 0 Then
                strQ &= " AND tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, TranHeadunkid)
            End If
            'If intTranHeadTypeID = enJVTransactionType.TRANSACTION_HEAD Then
            '    strQ &= " AND accountunkid = @accountunkid "
            '    objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountunkid)
            'End If

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            If intPeriodUnkId > 0 Then
                strQ &= " AND praccount_configuration.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If
            'Sohail (03 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadTypeID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                intUnkId = CInt(dsList.Tables(0).Rows(0)("accountconfigunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCCAccountConfigUnkId; Module Name: " & mstrModuleName)
        End Try
        Return intUnkId
    End Function
    'Sohail (12 Oct 2011) -- End

#Region " Message "
    '1, "Account Configuration for this Transaction Head is already defined. Please define new Account Configuration."
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Account Configuration is already defined. Please define new Account Configuration.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Some of the account of selected transactions are already inactive.")

		Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class