﻿'************************************************************************************************************************************
'Class Name : clsaccountconfig_costcenter.vb
'Purpose    :
'Date       :05-08-2011
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsaccountconfig_costcenter
    Private Shared ReadOnly mstrModuleName As String = "clsaccountconfig_costcenter"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAccountconfigccunkid As Integer
    Private mintTranheadunkid As Integer
    Private mintCostcenterunkid As Integer
    Private mintAccountunkid As Integer
    Private mblnIsactive As Boolean = True
    Private mintTransactiontype_Id As Integer
    'Sohail (14 Nov 2011) -- Start
    Private mintReferencecodeid As Integer
    Private mintReferencenameid As Integer
    Private mintReferencetypeid As Integer = -1
    'Sohail (14 Nov 2011) -- End
    Private mstrShortname As String = String.Empty 'Sohail (13 Mar 2013)
    Private mstrShortname2 As String = String.Empty 'Sohail (08 Jul 2017)
    Private mstrShortname3 As String = String.Empty 'Sohail (03 Mar 2020)


    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Private mblnIsInactive As Boolean = False
    'Gajanan [24-Aug-2020] -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accountconfigccunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Accountconfigccunkid() As Integer
        Get
            Return mintAccountconfigccunkid
        End Get
        Set(ByVal value As Integer)
            mintAccountconfigccunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set costcenterunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Costcenterunkid() As Integer
        Get
            Return mintCostcenterunkid
        End Get
        Set(ByVal value As Integer)
            mintCostcenterunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accountunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Accountunkid() As Integer
        Get
            Return mintAccountunkid
        End Get
        Set(ByVal value As Integer)
            mintAccountunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiontype_Id
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Transactiontype_Id() As Integer
        Get
            Return mintTransactiontype_Id
        End Get
        Set(ByVal value As Integer)
            mintTransactiontype_Id = Value
        End Set
    End Property

    'Sohail (14 Nov 2011) -- Start
    ''' <summary>
    ''' Purpose: Get or Set referencecodeid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Referencecodeid() As Integer
        Get
            Return mintReferencecodeid
        End Get
        Set(ByVal value As Integer)
            mintReferencecodeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set referencenameid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Referencenameid() As Integer
        Get
            Return mintReferencenameid
        End Get
        Set(ByVal value As Integer)
            mintReferencenameid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set referencetypeid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Referencetypeid() As Integer
        Get
            Return mintReferencetypeid
        End Get
        Set(ByVal value As Integer)
            mintReferencetypeid = value
        End Set
    End Property
    'Sohail (14 Nov 2011) -- End

    'Sohail (13 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set shortname
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Shortname() As String
        Get
            Return mstrShortname
        End Get
        Set(ByVal value As String)
            mstrShortname = Value
        End Set
    End Property
    'Sohail (13 Mar 2013) -- End

    'Sohail (08 Jul 2017) -- Start
    'TANAPA Enhancement - 68.1 - New JV Integration with Dynamics Nav.
    Public Property _Shortname2() As String
        Get
            Return mstrShortname2
        End Get
        Set(ByVal value As String)
            mstrShortname2 = value
        End Set
    End Property
    'Sohail (08 Jul 2017) -- End

    'Sohail (03 Mar 2020) -- Start
    'NMB Enhancement # : Need another option like short name on account configuration screens to set keywords to get concern value on JV.
    Public Property _Shortname3() As String
        Get
            Return mstrShortname3
        End Get
        Set(ByVal value As String)
            mstrShortname3 = value
        End Set
    End Property
    'Sohail (03 Mar 2020) -- End

    'Sohail (03 Jul 2020) -- Start
    'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
    Private mintPeriodunkid As Integer
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    Private mblnUsedefaultmapping As Boolean
    Public Property _Usedefaultmapping() As Boolean
        Get
            Return mblnUsedefaultmapping
        End Get
        Set(ByVal value As Boolean)
            mblnUsedefaultmapping = value
        End Set
    End Property

    Private mintAllocationbyId As Integer = enAnalysisReport.CostCenter
    Public Property _AllocationbyId() As Integer
        Get
            Return mintAllocationbyId
        End Get
        Set(ByVal value As Integer)
            mintAllocationbyId = value
        End Set
    End Property

    Private mintAllocationUnkId As Integer = 0
    Public Property _AllocationUnkId() As Integer
        Get
            Return mintAllocationUnkId
        End Get
        Set(ByVal value As Integer)
            mintAllocationUnkId = value
        End Set
    End Property
    'Sohail (03 Jul 2020) -- End

    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Public Property _Isinactive() As Boolean
        Get
            Return mblnIsInactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsInactive = value
        End Set
    End Property
    'Gajanan [24-Aug-2020] -- End


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  accountconfigccunkid " & _
              ", tranheadunkid " & _
              ", costcenterunkid " & _
              ", accountunkid " & _
              ", isactive " & _
              ", ISNULL(praccount_configuration_costcenter.isinactive, 0) AS isinactive " & _
              ", transactiontype_Id " & _
              ", referencecodeid " & _
              ", referencenameid " & _
              ", referencetypeid " & _
              ", ISNULL(shortname, '') AS shortname " & _
              ", ISNULL(praccount_configuration_costcenter.shortname2, '') AS shortname2 " & _
              ", ISNULL(praccount_configuration_costcenter.shortname3, '') AS shortname3 " & _
              ", ISNULL(praccount_configuration_costcenter.periodunkid, 0) AS periodunkid " & _
              ", ISNULL(praccount_configuration_costcenter.usedefaultmapping, 0) AS usedefaultmapping " & _
              ", ISNULL(praccount_configuration_costcenter.allocationbyid, " & enAnalysisReport.CostCenter & ") AS allocationbyid " & _
              ", ISNULL(praccount_configuration_costcenter.allocationunkid, 0) AS allocationunkid " & _
             "FROM praccount_configuration_costcenter " & _
             "WHERE accountconfigccunkid = @accountconfigccunkid "
            'Sohail (03 Jul 2020) - [periodunkid, usedefaultmapping, allocationbyid, allocationunkid]
            'Sohail (03 Mar 2020) - [shortname3]
            'Sohail (14 Nov 2011) - [referencecodeid, referencenameid, referencetypeid]
            'Sohail (08 Jul 2017) - [shortname2]
            'Sohail (13 Mar 2013) - [shortname]

            'Gajanan [24-Aug-2020] -- isinactive


            objDataOperation.AddParameter("@accountconfigccunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintAccountconfigccUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintaccountconfigccunkid = CInt(dtRow.Item("accountconfigccunkid"))
                minttranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                mintcostcenterunkid = CInt(dtRow.Item("costcenterunkid"))
                mintaccountunkid = CInt(dtRow.Item("accountunkid"))
                mblnisactive = CBool(dtRow.Item("isactive"))
                mintTransactiontype_Id = CInt(dtRow.Item("transactiontype_Id"))
                'Sohail (14 Nov 2011) -- Start
                mintReferencecodeid = CInt(dtRow.Item("referencecodeid"))
                mintReferencenameid = CInt(dtRow.Item("referencenameid"))
                mintReferencetypeid = CInt(dtRow.Item("referencetypeid"))
                'Sohail (14 Nov 2011) -- End
                mstrShortname = dtRow.Item("shortname").ToString 'Sohail (13 Mar 2013)
                mstrShortname2 = dtRow.Item("shortname2").ToString 'Sohail (08 Jul 2017)
                mstrShortname3 = dtRow.Item("shortname3").ToString 'Sohail (03 Mar 2020)
                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mblnUsedefaultmapping = CBool(dtRow.Item("usedefaultmapping"))
                mintAllocationbyId = CInt(dtRow.Item("allocationbyid"))
                mintAllocationUnkId = CInt(dtRow.Item("allocationunkid"))
                'Sohail (03 Jul 2020) -- End

                'Gajanan [24-Aug-2020] -- Start
                'NMB Enhancement : Allow to set account configuration mapping 
                'inactive for closed period to allow to map head on other account 
                'configuration screen from new period
                mblnIsInactive = CBool(dtRow.Item("isinactive"))
                'Gajanan [24-Aug-2020] -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal strFilter As String = "" _
                            , Optional ByVal blnAddGrouping As Boolean = False _
                            , Optional ByVal dtAsOnDate As Date = Nothing _
                            , Optional ByVal strFilterOuter As String = "" _
                            , Optional ByVal mintOnlyInActive As Integer = enTranHeadActiveInActive.ACTIVE _
                            ) As DataSet
        'Sohail (03 Jul 2020) - [dtAsOnDate, strFilterOuter]
        'Sohail (02 Nov 2019) - [blnAddGrouping]
        'Sohail (06 Aug 2016) - [strFilter]
        'Gajanan [24-Aug-2020] -- [mintOnlyInActive]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            'Sohail (03 Jul 2020) -- End

            'Sohail (06 Aug 2016) -- Start
            'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
            Dim objMaster As New clsMasterData
            Dim ds As DataSet = objMaster.getComboListJVTransactionType("TranType")
            Dim dicTranType As Dictionary(Of Integer, String) = (From p In ds.Tables("TranType") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            'Sohail (06 Aug 2016) -- End

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            Dim dsAllocation As DataSet = (New clsMasterData).GetReportAllocation("List", , , False)

            strQ = "SELECT * FROM ( "
            'Sohail (03 Jul 2020) -- End

            'Sohail (14 Nov 2011) -- Start
            'Changes : Loan Schme name and Saving Scheme name added.
            'strQ = "SELECT  accountconfigccunkid , " & _
            '          " praccount_configuration_costcenter.tranheadunkid , " & _
            '          " prtranhead_master.trnheadname, " & _
            '          " praccount_configuration_costcenter.costcenterunkid , " & _
            '          " prcostcenter_master.costcentername, " & _
            '          " praccount_configuration_costcenter.accountunkid , " & _
            '          " praccount_master.account_code, " & _
            '          " praccount_master.account_name, " & _
            '          " praccount_configuration_costcenter.isactive , " & _
            '          " transactiontype_Id " & _
            '          " FROM  praccount_configuration_costcenter " & _
            '          " LEFT JOIN prtranhead_master ON praccount_configuration_costcenter.tranheadunkid = prtranhead_master.tranheadunkid AND prtranhead_master.isvoid = 0 " & _
            '          " LEFT JOIN prcostcenter_master ON praccount_configuration_costcenter.costcenterunkid = prcostcenter_master.costcenterunkid AND prcostcenter_master.isactive = 1 " & _
            '          " LEFT JOIN praccount_master ON praccount_configuration_costcenter.accountunkid = praccount_master.accountunkid AND praccount_master.isactive = 1 " & _
            '          " WHERE 1=1 " 'Sohail (08 Nov 2011) - [account_code]
            strQ &= "SELECT  accountconfigccunkid , " & _
                      " praccount_configuration_costcenter.tranheadunkid , " & _
                      " ISNULL(CASE praccount_configuration_costcenter.transactiontype_Id " & _
                                 "WHEN " & enJVTransactionType.TRANSACTION_HEAD & " THEN prtranhead_master.trnheadcode " & _
                                 "WHEN " & enJVTransactionType.LOAN & " THEN lnloan_scheme_master.code " & _
                                 "WHEN " & enJVTransactionType.SAVINGS & " THEN svsavingscheme_master.savingschemecode " & _
                                 "WHEN " & enJVTransactionType.CASH & " THEN '" & If(dicTranType.ContainsKey(enJVTransactionType.CASH) = True, dicTranType.Item(enJVTransactionType.CASH), "CASH").ToString & "' " & _
                                 "WHEN " & enJVTransactionType.BANK & " THEN cfbankbranch_master.branchcode " & _
                                 "WHEN " & enJVTransactionType.COST_CENTER & " THEN '" & If(dicTranType.ContainsKey(enJVTransactionType.COST_CENTER) = True, dicTranType.Item(enJVTransactionType.COST_CENTER), "COST CENTER").ToString & "' " & _
                                 "WHEN " & enJVTransactionType.PAY_PER_ACTIVITY & " THEN practivity_master.code " & _
                                 "WHEN " & enJVTransactionType.CR_EXPENSE & " THEN cmexpense_master.code " & _
                                 "WHEN " & enJVTransactionType.COMPANY_BANK & " THEN CompBranch.branchcode " & _
                                     "ELSE '' " & _
                                 "END, '') AS trnheadcode, " & _
                        " ISNULL(CASE praccount_configuration_costcenter.transactiontype_Id " & _
                                 "WHEN " & enJVTransactionType.TRANSACTION_HEAD & " THEN prtranhead_master.trnheadname " & _
                                 "WHEN " & enJVTransactionType.LOAN & " THEN lnloan_scheme_master.name " & _
                                 "WHEN " & enJVTransactionType.SAVINGS & " THEN svsavingscheme_master.savingschemename " & _
                                 "WHEN " & enJVTransactionType.CASH & " THEN '" & If(dicTranType.ContainsKey(enJVTransactionType.CASH) = True, dicTranType.Item(enJVTransactionType.CASH), "CASH").ToString & "' " & _
                                 "WHEN " & enJVTransactionType.BANK & " THEN cfbankbranch_master.branchname " & _
                                 "WHEN " & enJVTransactionType.COST_CENTER & " THEN '" & If(dicTranType.ContainsKey(enJVTransactionType.COST_CENTER) = True, dicTranType.Item(enJVTransactionType.COST_CENTER), "COST CENTER").ToString & "' " & _
                                 "WHEN " & enJVTransactionType.PAY_PER_ACTIVITY & " THEN practivity_master.name " & _
                                 "WHEN " & enJVTransactionType.CR_EXPENSE & " THEN cmexpense_master.name " & _
                                 "WHEN " & enJVTransactionType.COMPANY_BANK & " THEN CompBranch.branchname " & _
                                 "ELSE '' " & _
                               "END, '') AS trnheadname, " & _
                      " praccount_configuration_costcenter.costcenterunkid , " & _
                      " ISNULL(CC.costcentercode, ' '+ @defaultacount) AS costcentercode , " & _
                      " ISNULL(CC.costcentername, ' '+ @defaultacount) AS costcentername, " & _
                      " ISNULL(CC.customcode, '') AS customcode, " & _
                      " praccount_configuration_costcenter.accountunkid , " & _
                      " praccount_master.account_code, " & _
                      " praccount_master.account_name, " & _
                      " praccount_configuration_costcenter.isactive , " & _
                      " ISNULL(praccount_configuration_costcenter.isinactive, 0) AS isinactive , " & _
                      " transactiontype_Id, " & _
                      " praccount_configuration_costcenter.referencecodeid, " & _
                      " praccount_configuration_costcenter.referencenameid, " & _
                      " praccount_configuration_costcenter.referencetypeid " & _
                      ", ISNULL(praccount_configuration_costcenter.shortname, '') AS shortname " & _
                      ", ISNULL(praccount_configuration_costcenter.shortname2, '') AS shortname2 " & _
                      ", ISNULL(praccount_configuration_costcenter.shortname3, '') AS shortname3 " & _
                      ", CAST(0 AS BIT) AS IsGrp " & _
                      ", CAST(0 AS BIT) AS IsChecked " & _
                      ", ISNULL(praccount_configuration_costcenter.periodunkid, 0) AS periodunkid " & _
                      ", ISNULL(cfcommon_period_tran.period_code, '') AS period_code " & _
                      ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
                      ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112), '') AS start_date " & _
                      ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112), '') AS end_date " & _
                      ", ISNULL(cfcommon_period_tran.statusid, '') AS statusid " & _
                      ", ISNULL(praccount_configuration_costcenter.usedefaultmapping, 0) AS usedefaultmapping " & _
                      ", ISNULL(praccount_configuration_costcenter.allocationunkid, 0) AS allocationunkid " & _
                      ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO "
            'Sohail (03 Jul 2020) - [periodunkid, period_code, start_date, end_date, usedefaultmapping, allocationunkid, ROWNO]
            'Sohail (04 Jun 2020) - [trnheadcode, customcode]
            'Sohail (03 Mar 2020) - [shortname3]
            'Sohail (02 Nov 2019) - [costcentercode, IsGrp, IsChecked]
            'Sohail (03 Jan 2019) - [CompBranch.branchname]
            'Sohail (08 Jul 2017) - [shortname2]
            'Gajanan [24-Aug-2020] -- [isinactive]

            'Sohail (06 Aug 2016) -- Start
            'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
            '"WHEN 5 THEN 'CASH' " & _
            '"WHEN 6 THEN 'BANK' " & _
            '"WHEN 7 THEN 'COST CENTER' " & _
            strQ &= ", CASE praccount_configuration_costcenter.transactiontype_Id "
            For Each pair In dicTranType
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS transactiontype_name "
            'Sohail (06 Aug 2016) -- End

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            strQ &= ", ISNULL(praccount_configuration_costcenter.allocationbyid, " & enAnalysisReport.CostCenter & ") AS allocationbyid " & _
                    ", CASE ISNULL(praccount_configuration_costcenter.allocationbyid, " & enAnalysisReport.CostCenter & ") "
            For Each dsRow As DataRow In dsAllocation.Tables(0).Rows
                If CInt(CInt(dsRow.Item("Id"))) <= 0 Then
                    strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN '" & dsRow.Item("AllocationTranName").ToString & "' "
                Else
                    strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN " & dsRow.Item("AllocationTranName").ToString.Replace("''", "' " & Language.getMessage(mstrModuleName, 6, "Default Account for ALL unmapped #costcenter#").Replace("#costcenter#", dsRow.Item("Name").ToString) & "'") & " "
                End If
                If CInt(CInt(dsRow.Item("Id"))) = enAnalysisReport.CostCenter Then
                    objDataOperation.AddParameter("@defaultacount", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Default Account for ALL unmapped #costcenter#").Replace("#costcenter#", dsRow.Item("Name").ToString))
                End If
            Next
            strQ &= " END AS AllocationName "

            strQ &= ", CASE ISNULL(praccount_configuration_costcenter.allocationbyid, " & enAnalysisReport.CostCenter & ") "
            For Each dsRow As DataRow In dsAllocation.Tables(0).Rows
                strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN '" & dsRow.Item("Name").ToString & "' "
            Next
            strQ &= " END AS AllocationByName "
            'Sohail (03 Jul 2020) -- End

            strQ &= " FROM  praccount_configuration_costcenter " & _
                      " LEFT JOIN prtranhead_master ON praccount_configuration_costcenter.tranheadunkid = prtranhead_master.tranheadunkid AND prtranhead_master.isvoid = 0 " & _
                      " LEFT JOIN prcostcenter_master AS CC ON praccount_configuration_costcenter.costcenterunkid = CC.costcenterunkid AND CC.isactive = 1 " & _
                      " LEFT JOIN prcostcenter_master ON praccount_configuration_costcenter.allocationunkid = prcostcenter_master.costcenterunkid AND prcostcenter_master.isactive = 1 AND ISNULL(praccount_configuration_costcenter.allocationbyid, " & enAnalysisReport.CostCenter & ") = " & CInt(enAnalysisReport.CostCenter) & " " & _
                      " LEFT JOIN praccount_master ON praccount_configuration_costcenter.accountunkid = praccount_master.accountunkid AND praccount_master.isactive = 1 " & _
                      " LEFT JOIN lnloan_scheme_master ON praccount_configuration_costcenter.tranheadunkid = lnloan_scheme_master.loanschemeunkid " & _
                      " LEFT JOIN svsavingscheme_master ON praccount_configuration_costcenter.tranheadunkid = svsavingscheme_master.savingschemeunkid " & _
                      "LEFT JOIN practivity_master ON praccount_configuration_costcenter.tranheadunkid = practivity_master.activityunkid " & _
                      "LEFT JOIN cmexpense_master ON praccount_configuration_costcenter.tranheadunkid = cmexpense_master.expenseunkid " & _
                      "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON praccount_configuration_costcenter.tranheadunkid = cfbankbranch_master.branchunkid " & _
                      "LEFT JOIN hrmsConfiguration..cfcompanybank_tran ON cfcompanybank_tran.companybanktranunkid = praccount_configuration_costcenter.tranheadunkid " & _
                      "LEFT JOIN hrmsConfiguration..cfbankbranch_master AS CompBranch ON cfcompanybank_tran.branchunkid = CompBranch.branchunkid " & _
                      "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                      "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = praccount_configuration_costcenter.allocationunkid AND ISNULL(praccount_configuration_costcenter.allocationbyid, " & enAnalysisReport.CostCenter & ") = " & CInt(enAnalysisReport.Department) & " " & _
                      "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = praccount_configuration_costcenter.allocationunkid AND ISNULL(praccount_configuration_costcenter.allocationbyid, " & enAnalysisReport.CostCenter & ") = " & CInt(enAnalysisReport.Job) & " " & _
                      "LEFT JOIN hrstation_master ON hrstation_master.stationunkid = praccount_configuration_costcenter.allocationunkid AND ISNULL(praccount_configuration_costcenter.allocationbyid, " & enAnalysisReport.CostCenter & ") = " & CInt(enAnalysisReport.Branch) & " " & _
                      "LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = praccount_configuration_costcenter.allocationunkid AND ISNULL(praccount_configuration_costcenter.allocationbyid, " & enAnalysisReport.CostCenter & ") = " & CInt(enAnalysisReport.DepartmentGroup) & " " & _
                      "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = praccount_configuration_costcenter.allocationunkid AND ISNULL(praccount_configuration_costcenter.allocationbyid, " & enAnalysisReport.CostCenter & ") = " & CInt(enAnalysisReport.Section) & " " & _
                      "LEFT JOIN hrunit_master ON hrunit_master.unitunkid = praccount_configuration_costcenter.allocationunkid AND ISNULL(praccount_configuration_costcenter.allocationbyid, " & enAnalysisReport.CostCenter & ") = " & CInt(enAnalysisReport.Unit) & " " & _
                      "LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = praccount_configuration_costcenter.allocationunkid AND ISNULL(praccount_configuration_costcenter.allocationbyid, " & enAnalysisReport.CostCenter & ") = " & CInt(enAnalysisReport.JobGroup) & " " & _
                      "LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = praccount_configuration_costcenter.allocationunkid AND ISNULL(praccount_configuration_costcenter.allocationbyid, " & enAnalysisReport.CostCenter & ") = " & CInt(enAnalysisReport.ClassGroup) & " " & _
                      "LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = praccount_configuration_costcenter.allocationunkid AND ISNULL(praccount_configuration_costcenter.allocationbyid, " & enAnalysisReport.CostCenter & ") = " & CInt(enAnalysisReport.Classs) & " " & _
                      "LEFT JOIN hrteam_master ON hrteam_master.teamunkid = praccount_configuration_costcenter.allocationunkid AND ISNULL(praccount_configuration_costcenter.allocationbyid, " & enAnalysisReport.CostCenter & ") = " & CInt(enAnalysisReport.Team) & " " & _
                      "LEFT JOIN hrunitgroup_master ON praccount_configuration_costcenter.allocationunkid = hrunitgroup_master.unitgroupunkid AND ISNULL(praccount_configuration_costcenter.allocationbyid, " & enAnalysisReport.CostCenter & ") = " & CInt(enAnalysisReport.UnitGroup) & " " & _
                      "LEFT JOIN hrsectiongroup_master ON praccount_configuration_costcenter.allocationunkid = hrsectiongroup_master.sectiongroupunkid AND ISNULL(praccount_configuration_costcenter.allocationbyid, " & enAnalysisReport.CostCenter & ") = " & CInt(enAnalysisReport.SectionGroup) & " " & _
                      " WHERE 1=1 "
            'Sohail (03 Jul 2020) - [LEFT JOIN cfcommon_period_tran]
            'Sohail (21 May 2020) - [LEFT JOIN hrmsConfiguration..cfcompanybank_tran ON cfcompanybank_tran.companybanktranunkid = praccount_configuration_costcenter.tranheadunkid]
            'Sohail (03 Jan 2019) - [LEFT JOIN hrmsConfiguration..cfbankbranch_master AS CompBranch ON praccount_configuration_costcenter.tranheadunkid = CompBranch.branchunkid]
            'Sohail (06 Aug 2016) - [LEFT JOIN cfbankbranch_master ON praccount_configuration_costcenter.tranheadunkid = cfbankbranch_master.branchunkid]
            'Sohail (12 Nov 2014) - [cmexpense_master.name]
            'Sohail (21 Jun 2013) - [CASH, BANK, COST CENTER, practivity_master.name]
            'Sohail (13 Mar 2013) - [shortname]
            'Sohail (14 Nov 2011) -- End

            If blnOnlyActive Then
                strQ &= " AND praccount_configuration_costcenter.isactive = 1 "
            End If

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            If dtAsOnDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
                objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            End If
            'Sohail (03 Jul 2020) -- End

            'Sohail (06 Aug 2016) -- Start
            'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
            'strQ &= " ORDER BY praccount_configuration_costcenter.costcenterunkid "
            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            'strQ &= " ORDER BY prcostcenter_master.costcentername " & _
            '               ", praccount_configuration_costcenter.transactiontype_Id "
            strQ &= " ) AS A " & _
                    " WHERE 1 = 1 "

            If dtAsOnDate <> Nothing Then
                strQ &= " AND A.ROWNO = 1 "
            End If

            'Gajanan [24-Aug-2020] -- Start
            'NMB Enhancement : Allow to set account configuration mapping 
            'inactive for closed period to allow to map head on other account 
            'configuration screen from new period
            If mintOnlyInActive = enTranHeadActiveInActive.INACTIVE Then
                strQ &= "AND A.isinactive = 1 "
            Else
                strQ &= "AND A.isinactive = 0 "
            End If
            'Gajanan [24-Aug-2020] -- End

            If strFilterOuter.Trim <> "" Then
                strQ &= " " & strFilterOuter & " "
            End If

            strQ &= " ORDER BY A.costcentername " & _
                           ", A.transactiontype_Id "
            'Sohail (03 Jul 2020) -- End
            'Sohail (06 Aug 2016) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Dim dtFinal As DataTable 'Sohail (03 Jul 2020)
            'Sohail (02 Nov 2019) -- Start
            'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
            If blnAddGrouping = True Then

                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                Dim dt As DataTable = New DataView(dsList.Tables(0)).ToTable(True, "costcentername", "costcentercode", "costcenterunkid")
                Dim dtCol As New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dt.Columns.Add(dtCol)

                dtCol = New DataColumn("IsGrp", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = True
                dtCol.AllowDBNull = False
                dt.Columns.Add(dtCol)

                dt.Merge(dsList.Tables(0), False)
                dt.DefaultView.Sort = "costcentername, costcentercode, costcenterunkid, IsGrp DESC"

                dsList.Tables.Clear()
                dsList.Tables.Add(dt.DefaultView.ToTable)
                dsList.Tables(0).Columns.Add("CostCenterColumn", System.Type.GetType("System.String")).Expression = "AllocationByName + ' : ' + AllocationName "
                'dsList.Tables(0).Columns.Add("CostCenterColumn", System.Type.GetType("System.String")).Expression = "AllocationByName + ' : ' + AllocationName "
                'dsList.Tables(0).Columns.Add("GroupName", System.Type.GetType("System.String")).Expression = "AllocationByName + ' : ' + AllocationName "
                'Dim lstRow = (From p In dsList.Tables(0) Select New With {Key .allocationbyid = CInt(p.Item("allocationbyid")), Key .costcenterunkid = CInt(p.Item("costcenterunkid")), Key .AllocationByName = p.Item("AllocationByName").ToString, Key .AllocationName = p.Item("AllocationName").ToString}).ToList.Distinct

                'For Each itm In lstRow
                '    Dim dr As DataRow = dsList.Tables(0).NewRow

                '    dr.Item("allocationbyid") = itm.allocationbyid
                '    dr.Item("costcenterunkid") = itm.costcenterunkid
                '    dr.Item("AllocationByName") = itm.AllocationByName
                '    dr.Item("AllocationName") = itm.AllocationName
                '    dr.Item("IsGrp") = True
                '    dsList.Tables(0).Rows.Add(dr)
                'Next
                'dsList.Tables(0).AcceptChanges()

                'dtFinal = New DataView(dsList.Tables(0), "", "GroupName, IsGrp DESC", DataViewRowState.CurrentRows).ToTable

                'dsList.Tables.Clear()
                'dsList.Tables.Add(dtFinal)
                'Sohail (03 Jul 2020) -- End

            End If
            'Sohail (02 Nov 2019) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (praccount_configuration_costcenter) </purpose>
    Public Function Insert(ByVal blnIsOverWrite As Boolean, ByVal intTranHeadTypeID As Integer, ByVal mstrTranheadunkid As String, ByVal mstCostCenterIDs As String, ByVal mstrAllocationTranUnkIDs As String) As Boolean 'Sohail (14 Nov 2011) - [mstCostCenterIDs]
        'Public Function Insert(ByVal blnIsOverWrite As Boolean, ByVal intTranHeadTypeID As Integer, Optional ByVal mstrTranheadunkid As String = "") As Boolean
        Dim objDataOperation As clsDataOperation
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try


            strQ = "INSERT INTO praccount_configuration_costcenter ( " & _
                  "  tranheadunkid " & _
                  ", costcenterunkid " & _
                  ", accountunkid " & _
                  ", isactive " & _
                  ", transactiontype_Id" & _
                  ", referencecodeid " & _
                  ", referencenameid " & _
                  ", referencetypeid " & _
                  ", shortname " & _
                  ", shortname2 " & _
                  ", shortname3 " & _
                  ", periodunkid " & _
                  ", usedefaultmapping " & _
                  ", allocationbyid " & _
                  ", allocationunkid " & _
                  ", isinactive " & _
                ") VALUES (" & _
                  "  @tranheadunkid " & _
                  ", @costcenterunkid " & _
                  ", @accountunkid " & _
                  ", @isactive " & _
                  ", @transactiontype_Id" & _
                  ", @referencecodeid " & _
                  ", @referencenameid " & _
                  ", @referencetypeid " & _
                  ", @shortname " & _
                  ", @shortname2 " & _
                  ", @shortname3 " & _
                  ", @periodunkid " & _
                  ", @usedefaultmapping " & _
                  ", @allocationbyid " & _
                  ", @allocationunkid " & _
                  ", @isinactive " & _
                "); SELECT @@identity"
            'Sohail (03 Jul 2020) = [periodunkid, usedefaultmapping, allocationbyid, allocationunkid]
            'Sohail (03 Mar 2020) - [shortname3]
            'Sohail (14 Nov 2011) - [referencecodeid, referencenameid, referencetypeid]
            'Sohail (08 Jul 2017) - [shortname2]
            'Sohail (13 Mar 2013) - [shortname]


            If mstrTranheadunkid <> "" Then

                Dim arTranheadunkid As String() = mstrTranheadunkid.Split(",")

                If arTranheadunkid.Length <= 0 Then Return False

                'Sohail (14 Nov 2011) -- Start
                Dim arCCUnkid As String() = mstCostCenterIDs.Split(",")

                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                Dim arrAllocationTranUnkid() As String = mstrAllocationTranUnkIDs.Split(",")

                For k As Integer = 0 To arrAllocationTranUnkid.Length - 1
                    mintAllocationUnkId = CInt(arrAllocationTranUnkid(k))
                    'Sohail (03 Jul 2020) -- End

                For j As Integer = 0 To arCCUnkid.Length - 1
                    mintCostcenterunkid = CInt(arCCUnkid(j))
                    'Sohail (14 Nov 2011) -- End

                For i As Integer = 0 To arTranheadunkid.Length - 1

                            If isExist(intTranHeadTypeID, CInt(arTranheadunkid(i)), mintCostcenterunkid, mintPeriodunkid, mintAllocationbyId, mintAllocationUnkId) Then
                                'Sohail (03 Jul 2020) - [mintAllocationbyId, mintPeriodunkid, mintAllocationUnkId]
                        If blnIsOverWrite Then
                            'Sohail (02 Aug 2011) -- Start
                            'mintAccountconfigccunkid = GetCCAccountConfigUnkId(objDataOperation, CInt(arTranheadunkid(i)))
                                'Sohail (09 Jul 2013) -- Start
                                'TRA - ENHANCEMENT
                                'mintAccountconfigccunkid = GetCCAccountConfigUnkId(intTranHeadTypeID, CInt(arTranheadunkid(i)))
                                    mintAccountconfigccunkid = GetCCAccountConfigUnkId(intTranHeadTypeID, CInt(arTranheadunkid(i)), mintCostcenterunkid, mintPeriodunkid, mintAllocationbyId, mintAllocationUnkId)
                                    'Sohail (03 Jul 2020) - [mintAllocationbyId, mintPeriodunkid, mintAllocationUnkId]
                                'Sohail (09 Jul 2013) -- End
                            'Sohail (02 Aug 2011) -- End
                            mintTranheadunkid = CInt(arTranheadunkid(i))
                            Update(objDataOperation)
                            mintTranheadunkid = -1
                            mintAccountconfigccunkid = -1
                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                            Continue For
                        Else
                            Continue For
                        End If
                    End If

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
                    objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountunkid.ToString)
                    objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                    objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactiontype_Id.ToString)
                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, arTranheadunkid(i).ToString)
                        'Sohail (14 Nov 2011) -- Start
                        objDataOperation.AddParameter("@referencecodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencecodeid.ToString)
                        objDataOperation.AddParameter("@referencenameid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencenameid.ToString)
                        objDataOperation.AddParameter("@referencetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencetypeid.ToString)
                        'Sohail (14 Nov 2011) -- End
                        objDataOperation.AddParameter("@shortname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname.ToString) 'Sohail (13 Mar 2013)
                        objDataOperation.AddParameter("@shortname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname2.ToString) 'Sohail (08 Jul 2017)
                        objDataOperation.AddParameter("@shortname3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname3.ToString) 'Sohail (03 Mar 2020)
                            'Sohail (03 Jul 2020) -- Start
                            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
                            objDataOperation.AddParameter("@usedefaultmapping", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnUsedefaultmapping)
                            objDataOperation.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationbyId)
                            objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationUnkId)
                            'Sohail (03 Jul 2020) -- End


                            'Gajanan [24-Aug-2020] -- Start
                            'NMB Enhancement : Allow to set account configuration mapping 
                            'inactive for closed period to allow to map head on other account 
                            'configuration screen from new period
                            objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                            'Gajanan [24-Aug-2020] -- End


                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Sohail (12 Oct 2011) -- Start
                    mintAccountconfigccunkid = dsList.Tables(0).Rows(0).Item(0)
                    If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "praccount_configuration_costcenter", "accountconfigccunkid", mintAccountconfigccunkid) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                    'Sohail (12 Oct 2011) -- End
                Next

                Next 'Sohail (14 Nov 2011)

                Next 'Sohail (03 Jul 2020)
            Else

                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                Dim arrAllocationTranUnkid() As String = mstrAllocationTranUnkIDs.Split(",")

                For k As Integer = 0 To arrAllocationTranUnkid.Length - 1
                    mintAllocationUnkId = CInt(arrAllocationTranUnkid(k))
                    'Sohail (03 Jul 2020) -- End

                'Sohail (14 Nov 2011) -- Start
                Dim arCCUnkid As String() = mstCostCenterIDs.Split(",")

                For j As Integer = 0 To arCCUnkid.Length - 1
                    mintCostcenterunkid = CInt(arCCUnkid(j))
                    'Sohail (14 Nov 2011) -- End

                'Sohail (02 Aug 2011) -- Start
                        If isExist(intTranHeadTypeID, 0, mintCostcenterunkid, mintPeriodunkid, mintAllocationbyId, mintAllocationUnkId) = True Then
                            'Sohail (03 Jul 2020) - [mintAllocationbyId, mintPeriodunkid, mintAllocationUnkId]
                    If blnIsOverWrite Then
                            'Sohail (09 Jul 2013) -- Start
                            'TRA - ENHANCEMENT
                            'mintAccountconfigccunkid = GetCCAccountConfigUnkId(intTranHeadTypeID, 0)
                                mintAccountconfigccunkid = GetCCAccountConfigUnkId(intTranHeadTypeID, 0, mintCostcenterunkid, mintPeriodunkid, mintAllocationbyId, mintAllocationUnkId)
                                'Sohail (03 Jul 2020) - [mintAllocationbyId, mintPeriodunkid, mintAllocationUnkId]
                            'Sohail (09 Jul 2013) -- End
                        'Sohail (12 Oct 2011) -- Start
                        'Call Update(True)
                            If Update(True) = False Then
                                objDataOperation.ReleaseTransaction(False)
                                Return False
                            End If

                        'Sohail (12 Oct 2011) -- End
                    End If
                Else
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
                    objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountunkid.ToString)
                    objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                    objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactiontype_Id.ToString)
                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid)
                        'Sohail (14 Nov 2011) -- Start
                        objDataOperation.AddParameter("@referencecodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencecodeid.ToString)
                        objDataOperation.AddParameter("@referencenameid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencenameid.ToString)
                        objDataOperation.AddParameter("@referencetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencetypeid.ToString)
                        'Sohail (14 Nov 2011) -- End
                        objDataOperation.AddParameter("@shortname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname.ToString) 'Sohail (13 Mar 2013)
                        objDataOperation.AddParameter("@shortname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname2.ToString) 'Sohail (08 Jul 2017)
                        objDataOperation.AddParameter("@shortname3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname3.ToString) 'Sohail (03 Mar 2020)
                            'Sohail (03 Jul 2020) -- Start
                            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
                            objDataOperation.AddParameter("@usedefaultmapping", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnUsedefaultmapping)
                            objDataOperation.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationbyId)
                            objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationUnkId)
                            'Sohail (03 Jul 2020) -- End


                            'Gajanan [24-Aug-2020] -- Start
                            'NMB Enhancement : Allow to set account configuration mapping 
                            'inactive for closed period to allow to map head on other account 
                            'configuration screen from new period
                            objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                            'Gajanan [24-Aug-2020] -- End


                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Sohail (12 Oct 2011) -- Start
                    mintAccountconfigccunkid = dsList.Tables(0).Rows(0).Item(0)
                    If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "praccount_configuration_costcenter", "accountconfigccunkid", mintAccountconfigccunkid) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                    'Sohail (12 Oct 2011) -- End
                End If
                'Sohail (02 Aug 2011) -- End

                Next 'Sohail (14 Nov 2011)

                Next 'Sohail (03 Jul 2020)
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            objDataOperation.ReleaseTransaction(False)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (praccount_configuration_costcenter) </purpose>
    Public Function Update(ByVal blnIsOverWrite As Boolean) As Boolean
        Dim objDataOperation As clsDataOperation
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try

            If isExist(mintTransactiontype_Id, mintTranheadunkid, mintCostcenterunkid, mintPeriodunkid, mintAllocationbyId, mintAllocationUnkId, mintAccountunkid, mintAccountconfigccunkid) Then
                'Sohail (03 Jul 2020) - [mintPeriodunkid, mintAllocationbyId, mintAllocationUnkId]
                If blnIsOverWrite Then
                    GoTo OverWrite
                Else
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "Account Configuration for this Transaction Head is already defined. Please define new Cost Center Account Configuration.")
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

OverWrite:
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@accountconfigccunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountconfigccunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
            objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactiontype_Id.ToString)
            'Sohail (14 Nov 2011) -- Start
            objDataOperation.AddParameter("@referencecodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencecodeid.ToString)
            objDataOperation.AddParameter("@referencenameid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencenameid.ToString)
            objDataOperation.AddParameter("@referencetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencetypeid.ToString)
            'Sohail (14 Nov 2011) -- End
            objDataOperation.AddParameter("@shortname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname.ToString) 'Sohail (13 Mar 2013)
            objDataOperation.AddParameter("@shortname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname2.ToString) 'Sohail (08 Jul 2017)
            objDataOperation.AddParameter("@shortname3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname3.ToString) 'Sohail (03 Mar 2020)
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
            objDataOperation.AddParameter("@usedefaultmapping", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnUsedefaultmapping)
            objDataOperation.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationbyId)
            objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationUnkId)
            'Sohail (03 Jul 2020) -- End


            'Gajanan [24-Aug-2020] -- Start
            'NMB Enhancement : Allow to set account configuration mapping 
            'inactive for closed period to allow to map head on other account 
            'configuration screen from new period
            objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsInactive.ToString)
            'Gajanan [24-Aug-2020] -- End


            strQ = "UPDATE praccount_configuration_costcenter SET " & _
              "  tranheadunkid = @tranheadunkid" & _
              ", costcenterunkid = @costcenterunkid" & _
              ", accountunkid = @accountunkid" & _
              ", isactive = @isactive" & _
              ", transactiontype_Id = @transactiontype_Id " & _
              ", referencecodeid = @referencecodeid" & _
              ", referencenameid = @referencenameid" & _
              ", referencetypeid = @referencetypeid " & _
              ", shortname = @shortname " & _
              ", shortname2 = @shortname2 " & _
              ", shortname3 = @shortname3 " & _
              ", periodunkid = @periodunkid " & _
              ", usedefaultmapping = @usedefaultmapping " & _
              ", allocationbyid = @allocationbyid " & _
              ", allocationunkid = @allocationunkid " & _
              ", isinactive = @isinactive " & _
            "WHERE accountconfigccunkid = @accountconfigccunkid "
            'Sohail (03 Jul 2020) = [periodunkid, usedefaultmapping, allocationbyid, allocationunkid]
            'Sohail (03 Mar 2020) - [shortname3]
            'Sohail (14 Nov 2011) - [referencecodeid, referencenameid, referencetypeid]
            'Sohail (08 Jul 2017) - [shortname2]
            'Sohail (13 Mar 2013) - [shortname]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (12 Oct 2011) -- Start
            Dim blnToInsert As Boolean
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "praccount_configuration_costcenter", mintAccountconfigccunkid, "accountconfigccunkid", 2) Then
                blnToInsert = True
            End If
            If blnToInsert = True AndAlso clsCommonATLog.Insert_AtLog(objDataOperation, 2, "praccount_configuration_costcenter", "accountconfigccunkid", mintAccountconfigccunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (12 Oct 2011) -- End

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (praccount_configuration_costcenter) </purpose>
    Public Function Update(ByVal objDataOperation As clsDataOperation) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@accountconfigccunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountconfigccunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid.ToString)
            objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactiontype_Id.ToString)
            'Sohail (14 Nov 2011) -- Start
            objDataOperation.AddParameter("@referencecodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencecodeid.ToString)
            objDataOperation.AddParameter("@referencenameid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencenameid.ToString)
            objDataOperation.AddParameter("@referencetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencetypeid.ToString)
            'Sohail (14 Nov 2011) -- End
            objDataOperation.AddParameter("@shortname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname.ToString) 'Sohail (13 Mar 2013)
            objDataOperation.AddParameter("@shortname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname2.ToString) 'Sohail (08 Jul 2017)
            objDataOperation.AddParameter("@shortname3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname3.ToString) 'Sohail (03 Mar 2020)
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
            objDataOperation.AddParameter("@usedefaultmapping", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnUsedefaultmapping)
            objDataOperation.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationbyId)
            objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationUnkId)
            'Sohail (03 Jul 2020) -- End

            'Gajanan [24-Aug-2020] -- Start
            'NMB Enhancement : Allow to set account configuration mapping 
            'inactive for closed period to allow to map head on other account 
            'configuration screen from new period
            objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsInactive.ToString)
            'Gajanan [24-Aug-2020] -- End


            strQ = "UPDATE praccount_configuration_costcenter SET " & _
              "  tranheadunkid = @tranheadunkid" & _
              ", costcenterunkid = @costcenterunkid" & _
              ", accountunkid = @accountunkid" & _
              ", isactive = @isactive" & _
              ", transactiontype_Id = @transactiontype_Id " & _
              ", referencecodeid = @referencecodeid" & _
              ", referencenameid = @referencenameid" & _
              ", referencetypeid = @referencetypeid " & _
              ", shortname = @shortname " & _
              ", shortname2 = @shortname2 " & _
              ", shortname3 = @shortname3 " & _
              ", periodunkid = @periodunkid " & _
              ", usedefaultmapping = @usedefaultmapping " & _
              ", allocationbyid = @allocationbyid " & _
              ", allocationunkid = @allocationunkid " & _
              ", isinactive = @isinactive " & _
            "WHERE accountconfigccunkid = @accountconfigccunkid "
            'Sohail (03 Jul 2020) = [periodunkid, usedefaultmapping, allocationbyid, allocationunkid]
            'Sohail (03 Mar 2020) - [shortname3]
            'Sohail (14 Nov 2011) - [referencecodeid, referencenameid, referencetypeid]
            'Sohail (08 Jul 2017) - [shortname2]
            'Sohail (13 Mar 2013) - [shortname]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (12 Oct 2011) -- Start
            Dim blnToInsert As Boolean
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "praccount_configuration_costcenter", mintAccountconfigccunkid, "accountconfigccunkid", 2) Then
                blnToInsert = True
            End If
            If blnToInsert = True AndAlso clsCommonATLog.Insert_AtLog(objDataOperation, 2, "praccount_configuration_costcenter", "accountconfigccunkid", mintAccountconfigccunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (12 Oct 2011) -- End

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (praccount_configuration_costcenter) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            strQ = "Update  praccount_configuration_costcenter set isactive = 0" & _
            "WHERE accountconfigccunkid = @accountconfigccunkid "

            objDataOperation.AddParameter("@accountconfigccunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (12 Oct 2011) -- Start
            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "praccount_configuration_costcenter", "accountconfigccunkid", intUnkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
            Return True
            End If
            'Sohail (12 Oct 2011) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (30 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    Public Function VoidAll(ByVal strUnkIDs As String) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE  praccount_configuration_costcenter " & _
                   "SET     isactive = 0 " & _
                   "WHERE   accountconfigccunkid IN ( " & strUnkIDs & " ) "


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim arrID() As String = strUnkIDs.Split(",")
            For i = 0 To arrID.Count - 1
                If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "praccount_configuration_costcenter", "accountconfigccunkid", CInt(arrID(i))) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Next
            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoidAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (30 Aug 2013) -- End


    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Public Function InactiveAll(ByVal strUnkIDs As String, ByVal intPeriodid As Integer) As Boolean

        'If isInactive(strUnkIDs) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Some of the account of selected transactions are already inactive.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim newIds As String = ""

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "INSERT INTO praccount_configuration_costcenter (" & _
                   " tranheadunkid " & _
                   ",costcenterunkid  " & _
                   ",accountunkid  " & _
                   ",isactive " & _
                   ",transactiontype_Id " & _
                   ",referencecodeid " & _
                   ",referencenameid " & _
                   ",referencetypeid " & _
                   ",shortname " & _
                   ",shortname2 " & _
                   ",shortname3 " & _
                   ",periodunkid " & _
                   ",allocationbyid " & _
                   ",usedefaultmapping " & _
                   ",allocationunkid " & _
                   ",isinactive " & _
                   ") select  " & _
                   " tranheadunkid " & _
                   ",costcenterunkid  " & _
                   ",accountunkid  " & _
                   ",@isactive " & _
                   ",transactiontype_Id " & _
                   ",referencecodeid " & _
                   ",referencenameid " & _
                   ",referencetypeid " & _
                   ",shortname " & _
                   ",shortname2 " & _
                   ",shortname3 " & _
                   ",@periodunkid " & _
                   ",allocationbyid " & _
                   ",usedefaultmapping " & _
                   ",allocationunkid " & _
                   ",@isinactive " & _
                   " from praccount_configuration_costcenter " & _
                   "WHERE accountconfigccunkid = @accountconfigccunkid " & _
                   "; SELECT @@identity"

            For Each id As String In strUnkIDs.Split(",")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodid)
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@accountconfigccunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(id))

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If newIds.Trim.Length <= 0 Then
                    newIds = dsList.Tables(0).Rows(0).Item(0)
                Else
                    newIds &= "," & dsList.Tables(0).Rows(0).Item(0)
                End If
                dsList = Nothing
            Next

            Dim arrID() As String = newIds.Split(",")
            For i = 0 To arrID.Count - 1
                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "praccount_configuration_costcenter", "accountconfigccunkid", CInt(arrID(i))) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Next
            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InactiveAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isInactive(ByVal strUnkIDs As String) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  accountconfigccunkid " & _
              "FROM praccount_configuration_costcenter " & _
              "WHERE isactive = 1 and isinactive = 1 and accountconfigccunkid IN ( " & strUnkIDs & " ) "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isInactive; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Gajanan [24-Aug-2020] -- End


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@accountconfigccunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intTranHeadTypeID As Integer, ByVal Tranheadunkid As Integer, ByVal Costcenterunkid As Integer, ByVal intPeriodUnkId As Integer, ByVal intAllocationById As Integer, ByVal intAllocationUnkId As Integer, Optional ByVal Accountunkid As Integer = -1, Optional ByVal intunkid As Integer = -1) As Boolean
        'Sohail (03 Jul 2020) - [intAllocationById, intPeriodUnkId, intAllocationUnkId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            objDataOperation = New clsDataOperation

            objDataOperation.ClearParameters()

            strQ = "SELECT " & _
              "  accountconfigccunkid " & _
              ", tranheadunkid " & _
              ", costcenterunkid " & _
              ", accountunkid " & _
              ", isactive " & _
              ", transactiontype_Id " & _
              ", referencecodeid " & _
              ", referencenameid " & _
              ", referencetypeid " & _
             "FROM praccount_configuration_costcenter " & _
             "WHERE transactiontype_Id = @transactiontype_Id " & _
             "AND  isactive = 1 " 'Sohail (02 Aug 2011), 'Sohail (14 Nov 2011) - [referencecodeid, referencenameid, referencetypeid]

            'Sohail (02 Aug 2011) -- Start
            If Tranheadunkid > 0 Then
                strQ &= " AND tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Tranheadunkid)
            End If
            'Sohail (14 Nov 2011) -- Start
            'If Costcenterunkid > 0 Then 'Sohail (25 Jul 2020)
                'If intTranHeadTypeID = enJVTransactionType.TRANSACTION_HEAD Then
                'Sohail (14 Nov 2011) -- End
                strQ &= " AND costcenterunkid = @costcenterunkid "
                objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Costcenterunkid)
            'End If 'Sohail (25 Jul 2020)
            'Sohail (02 Aug 2011) -- End

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            If intAllocationById > 0 Then
                strQ &= " AND praccount_configuration_costcenter.allocationbyid = @allocationbyid "
                objDataOperation.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationById)
            End If

            If intAllocationUnkId > 0 Then
                strQ &= " AND praccount_configuration_costcenter.allocationunkid = @allocationunkid "
                objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkId)
            End If

            If intPeriodUnkId > 0 Then
                strQ &= " AND praccount_configuration_costcenter.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If
            'Sohail (03 Jul 2020) -- End

            If Accountunkid > 0 Then
                strQ &= " AND accountunkid = @accountunkid"
                objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Accountunkid)
            End If

            If intunkid > 0 Then
                strQ &= " AND accountconfigccunkid <> @accountconfigccunkid"
                objDataOperation.AddParameter("@accountconfigccunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intunkid)
            End If

            'Sohail (02 Aug 2011) -- Start
            'objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Tranheadunkid)
            'objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Costcenterunkid)
            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadTypeID)
            'Sohail (02 Aug 2011) -- End
            
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function

    'Sohail (03 Jul 2020) -- Start
    'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
    Public Function isExistForOtherAllocation(ByVal intTranHeadTypeID As Integer, ByVal strTranheadIDs As String, ByVal intAllocationById As Integer, ByVal intPeriodUnkId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            objDataOperation = New clsDataOperation

            objDataOperation.ClearParameters()

            strQ = "SELECT " & _
              "  accountconfigccunkid " & _
              ", tranheadunkid " & _
              ", costcenterunkid " & _
              ", accountunkid " & _
              ", isactive " & _
              ", transactiontype_Id " & _
              ", referencecodeid " & _
              ", referencenameid " & _
              ", referencetypeid " & _
             "FROM praccount_configuration_costcenter " & _
             "WHERE isactive = 1 " & _
             "AND transactiontype_Id = @transactiontype_Id "

            If strTranheadIDs.Trim <> "" Then
                strQ &= " AND tranheadunkid IN (" & strTranheadIDs & ") "
            End If

            If intPeriodUnkId > 0 Then
                strQ &= " AND praccount_configuration_costcenter.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If

            If intAllocationById > 0 Then
                strQ &= " AND praccount_configuration_costcenter.allocationbyid <> @allocationbyid "
                objDataOperation.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationById)
            End If

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadTypeID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistForOtherAllocation; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function
    'Sohail (03 Jul 2020) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetCCAccountConfigUnkId(ByVal intTranHeadTypeID As Integer, ByVal TranHeadunkid As Integer, ByVal intCostcenterunkid As Integer, ByVal intPeriodUnkId As Integer, ByVal intAllocationById As Integer, ByVal intAllocationUnkId As Integer) As Integer
        'Sohail (03 Jul 2020) - [intAllocationById, intPeriodUnkId, intAllocationUnkId]
        'Sohail (09 Jul 2013) - [intCostcenterunkid]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intUnkId As Integer = -1

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  accountconfigccunkid " & _
             "FROM praccount_configuration_costcenter " & _
             "WHERE transactiontype_Id = @transactiontype_Id  " & _
             "AND isactive = 1"

            'Sohail (02 Aug 2011) -- Start
            If TranHeadunkid > 0 Then
                strQ &= " AND tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, TranHeadunkid)
            End If
            'If intTranHeadTypeID = enJVTransactionType.TRANSACTION_HEAD Then
            '    strQ &= " AND costcenterunkid = @costcenterunkid "
            '    objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid)
            'End If
            'Sohail (02 Aug 2011) -- End

            'Sohail (09 Jul 2013) -- Start
            'TRA - ENHANCEMENT
            If intCostcenterunkid > 0 Then
                strQ &= " AND costcenterunkid = @costcenterunkid "
                objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCostcenterunkid)
            End If
            'Sohail (09 Jul 2013) -- End

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            If intAllocationById > 0 Then
                strQ &= " AND praccount_configuration_costcenter.allocationbyid = @allocationbyid "
                objDataOperation.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationById)
            End If

            If intAllocationUnkId > 0 Then
                strQ &= " AND praccount_configuration_costcenter.allocationunkid = @allocationunkid "
                objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationUnkId)
            End If

            If intPeriodUnkId > 0 Then
                strQ &= " AND praccount_configuration_costcenter.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If
            'Sohail (03 Jul 2020) -- End

            'Sohail (02 Aug 2011) -- Start
            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadTypeID)
            'objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, TranHeadunkid)
            'objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid)
            'Sohail (02 Aug 2011) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                intUnkId = CInt(dsList.Tables(0).Rows(0)("accountconfigccunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCCAccountConfigUnkId; Module Name: " & mstrModuleName)
        End Try
        Return intUnkId
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Account Configuration for this Transaction Head is already defined. Please define new Cost Center Account Configuration.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Some of the account of selected transactions are already inactive.")

		Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class