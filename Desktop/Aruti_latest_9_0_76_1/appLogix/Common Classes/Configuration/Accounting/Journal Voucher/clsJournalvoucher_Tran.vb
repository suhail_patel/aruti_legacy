﻿'************************************************************************************************************************************
'Class Name : clsJournalvoucher_Tran.vb
'Purpose    :
'Date       :02/09/2010
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsJournalvoucher_Tran
    Private Const mstrModuleName = "clsJournalvoucher_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    'Private mintJournalvouchertranunkid As Integer
    Private mintJournalvoucherunkid As Integer
    'Private mintAccountunkid As Integer
    'Private mintDebitcredit_Id As Integer
    'Private mdblAmount As Double
    'Private mintUserunkid As Integer
    'Private mblnIsvoid As Boolean
    'Private mintVoiduserunkid As Integer
    'Private mdtVoiddatetime As Date
    'Private mstrVoidreason As String = String.Empty

    Private mdtTran As DataTable
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set journalvoucherunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Journalvoucherunkid() As Integer
        Get
            Return mintJournalvoucherunkid
        End Get
        Set(ByVal value As Integer)
            mintJournalvoucherunkid = value
            Call getJVTran()
        End Set
    End Property

    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set journalvouchertranunkid
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Journalvouchertranunkid() As Integer
    '    Get
    '        Return mintJournalvouchertranunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintJournalvouchertranunkid = Value
    '        Call getData()
    '    End Set
    'End Property


    '''' <summary>
    '''' Purpose: Get or Set accountunkid
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Accountunkid() As Integer
    '    Get
    '        Return mintAccountunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintAccountunkid = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set debitcredit_id
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Debitcredit_Id() As Integer
    '    Get
    '        Return mintDebitcredit_Id
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintDebitcredit_Id = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set amount
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Amount() As Double
    '    Get
    '        Return mdblAmount
    '    End Get
    '    Set(ByVal value As Double)
    '        mdblAmount = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set userunkid
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Userunkid() As Integer
    '    Get
    '        Return mintUserunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintUserunkid = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set isvoid
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Isvoid() As Boolean
    '    Get
    '        Return mblnIsvoid
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnIsvoid = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set voiduserunkid
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Voiduserunkid() As Integer
    '    Get
    '        Return mintVoiduserunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintVoiduserunkid = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set voiddatetime
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Voiddatetime() As Date
    '    Get
    '        Return mdtVoiddatetime
    '    End Get
    '    Set(ByVal value As Date)
    '        mdtVoiddatetime = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set voidreason
    '''' Modify By: Sohail
    '''' </summary>
    'Public Property _Voidreason() As String
    '    Get
    '        Return mstrVoidreason
    '    End Get
    '    Set(ByVal value As String)
    '        mstrVoidreason = Value
    '    End Set
    'End Property

#End Region

#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("BatchTran")
        Try
            With mdtTran
                .Columns.Add(New DataColumn("journalvouchertranunkid", System.Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("journalvoucherunkid", System.Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("accountunkid", System.Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("debitcredit_id", System.Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("amount", System.Type.GetType("System.Decimal")))
                .Columns.Add(New DataColumn("userunkid", System.Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("isvoid", System.Type.GetType("System.Boolean")))
                .Columns.Add(New DataColumn("voiduserunkid", System.Type.GetType("System.Int32")))
                .Columns.Add(New DataColumn("voiddatetime", System.Type.GetType("System.DateTime")))
                .Columns.Add(New DataColumn("voidreason", System.Type.GetType("System.String")))
                .Columns.Add(New DataColumn("GUID", System.Type.GetType("System.String")))
                .Columns.Add(New DataColumn("AUD", System.Type.GetType("System.String")))
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

    Private Sub getJVTran()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim drawJVInfo As DataRow
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            strQ = "SELECT  prjournalvoucher_tran.journalvouchertranunkid " & _
                          ", prjournalvoucher_tran.journalvoucherunkid " & _
                          ", prjournalvoucher_tran.accountunkid " & _
                          ", prjournalvoucher_tran.debitcredit_id " & _
                          ", prjournalvoucher_tran.amount " & _
                          ", prjournalvoucher_tran.userunkid " & _
                          ", prjournalvoucher_tran.isvoid " & _
                          ", prjournalvoucher_tran.voiduserunkid " & _
                          ", prjournalvoucher_tran.voiddatetime " & _
                          ", prjournalvoucher_tran.voidreason " & _
                          ", '' As AUD " & _
                    "FROM    prjournalvoucher_tran " & _
                    "WHERE   journalvoucherunkid = @journalvoucherunkid " & _
                            "AND isvoid = 0 "

            objDataOperation.AddParameter("@journalvoucherunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJournalvoucherunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    drawJVInfo = mdtTran.NewRow()
                    drawJVInfo.Item("journalvouchertranunkid") = .Item("journalvouchertranunkid")
                    drawJVInfo.Item("journalvoucherunkid") = .Item("journalvoucherunkid")
                    drawJVInfo.Item("accountunkid") = .Item("accountunkid")
                    drawJVInfo.Item("debitcredit_id") = .Item("debitcredit_id")
                    drawJVInfo.Item("amount") = .Item("amount")
                    drawJVInfo.Item("userunkid") = .Item("userunkid")
                    drawJVInfo.Item("isvoid") = .Item("isvoid")
                    drawJVInfo.Item("voiduserunkid") = .Item("voiduserunkid")
                    drawJVInfo.Item("voiddatetime") = .Item("voiddatetime")
                    drawJVInfo.Item("voidreason") = .Item("voidreason")
                    drawJVInfo.Item("AUD") = .Item("AUD")

                    mdtTran.Rows.Add(drawJVInfo)
                End With
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getJVTran", mstrModuleName)
        Finally
            exForce = Nothing
        End Try
    End Sub

    Public Function InserUpdateDeleteJVTran() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then

                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO prjournalvoucher_tran ( " & _
                                              "  journalvoucherunkid " & _
                                              ", accountunkid " & _
                                              ", debitcredit_id " & _
                                              ", amount " & _
                                              ", userunkid " & _
                                              ", isvoid " & _
                                              ", voiduserunkid " & _
                                              ", voiddatetime " & _
                                              ", voidreason" & _
                                        ") VALUES (" & _
                                              "  @journalvoucherunkid " & _
                                              ", @accountunkid " & _
                                              ", @debitcredit_id " & _
                                              ", @amount " & _
                                              ", @userunkid " & _
                                              ", @isvoid " & _
                                              ", @voiduserunkid " & _
                                              ", @voiddatetime " & _
                                              ", @voidreason" & _
                                        "); SELECT @@identity"

                                objDataOperation.AddParameter("@journalvoucherunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJournalvoucherunkid.ToString)
                                objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("accountunkid").ToString)
                                objDataOperation.AddParameter("@debitcredit_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("debitcredit_id").ToString)
                                objDataOperation.AddParameter("@amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, .Item("amount").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If .Item("voiddatetime").ToString = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "U"
                                strQ = "UPDATE prjournalvoucher_tran SET " & _
                                  "  journalvoucherunkid = @journalvoucherunkid" & _
                                  ", accountunkid = @accountunkid" & _
                                  ", debitcredit_id = @debitcredit_id" & _
                                  ", amount = @amount" & _
                                  ", userunkid = @userunkid" & _
                                  ", isvoid = @isvoid" & _
                                  ", voiduserunkid = @voiduserunkid" & _
                                  ", voiddatetime = @voiddatetime" & _
                                  ", voidreason = @voidreason " & _
                                "WHERE journalvouchertranunkid = @journalvouchertranunkid "

                                objDataOperation.AddParameter("@journalvouchertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("journalvouchertranunkid").ToString)
                                objDataOperation.AddParameter("@journalvoucherunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJournalvoucherunkid.ToString)
                                objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("accountunkid").ToString)
                                objDataOperation.AddParameter("@debitcredit_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("debitcredit_id").ToString)
                                objDataOperation.AddParameter("@amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, .Item("amount").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If .Item("voiddatetime").ToString = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"
                                strQ = "DELETE FROM prjournalvoucher_tran " & _
                                "WHERE journalvouchertranunkid = @journalvouchertranunkid "

                                objDataOperation.AddParameter("@journalvouchertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("journalvouchertranunkid").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If
                End With
            Next

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InserUpdateDeleteJVTran", mstrModuleName)
            objDataOperation.ReleaseTransaction(False)
            Return False
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Void(ByVal intJVUnkID As Integer, _
                         ByVal intVoidUserID As Integer, _
                         ByVal mdtVoidDateTime As DateTime, _
                         ByVal strVoidReason As String) As Boolean
        Dim exForce As Exception
        Dim strQ As String = ""
        Try

            objDataOperation = New clsDataOperation
            strQ = "UPDATE prbatch_transaction_tran SET " & _
                    "   isvoid=1 " & _
                    ",  voiduserunkid=@voiduserunkid " & _
                    ",  voiddatetime=@voiddatetime " & _
                    ", voidreason = @voidreason " & _
                    "WHERE batchtransactionunkid=@batchtransactionunkid "
            strQ = "UPDATE prjournalvoucher_tran SET " & _
                         " isvoid = 1 " & _
                         ", voiduserunkid = @voiduserunkid" & _
                         ", voiddatetime = @voiddatetime" & _
                         ", voidreason = @voidreason " & _
                    "WHERE journalvoucherunkid = @journalvoucherunkid "

            If mdtVoidDateTime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtVoidDateTime) & " " & Format(mdtVoidDateTime, "HH:mm:ss"))
            End If
            objDataOperation.AddParameter("@journalvoucherunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJVUnkID)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Void", mstrModuleName)
            Return False
        Finally
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function



    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Sub GetData()
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  journalvouchertranunkid " & _
    '          ", journalvoucherunkid " & _
    '          ", accountunkid " & _
    '          ", debitcredit_id " & _
    '          ", amount " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '         "FROM prjournalvoucher_tran " & _
    '         "WHERE journalvouchertranunkid = @journalvouchertranunkid "

    '        objDataOperation.AddParameter("@journalvouchertranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintJournalvoucherTranUnkId.ToString)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        For Each dtRow As DataRow In dsList.Tables(0).Rows
    '            mintjournalvouchertranunkid = CInt(dtRow.Item("journalvouchertranunkid"))
    '            mintjournalvoucherunkid = CInt(dtRow.Item("journalvoucherunkid"))
    '            mintaccountunkid = CInt(dtRow.Item("accountunkid"))
    '            mintdebitcredit_id = CInt(dtRow.Item("debitcredit_id"))
    '            mdblamount = cdec(dtRow.Item("amount"))
    '            mintuserunkid = CInt(dtRow.Item("userunkid"))
    '            mblnisvoid = CBool(dtRow.Item("isvoid"))
    '            mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
    '            mdtvoiddatetime = dtRow.Item("voiddatetime")
    '            mstrvoidreason = dtRow.Item("voidreason").ToString
    '            Exit For
    '        Next
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Sub


    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  journalvouchertranunkid " & _
    '          ", journalvoucherunkid " & _
    '          ", accountunkid " & _
    '          ", debitcredit_id " & _
    '          ", amount " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '         "FROM prjournalvoucher_tran "

    '        If blnOnlyActive Then
    '            strQ &= " WHERE isactive = 1 "
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function


    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (prjournalvoucher_tran) </purpose>
    'Public Function Insert() As Boolean
    '    'If isExist(mstrName) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@journalvoucherunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintjournalvoucherunkid.ToString)
    '        objDataOperation.AddParameter("@accountunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintaccountunkid.ToString)
    '        objDataOperation.AddParameter("@debitcredit_id", SqlDbType.int, eZeeDataType.INT_SIZE, mintdebitcredit_id.ToString)
    '        objDataOperation.AddParameter("@amount", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblamount.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

    '        StrQ = "INSERT INTO prjournalvoucher_tran ( " & _
    '          "  journalvoucherunkid " & _
    '          ", accountunkid " & _
    '          ", debitcredit_id " & _
    '          ", amount " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason" & _
    '        ") VALUES (" & _
    '          "  @journalvoucherunkid " & _
    '          ", @accountunkid " & _
    '          ", @debitcredit_id " & _
    '          ", @amount " & _
    '          ", @userunkid " & _
    '          ", @isvoid " & _
    '          ", @voiduserunkid " & _
    '          ", @voiddatetime " & _
    '          ", @voidreason" & _
    '        "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintJournalvoucherTranUnkId = dsList.Tables(0).Rows(0).Item(0)

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (prjournalvoucher_tran) </purpose>
    'Public Function Update() As Boolean
    '    'If isExist(mstrName, mintJournalvouchertranunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@journalvouchertranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintjournalvouchertranunkid.ToString)
    '        objDataOperation.AddParameter("@journalvoucherunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintjournalvoucherunkid.ToString)
    '        objDataOperation.AddParameter("@accountunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintaccountunkid.ToString)
    '        objDataOperation.AddParameter("@debitcredit_id", SqlDbType.int, eZeeDataType.INT_SIZE, mintdebitcredit_id.ToString)
    '        objDataOperation.AddParameter("@amount", SqlDbType.money, eZeeDataType.MONEY_SIZE, mdblamount.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

    '        StrQ = "UPDATE prjournalvoucher_tran SET " & _
    '          "  journalvoucherunkid = @journalvoucherunkid" & _
    '          ", accountunkid = @accountunkid" & _
    '          ", debitcredit_id = @debitcredit_id" & _
    '          ", amount = @amount" & _
    '          ", userunkid = @userunkid" & _
    '          ", isvoid = @isvoid" & _
    '          ", voiduserunkid = @voiduserunkid" & _
    '          ", voiddatetime = @voiddatetime" & _
    '          ", voidreason = @voidreason " & _
    '        "WHERE journalvouchertranunkid = @journalvouchertranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (prjournalvoucher_tran) </purpose>
    'Public Function Delete(ByVal intUnkid As Integer) As Boolean
    '    If isUsed(intUnkid) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "DELETE FROM prjournalvoucher_tran " & _
    '        "WHERE journalvouchertranunkid = @journalvouchertranunkid "

    '        objDataOperation.AddParameter("@journalvouchertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@journalvouchertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  journalvouchertranunkid " & _
    '          ", journalvoucherunkid " & _
    '          ", accountunkid " & _
    '          ", debitcredit_id " & _
    '          ", amount " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '         "FROM prjournalvoucher_tran " & _
    '         "WHERE name = @name " & _
    '         "AND code = @code "

    '        If intUnkid > 0 Then
    '            strQ &= " AND journalvouchertranunkid <> @journalvouchertranunkid"
    '        End If

    '        objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@journalvouchertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

End Class