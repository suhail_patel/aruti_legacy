﻿'************************************************************************************************************************************
'Class Name : clsPasswordResetRequest.vb
'Purpose    :
'Date       :19-OCT-2021
'Written By :Sandeep
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports System.Globalization

''' <summary>
''' Purpose: 
''' Developer: Sandeep
''' </summary>
Public Class clsPasswordResetRequest
    Private Shared ReadOnly mstrModuleName As String = "clsPasswordResetRequest"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private Variables "

    Private mintUserunkind As Integer = 0
    Private mintEmployeeunkid As Integer = 0
    Private mintCompanyunkid As Integer = 0
    Private mdtRequestdate As DateTime = Nothing
    Private mstrLinkissued As String = ""
    Private mblnIsaccessed As Boolean = False
    Private mblnIschanged As Boolean = False
    Private mdtChangedatetime As DateTime = Nothing
    Private mstrIP As String = ""
    Private mstrHost As String = ""
    Private mstrFormName As String = ""
    Private mintUniquecode As Int64 = 0

#End Region

#Region " Properties "

    Public Property _Userunkind() As Integer
        Get
            Return mintUserunkind
        End Get
        Set(ByVal value As Integer)
            mintUserunkind = value
        End Set
    End Property

    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Public Property _Companyunkid() As Integer
        Get
            Return mintCompanyunkid
        End Get
        Set(ByVal value As Integer)
            mintCompanyunkid = value
        End Set
    End Property

    Public Property _Requestdate() As DateTime
        Get
            Return mdtRequestdate
        End Get
        Set(ByVal value As DateTime)
            mdtRequestdate = value
        End Set
    End Property

    Public Property _Linkissued() As String
        Get
            Return mstrLinkissued
        End Get
        Set(ByVal value As String)
            mstrLinkissued = value
        End Set
    End Property

    Public Property _Isaccessed() As Boolean
        Get
            Return mblnIsaccessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsaccessed = value
        End Set
    End Property

    Public Property _Ischanged() As Boolean
        Get
            Return mblnIschanged
        End Get
        Set(ByVal value As Boolean)
            mblnIschanged = value
        End Set
    End Property

    Public Property _Changedatetime() As DateTime
        Get
            Return mdtChangedatetime
        End Get
        Set(ByVal value As DateTime)
            mdtChangedatetime = value
        End Set
    End Property

    Public Property _IP() As String
        Get
            Return mstrIP
        End Get
        Set(ByVal value As String)
            mstrIP = value
        End Set
    End Property

    Public Property _Host() As String
        Get
            Return mstrHost
        End Get
        Set(ByVal value As String)
            mstrHost = value
        End Set
    End Property

    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public Property _Uniquecode() As Int64
        Get
            Return mintUniquecode
        End Get
        Set(ByVal value As Int64)
            mintUniquecode = value
        End Set
    End Property

#End Region

    Public Function GenerateUniqueCode() As Int64
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Dim xData As New DataSet
        Dim uNum As Int64 = 0
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT uniquecode FROM hrmsConfiguration..cfpwdreset_request "

                xData = objDo.ExecQuery(StrQ, "lst")

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

                Dim iList As New List(Of Int64)

                iList = xData.Tables("lst").AsEnumerable().Select(Function(x) x.Field(Of Int64)("uniquecode")).ToList()

                Dim ci As CultureInfo = CultureInfo.InvariantCulture

                uNum = Now.ToString("HHmmssfff", ci)

                While iList.Contains(uNum)
                    uNum = Now.ToString("HHmmssfff", ci)
                End While

            End Using
            Return uNum
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GenerateUniqueCode; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Function Insert() As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Try
            Using objDo As New clsDataOperation

                StrQ = "INSERT INTO hrmsConfiguration..cfpwdreset_request " & _
                       "( " & _
                       "     userunkind " & _
                       "    ,employeeunkid " & _
                       "    ,companyunkid " & _
                       "    ,requestdate " & _
                       "    ,linkissued " & _
                       "    ,isaccessed " & _
                       "    ,ischanged " & _
                       "    ,changedatetime " & _
                       "    ,uniquecode " & _
                       "    ,ip " & _
                       "    ,host " & _
                       "    ,form_name " & _
                       ") " & _
                       "VALUES " & _
                       "( " & _
                        "    @userunkind " & _
                       "    ,@employeeunkid " & _
                       "    ,@companyunkid " & _
                       "    ,@requestdate " & _
                       "    ,@linkissued " & _
                       "    ,@isaccessed " & _
                       "    ,@ischanged " & _
                       "    ,@changedatetime " & _
                       "    ,@uniquecode " & _
                       "    ,@ip " & _
                       "    ,@host " & _
                       "    ,@form_name " & _
                       ") "

                objDo.AddParameter("@userunkind", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkind)
                objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                objDo.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid)
                objDo.AddParameter("@requestdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRequestdate)
                objDo.AddParameter("@linkissued", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrLinkissued)
                objDo.AddParameter("@isaccessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsaccessed)
                objDo.AddParameter("@ischanged", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIschanged)
                If mdtChangedatetime <> Nothing Then
                    objDo.AddParameter("@changedatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtChangedatetime)
                Else
                    objDo.AddParameter("@changedatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                objDo.AddParameter("@uniquecode", SqlDbType.BigInt, mintUniquecode.ToString.Length, mintUniquecode)
                objDo.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIP)
                objDo.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHost)
                objDo.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GenerateUniqueCode; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Function Update() As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Try
            Using objDo As New clsDataOperation
                StrQ = "UPDATE hrmsConfiguration..cfpwdreset_request SET " & _
                       "     isaccessed = @isaccessed " & _
                       "    ,ischanged = @ischanged " & _
                       "    ,changedatetime = @changedatetime " & _
                       "WHERE uniquecode = @uniquecode "

                objDo.AddParameter("@isaccessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsaccessed)
                objDo.AddParameter("@ischanged", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIschanged)
                If mdtChangedatetime <> Nothing Then
                    objDo.AddParameter("@changedatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtChangedatetime)
                Else
                    objDo.AddParameter("@changedatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                objDo.AddParameter("@uniquecode", SqlDbType.BigInt, mintUniquecode.ToString.Length, mintUniquecode)        

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If
            End Using
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Function IsRequestExists(ByVal intEmployeeId As Integer) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Dim blnFlag As Boolean = False
        Dim xData As New DataSet
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT 1 FROM hrmsConfiguration..cfpwdreset_request WHERE  employeeunkid = @employeeunkid AND DATEDIFF(HOUR,requestdate,GETDATE()) < 24 AND ischanged = 0 "

                objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                xData = objDo.ExecQuery(StrQ, "lst")

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

                If xData.Tables(0).Rows.Count > 0 Then blnFlag = True

            End Using
            Return blnFlag
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsRequestExists; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Function IsValidLink(ByVal bintUniquecode As Int64, ByRef xMsg As String) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Dim blnFlag As Boolean = True
        Dim xData As New DataSet
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT requestdate,ischanged FROM hrmsConfiguration..cfpwdreset_request WHERE uniquecode = @uniquecode "

                objDo.AddParameter("@uniquecode", SqlDbType.BigInt, bintUniquecode.ToString.Length, bintUniquecode)
                xData = objDo.ExecQuery(StrQ, "lst")

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

                If xData.Tables(0).Rows.Count <= 0 Then
                    xMsg = Language.getMessage(mstrModuleName, 100, "Sorry, Invalid Link. Please check the link and reset password again.")
                    blnFlag = False
                End If


                If xData.Tables(0).Rows.Count > 0 Then
                    If DateDiff(DateInterval.Minute, CType(xData.Tables(0).Rows(0)("requestdate"), DateTime), Now) > 1440 Then
                        xMsg = Language.getMessage(mstrModuleName, 101, "Sorry, Link has been expired. Please click forgot password again to generate new link.")
                        blnFlag = False
                    End If
                    If CBool(xData.Tables(0).Rows(0)("ischanged")) = True Then
                        xMsg = Language.getMessage(mstrModuleName, 103, "Sorry, You have already used this link to reset your password. Please click forgot password again to generate new link.")
                        blnFlag = False
                    End If
                End If

            End Using
            Return blnFlag
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValidLink; Module Name: " & mstrModuleName)
        End Try
    End Function

End Class
