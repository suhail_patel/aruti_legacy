﻿'************************************************************************************************************************************
'Class Name : clsannouncement_acknowledgement.vb
'Purpose    :
'Date       :01/09/2021
'Written By :Pinkal Jariwal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwal
''' </summary>
Public Class clsannouncement_acknowledgement
    Private Const mstrModuleName = "clsannouncement_acknowledgement"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mstrRow_Guid As String = String.Empty
    Private mintAnnouncementunkid As Integer
    Private mdtTransactiondate As Date
    Private mintUserunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mblnIsdownload As Boolean
    Private mstrIp As String = String.Empty
    Private mstrMachine_Name As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set row_guid
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    Public Property _Row_Guid() As String
        Get
            Return mstrRow_Guid
        End Get
        Set(ByVal value As String)
            mstrRow_Guid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set announcementunkid
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    Public Property _Announcementunkid() As Integer
        Get
            Return mintAnnouncementunkid
        End Get
        Set(ByVal value As Integer)
            mintAnnouncementunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isdownload
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    Public Property _Isdownload() As Boolean
        Get
            Return mblnIsdownload
        End Get
        Set(ByVal value As Boolean)
            mblnIsdownload = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set machine_name
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    Public Property _Machine_Name() As String
        Get
            Return mstrMachine_Name
        End Get
        Set(ByVal value As String)
            mstrMachine_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  row_guid " & _
                      ", announcementunkid " & _
                      ", transactiondate " & _
                      ", userunkid " & _
                      ", employeeunkid " & _
                      ", isdownload " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", isweb " & _
                     "FROM hrmsConfiguration..cfannouncement_acknowledgement " & _
                     "WHERE row_guid = @row_guid "

            objDataOperation.AddParameter("@row_guid", SqlDbType.ntext, eZeeDataType.NAME_SIZE, mstrRow_guid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mstrrow_guid = dtRow.Item("row_guid").ToString
                mintannouncementunkid = CInt(dtRow.Item("announcementunkid"))
                mdttransactiondate = dtRow.Item("transactiondate")
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mblnisdownload = CBool(dtRow.Item("isdownload"))
                mstrip = dtRow.Item("ip").ToString
                mstrmachine_name = dtRow.Item("machine_name").ToString
                mstrform_name = dtRow.Item("form_name").ToString
                mblnisweb = CBool(dtRow.Item("isweb"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  row_guid " & _
              ", announcementunkid " & _
              ", transactiondate " & _
              ", userunkid " & _
              ", employeeunkid " & _
              ", isdownload " & _
              ", ip " & _
              ", machine_name " & _
              ", form_name " & _
              ", isweb " & _
             "FROM cfannouncement_acknowledgement "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfannouncement_acknowledgement) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@row_guid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRow_Guid.ToString)
            objDataOperation.AddParameter("@announcementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnnouncementunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isdownload", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsdownload.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMachine_Name.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)

            strQ = "INSERT INTO hrmsConfiguration..cfannouncement_acknowledgement ( " & _
                      "  row_guid " & _
                      ", announcementunkid " & _
                      ", transactiondate " & _
                      ", userunkid " & _
                      ", employeeunkid " & _
                      ", isdownload " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", isweb" & _
                    ") VALUES (" & _
                      "  @row_guid " & _
                      ", @announcementunkid " & _
                      ", @transactiondate " & _
                      ", @userunkid " & _
                      ", @employeeunkid " & _
                      ", @isdownload " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      ", @isweb" & _
                    ");"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfannouncement_acknowledgement) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@row_guid", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrRow_Guid.ToString)
            objDataOperation.AddParameter("@announcementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnnouncementunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isdownload", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsdownload.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMachine_Name.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)

            strQ = "UPDATE hrmsConfiguration..cfannouncement_acknowledgement SET " & _
                      "  announcementunkid = @announcementunkid" & _
                      ", transactiondate = @transactiondate" & _
                      ", userunkid = @userunkid" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", isdownload = @isdownload" & _
                      ", ip = @ip" & _
                      ", machine_name = @machine_name" & _
                      ", form_name = @form_name" & _
                      ", isweb = @isweb " & _
                      " WHERE row_guid = @row_guid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfannouncement_acknowledgement) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "DELETE FROM hrmsConfiguration..cfannouncement_acknowledgement " & _
                      " WHERE row_guid = @row_guid "

            objDataOperation.AddParameter("@row_guid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@row_guid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xAnnouncementId As Integer, ByVal xUserId As Integer, ByVal xEmployeeId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  cfannouncement_acknowledgement.row_guid " & _
                      ", cfannouncement_acknowledgement.announcementunkid " & _
                      ", cfannouncement_acknowledgement.transactiondate " & _
                      ", cfannouncement_acknowledgement.userunkid " & _
                      ", cfannouncement_acknowledgement.employeeunkid " & _
                      ", cfannouncement_acknowledgement.isdownload " & _
                      ", cfannouncement_acknowledgement.ip " & _
                      ", cfannouncement_acknowledgement.machine_name " & _
                      ", cfannouncement_acknowledgement.form_name " & _
                      ", cfannouncement_acknowledgement.isweb " & _
                      ", cfannouncement_master.requireacknowledgement " & _
                     " FROM hrmsConfiguration..cfannouncement_acknowledgement " & _
                     " JOIN hrmsConfiguration..cfannouncement_master ON cfannouncement_master.announcementunkid = hrmsConfiguration..cfannouncement_acknowledgement.announcementunkid " & _
                     " WHERE hrmsConfiguration..cfannouncement_acknowledgement.announcementunkid= @announcementunkid "

            If xUserId > 0 Then
                strQ &= " AND cfannouncement_acknowledgement.userunkid  = @userunkid "
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)
            End If

            If xEmployeeId > 0 Then
                strQ &= " AND cfannouncement_acknowledgement.employeeunkid  = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            End If

            objDataOperation.AddParameter("@announcementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAnnouncementId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

End Class