﻿'************************************************************************************************************************************
'Class Name : clsGroup_Master.vb
'Purpose    :
'Date       :2011-06-03
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsGroup_Master
    Private Shared ReadOnly mstrModuleName As String = "clsGroup_Master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintGroupunkid As Integer
    Private mstrGroupname As String = String.Empty
    Private mstrAddress1 As String = String.Empty
    Private mstrAddress2 As String = String.Empty
    Private mstrCity As String = String.Empty
    Private mstrZipcode As String = String.Empty
    Private mstrState As String = String.Empty
    Private mstrCountry As String = String.Empty
    Private mstrPhone As String = String.Empty
    Private mstrFax As String = String.Empty
    Private mstrEmail As String = String.Empty
    Private mstrWebsite As String = String.Empty
    Private mdtStartdate As Date
    Private mdtFreezdate As Date
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set groupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Groupunkid() As Integer
        Get
            Return mintGroupunkid
        End Get
        Set(ByVal value As Integer)
            mintGroupunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set groupname
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Groupname() As String
        Get
            Return mstrGroupname
        End Get
        Set(ByVal value As String)
            mstrGroupname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Address1() As String
        Get
            Return mstrAddress1
        End Get
        Set(ByVal value As String)
            mstrAddress1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Address2() As String
        Get
            Return mstrAddress2
        End Get
        Set(ByVal value As String)
            mstrAddress2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set city
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _City() As String
        Get
            Return mstrCity
        End Get
        Set(ByVal value As String)
            mstrCity = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set zipcode
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Zipcode() As String
        Get
            Return mstrZipcode
        End Get
        Set(ByVal value As String)
            mstrZipcode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set state
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _State() As String
        Get
            Return mstrState
        End Get
        Set(ByVal value As String)
            mstrState = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set country
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Country() As String
        Get
            Return mstrCountry
        End Get
        Set(ByVal value As String)
            mstrCountry = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set phone
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Phone() As String
        Get
            Return mstrPhone
        End Get
        Set(ByVal value As String)
            mstrPhone = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fax
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Fax() As String
        Get
            Return mstrFax
        End Get
        Set(ByVal value As String)
            mstrFax = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set email
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Email() As String
        Get
            Return mstrEmail
        End Get
        Set(ByVal value As String)
            mstrEmail = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set website
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Website() As String
        Get
            Return mstrWebsite
        End Get
        Set(ByVal value As String)
            mstrWebsite = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Startdate() As Date
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As Date)
            mdtStartdate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set freezdate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Freezdate() As Date
        Get
            Return mdtFreezdate
        End Get
        Set(ByVal value As Date)
            mdtFreezdate = Value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New(Optional ByVal IsFillInfo As Boolean = False)
        If IsFillInfo Then
            mintGroupunkid = 1
            Call GetData()
        End If
    End Sub

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                     "  groupunkid " & _
                     ", groupname " & _
                     ", address1 " & _
                     ", address2 " & _
                     ", city " & _
                     ", zipcode " & _
                     ", state " & _
                     ", country " & _
                     ", phone " & _
                     ", fax " & _
                     ", email " & _
                     ", website " & _
                     ", startdate " & _
                     ", freezdate " & _
                    "FROM hrmsconfiguration..cfgroup_master " & _
                    "WHERE groupunkid = @groupunkid "

            objDataOperation.AddParameter("@groupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintGroupUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintgroupunkid = CInt(dtRow.Item("groupunkid"))
                mstrgroupname = dtRow.Item("groupname").ToString
                mstraddress1 = dtRow.Item("address1").ToString
                mstraddress2 = dtRow.Item("address2").ToString
                mstrcity = dtRow.Item("city").ToString
                mstrzipcode = dtRow.Item("zipcode").ToString
                mstrstate = dtRow.Item("state").ToString
                mstrcountry = dtRow.Item("country").ToString
                mstrphone = dtRow.Item("phone").ToString
                mstrfax = dtRow.Item("fax").ToString
                mstremail = dtRow.Item("email").ToString
                mstrwebsite = dtRow.Item("website").ToString
                If IsDBNull(dtRow.Item("startdate")) Then
                    mdtStartdate = Nothing
                Else
                    mdtStartdate = dtRow.Item("startdate")
                End If

                If IsDBNull(dtRow.Item("freezdate")) Then
                    mdtFreezdate = Nothing
                Else
                    mdtFreezdate = dtRow.Item("freezdate")
                End If

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  groupunkid " & _
              ", groupname " & _
              ", address1 " & _
              ", address2 " & _
              ", city " & _
              ", zipcode " & _
              ", state " & _
              ", country " & _
              ", phone " & _
              ", fax " & _
              ", email " & _
              ", website " & _
              ", startdate " & _
              ", freezdate " & _
             "FROM hrmsconfiguration..cfgroup_master "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfgroup_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrGroupname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Group Name already exists. Please define new Group Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            
 'Sohail (07 Sep 2013) -- Start
            'TRA - ENHANCEMENT - To give 30 days demo when Aruti is installed very first time OR Clear Licence
            Dim objConfig As New clsConfigOptions
            If objConfig.IsKeyExist(0, "PayrollProcess") = True Then

                Dim strParameterDemoKey As String = "PayrollProcess" '7WJwOhm23sKevuWxPqT+KWnDtA3JD0a3nsx+VdUTlYz1cEq+dw+hAbphOf3xJIGN
                Dim strParameterActivateKey As String = "TnAActivate" '7WJwOhm23sKevuWxPqT+KRAcmRDYVV6gWmUO9S8mWgamUv0lfwMfwbckAf2ziVd07b8begW0Zl2Hv8QL7CzCoA==

                Dim strKey As String = mstrGroupname
                If strKey.Length > 4 Then
                    strKey = strKey.Substring(0, 1) + strKey.Substring(1, 1) + strKey.Substring(strKey.Length - 2, 1) + strKey.Substring(strKey.Length - 1, 1)
                End If

                Dim strParameterDemoValue As String = clsSecurity.Encrypt(objConfig.GetKeyValue(Nothing, "PayrollProcess"), strKey)
                Dim strParameterActivateValue As String = clsSecurity.Encrypt("Aruti Lic Not Activated", strKey)

                objDataOperation.ExecNonQuery("UPDATE hrmsconfiguration..cfconfiguration SET key_value = '" + strParameterDemoValue + "' WHERE key_name = '" + strParameterDemoKey + "'")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objDataOperation.ExecNonQuery("UPDATE hrmsconfiguration..cfconfiguration SET key_value = '" + strParameterActivateValue + "' WHERE key_name = '" + strParameterActivateKey + "'")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            objConfig = Nothing
            'Sohail (07 Sep 2013) -- End

            objDataOperation.AddParameter("@groupname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrgroupname.ToString)
            objDataOperation.AddParameter("@address1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraddress1.ToString)
            objDataOperation.AddParameter("@address2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraddress2.ToString)
            objDataOperation.AddParameter("@city", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcity.ToString)
            objDataOperation.AddParameter("@zipcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrzipcode.ToString)
            objDataOperation.AddParameter("@state", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrstate.ToString)
            objDataOperation.AddParameter("@country", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcountry.ToString)
            objDataOperation.AddParameter("@phone", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrphone.ToString)
            objDataOperation.AddParameter("@fax", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrfax.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstremail.ToString)
            objDataOperation.AddParameter("@website", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrwebsite.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            objDataOperation.AddParameter("@freezdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFreezdate)

            strQ = "INSERT INTO hrmsconfiguration..cfgroup_master ( " & _
                      "  groupname " & _
                      ", address1 " & _
                      ", address2 " & _
                      ", city " & _
                      ", zipcode " & _
                      ", state " & _
                      ", country " & _
                      ", phone " & _
                      ", fax " & _
                      ", email " & _
                      ", website " & _
                      ", startdate " & _
                      ", freezdate" & _
                    ") VALUES (" & _
                      "  @groupname " & _
                      ", @address1 " & _
                      ", @address2 " & _
                      ", @city " & _
                      ", @zipcode " & _
                      ", @state " & _
                      ", @country " & _
                      ", @phone " & _
                      ", @fax " & _
                      ", @email " & _
                      ", @website " & _
                      ", @startdate " & _
                      ", @freezdate" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintGroupUnkId = dsList.Tables(0).Rows(0).Item(0)

            

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfgroup_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrGroupname, mintGroupunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Group Name already exists. Please define new Group Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@groupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintgroupunkid.ToString)
            objDataOperation.AddParameter("@groupname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrgroupname.ToString)
            objDataOperation.AddParameter("@address1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraddress1.ToString)
            objDataOperation.AddParameter("@address2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstraddress2.ToString)
            objDataOperation.AddParameter("@city", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcity.ToString)
            objDataOperation.AddParameter("@zipcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrzipcode.ToString)
            objDataOperation.AddParameter("@state", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrstate.ToString)
            objDataOperation.AddParameter("@country", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcountry.ToString)
            objDataOperation.AddParameter("@phone", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrphone.ToString)
            objDataOperation.AddParameter("@fax", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrfax.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstremail.ToString)
            objDataOperation.AddParameter("@website", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebsite.ToString)
            If mdtStartdate = Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            End If
            If mdtFreezdate = Nothing Then
                objDataOperation.AddParameter("@freezdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@freezdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFreezdate)
            End If


            strQ = "UPDATE hrmsconfiguration..cfgroup_master SET " & _
                      "  groupname = @groupname" & _
                      ", address1 = @address1" & _
                      ", address2 = @address2" & _
                      ", city = @city" & _
                      ", zipcode = @zipcode" & _
                      ", state = @state" & _
                      ", country = @country" & _
                      ", phone = @phone" & _
                      ", fax = @fax" & _
                      ", email = @email" & _
                      ", website = @website" & _
                      ", startdate = @startdate" & _
                      ", freezdate = @freezdate " & _
                    "WHERE groupunkid = @groupunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfgroup_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "DELETE FROM hrmsconfiguration..cfgroup_master " & _
            "WHERE groupunkid = @groupunkid "

            objDataOperation.AddParameter("@groupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = ""

            objDataOperation.AddParameter("@groupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  groupunkid " & _
              ", groupname " & _
              ", address1 " & _
              ", address2 " & _
              ", city " & _
              ", zipcode " & _
              ", state " & _
              ", country " & _
              ", phone " & _
              ", fax " & _
              ", email " & _
              ", website " & _
              ", startdate " & _
              ", freezdate " & _
             "FROM hrmsconfiguration..cfgroup_master " & _
             "WHERE groupname = @name "

            If intUnkid > 0 Then
                strQ &= " AND groupunkid <> @groupunkid"
            End If

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@groupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Group Name already exists. Please define new Group Name.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class