﻿'************************************************************************************************************************************
'Class Name : clsdenomination.vb
'Purpose    :
'Date       : 30/06/2010
'Written By : Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data
''' <summary>
''' Purpose: 
''' Developer: Anjan
''' </summary>
Public Class clsDenomination
    Private Shared ReadOnly mstrModuleName As String = "clsDenomination"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintDenomunkid As Integer
    Private mstrCurrecnySign As String = Nothing
    Private mstrDenom_Code As String = String.Empty
    'Sandeep [ 29 NOV 2010 ] -- Start
    'Private mstrDenomination As String = String.Empty
    Private mdecDenomination As Decimal = 0 'Sohail (11 May 2011)
    'Sandeep [ 29 NOV 2010 ] -- End 
    Private mblnIsactive As Boolean = True
    Private mintCountryunkid As Integer 'Sohail (03 Sep 2012)

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Anjan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set denomunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Denomunkid() As Integer
        Get
            Return mintDenomunkid
        End Get
        Set(ByVal value As Integer)
            mintDenomunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set currency_sign
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Currency_Sign() As String
        Get
            Return mstrCurrecnySign
        End Get
        Set(ByVal value As String)
            mstrCurrecnySign = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set denom_code
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Denom_Code() As String
        Get
            Return mstrDenom_Code
        End Get
        Set(ByVal value As String)
            mstrDenom_Code = Value
        End Set
    End Property


    'Sandeep [ 29 NOV 2010 ] -- Start
    ''' <summary>
    ''' Purpose: Get or Set denomination
    ''' Modify By: Anjan
    ''' </summary>
    '''Public Property _Denomination() As String
    '''    Get
    '''      Return mstrDenomination
    '''End Get
    ''' Set(ByVal value As String)
    '''   mstrDenomination = Value
    '''End Set
    '''End Property
    Public Property _Denomination() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecDenomination
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecDenomination = value
        End Set
    End Property
    'Sandeep [ 29 NOV 2010 ] -- End 



    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    'Sohail (03 Sep 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property
    'Sohail (03 Sep 2012) -- End

#End Region

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  denomunkid " & _
                      ", currency_sign " & _
                      ", denom_code " & _
                      ", denomination " & _
                      ", isactive " & _
                      ", ISNULL(cfdenomination_master.countryunkid, 0) AS countryunkid " & _
                    "FROM cfdenomination_master " & _
                    "WHERE denomunkid = @denomunkid " 'Sohail (03 Sep 2012) - [countryunkid]

            objDataOperation.AddParameter("@denomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDenomunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintDenomunkid = CInt(dtRow.Item("denomunkid"))
                mstrCurrecnySign = dtRow.Item("currency_sign")
                mstrDenom_Code = dtRow.Item("denom_code").ToString
                'Sandeep [ 29 NOV 2010 ] -- Start
                'mstrdenomination = dtRow.Item("denomination").ToString
                mdecDenomination = dtRow.Item("denomination")
                'Sandeep [ 29 NOV 2010 ] -- End 
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid")) 'Sohail (03 Sep 2012)
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    'Sandeep [ 29 NOV 2010 ] -- Start
    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal blnCashCount As Boolean = False, Optional ByVal intExRateId As Integer = 1) As DataSet
        'Sandeep [ 29 NOV 2010 ] -- End 
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'S.SANDEEP [ 23 JUNE 2011 ] -- START
            'ISSUE : EXCHANGE RATE CHANGES

            'If blnCashCount = False Then
            '    strQ = "SELECT " & _
            '            "  denomunkid " & _
            '            ", currency_sign " & _
            '            ", denom_code " & _
            '            ", denomination " & _
            '            ", isactive " & _
            '        "FROM cfdenomination_master "

            '    If blnOnlyActive Then
            '        strQ &= " WHERE isactive = 1 "
            '    End If

            '    strQ &= " ORDER BY currency_sign,denomunkid "
            'Else
            '    strQ = "SELECT " & _
            '                "	 denomunkid " & _
            '                "	,denomination " & _
            '                "	,cfdenomination_master.currency_sign " & _
            '                "FROM cfdenomination_master " & _
            '                "	JOIN cfexchange_rate ON cfdenomination_master.currency_sign = cfexchange_rate.currency_sign " & _
            '                "WHERE cfexchange_rate.exchangerateunkid = @ExRateId " & _
            '                "	AND cfdenomination_master.isactive = 1 " & _
            '                "ORDER BY cfdenomination_master.currency_sign,denomination DESC "

            '    objDataOperation.AddParameter("@ExRateId", SqlDbType.Int, eZeeDataType.INT_SIZE, intExRateId)
            'End If

            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'strQ = "SELECT " & _
            '         "  cfdenomination_master.denomunkid " & _
            '         " ,cfexchange_rate.currency_sign " & _
            '         " ,cfdenomination_master.denom_code " & _
            '         " ,cfdenomination_master.denomination " & _
            '         " ,cfdenomination_master.isactive " & _
            '         ", ISNULL(cfdenomination_master.countryunkid, 0) AS countryunkid " & _
            '         "FROM cfdenomination_master " & _
            '         " JOIN cfexchange_rate ON cfdenomination_master.currency_sign = cfexchange_rate.exchangerateunkid " & _
            '         "WHERE 1 = 1 "

            strQ = "SELECT " & _
                     "  cfdenomination_master.denomunkid " & _
                     " ,cfdenomination_master.currency_sign " & _
                     " ,cfdenomination_master.denom_code " & _
                     " ,cfdenomination_master.denomination " & _
                     " ,cfdenomination_master.isactive " & _
                     ", ISNULL(cfdenomination_master.countryunkid, 0) AS countryunkid " & _
                     "FROM cfdenomination_master " & _
                     "WHERE 1 = 1 "
            'Sohail (03 Sep 2012) -- End

            
            If blnOnlyActive Then
                strQ &= " AND cfdenomination_master.isactive = 1 "
            End If

            If blnCashCount = False Then
            strQ &= " ORDER BY currency_sign,denomunkid "
            Else
                'Sohail (03 Sep 2012) -- Start
                'TRA - ENHANCEMENT
                'strQ &= " AND  cfexchange_rate.exchangerateunkid = @ExRateId " & _
                '            "ORDER BY cfdenomination_master.currency_sign,denomination DESC "
                strQ &= " AND  cfdenomination_master.countryunkid = @ExRateId " & _
                            "ORDER BY cfdenomination_master.currency_sign,denomination DESC "
                'Sohail (03 Sep 2012) -- End

                objDataOperation.AddParameter("@ExRateId", SqlDbType.Int, eZeeDataType.INT_SIZE, intExRateId)
            End If
            'S.SANDEEP [ 23 JUNE 2011 ] -- END 

            'Sohail (03 Sep 2012) - [countryunkid]

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfdenomination_master) </purpose>
    Public Function Insert() As Boolean
        'Sohail (03 Sep 2012) -- Start
        'TRA - ENHANCEMENT
        'If isExist(mstrDenom_Code, , mstrCurrecnySign) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
        '    Return False
        'End If
        If isExist(mstrDenom_Code, , mintCountryunkid.ToString) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
            Return False
        End If
        'Sohail (03 Sep 2012) -- End

        'Sandeep [ 29 NOV 2010 ] -- Start
        'If isExist(, mstrDenomination, mstrCurrecnySign) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
        '    Return False
        'End If

        'Sohail (03 Sep 2012) -- Start
        'TRA - ENHANCEMENT
        'If isExist(, mdecDenomination, mstrCurrecnySign) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
        '    Return False
        'End If
        If isExist(, mdecDenomination, mintCountryunkid.ToString) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
            Return False
        End If
        'Sohail (03 Sep 2012) -- End

        'Sandeep [ 29 NOV 2010 ] -- End 

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 

        Try
            objDataOperation.AddParameter("@currency_sign", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCurrecnySign.ToString)
            objDataOperation.AddParameter("@denom_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDenom_Code.ToString)
            'Sandeep [ 29 NOV 2010 ] -- Start
            'objDataOperation.AddParameter("@denomination", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrdenomination.ToString)
            objDataOperation.AddParameter("@denomination", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDenomination.ToString) 'Sohail (11 May 2011)
            'Sandeep [ 29 NOV 2010 ] -- End 
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString) 'Sohail (03 Sep 2012)

            strQ = "INSERT INTO cfdenomination_master ( " & _
                      "  currency_sign " & _
                      ", denom_code " & _
                      ", denomination " & _
                      ", isactive" & _
                      ", countryunkid " & _
                    ") VALUES (" & _
                      "  @currency_sign " & _
                      ", @denom_code " & _
                      ", @denomination " & _
                      ", @isactive" & _
                      ", @countryunkid " & _
                    "); SELECT @@identity"
            'Sohail (03 Sep 2012) - [countryunkid]
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintDenomunkid = dsList.Tables(0).Rows(0).Item(0)

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "cfdenomination_master", "denomunkid", mintDenomunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 


            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfdenomination_master) </purpose>
    Public Function Update() As Boolean


        'Sandeep [ 29 NOV 2010 ] -- Start
        'If isExist(, mstrDenomination, mstrCurrecnySign, mintDenomunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Name is already defined. Please define new Name.")
        '    Return False
        'End If

        'Sohail (03 Sep 2012) -- Start
        'TRA - ENHANCEMENT
        'If isExist(, mdecDenomination, mstrCurrecnySign, mintDenomunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
        '    Return False
        'End If
        If isExist(, mdecDenomination, mintCountryunkid.ToString, mintDenomunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
            Return False
        End If
        'Sohail (03 Sep 2012) -- End

        'Sandeep [ 29 NOV 2010 ] -- End 

        'Sohail (03 Sep 2012) -- Start
        'TRA - ENHANCEMENT
        'If isExist(mstrDenom_Code, , mstrCurrecnySign, mintDenomunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
        '    Return False
        'End If
        If isExist(mstrDenom_Code, , mintCountryunkid.ToString, mintDenomunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
            Return False
        End If
        'Sohail (03 Sep 2012) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 

        Try
            objDataOperation.AddParameter("@denomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDenomunkid.ToString)
            objDataOperation.AddParameter("@currency_sign", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCurrecnySign.ToString)
            objDataOperation.AddParameter("@denom_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDenom_Code.ToString)
            'Sandeep [ 29 NOV 2010 ] -- Start
            'objDataOperation.AddParameter("@denomination", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrdenomination.ToString)
            objDataOperation.AddParameter("@denomination", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDenomination.ToString) 'Sohail (11 May 2011)
            'Sandeep [ 29 NOV 2010 ] -- End 
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString) 'Sohail (03 Sep 2012)

            strQ = "UPDATE cfdenomination_master SET " & _
                      "  currency_sign = @currency_sign" & _
                      ", denom_code = @denom_code" & _
                      ", denomination = @denomination" & _
                      ", isactive = @isactive " & _
                      ", countryunkid = @countryunkid " & _
                    "WHERE denomunkid = @denomunkid " 'Sohail (03 Sep 2012) - [countryunkid]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "cfdenomination_master", mintDenomunkid, "denomunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "cfdenomination_master", "denomunkid", mintDenomunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 


            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfdenomination_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 

        Try
            'Sohail (28 Dec 2010) -- Start
            'strQ = "DELETE FROM cfdenomination_master " & _
            '        "WHERE denomunkid = @denomunkid "
            strQ = "UPDATE cfdenomination_master SET " & _
                     " isactive = 0 " & _
                    "WHERE denomunkid = @denomunkid "
            'Sohail (28 Dec 2010) -- End

            objDataOperation.AddParameter("@denomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "cfdenomination_master", "denomunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 


            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (28 Dec 2010) -- Start
            'strQ = "<Query>"
            strQ = "SELECT DISTINCT denominatonunkid " & _
                    "FROM    prcashdenomenation_tran " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND denominatonunkid = @denomunkid "
            'Sohail (28 Dec 2010) -- End

            objDataOperation.AddParameter("@denomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'Sandeep [ 29 NOV 2010 ] -- Start
    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal strcurrency_sign As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal decDemomination As Decimal = 0, Optional ByVal strcurrency_sign As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        'Sandeep [ 29 NOV 2010 ] -- End 
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'strQ = "SELECT " & _
            '          "  denomunkid " & _
            '          ", currency_sign " & _
            '          ", denom_code " & _
            '          ", denomination " & _
            '          ", isactive " & _
            '        "FROM cfdenomination_master " & _
            '        "WHERE currency_sign = @currency_sign "
            strQ = "SELECT " & _
                      "  denomunkid " & _
                      ", currency_sign " & _
                      ", denom_code " & _
                      ", denomination " & _
                      ", isactive " & _
                    "FROM cfdenomination_master " & _
                    "WHERE countryunkid = @countryunkid "
            'Sohail (03 Sep 2012) -- End

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strCode.Length > 0 Then
                strQ &= "AND denom_code = @denomcode "
                objDataOperation.AddParameter("@denomcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If


            'Sandeep [ 29 NOV 2010 ] -- Start
            'If strName.Length > 0 Then
            '    strQ &= "AND denomination = @denomination"
            '    objDataOperation.AddParameter("@denomination", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            'End If

            If decDemomination > 0 Then
                strQ &= "AND denomination = @denomination"
                objDataOperation.AddParameter("@denomination", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decDemomination) 'Sohail (11 May 2011)
            End If
            'Sandeep [ 29 NOV 2010 ] -- End 

            If intUnkid > 0 Then
                strQ &= " AND denomunkid <> @denomunkid"
                objDataOperation.AddParameter("@denomunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@currency_sign", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strcurrency_sign)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid)
            'Sohail (03 Sep 2012) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
			Language.setMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class