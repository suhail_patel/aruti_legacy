﻿'************************************************************************************************************************************
'Class Name : clsCompany_Bank_tran.vb
'Purpose    :
'Date       : 03/03/2010
'Written By : Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsCompany_Bank_tran

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = ""
    Dim objDataOperation As clsDataOperation
    Private mstrMessage As String = ""
    Private mintCompanyId As Integer = 0
    Private mdtTran As DataTable
    'S.SANDEEP [ 12 OCT 2011 ] -- START
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    Private mintCompanyBankTranID As Integer
    'S.SANDEEP [ 12 OCT 2011 ] -- END 

#End Region

#Region " Properties "

    Public Property _CompanyId() As Integer
        Get
            Return mintCompanyId
        End Get
        Set(ByVal value As Integer)
            mintCompanyId = value
            Call Get_BankTranList()
        End Set
    End Property

    Public Property _SetDataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

#Region " Contructor "

    Public Sub New()
        mdtTran = New DataTable("Bank_Tran")
        Try
            mdtTran.Columns.Add("companybanktranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("companyunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("groupmasterunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("branchunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("accounttypeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("account_no", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("bank_no", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("AccName", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("BranchName", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("BankName", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("isBeingEdit", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("AccCode", System.Type.GetType("System.String")).DefaultValue = "" 'Sohail (08 Jun 2016)
            'Sohail (03 Jan 2019) -- Start
            'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
            mdtTran.Columns.Add("BranchCode", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (03 Jan 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub Get_BankTranList()
        Dim StrQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim dtRow As DataRow
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                            "  cfcompanybank_tran.companybanktranunkid " & _
                            ", cfcompanybank_tran.companyunkid " & _
                            ", cfcompanybank_tran.groupmasterunkid " & _
                            ", cfcompanybank_tran.branchunkid " & _
                            ", cfcompanybank_tran.accounttypeunkid " & _
                            ", cfcompanybank_tran.account_no " & _
                            ", cfcompanybank_tran.bank_no " & _
                            ", cfcompanybank_tran.userunkid " & _
                            ", cfcompanybank_tran.isvoid " & _
                            ", cfcompanybank_tran.voiduserunkid " & _
                            ", cfcompanybank_tran.voiddatetime " & _
                            ", cfcompanybank_tran.voidreason " & _
                            ", '' AS AUD " & _
                            ", cfbankacctype_master.accounttype_name AS AccName " & _
                            ", cfbankbranch_master.branchcode AS BranchCode " & _
                            ", cfbankbranch_master.branchname AS BranchName " & _
                            ", cfpayrollgroup_master.groupname AS BankName " & _
                            ", cfbankacctype_master.accounttype_code AS AccCode " & _
                            "FROM hrmsConfiguration..cfcompanybank_tran " & _
                            "JOIN hrmsConfiguration..cfbankacctype_master ON cfcompanybank_tran.accounttypeunkid = cfbankacctype_master.accounttypeunkid " & _
                            "JOIN hrmsConfiguration..cfbankbranch_master ON cfcompanybank_tran.branchunkid = cfbankbranch_master.branchunkid " & _
                            "JOIN hrmsConfiguration..cfpayrollgroup_master ON cfcompanybank_tran.groupmasterunkid = cfpayrollgroup_master.groupmasterunkid AND cfpayrollgroup_master.grouptype_id = " & enPayrollGroupType.Bank & " " & _
                        "WHERE cfcompanybank_tran.companyunkid = @CompId AND cfcompanybank_tran.isvoid = 0 "
            'Sohail (03 Jan 2019) - [BranchCode]
            'Sohail (08 Jun 2016) - [cfbankacctype_master.accounttype_code AS AccCode]

            objDataOperation.AddParameter("@CompId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    dtRow = mdtTran.NewRow()

                    dtRow.Item("companybanktranunkid") = .Item("companybanktranunkid")
                    dtRow.Item("companyunkid") = .Item("companyunkid")
                    dtRow.Item("groupmasterunkid") = .Item("groupmasterunkid")
                    dtRow.Item("branchunkid") = .Item("branchunkid")
                    dtRow.Item("accounttypeunkid") = .Item("accounttypeunkid")
                    dtRow.Item("account_no") = .Item("account_no")
                    dtRow.Item("bank_no") = .Item("bank_no")
                    dtRow.Item("userunkid") = .Item("userunkid")
                    dtRow.Item("isvoid") = .Item("isvoid")
                    dtRow.Item("voiduserunkid") = .Item("voiduserunkid")
                    dtRow.Item("voiddatetime") = .Item("voiddatetime")
                    dtRow.Item("voidreason") = .Item("voidreason")
                    dtRow.Item("AUD") = .Item("AUD")
                    dtRow.Item("AccName") = .Item("AccName")
                    dtRow.Item("BranchName") = .Item("BranchName")
                    dtRow.Item("BankName") = .Item("BankName")
                    dtRow.Item("AccCode") = .Item("AccCode") 'Sohail (08 Jun 2016)
                    'Sohail (03 Jan 2019) -- Start
                    'OFF-GRID Enhancement 76.1 - "Reference Code Mapped with Company Bank" and "Reference Name Mapped with Company Bank" option to be mapped with Net Pay in all account configuration to show account code mapped with company bank in "Company Bank" Transaction type.
                    dtRow.Item("BranchCode") = .Item("BranchCode")
                    'Sohail (03 Jan 2019) -- End

                    mdtTran.Rows.Add(dtRow)
                End With
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_BankTranList", mstrModuleName)
        End Try
    End Sub

    Public Function InsertUpdateDelete_BankTranList() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO cfcompanybank_tran ( " & _
                                            "  companyunkid " & _
                                            ", groupmasterunkid " & _
                                            ", branchunkid " & _
                                            ", accounttypeunkid " & _
                                            ", account_no " & _
                                            ", bank_no " & _
                                            ", userunkid " & _
                                            ", isvoid " & _
                                            ", voiduserunkid " & _
                                            ", voiddatetime " & _
                                            ", voidreason" & _
                                        ") VALUES (" & _
                                            "  @companyunkid " & _
                                            ", @groupmasterunkid " & _
                                            ", @branchunkid " & _
                                            ", @accounttypeunkid " & _
                                            ", @account_no " & _
                                            ", @bank_no " & _
                                            ", @userunkid " & _
                                            ", @isvoid " & _
                                            ", @voiduserunkid " & _
                                            ", @voiddatetime " & _
                                            ", @voidreason" & _
                                        "); SELECT @@identity"

                                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyId)
                                objDataOperation.AddParameter("@groupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("groupmasterunkid").ToString)
                                objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("branchunkid").ToString)
                                objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("accounttypeunkid").ToString)
                                objDataOperation.AddParameter("@account_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("account_no").ToString)
                                objDataOperation.AddParameter("@bank_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("bank_no").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'objDataOperation.ExecNonQuery(strQ)
                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                mintCompanyBankTranID = dsList.Tables(0).Rows(0)(0)
                                If .Item("companyunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "cfcompany_master", "companyunkid", .Item("companyunkid"), "cfcompanybank_tran", "companybanktranunkid", mintCompanyBankTranID, 2, 1, True) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "cfcompany_master", "companyunkid", mintCompanyId, "cfcompanybank_tran", "companybanktranunkid", mintCompanyBankTranID, 1, 1, True) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                            Case "U"
                                strQ = "UPDATE cfcompanybank_tran SET " & _
                                        "  companyunkid = @companyunkid" & _
                                        ", groupmasterunkid = @groupmasterunkid" & _
                                        ", branchunkid = @branchunkid" & _
                                        ", accounttypeunkid = @accounttypeunkid" & _
                                        ", account_no = @account_no" & _
                                        ", bank_no = @bank_no" & _
                                        ", userunkid = @userunkid" & _
                                        ", isvoid = @isvoid" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voidreason = @voidreason " & _
                                      "WHERE companybanktranunkid = @companybanktranunkid "

                                objDataOperation.AddParameter("@companybanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("companybanktranunkid").ToString)
                                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("companyunkid").ToString)
                                objDataOperation.AddParameter("@groupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("groupmasterunkid").ToString)
                                objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("branchunkid").ToString)
                                objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("accounttypeunkid").ToString)
                                objDataOperation.AddParameter("@account_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("account_no").ToString)
                                objDataOperation.AddParameter("@bank_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("bank_no").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "cfcompany_master", "companyunkid", .Item("companyunkid"), "cfcompanybank_tran", "companybanktranunkid", .Item("companybanktranunkid"), 2, 2, True) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                            Case "D"

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                If .Item("companybanktranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "cfcompany_master", "companyunkid", .Item("companyunkid"), "cfcompanybank_tran", "companybanktranunkid", .Item("companybanktranunkid"), 2, 3, True) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                                strQ = "UPDATE cfcompanybank_tran SET " & _
                                        "  isvoid = @isvoid" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voidreason = @voidreason " & _
                                      "WHERE companybanktranunkid = @companybanktranunkid "

                                objDataOperation.AddParameter("@companybanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("companybanktranunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_BankTranList", mstrModuleName)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Get the List Of Combo</purpose>
    ''' <param name =" intCoumpanyId">Company Id</param>
    ''' <param name =" intGetIndex">1 = Bank List , 2 = Branch List , 3 = Account Type List </param>
    Public Function GetComboList(ByVal intCoumpanyId As Integer, ByVal intGetIndex As Integer, Optional ByVal StrList As String = "List" _
                                 , Optional ByVal strFilter As String = "") As DataSet 'Sohail (16 Jul 2012) - [strFilter]
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT 0 AS Id, @Select AS Name "

            Select Case intGetIndex
                Case 1
                    StrQ &= " UNION " & _
                                 " SELECT DISTINCT cfcompanybank_tran.groupmasterunkid AS Id " & _
                                 "  ,cfpayrollgroup_master.groupname AS Name " & _
                                 " FROM hrmsConfiguration..cfcompanybank_tran " & _
                                 "      JOIN hrmsConfiguration..cfpayrollgroup_master ON cfcompanybank_tran.groupmasterunkid = cfpayrollgroup_master.groupmasterunkid AND cfpayrollgroup_master.grouptype_id =" & enPayrollGroupType.Bank & "  " & _
                                 " WHERE cfcompanybank_tran.companyunkid = @CompId " & _
                                 " AND ISNULL(cfcompanybank_tran.isvoid, 0) = 0 " & _
                                 " AND ISNULL(cfpayrollgroup_master.isactive, 1) = 1 "

                    'Sohail (16 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    If strFilter.Trim <> "" Then
                        StrQ &= " AND " & strFilter
                    End If
                    'Sohail (16 Jul 2012) -- End

                Case 2
                    StrQ &= " UNION " & _
                                " SELECT DISTINCT cfcompanybank_tran.branchunkid AS Id " & _
                                "  ,cfbankbranch_master.branchname AS Name " & _
                                " FROM hrmsConfiguration..cfcompanybank_tran " & _
                                "   JOIN hrmsConfiguration..cfbankbranch_master ON cfcompanybank_tran.branchunkid = cfbankbranch_master.branchunkid " & _
                                " WHERE cfcompanybank_tran.companyunkid = @CompId " & _
                                 " AND ISNULL(cfcompanybank_tran.isvoid, 0) = 0 " & _
                                 " AND ISNULL(cfbankbranch_master.isactive, 1) = 1 "

                    'Sohail (16 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    If strFilter.Trim <> "" Then
                        StrQ &= " AND " & strFilter
                    End If
                    'Sohail (16 Jul 2012) -- End

                Case 3
                    StrQ &= " UNION " & _
                                 " SELECT DISTINCT cfcompanybank_tran.accounttypeunkid AS Id " & _
                                 "  ,cfbankacctype_master.accounttype_name AS Name " & _
                                 " FROM hrmsConfiguration..cfcompanybank_tran " & _
                                 "   JOIN hrmsConfiguration..cfbankacctype_master ON cfcompanybank_tran.accounttypeunkid = cfbankacctype_master.accounttypeunkid " & _
                                 " WHERE cfcompanybank_tran.companyunkid = @CompId " & _
                                 " AND ISNULL(cfcompanybank_tran.isvoid, 0) = 0 " & _
                                 " AND ISNULL(cfbankacctype_master.isactive, 1) = 1 "

                    'Sohail (16 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    If strFilter.Trim <> "" Then
                        StrQ &= " AND " & strFilter
                    End If
                    'Sohail (16 Jul 2012) -- End

                    'Sohail (21 May 2020) -- Start
                    'FDRC Enhancement # : Orbit Integrtion for Bulk Payment Process.
                Case 4
                    StrQ &= " UNION " & _
                                " SELECT DISTINCT cfcompanybank_tran.companybanktranunkid AS Id " & _
                                "  ,cfcompanybank_tran.account_no AS Name " & _
                                " FROM hrmsConfiguration..cfcompanybank_tran " & _
                                "   JOIN hrmsConfiguration..cfbankbranch_master ON cfcompanybank_tran.branchunkid = cfbankbranch_master.branchunkid " & _
                                " WHERE cfcompanybank_tran.companyunkid = @CompId " & _
                                 " AND ISNULL(cfcompanybank_tran.isvoid, 0) = 0 " & _
                                 " AND ISNULL(cfbankbranch_master.isactive, 1) = 1 "

                    If strFilter.Trim <> "" Then
                        StrQ &= " AND " & strFilter
                    End If
                    'Sohail (21 May 2020) -- End

            End Select

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOperation.AddParameter("@CompId", SqlDbType.Int, eZeeDataType.INT_SIZE, intCoumpanyId)

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetComboList", mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function GetComboListBankAccount(ByVal strList As String _
                                            , ByVal intCoumpanyId As Integer _
                                            , Optional ByVal blnAddSelect As Boolean = True _
                                            , Optional ByVal intBankGroupUnkId As Integer = 0 _
                                            , Optional ByVal intBankBranchUnkId As Integer = 0 _
                                            ) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            If blnAddSelect = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name " & _
                        "UNION "

                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            End If

            StrQ &= "SELECT  companybanktranunkid AS Id " & _
                          ", account_no AS Name " & _
                    "FROM    hrmsConfiguration..cfcompanybank_tran " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND companyunkid = @companyunkid "

            If intBankGroupUnkId > 0 Then
                StrQ &= "AND groupmasterunkid = @groupmasterunkid "
                objDataOperation.AddParameter("@groupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBankGroupUnkId)
            End If

            If intBankBranchUnkId > 0 Then
                StrQ &= "AND branchunkid = @branchunkid "
                objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBankBranchUnkId)
            End If

            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCoumpanyId)

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetComboListBankAccount", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function UpdatePastPaymentAccountNo(ByVal intCompanyUnkId As Integer) As Boolean
        Dim strQ As String = ""
        Dim dsList As DataSet
        Dim exForce As Exception
        Dim objCompany As New clsCompany_Master
        Dim mstrDatabaseName As String

        Try

            dsList = objCompany.GetFinancialYearList(intCompanyUnkId, , "List")

            objDataOperation = New clsDataOperation

            For Each dsRow As DataRow In dsList.Tables("List").Rows

                mstrDatabaseName = dsRow.Item("database_name").ToString

                objDataOperation.ClearParameters()

                strQ = "IF EXISTS(SELECT TOP 1 accountno FROM " & mstrDatabaseName & "..prpayment_tran WHERE accountno IS NULL) " & _
                       "BEGIN " & _
                           "DECLARE @account_no NVARCHAR(MAX) " & _
                           "SELECT TOP 1 @account_no=account_no FROM hrmsConfiguration..cfcompanybank_tran " & _
                           "WHERE ISNULL(isvoid, 0) = 0 AND companyunkid = @companyunkid " & _
                           "UPDATE " & mstrDatabaseName & "..prpayment_tran SET accountno = @account_no WHERE paymentmode IN (" & enPaymentMode.CHEQUE & ", " & enPaymentMode.TRANSFER & ") AND accountno IS NULL " & _
                           "UPDATE " & mstrDatabaseName & "..atprpayment_tran SET accountno = @account_no WHERE paymentmode IN (" & enPaymentMode.CHEQUE & ", " & enPaymentMode.TRANSFER & ") AND accountno IS NULL " & _
                       "END "


                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyUnkId)

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdatePastPaymentAccountNo; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function
    'Sohail (21 Jul 2012) -- End

    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Can_Change_AccNo(ByVal StrAccNo As String) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim blnFlag As Boolean = True
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT DISTINCT " & _
                   "     REVERSE(RIGHT(REVERSE(physical_name),(LEN(physical_name)-CHARINDEX('\', REVERSE(physical_name),1))+1)) +''+ sys.databases.name As PhyPath " & _
                   "    ,sys.databases.name as dbName " & _
                   "FROM sys.master_files " & _
                   "    JOIN sys.databases ON sys.master_files.database_id = sys.databases.database_id " & _
                   "WHERE sys.databases.name LIKE 'tran_%' "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables("List").Rows
                If (IO.File.Exists(dRow.Item("PhyPath").ToString & ".mdf") = True AndAlso IO.File.Exists(dRow.Item("PhyPath").ToString & "_log.LDF") = True) Then
                    'Sohail (17 Jan 2020) -- Start
                    'Ifakara Issue # : Unable to change company bank account no. even after voiding app payslips payment.
                    'StrQ = "SELECT * FROM " & dRow.Item("dbName").ToString.Trim & "..prpayment_tran WHERE accountno = '" & StrAccNo & "' "
                    StrQ = "SELECT * FROM " & dRow.Item("dbName").ToString.Trim & "..prpayment_tran WHERE isvoid = 0 AND accountno = '" & StrAccNo & "' "
                    'Sohail (17 Jan 2020) -- End
                    If objDataOperation.RecordCount(StrQ) > 0 Then
                        blnFlag = False : Exit For
                    End If
                End If
            Next

            Return blnFlag

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Can_Change_AccNo", mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 12 NOV 2012 ] -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
