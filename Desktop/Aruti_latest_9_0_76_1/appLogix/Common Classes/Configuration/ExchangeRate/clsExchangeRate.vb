﻿'************************************************************************************************************************************
'Class Name : clscurrency.vb
'Purpose    :
'Date       :29/06/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib

''' <summary>
''' Purpose: 
''' Developer: Anjan
''' </summary>
Public Class clsExchangeRate
    Private Shared ReadOnly mstrModuleName As String = "clsExchangeRate"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintExchangeRateunkid As Integer
    Private mintCountryunkid As Integer
    Private mstrCurrency_Name As String = String.Empty
    Private mstrCurrency_Sign As String = String.Empty
    Private msinExchange_Rate As Single
    Private mblnIsbasecurrency As Boolean
    Private mblnIsactive As Boolean = True
    Private msinExchange_Rate1 As Single
    Private msinExchange_Rate2 As Single
    Private mblnIsprefix As Boolean
    Private mintDigits_After_Decimal As Integer
    Private mstrfmtCurrency As String = ""

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES;Date is to be Place
    Private mdtExchangeDate As Date = Nothing
    Private mintLatestExRateUnkid As Integer = -1
    'S.SANDEEP [ 07 NOV 2011 ] -- END

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Anjan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set exchangerateunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _ExchangeRateunkid() As Integer
        Get
            Return mintExchangeRateunkid
        End Get
        Set(ByVal value As Integer)
            mintExchangeRateunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set currency_name
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Currency_Name() As String
        Get
            Return mstrCurrency_Name
        End Get
        Set(ByVal value As String)
            mstrCurrency_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set currency_sign
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Currency_Sign() As String
        Get
            Return mstrCurrency_Sign
        End Get
        Set(ByVal value As String)
            mstrCurrency_Sign = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set exchange_rate
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Exchange_Rate() As Single
        Get
            Return msinExchange_Rate
        End Get
        Set(ByVal value As Single)
            msinExchange_Rate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isbasecurrency
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isbasecurrency() As Boolean
        Get
            Return mblnIsbasecurrency
        End Get
        Set(ByVal value As Boolean)
            mblnIsbasecurrency = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set exchange_rate1
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Exchange_Rate1() As Single
        Get
            Return msinExchange_Rate1
        End Get
        Set(ByVal value As Single)
            msinExchange_Rate1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set exchange_rate2
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Exchange_Rate2() As Single
        Get
            Return msinExchange_Rate2
        End Get
        Set(ByVal value As Single)
            msinExchange_Rate2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isprefix
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isprefix() As Boolean
        Get
            Return mblnIsprefix
        End Get
        Set(ByVal value As Boolean)
            mblnIsprefix = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set digits_after_decimal
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Digits_After_Decimal() As Integer
        Get
            Return mintDigits_After_Decimal
        End Get
        Set(ByVal value As Integer)
            mintDigits_After_Decimal = value
        End Set
    End Property

    Public ReadOnly Property _fmtCurrency() As String
        Get
            Return mstrfmtCurrency
        End Get

    End Property

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set Exchange_Rate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Exchange_Date() As Date
        Get
            Return mdtExchangeDate
        End Get
        Set(ByVal value As Date)
            mdtExchangeDate = value
        End Set
    End Property
    'S.SANDEEP [ 07 NOV 2011 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  exchangerateunkid " & _
                      ", countryunkid " & _
                      ", currency_name " & _
                      ", currency_sign " & _
                      ", exchange_rate " & _
                      ", isbasecurrency " & _
                      ", isactive " & _
                      ", exchange_rate1 " & _
                      ", exchange_rate2 " & _
                      ", isprefix " & _
                      ", digits_after_decimal " & _
                      ", exchange_date " & _
                    "FROM cfexchange_Rate " & _
                    "WHERE exchangerateunkid = @exchangerateunkid " 'S.SANDEEP [ 07 NOV 2011, exchange_date ]

            objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExchangeRateunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintExchangeRateunkid = CInt(dtRow.Item("exchangerateunkid"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mstrCurrency_Name = dtRow.Item("currency_name").ToString
                mstrCurrency_Sign = dtRow.Item("currency_sign").ToString
                msinExchange_Rate = dtRow.Item("exchange_rate")
                mblnIsbasecurrency = CBool(dtRow.Item("isbasecurrency"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                msinExchange_Rate1 = dtRow.Item("exchange_rate1")
                msinExchange_Rate2 = dtRow.Item("exchange_rate2")
                mblnIsprefix = CBool(dtRow.Item("isprefix"))
                mintDigits_After_Decimal = CInt(dtRow.Item("digits_after_decimal"))

                If mintDigits_After_Decimal = 0 Then
                    mstrfmtCurrency = "#,###,###,##0"
                Else
                    mstrfmtCurrency = "#,###,###,##0." & New String("0", mintDigits_After_Decimal)
                End If

                'S.SANDEEP [ 07 NOV 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If IsDBNull(dtRow.Item("exchange_date")) Then
                    mdtExchangeDate = Nothing
                Else
                    mdtExchangeDate = dtRow.Item("exchange_date")
                End If
                'S.SANDEEP [ 07 NOV 2011 ] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal IsForImport As Boolean = False _
                            , Optional ByVal intExchangeRateUnkId As Integer = 0 _
                            , Optional ByVal intCountryUnkId As Integer = 0 _
                            , Optional ByVal blnOrderExchangeDateDESC As Boolean = False _
                            , Optional ByVal dtExchangeDate As DateTime = Nothing _
                            , Optional ByVal blnOnlyTOP_1 As Boolean = False _
                            , Optional ByVal xDataOp As clsDataOperation = Nothing _
                            ) As DataSet
        'Sohail (03 May 2018) - [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try

            'Pinkal (10-Mar-2011) -- Start

            'strQ = "SELECT " & _
            '        "  exchangerateunkid " & _
            '        ", countryunkid " & _
            '        ", currency_name " & _
            '        ", currency_sign " & _
            '        ", exchange_rate " & _
            '        ", isbasecurrency " & _
            '        ", isactive " & _
            '        ", exchange_rate1 " & _
            '        ", exchange_rate2 " & _
            '        ", isprefix " & _
            '        ", digits_after_decimal " & _
            '      "FROM cfexchange_Rate "

            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'strQ = "SELECT " & _
            '        "  exchangerateunkid " & _
            '        ", cfexchange_Rate.countryunkid " & _
            '        ", currency_name " & _
            '        ", currency_sign " & _
            '        ", exchange_rate " & _
            '        ", isbasecurrency " & _
            '        ", isactive " & _
            '        ", exchange_rate1 " & _
            '        ", exchange_rate2 " & _
            '        ", isprefix " & _
            '        ", digits_after_decimal " & _
            '        ", CONVERT(CHAR(8),exchange_date,112) as exchange_date " 'S.SANDEEP [ 07 NOV 2011 ]
            strQ = "SELECT  A.exchangerateunkid " & _
                         ", A.countryunkid " & _
                         ", A.currency_name " & _
                         ", A.currency_sign " & _
                         ", A.exchange_rate " & _
                         ", A.isbasecurrency " & _
                         ", A.isactive " & _
                         ", A.exchange_rate1 " & _
                         ", A.exchange_rate2 " & _
                         ", A.isprefix " & _
                         ", A.digits_after_decimal " & _
                         ", A.exchange_date " & _
                    "FROM    ( SELECT " & _
                    "  exchangerateunkid " & _
                   ", cfexchange_Rate.countryunkid " & _
                    ", cfcountry_master.currency AS currency_name " & _
                    ", cfcountry_master.currency AS currency_sign " & _
                    ", exchange_rate " & _
                    ", isbasecurrency " & _
                    ", isactive " & _
                    ", exchange_rate1 " & _
                    ", exchange_rate2 " & _
                    ", isprefix " & _
                    ", digits_after_decimal " & _
                                 ", CONVERT(CHAR(8),exchange_date,112) as exchange_date " & _
                    ", DENSE_RANK() OVER ( PARTITION BY cfexchange_Rate.countryunkid ORDER BY exchange_date "
            'Sohail (10 Jan 2020) - [currency_name]=[cfcountry.currency AS currency_name], [currency_sign]=[cfcountry.currency AS currency_sign]

            If blnOrderExchangeDateDESC = True Then
                strQ &= " DESC "
            Else
                strQ &= " ASC "
            End If
            strQ &= " ) AS ROWNO "
            'Sohail (03 Sep 2012) -- End

            If IsForImport Then
                strQ &= ", hrmsConfiguration..cfcountry_master.country_name as country "
            End If

            strQ &= " FROM cfexchange_Rate "

            'If IsForImport Then 'Sohail (10 Jan 2020)
                strQ &= " LEFT JOIN hrmsConfiguration..cfcountry_master on hrmsConfiguration..cfcountry_master.countryunkid = cfexchange_Rate.countryunkid "
            'End If 'Sohail (10 Jan 2020)

            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'If blnOnlyActive Then
            '    strQ &= " WHERE isactive = 1 "
            'End If
            strQ &= " WHERE 1 = 1 "
            If blnOnlyActive Then
                strQ &= " AND isactive = 1 "
            End If

            If intExchangeRateUnkId > 0 Then
                strQ &= " AND exchangerateunkid = @exchangerateunkid "
                objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intExchangeRateUnkId)
            End If

            If intCountryUnkId > 0 Then
                strQ &= " AND cfexchange_Rate.countryunkid = @countryunkid "
                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCountryUnkId)
            End If

            If dtExchangeDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),exchange_date,112) <= @exchange_date "
                objDataOperation.AddParameter("@exchange_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtExchangeDate))
            End If

            strQ &= " ) AS A WHERE 1 = 1 "


            If blnOnlyTOP_1 = True Then
                strQ &= " AND ROWNO = 1 "
            End If


            'Sohail (03 Sep 2012) -- End

            'Pinkal (10-Mar-2011) -- End

            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            If blnOrderExchangeDateDESC = True Then
                strQ &= " ORDER BY exchange_date DESC "
            End If
            'Sohail (03 Sep 2012) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfexchange_Rate) </purpose>
    Public Function Insert() As Boolean
        'S.SANDEEP [ 07 NOV 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(mintCountryunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Exchange Rate is already defined for this Currency.")
        '    Return False
        'End If

        If isExist(mintCountryunkid, mdtExchangeDate) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Exchange Rate is already defined for this Currency.")
            Return False
        End If

        'S.SANDEEP [ 07 NOV 2011 ] -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 

        Try
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@currency_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCurrency_Name.ToString)
            objDataOperation.AddParameter("@currency_sign", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCurrency_Sign.ToString)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, msinExchange_Rate.ToString)
            objDataOperation.AddParameter("@isbasecurrency", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbasecurrency.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@exchange_rate1", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, msinExchange_Rate1.ToString)
            objDataOperation.AddParameter("@exchange_rate2", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, msinExchange_Rate2.ToString)
            objDataOperation.AddParameter("@isprefix", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprefix.ToString)
            objDataOperation.AddParameter("@digits_after_decimal", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDigits_After_Decimal.ToString)
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@exchange_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtExchangeDate)
            'S.SANDEEP [ 07 NOV 2011 ] -- END


            strQ = "INSERT INTO cfexchange_Rate ( " & _
              "  countryunkid " & _
              ", currency_name " & _
              ", currency_sign " & _
              ", exchange_rate " & _
              ", isbasecurrency " & _
              ", isactive " & _
              ", exchange_rate1 " & _
              ", exchange_rate2 " & _
              ", isprefix " & _
              ", digits_after_decimal" & _
              ", exchange_date " & _
            ") VALUES (" & _
              "  @countryunkid " & _
              ", @currency_name " & _
              ", @currency_sign " & _
              ", @exchange_rate " & _
              ", @isbasecurrency " & _
              ", @isactive " & _
              ", @exchange_rate1 " & _
              ", @exchange_rate2 " & _
              ", @isprefix " & _
              ", @digits_after_decimal" & _
              ", @exchange_date " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintExchangeRateunkid = dsList.Tables(0).Rows(0).Item(0)

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "cfexchange_Rate", "exchangerateunkid", mintExchangeRateunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 

            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfexchange_Rate) </purpose>
    Public Function Update() As Boolean
        'S.SANDEEP [ 07 NOV 2011 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(mintCountryunkid, mintExchangeRateunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Exchange Rate is already defined for this Currency.")
        '    Return False
        'End If
        If isExist(mintCountryunkid, mdtExchangeDate) Then
            mintExchangeRateunkid = mintLatestExRateUnkid
        End If
        'S.SANDEEP [ 07 NOV 2011 ] -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 


        Try
            objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExchangeRateunkid.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@currency_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCurrency_Name.ToString)
            objDataOperation.AddParameter("@currency_sign", SqlDbType.NVarChar, eZeeDataType.VOUCHERNO_SIZE, mstrCurrency_Sign.ToString)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.MONEY_SIZE, msinExchange_Rate.ToString)
            objDataOperation.AddParameter("@isbasecurrency", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbasecurrency.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@exchange_rate1", SqlDbType.Decimal, eZeeDataType.MONEY_SIZE, msinExchange_Rate1.ToString)
            objDataOperation.AddParameter("@exchange_rate2", SqlDbType.Decimal, eZeeDataType.MONEY_SIZE, msinExchange_Rate2.ToString)
            objDataOperation.AddParameter("@isprefix", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprefix.ToString)
            objDataOperation.AddParameter("@digits_after_decimal", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDigits_After_Decimal.ToString)
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@exchange_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtExchangeDate)
            'S.SANDEEP [ 07 NOV 2011 ] -- END


            strQ = "UPDATE cfexchange_Rate SET " & _
                      "  countryunkid = @countryunkid" & _
                      ", currency_name = @currency_name" & _
                      ", currency_sign = @currency_sign" & _
                      ", exchange_rate = @exchange_rate" & _
                      ", isbasecurrency = @isbasecurrency" & _
                      ", isactive = @isactive" & _
                      ", exchange_rate1 = @exchange_rate1" & _
                      ", exchange_rate2 = @exchange_rate2" & _
                      ", isprefix = @isprefix" & _
                      ", digits_after_decimal = @digits_after_decimal " & _
                      ", exchange_date = @exchange_date " & _
                    "WHERE exchangerateunkid = @exchangerateunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "cfexchange_Rate", mintExchangeRateunkid, "exchangerateunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "cfexchange_Rate", "exchangerateunkid", mintExchangeRateunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 


            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfexchange_Rate) </purpose>
    Public Function Delete(ByVal intexchangerateunkid As Integer) As Boolean
        'If isUsed(intexchangerateunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete this Exchange Rate. Reason: This Exchange Rate is in use.") '?2
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (28 Dec 2010) -- Start
            'strQ = "DELETE FROM cfexchange_Rate " & _
            '        "WHERE exchangerateunkid = @exchangerateunkid "
            strQ = "UPDATE cfexchange_Rate SET " & _
                      " isactive = 0 " & _
                    "WHERE exchangerateunkid = @exchangerateunkid "
            'Sohail (28 Dec 2010) -- End

            objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intexchangerateunkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "cfexchange_Rate", "exchangerateunkid", intexchangerateunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 


            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName) 'Sohail (28 Dec 2010)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intexchangerateunkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (19 Nov 2010) -- Start
            'strQ = "<Query>"
            strQ = "SELECT  currencyunkid " & _
                    "FROM    prbankedi_master " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND currencyunkid = @exchangerateunkid "
            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            strQ &= "UNION ALL " & _
                    "SELECT  paidcurrencyid " & _
                    "FROM    prpayment_tran " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND paidcurrencyid = @exchangerateunkid " & _
                    "UNION ALL " & _
                    "SELECT  exchagerateunkid " & _
                    "FROM    prcashdenomenation_master " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND exchagerateunkid = @exchangerateunkid " & _
                    "UNION ALL " & _
                    "SELECT  denomunkid " & _
                    "FROM    cfdenomination_master " & _
                    "WHERE   isactive = 1 " & _
                            "AND currency_sign = @exchangerateunkid "
            'Sohail (03 Sep 2012) -- End

            'Sohail (19 Nov 2010) -- End



            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'Hemant (11 Apr 2019) -- Start
            'ISSUE#3732(GOOD NEIGHBORS TZ): Editing currency throws an error "Invalid column name 'exchagerateunkid'"
            'strQ &= " UNION ALL " & _
            '                " SELECT  exchangerateunkid " & _
            '                " FROM    cmclaim_request_tran " & _
            '                " WHERE   ISNULL(isvoid, 0) = 0 " & _
            '                " AND exchangerateunkid = @exchangerateunkid " & _
            '                " UNION ALL " & _
            '                " SELECT  exchagerateunkid " & _
            '                " FROM    cmclaim_approval_tran " & _
            '                "  WHERE   ISNULL(isvoid, 0) = 0 " & _
            '                " AND exchangerateunkid = @exchangerateunkid "
            strQ &= " UNION ALL " & _
                            " SELECT  exchangerateunkid " & _
                            " FROM    cmclaim_request_tran " & _
                            " WHERE   ISNULL(isvoid, 0) = 0 " & _
                            " AND exchangerateunkid = @exchangerateunkid " & _
                            " UNION ALL " & _
                            " SELECT  exchangerateunkid " & _
                            " FROM    cmclaim_approval_tran " & _
                            "  WHERE   ISNULL(isvoid, 0) = 0 " & _
                            " AND exchangerateunkid = @exchangerateunkid "
            'Hemant (11 Apr 2019) -- End            
            'Pinkal (04-Feb-2019) -- End


            objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intexchangerateunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intCountryUnkid As Integer, ByVal dtExchange_Date As Date) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT exchangerateunkid, countryunkid " & _
                   "FROM cfexchange_rate WHERE countryunkid = @countryunkid " & _
                   "AND CONVERT(CHAR(8),exchange_date,112) = @Exchange_Date "

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            objDataOperation.AddParameter("@countryunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCountryUnkid.ToString)
            objDataOperation.AddParameter("@Exchange_Date", SqlDbType.VarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtExchange_Date))


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mintLatestExRateUnkid = dsList.Tables(0).Rows(0)(0)
            Else
                mintLatestExRateUnkid = -1
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> GET ComboList </purpose>
    ''' 'S.SANDEEP [ 23 JUNE 2011 ] -- START
    ''' 'ISSUE : EXCHANGE RATE CHANGES
    ''' Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal blnFlag As Boolean = False) As DataSet
    Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal blnFlag As Boolean = False, Optional ByVal blnOnlyBaseCurr As Boolean = False) As DataSet
        'S.SANDEEP [ 23 JUNE 2011 ] -- END 


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If blnFlag = True Then
                strQ = "SELECT 0 As exchangerateunkid ,@ItemName As currency_name, 0 As isbasecurrency,  '' As currency_sign, 0 AS countryunkid  UNION "
            End If


            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ &= "SELECT exchangerateunkid,currency_name, isbasecurrency, currency_sign FROM cfexchange_rate WHERE isactive =1 "
            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'strQ &= "SELECT cfexchange_rate.exchangerateunkid,cfexchange_rate.currency_name, cfexchange_rate.isbasecurrency, cfexchange_rate.currency_sign " & _
            '        " FROM cfexchange_rate, (SELECT MAX(exchangerateunkid) AS exchangerateunkid, MAX(exchange_date) AS temp FROM cfexchange_rate GROUP BY countryunkid, currency_name) AS A " & _
            '        " WHERE cfexchange_rate.exchangerateunkid = a.exchangerateunkid AND isactive = 1 "
            'Sohail (10 Jan 2020) -- Start
            'NMB Enhancement # : New screen for Country list and allow to edit currency sign on country list.
            'strQ &= "SELECT cfexchange_rate.exchangerateunkid,cfexchange_rate.currency_name, cfexchange_rate.isbasecurrency, cfexchange_rate.currency_sign " & _
            '             ", cfexchange_rate.countryunkid " & _
            '        " FROM cfexchange_rate, (SELECT MAX(exchangerateunkid) AS exchangerateunkid, MAX(exchange_date) AS temp FROM cfexchange_rate WHERE isactive = 1 GROUP BY countryunkid, currency_name) AS A " & _
            '        " WHERE cfexchange_rate.exchangerateunkid = a.exchangerateunkid AND isactive = 1 "
            strQ &= "SELECT cfexchange_rate.exchangerateunkid,cfcountry_master.currency AS currency_name, cfexchange_rate.isbasecurrency, cfcountry_master.currency AS currency_sign " & _
                         ", cfexchange_rate.countryunkid " & _
                    " FROM cfexchange_rate " & _
                    " LEFT JOIN hrmsConfiguration..cfcountry_master on cfcountry_master.countryunkid = cfexchange_Rate.countryunkid " & _
                    " , (SELECT MAX(exchangerateunkid) AS exchangerateunkid, MAX(exchange_date) AS temp FROM cfexchange_rate WHERE isactive = 1 GROUP BY countryunkid) AS A " & _
                    " WHERE cfexchange_rate.exchangerateunkid = a.exchangerateunkid AND isactive = 1 "
            'Sohail (10 Jan 2020) -- End
            'Sohail (03 Sep 2012) -- End
            'S.SANDEEP [ 07 NOV 2011 ] -- END

            'S.SANDEEP [ 23 JUNE 2011 ] -- START
            'ISSUE : EXCHANGE RATE CHANGES
            If blnOnlyBaseCurr = True Then
                strQ &= " AND isbasecurrency = 1 "
            End If
            'S.SANDEEP [ 23 JUNE 2011 ] -- END 

            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        End Try
    End Function


    'Sandeep | 04 JAN 2010 | -- Start
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> SET BASE CURRENCY </purpose>
    Public Function SetBaseCurrency(ByVal intOldCurrId As Integer, ByVal intNewCurrId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        objDataOperation = New clsDataOperation


        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 


        Try
            StrQ = "UPDATE cfexchange_rate " & _
                   "	SET isbasecurrency = 1 WHERE exchangerateunkid = @ExNewId; " & _
                   "UPDATE cfexchange_rate " & _
                   "	SET isbasecurrency=0 WHERE exchangerateunkid = @ExOldId "

            objDataOperation.AddParameter("@ExNewId", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewCurrId)
            objDataOperation.AddParameter("@ExOldId", SqlDbType.Int, eZeeDataType.INT_SIZE, intOldCurrId)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "cfexchange_Rate", intNewCurrId, "exchangerateunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "cfexchange_Rate", "exchangerateunkid", intNewCurrId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            DisplayError.Show("-1", ex.Message, "SetBaseCurrency", mstrModuleName)
            Return False
        End Try
    End Function
    'Sandeep | 04 JAN 2010 | -- END 

    'Sohail (15 Dec 2015) -- Start
    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
    Public Function getBaseCurrencyCountryID() As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intBaseCurrencyID As Integer = 0

        objDataOperation = New clsDataOperation

        Try


            strQ = "SELECT  cfexchange_rate.exchangerateunkid  " & _
                          ", cfexchange_rate.countryunkid " & _
                    "FROM    cfexchange_rate " & _
                    "WHERE   cfexchange_rate.isactive = 1 " & _
                            "AND cfexchange_rate.isbasecurrency = 1 "


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intBaseCurrencyID = CInt(dsList.Tables("List").Rows(0).Item("countryunkid"))
            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getBaseCurrencyCountryID", mstrModuleName)
            Return Nothing
        End Try
        Return intBaseCurrencyID
    End Function
    'Sohail (15 Dec 2015) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Exchange Rate is already defined for this Currency.")
			Language.setMessage(mstrModuleName, 2, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class