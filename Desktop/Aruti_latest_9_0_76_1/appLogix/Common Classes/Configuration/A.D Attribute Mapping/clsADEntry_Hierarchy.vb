﻿'************************************************************************************************************************************
'Class Name : clsadentry_hierarchy.vb
'Purpose    :
'Date       :13-Mar-2020
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsadentry_hierarchy
    Private Const mstrModuleName = "clsadentry_hierarchy"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintHierarchyunkid As Integer
    Private mintAllocationrefid As Integer
    Private mintAllocationtranunkid As Integer
    Private mintAllocationrefid1 As Integer
    Private mintAllocationrefid2 As Integer
    Private mintAllocationrefid3 As Integer
    Private mintAllocationrefid4 As Integer
    Private mintAllocationrefid5 As Integer
    Private mintAllocationrefid6 As Integer
    Private mintAllocationrefid7 As Integer
    Private mintAllocationrefid8 As Integer
    Private mintAllocationrefid9 As Integer
    Private mintAllocationrefid10 As Integer
    Private mintAllocationrefid11 As Integer
    Private mintCompanyunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrFormName As String
    Private mstrClientIP As String
    Private mstrHostName As String
    Private mblnIsWeb As Boolean
    Private mintAuditUserId As Integer
    Private mdtAuditDate As DateTime

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hierarchyunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Hierarchyunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintHierarchyunkid
        End Get
        Set(ByVal value As Integer)
            mintHierarchyunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationrefid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Allocationrefid() As Integer
        Get
            Return mintAllocationrefid
        End Get
        Set(ByVal value As Integer)
            mintAllocationrefid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationtranunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Allocationtranunkid() As Integer
        Get
            Return mintAllocationtranunkid
        End Get
        Set(ByVal value As Integer)
            mintAllocationtranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationrefid1
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Allocationrefid1() As Integer
        Get
            Return mintAllocationrefid1
        End Get
        Set(ByVal value As Integer)
            mintAllocationrefid1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationrefid2
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Allocationrefid2() As Integer
        Get
            Return mintAllocationrefid2
        End Get
        Set(ByVal value As Integer)
            mintAllocationrefid2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationrefid3
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Allocationrefid3() As Integer
        Get
            Return mintAllocationrefid3
        End Get
        Set(ByVal value As Integer)
            mintAllocationrefid3 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationrefid4
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Allocationrefid4() As Integer
        Get
            Return mintAllocationrefid4
        End Get
        Set(ByVal value As Integer)
            mintAllocationrefid4 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationrefid5
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Allocationrefid5() As Integer
        Get
            Return mintAllocationrefid5
        End Get
        Set(ByVal value As Integer)
            mintAllocationrefid5 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationrefid6
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Allocationrefid6() As Integer
        Get
            Return mintAllocationrefid6
        End Get
        Set(ByVal value As Integer)
            mintAllocationrefid6 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationrefid7
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Allocationrefid7() As Integer
        Get
            Return mintAllocationrefid7
        End Get
        Set(ByVal value As Integer)
            mintAllocationrefid7 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationrefid8
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Allocationrefid8() As Integer
        Get
            Return mintAllocationrefid8
        End Get
        Set(ByVal value As Integer)
            mintAllocationrefid8 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationrefid9
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Allocationrefid9() As Integer
        Get
            Return mintAllocationrefid9
        End Get
        Set(ByVal value As Integer)
            mintAllocationrefid9 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationrefid10
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Allocationrefid10() As Integer
        Get
            Return mintAllocationrefid10
        End Get
        Set(ByVal value As Integer)
            mintAllocationrefid10 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationrefid11
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Allocationrefid11() As Integer
        Get
            Return mintAllocationrefid11
        End Get
        Set(ByVal value As Integer)
            mintAllocationrefid11 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set companyunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Companyunkid() As Integer
        Get
            Return mintCompanyunkid
        End Get
        Set(ByVal value As Integer)
            mintCompanyunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddateime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _FromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FromWeb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _AuditDate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditDate() As DateTime
        Get
            Return mdtAuditDate
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property


#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  hierarchyunkid " & _
                      ", allocationrefid " & _
                      ", allocationtranunkid " & _
                      ", allocationrefid1 " & _
                      ", allocationrefid2 " & _
                      ", allocationrefid3 " & _
                      ", allocationrefid4 " & _
                      ", allocationrefid5 " & _
                      ", allocationrefid6 " & _
                      ", allocationrefid7 " & _
                      ", allocationrefid8 " & _
                      ", allocationrefid9 " & _
                      ", allocationrefid10 " & _
                      ", allocationrefid11 " & _
                      ", companyunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      " FROM cfadentry_hierarchy " & _
                      " WHERE hierarchyunkid = @hierarchyunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@hierarchyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHierarchyunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintHierarchyunkid = CInt(dtRow.Item("hierarchyunkid"))
                mintAllocationrefid = CInt(dtRow.Item("allocationrefid"))
                mintAllocationtranunkid = CInt(dtRow.Item("allocationtranunkid"))
                mintAllocationrefid1 = CInt(dtRow.Item("allocationrefid1"))
                mintAllocationrefid2 = CInt(dtRow.Item("allocationrefid2"))
                mintAllocationrefid3 = CInt(dtRow.Item("allocationrefid3"))
                mintAllocationrefid4 = CInt(dtRow.Item("allocationrefid4"))
                mintAllocationrefid5 = CInt(dtRow.Item("allocationrefid5"))
                mintAllocationrefid6 = CInt(dtRow.Item("allocationrefid6"))
                mintAllocationrefid7 = CInt(dtRow.Item("allocationrefid7"))
                mintAllocationrefid8 = CInt(dtRow.Item("allocationrefid8"))
                mintAllocationrefid9 = CInt(dtRow.Item("allocationrefid9"))
                mintAllocationrefid10 = CInt(dtRow.Item("allocationrefid10"))
                mintAllocationrefid11 = CInt(dtRow.Item("allocationrefid11"))
                mintCompanyunkid = CInt(dtRow.Item("companyunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xCompanyId As Integer, Optional ByVal blnOnlyActive As Boolean = True _
                                   , Optional ByVal xAllocationrefid As Integer = 0, Optional ByVal xAllocationtranunkid As Integer = 0, Optional ByVal objDoOperation As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  hierarchyunkid " & _
                      ", allocationrefid " & _
                      ", allocationtranunkid " & _
                      ", allocationrefid1 " & _
                      ", allocationrefid2 " & _
                      ", allocationrefid3 " & _
                      ", allocationrefid4 " & _
                      ", allocationrefid5 " & _
                      ", allocationrefid6 " & _
                      ", allocationrefid7 " & _
                      ", allocationrefid8 " & _
                      ", allocationrefid9 " & _
                      ", allocationrefid10 " & _
                      ", allocationrefid11 " & _
                      ", companyunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      " FROM hrmsconfiguration..cfadentry_hierarchy "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            If xCompanyId > 0 Then
                strQ &= " AND companyunkid = @companyunkid "
                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
            End If

            If xAllocationrefid > 0 Then
                strQ &= " AND allocationrefid = @allocationrefid "
                objDataOperation.AddParameter("@allocationrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationrefid)
            End If

            If xAllocationtranunkid > 0 Then
                strQ &= "  AND allocationtranunkid = @allocationtranunkid "
                objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationtranunkid)
            End If


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfadentry_hierarchy) </purpose>
    Public Function Insert() As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        Dim dsHierarchyList As DataSet = GetList("List", mintCompanyunkid, True, 0, 0)
        Dim mintOldAllocationRefID As Integer = 0
        Dim LstOldAllocationRefID As IEnumerable(Of Integer) = Nothing
        If dsHierarchyList IsNot Nothing AndAlso dsHierarchyList.Tables(0).Rows.Count > 0 Then
            LstOldAllocationRefID = dsHierarchyList.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("allocationrefid")).Distinct()
            If LstOldAllocationRefID IsNot Nothing AndAlso LstOldAllocationRefID.Count > 0 Then
                mintOldAllocationRefID = LstOldAllocationRefID(0)
            End If
        End If


        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try


            If mintOldAllocationRefID <> mintAllocationrefid Then
                For Each dr In dsHierarchyList.Tables(0).Rows
                    If Delete(objDataOperation, mintCompanyunkid, CInt(dr("hierarchyunkid")), CInt(dr("allocationrefid")), CInt(dr("allocationtranunkid")), CInt(dr("allocationrefid1")), CInt(dr("allocationrefid2")), CInt(dr("allocationrefid3")), CInt(dr("allocationrefid4")) _
                                , CInt(dr("allocationrefid5")), CInt(dr("allocationrefid6")), CInt(dr("allocationrefid7")), CInt(dr("allocationrefid8")), CInt(dr("allocationrefid9")), CInt(dr("allocationrefid10")), CInt(dr("allocationrefid11"))) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

            Else


                If mintHierarchyunkid > 0 Then

                    If Update(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    Else
                        objDataOperation.ReleaseTransaction(True)
                        Return True
                    End If
                Else

                    If isExist(mintAllocationrefid, mintAllocationtranunkid, mintAllocationrefid1, mintAllocationrefid2, mintAllocationrefid3, mintAllocationrefid4, mintAllocationrefid5, mintAllocationrefid6 _
                          , mintAllocationrefid7, mintAllocationrefid8, mintAllocationrefid9, mintAllocationrefid10, mintAllocationrefid11, objDataOperation) Then


                        If Update(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        Else
                            objDataOperation.ReleaseTransaction(True)
                            Return True
                        End If
                    End If  'If IsExist
                End If
              
            End If  ' If mintOldAllocationRefID <> mintAllocationrefid Then

            strQ = "INSERT INTO cfadentry_hierarchy ( " & _
                        "  allocationrefid " & _
                        ", allocationtranunkid " & _
                        ", allocationrefid1 " & _
                        ", allocationrefid2 " & _
                        ", allocationrefid3 " & _
                        ", allocationrefid4 " & _
                        ", allocationrefid5 " & _
                        ", allocationrefid6 " & _
                        ", allocationrefid7 " & _
                        ", allocationrefid8 " & _
                        ", allocationrefid9 " & _
                        ", allocationrefid10 " & _
                        ", allocationrefid11 " & _
                        ", companyunkid " & _
                        ", userunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime" & _
                      ") VALUES (" & _
                        "  @allocationrefid " & _
                        ", @allocationtranunkid " & _
                        ", @allocationrefid1 " & _
                        ", @allocationrefid2 " & _
                        ", @allocationrefid3 " & _
                        ", @allocationrefid4 " & _
                        ", @allocationrefid5 " & _
                        ", @allocationrefid6 " & _
                        ", @allocationrefid7 " & _
                        ", @allocationrefid8 " & _
                        ", @allocationrefid9 " & _
                        ", @allocationrefid10 " & _
                        ", @allocationrefid11 " & _
                        ", @companyunkid " & _
                        ", @userunkid " & _
                        ", @isvoid " & _
                        ", @voiduserunkid " & _
                        ", @voiddatetime" & _
                      "); SELECT @@identity"


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@allocationrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid)
            objDataOperation.AddParameter("@allocationrefid1", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid1)
            objDataOperation.AddParameter("@allocationrefid2", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid2)
            objDataOperation.AddParameter("@allocationrefid3", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid3)
            objDataOperation.AddParameter("@allocationrefid4", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid4)
            objDataOperation.AddParameter("@allocationrefid5", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid5)
            objDataOperation.AddParameter("@allocationrefid6", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid6)
            objDataOperation.AddParameter("@allocationrefid7", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid7)
            objDataOperation.AddParameter("@allocationrefid8", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid8)
            objDataOperation.AddParameter("@allocationrefid9", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid9)
            objDataOperation.AddParameter("@allocationrefid10", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid10)
            objDataOperation.AddParameter("@allocationrefid11", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid11)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintHierarchyunkid = dsList.Tables(0).Rows(0).Item(0)

            If ATInsertADEntryhierarchy(1, objDataOperation, mintCompanyunkid, mintHierarchyunkid, mintAllocationrefid, mintAllocationtranunkid, mintAllocationrefid1, mintAllocationrefid2, mintAllocationrefid3 _
                                                    , mintAllocationrefid4, mintAllocationrefid5, mintAllocationrefid6, mintAllocationrefid7, mintAllocationrefid8, mintAllocationrefid9, mintAllocationrefid10, mintAllocationrefid11) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfadentry_hierarchy) </purpose>
    Public Function Update(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDoOperation = objDataOperation
        End If


        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@hierarchyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHierarchyunkid.ToString)
            objDataOperation.AddParameter("@allocationrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid.ToString)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid.ToString)
            objDataOperation.AddParameter("@allocationrefid1", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid1.ToString)
            objDataOperation.AddParameter("@allocationrefid2", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid2.ToString)
            objDataOperation.AddParameter("@allocationrefid3", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid3.ToString)
            objDataOperation.AddParameter("@allocationrefid4", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid4.ToString)
            objDataOperation.AddParameter("@allocationrefid5", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid5.ToString)
            objDataOperation.AddParameter("@allocationrefid6", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid6.ToString)
            objDataOperation.AddParameter("@allocationrefid7", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid7.ToString)
            objDataOperation.AddParameter("@allocationrefid8", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid8.ToString)
            objDataOperation.AddParameter("@allocationrefid9", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid9.ToString)
            objDataOperation.AddParameter("@allocationrefid10", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid10.ToString)
            objDataOperation.AddParameter("@allocationrefid11", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationrefid11.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If


            strQ = "UPDATE cfadentry_hierarchy SET " & _
                      "  allocationrefid = @allocationrefid" & _
                      ", allocationtranunkid = @allocationtranunkid" & _
                      ", allocationrefid1 = @allocationrefid1" & _
                      ", allocationrefid2 = @allocationrefid2" & _
                      ", allocationrefid3 = @allocationrefid3" & _
                      ", allocationrefid4 = @allocationrefid4" & _
                      ", allocationrefid5 = @allocationrefid5" & _
                      ", allocationrefid6 = @allocationrefid6" & _
                      ", allocationrefid7 = @allocationrefid7" & _
                      ", allocationrefid8 = @allocationrefid8" & _
                      ", allocationrefid9 = @allocationrefid9" & _
                      ", allocationrefid10 = @allocationrefid10" & _
                      ", allocationrefid11 = @allocationrefid11" & _
                      ", companyunkid = @companyunkid" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime " & _
                      " WHERE hierarchyunkid = @hierarchyunkid AND isvoid = 0 "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If ATInsertADEntryhierarchy(2, objDataOperation, mintCompanyunkid, mintHierarchyunkid, mintAllocationrefid, mintAllocationtranunkid, mintAllocationrefid1, mintAllocationrefid2, mintAllocationrefid3 _
                                                   , mintAllocationrefid4, mintAllocationrefid5, mintAllocationrefid6, mintAllocationrefid7, mintAllocationrefid8, mintAllocationrefid9, mintAllocationrefid10, mintAllocationrefid11) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfadentry_hierarchy) </purpose>
    Public Function Delete(ByVal objDoOperation As clsDataOperation, ByVal xCompanyId As Integer, ByVal xhierarchyunkid As Integer, ByVal xAllocationId As Integer, ByVal xAllocationTranId As Integer _
                                   , ByVal xAllocationRefId1 As Integer, ByVal xAllocationRefId2 As Integer, ByVal xAllocationRefId3 As Integer, ByVal xAllocationRefId4 As Integer, ByVal xAllocationRefId5 As Integer _
                                   , ByVal xAllocationRefId6 As Integer, ByVal xAllocationRefId7 As Integer, ByVal xAllocationRefId8 As Integer, ByVal xAllocationRefId9 As Integer, ByVal xAllocationRefId10 As Integer _
                                   , ByVal xAllocationRefId11 As Integer) As Boolean

        Dim dsList As DataSet = Nothing

        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            strQ = " UPDATE cfadentry_hierarchy  SET isvoid = 1,voiduserunkid = @voiduserunkid,voiddatetime = GetDate() " & _
                      " WHERE companyunkid = @companyunkid AND hierarchyunkid = @hierarchyunkid AND allocationrefid = @allocationrefid " & _
                      " AND allocationtranunkid = @allocationtranunkid AND isvoid = 0 "

            objDoOperation.ClearParameters()
            objDoOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
            objDoOperation.AddParameter("@hierarchyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xhierarchyunkid)
            objDoOperation.AddParameter("@allocationrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationId)
            objDoOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationTranId)
            objDoOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            Call objDoOperation.ExecNonQuery(strQ)

            If objDoOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                Throw exForce
            End If

            If ATInsertADEntryhierarchy(3, objDoOperation, xCompanyId, xhierarchyunkid, xAllocationId, xAllocationTranId, xAllocationRefId1, xAllocationRefId2, xAllocationRefId3, xAllocationRefId4 _
                                                    , xAllocationRefId5, xAllocationRefId6, xAllocationRefId7, xAllocationRefId8, xAllocationRefId9, xAllocationRefId10, xAllocationRefId11) = False Then
                exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xAllocationRefId As Integer, ByVal xAllocationTranId As Integer, ByVal xAllocationRefId1 As Integer, ByVal xAllocationRefId2 As Integer, ByVal xAllocationRefId3 As Integer, ByVal xAllocationRefId4 As Integer, ByVal xAllocationRefId5 As Integer _
                                  , ByVal xAllocationRefId6 As Integer, ByVal xAllocationRefId7 As Integer, ByVal xAllocationRefId8 As Integer, ByVal xAllocationRefId9 As Integer, ByVal xAllocationRefId10 As Integer, ByVal xAllocationRefId11 As Integer _
                                  , Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDoOperation = objDataOperation
        End If


        Try
            strQ = "SELECT " & _
                      "  hierarchyunkid " & _
                      ", allocationrefid " & _
                      ", allocationtranunkid " & _
                      ", allocationrefid1 " & _
                      ", allocationrefid2 " & _
                      ", allocationrefid3 " & _
                      ", allocationrefid4 " & _
                      ", allocationrefid5 " & _
                      ", allocationrefid6 " & _
                      ", allocationrefid7 " & _
                      ", allocationrefid8 " & _
                      ", allocationrefid9 " & _
                      ", allocationrefid10 " & _
                      ", allocationrefid11 " & _
                      ", companyunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      " FROM cfadentry_hierarchy " & _
                      " WHERE isvoid = 0 " & _
                      " AND allocationrefid = @allocationrefid AND allocationtranunkid = @allocationtranunkid "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@allocationrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationTranId)

            If xAllocationRefId1 > 0 Then
                strQ &= " AND allocationrefid1 = @allocationrefid1 "
                objDataOperation.AddParameter("@allocationrefid1", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId1)
            End If

            If xAllocationRefId2 > 0 Then
                strQ &= " AND allocationrefid2 = @allocationrefid2 "
                objDataOperation.AddParameter("@allocationrefid2", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId2)
            End If

            If xAllocationRefId3 > 0 Then
                strQ &= " AND allocationrefid3 = @allocationrefid3 "
                objDataOperation.AddParameter("@allocationrefid3", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId3)
            End If

            If xAllocationRefId4 > 0 Then
                strQ &= " AND allocationrefid4 = @allocationrefid4 "
                objDataOperation.AddParameter("@allocationrefid4", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId4)
            End If

            If xAllocationRefId5 > 0 Then
                strQ &= " AND allocationrefid5 = @allocationrefid5 "
                objDataOperation.AddParameter("@allocationrefid5", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId5)
            End If

            If xAllocationRefId6 > 0 Then
                strQ &= " AND allocationrefid6 = @allocationrefid6 "
                objDataOperation.AddParameter("@allocationrefid6", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId6)
            End If

            If xAllocationRefId7 > 0 Then
                strQ &= " AND allocationrefid7 = @allocationrefid7 "
                objDataOperation.AddParameter("@allocationrefid7", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId7)
            End If

            If xAllocationRefId8 > 0 Then
                strQ &= " AND allocationrefid8 = @allocationrefid8 "
                objDataOperation.AddParameter("@allocationrefid8", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId8)
            End If

            If xAllocationRefId9 > 0 Then
                strQ &= " AND allocationrefid9 = @allocationrefid9 "
                objDataOperation.AddParameter("@allocationrefid9", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId9)
            End If

            If xAllocationRefId10 > 0 Then
                strQ &= " AND allocationrefid10 = @allocationrefid10 "
                objDataOperation.AddParameter("@allocationrefid10", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId10)
            End If

            If xAllocationRefId11 > 0 Then
                strQ &= " AND allocationrefid11 = @allocationrefid11 "
                objDataOperation.AddParameter("@allocationrefid11", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId11)
            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintHierarchyunkid = CInt(dsList.Tables(0).Rows(0)("hierarchyunkid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function ATInsertADEntryhierarchy(ByVal xAuditType As Integer, ByVal objDoOperation As clsDataOperation, ByVal xCompanyunkid As Integer, ByVal xHierarchyunkid As Integer, ByVal xAllocationRefId As Integer, ByVal xAllocationTranId As Integer _
                                                               , ByVal xAllocationRefId1 As Integer, ByVal xAllocationRefId2 As Integer, ByVal xAllocationRefId3 As Integer, ByVal xAllocationRefId4 As Integer _
                                                               , ByVal xAllocationRefId5 As Integer, ByVal xAllocationRefId6 As Integer, ByVal xAllocationRefId7 As Integer, ByVal xAllocationRefId8 As Integer _
                                                               , ByVal xAllocationRefId9 As Integer, ByVal xAllocationRefId10 As Integer, ByVal xAllocationRefId11 As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            strQ = " INSERT INTO hrmsConfiguration..atcfadentry_hierarchy ( " & _
                          "  hierarchyunkid " & _
                          ", allocationrefid " & _
                          ", allocationtranunkid " & _
                          ", allocationrefid1 " & _
                          ", allocationrefid2 " & _
                          ", allocationrefid3 " & _
                          ", allocationrefid4 " & _
                          ", allocationrefid5 " & _
                          ", allocationrefid6 " & _
                          ", allocationrefid7 " & _
                          ", allocationrefid8 " & _
                          ", allocationrefid9 " & _
                          ", allocationrefid10 " & _
                          ", allocationrefid11 " & _
                          ", companyunkid " & _
                         ", audittype " & _
                         ", audituserunkid " & _
                         ", auditdatetime " & _
                         ", ip " & _
                         ", host " & _
                         ", form_name " & _
                         ", isweb" & _
                       ") VALUES (" & _
                          "  @hierarchyunkid " & _
                          ", @allocationrefid " & _
                          ", @allocationtranunkid " & _
                          ", @allocationrefid1 " & _
                          ", @allocationrefid2 " & _
                          ", @allocationrefid3 " & _
                          ", @allocationrefid4 " & _
                          ", @allocationrefid5 " & _
                          ", @allocationrefid6 " & _
                          ", @allocationrefid7 " & _
                          ", @allocationrefid8 " & _
                          ", @allocationrefid9 " & _
                          ", @allocationrefid10 " & _
                          ", @allocationrefid11 " & _
                          ", @companyunkid " & _
                          ", @audittype " & _
                          ", @audituserunkid " & _
                          ", @auditdatetime " & _
                          ", @ip " & _
                          ", @host " & _
                          ", @form_name " & _
                          ", @isweb" & _
                       ") ; SELECT @@identity"

            objDoOperation.ClearParameters()
            objDoOperation.AddParameter("@hierarchyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xHierarchyunkid.ToString)
            objDoOperation.AddParameter("@allocationrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId.ToString)
            objDoOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationTranId.ToString)
            objDoOperation.AddParameter("@allocationrefid1", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId1.ToString)
            objDoOperation.AddParameter("@allocationrefid2", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId2.ToString)
            objDoOperation.AddParameter("@allocationrefid3", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId3.ToString)
            objDoOperation.AddParameter("@allocationrefid4", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId4.ToString)
            objDoOperation.AddParameter("@allocationrefid5", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId5.ToString)
            objDoOperation.AddParameter("@allocationrefid6", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId6.ToString)
            objDoOperation.AddParameter("@allocationrefid7", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId7.ToString)
            objDoOperation.AddParameter("@allocationrefid8", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId8.ToString)
            objDoOperation.AddParameter("@allocationrefid9", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId9.ToString)
            objDoOperation.AddParameter("@allocationrefid10", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId10.ToString)
            objDoOperation.AddParameter("@allocationrefid11", SqlDbType.Int, eZeeDataType.INT_SIZE, xAllocationRefId11.ToString)
            objDoOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyunkid.ToString)
            objDoOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType.ToString)
            objDoOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId.ToString)
            objDoOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDoOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP.ToString)
            objDoOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName.ToString)
            objDoOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName.ToString)
            objDoOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb.ToString)
            dsList = objDoOperation.ExecQuery(strQ, "List")

            If objDoOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ATInsertADEntryhierarchy; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

End Class
