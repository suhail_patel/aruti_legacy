﻿'************************************************************************************************************************************
'Class Name : clsEmail_setup.vb
'Purpose    :
'Date       :04-10-2021
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsEmail_setup
    Private Shared ReadOnly mstrModuleName As String = "clsEmail_setup"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintEmailsetupunkid As Integer
    Private mstrSendername As String = String.Empty
    Private mstrSenderaddress As String = String.Empty
    Private mstrReference As String = String.Empty
    Private mstrMailserverip As String = String.Empty
    Private mintMailserverport As Integer
    Private mstrUsername As String = String.Empty
    Private mstrPassword As String = String.Empty
    Private mblnIsloginssl As Boolean
    Private mblnIscrt_authenticated As Boolean
    Private mblnIsbypassproxy As Boolean
    Private mstrMail_Body As String = String.Empty
    Private mintEmail_Type As Integer
    Private mstrEws_Url As String = String.Empty
    Private mstrEws_Domain As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'Hemant (01 Feb 2022) -- Start
    'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
    Private mintProtocolunkid As Integer
    'Hemant (01 Feb 2022) -- End
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emailsetupunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Emailsetupunkid() As Integer
        Get
            Return mintEmailsetupunkid
        End Get
        Set(ByVal value As Integer)
            mintEmailsetupunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sendername
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Sendername() As String
        Get
            Return mstrSendername
        End Get
        Set(ByVal value As String)
            mstrSendername = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set senderaddress
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Senderaddress() As String
        Get
            Return mstrSenderaddress
        End Get
        Set(ByVal value As String)
            mstrSenderaddress = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reference
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Reference() As String
        Get
            Return mstrReference
        End Get
        Set(ByVal value As String)
            mstrReference = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mailserverip
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Mailserverip() As String
        Get
            Return mstrMailserverip
        End Get
        Set(ByVal value As String)
            mstrMailserverip = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mailserverport
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Mailserverport() As Integer
        Get
            Return mintMailserverport
        End Get
        Set(ByVal value As Integer)
            mintMailserverport = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set username
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Username() As String
        Get
            Return mstrUsername
        End Get
        Set(ByVal value As String)
            mstrUsername = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set password
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Password() As String
        Get
            Return mstrPassword
        End Get
        Set(ByVal value As String)
            mstrPassword = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isloginssl
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isloginssl() As Boolean
        Get
            Return mblnIsloginssl
        End Get
        Set(ByVal value As Boolean)
            mblnIsloginssl = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscrt_authenticated
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Iscrt_authenticated() As Boolean
        Get
            Return mblnIscrt_authenticated
        End Get
        Set(ByVal value As Boolean)
            mblnIscrt_authenticated = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isbypassproxy
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isbypassproxy() As Boolean
        Get
            Return mblnIsbypassproxy
        End Get
        Set(ByVal value As Boolean)
            mblnIsbypassproxy = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mail_body
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Mail_Body() As String
        Get
            Return mstrMail_Body
        End Get
        Set(ByVal value As String)
            mstrMail_Body = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set email_type
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Email_Type() As Integer
        Get
            Return mintEmail_Type
        End Get
        Set(ByVal value As Integer)
            mintEmail_Type = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ews_url
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Ews_Url() As String
        Get
            Return mstrEws_Url
        End Get
        Set(ByVal value As String)
            mstrEws_Url = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ews_domain
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Ews_Domain() As String
        Get
            Return mstrEws_Domain
        End Get
        Set(ByVal value As String)
            mstrEws_Domain = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set email_type
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property


    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mblnIsweb As Boolean
    Public WriteOnly Property _Isweb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    Private mdtAuditDate As DateTime
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private mstrClientIp As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIp = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mstrFormName As String = ""
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    'Hemant (01 Feb 2022) -- Start
    'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
    Public Property _Protocolunkid() As Integer
        Get
            Return mintProtocolunkid
        End Get
        Set(ByVal value As Integer)
            mintProtocolunkid = value
        End Set
    End Property
    'Hemant (01 Feb 2022) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  hrmsConfiguration..cfemail_setup.emailsetupunkid " & _
              ", hrmsConfiguration..cfemail_setup.sendername " & _
              ", hrmsConfiguration..cfemail_setup.senderaddress " & _
              ", hrmsConfiguration..cfemail_setup.reference " & _
              ", hrmsConfiguration..cfemail_setup.mailserverip " & _
              ", hrmsConfiguration..cfemail_setup.mailserverport " & _
              ", hrmsConfiguration..cfemail_setup.username " & _
              ", hrmsConfiguration..cfemail_setup.password " & _
              ", hrmsConfiguration..cfemail_setup.isloginssl " & _
              ", hrmsConfiguration..cfemail_setup.iscrt_authenticated " & _
              ", hrmsConfiguration..cfemail_setup.isbypassproxy " & _
              ", hrmsConfiguration..cfemail_setup.mail_body " & _
              ", hrmsConfiguration..cfemail_setup.email_type " & _
              ", hrmsConfiguration..cfemail_setup.ews_url " & _
              ", hrmsConfiguration..cfemail_setup.ews_domain " & _
              ", hrmsConfiguration..cfemail_setup.userunkid " & _
              ", hrmsConfiguration..cfemail_setup.isvoid " & _
              ", hrmsConfiguration..cfemail_setup.voiduserunkid " & _
              ", hrmsConfiguration..cfemail_setup.voiddatetime " & _
              ", hrmsConfiguration..cfemail_setup.voidreason " & _
              ", ISNULL(hrmsConfiguration..cfemail_setup.protocolunkid, 0) AS protocolunkid " & _
             "FROM hrmsConfiguration..cfemail_setup " & _
             "WHERE emailsetupunkid = @emailsetupunkid "
            'Hemant (01 Feb 2022) -- [protocolunkid]

            objDataOperation.AddParameter("@emailsetupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintEmailsetupUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintemailsetupunkid = CInt(dtRow.Item("emailsetupunkid"))
                mstrsendername = dtRow.Item("sendername").ToString
                mstrsenderaddress = dtRow.Item("senderaddress").ToString
                mstrreference = dtRow.Item("reference").ToString
                mstrmailserverip = dtRow.Item("mailserverip").ToString
                mintmailserverport = CInt(dtRow.Item("mailserverport"))
                mstrusername = dtRow.Item("username").ToString
                mstrpassword = dtRow.Item("password").ToString
                mblnisloginssl = CBool(dtRow.Item("isloginssl"))
                mstrmail_body = dtRow.Item("mail_body").ToString
                mintemail_type = CInt(dtRow.Item("email_type"))
                mstrews_url = dtRow.Item("ews_url").ToString
                mstrEws_Domain = dtRow.Item("ews_domain").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Hemant (01 Feb 2022) -- Start
                'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
                mintProtocolunkid = CInt(dtRow.Item("protocolunkid"))
                'Hemant (01 Feb 2022) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intEmailSetupUnkId As Integer = 0, Optional ByVal strFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  hrmsConfiguration..cfemail_setup.emailsetupunkid " & _
                      ", hrmsConfiguration..cfemail_setup.sendername " & _
                      ", hrmsConfiguration..cfemail_setup.senderaddress " & _
                      ", hrmsConfiguration..cfemail_setup.reference " & _
                      ", hrmsConfiguration..cfemail_setup.mailserverip " & _
                      ", hrmsConfiguration..cfemail_setup.mailserverport " & _
                      ", hrmsConfiguration..cfemail_setup.username " & _
                      ", hrmsConfiguration..cfemail_setup.password " & _
                      ", hrmsConfiguration..cfemail_setup.isloginssl " & _
                      ", hrmsConfiguration..cfemail_setup.iscrt_authenticated " & _
                      ", hrmsConfiguration..cfemail_setup.isbypassproxy " & _
                      ", hrmsConfiguration..cfemail_setup.mail_body " & _
                      ", hrmsConfiguration..cfemail_setup.email_type " & _
                      ", CASE hrmsConfiguration..cfemail_setup.email_type WHEN 1 THEN @ews ELSE @smtp END AS email_typename " & _
                      ", hrmsConfiguration..cfemail_setup.ews_url " & _
                      ", hrmsConfiguration..cfemail_setup.ews_domain " & _
                      ", hrmsConfiguration..cfemail_setup.userunkid " & _
                      ", hrmsConfiguration..cfemail_setup.isvoid " & _
                      ", hrmsConfiguration..cfemail_setup.voiduserunkid " & _
                      ", hrmsConfiguration..cfemail_setup.voiddatetime " & _
                      ", hrmsConfiguration..cfemail_setup.voidreason " & _
                      ", ISNULL(hrmsConfiguration..cfemail_setup.protocolunkid, 0) AS protocolunkid " & _
                 "FROM hrmsConfiguration..cfemail_setup " & _
                 "WHERE cfemail_setup.isvoid = 0 "

            'Hemant (01 Feb 2022) -- [protocolunkid]
            If intEmailSetupUnkId > 0 Then
                strQ &= " AND cfemail_setup.emailsetupunkid = @emailsetupunkid "
                objDataOperation.AddParameter("@emailsetupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmailSetupUnkId)
            End If

            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter & " "
            End If

            objDataOperation.AddParameter("@smtp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "SMTP"))
            objDataOperation.AddParameter("@ews", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "EWS"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function getListForCombo(ByVal strTableName As String, ByVal blnAddSelect As Boolean, Optional ByVal strEmail As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If blnAddSelect = True Then
                strQ = "SELECT 0 AS Id, ' ' + @Select AS Name UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            End If

            strQ &= "SELECT " & _
                      "  hrmsConfiguration..cfemail_setup.emailsetupunkid AS Id " & _
                      ", hrmsConfiguration..cfemail_setup.username AS Name " & _
                 "FROM hrmsConfiguration..cfemail_setup " & _
                 "WHERE cfemail_setup.isvoid = 0 "

            If strEmail.ToString <> "" Then
                strQ &= " AND cfemail_setup.username = @username "
                objDataOperation.AddParameter("@username", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmail)
            End If


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getListForCombo; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfemail_setup) </purpose>
    Public Function Insert(ByVal xDataOp As clsDataOperation) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@sendername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSendername.ToString)
            objDataOperation.AddParameter("@senderaddress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSenderaddress.ToString)
            objDataOperation.AddParameter("@reference", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReference.ToString)
            objDataOperation.AddParameter("@mailserverip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMailserverip.ToString)
            objDataOperation.AddParameter("@mailserverport", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMailserverport.ToString)
            objDataOperation.AddParameter("@username", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrUsername.ToString)
            objDataOperation.AddParameter("@password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPassword.ToString)
            objDataOperation.AddParameter("@isloginssl", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloginssl.ToString)
            objDataOperation.AddParameter("@iscrt_authenticated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscrt_authenticated.ToString)
            objDataOperation.AddParameter("@isbypassproxy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbypassproxy.ToString)
            objDataOperation.AddParameter("@mail_body", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMail_Body.ToString)
            objDataOperation.AddParameter("@email_type", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmail_Type.ToString)
            objDataOperation.AddParameter("@ews_url", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEws_Url.ToString)
            objDataOperation.AddParameter("@ews_domain", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEws_Domain.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Hemant (01 Feb 2022) -- Start
            'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
            objDataOperation.AddParameter("@protocolunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProtocolunkid.ToString)
            'Hemant (01 Feb 2022) -- End

            strQ = "INSERT INTO hrmsConfiguration..cfemail_setup ( " & _
              "  sendername " & _
              ", senderaddress " & _
              ", reference " & _
              ", mailserverip " & _
              ", mailserverport " & _
              ", username " & _
              ", password " & _
              ", isloginssl " & _
              ", iscrt_authenticated " & _
              ", isbypassproxy " & _
              ", mail_body " & _
              ", email_type " & _
              ", ews_url " & _
              ", ews_domain " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", protocolunkid " & _
            ") VALUES (" & _
              "  @sendername " & _
              ", @senderaddress " & _
              ", @reference " & _
              ", @mailserverip " & _
              ", @mailserverport " & _
              ", @username " & _
              ", @password " & _
              ", @isloginssl " & _
              ", @iscrt_authenticated " & _
              ", @isbypassproxy " & _
              ", @mail_body " & _
              ", @email_type " & _
              ", @ews_url " & _
              ", @ews_domain " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @protocolunkid " & _
            "); SELECT @@identity"

            'Hemant (01 Feb 2022) -- [protocolunkid]
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintEmailsetupunkid = dsList.Tables(0).Rows(0).Item(0)

            If Insert_AtTranLog(objDataOperation, 1) = False Then
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfemail_setup) </purpose>
    Public Function Update(ByVal xDataOp As clsDataOperation) As Boolean
        'If isExist(mstrName, mintEmailsetupunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@emailsetupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmailsetupunkid.ToString)
            objDataOperation.AddParameter("@sendername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSendername.ToString)
            objDataOperation.AddParameter("@senderaddress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSenderaddress.ToString)
            objDataOperation.AddParameter("@reference", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReference.ToString)
            objDataOperation.AddParameter("@mailserverip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMailserverip.ToString)
            objDataOperation.AddParameter("@mailserverport", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMailserverport.ToString)
            objDataOperation.AddParameter("@username", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrUsername.ToString)
            objDataOperation.AddParameter("@password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPassword.ToString)
            objDataOperation.AddParameter("@isloginssl", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloginssl.ToString)
            objDataOperation.AddParameter("@iscrt_authenticated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscrt_authenticated.ToString)
            objDataOperation.AddParameter("@isbypassproxy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbypassproxy.ToString)
            objDataOperation.AddParameter("@mail_body", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMail_Body.ToString)
            objDataOperation.AddParameter("@email_type", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmail_Type.ToString)
            objDataOperation.AddParameter("@ews_url", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEws_Url.ToString)
            objDataOperation.AddParameter("@ews_domain", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEws_Domain.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Hemant (01 Feb 2022) -- Start
            'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
            objDataOperation.AddParameter("@protocolunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProtocolunkid.ToString)
            'Hemant (01 Feb 2022) -- End

            strQ = "UPDATE hrmsConfiguration..cfemail_setup SET " & _
              "  sendername = @sendername" & _
              ", senderaddress = @senderaddress" & _
              ", reference = @reference" & _
              ", mailserverip = @mailserverip" & _
              ", mailserverport = @mailserverport" & _
              ", username = @username" & _
              ", password = @password" & _
              ", isloginssl = @isloginssl" & _
              ", iscrt_authenticated = @iscrt_authenticated " & _
              ", isbypassproxy = @isbypassproxy " & _
              ", mail_body = @mail_body" & _
              ", email_type = @email_type" & _
              ", ews_url = @ews_url" & _
              ", ews_domain = @ews_domain" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", Syncdatetime = NULL " & _
              ", protocolunkid = @protocolunkid " & _
            "WHERE emailsetupunkid = @emailsetupunkid "

            'Hemant (01 Feb 2022) -- [protocolunkid]
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsTableDataUpdate(mintEmailsetupunkid, objDataOperation) = True Then

                If Insert_AtTranLog(objDataOperation, 2) = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Insert_AtTranLog(ByVal objDoOps As clsDataOperation, ByVal AuditType As Integer)
        Dim StrQ As String = String.Empty
        Dim extForce As Exception
        Dim dsList As New DataSet

        Try
            StrQ = "INSERT INTO hrmsConfiguration..atcfemail_setup ( " & _
              "  emailsetupunkid " & _
              ", sendername " & _
              ", senderaddress " & _
              ", reference " & _
              ", mailserverip " & _
              ", mailserverport " & _
              ", username " & _
              ", password " & _
              ", isloginssl " & _
              ", iscrt_authenticated " & _
              ", isbypassproxy " & _
              ", mail_body " & _
              ", email_type " & _
              ", ews_url " & _
              ", ews_domain " & _
              ", audituserunkid " & _
              ", audittypeid " & _
              ", auditdatetime " & _
              ", isweb " & _
              ", ip " & _
              ", host " & _
              ", form_name " & _
              ", protocolunkid " & _
            ") VALUES (" & _
              "  @emailsetupunkid " & _
              ", @sendername " & _
              ", @senderaddress " & _
              ", @reference " & _
              ", @mailserverip " & _
              ", @mailserverport " & _
              ", @username " & _
              ", @password " & _
              ", @isloginssl " & _
              ", @iscrt_authenticated " & _
              ", @isbypassproxy " & _
              ", @mail_body " & _
              ", @email_type " & _
              ", @ews_url " & _
              ", @ews_domain " & _
              ", @audituserunkid " & _
              ", @audittypeid " & _
              ", @auditdatetime " & _
              ", @isweb " & _
              ", @ip " & _
              ", @host " & _
              ", @form_name " & _
              ", @protocolunkid " & _
            ")"

            'Hemant (01 Feb 2022) -- [protocolunkid]
            objDoOps.ClearParameters()
            objDoOps.AddParameter("@emailsetupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmailsetupunkid.ToString)
            objDoOps.AddParameter("@sendername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSendername.ToString)
            objDoOps.AddParameter("@senderaddress", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSenderaddress.ToString)
            objDoOps.AddParameter("@reference", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReference.ToString)
            objDoOps.AddParameter("@mailserverip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMailserverip.ToString)
            objDoOps.AddParameter("@mailserverport", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMailserverport.ToString)
            objDoOps.AddParameter("@username", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrUsername.ToString)
            objDoOps.AddParameter("@password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPassword.ToString)
            objDoOps.AddParameter("@isloginssl", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsloginssl.ToString)
            objDoOps.AddParameter("@iscrt_authenticated", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscrt_authenticated.ToString)
            objDoOps.AddParameter("@isbypassproxy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsbypassproxy.ToString)
            objDoOps.AddParameter("@mail_body", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMail_Body.ToString)
            objDoOps.AddParameter("@email_type", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmail_Type.ToString)
            objDoOps.AddParameter("@ews_url", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEws_Url.ToString)
            objDoOps.AddParameter("@ews_domain", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEws_Domain.ToString)

            objDoOps.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDoOps.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDoOps.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDoOps.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb)
            objDoOps.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIp)
            objDoOps.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDoOps.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            'Hemant (01 Feb 2022) -- Start
            'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
            objDataOperation.AddParameter("@protocolunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProtocolunkid.ToString)
            'Hemant (01 Feb 2022) -- End

            dsList = objDoOps.ExecQuery(StrQ, "List")

            If objDoOps.ErrorMessage <> "" Then
                extForce = New Exception(objDoOps.ErrorNumber & ": " & objDoOps.ErrorMessage)
                Throw extForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
        End Try
        Return True

    End Function

    Public Function IsTableDataUpdate(ByVal intUnkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim strFields As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            strFields = " sendername,senderaddress,reference,mailserverip,mailserverport,username,password,isloginssl,iscrt_authenticated,isbypassproxy,mail_body,email_type,ews_url,ews_domain,protocolunkid "
            'Hemant (01 Feb 2022) -- [protocolunkid]
            strQ = "WITH CTE AS ( " & _
                            "SELECT TOP 1 " & strFields & " " & _
                            "FROM hrmsConfiguration..atcfemail_setup " & _
                            "WHERE atcfemail_setup.emailsetupunkid = @emailsetupunkid " & _
                            "AND atcfemail_setup.audittypeid <> 3 " & _
                            "ORDER BY atcfemail_setup.atemailsetupunkid DESC " & _
                        ") " & _
                    " " & _
                    "SELECT " & strFields & " " & _
                    "FROM hrmsConfiguration..cfemail_setup " & _
                    "WHERE cfemail_setup.emailsetupunkid = @emailsetupunkid " & _
                    "EXCEPT " & _
                    "SELECT * FROM cte "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@emailsetupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfemail_setup) </purpose>
    Public Function Void(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "UPDATE hrmsConfiguration..cfemail_setup SET " & _
                      "  isvoid = 1 " & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                      ", Syncdatetime = NULL " & _
            "WHERE emailsetupunkid = @emailsetupunkid "

            objDataOperation.AddParameter("@emailsetupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT 1 FROM cfconfiguration " & _
                    "WHERE cfconfiguration.key_name = 'RecruitmentEmailSetupUnkid' " & _
                    "AND cfconfiguration.key_value = @key_value "

            objDataOperation.AddParameter("@key_value", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  emailsetupunkid " & _
              ", sendername " & _
              ", senderaddress " & _
              ", reference " & _
              ", mailserverip " & _
              ", mailserverport " & _
              ", username " & _
              ", password " & _
              ", isloginssl " & _
              ", iscrt_authenticated " & _
              ", isbypassproxy " & _
              ", mail_body " & _
              ", email_type " & _
              ", ews_url " & _
              ", ews_domain " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(protocolunkid,0) AS protocolunkid " & _
             "FROM hrmsConfiguration..cfemail_setup " & _
             "WHERE name = @name " & _
             "AND code = @code "
            'Hemant (01 Feb 2022) -- [protocolunkid]
            If intUnkid > 0 Then
                strQ &= " AND emailsetupunkid <> @emailsetupunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@emailsetupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "SMTP")
			Language.setMessage(mstrModuleName, 3, "EWS")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
