﻿Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System.Globalization



Public Class clsSAGEM

    Private Shared ReadOnly mstrModuleName As String = "clsSAGEM"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""


#Region " Private variables "

    Private mstrDatabaseServer As String = ""
    Private mstrDatabaseName As String = ""
    Private mstrDatabaseUserName As String = ""
    Private mstrDatabasePassword As String = ""
    Private mdtFromdate As Date
    Private mdtTodate As Date

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get or Set DatabaseServer
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DatabaseServer() As String
        Get
            Return mstrDatabaseServer
        End Get
        Set(ByVal value As String)
            mstrDatabaseServer = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DatabaseName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DatabaseName() As String
        Get
            Return mstrDatabaseName
        End Get
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DatabaseUserName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DatabaseUserName() As String
        Get
            Return mstrDatabaseUserName
        End Get
        Set(ByVal value As String)
            mstrDatabaseUserName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DatabasePassword
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DatabasePassword() As String
        Get
            Return mstrDatabasePassword
        End Get
        Set(ByVal value As String)
            mstrDatabasePassword = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fromdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Fromdate() As Date
        Get
            Return mdtFromdate
        End Get
        Set(ByVal value As Date)
            mdtFromdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set todate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Todate() As Date
        Get
            Return mdtTodate
        End Get
        Set(ByVal value As Date)
            mdtTodate = value
        End Set
    End Property

#End Region

#Region " Private Methods "

    Public Function GetSAGEMAttendanceData() As DataTable
        Dim dtTable As DataTable = Nothing
        Dim exForce As Exception
        Dim strQ As String = ""
        Try

            'Pinkal (29-Dec-2021)-- Start
            'EHNAHNCEMENT : MST MORPHO TNA DEVICE INEGRATION.

            Dim StrConn As String = ""
            Dim mblnMorphoAccessIdentifier As Boolean = False
            Dim mblnOnSiteOffSiteStatus As Boolean = False
            Using SQLConn As SqlClient.SqlConnection = New SqlClient.SqlConnection
                StrConn = "Data Source=" & mstrDatabaseServer & ";Initial Catalog=" & mstrDatabaseName & "; User ID=" & mstrDatabaseUserName & ";Password = " & mstrDatabasePassword & " "
                SQLConn.ConnectionString = StrConn
                SQLConn.Open()

                strQ = "SELECT 1 WHERE EXISTS (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'user_' and COLUMN_NAME = 'MORPHOACCESSIDENTIFIER')"
                Dim sqlcmd As New SqlClient.SqlCommand(strQ)
                sqlcmd.Connection = SQLConn
                sqlcmd.CommandTimeout = 0
                sqlcmd.Parameters.Clear()
                Dim adp As New SqlClient.SqlDataAdapter(sqlcmd)
                Dim dtExist As New DataTable
                adp.Fill(dtExist)

                If dtExist IsNot Nothing AndAlso dtExist.Rows.Count > 0 Then mblnMorphoAccessIdentifier = True

                strQ = ""
                strQ = "SELECT 1 WHERE EXISTS (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'user_' and COLUMN_NAME = 'ONSITEOFFSITESTATUS')"
                sqlcmd = New SqlClient.SqlCommand(strQ)
                sqlcmd.Connection = SQLConn
                sqlcmd.CommandTimeout = 0
                sqlcmd.Parameters.Clear()
                adp = New SqlClient.SqlDataAdapter(sqlcmd)
                dtExist = New DataTable
                adp.Fill(dtExist)

                If dtExist IsNot Nothing AndAlso dtExist.Rows.Count > 0 Then mblnOnSiteOffSiteStatus = True

                strQ = ""

                strQ = " SELECT " & _
                          "     BiometricDevice.ID AS DeviceID " & _
                          ",    BiometricDevice.NAME_ AS DeviceName " & _
                          ",    BiometricDevice.TIMEZONE As Timezone " & _
                          ",    User_.ID AS UserID " & _
                          ",    EMPLOYEEID " & _
                          ",    ISNULL(FIRSTNAME,'') AS FIRSTNAME " & _
                          ",    ISNULL(MIDDLENAME,'') AS MIDDLENAME  " & _
                          ",    ISNULL(LASTNAME,'') AS LASTNAME " & _
                          ",    MORPHOACCESSDISPLAYNAME " & _
                          ",    DISABLED " & _
                          ",    AccessLog.LOGDATETIME " & _
                          ",    AccessLog.ID AS AccessID "

                If mblnMorphoAccessIdentifier Then
                    strQ &= ",    MORPHOACCESSIDENTIFIER "
                Else
                    strQ &= ",  EMPLOYEEID AS  MORPHOACCESSIDENTIFIER "
                End If

                If mblnOnSiteOffSiteStatus Then
                    strQ &= ",    ONSITEOFFSITESTATUS "
                Else
                    strQ &= ",  0 AS ONSITEOFFSITESTATUS "
                End If

                strQ &= " FROM AccessLog " & _
                          " JOIN User_ ON User_.ID = AccessLog.USERID " & _
                          " JOIN BiometricDevice ON BiometricDevice.ID = AccessLog.MORPHOACCESSID " & _
                          " WHERE CONVERT(char(8),AccessLog.LOGDATETIME,112) BETWEEN @FromDate AND @ToDate " & _
                          " ORDER by User_.ID,AccessLog.LOGDATETIME "

                'Pinkal (29-Dec-2021) -- End

                sqlcmd = New SqlClient.SqlCommand(strQ)
                sqlcmd.Connection = SQLConn
                sqlcmd.CommandTimeout = 0
                sqlcmd.Parameters.Clear()
                sqlcmd.Parameters.Add("@FromDate", SqlDbType.NVarChar).Value = eZeeDate.convertDate(mdtFromdate).ToString()
                sqlcmd.Parameters.Add("@ToDate", SqlDbType.NVarChar).Value = eZeeDate.convertDate(mdtTodate).ToString()
                adp = New SqlClient.SqlDataAdapter(sqlcmd)
                Dim dsList As New DataSet
                adp.Fill(dsList)
                If dsList IsNot Nothing Then dtTable = dsList.Tables(0)
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSAGEMAttendanceData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
        Return dtTable
    End Function

#End Region

End Class


