﻿
Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System.Globalization

Public Class clsHandpunch
    Private Shared ReadOnly mstrModuleName As String = "clsHandpunch"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""


#Region " Private variables "

    Private mdtFromdate As Date
    Private mdtTodate As Date

#End Region


#Region " Properties "

    ''' <summary>
    ''' Purpose: Get or Set fromdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Fromdate() As Date
        Get
            Return mdtFromdate
        End Get
        Set(ByVal value As Date)
            mdtFromdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set todate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Todate() As Date
        Get
            Return mdtTodate
        End Get
        Set(ByVal value As Date)
            mdtTodate = value
        End Set
    End Property

#End Region


#Region " Private Methods "

    Public Function GetHandpunchAttendanceData() As DataTable
        Dim dtTable As DataTable = Nothing
        Dim exForce As Exception
        Dim strQ As String = ""
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()

            strQ = " SELECT DV.DeviceName, Att.DeviceUserID,Att.DeviceID,Att.AttendanceTime,Att.TACode,Att.DownloadTime FROM " & _
                      " Handpunch_Data..AttendanceLogs Att" & _
                      " JOIN Handpunch_Data..DeviceList Dv ON DV.DeviceID = Att.DeviceID AND DV.Active = 1 " & _
                       " WHERE convert(CHAR(8),Att.AttendanceTime,112) >= @FromDate AND convert(CHAR(8),Att.AttendanceTime,112) <= @ToDate Order by Att.DeviceUserID,Att.DeviceID"

            objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFromdate))
            objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtTodate))

            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing Then dtTable = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetHandpunchAttendanceData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
        Return dtTable
    End Function


#End Region

End Class
