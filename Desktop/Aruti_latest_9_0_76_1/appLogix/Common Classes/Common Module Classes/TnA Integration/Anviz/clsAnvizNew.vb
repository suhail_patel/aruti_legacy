﻿Imports System.Runtime.InteropServices

Public Class clsAnvizNew
    Public Enum CustomType As Integer
        ANVIZ_CUSTOM_EMPLOYEE_FOR_DR_ADD_NAME2 = &H10
        ANVIZ_CUSTOM_EMPLOYEE_FOR_W2_ADD_TIME = &H20
        DEV_TYPE_VER_4_NEWID = &H40
        ANVIZ_CUSTOM_MESSAGE_FOR_SEATS_200 = &H400
    End Enum

    Public Enum MessageType As Integer
        DEV_TYPE_FLAG_MSG_ASCII_48 = &H100
        DEV_TYPE_FLAG_MSG_UNICODE_96 = &H200
    End Enum

    Public Enum EmployeeType As Integer
        DEV_TYPE_FLAG_MSG_ASCII_32 = &H2
        DEV_TYPE_FLAG_MSG_UNICODE_40 = &H4
    End Enum

    Public Enum NetCardType As Integer
        NETCARD_WITHOUT_DNS = 63
        NETCARD_WITH_DNS = 167
        NETCARD_TWO_CARD = 102
    End Enum

    Public Enum MsgType As Integer
        CCHEX_RET_RECORD_INFO_TYPE = 1
        CCHEX_RET_DEV_LOGIN_TYPE
        CCHEX_RET_DEV_LOGOUT_TYPE
        CCHEX_RET_DLFINGERPRT_TYPE = 4
        CCHEX_RET_ULFINGERPRT_TYPE = 5
        CCHEX_RET_ULEMPLOYEE_INFO_TYPE = 6
        CCHEX_RET_ULEMPLOYEE2_INFO_TYPE = 7
        CCHEX_RET_ULEMPLOYEE2_UNICODE_INFO_TYPE = 8
        CCHEX_RET_LIST_PERSON_INFO_TYPE = 9
        CCHEX_RET_MSGGETBYIDX_INFO_TYPE = 12
        CCHEX_RET_MSGGETBYIDX_UNICODE_INFO_TYPE = 13
        CCHEX_RET_MSGADDNEW_INFO_TYPE = 14
        CCHEX_RET_MSGADDNEW_UNICODE_INFO_TYPE = 15
        CCHEX_RET_MSGDELBYIDX_INFO_TYPE = 16
        CCHEX_RET_MSGGETALLHEAD_INFO_TYPE = 17
        CCHEX_RET_REBOOT_TYPE = 18
        CCHEX_RET_DEV_STATUS_TYPE = 19
        CCHEX_RET_MSGGETALLHEADUNICODE_INFO_TYPE = 20
        CCHEX_RET_SETTIME_TYPE = 21
        CCHEX_RET_UPLOADFILE_TYPE = 22 ' = 22
        CCHEX_RET_GETNETCFG_TYPE = 23
        CCHEX_RET_SETNETCFG_TYPE = 24
        CCHEX_RET_GET_SN_TYPE = 25
        CCHEX_RET_SET_SN_TYPE = 26
        CCHEX_RET_DLEMPLOYEE_3_TYPE = 27 ' 761
        CCHEX_RET_ULEMPLOYEE_3_TYPE = 28 ' 761
        CCHEX_RET_GET_BASIC_CFG_TYPE = 29
        CCHEX_RET_SET_BASIC_CFG_TYPE = 30
        CCHEX_RET_DEL_PERSON_INFO_TYPE = 31
        CCHEX_RET_DEL_RECORD_OR_FLAG_INFO_TYPE = 33
        CCHEX_RET_MSGGETBYIDX_UNICODE_S_DATE_INFO_TYPE = 34
        CCHEX_RET_MSGADDNEW_UNICODE_S_DATE_INFO_TYPE = 35
        CCHEX_RET_MSGGETALLHEADUNICODE_S_DATE_INFO_TYPE = 36
        CCHEX_RET_GET_BASIC_CFG2_TYPE = 37
        CCHEX_RET_SET_BASIC_CFG2_TYPE = 38
        CCHEX_RET_GETTIME_TYPE = 39
        CCHEX_RET_INIT_USER_AREA_TYPE = 40
        CCHEX_RET_INIT_SYSTEM_TYPE = 41
        CCHEX_RET_GET_PERIOD_TIME_TYPE = 42
        CCHEX_RET_SET_PERIOD_TIME_TYPE = 43
        CCHEX_RET_GET_TEAM_INFO_TYPE = 44
        CCHEX_RET_SET_TEAM_INFO_TYPE = 45
        CCHEX_RET_ADD_FINGERPRINT_ONLINE_TYPE = 46
        CCHEX_RET_FORCED_UNLOCK_TYPE = 47
        CCHEX_RET_UDP_SEARCH_DEV_TYPE = 48
        CCHEX_RET_UDP_SET_DEV_CONFIG_TYPE = 49


        '

        CCHEX_RET_GET_INFOMATION_CODE_TYPE = 50
        CCHEX_RET_SET_INFOMATION_CODE_TYPE = 51
        CCHEX_RET_GET_BELL_INFO_TYPE = 52
        CCHEX_RET_SET_BELL_INFO_TYPE = 53
        CCHEX_RET_LIVE_SEND_ATTENDANCE_TYPE = 54
        CCHEX_RET_GET_USER_ATTENDANCE_STATUS_TYPE = 55
        CCHEX_RET_SET_USER_ATTENDANCE_STATUS_TYPE = 56
        CCHEX_RET_CLEAR_ADMINISTRAT_FLAG_TYPE = 57
        CCHEX_RET_GET_SPECIAL_STATUS_TYPE = 58
        CCHEX_RET_GET_ADMIN_CARD_PWD_TYPE = 59
        CCHEX_RET_SET_ADMIN_CARD_PWD_TYPE = 60
        CCHEX_RET_GET_DST_PARAM_TYPE = 61
        CCHEX_RET_SET_DST_PARAM_TYPE = 62
        CCHEX_RET_GET_DEV_EXT_INFO_TYPE = 63
        CCHEX_RET_SET_DEV_EXT_INFO_TYPE = 64
        CCHEX_RET_GET_BASIC_CFG3_TYPE = 65
        CCHEX_RET_SET_BASIC_CFG3_TYPE = 66
        CCHEX_RET_CONNECTION_AUTHENTICATION_TYPE = 67
        CCHEX_RET_GET_RECORD_NUMBER_TYPE = 68
        CCHEX_RET_GET_RECORD_BY_EMPLOYEE_TIME_TYPE = 69
        CCHEX_RET_GET_RECORD_INFO_STATUS_TYPE = 70
        CCHEX_RET_GET_NEW_RECORD_INFO_TYPE = 71
        CCHEX_RET_ULEMPLOYEE2W2_INFO_TYPE = 72
        CCHEX_RET_GET_BASIC_CFG5_TYPE = 73
        CCHEX_RET_SET_BASIC_CFG5_TYPE = 74
        CCHEX_RET_GET_CARD_ID_TYPE = 75
        CCHEX_RET_SET_DEV_CURRENT_STATUS_TYPE = 76
        CCHEX_RET_GET_URL_TYPE = 77
        CCHEX_RET_SET_URL_TYPE = 78
        CCHEX_RET_GET_STATUS_SWITCH_TYPE = 79
        CCHEX_RET_SET_STATUS_SWITCH_TYPE = 80
        CCHEX_RET_GET_STATUS_SWITCH_EXT_TYPE = 81
        CCHEX_RET_SET_STATUS_SWITCH_EXT_TYPE = 82
        CCHEX_RET_UPDATEFILE_STATUS_TYPE = 83
        CCHEX_RET_GET_MACHINE_ID_TYPE = 84
        CCHEX_RET_SET_MACHINE_ID_TYPE = 85
        CCHEX_RET_GET_MACHINE_TYPE_TYPE = 86
        CCHEX_RET_UPLOAD_RECORD_TYPE = 87
        CCHEX_RET_GET_ONE_EMPLOYEE_INFO_TYPE = 88
        CCHEX_RET_ULEMPLOYEE_VER_4_NEWID_TYPE = 89
        CCHEX_RET_MANAGE_LOG_RECORD_TYPE = 90
        CCHEX_RET_PICTURE_GET_TOTAL_NUMBER_TYPE = 91
        CCHEX_RET_PICTURE_GET_ALL_HEAD_TYPE = 92
        CCHEX_RET_PICTURE_GET_DATA_BY_EID_TIME_TYPE = 93
        CCHEX_RET_PICTURE_DEL_DATA_BY_EID_TIME_TYPE = 94
        CCHEX_RET_CLINECT_CONNECT_FAIL_TYPE = 200
        CCHEX_RET_DEV_LOGIN_CHANGE_TYPE = 201
        CCHEX_SAC_DOWNLOAD_EMPLOYEE_TYPE = 301
        CCHEX_SAC_UPLOAD_EMPLOYEE_TYPE = 302
        CCHEX_SAC_DOWNLOAD_GROUP_TYPE = 303
        CCHEX_SAC_UPLOAD_GROUP_TYPE = 304
        CCHEX_SAC_DOWNLOAD_EMPLOYEE_WITH_GROUP_TYPE = 305
        CCHEX_SAC_UPLOAD_EMPLOYEE_WITH_GROUP_TYPE = 306
        CCHEX_SAC_GET_DOOR_INFO_TYPE = 307
        CCHEX_SAC_SET_DOOR_INFO_TYPE = 308
        CCHEX_SAC_DOWNLOAD_DOOR_GROUP_TYPE = 309
        CCHEX_SAC_UPLOAD_DOOR_GROUP_TYPE = 310
        CCHEX_SAC_UPLOAD_DOOR_WITH_DOORGROUP_TYPE = 311
        CCHEX_SAC_DOWNLOAD_DOOR_WITH_DOORGROUP_TYPE = 312
        CCHEX_SAC_DOWNLOAD_TIME_FRAME_INFO_TYPE = 313
        CCHEX_SAC_UPLOAD_TIME_FRAME_INFO_TYPE = 314
        CCHEX_SAC_DOWNLOAD_TIME_FRAME_GROUP_TYPE = 315
        CCHEX_SAC_UPLOAD_TIME_FRAME_GROUP_TYPE = 316
        CCHEX_SAC_DOWNLOAD_TIME_FRAME_WITH_TIME_GROUP_TYPE = 317
        CCHEX_SAC_UPLOAD_TIME_FRAME_WITH_TIME_GROUP_TYPE = 318
        CCHEX_SAC_DOWNLOAD_ACCESS_CONTROL_GROUP_TYPE = 319
        CCHEX_SAC_UPLOAD_ACCESS_CONTROL_GROUP_TYPE = 320
    End Enum

    Public Const FP_LEN As Integer = 15360

    <StructLayout(LayoutKind.Sequential, Size:=27, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_NETCFG_INFO_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public IpAddr As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public IpMask As Byte()   '子网掩码
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=6)> _
        Public MacAddr As Byte()  'MAC地址
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public GwAddr As Byte()   '网关地址
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public ServAddr As Byte() '服务器ip
        Public RemoteEnable As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public Port As Byte()     '端口
        Public Mode As Byte       '方式
        Public DhcpEnable As Byte
    End Structure '27 bytes
    <StructLayout(LayoutKind.Sequential, Size:=8, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_COMMON_STRU
        Public MachineId As UInteger
        Public Result As Integer '0 ok, -1 err
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=8, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_REBOOT_STRU
        Public MachineId As UInteger
        Public Result As Integer
    End Structure


    '数组网络
    <StructLayout(LayoutKind.Sequential, Size:=8, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_SETNETCFG_STRU
        Public MachineId As UInteger
        Public Result As Integer
    End Structure


    '更新回复
    <StructLayout(LayoutKind.Sequential, Size:=8, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_UPLOADFILE_STRU
        Public MachineId As UInteger
        Public Result As Integer '0 ok, -1 err
        Public TotalBytes As UInteger
        Public SendBytes As UInteger
    End Structure


    '网络配置回复
    <StructLayout(LayoutKind.Sequential, Size:=8, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GETNETCFG_STRU
        Public MachineId As UInteger
        Public Result As Integer '0 ok, -1 err
        Public Cfg As CCHEX_NETCFG_INFO_STRU
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=14, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_UPLOAD_RECORD_INFO_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()    '
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public [date] As Byte()          '日期时间
        Public back_id As Byte           '备份号
        Public record_type As Byte       '记录类型
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public work_type As Byte()      '工种        (ONLY use 3bytes )
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=37, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_UPLOAD_RECORD_INFO_STRU_VER_4_NEWID
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)> _
        Public EmployeeId As Byte()    '
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public [date] As Byte()          '日期时间
        Public back_id As Byte           '备份号
        Public record_type As Byte       '记录类型
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public work_type As Byte()      '工种        (ONLY use 3bytes )
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=14 + 4 + 4, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_LIVE_SEND_ATTENDANCE_STRU
        Public MachineId As UInteger         '机器号
        Private Result As Integer '0 ok, -1 err
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()    '
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public [Date] As Byte()          '日期时间
        Public BackId As Byte           '备份号
        Public RecordType As Byte       '记录类型
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public WorkType As Byte()      '工种        (ONLY use 3bytes )
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=37 + 4 + 4, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_LIVE_SEND_ATTENDANCE_STRU_VER_4_NEWID
        Public MachineId As UInteger         '机器号
        Private Result As Integer '0 ok, -1 err
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)> _
        Public EmployeeId As Byte()    '
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public [Date] As Byte()          '日期时间
        Public BackId As Byte           '备份号
        Public RecordType As Byte       '记录类型
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public WorkType As Byte()      '工种        (ONLY use 3bytes )
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=28, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_RECORD_INFO_STRU
        Public MachineId As UInteger         '机器号
        Public NewRecordFlag As Byte    '是否是新记录
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()    '
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public [Date] As Byte()          '日期时间
        Public BackId As Byte           '备份号
        Public RecordType As Byte       '记录类型
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public WorkType As Byte()      '工种        (ONLY use 3bytes )
        Public Rsv As Byte
        Public CurIdx As UInteger             'add  VER 22
        Public TotalCnt As UInteger           'add  VER 22
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=51, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_RECORD_INFO_STRU_VER_4_NEWID
        Public MachineId As UInteger         '机器号
        Public NewRecordFlag As Byte    '是否是新记录
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)> _
        Public EmployeeId As Byte()    '
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public [Date] As Byte()          '日期时间
        Public BackId As Byte           '备份号
        Public RecordType As Byte       '记录类型
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public WorkType As Byte()      '工种        (ONLY use 3bytes )
        Public Rsv As Byte
        Public CurIdx As UInteger             'add  VER 22
        Public TotalCnt As UInteger           'add  VER 22
    End Structure



    'device login-logout struct
    <StructLayout(LayoutKind.Sequential, Size:=52, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_DEV_LOGIN_STRU
        Public DevIdx As Integer
        Public MachineId As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=24)> _
        Public Addr As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public Version As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public DevType As Byte()
        Public DevTypeFlag As UInteger
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=52, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_DEV_LOGOUT_STRU
        Public DevIdx As Integer
        Public MachineId As UInteger
        Public Live As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=24)> _
        Public Addr As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public Version As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public DevType As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=11, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_MSGHEAD_INFO_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()
        Public StartYear As Byte
        Public StartMonth As Byte
        Public StartDay As Byte
        Public EndYear As Byte
        Public EndMonth As Byte
        Public EndDay As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=34, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_MSGHEAD_INFO_STRU_VER_4_NEWID
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)> _
        Public EmployeeId As Byte()
        Public StartYear As Byte
        Public StartMonth As Byte
        Public StartDay As Byte
        Public EndYear As Byte
        Public EndMonth As Byte
        Public EndDay As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=17, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_MSGHEADUNICODE_INFO_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()
        Public StartYear As Byte
        Public StartMonth As Byte
        Public StartDay As Byte
        Public StartHour As Byte
        Public StartMin As Byte
        Public StartSec As Byte
        Public EndYear As Byte
        Public EndMonth As Byte
        Public EndDay As Byte
        Public EndHour As Byte
        Public EndMin As Byte
        Public EndSec As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=40, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_MSGHEADUNICODE_INFO_STRU_VER_4_NEWID
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)> _
        Public EmployeeId As Byte()
        Public StartYear As Byte
        Public StartMonth As Byte
        Public StartDay As Byte
        Public StartHour As Byte
        Public StartMin As Byte
        Public StartSec As Byte
        Public EndYear As Byte
        Public EndMonth As Byte
        Public EndDay As Byte
        Public EndHour As Byte
        Public EndMin As Byte
        Public EndSec As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=12, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_MSGGETBYIDX_UNICODE_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public Len As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=12, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_MSGADDNEW_UNICODE_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public Len As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=12, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_MSGGETALLHEAD_UNICODE_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public Len As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=9, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_MSGDELBYIDX_UNICODE_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public Idx As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=28, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_DEV_STATUS_STRU
        Public MachineId As UInteger
        Public EmployeeNum As UInteger
        Public FingerPrtNum As UInteger
        Public PasswdNum As UInteger
        Public CardNum As UInteger
        Public TotalRecNum As UInteger
        Public NewRecNum As UInteger
    End Structure

    Public Const SN_LEN As Integer = 16

    <StructLayout(LayoutKind.Sequential, Size:=24, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_SN_STRU
        Public MachineId As UInteger
        Public Result As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=SN_LEN)> _
        Public sn As Byte()
    End Structure


    ' basic config info
    <StructLayout(LayoutKind.Sequential, Size:=20, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_GET_BASIC_CFG_INFO_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public software_version As Byte()
        Public password As UInteger
        Public delay_for_sleep As Byte
        Public volume As Byte
        Public language As Byte
        Public date_format As Byte
        Public time_format As Byte
        Public machine_status As Byte
        Public modify_language As Byte
        Public cmd_version As Byte
    End Structure '20 bytes
    <StructLayout(LayoutKind.Sequential, Size:=13, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_SET_BASIC_CFG_INFO_STRU
        Public password As UInteger
        Public pwd_len As Byte
        Public delay_for_sleep As Byte
        Public volume As Byte
        Public language As Byte
        Public date_format As Byte
        Public time_format As Byte
        Public machine_status As Byte
        Public modify_language As Byte
        Public reserved As Byte
    End Structure '13 bytes
    <StructLayout(LayoutKind.Sequential, Size:=8, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_BASIC_CFG_STRU
        Public MachineId As UInteger
        Public Result As Integer '0 ok, -1 err
        Public Cfg As CCHEX_GET_BASIC_CFG_INFO_STRU
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=16, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_DEL_EMPLOYEE_INFO_STRU
        Public MachineId As UInteger
        Public Result As Integer '0 ok, -1 err
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()  ' 5 bytes
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public padding As Byte()  ' 5 bytes,12 digitals
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=36, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_DEL_EMPLOYEE_INFO_STRU_VER_4_NEWID
        Public MachineId As UInteger
        Public Result As Integer '0 ok, -1 err
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)> _
        Public EmployeeId As Byte()  ' 
    End Structure

    ' list,modify and delete person
    <StructLayout(LayoutKind.Sequential, Size:=294, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_PERSON_INFO_STRU
        Public MachineId As UInteger
        Public CurIdx As Integer
        Public TotalCnt As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()  ' 5 bytes,12 digitals
        Public password_len As Byte
        Public max_password As Byte  ' now only 6 digitals, do not modify
        Public password As Integer       ' do not exceed max_password digitals
        Public max_card_id As Byte   ' 6 for 3 bytes,10 for 4 bytes, do not modify
        Public card_id As UInteger       ' do not exceed max_card_id digitals
        Public max_EmployeeName As Byte  ' may be 10 or 20 or 64, do not modify
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=64)> _
        Public EmployeeName As Byte() ' do not exceed max_EmployeeName digitals
        Public DepartmentId As Byte
        Public GroupId As Byte
        Public Mode As Byte
        Public Fp_Status As UInteger    ' 0~9:fp; 10:face; 11:iris1; 12:iris2
        Public Rserved1 As Byte      ' for 22
        Public Rserved2 As Byte      ' for 72 and 22
        Public Special As Byte

        ' DR info
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=160)> _
        Public EmployeeName2 As Byte() ' do not exceed max_EmployeeName digitals
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=13)> _
        Public RFC As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=18)> _
        Public CURP As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=302, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_EMPLOYEE_INFO_STRU_EXT_INF_FOR_W2
        Private MachineId As UInteger
        Private CurIdx As Integer
        Private TotalCnt As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()  ' 5 bytes,12 digitals
        Public password_len As Byte
        Public max_password As Byte  ' now only 6 digitals, do not modify
        Public password As Integer       ' do not exceed max_password digitals
        Public max_card_id As Byte   ' 6 for 3 bytes,10 for 4 bytes, do not modify
        Public card_id As UInteger       ' do not exceed max_card_id digitals
        Public max_EmployeeName As Byte  ' may be 10 or 20 or 64, do not modify
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=64)> _
        Public EmployeeName As Byte() ' do not exceed max_EmployeeName digitals
        Public DepartmentId As Byte
        Public GroupId As Byte
        Public Mode As Byte
        Public Fp_Status As UInteger    ' 0~9:fp; 10:face; 11:iris1; 12:iris2
        Public Rserved1 As Byte      ' for 22
        Public Rserved2 As Byte      ' for 72 and 22
        Public Special As Byte

        ' DR info
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=160)> _
        Public EmployeeName2 As Byte() ' do not exceed max_EmployeeName digitals
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=13)> _
        Public RFC As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=18)> _
        Public CURP As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public start_date As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public end_date As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=325, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_DLEMPLOYEE_INFO_STRU_EXT_INF_FOR_VER_4
        Public MachineId As UInteger
        Public CurIdx As Integer
        Public TotalCnt As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)> _
        Public EmployeeId As Byte()  ' 5 bytes,12 digitals
        Public password_len As Byte
        Public max_password As Byte  ' now only 6 digitals, do not modify
        Public password As Integer       ' do not exceed max_password digitals
        Public max_card_id As Byte   ' 6 for 3 bytes,10 for 4 bytes, do not modify
        Public card_id As UInteger       ' do not exceed max_card_id digitals
        Public max_EmployeeName As Byte  ' may be 10 or 20 or 64, do not modify
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=64)> _
        Public EmployeeName As Byte() ' do not exceed max_EmployeeName digitals
        Public DepartmentId As Byte
        Public GroupId As Byte
        Public Mode As Byte
        Public Fp_Status As UInteger    ' 0~9:fp; 10:face; 11:iris1; 12:iris2
        Public Rserved1 As Byte      ' for 22
        Public Rserved2 As Byte      ' for 72 and 22
        Public Special As Byte

        ' DR info
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=160)> _
        Public EmployeeName2 As Byte() ' do not exceed max_EmployeeName digitals
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=13)> _
        Public RFC As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=18)> _
        Public CURP As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public start_date As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public end_date As Byte()
    End Structure

    '[StructLayout(LayoutKind.Sequential, Size = 8, CharSet = CharSet.Ansi), Serializable]
    '    public struct CCHEX_RET_PERSON_INFO_STRU
    '    {
    '        public uint MachineId;
    '        public int Result; //0 ok, -1 err
    '        public CCHEX_PERSON_INFO_STRU person;
    '    }

    <StructLayout(LayoutKind.Sequential, Size:=6, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_DEL_PERSON_INFO_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()  ' 5 bytes,12 digitals
        Public operation As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=29, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_DEL_EMPLOYEE_INFO_STRU_EXT_INF_ID_VER_4_NEWID
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)> _
        Public EmployeeId As Byte()  '
        Public operation As Byte
    End Structure

    '获取单个员工资料
    <StructLayout(LayoutKind.Sequential, Size:=5, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_GET_ONE_EMPLOYEE_INFO_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()  ' 5 bytes
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=28, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_GET_ONE_EMPLOYEE_INFO_STRU_ID_VER_4_NEWID
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)> _
        Public EmployeeId As Byte()  '
    End Structure


    ' download finger print
    <StructLayout(LayoutKind.Sequential, Size:=fp_len + 18, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_DLFINGERPRT_STRU
        Public MachineId As UInteger
        Public Result As Integer '0 ok, -1 err
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()
        Public FpIdx As Byte
        Public fp_len As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=clsAnvizNew.FP_LEN)> _
        Public Data As Byte()
    End Structure

    ' download finger print
    <StructLayout(LayoutKind.Sequential, Size:=fp_len + 41, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_DLFINGERPRT_STRU_VER_4_NEWID
        Public MachineId As UInteger
        Public Result As Integer '0 ok, -1 err
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)> _
        Public EmployeeId As Byte()
        Public FpIdx As Byte
        Public fp_len As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=clsAnvizNew.FP_LEN)> _
        Public Data As Byte()
    End Structure



    ' delete record or new flag
    <StructLayout(LayoutKind.Sequential, Size:=5, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_DEL_RECORD_INFO_STRU
        Public del_type As Byte
        Public del_count As UInteger
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=12, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_DEL_RECORD_STRU
        Public MachineId As UInteger
        Public Result As Integer '0 ok, -1 err
        Public deleted_count As UInteger
    End Structure


    'get time~~~~~~~~~~~~~~~~~~~~~
    <StructLayout(LayoutKind.Sequential, Size:=24, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_MSG_GETTIME_STRU
        Public Year As UInteger
        Public Month As UInteger
        Public Day As UInteger
        Public Hour As UInteger
        Public Min As UInteger
        Public Sec As UInteger
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=32, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_MSG_GETTIME_STRU_EXT_INF
        Public MachineId As UInteger
        Public Result As Integer '0 ok, -1 err
        Public config As CCHEX_MSG_GETTIME_STRU
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=16, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_GET_BASIC_CFG_INFO2_STRU_EXT_INF
        Public compare_level As Byte
        Public wiegand_range As Byte
        Public wiegand_type As Byte
        Public work_code As Byte
        Public real_time_send As Byte
        Public auto_update As Byte
        Public bell_lock As Byte
        Public lock_delay As Byte
        Public record_over_alarm As UInteger
        Public re_attendance_delay As Byte
        Public door_sensor_alarm As Byte
        Public bell_delay As Byte
        Public correct_time As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=24, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_BASIC_CFG2_STRU_EXT_INF
        Public MachineId As UInteger
        Public Result As Integer '0 ok, -1 err
        Public config As CCHEX_GET_BASIC_CFG_INFO2_STRU_EXT_INF
    End Structure

    'Period of time
    <StructLayout(LayoutKind.Sequential, Size:=4, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_GET_PERIOD_TIME_ONE_STRU_EXT_INF
        Public StartHour As Byte
        Public StartMin As Byte
        Public EndHour As Byte
        Public EndMin As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=36, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_GET_PERIOD_TIME_STRU_EXT_INF
        Public MachineId As UInteger
        Public Result As Integer '0 ok, -1 err
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=7)> _
        Public day_week As CCHEX_GET_PERIOD_TIME_ONE_STRU_EXT_INF()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=29, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_SET_PERIOD_TIME_STRU_EXT_INF
        Public SerialNumbe As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=7)> _
        Public day_week As CCHEX_GET_PERIOD_TIME_ONE_STRU_EXT_INF()
    End Structure


    'Team Info
    <StructLayout(LayoutKind.Sequential, Size:=12, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_GET_TEAM_INFO_STRU_EXT_INF
        Public MachineId As UInteger
        Public Result As Integer '0 ok, -1 err
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public PeriodTimeNumber As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=5, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_SET_TEAM_INFO_STRU_EXT_INF
        Public TeamNumbe As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public PeriodTimeNumber As Byte()
    End Structure

    'bell  Info
    <StructLayout(LayoutKind.Sequential, Size:=3, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_BELL_TIME_POINT
        Public hour As Byte
        Public minute As Byte
        Public flag_week As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=100, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_BELL_INFO_STRU
        Public MachineId As UInteger
        Public Result As Integer '0 ok, -1 err
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=30)> _
        Public time_point As CCHEX_RET_GET_BELL_TIME_POINT()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public padding As Byte()
    End Structure

    'Add
    <StructLayout(LayoutKind.Sequential, Size:=6, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_ADD_FINGERPRINT_ONLINE_STRU_EXT_INF
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()  ' 5 bytes
        Public BackupNum As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=29, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_ADD_FINGERPRINT_ONLINE_STRU_EXT_INF_ID_VER_4_NEWID
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)> _
        Public EmployeeId As Byte()  ' 5 bytes
        Public BackupNum As Byte
    End Structure



    'forced unlock
    <StructLayout(LayoutKind.Sequential, Size:=6, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_FORCED_UNLOCK_STRU_EXT_INF
        Public LockCmd As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()
    End Structure


    'UDP  search dev       
    <StructLayout(LayoutKind.Sequential, Size:=25, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_DEV_NET_INFO_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public IpAddr As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public IpMask As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public GwAddr As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=6)> _
        Public MacAddr As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public ServAddr As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public Port As Byte()
        Public NetMode As Byte
    End Structure '25 bytes
    <StructLayout(LayoutKind.Sequential, Size:=63, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_UDP_SEARCH_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=10)> _
        Public DevType As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public DevSerialNum As Byte()
        Public DevNetInfo As CCHEX_DEV_NET_INFO_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public Version As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public Reserved As Byte()
    End Structure '  63   ::  0:Dev without DNS;
    <StructLayout(LayoutKind.Sequential, Size:=167, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_UDP_SEARCH_WITH_DNS_STRU
        Public BasicSearch As CCHEX_UDP_SEARCH_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public Dns As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=100)> _
        Public Url As Byte()
    End Structure '  167  ::  1:Dev has DNS;
    <StructLayout(LayoutKind.Sequential, Size:=28, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_UDP_SEARCH_CARD_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=10)> _
        Public CardName As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public IpAddr As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public IpMask As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public GwAddr As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=6)> _
        Public MacAddr As Byte()
    End Structure '28
    <StructLayout(LayoutKind.Sequential, Size:=102, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_UDP_SEARCH_TWO_CARD_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=10)> _
        Public DevType As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public DevSerialNum As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public ServAddr As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public Port As Byte()
        Public NetMode As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public Version As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public Reserved As Byte()
        Public CardNumber As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public CardInfo As CCHEX_UDP_SEARCH_CARD_STRU()
    End Structure '46+28*2 = 102    ::   2:Dev has two NetCard
    <StructLayout(LayoutKind.Sequential, Size:=180, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_UDP_SEARCH_STRU_EXT_INF

        '0:CCHEX_UDP_SEARCH_STRU;1:CCHEX_UDP_SEARCH_WITH_DNS_STRU;2:CCHEX_UDP_SEARCH_TWO_CARD_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=167)> _
        Public Data As Byte()
        Public Padding As Byte
        Public Result As Integer '0: ok -1: fail
        Public MachineId As UInteger
        Public DevHardwareType As Integer    '0:Dev without DNS;1:Dev has DNS;2:Dev has two NetCard
    End Structure '8+167+1
    <StructLayout(LayoutKind.Sequential, Size:=9004, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_UDP_SEARCH_ALL_STRU_EXT_INF
        Public DevNum As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=50)> _
        Public dev_net_info As CCHEX_UDP_SEARCH_STRU_EXT_INF()
    End Structure '4+DevNum*sizeof(CCHEX_UDP_SEARCH_STRU_EXT_INF)

    'UDP  set dev config
    <StructLayout(LayoutKind.Sequential, Size:=168, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_UDP_SET_DEV_CONFIG_STRU_EXT_INF
        Public DevNetInfo As CCHEX_DEV_NET_INFO_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public Padding As Byte()
        Public NewMachineId As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public Reserved As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=12)> _
        Public DevUserName As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=12)> _
        Public DevPassWord As Byte()
        Public DevHardwareType As Integer '0:Dev without DNS;1:Dev has DNS;
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public Dns As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=100)> _
        Public Url As Byte()
    End Structure '

    '3.0
    <StructLayout(LayoutKind.Sequential, Size:=168, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_SET_USER_ATTENDANCE_STATUS_STRU
        Public fp_len As UInteger                        'data_info :: ANSI VERSION   fp_len = 80  UNICODE VERSION   fp_len = 160
        Public atten_status_number As Byte          'Param:8 grounp
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=160)> _
        Public data_info As Byte()               'Param:8 grounp  ANSI VERSION: unsigned char [8][10]  UNICODE VERSION: unsigned char[8][20] 
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public padding As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=7, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure GET_DST_PARAM_TIME
        Public month As Byte
        Public day As Byte
        Public week_num As Byte       '周次定义如下：0x01-0x04：前1-4周 0x81-0x82：后1-2周
        Public flag_week As Byte      '星期标志flag_week 0-6:(用二进制0000000分别表示：六五四三二一日)
        Public hour As Byte
        Public minute As Byte
        Public sec As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=16, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_SET_DST_PARAM_STRU
        Public enabled As Byte          '0-不启用      1-启用；
        Public date_week_type As Byte   '1-日期格式	2-星期格式；
        Public start_time As GET_DST_PARAM_TIME
        Public special_time As GET_DST_PARAM_TIME
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=24, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_DST_PARAM_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public param As CCHEX_SET_DST_PARAM_STRU
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=320, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_SET_DEV_EXT_INFO_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=50)> _
        Public manufacturer_name As Byte()         '厂商名称
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=100)> _
        Public manufacturer_addr As Byte()        '厂商地址
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=15)> _
        Public duty_paragraph As Byte()            '税号
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=155)> _
        Public reserved As Byte()                 '预留
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=328, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_DEV_EXT_INFO_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public param As CCHEX_SET_DEV_EXT_INFO_STRU
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=15, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_SET_BASIC_CFG_INFO3_STRU
        Public wiegand_type As Byte         '韦根读取方式
        Public online_mode As Byte
        Public collect_level As Byte
        Public pwd_status As Byte           '连接密码状态  =0 网络连接时不需要验证通讯密码 =1网络连接时需要先发送0x04命令 验证通讯密码
        Public sensor_status As Byte           '=0 不主动汇报门磁状态  =1主动汇报门磁状态（设备主动发送0x2F命令的应答包)
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public reserved As Byte()
        Public independent_time As Byte
        Public m5_t5_status As Byte         '= 0	禁用 = 1	启用，本机状态为出=2	启用，本机状态为入 =4	禁用，本机状态为出 =5	禁用，本机状态为入
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=24, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_BASIC_CFG_INFO3_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public param As CCHEX_SET_BASIC_CFG_INFO3_STRU
        Public padding As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=24, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_CONNECTION_AUTHENTICATION_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=12)> _
        Public username As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=12)> _
        Public password As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=12, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_RECORD_NUMBER_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public record_num As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=13, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_GET_RECORD_INFO_BY_TIME
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public start_date As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public end_date As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=36, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_GET_RECORD_INFO_BY_TIME_VER_4_NEWID
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)> _
        Public EmployeeId As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public start_date As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public end_date As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=24 + 8, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_EMPLOYEE_RECORD_BY_TIME_STRU
        Public MachineId As UInteger
        Public Result As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public [date] As Byte()
        Public back_id As Byte                      '备份号
        Public record_type As Byte                  '记录类型
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public work_type As Byte() '工种        (ONLY use 3bytes )
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public padding As Byte()
        Public CurIdx As Integer
        Public TotalCnt As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=47 + 8, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_EMPLOYEE_RECORD_BY_TIME_STRU_VER_4_NEWID
        Public MachineId As UInteger
        Public Result As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)> _
        Public EmployeeId As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public [date] As Byte()
        Public back_id As Byte                      '备份号
        Public record_type As Byte                  '记录类型
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public work_type As Byte() '工种        (ONLY use 3bytes )
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public padding As Byte()
        Public CurIdx As Integer
        Public TotalCnt As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=96, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_SET_BASIC_CFG_INFO5_STRU
        Public fail_alarm_time As Byte
        Public tamper_alarm As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=94)> _
        Public reserved As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=104, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_GET_BASIC_CFG_INFO5_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public param As CCHEX_SET_BASIC_CFG_INFO5_STRU
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=12, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_GET_CARD_ID_STRUF
        Public MachineId As UInteger
        Public Result As Integer
        Public card_id As UInteger
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=96, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_SET_DEV_CURRENT_STATUS_STRU
        Public alarm_stop As Byte
        Public door_status As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=94)> _
        Public reserved As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=112, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_GET_URL_STRU
        Public MachineId As UInteger
        Public Result As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public Dns As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=100)> _
        Public Url As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=32, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_SET_STATUS_SWITCH_STRU
        Public group_id As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=7)> _
        Public day_week As CCHEX_GET_PERIOD_TIME_ONE_STRU_EXT_INF()
        Public status_id As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public padding As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=40, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_STATUS_SWITCH_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public group_id As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=7)> _
        Public day_week As CCHEX_GET_PERIOD_TIME_ONE_STRU_EXT_INF()
        Public status_id As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public padding As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=4, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_ONE_TIMER_STATUS
        Public StartHour As Byte
        Public StartMin As Byte
        Public EndHour As Byte
        Public EndMin As Byte
        Public status_id As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=44, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_SET_STATUS_SWITCH_STRU_EXT
        Public flag_week As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public one_time As CCHEX_ONE_TIMER_STATUS()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public padding As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=52, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_STATUS_SWITCH_STRU_EXT
        Public MachineId As UInteger
        Public Result As Integer
        Public flag_week As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public one_time As CCHEX_ONE_TIMER_STATUS()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public padding As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=52, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_UPDATEFILE_STATUS
        Public MachineId As UInteger
        Public Result As Integer
        Public verify_status As Integer
        Public verify_ret As Integer
    End Structure


    'add 3.0
    <StructLayout(LayoutKind.Sequential, Size:=52, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_SPECIAL_STATUS_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public status As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=7)> _
        Public reserved As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=12, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_MACHINE_ID_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public cur_machineid As UInteger
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=16, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_SET_MACHINE_ID_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public cur_machineid As UInteger
        Public old_machineid As UInteger
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=20, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_MACHINE_TYPE_STRU
        Public MachineId As UInteger
        Public Result As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public DevType As Byte()
        Public DevTypeFlag As Integer
    End Structure


    <StructLayout(LayoutKind.Sequential, Size:=10, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_MANAGE_LOG_RECORD
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public start_date As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public end_date As Byte()
        Public CmdType As Byte
        Public AutoFlag As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=40, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_MANAGE_LOG_RECORD
        Public MachineId As UInteger
        Public CmdType As UInteger
        Public Result As Integer
        Public IsAuto As UInteger
        Public TotalNum As UInteger
        Public CurNum As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public [Date] As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public LogType As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public LogLen As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public padding As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=40, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_EMPLOYEE_INFO_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()      ' 
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public Password As Byte()                      ' 
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public Card As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=20)> _
        Public EmployeeName As Byte()
        Public CardType As Byte
        Public GroupId As Byte
        Public AttendanceMode As Byte
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public FpStatus As Byte()
        Public PwdH8bit As Byte
        Public HolidayGroup As Byte
        Public Special As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=52, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_DOWNLOAD_EMPLOYEE_INFO_STRU
        Public MachineId As UInteger
        Public CurIdx As Integer
        Public TotalCnt As Integer
        Public employee As SAC_EMPLOYEE_INFO_STRU
    End Structure



    <StructLayout(LayoutKind.Sequential, Size:=101, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_GROUP_INFO_STRU
        Public GroupId As Byte      ' 
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=100)> _
        Public GroupName As Byte()                      ' 
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=113, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_RET_DOWNLOAD_GROUP_INFO_STRU
        Public MachineId As UInteger
        Public CurIdx As Integer
        Public TotalCnt As Integer
        Public GroupData As SAC_GROUP_INFO_STRU
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=113, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_RET_UPLOAD_GROUP_INFO_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public GroupFlag As UInteger
    End Structure


    <StructLayout(LayoutKind.Sequential, Size:=6, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_EMPLOYEE_WITH_GROUP_INFO_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()                      ' 
        Public GroupId As Byte      ' 
    End Structure


    <StructLayout(LayoutKind.Sequential, Size:=113, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_RET_DOWNLOAD_EMPLOYEE_WITH_GROUP_INFO_STRU
        Public MachineId As UInteger
        Public CurIdx As Integer
        Public TotalCnt As Integer
        Public Data As SAC_EMPLOYEE_WITH_GROUP_INFO_STRU
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=113, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_UPLOAD_EMPLOYEE_WITH_GROUP_INFO_STRU
        Public MachineId As UInteger
        Public Result As Integer '0 ok, -1 err
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=145, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_SET_DOOR_INFO_STRU
        Public DoorId As Byte      ' 
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=100)> _
        Public DoorName As Byte()                      ' 
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=40)> _
        Public DevName As Byte()
        Public Anti_subType As Byte      ' 
        Public InterlockFlag As Byte      ' 
        Public InterlockDoorId As Byte      ' 
        Public DoorStatus As Byte      ' 
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=145 + 8, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_RET_GET_DOOR_INFO_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public Data As SAC_SET_DOOR_INFO_STRU
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=12, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_RET_SET_DOOR_INFO_STRU
        Public MachineId As UInteger
        Public Result As Integer '0 ok, -1 err
        Public DoorId As UInteger
    End Structure


    <StructLayout(LayoutKind.Sequential, Size:=3, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_DOOR_WITH_DOORGROUP_INFO_STRU
        Public CombinationId As Byte      ' 
        Public DoorId As Byte      ' 
        Public DoorGroupId As Byte      ' 
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=3 + 12, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_RET_DOWNLOAD_DOOR_WITH_DOORGROUP_STRU
        Public MachineId As UInteger
        Public CurIdx As Integer
        Public TotalCnt As Integer
        Public Data As SAC_DOOR_WITH_DOORGROUP_INFO_STRU
    End Structure


    <StructLayout(LayoutKind.Sequential, Size:=40, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_UPLOAD_TIME_FRAME_INFO_STRU
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=10)> _
        Public [date] As CCHEX_GET_PERIOD_TIME_ONE_STRU_EXT_INF()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=40 + 12, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_RET_DOWNLOAD_TIME_FRAME_INFO_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public TimeFrameNum As UInteger
        Public Data As SAC_UPLOAD_TIME_FRAME_INFO_STRU
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=12, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_RET_UPLOAD_TIME_FRAME_INFO_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public TimeFrameNum As UInteger
    End Structure


    <StructLayout(LayoutKind.Sequential, Size:=3, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_TimeFrame_WITH_TimeGROUP_INFO_STRU
        Public CombinationId As Byte      ' 
        Public TimeFrameId As Byte      ' 
        Public TimeFrameGroupId As Byte      ' 
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=3 + 12, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_RET_DOWNLOAD_TimeFrame_WITH_TimeGROUP_STRU
        Public MachineId As UInteger
        Public CurIdx As Integer
        Public TotalCnt As Integer
        Public Data As SAC_TimeFrame_WITH_TimeGROUP_INFO_STRU
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=12, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_RET_UPLOAD_TimeFrame_WITH_TimeGROUP_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public GroupFlag As UInteger
    End Structure


    <StructLayout(LayoutKind.Sequential, Size:=104, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_ACCESS_CONTROL_GROUP_INFO_STRU
        Public SAC_GroupId As Byte      ' 
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=100)> _
        Public GroupName As Byte()
        Public EmployeeGroupId As Byte      ' 
        Public DoorGroupId As Byte      ' 
        Public TimeGroupId As Byte      ' 
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=104 + 12, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SAC_RET_DOWNLOAD_CONTROL_GROUP_INFO_STRU
        Public MachineId As UInteger
        Public CurIdx As Integer
        Public TotalCnt As Integer
        Public Data As SAC_ACCESS_CONTROL_GROUP_INFO_STRU
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=12, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_PICTURE_NUMBER_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public PictureTotal As UInteger
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=25, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_PICTURE_HEAD_INFO_STRU
        Public MachineId As UInteger
        Public Result As Integer
        Public CurIdx As Integer
        Public TotalCnt As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public DateTime As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=48, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_PICTURE_HEAD_INFO_STRU_VER_4_NEWID
        Public MachineId As UInteger
        Public Result As Integer
        Public CurIdx As Integer
        Public TotalCnt As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)> _
        Public EmployeeId As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public DateTime As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=9, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_PICTURE_BY_EID_AND_TIME
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public DateTime As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=32, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_PICTURE_BY_EID_AND_TIME_VER_4_NEWID
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)> _
        Public EmployeeId As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public DateTime As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=24, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_PICTURE_BY_EID_AND_TIME
        Public MachineId As UInteger
        Public Result As Integer
        Public DataLen As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public DateTime As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public padding As Byte()
        '[MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 0)]
        'public byte[] PictureData;
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=44, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_GET_PICTURE_BY_EID_AND_TIME_VER_4_NEWID
        Public MachineId As UInteger
        Public Result As Integer
        Public DataLen As UInteger
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)> _
        Public EmployeeId As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public DateTime As Byte()
        '[MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 0)]
        'public byte[] PictureData;
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=17, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_DEL_PICTURE_BY_EID_AND_TIME
        Public MachineId As UInteger
        Public Result As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public EmployeeId As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public DateTime As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=40, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CCHEX_RET_DEL_PICTURE_BY_EID_AND_TIME_VER_4_NEWID
        Public MachineId As UInteger
        Public Result As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=28)> _
        Public EmployeeId As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public DateTime As Byte()
    End Structure



    'function
    <DllImport("tc-b_new_sdk.dll")> _
    Public Shared Function CChex_Version() As UInteger
    End Function

    <DllImport("tc-b_new_sdk.dll")> _
    Public Shared Sub CChex_Init()
    End Sub



    <DllImport("tc-b_new_sdk.dll")> _
    Public Shared Function CChex_Start() As IntPtr
    End Function


    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_Start_With_Param(ByVal IsCloseServer As UInteger, ByVal ServerPort As UInteger) As IntPtr
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_Set_Service_Port(ByVal Port As UShort) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll")> _
    Public Shared Function CChex_Set_Service_Disenable() As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_Get_Service_Port(ByVal CchexHandle As IntPtr) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Sub CChex_Stop(ByVal CchexHandle As IntPtr)
    End Sub


    '网络
    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetNetConfig(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetNetConfig(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Config As CCHEX_NETCFG_INFO_STRU) As Integer
    End Function 'CCHEX_NETCFG_INFO_STRU *

    '消息
    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_MsgGetByIdx(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal Idx As Byte) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_MsgDelByIdx(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal Idx As Byte) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_MsgAddNew(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal Data As Byte(), ByVal Len As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_MsgGetAllHead(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function


    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_Update(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer(), ByVal Type As Integer(), ByVal Buff As IntPtr, ByVal Len As Integer) As Integer
    End Function


    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_RebootDevice(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function


    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetTime(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal Year As Integer, ByVal Month As Integer, ByVal Day As Integer, ByVal Hour As Integer, ByVal Min As Integer, ByVal Sec As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
   Public Shared Function CChex_GetSNConfig(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function


    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_UploadFile(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal FileType As Byte, ByVal FileName As Byte(), ByVal Buff As Byte(), ByVal Len As Integer) As Integer
    End Function


    ' download all record
    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_DownloadAllRecords(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function


    ' download new record
    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_DownloadAllNewRecords(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function


    ' basic config info
    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetBasicConfigInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetBasicConfigInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Config As CCHEX_SET_BASIC_CFG_INFO_STRU) As Integer
    End Function


    ' list, modify and delete person 
    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_ListPersonInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_ModifyPersonInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef personlist As CCHEX_RET_PERSON_INFO_STRU, ByVal person_num As Byte) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_ModifyPersonW2Info(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef personlist As CCHEX_EMPLOYEE_INFO_STRU_EXT_INF_FOR_W2, ByVal person_num As Byte) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_ModifyPersonInfo_VER_4_NEWID(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef personlist As CCHEX_RET_DLEMPLOYEE_INFO_STRU_EXT_INF_FOR_VER_4, ByVal person_num As Byte) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_DeletePersonInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Config As CCHEX_DEL_PERSON_INFO_STRU) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_DeletePersonInfo_VER_4_NEWID(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Config As CCHEX_DEL_EMPLOYEE_INFO_STRU_EXT_INF_ID_VER_4_NEWID) As Integer
    End Function


    '获取单个员工资料
    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetOnePersonInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Config As CCHEX_GET_ONE_EMPLOYEE_INFO_STRU) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetOnePersonInfo_VER_4_NEWID(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Config As CCHEX_GET_ONE_EMPLOYEE_INFO_STRU_ID_VER_4_NEWID) As Integer
    End Function


    ' get, put fp raw data
    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_DownloadFingerPrint(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal EmployeeId As Byte(), ByVal FingerIdx As Byte) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_UploadFingerPrint(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal EmployeeId As Byte(), ByVal FingerIdx As Byte, ByVal FingerData As Byte(), ByVal DataLen As Integer) As Integer
    End Function

    '
    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_DownloadFingerPrint_VER_4_NEWID(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal EmployeeId As Byte(), ByVal FingerIdx As Byte) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_UploadFingerPrint_VER_4_NEWID(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal EmployeeId As Byte(), ByVal FingerIdx As Byte, ByVal FingerData As Byte(), ByVal DataLen As Integer) As Integer
    End Function


    ' delete record or new flag
    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_DeleteRecordInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Config As CCHEX_DEL_RECORD_INFO_STRU) As Integer
    End Function



    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetTime(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_InitUserArea(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_InitSystem(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetBasicConfigInfo2(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetBasicConfigInfo2(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef config As CCHEX_GET_BASIC_CFG_INFO2_STRU_EXT_INF) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetPeriodTime(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal SerialNumbe As Byte) As Integer
    End Function '(SerialNumbe==(1--32))
    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetPeriodTime(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef config As CCHEX_SET_PERIOD_TIME_STRU_EXT_INF) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetTeamInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal TeamNumbe As Byte) As Integer
    End Function '(TeamNumbe==(0--16))
    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetTeamInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef config As CCHEX_SET_TEAM_INFO_STRU_EXT_INF) As Integer
    End Function '(TeamNumbe==(2--16))
    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CCHex_AddFingerprintOnline(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_ADD_FINGERPRINT_ONLINE_STRU_EXT_INF) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CCHex_AddFingerprintOnline_VER_4_NEWID(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_ADD_FINGERPRINT_ONLINE_STRU_EXT_INF_ID_VER_4_NEWID) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CCHex_ForcedUnlock(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal CCHEX_FORCED_UNLOCK_STRU_EXT_INF As IntPtr) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CCHex_Udp_Search_Dev(ByVal CchexHandle As IntPtr) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CCHex_Udp_Set_Dev_Config(ByVal CchexHandle As IntPtr, ByRef config As CCHEX_UDP_SET_DEV_CONFIG_STRU_EXT_INF) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CCHex_ClientConnect(ByVal CchexHandle As IntPtr, ByVal Ip As Byte(), ByVal Port As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CCHex_ClientDisconnect(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Sub CChex_SetSdkConfig(ByVal CchexHandle As IntPtr, ByVal SetAutoDownload As Integer, ByVal SetRecordflag As Integer, ByVal SetLogFile As Integer)
    End Sub

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetInfomationCode(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetInfomationCode(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal Data As Byte(), ByVal DataLen As UInteger) As Integer
    End Function 'ANSI:10     UNICODE 20
    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetBellInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetBellInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal BellTimeNum As Byte, ByVal Hour As Byte, ByVal Min As Byte, ByVal FlagWeek As Byte) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetUserAttendanceStatusInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetUserAttendanceStatusInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_SET_USER_ATTENDANCE_STATUS_STRU) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_ClearAdministratFlag(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetSpecialStatus(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetAdminCardnumberPassword(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetAdminCardnumberPassword(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal Data As Byte(), ByVal DataLen As UInteger) As Integer
    End Function 'Data[13] DataLen == 13
    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetDSTParam(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetDSTParam(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_SET_DST_PARAM_STRU) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetDevExtInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetDevExtInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_SET_DEV_EXT_INFO_STRU) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetBasicConfigInfo3(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetBasicConfigInfo3(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_SET_BASIC_CFG_INFO3_STRU) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_ConnectionAuthentication(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_CONNECTION_AUTHENTICATION_STRU) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetRecordNumByEmployeeIdAndTime(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_GET_RECORD_INFO_BY_TIME) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_DownloadRecordByEmployeeIdAndTime(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_GET_RECORD_INFO_BY_TIME) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetRecordNumByEmployeeIdAndTime_VER_4_NEWID(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_GET_RECORD_INFO_BY_TIME_VER_4_NEWID) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_DownloadRecordByEmployeeIdAndTime_VER_4_NEWID(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_GET_RECORD_INFO_BY_TIME_VER_4_NEWID) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetRecordInfoStatus(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function


    ' Bolid   BasiConfigInfo5
    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetBasicConfigInfo5(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetBasicConfigInfo5(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_SET_BASIC_CFG_INFO5_STRU) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetCardNo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetDevCurrentStatus(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_SET_DEV_CURRENT_STATUS_STRU) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetServiceURL(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetServiceURL(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal UrlBuff As Byte(), ByVal Bufflen As UInteger) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetStatusSwitch(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal GroupId As Byte) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetStatusSwitch(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_SET_STATUS_SWITCH_STRU) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetStatusSwitch_EXT(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal FlagWeek As Byte) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetStatusSwitch_EXT(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_SET_STATUS_SWITCH_STRU_EXT) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_UpdateDevStatus(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    ' [DllImport("tc-b_new_sdk.dll", CallingConvention = CallingConvention.Cdecl)]
    ' public static extern int CChex_UploadFile(IntPtr CchexHandle, int DevIdx,byte FileType,byte[] FileName,byte[] Buff, int Len);

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetMachineId(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SetMachineId(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal MachineId As UInteger) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_Find_DevIdx_By_MachineId(ByVal CchexHandle As IntPtr, ByVal MachineId As UInteger) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
Public Shared Function CChex_Set_Connect_Authentication(ByVal CchexHandle As IntPtr, ByVal CCHEX_CONNECTION_AUTHENTICATION_STRU As IntPtr) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_UploadRecord(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef record As CCHEX_UPLOAD_RECORD_INFO_STRU) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_UploadRecord_VER_4_NEWID(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef record As CCHEX_UPLOAD_RECORD_INFO_STRU_VER_4_NEWID) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_ManageLogRecord(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_MANAGE_LOG_RECORD) As Integer
    End Function




    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetPictureNumber(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function


    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetPictureAllHeadInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function


    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetPictureByEmployeeIdAndTime(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_PICTURE_BY_EID_AND_TIME) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_GetPictureByEmployeeIdAndTime_VER_4_NEWID(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_PICTURE_BY_EID_AND_TIME_VER_4_NEWID) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_DelPictureByEmployeeIdAndTime(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_PICTURE_BY_EID_AND_TIME) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_DelPictureByEmployeeIdAndTime_VER_4_NEWID(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As CCHEX_PICTURE_BY_EID_AND_TIME_VER_4_NEWID) As Integer
    End Function


    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_DownloadAllEmployeeInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_UploadEmployeeInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef EmployeeList As SAC_EMPLOYEE_INFO_STRU, ByVal EmployeeNum As UInteger) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_DownloadAllGroupInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_UploadGroupInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef GroupList As SAC_GROUP_INFO_STRU, ByVal GroupNum As UInteger) As Integer
    End Function


    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_DownloadAllEmployeeWithGroupInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_UploadEmployeeWithGroupInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef DataList As SAC_EMPLOYEE_WITH_GROUP_INFO_STRU, ByVal DataNum As UInteger) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_GetDoorInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal DoorIdx As Byte) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_SetDoorInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As SAC_SET_DOOR_INFO_STRU) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_DownloadAllDoorGroupInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_UploadDoorGroupInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef DataList As SAC_GROUP_INFO_STRU, ByVal DataNum As UInteger) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_DownloadAllDoorWithDoorGroupInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_UploadDoorWithDoorGroupInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef DataList As SAC_DOOR_WITH_DOORGROUP_INFO_STRU, ByVal DataNum As UInteger) As Integer
    End Function


    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_DownloadTimeFrameInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByVal TimeFrameNum As Byte) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_UploadTimeFrameInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef Param As SAC_UPLOAD_TIME_FRAME_INFO_STRU, ByVal TimeFrameNum As Byte) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_DownloadAllTimeGroupInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_UploadTimeGroupInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef DataList As SAC_GROUP_INFO_STRU, ByVal DataNum As UInteger) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_DownloadAllTimeFrameWithTimeGroupInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_UploadTimeFrameWithTimeGroupInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef DataList As SAC_TimeFrame_WITH_TimeGROUP_INFO_STRU, ByVal DataNum As UInteger) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_DownloadAccessControlGroupInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer) As Integer
    End Function

    <DllImport("tc-b_new_sdk.dll", CallingConvention:=CallingConvention.Cdecl)> _
    Public Shared Function CChex_SAC_UploadAccessControlGroupInfo(ByVal CchexHandle As IntPtr, ByVal DevIdx As Integer, ByRef DataList As SAC_ACCESS_CONTROL_GROUP_INFO_STRU, ByVal DataNum As UInteger) As Integer
    End Function
End Class
