﻿Imports System.Collections
Imports System.Data
Imports System.Diagnostics
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Runtime.InteropServices
Imports Microsoft.VisualBasic

Public Class clsAnviz

    Public Const CKT_ERROR_INVPARAM As Long = -1
    Public Const CKT_ERROR_NETDAEMONREADY As Long = -1
    Public Const CKT_ERROR_CHECKSUMERR As Long = -2
    Public Const CKT_ERROR_MEMORYFULL As Long = -1
    Public Const CKT_ERROR_INVFILENAME As Long = -3
    Public Const CKT_ERROR_FILECANNOTOPEN As Long = -4
    Public Const CKT_ERROR_FILECONTENTBAD As Long = -5
    Public Const CKT_ERROR_FILECANNOTCREATED As Long = -2
    Public Const CKT_ERROR_NOTHISPERSON As Long = -1

    Public Const CKT_RESULT_OK As Long = 1
    Public Const CKT_RESULT_ADDOK As Long = 1
    Public Const CKT_RESULT_HASMORECONTENT As Long = 2

    ' Types
    <StructLayout(LayoutKind.Sequential, Size:=26, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure NETINFO
        Public ID As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public IP As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public Mask As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public Gateway As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public ServerIP As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=6)> _
        Public MAC As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=164, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CKT_KQState
        Public num As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=160)> _
        Public kqmsg As Byte()
        '0 To 9, 0 To 15
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=16, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure SYSTEMTIME
        Public wYear As Short
        Public wMonth As Short
        Public wDayOfWeek As Short
        Public wDay As Short
        Public wHour As Short
        Public wMinute As Short
        Public wSecond As Short
        Public wMilliseconds As Short
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=18, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure DATETIMEINFO
        Public ID As Integer
        Public Year As UShort
        Public Month As Byte
        Public Day As Byte
        Public Hour As Byte
        Public Minute As Byte
        Public Second As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=48, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure PERSONINFO
        Public PersonID As Integer
        '人员编号
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public Password As Byte()
        '密码
        Public CardNo As Integer
        '卡号
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=12)> _
        Public Name As Byte()
        '姓名
        Public Dept As Integer
        '部门
        Public Group As Integer
        '组号
        Public KQOption As Integer
        '考勤比对标志
        '0：独立（指纹，卡，密码都可以）
        '1：卡+指纹
        '2：密码+指纹
        '3：卡+密码
        '4：工号+指纹 
        Public FPMark As Integer
        '位0 = 1表示已登记指纹1，位1 = 1表示已登记指纹2
        '例如FPMark=3(00000011)表示已登记指纹1和2，
        'FPMark=１表示已登记指纹1，FPMark=2表示已登记指纹2
        Public Other As Integer
        '特殊信息 =0 普通人员, =1 管理员
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=100, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure PERSONINFOEX
        Public PersonID As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public Password As Byte()
        Public CardNo As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=64)> _
        Public Name As Byte()
        Public Dept As Integer
        Public Group As Integer
        Public KQOption As Integer
        Public FPMark As Integer
        Public Other As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=40, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CLOCKINGRECORD
        Public ID As Integer
        Public PersonID As Integer
        Public Stat As Integer
        Public BackupCode As Integer
        Public WorkType As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=20)> _
        Public Time As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=24, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CKT_PictureFileHead
        Public ID As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=20)> _
        Public STime As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=60, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure DEVICEINFO
        Public ID As Integer
        Public MajorVersion As Integer
        Public MinorVersion As Integer
        Public SpeakerVolume As Integer
        Public Parameter As Integer
        Public DefaultAuth As Integer
        Public FixWGHead As Integer
        Public WGOption As Integer
        Public AutoUpdateAllow As Integer
        Public KQRepeatTime As Integer
        Public RealTimeAllow As Integer
        Public RingAllow As Integer
        Public LockDelayTime As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public AdminPassword As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=12, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure RINGTIME
        Public hour As Integer
        Public minute As Integer
        Public week As Integer
    End Structure
    '
    '        [StructLayout(LayoutKind.Explicit, Pack = 1)]
    '        public struct TIMESECT
    '        {
    '            [FieldOffset(0)]
    '            public byte bHour;
    '            [FieldOffset(1)]
    '            public byte bMinute;
    '            [FieldOffset(2)]
    '            public byte eHour;
    '            [FieldOffset(3)]
    '            public byte eMinute;
    '        }
    '


    <StructLayout(LayoutKind.Sequential, Size:=4, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure TIMESECT
        Public bHour As Byte
        Public bMinute As Byte
        Public eHour As Byte
        Public eMinute As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=76, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CKT_MessageInfo
        Public PersonID As Integer
        Public sYear As Integer
        Public sMon As Integer
        Public sDay As Integer
        Public eYear As Integer
        Public eMon As Integer
        Public eDay As Integer
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=48)> _
        Public msg As Byte()
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=28, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure CKT_MessageHead
        Public PersonID As Integer
        Public sYear As Integer
        Public sMon As Integer
        Public sDay As Integer
        Public eYear As Integer
        Public eMon As Integer
        Public eDay As Integer
    End Structure

    <StructLayout(LayoutKind.Sequential, Size:=108, CharSet:=CharSet.Ansi), Serializable()> _
    Public Structure GPRSinfo
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=32)> _
        Public APN As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public ServerIP As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=2)> _
        Public Port As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public LocalIP As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=18)> _
        Public UserName As Byte()
        <MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst:=18)> _
        Public Password As Byte()
        Public Mode As Byte
        'public byte Reserved;
    End Structure

    ' Routines
    '[DllImport("tc400.dll", ExactSpelling = true)] 
    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetNetTimeouts(ByVal LTime As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetComTimeouts(ByVal LTime As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_FreeMemory(ByVal address As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_RegisterSno(ByVal Sno As Integer, ByVal ComPort As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_RegisterNet(ByVal Sno As Integer, ByVal Addr As [String]) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_RegisterUSB(ByVal Sno As Integer, ByVal Index As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_ModifyDeviceSno(ByVal Sno As Integer, ByVal num As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetSleepTime(ByVal Sno As Integer, ByVal min As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Sub CKT_UnregisterSnoNet(ByVal Sno As Integer)
    End Sub

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_NetDaemon() As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_ComDaemon() As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Sub CKT_Disconnect()
    End Sub

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_ReportConnections(ByRef ppSno As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetDeviceNetInfo(ByVal Sno As Integer, ByRef pNetInfo As NETINFO) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetDeviceIPAddr(ByVal Sno As Integer, ByVal IP As Byte()) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetDaylightSavingTime(ByVal Sno As Integer, ByVal dst As Byte()) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetDaylightSavingTime(ByVal Sno As Integer, ByVal dst As Byte()) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetDeviceMask(ByVal Sno As Integer, ByVal Mask As Byte()) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetDeviceGateway(ByVal Sno As Integer, ByVal Gate As Byte()) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetDeviceServerIPAddr(ByVal Sno As Integer, ByVal Svr As Byte()) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetDeviceMAC(ByVal Sno As Integer, ByVal MAC As Byte()) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetDevicePort(ByVal Sno As Integer, ByVal Pno As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetDeviceClock(ByVal Sno As Integer, ByRef pDateTimeInfo As DATETIMEINFO) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetDeviceClock(ByVal Sno As Integer, ByRef pDateTimeInfo As DATETIMEINFO) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetDeviceDate(ByVal Sno As Integer, ByVal Year_Renamed As Short, ByVal Month_Renamed As Short, ByVal Day_Renamed As Short) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetDeviceTime(ByVal Sno As Integer, ByVal Hour_Renamed As Short, ByVal Minute_Renamed As Short, ByVal Second_Renamed As Short) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetFPTemplate(ByVal Sno As Integer, ByVal PersonID As Integer, ByVal FPID As Integer, ByRef pFPData As Integer, ByRef FPDataLen As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_PutFPTemplate(ByVal Sno As Integer, ByVal PersonID As Integer, ByVal FPID As Integer, ByVal pFPData As Byte(), ByVal FPDataLen As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetFPTemplateSaveFile(ByVal Sno As Integer, ByVal PersonID As Integer, ByVal FPID As Integer, ByVal FPDataFilename As [String]) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_PutFPTemplateLoadFile(ByVal Sno As Integer, ByVal PersonID As Integer, ByVal FPID As Integer, ByVal FPDataFilename As [String]) As Integer
    End Function

    'public static extern int CKT_GetFPRawData(int Sno, int PersonID, int FPID, ref byte FPRawData);
    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetFPRawData(ByVal Sno As Integer, ByVal PersonID As Integer, ByVal FPID As Integer, ByVal FPRawData As Byte()) As Integer
    End Function


    <DllImport("tc400.dll")> _
    Public Shared Function CKT_PutFPRawData(ByVal Sno As Integer, ByVal PersonID As Integer, ByVal FPID As Integer, ByRef FPRawData As Byte, ByVal FPDataLen As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetFPRawDataSaveFile(ByVal Sno As Integer, ByVal PersonID As Integer, ByVal FPID As Integer, ByVal FPDataFilename As [String]) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_PutFPRawDataLoadFile(ByVal Sno As Integer, ByVal PersonID As Integer, ByVal FPID As Integer, ByVal FPDataFilename As [String]) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_ListPersonInfo(ByVal Sno As Integer, ByRef pRecordCount As Integer, ByRef ppPersons As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_ModifyPersonInfo(ByVal Sno As Integer, ByRef person As PERSONINFO) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_ModifyPersonInfoLongName(ByVal Sno As Integer, ByRef person As PERSONINFOEX) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_DeletePersonInfo(ByVal Sno As Integer, ByVal PersonID As Integer, ByVal backupID As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_EraseAllPerson(ByVal Sno As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_ListPersonInfoEx(ByVal Sno As Integer, ByRef ppLongRun As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_ListPersonProgress(ByVal pLongRun As Integer, ByRef pRecCount As Integer, ByRef pRetCount As Integer, ByRef ppPersons As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_ListPersonProgressEx(ByVal pLongRun As Integer, ByRef pRecCount As Integer, ByRef pRetCount As Integer, ByRef ppPersons As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_ListPersonProgressLongName(ByVal pLongRun As Integer, ByRef pRecCount As Integer, ByRef pRetCount As Integer, ByRef ppPersons As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetCounts(ByVal Sno As Integer, ByRef pPersonCount As Integer, ByRef pFPCount As Integer, ByRef pClockingsCount As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_ClearClockingRecord(ByVal Sno As Integer, ByVal type As Integer, ByVal count As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetClockingRecordEx(ByVal Sno As Integer, ByRef ppLongRun As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetClockingNewRecordEx(ByVal Sno As Integer, ByRef ppLongRun As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetClockingRecordProgress(ByVal pLongRun As Integer, ByRef pRecCount As Integer, ByRef pRetCount As Integer, ByRef ppPersons As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_ResetDevice(ByVal Sno As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetDeviceInfo(ByVal Sno As Integer, ByRef devinfo As DEVICEINFO) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetDeviceInfoEx(ByVal Sno As Integer, ByRef devinfo As DEVICEINFO) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetDefaultAuth(ByVal Sno As Integer, ByVal Auth As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetDoor(ByVal Sno As Integer, ByVal Second_Renamed As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetSpeakerVolume(ByVal Sno As Integer, ByVal Volume As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetDeviceAdminPassword(ByVal Sno As Integer, ByVal Password As [String]) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetRealtimeMode(ByVal Sno As Integer, ByVal RealMode As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetFixWGHead(ByVal Sno As Integer, ByVal WGHead As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetWG(ByVal Sno As Integer, ByVal WGMode As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetRingAllow(ByVal Sno As Integer, ByVal type As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetRepeatKQ(ByVal Sno As Integer, ByVal time As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetAutoUpdate(ByVal Sno As Integer, ByVal AutoUpdate As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_ForceOpenLock(ByVal Sno As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_ReadRealtimeClocking(ByRef ppClockings As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetTimeSection(ByVal Sno As Integer, ByVal ord As Integer, ByVal ts As IntPtr) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetTimeSection(ByVal Sno As Integer, ByVal ord As Integer, ByVal ts As TIMESECT()) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetPictureFileHead(ByVal Sno As Integer, ByVal param As Integer, ByVal reqcount As Integer, ByVal ts As IntPtr, ByRef RetCount As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetPictureFile(ByVal Sno As Integer, ByVal usrID As Integer, ByVal phead As [String], ByVal pfile As [String]) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_DelPictureFile(ByVal Sno As Integer, ByVal usrID As Integer, ByVal phead As [String], ByVal pfile As [String]) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetStateChangeInfo(ByVal Sno As Integer, ByVal ord As Integer, ByVal sc As [Byte]()) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetStateChangeInfo(ByVal Sno As Integer, ByVal ord As Integer, ByVal sc As [Byte]()) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetGroup(ByVal Sno As Integer, ByVal ord As Integer, ByVal grp As Integer()) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetGroup(ByVal Sno As Integer, ByVal ord As Integer, ByVal grp As Integer()) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetHitRingInfo(ByVal Sno As Integer, ByVal hri As IntPtr) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetHitRingInfo(ByVal Sno As Integer, ByVal ord As Integer, ByRef ring As RINGTIME) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetMessageByIndex(ByVal Sno As Integer, ByVal idx As Integer, ByRef msg As CKT_MessageInfo) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_AddMessage(ByVal Sno As Integer, ByRef msg As CKT_MessageInfo) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetAllMessageHead(ByVal Sno As Integer, ByVal mh As IntPtr) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_DelMessageByIndex(ByVal Sno As Integer, ByVal idx As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetDeviceMode(ByVal Sno As Integer, ByVal Mode As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_ChangeConnectionMode(ByVal Mode As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetWorkCode(ByVal Sno As Integer, ByVal Mode As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetMachineNumber(ByVal Sno As Integer, ByVal MMID As [Byte]()) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetKQState(ByVal Sno As Integer, ByRef kqs As CKT_KQState) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetKQState(ByVal Sno As Integer, ByRef kqs As CKT_KQState) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetDateTimeFormat(ByVal Sno As Integer, ByVal dateF As Integer, ByVal timeF As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_DeleteAllPersonInfo(ByVal Sno As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetGPRS(ByVal Sno As Integer, ByRef gpsi As GPRSinfo) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_GetGPRS(ByVal Sno As Integer, ByRef gpsi As GPRSinfo) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetComparePrecision(ByVal Sno As Integer, ByVal bdjd As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_SetComparePrecisionEx(ByVal Sno As Integer, ByVal bdjd11 As Integer, ByVal bdjd1n As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_DeleteAdmins(ByVal Sno As Integer) As Integer
    End Function

    <DllImport("tc400.dll")> _
    Public Shared Function CKT_RemoteSetDevice(ByVal Sno As Integer) As Integer
    End Function

End Class
