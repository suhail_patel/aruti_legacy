﻿
Imports eZeeCommonLib
Imports System

Public Class clsgrievanceapprover_master
    Private Shared ReadOnly mstrModuleName As String = "clsgrievanceapprover_master"
    Dim objDataOperation As clsDataOperation
    Dim objgrievanceapprover_Tran As New clsgrievanceapprover_Tran

    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintapprovermasterunkid As Integer = 0
    Private mintapprlevelunkid As Integer = 0
    Private mintapproverempid As Integer = 0
    Private mintmapuserunkid As Integer = 0
    Private mintapprovalsettingid As Integer = 0
    Private mintuserunkid As Integer = 0
    Private mblnisactive As Boolean = False
    Private mblnisexternal As Boolean = False
    Private mblnisvoid As Boolean = False
    Private mintvoiduserunkid As Integer = 0
    Private mdtvoiddatetime As DateTime = Nothing
    Private mstrvoidreason As String = String.Empty

    Private minAuditUserid As Integer = 0
    Private minAuditDate As DateTime = Nothing
    Private mstrClientIp As String = String.Empty
    Private mintloginemployeeunkid As Integer = 0
    Private mstrHostName As String = String.Empty
    Private mstrFormName As String = String.Empty
    Private blnIsFromWeb As Boolean = False

    Dim objDoOperation As clsDataOperation
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Approvermasterunkid
    ''' Modify By: Gajanan
    ''' </summary>
    ''' 
    Public Property _Approvermasterunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintapprovermasterunkid
        End Get
        Set(ByVal value As Integer)
            mintapprovermasterunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Apprlevelunkid() As Integer
        Get
            Return mintapprlevelunkid
        End Get
        Set(ByVal value As Integer)
            mintapprlevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Approverempid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Approverempid() As Integer
        Get
            Return mintapproverempid
        End Get
        Set(ByVal value As Integer)
            mintapproverempid = value
        End Set
    End Property

    Public Property _Mapuserunkid() As Integer
        Get
            Return mintmapuserunkid
        End Get
        Set(ByVal value As Integer)
            mintmapuserunkid = value
        End Set
    End Property

    Public Property _Approvalsettingid() As Integer
        Get
            Return mintapprovalsettingid
        End Get
        Set(ByVal value As Integer)
            mintapprovalsettingid = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintuserunkid
        End Get
        Set(ByVal value As Integer)
            mintuserunkid = value
        End Set
    End Property

    Public Property _Isactive() As Boolean
        Get
            Return mblnisactive
        End Get
        Set(ByVal value As Boolean)
            mblnisactive = value
        End Set
    End Property

    Public Property _Isexternal() As Boolean
        Get
            Return mblnisexternal
        End Get
        Set(ByVal value As Boolean)
            mblnisexternal = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnisvoid
        End Get
        Set(ByVal value As Boolean)
            mblnisvoid = value
        End Set
    End Property

    Public Property _Voiduserunkid() As Integer
        Get
            Return mintvoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintvoiduserunkid = value
        End Set
    End Property

    Public Property _Voiddatetime() As DateTime
        Get
            Return mdtvoiddatetime
        End Get
        Set(ByVal value As DateTime)
            mdtvoiddatetime = value
        End Set
    End Property

    Public Property _Voidreason() As String
        Get
            Return mstrvoidreason
        End Get
        Set(ByVal value As String)
            mstrvoidreason = value
        End Set
    End Property

    Public Property _AuditUserid() As Integer
        Get
            Return minAuditUserid
        End Get
        Set(ByVal value As Integer)
            minAuditUserid = value
        End Set
    End Property

    Public Property _AuditDate() As DateTime
        Get
            Return minAuditDate
        End Get
        Set(ByVal value As DateTime)
            minAuditDate = value
        End Set
    End Property

    Public Property _ClientIp() As String
        Get
            Return mstrClientIp
        End Get
        Set(ByVal value As String)
            mstrClientIp = value
        End Set
    End Property

    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public Property _IsFromWeb() As Boolean
        Get
            Return blnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            blnIsFromWeb = value
        End Set
    End Property

    Public Property _loginemployeeunkid() As Integer
        Get
            Return mintloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintloginemployeeunkid = value
        End Set
    End Property

#End Region

    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT " & _
              "  approvermasterunkid " & _
              ", apprlevelunkid " & _
              ", approverempid " & _
              ", mapuserunkid " & _
              ", approvalsettingid " & _
              ", userunkid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", isactive " & _
              ", isexternal " & _
              ", isvoid " & _
             "FROM greapprover_master " & _
             "WHERE approvermasterunkid = @approvermasterunkid and isvoid = 0 "

            objDataOperation.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprovermasterunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintapprovermasterunkid = CInt(dtRow.Item("approvermasterunkid"))
                mintapprlevelunkid = CInt(dtRow.Item("apprlevelunkid"))

                mintapprovalsettingid = CInt(dtRow.Item("approvalsettingid"))
                mintapproverempid = CInt(dtRow.Item("approverempid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtvoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If dtRow.Item("voidreason") Is Nothing Then
                    mstrvoidreason = dtRow.Item("voidreason").ToString
                End If
                mintmapuserunkid = CInt(dtRow.Item("mapuserunkid"))
                mblnisactive = CBool(dtRow.Item("isactive"))
                mblnisexternal = CBool(dtRow.Item("isexternal"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    Public Function GetList(ByVal strTableName As String, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                          , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                          , ByVal strEmployeeAsOnDate As String _
                          , ByVal xUserModeSetting As String _
                          , ByVal xOnlyApproved As Boolean _
                          , ByVal xIncludeIn_ActiveEmployee As Boolean _
                          , Optional ByVal blnOnlyActive As Boolean = True _
                          , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                          , Optional ByVal strEmpIds As String = "", Optional ByVal objDOperation As clsDataOperation = Nothing _
                          , Optional ByVal mstrFilter As String = "" _
                          , Optional ByVal strUserAccessLevelFilterString As String = "" _
                          , Optional ByVal intApproverTranUnkid As Integer = 0 _
                          , Optional ByVal blnForApproval As Boolean = False _
                          , Optional ByVal blnAddSelect As Boolean = False _
                          , Optional ByVal mintApprovalSetting As Integer = -1 _
                          , Optional ByVal xmintMapUserid As Integer = -1 _
                          ) As DataSet 'Gajanan [4-July-2019] -- ADD xmintMapUserid 








        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If
        objDataOperation.ClearParameters()

        Try
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQDtFilters As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            'S.SANDEEP |30-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Grievance UAT]
            'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , , xDatabaseName)
            'Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)

            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , , xDatabaseName, "#empappr#")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName, "#empappr#")
            'S.SANDEEP |30-MAY-2019| -- END
            

            Dim strSelect As String = ""
            If blnAddSelect Then
                strSelect = "SELECT " & _
                            "  0 AS approvermasterunkid " & _
                            ", 0 AS apprlevelunkid " & _
                            ", '' AS levelname " & _
                            ", 0 AS priority " & _
                            ", 0 AS approverempid " & _
                            ", @Select as name" & _
                            ", '' as email " & _
                            ", 0 AS departmentunkid " & _
                            ", '' AS departmentname " & _
                            ", 0 AS jobunkid" & _
                            ", '' As jobname " & _
                            ", CAST(0 AS BIT) AS isvoid " & _
                            ", 0 AS mapuserunkid " & _
                            ", NULL AS voiddatetime " & _
                            ", 0 AS userunkid " & _
                            ", 0 AS voiduserunkid " & _
                            ", '' AS voidreason " & _
                            ", CAST(0 AS BIT) AS isexternal  " & _
                            ", CAST(0 AS BIT) AS isactive  " & _
                            ", '' ExAppr " & _
                            ", CAST(0 AS BIT) AS activestatus " & _
                            ", @Select as ApproverNameWithLevel " & _
                            " UNION ALL "
            End If
            strQ &= "SELECT " & _
                    "  greapprover_master.approvermasterunkid " & _
                    ", greapprover_master.apprlevelunkid " & _
                    ", greapproverlevel_master.levelname " & _
                    ", greapproverlevel_master.priority " & _
                    ", greapprover_master.approverempid " & _
                    ", #APPR_NAME# as name" & _
                    ", #APPR_EMAIL# as email " & _
                    ", #DEPT_ID# AS departmentunkid " & _
                    ", #DEPT_NAME# as departmentname " & _
                    ", #JOB_ID# AS jobunkid" & _
                    ", #JOB_NAME# As  jobname " & _
                    ", greapprover_master.isvoid " & _
                    ", greapprover_master.mapuserunkid " & _
                    ", greapprover_master.voiddatetime " & _
                    ", greapprover_master.userunkid " & _
                    ", greapprover_master.voiduserunkid " & _
                    ", greapprover_master.voidreason " & _
                    ", greapprover_master.isexternal  " & _
                    ", greapprover_master.isactive  " & _
                    ", CASE WHEN greapprover_master.isexternal = 1 THEN @YES ELSE @NO END AS ExAppr " & _
                    ", #ACT_DATA# " & _
                    ", #APPR_NAME# + ' (' + greapproverlevel_master.levelname   + ')' AS ApproverNameWithLevel " & _
                    "FROM greapprover_master " & _
                    " LEFT JOIN greapproverlevel_master on greapproverlevel_master.apprlevelunkid = greapprover_master.apprlevelunkid " & _
                    " #EMPL_JOIN# " & _
                    " #DATA_JOIN# "

            If blnAddSelect = False Then
                If blnForApproval Then
                    strQ &= " JOIN greapprover_tran ON greapprover_tran.approvermasterunkid = greapprover_master.approvermasterunkid "
                End If
            End If

            StrFinalQurey = strQ

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            StrQCondition &= " WHERE 1 = 1 AND greapprover_master.isexternal = #ExAppr# "


            If blnForApproval Then
                If strEmpIds.Trim.Length > 0 Then
                    StrQCondition &= " AND greapprover_tran.employeeunkid IN (" & strEmpIds & ") "
                End If
            End If


            If mintApprovalSetting > 0 Then
                StrQCondition &= " And greapprover_master.approvalsettingid = @approvalsetting "
            End If


            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQDtFilters &= xDateFilterQry & " "
                End If
            End If

            If blnOnlyActive Then
                StrQCondition &= " AND greapprover_master.isvoid = 0 AND greapprover_master.isactive = 1 "
            Else
                StrQCondition &= " AND greapprover_master.isvoid = 0  AND greapprover_master.isactive = 0 "
            End If

            If intApproverTranUnkid > 0 Then
                StrQCondition &= " AND greapprover_master.approverempid = '" & intApproverTranUnkid & "' "
            End If
            If blnAddSelect = False Then
                If blnForApproval Then
                    StrQCondition &= " AND greapprover_tran.isvoid = 0 "
                End If
            End If

            If mstrFilter.Trim.Length > 0 Then
                StrQCondition &= mstrFilter
            End If


            'Gajanan [4-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes
            If xmintMapUserid > 0 Then
                StrQCondition &= "and greapprover_master.mapuserunkid = " & xmintMapUserid & ""
            End If
            'Gajanan [4-July-2019] -- End


            'S.SANDEEP |30-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Grievance UAT]
            'Dim StrDataJoin As String = " LEFT JOIN " & _
            '                              " ( " & _
            '                              "    SELECT " & _
            '                              "        departmentunkid " & _
            '                              "        ,employeeunkid " & _
            '                              "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '                              "    FROM #DB_Name#hremployee_transfer_tran " & _
            '                              "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
            '                              " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
            '                              " LEFT JOIN #DB_Name#hrdepartment_master on hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
            '                              " LEFT JOIN " & _
            '                              " ( " & _
            '                              "    SELECT " & _
            '                              "         jobunkid " & _
            '                              "        ,jobgroupunkid " & _
            '                              "        ,employeeunkid " & _
            '                              "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '                              "    FROM #DB_Name#hremployee_categorization_tran " & _
            '                              "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
            '                              " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
            '                              " LEFT JOIN #DB_Name#hrjob_master on hrjob_master.jobunkid = Jobs.jobunkid "

            'strQ = strQ.Replace("#APPR_NAME#", "CASE " & _
            '                                    "WHEN greapprover_master.approvalsettingid = 1 THEN CASE " & _
            '                                    "WHEN ISNULL(guser.firstname + guser.lastname, '') = '' THEN " & _
            '                                    "ISNULL(guser.username, '') " & _
            '                                    "ELSE " & _
            '                                    "ISNULL(guser.firstname, '') + ' ' + ISNULL(guser.lastname, '') " & _
            '                                    "END " & _
            '                                    "WHEN greapprover_master.approvalsettingid = 2 THEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') " & _
            '                                    "END ")
            'strQ = strQ.Replace("#APPR_EMAIL#", "CASE when ISNULL(hremployee_master.email, '') = '' THEN " & _
            '                                    "ISNULL(guser.email, '') " & _
            '                                    "ELSE " & _
            '                                    "ISNULL(hremployee_master.email, '') " & _
            '                                    "END ")
            
            'strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master AS guser " & _
            '                                   "ON guser.userunkid = greapprover_master.mapuserunkid " & _
            '                                   "LEFT JOIN hremployee_master " & _
            '                                   "ON hremployee_master.employeeunkid = guser.employeeunkid ")


            Dim StrDataJoin As String = " LEFT JOIN " & _
                                          " ( " & _
                                          "    SELECT " & _
                                          "        departmentunkid " & _
                                          "        ,employeeunkid " & _
                                          "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                          "    FROM #DB_Name#hremployee_transfer_tran " & _
                                          "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                                          " ) AS Alloc ON Alloc.employeeunkid = #empappr#.employeeunkid AND Alloc.rno = 1 " & _
                                          " LEFT JOIN #DB_Name#hrdepartment_master on hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                                          " LEFT JOIN " & _
                                          " ( " & _
                                          "    SELECT " & _
                                          "         jobunkid " & _
                                          "        ,jobgroupunkid " & _
                                          "        ,employeeunkid " & _
                                          "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                          "    FROM #DB_Name#hremployee_categorization_tran " & _
                                          "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
                                          " ) AS Jobs ON Jobs.employeeunkid = #empappr#.employeeunkid AND Jobs.rno = 1 " & _
                                          " LEFT JOIN #DB_Name#hrjob_master on hrjob_master.jobunkid = Jobs.jobunkid "

            strQ = strQ.Replace("#APPR_NAME#", "CASE " & _
                                                "WHEN greapprover_master.approvalsettingid = 1 THEN CASE " & _
                                                "WHEN ISNULL(guser.firstname + guser.lastname, '') = '' THEN " & _
                                                "ISNULL(guser.username, '') " & _
                                                "ELSE " & _
                                                "ISNULL(guser.firstname, '') + ' ' + ISNULL(guser.lastname, '') " & _
                                                "END " & _
                                                " WHEN greapprover_master.approvalsettingid = 2 THEN ISNULL(empappr.firstname, '') + ' ' + ISNULL(empappr.surname, '') END ")
            strQ = strQ.Replace("#APPR_EMAIL#", "CASE WHEN ISNULL(empappr.email, '') = '' THEN ISNULL(guser.email, '') ELSE ISNULL(empappr.email, '') END ")

            strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master AS guser ON guser.userunkid = greapprover_master.mapuserunkid " & _
                                               "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = guser.employeeunkid " & _
                                               "LEFT JOIN hremployee_master AS empappr ON empappr.employeeunkid = greapprover_master.approverempid ")
            'S.SANDEEP |30-MAY-2019| -- END
            
            strQ = strQ.Replace("#DATA_JOIN#", StrDataJoin)
            strQ = strQ.Replace("#DB_Name#", "")
            strQ = strQ.Replace("#DEPT_ID#", "hrdepartment_master.departmentunkid")
            strQ = strQ.Replace("#DEPT_NAME#", "ISNULL(hrdepartment_master.name,'') ")
            strQ = strQ.Replace("#JOB_ID#", "hrjob_master.jobunkid")
            strQ = strQ.Replace("#JOB_NAME#", "ISNULL(hrjob_master.job_name,'') ")

            'S.SANDEEP |30-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Grievance UAT]
            'strQ = strQ.Replace("#ACT_DATA#", "CAST(CASE WHEN CONVERT(CHAR(8),hremployee_master.appointeddate, 112) <= #EFDATE# " & _
            '                                  "       AND ISNULL(CONVERT(CHAR(8),TRM.LEAVING, 112),#EFDATE#) >= #EFDATE# " & _
            '                                  "       AND ISNULL(CONVERT(CHAR(8),RET.RETIRE, 112),#EFDATE#) >= #EFDATE# " & _
            '                                  "       AND ISNULL(CONVERT(CHAR(8),TRM.EOC, 112),#EFDATE#) >= #EFDATE# " & _
            '                                  "  THEN 1 ELSE 0 END AS BIT) AS activestatus ")

            strQ = strQ.Replace("#ACT_DATA#", "CAST(CASE WHEN CONVERT(CHAR(8),#empappr#.appointeddate, 112) <= #EFDATE# " & _
                                              "       AND ISNULL(CONVERT(CHAR(8),TRM.LEAVING, 112),#EFDATE#) >= #EFDATE# " & _
                                              "       AND ISNULL(CONVERT(CHAR(8),RET.RETIRE, 112),#EFDATE#) >= #EFDATE# " & _
                                              "       AND ISNULL(CONVERT(CHAR(8),TRM.EOC, 112),#EFDATE#) >= #EFDATE# " & _
                                              "  THEN 1 ELSE 0 END AS BIT) AS activestatus ")
            'S.SANDEEP |30-MAY-2019| -- END
            
            strQ = strQ.Replace("#EFDATE#", "'" & strEmployeeAsOnDate & "' ")

            strQ &= StrQCondition
            strQ &= StrQDtFilters
            strQ = strQ.Replace("#ExAppr#", "0")
            'S.SANDEEP |30-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Grievance UAT]
            strQ = strQ.Replace("#empappr#", "empappr")
            'S.SANDEEP |30-MAY-2019| -- END
            strSelect &= strQ
            objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Yes")
            objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "No")
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Select"))
            objDataOperation.AddParameter("@approvalsetting", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovalSetting)
            dsList = objDataOperation.ExecQuery(strSelect, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Dim dsCompany As New DataSet
            dsCompany = GetExternalApproverList(objDataOperation, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr In dsCompany.Tables(0).Rows
                strQ = StrFinalQurey : StrQDtFilters = ""
                Dim dstmp As New DataSet
                If dr("DName").Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPR_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = greapprover_master.approverempid  ")
                    strQ = strQ.Replace("#ExAppr#", "1")
                    strQ = strQ.Replace("#DB_Name#", "")
                    strQ = strQ.Replace("#DATA_JOIN#", "")
                    strQ = strQ.Replace("#DEPT_ID#", "0")
                    strQ = strQ.Replace("#DEPT_NAME#", "'' ")
                    strQ = strQ.Replace("#JOB_ID#", "0")
                    strQ = strQ.Replace("#JOB_NAME#", "'' ")
                    strQ = strQ.Replace("#APPR_EMAIL#", "ISNULL(cfuser_master.email,'')")
                    strQ = strQ.Replace("#ACT_DATA#", " hrmsConfiguration..cfuser_master.isactive AS activestatus ")
                    'S.SANDEEP |30-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : [Grievance UAT]
                    strQ = strQ.Replace("#empappr#", "")
                    'S.SANDEEP |30-MAY-2019| -- END
                Else
                    xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
                    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dr("EDate")), eZeeDate.convertDate(dr("EDate")), , , dr("DName"))
                    Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(dr("EDate")), dr("DName"))

                    strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                       "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = greapprover_master.approverempid " & _
                                                       "LEFT JOIN #DB_Name#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DATA_JOIN#", StrDataJoin)
                    strQ = strQ.Replace("#DB_Name#", dr("DName") & "..")
                    strQ = strQ.Replace("#DEPT_ID#", "hrdepartment_master.departmentunkid")
                    strQ = strQ.Replace("#DEPT_NAME#", "ISNULL(hrdepartment_master.name,'') ")
                    strQ = strQ.Replace("#SEC_NAME#", "ISNULL(hrsection_master.name,'') ")
                    strQ = strQ.Replace("#JOB_ID#", "hrjob_master.jobunkid")
                    strQ = strQ.Replace("#JOB_NAME#", "ISNULL(hrjob_master.job_name,'') ")
                    strQ = strQ.Replace("#APPR_EMAIL#", "ISNULL(hremployee_master.email,'')")
                    strQ = strQ.Replace("#ACT_DATA#", "CAST(CASE WHEN CONVERT(CHAR(8),hremployee_master.appointeddate, 112) <= #EFDATE# " & _
                                              "       AND ISNULL(CONVERT(CHAR(8),TRM.LEAVING, 112),#EFDATE#) >= #EFDATE# " & _
                                              "       AND ISNULL(CONVERT(CHAR(8),RET.RETIRE, 112),#EFDATE#) >= #EFDATE# " & _
                                              "       AND ISNULL(CONVERT(CHAR(8),TRM.EOC, 112),#EFDATE#) >= #EFDATE# " & _
                                              "  THEN 1 ELSE 0 END AS BIT) AS activestatus ")
                    strQ = strQ.Replace("#EFDATE#", "'" & dr("EDate") & "' ")
                    'S.SANDEEP |30-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : [Grievance UAT]
                    strQ = strQ.Replace("#empappr#", "hremployee_master")
                    'S.SANDEEP |30-MAY-2019| -- END

                    If xDateJoinQry.Trim.Length > 0 Then
                        strQ &= xDateJoinQry
                    End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry
                    End If

                    If xIncludeIn_ActiveEmployee = False Then
                        If xDateFilterQry.Trim.Length > 0 Then
                            StrQDtFilters &= xDateFilterQry & " "
                        End If
                    End If

                End If

                strQ &= StrQCondition
                strQ &= StrQDtFilters
                strQ = strQ.Replace("#ExAppr#", "1")
                strQ &= " AND cfuser_master.companyunkid = " & dr("companyunkid")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Yes")
                objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "No")
                objDataOperation.AddParameter("@approvalsetting", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovalSetting)
                dstmp = objDataOperation.ExecQuery(strQ, strTableName)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dstmp.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dstmp.Tables(0), True)
                End If
            Next


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList

    End Function

    Public Function GetExternalApproverList(Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal strList As String = "List", Optional ByVal blnIncludeDelete As Boolean = False) As DataSet
        Dim objDataOperation As clsDataOperation
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsApprover As New DataSet

        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            If strList.Trim.Length <= 0 Then strList = "List"

            StrQ = "SELECT DISTINCT " & _
                   "     ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                   "    ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                   "    ,ISNULL(cffinancial_year_tran.yearunkid,0) AS yearunkid " & _
                   "    ,ISNULL(EffDt.key_value,'') AS EDate " & _
                   "    ,ISNULL(UC.key_value,'') AS ModeSet " & _
                   "FROM greapprover_master " & _
                   "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = greapprover_master.approverempid " & _
                   "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         cfconfiguration.companyunkid " & _
                   "        ,cfconfiguration.key_value " & _
                   "    FROM hrmsConfiguration..cfconfiguration " & _
                   "    WHERE UPPER(cfconfiguration.key_name) IN ('EMPLOYEEASONDATE') " & _
                   ") AS EffDt ON EffDt.companyunkid = cffinancial_year_tran.companyunkid " & _
                   "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         cfconfiguration.companyunkid " & _
                   "        ,cfconfiguration.key_value " & _
                   "    FROM hrmsConfiguration..cfconfiguration " & _
                   "    WHERE UPPER(cfconfiguration.key_name) IN ('USERACCESSMODESETTING') " & _
                   ") AS UC ON UC.companyunkid = cffinancial_year_tran.companyunkid " & _
                   "WHERE greapprover_master.isexternal = 1 "


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            'AND lvleaveapprover_master.isactive = 1
            If blnIncludeDelete = False Then
                StrQ &= " AND greapprover_master.isvoid = 0 "


            End If
            'Pinkal (01-Mar-2016) -- End



            dsApprover = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetExternalApproverList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsApprover
    End Function

    Public Function GetApproverEmployeeId(ByVal ApproveID As Integer, ByVal blnExternalApprover As Boolean) As String 'S.SANDEEP [30 JAN 2016] -- START {blnExternalApprover} -- END
        Dim mstrEmployeeIds As String = ""
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' " & _
                               "+ CAST(greapprover_tran.employeeunkid AS VARCHAR(MAX)) " & _
                      "FROM     greapprover_master " & _
                                "JOIN greapprover_tran ON greapprover_master.approvermasterunkid = greapprover_tran.approvermasterunkid AND greapprover_tran.isvoid = 0" & _
                      "WHERE    greapprover_master.approverempid = @approverempid AND greapprover_master.isvoid = 0 AND greapprover_master.isactive = 1 " & _
                      " AND greapprover_master.isexternal = @isexternal " & _
                      "ORDER BY greapprover_tran.employeeunkid " & _
                    "FOR " & _
                      "XML PATH('') " & _
                    "), 1, 1, ''), '') EmployeeIds "

            objDataOperation.AddParameter("@isexternal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnExternalApprover)
            objDataOperation.AddParameter("@approverempid", SqlDbType.Int, eZeeDataType.INT_SIZE, ApproveID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dsList.Tables(0).Rows.Count > 0 Then
                mstrEmployeeIds = dsList.Tables(0).Rows(0)("EmployeeIds").ToString()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverEmployeeId", mstrModuleName)
        End Try
        Return mstrEmployeeIds
    End Function

    Public Function Insert(ByVal mdtran As DataTable) As Boolean


        If mintapprovalsettingid = enGrievanceApproval.UserAcess Then
            If isExistApproverForAllDepartment(mintmapuserunkid, 0, , , mblnisexternal) Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Grievance Approver already exists. Please define new Grievance Approver.")
                Return False
            End If

            'Gajanan [06-SEP-2019] -- Start      

            'ElseIf mintapprovalsettingid = enGrievanceApproval.ApproverEmpMapping Then
            '    If isExistApproverForAllDepartment(mintapproverempid, 0, , mblnisexternal) Then
            '        mstrMessage = Language.getMessage(mstrModuleName, 1, "This Grievance Approver already exists. Please define new Grievance Approver.")
            '        Return False
            '    End If

            'Gajanan [06-SEP-2019] -- End
        End If

        If isExistApproverForAllDepartment(mintapproverempid, mintapprlevelunkid, , , mblnisexternal) Then
            mstrMessage = "This Grievance Approver already exists. Please define new Grievance Approver."
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "INSERT INTO greapprover_master ( " & _
              "  apprlevelunkid " & _
              ", approverempid " & _
              ", mapuserunkid " & _
              ", approvalsettingid" & _
              ", userunkid" & _
              ", isactive " & _
              ", isexternal " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid" & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @apprlevelunkid " & _
              ", @approverempid " & _
              ", @mapuserunkid " & _
              ", @approvalsettingid" & _
              ", @userunkid" & _
              ", @isactive " & _
              ", @isexternal " & _
              ", @isvoid " & _
              ", @voiddatetime " & _
              ", @voiduserunkid" & _
              ", @voidreason" & _
            "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@apprlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprlevelunkid.ToString())
            objDataOperation.AddParameter("@approverempid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapproverempid.ToString())
            If mblnisexternal Then If mintmapuserunkid <= 0 Then mintmapuserunkid = mintapproverempid
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintmapuserunkid.ToString())
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprovalsettingid.ToString())
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintuserunkid.ToString())
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString())
            objDataOperation.AddParameter("@isexternal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisexternal.ToString())
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString())
            If mdtvoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString())
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrvoidreason.ToString())

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintapprovermasterunkid = dsList.Tables(0).Rows(0).Item(0)

            objgrievanceapprover_Tran._Approvermasterunkid = mintapprovermasterunkid
            objgrievanceapprover_Tran._DataList = mdtran
            objgrievanceapprover_Tran._Userunkid = mintuserunkid
            objgrievanceapprover_Tran._ClientIp = mstrClientIp
            objgrievanceapprover_Tran._loginemployeeunkid = mintloginemployeeunkid
            objgrievanceapprover_Tran._HostName = mstrHostName
            objgrievanceapprover_Tran._FormName = mstrFormName
            objgrievanceapprover_Tran._IsFromWeb = blnIsFromWeb

            If objgrievanceapprover_Tran._DataList IsNot Nothing AndAlso objgrievanceapprover_Tran._DataList.Rows.Count > 0 Then
                If objgrievanceapprover_Tran.InsertDelete_GrievanceApproverData(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If Insert_AtTranLog(objDataOperation, 1, mintapprovermasterunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Update(ByVal mdtran As DataTable) As Boolean

        If isExistApproverForAllDepartment(mintapproverempid, mintapprlevelunkid, mintapprovermasterunkid, , mblnisexternal) Then
            'Hemant (29 May 2019) -- Start
            'ISSUE/ENHANCEMENT : UAT Changes
            'mstrMessage = Language.getMessage(mstrModuleName, 1, "This Leave Approver already exists. Please define new Leave Approver.")
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Grievance Approver already exists. Please define new Grievance Approver.")
            'Hemant (29 May 2019) -- End
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = "UPDATE greapprover_master SET " & _
              "  apprlevelunkid = @apprlevelunkid" & _
              ", approverempid = @approverempid" & _
              ", mapuserunkid = @mapuserunkid" & _
              ", userunkid = @userunkid" & _
              ", isactive = @isactive" & _
              ", isexternal = @isexternal " & _
              ", isvoid = @isvoid " & _
              ", voiduserunkid = @voiduserunkid " & _
              ", voidreason = @voidreason " & _
            "WHERE approvermasterunkid = @approvermasterunkid And approvalsettingid =  @approvalsettingid"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprovermasterunkid.ToString)
            objDataOperation.AddParameter("@apprlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprlevelunkid.ToString)
            objDataOperation.AddParameter("@approverempid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mintapproverempid.ToString)
            If mblnisexternal Then If mintmapuserunkid <= 0 Then mintmapuserunkid = mintapproverempid
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintmapuserunkid.ToString())
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprovalsettingid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@isexternal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisexternal.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrvoidreason.ToString)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objgrievanceapprover_Tran._Approvermasterunkid = mintapprovermasterunkid
            objgrievanceapprover_Tran._DataList = mdtran
            objgrievanceapprover_Tran._Userunkid = mintuserunkid
            objgrievanceapprover_Tran._ClientIp = mstrClientIp
            objgrievanceapprover_Tran._loginemployeeunkid = mintloginemployeeunkid
            objgrievanceapprover_Tran._HostName = mstrHostName
            objgrievanceapprover_Tran._FormName = mstrFormName
            objgrievanceapprover_Tran._IsFromWeb = blnIsFromWeb

            If objgrievanceapprover_Tran._DataList IsNot Nothing AndAlso objgrievanceapprover_Tran._DataList.Rows.Count > 0 Then
                If objgrievanceapprover_Tran.InsertDelete_GrievanceApproverData(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If Insert_AtTranLog(objDataOperation, 2, mintapprovermasterunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Delete(ByVal intUnkid As Integer, ByVal mdtEmployeeAsonDate As Date) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            mintapprovermasterunkid = intUnkid

            objgrievanceapprover_Tran.GetApproverTran(mdtEmployeeAsonDate, intUnkid)
            Dim dt_temp As DataTable
            dt_temp = objgrievanceapprover_Tran._DataList

            For i = 0 To dt_temp.Rows.Count - 1
                With dt_temp.Rows(i)

                    Dim tranunkid As Integer = CInt(.Item("approvertranunkid").ToString)

                    strQ = " Update greapprover_tran set " & _
                                          " isvoid = 1,voiddatetime=@voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                                          " WHERE approvermasterunkid = @approvermasterunkid  AND isvoid = 0 "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoiduserunkid)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrvoidreason)
                    objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, tranunkid)

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    objgrievanceapprover_Tran._Approvertranunkid = tranunkid
                    objgrievanceapprover_Tran._AuditUserid = minAuditUserid
                    objgrievanceapprover_Tran._Employeeunkid = CInt(.Item("employeeunkid").ToString)
                    objgrievanceapprover_Tran._loginemployeeunkid = mintloginemployeeunkid
                    objgrievanceapprover_Tran._ClientIp = mstrClientIp
                    objgrievanceapprover_Tran._HostName = mstrHostName
                    objgrievanceapprover_Tran._FormName = mstrFormName
                    objgrievanceapprover_Tran._IsFromWeb = blnIsFromWeb

                    If objgrievanceapprover_Tran.Insert_AtTranLog(objDataOperation, 3, tranunkid, mintapprovermasterunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End With
            Next


            Me._Approvermasterunkid(objDataOperation) = intUnkid
            strQ = " Update greapprover_master set " & _
                   " isvoid = 1,voiddatetime=@voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                   " WHERE approvermasterunkid = @approvermasterunkid and approvalsettingid=@approvalsettingid and isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrvoidreason)
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprovalsettingid)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            If Insert_AtTranLog(objDataOperation, 3, mintapprovermasterunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isApproverInGrievanceUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing
        Dim blnIsUsed As Boolean = False
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                             "* " & _
                             "FROM gregrievance_master " & _
                             "LEFT JOIN greresolution_step_tran " & _
                             "ON gregrievance_master.grievancemasterunkid = greresolution_step_tran.grievancemasterunkid " & _
                             "WHERE gregrievance_master.isprocessed = 0 " & _
                             "AND gregrievance_master.isvoid = 0 " & _
                             "AND greresolution_step_tran.approvermasterunkid =@approvermasterunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing
        Dim blnIsUsed As Boolean = False
        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT " & _
                      "TABLE_NAME AS TableName " & _
                   "FROM INFORMATION_SCHEMA.COLUMNS " & _
                   "WHERE COLUMN_NAME='approvermasterunkid' "


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""

            For Each dtRow As DataRow In dsList.Tables("List").Rows

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

                If dtRow.Item("TableName") = "greresolution_step_tran" Then
                    strQ = "SELECT approvermasterunkid FROM " & dtRow.Item("TableName").ToString & " WHERE approvermasterunkid = @approvermasterunkid  and isvoid = 0 AND isprocessed = 0 "
                    objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                    dsList = objDataOperation.ExecQuery(strQ, "Used")


                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList.Tables("Used").Rows.Count > 0 Then
                        blnIsUsed = True
                        Exit For
                    End If
                End If


            Next

            mstrMessage = ""
            Return blnIsUsed
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    Public Function ActiveDeactiveEmployee(ByVal intUnkid As Integer, ByVal blnisactive As Boolean)
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            strQ = "UPDATE greapprover_master " & _
             "  SET isactive = @isacive " & _
             " WHERE approvermasterunkid = @approvermasterunkid " & _
             " and approvalsettingid = @approvalsettingid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isacive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnisactive)
            objDataOperation.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprovalsettingid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            '_Approvermasterunkid(objDataOperation) = intUnkid
            'mblnisactive = blnisactive


            If Insert_AtTranLog(objDataOperation, 2, mintapprovermasterunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: ActiveEmployee; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try

    End Function


    Public Function isExistApproverForAllDepartment(ByVal intApproverempunkid As Integer, ByVal intApproveLevelunkid As Integer, Optional ByVal intunkid As Integer = -1, Optional ByRef intApproverMstId As Integer = 0, Optional ByVal blnIsExAppr As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT " & _
              "  approvermasterunkid " & _
              ", apprlevelunkid " & _
              ", approverempid " & _
              ", mapuserunkid " & _
              ", approvalsettingid " & _
              ", userunkid " & _
              ", isactive " & _
              ", isexternal " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              " FROM greapprover_master " & _
              " WHERE  isvoid = 0 AND isexternal = @isexternal "

            objDataOperation.ClearParameters()
            If intApproveLevelunkid > 0 Then
                strQ &= " AND apprlevelunkid = @apprlevelunkid"
                objDataOperation.AddParameter("@apprlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprlevelunkid)
            End If

            If mintapprovalsettingid = enGrievanceApproval.UserAcess Then
                strQ &= " and mapuserunkid = @approverempid "
                objDataOperation.AddParameter("@approverempid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverempunkid)

            ElseIf mintapprovalsettingid = enGrievanceApproval.ApproverEmpMapping Then
                strQ &= " and approverempid=@approverempid "
                objDataOperation.AddParameter("@approverempid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverempunkid)
            End If

            objDataOperation.AddParameter("@isexternal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsExAppr)

            If intunkid > 0 Then
                strQ &= " AND approvermasterunkid <> @approvermasterunkid"
                objDataOperation.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intunkid)
            End If
            strQ &= " AND approvalsettingid = @approvalsettingid "
            objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprovalsettingid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intApproverMstId = dsList.Tables(0).Rows(0)("approvermasterunkid")
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Insert_AtTranLog(ByVal objDoOps As clsDataOperation, ByVal AuditType As Integer, ByVal intUnkid As Integer)
        Dim StrQ As String = String.Empty
        Dim extForce As Exception
        Dim dsList As New DataSet

        Try
            StrQ = "INSERT INTO atgreapprover_master ( " & _
             "  row_guid " & _
             ", approvermasterunkid " & _
             ", apprlevelunkid " & _
             ", approverempid " & _
             ", mapuserunkid " & _
             ", approvalsettingid " & _
             ", isactive " & _
             ", isexternal " & _
             ", audittype " & _
             ", auditdatetime " & _
             ", audituserunkid " & _
             ", loginemployeeunkid " & _
             ", ip " & _
             ", host " & _
             ", form_name " & _
             ", isweb " & _
           ") VALUES (" & _
              "  @row_guid " & _
              ", @approvermasterunkid " & _
              ", @apprlevelunkid " & _
              ", @approverempid " & _
              ", @mapuserunkid " & _
              ", @approvalsettingid " & _
              ", @isexternal " & _
              ", @isactive " & _
              ", @audittype " & _
              ", GETDATE() " & _
              ", @audituserunkid " & _
              ", @loginemployeeunkid " & _
              ", @ip " & _
              ", @host " & _
              ", @form_name " & _
              ", @isweb )"

            objDoOps.ClearParameters()
            objDoOps.AddParameter("@row_guid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
            objDoOps.AddParameter("@approvermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDoOps.AddParameter("@apprlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprlevelunkid)
            objDoOps.AddParameter("@approverempid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapproverempid)
            objDoOps.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintmapuserunkid)
            objDoOps.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintapprovalsettingid)
            objDoOps.AddParameter("@isexternal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisexternal)
            objDoOps.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisactive)
            objDoOps.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDoOps.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minAuditUserid)
            objDoOps.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintloginemployeeunkid)
            objDoOps.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIp)
            objDoOps.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDoOps.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDoOps.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)

            dsList = objDoOps.ExecQuery(StrQ, "List")

            If objDoOps.ErrorMessage <> "" Then
                extForce = New Exception(objDoOps.ErrorNumber & ": " & objDoOps.ErrorMessage)
                Throw extForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    'S.SANDEEP [31-OCT-2018] -- START
    Public Sub GetUACFilters(ByVal strDatabaseName As String, ByVal strUserAccessMode As String, ByRef strFilter As String, ByRef strJoin As String, ByRef strSelect As String, ByRef strAccessJoin As String, ByRef strOuterJoin As String, ByRef strFinalString As String, ByVal intUserId As Integer, ByVal eMovementType As clsEmployeeMovmentApproval.enMovementType, Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim objDataOperation As New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Dim StrQ As String = ""
        Try
            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
            Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
            For index As Integer = 0 To strvalues.Length - 1
                Select Case CInt(strvalues(index))
                    Case enAllocation.BRANCH
                        strSelect &= ",ISNULL(BR.branchid,0) AS branchid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as branchid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.branchid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.BRANCH) & "' " & _
                                         "    ) AS BR ON BR.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(BR.branchid,0) > 0 "
                        strJoin &= "AND Fn.branchid = [@emp].branchid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrstation_master ON Fn.branchid = hrstation_master.stationunkid "

                    Case enAllocation.DEPARTMENT_GROUP
                        strSelect &= ",ISNULL(DG.deptgrpid,0) AS deptgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as deptgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.deptgrpid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT_GROUP) & "' " & _
                                         "    ) AS DG ON DG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(DG.deptgrpid,0) > 0 "
                        strJoin &= "AND Fn.deptgrpid = [@emp].deptgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_group_master ON Fn.deptgrpid = hrdepartment_group_master.deptgroupunkid "

                    Case enAllocation.DEPARTMENT
                        strSelect &= ",ISNULL(DP.deptid,0) AS deptid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as deptid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.deptid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT) & "' " & _
                                         "    ) AS DP ON DP.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(DP.deptid,0) > 0 "
                        strJoin &= "AND Fn.deptid = [@emp].deptid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_master ON Fn.deptid = hrdepartment_master.departmentunkid "

                    Case enAllocation.SECTION_GROUP
                        strSelect &= ",ISNULL(SG.secgrpid,0) AS secgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as secgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.secgrpid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION_GROUP) & "' " & _
                                         "    ) AS SG ON SG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(SG.secgrpid,0) > 0 "
                        strJoin &= "AND Fn.secgrpid = [@emp].secgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsectiongroup_master ON Fn.secgrpid = hrsectiongroup_master.sectiongroupunkid "

                    Case enAllocation.SECTION
                        strSelect &= ",ISNULL(SC.secid,0) AS secid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as secid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.secid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION) & "' " & _
                                         "    ) AS SC ON SC.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(SC.secid,0) > 0 "
                        strJoin &= "AND Fn.secid = [@emp].secid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsection_master ON Fn.secid = hrsection_master.sectionunkid "

                    Case enAllocation.UNIT_GROUP
                        strSelect &= ",ISNULL(UG.unitgrpid,0) AS unitgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as unitgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.unitgrpid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT_GROUP) & "' " & _
                                         "    ) AS UG ON UG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(UG.unitgrpid,0) > 0 "
                        strJoin &= "AND Fn.unitgrpid = [@emp].unitgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunitgroup_master ON Fn.unitgrpid = hrunitgroup_master.unitgroupunkid "

                    Case enAllocation.UNIT
                        strSelect &= ",ISNULL(UT.unitid,0) AS unitid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as unitid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.unitid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT) & "' " & _
                                         "    ) AS UT ON UT.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(UT.unitid,0) > 0 "
                        strJoin &= "AND Fn.unitid = [@emp].unitid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunit_master ON Fn.unitid = hrunit_master.unitunkid "

                    Case enAllocation.TEAM
                        strSelect &= ",ISNULL(TM.teamid,0) AS teamid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as teamid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.teamid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.TEAM) & "' " & _
                                         "    ) AS TM ON TM.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(TM.teamid,0) > 0 "
                        strJoin &= "AND Fn.teamid = [@emp].teamid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrteam_master ON Fn.teamid = hrteam_master.teamunkid "

                    Case enAllocation.JOB_GROUP
                        strSelect &= " ,ISNULL(JG.jgrpid,0) AS jgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as jgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.jgrpid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOB_GROUP) & "' " & _
                                         "    ) AS JG ON JG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(JG.jgrpid,0) > 0 "
                        strJoin &= "AND Fn.jgrpid = [@emp].jgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjobgroup_master ON Fn.jgrpid = hrjobgroup_master.jobgroupunkid "

                    Case enAllocation.JOBS
                        strSelect &= ",ISNULL(JB.jobid,0) AS jobid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as jobid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.jobid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOBS) & "' " & _
                                         "    ) AS JB ON JB.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(JB.jobid,0) > 0 "
                        strJoin &= "AND Fn.jobid = [@emp].jobid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjob_master ON Fn.jobid = hrjob_master.jobunkid "

                    Case enAllocation.CLASS_GROUP
                        strSelect &= ",ISNULL(CG.clsgrpid,0) AS clsgrpid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as clsgrpid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.clsgrpid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASS_GROUP) & "' " & _
                                         "    ) AS CG ON CG.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(CG.clsgrpid,0) > 0 "
                        strJoin &= "AND Fn.clsgrpid = [@emp].clsgrpid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master ON Fn.clsgrpid = hrclassgroup_master.classgroupunkid "

                    Case enAllocation.CLASSES
                        strSelect &= ",ISNULL(CL.clsid,0) AS clsid "
                        strAccessJoin &= "    LEFT JOIN " & _
                                         "    ( " & _
                                         "        SELECT " & _
                                         "             UPM.userunkid " & _
                                         "            ,allocationunkid as clsid " & _
                                         "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                                         "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                                         "            JOIN #EMP ON #EMP.clsid = UPT.allocationunkid " & _
                                         "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASSES) & "' " & _
                                         "    ) AS CL ON CL.userunkid = cfuser_master.userunkid "
                        strFilter &= "AND ISNULL(CL.clsid,0) > 0 "
                        strJoin &= "AND Fn.clsid = [@emp].clsid "
                        strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclasses_master ON Fn.clsid = hrclasses_master.classesunkid "
                End Select

                objDataOperation.ClearParameters()
                StrQ = "SELECT @Value = ISNULL(STUFF((SELECT DISTINCT ',' + CAST(allocationunkid AS NVARCHAR(MAX)) FROM hrmsConfiguration..cfuseraccess_privilege_tran WHERE useraccessprivilegeunkid IN (SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE userunkid = " & intUserId & " AND referenceunkid = " & strvalues(index) & ") FOR XML PATH('')),1,1,''),'') "
                Dim strvalue = ""
                objDataOperation.AddParameter("@Value", SqlDbType.NVarChar, 4000, strvalue, ParameterDirection.InputOutput)

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                End If

                strvalue = CStr(objDataOperation.GetParameterValue("@Value"))

                If strvalue.Trim.Length > 0 Then
                    Select Case CInt(strvalues(index))
                        Case enAllocation.BRANCH
                            strFinalString &= " AND Fn.branchid IN (" & strvalue & ") "

                        Case enAllocation.DEPARTMENT_GROUP
                            strFinalString &= " AND Fn.deptgrpid IN (" & strvalue & ") "

                        Case enAllocation.DEPARTMENT
                            strFinalString &= " AND Fn.deptid IN (" & strvalue & ") "

                        Case enAllocation.SECTION_GROUP
                            strFinalString &= " AND Fn.secgrpid IN (" & strvalue & ") "

                        Case enAllocation.SECTION
                            strFinalString &= " AND Fn.secid IN (" & strvalue & ") "

                        Case enAllocation.UNIT_GROUP
                            strFinalString &= " AND Fn.unitgrpid IN (" & strvalue & ") "

                        Case enAllocation.UNIT
                            strFinalString &= " AND Fn.unitid IN (" & strvalue & ") "

                        Case enAllocation.TEAM
                            strFinalString &= " AND Fn.teamid IN (" & strvalue & ") "

                        Case enAllocation.JOB_GROUP
                            strFinalString &= " AND Fn.jgrpid IN (" & strvalue & ") "

                        Case enAllocation.JOBS
                            strFinalString &= " AND Fn.jobid IN (" & strvalue & ") "

                        Case enAllocation.CLASS_GROUP
                            strFinalString &= " AND Fn.clsgrpid IN (" & strvalue & ") "

                        Case enAllocation.CLASSES
                            strFinalString &= " AND Fn.clsid IN (" & strvalue & ") "

                    End Select
                End If
            Next
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
            If strJoin.Trim.Length > 0 Then strJoin = strJoin.Substring(3)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUACFilters; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [31-OCT-2018] -- END

    Public Function GetApproversList(ByVal enApprSetting As enGrievanceApproval _
                                   , ByVal xDatabaseName As String _
                                   , ByVal xUserUnkid As Integer _
                                   , ByVal xYearUnkid As Integer _
                                   , ByVal xCompanyUnkid As Integer _
                                   , ByVal xPrivilegId As Integer _
                                   , ByVal xUserModeSetting As String _
                                   , ByVal xOnlyApproved As Boolean _
                                   , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                   , ByVal blnOnlyActive As Boolean _
                                   , ByVal xEmployeeAsOnDate As String _
                                   , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                   , Optional ByVal objDOperation As clsDataOperation = Nothing _
                                   , Optional ByVal strEmployeeIds As String = "" _
                                   , Optional ByVal blnAddSelect As Boolean = False _
                                   , Optional ByVal mstrfilter As String = "" _
                                   , Optional ByVal blnadduserfiler As Boolean = True _
                                   , Optional ByVal xmintMapUserid As Integer = -1 _
                                   , Optional ByVal xmblnApplyIsProcessFilter As Boolean = True) As DataSet
'Gajanan [24-OCT-2019] -- Add [xmblnApplyIsProcessFilter]
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim objMovementApproval As New clsEmployeeMovmentApproval
        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If
        objDataOperation.ClearParameters()
        Try
            Select Case enApprSetting

                'Gajanan [24-OCT-2019] -- Start   
                'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
                Case enGrievanceApproval.ReportingTo
                    dsList = GetReportingApproverList("List", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xEmployeeAsOnDate, xUserModeSetting, True, xIncludeIn_ActiveEmployee, strEmployeeIds, objDataOperation, mstrfilter, True, blnAddSelect, CInt(enApprSetting), xmintMapUserid, xmblnApplyIsProcessFilter)
                    'Gajanan [24-OCT-2019] -- End


                Case enGrievanceApproval.ApproverEmpMapping

                    'Gajanan [4-July-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes
                    'dsList = GetList("List", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xEmployeeAsOnDate, xUserModeSetting, True, xIncludeIn_ActiveEmployee, blnOnlyActive, blnApplyUserAccessFilter, strEmployeeIds, objDataOperation, mstrfilter, "", 0, True, blnAddSelect, CInt(enApprSetting))
                    dsList = GetList("List", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xEmployeeAsOnDate, xUserModeSetting, True, xIncludeIn_ActiveEmployee, blnOnlyActive, blnApplyUserAccessFilter, strEmployeeIds, objDataOperation, mstrfilter, "", 0, True, blnAddSelect, CInt(enApprSetting), xmintMapUserid)
                    'Gajanan [4-July-2019] -- End


                Case enGrievanceApproval.UserAcess
                    Dim strCurrUsr, strActiveUsr As String
                    StrQ = "SELECT ISNULL(STUFF((SELECT DISTINCT ',' + CAST(mapuserunkid AS NVARCHAR(10)) FROM greapprover_master WHERE approvalsettingid = " & enApprSetting & " AND isactive = 1 FOR XML PATH('')), 1, 1, ''),'0') AS ivalue "
                    dsList = objDataOperation.ExecQuery(StrQ, "List")
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    If dsList.Tables("List").Rows.Count > 0 Then

                        strCurrUsr = "" : strActiveUsr = ""
                        strCurrUsr = dsList.Tables("List").Rows(0)("ivalue")
                        Dim objConfig As New clsConfigOptions
                        objConfig.GetActiveUserList_CSV(strCurrUsr, strActiveUsr)
                        objConfig = Nothing
                    End If

                    Dim strFilter As String = String.Empty
                    Dim strJoin As String = String.Empty
                    Dim strSelect As String = String.Empty
                    Dim strAccessJoin As String = String.Empty
                    Dim strOuterJoin As String = String.Empty
                    Dim strFinalString As String = ""
                    'clsEmployeeMovmentApproval.enMovementType.CONFIRMATION --> PASSED INTENSIONALLY, TO GET ALL MOVEMENT INFO

                    'S.SANDEEP [31-OCT-2018] -- START
                    GetUACFilters(xDatabaseName, xUserModeSetting, strFilter, strJoin, strSelect, strAccessJoin, strOuterJoin, strFinalString, xUserUnkid, clsEmployeeMovmentApproval.enMovementType.CONFIRMATION, objDataOperation)
                    'S.SANDEEP [31-OCT-2018] -- END

                    Dim strUACJoin, strUACFilter As String
                    strUACJoin = "" : strUACFilter = ""
                    If blnApplyUserAccessFilter = True Then modGlobal.NewAccessLevelFilterString(strUACJoin, strUACFilter, eZeeDate.convertDate(xEmployeeAsOnDate), True, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, "", True)

                    'S.SANDEEP [31-OCT-2018] -- START
                    StrQ = "IF OBJECT_ID('tempdb..#EMP') IS NOT NULL " & _
                           "DROP TABLE #EMP "
                    'S.SANDEEP [31-OCT-2018] -- END


                    StrQ &= "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
                           "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
                            "SELECT DISTINCT " & _
                           "     hremployee_master.employeeunkid " & _
                           "    ,ISNULL(T.departmentunkid,0) AS departmentunkid " & _
                           "    ,ISNULL(J.jobunkid,0) AS jobunkid " & _
                           "    ,ISNULL(T.classgroupunkid,0) AS classgroupunkid " & _
                           "    ,ISNULL(T.classunkid,0) AS classunkid " & _
                           "    ,ISNULL(T.stationunkid,0) AS stationunkid " & _
                           "    ,ISNULL(T.deptgroupunkid,0) AS deptgroupunkid " & _
                           "    ,ISNULL(T.sectiongroupunkid,0) AS sectiongroupunkid " & _
                           "    ,ISNULL(T.sectionunkid,0) AS sectionunkid " & _
                           "    ,ISNULL(T.unitgroupunkid,0) AS unitgroupunkid " & _
                           "    ,ISNULL(T.unitunkid,0) AS unitunkid " & _
                           "    ,ISNULL(T.teamunkid,0) AS teamunkid " & _
                           "    ,ISNULL(J.jobgroupunkid,0) AS jobgroupunkid " & _
                            "FROM hremployee_master " & _
                            "   JOIN gregrievance_master ON gregrievance_master.fromemployeeunkid = hremployee_master.employeeunkid "
                    If strUACJoin.Trim.Length > 0 Then
                        StrQ &= strUACJoin
                    End If
                    StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        stationunkid " & _
                            "       ,deptgroupunkid " & _
                            "       ,departmentunkid " & _
                            "       ,sectiongroupunkid " & _
                            "       ,sectionunkid " & _
                            "       ,unitgroupunkid " & _
                            "       ,unitunkid " & _
                            "       ,teamunkid " & _
                            "       ,classgroupunkid " & _
                            "       ,classunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                            "   FROM hremployee_transfer_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' "
                    If strEmployeeIds.Trim.Length > 0 Then StrQ &= " AND employeeunkid IN (" & strEmployeeIds & ") "
                    StrQ &= ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                            "LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        jobgroupunkid " & _
                            "       ,jobunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                            "   FROM hremployee_categorization_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & xEmployeeAsOnDate & "' "
                    If strEmployeeIds.Trim.Length > 0 Then StrQ &= " AND employeeunkid IN (" & strEmployeeIds & ") "
                    StrQ &= ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                            "WHERE 1 = 1 AND gregrievance_master.isvoid = 0 AND gregrievance_master.issubmitted = 1 AND gregrievance_master.isprocessed = 0 "
                    If strEmployeeIds.Trim.Length > 0 Then StrQ &= " AND hremployee_master.employeeunkid IN (" & strEmployeeIds & ") "
                    'S.SANDEEP [31-OCT-2018] -- START
                    StrQ &= " SELECT * INTO #EMP FROM @emp "
                    'S.SANDEEP [31-OCT-2018] -- END
                    If blnAddSelect Then
                        StrQ &= "SELECT " & _
                                "     0 AS approvermasterunkid " & _
                                "    ,@Select AS name " & _
                                "    ,'' AS email " & _
                                "    ,0 AS priority " & _
                                "    ,0 AS employeeunkid " & _
                                "    ,'' AS levelname " & _
                                "    ,'' AS ecode " & _
                                "    ,'' AS ename " & _
                                "    ,0 AS approverempid " & _
                                "    ,0 AS ucompanyid " & _
                                "    ,0 AS mapuserunkid " & _
                                "    ,CAST(1 AS BIT) AS activestatus " & _
                                "    ,1 AS rno UNION ALL "
                    End If
                    StrQ &= "SELECT DISTINCT " & _
                            "     Fn.approvermasterunkid AS approvermasterunkid " & _
                            ",    CASE WHEN ISNULL(fn.fullname,'') = '' THEN " & _
                            "    Fn.username " & _
                            "       ELSE " & _
                            "    fn.fullname " & _
                            "    end as name " & _
                            "    ,Fn.email " & _
                            "    ,Fn.priority " & _
                            "    ,EM.employeeunkid " & _
                            "    ,ISNULL(Fn.LvName,'') AS levelname " & _
                            "    ,EM.employeecode as ecode " & _
                            "    ,EM.firstname+' '+ISNULL(EM.othername,'')+' '+EM.surname AS ename " & _
                            "    ,approverempid " & _
                            "    ,ucompanyid " & _
                            "    ,Fn.userunkid AS mapuserunkid " & _
                            "    ,CAST(1 AS BIT) AS activestatus " & _
                            "    ,ROW_NUMBER()OVER(PARTITION BY EM.employeeunkid ORDER BY Fn.priority ASC) AS rno " & _
                            "FROM @emp " & _
                            "    JOIN " & xDatabaseName & "..hremployee_master AS EM ON [@emp].empid = EM.employeeunkid " & _
                            "    JOIN " & _
                            "    ( " & _
                            "        SELECT DISTINCT " & _
                            "             cfuser_master.userunkid " & _
                            "            ,cfuser_master.username " & _
                            "            ,cfuser_master.firstname + ' ' +  cfuser_master.lastname as fullname " & _
                            "            ,cfuser_master.email " & _
                                          strSelect & " " & _
                            "            ,LM.priority " & _
                            "            ,LM.levelname AS LvName " & _
                            "            ,cfuser_master.employeeunkid as approverempid " & _
                            "            ,cfuser_master.companyunkid as ucompanyid " & _
                            "            ,cfuser_master.userunkid AS mapuserunkid " & _
                            "            ,EM.approvermasterunkid AS approvermasterunkid " & _
                            "        FROM hrmsConfiguration..cfuser_master " & _
                            "            JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                            "            JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                                        strAccessJoin & " " & _
                            "        JOIN " & xDatabaseName & "..greapprover_master EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
                            "        JOIN " & xDatabaseName & "..greapproverlevel_master AS LM ON EM.apprlevelunkid = LM.apprlevelunkid " & _
                            "        WHERE cfuser_master.isactive = 1 AND (" & strFilter & " ) AND EM.isactive = 1 AND EM.isvoid = 0 "
                    If strActiveUsr.Trim.Length > 0 Then
                        StrQ &= "AND EM.mapuserunkid IN (" & strActiveUsr & ") "
                    End If
                    StrQ &= "            AND EM.approvalsettingid = " & enApprSetting & " " & _
                            "            /*AND companyunkid = @C*/ AND yearunkid = @Y AND privilegeunkid = @P  "

                    If blnadduserfiler Then
                        If xUserUnkid > 0 Then
                            StrQ &= " AND hrmsConfiguration..cfuser_master.userunkid = @U "
                        End If
                    End If

                    StrQ &= ") AS Fn ON " & strJoin & " " & _
                            strOuterJoin & " " & _
                            "WHERE 1 = 1 "

                    'S.SANDEEP [31-OCT-2018] -- START
                    StrQ &= "IF OBJECT_ID('tempdb..#EMP') IS NOT NULL " & _
                           "DROP TABLE #EMP "
                    'S.SANDEEP [31-OCT-2018] -- END

                    objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyUnkid)
                    objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, xYearUnkid)
                    objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, xPrivilegId)
                    objDataOperation.AddParameter("@U", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkid)
                    objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Select"))

                    dsList = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

            End Select

            'Dim dtTab As DataTable = New DataView(dsList.Tables(0), "email <> ''", "", DataViewRowState.CurrentRows).ToTable.Copy()
            'dsList.Tables.RemoveAt(0)
            'dsList.Tables.Add(dtTab.Copy())

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproversList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Sub SendNotificationToApprover(ByVal dtr() As DataRow, _
                                          ByVal strSelfServiceURL As String, _
                                          ByVal strFormName As String, _
                                          ByVal eMode As enLogin_Mode, _
                                          ByVal strSenderAddress As String, _
                                          ByVal strAgainstEmployee As String, _
                                          ByVal strGrievanceRefNo As String, _
                                          ByVal intGrievanceId As Integer, _
                                          ByVal strRaisedEmployee As String, _
                                          ByVal intCompanyId As Integer, _
                                          ByVal iLoginEmployeeId As Integer, _
                                          ByVal mstrWebClientIP As String, _
                                          ByVal mstrWebHostName As String, _
                                          ByVal mstrGrievanceType As String)'Gajanan [23-SEP-2019] -- Add [mstrGrievanceType]

        Dim dsList As New DataSet
        Try
            For index As Integer = 0 To dtr.Length - 1
                If dtr(index)("email").ToString().Length <= 0 Then Continue For
                Dim StrMessage As New System.Text.StringBuilder
                StrMessage.Append("<HTML><BODY>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")


                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'StrMessage.Append("Dear <b>" & dtr(index)("name") & "</b></span></p>")
                'Hemant (29 May 2019) -- Start
                'ISSUE/ENHANCEMENT : UAT Changes


                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        

                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                'StrMessage.Append(Language.getMessage(mstrModuleName, 10, "Dear") & " <b>" & getTitleCase(dtr(index)("name")) & "</b></span></p>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 10, "Dear") & " " & getTitleCase(dtr(index)("name")) & "</span></p>")
                'Gajanan [27-June-2019] -- End

                'StrMessage.Append(Language.getMessage(mstrModuleName, 11, "Dear Approver ") & " <b>" & getTitleCase(dtr(index)("name")) & ",</b></span></p>")
                'Gajanan [10-June-2019] -- End

                'Hemant (29 May 2019) -- End
                'Gajanan [27-Mar-2019] -- End


                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language

                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & Language.getMessage(mstrModuleName, 2, "This is to inform you that, I have raised grievance against ") & " <b>" & _
                'Hemant (29 May 2019) -- Start
                'ISSUE/ENHANCEMENT : UAT Changes
                'StrMessage.Append(Language.getMessage(mstrModuleName, 2, "This is to inform you that, I have raised grievance against ") & " <b>" & _
                '                    strAgainstEmployee & "</b> " & Language.getMessage(mstrModuleName, 3, " with reference number") & " <b>" & strGrievanceRefNo & "</b>. " & _
                '                    Language.getMessage(mstrModuleName, 4, "Please click below link in order to provide response.") & "</span></p>")

                'Gajanan [23-SEP-2019] -- Start    


                'StrMessage.Append(Language.getMessage(mstrModuleName, 13, "This is to inform you that I have raised grievance against ") & " <b>" & _
                '                    strAgainstEmployee & "</b> " & Language.getMessage(mstrModuleName, 3, "with reference number") & " <b>" & strGrievanceRefNo & "</b>. " & _
                '                    Language.getMessage(mstrModuleName, 12, "Please click the link below for further details and appropriate actions.") & "</span></p>")

                StrMessage.Append(Language.getMessage(mstrModuleName, 20, "This is to inform you that I have raised grievance against") & " (" & mstrGrievanceType & ") " & Language.getMessage(mstrModuleName, 21, "against") & " <b>" & _
                                    strAgainstEmployee & "</b> " & Language.getMessage(mstrModuleName, 3, "with reference number") & " <b>" & strGrievanceRefNo & "</b>. " & _
                                    Language.getMessage(mstrModuleName, 12, "Please click the link below for further details and appropriate actions.") & "</span></p>")
                'Gajanan [23-SEP-2019] -- End


                'Hemant (29 May 2019) -- End                
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                Dim strLink As String = ""


                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                If strSelfServiceURL.Last() = "/" Then
                    strSelfServiceURL = strSelfServiceURL.Remove(strSelfServiceURL.Length - 1, 1)
                End If
                'Gajanan [27-June-2019] -- End

                strLink = strSelfServiceURL & "/Grievance/wPg_ResolutionStep.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyId.ToString & "|" & IIf(CInt(dtr(index)("mapuserunkid")) = 0, dtr(index)("approverempid").ToString(), dtr(index)("mapuserunkid").ToString()) & "|" & intGrievanceId.ToString() & "|" & dtr(index)("approvermasterunkid").ToString()))
                StrMessage.Append(strLink)

                'Gajanan [24-June-2019] -- Start      
                'StrMessage.Append("</span></p>")
                'Gajanan [24-June-2019] -- End

                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        


                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes

                'StrMessage.Append(Language.getMessage(mstrModuleName, 14, "Regards,"))
                'StrMessage.Append("<BR>")
                StrMessage.Append("<BR>")

                'Gajanan [10-July-2019] -- Start      
                StrMessage.Append("<BR>")
                'Gajanan [10-July-2019] -- End

                StrMessage.Append(Language.getMessage(mstrModuleName, 14, "Regards,"))
                StrMessage.Append("<BR>")
                'Gajanan [27-June-2019] -- End

                StrMessage.Append(strRaisedEmployee)
                'Gajanan [10-June-2019] -- End


                'Gajanan [24-June-2019] -- Start      
                StrMessage.Append("</span></p>")
                'Gajanan [24-June-2019] -- End

                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")




                'Gajanan [27-Mar-2019] -- Start
                'Enhancement - Change Email Language
                'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("</span></p>")
                StrMessage.Append("</BODY></HTML>")

                Dim objMail As New clsSendMail
                objMail._Subject = Language.getMessage(mstrModuleName, 5, "Notification for Grievance Raised")
                objMail._Message = StrMessage.ToString()
                objMail._ToEmail = dtr(index)("email").ToString()
                objMail._Form_Name = mstrFormName
                objMail._WebClientIP = mstrWebClientIP
                objMail._WebHostName = mstrWebHostName
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = eMode
                objMail._UserUnkid = 0
                objMail._SenderAddress = strSenderAddress
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.GRIEVANCE_MGT
                objMail.SendMail(intCompanyId)
                objMail = Nothing
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendNotificationToApprover; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Sub SendNotificationToEmployee(ByVal strSelfServiceURL As String _
                                        , ByVal xEmpUnkid As Integer _
                                        , ByVal strEmailAddress As String _
                                        , ByVal xYearUnkid As Integer _
                                        , ByVal xCompanyUnkid As Integer _
                                        , ByVal intGrievanceUnkid As Integer _
                                        , ByVal strGrievanceRefNo As String _
                                        , ByVal strFormName As String _
                                        , ByVal eMode As enLogin_Mode _
                                        , ByVal strSenderAddress As String _
                                        , ByVal strRaisedEmployee As String _
                                        , ByVal iUserId As Integer _
                                        , ByVal mstrWebClientIP As String _
                                        , ByVal mstrWebHostName As String _
                                        , ByVal intResolutionStepTranId As Integer _
                                        , ByVal intPriority As Integer _
                                        , ByVal strAgainstEmployee As String) 'Gajanan [27-June-2019] -- ADD strAgainstEmployee



        Dim dsList As New DataSet
        Try
            Dim StrMessage As New System.Text.StringBuilder

            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("Dear <b>" & strRaisedEmployee & "</b></span></p>")


            'Gajanan [27-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
            'StrMessage.Append(Language.getMessage(mstrModuleName, 10, "Dear") & " <b>" & getTitleCase(strRaisedEmployee) & "</b></span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 10, "Dear") & " " & getTitleCase(strRaisedEmployee) & "</span></p>")
            'Gajanan [27-June-2019] -- End
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language

            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " & Language.getMessage(mstrModuleName, 8, "This is to inform you that, Resolution information has been added to grievance raised by you") & " <b>" & _



            'Gajanan [3-July-2019] -- Start      

            'Gajanan [27-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes


            'StrMessage.Append(Language.getMessage(mstrModuleName, 8, "This is to inform you that, Resolution information has been added to grievance raised by you") & " " & _
            '                  Language.getMessage(mstrModuleName, 3, "with reference number") & " <b>" & strGrievanceRefNo & "</b>. " & _
            '                  Language.getMessage(mstrModuleName, 4, "Please click below link in order to provide response.") & "</span></p>")

            'StrMessage.Append(Language.getMessage(mstrModuleName, 8, "This is to inform you that, Resolution information has been added to grievance raised by you") & " " & strAgainstEmployee & " " & _
            '                    Language.getMessage(mstrModuleName, 3, "with reference number") & " <b>" & strGrievanceRefNo & "</b>. " & _
            '                    Language.getMessage(mstrModuleName, 4, "Please click below link in order to provide response.") & "</span></p>")



            StrMessage.Append(Language.getMessage(mstrModuleName, 8, "This is to inform you that, Resolution information has been added to grievance raised by you") & " <b>" & getTitleCase(strAgainstEmployee) & "</b> " & _
                               Language.getMessage(mstrModuleName, 3, "with reference number") & " <b>" & strGrievanceRefNo & "</b> " & _
                                Language.getMessage(mstrModuleName, 4, "Please click below link in order to provide response.") & "</span></p>")

            'Gajanan [27-June-2019] -- End
            'Gajanan [3-July-2019] -- End      


            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            Dim strLink As String = ""

            'Gajanan [27-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
            If strSelfServiceURL.Last() = "/" Then
                strSelfServiceURL = strSelfServiceURL.Remove(strSelfServiceURL.Length - 1, 1)
            End If
            'Gajanan [27-June-2019] -- End

            strLink = strSelfServiceURL & "/Grievance/AgreeDisagreeResponse.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(xCompanyUnkid.ToString & "|" & xEmpUnkid.ToString() & "|" & intGrievanceUnkid.ToString() & "|" & intResolutionStepTranId.ToString() & "|" & intPriority.ToString()))
            StrMessage.Append(strLink)
            StrMessage.Append("</span></p>")
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            Dim objMail As New clsSendMail
            objMail._Subject = Language.getMessage(mstrModuleName, 9, "Notification for Resolution Step Added")
            objMail._Message = StrMessage.ToString()
            objMail._ToEmail = strEmailAddress
            objMail._Form_Name = mstrFormName
            objMail._WebClientIP = mstrWebClientIP
            objMail._WebHostName = mstrWebHostName
            objMail._LogEmployeeUnkid = 0
            objMail._OperationModeId = eMode
            objMail._UserUnkid = iUserId
            objMail._SenderAddress = strSenderAddress
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.GRIEVANCE_MGT
            objMail.SendMail(xCompanyUnkid)
            objMail = Nothing

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendNotificationToEmployee; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub


    'Gajanan [10-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Changes        
    Public Sub SendNotificationToEmployeeOnRaisedGrievance(ByVal dtr() As DataRow, _
                                          ByVal strFormName As String, _
                                          ByVal strEmailAddress As String, _
                                          ByVal eMode As enLogin_Mode, _
                                          ByVal strSenderAddress As String, _
                                          ByVal strAgainstEmployee As String, _
                                          ByVal strGrievanceRefNo As String, _
                                          ByVal intGrievanceId As Integer, _
                                          ByVal strRaisedEmployee As String, _
                                          ByVal intCompanyId As Integer, _
                                          ByVal iLoginEmployeeId As Integer, _
                                          ByVal mstrWebClientIP As String, _
                                          ByVal mstrWebHostName As String, _
                                          ByVal mstrGrievanceType As String)

        'Gajanan [23-SEP-2019] -- Add [mstrGrievanceType] 


        Dim dsList As New DataSet
        Try
            For index As Integer = 0 To dtr.Length - 1
                If dtr(index)("email").ToString().Length <= 0 Then Continue For
                Dim StrMessage As New System.Text.StringBuilder
                StrMessage.Append("<HTML><BODY>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")



                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes

                'StrMessage.Append(Language.getMessage(mstrModuleName, 10, "Dear") & " <b>" & getTitleCase(strRaisedEmployee) & "</b></span></p>")
                StrMessage.Append(Language.getMessage(mstrModuleName, 10, "Dear") & " " & getTitleCase(strRaisedEmployee) & "</span></p>")

                'Gajanan [27-June-2019] -- End

                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")


                'Gajanan [23-SEP-2019] -- Start    

                'StrMessage.Append(Language.getMessage(mstrModuleName, 15, "This is to inform you that your grievance against") & " <b>" & _
                '                    strAgainstEmployee & "</b> " & Language.getMessage(mstrModuleName, 3, "with reference number") & " <b>" & strGrievanceRefNo & "</b> " & _
                '                    Language.getMessage(mstrModuleName, 16, "has been successfully submitted to") & " <b>" & getTitleCase(dtr(index)("name")) & "</b> " & "</span></p>")


                StrMessage.Append(Language.getMessage(mstrModuleName, 22, "This is to inform you that your grievance") & " (" & mstrGrievanceType & ") " & Language.getMessage(mstrModuleName, 21, "against") & " <b>" & _
                                    strAgainstEmployee & "</b> " & Language.getMessage(mstrModuleName, 3, "with reference number") & " <b>" & strGrievanceRefNo & "</b> " & _
                                    Language.getMessage(mstrModuleName, 16, "has been successfully submitted to") & " <b>" & getTitleCase(dtr(index)("name")) & "</b> " & "</span></p>")
                'Gajanan [23-SEP-2019] -- End


                'Hemant (29 May 2019) -- End                
                'Gajanan [27-Mar-2019] -- End

                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                StrMessage.Append("</span></p>")
                StrMessage.Append("</BODY></HTML>")

                Dim objMail As New clsSendMail
                objMail._Subject = Language.getMessage(mstrModuleName, 17, "Grievance Submission")
                objMail._Message = StrMessage.ToString()
                objMail._ToEmail = strEmailAddress
                objMail._Form_Name = mstrFormName
                objMail._WebClientIP = mstrWebClientIP
                objMail._WebHostName = mstrWebHostName
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = eMode
                objMail._UserUnkid = 0
                objMail._SenderAddress = strSenderAddress
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.GRIEVANCE_MGT
                objMail.SendMail(intCompanyId)
                objMail = Nothing
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendNotificationToEmployeeOnRaisedGrievance; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'Gajanan [10-June-2019] -- End


    'Gajanan [10-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Changes        
    Public Sub SendNotificationToEmployeeAfterDisagreeFinalResolution(ByVal strFormName As String, _
                                          ByVal strEmailAddress As String, _
                                          ByVal eMode As enLogin_Mode, _
                                          ByVal strSenderAddress As String, _
                                          ByVal strAgainstEmployee As String, _
                                          ByVal strGrievanceRefNo As String, _
                                          ByVal intGrievanceId As Integer, _
                                          ByVal strRaisedEmployee As String, _
                                          ByVal intCompanyId As Integer, _
                                          ByVal iLoginEmployeeId As Integer, _
                                          ByVal mstrWebClientIP As String, _
                                          ByVal mstrWebHostName As String)




        Dim dsList As New DataSet
        Try
            Dim StrMessage As New System.Text.StringBuilder
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")



            StrMessage.Append(Language.getMessage(mstrModuleName, 10, "Dear") & " " & getTitleCase(strRaisedEmployee) & "</span></p>")

            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            StrMessage.Append(Language.getMessage(mstrModuleName, 18, "Your grievance resolution has reached a final stage, if you are dissatisfied with the appeal outcome, please contact employee relations office for further guidance."))

            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            Dim objMail As New clsSendMail
            objMail._Subject = Language.getMessage(mstrModuleName, 19, "Notification For Grievance Disagreement")
            objMail._Message = StrMessage.ToString()
            objMail._ToEmail = strEmailAddress
            objMail._Form_Name = mstrFormName
            objMail._WebClientIP = mstrWebClientIP
            objMail._WebHostName = mstrWebHostName
            objMail._LogEmployeeUnkid = iLoginEmployeeId
            objMail._OperationModeId = eMode
            objMail._UserUnkid = 0
            objMail._SenderAddress = strSenderAddress
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.GRIEVANCE_MGT
            objMail.SendMail(intCompanyId)
            objMail = Nothing

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendNotificationToEmployeeAfterDisagreeFinalResolution; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'Gajanan [10-June-2019] -- End


    'Gajanan [10-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Changes        

    'Public Function GetEmployeeByApprover(ByVal enApprSetting As enGrievanceApproval, _
    '                                          ByVal strList As String, _
    '                                          ByVal intMappedUserId As Integer, _
    '                                          ByVal strDatabaseName As String, _
    '                                          ByVal intCompanyId As Integer, _
    '                                          ByVal intYearId As Integer, _
    '                                          ByVal strUserAccessMode As String, _
    '                                          ByVal strEmpAsOnDate As String, _
    '                                          ByVal mblnAddSelect As Boolean, _
    '                                          Optional ByVal blnOnlyGrievanceAppliedEmp As Boolean = False) As DataSet
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception = Nothing
    '    Dim objDataoperation As New clsDataOperation
    '    Dim strSelect As String = String.Empty
    '    Try
    '        Dim strCurrUsr, strActiveUsr As String
    '        strCurrUsr = "" : strActiveUsr = ""
    '        If enApprSetting = enGrievanceApproval.UserAcess Then
    '            StrQ = "SELECT ISNULL(STUFF((SELECT DISTINCT ',' + CAST(mapuserunkid AS NVARCHAR(10)) FROM greapprover_master WHERE approvalsettingid = " & enApprSetting & " AND isactive = 1 FOR XML PATH('')), 1, 1, ''),'0') AS ivalue "
    '            dsList = objDataoperation.ExecQuery(StrQ, "List")
    '            If objDataoperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
    '                Throw exForce
    '            End If
    '            If dsList.Tables("List").Rows.Count > 0 Then
    '                strCurrUsr = dsList.Tables("List").Rows(0)("ivalue")
    '                Dim objConfig As New clsConfigOptions
    '                objConfig.GetActiveUserList_CSV(strCurrUsr, strActiveUsr)
    '                objConfig = Nothing
    '            End If
    '        End If

    '        If mblnAddSelect Then
    '            'S.SANDEEP |30-MAY-2019| -- START
    '            'ISSUE/ENHANCEMENT : [Grievance UAT]
    '            'strSelect = "SELECT ' ' AS employeecode, ' ' + @Select AS employeename, 0 AS employeeunkid, ' ' + @Select AS EmpCodeName, 0 AS visible,0 as grievancemasterunkid UNION ALL "
    '            strSelect = "SELECT ' ' AS employeecode, ' ' + @Select AS employeename, 0 AS employeeunkid, ' ' + @Select AS EmpCodeName, 0 AS visible,0 as grievancemasterunkid, 0 AS approvermasterunkid UNION ALL "
    '            'S.SANDEEP |30-MAY-2019| -- END
    '            objDataoperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "  Select"))
    '        End If

    '        Dim xDateJoinQry, xDataFilterQry As String
    '        xDateJoinQry = "" : xDataFilterQry = ""
    '        Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, eZeeDate.convertDate(strEmpAsOnDate), eZeeDate.convertDate(strEmpAsOnDate), True, False, strDatabaseName, "EM")

    '        Select Case enApprSetting
    '            Case enGrievanceApproval.ApproverEmpMapping

    '                StrQ &= "SELECT DISTINCT " & _
    '                        "    EM.employeecode AS employeecode " & _
    '                        "   ,ISNULL(EM.firstname,'')+' '+ISNULL(EM.othername,'')+' '+ISNULL(EM.surname,'') AS employeename " & _
    '                        "   ,EM.employeeunkid As employeeunkid " & _
    '                        "   ,ISNULL(EM.employeecode,'') + ' - ' + ISNULL(EM.firstname,'') + ' ' + ISNULL(EM.othername,'') + ' ' + ISNULL(EM.surname,'') AS EmpCodeName " & _
    '                        "   ,CASE WHEN GAM.isexternal = 0 AND GGM.againstemployeeunkid = GAM.approverempid THEN 0 ELSE 1 END AS visible " & _
    '                        "   ,GGM.grievancemasterunkid " & _
    '                        "   ,GAM.approvermasterunkid " & _
    '                        "FROM greapprover_tran AS GAT " & _
    '                        "   JOIN hremployee_master EM ON EM.employeeunkid = GAT.employeeunkid " & _
    '                        "   JOIN greapprover_master GAM ON GAM.approvermasterunkid = GAT.approvermasterunkid "


    '                'Gajanan [10-June-2019] -- Start      
    '                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
    '                '"   JOIN gregrievance_master AS GGM ON EM.employeeunkid = GGM.fromemployeeunkid AND GAM.approvalsettingid = GGM.approvalSettingid "
    '                StrQ &= "   JOIN gregrievance_master AS GGM ON EM.employeeunkid = GGM.againstemployeeunkid AND GAM.approvalsettingid = GGM.approvalSettingid "
    '                'Gajanan [10-June-2019] -- End

    '                If xDateJoinQry.Trim.Length > 0 Then
    '                    StrQ &= xDateJoinQry
    '                End If



    '                'Gajanan [10-June-2019] -- Start      
    '                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
    '                StrQ &= "JOIN " & _
    '                        "( " & _
    '                        "SELECT " & _
    '                             "gregrievance_master.againstemployeeunkid " & _
    '                             ",gregrievance_master.grievancemasterunkid " & _
    '                             ",CASE WHEN ISNULL(greresolution_step_tran.resolutionsteptranunkid,0) <=0 and ISNULL(greinitiater_response_tran.initiatortranunkid,0) <=0  THEN 1 " & _
    '                             "WHEN ISNULL(greresolution_step_tran.resolutionsteptranunkid,0) > 0 and ISNULL(greinitiater_response_tran.initiatortranunkid,0) > 0  THEN 1 " & _
    '                             "ELSE 0 END AS visible " & _
    '                        "FROM gregrievance_master " & _
    '                        "LEFT JOIN greresolution_step_tran " & _
    '                             "ON gregrievance_master.grievancemasterunkid = greresolution_step_tran.grievancemasterunkid and  greresolution_step_tran.isvoid = 0 " & _
    '                        "LEFT JOIN greinitiater_response_tran " & _
    '                             "ON gregrievance_master.grievancemasterunkid = greinitiater_response_tran.grievancemasterunkid and  greinitiater_response_tran.isvoid = 0 " & _
    '                        "WHERE gregrievance_master.isvoid = 0 " & _
    '                        ") as e on EM.employeeunkid = EM.employeeunkid AND GGM.grievancemasterunkid = e.grievancemasterunkid "
    '                'Gajanan [10-June-2019] -- End



    '                StrQ &= "WHERE GAM.isvoid = 0 and GGM.isprocessed = 0 AND GAT.isvoid = 0 AND GAM.mapuserunkid = @mapuserunkid AND GGM.approvalSettingid = @approvalSettingid AND GGM.isvoid = 0 " & _
    '                        " AND (CASE WHEN GAM.isexternal = 0 AND GGM.againstemployeeunkid = GAM.approverempid THEN 0 ELSE 1 END) = 1 "

    '                If xDataFilterQry.Trim.Length > 0 Then
    '                    StrQ &= xDataFilterQry
    '                End If

    '                StrQ = strSelect & " " & StrQ

    '                StrQ &= " AND e.visible = 1 "

    '                objDataoperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMappedUserId)
    '                objDataoperation.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, enApprSetting)

    '                dsList = objDataoperation.ExecQuery(StrQ, "List")

    '                If objDataoperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
    '                    Throw exForce
    '                End If

    '            Case enGrievanceApproval.UserAcess
    '                Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
    '                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmpAsOnDate).Date, True, strDatabaseName, intMappedUserId, intCompanyId, intYearId, strUserAccessMode, "EM")

    '                If strActiveUsr.Trim.Length > 0 Then
    '                    'S.SANDEEP |30-MAY-2019| -- START
    '                    'ISSUE/ENHANCEMENT : [Grievance UAT]
    '                    'StrQ = "DECLARE @table as table (empid int, coid int, usrid int) " & _
    '                    '        "INSERT INTO @table(empid,coid,usrid) " & _
    '                    '        "SELECT " & _
    '                    '        "     UM.employeeunkid " & _
    '                    '        "    ,UM.companyunkid " & _
    '                    '        "    ,GAM.mapuserunkid " & _
    '                    '        "FROM greapprover_master AS GAM " & _
    '                    '        " JOIN hrmsConfiguration..cfuser_master AS UM ON UM.userunkid = GAM.mapuserunkid " & _
    '                    '        "WHERE GAM.isactive = 1 AND GAM.isvoid = 0 AND GAM.approvalsettingid = 1 " & _
    '                    '        "AND GAM.mapuserunkid IN (" & strActiveUsr & ") "

    '                    StrQ = "DECLARE @table as table (empid int, coid int, usrid int, approvermasterunkid int) " & _
    '                            "INSERT INTO @table(empid,coid,usrid) " & _
    '                            "SELECT " & _
    '                            "     UM.employeeunkid " & _
    '                            "    ,UM.companyunkid " & _
    '                            "    ,GAM.mapuserunkid " & _
    '                            "    ,GAM.approvermasterunkid " & _
    '                            "FROM greapprover_master AS GAM " & _
    '                            " JOIN hrmsConfiguration..cfuser_master AS UM ON UM.userunkid = GAM.mapuserunkid " & _
    '                            "WHERE GAM.isactive = 1 AND GAM.isvoid = 0 AND GAM.approvalsettingid = 1 " & _
    '                            "AND GAM.mapuserunkid IN (" & strActiveUsr & ") "
    '                    'S.SANDEEP |30-MAY-2019| -- END

    '                End If


    '                StrQ &= strSelect & " " & "SELECT DISTINCT " & _
    '                        "   EM.employeecode AS employeecode " & _
    '                        ",  ISNULL(EM.firstname,'')+' '+ISNULL(EM.othername,'')+' '+ISNULL(EM.surname,'') AS employeename " & _
    '                        ",  EM.employeeunkid As employeeunkid " & _
    '                        ",  ISNULL(EM.employeecode,'') + ' - ' + ISNULL(EM.firstname,'') + ' ' + ISNULL(EM.othername,'') + ' ' + ISNULL(EM.surname,'') AS EmpCodeName "
    '                If strActiveUsr.Trim.Length > 0 Then
    '                    StrQ &= ", CASE WHEN ([@table].empid = GGM.againstemployeeunkid AND [@table].coid = EM.companyunkid) THEN 0 ELSE 1 END AS visible "
    '                Else
    '                    StrQ &= ",  1 AS visible "
    '                End If

    '                'S.SANDEEP |30-MAY-2019| -- START
    '                'ISSUE/ENHANCEMENT : [Grievance UAT]
    '                'StrQ &= ", GGM.grievancemasterunkid " & _
    '                '        "FROM hremployee_master AS EM " & _
    '                '        "  JOIN gregrievance_master AS GGM ON EM.employeeunkid = GGM.fromemployeeunkid "

    '                StrQ &= ", GGM.grievancemasterunkid " & _
    '                        ", [@table].approvermasterunkid " & _
    '                        "FROM hremployee_master AS EM " & _
    '                        "  JOIN gregrievance_master AS GGM ON EM.employeeunkid = GGM.fromemployeeunkid "
    '                'S.SANDEEP |30-MAY-2019| -- END

    '                If strActiveUsr.Trim.Length > 0 Then
    '                    StrQ &= " LEFT JOIN @table ON empid = GGM.againstemployeeunkid AND [@table].usrid  = '" & intMappedUserId & "' "
    '                End If
    '                If xUACQry.Trim.Length > 0 Then
    '                    StrQ &= xUACQry
    '                End If

    '                If xDateJoinQry.Trim.Length > 0 Then
    '                    StrQ &= xDateJoinQry
    '                End If

    '                StrQ &= "WHERE 1 = 1 AND GGM.isvoid = 0 AND GGM.approvalSettingid = @approvalSettingid "
    '                If strActiveUsr.Trim.Length > 0 Then
    '                    StrQ &= " AND (CASE WHEN ([@table].empid = GGM.againstemployeeunkid AND [@table].coid = EM.companyunkid) THEN 0 ELSE 1 END) = 1 "
    '                Else
    '                    StrQ &= " AND visible = 1 "
    '                End If


    '                If xDataFilterQry.Trim.Length > 0 Then
    '                    StrQ &= xDataFilterQry
    '                End If

    '                objDataoperation.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, enApprSetting)

    '                dsList = objDataoperation.ExecQuery(StrQ, "List")

    '                If objDataoperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
    '                    Throw exForce
    '                End If
    '        End Select
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeByApprover; Module Name: " & mstrModuleName)
    '    Finally
    '        objDataoperation = Nothing
    '    End Try
    '    Return dsList
    'End Function


    Public Function GetEmployeeByApprover(ByVal enApprSetting As enGrievanceApproval, _
                                          ByVal strList As String, _
                                          ByVal intMappedUserId As Integer, _
                                          ByVal strDatabaseName As String, _
                                          ByVal intCompanyId As Integer, _
                                          ByVal intYearId As Integer, _
                                          ByVal strUserAccessMode As String, _
                                          ByVal strEmpAsOnDate As String, _
                                          ByVal mblnAddSelect As Boolean, _
                                          Optional ByVal blnOnlyGrievanceAppliedEmp As Boolean = False) As DataSet
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Dim objDataoperation As New clsDataOperation
        Dim strSelect As String = String.Empty
        Try
            Dim strCurrUsr, strActiveUsr As String
            strCurrUsr = "" : strActiveUsr = ""
            If enApprSetting = enGrievanceApproval.UserAcess Then
                StrQ = "SELECT ISNULL(STUFF((SELECT DISTINCT ',' + CAST(mapuserunkid AS NVARCHAR(10)) FROM greapprover_master WHERE approvalsettingid = " & enApprSetting & " AND isactive = 1 FOR XML PATH('')), 1, 1, ''),'0') AS ivalue "
                dsList = objDataoperation.ExecQuery(StrQ, "List")
                If objDataoperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
                    Throw exForce
                End If
                If dsList.Tables("List").Rows.Count > 0 Then
                    strCurrUsr = dsList.Tables("List").Rows(0)("ivalue")
                    Dim objConfig As New clsConfigOptions
                    objConfig.GetActiveUserList_CSV(strCurrUsr, strActiveUsr)
                    objConfig = Nothing
                End If
            End If


            If mblnAddSelect Then
                strSelect = "SELECT ' ' AS employeecode, ' ' + @Select AS employeename, 0 AS employeeunkid, ' ' + @Select AS EmpCodeName, 0 AS visible,0 as grievancemasterunkid, 0 AS approvermasterunkid UNION ALL "
                objDataoperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Select"))
            End If

            Dim xDateJoinQry, xDataFilterQry As String
            xDateJoinQry = "" : xDataFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, eZeeDate.convertDate(strEmpAsOnDate), eZeeDate.convertDate(strEmpAsOnDate), True, False, strDatabaseName, "EM")

            Select Case enApprSetting

                'Gajanan [24-OCT-2019] -- Start   
                'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
                

                Case enGrievanceApproval.ReportingTo
                    If mblnAddSelect Then
                        strSelect = "SELECT ' ' AS employeecode ,' ' + @Select AS employeename ,0 AS employeeunkid ,' ' + @Select AS EmpCodeName ,0 AS priority ,0 AS grievancemasterunkid ,0 AS resolutionsteptranunkid ,0 AS initiatortranunkid ,0 AS statusunkid ,0 AS approvermasterunkid ,'' AS grievancerefno,0 as Resolutionissubmitted ,1 AS visible UNION ALL"
                        objDataoperation.ClearParameters()
                        objDataoperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Select"))
                    End If

                    'StrQ = "IF OBJECT_ID('tempdb..#GrievanceAgainstEmp') IS NOT NULL " & _
                    '              "DROP TABLE #GrievanceAgainstEmp " & _
                    '              "CREATE TABLE #GrievanceAgainstEmp ( " & _
                    '                   "EmpIndex INT identity(1, 1) " & _
                    '                   ",empid INT " & _
                    '                   ") " & _
                    '              "IF OBJECT_ID('tempdb..#Results') IS NOT NULL " & _
                    '                   "DROP TABLE #Results " & _
                    '              "CREATE TABLE #Results ( " & _
                    '                   "eid INT " & _
                    '                   ",rempid INT " & _
                    '                   ",empid INT " & _
                    '                   ",priority INT " & _
                    '                   ") " & _
                    '              " " & _
                    '              "DECLARE @REmpId AS INT " & _
                    '                   ",@NxEmpId AS INT " & _
                    '                   ",@Eid AS INT " & _
                    '                   ",@priority AS INT " & _
                    '              "SET @REmpId = 0 " & _
                    '              "SET @NxEmpId = 0 " & _
                    '              "SET @eid = 0 " & _
                    '              "SET @priority = 0 " & _
                    '              "INSERT INTO #GrievanceAgainstEmp " & _
                    '              "SELECT againstemployeeunkid " & _
                    '              "FROM ( " & _
                    '                   "SELECT againstemployeeunkid " & _
                    '                        ",ROW_NUMBER() OVER ( " & _
                    '                             "PARTITION BY againstemployeeunkid ORDER BY againstemployeeunkid " & _
                    '                             ") AS rno " & _
                    '                   "FROM gregrievance_master " & _
                    '                   "WHERE issubmitted = 1 " & _
                    '                        "AND isprocessed = 0 " & _
                    '                        "AND isvoid = 0 " & _
                    '                   ") AS b " & _
                    '              "WHERE b.rno = 1 " & _
                    '              "DECLARE @TotalRows INT = ( " & _
                    '                        "SELECT COUNT(1) " & _
                    '                        "FROM #GrievanceAgainstEmp " & _
                    '                        ") " & _
                    '                   ",@i INT = 1 " & _
                    '              "WHILE @i <= @TotalRows " & _
                    '              "BEGIN " & _
                    '                   "SELECT @NxEmpId = empid " & _
                    '                   "FROM #GrievanceAgainstEmp " & _
                    '                   "WHERE EmpIndex = @i " & _
                    '                   "SET @Eid = @NxEmpId " & _
                    '                   "WHILE @NxEmpId > 0 " & _
                    '                   "BEGIN " & _
                    '                        "SET @priority = @priority + 1 " & _
                    '                        "SET @REmpId = ISNULL(( " & _
                    '                                       "SELECT reporttoemployeeunkid " & _
                    '                                       "FROM hremployee_reportto " & _
                    '                                       "WHERE employeeunkid = @NxEmpId " & _
                    '                                            "AND ishierarchy = 1 " & _
                    '                                            "AND isvoid = 0 " & _
                    '                                       "), 0) " & _
                    '                        "IF (@NxEmpId = @REmpId) " & _
                    '                             "SET @REmpId = 0 " & _
                    '                        "INSERT INTO #Results ( " & _
                    '                             "eid " & _
                    '                             ",rempid " & _
                    '                             ",empid " & _
                    '                             ",priority " & _
                    '                             ") " & _
                    '                        "VALUES ( " & _
                    '                             "@NxEmpId " & _
                    '                             ",@REmpId " & _
                    '                             ",@Eid " & _
                    '                             ",@priority " & _
                    '                             ") " & _
                    '                        "IF (@REmpId = 0) " & _
                    '                        "BEGIN " & _
                    '                             "SET @i = @i + 1 " & _
                    '                             "SET @priority = 0 " & _
                    '                             "SELECT @NxEmpId = empid " & _
                    '                             "FROM #GrievanceAgainstEmp " & _
                    '                             "WHERE EmpIndex = @i " & _
                    '                             "BREAK; " & _
                    '                        "END " & _
                    '                        "ELSE " & _
                    '                             "SET @NxEmpId = @REmpId " & _
                    '                        "DECLARE @Rcnt AS INT " & _
                    '                        "SET @Rcnt = ( " & _
                    '                                  "SELECT COUNT(1) " & _
                    '                                  "FROM #Results " & _
                    '                                  "WHERE rempid = @NxEmpId " & _
                    '                                       "AND #Results.empid = @Eid " & _
                    '                                  ") " & _
                    '                        "IF (@Rcnt > 1) " & _
                    '                        "BEGIN " & _
                    '                             "SET @i = @i + 1 " & _
                    '                             "SELECT @NxEmpId = empid " & _
                    '                             "FROM #GrievanceAgainstEmp " & _
                    '                             "WHERE EmpIndex = @i " & _
                    '                             "SET @priority = 0 " & _
                    '                             "BREAK; " & _
                    '                        "END " & _
                    '                        "ELSE " & _
                    '                             "CONTINUE " & _
                    '                   "END " & _
                    '                "END "

                    StrQ = getGrievanceReportingToQuery(False, True)

                    StrQ &= strSelect

                    StrQ &= " SELECT " & _
                                 "* " & _
                            "FROM (SELECT " & _
                                      "hremployee_master.employeecode " & _
                                    ",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                                    ",againstemployeeunkid AS employeeunkid " & _
                                    ",ISNULL(hremployee_master.employeecode, '') + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpCodeName " & _
                                    ",#Results.priority " & _
                                    ",gregrievance_master.grievancemasterunkid " & _
                                    ",ISNULL(RST.resolutionsteptranunkid, 0) AS resolutionsteptranunkid " & _
                                    ",ISNULL(IST.initiatortranunkid, 0) AS initiatortranunkid " & _
                                    ",ISNULL(IST.statusunkid, 0) AS statusunkid " & _
                                    ",#Results.rempid " & _
                                    ",grievancerefno " & _
                                    ",ISNULL(RST.issubmitted, 0) AS Resolutionissubmitted " & _
                                    ",0 AS visible " & _
                                 "FROM #Results " & _
                                 "JOIN gregrievance_master " & _
                                      "ON gregrievance_master.againstemployeeunkid = #Results.empid AND gregrievance_master.grievancemasterunkid = #Results.grievanceId " & _
                                 "JOIN hremployee_master " & _
                                      "ON gregrievance_master.againstemployeeunkid = hremployee_master.employeeunkid " & _
                                 "LEFT JOIN (SELECT " & _
                                           "grievancemasterunkid AS rgrevid " & _
                                         ",priority AS rprioid " & _
                                         ",resolutionsteptranunkid " & _
                                         ",greresolution_step_tran.issubmitted " & _
                                      "FROM greresolution_step_tran " & _
                                      "LEFT JOIN #Results " & _
                                           "ON #Results.rempid = greresolution_step_tran.approverempid " & _
                                           "and #Results.grievanceId = greresolution_step_tran.grievancemasterunkid " & _
                                      "WHERE greresolution_step_tran.isvoid = 0 " & _
                                      "AND greresolution_step_tran.approvalsettingid = " & CInt(enGrievanceApproval.ReportingTo) & ") AS RST " & _
                                      "ON RST.rgrevid = gregrievance_master.grievancemasterunkid " & _
                                      "AND priority = RST.rprioid " & _
                                 "LEFT JOIN (SELECT " & _
                                           "greinitiater_response_tran.grievancemasterunkid AS igrevid " & _
                                         ",priority AS rprioid " & _
                                         ",greinitiater_response_tran.resolutionsteptranunkid " & _
                                         ",initiatortranunkid " & _
                                         ",statusunkid " & _
                                      "FROM greinitiater_response_tran " & _
                                      "LEFT JOIN greresolution_step_tran " & _
                                           "ON greresolution_step_tran.resolutionsteptranunkid = greinitiater_response_tran.resolutionsteptranunkid " & _
                                      "LEFT JOIN #Results " & _
                                           "ON #Results.rempid = greresolution_step_tran.approverempid " & _
                                           "and #Results.grievanceId = greresolution_step_tran.grievancemasterunkid " & _
                                      "WHERE greresolution_step_tran.isvoid = 0 " & _
                                      "AND greinitiater_response_tran.isvoid = 0 " & _
                                      "AND greresolution_step_tran.approvalSettingid = " & CInt(enGrievanceApproval.ReportingTo) & " " & _
                                      "AND greinitiater_response_tran.approvalSettingid = " & CInt(enGrievanceApproval.ReportingTo) & ") AS IST " & _
                                      "ON IST.igrevid = gregrievance_master.grievancemasterunkid " & _
                                      "AND IST.rprioid = priority " & _
                                 "WHERE " & _
                                 "#Results.rempid > 0 " & _
                                 "and gregrievance_master.approvalSettingid = " & CInt(enGrievanceApproval.ReportingTo) & " " & _
                                 "AND gregrievance_master.issubmitted = 1 " & _
                                 "AND gregrievance_master.isprocessed = 0) AS a " & _
                            "ORDER BY grievancemasterunkid, priority " & _
                            " " & _
                            "DROP TABLE #Results " & _
                            "DROP TABLE #GrievanceAgainstEmp "

                    dsList = objDataoperation.ExecQuery(StrQ, "List")

                    If objDataoperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                        Throw exForce
                    End If
                    'Gajanan [24-OCT-2019] -- End


                Case enGrievanceApproval.ApproverEmpMapping
                    If mblnAddSelect Then
                'Gajanan [24-June-2019] -- Start      
                        'strSelect = "SELECT ' ' AS employeecode ,' ' + '@Select' AS employeename ,0 AS employeeunkid ,' ' + '@Select' AS EmpCodeName ,0 AS priority ,0 AS grievancemasterunkid ,0 AS resolutionsteptranunkid ,0 AS initiatortranunkid ,0 AS statusunkid ,0 AS approvermasterunkid ,0 AS grievancerefno,0 as Resolutionissubmitted ,1 AS visible UNION ALL"
                        strSelect = "SELECT ' ' AS employeecode ,' ' + @Select AS employeename ,0 AS employeeunkid ,' ' + @Select AS EmpCodeName ,0 AS priority ,0 AS grievancemasterunkid ,0 AS resolutionsteptranunkid ,0 AS initiatortranunkid ,0 AS statusunkid ,0 AS approvermasterunkid ,'' AS grievancerefno,0 as Resolutionissubmitted ,1 AS visible UNION ALL"
                'Gajanan [24-June-2019] -- End
                        objDataoperation.ClearParameters()
                        objDataoperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Select"))
                    End If


                    StrQ = strSelect & " SELECT * from ( " & _
                                "SELECT " & _
                                        "hremployee_master.employeecode " & _
                                        ",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                                        ",againstemployeeunkid AS employeeunkid " & _
                                        ",ISNULL(hremployee_master.employeecode, '') + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpCodeName " & _
                                        ",greapproverlevel_master.priority " & _
                                        ",gregrievance_master.grievancemasterunkid " & _
                                        ",ISNULL(RST.resolutionsteptranunkid, 0) AS resolutionsteptranunkid " & _
                                        ",ISNULL(IST.initiatortranunkid, 0) AS initiatortranunkid " & _
                                        ",ISNULL(IST.statusunkid, 0) AS statusunkid " & _
                                        ",greapprover_master.approvermasterunkid " & _
                                        ",grievancerefno " & _
                                        ",ISNULL( RST.issubmitted,0) as Resolutionissubmitted " & _
                                        ",0 AS visible " & _
                                     "FROM greapprover_master " & _
                                     "JOIN greapprover_tran " & _
                                          "ON greapprover_master.approvermasterunkid = greapprover_tran.approvermasterunkid " & _
                                     "JOIN greapproverlevel_master " & _
                                          "ON greapprover_master.apprlevelunkid = greapproverlevel_master.apprlevelunkid " & _
                                     "JOIN gregrievance_master " & _
                                          "ON gregrievance_master.againstemployeeunkid = greapprover_tran.employeeunkid " & _
                                     "JOIN hremployee_master " & _
                                          "ON gregrievance_master.againstemployeeunkid = hremployee_master.employeeunkid " & _
                                     "LEFT JOIN (SELECT " & _
                                               "grievancemasterunkid AS rgrevid " & _
                                             ",priority AS rprioid " & _
                                             ",resolutionsteptranunkid " & _
                                             ",greresolution_step_tran.issubmitted " & _
                                          "FROM greresolution_step_tran " & _
                                          "LEFT JOIN greapproverlevel_master " & _
                                               "ON greapproverlevel_master.apprlevelunkid = greresolution_step_tran.apprlevelunkid " & _
                                          "WHERE isvoid = 0 " & _
                                          "AND isactive = 1 " & _
                                          "AND greapproverlevel_master.approvalsettingid = 2) AS RST " & _
                                          "ON RST.rgrevid = gregrievance_master.grievancemasterunkid " & _
                                          "AND priority = RST.rprioid " & _
                                     "LEFT JOIN (SELECT " & _
                                               "greinitiater_response_tran.grievancemasterunkid AS igrevid " & _
                                             ",priority AS rprioid " & _
                                             ",greinitiater_response_tran.resolutionsteptranunkid " & _
                                             ",initiatortranunkid " & _
                                             ",statusunkid " & _
                                          "FROM greinitiater_response_tran " & _
                                          "LEFT JOIN greresolution_step_tran " & _
                                               "ON greresolution_step_tran.resolutionsteptranunkid = greinitiater_response_tran.resolutionsteptranunkid " & _
                                          "LEFT JOIN greapproverlevel_master " & _
                                               "ON greapproverlevel_master.apprlevelunkid = greresolution_step_tran.apprlevelunkid " & _
                                          "WHERE greresolution_step_tran.isvoid = 0 " & _
                                          "AND greinitiater_response_tran.isvoid = 0 " & _
                                          "AND greapproverlevel_master.approvalSettingid = 2) AS IST " & _
                                          "ON IST.igrevid = gregrievance_master.grievancemasterunkid " & _
                                          "AND IST.rprioid = priority " & _
                                     "WHERE greapprover_master.approvalsettingid = 2 " & _
                                     "AND greapprover_master.isvoid = 0 " & _
                                     "AND greapprover_tran.isvoid = 0 " & _
                                     "AND gregrievance_master.approvalSettingid = 2 " & _
                                     "AND gregrievance_master.issubmitted = 1 " & _
                                     "AND gregrievance_master.isprocessed = 0 " & _
                                ") as a " & _
                        "ORDER BY grievancemasterunkid,priority "

                    dsList = objDataoperation.ExecQuery(StrQ, "List")

                    If objDataoperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                        Throw exForce
                    End If

                Case enGrievanceApproval.UserAcess
                    Dim xUACQry, xUACFiltrQry As String : xUACQry = "" : xUACFiltrQry = ""
                    Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, eZeeDate.convertDate(strEmpAsOnDate).Date, True, strDatabaseName, intMappedUserId, intCompanyId, intYearId, strUserAccessMode, "EM")

                    If strActiveUsr.Trim.Length > 0 Then
                        'S.SANDEEP |30-MAY-2019| -- START
                        'ISSUE/ENHANCEMENT : [Grievance UAT]
                        'StrQ = "DECLARE @table as table (empid int, coid int, usrid int) " & _
                        '        "INSERT INTO @table(empid,coid,usrid) " & _
                        '        "SELECT " & _
                        '        "     UM.employeeunkid " & _
                        '        "    ,UM.companyunkid " & _
                        '        "    ,GAM.mapuserunkid " & _
                        '        "FROM greapprover_master AS GAM " & _
                        '        " JOIN hrmsConfiguration..cfuser_master AS UM ON UM.userunkid = GAM.mapuserunkid " & _
                        '        "WHERE GAM.isactive = 1 AND GAM.isvoid = 0 AND GAM.approvalsettingid = 1 " & _
                        '        "AND GAM.mapuserunkid IN (" & strActiveUsr & ") "

                        StrQ = "DECLARE @table as table (empid int, coid int, usrid int, approvermasterunkid int) " & _
                                "INSERT INTO @table(empid,coid,usrid) " & _
                                "SELECT " & _
                                "     UM.employeeunkid " & _
                                "    ,UM.companyunkid " & _
                                "    ,GAM.mapuserunkid " & _
                                "    ,GAM.approvermasterunkid " & _
                                "FROM greapprover_master AS GAM " & _
                                " JOIN hrmsConfiguration..cfuser_master AS UM ON UM.userunkid = GAM.mapuserunkid " & _
                                "WHERE GAM.isactive = 1 AND GAM.isvoid = 0 AND GAM.approvalsettingid = 1 " & _
                                "AND GAM.mapuserunkid IN (" & strActiveUsr & ") "
                        'S.SANDEEP |30-MAY-2019| -- END
                        
                    End If


                    StrQ &= strSelect & " " & "SELECT DISTINCT " & _
                            "   EM.employeecode AS employeecode " & _
                            ",  ISNULL(EM.firstname,'')+' '+ISNULL(EM.othername,'')+' '+ISNULL(EM.surname,'') AS employeename " & _
                            ",  EM.employeeunkid As employeeunkid " & _
                            ",  ISNULL(EM.employeecode,'') + ' - ' + ISNULL(EM.firstname,'') + ' ' + ISNULL(EM.othername,'') + ' ' + ISNULL(EM.surname,'') AS EmpCodeName "
                    If strActiveUsr.Trim.Length > 0 Then
                        StrQ &= ", CASE WHEN ([@table].empid = GGM.againstemployeeunkid AND [@table].coid = EM.companyunkid) THEN 0 ELSE 1 END AS visible "
                    Else
                        StrQ &= ",  1 AS visible "
                    End If

                    'S.SANDEEP |30-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : [Grievance UAT]
                    'StrQ &= ", GGM.grievancemasterunkid " & _
                    '        "FROM hremployee_master AS EM " & _
                    '        "  JOIN gregrievance_master AS GGM ON EM.employeeunkid = GGM.fromemployeeunkid "

                    StrQ &= ", GGM.grievancemasterunkid " & _
                            ", [@table].approvermasterunkid " & _
                            "FROM hremployee_master AS EM " & _
                            "  JOIN gregrievance_master AS GGM ON EM.employeeunkid = GGM.fromemployeeunkid "
                    'S.SANDEEP |30-MAY-2019| -- END
                    
                    If strActiveUsr.Trim.Length > 0 Then
                        StrQ &= " LEFT JOIN @table ON empid = GGM.againstemployeeunkid AND [@table].usrid  = '" & intMappedUserId & "' "
                    End If
                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= xUACQry
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry
                    End If

                    StrQ &= "WHERE 1 = 1 AND GGM.isvoid = 0 AND GGM.approvalSettingid = @approvalSettingid "
                    If strActiveUsr.Trim.Length > 0 Then
                        StrQ &= " AND (CASE WHEN ([@table].empid = GGM.againstemployeeunkid AND [@table].coid = EM.companyunkid) THEN 0 ELSE 1 END) = 1 "
                    Else
                        StrQ &= " AND visible = 1 "
                    End If


                    If xDataFilterQry.Trim.Length > 0 Then
                        StrQ &= xDataFilterQry
                    End If

                    objDataoperation.AddParameter("@approvalSettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, enApprSetting)

                    dsList = objDataoperation.ExecQuery(StrQ, "List")

                    If objDataoperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                        Throw exForce
                    End If
            End Select
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeByApprover; Module Name: " & mstrModuleName)
        Finally
            objDataoperation = Nothing
        End Try
        Return dsList
    End Function


    'Gajanan [10-June-2019] -- End


    'Gajanan [24-OCT-2019] -- Start   
    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
    Public Function GetReportingApproverList(ByVal strTableName As String _
                          , ByVal xDatabaseName As String _
                          , ByVal xUserUnkid As Integer _
                          , ByVal xYearUnkid As Integer _
                          , ByVal xCompanyUnkid As Integer _
                          , ByVal strEmployeeAsOnDate As String _
                          , ByVal xUserModeSetting As String _
                          , ByVal xOnlyApproved As Boolean _
                          , ByVal xIncludeIn_ActiveEmployee As Boolean _
                          , Optional ByVal strEmpIds As String = "" _
                          , Optional ByVal objDOperation As clsDataOperation = Nothing _
                          , Optional ByVal mstrFilter As String = "" _
                          , Optional ByVal blnForApproval As Boolean = False _
                          , Optional ByVal blnAddSelect As Boolean = False _
                          , Optional ByVal mintApprovalSetting As Integer = -1 _
                          , Optional ByVal xmintMapUserid As Integer = -1 _
                          , Optional ByVal xmblnApplyIsProcessFilter As Boolean = True) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim Flag As Boolean = False

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If
        objDataOperation.ClearParameters()

        Try
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQDtFilters As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(strEmployeeAsOnDate), eZeeDate.convertDate(strEmployeeAsOnDate), , , xDatabaseName, "#empappr#")

            If strEmpIds.Trim.Length > 0 Then

                Dim objReportingTo As New clsReportingToEmployee

                Flag = objReportingTo.IsGrievanceEmployeeReportingToWrong(strEmpIds)

                If Flag = True Then
                    mstrMessage = Language.getMessage(mstrModuleName, 23, "Sorry Reporting To Mapping Is Incorrect It's Create Recursion.")
                    Return Nothing
                End If

                'strQ = "IF OBJECT_ID('tempdb..#Results') IS NOT NULL " & _
                '       "DROP TABLE #Results " & _
                '       "CREATE TABLE #Results ( " & _
                '            "eid INT " & _
                '            ",rempid INT " & _
                '            ",empid INT " & _
                '            ",priority INT " & _
                '            ") " & _
                '       "DECLARE @REmpId AS INT " & _
                '            ",@NxEmpId AS INT " & _
                '            ",@Eid AS INT " & _
                '            ",@priority AS INT " & _
                '       "SET @REmpId = 0 " & _
                '       "SET @NxEmpId = " & strEmpIds & " " & _
                '       "SET @eid = " & strEmpIds & " " & _
                '       "SET @priority = 0 " & _
                '       "WHILE (@NxEmpId) > 0 " & _
                '       "BEGIN " & _
                '            "SET @priority = @priority + 1 " & _
                '            "SET @REmpId = ISNULL(( " & _
                '                           "SELECT reporttoemployeeunkid " & _
                '                           "FROM hremployee_reportto " & _
                '                           "WHERE employeeunkid = @NxEmpId " & _
                '                                "AND ishierarchy = 1 " & _
                '                                "AND isvoid = 0 " & _
                '                           "), 0) " & _
                '            "IF (@NxEmpId = @REmpId) " & _
                '                 "SET @REmpId = 0 " & _
                '            "INSERT INTO #Results ( " & _
                '                 "eid " & _
                '                 ",rempid " & _
                '                 ",empid " & _
                '                 ",priority " & _
                '                 ") " & _
                '            "VALUES ( " & _
                '                 "@NxEmpId " & _
                '                 ",@REmpId " & _
                '                 ",@eid " & _
                '                 ",@priority " & _
                '                 ") " & _
                '            "SET @NxEmpId = @REmpId " & _
                '            "DECLARE @Rcnt AS INT " & _
                '            "SET @Rcnt = ( " & _
                '                      "SELECT COUNT(1) " & _
                '                      "FROM #Results " & _
                '                      "WHERE rempid = @NxEmpId " & _
                '                      ") " & _
                '            "IF (@Rcnt > 1) " & _
                '                 "BREAK " & _
                '            "ELSE " & _
                '                 "CONTINUE " & _
                '       "END "

                strQ = getGrievanceReportingToQuery(True, False)

                strQ = (strQ.Replace("#strEmpIds#", strEmpIds))


            Else
                'strQ = "IF OBJECT_ID('tempdb..#GrievanceAgainstEmp') IS NOT NULL " & _
                '       "DROP TABLE #GrievanceAgainstEmp " & _
                '       "CREATE TABLE #GrievanceAgainstEmp ( " & _
                '            "EmpIndex INT identity(1, 1) " & _
                '            ",empid INT " & _
                '            ") " & _
                '       "IF OBJECT_ID('tempdb..#Results') IS NOT NULL " & _
                '            "DROP TABLE #Results " & _
                '       "CREATE TABLE #Results ( " & _
                '            "eid INT " & _
                '            ",rempid INT " & _
                '            ",empid INT " & _
                '            ",priority INT " & _
                '            ") " & _
                '       " " & _
                '       "DECLARE @REmpId AS INT " & _
                '            ",@NxEmpId AS INT " & _
                '            ",@Eid AS INT " & _
                '            ",@priority AS INT " & _
                '       "SET @REmpId = 0 " & _
                '       "SET @NxEmpId = 0 " & _
                '       "SET @eid = 0 " & _
                '       "SET @priority = 0 " & _
                '       "INSERT INTO #GrievanceAgainstEmp " & _
                '       "SELECT againstemployeeunkid " & _
                '       "FROM ( " & _
                '            "SELECT againstemployeeunkid " & _
                '                 ",ROW_NUMBER() OVER ( " & _
                '                      "PARTITION BY againstemployeeunkid ORDER BY againstemployeeunkid " & _
                '                      ") AS rno " & _
                '            "FROM gregrievance_master " & _
                '            "WHERE issubmitted = 1 " & _
                '                 "AND isprocessed = 0 " & _
                '                 "AND isvoid = 0 " & _
                '            ") AS b " & _
                '       "WHERE b.rno = 1 " & _
                '       "DECLARE @TotalRows INT = ( " & _
                '                 "SELECT COUNT(1) " & _
                '                 "FROM #GrievanceAgainstEmp " & _
                '                 ") " & _
                '            ",@i INT = 1 " & _
                '       "WHILE @i <= @TotalRows " & _
                '       "BEGIN " & _
                '            "SELECT @NxEmpId = empid " & _
                '            "FROM #GrievanceAgainstEmp " & _
                '            "WHERE EmpIndex = @i " & _
                '            "SET @Eid = @NxEmpId " & _
                '            "WHILE @NxEmpId > 0 " & _
                '            "BEGIN " & _
                '                 "SET @priority = @priority + 1 " & _
                '                 "SET @REmpId = ISNULL(( " & _
                '                                "SELECT reporttoemployeeunkid " & _
                '                                "FROM hremployee_reportto " & _
                '                                "WHERE employeeunkid = @NxEmpId " & _
                '                                     "AND ishierarchy = 1 " & _
                '                                     "AND isvoid = 0 " & _
                '                                "), 0) " & _
                '                 "IF (@NxEmpId = @REmpId) " & _
                '                      "SET @REmpId = 0 " & _
                '                 "INSERT INTO #Results ( " & _
                '                      "eid " & _
                '                      ",rempid " & _
                '                      ",empid " & _
                '                      ",priority " & _
                '                      ") " & _
                '                 "VALUES ( " & _
                '                      "@NxEmpId " & _
                '                      ",@REmpId " & _
                '                      ",@Eid " & _
                '                      ",@priority " & _
                '                      ") " & _
                '                 "IF (@REmpId = 0) " & _
                '                 "BEGIN " & _
                '                      "SET @i = @i + 1 " & _
                '                      "SET @priority = 0 " & _
                '                      "SELECT @NxEmpId = empid " & _
                '                      "FROM #GrievanceAgainstEmp " & _
                '                      "WHERE EmpIndex = @i " & _
                '                      "BREAK; " & _
                '                 "END " & _
                '                 "ELSE " & _
                '                      "SET @NxEmpId = @REmpId " & _
                '                 "DECLARE @Rcnt AS INT " & _
                '                 "SET @Rcnt = ( " & _
                '                           "SELECT COUNT(1) " & _
                '                           "FROM #Results " & _
                '                           "WHERE rempid = @NxEmpId " & _
                '                                "AND #Results.empid = @Eid " & _
                '                           ") " & _
                '                 "IF (@Rcnt > 1) " & _
                '                 "BEGIN " & _
                '                      "SET @i = @i + 1 " & _
                '                      "SELECT @NxEmpId = empid " & _
                '                      "FROM #GrievanceAgainstEmp " & _
                '                      "WHERE EmpIndex = @i " & _
                '                      "SET @priority = 0 " & _
                '                      "BREAK; " & _
                '                 "END " & _
                '                 "ELSE " & _
                '                      "CONTINUE " & _
                '            "END " & _
                '       "END "

                strQ = getGrievanceReportingToQuery(False, xmblnApplyIsProcessFilter)

            End If

            If blnAddSelect Then
                strQ &= "SELECT " & _
                            "  0 AS approvermasterunkid " & _
                            ", 0 AS apprlevelunkid " & _
                            ", '' AS levelname " & _
                            ", 0 AS priority " & _
                            ", 0 AS approverempid " & _
                            ", @Select as name" & _
                            ", '' as email " & _
                            ", 0 AS mapuserunkid " & _
                            ", 0 AS userunkid " & _
                            ", CAST(0 AS BIT) AS isexternal  " & _
                            ", '' ExAppr " & _
                            ", CAST(0 AS BIT) AS activestatus "

                If strEmpIds.Trim.Length <= 0 Then
                    strQ &= ",0 as grievanceId "
                End If
                strQ &= " UNION ALL "
            End If

            strQ &= "SELECT #Results.rempid AS approvermasterunkid " & _
                    ",0 AS apprlevelunkid " & _
                    ",'' AS levelname " & _
                    ",#Results.priority " & _
                    ",#Results.rempid AS approverempid " & _
                    ",ISNULL(empappr.firstname, '') + ' ' + ISNULL(empappr.surname, '') AS name " & _
                    ",ISNULL(empappr.email, '') AS email " & _
                    ",guser.userunkid AS mapuserunkid " & _
                    ",guser.userunkid " & _
                    ",0 AS isexternal " & _
                    ",'@NO' AS ExAppr " & _
                    ",#ACT_DATA# "

            If strEmpIds.Trim.Length <= 0 Then
                strQ &= ",#Results.grievanceId "
            End If

            strQ &= "FROM #Results " & _
                   "LEFT JOIN hremployee_master AS empappr ON empappr.employeeunkid = #Results.rempid " & _
                   "LEFT JOIN hrmsConfiguration..cfuser_master AS guser ON guser.employeeunkid = empappr.employeeunkid "

            StrFinalQurey = strQ
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            StrQCondition &= " WHERE 1 = 1 AND #Results.rempid > 0 AND guser.isactive = 1  "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQDtFilters &= xDateFilterQry & " "
                End If
            End If

            If mstrFilter.Trim.Length > 0 Then
                StrQCondition &= mstrFilter
            End If

            If xmintMapUserid > 0 Then
                StrQCondition &= "and guser.userunkid = " & xmintMapUserid & ""
            End If


            strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN hremployee_master AS empappr ON empappr.employeeunkid = #Results.rempid " & _
                                               "LEFT JOIN hrmsConfiguration..cfuser_master AS guser ON guser.employeeunkid = empappr.employeeunkid ")

            strQ = strQ.Replace("#ACT_DATA#", "CAST(CASE WHEN CONVERT(CHAR(8),#empappr#.appointeddate, 112) <= #EFDATE# " & _
                                              "       AND ISNULL(CONVERT(CHAR(8),TRM.LEAVING, 112),#EFDATE#) >= #EFDATE# " & _
                                              "       AND ISNULL(CONVERT(CHAR(8),RET.RETIRE, 112),#EFDATE#) >= #EFDATE# " & _
                                              "       AND ISNULL(CONVERT(CHAR(8),TRM.EOC, 112),#EFDATE#) >= #EFDATE# " & _
                                              "  THEN 1 ELSE 0 END AS BIT) AS activestatus ")
            strQ = strQ.Replace("#EFDATE#", "'" & strEmployeeAsOnDate & "' ")

            strQ &= StrQCondition
            strQ &= StrQDtFilters
            strQ = strQ.Replace("#ExAppr#", "0")
            strQ = strQ.Replace("#empappr#", "empappr")

            If strEmpIds.Trim.Length <= 0 AndAlso blnAddSelect = False Then
                strQ &= " ORDER BY #Results.grievanceId,#Results.empid,priority "
            End If

            If strEmpIds.Trim.Length <= 0 Then
                strQ &= " DROP TABLE #GrievanceAgainstEmp "
            End If




            strQ &= " DROP TABLE #Results "

            objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Yes")
            objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "No")
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Select"))
            objDataOperation.AddParameter("@approvalsetting", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovalSetting)
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetReportingApproverList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList

    End Function
    'Gajanan [24-OCT-2019] -- End

    'Gajanan [24-OCT-2019] -- Start   
    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
    Public Function getGrievanceReportingToQuery(ByVal mblnGetEmployeeWiseQyery As Boolean, ByVal mblngetIsprocessData As Boolean) As String
        Dim exForce As Exception
        Dim strQ As String = ""
        Try

            If mblnGetEmployeeWiseQyery = True Then
                strQ = "IF OBJECT_ID('tempdb..#Results') IS NOT NULL " & _
                           "DROP TABLE #Results " & _
                           "CREATE TABLE #Results ( " & _
                                "eid INT " & _
                                ",rempid INT " & _
                                ",empid INT " & _
                                ",priority INT " & _
                                ") " & _
                           "DECLARE @REmpId AS INT " & _
                                ",@NxEmpId AS INT " & _
                                ",@Eid AS INT " & _
                                ",@priority AS INT " & _
                           "SET @REmpId = 0 " & _
                           "SET @NxEmpId = #strEmpIds# " & _
                           "SET @eid =  #strEmpIds#  " & _
                           "SET @priority = 0 " & _
                           "WHILE (@NxEmpId) > 0 " & _
                           "BEGIN " & _
                                "SET @priority = @priority + 1 " & _
                                "SET @REmpId = ISNULL(( " & _
                                               "SELECT reporttoemployeeunkid " & _
                                               "FROM hremployee_reportto " & _
                                               "WHERE employeeunkid = @NxEmpId " & _
                                                    "AND ishierarchy = 1 " & _
                                                    "AND isvoid = 0 " & _
                                               "), 0) " & _
                                "IF (@NxEmpId = @REmpId) " & _
                                     "SET @REmpId = 0 " & _
                                "INSERT INTO #Results ( " & _
                                     "eid " & _
                                     ",rempid " & _
                                     ",empid " & _
                                     ",priority " & _
                                     ") " & _
                                "VALUES ( " & _
                                     "@NxEmpId " & _
                                     ",@REmpId " & _
                                     ",@eid " & _
                                     ",@priority " & _
                                     ") " & _
                                "SET @NxEmpId = @REmpId " & _
                                "DECLARE @Rcnt AS INT " & _
                                "SET @Rcnt = ( " & _
                                          "SELECT COUNT(1) " & _
                                          "FROM #Results " & _
                                          "WHERE rempid = @NxEmpId " & _
                                          ") " & _
                                "IF (@Rcnt > 1) " & _
                                     "BREAK " & _
                                "ELSE " & _
                                     "CONTINUE " & _
                           "END "
            Else
                strQ = "IF OBJECT_ID('tempdb..#GrievanceAgainstEmp') IS NOT NULL " & _
                            "DROP TABLE #GrievanceAgainstEmp " & _
                       "CREATE TABLE #GrievanceAgainstEmp ( " & _
                            "EmpIndex INT identity(1, 1) " & _
                            ",empid INT " & _
                            ",grievanceId INT " & _
                            ") " & _
                       "IF OBJECT_ID('tempdb..#Results') IS NOT NULL " & _
                            "DROP TABLE #Results " & _
                       "CREATE TABLE #Results ( " & _
                            "eid INT " & _
                            ",rempid INT " & _
                            ",empid INT " & _
                            ",priority INT " & _
                            ",grievanceId INT " & _
                            ") " & _
                       "DECLARE @REmpId AS INT " & _
                            ",@NxEmpId AS INT " & _
                            ",@Eid AS INT " & _
                            ",@priority AS INT " & _
                            ",@grievanceId AS INT " & _
                       "SET @REmpId = 0 " & _
                       "SET @NxEmpId = 0 " & _
                       "SET @eid = 0 " & _
                       "SET @priority = 0 " & _
                       "SET @grievanceId = 0 " & _
                       " " & _
                       "INSERT INTO #GrievanceAgainstEmp " & _
                       "SELECT againstemployeeunkid " & _
                            ",grievancemasterunkid " & _
                       "FROM ( " & _
                            "SELECT againstemployeeunkid " & _
                                 ",ROW_NUMBER() OVER ( " & _
                                      "PARTITION BY againstemployeeunkid " & _
                                      ",grievancemasterunkid ORDER BY againstemployeeunkid " & _
                                      ") AS rno " & _
                                 ",gregrievance_master.grievancemasterunkid " & _
                            "FROM gregrievance_master " & _
                            "WHERE issubmitted = 1 "


                If mblngetIsprocessData = True Then
                    strQ &= "AND isprocessed = 0 "
                End If

                strQ &= "AND isvoid = 0 " & _
                            ") AS b " & _
                       "WHERE b.rno = 1 " & _
                       "DECLARE @TotalRows INT = ( " & _
                                 "SELECT COUNT(1) " & _
                                 "FROM #GrievanceAgainstEmp " & _
                                 ") " & _
                            ",@i INT = 1 " & _
                       "WHILE @i <= @TotalRows " & _
                       "BEGIN " & _
                            "SELECT @NxEmpId = empid " & _
                                 ",@grievanceId = #GrievanceAgainstEmp.grievanceId " & _
                            "FROM #GrievanceAgainstEmp " & _
                            "WHERE EmpIndex = @i " & _
                            "SET @Eid = @NxEmpId " & _
                            "INSERT INTO #Results " & _
                            "SELECT @Eid " & _
                                 ",greresolution_step_tran.approvermasterunkid " & _
                                 ",@Eid " & _
                                 ",ROW_NUMBER() OVER ( " & _
                                      "ORDER BY resolutionsteptranunkid " & _
                                      ") " & _
                                 ",@grievanceId " & _
                            "FROM greresolution_step_tran " & _
                            "WHERE grievancemasterunkid = @grievanceId " & _
                            "WHILE @NxEmpId > 0 " & _
                            "BEGIN " & _
                                 "SET @priority = ISNULL(( " & _
                                                "SELECT MAX(priority) " & _
                                                "FROM #Results " & _
                                                "WHERE empid = @Eid " & _
                                                     "AND grievanceId = @grievanceId " & _
                                                "), 0) + 1 " & _
                                 "SET @REmpId = ISNULL(( " & _
                                                "SELECT reporttoemployeeunkid " & _
                                                "FROM hremployee_reportto " & _
                                                "WHERE employeeunkid = @NxEmpId " & _
                                                     "AND ishierarchy = 1 " & _
                                                     "AND isvoid = 0 " & _
                                                "), 0) " & _
                                 "IF (@NxEmpId = @REmpId) " & _
                                      "SET @REmpId = 0 " & _
                                 "INSERT INTO #Results ( " & _
                                      "eid " & _
                                      ",rempid " & _
                                      ",empid " & _
                                      ",priority " & _
                                      ",grievanceId " & _
                                      ") " & _
                                 "SELECT @NxEmpId " & _
                                      ",@REmpId " & _
                                      ",@Eid " & _
                                      ",@priority " & _
                                      ",@grievanceId " & _
                                 "WHERE NOT EXISTS ( " & _
                                           "SELECT * " & _
                                           "FROM greresolution_step_tran AS RST " & _
                                           "INNER JOIN gregrievance_master AS GM ON GM.grievancemasterunkid = RST.grievancemasterunkid " & _
                                                "AND RST.approverempid = @REmpId " & _
                                                "AND GM.againstemployeeunkid = @Eid " & _
                                                "AND GM.grievancemasterunkid = @grievanceId " & _
                                           ") " & _
                                 "IF (@REmpId = 0) " & _
                                 "BEGIN " & _
                                      "SET @i = @i + 1 " & _
                                      "SET @priority = 0 " & _
                                      "SELECT @NxEmpId = empid " & _
                                      "FROM #GrievanceAgainstEmp " & _
                                      "WHERE EmpIndex = @i " & _
                                           "AND grievanceId = @grievanceId " & _
                                      "BREAK; " & _
                                 "END " & _
                                 "ELSE " & _
                                      "SET @NxEmpId = @REmpId " & _
                                 "DECLARE @Rcnt AS INT " & _
                                 "SET @Rcnt = ( " & _
                                           "SELECT COUNT(1) " & _
                                           "FROM #Results " & _
                                           "WHERE rempid = @NxEmpId " & _
                                                "AND #Results.empid = @Eid " & _
                                           ") " & _
                                 "IF (@Rcnt > 1) " & _
                                 "BEGIN " & _
                                      "SET @i = @i + 1 " & _
                                      "SELECT @NxEmpId = empid " & _
                                      "FROM #GrievanceAgainstEmp " & _
                                      "WHERE EmpIndex = @i " & _
                                           "AND grievanceId = @grievanceId " & _
                                      "SET @priority = 0 " & _
                                 "END " & _
                                 "ELSE " & _
                                      "CONTINUE " & _
                            "END " & _
                       "END "
            End If

            Return strQ
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getGrievanceReportingToQuery", "modGlobal")
            Return ""
        Finally
            exForce = Nothing
        End Try
    End Function
    'Gajanan [24-OCT-2019] -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
			Language.setMessage(mstrModuleName, 1, "This Grievance Approver already exists. Please define new Grievance Approver.")
			Language.setMessage(mstrModuleName, 3, "with reference number")
            Language.setMessage(mstrModuleName, 4, "Please click below link in order to provide response.")
            Language.setMessage(mstrModuleName, 5, "Notification for Grievance Raised")
            Language.setMessage(mstrModuleName, 7, "Select")
            Language.setMessage(mstrModuleName, 8, "This is to inform you that, Resolution information has been added to grievance raised by you")
            Language.setMessage(mstrModuleName, 9, "Notification for Resolution Step Added")
			Language.setMessage(mstrModuleName, 10, "Dear")
			Language.setMessage(mstrModuleName, 12, "Please click the link below for further details and appropriate actions.")
			Language.setMessage(mstrModuleName, 14, "Regards,")
			Language.setMessage(mstrModuleName, 16, "has been successfully submitted to")
			Language.setMessage(mstrModuleName, 17, "Grievance Submission")
			Language.setMessage(mstrModuleName, 18, "Your grievance resolution has reached a final stage, if you are dissatisfied with the appeal outcome, please contact employee relations office for further guidance.")
			Language.setMessage(mstrModuleName, 19, "Notification For Grievance Disagreement")
			Language.setMessage(mstrModuleName, 20, "This is to inform you that I have raised grievance against")
			Language.setMessage(mstrModuleName, 21, "against")
			Language.setMessage(mstrModuleName, 22, "This is to inform you that your grievance")
			Language.setMessage(mstrModuleName, 23, "Sorry Reporting To Mapping Is Incorrect It's Create Recursion.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class