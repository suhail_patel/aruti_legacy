﻿'************************************************************************************************************************************
'Class Name : clsclaim_retirement_approval_Tran.vb
'Purpose    :
'Date       :23-Sep-2019
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsclaim_retirement_approval_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsclaim_retirement_approval_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mdtClaimRetirementTran As DataTable
    Private mintClaimretirementapprovaltranunkid As Integer
    Private mintClaimretirementtranunkid As Integer
    Private mintClaimretirementunkid As Integer
    Private mintCrmasterunkid As Integer
    Private mintApproveremployeeunkid As Integer
    Private mintApproverunkid As Integer
    Private mintStatusunkid As Integer
    Private mintVisibleid As Integer
    Private mintUserunkid As Integer
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Private mstrFormName As String = ""
    Private mblnIsWeb As Boolean = False
    Private objClaimRetirementTran As New clsclaim_retirement_Tran

    'Pinkal (04-Jul-2020) -- Start
    'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
    Private objRetirementProcessTran As New clsretire_process_tran
    'Pinkal (04-Jul-2020) -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property


    ''' <summary>
    ''' Purpose: Get or Set Datatable
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _ClaimRetirementTran() As DataTable
        Get
            Return mdtClaimRetirementTran
        End Get
        Set(ByVal value As DataTable)
            mdtClaimRetirementTran = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set claimretirementapprovaltranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Claimretirementapprovaltranunkid() As Integer
        Get
            Return mintClaimretirementapprovaltranunkid
        End Get
        Set(ByVal value As Integer)
            mintClaimretirementapprovaltranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Claimretirementtranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Claimretirementtranunkid() As Integer
        Get
            Return mintClaimretirementtranunkid
        End Get
        Set(ByVal value As Integer)
            mintClaimretirementtranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set claimretirementunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Claimretirementunkid() As Integer
        Get
            Return mintClaimretirementunkid
        End Get
        Set(ByVal value As Integer)
            mintClaimretirementunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crmasterunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Crmasterunkid() As Integer
        Get
            Return mintCrmasterunkid
        End Get
        Set(ByVal value As Integer)
            mintCrmasterunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approveremployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approveremployeeunkid() As Integer
        Get
            Return mintApproveremployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintApproveremployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approverunkid() As Integer
        Get
            Return mintApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set visibleid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Visibleid() As Integer
        Get
            Return mintVisibleid
        End Get
        Set(ByVal value As Integer)
            mintVisibleid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsWeb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New()

        mdtClaimRetirementTran = New DataTable("List")

        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("claimretirementapprovaltranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("claimretirementtranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("claimretirementunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("crmasterunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("expenseunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("expense")
            dCol.DataType = System.Type.GetType("System.String")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("unitprice")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("quantity")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("amount")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("expense_remark")
            dCol.DataType = System.Type.GetType("System.String")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("costcenterunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("costcentercode")
            dCol.DataType = System.Type.GetType("System.String")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("countryunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("base_countryunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("base_amount")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("exchangerateunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("exchange_rate")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("cancelfrommoduleid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("iscancel")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("canceluserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("cancel_remark")
            dCol.DataType = System.Type.GetType("System.String")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("cancel_datetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("loginemployeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtClaimRetirementTran.Columns.Add(dCol)

            dCol = New DataColumn("crprocesstranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtClaimRetirementTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (28-Oct-2021)-- Start
    'Problem in Assigning Leave Accrue Issue.

    'Public Function GetClaimRetirementApproverExpesneList(ByVal strTableName As String, ByVal IsDistinct As Boolean, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
    '                                                             , ByVal strEmployeeAsOnDate As String _
    '                                                             , Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intApproverID As Integer = -1 _
    '                                                             , Optional ByVal mstFilter As String = "", Optional ByVal intClaimRetirementMstId As Integer = -1 _
    '                                                             , Optional ByVal objDoOperation As clsDataOperation = Nothing) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    If objDoOperation Is Nothing Then
    '        objDataOperation = New clsDataOperation
    '    Else
    '        objDataOperation = objDoOperation
    '    End If
    '    objDataOperation.ClearParameters()


    '    Try


    '        Dim dsExternalCompany As New DataSet
    '        Dim StrExternalQry As String : StrExternalQry = ""


    '        Dim xAdvanceJoinQry As String = ""
    '        Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)




    '        StrExternalQry = "SELECT  "

    '        If IsDistinct Then
    '            StrExternalQry &= " DISTINCT "
    '        Else
    '            StrExternalQry &= " '' AS AUD,'' AS GUID , "
    '        End If

    '        StrExternalQry &= " Cast (0 as bit) As Ischeck " & _
    '                                    ", cmclaim_retirement_master.claimretirementunkid " & _
    '                                    ", cmclaim_retirement_master.claimretirementno " & _
    '                                    ", cmclaim_retirement_master.crmasterunkid " & _
    '                                    ", cmclaim_request_master.claimrequestno " & _
    '                                    ", h1.employeecode " & _
    '                                    ", ISNULL(h1.firstname,'') + ' ' + isnull(h1.surname,'') as employeename " & _
    '                                     ", #APPR_NAME# as approvername " & _
    '                                    ", #APPR_EMAIL_VALUE# AS approveremail " & _
    '                                    ", cmapproverlevel_master.crlevelname " & _
    '                                    ", cmapproverlevel_master.crpriority " & _
    '                                    ", cmapproverlevel_master.crlevelunkid " & _
    '                                    ", cmexpapprover_master.isexternalapprover " & _
    '                                    ", cmclaim_retirement_master.employeeunkid " & _
    '                                    ", cmclaim_retirement_master.expensetypeunkid " & _
    '                                    ", cmclaim_retirement_approval_tran.statusunkid " & _
    '                                    ", cmclaim_retirement_approval_tran.visibleid " & _
    '                                    ", cmclaim_retirement_approval_tran.approverunkid " & _
    '                                    ", cmclaim_retirement_approval_tran.approveremployeeunkid " & _
    '                                    ", ISNULL(hrapprover_usermapping.userunkid,-1) as mapuserunkid " & _
    '                                    ", '' AS period " & _
    '                                    ", CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) AS tdate " & _
    '                                    ", CASE WHEN cmclaim_retirement_master.expensetypeunkid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
    '                                    "         WHEN cmclaim_retirement_master.expensetypeunkid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
    '                                    "         WHEN cmclaim_retirement_master.expensetypeunkid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous " & _
    '                                    "         WHEN cmclaim_retirement_master.expensetypeunkid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training  " & _
    '                                    "         WHEN cmclaim_retirement_master.expensetypeunkid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest END AS expensetype " & _
    '                                    ", CASE WHEN cmclaim_retirement_approval_tran.statusunkid = 1 then @Approve " & _
    '                                    "         WHEN cmclaim_retirement_approval_tran.statusunkid = 2 then @Pending  " & _
    '                                    "         WHEN cmclaim_retirement_approval_tran.statusunkid = 3 then @Reject  " & _
    '                                    "         WHEN cmclaim_retirement_approval_tran.statusunkid = 6 then @Cancel " & _
    '                                    "  END as status " & _
    '                                    ", ISNULL(cmclaim_retirement_master.claim_amount,0.00) AS claim_amount " & _
    '                                    ", 0.00 AS balance  "


    '        'Pinkal (07-Nov-2019) -- Enhancement NMB - Working On OT & Imprest Enhancement for NMB.[", ISNULL(cmclaim_retirement_master.claim_amount,0.00) AS claim_amount,0.00 AS balance "]

    '        If IsDistinct Then
    '            StrExternalQry &= ", ISNULL((SELECT SUM(ct.amount) From cmclaim_retirement_approval_tran  ct WHERE ct.claimretirementunkid = cmclaim_retirement_approval_tran.claimretirementunkid AND ct.approveremployeeunkid = cmclaim_retirement_approval_tran.approveremployeeunkid AND ct.approverunkid = cmclaim_retirement_approval_tran.approverunkid AND ct.isvoid = 0 AND cmclaim_retirement_approval_tran.isvoid = 0 AND ct.iscancel = 0),0.00 ) amount " & _
    '                                       ", App.AppprovalStatus "
    '        Else
    '            StrExternalQry &= ", ISNULL(cmclaim_retirement_approval_tran.claimretirementapprovaltranunkid ,0) AS claimretirementapprovaltranunkid " & _
    '                                        ", ISNULL(cmclaim_retirement_approval_tran.claimretirementtranunkid ,0) AS claimretirementtranunkid " & _
    '                                        ", ISNULL(cmclaim_retirement_approval_tran.expenseunkid,0) AS expenseunkid " & _
    '                                        ", ISNULL(cmexpense_master.name,'') AS expense " & _
    '                                        ", ISNULL(cmexpense_master.isaccrue,0) AS isaccrue " & _
    '                                        ", ISNULL(cmexpense_master.isleaveencashment,0) AS isleaveencashment " & _
    '                                        ", ISNULL(cmexpense_master.isconsiderforpayroll,0) AS isconsiderforpayroll " & _
    '                                        ", ISNULL(cmclaim_retirement_approval_tran.unitprice,0.00 ) unitprice " & _
    '                                        ", ISNULL(cmclaim_retirement_approval_tran.quantity,0.00 ) quantity " & _
    '                                        ", ISNULL(cmclaim_retirement_approval_tran.amount,0.00 ) amount " & _
    '                                        ", ISNULL(cmclaim_retirement_approval_tran.expense_remark,'') expense_remark " & _
    '                                        ", ISNULL(cmclaim_retirement_approval_tran.cancel_remark,'') AS cancel_remark " & _
    '                                        ", ISNULL(cmclaim_retirement_approval_tran.canceluserunkid,0) AS canceluserunkid " & _
    '                                        ", cmclaim_retirement_approval_tran.cancel_datetime " & _
    '                                        ", ISNULL(cmclaim_retirement_approval_tran.costcenterunkid,0) AS costcenterunkid " & _
    '                                        ", ISNULL(prcostcenter_master.costcentercode,'') AS costcentercode " & _
    '                                        ", ISNULL(prcostcenter_master.costcentername,'') AS costcentername " & _
    '                                        ", ISNULL(cmexpense_master.isbudgetmandatory,0) AS isbudgetmandatory " & _
    '                                        ", ISNULL(cmexpense_master.ishrexpense,0) AS ishrexpense " & _
    '                                        ",ISNULL(cmclaim_retirement_approval_tran.countryunkid,0) AS countryunkid " & _
    '                                        ",ISNULL(cmclaim_retirement_approval_tran.base_countryunkid,0) AS base_countryunkid " & _
    '                                        ",ISNULL(cmclaim_retirement_approval_tran.base_amount,0.00) AS base_amount " & _
    '                                        ",ISNULL(cmclaim_retirement_approval_tran.exchangerateunkid,0) AS exchangerateunkid " & _
    '                                        ",ISNULL(cmclaim_retirement_approval_tran.exchange_rate,0.00) AS exchange_rate " & _
    '                                        ",ISNULL(cfexchange_rate.currency_sign,'') AS currency_sign "

    '        End If

    '        StrExternalQry &= ", cmclaim_retirement_approval_tran.iscancel " & _
    '                                    ", ISNULL(cmclaim_retirement_approval_tran.isvoid,0) AS isvoid " & _
    '                                    ", ISNULL(cmclaim_retirement_approval_tran.voiduserunkid,0) AS voiduserunkid " & _
    '                                    ", cmclaim_retirement_approval_tran.voiddatetime " & _
    '                                    ", ISNULL(cmclaim_retirement_approval_tran.voidreason,'') AS voidreason " & _
    '                                    ", 0 AS loginemployeeunkid " & _
    '                                    ", 0 AS voidloginemployeeunkid " & _
    '                                    ", ISNULL(Alloc.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                                    ", ISNULL(Alloc.unitgroupunkid,0) AS unitgroupunkid " & _
    '                                    ", ISNULL(Alloc.teamunkid,0) AS teamunkid " & _
    '                                    ", ISNULL(Alloc.stationunkid,0) AS stationunkid " & _
    '                                    ", ISNULL(Alloc.deptgroupunkid,0) AS deptgroupunkid " & _
    '                                    ", ISNULL(Alloc.departmentunkid,0) AS departmentunkid " & _
    '                                    ", ISNULL(Alloc.sectionunkid,0) AS sectionunkid " & _
    '                                    ", ISNULL(Alloc.unitunkid,0) AS unitunkid " & _
    '                                    ", ISNULL(Alloc.classgroupunkid,0) AS classgroupunkid " & _
    '                                    ", ISNULL(Alloc.classunkid,0) AS classunkid " & _
    '                                    ", ISNULL(Jobs.jobunkid,0) AS jobunkid " & _
    '                                    ", ISNULL(Jobs.jobgroupunkid,0) AS jobgroupunkid " & _
    '                                    ", ISNULL(Grds.gradegroupunkid,0) AS gradegroupunkid " & _
    '                                    ", ISNULL(Grds.gradeunkid,0) AS gradeunkid " & _
    '                                    ", ISNULL(Grds.gradelevelunkid,0) AS gradelevelunkid " & _
    '                                    ", CONVERT(CHAR(8),cmclaim_retirement_approval_tran.approvaldate, 112) AS approvaldate" & _
    '                                    "  FROM cmclaim_retirement_approval_tran " & _
    '                                    "  JOIN cmclaim_retirement_master ON cmclaim_retirement_master.claimretirementunkid = cmclaim_retirement_approval_tran.claimretirementunkid AND cmclaim_retirement_master.isvoid = 0 " & _
    '                                    "  JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_retirement_approval_tran.crmasterunkid And cmclaim_request_master.isvoid = 0 " & _
    '                                    "  LEFT JOIN hremployee_master h1 on h1.employeeunkid = cmclaim_retirement_master.employeeunkid  " & _
    '                                    "  #EMPL_JOIN#  " & _
    '                                    "  LEFT JOIN cmexpapprover_master ON cmexpapprover_master.employeeunkid = cmclaim_retirement_approval_tran.approveremployeeunkid  " & _
    '                                    "  AND cmexpapprover_master.crapproverunkid = cmclaim_retirement_approval_tran.approverunkid  AND cmexpapprover_master.isexternalapprover = #ExAppr#" & _
    '                                    "  AND  cmexpapprover_master.expensetypeid = cmclaim_retirement_master.expensetypeunkid " & _
    '                                    "  LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid AND cmapproverlevel_master.expensetypeid = cmexpapprover_master.expensetypeid " & _
    '                                    "  LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = cmexpapprover_master.crapproverunkid " & " AND hrapprover_usermapping.usertypeid = " & enUserType.crApprover & _
    '                                    "  LEFT JOIN " & _
    '                                    "  ( " & _
    '                                    "   SELECT " & _
    '                                    "         stationunkid " & _
    '                                    "        ,deptgroupunkid " & _
    '                                    "        ,departmentunkid " & _
    '                                    "        ,sectiongroupunkid " & _
    '                                    "        ,sectionunkid " & _
    '                                    "        ,unitgroupunkid " & _
    '                                    "        ,unitunkid " & _
    '                                    "        ,teamunkid " & _
    '                                    "        ,classgroupunkid " & _
    '                                    "        ,classunkid " & _
    '                                    "        ,employeeunkid " & _
    '                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                                    "    FROM hremployee_transfer_tran " & _
    '                                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
    '                                    "   ) AS Alloc ON Alloc.employeeunkid = h1.employeeunkid AND Alloc.rno = 1 " & _
    '                                    "  LEFT JOIN " & _
    '                                    "   ( " & _
    '                                    "    SELECT " & _
    '                                    "         jobunkid " & _
    '                                    "        ,jobgroupunkid " & _
    '                                    "        ,employeeunkid " & _
    '                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                                    "    FROM hremployee_categorization_tran " & _
    '                                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmployeeAsOnDate & "' " & _
    '                                    "   ) AS Jobs ON Jobs.employeeunkid = h1.employeeunkid AND Jobs.rno = 1 " & _
    '                                    "  JOIN " & _
    '                                    "   ( " & _
    '                                    "        SELECT " & _
    '                                    "         gradegroupunkid " & _
    '                                    "        ,gradeunkid " & _
    '                                    "        ,gradelevelunkid " & _
    '                                    "        ,employeeunkid " & _
    '                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
    '                                    "    FROM prsalaryincrement_tran " & _
    '                                    "    WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & strEmployeeAsOnDate & "' " & _
    '                                    "   ) AS Grds ON Grds.employeeunkid = h1.employeeunkid AND Grds.rno = 1 "


    '        If IsDistinct = False Then
    '            StrExternalQry &= " JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_retirement_approval_tran.expenseunkid " & _
    '                                       " LEFT JOIN praccount_master ON praccount_master.accountunkid = cmexpense_master.glcodeunkid " & _
    '                                       " LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = cmclaim_retirement_approval_tran.costcenterunkid " & _
    '                                       " LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = ISNULL(cmclaim_retirement_approval_tran.exchangerateunkid,0) "
    '        End If


    '        If IsDistinct Then

    '            StrExternalQry &= " LEFT JOIN " & _
    '                                       " ( " & _
    '                                       "    SELECT cmclaim_retirement_approval_tran.claimretirementunkid " & _
    '                                       "      ,cmclaim_retirement_approval_tran.approverunkid " & _
    '                                       "      ,cmapproverlevel_master.crpriority " & _
    '                                       "      ,cmclaim_retirement_approval_tran.statusunkid " & _
    '                                       "      ,ROW_NUMBER() OVER (PARTITION BY cmclaim_retirement_approval_tran.claimretirementunkid,cmapproverlevel_master.crpriority ORDER BY cmclaim_retirement_approval_tran.approvaldate DESC ) AS rno " & _
    '                                       "      ,CASE WHEN cmclaim_retirement_approval_tran.statusunkid = 1  THEN  @ApprovedBy + ' ' + ISNULL(cuser.username, '') " & _
    '                                       "               WHEN cmclaim_retirement_approval_tran.statusunkid = 2  THEN  @Pending " & _
    '                                       "               WHEN cmclaim_retirement_approval_tran.statusunkid = 3  THEN  @RejectedBy + ' ' + ISNULL(cuser.username, '') " & _
    '                                       "       END AS 'AppprovalStatus' " & _
    '                                       "  FROM cmclaim_retirement_approval_tran " & _
    '                                       "  LEFT JOIN cmexpapprover_master ON cmexpapprover_master.employeeunkid = cmclaim_retirement_approval_tran.approveremployeeunkid " & _
    '                                       "  AND cmexpapprover_master.crapproverunkid = cmclaim_retirement_approval_tran.approverunkid AND cmexpapprover_master.isexternalapprover = 0 " & _
    '                                       "  LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = cmexpapprover_master.crapproverunkid AND usertypeid = " & enUserType.crApprover & _
    '                                       "  LEFT JOIN hrmsConfiguration..cfuser_master AS cuser ON cuser.userunkid = hrapprover_usermapping.userunkid " & _
    '                                       "  LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
    '                                       "  WHERE cmclaim_retirement_approval_tran.isvoid = 0 " & _
    '                                       " ) " & _
    '                                       " AS App ON app.claimretirementunkid = cmclaim_retirement_approval_tran.claimretirementunkid AND app.crpriority = cmapproverlevel_master.crpriority AND app.rno = 1 "
    '        End If


    '        If xAdvanceJoinQry.Trim.Length > 0 Then
    '            StrExternalQry &= xAdvanceJoinQry.Replace("hremployee_master", "h1")
    '        End If

    '        StrExternalQry &= " WHERE 1 = 1 "

    '        If blnOnlyActive Then
    '            StrExternalQry &= " AND cmclaim_retirement_approval_tran.isvoid = 0 "
    '        End If

    '        If intApproverID > 0 Then
    '            StrExternalQry &= " AND cmclaim_retirement_approval_tran.approverunkid = " & intApproverID
    '        End If

    '        If mstFilter.Trim.Length > 0 Then
    '            StrExternalQry &= " AND " & mstFilter
    '        End If

    '        If intClaimRetirementMstId > -1 Then
    '            StrExternalQry &= " AND cmclaim_retirement_master.claimretirementunkid = " & intClaimRetirementMstId
    '        End If

    '        strQ &= StrExternalQry

    '        strQ = strQ.Replace("#APPR_NAME#", "ISNULL(h2.firstname,'') + ' ' + isnull(h2.surname,'') ")
    '        strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN #DName#hremployee_master h2 on h2.employeeunkid = cmclaim_retirement_approval_tran.approveremployeeunkid ")
    '        strQ = strQ.Replace("#DName#", "")
    '        strQ = strQ.Replace("#ExAppr#", "0")
    '        strQ = strQ.Replace("#APPR_EMAIL_VALUE#", "ISNULL(h2.email,'') ")

    '        dsExternalCompany = GetClaimRetirementExternalApproverList(objDoOperation, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        For Each dr In dsExternalCompany.Tables(0).Rows
    '            If strQ.Trim.Length > 0 Then strQ &= " UNION "
    '            strQ &= StrExternalQry & " AND UEmp.companyunkid = " & dr("companyunkid") & " "
    '            If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
    '                strQ = strQ.Replace("#APPR_NAME#", " ISNULL(UEmp.username,'') ")
    '                strQ = strQ.Replace("#APPR_EMAIL_VALUE#", "ISNULL(UEmp.email,'') ")
    '                strQ = strQ.Replace("#EMPL_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = cmclaim_retirement_approval_tran.approveremployeeunkid ")
    '            Else
    '                strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.surname, '') END ")
    '                strQ = strQ.Replace("#APPR_EMAIL_VALUE#", "ISNULL(UEmp.email,'') ")
    '                strQ = strQ.Replace("#EMPL_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = cmclaim_retirement_approval_tran.approveremployeeunkid " & _
    '                                               " LEFT JOIN #DName#hremployee_master h2 on h2.employeeunkid = UEmp.employeeunkid AND h2.isapproved = 1")
    '            End If

    '            If dr("DName").ToString.Trim.Length > 0 Then
    '                strQ = strQ.Replace("#DName#", dr("DName") & "..")
    '            Else
    '                strQ = strQ.Replace("#DName#", "")
    '            End If
    '            strQ = strQ.Replace("#ExAppr#", "1")
    '        Next


    '        strQ &= " ORDER BY cmclaim_retirement_master.crmasterunkid,cmapproverlevel_master.crpriority "


    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
    '        objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
    '        objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
    '        objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))
    '        objDataOperation.AddParameter("@Imprest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 9, "Imprest"))

    '        objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
    '        objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
    '        objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
    '        objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))

    '        If IsDistinct = False Then
    '            objDataOperation.AddParameter("@Qty", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 6, "Quantity"))
    '            objDataOperation.AddParameter("@Amt", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 7, "Amount"))
    '        End If

    '        objDataOperation.AddParameter("@ApprovedBy", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Approved By :"))
    '        objDataOperation.AddParameter("@RejectedBy", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Rejected By :"))

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetClaimRetirementApproverExpesneList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        If objDoOperation Is Nothing Then objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    Public Function GetClaimRetirementApproverExpesneList(ByVal strTableName As String, ByVal IsDistinct As Boolean, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                                                 , ByVal strEmployeeAsOnDate As String _
                                                                 , Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intApproverID As Integer = -1 _
                                                                 , Optional ByVal mstFilter As String = "", Optional ByVal intClaimRetirementMstId As Integer = -1 _
                                                               , Optional ByVal objDoOperation As clsDataOperation = Nothing _
                                                               , Optional ByVal mblnIncludeGrp As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()


        Try


            Dim dsExternalCompany As New DataSet
            Dim StrExternalQry As String : StrExternalQry = ""


            Dim xAdvanceJoinQry As String = ""
            Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(strEmployeeAsOnDate), xDatabaseName)


            If mblnIncludeGrp AndAlso IsDistinct Then

                StrExternalQry &= " SELECT DISTINCT CAST(1 AS BIT) AS IsGrp " & _
                                           ", Cast(0 AS BIT) AS Ischeck " & _
                                           ",cmclaim_retirement_master.claimretirementunkid " & _
                                           ",cmclaim_retirement_master.claimretirementno " & _
                                           ",cmclaim_retirement_master.crmasterunkid " & _
                                           ",cmclaim_request_master.claimrequestno " & _
                                           ",h1.employeecode " & _
                                           ",ISNULL(h1.firstname, '') + ' ' + isnull(h1.surname, '') AS employeename " & _
                                           ",'' AS approvername " & _
                                           ",''  AS approveremail " & _
                                           ",'' AS crlevelname " & _
                                           ",-1 AS crpriority " & _
                                           ", 0 AS crlevelunkid " & _
                                           ",CAST (0 AS BIT) AS isexternalapprover " & _
                                           ",cmclaim_retirement_master.employeeunkid " & _
                                           ",cmclaim_retirement_master.expensetypeunkid " & _
                                           ",0 AS statusunkid " & _
                                           ",0 AS visibleid " & _
                                           ",0  AS  approverunkid " & _
                                           ",0 AS approveremployeeunkid " & _
                                           ",- 1 AS mapuserunkid " & _
                                           ",'' AS period " & _
                                           ",CONVERT(CHAR(8), cmclaim_request_master.transactiondate, 112) AS tdate " & _
                                           ", CASE WHEN cmclaim_retirement_master.expensetypeunkid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
                                           "         WHEN cmclaim_retirement_master.expensetypeunkid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
                                           "         WHEN cmclaim_retirement_master.expensetypeunkid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous " & _
                                           "         WHEN cmclaim_retirement_master.expensetypeunkid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training  " & _
                                           "         WHEN cmclaim_retirement_master.expensetypeunkid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest END AS expensetype " & _
                                           ",'' STATUS " & _
                                           ",ISNULL(cmclaim_retirement_master.claim_amount, 0.00) AS claim_amount " & _
                                           ",0.00 AS balance " & _
                                           ",0.00 AS Amount " & _
                                           ",'' AS AppprovalStatus " & _
                                           ",cmclaim_retirement_approval_tran.iscancel " & _
                                           ",ISNULL(cmclaim_retirement_approval_tran.isvoid, 0) AS isvoid " & _
                                           ",ISNULL(cmclaim_retirement_approval_tran.voiduserunkid, 0) AS voiduserunkid " & _
                                           ",cmclaim_retirement_approval_tran.voiddatetime " & _
                                           ",ISNULL(cmclaim_retirement_approval_tran.voidreason, '') AS voidreason " & _
                                           ",0 AS loginemployeeunkid " & _
                                           ",0 AS voidloginemployeeunkid " & _
                                           ", 0 AS sectiongroupunkid " & _
                                           ", 0 AS unitgroupunkid " & _
                                           ",0 AS teamunkid " & _
                                           ",0 AS stationunkid " & _
                                           ",0 AS deptgroupunkid " & _
                                           ",0 AS departmentunkid " & _
                                           ",0 AS sectionunkid " & _
                                           ",0 AS unitunkid " & _
                                           ",0 AS classgroupunkid " & _
                                           ",0 AS classunkid " & _
                                           ",0 AS jobunkid " & _
                                           ",0 AS jobgroupunkid " & _
                                           ",0 AS gradegroupunkid " & _
                                           ",0 AS gradeunkid " & _
                                           ",0 AS gradelevelunkid " & _
                                           ",NULL AS approvaldate " & _
                                           "  FROM cmclaim_retirement_approval_tran " & _
                                           "  JOIN cmclaim_retirement_master ON cmclaim_retirement_master.claimretirementunkid = cmclaim_retirement_approval_tran.claimretirementunkid AND cmclaim_retirement_master.isvoid = 0 " & _
                                           "  JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_retirement_approval_tran.crmasterunkid And cmclaim_request_master.isvoid = 0 " & _
                                           "  LEFT JOIN hremployee_master h1 ON h1.employeeunkid = cmclaim_retirement_master.employeeunkid " & _
                                           "  JOIN cmexpapprover_master ON cmexpapprover_master.employeeunkid = cmclaim_retirement_approval_tran.approveremployeeunkid  " & _
                                           "  AND cmexpapprover_master.crapproverunkid = cmclaim_retirement_approval_tran.approverunkid  AND cmexpapprover_master.isexternalapprover = #ExAppr#" & _
                                           "  LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                                           "  LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = cmexpapprover_master.crapproverunkid AND hrapprover_usermapping.usertypeid = " & enUserType.crApprover

                'Pinkal (30-Nov-2021)-- KBC Claim Retirement Approval Issue.[JOIN cmexpapprover_master ON cmexpapprover_master.employeeunkid = cmclaim_retirement_approval_tran.approveremployeeunkid  " & _]

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrExternalQry &= xAdvanceJoinQry.Replace("hremployee_master", "h1")
                End If

                StrExternalQry &= " WHERE 1 = 1 "

                If blnOnlyActive Then
                    StrExternalQry &= " AND cmclaim_retirement_approval_tran.isvoid = 0 "
                End If

                If intApproverID > 0 Then
                    StrExternalQry &= " AND cmclaim_retirement_approval_tran.approverunkid = " & intApproverID
                End If

                If mstFilter.Trim.Length > 0 Then
                    StrExternalQry &= " AND " & mstFilter
                End If

                If intClaimRetirementMstId > -1 Then
                    StrExternalQry &= " AND cmclaim_retirement_master.claimretirementunkid = " & intClaimRetirementMstId
                End If

                StrExternalQry &= " UNION "

            End If

            StrExternalQry &= "SELECT  "

            If IsDistinct Then
                StrExternalQry &= " DISTINCT "
            Else
                StrExternalQry &= " '' AS AUD,'' AS GUID , "
            End If

            If IsDistinct AndAlso mblnIncludeGrp Then StrExternalQry &= " CAST(0 AS BIT) AS IsGrp , "

            StrExternalQry &= " Cast (0 as bit) As Ischeck " & _
                                        ", cmclaim_retirement_master.claimretirementunkid " & _
                                        ", cmclaim_retirement_master.claimretirementno " & _
                                        ", cmclaim_retirement_master.crmasterunkid " & _
                                        ", cmclaim_request_master.claimrequestno " & _
                                        ", h1.employeecode " & _
                                        ", ISNULL(h1.firstname,'') + ' ' + isnull(h1.surname,'') as employeename " & _
                                         ", #APPR_NAME# as approvername " & _
                                        ", #APPR_EMAIL_VALUE# AS approveremail " & _
                                        ", cmapproverlevel_master.crlevelname " & _
                                        ", cmapproverlevel_master.crpriority " & _
                                        ", cmapproverlevel_master.crlevelunkid " & _
                                        ", cmexpapprover_master.isexternalapprover " & _
                                        ", cmclaim_retirement_master.employeeunkid " & _
                                        ", cmclaim_retirement_master.expensetypeunkid " & _
                                        ", cmclaim_retirement_approval_tran.statusunkid " & _
                                        ", cmclaim_retirement_approval_tran.visibleid " & _
                                        ", cmclaim_retirement_approval_tran.approverunkid " & _
                                        ", cmclaim_retirement_approval_tran.approveremployeeunkid " & _
                                        ", ISNULL(hrapprover_usermapping.userunkid,-1) as mapuserunkid " & _
                                        ", '' AS period " & _
                                        ", CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) AS tdate " & _
                                        ", CASE WHEN cmclaim_retirement_master.expensetypeunkid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
                                        "         WHEN cmclaim_retirement_master.expensetypeunkid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
                                        "         WHEN cmclaim_retirement_master.expensetypeunkid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous " & _
                                        "         WHEN cmclaim_retirement_master.expensetypeunkid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training  " & _
                                        "         WHEN cmclaim_retirement_master.expensetypeunkid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest END AS expensetype " & _
                                        ", CASE WHEN cmclaim_retirement_approval_tran.statusunkid = 1 then @Approve " & _
                                        "         WHEN cmclaim_retirement_approval_tran.statusunkid = 2 then @Pending  " & _
                                        "         WHEN cmclaim_retirement_approval_tran.statusunkid = 3 then @Reject  " & _
                                        "         WHEN cmclaim_retirement_approval_tran.statusunkid = 6 then @Cancel " & _
                                        "  END as status " & _
                                        ", ISNULL(cmclaim_retirement_master.claim_amount,0.00) AS claim_amount " & _
                                        ", 0.00 AS balance  "



            If IsDistinct Then

                'Pinkal (30-Nov-2021)-- Start
                'KBC Claim Retirement Approval Issue.
                'StrExternalQry &= ", App.Amount  " & _
                '                           ", App.AppprovalStatus 
                StrExternalQry &= ", ISNULL(App.Amount,0.00) AS Amount  " & _
                                           ", ISNULL(App.AppprovalStatus,'') AS AppprovalStatus "
                'Pinkal (30-Nov-2021) -- End

            Else
                StrExternalQry &= ", ISNULL(cmclaim_retirement_approval_tran.claimretirementapprovaltranunkid ,0) AS claimretirementapprovaltranunkid " & _
                                            ", ISNULL(cmclaim_retirement_approval_tran.claimretirementtranunkid ,0) AS claimretirementtranunkid " & _
                                            ", ISNULL(cmclaim_retirement_approval_tran.expenseunkid,0) AS expenseunkid " & _
                                            ", ISNULL(cmexpense_master.name,'') AS expense " & _
                                            ", ISNULL(cmexpense_master.isaccrue,0) AS isaccrue " & _
                                            ", ISNULL(cmexpense_master.isleaveencashment,0) AS isleaveencashment " & _
                                            ", ISNULL(cmexpense_master.isconsiderforpayroll,0) AS isconsiderforpayroll " & _
                                            ", ISNULL(cmclaim_retirement_approval_tran.unitprice,0.00 ) unitprice " & _
                                            ", ISNULL(cmclaim_retirement_approval_tran.quantity,0.00 ) quantity " & _
                                            ", ISNULL(cmclaim_retirement_approval_tran.amount,0.00 ) amount " & _
                                            ", ISNULL(cmclaim_retirement_approval_tran.expense_remark,'') expense_remark " & _
                                            ", ISNULL(cmclaim_retirement_approval_tran.cancel_remark,'') AS cancel_remark " & _
                                            ", ISNULL(cmclaim_retirement_approval_tran.canceluserunkid,0) AS canceluserunkid " & _
                                            ", cmclaim_retirement_approval_tran.cancel_datetime " & _
                                            ", ISNULL(cmclaim_retirement_approval_tran.costcenterunkid,0) AS costcenterunkid " & _
                                            ", ISNULL(prcostcenter_master.costcentercode,'') AS costcentercode " & _
                                            ", ISNULL(prcostcenter_master.costcentername,'') AS costcentername " & _
                                            ", ISNULL(cmexpense_master.isbudgetmandatory,0) AS isbudgetmandatory " & _
                                            ", ISNULL(cmexpense_master.ishrexpense,0) AS ishrexpense " & _
                                            ",ISNULL(cmclaim_retirement_approval_tran.countryunkid,0) AS countryunkid " & _
                                            ",ISNULL(cmclaim_retirement_approval_tran.base_countryunkid,0) AS base_countryunkid " & _
                                            ",ISNULL(cmclaim_retirement_approval_tran.base_amount,0.00) AS base_amount " & _
                                            ",ISNULL(cmclaim_retirement_approval_tran.exchangerateunkid,0) AS exchangerateunkid " & _
                                            ",ISNULL(cmclaim_retirement_approval_tran.exchange_rate,0.00) AS exchange_rate " & _
                                            ",ISNULL(cfexchange_rate.currency_sign,'') AS currency_sign "

            End If

            StrExternalQry &= ", cmclaim_retirement_approval_tran.iscancel " & _
                                        ", ISNULL(cmclaim_retirement_approval_tran.isvoid,0) AS isvoid " & _
                                        ", ISNULL(cmclaim_retirement_approval_tran.voiduserunkid,0) AS voiduserunkid " & _
                                        ", cmclaim_retirement_approval_tran.voiddatetime " & _
                                        ", ISNULL(cmclaim_retirement_approval_tran.voidreason,'') AS voidreason " & _
                                        ", 0 AS loginemployeeunkid " & _
                                        ", 0 AS voidloginemployeeunkid " & _
                                        ", ISNULL(ADF.sectiongroupunkid,0) AS sectiongroupunkid " & _
                                        ", ISNULL(ADF.unitgroupunkid,0) AS unitgroupunkid " & _
                                        ", ISNULL(ADF.teamunkid,0) AS teamunkid " & _
                                        ", ISNULL(ADF.stationunkid,0) AS stationunkid " & _
                                        ", ISNULL(ADF.deptgroupunkid,0) AS deptgroupunkid " & _
                                        ", ISNULL(ADF.departmentunkid,0) AS departmentunkid " & _
                                        ", ISNULL(ADF.sectionunkid,0) AS sectionunkid " & _
                                        ", ISNULL(ADF.unitunkid,0) AS unitunkid " & _
                                        ", ISNULL(ADF.classgroupunkid,0) AS classgroupunkid " & _
                                        ", ISNULL(ADF.classunkid,0) AS classunkid " & _
                                        ", ISNULL(ADF.jobunkid,0) AS jobunkid " & _
                                        ", ISNULL(ADF.jobgroupunkid,0) AS jobgroupunkid " & _
                                        ", ISNULL(ADF.gradegroupunkid,0) AS gradegroupunkid " & _
                                        ", ISNULL(ADF.gradeunkid,0) AS gradeunkid " & _
                                        ", ISNULL(ADF.gradelevelunkid,0) AS gradelevelunkid " & _
                                        ", CONVERT(CHAR(8),cmclaim_retirement_approval_tran.approvaldate, 112) AS approvaldate" & _
                                        "  FROM cmclaim_retirement_approval_tran " & _
                                        "  JOIN cmclaim_retirement_master ON cmclaim_retirement_master.claimretirementunkid = cmclaim_retirement_approval_tran.claimretirementunkid AND cmclaim_retirement_master.isvoid = 0 " & _
                                        "  JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_retirement_approval_tran.crmasterunkid And cmclaim_request_master.isvoid = 0 " & _
                                        "  JOIN cmexpapprover_master ON cmexpapprover_master.employeeunkid = cmclaim_retirement_approval_tran.approveremployeeunkid  " & _
                                        "  AND cmexpapprover_master.crapproverunkid = cmclaim_retirement_approval_tran.approverunkid  AND cmexpapprover_master.isexternalapprover = #ExAppr#" & _
                                        "  LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid	" & _
                                        "  LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = cmexpapprover_master.crapproverunkid " & " AND hrapprover_usermapping.usertypeid = " & enUserType.crApprover & _
                                        "  LEFT JOIN hremployee_master h1 on h1.employeeunkid = cmclaim_retirement_master.employeeunkid  " & _
                                        "  #EMPL_JOIN#  "

            'Pinkal (30-Nov-2021)-- KBC Claim Retirement Approval Issue.[JOIN cmexpapprover_master ON cmexpapprover_master.employeeunkid = cmclaim_retirement_approval_tran.approveremployeeunkid  " & _]

            If IsDistinct = False Then
                StrExternalQry &= " JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_retirement_approval_tran.expenseunkid " & _
                                           " LEFT JOIN praccount_master ON praccount_master.accountunkid = cmexpense_master.glcodeunkid " & _
                                           " LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = cmclaim_retirement_approval_tran.costcenterunkid " & _
                                           " LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = ISNULL(cmclaim_retirement_approval_tran.exchangerateunkid,0) "
            End If


            If IsDistinct Then

                StrExternalQry &= " LEFT JOIN " & _
                                           " ( " & _
                                           "    SELECT cmclaim_retirement_approval_tran.claimretirementunkid " & _
                                           "      ,cmclaim_retirement_approval_tran.approverunkid " & _
                                           "      ,cmapproverlevel_master.crpriority " & _
                                           "      ,cmclaim_retirement_approval_tran.statusunkid " & _
                                           "      ,ROW_NUMBER() OVER (PARTITION BY cmclaim_retirement_approval_tran.claimretirementunkid,cmapproverlevel_master.crpriority ORDER BY cmclaim_retirement_approval_tran.approvaldate DESC ) AS rno " & _
                                           "      ,CASE WHEN cmclaim_retirement_approval_tran.statusunkid = 1  THEN  @ApprovedBy + ' ' + ISNULL(cuser.username, '') " & _
                                           "               WHEN cmclaim_retirement_approval_tran.statusunkid = 2  THEN  @Pending " & _
                                           "               WHEN cmclaim_retirement_approval_tran.statusunkid = 3  THEN  @RejectedBy + ' ' + ISNULL(cuser.username, '') " & _
                                           "       END AS 'AppprovalStatus' " & _
                                           "    ,SUM(cmclaim_retirement_approval_tran.amount) As amount " & _
                                           "  FROM cmclaim_retirement_approval_tran " & _
                                           "  LEFT JOIN cmexpapprover_master ON cmexpapprover_master.employeeunkid = cmclaim_retirement_approval_tran.approveremployeeunkid " & _
                                           "  AND cmexpapprover_master.crapproverunkid = cmclaim_retirement_approval_tran.approverunkid AND cmexpapprover_master.isexternalapprover = #ExAppr# " & _
                                           "  LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = cmexpapprover_master.crapproverunkid AND usertypeid = " & enUserType.crApprover & _
                                           "  LEFT JOIN hrmsConfiguration..cfuser_master AS cuser ON cuser.userunkid = hrapprover_usermapping.userunkid " & _
                                           "  LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                                           "  WHERE cmclaim_retirement_approval_tran.isvoid = 0 AND cmclaim_retirement_approval_tran.iscancel = 0 " & _
                                           "  GROUP BY cmclaim_retirement_approval_tran.claimretirementunkid,cmclaim_retirement_approval_tran.approverunkid " & _
                                           "  ,cmapproverlevel_master.crpriority ,cmclaim_retirement_approval_tran.statusunkid,cmclaim_retirement_approval_tran.approvaldate,ISNULL(cuser.username, '') " & _
                                           " ) " & _
                                           " AS App ON app.claimretirementunkid = cmclaim_retirement_approval_tran.claimretirementunkid AND app.crpriority = cmapproverlevel_master.crpriority AND app.rno = 1 "

                'Pinkal (30-Nov-2021)-- KBC Claim Retirement Approval Issue.[AND cmexpapprover_master.isexternalapprover = #ExAppr# ]

            End If


            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrExternalQry &= xAdvanceJoinQry.Replace("hremployee_master", "h1")
            End If

            StrExternalQry &= " WHERE 1 = 1 "

            If blnOnlyActive Then
                StrExternalQry &= " AND cmclaim_retirement_approval_tran.isvoid = 0 "
            End If

            If intApproverID > 0 Then
                StrExternalQry &= " AND cmclaim_retirement_approval_tran.approverunkid = " & intApproverID
            End If

            If mstFilter.Trim.Length > 0 Then
                StrExternalQry &= " AND " & mstFilter
            End If

            If intClaimRetirementMstId > -1 Then
                StrExternalQry &= " AND cmclaim_retirement_master.claimretirementunkid = " & intClaimRetirementMstId
            End If

            strQ &= StrExternalQry

            strQ = strQ.Replace("#APPR_NAME#", "ISNULL(h2.firstname,'') + ' ' + isnull(h2.surname,'') ")
            strQ = strQ.Replace("#EMPL_JOIN#", "LEFT JOIN #DName#hremployee_master h2 on h2.employeeunkid = cmclaim_retirement_approval_tran.approveremployeeunkid ")
            strQ = strQ.Replace("#DName#", "")
            strQ = strQ.Replace("#ExAppr#", "0")
            strQ = strQ.Replace("#APPR_EMAIL_VALUE#", "ISNULL(h2.email,'') ")

            dsExternalCompany = GetClaimRetirementExternalApproverList(objDoOperation, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr In dsExternalCompany.Tables(0).Rows
                If strQ.Trim.Length > 0 Then strQ &= " UNION "
                strQ &= StrExternalQry & " AND UEmp.companyunkid = " & dr("companyunkid") & " "
                If dr("companyunkid") <= 0 AndAlso dr("DName").ToString.Trim = "" Then
                    strQ = strQ.Replace("#APPR_NAME#", " ISNULL(UEmp.username,'') ")
                    strQ = strQ.Replace("#APPR_EMAIL_VALUE#", "ISNULL(UEmp.email,'') ")
                    strQ = strQ.Replace("#EMPL_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = cmclaim_retirement_approval_tran.approveremployeeunkid ")
                Else
                    strQ = strQ.Replace("#APPR_NAME#", "CASE WHEN ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.surname, '') = ' ' THEN ISNULL(UEmp.username,'') ELSE ISNULL(h2.firstname, '') + ' ' + ISNULL(h2.surname, '') END ")
                    strQ = strQ.Replace("#APPR_EMAIL_VALUE#", "ISNULL(UEmp.email,'') ")
                    strQ = strQ.Replace("#EMPL_JOIN#", " LEFT JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = cmclaim_retirement_approval_tran.approveremployeeunkid " & _
                                                   " LEFT JOIN #DName#hremployee_master h2 on h2.employeeunkid = UEmp.employeeunkid AND h2.isapproved = 1")
                End If

                If dr("DName").ToString.Trim.Length > 0 Then
                    strQ = strQ.Replace("#DName#", dr("DName") & "..")
                Else
                    strQ = strQ.Replace("#DName#", "")
                End If
                strQ = strQ.Replace("#ExAppr#", "1")
            Next


            strQ &= " ORDER BY cmclaim_retirement_master.crmasterunkid,crpriority "

            If mblnIncludeGrp Then strQ &= ",IsGrp DESC"


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
            objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
            objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
            objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))
            objDataOperation.AddParameter("@Imprest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 9, "Imprest"))

            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))

            If IsDistinct = False Then
                objDataOperation.AddParameter("@Qty", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 6, "Quantity"))
                objDataOperation.AddParameter("@Amt", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 7, "Amount"))
            End If

            objDataOperation.AddParameter("@ApprovedBy", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Approved By :"))
            objDataOperation.AddParameter("@RejectedBy", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Rejected By :"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetClaimRetirementApproverExpesneList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Pinkal (28-Oct-2021)-- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cmclaim_retirement_approval_tran) </purpose>
    Public Function InsertUpdate_ClaimRetirement_Approval(ByVal objDoOperation As clsDataOperation _
                                                                                  , Optional ByVal mstrRejcectRemark As String = "" _
                                                                                  , Optional ByVal blnIsLastApprover As Boolean = False _
                                                                                  , Optional ByVal mdtApprovalDate As DateTime = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        Try

            If objDoOperation Is Nothing Then
                objDataOperation = New clsDataOperation()
                objDataOperation.BindTransaction()
            Else
                objDataOperation = objDoOperation
            End If

            Dim objExpMst As New clsExpense_Master
            Dim mdecTotalImprestAmt As Decimal = 0
            For i = 0 To mdtClaimRetirementTran.Rows.Count - 1

                With mdtClaimRetirementTran.Rows(i)
                    objDataOperation.ClearParameters()

                    If Not IsDBNull(.Item("AUD")) Then

                        objExpMst._Expenseunkid = CInt(.Item("expenseunkid"))
                        Dim mblnIsConsiderForPayroll As Boolean = objExpMst._IsConsiderForPayroll

                        mdecTotalImprestAmt = mdtClaimRetirementTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "").DefaultIfEmpty().Sum(Function(x) x.Field(Of Decimal)("amount"))
                        Dim mdecBalance As Decimal = CDec(.Item("claim_amount")) - mdecTotalImprestAmt
                        Dim mintEmployeeId As Integer = CInt(.Item("employeeunkid"))

                        Select Case .Item("AUD")
                            Case "A"


                                If CInt(.Item("claimretirementtranunkid")) <= 0 Then
                                    objClaimRetirementTran._Claimretirementunkid = CInt(.Item("claimretirementunkid"))
                                    objClaimRetirementTran._ApproverTranId = mintApproverunkid
                                    objClaimRetirementTran._ApproverEmpId = mintApproveremployeeunkid
                                    Dim dtclaimRetirment As New DataTable()
                                    dtclaimRetirment = mdtClaimRetirementTran.Clone()
                                    dtclaimRetirment.ImportRow(mdtClaimRetirementTran.Rows(i))
                                    objClaimRetirementTran._DataTable = dtclaimRetirment.Copy()
                                    objClaimRetirementTran._Userunkid = mintUserunkid
                                    objClaimRetirementTran._FormName = mstrFormName
                                    objClaimRetirementTran._WebClientIP = mstrWebClientIP
                                    objClaimRetirementTran._WebHostName = mstrWebHostName
                                    objClaimRetirementTran._IsWeb = mblnIsWeb
                                    If objClaimRetirementTran.InsertUpdateDelete_ClaimRetirement_Tran(objDataOperation) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                    .Item("claimretirementtranunkid") = objClaimRetirementTran._Claimretirementtranunkid
                                End If

                                strQ = "INSERT INTO cmclaim_retirement_approval_tran ( " & _
                                            "  claimretirementtranunkid " & _
                                            ", claimretirementunkid " & _
                                            ", crmasterunkid " & _
                                            ", approvaldate " & _
                                            ", expenseunkid " & _
                                            ", unitprice " & _
                                            ", quantity " & _
                                            ", amount " & _
                                            ", expense_remark " & _
                                            ", costcenterunkid " & _
                                            ", countryunkid " & _
                                            ", base_countryunkid " & _
                                            ", base_amount " & _
                                            ", exchangerateunkid " & _
                                            ", exchange_rate " & _
                                            ", approveremployeeunkid " & _
                                            ", approverunkid " & _
                                            ", statusunkid " & _
                                            ", visibleid " & _
                                            ", userunkid " & _
                                            ", isvoid " & _
                                            ", voiduserunkid " & _
                                            ", voiddatetime " & _
                                            ", voidreason " & _
                                            ", iscancel " & _
                                            ", canceluserunkid " & _
                                            ", cancel_remark " & _
                                            ", cancel_datetime" & _
                                          ") VALUES (" & _
                                            "  @claimretirementtranunkid " & _
                                            ", @claimretirementunkid " & _
                                            ", @crmasterunkid " & _
                                            ", @approvaldate " & _
                                            ", @expenseunkid " & _
                                            ", @unitprice " & _
                                            ", @quantity " & _
                                            ", @amount " & _
                                            ", @expense_remark " & _
                                            ", @costcenterunkid " & _
                                            ", @countryunkid " & _
                                            ", @base_countryunkid " & _
                                            ", @base_amount " & _
                                            ", @exchangerateunkid " & _
                                            ", @exchange_rate " & _
                                            ", @approveremployeeunkid " & _
                                            ", @approverunkid " & _
                                            ", @statusunkid " & _
                                            ", @visibleid " & _
                                            ", @userunkid " & _
                                            ", @isvoid " & _
                                            ", @voiduserunkid " & _
                                            ", @voiddatetime " & _
                                            ", @voidreason " & _
                                            ", @iscancel " & _
                                            ", @canceluserunkid " & _
                                            ", @cancel_remark " & _
                                            ", @cancel_datetime" & _
                                          "); SELECT @@identity"

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@claimretirementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("claimretirementtranunkid").ToString)
                                objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("claimretirementunkid").ToString)
                                objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("crmasterunkid").ToString)
                                If mdtApprovalDate <> Nothing Then
                                    objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovalDate)
                                Else
                                    objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If

                                objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("expenseunkid").ToString)
                                objDataOperation.AddParameter("@unitprice", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("unitprice").ToString)
                                objDataOperation.AddParameter("@quantity", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("quantity").ToString)
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amount").ToString)
                                objDataOperation.AddParameter("@expense_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("expense_remark").ToString)
                                objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("costcenterunkid").ToString)
                                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countryunkid").ToString)
                                objDataOperation.AddParameter("@base_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("base_countryunkid").ToString)
                                objDataOperation.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, .Item("base_amount").ToString)
                                objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("exchangerateunkid").ToString)
                                objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, .Item("exchange_rate").ToString)
                                objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveremployeeunkid.ToString)
                                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
                                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
                                objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleid.ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)

                                If Not IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If

                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("iscancel").ToString)
                                objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("canceluserunkid").ToString)
                                objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("cancel_remark").ToString)

                                If Not IsDBNull(.Item("cancel_datetime")) Then
                                    objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("cancel_datetime"))
                                Else
                                    objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintClaimretirementapprovaltranunkid = dsList.Tables(0).Rows(0).Item(0)
                                mintClaimretirementtranunkid = CInt(.Item("claimretirementtranunkid"))
                                mintClaimretirementunkid = CInt(.Item("claimretirementunkid"))
                                mintCrmasterunkid = CInt(.Item("crmasterunkid"))


                                If ATInsertUpdateClaimRetirement_Approval(objDataOperation, 1, mdtClaimRetirementTran.Rows(i), mdtApprovalDate) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If blnIsLastApprover AndAlso mblnIsConsiderForPayroll AndAlso mintStatusunkid = 1 Then
                                    objRetirementProcessTran._Claimretirementapprovaltranunkid = mintClaimretirementapprovaltranunkid
                                    objRetirementProcessTran._Claimretirementtranunkid = mintClaimretirementtranunkid
                                    objRetirementProcessTran._Claimretirementunkid = mintClaimretirementunkid
                                    'Pinkal (10-Feb-2021) -- Start
                                    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                                    objRetirementProcessTran._Crmasterunkid = mintCrmasterunkid
                                    objRetirementProcessTran._ClaimApprovalTranId = 0
                                    'Pinkal (10-Feb-2021) -- End
                                    objRetirementProcessTran._Bal_Amount = mdecBalance
                                    objRetirementProcessTran._Employeeunkid = mintEmployeeId
                                    objRetirementProcessTran._Expenseunkid = CInt(.Item("expenseunkid"))
                                    objRetirementProcessTran._Expense_Remark = .Item("expense_remark").ToString()
                                    objRetirementProcessTran._Bal_Amount = mdecBalance
                                    objRetirementProcessTran._Employeeunkid = mintEmployeeId
                                    objRetirementProcessTran._Countryunkid = CInt(.Item("countryunkid"))
                                    objRetirementProcessTran._Base_Countryunkid = CInt(.Item("base_countryunkid"))
                                    objRetirementProcessTran._Base_Amount = mdecBalance / CDec(.Item("exchange_rate"))
                                    objRetirementProcessTran._Exchangerateunkid = CInt(.Item("exchangerateunkid"))
                                    objRetirementProcessTran._Exchange_Rate = CDec(.Item("exchange_rate"))
                                    objRetirementProcessTran._Periodunkid = -1
                                    objRetirementProcessTran._Userunkid = mintUserunkid
                                    objRetirementProcessTran._Approveremployeeunkid = mintApproveremployeeunkid
                                    objRetirementProcessTran._Crapproverunkid = mintApproverunkid
                                    objRetirementProcessTran._WebFormName = mstrFormName
                                    objRetirementProcessTran._WebHostName = mstrWebHostName
                                    objRetirementProcessTran._WebClientIP = mstrWebClientIP

                                    If objRetirementProcessTran.Insert(mdtClaimRetirementTran.Rows(i), mdtApprovalDate, objDataOperation) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                            Case "U"

                                strQ = " UPDATE cmclaim_retirement_approval_tran SET " & _
                                            "  claimretirementtranunkid = @claimretirementtranunkid" & _
                                            ", claimretirementunkid = @claimretirementunkid" & _
                                            ", crmasterunkid = @crmasterunkid" & _
                                            ", approvaldate = @approvaldate" & _
                                            ", expenseunkid = @expenseunkid" & _
                                            ", unitprice = @unitprice" & _
                                            ", quantity = @quantity" & _
                                            ", amount = @amount" & _
                                            ", expense_remark = @expense_remark" & _
                                            ", costcenterunkid = @costcenterunkid" & _
                                            ", countryunkid = @countryunkid" & _
                                            ", base_countryunkid = @base_countryunkid" & _
                                            ", base_amount = @base_amount" & _
                                            ", exchangerateunkid = @exchangerateunkid" & _
                                            ", exchange_rate = @exchange_rate" & _
                                            ", approveremployeeunkid = @approveremployeeunkid" & _
                                            ", approverunkid = @approverunkid" & _
                                            ", statusunkid = @statusunkid" & _
                                            ", visibleid = @visibleid" & _
                                            ", userunkid = @userunkid" & _
                                            ", isvoid = @isvoid" & _
                                            ", voiduserunkid = @voiduserunkid" & _
                                            ", voiddatetime = @voiddatetime" & _
                                            ", voidreason = @voidreason" & _
                                            ", iscancel = @iscancel" & _
                                            ", canceluserunkid = @canceluserunkid" & _
                                            ", cancel_remark = @cancel_remark" & _
                                            ", cancel_datetime = @cancel_datetime " & _
                                            " WHERE isvoid = 0 AND claimretirementtranunkid = @claimretirementtranunkid  " & _
                                            " AND crmasterunkid = @crmasterunkid AND expenseunkid = @expenseunkid  " & _
                                            " AND approverunkid = @approverunkid "


                                objDataOperation.AddParameter("@claimretirementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("claimretirementtranunkid").ToString)
                                objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("claimretirementunkid").ToString)
                                objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("crmasterunkid").ToString)

                                If mdtApprovalDate <> Nothing Then
                                    objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovalDate.ToString)
                                Else
                                    objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If

                                objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("expenseunkid").ToString)
                                objDataOperation.AddParameter("@unitprice", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("unitprice").ToString)
                                objDataOperation.AddParameter("@quantity", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("quantity").ToString)
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amount").ToString)
                                objDataOperation.AddParameter("@expense_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("expense_remark").ToString)
                                objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("costcenterunkid").ToString)
                                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countryunkid").ToString)
                                objDataOperation.AddParameter("@base_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("base_countryunkid").ToString)
                                objDataOperation.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, .Item("base_amount").ToString)
                                objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("exchangerateunkid").ToString)
                                objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, .Item("exchange_rate").ToString)
                                objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveremployeeunkid.ToString)
                                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
                                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
                                objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleid.ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)

                                If Not IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If

                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("iscancel").ToString)
                                objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("canceluserunkid").ToString)
                                objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("cancel_remark").ToString)

                                If Not IsDBNull(.Item("cancel_datetime")) Then
                                    objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("cancel_datetime").ToString)
                                Else
                                    objDataOperation.AddParameter("@cancel_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If


                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                strQ = " SELECT @claimretirementapprovaltranunkid =  ISNULL(claimretirementapprovaltranunkid,0)  " & _
                                          " FROM cmclaim_retirement_approval_tran WHERE isvoid = 0 AND claimretirementtranunkid = @claimretirementtranunkid  " & _
                                          " AND crmasterunkid = @crmasterunkid AND expenseunkid = @expenseunkid AND  approverunkid = @approverunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@claimretirementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("claimretirementtranunkid")))
                                objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("crmasterunkid")))
                                objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("expenseunkid")))
                                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                                objDataOperation.AddParameter("@claimretirementapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementapprovaltranunkid, ParameterDirection.InputOutput)
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintClaimretirementapprovaltranunkid = objDataOperation.GetParameterValue("@claimretirementapprovaltranunkid")

                                mintClaimretirementtranunkid = CInt(.Item("claimretirementtranunkid"))
                                mintClaimretirementunkid = CInt(.Item("claimretirementunkid"))
                                mintCrmasterunkid = CInt(.Item("crmasterunkid"))

                                If ATInsertUpdateClaimRetirement_Approval(objDataOperation, 2, mdtClaimRetirementTran.Rows(i), mdtApprovalDate) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                If blnIsLastApprover AndAlso mblnIsConsiderForPayroll AndAlso mintStatusunkid = 1 Then
                                    objRetirementProcessTran._Claimretirementapprovaltranunkid = mintClaimretirementapprovaltranunkid
                                    objRetirementProcessTran._Claimretirementtranunkid = mintClaimretirementtranunkid
                                    objRetirementProcessTran._Claimretirementunkid = mintClaimretirementunkid

                                    'Pinkal (10-Feb-2021) -- Start
                                    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                                    objRetirementProcessTran._Crmasterunkid = mintCrmasterunkid
                                    objRetirementProcessTran._ClaimApprovalTranId = 0
                                    'Pinkal (10-Feb-2021) -- End

                                    objRetirementProcessTran._Expenseunkid = CInt(.Item("expenseunkid"))
                                    objRetirementProcessTran._Expense_Remark = .Item("expense_remark").ToString()
                                    objRetirementProcessTran._Bal_Amount = mdecBalance
                                    objRetirementProcessTran._Employeeunkid = mintEmployeeId
                                    objRetirementProcessTran._Countryunkid = CInt(.Item("countryunkid"))
                                    objRetirementProcessTran._Base_Countryunkid = CInt(.Item("base_countryunkid"))
                                    objRetirementProcessTran._Base_Amount = mdecBalance / CDec(.Item("exchange_rate"))
                                    objRetirementProcessTran._Exchangerateunkid = CInt(.Item("exchangerateunkid"))
                                    objRetirementProcessTran._Exchange_Rate = CDec(.Item("exchange_rate"))
                                    objRetirementProcessTran._Periodunkid = -1
                                    objRetirementProcessTran._Userunkid = mintUserunkid
                                    objRetirementProcessTran._Approveremployeeunkid = mintApproveremployeeunkid
                                    objRetirementProcessTran._Crapproverunkid = mintApproverunkid
                                    objRetirementProcessTran._WebFormName = mstrFormName
                                    objRetirementProcessTran._WebHostName = mstrWebHostName
                                    objRetirementProcessTran._WebClientIP = mstrWebClientIP

                                    If objRetirementProcessTran.Insert(mdtClaimRetirementTran.Rows(i), mdtApprovalDate, objDataOperation) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                            Case "D"


                                strQ = " SELECT @claimretirementapprovaltranunkid =  ISNULL(claimretirementapprovaltranunkid,0)  " & _
                                         " FROM cmclaim_retirement_approval_tran WHERE isvoid = 0 AND claimretirementtranunkid = @claimretirementtranunkid  " & _
                                         " AND crmasterunkid = @crmasterunkid AND expenseunkid = @expenseunkid AND  approverunkid = @approverunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@claimretirementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("claimretirementtranunkid")))
                                objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("crmasterunkid")))
                                objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("expenseunkid")))
                                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                                objDataOperation.AddParameter("@claimretirementapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementapprovaltranunkid, ParameterDirection.InputOutput)
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintClaimretirementapprovaltranunkid = objDataOperation.GetParameterValue("@claimretirementapprovaltranunkid")


                                strQ = " UPDATE cmclaim_retirement_approval_tran SET " & _
                                          "  isvoid = @isvoid" & _
                                          ", voiduserunkid = @voiduserunkid" & _
                                          ", voiddatetime = Getdate()" & _
                                          ", voidreason = @voidreason" & _
                                          " WHERE claimretirementapprovaltranunkid = @claimretirementapprovaltranunkid AND isvoid = 0 "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@claimretirementapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementapprovaltranunkid)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintClaimretirementtranunkid = CInt(.Item("claimretirementtranunkid"))
                                mintClaimretirementunkid = CInt(.Item("claimretirementunkid"))
                                mintCrmasterunkid = CInt(.Item("crmasterunkid"))

                                If ATInsertUpdateClaimRetirement_Approval(objDataOperation, 3, mdtClaimRetirementTran.Rows(i), mdtApprovalDate) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                        End Select

                    End If

                End With

            Next

            If blnIsLastApprover Then

                'Pinkal (04-Jul-2020) -- Start
                'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
                'Dim mdecTotalImprestAmt As Decimal = mdtClaimRetirementTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "").Sum(Function(x) x.Field(Of Decimal)("amount"))
                'Pinkal (04-Jul-2020) -- End
                If UpdateClaimRetirementStatusByApprover(objDataOperation, mintStatusunkid, mintClaimretirementunkid, mintCrmasterunkid, mdecTotalImprestAmt, mintApproverunkid, mintApproveremployeeunkid, mintUserunkid, mstrRejcectRemark) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If objDoOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdate_ClaimRetirement_Approval; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cmclaim_retirement_approval_tran) </purpose>
    Public Function DeleteRetirementApproval(ByVal objDataOperation As clsDataOperation, ByVal mstrVoidReason As String, ByVal mdtImprest As DataTable) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            strQ = " UPDATE cmclaim_retirement_approval_tran SET " & _
                      " isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = GetDate()" & _
                      ", voidreason = @voidreason" & _
                      " WHERE claimretirementapprovaltranunkid = @claimretirementapprovaltranunkid AND isvoid = 0 "

            If mdtImprest IsNot Nothing AndAlso mdtImprest.Rows.Count Then

                For Each dr As DataRow In mdtImprest.Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@claimretirementapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("claimretirementapprovaltranunkid")))
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason)
                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If ATInsertUpdateClaimRetirement_Approval(objDataOperation, 3, dr, Nothing) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DeleteRetirementApproval; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@claimretirementapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  claimretirementapprovaltranunkid " & _
              ", claimretirementtranunkid " & _
              ", claimretirementunkid " & _
              ", crmasterunkid " & _
              ", approvaldate " & _
              ", expenseunkid " & _
              ", secrouteunkid " & _
              ", costingunkid " & _
              ", unitprice " & _
              ", quantity " & _
              ", amount " & _
              ", expense_remark " & _
              ", costcenterunkid " & _
              ", countryunkid " & _
              ", base_countryunkid " & _
              ", base_amount " & _
              ", exchangerateunkid " & _
              ", exchange_rate " & _
              ", approveremployeeunkid " & _
              ", approverunkid " & _
              ", statusunkid " & _
              ", visibleid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", iscancel " & _
              ", canceluserunkid " & _
              ", cancel_remark " & _
              ", cancel_datetime " & _
             "FROM cmclaim_retirement_approval_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND claimretirementapprovaltranunkid <> @claimretirementapprovaltranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@claimretirementapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cmclaim_retirement_approval_tran) </purpose>
    Public Function ATInsertUpdateClaimRetirement_Approval(ByVal objDataOperation As clsDataOperation, ByVal xAuditType As Integer _
                                                                                  , ByVal dr As DataRow, ByVal mdtApprovaldate As DateTime) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@claimretirementapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementapprovaltranunkid.ToString)
            objDataOperation.AddParameter("@claimretirementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementtranunkid.ToString)
            objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementunkid.ToString)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrmasterunkid.ToString)

            If mdtApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate)
            Else
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("expenseunkid").ToString)
            objDataOperation.AddParameter("@unitprice", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dr("unitprice").ToString)
            objDataOperation.AddParameter("@quantity", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dr("unitprice").ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dr("amount").ToString)
            objDataOperation.AddParameter("@expense_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dr("expense_remark").ToString)
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("costcenterunkid").ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("countryunkid").ToString)
            objDataOperation.AddParameter("@base_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("base_countryunkid").ToString)
            objDataOperation.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, dr("base_amount").ToString)
            objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("exchangerateunkid").ToString)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, dr("exchange_rate").ToString)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveremployeeunkid.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@visibleid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleid.ToString)
            objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dr("iscancel").ToString)
            objDataOperation.AddParameter("@cancel_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dr("cancel_remark").ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "INSERT INTO atcmclaim_retirement_approval_tran ( " & _
                      "  claimretirementapprovaltranunkid " & _
                      ", claimretirementtranunkid " & _
                      ", claimretirementunkid " & _
                      ", crmasterunkid " & _
                      ", approvaldate " & _
                      ", expenseunkid " & _
                      ", unitprice " & _
                      ", quantity " & _
                      ", amount " & _
                      ", expense_remark " & _
                      ", costcenterunkid " & _
                      ", countryunkid " & _
                      ", base_countryunkid " & _
                      ", base_amount " & _
                      ", exchangerateunkid " & _
                      ", exchange_rate " & _
                      ", approveremployeeunkid " & _
                      ", approverunkid " & _
                      ", statusunkid " & _
                      ", visibleid " & _
                      ", iscancel " & _
                      ", cancel_remark " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", isweb " & _
                    ") VALUES (" & _
                      "  @claimretirementapprovaltranunkid " & _
                      ", @claimretirementtranunkid " & _
                      ", @claimretirementunkid " & _
                      ", @crmasterunkid " & _
                      ", @approvaldate " & _
                      ", @expenseunkid " & _
                      ", @unitprice " & _
                      ", @quantity " & _
                      ", @amount " & _
                      ", @expense_remark " & _
                      ", @costcenterunkid " & _
                      ", @countryunkid " & _
                      ", @base_countryunkid " & _
                      ", @base_amount " & _
                      ", @exchangerateunkid " & _
                      ", @exchange_rate " & _
                      ", @approveremployeeunkid " & _
                      ", @approverunkid " & _
                      ", @statusunkid " & _
                      ", @visibleid " & _
                      ", @iscancel " & _
                      ", @cancel_remark " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", getdate() " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      ", @isweb " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ATInsertUpdateClaimRetirement_Approval; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cmclaim_retirement_approval_tran) </purpose>
    Public Function GetClaimRetirementExternalApproverList(Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal strList As String = "List") As DataSet
        Dim objDataOperation As clsDataOperation
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsApprover As New DataSet

        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try

            If strList.Trim.Length <= 0 Then strList = "List"

            StrQ = " SELECT DISTINCT " & _
                       "     ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                       "    ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                       "    ,ISNULL(cffinancial_year_tran.yearunkid,0) AS yearunkid " & _
                       "    ,ISNULL(EffDt.key_value,'') AS EDate " & _
                       "    ,ISNULL(UC.key_value,'') AS ModeSet " & _
                       " FROM cmexpapprover_master " & _
                       "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = cmexpapprover_master.employeeunkid " & _
                       "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                       " LEFT JOIN " & _
                       " ( " & _
                       "    SELECT " & _
                       "         cfconfiguration.companyunkid " & _
                       "        ,cfconfiguration.key_value " & _
                       "    FROM hrmsConfiguration..cfconfiguration " & _
                       "    WHERE UPPER(cfconfiguration.key_name) IN ('EMPLOYEEASONDATE') " & _
                       " ) AS EffDt ON EffDt.companyunkid = cffinancial_year_tran.companyunkid " & _
                       " LEFT JOIN " & _
                       " ( " & _
                       "    SELECT " & _
                       "         cfconfiguration.companyunkid " & _
                       "        ,cfconfiguration.key_value " & _
                       "    FROM hrmsConfiguration..cfconfiguration " & _
                       "    WHERE UPPER(cfconfiguration.key_name) IN ('USERACCESSMODESETTING') " & _
                       " ) AS UC ON UC.companyunkid = cffinancial_year_tran.companyunkid " & _
                       " WHERE cmexpapprover_master.isvoid = 0 AND cmexpapprover_master.isswap = 0 " & _
                       " AND cmexpapprover_master.isexternalapprover = 1 AND cmexpapprover_master.expensetypeid = @expensetypeid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, enExpenseType.EXP_IMPREST)
            dsApprover = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetClaimRetirementExternalApproverList; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsApprover
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Private Function UpdateClaimRetirementStatusByApprover(ByVal objDataOperation As clsDataOperation, ByVal intStatusId As Integer _
                                                                               , ByVal xClaimRetirementId As Integer, ByVal xClaimRequestId As Integer, ByVal xImprest_amount As Decimal _
                                                                               , ByVal xApproverId As Integer, ByVal xApproverEmpId As Integer, ByVal intUserID As Integer _
                                                                               , Optional ByVal mstrRejectRemark As String = "") As Boolean
        Dim mblnFlag As Boolean = False
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = " UPDATE cmclaim_retirement_master SET statusunkid = @statusunkid "

            If intStatusId = 1 Then 'Approved
                strQ &= ",imprest_amount = @imprest_amount,balance = claim_amount -@imprest_amount ,approverunkid  = @approverunkid, approveremployeeunkid = @approveremployeeunkid "
            End If

            If mstrRejectRemark.Trim.Length > 0 Then
                strQ &= ",retirement_remark = @remark "
            End If

            strQ &= " WHERE claimretirementunkid = @claimretirementunkid AND crmasterunkid = @crmasterunkid AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusId)
            objDataOperation.AddParameter("@imprest_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, xImprest_amount)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xApproverId)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xApproverEmpId)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrRejectRemark)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xClaimRequestId)
            objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xClaimRetirementId)
            objDataOperation.ExecNonQuery(strQ)


            Dim objClaimRetirementMst As New clsclaim_retirement_master
            objClaimRetirementMst._Claimretirementunkid(objDataOperation) = xClaimRetirementId
            objClaimRetirementMst._Userunkid = mintUserunkid
            objClaimRetirementMst._WebClientIP = mstrWebClientIP
            objClaimRetirementMst._WebHostName = mstrWebHostName
            objClaimRetirementMst._FormName = mstrFormName
            objClaimRetirementMst._IsWeb = mblnIsWeb


            If objClaimRetirementMst.ATInsertClaim_Retirement(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mblnFlag = True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateClaimRetirementStatusByApprover; Module Name: " & mstrModuleName)
        End Try
        Return mblnFlag
    End Function


    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub SendMailToApprover(ByVal xDatabaseName As String, ByVal strEmployeeAsOnDate As String, ByVal xClaimRetirementId As Integer, ByVal mstrRetirementFormNo As String, ByVal intEmpId As Integer _
                                                  , ByVal intPriority As Integer, ByVal intStatusID As Integer _
                                                  , ByVal mstrPriorityFilter As String, Optional ByVal intCompanyUnkId As Integer = 0, Optional ByVal strArutiSelfServiceURL As String = "" _
                                                  , Optional ByVal iLoginTypeId As Integer = 0, Optional ByVal iLoginEmployeeId As Integer = 0 _
                                                  , Optional ByVal iUserId As Integer = 0, Optional ByVal mstrWebFrmName As String = "" _
                                                  , Optional ByVal objDoOperation As clsDataOperation = Nothing)

        Dim strLink As String
        Try

            If intCompanyUnkId <= 0 Then intCompanyUnkId = Company._Object._Companyunkid
            If strArutiSelfServiceURL = "" Then strArutiSelfServiceURL = ConfigParameter._Object._ArutiSelfServiceURL

            Dim objNet As New clsNetConnectivity
            If objNet._Conected = False Then Exit Sub

            If intStatusID <> 1 Then Exit Sub 'APPROVED STATUS


            Dim mstrFiler As String = "cmclaim_retirement_master.employeeunkid = " & intEmpId & " AND claimretirementno = '" & mstrRetirementFormNo & "' " & IIf(mstrPriorityFilter.Trim.Length > 0, " AND " & mstrPriorityFilter, "")

            Dim dsPedingList As DataSet = GetClaimRetirementApproverExpesneList("List", False, xDatabaseName, mintUserunkid, strEmployeeAsOnDate, True, -1, mstrFiler, xClaimRetirementId, objDoOperation)

            Dim objMail As New clsSendMail

            If dsPedingList Is Nothing OrElse dsPedingList.Tables(0).Rows.Count <= 0 Then Exit Sub

            If dsPedingList.Tables(0).Rows.Count > 0 Then
                Dim xApproverID As Integer = -1

                For i As Integer = 0 To dsPedingList.Tables(0).Rows.Count - 1

                    If xApproverID <> CInt(dsPedingList.Tables(0).Rows(i)("approverunkid")) Then
                        strLink = strArutiSelfServiceURL & "/Claim_Retirement/wPgClaimRetirementApproval.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyUnkId.ToString & "|" & dsPedingList.Tables(0).Rows(i)("mapuserunkid").ToString & "|" & intEmpId.ToString & "|" & dsPedingList.Tables(0).Rows(i)("approverunkid").ToString & "|" & dsPedingList.Tables(0).Rows(i)("claimretirementunkid").ToString & "|" & dsPedingList.Tables(0).Rows(i)("claimretirementapprovaltranunkid").ToString))

                        objMail._Subject = Language.getMessage(mstrModuleName, 1, "Notification for approving Claim Retirement form")

                        Dim strMessage As String = ""

                        strMessage = "<HTML> <BODY>"

                        strMessage &= Language.getMessage(mstrModuleName, 2, "Dear") & " " & getTitleCase(dsPedingList.Tables(0).Rows(i)("approvername").ToString()) & ", <BR><BR>"
                        strMessage &= Language.getMessage(mstrModuleName, 3, "This is the notification for approving claim retirement application no") & " <B>(" & mstrRetirementFormNo.Trim() & ")</B> " & _
                                                Language.getMessage(mstrModuleName, 4, " of ") & " " & getTitleCase(dsPedingList.Tables(0).Rows(i)("employeename").ToString()) & "."
                        strMessage &= Language.getMessage(mstrModuleName, 5, "Please click on the following link to approve claim retirement.")
                        strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"

                        strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                        strMessage &= "</BODY></HTML>"

                        objMail._Message = strMessage
                        objMail._ToEmail = dsPedingList.Tables(0).Rows(i)("approveremail").ToString()

                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        If mstrWebFrmName.Trim.Length > 0 Then
                            objMail._Form_Name = mstrWebFrmName
                            objMail._WebClientIP = mstrWebClientIP
                            objMail._WebHostName = mstrWebHostName
                        End If
                        objMail._LogEmployeeUnkid = iLoginEmployeeId
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = iUserId
                        objMail._SenderAddress = IIf(dsPedingList.Tables(0).Rows(i)("approveremail") = "", dsPedingList.Tables(0).Rows(i)("approvername"), dsPedingList.Tables(0).Rows(i)("approveremail"))
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.CLAIM_RETIREMENT_MGT
                        objMail.SendMail(intCompanyUnkId)
                        xApproverID = CInt(dsPedingList.Tables(0).Rows(i)("approverunkid"))
                    Else
                        Exit For
                    End If
                Next

            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendMailToApprover; Module Name: " & mstrModuleName)
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
            Language.setMessage(mstrModuleName, 1, "Notification for approving Claim Retirement form")
            Language.setMessage(mstrModuleName, 2, "Dear")
            Language.setMessage(mstrModuleName, 3, "This is the notification for approving claim retirement application no")
            Language.setMessage(mstrModuleName, 4, " of")
            Language.setMessage(mstrModuleName, 5, "Please click on the following link to approve claim retirement.")
            Language.setMessage(mstrModuleName, 6, "Approved By :")
            Language.setMessage(mstrModuleName, 7, "Rejected By :")

			Language.setMessage("clsExpCommonMethods", 2, "Leave")
			Language.setMessage("clsExpCommonMethods", 3, "Medical")
            Language.setMessage("clsExpCommonMethods", 4, "Training")
            Language.setMessage("clsExpCommonMethods", 6, "Quantity")
			Language.setMessage("clsExpCommonMethods", 7, "Amount")
			Language.setMessage("clsExpCommonMethods", 8, "Miscellaneous")
			Language.setMessage("clsExpCommonMethods", 9, "Imprest")
			Language.setMessage("clsMasterData", 110, "Approved")
			Language.setMessage("clsMasterData", 111, "Pending")
			Language.setMessage("clsMasterData", 112, "Rejected")
			Language.setMessage("clsMasterData", 115, "Cancelled")
			
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class