﻿'************************************************************************************************************************************
'Class Name : clsretire_process_tran.vb
'Purpose    :
'Date       :04/07/2020
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports Aruti.Data


''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsretire_process_tran

    Private Shared ReadOnly mstrModuleName As String = "clsretire_process_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintCrretirementprocessunkid As Integer
    Private mintClaimretirementapprovaltranunkid As Integer
    Private mintClaimretirementtranunkid As Integer
    Private mintClaimretirementunkid As Integer

    'Pinkal (10-Feb-2021) -- Start
    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
    Private mintCrmasterunkid As Integer
    Private mintClaimApprovalTranId As Integer = 0
    'Pinkal (10-Feb-2021) -- End

    Private mintEmployeeunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintExpenseunkid As Integer
    Private mdecBal_Amount As Decimal
    Private mintCountryunkid As Integer
    Private mintBase_Countryunkid As Integer
    Private mdecBase_Amount As Decimal
    Private mintExchangerateunkid As Integer
    Private mdecExchange_Rate As Decimal
    Private mintTranheadunkid As Integer
    Private mintApproveremployeeunkid As Integer
    Private mintCrapproverunkid As Integer
    Private mstrExpense_Remark As String = String.Empty
    Private mblnIsposted As Boolean
    Private mblnIsprocessed As Boolean
    Private mintProcessuserunkid As Integer
    Private mdtProcessdatetime As Date
    Private mdecProcessedrate As Decimal
    Private mdecProcessedamt As Decimal
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = ""
    Private mstrWebhostName As String = ""
    Private mblnIsWeb As Boolean = False
#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crretirementprocessunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Crretirementprocessunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintCrretirementprocessunkid
        End Get
        Set(ByVal value As Integer)
            mintCrretirementprocessunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set claimretirementapprovaltranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Claimretirementapprovaltranunkid() As Integer
        Get
            Return mintClaimretirementapprovaltranunkid
        End Get
        Set(ByVal value As Integer)
            mintClaimretirementapprovaltranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set claimretirementtranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Claimretirementtranunkid() As Integer
        Get
            Return mintClaimretirementtranunkid
        End Get
        Set(ByVal value As Integer)
            mintClaimretirementtranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set claimretirementunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Claimretirementunkid() As Integer
        Get
            Return mintClaimretirementunkid
        End Get
        Set(ByVal value As Integer)
            mintClaimretirementunkid = value
        End Set
    End Property

    'Pinkal (10-Feb-2021) -- Start
    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.

    ''' <summary>
    ''' Purpose: Get or Set Crmasterunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Crmasterunkid() As Integer
        Get
            Return mintCrmasterunkid
        End Get
        Set(ByVal value As Integer)
            mintCrmasterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClaimApprovalTranId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClaimApprovalTranId() As Integer
        Get
            Return mintClaimApprovalTranId
        End Get
        Set(ByVal value As Integer)
            mintClaimApprovalTranId = value
        End Set
    End Property

    'Pinkal (10-Feb-2021) -- End


    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set expenseunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Expenseunkid() As Integer
        Get
            Return mintExpenseunkid
        End Get
        Set(ByVal value As Integer)
            mintExpenseunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set bal_amount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Bal_Amount() As Decimal
        Get
            Return mdecBal_Amount
        End Get
        Set(ByVal value As Decimal)
            mdecBal_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set base_countryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Base_Countryunkid() As Integer
        Get
            Return mintBase_Countryunkid
        End Get
        Set(ByVal value As Integer)
            mintBase_Countryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set base_amount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Base_Amount() As Decimal
        Get
            Return mdecBase_Amount
        End Get
        Set(ByVal value As Decimal)
            mdecBase_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set exchangerateunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Exchangerateunkid() As Integer
        Get
            Return mintExchangerateunkid
        End Get
        Set(ByVal value As Integer)
            mintExchangerateunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set exchange_rate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Exchange_Rate() As Decimal
        Get
            Return mdecExchange_Rate
        End Get
        Set(ByVal value As Decimal)
            mdecExchange_Rate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approveremployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approveremployeeunkid() As Integer
        Get
            Return mintApproveremployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintApproveremployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crapproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Crapproverunkid() As Integer
        Get
            Return mintCrapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintCrapproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set expense_remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Expense_Remark() As String
        Get
            Return mstrExpense_Remark
        End Get
        Set(ByVal value As String)
            mstrExpense_Remark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isposted
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isposted() As Boolean
        Get
            Return mblnIsposted
        End Get
        Set(ByVal value As Boolean)
            mblnIsposted = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isprocessed
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isprocessed() As Boolean
        Get
            Return mblnIsprocessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsprocessed = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processuserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Processuserunkid() As Integer
        Get
            Return mintProcessuserunkid
        End Get
        Set(ByVal value As Integer)
            mintProcessuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processdatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Processdatetime() As Date
        Get
            Return mdtProcessdatetime
        End Get
        Set(ByVal value As Date)
            mdtProcessdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processedrate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Processedrate() As Decimal
        Get
            Return mdecProcessedrate
        End Get
        Set(ByVal value As Decimal)
            mdecProcessedrate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processedamt
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Processedamt() As Decimal
        Get
            Return mdecProcessedamt
        End Get
        Set(ByVal value As Decimal)
            mdecProcessedamt = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Webhostname
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebhostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

#End Region


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()


        Try
            strQ = "SELECT " & _
                      "  crretirementprocessunkid " & _
                      ", claimretirementapprovaltranunkid " & _
                      ", claimretirementtranunkid " & _
                      ", claimretirementunkid " & _
                      ", ISNULL(crmasterunkid,0) AS crmasterunkid " & _
                      ", ISNULL(crapprovaltranunkid,0) AS crapprovaltranunkid " & _
                      ", employeeunkid " & _
                      ", periodunkid " & _
                      ", expenseunkid " & _
                      ", bal_amount " & _
                      ", countryunkid " & _
                      ", base_countryunkid " & _
                      ", base_amount " & _
                      ", exchangerateunkid " & _
                      ", exchange_rate " & _
                      ", tranheadunkid " & _
                      ", approveremployeeunkid " & _
                      ", crapproverunkid " & _
                      ", expense_remark " & _
                      ", isposted " & _
                      ", isprocessed " & _
                      ", processuserunkid " & _
                      ", processdatetime " & _
                      ", processedrate " & _
                      ", processedamt " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     "FROM cmretire_process_tran " & _
                     "WHERE crretirementprocessunkid = @crretirementprocessunkid "

            objDataOperation.AddParameter("@crretirementprocessunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrretirementprocessunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCrretirementprocessunkid = CInt(dtRow.Item("crretirementprocessunkid"))
                mintClaimretirementapprovaltranunkid = CInt(dtRow.Item("claimretirementapprovaltranunkid"))
                mintClaimretirementtranunkid = CInt(dtRow.Item("claimretirementtranunkid"))
                mintClaimretirementunkid = CInt(dtRow.Item("claimretirementunkid"))

                'Pinkal (10-Feb-2021) -- Start
                'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                mintCrmasterunkid = CInt(dtRow.Item("crmasterunkid"))
                mintClaimApprovalTranId = CInt(dtRow.Item("crapprovaltranunkid"))
                'Pinkal (10-Feb-2021) -- End

                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintExpenseunkid = CInt(dtRow.Item("expenseunkid"))
                mdecBal_Amount = CDec(dtRow.Item("bal_amount"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mintBase_Countryunkid = CInt(dtRow.Item("base_countryunkid"))
                mdecBase_Amount = CDec(dtRow.Item("base_amount"))
                mintExchangerateunkid = CInt(dtRow.Item("exchangerateunkid"))
                mdecExchange_Rate = CDec(dtRow.Item("exchange_rate"))
                mintTranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                mintApproveremployeeunkid = CInt(dtRow.Item("approveremployeeunkid"))
                mintCrapproverunkid = CInt(dtRow.Item("crapproverunkid"))
                mstrExpense_Remark = dtRow.Item("expense_remark").ToString
                mblnIsposted = CBool(dtRow.Item("isposted"))
                mblnIsprocessed = CBool(dtRow.Item("isprocessed"))
                mintProcessuserunkid = CInt(dtRow.Item("processuserunkid"))

                If IsDBNull(dtRow.Item("processdatetime")) = False Then
                    mdtProcessdatetime = dtRow.Item("processdatetime")
                End If

                mdecProcessedrate = dtRow.Item("processedrate")
                mdecProcessedamt = dtRow.Item("processedamt")
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal mblnAddGrp As Boolean = True _
                                  , Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intIsposted As Integer = -1 _
                                  , Optional ByVal mstrFilter As String = "", Optional ByVal xDataOp As clsDataOperation = Nothing) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            If mblnAddGrp Then

                'S.SANDEEP |25-FEB-2022| -- START
                'ISSUE : OLD-575
                'strQ = "SELECT " & _
                '          "  CAST(1 AS BIT) As Isgroup " & _
                '          ", CAST(0 AS BIT) As Ischecked " & _
                '          ", CAST(0 AS BIT) As ischange " & _
                '          ", 0 AS crretirementprocessunkid " & _
                '          ", 0 AS claimretirementapprovaltranunkid " & _
                '          ", cmretire_process_tran.claimretirementtranunkid " & _
                '          ", cmretire_process_tran.claimretirementunkid " & _
                '          ", ISNULL(cmretire_process_tran.crmasterunkid,0) AS crmasterunkid " & _
                '          ", ISNULL(cmretire_process_tran.crapprovaltranunkid,0) AS crapprovaltranunkid " & _
                '          ", cmretire_process_tran.employeeunkid " & _
                '          ", 0 AS periodunkid " & _
                '          ", cmretire_process_tran.expenseunkid " & _
                '          ", cmclaim_retirement_master.claimretirementno " & _
                '          ", CONVERT(CHAR(8),cmclaim_retirement_master.transactiondate,112) AS transactiondate " & _
                '          ", ISNULL(hremployee_master.firstname,'') + '  ' + ISNULL(hremployee_master.othername,'')  + '  ' +  ISNULL(hremployee_master.surname,'') AS Employee " & _
                '          ", cmclaim_retirement_master.claimretirementno + '  -  ' + ISNULL(hremployee_master.firstname,'') + '  ' + ISNULL(hremployee_master.othername,'')  + '  ' +  ISNULL(hremployee_master.surname,'') AS Retirement " & _
                '          ", '' AS Expense " & _
                '          ", '' AS Period " & _
                '          ", 0 AS tranheadunkid " & _
                '          ", '' AS tranhead " & _
                '          ", 0.00 as bal_amount " & _
                '          ", 0 AS countryunkid " & _
                '          ", 0 AS base_countryunkid " & _
                '          ", 0.00 AS base_amount " & _
                '          ", 0 AS exchangerateunkid  " & _
                '          ", 0.00 AS exchange_rate " & _
                '          ", '' AS currency_sign " & _
                '          ", 0 AS default_costcenterunkid " & _
                '          ", cmretire_process_tran.processedrate " & _
                '          ", cmretire_process_tran.processedamt " & _
                '          ", cmretire_process_tran.expense_remark " & _
                '          ", cmretire_process_tran.approveremployeeunkid " & _
                '          ", cmretire_process_tran.crapproverunkid " & _
                '          ", cmretire_process_tran.userunkid " & _
                '          ", cmretire_process_tran.isvoid " & _
                '          ", cmretire_process_tran.voiduserunkid " & _
                '          ", cmretire_process_tran.voiddatetime " & _
                '          ", cmretire_process_tran.voidreason " & _
                '          ", cmretire_process_tran.isposted " & _
                '          ", cmretire_process_tran.isprocessed " & _
                '          ", cmretire_process_tran.processuserunkid " & _
                '          ", cmretire_process_tran.processdatetime " & _
                '          " FROM cmretire_process_tran " & _
                '          " JOIN cmclaim_retirement_master ON cmclaim_retirement_master.claimretirementunkid = cmretire_process_tran.claimretirementunkid AND cmclaim_retirement_master.isvoid = 0 " & _
                '          " JOIN hremployee_master ON hremployee_master.employeeunkid = cmretire_process_tran.employeeunkid " & _
                '          " JOIN cmexpense_master ON cmexpense_master.expenseunkid  = cmretire_process_tran.expenseunkid AND cmexpense_master.isactive = 1 " & _
                '          " WHERE cmretire_process_tran.isprocessed = 0 "

                strQ = "SELECT DISTINCT " & _
                          "  CAST(1 AS BIT) As Isgroup " & _
                          ", CAST(0 AS BIT) As Ischecked " & _
                          ", CAST(0 AS BIT) As ischange " & _
                          ", 0 AS crretirementprocessunkid " & _
                          ", 0 AS claimretirementapprovaltranunkid " & _
                          ", 0 AS claimretirementtranunkid " & _
                          ", cmretire_process_tran.claimretirementunkid " & _
                          ", ISNULL(cmretire_process_tran.crmasterunkid,0) AS crmasterunkid " & _
                          ", ISNULL(cmretire_process_tran.crapprovaltranunkid,0) AS crapprovaltranunkid " & _
                          ", cmretire_process_tran.employeeunkid " & _
                          ", 0 AS periodunkid " & _
                          ", 0 AS expenseunkid " & _
                          ", cmclaim_retirement_master.claimretirementno " & _
                          ", CONVERT(CHAR(8),cmclaim_retirement_master.transactiondate,112) AS transactiondate " & _
                          ", ISNULL(hremployee_master.firstname,'') + '  ' + ISNULL(hremployee_master.othername,'')  + '  ' +  ISNULL(hremployee_master.surname,'') AS Employee " & _
                          ", cmclaim_retirement_master.claimretirementno + '  -  ' + ISNULL(hremployee_master.firstname,'') + '  ' + ISNULL(hremployee_master.othername,'')  + '  ' +  ISNULL(hremployee_master.surname,'') AS Retirement " & _
                          ", '' AS Expense " & _
                          ", '' AS Period " & _
                          ", 0 AS tranheadunkid " & _
                          ", '' AS tranhead " & _
                          ", 0.00 as bal_amount " & _
                          ", 0 AS countryunkid " & _
                          ", 0 AS base_countryunkid " & _
                          ", 0.00 AS base_amount " & _
                          ", 0 AS exchangerateunkid  " & _
                          ", 0.00 AS exchange_rate " & _
                          ", '' AS currency_sign " & _
                          ", 0 AS default_costcenterunkid " & _
                          ", cmretire_process_tran.processedrate " & _
                          ", cmretire_process_tran.processedamt " & _
                          ", '' AS expense_remark " & _
                          ", cmretire_process_tran.approveremployeeunkid " & _
                          ", cmretire_process_tran.crapproverunkid " & _
                          ", cmretire_process_tran.userunkid " & _
                          ", cmretire_process_tran.isvoid " & _
                          ", cmretire_process_tran.voiduserunkid " & _
                          ", cmretire_process_tran.voiddatetime " & _
                          ", cmretire_process_tran.voidreason " & _
                          ", cmretire_process_tran.isposted " & _
                          ", cmretire_process_tran.isprocessed " & _
                          ", cmretire_process_tran.processuserunkid " & _
                          ", cmretire_process_tran.processdatetime " & _
                          " FROM cmretire_process_tran " & _
                          " JOIN cmclaim_retirement_master ON cmclaim_retirement_master.claimretirementunkid = cmretire_process_tran.claimretirementunkid AND cmclaim_retirement_master.isvoid = 0 " & _
                          " JOIN hremployee_master ON hremployee_master.employeeunkid = cmretire_process_tran.employeeunkid " & _
                          " JOIN cmexpense_master ON cmexpense_master.expenseunkid  = cmretire_process_tran.expenseunkid AND cmexpense_master.isactive = 1 " & _
                          " WHERE cmretire_process_tran.isprocessed = 0 "
                'S.SANDEEP |25-FEB-2022| -- END
                
                'Sohail (07 Jun 2021) - [default_costcenterunkid]

                If blnOnlyActive Then
                    strQ &= " AND cmretire_process_tran.isvoid = 0 "
                End If

                If intIsposted = 0 OrElse intIsposted = 1 Then
                    strQ &= " AND cmretire_process_tran.isposted = " & intIsposted & " "
                End If

                If mstrFilter.Trim.Length > 0 Then
                    strQ &= " AND " & mstrFilter
                End If

                strQ &= " UNION "


            End If

            strQ &= "SELECT " & _
                      "  CAST(0 AS BIT) As Isgroup " & _
                      ", CAST(0 AS BIT) As Ischecked " & _
                      ", CAST(0 AS BIT) As ischange " & _
                      ", cmretire_process_tran.crretirementprocessunkid " & _
                      ", cmretire_process_tran.claimretirementapprovaltranunkid " & _
                      ", cmretire_process_tran.claimretirementtranunkid " & _
                      ", cmretire_process_tran.claimretirementunkid " & _
                      ", ISNULL(cmretire_process_tran.crmasterunkid,0) as crmasterunkid " & _
                      ", ISNULL(cmretire_process_tran.crapprovaltranunkid,0) AS crapprovaltranunkid " & _
                      ", cmretire_process_tran.employeeunkid " & _
                      ", cmretire_process_tran.periodunkid " & _
                      ", cmretire_process_tran.expenseunkid " & _
                      ", cmclaim_retirement_master.claimretirementno " & _
                      ", CONVERT(CHAR(8),cmclaim_retirement_master.transactiondate,112) AS transactiondate " & _
                      ", ISNULL(hremployee_master.firstname,'') + '  ' + ISNULL(hremployee_master.othername,'')  + '  ' +  ISNULL(hremployee_master.surname,'') AS Employee " & _
                      ", '' AS Retirement " & _
                      ", ISNULL(cmexpense_master.name,'') AS Expense " & _
                      ", ISNULL(cfcommon_period_tran.period_name,'') AS Period " & _
                      ", ISNULL(prtranhead_master.tranheadunkid,'') AS tranheadunkid " & _
                      ", ISNULL(prtranhead_master.trnheadname,'') AS tranhead " & _
                      ", cmretire_process_tran.bal_amount " & _
                      ", ISNULL(cmretire_process_tran.countryunkid,0) AS countryunkid " & _
                      ", ISNULL(cmretire_process_tran.base_countryunkid ,0) AS base_countryunkid " & _
                      ", ISNULL(base_amount,0) AS base_amount " & _
                      ", ISNULL(cmretire_process_tran.exchangerateunkid,0) AS exchangerateunkid  " & _
                      ", ISNULL(cmretire_process_tran.exchange_rate,0) AS exchange_rate " & _
                      ", ISNULL(cfexchange_rate.currency_sign,'') AS currency_sign " & _
                      ", ISNULL(cmexpense_master.default_costcenterunkid, 0) AS default_costcenterunkid " & _
                      ", cmretire_process_tran.processedrate " & _
                      ", cmretire_process_tran.processedamt " & _
                      ", cmretire_process_tran.expense_remark " & _
                      ", cmretire_process_tran.approveremployeeunkid " & _
                      ", cmretire_process_tran.crapproverunkid " & _
                      ", cmretire_process_tran.userunkid " & _
                      ", cmretire_process_tran.isvoid " & _
                      ", cmretire_process_tran.voiduserunkid " & _
                      ", cmretire_process_tran.voiddatetime " & _
                      ", cmretire_process_tran.voidreason " & _
                      ", cmretire_process_tran.isposted " & _
                      ", cmretire_process_tran.isprocessed " & _
                      ", cmretire_process_tran.processuserunkid " & _
                      ", cmretire_process_tran.processdatetime " & _
                      " FROM cmretire_process_tran " & _
                      " JOIN cmclaim_retirement_master ON cmclaim_retirement_master.claimretirementunkid = cmretire_process_tran.claimretirementunkid AND cmclaim_retirement_master.isvoid = 0 " & _
                      " JOIN hremployee_master ON hremployee_master.employeeunkid = cmretire_process_tran.employeeunkid " & _
                      " JOIN cmexpense_master ON cmexpense_master.expenseunkid  = cmretire_process_tran.expenseunkid AND cmexpense_master.isactive = 1 " & _
                      " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid  = cmretire_process_tran.periodunkid " & _
                      " LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = cmretire_process_tran.tranheadunkid AND prtranhead_master.isvoid  = 0 " & _
                      " LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = ISNULL(cmretire_process_tran.exchangerateunkid,0) " & _
                      " WHERE cmretire_process_tran.isprocessed = 0 "
            'Sohail (07 Jun 2021) - [default_costcenterunkid]

            If blnOnlyActive Then
                strQ &= " AND cmretire_process_tran.isvoid = 0 "
            End If

            If intIsposted = 0 OrElse intIsposted = 1 Then
                strQ &= " AND cmretire_process_tran.isposted = " & intIsposted & " "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If


            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            strQ &= " ORDER BY claimretirementunkid,transactiondate DESC,Isgroup DESC "
            'Pinkal (10-Feb-2021) -- End



            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cmclaim_process_tran) </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function Insert(ByVal drRow As DataRow, Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
    Public Function Insert(ByVal drRow As DataRow, ByVal dtCurrentDateAndTime As DateTime, Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@claimretirementapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementapprovaltranunkid)
            objDataOperation.AddParameter("@claimretirementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("claimretirementtranunkid").ToString)
            objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("claimretirementunkid").ToString)
            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrmasterunkid)
            objDataOperation.AddParameter("@crapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimApprovalTranId)
            'Pinkal (10-Feb-2021) -- End
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("expenseunkid").ToString)
            objDataOperation.AddParameter("@bal_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBal_Amount)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid)
            objDataOperation.AddParameter("@base_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBase_Countryunkid)
            objDataOperation.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBase_Amount)
            objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExchangerateunkid)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchange_Rate)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveremployeeunkid)
            objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrapproverunkid)
            objDataOperation.AddParameter("@expense_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, drRow("expense_remark").ToString)
            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False.ToString)
            objDataOperation.AddParameter("@processuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            objDataOperation.AddParameter("@processdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            objDataOperation.AddParameter("@processedrate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecProcessedrate)
            objDataOperation.AddParameter("@processedamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecProcessedAmt)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, String.Empty)
           
          
            strQ = "INSERT INTO cmretire_process_tran ( " & _
                     "  claimretirementapprovaltranunkid " & _
                     ", claimretirementtranunkid " & _
                     ", claimretirementunkid " & _
                     ", crmasterunkid " & _
                     ", crapprovaltranunkid " & _
                     ", employeeunkid " & _
                     ", periodunkid " & _
                     ", expenseunkid " & _
                     ", bal_amount " & _
                     ", countryunkid " & _
                     ", base_countryunkid " & _
                     ", base_amount " & _
                     ", exchangerateunkid " & _
                     ", exchange_rate " & _
                     ", tranheadunkid " & _
                     ", approveremployeeunkid " & _
                     ", crapproverunkid " & _
                     ", expense_remark " & _
                     ", isposted " & _
                     ", isprocessed " & _
                     ", processuserunkid " & _
                     ", processdatetime " & _
                     ", processedrate " & _
                     ", processedamt " & _
                     ", userunkid " & _
                     ", isvoid " & _
                     ", voiduserunkid " & _
                     ", voiddatetime " & _
                     ", voidreason" & _
                   ") VALUES (" & _
                     "  @claimretirementapprovaltranunkid " & _
                     ", @claimretirementtranunkid " & _
                     ", @claimretirementunkid " & _
                     ", @crmasterunkid " & _
                     ", @crapprovaltranunkid " & _
                     ", @employeeunkid " & _
                     ", @periodunkid " & _
                     ", @expenseunkid " & _
                     ", @bal_amount " & _
                     ", @countryunkid " & _
                     ", @base_countryunkid " & _
                     ", @base_amount " & _
                     ", @exchangerateunkid " & _
                     ", @exchange_rate " & _
                     ", @tranheadunkid " & _
                     ", @approveremployeeunkid " & _
                     ", @crapproverunkid " & _
                     ", @expense_remark " & _
                     ", @isposted " & _
                     ", @isprocessed " & _
                     ", @processuserunkid " & _
                     ", @processdatetime " & _
                     ", @processedrate " & _
                     ", @processedamt " & _
                     ", @userunkid " & _
                     ", @isvoid " & _
                     ", @voiduserunkid " & _
                     ", @voiddatetime " & _
                     ", @voidreason" & _
                   "); SELECT @@identity"


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCrretirementprocessunkid = dsList.Tables(0).Rows(0).Item(0)


            If InsertAudiTrailForRetirementProcessTran(objDataOperation, 1, dtCurrentDateAndTime) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cmclaim_process_tran) </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function Update(ByVal objDataOperation As clsDataOperation) As Boolean
    Public Function Update(ByVal objDataOperation As clsDataOperation, ByVal dtCurrentDateAndTime As DateTime) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crretirementprocessunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrretirementprocessunkid.ToString)
            objDataOperation.AddParameter("@claimretirementapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementapprovaltranunkid.ToString)
            objDataOperation.AddParameter("@claimretirementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementtranunkid.ToString)
            objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementunkid.ToString)
            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrmasterunkid.ToString)
            objDataOperation.AddParameter("@crapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimApprovalTranId.ToString)
            'Pinkal (10-Feb-2021) -- End
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseunkid.ToString)
            objDataOperation.AddParameter("@bal_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBal_Amount.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@base_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBase_Countryunkid.ToString)
            objDataOperation.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBase_Amount.ToString)
            objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExchangerateunkid.ToString)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchange_Rate.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveremployeeunkid.ToString)
            objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrapproverunkid.ToString)
            objDataOperation.AddParameter("@expense_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrExpense_Remark.ToString)
            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsposted.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)
            objDataOperation.AddParameter("@processuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessuserunkid.ToString)
            objDataOperation.AddParameter("@processdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProcessdatetime.ToString)
            objDataOperation.AddParameter("@processedrate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecProcessedrate.ToString)
            objDataOperation.AddParameter("@processedamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecProcessedamt.ToString)


            strQ = " UPDATE cmretire_process_tran SET " & _
                      "  claimretirementapprovaltranunkid = @claimretirementapprovaltranunkid" & _
                      ", claimretirementtranunkid = @claimretirementtranunkid" & _
                      ", claimretirementunkid = @claimretirementunkid" & _
                      ", crmasterunkid = @crmasterunkid " & _
                      ", employeeunkid = @employeeunkid" & _
                      ", periodunkid = @periodunkid" & _
                      ", expenseunkid = @expenseunkid" & _
                      ", bal_amount = @bal_amount" & _
                      ", countryunkid = @countryunkid" & _
                      ", base_countryunkid = @base_countryunkid" & _
                      ", base_amount = @base_amount" & _
                      ", exchangerateunkid = @exchangerateunkid" & _
                      ", exchange_rate = @exchange_rate" & _
                      ", tranheadunkid = @tranheadunkid" & _
                      ", approveremployeeunkid = @approveremployeeunkid" & _
                      ", crapproverunkid = @crapproverunkid" & _
                      ", expense_remark = @expense_remark" & _
                      ", isposted = @isposted" & _
                      ", isprocessed = @isprocessed" & _
                      ", processuserunkid = @processuserunkid" & _
                      ", processdatetime = @processdatetime" & _
                      ", processedrate = @processedrate" & _
                      ", processedamt = @processedamt" & _
                      " WHERE crretirementprocessunkid = @crretirementprocessunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If InsertAudiTrailForRetirementProcessTran(objDataOperation, 2, dtCurrentDateAndTime) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cmclaim_process_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " DELETE FROM cmretire_process_tran " & _
                      " WHERE crretirementprocessunkid = @crretirementprocessunkid "

            objDataOperation.AddParameter("@crretirementprocessunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@crprocesstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                     "  crretirementprocessunkid " & _
                     ", claimretirementapprovaltranunkid " & _
                     ", claimretirementtranunkid " & _
                     ", claimretirementunkid " & _
                     ", employeeunkid " & _
                     ", periodunkid " & _
                     ", expenseunkid " & _
                     ", bal_amount " & _
                     ", countryunkid " & _
                     ", base_countryunkid " & _
                     ", base_amount " & _
                     ", exchangerateunkid " & _
                     ", exchange_rate " & _
                     ", tranheadunkid " & _
                     ", approveremployeeunkid " & _
                     ", crapproverunkid " & _
                     ", expense_remark " & _
                     ", isposted " & _
                     ", isprocessed " & _
                     ", processuserunkid " & _
                     ", processdatetime " & _
                     ", processedrate " & _
                     ", processedamt " & _
                     ", userunkid " & _
                     ", isvoid " & _
                     ", voiduserunkid " & _
                     ", voiddatetime " & _
                     ", voidreason " & _
                     " FROM cmretire_process_tran " & _
                     " WHERE  1 = 1 "

            If intUnkid > 0 Then
                strQ &= " AND crretirementprocessunkid <> @crretirementprocessunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crprocesstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    '''Pinkal (10-Feb-2021) -- Start
    '''Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
    Public Function Posting_Unposting_Retirement(ByVal blnIsPosted As Boolean, ByVal xPeriodId As Integer, ByVal xtranheadId As Integer, ByVal arrListProcessIds As ArrayList, ByVal arrUnRetireIds As ArrayList) As Boolean
        'Public Function Posting_Unposting_Retirement(ByVal blnIsPosted As Boolean, ByVal xPeriodId As Integer, ByVal xtranheadId As Integer, ByVal arrListProcessIds As ArrayList) As Boolean
        'Pinkal (10-Feb-2021) -- End


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.BindTransaction()

            If arrListProcessIds IsNot Nothing AndAlso arrListProcessIds.Count > 0 Then

                For i As Integer = 0 To arrListProcessIds.Count - 1

                    Dim strRetirementProcessIds As String = arrListProcessIds(i)

                    If strRetirementProcessIds.Trim.Length <= 0 Then Continue For

                    strQ = "IF OBJECT_ID(N'tempdb..#RetirmentProcessIds') IS NOT NULL " & _
                               " DROP TABLE #RetirmentProcessIds " & _
                               " CREATE TABLE #RetirmentProcessIds (retirementprocessid int default(0)) " & _
                               " DECLARE @DATA NVARCHAR(MAX) = '" & strRetirementProcessIds & "' " & _
                               " DECLARE @CNT INT " & _
                               " DECLARE @SEP AS CHAR(1) " & _
                               " SET @SEP = ',' " & _
                               " SET @CNT = 1 " & _
                               " WHILE (CHARINDEX(@SEP,@DATA)>0) " & _
                               "    BEGIN " & _
                               "        INSERT INTO #RetirmentProcessIds (retirementprocessid) " & _
                               "        SELECT DATA = LTRIM(RTRIM(SUBSTRING(@DATA,1,CHARINDEX(@SEP,@DATA)-1))) " & _
                               "        SET @DATA = SUBSTRING(@DATA,CHARINDEX(@SEP,@DATA)+1,LEN(@DATA)) " & _
                               "        SET @CNT = @CNT + 1 " & _
                               "    END " & _
                               " INSERT INTO #RetirmentProcessIds (retirementprocessid) SELECT DATA = LTRIM(RTRIM(@DATA)) " & _
                               " UPDATE cmretire_process_tran  SET " & _
                               " periodunkid = @periodunkid, tranheadunkid = @tranheadunkid,isposted = @isposted " & _
                               " FROM " & _
                               "( " & _
                               "    SELECT " & _
                               "        #RetirmentProcessIds.retirementprocessid " & _
                               "    FROM #RetirmentProcessIds " & _
                               "    JOIN cmretire_process_tran AS CPT ON #RetirmentProcessIds.retirementprocessid = CPT.crretirementprocessunkid " & _
                               "    WHERE CPT.isvoid = 0 " & _
                               " ) AS CR WHERE CR.retirementprocessid = cmretire_process_tran.crretirementprocessunkid " & _
                               " INSERT INTO atcmretire_process_tran (crretirementprocessunkid,claimretirementapprovaltranunkid,claimretirementtranunkid,claimretirementunkid,crmasterunkid,crapprovaltranunkid " & _
                               " ,employeeunkid,periodunkid,expenseunkid,bal_amount,countryunkid,base_countryunkid,base_amount,exchangerateunkid,exchange_rate,tranheadunkid " & _
                               ",approveremployeeunkid,crapproverunkid,expense_remark,isposted,isprocessed,processuserunkid,processdatetime,processedrate,processedamt " & _
                               ",audittype,audituserunkid,auditdatetime,ip,machine_name,form_name,isweb) " & _
                               " SELECT crretirementprocessunkid,claimretirementapprovaltranunkid,claimretirementtranunkid,claimretirementunkid,crmasterunkid,crapprovaltranunkid " & _
                               " ,employeeunkid,periodunkid,expenseunkid,bal_amount,countryunkid,base_countryunkid,base_amount,exchangerateunkid,exchange_rate,tranheadunkid " & _
                               ",approveremployeeunkid,crapproverunkid,expense_remark,isposted,isprocessed,processuserunkid,processdatetime,processedrate,processedamt " & _
                               ",2," & mintUserunkid & ",getdate(),'" & mstrWebClientIP & "','" & mstrWebhostName & "','" & mstrWebFormName & "',CAST('" & IIf(mblnIsWeb, 1, 0) & "' AS BIT) " & _
                               " FROM #RetirmentProcessIds " & _
                               " JOIN cmretire_process_tran ON #RetirmentProcessIds.retirementprocessid = cmretire_process_tran.crretirementprocessunkid " & _
                               " WHERE cmretire_process_tran.isvoid = 0 " & _
                               " " & _
                               " IF OBJECT_ID(N'tempdb..#RetirmentProcessIds') IS NOT NULL " & _
                               " DROP TABLE #RetirmentProcessIds "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xtranheadId)
                    objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsPosted)
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            End If

            If arrUnRetireIds IsNot Nothing AndAlso arrUnRetireIds.Count > 0 Then

                For i As Integer = 0 To arrUnRetireIds.Count - 1

                    Dim strUnRetireIds As String = arrUnRetireIds(i)

                    If strUnRetireIds.Trim.Length <= 0 Then Continue For

                    strQ = " INSERT INTO cmretire_process_tran ( " & _
                              " claimretirementapprovaltranunkid " & _
                              " ,claimretirementtranunkid " & _
                              " ,claimretirementunkid " & _
                              " ,crmasterunkid " & _
                              ", crapprovaltranunkid " & _
                              " ,employeeunkid " & _
                              " ,periodunkid " & _
                              " ,expenseunkid " & _
                              " ,bal_amount " & _
                              " ,countryunkid " & _
                              " ,base_countryunkid " & _
                              " ,base_amount " & _
                              " ,exchangerateunkid " & _
                              " ,exchange_rate " & _
                              " ,tranheadunkid " & _
                              " ,approveremployeeunkid " & _
                              " ,crapproverunkid " & _
                              " ,expense_remark " & _
                              " ,isposted " & _
                              " ,isprocessed " & _
                              " ,processuserunkid " & _
                              " ,processdatetime " & _
                              " ,processedrate " & _
                              " ,processedamt " & _
                              " ,userunkid " & _
                              " ,isvoid " & _
                              " ,voiduserunkid " & _
                              " ,voiddatetime " & _
                              " ,voidreason " & _
                              ") " & _
                              "    SELECT " & _
                              "     0 " & _
                              "    ,0 " & _
                              "    ,0 " & _
                              "    ,crmasterunkid " & _
                              "    , crapprovaltranunkid " & _
                              "    ,employeeunkid " & _
                              "    ,@periodunkid " & _
                               "    ,expenseunkid " & _
                               "    ,ClaimAmt " & _
                               "    ,countryunkid " & _
                               "    ,base_countryunkid " & _
                               "    ,base_amount " & _
                               "    ,exchangerateunkid " & _
                               "    ,exchange_rate " & _
                               "    ,@tranheadunkid " & _
                               "    ,approveremployeeunkid " & _
                               "    ,crapproverunkid " & _
                               "    ,expense_remark " & _
                               "    ,@isposted " & _
                               "    ,0 " & _
                               "    ,0 " & _
                               "    ,NULL " & _
                               "    ,0 " & _
                               "    ,0 " & _
                               "    ,@userunkid " & _
                               "    ,0 " & _
                               "    ,0 " & _
                               "    ,NULL " & _
                               "    ,'' " & _
                               "    FROM ( " & _
                               "                    SELECT " & _
                               "                        cmclaim_approval_tran.crapproverunkid AS crapproverunkid " & _
                               "                       ,cmclaim_approval_tran.approveremployeeunkid AS approveremployeeunkid " & _
                               "                       ,cmclaim_approval_tran.crmasterunkid " & _
                               "                       ,cmclaim_approval_tran.crapprovaltranunkid " & _
                               "                       ,cmclaim_request_master.employeeunkid " & _
                               "                       ,cmclaim_approval_tran.expenseunkid " & _
                               "                       ,cmclaim_approval_tran.countryunkid " & _
                               "                       ,cmclaim_approval_tran.base_countryunkid " & _
                               "                       ,cmclaim_approval_tran.exchangerateunkid " & _
                               "                       ,cmclaim_approval_tran.exchange_rate " & _
                               "                       ,cmclaim_approval_tran.base_amount " & _
                               "                       ,cmclaim_approval_tran.amount AS ClaimAmt " & _
                               "                       ,cmclaim_approval_tran.quantity AS ClaimQty " & _
                               "                       ,cmclaim_approval_tran.approvaldate " & _
                               "                       ,cmclaim_approval_tran.statusunkid " & _
                               "                       ,ISNULL(cfexchange_rate.currency_sign, '') AS currency_sign " & _
                               "                       ,cmapproverlevel_master.crpriority " & _
                               "                       ,cmclaim_approval_tran.expense_remark " & _
                               "                       ,ROW_NUMBER() OVER (PARTITION BY cmclaim_approval_tran.crmasterunkid, cmclaim_approval_tran.crtranunkid ORDER BY cmclaim_approval_tran.statusunkid, crpriority DESC) AS Rno " & _
                               "                FROM cmclaim_approval_tran " & _
                               "                LEFT JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
                               "                LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
                               "                LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                               "                JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid AND cmexpense_master.isimprest = 1 " & _
                               "                LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmexpapprover_master.employeeunkid " & _
                               "                LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_approval_tran.exchangerateunkid " & _
                               "                WHERE cmclaim_approval_tran.isvoid = 0 AND cmclaim_approval_tran.statusunkid = 1 " & _
                               "                ) AS A WHERE A.Rno <= 1 AND A.crapprovaltranunkid IN (" & strUnRetireIds & ")  ;" & _
                               " INSERT INTO atcmretire_process_tran ( " & _
                               "  crretirementprocessunkid " & _
                               " ,claimretirementapprovaltranunkid " & _
                               " ,claimretirementtranunkid " & _
                               " ,claimretirementunkid " & _
                               " ,crmasterunkid " & _
                               ", crapprovaltranunkid " & _
                               " ,employeeunkid " & _
                               " ,periodunkid " & _
                               " ,expenseunkid " & _
                               " ,bal_amount " & _
                               " ,countryunkid " & _
                               " ,base_countryunkid " & _
                               " ,base_amount " & _
                               " ,exchangerateunkid " & _
                               " ,exchange_rate " & _
                               " ,tranheadunkid " & _
                               " ,approveremployeeunkid " & _
                               " ,crapproverunkid " & _
                               " ,expense_remark " & _
                               " ,isposted " & _
                               " ,isprocessed " & _
                               " ,processuserunkid " & _
                               " ,processdatetime " & _
                               " ,processedrate " & _
                               " ,processedamt " & _
                               " ,audittype " & _
                               " ,audituserunkid " & _
                               " ,auditdatetime " & _
                               " ,ip " & _
                               " ,machine_name " & _
                               " ,form_name " & _
                               " ,isweb) " & _
                               "    SELECT  " & _
                               "         crretirementprocessunkid " & _
                               "        ,claimretirementapprovaltranunkid " & _
                               "        ,claimretirementtranunkid " & _
                               "        ,claimretirementunkid " & _
                               "        ,crmasterunkid " & _
                               "        ,crapprovaltranunkid " & _
                               "        ,employeeunkid " & _
                               "        ,periodunkid " & _
                               "        ,expenseunkid " & _
                               "        ,bal_amount " & _
                               "        ,countryunkid " & _
                               "        ,base_countryunkid " & _
                               "        ,base_amount " & _
                               "        ,exchangerateunkid " & _
                               "        ,exchange_rate " & _
                               "        ,tranheadunkid " & _
                               "        ,approveremployeeunkid " & _
                               "        ,crapproverunkid " & _
                               "        ,expense_remark " & _
                               "        ,isposted " & _
                               "        ,isprocessed " & _
                               "        ,processuserunkid " & _
                               "        ,processdatetime " & _
                               "        ,processedrate " & _
                               "        ,processedamt " & _
                               "        ,1 " & _
                               "        ," & mintUserunkid & _
                               "        ,getdate() " & _
                               "        ,'" & mstrWebClientIP & "'" & _
                               "        ,'" & mstrWebhostName & "'" & _
                               "        ,'" & mstrWebFormName & "'" & _
                               "        ,CAST('" & IIf(mblnIsWeb, 1, 0) & "' AS BIT) " & _
                               " FROM cmretire_process_tran " & _
                               " WHERE cmretire_process_tran.isvoid = 0 AND ISNULL(cmretire_process_tran.crapprovaltranunkid,0) >0 " & _
                               " AND cmretire_process_tran.crapprovaltranunkid IN (" & strUnRetireIds & ")"


                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xtranheadId)
                    objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsPosted)
                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Posting_Unposting_Retirement; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function InsertAudiTrailForClaimProcessTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
    Public Function InsertAudiTrailForRetirementProcessTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal dtCurrentDateAndTime As DateTime) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try


            strQ = "INSERT INTO atcmretire_process_tran ( " & _
                     "  crretirementprocessunkid " & _
                     ", claimretirementapprovaltranunkid " & _
                     ", claimretirementtranunkid " & _
                     ", claimretirementunkid " & _
                     ", crmasterunkid " & _
                     ", crapprovaltranunkid " & _
                     ", employeeunkid " & _
                     ", periodunkid " & _
                     ", expenseunkid " & _
                     ", bal_amount " & _
                     ", countryunkid " & _
                     ", base_countryunkid " & _
                     ", base_amount " & _
                     ", exchangerateunkid " & _
                     ", exchange_rate " & _
                     ", tranheadunkid " & _
                     ", approveremployeeunkid " & _
                     ", crapproverunkid " & _
                     ", expense_remark " & _
                     ", isposted " & _
                     ", isprocessed " & _
                     ", processuserunkid " & _
                     ", processdatetime " & _
                     ", processedrate " & _
                     ", processedamt " & _
                     ",audittype " & _
                     ",audituserunkid " & _
                     ",auditdatetime " & _
                     ", ip " & _
                     ",machine_name " & _
                     ",form_name " & _
                     ",isweb " & _
                     ") VALUES (" & _
                     "  @crretirementprocessunkid " & _
                     ", @claimretirementapprovaltranunkid " & _
                     ", @claimretirementtranunkid " & _
                     ", @claimretirementunkid " & _
                     ", @crmasterunkid " & _
                     ", @crapprovaltranunkid " & _
                     ", @employeeunkid " & _
                     ", @periodunkid " & _
                     ", @expenseunkid " & _
                     ", @bal_amount " & _
                     ", @countryunkid " & _
                     ", @base_countryunkid " & _
                     ", @base_amount " & _
                     ", @exchangerateunkid " & _
                     ", @exchange_rate " & _
                     ", @tranheadunkid " & _
                     ", @approveremployeeunkid " & _
                     ", @crapproverunkid " & _
                     ", @expense_remark " & _
                     ", @isposted " & _
                     ", @isprocessed " & _
                     ", @processuserunkid " & _
                     ", @processdatetime " & _
                     ", @processedrate " & _
                     ", @processedamt " & _
                     ", @audittype " & _
                     ", @audituserunkid " & _
                     ", @auditdatetime " & _
                     ", @ip " & _
                     ", @machine_name " & _
                     ", @form_name " & _
                     ", @isweb " & _
                   "); SELECT @@identity"


            If mstrWebClientIP.ToString().Length <= 0 Then
                mstrWebClientIP = getIP()
            End If

            If mstrWebhostName.ToString().Length <= 0 Then
                mstrWebhostName = getHostName()
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crretirementprocessunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrretirementprocessunkid.ToString)
            objDataOperation.AddParameter("@claimretirementapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementapprovaltranunkid.ToString)
            objDataOperation.AddParameter("@claimretirementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementtranunkid.ToString)
            objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementunkid.ToString)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrmasterunkid.ToString)
            objDataOperation.AddParameter("@crapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimApprovalTranId.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseunkid.ToString)
            objDataOperation.AddParameter("@bal_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBal_Amount.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@base_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBase_Countryunkid.ToString)
            objDataOperation.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBase_Amount.ToString)
            objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExchangerateunkid.ToString)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchange_Rate.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveremployeeunkid.ToString)
            objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrapproverunkid.ToString)
            objDataOperation.AddParameter("@expense_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrExpense_Remark.ToString)
            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsposted.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)
            objDataOperation.AddParameter("@processuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessuserunkid.ToString)

            If mdtProcessdatetime <> Nothing Then
                objDataOperation.AddParameter("@processdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProcessdatetime.ToString)
            Else
                objDataOperation.AddParameter("@processdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@processedrate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecProcessedrate.ToString)
            objDataOperation.AddParameter("@processedamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecProcessedamt.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 5, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)


            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAudiTrailForRetirementProcessTran; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    'Public Function GetEmployeeExpenseQty(ByVal intEmployeeId As Integer, ByVal intExpenseId As Integer, ByVal intPeriodId As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Decimal
    '    'Sohail (03 May 2018) - [xDataOp]
    '    Dim mdecQty As Decimal = 0
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Try


    '        'Sohail (03 May 2018) -- Start
    '        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
    '        'Dim objDataOperation As New clsDataOperation
    '        Dim objDataOperation As clsDataOperation
    '        If xDataOp Is Nothing Then
    '            objDataOperation = New clsDataOperation
    '        Else
    '            objDataOperation = xDataOp
    '        End If
    '        'Sohail (03 May 2018) -- End
    '        objDataOperation.ClearParameters()

    '        strQ = " SELECT ISNULL(SUM(quantity),0) AS Qty FROM cmclaim_process_tran " & _
    '                   " WHERE cmclaim_process_tran.employeeunkid = @employeeunkid " & _
    '                   " AND cmclaim_process_tran.periodunkid = @periodunkid AND cmclaim_process_tran.isposted = 1 AND isprocessed  = 0  AND cmclaim_process_tran.isvoid = 0 "

    '        If intExpenseId > 0 Then
    '            strQ &= " AND cmclaim_process_tran.expenseunkid = @expenseunkid  "
    '            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intExpenseId)
    '        End If
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
    '        objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
    '            mdecQty = CDec(dsList.Tables(0).Rows(0)("Qty"))
    '        End If


    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeExpenseQty; Module Name: " & mstrModuleName)
    '        'Sohail (03 May 2018) -- Start
    '        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
    '    Finally
    '        If xDataOp Is Nothing Then objDataOperation = Nothing
    '        'Sohail (03 May 2018) -- End
    '    End Try
    '    Return mdecQty
    'End Function

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetEmployeeExpenseAmount(ByVal intEmployeeId As Integer, ByVal intExpenseId As Integer, ByVal intPeriodId As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Decimal
    '    'Sohail (03 May 2018) - [xDataOp]
    '    Dim mdecAmount As Decimal = 0
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Try

    '        'Sohail (03 May 2018) -- Start
    '        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
    '        'Dim objDataOperation As New clsDataOperation
    '        Dim objDataOperation As clsDataOperation
    '        If xDataOp Is Nothing Then
    '            objDataOperation = New clsDataOperation
    '        Else
    '            objDataOperation = xDataOp
    '        End If
    '        'Sohail (03 May 2018) -- End
    '        objDataOperation.ClearParameters()


    '        'Pinkal (04-Feb-2019) -- Start
    '        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

    '        'strQ = " SELECT ISNULL(SUM(amount),0) AS Amount FROM cmclaim_process_tran " & _
    '        '           " WHERE cmclaim_process_tran.employeeunkid = @employeeunkid  " & _
    '        '           " AND cmclaim_process_tran.periodunkid = @periodunkid AND cmclaim_process_tran.isposted = 1 AND isprocessed  = 0  AND cmclaim_process_tran.isvoid = 0 "

    '        strQ = " SELECT ISNULL(SUM(amount),0) AS Amount,ISNULL(SUM(base_amount),0) AS Base_Amount FROM cmclaim_process_tran " & _
    '                   " WHERE cmclaim_process_tran.employeeunkid = @employeeunkid  " & _
    '                   " AND cmclaim_process_tran.periodunkid = @periodunkid AND cmclaim_process_tran.isposted = 1 AND isprocessed  = 0  AND cmclaim_process_tran.isvoid = 0 "

    '        'Pinkal (04-Feb-2019) -- End


    '        If intExpenseId > 0 Then
    '            strQ &= " AND cmclaim_process_tran.expenseunkid = @expenseunkid  "
    '            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intExpenseId)
    '        End If

    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
    '        objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
    '            'Pinkal (04-Feb-2019) -- Start
    '            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[Used in Payroll formula due to that we are using base amount for further process.]
    '            'mdecAmount = CDec(dsList.Tables(0).Rows(0)("Amount"))
    '            mdecAmount = CDec(dsList.Tables(0).Rows(0)("Base_Amount"))
    '            'Pinkal (04-Feb-2019) -- End
    '        End If


    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeExpenseAmount; Module Name: " & mstrModuleName)
    '        'Sohail (03 May 2018) -- Start
    '        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
    '    Finally
    '        If xDataOp Is Nothing Then objDataOperation = Nothing
    '        'Sohail (03 May 2018) -- End
    '    End Try
    '    Return mdecAmount
    'End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.


    'Pinkal (10-Feb-2021) -- Start
    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetUnRetiredList(ByVal strTableName As String, Optional ByVal mblnAddGrp As Boolean = True _
                                 , Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intIsposted As Integer = -1 _
                                 , Optional ByVal mstrFilter As String = "", Optional ByVal xDataOp As clsDataOperation = Nothing) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            If mblnAddGrp Then

                strQ = "SELECT DISTINCT " & _
                          " CAST(1 AS BIT) As Isgroup " & _
                          ", CAST(0 AS BIT) As Ischecked " & _
                          ", CAST(0 AS BIT) As ischange " & _
                          ", 0 AS crretirementprocessunkid " & _
                          ", 0 AS claimretirementapprovaltranunkid " & _
                          ", 0 AS claimretirementtranunkid " & _
                          ", 0 AS claimretirementunkid " & _
                          ", ISNULL(cmclaim_request_master.crmasterunkid,0) AS crmasterunkid " & _
                          ", 0 AS crapprovaltranunkid " & _
                          ", cmclaim_request_master.employeeunkid " & _
                          ", 0 AS periodunkid " & _
                          ", 0 AS expenseunkid " & _
                          ", cmclaim_request_master.claimrequestno AS claimretirementno " & _
                          ", CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112) AS transactiondate " & _
                          ", ISNULL(hremployee_master.firstname,'') + '  ' + ISNULL(hremployee_master.othername,'')  + '  ' +  ISNULL(hremployee_master.surname,'') AS Employee " & _
                          ", ISNULL(cmclaim_retirement_master.claimretirementno,cmclaim_request_master.claimrequestno) + '  -  ' + ISNULL(hremployee_master.firstname,'') + '  ' + ISNULL(hremployee_master.othername,'')  + '  ' +  ISNULL(hremployee_master.surname,'') AS Retirement " & _
                          ", '' AS Expense " & _
                          ", '' AS Period " & _
                          ", 0 AS tranheadunkid " & _
                          ", '' AS tranhead " & _
                          ", 0.00 as bal_amount " & _
                          ", 0 AS countryunkid " & _
                          ", 0 AS base_countryunkid " & _
                          ", 0.00 AS base_amount " & _
                          ", 0 AS exchangerateunkid " & _
                          ", 0.00 AS exchange_rate " & _
                          ", '' AS currency_sign " & _
                          ", 0.00 AS processedrate " & _
                          ", 0.00 AS processedamt " & _
                          ", '' AS expense_remark " & _
                          ", 0 AS approveremployeeunkid " & _
                          ", 0 AS crapproverunkid " & _
                          ", cmclaim_request_master.userunkid " & _
                          ", cmclaim_request_master.isvoid " & _
                          ", cmclaim_request_master.voiduserunkid " & _
                          ", cmclaim_request_master.voiddatetime " & _
                          ", cmclaim_request_master.voidreason " & _
                          ", 0 AS isposted " & _
                          ", 0 AS isprocessed " & _
                          ", 0 AS processuserunkid " & _
                          ", NULL AS processdatetime " & _
                          " FROM cmclaim_request_master " & _
                          " LEFT JOIN cmclaim_approval_tran ON cmclaim_approval_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_approval_tran.isvoid = 0 " & _
                          " LEFT JOIN cmretire_process_tran ON cmclaim_request_master.crmasterunkid = cmretire_process_tran.crmasterunkid AND cmclaim_approval_tran.crapprovaltranunkid = ISNULL(cmretire_process_tran.crapprovaltranunkid,0)  " & _
                          " LEFT JOIN cmclaim_retirement_master ON cmclaim_retirement_master.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_retirement_master.isvoid = 0 " & _
                          " JOIN ( " & _
                          "                  SELECT " & _
                          "                          crmasterunkid,crapprovaltranunkid ,ClaimAmt " & _
                          "                         ,ClaimQty ,statusunkid " & _
                          "                         ,crapproverunkid " & _
                          "                         ,approveremployeeunkid " & _
                          "                         ,'' AS expense_remark " & _
                          "                         ,expenseunkid " & _
                          "                         ,approvaldate " & _
                          "                         ,countryunkid " & _
                          "                         ,base_countryunkid " & _
                          "                         ,exchangerateunkid " & _
                          "                         ,exchange_rate " & _
                          "                         ,base_amount " & _
                          "                         ,currency_sign " & _
                          "                 FROM " & _
                          "                 ( " & _
                          "                          SELECT " & _
                          "                                    cmclaim_approval_tran.crapproverunkid as crapproverunkid " & _
                          "                                   ,cmclaim_approval_tran.approveremployeeunkid As  approveremployeeunkid " & _
                          "                                   ,cmclaim_approval_tran.crmasterunkid " & _
                          "                                   ,cmclaim_approval_tran.crapprovaltranunkid " & _
                          "                                   ,cmclaim_approval_tran.expenseunkid " & _
                          "                                   ,cmclaim_approval_tran.amount AS ClaimAmt " & _
                          "                                   ,cmclaim_approval_tran.quantity AS ClaimQty " & _
                          "                                   ,cmclaim_approval_tran.approvaldate " & _
                          "                                   ,cmclaim_approval_tran.countryunkid " & _
                          "                                   ,cmclaim_approval_tran.base_countryunkid " & _
                          "                                   ,cmclaim_approval_tran.exchangerateunkid " & _
                          "                                   ,cmclaim_approval_tran.exchange_rate " & _
                          "                                   ,cmclaim_approval_tran.base_amount " & _
                          "                                   ,cmclaim_approval_tran.statusunkid " & _
                          "                                   ,cmapproverlevel_master.crpriority " & _
                          "                                   ,ISNULL(cfexchange_rate.currency_sign,'') as currency_sign " & _
                          "                                   ,ROW_NUMBER() OVER (PARTITION BY cmclaim_approval_tran.crmasterunkid,cmclaim_approval_tran.crtranunkid ORDER BY cmclaim_approval_tran.statusunkid, crpriority DESC) AS Rno " & _
                          "                         FROM cmclaim_approval_tran " & _
                          "                         LEFT JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
                          "                         LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
                          "                         LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                          "                         JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid AND cmexpense_master.isimprest = 1 " & _
                          "                         LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmexpapprover_master.employeeunkid " & _
                          "                         LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_approval_tran.exchangerateunkid " & _
                          "                         WHERE cmclaim_approval_tran.isvoid = 0 AND cmclaim_approval_tran.statusunkid = 1 " & _
                          "                ) as Claim WHERE Claim.Rno = 1 " & _
                          "    ) AS AppClaim on AppClaim.crmasterunkid = cmclaim_request_master.crmasterunkid AND AppClaim.crapprovaltranunkid = cmclaim_approval_tran.crapprovaltranunkid  " & _
                          " JOIN hremployee_master ON hremployee_master.employeeunkid = cmclaim_request_master.employeeunkid " & _
                          " JOIN cmexpense_master ON cmexpense_master.expenseunkid  = cmclaim_approval_tran.expenseunkid  AND cmexpense_master.isactive = 1 AND cmexpense_master.isimprest =1 " & _
                          " WHERE  ISNULL(cmclaim_retirement_master.claimretirementunkid,0) <=0  AND cmclaim_request_master.statusunkid = 1 AND ISNULL(cmretire_process_tran.isprocessed,0) = 0 "

                If blnOnlyActive Then
                    strQ &= " AND cmclaim_request_master.isvoid = 0 AND ISNULL(cmretire_process_tran.isvoid,0) = 0 "
                End If

                If intIsposted = 0 OrElse intIsposted = 1 Then
                    strQ &= " AND ISNUL(cmretire_process_tran.isposted,0) = " & intIsposted & " "
                End If

                If mstrFilter.Trim.Length > 0 Then
                    strQ &= " AND " & mstrFilter
                End If

                strQ &= " UNION "


            End If

            strQ &= "SELECT " & _
                         " CAST(0 AS BIT) As Isgroup " & _
                         ", CAST(0 AS BIT) As Ischecked " & _
                         ", CAST(0 AS BIT) As ischange " & _
                         ", ISNULL(cmretire_process_tran.crretirementprocessunkid ,0) as crretirementprocessunkid " & _
                         ", ISNULL(cmretire_process_tran.claimretirementapprovaltranunkid ,0) as claimretirementapprovaltranunkid " & _
                         ", ISNULL(cmretire_process_tran.claimretirementtranunkid,0) AS  claimretirementtranunkid " & _
                         ", ISNULL(cmretire_process_tran.claimretirementunkid,0) AS claimretirementunkid " & _
                         ", ISNULL(cmretire_process_tran.crmasterunkid,cmclaim_request_master.crmasterunkid) as crmasterunkid " & _
                         ", ISNULL(cmretire_process_tran.crapprovaltranunkid,cmclaim_approval_tran.crapprovaltranunkid) as crapprovaltranunkid " & _
                         ", ISNULL(cmretire_process_tran.employeeunkid ,cmclaim_request_master.employeeunkid) AS employeeunkid " & _
                         ", ISNULL(cmretire_process_tran.periodunkid ,0) AS periodunkid " & _
                         ", ISNULL(cmretire_process_tran.expenseunkid,AppClaim.expenseunkid) AS expenseunkid " & _
                         ", ISNULL(cmclaim_retirement_master.claimretirementno ,cmclaim_request_master.claimrequestno) AS claimretirementno " & _
                         ", CONVERT(CHAR(8),ISNULL(cmclaim_retirement_master.transactiondate,AppClaim.approvaldate),112) AS transactiondate " & _
                         ", ISNULL(hremployee_master.firstname,'') + '  ' + ISNULL(hremployee_master.othername,'')  + '  ' +  ISNULL(hremployee_master.surname,'') AS Employee " & _
                         ", '' AS Retirement " & _
                         ", ISNULL(cmexpense_master.name,'') AS Expense " & _
                         ", ISNULL(cfcommon_period_tran.period_name,'') AS Period " & _
                         ", ISNULL(prtranhead_master.tranheadunkid,'') AS tranheadunkid " & _
                         ", ISNULL(prtranhead_master.trnheadname,'') AS tranhead " & _
                         ", ISNULL(cmretire_process_tran.bal_amount ,AppClaim.ClaimAmt) as bal_amount " & _
                         ", ISNULL(cmretire_process_tran.countryunkid,AppClaim.countryunkid) AS countryunkid " & _
                         ", ISNULL(cmretire_process_tran.base_countryunkid ,AppClaim.base_countryunkid) AS base_countryunkid " & _
                         ", ISNULL(cmretire_process_tran.base_amount,AppClaim.base_amount) AS base_amount " & _
                         ", ISNULL(cmretire_process_tran.exchangerateunkid,AppClaim.exchangerateunkid) AS exchangerateunkid " & _
                         ", ISNULL(cmretire_process_tran.exchange_rate,AppClaim.exchange_rate) AS exchange_rate " & _
                         ", ISNULL(cfexchange_rate.currency_sign,AppClaim.currency_sign) AS currency_sign " & _
                         ", ISNULL(cmretire_process_tran.processedrate,0.00) AS  processedrate " & _
                         ", ISNULL(cmretire_process_tran.processedamt,0.00) AS  processedamt " & _
                         ", ISNULL(cmretire_process_tran.expense_remark,AppClaim.expense_remark) AS expense_remark " & _
                         ", ISNULL(cmretire_process_tran.approveremployeeunkid,AppClaim.approveremployeeunkid) AS  approveremployeeunkid " & _
                         ", ISNULL(cmretire_process_tran.crapproverunkid,AppClaim.crapproverunkid) AS  crapproverunkid " & _
                         ", ISNULL(cmretire_process_tran.userunkid,0) AS  userunkid " & _
                         ", ISNULL(cmretire_process_tran.isvoid,0) AS isvoid " & _
                         ", ISNULL(cmretire_process_tran.voiduserunkid,0) As  voiduserunkid " & _
                         ", cmretire_process_tran.voiddatetime " & _
                         ", ISNULL(cmretire_process_tran.voidreason ,'') as voidreason " & _
                         ", ISNULL(cmretire_process_tran.isposted,0) AS  isposted " & _
                         ", ISNULL(cmretire_process_tran.isprocessed,0) AS  isprocessed " & _
                         ", ISNULL(cmretire_process_tran.processuserunkid,0) AS processuserunkid " & _
                         ", cmretire_process_tran.processdatetime " & _
                         " FROM  cmclaim_request_master " & _
                         " LEFT JOIN cmclaim_approval_tran ON cmclaim_approval_tran.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_approval_tran.isvoid = 0 " & _
                         " LEFT JOIN cmretire_process_tran ON cmclaim_request_master.crmasterunkid = cmretire_process_tran.crmasterunkid AND cmclaim_approval_tran.crapprovaltranunkid = ISNULL(cmretire_process_tran.crapprovaltranunkid,0)  " & _
                         " JOIN ( " & _
                         "          SELECT " & _
                         "               crmasterunkid,crapprovaltranunkid ,ClaimAmt " & _
                         "              ,ClaimQty ,statusunkid " & _
                         "               ,crapproverunkid " & _
                         "               ,approveremployeeunkid " & _
                         "               ,expense_remark " & _
                         "               ,expenseunkid " & _
                         "               ,approvaldate " & _
                         "               ,countryunkid " & _
                         "               ,base_countryunkid " & _
                         "               ,exchangerateunkid " & _
                         "               ,exchange_rate " & _
                         "               ,base_amount " & _
                         "               ,currency_sign " & _
                         "       FROM " & _
                         "      ( " & _
                         "                  SELECT " & _
                         "                           cmclaim_approval_tran.crapproverunkid as crapproverunkid " & _
                         "                          ,cmclaim_approval_tran.approveremployeeunkid As  approveremployeeunkid " & _
                         "                          ,cmclaim_approval_tran.crmasterunkid " & _
                         "                          ,cmclaim_approval_tran.crapprovaltranunkid " & _
                         "                          ,cmclaim_approval_tran.expense_remark " & _
                         "                         ,cmclaim_approval_tran.expenseunkid " & _
                         "                         ,cmclaim_approval_tran.countryunkid " & _
                         "                         ,cmclaim_approval_tran.base_countryunkid " & _
                         "                         ,cmclaim_approval_tran.exchangerateunkid " & _
                         "                         ,cmclaim_approval_tran.exchange_rate " & _
                         "                         ,cmclaim_approval_tran.base_amount " & _
                         "                         ,cmclaim_approval_tran.amount AS ClaimAmt " & _
                         "                         ,cmclaim_approval_tran.quantity AS ClaimQty " & _
                         "                         ,cmclaim_approval_tran.approvaldate " & _
                         "                         ,cmclaim_approval_tran.statusunkid " & _
                         "                         ,cmapproverlevel_master.crpriority " & _
                         "                         ,ISNULL(cfexchange_rate.currency_sign,'') as currency_sign " & _
                         "                         ,ROW_NUMBER() OVER (PARTITION BY cmclaim_approval_tran.crmasterunkid,cmclaim_approval_tran.crtranunkid ORDER BY cmclaim_approval_tran.statusunkid, crpriority DESC) AS Rno " & _
                         "               FROM cmclaim_approval_tran " & _
                         "               LEFT JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid  AND cmclaim_request_master.isvoid = 0 " & _
                         "               LEFT JOIN cmexpapprover_master ON cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
                         "               LEFT JOIN cmapproverlevel_master ON cmapproverlevel_master.crlevelunkid = cmexpapprover_master.crlevelunkid " & _
                         "               JOIN cmexpense_master ON cmexpense_master.expenseunkid = cmclaim_approval_tran.expenseunkid AND cmexpense_master.isimprest = 1 " & _
                         "               LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmexpapprover_master.employeeunkid " & _
                         "               LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = cmclaim_approval_tran.exchangerateunkid " & _
                         "               WHERE cmclaim_approval_tran.isvoid = 0 AND cmclaim_approval_tran.statusunkid = 1 " & _
                         "      ) as Claim WHERE Claim.Rno = 1 " & _
                         "  ) AS AppClaim on AppClaim.crmasterunkid = cmclaim_request_master.crmasterunkid AND AppClaim.crapprovaltranunkid = cmclaim_approval_tran.crapprovaltranunkid " & _
                         "  LEFT JOIN cmclaim_retirement_master ON cmclaim_retirement_master.crmasterunkid = cmclaim_request_master.crmasterunkid AND cmclaim_retirement_master.isvoid = 0 " & _
                         "  LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmclaim_request_master.employeeunkid " & _
                         "  JOIN cmexpense_master ON cmexpense_master.expenseunkid  = cmclaim_approval_tran.expenseunkid AND cmexpense_master.isactive = 1 AND cmexpense_master.isimprest =1 " & _
                         "  LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid  = ISNULL(cmretire_process_tran.periodunkid,0) " & _
                         "  LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = ISNULL(cmretire_process_tran.tranheadunkid,0) AND prtranhead_master.isvoid  = 0 " & _
                         "  LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = ISNULL(cmretire_process_tran.exchangerateunkid,0) " & _
                         " WHERE  ISNULL(cmclaim_retirement_master.claimretirementunkid,0) <=0  AND cmclaim_request_master.statusunkid = 1 AND ISNULL(cmretire_process_tran.isprocessed,0) = 0 "

            If blnOnlyActive Then
                strQ &= " AND cmclaim_request_master.isvoid = 0 AND ISNULL(cmretire_process_tran.isvoid,0) = 0 "
            End If

            If intIsposted = 0 OrElse intIsposted = 1 Then
                strQ &= " AND ISNULL(cmretire_process_tran.isposted,0) = " & intIsposted & " "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If


            strQ &= " ORDER BY crmasterunkid,transactiondate DESC,Isgroup DESC "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUnRetiredList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Pinkal (10-Feb-2021) -- End


#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 5, "WEB")

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
