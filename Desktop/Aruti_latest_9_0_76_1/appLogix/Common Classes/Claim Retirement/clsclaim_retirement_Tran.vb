﻿'************************************************************************************************************************************
'Class Name : clsclaim_retirement_Tran.vb
'Purpose    :
'Date       :23-Sep-2019
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsclaim_retirement_Tran
    Private Const mstrModuleName = "clsclaim_retirement_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mdtTran As DataTable = Nothing
    Private mintClaimretirementtranunkid As Integer
    Private mintClaimretirementunkid As Integer
    Private mintApproverTranId As Integer = 0
    Private mintApproverEmpId As Integer = 0

    Private mintUserunkid As Integer
    Private mintStatusId As Integer = 0
    Private mintVisibleId As Integer = -1
    Private mintLoginemployeeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintVoidloginemployeeunkid As Integer
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Private mstrFormName As String = ""
    Private mblnIsWeb As Boolean = False

#End Region

#Region " Constructor "

    Public Sub New()
        mdtTran = New DataTable("List")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("Claimretirementtranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("Claimretirementunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("crmasterunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("expenseunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("expense")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("quantity")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("unitprice")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("amount")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("balance")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("expense_remark")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("costcenterunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("costcentercode")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("costcentername")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("glcodeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("glcode")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("gldesc")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isbudgetmandatory")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ishrExpense")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("countryunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("base_countryunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("base_amount")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("exchangerateunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("exchange_rate")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("loginemployeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidloginemployeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("iscancel")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("canceluserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("cancel_datetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("cancel_remark")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)


            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            dCol = New DataColumn("claim_amount")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            'Pinkal (04-Jul-2020) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set claimretirementtranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Claimretirementtranunkid() As Integer
        Get
            Return mintClaimretirementtranunkid
        End Get
        Set(ByVal value As Integer)
            mintClaimretirementtranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set claimretirementunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Claimretirementunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintClaimretirementunkid
        End Get
        Set(ByVal value As Integer)
            mintClaimretirementunkid = value
            GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ApproverTranId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ApproverTranId() As Integer
        Get
            Return mintApproverTranID
        End Get
        Set(ByVal value As Integer)
            mintApproverTranID = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ApproverEmpId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ApproverEmpId() As Integer
        Get
            Return mintApproverEmpId
        End Get
        Set(ByVal value As Integer)
            mintApproverEmpId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Datatable
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsWeb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try

            strQ = "SELECT " & _
                    "  cmclaim_retirement_tran.claimretirementtranunkid " & _
                    ", cmclaim_retirement_tran.claimretirementunkid " & _
                    ", cmclaim_retirement_tran.crmasterunkid " & _
                    ", cmclaim_retirement_tran.expenseunkid " & _
                    ", cmclaim_retirement_tran.approvertranunkid " & _
                    ", cmclaim_retirement_tran.approveremployeeunkid " & _
                    ", ISNULL(cmexpense_master.name,'') As expense " & _
                    ", cmclaim_retirement_tran.unitprice " & _
                    ", cmclaim_retirement_tran.quantity " & _
                    ", cmclaim_retirement_tran.unitprice " & _
                    ", cmclaim_retirement_tran.amount " & _
                    ", 0.00 AS balance  " & _
                    ", cmclaim_retirement_tran.expense_remark " & _
                    ", ISNULL(cmclaim_retirement_tran.costcenterunkid,0) AS costcenterunkid " & _
                    ", ISNULL(prcostcenter_master.costcentercode,'') AS costcentercode " & _
                    ", ISNULL(prcostcenter_master.costcentername,'') AS costcentername " & _
                    ", ISNULL(cmexpense_master.isbudgetmandatory,0) AS isbudgetmandatory " & _
                    ", ISNULL(cmexpense_master.ishrexpense,0) AS ishrexpense " & _
                    ", ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid " & _
                    ", ISNULL(praccount_master.account_code,'') AS glcode " & _
                    ", ISNULL(praccount_master.account_name,'') AS glcodeName " & _
                    ", ISNULL(cmexpense_master.description,'') AS gldesc " & _
                    ",ISNULL(cmclaim_retirement_tran.countryunkid,0) AS countryunkid " & _
                    ",ISNULL(cmclaim_retirement_tran.base_countryunkid,0) AS base_countryunkid " & _
                    ",ISNULL(cmclaim_retirement_tran.base_amount,0.00) AS base_amount " & _
                    ",ISNULL(cmclaim_retirement_tran.exchangerateunkid,0) AS exchangerateunkid " & _
                    ",ISNULL(cmclaim_retirement_tran.exchange_rate,0.00) AS exchange_rate " & _
                    ", cmclaim_retirement_tran.isvoid " & _
                    ", cmclaim_retirement_tran.voiduserunkid " & _
                    ", cmclaim_retirement_tran.voiddatetime " & _
                    ", cmclaim_retirement_tran.voidreason " & _
                    ", cmclaim_retirement_tran.loginemployeeunkid " & _
                    ", 0 As iscancel " & _
                    ", -1 As canceluserunkid " & _
                    ", NULL As cancel_datetime " & _
                    ", '' AS cancel_remark " & _
                    ", cmclaim_retirement_tran.loginemployeeunkid " & _
                    ", '' AS AUD " & _
                    ", '' AS GUID " & _
                    ", cmclaim_retirement_tran.voidloginemployeeunkid " & _
                    ", ISNULL(cmclaim_retirement_master.employeeunkid,0) AS employeeunkid " & _
                    ", ISNULL(cmclaim_retirement_master.claim_amount,0.00) AS claim_amount " & _
                    " FROM cmclaim_retirement_tran " & _
                    " JOIN cmclaim_retirement_master ON cmclaim_retirement_master.claimretirementunkid = cmclaim_retirement_tran.claimretirementunkid AND cmclaim_retirement_master.isvoid = 0 " & _
                    " JOIN cmexpense_master ON cmclaim_retirement_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                    " LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = cmclaim_retirement_tran.costcenterunkid " & _
                    " LEFT JOIN praccount_master ON praccount_master.accountunkid = cmexpense_master.glcodeunkid " & _
                    " WHERE cmclaim_retirement_tran.claimretirementunkid = @claimretirementunkid AND cmclaim_retirement_tran.isvoid = 0  "


            'Pinkal (04-Jul-2020) -- Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.[ ISNULL(cmclaim_retirement_master.employeeunkid,0) AS employeeunkid,ISNULL(cmclaim_retirement_master.claim_amount,0.00) AS claim_amount]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()

            For Each dRow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(dRow)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cmclaim_retirement_tran) </purpose>
    Public Function InsertUpdateDelete_ClaimRetirement_Tran(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            If mdtTran Is Nothing Then Return True

            For i = 0 To mdtTran.Rows.Count - 1

                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"

                                strQ = " INSERT INTO cmclaim_retirement_tran ( " & _
                                         "  claimretirementunkid " & _
                                         ", crmasterunkid " & _
                                         ", expenseunkid " & _
                                         ", approvertranunkid " & _
                                         ", approveremployeeunkid " & _
                                         ", quantity " & _
                                         ", unitprice " & _
                                         ", amount " & _
                                         ", expense_remark " & _
                                         ", costcenterunkid " & _
                                         ", countryunkid " & _
                                         ", base_countryunkid " & _
                                         ", base_amount " & _
                                         ", exchangerateunkid " & _
                                         ", exchange_rate " & _
                                         ", userunkid " & _
                                         ", loginemployeeunkid " & _
                                         ", isvoid " & _
                                         ", voiduserunkid " & _
                                         ", voiddatetime " & _
                                         ", voidreason " & _
                                         ", voidloginemployeeunkid" & _
                                         " ) VALUES (" & _
                                         "  @claimretirementunkid " & _
                                         ", @crmasterunkid " & _
                                         ", @expenseunkid " & _
                                         ", @approvertranunkid " & _
                                         ", @approveremployeeunkid " & _
                                         ", @quantity " & _
                                         ", @unitprice " & _
                                         ", @amount " & _
                                         ", @expense_remark " & _
                                         ", @costcenterunkid " & _
                                         ", @countryunkid " & _
                                         ", @base_countryunkid " & _
                                         ", @base_amount " & _
                                         ", @exchangerateunkid " & _
                                         ", @exchange_rate " & _
                                         ", @userunkid " & _
                                         ", @loginemployeeunkid " & _
                                         ", @isvoid " & _
                                         ", @voiduserunkid " & _
                                         ", @voiddatetime " & _
                                         ", @voidreason " & _
                                         ", @voidloginemployeeunkid" & _
                                         " ); SELECT @@identity"

                                objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementunkid.ToString)
                                objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("crmasterunkid").ToString())
                                objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("expenseunkid").ToString())
                                objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranId.ToString)
                                objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpId.ToString)
                                objDataOperation.AddParameter("@quantity", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("quantity").ToString())
                                objDataOperation.AddParameter("@unitprice", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("unitprice").ToString())
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amount").ToString())
                                objDataOperation.AddParameter("@expense_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("expense_remark").ToString())
                                objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("costcenterunkid").ToString())
                                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countryunkid").ToString())
                                objDataOperation.AddParameter("@base_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("base_countryunkid").ToString())
                                objDataOperation.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("base_amount").ToString())
                                objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("exchangerateunkid").ToString())
                                objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("exchange_rate").ToString())
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString())
                                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("loginemployeeunkid").ToString())
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString())
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString())

                                If Not IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If

                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voidloginemployeeunkid").ToString)

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintClaimretirementtranunkid = CInt(dsList.Tables(0).Rows(0).Item(0))

                                If ATInsertClaimRetirement_Tran(objDataOperation, 1, mdtTran.Rows(i)) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                .Item("Claimretirementtranunkid") = mintClaimretirementtranunkid
                                .Item("claimretirementunkid") = mintClaimretirementunkid
                                .AcceptChanges()


                            Case "U"

                                strQ = " UPDATE cmclaim_retirement_tran SET " & _
                                          "  claimretirementunkid = @claimretirementunkid" & _
                                          ", crmasterunkid = @crmasterunkid " & _
                                          ", expenseunkid = @expenseunkid" & _
                                          ", approvertranunkid = @approvertranunkid" & _
                                          ", approveremployeeunkid = @approveremployeeunkid" & _
                                          ", unitprice = @unitprice" & _
                                          ", quantity = @quantity" & _
                                          ", amount = @amount" & _
                                          ", expense_remark = @expense_remark" & _
                                          ", costcenterunkid = @costcenterunkid " & _
                                          ", countryunkid = @countryunkid " & _
                                          ", base_countryunkid = @base_countryunkid " & _
                                          ", base_amount = @base_amount " & _
                                          ", exchangerateunkid = @exchangerateunkid " & _
                                          ", exchange_rate = @exchange_rate " & _
                                          ", userunkid = @userunkid " & _
                                          ", isvoid = @isvoid" & _
                                          ", voiduserunkid = @voiduserunkid" & _
                                          ", voiddatetime = @voiddatetime" & _
                                          ", voidreason = @voidreason" & _
                                          ", loginemployeeunkid = @loginemployeeunkid " & _
                                          ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                                          " WHERE Claimretirementtranunkid = @Claimretirementtranunkid AND isvoid = 0 "


                                objDataOperation.AddParameter("@Claimretirementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("Claimretirementtranunkid").ToString)
                                objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementunkid.ToString)
                                objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("crmasterunkid").ToString())
                                objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("expenseunkid").ToString())
                                objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranId.ToString)
                                objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpId.ToString)
                                objDataOperation.AddParameter("@quantity", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("quantity").ToString())
                                objDataOperation.AddParameter("@unitprice", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("unitprice").ToString())
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amount").ToString())
                                objDataOperation.AddParameter("@expense_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("expense_remark").ToString())
                                objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("costcenterunkid").ToString())
                                objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countryunkid").ToString)
                                objDataOperation.AddParameter("@base_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("base_countryunkid").ToString)
                                objDataOperation.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("base_amount").ToString)
                                objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("exchangerateunkid").ToString)
                                objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("exchange_rate").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("loginemployeeunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)

                                If Not IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If

                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voidloginemployeeunkid").ToString)


                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If ATInsertClaimRetirement_Tran(objDataOperation, 2, mdtTran.Rows(i)) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"


                                strQ = " UPDATE cmclaim_retirement_tran SET " & _
                                          " isvoid = @isvoid" & _
                                          ", voiduserunkid = @voiduserunkid" & _
                                          ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
                                          ", voiddatetime = GetDate()" & _
                                          ", voidreason = @voidreason" & _
                                          " WHERE Claimretirementtranunkid = @Claimretirementtranunkid AND isvoid = 0 "


                                objDataOperation.AddParameter("@Claimretirementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("Claimretirementtranunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)

                                'Pinkal (26-Nov-2021)-- Start
                                'Solved problem for KBC.
                                If CInt(.Item("voiduserunkid")) > 0 Then
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                    objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                                Else
                                    objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voidloginemployeeunkid").ToString)
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
                                End If
                                'Pinkal (26-Nov-2021)-- End


                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If ATInsertClaimRetirement_Tran(objDataOperation, 3, mdtTran.Rows(i)) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select

                    End If

                End With

            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_ClaimRetirement_Tran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function DeleteRetirementTran(ByVal objDataOperation As clsDataOperation, ByVal mstrVoidReason As String, ByVal mdtImprest As DataTable) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = " UPDATE cmclaim_retirement_tran SET " & _
                      " isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
                      ", voiddatetime = GetDate()" & _
                      ", voidreason = @voidreason" & _
                      " WHERE claimretirementtranunkid = @claimretirementtranunkid AND isvoid = 0 "

            If mdtImprest IsNot Nothing AndAlso mdtImprest.Rows.Count Then

                For Each dr As DataRow In mdtImprest.Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@claimretirementapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("claimretirementapprovaltranunkid")))
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                    If mintUserunkid > 0 Then
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                    Else
                        objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid)
                    End If

                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason)
                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If ATInsertClaimRetirement_Tran(objDataOperation, 3, dr) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DeleteRetirementTran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@claimretirementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose> 
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  claimretirementtranunkid " & _
              ", claimretirementunkid " & _
              ", crmasterunkid " & _
              ", expenseunkid " & _
              ", secrouteunkid " & _
              ", costingunkid " & _
              ", quantity " & _
              ", unitprice " & _
              ", amount " & _
              ", expense_remark " & _
              ", costcenterunkid " & _
              ", countryunkid " & _
              ", base_countryunkid " & _
              ", base_amount " & _
              ", exchangerateunkid " & _
              ", exchange_rate " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", voidloginemployeeunkid " & _
             "FROM cmclaim_retirement_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND claimretirementtranunkid <> @claimretirementtranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@claimretirementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cmclaim_retirement_tran) </purpose>
    Public Function ATInsertClaimRetirement_Tran(ByVal objDataOperation As clsDataOperation, ByVal xAuditType As Integer, ByVal dr As DataRow) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@claimretirementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("claimretirementtranunkid").ToString)
            objDataOperation.AddParameter("@claimretirementunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimretirementunkid.ToString)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("crmasterunkid").ToString())
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("expenseunkid").ToString())
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranId.ToString)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpId.ToString)
            objDataOperation.AddParameter("@quantity", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dr("quantity").ToString())
            objDataOperation.AddParameter("@unitprice", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dr("unitprice").ToString())
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dr("amount").ToString())
            objDataOperation.AddParameter("@expense_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dr("expense_remark").ToString())
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("costcenterunkid").ToString())
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("countryunkid").ToString)
            objDataOperation.AddParameter("@base_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("base_countryunkid").ToString)
            objDataOperation.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dr("base_amount").ToString)
            objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("exchangerateunkid").ToString)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, dr("exchange_rate").ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("Loginemployeeunkid").ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "INSERT INTO atcmclaim_retirement_tran ( " & _
                      "  claimretirementtranunkid " & _
                      ", claimretirementunkid " & _
                      ", crmasterunkid " & _
                      ", expenseunkid " & _
                      ", approvertranunkid " & _
                      ", approveremployeeunkid " & _
                      ", quantity " & _
                      ", unitprice " & _
                      ", amount " & _
                      ", expense_remark " & _
                      ", costcenterunkid " & _
                      ", countryunkid " & _
                      ", base_countryunkid " & _
                      ", base_amount " & _
                      ", exchangerateunkid " & _
                      ", exchange_rate " & _
                      ", loginemployeeunkid " & _
                      ", voidloginemployeeunkid " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", isweb " & _
                    ") VALUES (" & _
                      "  @claimretirementtranunkid " & _
                      ", @claimretirementunkid " & _
                      ", @crmasterunkid " & _
                      ", @expenseunkid " & _
                      ", @approvertranunkid " & _
                      ", @approveremployeeunkid " & _
                      ", @quantity " & _
                      ", @unitprice " & _
                      ", @amount " & _
                      ", @expense_remark " & _
                      ", @costcenterunkid " & _
                      ", @countryunkid " & _
                      ", @base_countryunkid " & _
                      ", @base_amount " & _
                      ", @exchangerateunkid " & _
                      ", @exchange_rate " & _
                      ", @loginemployeeunkid " & _
                      ", @voidloginemployeeunkid " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", getdate() " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      ", @isweb " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ATInsertClaimRetirement_Tran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

End Class