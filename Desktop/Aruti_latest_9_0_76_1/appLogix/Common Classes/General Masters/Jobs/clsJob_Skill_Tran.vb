﻿'************************************************************************************************************************************
'Class Name : clsIdentity_tran.vb
'Purpose    :
'Date       :30/06/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsJob_Skill_Tran

#Region " Private Variables "
    Private Shared ReadOnly mstrModuleName As String = "clsJob_Skill_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private mintJobUnkid As Integer = -1
    Private mdtTran As DataTable
    'S.SANDEEP [ 12 OCT 2011 ] -- START
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    Private mintJobSkillTranId As Integer = 0
    'S.SANDEEP [ 12 OCT 2011 ] -- END 
#End Region

#Region " Properties "
    Public Property _JobUnkid() As Integer
        Get
            Return mintJobUnkid
        End Get
        Set(ByVal value As Integer)
            mintJobUnkid = value
            Call Get_Job_Skill()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property
#End Region

#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("JobSkill")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("jobskilltranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("jobunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("skillcategoryunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("skillunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            'S.SANDEEP [23-Mar-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002120|#ARUTI-56}
            mdtTran.Columns.Add("isactive", GetType(System.Boolean)).DefaultValue = True
            'S.SANDEEP [23-Mar-2018] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Methods "
    Private Sub Get_Job_Skill()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim dRowJS_Tran As DataRow
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            strQ = "SELECT " & _
                     "  jobskilltranunkid " & _
                     ", jobunkid " & _
                     ", skillcategoryunkid " & _
                     ", skillunkid " & _
                     ", '' As AUD " & _
                     ",ISNULL(isactive,1) AS isactive " & _
                   "FROM hrjob_skill_tran " & _
                   "WHERE jobunkid = @jobunkid AND ISNULL(isactive,1) = 1 "
            'S.SANDEEP [23-Mar-2018] -- START {#0002120|#ARUTI-56|isactive} -- END

            objDataOperation.AddParameter("@jobunkid ", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    dRowJS_Tran = mdtTran.NewRow()

                    dRowJS_Tran.Item("jobskilltranunkid") = .Item("jobskilltranunkid")
                    dRowJS_Tran.Item("jobunkid") = .Item("jobunkid")
                    dRowJS_Tran.Item("skillcategoryunkid") = .Item("skillcategoryunkid")
                    dRowJS_Tran.Item("skillunkid") = .Item("skillunkid")
                    dRowJS_Tran.Item("AUD") = .Item("AUD")
                    dRowJS_Tran.Item("isactive") = .Item("isactive") 'S.SANDEEP [23-Mar-2018] -- START {#0002120|#ARUTI-56|isactive} -- END


                    mdtTran.Rows.Add(dRowJS_Tran)
                End With
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Job_Skill", mstrModuleName)
        End Try
    End Sub

    Public Function InsertUpdateDelete_JobSkills() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            'objDataOperation.BindTransaction()

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO hrjob_skill_tran ( " & _
                                            "  jobunkid " & _
                                            ", skillcategoryunkid " & _
                                            ", skillunkid " & _
                                            ", isactive " & _
                                       ") VALUES (" & _
                                            "  @jobunkid " & _
                                            ", @skillcategoryunkid " & _
                                            ", @skillunkid " & _
                                            ", @isactive " & _
                                       "); SELECT @@identity"
                                'S.SANDEEP [23-Mar-2018] -- START {#0002120|#ARUTI-56|isactive} -- END

                                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobUnkid.ToString)
                                objDataOperation.AddParameter("@skillcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("skillcategoryunkid").ToString)
                                objDataOperation.AddParameter("@skillunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("skillunkid").ToString)
                                'S.SANDEEP [23-Mar-2018] -- START
                                'ISSUE/ENHANCEMENT : {#0002120|#ARUTI-56}
                                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isactive").ToString)
                                'S.SANDEEP [23-Mar-2018] -- END

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'objDataOperation.ExecNonQuery(strQ)
                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 


                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                mintJobSkillTranId = dsList.Tables(0).Rows(0)(0)

                                If .Item("jobunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", .Item("jobunkid"), "hrjob_skill_tran", "jobskilltranunkid", mintJobSkillTranId, 2, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", mintJobUnkid, "hrjob_skill_tran", "jobskilltranunkid", mintJobSkillTranId, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                            Case "U"
                                strQ = "UPDATE hrjob_skill_tran SET " & _
                                         "  jobunkid = @jobunkid" & _
                                         ", skillcategoryunkid = @skillcategoryunkid" & _
                                         ", skillunkid = @skillunkid " & _
                                         ", Syncdatetime = NULL " & _
                                         ", isactive = @isactive " & _
                                       "WHERE jobskilltranunkid = @jobskilltranunkid "
                                'Sohail (02 Nov 2016) - [Syncdatetime = NULL]
                                'S.SANDEEP [23-Mar-2018] -- START {#0002120|#ARUTI-56|isactive} -- END

                                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("jobunkid").ToString)
                                objDataOperation.AddParameter("@jobskilltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("jobskilltranunkid").ToString)
                                objDataOperation.AddParameter("@skillcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("skillcategoryunkid").ToString)
                                objDataOperation.AddParameter("@skillunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("skillunkid").ToString)
                                'S.SANDEEP [23-Mar-2018] -- START
                                'ISSUE/ENHANCEMENT : {#0002120|#ARUTI-56}
                                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isactive").ToString)
                                'S.SANDEEP [23-Mar-2018] -- END

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", .Item("jobunkid"), "hrjob_skill_tran", "jobskilltranunkid", .Item("jobskilltranunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                            Case "D"

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                If .Item("jobskilltranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", .Item("jobunkid"), "hrjob_skill_tran", "jobskilltranunkid", .Item("jobskilltranunkid"), 2, 3) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END

                                'S.SANDEEP [23-Mar-2018] -- START
                                'ISSUE/ENHANCEMENT : {#0002120|#ARUTI-56}
                                'strQ = "DELETE FROM hrjob_skill_tran " & _
                                '        "WHERE jobskilltranunkid = @jobskilltranunkid "
                                strQ = "UPDATE hrjob_skill_tran SET " & _
                                        "  isactive = @isactive " & _
                                        ",Syncdatetime = NULL " & _
                                        "WHERE jobskilltranunkid = @jobskilltranunkid "
                                'S.SANDEEP [23-Mar-2018] -- END

                                objDataOperation.AddParameter("@jobskilltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("jobskilltranunkid").ToString)
                                'S.SANDEEP [23-Mar-2018] -- START
                                'ISSUE/ENHANCEMENT : {#0002120|#ARUTI-56}
                                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isactive").ToString)
                                'S.SANDEEP [23-Mar-2018] -- END

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_JobSkills", mstrModuleName)
            Return False
        End Try
    End Function
#End Region

End Class
