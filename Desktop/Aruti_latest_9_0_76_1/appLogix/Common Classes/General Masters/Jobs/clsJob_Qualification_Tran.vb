﻿'************************************************************************************************************************************
'Class Name : clsHrjob_qualification_Tran.vb
'Purpose    :
'Date       :20/12/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsJob_Qualification_Tran

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "clsJob_Qualification_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private mintJobUnkid As Integer = -1
    Private mdtTran As DataTable
    Private mintJobQualificationTranId As Integer

#End Region

#Region " Properties "

    Public Property _JobUnkid() As Integer
        Get
            Return mintJobUnkid
        End Get
        Set(ByVal value As Integer)
            mintJobUnkid = value
            Call Get_Job_Qualification()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New()
        mdtTran = New DataTable("JobQualification")
        Try
            mdtTran.Columns.Add("jobqualificationtranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("jobunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("qualificationgroupunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("qualificationunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("QualificationGrp", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("Qualification", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
            'S.SANDEEP [23-Mar-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002120|#ARUTI-56}
            mdtTran.Columns.Add("isactive", GetType(System.Boolean)).DefaultValue = True
            'S.SANDEEP [23-Mar-2018] -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub Get_Job_Qualification()
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim dRow As DataRow = Nothing
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                            " jobqualificationtranunkid " & _
                            ",hrjob_qualification_tran.jobunkid " & _
                            ",hrjob_qualification_tran.qualificationgroupunkid " & _
                            ",hrjob_qualification_tran.qualificationunkid " & _
                            ",ISNULL(cfcommon_master.name,'') AS QualificationGrp " & _
                            ",ISNULL(hrqualification_master.qualificationname,'') AS Qualification " & _
                            ",'' AS AUD " & _
                            ",ISNULL(hrjob_qualification_tran.isactive,1) AS isactive " & _
                        "FROM hrjob_qualification_tran " & _
                            "LEFT JOIN hrqualification_master ON hrjob_qualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                            "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrjob_qualification_tran.qualificationgroupunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP & "  " & _
                        "WHERE hrjob_qualification_tran.jobunkid = @jobunkid AND ISNULL(hrjob_qualification_tran.isactive,1) = 1 "
            'S.SANDEEP [23-Mar-2018] -- START {#0002120|#ARUTI-56|isactive} -- END

            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobUnkid.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                dRow = mdtTran.NewRow
                'S.SANDEEP [23-Mar-2018] -- START
                'ISSUE/ENHANCEMENT : {#0002120|#ARUTI-56}
                'dRow.Item("jobqualificationtranunkid") = dtRow.Item("jobqualificationtranunkid")
                'dRow.Item("jobunkid") = dtRow.Item("jobunkid")
                'dRow.Item("qualificationgroupunkid") = dtRow.Item("qualificationgroupunkid")
                'dRow.Item("qualificationunkid") = dtRow.Item("qualificationunkid")
                'dRow.Item("QualificationGrp") = dtRow.Item("QualificationGrp")
                'dRow.Item("Qualification") = dtRow.Item("Qualification")
                'dRow.Item("AUD") = dtRow.Item("AUD")
                'mdtTran.Rows.Add(dRow)
                mdtTran.ImportRow(dtRow)
                'S.SANDEEP [23-Mar-2018] -- END
            Next


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Job_Qualification; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function InsertUpdateDelete_JobQualification() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO hrjob_qualification_tran ( " & _
                                            "  jobunkid " & _
                                            ", qualificationgroupunkid " & _
                                            ", qualificationunkid" & _
                                            ", isactive " & _
                                       ") VALUES (" & _
                                            "  @jobunkid " & _
                                            ", @qualificationgroupunkid " & _
                                            ", @qualificationunkid" & _
                                            ", @isactive " & _
                                       "); SELECT @@identity"
                                'S.SANDEEP [23-Mar-2018] -- START {#0002120|#ARUTI-56|isactive} -- END
                                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobUnkid.ToString)
                                objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationgroupunkid").ToString)
                                objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationunkid").ToString)
                                'S.SANDEEP [23-Mar-2018] -- START
                                'ISSUE/ENHANCEMENT : {#0002120|#ARUTI-56}
                                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isactive").ToString)
                                'S.SANDEEP [23-Mar-2018] -- END

                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintJobQualificationTranId = dsList.Tables(0).Rows(0)(0)

                                If .Item("jobunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", .Item("jobunkid"), "hrjob_qualification_tran", "jobqualificationtranunkid ", mintJobQualificationTranId, 2, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", mintJobUnkid, "hrjob_qualification_tran", "jobqualificationtranunkid ", mintJobQualificationTranId, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                            Case "U"
                                strQ = "UPDATE hrjob_qualification_tran SET " & _
                                        "  jobunkid = @jobunkid" & _
                                        ", qualificationgroupunkid = @qualificationgroupunkid" & _
                                        ", qualificationunkid = @qualificationunkid " & _
                                        ", Syncdatetime = NULL " & _
                                        ", isactive = @isactive " & _
                                       "WHERE jobqualificationtranunkid = @jobqualificationtranunkid "
                                'Sohail (02 Nov 2016) - [Syncdatetime = NULL]

                                'S.SANDEEP [23-Mar-2018] -- START {#0002120|#ARUTI-56|isactive} -- END

                                objDataOperation.AddParameter("@jobqualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("jobqualificationtranunkid").ToString)
                                objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("jobunkid").ToString)
                                objDataOperation.AddParameter("@qualificationgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationgroupunkid").ToString)
                                objDataOperation.AddParameter("@qualificationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("qualificationunkid").ToString)
                                'S.SANDEEP [23-Mar-2018] -- START
                                'ISSUE/ENHANCEMENT : {#0002120|#ARUTI-56}
                                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isactive").ToString)
                                'S.SANDEEP [23-Mar-2018] -- END

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", .Item("jobunkid"), "hrjob_qualification_tran", "jobqualificationtranunkid", .Item("jobqualificationtranunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"

                                If .Item("jobqualificationtranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrjob_master", "jobunkid", .Item("jobunkid"), "hrjob_qualification_tran", "jobqualificationtranunkid", .Item("jobqualificationtranunkid"), 2, 3) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                                'S.SANDEEP [23-Mar-2018] -- START
                                'ISSUE/ENHANCEMENT : {#0002120|#ARUTI-56}
                                'strQ = "DELETE FROM hrjob_qualification_tran " & _
                                '       "WHERE jobqualificationtranunkid = @jobqualificationtranunkid "

                                strQ = "UPDATE hrjob_qualification_tran SET " & _
                                       " isactive = @isactive " & _
                                       ",Syncdatetime = NULL " & _
                                       "WHERE jobqualificationtranunkid = @jobqualificationtranunkid "
                                'S.SANDEEP [23-Mar-2018] -- END
                                

                                objDataOperation.AddParameter("@jobqualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("jobqualificationtranunkid").ToString)
                                'S.SANDEEP [23-Mar-2018] -- START
                                'ISSUE/ENHANCEMENT : {#0002120|#ARUTI-56}
                                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isactive").ToString)
                                'S.SANDEEP [23-Mar-2018] -- END

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_JobQualification; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

End Class
