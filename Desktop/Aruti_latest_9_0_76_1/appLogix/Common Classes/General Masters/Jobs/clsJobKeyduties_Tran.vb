﻿'************************************************************************************************************************************
'Class Name : clsjobkeyduties_Tran.vb
'Purpose    :
'Date       :12/3/2015
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsJobKeyduties_Tran
    Private Const mstrModuleName = "clsJobKeyduties_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintJobkeydutiesunkid As Integer
    Private mintJobunkid As Integer
    Private mstrKeyduties As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdtKeyDuties As DataTable = Nothing

#End Region

#Region "Constructor"

    Public Sub New()
        mdtKeyDuties = New DataTable("JobKeyDuties")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("jobkeydutiesunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtKeyDuties.Columns.Add(dCol)

            dCol = New DataColumn("jobunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtKeyDuties.Columns.Add(dCol)

            dCol = New DataColumn("keyduties")
            dCol.DataType = System.Type.GetType("System.String")
            mdtKeyDuties.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtKeyDuties.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtKeyDuties.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = value
            GetKeyDuties_tran()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdtKeyDuties
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _mdtKeyDuties() As DataTable
        Get
            Return mdtKeyDuties
        End Get
        Set(ByVal value As DataTable)
            mdtKeyDuties = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetKeyDuties_tran() As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  jobkeydutiesunkid " & _
                      ", jobunkid " & _
                      ", keyduties " & _
                      ", '' AS AUD " & _
                      ", '' AS GUID " & _
                     "FROM hrjobkeyduties_tran " & _
                     " WHERE isvoid  = 0 AND jobunkid  = @jobunkid "

            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid)

            dsList = objDataOperation.ExecQuery(strQ, "KeyDuties")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtKeyDuties.Clear()
            Dim drKduties_Row As DataRow = Nothing
            For i As Integer = 0 To dsList.Tables("KeyDuties").Rows.Count - 1
                With dsList.Tables("KeyDuties").Rows(i)
                    drKduties_Row = mdtKeyDuties.NewRow()
                    drKduties_Row.Item("jobkeydutiesunkid") = .Item("jobkeydutiesunkid")
                    drKduties_Row.Item("jobunkid") = .Item("jobunkid")
                    drKduties_Row.Item("keyduties") = .Item("keyduties")
                    drKduties_Row.Item("AUD") = .Item("AUD")
                    drKduties_Row.Item("GUID") = .Item("GUID")
                    mdtKeyDuties.Rows.Add(drKduties_Row)
                End With
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetKeyDuties_tran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertUpdateDelete_JobKeyDuties(ByVal objDoOperation As clsDataOperation) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            For i = 0 To mdtKeyDuties.Rows.Count - 1
                With mdtKeyDuties.Rows(i)
                    objDoOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"

                                objDoOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
                                objDoOperation.AddParameter("@keyduties", SqlDbType.NVarChar, .Item("keyduties").ToString.Trim.Length, .Item("keyduties").ToString)
                                objDoOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDoOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                objDoOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                objDoOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                                objDoOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)

                                strQ = "INSERT INTO hrjobkeyduties_tran ( " & _
                                          "  jobunkid " & _
                                          ", keyduties " & _
                                          ", userunkid " & _
                                          ", isvoid " & _
                                          ", voiduserunkid " & _
                                          ", voiddatetime " & _
                                          ", voidreason" & _
                                        ") VALUES (" & _
                                          "  @jobunkid " & _
                                          ", @keyduties " & _
                                          ", @userunkid " & _
                                          ", @isvoid " & _
                                          ", @voiduserunkid " & _
                                          ", @voiddatetime " & _
                                          ", @voidreason" & _
                                        "); SELECT @@identity"


                                Dim dsList As DataSet = objDoOperation.ExecQuery(strQ, "List")

                                If objDoOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintJobkeydutiesunkid = dsList.Tables(0).Rows(0)(0)

                                If .Item("jobunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDoOperation, "hrjob_master", "jobunkid", mintJobunkid, "hrjobkeyduties_tran", "jobkeydutiesunkid ", mintJobkeydutiesunkid, 2, 1) = False Then
                                        exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDoOperation, "hrjob_master", "jobunkid", mintJobunkid, "hrjobkeyduties_tran", "jobkeydutiesunkid ", mintJobkeydutiesunkid, 1, 1) = False Then
                                        exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                            Case "U"

                                strQ = "UPDATE hrjobkeyduties_tran SET " & _
                                          "  jobunkid = @jobunkid " & _
                                          ", keyduties = @keyduties " & _
                                          " WHERE jobkeydutiesunkid = @jobkeydutiesunkid "


                                objDoOperation.AddParameter("@jobkeydutiesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("jobkeydutiesunkid").ToString)
                                objDoOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid)
                                objDoOperation.AddParameter("@keyduties", SqlDbType.NVarChar, .Item("keyduties").ToString.Trim.Length, .Item("keyduties").ToString)

                                objDoOperation.ExecNonQuery(strQ)

                                If objDoOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_TranAtLog(objDoOperation, "hrjob_master", "jobunkid", .Item("jobunkid"), "hrjobkeyduties_tran", "jobkeydutiesunkid", .Item("jobkeydutiesunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"

                                strQ = "UPDATE hrjobkeyduties_tran SET " & _
                                           " isvoid = @isvoid " & _
                                          ", voiduserunkid = @voiduserunkid " & _
                                          ", voiddatetime = getdate() " & _
                                          ", voidreason = @voidreason " & _
                                          " WHERE jobkeydutiesunkid = @jobkeydutiesunkid "

                                objDoOperation.AddParameter("@jobkeydutiesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("jobkeydutiesunkid").ToString)
                                objDoOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                                objDoOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDoOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "")
                                Call objDoOperation.ExecNonQuery(strQ)

                                If objDoOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If .Item("jobkeydutiesunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDoOperation, "hrjob_master", "jobunkid", .Item("jobunkid"), "hrjobkeyduties_tran", "jobkeydutiesunkid", .Item("jobkeydutiesunkid"), 2, 3) = False Then
                                        exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                        End Select

                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_JobKeyDuties; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@jobkeydutiesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  jobkeydutiesunkid " & _
                      ", jobunkid " & _
                      ", keyduties " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     "FROM hrjobkeyduties_tran " & _
                     "WHERE  isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND jobkeydutiesunkid <> @jobkeydutiesunkid"
            End If

            objDataOperation.AddParameter("@jobkeydutiesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function DeleteKeyDuties_tran(ByVal objDoOperation As clsDataOperation, ByVal intJobID As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try


            strQ = " SELECT ISNULL(jobkeydutiesunkid,0) AS  jobkeydutiesunkid FROM hrjobkeyduties_tran WHERE  isvoid = 0 AND jobunkid = @jobunkid "
            objDoOperation.ClearParameters()
            objDoOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobID)
            Dim dsList As DataSet = objDoOperation.ExecQuery(strQ, "List")


            strQ = "UPDATE hrjobkeyduties_tran SET " & _
                      " isvoid = @isvoid " & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", voiddatetime = getdate() " & _
                      ", voidreason = @voidreason " & _
                      " WHERE jobunkid = @jobunkid AND isvoid = 0 "

            objDoOperation.ClearParameters()
            objDoOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobID)
            objDoOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDoOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDoOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "")
            Call objDoOperation.ExecNonQuery(strQ)

            If objDoOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsList.Tables(0).Rows
                    If clsCommonATLog.Insert_TranAtLog(objDoOperation, "hrjob_master", "jobunkid", intJobID, "hrjobkeyduties_tran", "jobkeydutiesunkid", CInt(dr("jobkeydutiesunkid")), 3, 3) = False Then
                        exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If
            dsList = Nothing
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DeleteKeyDuties_tran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
    End Function

End Class