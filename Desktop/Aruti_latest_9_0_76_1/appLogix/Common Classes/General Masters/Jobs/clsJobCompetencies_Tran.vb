﻿'************************************************************************************************************************************
'Class Name : clsJobCompetencies_Tran.vb
'Purpose    :
'Date       :12/4/2015
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsJobCompetencies_Tran
    Private Const mstrModuleName = "clsJobCompetencies_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintKJobCompetencetranunkid As Integer
    Private mintJobunkid As Integer
    Private mintCompetence_Categoryunkid As Integer
    Private mintCompetenciesunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdtCompetenciesTran As DataTable = Nothing

#End Region

#Region "Constructor"

    Public Sub New()
        mdtCompetenciesTran = New DataTable("JobCompetencies")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("jobcompetencetranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtCompetenciesTran.Columns.Add(dCol)

            dCol = New DataColumn("jobunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtCompetenciesTran.Columns.Add(dCol)

            dCol = New DataColumn("competence_categoryunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtCompetenciesTran.Columns.Add(dCol)

            dCol = New DataColumn("competence_category")
            dCol.DataType = System.Type.GetType("System.String")
            mdtCompetenciesTran.Columns.Add(dCol)

            dCol = New DataColumn("competenciesunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtCompetenciesTran.Columns.Add(dCol)

            dCol = New DataColumn("competency")
            dCol.DataType = System.Type.GetType("System.String")
            mdtCompetenciesTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtCompetenciesTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtCompetenciesTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = value
            GetCompetencies_tran()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdtCompetenciesTran
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _mdtCompetenciesTran() As DataTable
        Get
            Return mdtCompetenciesTran
        End Get
        Set(ByVal value As DataTable)
            mdtCompetenciesTran = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetCompetencies_tran() As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                     " jobcompetencetranunkid " & _
                     ", hrjobcompetencies_tran.jobunkid " & _
                     ", hrjobcompetencies_tran.competence_categoryunkid " & _
                     ", ISNULL(cfcommon_master.name,'') AS  competence_category " & _
                     ", hrjobcompetencies_tran.competenciesunkid " & _
                     ", ISNULL(hrassess_competencies_master.name,'') AS  competency " & _
                     ", userunkid " & _
                     ", '' AS AUD " & _
                     ", '' AS GUID " & _
                     " FROM hrjobcompetencies_tran " & _
                     " LEFT JOIN cfcommon_master ON hrjobcompetencies_tran.competence_categoryunkid = cfcommon_master.masterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES & "' " & _
                     " LEFT JOIN hrassess_competencies_master ON hrjobcompetencies_tran.competenciesunkid = hrassess_competencies_master.competenciesunkid " & _
                     " WHERE isvoid  = 0 AND hrjobcompetencies_tran.jobunkid  = @jobunkid "

            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid)

            dsList = objDataOperation.ExecQuery(strQ, "JobCompetencies")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtCompetenciesTran.Clear()
            Dim drcompetecy_Row As DataRow = Nothing
            For i As Integer = 0 To dsList.Tables("JobCompetencies").Rows.Count - 1
                With dsList.Tables("JobCompetencies").Rows(i)
                    drcompetecy_Row = mdtCompetenciesTran.NewRow()
                    drcompetecy_Row.Item("jobcompetencetranunkid") = .Item("jobcompetencetranunkid")
                    drcompetecy_Row.Item("jobunkid") = .Item("jobunkid")
                    drcompetecy_Row.Item("competence_categoryunkid") = .Item("competence_categoryunkid")
                    drcompetecy_Row.Item("competence_category") = .Item("competence_category")
                    drcompetecy_Row.Item("competenciesunkid") = .Item("competenciesunkid")
                    drcompetecy_Row.Item("competency") = .Item("competency")
                    drcompetecy_Row.Item("AUD") = .Item("AUD")
                    drcompetecy_Row.Item("GUID") = .Item("GUID")
                    mdtCompetenciesTran.Rows.Add(drcompetecy_Row)
                End With
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCompetencies_tran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertUpdateDelete_JobCompetencies(ByVal objDoOperation As clsDataOperation) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            For i = 0 To mdtCompetenciesTran.Rows.Count - 1
                With mdtCompetenciesTran.Rows(i)
                    objDoOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"

                                objDoOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
                                objDoOperation.AddParameter("@competence_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("competence_categoryunkid").ToString)
                                objDoOperation.AddParameter("@competenciesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("competenciesunkid").ToString)
                                objDoOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDoOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                objDoOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                objDoOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                                objDoOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)

                                strQ = "INSERT INTO hrjobcompetencies_tran ( " & _
                                          "  jobunkid " & _
                                          ", competence_categoryunkid " & _
                                          ", competenciesunkid " & _
                                          ", userunkid " & _
                                          ", isvoid " & _
                                          ", voiduserunkid " & _
                                          ", voiddatetime " & _
                                          ", voidreason" & _
                                        ") VALUES (" & _
                                          "  @jobunkid " & _
                                          ", @competence_categoryunkid " & _
                                          ", @competenciesunkid " & _
                                          ", @userunkid " & _
                                          ", @isvoid " & _
                                          ", @voiduserunkid " & _
                                          ", @voiddatetime " & _
                                          ", @voidreason" & _
                                        "); SELECT @@identity"


                                Dim dsList As DataSet = objDoOperation.ExecQuery(strQ, "List")

                                If objDoOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintKJobCompetencetranunkid = dsList.Tables(0).Rows(0)(0)

                                If .Item("jobunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDoOperation, "hrjob_master", "jobunkid", mintJobunkid, "hrjobcompetencies_tran", "jobcompetencetranunkid ", mintKJobCompetencetranunkid, 2, 1) = False Then
                                        exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDoOperation, "hrjob_master", "jobunkid", mintJobunkid, "hrjobcompetencies_tran", "jobcompetencetranunkid ", mintKJobCompetencetranunkid, 1, 1) = False Then
                                        exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                            Case "U"

                                strQ = "UPDATE hrjobcompetencies_tran SET " & _
                                          "  jobunkid = @jobunkid " & _
                                          ", competence_categoryunkid =  @competence_categoryunkid " & _
                                          ", competenciesunkid = @competenciesunkid " & _
                                          " WHERE jobcompetencetranunkid = @jobcompetencetranunkid "


                                objDoOperation.AddParameter("@jobcompetencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("jobcompetencetranunkid").ToString)
                                objDoOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid)
                                objDoOperation.AddParameter("@competence_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("competence_categoryunkid").ToString)
                                objDoOperation.AddParameter("@competenciesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("competenciesunkid").ToString)

                                objDoOperation.ExecNonQuery(strQ)

                                If objDoOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_TranAtLog(objDoOperation, "hrjob_master", "jobunkid", .Item("jobunkid"), "hrjobcompetencies_tran", "jobcompetencetranunkid", .Item("jobcompetencetranunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"

                                strQ = "UPDATE hrjobcompetencies_tran SET " & _
                                           " isvoid = @isvoid " & _
                                          ", voiduserunkid = @voiduserunkid " & _
                                          ", voiddatetime = getdate() " & _
                                          ", voidreason = @voidreason " & _
                                          " WHERE jobcompetencetranunkid = @jobcompetencetranunkid "

                                objDoOperation.AddParameter("@jobcompetencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("jobcompetencetranunkid").ToString)
                                objDoOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                                objDoOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDoOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "")
                                Call objDoOperation.ExecNonQuery(strQ)

                                If objDoOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If .Item("jobcompetencetranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDoOperation, "hrjob_master", "jobunkid", .Item("jobunkid"), "hrjobcompetencies_tran", "jobcompetencetranunkid", .Item("jobcompetencetranunkid"), 2, 3) = False Then
                                        exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                        End Select

                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_JobCompetencies; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  jobcompetencetranunkid " & _
                      ", jobunkid " & _
                      ", competence_categoryunkid  " & _
                      ", competenciesunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     "FROM hrjobcompetencies_tran " & _
                     "WHERE  isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND jobcompetencetranunkid <> @jobcompetencetranunkid"
            End If

            objDataOperation.AddParameter("@jobcompetencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function DeleteJobCompetencies_tran(ByVal objDoOperation As clsDataOperation, ByVal intJobID As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try


            strQ = " SELECT ISNULL(jobcompetencetranunkid,0) AS  jobcompetencetranunkid FROM hrjobcompetencies_tran WHERE  isvoid = 0 AND jobunkid = @jobunkid "
            objDoOperation.ClearParameters()
            objDoOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobID)
            Dim dsList As DataSet = objDoOperation.ExecQuery(strQ, "List")


            strQ = "UPDATE hrjobcompetencies_tran SET " & _
                      " isvoid = @isvoid " & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", voiddatetime = getdate() " & _
                      ", voidreason = @voidreason " & _
                      " WHERE jobunkid = @jobunkid AND isvoid = 0 "

            objDoOperation.ClearParameters()
            objDoOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intJobID)
            objDoOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDoOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDoOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "")
            Call objDoOperation.ExecNonQuery(strQ)

            If objDoOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsList.Tables(0).Rows
                    If clsCommonATLog.Insert_TranAtLog(objDoOperation, "hrjob_master", "jobunkid", intJobID, "hrjobcompetencies_tran", "jobcompetencetranunkid", CInt(dr("jobcompetencetranunkid")), 3, 3) = False Then
                        exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If
            dsList = Nothing
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DeleteJobCompetencies_tran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
        End Try
    End Function

End Class