﻿'************************************************************************************************************************************
'Class Name : clsGradeGroup.vb
'Purpose    :
'Date       :6/24/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsGradeGroup
    Private Shared ReadOnly mstrModuleName As String = "clsGradeGroup"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintGradegroupunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
    Private mintPriority As Integer = 0 'Sohail (27 Apr 2016)
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradegroupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Gradegroupunkid() As Integer
        Get
            Return mintGradegroupunkid
        End Get
        Set(ByVal value As Integer)
            mintGradegroupunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = value
        End Set
    End Property

    'Sohail (27 Apr 2016) -- Start
    'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property
    'Sohail (27 Apr 2016) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  gradegroupunkid " & _
                      ", code " & _
                      ", name " & _
                      ", description " & _
                      ", isactive " & _
                      ", name1 " & _
                      ", name2 " & _
                      ", ISNULL(priority, 0) AS priority " & _
                    "FROM hrgradegroup_master " & _
                    "WHERE gradegroupunkid = @gradegroupunkid "
            'Sohail (27 Apr 2016) - [priority]

            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintGradegroupunkid = CInt(dtRow.Item("gradegroupunkid"))
                mstrCode = dtRow.Item("code").ToString
                mstrName = dtRow.Item("name").ToString
                mstrDescription = dtRow.Item("description").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrName1 = dtRow.Item("name1").ToString
                mstrName2 = dtRow.Item("name2").ToString
                mintPriority = CInt(dtRow.Item("priority")) 'Sohail (27 Apr 2016)
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  gradegroupunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2 " & _
              ", ISNULL(priority, 0) AS priority " & _
             "FROM hrgradegroup_master "
            'Sohail (27 Apr 2016) - [priority]

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrgradegroup_master) </purpose>
    Public Function Insert() As Boolean
      

        If isExist(mstrCode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Grade Group Code is already defined. Please define new Grade Group Code.")
            Return False
        End If

        If isExist(, mstrName) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Grade Group Name is already defined. Please define new Grade Group Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority) 'Sohail (27 Apr 2016)

            strQ = "INSERT INTO hrgradegroup_master ( " & _
                            "  code " & _
                            ", name " & _
                            ", description " & _
                            ", isactive " & _
                            ", name1 " & _
                            ", name2" & _
                            ", priority" & _
                     ") VALUES (" & _
                            "  @code " & _
                            ", @name " & _
                            ", @description " & _
                            ", @isactive " & _
                            ", @name1 " & _
                            ", @name2" & _
                            ", @priority" & _
                    "); SELECT @@identity"
            'Sohail (27 Apr 2016) - [priority]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintGradegroupunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE   
            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrgradegroup_master", "gradegroupunkid", mintGradegroupunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrgradegroup_master) </purpose>
    Public Function Update() As Boolean

        If isExist(mstrCode, , mintGradegroupunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Grade Group Code is already defined. Please define new Grade Group Code.")
            Return False
        End If

        If isExist(, mstrName, mintGradegroupunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Grade Group Name is already defined. Please define new Grade Group Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority) 'Sohail (27 Apr 2016)

            strQ = "UPDATE hrgradegroup_master SET " & _
              "  code = @code" & _
              ", name = @name" & _
              ", description = @description" & _
              ", isactive = @isactive" & _
              ", name1 = @name1" & _
              ", name2 = @name2 " & _
              ", priority = @priority " & _
            "WHERE gradegroupunkid = @gradegroupunkid "
            'Sohail (27 Apr 2016) - [priority]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE   
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrgradegroup_master", mintGradegroupunkid, "gradegroupunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrgradegroup_master", "gradegroupunkid", mintGradegroupunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 


            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (27 Apr 2016) -- Start
    'Enhancement - 61.1 - Priority Order on Grade Group, Grade and Grade Level; Auto Salary Increment to next Grade Level as per Priority Order; Grade filter on Wages Table; Export and Import for Wages Table for KBC.
    '#23 (KBC & Kenya Project Comments List.xls) - Auto Salary incrment Per employee Anniversary,Employee Anniversay,All KBC Employees have Annivesary date for Salary increment,date is sent  when employee is confimed ,the salary is done automatically by the system when annivesary is due from current level to next level and stops at the Last level (notch),unless changed to higher Grade
    Public Function UpdatePriority(ByVal dtTable As DataTable) As Boolean

       
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        objDataOperation.BindTransaction()

        Try

            For Each dtRow As DataRow In dtTable.Rows

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtRow.Item("gradegroupunkid")))
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtRow.Item("priority")))

                strQ = "UPDATE hrgradegroup_master SET " & _
                            " priority = @priority " & _
                        "WHERE gradegroupunkid = @gradegroupunkid "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrgradegroup_master", CInt(dtRow.Item("gradegroupunkid")), "gradegroupunkid", 2) Then
                    If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrgradegroup_master", "gradegroupunkid", CInt(dtRow.Item("gradegroupunkid"))) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

            Next

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdatePriority; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (27 Apr 2016) -- End

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrgradegroup_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this Grade Group. Reason : This Grade Group is already linked with some transaction.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            strQ = "UPDATE hrgradegroup_master SET " & _
                    " isactive = 0 " & _
            "WHERE gradegroupunkid = @gradegroupunkid "

            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE   
            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrgradegroup_master", "gradegroupunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Sandeep [ 18 Aug 2010 ] -- Start
        Dim dsTables As DataSet = Nothing
        Dim blnIsUsed As Boolean = False
        'Sandeep [ 18 Aug 2010 ] -- End 

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                  " TABLE_NAME AS TableName " & _
                  " FROM INFORMATION_SCHEMA.COLUMNS " & _
                  " WHERE COLUMN_NAME='gradegroupunkid' "

            dsTables = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            strQ = ""
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            For Each dtRow As DataRow In dsTables.Tables("List").Rows
                If dtRow.Item("TableName") = "hrgradegroup_master" Then Continue For
                strQ = "SELECT gradegroupunkid FROM " & dtRow.Item("TableName").ToString & " WHERE gradegroupunkid = @gradegroupunkid "
                dsList = objDataOperation.ExecQuery(strQ, "Used")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1, Optional ByRef intRetUnkIdIfExist As Integer = 0) As Boolean
        'Sohail (10 Nov 2014) - [intRetUnkIdIfExist]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        intRetUnkIdIfExist = 0 'Sohail (10 Nov 2014)

        Try
            strQ = "SELECT " & _
              "  gradegroupunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2 " & _
             "FROM hrgradegroup_master " & _
             "WHERE 1=1 "


            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strName.Length > 0 Then
                strQ &= "AND name = @name "
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If strCode.Length > 0 Then
                strQ &= "AND code = @code "
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If

            If intUnkid > 0 Then
                strQ &= " AND gradegroupunkid <> @gradegroupunkid"
                objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (10 Nov 2014) -- Start
            'Enhancement - Change Grade option on Import Salary Change.
            If dsList.Tables(0).Rows.Count > 0 Then
                intRetUnkIdIfExist = CInt(dsList.Tables(0).Rows(0).Item("gradegroupunkid"))
            End If
            'Sohail (10 Nov 2014) -- End

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal mblnFlag As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If mblnFlag = True Then
                strQ = "SELECT 0 As gradegroupunkid , @ItemName As  name  UNION "
            End If
            strQ &= "SELECT gradegroupunkid,name FROM hrgradegroup_master WHERE isactive =1 "

            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        End Try
    End Function

    'Pinkal (10-Mar-2011) --Start

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetGradeGrpUnkId(ByVal mstrName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = " SELECT " & _
                      " gradegroupunkid " & _
                      "  FROM hrgradegroup_master " & _
                      " WHERE name = @name "

            'S.SANDEEP [19 AUG 2016] -- START
            'ISSUE : ISACTIVE WAS NOT KEPT FOR CHECKING
            strQ &= " AND isactive = 1 "
            'S.SANDEEP [19 AUG 2016] -- START

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("gradegroupunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetGradeGrpUnkId", mstrModuleName)
        End Try
        Return -1

    End Function

    'Pinkal (10-Mar-2011) --End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Grade Group Code is already defined. Please define new Grade Group Code.")
			Language.setMessage(mstrModuleName, 2, "This Grade Group Name is already defined. Please define new Grade Group Name.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "Sorry, you cannot delete this Grade Group. Reason : This Grade Group is already linked with some transaction.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class