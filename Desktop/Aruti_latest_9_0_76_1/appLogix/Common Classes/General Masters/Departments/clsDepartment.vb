﻿'************************************************************************************************************************************
'Class Name : clsDepartment.vb
'Purpose    :
'Date       :26/06/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsDepartment
    Private Shared ReadOnly mstrModuleName As String = "clsDepartment"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintDepartmentunkid As Integer
    Private mintDeptgroupunkid As Integer
    Private mintStationunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set departmentunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Departmentunkid() As Integer
        Get
            Return mintDepartmentunkid
        End Get
        Set(ByVal value As Integer)
            mintDepartmentunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set deptgroupunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Deptgroupunkid() As Integer
        Get
            Return mintDeptgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintDeptgroupunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stationunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Stationunkid() As Integer
        Get
            Return mintStationunkid
        End Get
        Set(ByVal value As Integer)
            mintStationunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  departmentunkid " & _
              ", deptgroupunkid " & _
              ", stationunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2 " & _
             "FROM hrdepartment_master " & _
             "WHERE departmentunkid = @departmentunkid "

            objDataOperation.AddParameter("@departmentunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintDepartmentUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintdepartmentunkid = CInt(dtRow.Item("departmentunkid"))
                mintdeptgroupunkid = CInt(dtRow.Item("deptgroupunkid"))
                mintstationunkid = CInt(dtRow.Item("stationunkid"))
                mstrcode = dtRow.Item("code").ToString
                mstrname = dtRow.Item("name").ToString
                mstrdescription = dtRow.Item("description").ToString
                mblnisactive = CBool(dtRow.Item("isactive"))
                mstrname1 = dtRow.Item("name1").ToString
                mstrname2 = dtRow.Item("name2").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                            "	  hrdepartment_master.departmentunkid " & _
                            "	, hrdepartment_master.deptgroupunkid " & _
                            "	, hrdepartment_master.stationunkid " & _
                            "	, hrdepartment_master.code " & _
                            "	, hrdepartment_master.name " & _
                            "	, hrdepartment_master.description " & _
                            "	, hrdepartment_master.isactive " & _
                            "	, hrdepartment_master.name1 " & _
                            "	, hrdepartment_master.name2 " & _
                            "	, hrstation_master.name AS StationName " & _
                            "	, hrdepartment_group_master.name AS DeptGroupName " & _
                    "FROM hrdepartment_master " & _
                        "	LEFT JOIN hrstation_master ON hrdepartment_master.stationunkid = hrstation_master.stationunkid " & _
                        "	LEFT JOIN hrdepartment_group_master ON hrdepartment_master.deptgroupunkid = hrdepartment_group_master.deptgroupunkid "

            If blnOnlyActive Then
                strQ &= " WHERE hrdepartment_master.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrdepartment_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Department Code is already defined. Please define new Department Code.")
            Return False
        End If

        If isExist(, mstrName) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Department Name is already defined. Please define new Department Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintdeptgroupunkid.ToString)
            objDataOperation.AddParameter("@stationunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstationunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname2.ToString)

            strQ = "INSERT INTO hrdepartment_master ( " & _
                          "  deptgroupunkid " & _
                          ", stationunkid " & _
                          ", code " & _
                          ", name " & _
                          ", description " & _
                          ", isactive " & _
                          ", name1 " & _
                          ", name2" & _
                    ") VALUES (" & _
                          "  @deptgroupunkid " & _
                          ", @stationunkid " & _
                          ", @code " & _
                          ", @name " & _
                          ", @description " & _
                          ", @isactive " & _
                          ", @name1 " & _
                          ", @name2" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintDepartmentunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE   
            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrdepartment_master", "departmentunkid", mintDepartmentunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 


            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim objUserAddEdit As New clsUserAddEdit
            'If objUserAddEdit.InsertAccess(1, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, mintDepartmentunkid, 0, enAllocation.DEPARTMENT) = True Then
            '    If User._Object._Userunkid = 1 Then
            '        'S.SANDEEP [ 01 JUNE 2012 ] -- START
            '        'ENHANCEMENT : TRA DISCIPLINE CHANGES
            '        'If ConfigParameter._Object._UserAccessModeSetting = enAllocation.DEPARTMENT Then
            '        '    If UserAccessLevel._AccessLevel.Trim.Length > 0 Then
            '        '        UserAccessLevel._AccessLevel = UserAccessLevel._AccessLevel & "," & mintDepartmentunkid.ToString
            '        '    End If
            '        'End If
            '        Dim arrId() As String = ConfigParameter._Object._UserAccessModeSetting.Split(",")
            '        Dim objMaster As New clsMasterData
            '        For i As Integer = 0 To arrId.Length - 1
            '            If CInt(arrId(i)) = enAllocation.DEPARTMENT Then
            '                objMaster.GetUserAccessLevel()
            '            End If
            '        Next
            '        'S.SANDEEP [ 01 JUNE 2012 ] -- END
            '    End If
            'End If
            'objUserAddEdit = Nothing
            'S.SANDEEP [04 JUN 2015] -- END

            
            'S.SANDEEP [ 04 FEB 2012 ] -- END



            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrdepartment_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrCode, , mintDepartmentunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Department Code is already defined. Please define new Department Code.")
            Return False
        End If

        If isExist(, mstrName, mintDepartmentunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Department Name is already defined. Please define new Department Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintdepartmentunkid.ToString)
            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintdeptgroupunkid.ToString)
            objDataOperation.AddParameter("@stationunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstationunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname2.ToString)

            StrQ = "UPDATE hrdepartment_master SET " & _
              "  deptgroupunkid = @deptgroupunkid" & _
              ", stationunkid = @stationunkid" & _
              ", code = @code" & _
              ", name = @name" & _
              ", description = @description" & _
              ", isactive = @isactive" & _
              ", name1 = @name1" & _
              ", name2 = @name2 " & _
            "WHERE departmentunkid = @departmentunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrdepartment_master", mintDepartmentunkid, "departmentunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrdepartment_master", "departmentunkid", mintDepartmentunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrdepartment_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this Department. Reason : This Department is already linked with some transaction.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            strQ = "UPDATE hrdepartment_master SET " & _
                    " isactive = 0 " & _
            "WHERE departmentunkid = @departmentunkid "

            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE   
            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrdepartment_master", "departmentunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Sandeep [ 18 Aug 2010 ] -- Start
        Dim dsTables As DataSet = Nothing
        Dim blnIsUsed As Boolean = False
        'Sandeep [ 18 Aug 2010 ] -- End 

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                       "TABLE_NAME AS TableName " & _
                    "FROM INFORMATION_SCHEMA.COLUMNS " & _
                    "WHERE COLUMN_NAME='departmentunkid' "

            dsTables = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            strQ = ""
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            For Each dtRow As DataRow In dsTables.Tables("List").Rows
                If dtRow.Item("TableName") = "hrdepartment_master" Then Continue For
                strQ = "SELECT departmentunkid FROM " & dtRow.Item("TableName").ToString & " WHERE departmentunkid = @departmentunkid "
                dsList = objDataOperation.ExecQuery(strQ, "Used")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next
            mstrMessage = ""
            Return blnIsUsed
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                            "  departmentunkid " & _
                            ", deptgroupunkid " & _
                            ", stationunkid " & _
                            ", code " & _
                            ", name " & _
                            ", description " & _
                            ", isactive " & _
                            ", name1 " & _
                            ", name2 " & _
                        "FROM hrdepartment_master " & _
                        "WHERE 1= 1 "

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strName.Length > 0 Then
                strQ &= "AND name = @name "
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If strCode.Length > 0 Then
                strQ &= "AND code = @code "
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If



            If intUnkid > 0 Then
                strQ &= " AND departmentunkid <> @departmentunkid "
                objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal mblnFlag As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If mblnFlag = True Then
                strQ = "SELECT 0 As departmentunkid , @ItemName As  name  UNION "
            End If
            strQ &= "SELECT departmentunkid,name FROM hrdepartment_master WHERE isactive =1 "

            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        End Try
    End Function

    'S.SANDEEP [ 21 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function getComboList(ByVal intDeptGrpId As Integer, Optional ByVal strListName As String = "List", Optional ByVal mblnFlag As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If mblnFlag = True Then
                strQ = "SELECT 0 As departmentunkid , @ItemName As  name "
                If intDeptGrpId > 0 Then strQ &= " UNION "
            End If

            If intDeptGrpId > 0 Then
                strQ &= "SELECT departmentunkid,name FROM hrdepartment_master " & _
                        " JOIN hralloc_mapping_tran ON hralloc_mapping_tran.c_referenceunkid =  hrdepartment_master.departmentunkid " & _
                        " JOIN hralloc_mapping_master ON hralloc_mapping_master.mappingunkid =  hralloc_mapping_tran.mappingunkid " & _
                        " WHERE isactive = 1 " & _
                        " AND hralloc_mapping_tran.isvoid = 0 AND hralloc_mapping_master.isvoid = 0 AND hralloc_mapping_master.p_referenceunkid = '" & intDeptGrpId & "' AND p_allocationid = " & enAllocation.DEPARTMENT_GROUP & _
                        " AND c_allocationid = " & enAllocation.DEPARTMENT

            End If


            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 21 SEP 2012 ] -- END


    'Pinkal (10-Mar-2011) --Start

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose> 
    Public Function GetDepartmentUnkId(ByVal mstrState As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = " SELECT " & _
                      " departmentunkid " & _
                      "  FROM hrdepartment_master " & _
                      " WHERE name = @name "

            'S.SANDEEP [19 AUG 2016] -- START
            'ISSUE : ISACTIVE WAS NOT KEPT FOR CHECKING
            strQ &= " AND isactive = 1 "
            'S.SANDEEP [19 AUG 2016] -- START

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrState)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("departmentunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetDepartmentUnkId", mstrModuleName)
        End Try
        Return -1

    End Function

    'Pinkal (10-Mar-2011) --End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Department Code is already defined. Please define new Department Code.")
			Language.setMessage(mstrModuleName, 2, "This Department Name is already defined. Please define new Department Name.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "Sorry, you cannot delete this Department. Reason : This Department is already linked with some transaction.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
