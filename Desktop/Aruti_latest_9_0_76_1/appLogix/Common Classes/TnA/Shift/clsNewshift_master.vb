﻿Imports eZeeCommonLib

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsNewshift_master

    Private Shared ReadOnly mstrModuleName As String = "clsNewshift_master"
    Dim objShiftDay As New clsshift_tran
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintShiftunkid As Integer
    Private mintShifttypeunkid As Integer
    Private mstrShiftcode As String = String.Empty
    Private mstrShiftname As String = String.Empty
    Private mintTotalWeekHrs As Integer = 0
    Private mblnIsactive As Boolean = True
    Private mstrShiftname1 As String = String.Empty
    Private mstrShiftname2 As String = String.Empty

    'Pinkal (09-Jun-2015) -- Start
    'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
    Private mblnIsOpenShift As Boolean = False
    'Pinkal (09-Jun-2015) -- End


    'Pinkal (03-AUG-2015) -- Start
    'Enhancement - IMEPLEMENTING CHANGES OF TNA FOR NAH.
    Private mintMaxHours As Integer = 0
    'Pinkal (03-AUG-2015) -- End


    'Pinkal (06-May-2016) -- Start
    'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
    Private mblnAttOnDeviceInOutStatus As Boolean = False
    'Pinkal (06-May-2016) -- End


    'Pinkal (18-Jul-2017) -- Start
    'Enhancement - TnA Enhancement for B5 Plus Company .
    Private mblnIsNormalHrsForWeekend As Boolean = False
    Private mblnIsNormalHrsForHoliday As Boolean = False
    Private mblnIsNormalHrsForDayOff As Boolean = False
    'Pinkal (18-Jul-2017) -- End

    'Pinkal (07-Sep-2017) -- Start
    'Enhancement - Working on B5 Plus Enhancement.
    Private mblnCountOnlyShiftTime As Boolean = False
    'Pinkal (07-Sep-2017) -- End


    'Pinkal (12-Oct-2017) -- Start
    'Enhancement - Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).
    Private mblnCountOTonTotalWorkedHrs As Boolean = True
    'Pinkal (12-Oct-2017) -- End


#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shiftunkid
    ''' Modify By: Pinkal
    ''' </summary>
    'Pinkal (01-Feb-2022) -- Start
    'Enhancement NMB  - Language Change Issue for All Modules.	
    Public Property _Shiftunkid(Optional ByVal xDataOp As clsDataOperation = Nothing) As Integer
        Get
            Return mintShiftunkid
        End Get
        Set(ByVal value As Integer)
            mintShiftunkid = value
            Call GetData(mintShiftunkid, xDataOp)
        End Set
    End Property
    'Pinkal (01-Feb-2022) -- End

    ''' <summary>
    ''' Purpose: Get or Set shifttypeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Shifttypeunkid() As Integer
        Get
            Return mintShifttypeunkid
        End Get
        Set(ByVal value As Integer)
            mintShifttypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shiftcode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Shiftcode() As String
        Get
            Return mstrShiftcode
        End Get
        Set(ByVal value As String)
            mstrShiftcode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shiftname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Shiftname() As String
        Get
            Return mstrShiftname
        End Get
        Set(ByVal value As String)
            mstrShiftname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set TotalWeekHrs
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _TotalWeekHrs() As Integer
        Get
            Return mintTotalWeekHrs
        End Get
        Set(ByVal value As Integer)
            mintTotalWeekHrs = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shiftname1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Shiftname1() As String
        Get
            Return mstrShiftname1
        End Get
        Set(ByVal value As String)
            mstrShiftname1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shiftname2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Shiftname2() As String
        Get
            Return mstrShiftname2
        End Get
        Set(ByVal value As String)
            mstrShiftname2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isopenshift
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsOpenShift() As Boolean
        Get
            Return mblnIsOpenShift
        End Get
        Set(ByVal value As Boolean)
            mblnIsOpenShift = value
        End Set
    End Property


    'Pinkal (03-AUG-2015) -- Start
    'Enhancement - IMEPLEMENTING CHANGES OF TNA FOR NAH.

    ''' <summary>
    ''' Purpose: Get or Set MaxHrs
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _MaxHrs() As Integer
        Get
            Return mintMaxHours
        End Get
        Set(ByVal value As Integer)
            mintMaxHours = value
        End Set
    End Property

    'Pinkal (03-AUG-2015) -- End


    'Pinkal (06-May-2016) -- Start
    'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.

    ''' <summary>
    ''' Purpose: Get or Set AttOnDeviceInOutStatus
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AttOnDeviceInOutStatus() As Boolean
        Get
            Return mblnAttOnDeviceInOutStatus
        End Get
        Set(ByVal value As Boolean)
            mblnAttOnDeviceInOutStatus = value
        End Set
    End Property

    'Pinkal (06-May-2016) -- End



    'Pinkal (18-Jul-2017) -- Start
    'Enhancement - TnA Enhancement for B5 Plus Company .

    ''' <summary>
    ''' Purpose: Get or Set IsNormalHrsForWeekend
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsNormalHrsForWeekend() As Boolean
        Get
            Return mblnIsNormalHrsForWeekend
        End Get
        Set(ByVal value As Boolean)
            mblnIsNormalHrsForWeekend = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsNormalHrsForHoliday
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsNormalHrsForHoliday() As Boolean
        Get
            Return mblnIsNormalHrsForHoliday
        End Get
        Set(ByVal value As Boolean)
            mblnIsNormalHrsForHoliday = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _IsNormalHrsForDayOff
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsNormalHrsForDayOff() As Boolean
        Get
            Return mblnIsNormalHrsForDayOff
        End Get
        Set(ByVal value As Boolean)
            mblnIsNormalHrsForDayOff = value
        End Set
    End Property

    'Pinkal (18-Jul-2017) -- End


    'Pinkal (07-Sep-2017) -- Start
    'Enhancement - Working on B5 Plus Enhancement.

    ''' <summary>
    ''' Purpose: Get or Set _CountOnlyShiftTime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _CountOnlyShiftTime() As Boolean
        Get
            Return mblnCountOnlyShiftTime
        End Get
        Set(ByVal value As Boolean)
            mblnCountOnlyShiftTime = value
        End Set
    End Property

    'Pinkal (07-Sep-2017) -- End


    'Pinkal (12-Oct-2017) -- Start
    'Enhancement - Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).

    ''' <summary>
    ''' Purpose: Get or Set _CountOTonTotalWorkedHrs
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _CountOTonTotalWorkedHrs() As Boolean
        Get
            Return mblnCountOTonTotalWorkedHrs
        End Get
        Set(ByVal value As Boolean)
            mblnCountOTonTotalWorkedHrs = value
        End Set
    End Property

    'Pinkal (12-Oct-2017) -- End



#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal intUnkId As Integer = 0, Optional ByVal xDataOp As clsDataOperation = Nothing)
        'Sohail (03 May 2018) - [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        If intUnkId > 0 Then mintShiftunkid = intUnkId
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT " & _
              "  shiftunkid " & _
              ", shifttypeunkid " & _
              ", shiftcode " & _
              ", shiftname " & _
              ", weekhours " & _
              ", isactive " & _
              ", shiftname1 " & _
              ", shiftname2 " & _
              ", ISNULL(isopenshift,0) AS isopenshift " & _
                      ", ISNULL(maxshifthrs,0) AS maxshifthrs " & _
                      ", ISNULL(attondeviceiostatus,0) AS attondeviceiostatus " & _
              ", ISNULL(isnormalhrs_forwkend,0) AS isnormalhrs_forwkend " & _
              ", ISNULL(isnormalhrs_forph,0) AS isnormalhrs_forph " & _
              ", ISNULL(isnormalhrs_fordayoff,0) AS isnormalhrs_fordayoff " & _
              ", ISNULL(iscountshifttimeonly,0) AS iscountshifttimeonly " & _
              ", ISNULL(iscnt_ot_ontotalworkhrs,0) AS iscnt_ot_ontotalworkhrs " & _
             "FROM tnashift_master " & _
             "WHERE shiftunkid = @shiftunkid "


            'Pinkal (18-Jul-2017) -- Start
            'Enhancement - TnA Enhancement for B5 Plus Company .
            '", ISNULL(isnormalhrs_forwkend,0) AS isnormalhrs_forwkend " & _
            '  ", ISNULL(isnormalhrs_forph,0) AS isnormalhrs_forph " & _
            '  ", ISNULL(isnormalhrs_fordayoff,0) AS isnormalhrs_fordayoff " & _
            'Pinkal (18-Jul-2017) -- End
 	    'Pinkal (12-Oct-2017) --Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).[", ISNULL(iscnt_ot_ontotalworkhrs,0) AS iscnt_ot_ontotalworkhrs " & _]

            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintShiftunkid = CInt(dtRow.Item("shiftunkid"))
                mintShifttypeunkid = CInt(dtRow.Item("shifttypeunkid"))
                mstrShiftcode = dtRow.Item("shiftcode").ToString
                mstrShiftname = dtRow.Item("shiftname").ToString
                If Not IsDBNull(dtRow.Item("weekhours")) Then
                mintTotalWeekHrs = CInt(dtRow.Item("weekhours").ToString())
                End If
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrShiftname1 = dtRow.Item("shiftname1").ToString
                mstrShiftname2 = dtRow.Item("shiftname2").ToString


                'Pinkal (09-Jun-2015) -- Start
                'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
                mblnIsOpenShift = CBool(dtRow.Item("isopenshift"))
                'Pinkal (09-Jun-2015) -- End

                'Pinkal (03-AUG-2015) -- Start
                'Enhancement - IMEPLEMENTING CHANGES OF TNA FOR NAH.
                If Not IsDBNull(dtRow.Item("maxshifthrs")) Then
                    mintMaxHours = CInt(dtRow.Item("maxshifthrs"))
                End If
                'Pinkal (03-AUG-2015) -- End

                'Pinkal (11-Nov-2016) -- Start
                'Enhancement - Error Solved for KBC WHEN OPEN SHIFT IS TRUE AND ATTENDANCE ON DEVICE IO STATUS IS FALSE.
                mblnAttOnDeviceInOutStatus = CBool(dtRow.Item("attondeviceiostatus"))
                'Pinkal (11-Nov-2016) -- End


                'Pinkal (18-Jul-2017) -- Start
                'Enhancement - TnA Enhancement for B5 Plus Company .
                mblnIsNormalHrsForWeekend = CBool(dtRow.Item("isnormalhrs_forwkend"))
                mblnIsNormalHrsForHoliday = CBool(dtRow.Item("isnormalhrs_forph"))
                mblnIsNormalHrsForDayOff = CBool(dtRow.Item("isnormalhrs_fordayoff"))
                'Pinkal (18-Jul-2017) -- End


                'Pinkal (07-Sep-2017) -- Start
                'Enhancement - Working on B5 Plus Enhancement.
                mblnCountOnlyShiftTime = CBool(dtRow.Item("iscountshifttimeonly"))
                'Pinkal (07-Sep-2017) -- End


                'Pinkal (12-Oct-2017) -- Start
                'Enhancement - Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).
                mblnCountOTonTotalWorkedHrs = CBool(dtRow.Item("iscnt_ot_ontotalworkhrs"))
                'Pinkal (12-Oct-2017) -- End


                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnashift_master) </purpose>
    Public Function Insert(ByVal mdtShift As DataTable) As Boolean
        If isExist(mstrShiftcode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Shift Code is already defined. Please define new Shift Code.")
            Return False
        ElseIf isExist("", mstrShiftname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Shift Name is already defined. Please define new Shift Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@shifttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShifttypeunkid.ToString)
            objDataOperation.AddParameter("@shiftcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftcode.ToString)
            objDataOperation.AddParameter("@shiftname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftname.ToString)
            objDataOperation.AddParameter("@weekhrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotalWeekHrs)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@shiftname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftname1.ToString)
            objDataOperation.AddParameter("@shiftname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftname2.ToString)

            'Pinkal (09-Jun-2015) -- Start
            'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
            objDataOperation.AddParameter("@isopenshift", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsOpenShift.ToString)
            'Pinkal (09-Jun-2015) -- End


            'Pinkal (03-AUG-2015) -- Start
            'Enhancement - IMEPLEMENTING CHANGES OF TNA FOR NAH.
            objDataOperation.AddParameter("@maxshifthrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxHours)
            'Pinkal (03-AUG-2015) -- End


            'Pinkal (06-May-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
            objDataOperation.AddParameter("@attondeviceiostatus", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnAttOnDeviceInOutStatus)
            'Pinkal (06-May-2016) -- End


            'Pinkal (18-Jul-2017) -- Start
            'Enhancement - TnA Enhancement for B5 Plus Company .
            objDataOperation.AddParameter("@isnormalhrs_forwkend", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsNormalHrsForWeekend)
            objDataOperation.AddParameter("@isnormalhrs_forph", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsNormalHrsForHoliday)
            objDataOperation.AddParameter("@isnormalhrs_fordayoff", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsNormalHrsForDayOff)
            'Pinkal (18-Jul-2017) -- End


            'Pinkal (07-Sep-2017) -- Start
            'Enhancement - Working on B5 Plus Enhancement.
            objDataOperation.AddParameter("@iscountshifttimeonly", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnCountOnlyShiftTime)
            'Pinkal (07-Sep-2017) -- End


            'Pinkal (12-Oct-2017) -- Start
            'Enhancement - Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).
            objDataOperation.AddParameter("@iscnt_ot_ontotalworkhrs", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnCountOTonTotalWorkedHrs)
            'Pinkal (12-Oct-2017) -- End


            strQ = "INSERT INTO tnashift_master ( " & _
              "  shifttypeunkid " & _
              ", shiftcode " & _
              ", shiftname " & _
              ", weekhours " & _
              ", isactive " & _
              ", shiftname1 " & _
              ", shiftname2" & _
              ", isopenshift " & _
                      ", maxshifthrs " & _
                      ", attondeviceiostatus " & _
                      ",isnormalhrs_forwkend " & _
                      ",isnormalhrs_forph " & _
                      ",isnormalhrs_fordayoff " & _
                      ",iscountshifttimeonly " & _
                      ",iscnt_ot_ontotalworkhrs " & _
            ") VALUES (" & _
              "  @shifttypeunkid " & _
              ", @shiftcode " & _
              ", @shiftname " & _
              ", @weekhrs " & _
              ", @isactive " & _
              ", @shiftname1 " & _
              ", @shiftname2" & _
              ", @isopenshift " & _
                      ", @maxshifthrs " & _
                      ", @attondeviceiostatus " & _
                      ", @isnormalhrs_forwkend " & _
                      ", @isnormalhrs_forph " & _
                      ", @isnormalhrs_fordayoff " & _
                      ", @iscountshifttimeonly " & _
                      ", @iscnt_ot_ontotalworkhrs " & _
            "); SELECT @@identity"

            'Pinkal (09-Jun-2015) -- 'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE [isopenshift].
            'Pinkal (03-AUG-2015) -- Start 'Enhancement - IMEPLEMENTING CHANGES OF TNA FOR NAH.[maxshifthrs]


            'Pinkal (06-May-2016) --  WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.[", @attondeviceiostatus " & _]


            'Pinkal (07-Sep-2017) --  Working on B5 Plus Enhancement. [iscountshifttimeonly]


            'Pinkal (12-Oct-2017) --  Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).[iscnt_ot_ontotalworkhrs]


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintShiftunkid = dsList.Tables(0).Rows(0).Item(0)

            objShiftDay._Shiftunkid = mintShiftunkid
            objShiftDay._dtShiftday = mdtShift

            If objShiftDay._dtShiftday IsNot Nothing Then

                If objShiftDay._dtShiftday.Rows.Count > 0 Then

                    If objShiftDay.InsertUpdateShiftDays(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Else

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "tnashift_master", "shiftunkid", mintShiftunkid, "tnashift_tran", "shifttranunkid", -1, 1, 0) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                End If

            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tnashift_master) </purpose>
    Public Function Update(ByVal mdtShift As DataTable) As Boolean
        If isExist(mstrShiftcode, "", mintShiftunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Shift Code is already defined. Please define new Shift Code.")
            Return False
        ElseIf isExist("", mstrShiftname, mintShiftunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Shift Name is already defined. Please define new Shift Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftunkid.ToString)
            objDataOperation.AddParameter("@shifttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShifttypeunkid.ToString)
            objDataOperation.AddParameter("@shiftcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftcode.ToString)
            objDataOperation.AddParameter("@shiftname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftname.ToString)
            objDataOperation.AddParameter("@weekhrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTotalWeekHrs)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@shiftname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftname1.ToString)
            objDataOperation.AddParameter("@shiftname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShiftname2.ToString)


            'Pinkal (09-Jun-2015) -- Start
            'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE.
            objDataOperation.AddParameter("@isopenshift", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mblnIsOpenShift.ToString)
            'Pinkal (09-Jun-2015) -- End


            'Pinkal (03-AUG-2015) -- Start
            'Enhancement - IMEPLEMENTING CHANGES OF TNA FOR NAH.
            objDataOperation.AddParameter("@maxshifthrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxHours)
            'Pinkal (03-AUG-2015) -- End

            'Pinkal (06-May-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
            objDataOperation.AddParameter("@attondeviceiostatus", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnAttOnDeviceInOutStatus)
            'Pinkal (06-May-2016) -- End

            'Pinkal (18-Jul-2017) -- Start
            'Enhancement - TnA Enhancement for B5 Plus Company .
            objDataOperation.AddParameter("@isnormalhrs_forwkend", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsNormalHrsForWeekend)
            objDataOperation.AddParameter("@isnormalhrs_forph", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsNormalHrsForHoliday)
            objDataOperation.AddParameter("@isnormalhrs_fordayoff", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsNormalHrsForDayOff)
            'Pinkal (18-Jul-2017) -- End

            'Pinkal (07-Sep-2017) -- Start
            'Enhancement - Working on B5 Plus Enhancement.
            objDataOperation.AddParameter("@iscountshifttimeonly", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnCountOnlyShiftTime)
            'Pinkal (07-Sep-2017) -- End


            'Pinkal (12-Oct-2017) -- Start
            'Enhancement - Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).
            objDataOperation.AddParameter("@iscnt_ot_ontotalworkhrs", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnCountOTonTotalWorkedHrs)
            'Pinkal (12-Oct-2017) -- End



            strQ = "UPDATE tnashift_master SET " & _
              "  shifttypeunkid = @shifttypeunkid" & _
              ", shiftcode = @shiftcode" & _
              ", shiftname = @shiftname" & _
              ", weekhours = @weekhrs " & _
              ", isactive = @isactive" & _
              ", shiftname1 = @shiftname1" & _
              ", shiftname2 = @shiftname2 " & _
              ", isopenshift = @isopenshift " & _
                      ", maxshifthrs = @maxshifthrs " & _
                      ", attondeviceiostatus  = @attondeviceiostatus " & _
                      ",isnormalhrs_forwkend = @isnormalhrs_forwkend " & _
                      ", isnormalhrs_forph = @isnormalhrs_forph " & _
                      ",isnormalhrs_fordayoff = @isnormalhrs_fordayoff " & _
                      ",iscountshifttimeonly = @iscountshifttimeonly " & _
                      ",iscnt_ot_ontotalworkhrs = @iscnt_ot_ontotalworkhrs " & _
                      " WHERE shiftunkid = @shiftunkid "

            'Pinkal (09-Jun-2015) -- 'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE [isopenshift].

            'Pinkal (03-AUG-2015) -- Start  'Enhancement - IMEPLEMENTING CHANGES OF TNA FOR NAH. [maxshifthrs]

            'Pinkal (06-May-2016) -- 'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.[", attondeviceiostatus  = @attondeviceiostatus "]

            'Pinkal (07-Sep-2017) --  Working on B5 Plus Enhancement. [",iscountshifttimeonly = @iscountshifttimeonly " & _] 


            'Pinkal (12-Oct-2017) --  'Enhancement - Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).[ ",iscnt_ot_ontotalworkhrs = @iscnt_ot_ontotalworkhrs " & _]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objShiftDay._Shiftunkid = mintShiftunkid
            objShiftDay._dtShiftday = mdtShift

            Dim dt() As DataRow = objShiftDay._dtShiftday.Select("AUD=''")
            If dt.Length = objShiftDay._dtShiftday.Rows.Count Then

                If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "tnashift_master", mintShiftunkid, "shiftunkid", 2) Then

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "tnashift_master", "shiftunkid", mintShiftunkid, "tnashift_tran", "shifttranunkid", -1, 2, 0) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                End If

            Else
                If objShiftDay.InsertUpdateShiftDays(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tnashift_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "Update tnashift_master set isactive = 0 " & _
            "WHERE shiftunkid = @shiftunkid "

            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "tnashift_master", "shiftunkid", intUnkid, "tnashift_tran", "shifttranunkid", -1, 3, -1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Pinkal (20-Sep-2013) -- Start
            'Enhancement : TRA Changes

            'strQ = "Select isnull(shiftunkid,0) from hremployee_master where shiftunkid = @shiftunkid"
            strQ = "Select isnull(shiftunkid,0) from hremployee_shift_tran where shiftunkid = @shiftunkid"

            'Pinkal (20-Sep-2013) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            strQ = "Select isnull(shiftunkid,0) from tnalogin_summary where shiftunkid = @shiftunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  shiftunkid " & _
              ", shifttypeunkid " & _
              ", shiftcode " & _
              ", shiftname " & _
              ", weekhours " & _
              ", isactive " & _
              ", shiftname1 " & _
              ", shiftname2 " & _
              ", isopenshift " & _
                      ", maxshifthrs " & _
              ", attondeviceiostatus " & _
              ",isnormalhrs_forwkend " & _
              ", isnormalhrs_forph " & _
              ", isnormalhrs_fordayoff " & _
                      ", ISNULL(iscountshifttimeonly,0) AS  iscountshifttimeonly " & _
                      ", ISNULL(iscnt_ot_ontotalworkhrs,0) AS iscnt_ot_ontotalworkhrs " & _
              " FROM tnashift_master " & _
              " WHERE 1=1 "

            'Pinkal (09-Jun-2015) -- 'Enhancement - IMPLEMENTING ENHANCEMENT FOR NEW AFRICA HOTEL (NAH) IN TNA MODULE [isopenshift].
            'Pinkal (03-AUG-2015) -- Start    'Enhancement - IMEPLEMENTING CHANGES OF TNA FOR NAH.[maxshifthrs]

            'Pinkal (06-May-2016) -- 'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.[", attondeviceiostatus " & _]

            'Pinkal (12-Oct-2017) -- Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).[", ISNULL(iscnt_ot_ontotalworkhrs,0) AS iscnt_ot_ontotalworkhrs "]

            strQ &= " AND isactive = 1 "

            If strCode.Length > 0 Then
                strQ &= " AND shiftcode = @code "
            End If
            If strName.Length > 0 Then
                strQ &= " AND shiftname = @name "
            End If

            If intUnkid > 0 Then
                strQ &= " AND shiftunkid <> @shiftunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  shiftunkid " & _
              ", shifttypeunkid " & _
              ", shiftcode " & _
              ", shiftname " & _
              ",CONVERT(DECIMAL(5, 2), FLOOR(((tnashift_master.weekhours) / 60 )/ 60) + ( CONVERT(DECIMAL(5,2), ( tnashift_master.weekhours / 60 ) % 60)/ 100 )) weekhours " & _
              ", isactive " & _
              ", shiftname1 " & _
              ", shiftname2 " & _
              ", isopenshift " & _
              ", maxshifthrs  " & _
                      ", attondeviceiostatus " & _
                      ",isnormalhrs_forwkend " & _
                      ", isnormalhrs_forph " & _
                      ", isnormalhrs_fordayoff " & _
                      ", ISNULL(iscountshifttimeonly,0) AS  iscountshifttimeonly " & _
                      ", ISNULL(iscnt_ot_ontotalworkhrs,0) AS  iscnt_ot_ontotalworkhrs " & _
                      " FROM tnashift_master "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If


            'Pinkal (03-AUG-2015) -- Start  'Enhancement - IMEPLEMENTING CHANGES OF TNA FOR NAH.[maxshifthrs]

            'Pinkal (06-May-2016) -- Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.[attondeviceiostatus " & _]

            'Pinkal (12-Oct-2017) --  Working on B5 Plus TnA Enhancement for Overtime(Extra Hrs).[", ISNULL(iscnt_ot_ontotalworkhrs,0) AS  iscnt_ot_ontotalworkhrs "]

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal xShifTypeId As Integer = 0,Optional ByVal xDataOp As clsDataOperation = Nothing) As DataSet
        'Sohail (03 May 2018) - [xDataOp]
        'Pinkal (06-Nov-2017) -- 'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.[Optional ByVal xShifTypeId As Integer = 0]

        Dim dsList As New DataSet
        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'Dim objDataOperation As New clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try


            If mblFlag = True Then
                strQ = "SELECT 0 as shiftunkid, ' ' +  @name as name, '' AS shifttype UNION "
            End If
            strQ &= "SELECT tnashift_master.shiftunkid, tnashift_master.shiftname as name, ISNULL(cfcommon_master.name,'') AS shifttype " & _
                    "FROM tnashift_master " & _
                    " JOIN cfcommon_master ON cfcommon_master.masterunkid = tnashift_master.shifttypeunkid " & _
                    "WHERE tnashift_master.isactive = 1 "

            'Pinkal (06-Nov-2017) -- Start
            'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.
            objDataOperation.ClearParameters()
            If xShifTypeId > 0 Then
                strQ &= " AND tnashift_master.shifttypeunkid = @shifttypeunkid"
                objDataOperation.AddParameter("@shifttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xShifTypeId)
            End If
            strQ &= "  ORDER BY name "
            'Pinkal (06-Nov-2017) -- End


            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetShiftWorkingHours(ByVal intShiftunkid As Integer, ByVal intDayId As Integer) As Integer
        Dim Shifthours As Double = 0
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try

            strQ = " SELECT  ISNULL(tnashift_tran.workinghrs, 0) 'workinghours' " & _
                      " FROM tnashift_tran " & _
                      " JOIN tnashift_master ON tnashift_tran.shiftunkid = tnashift_master.shiftunkid " & _
                      " WHERE tnashift_tran.shiftunkid = @shiftunkid  AND tnashift_tran.dayid = @dayId " & _
                      " AND isactive = 1"

            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intShiftunkid.ToString)
            objDataOperation.AddParameter("@dayId", SqlDbType.Int, eZeeDataType.INT_SIZE, intDayId.ToString)

            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "ShiftHours")
            If Not dsList Is Nothing And dsList.Tables(0).Rows.Count > 0 Then
                Shifthours = CInt(dsList.Tables(0).Rows(0)("workinghours"))
            End If
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dsList IsNot Nothing Then dsList.Dispose()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetShiftWorkingHours; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
        Return Shifthours
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetShiftUnkId(ByVal mstrName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = " SELECT " & _
                      " shiftunkid " & _
                      "  FROM tnashift_master " & _
                      " WHERE shiftname = @name "

            'S.SANDEEP [19 AUG 2016] -- START
            'ISSUE : ISACTIVE WAS NOT KEPT FOR CHECKING
            strQ &= " AND isactive = 1 "
            'S.SANDEEP [19 AUG 2016] -- START

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("shiftunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetShiftUnkId", mstrModuleName)
        End Try
        Return -1
    End Function


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Shift Code is already defined. Please define new Shift Code.")
			Language.setMessage(mstrModuleName, 2, "This Shift Name is already defined. Please define new Shift Name.")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class