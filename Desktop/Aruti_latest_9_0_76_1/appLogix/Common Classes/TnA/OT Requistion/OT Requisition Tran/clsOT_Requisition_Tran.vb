﻿'************************************************************************************************************************************
'Class Name : clsOT_Requisition_Tran.vb
'Purpose    :
'Date       :23-10-2018
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Web
Imports System

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsOT_Requisition_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsOT_Requisition_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Dim objOTApproval As New clsTnaotrequisition_approval_Tran

#Region " Private variables "

    Private mintOtrequisitiontranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mdtTransactiondate As Date
    Private mdtRequestdate As Date
    Private mintOttypeunkid As Integer
    Private mintPeriodunkid As Integer
    Private mdtPlannedstart_Time As Date
    Private mdtPlannedend_Time As Date
    Private mintPlannedot_Hours As Integer
    Private mstrRequest_Reason As String = String.Empty
    Private mdtActualstart_Time As Date
    Private mdtActualend_Time As Date
    Private mintActualot_Hours As Integer
    Private mstrAdjustment_Reason As String = String.Empty
    Private mintStatusunkid As Integer
    Private mintFinalmappedapproverunkid As Integer
    Private mblnIssubmit_Approval As Boolean
    Private mintUserunkid As Integer
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Private mstrWebFormName As String = String.Empty
    Private mintLoginemployeeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mdtVoiddatetime As Date
    Private mblnIsFromWeb As Boolean = False
    Private mintVoidLoginemployeeunkid As Integer

    'Pinkal (24-Oct-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.
    Private mintplannedNight_Hours As Integer
    Private mintactuaNight_Hours As Integer
    'Pinkal (24-Oct-2019) -- End


    'Pinkal (23-Nov-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.
    Private mintShiftId As Integer = 0
    Private mintDayId As Integer = -1
    Private mblnIsweekend As Boolean = False
    Private mblnIsholiday As Boolean = False
    Private mblnIsDayOff As Boolean = False
    'Pinkal (23-Nov-2019) -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set otrequisitiontranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Otrequisitiontranunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintOtrequisitiontranunkid
        End Get
        Set(ByVal value As Integer)
            mintOtrequisitiontranunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set requestdate
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Requestdate() As Date
        Get
            Return mdtRequestdate
        End Get
        Set(ByVal value As Date)
            mdtRequestdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ottypeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Ottypeunkid() As Integer
        Get
            Return mintOttypeunkid
        End Get
        Set(ByVal value As Integer)
            mintOttypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set plannedstart_time
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Plannedstart_Time() As Date
        Get
            Return mdtPlannedstart_Time
        End Get
        Set(ByVal value As Date)
            mdtPlannedstart_Time = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set plannedend_time
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Plannedend_Time() As Date
        Get
            Return mdtPlannedend_Time
        End Get
        Set(ByVal value As Date)
            mdtPlannedend_Time = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set plannedot_hours
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Plannedot_Hours() As Integer
        Get
            Return mintPlannedot_Hours
        End Get
        Set(ByVal value As Integer)
            mintPlannedot_Hours = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set request_reason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Request_Reason() As String
        Get
            Return mstrRequest_Reason
        End Get
        Set(ByVal value As String)
            mstrRequest_Reason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actualstart_time
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Actualstart_Time() As Date
        Get
            Return mdtActualstart_Time
        End Get
        Set(ByVal value As Date)
            mdtActualstart_Time = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actualend_time
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Actualend_Time() As Date
        Get
            Return mdtActualend_Time
        End Get
        Set(ByVal value As Date)
            mdtActualend_Time = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actualot_hours
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Actualot_Hours() As Integer
        Get
            Return mintActualot_Hours
        End Get
        Set(ByVal value As Integer)
            mintActualot_Hours = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set adjustment_reason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Adjustment_Reason() As String
        Get
            Return mstrAdjustment_Reason
        End Get
        Set(ByVal value As String)
            mstrAdjustment_Reason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set finalmappedapproverunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Finalmappedapproverunkid() As Integer
        Get
            Return mintFinalmappedapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintFinalmappedapproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set issubmit_approval
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Issubmit_Approval() As Boolean
        Get
            Return mblnIssubmit_Approval
        End Get
        Set(ByVal value As Boolean)
            mblnIssubmit_Approval = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set VoidLoginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _VoidLoginemployeeunkid() As Integer
        Get
            Return mintVoidLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidLoginemployeeunkid = value
        End Set
    End Property



    'Pinkal (24-Oct-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.

    ''' <summary>
    ''' Purpose: Get or Set PlannedNight_Hours
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _PlannedNight_Hours() As Integer
        Get
            Return mintplannedNight_Hours
        End Get
        Set(ByVal value As Integer)
            mintplannedNight_Hours = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ActualNight_Hours
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ActualNight_Hours() As Integer
        Get
            Return mintactuaNight_Hours
        End Get
        Set(ByVal value As Integer)
            mintactuaNight_Hours = value
        End Set
    End Property
    'Pinkal (24-Oct-2019) -- End


    'Pinkal (23-Nov-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.

    ''' <summary>
    ''' Purpose: Get or Set ShiftId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ShiftId() As Integer
        Get
            Return mintShiftId
        End Get
        Set(ByVal value As Integer)
            mintShiftId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DayId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DayId() As Integer
        Get
            Return mintDayId
        End Get
        Set(ByVal value As Integer)
            mintDayId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Isweekend
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isweekend() As Boolean
        Get
            Return mblnIsweekend
        End Get
        Set(ByVal value As Boolean)
            mblnIsweekend = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Isholiday
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isholiday() As Boolean
        Get
            Return mblnIsholiday
        End Get
        Set(ByVal value As Boolean)
            mblnIsholiday = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsDayOff
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsDayOff() As Boolean
        Get
            Return mblnIsDayOff
        End Get
        Set(ByVal value As Boolean)
            mblnIsDayOff = value
        End Set
    End Property

    'Pinkal (23-Nov-2019) -- End


#End Region


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  otrequisitiontranunkid " & _
              ", employeeunkid " & _
              ", transactiondate " & _
              ", requestdate " & _
              ", ottypeunkid " & _
              ", periodunkid " & _
              ", plannedstart_time " & _
              ", plannedend_time " & _
              ", plannedot_hours " & _
              ", request_reason " & _
              ", actualstart_time " & _
              ", actualend_time " & _
              ", actualot_hours " & _
              ", adjustment_reason " & _
              ", statusunkid " & _
              ", finalmappedapproverunkid " & _
              ", issubmit_approval " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
              ", ISNULL(voidloginemployeeunkid,0) AS voidloginemployeeunkid " & _
              ", ISNULL(plannednight_hrs,0) AS plannednight_hrs " & _
              ", ISNULL(actualnight_hrs,0) AS actualnight_hrs " & _
                      ", ISNULL(shiftunkid,0) AS shiftunkid  " & _
                      ", ISNULL(dayid,DATEPART(dw,requestdate) - 1) AS dayid  " & _
                      ", ISNULL(isweekend,0) AS isweekend  " & _
                      ", ISNULL(isholiday,0) AS isholiday  " & _
                      ", ISNULL(isdayoff,0) AS isdayoff  " & _
              " FROM tnaot_requisition_tran " & _
              " WHERE isvoid = 0 and otrequisitiontranunkid = @otrequisitiontranunkid "

            'Pinkal (23-Nov-2019) -- Enhancement NMB - Working On OT Enhancement for NMB.[", ISNULL(shiftunkid,0) AS shiftunkid,ISNULL(dayid,DATEPART(dw,requestdate) - 1) AS dayid ", ISNULL(isweekend,0) AS isweekend ", ISNULL(isholiday,0) AS isholiday ", ISNULL(isdayoff,0) AS isdayoff  " & _]

            'Pinkal (24-Oct-2019) -- 'Enhancement NMB - Working On OT Enhancement for NMB.[", ISNULL(tnaot_requisition_tran.plannednight_hrs,0) AS plannednight_hrs ", ISNULL(tnaot_requisition_tran.actualnight_hrs,0) AS actualnight_hrs " & _]

            objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequisitiontranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintOtrequisitiontranunkid = CInt(dtRow.Item("otrequisitiontranunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                If IsDBNull(dtRow.Item("transactiondate")) = False Then
                    mdtTransactiondate = CDate(dtRow.Item("transactiondate"))
                Else
                    mdtTransactiondate = Nothing
                End If
                If IsDBNull(dtRow.Item("requestdate")) = False Then
                    mdtRequestdate = CDate(dtRow.Item("requestdate"))
                Else
                    mdtRequestdate = Nothing
                End If
                mintOttypeunkid = CInt(dtRow.Item("ottypeunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                If IsDBNull(dtRow.Item("plannedstart_time")) = False Then
                    mdtPlannedstart_Time = CDate(dtRow.Item("plannedstart_time"))
                Else
                    mdtPlannedstart_Time = Nothing
                End If
                If IsDBNull(dtRow.Item("plannedend_time")) = False Then
                    mdtPlannedend_Time = CDate(dtRow.Item("plannedend_time"))
                Else
                    mdtPlannedend_Time = Nothing
                End If
                mintPlannedot_Hours = CInt(dtRow.Item("plannedot_hours"))
                mstrRequest_Reason = dtRow.Item("request_reason").ToString
                If IsDBNull(dtRow.Item("actualstart_time")) = False Then
                    mdtActualstart_Time = CDate(dtRow.Item("actualstart_time"))
                Else
                    mdtActualstart_Time = Nothing
                End If
                If IsDBNull(dtRow.Item("actualend_time")) = False Then
                    mdtActualend_Time = CDate(dtRow.Item("actualend_time"))
                Else
                    mdtActualend_Time = Nothing
                End If
                mintActualot_Hours = CInt(dtRow.Item("actualot_hours"))
                mstrAdjustment_Reason = dtRow.Item("adjustment_reason").ToString
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintFinalmappedapproverunkid = CInt(dtRow.Item("finalmappedapproverunkid"))
                mblnIssubmit_Approval = CBool(dtRow.Item("issubmit_approval"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                Else
                    mdtVoiddatetime = Nothing
                End If
                mintVoidLoginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))

                'Pinkal (24-Oct-2019) -- Start
                'Enhancement NMB - Working On OT Enhancement for NMB.
                mintplannedNight_Hours = CInt(dtRow.Item("plannednight_hrs"))
                mintactuaNight_Hours = CInt(dtRow.Item("actualnight_hrs"))
                'Pinkal (24-Oct-2019) -- End

                'Pinkal (23-Nov-2019) -- Start
                'Enhancement NMB - Working On OT Enhancement for NMB.
                mintShiftId = CInt(dtRow.Item("shiftunkid"))
                mintDayId = CInt(dtRow.Item("dayid"))
                mblnIsweekend = CBool(dtRow.Item("isweekend"))
                mblnIsholiday = CBool(dtRow.Item("isholiday"))
                mblnIsDayOff = CBool(dtRow.Item("isdayoff"))
                'Pinkal (23-Nov-2019) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                            , ByVal strDatabaseName As String _
                            , ByVal intUserUnkid As Integer _
                            , ByVal intYearUnkId As Integer _
                            , ByVal intCompanyUnkid As Integer _
                            , ByVal blnIncludeIn_ActiveEmployee As Boolean _
                            , ByVal dtPeriodStart As Date _
                            , ByVal dtPeriodEnd As Date _
                            , ByVal strUserModeSetting As String _
                            , ByVal blnOnlyApproved As Boolean _
                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                            , Optional ByVal strAdvanceFilter As String = "" _
                            , Optional ByVal objDataOp As clsDataOperation = Nothing _
                            , Optional ByVal intEmployeeUnkId As Integer = -1 _
                            , Optional ByVal strEmpIDs As String = "" _
                            , Optional ByVal dtRequisitionFromDate As Date = Nothing _
                            , Optional ByVal dtRequisitionToDate As Date = Nothing _
                            , Optional ByVal intStatusID As Integer = 0 _
                            , Optional ByVal intPeriodunkid As Integer = 0 _
                            , Optional ByVal mblnAddSelect As Boolean = False _
                            , Optional ByVal intSubmitForApproval As Integer = -1 _
                            , Optional ByVal mblnActiveOnly As Boolean = True _
                            , Optional ByVal mblnFromCancelOT As Boolean = False) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)

            'Pinkal (07-Nov-2019) -- Start
            'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
            If blnApplyUserAccessFilter Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkId, strUserModeSetting)
            'Pinkal (07-Nov-2019) -- End


            'Pinkal (27-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            'Pinkal (27-Aug-2020) -- End



            If objDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDataOp
            End If
            objDataOperation.ClearParameters()

            strQ = "SELECT   hremployee_master.employeeunkid " & _
                           ", hremployee_master.employeecode " & _
                           ", hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname AS employeename " & _
                           ", hremployee_master.employeecode + ' - ' + hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname AS empcodename " & _
                    "INTO #tblEmp " & _
                    "FROM " & strDatabaseName & "..hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If


            'Pinkal (27-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    strQ &= xAdvanceJoinQry
            'End If
            'Pinkal (27-Aug-2020) -- End

            strQ &= "WHERE 1 = 1 "

            If blnIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If strAdvanceFilter.Length > 0 Then
                strQ &= "AND " & strAdvanceFilter
            End If

            'Pinkal (27-Feb-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            If intEmployeeUnkId <> -1 Then
                strQ &= " AND " & strDatabaseName & "..hremployee_master.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            If strEmpIDs <> Nothing AndAlso strEmpIDs.Trim <> "" Then
                strQ &= " AND " & strDatabaseName & "..hremployee_master.employeeunkid IN (" & strEmpIDs & ") "
            End If
            'Pinkal (27-Feb-2020) -- End


            If mblnAddSelect = True Then

                strQ &= "SELECT DISTINCT " & _
                       "  0 AS otrequisitiontranunkid " & _
                       ", #tblEmp.employeeunkid " & _
                       ", #tblEmp.employeecode " & _
                       ", #tblEmp.employeename " & _
                       ", #tblEmp.empcodename " & _
                       ", Null AS transactiondate " & _
                       ", NULL AS requestdate " & _
                       ", '' AS  RDate " & _
                       ", 0 AS ottypeunkid  " & _
                       ", 0  AS periodunkid " & _
                       ", NULL  AS plannedstart_time " & _
                       ", NULL AS plannedend_time " & _
                       ", 0  AS plannedot_hours  " & _
                       ", '' AS request_reason " & _
                       ", NULL  AS actualstart_time " & _
                       ", NULL  AS actualend_time " & _
                       ", 0  AS actualot_hours " & _
                       ", ''  AS adjustment_reason " & _
                       ", 0  AS statusunkid " & _
                       ", '' AS Status" & _
                       ", 0 AS finalmappedapproverunkid " & _
                       ", 0 AS issubmit_approval " & _
                       ", 0 AS userunkid " & _
                       ", 0 AS loginemployeeunkid " & _
                       ", 0 AS isvoid " & _
                       ", 0 AS voiduserunkid  " & _
                       ", '' AS voidreason  " & _
                       ", NULL AS voiddatetime  " & _
                       ", 0 AS plannednight_hrs  " & _
                       ", 0 AS actualnight_hrs  " & _
                       ", 0 AS shiftunkid  " & _
                       ", 0 AS dayid  " & _
                       ", CAST(0 AS BIT) AS isweekend  " & _
                       ", CAST(0 AS BIT) AS isholiday  " & _
                       ", CAST(0 AS BIT) AS isdayoff  "

                If mblnFromCancelOT Then
                    strQ &= ", 0 AS otrequisitionprocesstranunkid  " & _
                                  ", CAST(0 AS BIT) AS isposted  " & _
                                  ", CAST(0 AS BIT) AS isprocessed  "
                End If


                'Pinkal (12-Oct-2021)-- Start
                'NMB OT Requisition Performance Issue.

                strQ &= ", CAST(1 AS BIT) AS IsGrp " & _
                            "  FROM tnaot_requisition_tran " & _
                            " JOIN #tblEmp ON tnaot_requisition_tran.employeeunkid = #tblEmp.employeeunkid "


                If mblnFromCancelOT Then
                    strQ &= " JOIN tnaotrequisition_process_tran ON tnaotrequisition_process_tran.otrequisitiontranunkid = tnaot_requisition_tran.otrequisitiontranunkid AND tnaotrequisition_process_tran.isvoid = 0 " & _
                                " AND tnaot_requisition_tran.employeeunkid = tnaotrequisition_process_tran.employeeunkid "
                End If

                strQ &= " WHERE  1= 1"

                If mblnActiveOnly Then
                    strQ &= " AND tnaot_requisition_tran.isvoid = 0 "
                End If

                If intEmployeeUnkId <> -1 Then
                    strQ &= " AND tnaot_requisition_tran.employeeunkid = @employeeunkid "
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
                End If

                If strEmpIDs <> Nothing AndAlso strEmpIDs.Trim <> "" Then
                    strQ &= " AND tnaot_requisition_tran.employeeunkid IN (" & strEmpIDs & ") "
                End If

                If dtRequisitionFromDate <> Nothing Then
                    strQ &= " AND CONVERT(CHAR(8), tnaot_requisition_tran.requestdate, 112) >= @fromrequestdate "
                    objDataOperation.AddParameter("@fromrequestdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtRequisitionFromDate))
                End If

                If dtRequisitionToDate <> Nothing Then
                    strQ &= " AND CONVERT(CHAR(8), tnaot_requisition_tran.requestdate, 112) <= @torequestdate "
                    objDataOperation.AddParameter("@torequestdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtRequisitionToDate))
                End If

                If intStatusID <> Nothing AndAlso intStatusID > 0 Then
                    strQ &= " AND tnaot_requisition_tran.statusunkid = @statusunkid "
                    objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
                End If

                If intPeriodunkid <> Nothing AndAlso intPeriodunkid > 0 Then
                    strQ &= " AND tnaot_requisition_tran.periodunkid = @periodunkid "
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodunkid)
                End If

                If intSubmitForApproval <> -1 Then
                    strQ &= " AND tnaot_requisition_tran.issubmit_approval = @issubmit_approval "
                    objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, intSubmitForApproval)
                End If

                If mblnFromCancelOT Then
                    strQ &= " AND tnaotrequisition_process_tran.isposted = 0 AND tnaotrequisition_process_tran.isprocessed = 0 "
                End If

                'Pinkal (12-Oct-2021) -- End


                strQ &= " UNION ALL "

                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "  Select"))
            End If

                strQ &= "SELECT " & _
                              "  tnaot_requisition_tran.otrequisitiontranunkid " & _
                              ", tnaot_requisition_tran.employeeunkid " & _
                              ", #tblEmp.employeecode " & _
                              ", #tblEmp.employeename " & _
                              ", #tblEmp.empcodename " & _
                              ", tnaot_requisition_tran.transactiondate " & _
                              ", tnaot_requisition_tran.requestdate " & _
                              ", CONVERT(Char(8),tnaot_requisition_tran.requestdate,112) AS RDate " & _
                              ", tnaot_requisition_tran.ottypeunkid " & _
                              ", tnaot_requisition_tran.periodunkid " & _
                              ", tnaot_requisition_tran.plannedstart_time " & _
                              ", tnaot_requisition_tran.plannedend_time " & _
                              ", tnaot_requisition_tran.plannedot_hours " & _
                              ", tnaot_requisition_tran.request_reason " & _
                              ", tnaot_requisition_tran.actualstart_time " & _
                              ", tnaot_requisition_tran.actualend_time " & _
                              ", tnaot_requisition_tran.actualot_hours " & _
                              ", tnaot_requisition_tran.adjustment_reason " & _
                              ", tnaot_requisition_tran.statusunkid " & _
                              ", CASE WHEN tnaot_requisition_tran.statusunkid = 1 then @Approve " & _
                              "          when tnaot_requisition_tran.statusunkid = 2 then @Pending  " & _
                              "          when tnaot_requisition_tran.statusunkid = 3 then @Reject " & _
                              "          when tnaot_requisition_tran.statusunkid = 6 then @Cancel " & _
                              "          when tnaot_requisition_tran.statusunkid = 7 then @Issued END as status" & _
                              ", tnaot_requisition_tran.finalmappedapproverunkid " & _
                              ", tnaot_requisition_tran.issubmit_approval " & _
                              ", tnaot_requisition_tran.userunkid " & _
                              ", tnaot_requisition_tran.loginemployeeunkid " & _
                              ", tnaot_requisition_tran.isvoid " & _
                              ", tnaot_requisition_tran.voiduserunkid " & _
                              ", tnaot_requisition_tran.voidreason " & _
                              ", tnaot_requisition_tran.voiddatetime " & _
                              ", ISNULL(tnaot_requisition_tran.plannednight_hrs,0) AS plannednight_hrs " & _
                              ", ISNULL(tnaot_requisition_tran.actualnight_hrs,0) AS actualnight_hrs " & _
                              ", ISNULL(tnaot_requisition_tran.shiftunkid,0) AS shiftunkid  " & _
                              ", ISNULL(tnaot_requisition_tran.dayid,DATEPART(dw,tnaot_requisition_tran.requestdate) - 1) AS dayid  " & _
                              ", ISNULL(tnaot_requisition_tran.isweekend,0) AS isweekend  " & _
                              ", ISNULL(tnaot_requisition_tran.isholiday,0) AS isholiday  " & _
                              ", ISNULL(tnaot_requisition_tran.isdayoff,0) AS isdayoff  "


                If mblnFromCancelOT Then
                    strQ &= ", ISNULL(tnaotrequisition_process_tran.otrequisitionprocesstranunkid,0) AS otrequisitionprocesstranunkid  " & _
                                 ", ISNULL(tnaotrequisition_process_tran.isposted,0) AS isposted  " & _
                                 ", ISNULL(tnaotrequisition_process_tran.isprocessed,0) AS isprocessed  "
                End If

            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.

            strQ &= ", CAST(0 AS BIT) AS IsGrp " & _
                        " FROM tnaot_requisition_tran " & _
                            " JOIN #tblEmp ON tnaot_requisition_tran.employeeunkid = #tblEmp.employeeunkid "

            'Pinkal (12-Oct-2021)-- End

            If mblnFromCancelOT Then
                strQ &= " JOIN tnaotrequisition_process_tran ON tnaotrequisition_process_tran.otrequisitiontranunkid = tnaot_requisition_tran.otrequisitiontranunkid AND tnaotrequisition_process_tran.isvoid = 0 " & _
                            " AND tnaot_requisition_tran.employeeunkid = tnaotrequisition_process_tran.employeeunkid "
            End If

            strQ &= " WHERE  1= 1"

            If mblnActiveOnly Then
                strQ &= " AND tnaot_requisition_tran.isvoid = 0 "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 519, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 522, "Cancel"))
            objDataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 276, "Issued"))


            'Pinkal (27-Feb-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            'If intEmployeeUnkId <> Nothing AndAlso intEmployeeUnkId > 0 Then
            If intEmployeeUnkId <> -1 Then
                'Pinkal (27-Feb-2020) -- End
                strQ &= " AND tnaot_requisition_tran.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            End If

            If strEmpIDs <> Nothing AndAlso strEmpIDs.Trim <> "" Then
                strQ &= " AND tnaot_requisition_tran.employeeunkid IN (" & strEmpIDs & ") "
            End If

            If dtRequisitionFromDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8), tnaot_requisition_tran.requestdate, 112) >= @fromrequestdate "
                objDataOperation.AddParameter("@fromrequestdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtRequisitionFromDate))
            End If

            If dtRequisitionToDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8), tnaot_requisition_tran.requestdate, 112) <= @torequestdate "
                objDataOperation.AddParameter("@torequestdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtRequisitionToDate))
            End If

            If intStatusID <> Nothing AndAlso intStatusID > 0 Then
                strQ &= " AND tnaot_requisition_tran.statusunkid = @statusunkid "
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
            End If

            If intPeriodunkid <> Nothing AndAlso intPeriodunkid > 0 Then
                strQ &= " AND tnaot_requisition_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodunkid)
            End If

            If intSubmitForApproval <> -1 Then
                strQ &= " AND tnaot_requisition_tran.issubmit_approval = @issubmit_approval "
                objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, intSubmitForApproval)
            End If

            If mblnFromCancelOT Then
                strQ &= " AND tnaotrequisition_process_tran.isposted = 0 AND tnaotrequisition_process_tran.isprocessed = 0 "
            End If


            strQ &= " DROP TABLE #tblEmp "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnaot_requisition_tran) </purpose>
    Public Function Insert(ByVal xDatabaseName As String, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                   , ByVal mblnIncludeCapApprover As Boolean) As Boolean

        'Pinkal (23-Nov-2019) -- Enhancement NMB - Working On OT Enhancement for NMB.[ByVal mblnIncludeCapApprover As Boolean]

        If isExist(mdtRequestdate, mintOtrequisitiontranunkid, mintEmployeeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "OT Requisition already exists for requested date.Please define new OT Requisition for other date.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()


        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtTransactiondate <> Nothing, mdtTransactiondate, DBNull.Value))
            objDataOperation.AddParameter("@requestdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtRequestdate <> Nothing, mdtRequestdate, DBNull.Value))
            objDataOperation.AddParameter("@ottypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOttypeunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@plannedstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtPlannedstart_Time <> Nothing, mdtPlannedstart_Time, DBNull.Value))
            objDataOperation.AddParameter("@plannedend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtPlannedend_Time <> Nothing, mdtPlannedend_Time, DBNull.Value))
            objDataOperation.AddParameter("@plannedot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPlannedot_Hours.ToString)
            objDataOperation.AddParameter("@request_reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRequest_Reason.ToString)
            objDataOperation.AddParameter("@actualstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtActualstart_Time <> Nothing, mdtActualstart_Time, DBNull.Value))
            objDataOperation.AddParameter("@actualend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtActualend_Time <> Nothing, mdtActualend_Time, DBNull.Value))
            objDataOperation.AddParameter("@actualot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActualot_Hours.ToString)
            objDataOperation.AddParameter("@adjustment_reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAdjustment_Reason.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@finalmappedapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalmappedapproverunkid.ToString)
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmit_Approval.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))

            'Pinkal (24-Oct-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            objDataOperation.AddParameter("@plannednight_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintplannedNight_Hours.ToString)
            objDataOperation.AddParameter("@actualnight_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintactuaNight_Hours.ToString)
            'Pinkal (24-Oct-2019) -- End

            'Pinkal (23-Nov-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftId.ToString)
            objDataOperation.AddParameter("@isweekend", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweekend.ToString)
            objDataOperation.AddParameter("@isholiday", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsholiday.ToString)
            objDataOperation.AddParameter("@isdayoff", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDayOff.ToString)
            'Pinkal (23-Nov-2019) -- End



            strQ = "INSERT INTO tnaot_requisition_tran ( " & _
                      "  employeeunkid " & _
                      ", transactiondate " & _
                      ", requestdate " & _
                      ", ottypeunkid " & _
                      ", periodunkid " & _
                      ", plannedstart_time " & _
                      ", plannedend_time " & _
                      ", plannedot_hours " & _
                      ", request_reason " & _
                      ", actualstart_time " & _
                      ", actualend_time " & _
                      ", actualot_hours " & _
                      ", adjustment_reason " & _
                      ", plannednight_hrs " & _
                      ", actualnight_hrs " & _
                      ", statusunkid " & _
                      ", finalmappedapproverunkid " & _
                      ", issubmit_approval " & _
                      ", shiftunkid " & _
                      ", dayid " & _
                      ", isweekend " & _
                      ", isholiday " & _
                      ", isdayoff " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", voiddatetime" & _
                    ") VALUES (" & _
                      "  @employeeunkid " & _
                      ", @transactiondate " & _
                      ", @requestdate " & _
                      ", @ottypeunkid " & _
                      ", @periodunkid " & _
                      ", @plannedstart_time " & _
                      ", @plannedend_time " & _
                      ", @plannedot_hours " & _
                      ", @request_reason " & _
                      ", @actualstart_time " & _
                      ", @actualend_time " & _
                      ", @actualot_hours " & _
                      ", @adjustment_reason " & _
                      ", @plannednight_hrs " & _
                      ", @actualnight_hrs " & _
                      ", @statusunkid " & _
                      ", @finalmappedapproverunkid " & _
                      ", @issubmit_approval " & _
                      ", @shiftunkid " & _
                      ", (DATEPART(WEEKDAY,@requestdate)) -1 " & _
                      ", @isweekend " & _
                      ", @isholiday " & _
                      ", @isdayoff " & _
                      ", @userunkid " & _
                      ", @loginemployeeunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voidreason " & _
                      ", @voiddatetime" & _
                    "); SELECT @@identity"


            'Pinkal (23-Nov-2019) --  'Enhancement NMB - Working On OT Enhancement for NMB.[", @shiftunkid,(DATEPART(WEEKDAY,@requestdate)) -1, @isweekend , @isholiday , @isdayoff " ]

            'Pinkal (24-Oct-2019) -- 'Enhancement NMB - Working On OT Enhancement for NMB.[@plannednight_hrs,@actualnight_hrs ]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintOtrequisitiontranunkid = dsList.Tables(0).Rows(0).Item(0)

            If Insert_AtTranLog(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If mblnIssubmit_Approval Then

                Dim objApproverMst As New clsTnaapprover_master

                'Pinkal (23-Nov-2019) -- Start
                'Enhancement NMB - Working On OT Enhancement for NMB.
                'Dim dtApproverList As DataTable = objApproverMst.GetEmployeeApprover(xDatabaseName, xYearUnkid, xCompanyUnkid, mdtEmployeeAsonDate, False, -1, mintEmployeeunkid, -1, objDataOperation, False, "" )
                Dim dtApproverList As DataTable = objApproverMst.GetEmployeeApprover(xDatabaseName, xYearUnkid, xCompanyUnkid, mdtEmployeeAsonDate, False, -1, mintEmployeeunkid, -1, objDataOperation, False, "", mblnIncludeCapApprover)
                'Pinkal (23-Nov-2019) -- End


                objApproverMst = Nothing

                If dtApproverList IsNot Nothing AndAlso dtApproverList.Rows.Count > 0 Then

                    objOTApproval._Otrequisitiontranunkid = mintOtrequisitiontranunkid
                    objOTApproval._Employeeunkid = mintEmployeeunkid
                    objOTApproval._Approvaldate = Nothing
                    objOTApproval._Ottypeunkid = mintOttypeunkid
                    objOTApproval._Periodunkid = mintPeriodunkid
                    objOTApproval._Actualstart_Time = mdtPlannedstart_Time
                    objOTApproval._Actualend_Time = mdtPlannedend_Time
                    objOTApproval._Actualot_Hours = mintPlannedot_Hours
                    objOTApproval._Adjustment_Reason = mstrAdjustment_Reason
                    objOTApproval._Statusunkid = 2
                    objOTApproval._Userunkid = mintUserunkid
                    objOTApproval._Isvoid = False
                    objOTApproval._Voiduserunkid = -1
                    objOTApproval._Voiddatetime = Nothing
                    objOTApproval._Voidreason = ""
                    objOTApproval._Iscancel = False
                    objOTApproval._Canceluserunkid = -1
                    objOTApproval._Canceldatetime = Nothing
                    objOTApproval._Cancelreason = ""
                    objOTApproval._Form_Name = mstrWebFormName
                    objOTApproval._ClientIp = mstrWebClientIP
                    objOTApproval._Hostname = mstrWebHostName
                    objOTApproval._Audituserunkid = mintUserunkid
                    objOTApproval._Isweb = mblnIsFromWeb

                    'Pinkal (24-Oct-2019) -- Start
                    'Enhancement NMB - Working On OT Enhancement for NMB.
                    objOTApproval._ActualNight_Hours = mintplannedNight_Hours
                    'Pinkal (24-Oct-2019) -- End

                    Dim mintMinPriority As Integer = 0
                    mintMinPriority = dtApproverList.AsEnumerable().Select(Function(x) x.Field(Of Integer)("tnapriority")).Min()

                    For Each dr As DataRow In dtApproverList.Rows

                        objOTApproval._Tnamappingunkid = CInt(dr("tnamappingunkid"))
                        objOTApproval._Tnalevelunkid = CInt(dr("tnalevelunkid"))
                        objOTApproval._Mapuserunkid = CInt(dr("mapuserunkid"))
                        objOTApproval._Tnapriority = CInt(dr("tnapriority"))

                        If mintMinPriority = CInt(dr("tnapriority")) Then
                            objOTApproval._Visibleunkid = 2
                        Else
                            objOTApproval._Visibleunkid = -1
                        End If

                        If objOTApproval.Insert(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Next

                End If   ' If dtApproverList IsNot Nothing AndAlso dtApproverList.Rows.Count > 0 Then

            End If '  If mblnIssubmit_Approval Then

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tnaot_requisition_tran) </purpose>
    Public Function Update(ByVal xDatabaseName As String, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                   , ByVal mdtEmployeeAsonDate As Date, ByVal mblnIncludeCapApprover As Boolean, Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean

        'Pinkal (23-Nov-2019) -- Enhancement NMB - Working On OT Enhancement for NMB.[ByVal mblnIncludeCapApprover As Boolean]


        If isExist(mdtRequestdate, mintOtrequisitiontranunkid, mintEmployeeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "OT Requisition already exists for requested date.Please define new OT Requisition for other date.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception



        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequisitiontranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtTransactiondate <> Nothing, mdtTransactiondate, DBNull.Value))
            objDataOperation.AddParameter("@requestdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtRequestdate <> Nothing, mdtRequestdate, DBNull.Value))
            objDataOperation.AddParameter("@ottypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOttypeunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@plannedstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtPlannedstart_Time <> Nothing, mdtPlannedstart_Time, DBNull.Value))
            objDataOperation.AddParameter("@plannedend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtPlannedend_Time <> Nothing, mdtPlannedend_Time, DBNull.Value))
            objDataOperation.AddParameter("@plannedot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPlannedot_Hours.ToString)
            objDataOperation.AddParameter("@request_reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRequest_Reason.ToString)
            objDataOperation.AddParameter("@actualstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtActualstart_Time <> Nothing, mdtActualstart_Time, DBNull.Value))
            objDataOperation.AddParameter("@actualend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtActualend_Time <> Nothing, mdtActualend_Time, DBNull.Value))
            objDataOperation.AddParameter("@actualot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActualot_Hours.ToString)
            objDataOperation.AddParameter("@adjustment_reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAdjustment_Reason.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@finalmappedapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalmappedapproverunkid.ToString)
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmit_Approval.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))

            'Pinkal (24-Oct-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            objDataOperation.AddParameter("@plannednight_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintplannedNight_Hours.ToString)
            objDataOperation.AddParameter("@actualnight_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintactuaNight_Hours.ToString)
            'Pinkal (24-Oct-2019) -- End


            'Pinkal (23-Nov-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftId.ToString)
            objDataOperation.AddParameter("@isweekend", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweekend.ToString)
            objDataOperation.AddParameter("@isholiday", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsholiday.ToString)
            objDataOperation.AddParameter("@isdayoff", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDayOff.ToString)
            'Pinkal (23-Nov-2019) -- End

            strQ = "UPDATE tnaot_requisition_tran SET " & _
                      "  employeeunkid = @employeeunkid" & _
                      ", transactiondate = @transactiondate" & _
                      ", requestdate = @requestdate" & _
                      ", ottypeunkid = @ottypeunkid" & _
                      ", periodunkid = @periodunkid" & _
                      ", plannedstart_time = @plannedstart_time" & _
                      ", plannedend_time = @plannedend_time" & _
                      ", plannedot_hours = @plannedot_hours" & _
                      ", request_reason = @request_reason" & _
                      ", actualstart_time = @actualstart_time" & _
                      ", actualend_time = @actualend_time" & _
                      ", actualot_hours = @actualot_hours" & _
                      ", adjustment_reason = @adjustment_reason" & _
                      ", plannednight_hrs = @plannednight_hrs " & _
                      ", actualnight_hrs = @actualnight_hrs " & _
                      ", statusunkid = @statusunkid" & _
                      ", finalmappedapproverunkid = @finalmappedapproverunkid" & _
                      ", issubmit_approval = @issubmit_approval" & _
                      ", shiftunkid = @shiftunkid " & _
                      ", dayid = (DATEPART(WEEKDAY,@requestdate)) -1 " & _
                      ", isweekend = @isweekend " & _
                      ", isholiday = @isholiday " & _
                      ", isdayoff = @isdayoff " & _
                      ", userunkid = @userunkid" & _
                      ", loginemployeeunkid = @loginemployeeunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason" & _
                      ", voiddatetime = @voiddatetime " & _
                      " WHERE otrequisitiontranunkid = @otrequisitiontranunkid "


            'Pinkal (23-Nov-2019) --  'Enhancement NMB - Working On OT Enhancement for NMB.[ ", shiftunkid = @shiftunkid ,dayid = (DATEPART(WEEKDAY,@requestdate)) -1 , isweekend = @isweekend , isholiday = @isholiday , isdayoff = @isdayoff " & _]

            'Pinkal (24-Oct-2019) --  'Enhancement NMB - Working On OT Enhancement for NMB.[ plannednight_hrs = @plannednight_hrs , actualnight_hrs = @actualnight_hrs ]


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If Insert_AtTranLog(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If mblnIssubmit_Approval Then

                Dim objApproverMst As New clsTnaapprover_master

                'Pinkal (23-Nov-2019) -- Start
                'Enhancement NMB - Working On OT Enhancement for NMB.
                'Dim dtApproverList As DataTable = objApproverMst.GetEmployeeApprover(xDatabaseName, xYearUnkid, xCompanyUnkid, mdtEmployeeAsonDate, False, -1, mintEmployeeunkid, -1, objDataOperation, False, "")
                Dim dtApproverList As DataTable = objApproverMst.GetEmployeeApprover(xDatabaseName, xYearUnkid, xCompanyUnkid, mdtEmployeeAsonDate, False, -1, mintEmployeeunkid, -1, objDataOperation, False, "", mblnIncludeCapApprover)
                'Pinkal (23-Nov-2019) -- End

                objApproverMst = Nothing

                If dtApproverList IsNot Nothing AndAlso dtApproverList.Rows.Count > 0 Then

                    objOTApproval._Otrequisitiontranunkid = mintOtrequisitiontranunkid
                    objOTApproval._Employeeunkid = mintEmployeeunkid
                    objOTApproval._Approvaldate = Nothing
                    objOTApproval._Ottypeunkid = mintOttypeunkid
                    objOTApproval._Periodunkid = mintPeriodunkid
                    objOTApproval._Actualstart_Time = mdtPlannedstart_Time
                    objOTApproval._Actualend_Time = mdtPlannedend_Time
                    objOTApproval._Actualot_Hours = mintPlannedot_Hours
                    objOTApproval._Adjustment_Reason = mstrAdjustment_Reason
                    objOTApproval._Statusunkid = 2
                    objOTApproval._Userunkid = mintUserunkid
                    objOTApproval._Isvoid = False
                    objOTApproval._Voiduserunkid = -1
                    objOTApproval._Voiddatetime = Nothing
                    objOTApproval._Voidreason = ""
                    objOTApproval._Iscancel = False
                    objOTApproval._Canceluserunkid = -1
                    objOTApproval._Canceldatetime = Nothing
                    objOTApproval._Cancelreason = ""
                    objOTApproval._Form_Name = mstrWebFormName
                    objOTApproval._ClientIp = mstrWebClientIP
                    objOTApproval._Hostname = mstrWebHostName
                    objOTApproval._Audituserunkid = mintUserunkid
                    objOTApproval._Loginemoployeeunkid = mintLoginemployeeunkid
                    objOTApproval._Isweb = mblnIsFromWeb

                    'Pinkal (24-Oct-2019) -- Start
                    'Enhancement NMB - Working On OT Enhancement for NMB.
                    objOTApproval._ActualNight_Hours = mintplannedNight_Hours
                    'Pinkal (24-Oct-2019) -- End


                    Dim mintMinPriority As Integer = 0
                    mintMinPriority = dtApproverList.AsEnumerable().Select(Function(x) x.Field(Of Integer)("tnapriority")).Min()

                    For Each dr As DataRow In dtApproverList.Rows

                        objOTApproval._Tnamappingunkid = CInt(dr("tnamappingunkid"))
                        objOTApproval._Tnalevelunkid = CInt(dr("tnalevelunkid"))
                        objOTApproval._Mapuserunkid = CInt(dr("mapuserunkid"))
                        objOTApproval._Tnapriority = CInt(dr("tnapriority"))

                        If mintMinPriority = CInt(dr("tnapriority")) Then
                            objOTApproval._Visibleunkid = 2
                        Else
                            objOTApproval._Visibleunkid = -1
                        End If

                        If objOTApproval.Insert(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Next

                End If   ' If dtApproverList IsNot Nothing AndAlso dtApproverList.Rows.Count > 0 Then

            End If '  If mblnIssubmit_Approval Then

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tnaot_requisition_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation()
        objDataOperation.BindTransaction()

        Try

            strQ = "UPDATE tnaot_requisition_tran SET " & _
                   "  isvoid = @isvoid " & _
                   " ,voiduserunkid = @voiduserunkid " & _
                   " ,voiddatetime = @voiddatetime " & _
                   " ,voidreason = @voidreason " & _
                   " ,voidloginemployeeunkid = @voidloginemployeeunkid " & _
                   "WHERE otrequisitiontranunkid = @otrequisitiontranunkid"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginemployeeunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Otrequisitiontranunkid(objDataOperation) = intUnkid

            If Insert_AtTranLog(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal mdtRequestDate As DateTime, ByVal xOtRequisitionId As Integer, ByVal xEmployeeId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT " & _
                      "  otrequisitiontranunkid " & _
                      ", employeeunkid " & _
                      ", transactiondate " & _
                      ", requestdate " & _
                      ", ottypeunkid " & _
                      ", periodunkid " & _
                      ", plannedstart_time " & _
                      ", plannedend_time " & _
                      ", plannedot_hours " & _
                      ", request_reason " & _
                      ", actualstart_time " & _
                      ", actualend_time " & _
                      ", actualot_hours " & _
                      ", adjustment_reason " & _
                      ", statusunkid " & _
                      ", finalmappedapproverunkid " & _
                      ", issubmit_approval " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", voiddatetime " & _
                      ", ISNULL(plannednight_hrs,0) AS plannednight_hrs " & _
                      ", ISNULL(actualnight_hrs,0) AS actualnight_hrs " & _
                      ", ISNULL(shiftunkid,0) AS shiftunkid " & _
                      ", ISNULL(dayid,DATEPART(dw,requestdate) - 1) AS dayid  " & _
                      ", ISNULL(isweekend,0) AS isweekend  " & _
                      ", ISNULL(isholiday,0) AS isholiday  " & _
                      ", ISNULL(isdayoff,0) AS isdayoff  " & _
                      " FROM tnaot_requisition_tran " & _
                      " WHERE isvoid = 0 AND tnaot_requisition_tran.statusunkid not in (3,6) AND CONVERT(CHAR(8),requestdate,112) =  @requestdate " & _
                      " AND employeeunkid = @employeeunkid "

            If xOtRequisitionId > 0 Then
                strQ &= " AND otrequisitiontranunkid <> @otrequisitiontranunkid "
            End If


            'Pinkal (23-Nov-2019) -- Enhancement NMB - Working On OT Enhancement for NMB.[", ISNULL(shiftunkid,0) AS shiftunkid ,ISNULL(dayid,DATEPART(dw,requestdate) - 1) AS dayid , ISNULL(isweekend,0) AS isweekend , ISNULL(isholiday,0) AS isholiday, ISNULL(isdayoff,0) AS isdayoff  "]

            'Pinkal (14-Nov-2019) --  'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.[tnaot_requisition_tran.statusunkid not in (3,6)]

            'Pinkal (24-Oct-2019) --  'Enhancement NMB - Working On OT Enhancement for NMB.[ ", ISNULL(plannednight_hrs,0) AS plannednight_hrs, ", ISNULL(actualnight_hrs,0) AS actualnight_hrs " & _ ]

            objDataOperation.ClearParameters()

            If xOtRequisitionId > 0 Then
                objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xOtRequisitionId)
            End If

            objDataOperation.AddParameter("@requestdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtRequestDate))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Function Insert_AtTranLog(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO attnaot_Requisition_tran ( " & _
                      "  tranguid " & _
                      ", otrequisitiontranunkid " & _
                      ", employeeunkid " & _
                      ", transactiondate " & _
                      ", requestdate " & _
                      ", ottypeunkid " & _
                      ", periodunkid " & _
                      ", plannedstart_time " & _
                      ", plannedend_time " & _
                      ", plannedot_hours " & _
                      ", request_reason " & _
                      ", actualstart_time " & _
                      ", actualend_time " & _
                      ", actualot_hours " & _
                      ", adjustment_reason " & _
                      ", plannednight_hrs " & _
                      ", actualnight_hrs " & _
                      ", statusunkid " & _
                      ", finalmappedapproverunkid " & _
                      ", issubmit_approval " & _
                      ", shiftunkid " & _
                      ", dayid " & _
                      ", isweekend " & _
                      ", isholiday " & _
                      ", isdayoff " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip" & _
                      ", hostname" & _
                      ", form_name " & _
                      ", isweb " & _
                      ", loginemployeeunkid " & _
                      ", voidloginemployeeunkid " & _
                      " ) VALUES (" & _
                      "  @tranguid " & _
                      ", @otrequisitiontranunkid " & _
                      ", @employeeunkid " & _
                      ", @transactiondate " & _
                      ", @requestdate " & _
                      ", @ottypeunkid " & _
                      ", @periodunkid " & _
                      ", @plannedstart_time " & _
                      ", @plannedend_time " & _
                      ", @plannedot_hours " & _
                      ", @request_reason " & _
                      ", @actualstart_time " & _
                      ", @actualend_time " & _
                      ", @actualot_hours " & _
                      ", @adjustment_reason " & _
                      ", @plannednight_hrs " & _
                      ", @actualnight_hrs " & _
                      ", @statusunkid " & _
                      ", @finalmappedapproverunkid " & _
                      ", @issubmit_approval " & _
                      ", @shiftunkid " & _
                      ", (DATEPART(WEEKDAY,@requestdate)) -1 " & _
                      ", @isweekend " & _
                      ", @isholiday " & _
                      ", @isdayoff " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @auditdatetime " & _
                      ", @ip" & _
                      ", @hostname" & _
                      ", @form_name " & _
                      ", @isweb " & _
                      ", @loginemployeeunkid " & _
                      ", @voidloginemployeeunkid " & _
                      ") "

            'Pinkal (23-Nov-2019) --  'Enhancement NMB - Working On OT Enhancement for NMB.[@shiftunkid , (DATEPART(WEEKDAY,@requestdate)) -1, @isweekend , @isholiday , @isdayoff " ]

            'Pinkal (24-Oct-2019) --  'Enhancement NMB - Working On OT Enhancement for NMB.[ plannednight_hrs,actualnight_hrs]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
            objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequisitiontranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtTransactiondate <> Nothing, mdtTransactiondate, DBNull.Value))
            objDataOperation.AddParameter("@requestdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtRequestdate <> Nothing, mdtRequestdate, DBNull.Value))
            objDataOperation.AddParameter("@ottypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOttypeunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@plannedstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtPlannedstart_Time <> Nothing, mdtPlannedstart_Time, DBNull.Value))
            objDataOperation.AddParameter("@plannedend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtPlannedend_Time <> Nothing, mdtPlannedend_Time, DBNull.Value))
            objDataOperation.AddParameter("@plannedot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPlannedot_Hours.ToString)
            objDataOperation.AddParameter("@request_reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRequest_Reason.ToString)
            objDataOperation.AddParameter("@actualstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtActualstart_Time <> Nothing, mdtActualstart_Time, DBNull.Value))
            objDataOperation.AddParameter("@actualend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtActualend_Time <> Nothing, mdtActualend_Time, DBNull.Value))
            objDataOperation.AddParameter("@actualot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActualot_Hours.ToString)
            objDataOperation.AddParameter("@adjustment_reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAdjustment_Reason.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@finalmappedapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalmappedapproverunkid.ToString)
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmit_Approval.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginemployeeunkid)

            'Pinkal (24-Oct-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            objDataOperation.AddParameter("@plannednight_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintplannedNight_Hours.ToString)
            objDataOperation.AddParameter("@actualnight_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintactuaNight_Hours.ToString)
            'Pinkal (24-Oct-2019) -- End

            'Pinkal (23-Nov-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            objDataOperation.AddParameter("@shiftunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShiftId.ToString)
            objDataOperation.AddParameter("@isweekend", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweekend.ToString)
            objDataOperation.AddParameter("@isholiday", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsholiday.ToString)
            objDataOperation.AddParameter("@isdayoff", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDayOff.ToString)
            'Pinkal (23-Nov-2019) -- End

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Function UpdateOTRequisitionStatus(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mblnFlag As Boolean = False
        Try

            strQ = " UPDATE tnaot_requisition_tran SET " & _
                      "  actualstart_time = @actualstart_time " & _
                      ", actualend_time = @actualend_time " & _
                      ", actualot_hours = @actualot_hours " & _
                      ", adjustment_reason = @adjustment_reason " & _
                      ", finalmappedapproverunkid = @finalmappedapproverunkid " & _
                      ", statusunkid = @statusunkid " & _
                      ", actualnight_hrs = @actualnight_hrs " & _
                      " WHERE otrequisitiontranunkid = @otrequisitiontranunkid "

            'Pinkal (24-Oct-2019) -- 'Enhancement NMB - Working On OT Enhancement for NMB.[", actualnight_hrs = @actualnight_hrs " & _]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequisitiontranunkid)
            objDataOperation.AddParameter("@actualstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtActualstart_Time <> Nothing, mdtActualstart_Time, DBNull.Value))
            objDataOperation.AddParameter("@actualend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtActualend_Time <> Nothing, mdtActualend_Time, DBNull.Value))
            objDataOperation.AddParameter("@actualot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActualot_Hours)
            objDataOperation.AddParameter("@adjustment_reason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrAdjustment_Reason)
            objDataOperation.AddParameter("@finalmappedapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalmappedapproverunkid)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid)

            'Pinkal (24-Oct-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            objDataOperation.AddParameter("@actualnight_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintactuaNight_Hours)
            'Pinkal (24-Oct-2019) -- End


            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If Insert_AtTranLog(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mblnFlag = True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateOTRequisitionStatus; Module Name: " & mstrModuleName)
        End Try
        Return mblnFlag
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    'Public Function UpdateGlobalSubmitForApproval(ByVal xDataBaseName As String, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
    '                                                                    , ByVal mdtEmployeeAsonDate As Date, ByVal mdtEmpOT As DataTable, ByVal mblnIncludeCapApprover As Boolean _
    '                                                                    , ByVal xStartDate As Date, ByVal xEndDate As Date) As Boolean

    Public Function UpdateGlobalSubmitForApproval(ByVal xDataBaseName As String, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                                                       , ByVal mdtEmployeeAsonDate As Date, ByVal mstrOTRequisitionIds As String, ByVal mblnIncludeCapApprover As Boolean _
                                                                        , ByVal xStartDate As Date, ByVal xEndDate As Date) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mblnFlag As Boolean = False
        Try

            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.
            'If mdtEmpOT Is Nothing Then Return False
            'Pinkal (12-Oct-2021)-- End

            Dim objConfig As New clsConfigOptions
            Dim intTotalMins As Integer = 0
            Dim strCapOTHrsForHODApprovers As String = objConfig.GetKeyValue(xCompanyUnkid, "CapOTHrsForHODApprovers", Nothing)
            If strCapOTHrsForHODApprovers.Length > 0 Then
                Dim arrCapOTHrsForHODApprovers As String() = strCapOTHrsForHODApprovers.Split(New Char() {":"c})
                If arrCapOTHrsForHODApprovers.Length > 0 Then
                    intTotalMins = (Convert.ToInt32(arrCapOTHrsForHODApprovers(0)) * 60) + Convert.ToInt32(arrCapOTHrsForHODApprovers(1))
                End If
            End If
            objConfig = Nothing


            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.

            'For Each dr As DataRow In mdtEmpOT.Rows
            '    Dim xUserunkid As Integer = mintUserunkid
            '    _Otrequisitiontranunkid(objDataOperation) = CInt(dr("otrequisitiontranunkid"))
            '    mintUserunkid = xUserunkid

            '    mblnIssubmit_Approval = True

            '    mblnIncludeCapApprover = False
            '    If strCapOTHrsForHODApprovers.Trim.Length > 0 Then
            '        Dim intAppliedOThours As Double = CalculateTime(False, GetEmpTotalOThours(xStartDate, xEndDate, mintEmployeeunkid.ToString(), objDataOperation, True, True))
            '        If intTotalMins <= (intAppliedOThours + CalculateTime(False, CInt(dr("plannedot_hours")))) Then
            '            mblnIncludeCapApprover = True
            '        End If
            '    End If

            '    If Update(xDataBaseName, xYearUnkid, xCompanyUnkid, mdtEmployeeAsonDate, mblnIncludeCapApprover, objDataOperation) = False Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'Next

            If mstrOTRequisitionIds.Trim.Length <= 0 Then Return True

            Dim ar() As String = mstrOTRequisitionIds.Trim.Split(CChar(","))

            Dim objDataOperation As New clsDataOperation
            objDataOperation.BindTransaction()

            For i As Integer = 0 To ar.Length - 1
                Dim xUserunkid As Integer = mintUserunkid
                _Otrequisitiontranunkid(objDataOperation) = CInt(ar(i))
                mintUserunkid = xUserunkid

                mblnIssubmit_Approval = True

                mblnIncludeCapApprover = False
                If strCapOTHrsForHODApprovers.Trim.Length > 0 Then
                    Dim intAppliedOThours As Double = CalculateTime(False, GetEmpTotalOThours(xStartDate, xEndDate, mintEmployeeunkid.ToString(), objDataOperation, True, True))
                    If intTotalMins <= (intAppliedOThours + CalculateTime(False, mintPlannedot_Hours)) Then
                        mblnIncludeCapApprover = True
                    End If
                End If

                If Update(xDataBaseName, xYearUnkid, xCompanyUnkid, mdtEmployeeAsonDate, mblnIncludeCapApprover, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next

            'Pinkal (12-Oct-2021)-- End

            objDataOperation.ReleaseTransaction(True)
            mblnFlag = True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateGlobalSubmitForApproval; Module Name: " & mstrModuleName)
        End Try
        Return mblnFlag
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Function getPeriodwise_PendingEmpOtStatus(ByVal xStartDate As Date, ByVal xEndDate As Date, ByVal xEmployeeid As Integer _
                                                                             , Optional ByVal xOTRequisitiontranunkid As Integer = 0 _
                                                                             , Optional ByVal mblnIncludeSubmitForApproval As Boolean = False) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            If objDataOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDataOperation
            End If
            objDataOperation.ClearParameters()


            strQ = " SELECT statusunkid FROM tnaot_requisition_tran " & _
                      " WHERE isvoid =0 AND employeeunkid =@employeeunkid  and statusunkid = 2 AND CONVERT(CHAR(8),requestdate,112) BETWEEN @startdate AND @enddate  "

            If xOTRequisitiontranunkid > 0 Then
                strQ &= " AND otrequisitiontranunkid <> @otrequisitiontranunkid"
                objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xOTRequisitiontranunkid)
            End If


            If mblnIncludeSubmitForApproval Then
                strQ &= " AND issubmit_approval = @issubmit_approval"
                objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIncludeSubmitForApproval)
            End If

            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xStartDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xEndDate))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeid)

            dsList = objDataOperation.ExecQuery(strQ, "Pendinglist")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList.Tables("Pendinglist").Rows.Count > 0 Then
                Return True
            End If

            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getPeriodwise_PendingEmpOtStatus; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 

    'Pinkal (12-Oct-2021)-- Start
    'NMB OT Requisition Performance Issue.

    'Public Function CancelApprovedOTRequisition(ByVal xDatabaseName As String, ByVal mdtEmployeeAsonDate As Date, ByVal xUserId As Integer _
    '                                                                , ByVal dtRequisition As DataTable, ByVal mstrCancelReason As String) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim mblnFlag As Boolean = False
    '    Dim objDataOperation As clsDataOperation = Nothing
    '    Try

    '        objDataOperation = New clsDataOperation
    '        objDataOperation.BindTransaction()

    '        If dtRequisition Is Nothing Then Return True

    '        Dim objApproval As New clsTnaotrequisition_approval_Tran
    '        Dim objOTPosting As New clstnaotrequisition_process_Tran

    '        For Each dr As DataRow In dtRequisition.Rows


    '            strQ = " UPDATE tnaotrequisition_process_tran SET " & _
    '                       " isvoid = 1 " & _
    '                       ",voiduserunkid  = @voiduserunkid " & _
    '                       ",voiddatetime = getdate() " & _
    '                       ",voidreason = @voidreason " & _
    '                       " WHERE isvoid = 0 AND otrequisitiontranunkid = @otrequisitiontranunkid AND otrequisitionprocesstranunkid = @otrequisitionprocesstranunkid "

    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@otrequisitionprocesstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("otrequisitionprocesstranunkid")))
    '            objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("otrequisitiontranunkid")))
    '            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
    '            objDataOperation.AddParameter("@voidreason", SqlDbType.Text, eZeeDataType.DESC_SIZE, mstrCancelReason)
    '            objDataOperation.ExecNonQuery(strQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            objOTPosting._Otrequisitionprocesstranunkid(objDataOperation) = CInt(dr("otrequisitionprocesstranunkid"))

    '            'Pinkal (04-Aug-2020) -- Start
    '            'Enhancement NMB  - Working on Expense Setting related changes in Expense Approval for NMB.	
    '            'objOTPosting._Userunkid = mintUserunkid
    '            objOTPosting._Userunkid = xUserId
    '            'Pinkal (04-Aug-2020) -- End

    '            objOTPosting._WebFormName = mstrWebFormName
    '            objOTPosting._WebHostName = mstrWebHostName
    '            objOTPosting._WebClientIP = mstrWebClientIP
    '            objOTPosting._Isweb = mblnIsFromWeb

    '            If objOTPosting.InsertAudiTrailForOTRequisitionProcessTran(objDataOperation, 3) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If


    '            'Pinkal (27-Feb-2020) -- Start
    '            'Enhancement - Changes related To OT NMB Testing.
    '            'dsList = objApproval.GetList("List", xDatabaseName, mdtEmployeeAsonDate, xUserId, False, objDataOperation, True, CInt(dr("otrequisitiontranunkid")), False, "")
    '            dsList = objApproval.GetList("List", xDatabaseName, mdtEmployeeAsonDate, xUserId, False, Nothing, Nothing, objDataOperation, True, CInt(dr("otrequisitiontranunkid")), False, "")
    '            'Pinkal (27-Feb-2020) -- End




    '            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

    '                strQ = " UPDATE tnaotrequisition_approval_tran SET " & _
    '                          " iscancel = 1 " & _
    '                          ",canceluserunkid  = @canceluserunkid " & _
    '                          ",canceldatetime = getdate() " & _
    '                          ",cancelreason = @cancelreason " & _
    '                          " WHERE isvoid = 0 AND otrequisitiontranunkid = @otrequisitiontranunkid AND otrequestapprovaltranunkid = @otrequestapprovaltranunkid "


    '                For Each drRow As DataRow In dsList.Tables(0).Rows
    '                    objDataOperation.ClearParameters()
    '                    objDataOperation.AddParameter("@otrequestapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("otrequestapprovaltranunkid")))
    '                    objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("otrequisitiontranunkid")))

    '                    'Pinkal (04-Aug-2020) -- Start
    '                    'Enhancement NMB  - Working on Expense Setting related changes in Expense Approval for NMB.	
    '                    'objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
    '                    objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)
    '                    'Pinkal (04-Aug-2020) -- End

    '                    objDataOperation.AddParameter("@cancelreason", SqlDbType.Text, eZeeDataType.DESC_SIZE, mstrCancelReason)
    '                    objDataOperation.ExecNonQuery(strQ)

    '                    If objDataOperation.ErrorMessage <> "" Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If

    '                    objApproval._Otrequestapprovaltranunkid(objDataOperation) = CInt(drRow("otrequestapprovaltranunkid"))

    '                    'Pinkal (04-Aug-2020) -- Start
    '                    'Enhancement NMB  - Working on Expense Setting related changes in Expense Approval for NMB.	
    '                    'objApproval._Audituserunkid = mintUserunkid
    '                    'objApproval._Canceluserunkid = mintUserunkid
    '                    objApproval._Audituserunkid = xUserId
    '                    objApproval._Canceluserunkid = xUserId
    '                    'Pinkal (04-Aug-2020) -- End

    '                    objApproval._Form_Name = mstrWebFormName
    '                    objApproval._Hostname = mstrWebHostName
    '                    objApproval._ClientIp = mstrWebClientIP
    '                    objApproval._Isweb = mblnIsFromWeb

    '                    If objApproval.AtInsertApprovalTran(objDataOperation, 2) = False Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If

    '                Next

    '            End If

    '            strQ = " UPDATE tnaot_requisition_tran SET " & _
    '                      "  statusunkid = @statusunkid " & _
    '                      " WHERE isvoid = 0 AND statusunkid = 1 AND otrequisitiontranunkid = @otrequisitiontranunkid "

    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("otrequisitiontranunkid")))
    '            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid)
    '            objDataOperation.ExecNonQuery(strQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            _Otrequisitiontranunkid(objDataOperation) = CInt(dr("otrequisitiontranunkid"))


    '            'Pinkal (04-Aug-2020) -- Start
    '            'Enhancement NMB  - Working on Expense Setting related changes in Expense Approval for NMB.	
    '            mintUserunkid = xUserId
    '            'Pinkal (04-Aug-2020) -- End

    '            If Insert_AtTranLog(objDataOperation, 2) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '        Next

    '        objOTPosting = Nothing
    '        objApproval = Nothing

    '        objDataOperation.ReleaseTransaction(True)

    '        mblnFlag = True

    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: CancelApprovedOTRequisition; Module Name: " & mstrModuleName)
    '    End Try
    '    Return mblnFlag
    'End Function


    Public Function CancelApprovedOTRequisition(ByVal xDatabaseName As String, ByVal mdtEmployeeAsonDate As Date, ByVal xUserId As Integer _
                                                                    , ByVal mstrOTRequisitionIds As String, ByVal mstrCancelReason As String) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mblnFlag As Boolean = False
        Dim objDataOperation As clsDataOperation = Nothing
        Try

            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            If mstrOTRequisitionIds.Trim.Length <= 0 Then Return True

            Dim ar() As String = mstrOTRequisitionIds.Trim.Split(CChar(","))

            Dim objApproval As New clsTnaotrequisition_approval_Tran
            Dim objOTPosting As New clstnaotrequisition_process_Tran

            For i As Integer = 0 To ar.Length - 1

                Dim xotrequisitionprocesstranunkid As Integer = 0
                Dim dsProcessList As DataSet = objOTPosting.GetList("List", True, 0, "tnaotrequisition_process_tran.otrequisitiontranunkid = " & CInt(ar(i)), False, objDataOperation)

                If dsProcessList IsNot Nothing AndAlso dsProcessList.Tables(0).Rows.Count > 0 Then
                    xotrequisitionprocesstranunkid = CInt(dsProcessList.Tables(0).Rows(0)("otrequisitionprocesstranunkid"))
                End If


                strQ = " UPDATE tnaotrequisition_process_tran SET " & _
                           " isvoid = 1 " & _
                           ",voiduserunkid  = @voiduserunkid " & _
                           ",voiddatetime = getdate() " & _
                           ",voidreason = @voidreason " & _
                           " WHERE isvoid = 0 AND otrequisitiontranunkid = @otrequisitiontranunkid AND otrequisitionprocesstranunkid = @otrequisitionprocesstranunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@otrequisitionprocesstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xotrequisitionprocesstranunkid)
                objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(ar(i)))
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                objDataOperation.AddParameter("@voidreason", SqlDbType.Text, eZeeDataType.DESC_SIZE, mstrCancelReason)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objOTPosting._Otrequisitionprocesstranunkid(objDataOperation) = xotrequisitionprocesstranunkid
                objOTPosting._Userunkid = xUserId
                objOTPosting._WebFormName = mstrWebFormName
                objOTPosting._WebHostName = mstrWebHostName
                objOTPosting._WebClientIP = mstrWebClientIP
                objOTPosting._Isweb = mblnIsFromWeb

                If objOTPosting.InsertAudiTrailForOTRequisitionProcessTran(objDataOperation, 3) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                dsList = objApproval.GetList("List", xDatabaseName, mdtEmployeeAsonDate, xUserId, False, Nothing, Nothing, objDataOperation, True, CInt(ar(i)), False, "")

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                    strQ = " UPDATE tnaotrequisition_approval_tran SET " & _
                              " iscancel = 1 " & _
                              ",canceluserunkid  = @canceluserunkid " & _
                              ",canceldatetime = getdate() " & _
                              ",cancelreason = @cancelreason " & _
                              " WHERE isvoid = 0 AND otrequisitiontranunkid = @otrequisitiontranunkid AND otrequestapprovaltranunkid = @otrequestapprovaltranunkid "


                    For Each drRow As DataRow In dsList.Tables(0).Rows
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@otrequestapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("otrequestapprovaltranunkid")))
                        objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("otrequisitiontranunkid")))
                        objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)
                        objDataOperation.AddParameter("@cancelreason", SqlDbType.Text, eZeeDataType.DESC_SIZE, mstrCancelReason)
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        objApproval._Otrequestapprovaltranunkid(objDataOperation) = CInt(drRow("otrequestapprovaltranunkid"))
                        objApproval._Audituserunkid = xUserId
                        objApproval._Canceluserunkid = xUserId
                        objApproval._Form_Name = mstrWebFormName
                        objApproval._Hostname = mstrWebHostName
                        objApproval._ClientIp = mstrWebClientIP
                        objApproval._Isweb = mblnIsFromWeb

                        If objApproval.AtInsertApprovalTran(objDataOperation, 2) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Next

                End If

                strQ = " UPDATE tnaot_requisition_tran SET " & _
                          "  statusunkid = @statusunkid " & _
                          " WHERE isvoid = 0 AND statusunkid = 1 AND otrequisitiontranunkid = @otrequisitiontranunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(ar(i)))
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                _Otrequisitiontranunkid(objDataOperation) = CInt(ar(i))


                mintUserunkid = xUserId

                If Insert_AtTranLog(objDataOperation, 2) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

            objOTPosting = Nothing
            objApproval = Nothing

            objDataOperation.ReleaseTransaction(True)

            mblnFlag = True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: CancelApprovedOTRequisition; Module Name: " & mstrModuleName)
        End Try
        Return mblnFlag
    End Function

    'Pinkal (12-Oct-2021)-- End


    'Pinkal (07-Nov-2019) -- Start
    'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Sub Send_Notification_Approver(ByVal strDatabaseName As String, _
                                                            ByVal intYearID As Integer, _
                                                            ByVal intCompanyID As Integer, _
                                                            ByVal dtEmployeeAsOnDate As Date, _
                                                            ByVal blnIncludeInactiveEmp As Boolean, _
                                                            ByVal strEmployeeID As String, _
                                                            ByVal mdtOTStartDate As Date, _
                                                            ByVal mdtOTEndDate As Date, _
                                                            ByVal intCurrentPriority As Integer, _
                                                            ByVal dtSelectedData As DataTable, _
                                                            ByVal intLoginTypeId As Integer, _
                                                            ByVal intLoginEmployeeId As Integer, _
                                                            ByVal intUserId As Integer, _
                                                            ByVal strArutiSelfServiceURL As String, _
                                                            ByVal mblnApplyFromApproval As Boolean, _
                                                            ByVal mblnIncludeCapApprover As Boolean, _
                                                            Optional ByVal mstrAdjustmentReason As String = "")

        Try
            Dim objMail As New clsSendMail
            Dim intMinPriority As Integer = -1
            Dim strSubject As String = String.Empty
            Dim strMessage As String = String.Empty
            Dim strLink As String = String.Empty
            Dim objApprover As New clsTnaapprover_master
            Dim dtApproverList As DataTable = Nothing
            Dim dicEmployeeGroup As New Dictionary(Of String, Integer)
            Dim dicTsApprIDs As New Dictionary(Of Integer, Integer)

            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub


            Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo

            'Dim objPeriod As New clscommom_period_Tran
            'objPeriod._Periodunkid(strDatabaseName) = intPeriodID

            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.
            If dtSelectedData IsNot Nothing AndAlso dtSelectedData.Rows.Count > 0 Then
                dtSelectedData = New DataView(dtSelectedData, "IsGrp=0", "employeeunkid,RDate", DataViewRowState.CurrentRows).ToTable
            End If
            'Pinkal (12-Oct-2021)-- End


            dtApproverList = objApprover.GetEmployeeApprover(strDatabaseName, intYearID, intCompanyID, dtEmployeeAsOnDate, blnIncludeInactiveEmp, _
                                                                                    -1, strEmployeeID, -1, Nothing, False, "tnapriority > " & intCurrentPriority & " ", mblnIncludeCapApprover)

            objMail._Subject = Language.getMessage(mstrModuleName, 3, "Notification for Approving OT Requisition")

            Dim intTSApproverID As Integer = -1
            Dim arrayEmpID() As String = strEmployeeID.Split(",")
            If arrayEmpID.Length > 0 Then
                For i As Integer = 0 To arrayEmpID.Length - 1
                    Dim dtTable As DataTable = New DataView(dtApproverList, "employeeunkid=" & CInt(arrayEmpID(i)) & "", "", DataViewRowState.CurrentRows).ToTable
                    If dtTable.Rows.Count > 0 Then
                        Dim dRow() As DataRow = dtTable.Select("tnapriority=MIN(tnapriority)")

                        If dRow.Length > 0 Then

                            For Each dtRow In dRow

                                If dicTsApprIDs.Values.Contains(CInt(dtRow.Item("tnamappingunkid"))) = False Then
                                    dicTsApprIDs.Add(CInt(dtRow.Item("approveremployeeunkid")), CInt(dtRow.Item("tnamappingunkid")))

                                    If CStr(dtRow.Item("approveremail")) = "" Then Continue For

                                    strMessage = "<HTML><BODY>"
                                    strMessage &= Language.getMessage(mstrModuleName, 4, "Dear") & " " & info1.ToTitleCase(dtRow.Item("employeename").ToString().ToLower()) & ", <BR><BR>"


                                    'strMessage &= Language.getMessage(mstrModuleName, 5, "This is the notification for approving the Employee OT Requisition for period") & _
                                    '              " " & objPeriod._Period_Name.ToString & "." & "<BR></BR><BR></BR>"

                                    strMessage &= Language.getMessage(mstrModuleName, 5, "This is the notification for approving the Employee OT Requisition for period") & _
                                                 " <B>" & mdtOTStartDate.Date & " - " & mdtOTEndDate.Date & "</B>." & "<BR></BR><BR></BR>"


                                    'strLink = strArutiSelfServiceURL & "/TnA/OT_Requisition/Ot_RequisitionApproval.aspx?" & _
                                    '          HttpUtility.UrlEncode(clsCrypto.Encrypt(intPeriodID.ToString & "|" & dtRow.Item("tnamappingunkid").ToString & _
                                    '                                                  "|" & intCompanyID.ToString & "|" & dtRow.Item("mapuserunkid").ToString))

                                    strLink = strArutiSelfServiceURL & "/TnA/OT_Requisition/Ot_RequisitionApproval.aspx?" & _
                                            HttpUtility.UrlEncode(clsCrypto.Encrypt(eZeeDate.convertDate(mdtOTStartDate.Date).ToString() & "|" & eZeeDate.convertDate(mdtOTEndDate.Date).ToString() & "|" & dtRow.Item("tnamappingunkid").ToString & _
                                                                                    "|" & intCompanyID.ToString & "|" & dtRow.Item("mapuserunkid").ToString))


                                    strMessage &= Language.getMessage(mstrModuleName, 6, "Please click on the following link to approve.") & _
                                                          "<BR></BR><BR></BR><a href='" & strLink & "'>" & strLink & "</a>" & "<BR></BR><BR></BR>"


                                    'Pinkal (14-Nov-2019) -- Start
                                    'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.

                                    'strMessage &= Language.getMessage(mstrModuleName, 7, "Please refer the following OT Requisition Details.") & "<BR></BR><BR></BR>"

                                    'strMessage &= "<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=3 WIDTH='100%'>"

                                    'strMessage &= "<TR WIDTH='100%'>"
                                    'strMessage &= "<TD WIDTH='10%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 8, "Request Date") & "</B></FONT></TD>"
                                    'strMessage &= "<TD WIDTH='15%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 9, "Planned Start Time") & "</B></FONT></TD>"
                                    'strMessage &= "<TD WIDTH='15%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 10, "Planned End Time") & "</B></FONT></TD>"
                                    'strMessage &= "<TD WIDTH='15%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 11, "Planned OT Hours") & "</B></FONT></TD>"
                                    'strMessage &= "<TD WIDTH='18%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 12, "Request Reason") & "</B></FONT></TD>"

                                    'If mblnApplyFromApproval Then
                                    '    strMessage &= "<TD WIDTH='15%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 13, "Actual Start Time") & "</B></FONT></TD>"
                                    '    strMessage &= "<TD WIDTH='15%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 14, "Actual End Time") & "</B></FONT></TD>"
                                    '    strMessage &= "<TD WIDTH='15%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 15, "Actual OT Hours") & "</B></FONT></TD>"
                                    '    strMessage &= "<TD WIDTH='18%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 16, "Adjustment Reason") & "</B></FONT></TD>"
                                    'End If

                                    'strMessage &= "</TR>"

                                    'For Each row As DataRow In dtSelectedData.Rows

                                    '    _Otrequisitiontranunkid = CInt(row.Item("otrequisitiontranunkid"))
                                    '    If mintStatusunkid = 1 Then Continue For

                                    '    If dicEmployeeGroup.Keys.Contains(CStr(CInt(dtRow.Item("tnamappingunkid")) & "|" & CInt(row.Item("employeeunkid")))) = False Then
                                    '        dicEmployeeGroup.Add(CStr(CInt(dtRow.Item("tnamappingunkid")) & "|" & CInt(row.Item("employeeunkid"))), CInt(row.Item("employeeunkid")))
                                    '        strMessage &= "<TR WIDTH='100%'>"

                                    '        If mblnApplyFromApproval = False Then
                                    '            strMessage &= "<TD WIDTH='100%' COLSPAN='6' ALIGN='LEFT' BGCOLOR='#DDD'><FONT SIZE=2><FONT COLOR='BLACK' SIZE=2><B>" & info1.ToTitleCase(row.Item("employeename").ToString().ToLower()) & "</B></FONT></TD>"
                                    '        Else
                                    '            strMessage &= "<TD WIDTH='100%' COLSPAN='9' ALIGN='LEFT' BGCOLOR='#DDD'><FONT SIZE=2><FONT COLOR='BLACK' SIZE=2><B>" & info1.ToTitleCase(row.Item("employeename").ToString().ToLower()) & "</B></FONT></TD>"
                                    '        End If

                                    '        strMessage &= "</TR>"
                                    '    End If

                                    '    strMessage &= "<TR WIDTH='100%'>"
                                    '    strMessage &= "<TD WIDTH='10%' ALIGN='LEFT'><FONT SIZE=2>" & CDate(row.Item("requestdate")).Date & "</FONT></TD>"
                                    '    strMessage &= "<TD WIDTH='15%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("plannedstart_time").ToString & "</FONT></TD>"
                                    '    strMessage &= "<TD WIDTH='15%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("plannedend_time").ToString & "</FONT></TD>"
                                    '    If mblnApplyFromApproval = False Then
                                    '        strMessage &= "<TD WIDTH='15%' ALIGN='LEFT'><FONT SIZE=2>" & CalculateTime(True, row.Item("plannedot_hours").ToString).ToString("#00.00").Replace(".", ":") & "</FONT></TD>"
                                    '    Else
                                    '        strMessage &= "<TD WIDTH='15%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("plannedot_hours").ToString() & "</FONT></TD>"
                                    '    End If
                                    '    strMessage &= "<TD WIDTH='18%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("request_reason").ToString & "</FONT></TD>"

                                    '    If mblnApplyFromApproval Then
                                    '        strMessage &= "<TD WIDTH='15%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("actualstart_time").ToString() & "</FONT></TD>"
                                    '        strMessage &= "<TD WIDTH='15%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("actualend_time").ToString() & "</FONT></TD>"
                                    '        strMessage &= "<TD WIDTH='15%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("actualot_hours").ToString() & "</FONT></TD>"
                                    '        strMessage &= "<TD WIDTH='18%' ALIGN='LEFT'><FONT SIZE=2>" & mstrAdjustmentReason & "</FONT></TD>"
                                    '    End If

                                    '    strMessage &= "</TR>"

                                    'Next  '  For Each row As DataRow In dtSelectedData.Rows


                                    'Pinkal (14-Nov-2019) -- End

                                    strMessage &= "</TABLE>"
                                    strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                                    strMessage &= "</BODY></HTML>"


                                    objMail._Message = strMessage
                                    objMail._ToEmail = dtRow.Item("approveremail")
                                    If intLoginTypeId <= 0 Then intLoginTypeId = enLogin_Mode.DESKTOP
                                    If mstrWebFormName.Trim.Length > 0 Then
                                        objMail._Form_Name = mstrWebFormName
                                        objMail._WebClientIP = mstrWebClientIP
                                        objMail._WebHostName = mstrWebHostName
                                    End If
                                    objMail._LogEmployeeUnkid = intLoginEmployeeId
                                    objMail._OperationModeId = intLoginTypeId
                                    objMail._UserUnkid = intUserId

                                    objMail._SenderAddress = IIf(dtRow.Item("approveremail") = "", dtRow.Item("employeename"), dtRow.Item("approveremail"))

                                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.OTREQUISITION_MGT
                                    objMail.SendMail(intCompanyID)

                                End If

                            Next   ' For Each dtRow In dRow

                        End If

                    End If

                Next

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_Notification_Approver; Module Name: " & mstrModuleName)
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Sub Send_Notification_Employee(ByVal strDatabaseName As String, _
                                                              ByVal intYearID As Integer, _
                                                              ByVal intCompanyID As Integer, _
                                                              ByVal dtEmployeeAsOnDate As Date, _
                                                              ByVal blnIncludeInactiveEmp As Boolean, _
                                                              ByVal strUserAccessFilter As String, _
                                                              ByVal strEmployeeIDs As String, _
                                                              ByVal strOTRequisitionIDs As String, _
                                                              ByVal mdtOTStartDate As Date, _
                                                              ByVal mdtOTEndDate As Date, _
                                                              ByVal intStatusId As Integer, _
                                                              ByVal intLoginTypeId As Integer, _
                                                              ByVal intLoginEmployeeId As Integer, _
                                                              ByVal intUserId As Integer, _
                                                              Optional ByVal strRemarks As String = "")

        Dim dsList As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Try
            objDataOperation = New clsDataOperation
            Dim objMail As New clsSendMail
            Dim objEmp As New clsEmployee_Master

            'Pinkal (29-Jan-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            'Dim objPeriod As New clscommom_period_Tran
            'Pinkal (29-Jan-2020) -- End

            Dim strSubject As String = String.Empty
            Dim strMessage As String = String.Empty
            Dim strLink As String = String.Empty

            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub



            'Pinkal (27-Feb-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            Dim objconfig As New clsConfigOptions
            Dim strArutiSelfServiceURL As String = ""
            strArutiSelfServiceURL = objconfig.GetKeyValue(intCompanyID, "ArutiSelfServiceURL", Nothing)
            objconfig = Nothing
            'Pinkal (27-Feb-2020) -- End



            Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
            objMail._Subject = Language.getMessage(mstrModuleName, 17, "OT Requisition Status Notification")


            'Pinkal (29-Jan-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            'objPeriod._Periodunkid(strDatabaseName) = intPeriodID
            'Pinkal (29-Jan-2020) -- End


            Dim arrayEmpID() As String = strEmployeeIDs.Split(",")

            If arrayEmpID.Length > 0 Then

                For i As Integer = 0 To arrayEmpID.Length - 1

                    objEmp._Employeeunkid(dtEmployeeAsOnDate) = CInt(arrayEmpID(i))
                    If objEmp._Email = "" Then Exit Sub

                    strMessage = "<HTML><BODY>"
                    strMessage &= Language.getMessage(mstrModuleName, 4, "Dear") & " " & info1.ToTitleCase(objEmp._Firstname.ToLower()) & " " & info1.ToTitleCase(objEmp._Surname.ToLower()) & ", <BR></BR><BR></BR>"

                    strMessage &= Language.getMessage(mstrModuleName, 18, "This is to inform you that the OT Requisition that was submitted for period") & " <B>" & _
                                  mdtOTStartDate.Date & " - " & mdtOTEndDate.Date & "</B> " & Language.getMessage(mstrModuleName, 19, "has been") & " "


                    If intStatusId = 1 Then
                        strMessage &= Language.getMessage("clsMasterData", 519, "Approved")
                    ElseIf intStatusId = 3 Then
                        strMessage &= Language.getMessage("clsMasterData", 112, "Rejected")
                    ElseIf intStatusId = 4 Then  'Temparory Used Delete Status = 4 only for email sending to employee not to save statusId = 4 in database.
                        strMessage &= Language.getMessage(mstrModuleName, 20, "Deleted")
                    ElseIf intStatusId = 6 Then
                        strMessage &= Language.getMessage("clsMasterData", 522, "Cancel")
                    End If

                    strMessage &= ".<BR></BR>"


                    'Pinkal (27-Feb-2020) -- Start
                    'Enhancement - Changes related To OT NMB Testing.
                    strLink = strArutiSelfServiceURL & "/TnA/OT_Requisition/OTRequisition.aspx?" & _
                                          HttpUtility.UrlEncode(clsCrypto.Encrypt(eZeeDate.convertDate(mdtOTStartDate.Date).ToString() & "|" & eZeeDate.convertDate(mdtOTEndDate.Date).ToString() & "|" & CInt(arrayEmpID(i)) & _
                                                                                  "|" & intCompanyID.ToString & "|" & intStatusId.ToString))


                    strMessage &= "<BR></BR><a href='" & strLink & "'>" & strLink & "</a>" & "<BR></BR><BR></BR>"

                    'Pinkal (27-Feb-2020) -- End




                    'Pinkal (14-Nov-2019) -- Start
                    'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.

                    'strMessage &= Language.getMessage(mstrModuleName, 7, "Please refer the following OT Requisition Details.") & "<BR></BR><BR></BR>"

                    'If strOTRequisitionIDs.Trim.Length <= 0 Then Exit Sub


                    'dsList = GetList("List", strDatabaseName, intUserId, intYearID, intCompanyID, blnIncludeInactiveEmp, objPeriod._TnA_StartDate.Date, objPeriod._TnA_EndDate.Date _
                    '                     , strUserAccessFilter, True, False, "", Nothing, CInt(arrayEmpID(i)), "", Nothing, Nothing, IIf(intStatusId = 4, 2, intStatusId), intPeriodID, False _
                    '                     , IIf(intStatusId = 4, -1, 1), IIf(intStatusId = 4, False, True))

                    'If dsList Is Nothing OrElse dsList.Tables(0).Rows.Count <= 0 Then Exit Sub

                    'If dsList.Tables(0).Rows.Count > 0 Then

                    '    strMessage &= "<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=3 WIDTH='100%'>"
                    '    strMessage &= "<TR WIDTH='100%'>"
                    '    strMessage &= "<TD WIDTH='10%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 8, "Request Date") & "</B></FONT></TD>"
                    '    strMessage &= "<TD WIDTH='15%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 9, "Planned Start Time") & "</B></FONT></TD>"
                    '    strMessage &= "<TD WIDTH='15%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 10, "Planned End Time") & "</B></FONT></TD>"
                    '    strMessage &= "<TD WIDTH='15%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 11, "Planned OT Hours") & "</B></FONT></TD>"
                    '    strMessage &= "<TD WIDTH='18%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 12, "Request Reason") & "</B></FONT></TD>"

                    '    If intStatusId <> 4 Then  'EXCEPT DELETE STATUS
                    '        strMessage &= "<TD WIDTH='15%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 13, "Actual Start Time") & "</B></FONT></TD>"
                    '        strMessage &= "<TD WIDTH='15%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 14, "Actual End Time") & "</B></FONT></TD>"
                    '        strMessage &= "<TD WIDTH='15%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 15, "Actual OT Hours") & "</B></FONT></TD>"
                    '        strMessage &= "<TD WIDTH='18%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 16, "Adjustment Reason") & "</B></FONT></TD>"
                    '    End If

                    '    strMessage &= "</TR>"


                    '    For Each row As DataRow In dsList.Tables(0).Rows
                    '        strMessage &= "<TR WIDTH='100%'>"
                    '        strMessage &= "<TD WIDTH='10%' ALIGN='LEFT'><FONT SIZE=2>" & CDate(row.Item("requestdate")).Date & "</FONT></TD>"
                    '        strMessage &= "<TD WIDTH='15%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("plannedstart_time").ToString & "</FONT></TD>"
                    '        strMessage &= "<TD WIDTH='15%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("plannedend_time").ToString & "</FONT></TD>"
                    '        strMessage &= "<TD WIDTH='15%' ALIGN='LEFT'><FONT SIZE=2>" & CalculateTime(True, row.Item("plannedot_hours").ToString).ToString("#00.00").Replace(".", ":") & "</FONT></TD>"
                    '        strMessage &= "<TD WIDTH='18%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("request_reason").ToString & "</FONT></TD>"

                    '        If intStatusId <> 4 Then  'EXCEPT DELETE STATUS
                    '            strMessage &= "<TD WIDTH='15%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("actualstart_time").ToString() & "</FONT></TD>"
                    '            strMessage &= "<TD WIDTH='15%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("actualend_time").ToString() & "</FONT></TD>"
                    '            strMessage &= "<TD WIDTH='15%' ALIGN='LEFT'><FONT SIZE=2>" & CalculateTime(True, row.Item("actualot_hours").ToString()).ToString("#00.00").Replace(".", ":") & "</FONT></TD>"
                    '            If intStatusId = 4 OrElse intStatusId = 6 Then   'FOR ONLY DELETED OR CANCELLED
                    '                strMessage &= "<TD WIDTH='18%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("adjustment_reason").ToString() & "</FONT></TD>"
                    '            Else
                    '                strMessage &= "<TD WIDTH='18%' ALIGN='LEFT'><FONT SIZE=2>" & strRemarks.Trim() & "</FONT></TD>"
                    '            End If
                    '        End If

                    '        strMessage &= "</TR>"
                    '    Next
                    '    strMessage &= "</TABLE>"

                    '    If (intStatusId = 4 OrElse intStatusId = 6) AndAlso strRemarks.Trim.Length > 0 Then
                    '        strMessage &= "<BR></BR><BR></BR>" & Language.getMessage(mstrModuleName, 21, "Please refer the below Remarks/Comments:") & "<BR></BR><BR></BR>" & _
                    '                      ChrW(34) & strRemarks & ChrW(34)
                    '    End If

                    'End If 

                    'Pinkal (14-Nov-2019) -- End

                    strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                    strMessage &= "</BODY></HTML>"

                    objMail._Message = strMessage
                    objMail._ToEmail = objEmp._Email


                    If intLoginTypeId <= 0 Then intLoginTypeId = enLogin_Mode.DESKTOP

                    If mstrWebFormName.Trim.Length > 0 Then
                        objMail._Form_Name = mstrWebFormName
                        objMail._WebClientIP = mstrWebClientIP
                        objMail._WebHostName = mstrWebHostName
                    End If
                    objMail._LogEmployeeUnkid = intLoginEmployeeId
                    objMail._OperationModeId = intLoginTypeId
                    objMail._UserUnkid = intUserId

                    objMail._SenderAddress = objEmp._Firstname & " " & objEmp._Surname

                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.OTREQUISITION_MGT
                    objMail.SendMail(intCompanyID)

                Next
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_Notification_Employee; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Pinkal (07-Nov-2019) -- End


    'Pinkal (29-Jan-2020) -- Start
    'Enhancement - Changes related To OT NMB Testing.
    'Public Function GetEmpTotalOThours(ByVal xPeriodId As Integer, ByVal mstrEmployeeIds As String, Optional ByVal objDoOperation As clsDataOperation = Nothing _
    '                                                  , Optional ByVal blnIncludeOnlyPendingHrs As Boolean = True, Optional ByVal blnIncludeOnlyApprovedHrs As Boolean = True) As Integer
    'Pinkal (29-Jan-2020) -- End

    Public Function GetEmpTotalOThours(ByVal xStartDate As Date, ByVal xEndDate As Date, ByVal mstrEmployeeIds As String, Optional ByVal objDoOperation As clsDataOperation = Nothing _
                                                        , Optional ByVal blnIncludeOnlyPendingHrs As Boolean = True, Optional ByVal blnIncludeOnlyApprovedHrs As Boolean = True) As Integer
        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mintTotalOTHours As Integer = 0

        Try

            If mstrEmployeeIds.Trim.Length <= 0 Then Return mintTotalOTHours

            If objDoOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDoOperation
            End If

            strQ = " SELECT SUM(ISNULL(A.totalot_hours,0)) AS TotalHrs " & _
                      " FROM ( "

            If blnIncludeOnlyPendingHrs Then
                strQ &= "                     SELECT SUM(ISNULL(plannedot_hours,0)) AS totalot_hours " & _
                          "                     FROM tnaot_requisition_tran " & _
                          "                     WHERE isvoid = 0 and issubmit_approval = 1 and statusunkid = 2  AND employeeunkid in (" & mstrEmployeeIds & ")  " & _
                          "                     AND CONVERT(CHAR(8),requestdate,112) BETWEEN @startdate AND @enddate "

            End If

            If blnIncludeOnlyPendingHrs And blnIncludeOnlyApprovedHrs Then
                strQ &= "                     UNION "
            End If

            If blnIncludeOnlyApprovedHrs Then
                strQ &= "                     SELECT SUM(ISNULL(actualot_hours,0)) AS totalot_hours  " & _
                              "                     FROM tnaot_requisition_tran " & _
                              "                     WHERE isvoid = 0 and issubmit_approval = 1 and statusunkid = 1   AND employeeunkid in (" & mstrEmployeeIds & ")  " & _
                              "                     AND CONVERT(CHAR(8),requestdate,112) BETWEEN @startdate AND @enddate"

            End If

            strQ &= ") AS A"

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xStartDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xEndDate))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintTotalOTHours = CInt(dsList.Tables(0).Rows(0)("TotalHrs"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmpTotalOThours; Module Name: " & mstrModuleName)
        End Try
        Return mintTotalOTHours
    End Function
    'Pinkal (23-Nov-2019) -- End


    'Pinkal (10-Jan-2020) -- Start
    'Enhancements -  Working on OT Requisistion for NMB.

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetApproverPendingOTRequisition(ByVal intApproverID As Integer, ByVal mstrEmpID As String, Optional ByVal mobjDataOperation As clsDataOperation = Nothing) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrFormIDs As String = String.Empty
        Try
            Dim objDataOperation As clsDataOperation = Nothing

            If mobjDataOperation IsNot Nothing Then
                objDataOperation = mobjDataOperation
            Else
                objDataOperation = New clsDataOperation
            End If

            objDataOperation.ClearParameters()


            strQ = " SELECT ISNULL(STUFF( " & _
                      " (SELECT  DISTINCT ',' +  CAST(tnaot_requisition_tran.otrequisitiontranunkid AS NVARCHAR(max)) " & _
                      "   FROM tnaot_requisition_tran " & _
                      "   JOIN tnaotrequisition_approval_tran ON tnaot_requisition_tran.otrequisitiontranunkid = tnaotrequisition_approval_tran.otrequisitiontranunkid AND tnaotrequisition_approval_tran.tnamappingunkid  = " & intApproverID & _
                      "  WHERE tnaot_requisition_tran.statusunkid in (2)  AND tnaot_requisition_tran.isvoid = 0  AND tnaotrequisition_approval_tran.isvoid = 0 "

            If mstrEmpID.Trim.Length > 0 Then
                strQ &= " AND tnaot_requisition_tran.employeeunkid IN (" & mstrEmpID & ") "
            End If

            strQ &= "  FOR XML PATH('')),1,1,''),'') AS CSV"

            dsList = objDataOperation.ExecQuery(strQ, "FormList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrFormIDs = dsList.Tables(0).Rows(0)("CSV").ToString()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverPendingOTRequisition; Module Name: " & mstrModuleName)
        End Try
        Return mstrFormIDs
    End Function

    'Pinkal (10-Jan-2020) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "  Select")
            Language.setMessage(mstrModuleName, 2, "OT Requisition already exists for requested date.Please define new OT Requisition for other date.")
            Language.setMessage(mstrModuleName, 3, "Notification for Approving OT Requisition")
            Language.setMessage(mstrModuleName, 4, "Dear")
            Language.setMessage(mstrModuleName, 5, "This is the notification for approving the Employee OT Requisition for period")
            Language.setMessage(mstrModuleName, 6, "Please click on the following link to approve.")
            Language.setMessage(mstrModuleName, 7, "Please refer the following OT Requisition Details.")
            Language.setMessage(mstrModuleName, 8, "Request Date")
            Language.setMessage(mstrModuleName, 9, "Planned Start Time")
            Language.setMessage(mstrModuleName, 10, "Planned End Time")
            Language.setMessage(mstrModuleName, 11, "Planned OT Hours")
            Language.setMessage(mstrModuleName, 12, "Request Reason")
            Language.setMessage(mstrModuleName, 13, "Actual Start Time")
            Language.setMessage(mstrModuleName, 14, "Actual End Time")
            Language.setMessage(mstrModuleName, 15, "Actual OT Hours")
            Language.setMessage(mstrModuleName, 16, "Adjustment Reason")
            Language.setMessage(mstrModuleName, 17, "OT Requisition Status Notification")
            Language.setMessage(mstrModuleName, 18, "This is to inform you that the OT Requisition that was submitted for period")
            Language.setMessage(mstrModuleName, 19, "has been")
            Language.setMessage(mstrModuleName, 20, "Deleted")
            Language.setMessage(mstrModuleName, 21, "Please refer the below Remarks/Comments:")
            Language.setMessage("clsMasterData", 111, "Pending")
            Language.setMessage("clsMasterData", 112, "Rejected")
            Language.setMessage("clsMasterData", 276, "Issued")
            Language.setMessage("clsMasterData", 519, "Approved")
            Language.setMessage("clsMasterData", 522, "Cancel")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class