﻿'************************************************************************************************************************************
'Class Name : clsInterestRate_tran.vb
'Purpose    :
'Date       :10/07/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************


Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Anjan
''' </summary>

Public Class clsInterestRate_tran

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "clsInterestRate_tran"
    Dim mstrmessage As String = ""
    Dim objDataOperation As clsDataOperation
    Private mintSavingSchemeUnkid As Integer = -1
    Private mdtTran As DataTable
    Private mdblInterestRate As Double = 0


    'Pinkal (24-Jul-2012) -- Start
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'Pinkal (24-Jul-2012) -- End

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

#Region " Properties "
    Public Property _SavingScheme() As Integer
        Get
            Return mintSavingSchemeUnkid

        End Get
        Set(ByVal value As Integer)
            mintSavingSchemeUnkid = value
            Call GetInterestRate_tran()
        End Set
    End Property

    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public ReadOnly Property _Rate()
        Get
            Return mdblInterestRate
        End Get
    End Property


    'Pinkal (24-Jul-2012) -- Start
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    'Pinkal (24-Jul-2012) -- End

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("InterestRateTran")
        Dim dCol As DataColumn

        Try
            dCol = New DataColumn("savinginteresttranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("savingschemeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)


            dCol = New DataColumn("yearsupto")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("interestrate")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("userunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)


            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Methods "

    Private Sub GetInterestRate_tran()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim dRowID_Tran As DataRow
        Dim exForce As Exception

        Try
            objDataOperation = New clsDataOperation


            strQ = "SELECT  savinginteresttranunkid " & _
                           " , savingschemeunkid " & _
                           " , yearsupto " & _
                           " , interestrate " & _
                           " , userunkid " & _
                           " , isvoid " & _
                           " , voiduserunkid " & _
                           " , voiddatetime " & _
                           " , '' AUD " & _
                    "FROM svinterest_tran " & _
                    "WHERE savingschemeunkid = @savingschemeunkid "

            objDataOperation.AddParameter("@savingschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingSchemeUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    dRowID_Tran = mdtTran.NewRow()

                    dRowID_Tran.Item("savinginteresttranunkid") = .Item("savinginteresttranunkid")
                    dRowID_Tran.Item("savingschemeunkid") = .Item("savingschemeunkid")
                    dRowID_Tran.Item("yearsupto") = .Item("yearsupto")
                    dRowID_Tran.Item("interestrate") = .Item("interestrate")
                    dRowID_Tran.Item("userunkid") = .Item("userunkid")
                    dRowID_Tran.Item("isvoid") = .Item("isvoid")
                    dRowID_Tran.Item("voiduserunkid") = .Item("voiduserunkid")
                    dRowID_Tran.Item("voiddatetime") = .Item("voiddatetime")
                    dRowID_Tran.Item("AUD") = .Item("AUD")

                    mdtTran.Rows.Add(dRowID_Tran)
                End With
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetRedemption_tran", mstrModuleName)
        End Try
    End Sub

    Public Function InsertUpdateDelete_InterestRateTran(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            ' objDataOperation = New clsDataOperation
            ' objDataOperation.BindTransaction()
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO svinterest_tran ( " & _
                                             "  savingschemeunkid " & _
                                             ", yearsupto " & _
                                             ", interestrate " & _
                                             ", userunkid " & _
                                             ", isvoid " & _
                                             ", voiduserunkid " & _
                                             ", voiddatetime" & _
                                           ") VALUES (" & _
                                             "  @savingschemeunkid " & _
                                             ", @yearsupto " & _
                                             ", @interestrate " & _
                                             ", @userunkid " & _
                                             ", @isvoid " & _
                                             ", @voiduserunkid " & _
                                             ", @voiddatetime" & _
                                           "); SELECT @@identity"


                                objDataOperation.AddParameter("@savingschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingSchemeUnkid.ToString)
                                objDataOperation.AddParameter("@yearsupto", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("yearsupto").ToString)
                                objDataOperation.AddParameter("@interestrate", SqlDbType.Float, eZeeDataType.MONEY_SIZE, .Item("interestrate").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)

                                If .Item("voiddatetime").ToString = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If


                                'Pinkal (24-Jul-2012) -- Start
                                'Enhancement : TRA Changes

                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                Dim mintSavinginteresttranunkid As Integer = dsList.Tables(0).Rows(0)(0)

                                If InsertAuditSavingInterest(objDataOperation, mdtTran.Rows(i), 1, mintSavinginteresttranunkid) = False Then
                                    Return False
                                End If

                                'Pinkal (24-Jul-2012) -- End


                            Case "U"
                                strQ = "UPDATE svinterest_tran SET " & _
                                         "  savingschemeunkid = @savingschemeunkid" & _
                                         ", yearsupto = @yearsupto" & _
                                         ", interestrate = @interestrate" & _
                                         ", userunkid = @userunkid " & _
                                        "WHERE savinginteresttranunkid = @savinginteresttranunkid "

                                objDataOperation.AddParameter("@savinginteresttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("savinginteresttranunkid").ToString)
                                objDataOperation.AddParameter("@savingschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("savingschemeunkid").ToString)
                                objDataOperation.AddParameter("@yearsupto", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("yearsupto").ToString)
                                objDataOperation.AddParameter("@interestrate", SqlDbType.Float, eZeeDataType.MONEY_SIZE, .Item("interestrate").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                'objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)

                                'If .Item("voiddatetime").ToString = Nothing Then
                                '    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                'Else
                                '    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                'End If
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'Pinkal (24-Jul-2012) -- Start
                                'Enhancement : TRA Changes

                                mintSavingSchemeUnkid = CInt(.Item("savingschemeunkid"))

                                If InsertAuditSavingInterest(objDataOperation, mdtTran.Rows(i), 2, CInt(.Item("savinginteresttranunkid"))) = False Then
                                    Return False
                                End If

                                'Pinkal (24-Jul-2012) -- End

                            Case "D"


                                'Pinkal (24-Jul-2012) -- Start
                                'Enhancement : TRA Changes

                                mintSavingSchemeUnkid = CInt(.Item("savingschemeunkid"))

                                If InsertAuditSavingInterest(objDataOperation, mdtTran.Rows(i), 3, CInt(.Item("savinginteresttranunkid"))) = False Then
                                    Return False
                                End If

                                'Pinkal (24-Jul-2012) -- End

                                strQ = "DELETE FROM svinterest_tran " & _
                                            "WHERE savinginteresttranunkid = @savinginteresttranunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@savinginteresttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("savinginteresttranunkid").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next
            'objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            'objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_RedemptionTran", mstrModuleName)
            Return False
        End Try
    End Function

    'Sohail (26 Jul 2010) -- Start
    'Public Function GetInterestRate(ByVal intSavingScheme As Integer, ByVal intYear As Double) As Boolean

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function GetInterestRate(ByVal intSavingScheme As Integer, ByVal intDays As Integer) As Boolean
    Public Function GetInterestRate(ByVal intSavingScheme As Integer, ByVal intDays As Integer, ByVal xDatabaseStartDate As Date, ByVal xDatabaseEndDate As Date) As Boolean
        'Shani(24-Aug-2015) -- End

        'Sohail (26 Jul 2010) -- End

        Dim exForce As Exception
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing

        Dim dblYear As Double = 0
        Dim intDaysInYear As Integer = 0
        'Dim objCompany As New clsCompany_Master 'Sohail (18 Dec 2010)

        Try

            objDataOperation = New clsDataOperation

            'Sandeep [ 29 NOV 2010 ] -- Start
            'objCompany.GetFinancialDate(1)
            'objCompany.GetFinancialDate(Company._Object._Companyunkid) 'Sohail (18 Dec 2010)
            'Sandeep [ 29 NOV 2010 ] -- End 
            'Sohail (18 Dec 2010) -- Start
            'intDaysInYear = DateDiff(DateInterval.Day, objCompany._Financial_Start_Date, objCompany._Financial_End_Date) + 1

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'intDaysInYear = DateDiff(DateInterval.Day, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date) + 1
            intDaysInYear = DateDiff(DateInterval.Day, xDatabaseStartDate, xDatabaseEndDate) + 1
            'Shani(24-Aug-2015) -- End

            'Sohail (18 Dec 2010) -- End


            dblYear = intDays / intDaysInYear


            strQ = " SELECT TOP 1 " & _
                            "  svinterest_tran.savingschemeunkid " & _
                            ", svinterest_tran.yearsupto " & _
                            ", svinterest_tran.interestrate " & _
                    "FROM svinterest_tran " & _
                    "WHERE savingschemeunkid = @savingscheme " & _
                    "AND yearsupto >= @yearupto "

            objDataOperation.AddParameter("@savingscheme", SqlDbType.Int, eZeeDataType.INT_SIZE, intSavingScheme)
            objDataOperation.AddParameter("@yearupto", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, dblYear)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mdblInterestRate = dsList.Tables(0).Rows(0).Item(2)
            End If

            'Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetInterestRate; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
            'objCompany = Nothing 'Sohail (18 Dec 2010)
        End Try
    End Function

    'Sohail (26 Jul 2010) -- Start
    ''' <summary>
    ''' Get Interest Amount on the amount for the given rate and for the no. of days
    ''' Modify By : Sohail
    ''' </summary>
    ''' <param name="dblAmount"></param>
    ''' <param name="dblInterestRate"></param>
    ''' <param name="intDays"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function GetInterestAmount(ByVal dblAmount As Double, ByVal dblInterestRate As Double, ByVal intDays As Integer) As Double
    Public Function GetInterestAmount(ByVal dblAmount As Double, ByVal dblInterestRate As Double, ByVal intDays As Integer, ByVal xDatabaseStartDate As Date, ByVal xdatabaseEndDate As Date) As Double
        'Shani(24-Aug-2015) -- End
        Dim exForce As Exception
        'Dim objCompany As New clsCompany_Master 'Sohail (18 Dec 2010)
        Dim intDaysInYear As Integer = 0
        Dim dblInterestAmount As Double = 0

        Try
            'Sandeep [ 29 NOV 2010 ] -- Start
            'objCompany.GetFinancialDate(1)
            'objCompany.GetFinancialDate(Company._Object._Companyunkid)'Sohail (18 Dec 2010)
            'Sandeep [ 29 NOV 2010 ] -- End 

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'intDaysInYear = DateDiff(DateInterval.Day, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date) + 1 'Sohail (18 Dec 2010)
            intDaysInYear = DateDiff(DateInterval.Day, xDatabaseStartDate, xdatabaseEndDate) + 1
            'Shani(24-Aug-2015) -- End


            dblInterestAmount = (dblAmount * dblInterestRate * intDays) / (100 * intDaysInYear)

            
            Return dblInterestAmount
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetInterestAmount; Module Name: " & mstrModuleName)
            Return 0
        Finally
            exForce = Nothing
            'objCompany = Nothing 'Sohail (18 Dec 2010)
        End Try
    End Function
    'Sohail (26 Jul 2010) -- End



    'Pinkal (24-Jul-2012) -- Start
    'Enhancement : TRA Changes

    Public Function InsertAuditSavingInterest(ByVal objDataOperation As clsDataOperation, ByVal dr As DataRow, ByVal AuditType As Integer, ByVal mintSavinginteresttranunkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception = Nothing
        Try
            strQ = "INSERT INTO atsvinterest_tran ( " & _
                                          " savinginteresttranunkid " & _
                                          ",savingschemeunkid " & _
                                          ", yearsupto " & _
                                          ", interestrate " & _
                                          ", audittype " & _
                                          ", audituserunkid " & _
                                          ", auditdatetime " & _
                                          ", ip" & _
                                          ", machine_name " & _
                                          ", form_name " & _
                                          ", module_name1 " & _
                                          ", module_name2 " & _
                                          ", module_name3 " & _
                                          ", module_name4 " & _
                                          ", module_name5 " & _
                                          ", isweb " & _
                                        ") VALUES (" & _
                                          "  @savinginteresttranunkid " & _
                                          ", @savingschemeunkid " & _
                                          ", @yearsupto " & _
                                          ", @interestrate " & _
                                          ", @audittype " & _
                                          ", @audituserunkid " & _
                                          ", @auditdatetime " & _
                                          ", @ip" & _
                                          ", @machine_name " & _
                                          ", @form_name " & _
                                          ", @module_name1 " & _
                                          ", @module_name2 " & _
                                          ", @module_name3 " & _
                                          ", @module_name4 " & _
                                          ", @module_name5 " & _
                                          ", @isweb " & _
                                        "); SELECT @@identity"


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@savingschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingSchemeUnkid)
            objDataOperation.AddParameter("@savinginteresttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavinginteresttranunkid)
            objDataOperation.AddParameter("@yearsupto", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("yearsupto").ToString)
            objDataOperation.AddParameter("@interestrate", SqlDbType.Float, eZeeDataType.MONEY_SIZE, dr("interestrate").ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("userunkid"))
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)

            If mstrWebFormName.Trim.Length <= 0 Then
                'S.SANDEEP [ 11 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim frm As Form
                'For Each frm In Application.OpenForms
                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                '    End If
                'Next
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                'S.SANDEEP [ 11 AUG 2012 ] -- END
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditSavingInterest", mstrModuleName)
            Return False
        End Try
    End Function

    'Pinkal (24-Jul-2012) -- End


#End Region
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
