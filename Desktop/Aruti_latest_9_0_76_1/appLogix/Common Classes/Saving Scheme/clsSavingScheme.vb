﻿'************************************************************************************************************************************
'Class Name : clsSavingScheme.vb
'Purpose    :
'Date       :08/07/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Anjan
''' </summary>
Public Class clsSavingScheme
    Private Shared ReadOnly mstrModuleName As String = "clsSavingScheme"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintSavingschemeunkid As Integer
    Private mstrSavingschemecode As String = String.Empty
    Private mstrSavingschemename As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mdecMincontribution As Decimal 'Sohail (11 May 2011)
    Private mdecMinnetsalary As Decimal 'Sohail (11 May 2011)
    'Private mintMintenure As Integer
    Private mdecMaxwithdrawal_Year As Decimal 'Sohail (11 May 2011)
    Private mdecMaxwithdrawal_Amount As Decimal 'Sohail (11 May 2011)
    Private mdecAdditionalwithdrawal_Charge As Decimal 'Sohail (11 May 2011)
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    'Private objRedemptionTrans As New clsRedemption_tran
    Private objInterestRateTrans As New clsInterestRate_tran
    Private mstrVoidReason As String = String.Empty
    'Sohail (14 Mar 2019) -- Start
    'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
    Private mintCostcenterunkid As Integer = 0
    'Sohail (14 Mar 2019) -- End

    'SHANI [12 JAN 2015]-START
    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
    Private mdecDefaultContribution As Decimal
    Private mdecDefaultIntRate As Decimal
    'SHANI [12 JAN 2015]--END 

    
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Anjan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set savingschemeunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Savingschemeunkid() As Integer
        Get
            Return mintSavingschemeunkid
        End Get
        Set(ByVal value As Integer)
            mintSavingschemeunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set savingschemecode
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Savingschemecode() As String
        Get
            Return mstrSavingschemecode
        End Get
        Set(ByVal value As String)
            mstrSavingschemecode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set savingschemename
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Savingschemename() As String
        Get
            Return mstrSavingschemename
        End Get
        Set(ByVal value As String)
            mstrSavingschemename = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mincontribution
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Mincontribution() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecMincontribution
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecMincontribution = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set minnetsalary
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Minnetsalary() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecMinnetsalary
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecMinnetsalary = value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set mintenure
    '''' Modify By: Anjan
    '''' </summary>
    'Public Property _Mintenure() As Integer
    '    Get
    '        Return mintMintenure
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintMintenure = Value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set maxwithdrawal_year
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Maxwithdrawal_Year() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecMaxwithdrawal_Year
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecMaxwithdrawal_Year = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set maxwithdrawal_amount
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Maxwithdrawal_Amount() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecMaxwithdrawal_Amount
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecMaxwithdrawal_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set additionalwithdrawal_charge
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Additionalwithdrawal_Charge() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecAdditionalwithdrawal_Charge
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecAdditionalwithdrawal_Charge = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set VoidReason
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _VoidReason() As String
        Get
            Return mstrVoidReason
        End Get
        Set(ByVal value As String)
            mstrVoidReason = value
        End Set
    End Property

    'SHANI [12 JAN 2015]-START
    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
    ''' <summary>
    ''' Purpose: Get or Set Defualt Contribution
    ''' Modify By: Shani
    ''' </summary>
    Public Property _DefualtContribution() As Decimal
        Get
            Return mdecDefaultContribution
        End Get
        Set(ByVal value As Decimal)
            mdecDefaultContribution = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Defualt Interest
    ''' Modify By: Shani
    ''' </summary>
    Public Property _DefualtIntRate() As Decimal
        Get
            Return mdecDefaultIntRate
        End Get
        Set(ByVal value As Decimal)
            mdecDefaultIntRate = value
        End Set
    End Property
    'SHANI [12 JAN 2015]-END

    'Sohail (14 Mar 2019) -- Start
    'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
    Public Property _Costcenterunkid() As Integer
        Get
            Return mintCostcenterunkid
        End Get
        Set(ByVal value As Integer)
            mintCostcenterunkid = value
        End Set
    End Property
    'Sohail (14 Mar 2019) -- End

#End Region

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  savingschemeunkid " & _
              ", savingschemecode " & _
              ", savingschemename " & _
              ", description " & _
              ", mincontribution " & _
              ", minnetsalary " & _
              ", maxwithdrawal_year " & _
              ", maxwithdrawal_amount " & _
              ", additionalwithdrawal_charge " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(default_contribution, 0) AS default_contribution " & _
              ", ISNULL(default_interest_rate, 0) AS default_interest_rate " & _
              ", ISNULL(costcenterunkid, 0) AS costcenterunkid " & _
             "FROM svsavingscheme_master " & _
             "WHERE savingschemeunkid = @savingschemeunkid "
            'Sohail (14 Mar 2019) - [costcenterunkid]
            'SHANI [12 JAN 2015]-- [default_contribution, default_interest_rate]

            objDataOperation.AddParameter("@savingschemeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintSavingschemeUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintsavingschemeunkid = CInt(dtRow.Item("savingschemeunkid"))
                mstrsavingschemecode = dtRow.Item("savingschemecode").ToString
                mstrsavingschemename = dtRow.Item("savingschemename").ToString
                mstrdescription = dtRow.Item("description").ToString
                mdecMincontribution = CDec(dtRow.Item("mincontribution")) 'Sohail (11 May 2011)
                mdecMinnetsalary = CDec(dtRow.Item("minnetsalary")) 'Sohail (11 May 2011)
                'mintmintenure = CInt(dtRow.Item("mintenure"))
                mdecMaxwithdrawal_Year = CDec(dtRow.Item("maxwithdrawal_year")) 'Sohail (11 May 2011)
                mdecMaxwithdrawal_Amount = CDec(dtRow.Item("maxwithdrawal_amount")) 'Sohail (11 May 2011)
                mdecAdditionalwithdrawal_Charge = CDec(dtRow.Item("additionalwithdrawal_charge")) 'Sohail (11 May 2011)
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mstrVoidReason = dtRow.Item("voidreason")

                'SHANI [12 JAN 2015]-START
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                mdecDefaultContribution = CDec(dtRow.Item("default_contribution"))
                mdecDefaultIntRate = CDec(dtRow.Item("default_interest_rate"))
                'SHANI [12 JAN 2015]--END 
                'Sohail (14 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
                mintCostcenterunkid = Convert.ToInt32(dtRow.Item("costcenterunkid"))
                'Sohail (14 Mar 2019) -- End

                Exit For

            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  savingschemeunkid " & _
              ", savingschemecode " & _
              ", savingschemename " & _
              ", description " & _
              ", mincontribution " & _
              ", minnetsalary " & _
              ", maxwithdrawal_year " & _
              ", maxwithdrawal_amount " & _
              ", additionalwithdrawal_charge " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(default_contribution, 0) AS default_contribution " & _
              ", ISNULL(default_interest_rate, 0) AS default_interest_rate " & _
              ", ISNULL(costcenterunkid, 0) AS tranheadunkid " & _
             "FROM svsavingscheme_master " & _
             " WHERE Isvoid=0 "
            'Sohail (14 Mar 2019) - [costcenterunkid]
            'SHANI [12 JAN 2015]-- [default_contribution, default_interest_rate]
           

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (svsavingscheme_master) </purpose>
    Public Function Insert(Optional ByVal dtRedemptionTran As DataTable = Nothing, Optional ByVal dtInterestTran As DataTable = Nothing) As Boolean

        If isExist(mstrSavingschemecode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
            Return False
        End If

        If isExist(, mstrSavingschemename) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation


        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 


        Try
            objDataOperation.AddParameter("@savingschemecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSavingschemecode.ToString)
            objDataOperation.AddParameter("@savingschemename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSavingschemename.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@mincontribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMincontribution.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@minnetsalary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMinnetsalary.ToString) 'Sohail (11 May 2011)
            'objDataOperation.AddParameter("@mintenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMintenure.ToString)
            objDataOperation.AddParameter("@maxwithdrawal_year", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaxwithdrawal_Year.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@maxwithdrawal_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaxwithdrawal_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@additionalwithdrawal_charge", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAdditionalwithdrawal_Charge.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason.ToString)
            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            objDataOperation.AddParameter("@defaultContribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDefaultContribution.ToString)
            objDataOperation.AddParameter("@defaultIntRate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDefaultIntRate.ToString)
            'SHANI [12 JAN 2015]--END
            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid)
            'Sohail (14 Mar 2019) -- End

            strQ = "INSERT INTO svsavingscheme_master ( " & _
              "  savingschemecode " & _
              ", savingschemename " & _
              ", description " & _
              ", mincontribution " & _
              ", minnetsalary " & _
              ", maxwithdrawal_year " & _
              ", maxwithdrawal_amount " & _
              ", additionalwithdrawal_charge " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", default_contribution " & _
              ", default_interest_rate " & _
              ", costcenterunkid " & _
            ") VALUES (" & _
              "  @savingschemecode " & _
              ", @savingschemename " & _
              ", @description " & _
              ", @mincontribution " & _
              ", @minnetsalary " & _
              ", @maxwithdrawal_year " & _
              ", @maxwithdrawal_amount " & _
              ", @additionalwithdrawal_charge " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @defaultContribution " & _
              ", @defaultIntRate " & _
              ", @costcenterunkid " & _
            "); SELECT @@identity"
            'Sohail (14 Mar 2019) - [costcenterunkid]
            'SHANI [12 JAN 2015]-- [default_contribution, default_interest_rate]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintSavingschemeunkid = dsList.Tables(0).Rows(0).Item(0)

            'If dtRedemptionTran IsNot Nothing Then
            '    objRedemptionTrans._SavingScheme = mintSavingschemeunkid
            '    objRedemptionTrans._DataList = dtRedemptionTran
            '    objRedemptionTrans.InsertUpdateDelete_RedemptionTran()
            'End If

            If dtInterestTran IsNot Nothing Then
                objInterestRateTrans._SavingScheme = mintSavingschemeunkid
                objInterestRateTrans._DataList = dtInterestTran

                'Pinkal (24-Jul-2012) -- Start
                'Enhancement : TRA Changes
                'objInterestRateTrans.InsertUpdateDelete_InterestRateTran()
                If objInterestRateTrans.InsertUpdateDelete_InterestRateTran(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'Pinkal (24-Jul-2012) -- End

            End If


            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "svsavingscheme_master", "savingschemeunkid", mintSavingschemeunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (svsavingscheme_master) </purpose>
    Public Function Update(Optional ByVal dtRedemptionTran As DataTable = Nothing, Optional ByVal dtInterestTran As DataTable = Nothing) As Boolean
        If isExist(, mstrSavingschemename, mintSavingschemeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Saving Scheme is already defined. Please define new Saving Scheme.")
            Return False
        End If

        If isExist(mstrSavingschemecode, , mintSavingschemeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "This Saving Scheme Code is already defined. Please define new Saving Scheme Code.")
            Return False
        End If



        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 


        Try
            objDataOperation.AddParameter("@savingschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSavingschemeunkid.ToString)
            objDataOperation.AddParameter("@savingschemecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSavingschemecode.ToString)
            objDataOperation.AddParameter("@savingschemename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSavingschemename.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@mincontribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMincontribution.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@minnetsalary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMinnetsalary.ToString) 'Sohail (11 May 2011)
            'objDataOperation.AddParameter("@mintenure", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMintenure.ToString)
            objDataOperation.AddParameter("@maxwithdrawal_year", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaxwithdrawal_Year.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@maxwithdrawal_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaxwithdrawal_Amount.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@additionalwithdrawal_charge", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAdditionalwithdrawal_Charge.ToString) 'Sohail (11 May 2011)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason.ToString)

            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            objDataOperation.AddParameter("@defaultContribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDefaultContribution.ToString)
            objDataOperation.AddParameter("@defaultIntRate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDefaultIntRate.ToString)
            'SHANI [12 JAN 2015]--END
            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid)
            'Sohail (14 Mar 2019) -- End

            strQ = "UPDATE svsavingscheme_master SET " & _
              "  savingschemecode = @savingschemecode" & _
              ", savingschemename = @savingschemename" & _
              ", description = @description" & _
              ", mincontribution = @mincontribution" & _
              ", minnetsalary = @minnetsalary" & _
              ", maxwithdrawal_year = @maxwithdrawal_year" & _
              ", maxwithdrawal_amount = @maxwithdrawal_amount" & _
              ", additionalwithdrawal_charge = @additionalwithdrawal_charge" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
              ", default_contribution = @defaultContribution " & _
              ", default_interest_rate = @defaultIntRate " & _
              ", costcenterunkid = @costcenterunkid " & _
            "WHERE savingschemeunkid = @savingschemeunkid "
            'Sohail (14 Mar 2019) - [costcenterunkid]
            'SHANI [12 JAN 2015]-- [default_contribution, default_interest_rate]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If dtRedemptionTran IsNot Nothing Then
            '    objRedemptionTrans._SavingScheme = mintSavingschemeunkid
            '    objRedemptionTrans._DataList = dtRedemptionTran
            '    objRedemptionTrans.InsertUpdateDelete_RedemptionTran()
            'End If

            If dtInterestTran IsNot Nothing Then
                objInterestRateTrans._SavingScheme = mintSavingschemeunkid
                objInterestRateTrans._DataList = dtInterestTran

                'Pinkal (24-Jul-2012) -- Start
                'Enhancement : TRA Changes
                'objInterestRateTrans.InsertUpdateDelete_InterestRateTran()
                If objInterestRateTrans.InsertUpdateDelete_InterestRateTran(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'Pinkal (24-Jul-2012) -- End

            End If


            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "svsavingscheme_master", mintSavingschemeunkid, "savingschemeunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "svsavingscheme_master", "savingschemeunkid", mintSavingschemeunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 


            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (svsavingscheme_master) </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function Delete(ByVal intUnkid As Integer) As Boolean
    Public Function Delete(ByVal intUnkid As Integer, ByVal xUserUnkId As Integer) As Boolean
        'Shani(24-Aug-2015) -- End

        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 


        Try
            '<To Do> voiduseunkid has to be set with the logged in user.

            strQ = "UPDATE svsavingscheme_master SET " & _
                    " isvoid = 1 " & _
                    ", voiduserunkid = @voiduserunkid " & _
                    ", voiddatetime = @voiddatetime  " & _
                    ", voidreason = @voidreason " & _
            "WHERE savingschemeunkid = @savingschemeunkid "

            objDataOperation.AddParameter("@savingschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.SmallDateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason.ToString)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserUnkId)
            'Shani(24-Aug-2015) -- End



            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "svsavingscheme_master", "savingschemeunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 


            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                     "TABLE_NAME AS TableName " & _
                     ",COLUMN_NAME " & _
                     "FROM INFORMATION_SCHEMA.COLUMNS " & _
                     "WHERE COLUMN_NAME IN ('savingschemeunkid') "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@savingschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("TableName") = "svsavingscheme_master" Then Continue For
                strQ = "SELECT " & dtRow.Item("COLUMN_NAME") & " FROM " & dtRow.Item("TableName") & " WHERE " & dtRow.Item("COLUMN_NAME") & " = @savingschemeunkid "

                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            'Sohail (14 Nov 2011) -- Start
            If blnIsUsed = False Then
                strQ = "SELECT  tranheadunkid " & _
                    "FROM    praccount_configuration " & _
                    "WHERE   isactive = 1 " & _
                            "AND transactiontype_Id = " & enJVTransactionType.SAVINGS & " " & _
                            "AND tranheadunkid = @savingschemeunkid " & _
                    "UNION " & _
                    "SELECT  tranheadunkid " & _
                    "FROM    praccount_configuration_employee " & _
                    "WHERE   isactive = 1 " & _
                            "AND transactiontype_Id = " & enJVTransactionType.SAVINGS & " " & _
                            "AND tranheadunkid = @savingschemeunkid " & _
                    "UNION " & _
                    "SELECT  tranheadunkid " & _
                    "FROM    praccount_configuration_costcenter " & _
                    "WHERE   isactive = 1 " & _
                            "AND transactiontype_Id = " & enJVTransactionType.SAVINGS & " " & _
                            "AND tranheadunkid = @savingschemeunkid "

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                blnIsUsed = dsList.Tables("List").Rows.Count > 0
            End If
            'Sohail (14 Nov 2011) -- End

            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1, Optional ByRef intId As Integer = 0) As Boolean
        'Sohail (02 Aug 2017) - [intId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            intId = 0 'Sohail (02 Aug 2017)

            strQ = "SELECT " & _
              "  savingschemeunkid " & _
              ", savingschemecode " & _
              ", savingschemename " & _
              ", description " & _
              ", mincontribution " & _
              ", minnetsalary " & _
              ", maxwithdrawal_year " & _
              ", maxwithdrawal_amount " & _
              ", additionalwithdrawal_charge " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", default_contribution " & _
              ", default_interest_rate " & _
             "FROM svsavingscheme_master " & _
             "WHERE 1=1 "
            'SHANI [12 JAN 2015]-- [default_contribution, default_interest_rate]

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'Sohail (01 Mar 2018) -- Start
            'Issue : Head Code not found for Saving scheme account mappping on Import Empoyee Account Configuration in 70.1.
            'strQ &= " AND isvoid = 1 "
            strQ &= " AND isvoid = 0 "
            'Sohail (01 Mar 2018) -- End
            'Anjan (10 Feb 2012)-End 

            If strCode.Length > 0 Then
                strQ &= " AND savingschemecode = @code "
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If

            If strName.Length > 0 Then
                strQ &= " AND savingschemename = @name "
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If intUnkid > 0 Then
                strQ &= " AND savingschemeunkid <> @savingschemeunkid "
                objDataOperation.AddParameter("@savingschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (02 Aug 2017) -- Start
            'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
            If dsList.Tables(0).Rows.Count > 0 Then
                intId = CInt(dsList.Tables(0).Rows(0).Item("savingschemeunkid"))
            End If
            'Sohail (02 Aug 2017) -- End

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    '''  Modify By: Anjan
    ''' </summary>
    ''' <param name="blnNA"></param>
    ''' <param name="strListName"></param>
    ''' <param name="intLanguageID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getComboList(Optional ByVal blnNA As Boolean = False, _
                                    Optional ByVal strListName As String = "List", _
                                      Optional ByVal intLanguageID As Integer = -1, _
                                      Optional ByVal strFilter As String = "", _
                                 Optional ByVal strOrderByQuery As String = "") As DataSet
        'Hemant (23 May 2019) -- [strOrderByQuery] 
        'Sohail (03 Feb 2016) - [strFilter]

        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation

        Try

            If blnNA Then
                strQ = "SELECT 0 AS savingschemeunkid " & _
                          ",  ' ' + @Select AS name " & _
                          ",  ' ' as code " & _
                       "UNION "

                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Select"))

            End If

            strQ &= "SELECT savingschemeunkid " & _
                        ", savingschemename AS name " & _
                        ", savingschemecode as code " & _
                     "FROM svsavingscheme_master " & _
                     "WHERE isvoid = 0 "

            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'strQ &= "ORDER BY  name "
            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If
            'Hemant (23 May 2019) -- Start
            'Support Issue Id # 0003829(KCMU) : Allow the payroll transaction heads to be arranged due to Transaction head code on payroll reports.
            'strQ &= " ORDER BY  name "
            If strOrderByQuery.Trim <> "" Then
                strQ &= strOrderByQuery
            Else
            strQ &= " ORDER BY  name "
            End If
            'Hemant (23 May 2019) -- End
            'Sohail (03 Feb 2016) -- End

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function

    'Sohail (14 Nov 2011) -- Start
    Public Function GetSavingSchemeIDUsedInJV(ByVal enAccConfigType As enAccountConfigType, ByVal dtAsOnDate As Date) As String
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( "
            'Sohail (25 Jul 2020) -- End

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_employee" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_costcenter" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.SAVINGS)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
            Else
                strIDs = "-999"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSavingSchemeIDUsedInJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function

    Public Function GetSavingSchemeIDUsedInOtherJV(ByVal enAccConfigType As enAccountConfigType, ByVal dtAsOnDate As Date) As String
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( "
            'Sohail (25 Jul 2020) -- End

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration_costcenter"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_costcenter"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.SAVINGS)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
            Else
                strIDs = "-999"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSavingSchemeIDUsedInOtherJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function
    'Sohail (14 Nov 2011) -- End

    'S.SANDEEP [ 29 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES 
    'TYPE : EMPLOYEMENT CONTRACT PROCESS
    Public Function GetSavingSchemeUnkid(ByVal mstrSavingName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ &= "SELECT savingschemeunkid FROM svsavingscheme_master WHERE savingschemename = @name "

            'S.SANDEEP [19 AUG 2016] -- START
            'ISSUE : ISACTIVE WAS NOT KEPT FOR CHECKING
            strQ &= " AND isvoid = 0 "
            'S.SANDEEP [19 AUG 2016] -- START

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSavingName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0).Item("savingschemeunkid")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSavingSchemeUnkid; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 29 DEC 2011 ] -- END


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Code is already defined. Please define new Code.")
			Language.setMessage(mstrModuleName, 2, "This Name is already defined. Please define new Name.")
			Language.setMessage(mstrModuleName, 3, "This Saving Scheme is already defined. Please define new Saving Scheme.")
			Language.setMessage(mstrModuleName, 4, "This Saving Scheme Code is already defined. Please define new Saving Scheme Code.")
			Language.setMessage(mstrModuleName, 5, "Select")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class