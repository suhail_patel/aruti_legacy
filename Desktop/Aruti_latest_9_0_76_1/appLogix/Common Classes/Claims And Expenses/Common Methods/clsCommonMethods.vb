﻿'************************************************************************************************************************************
'Class Name : clsExpCommonMethods.vb
'Purpose    :
'Date       :04-Feb-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsExpCommonMethods
    Private Shared ReadOnly mstrModuleName As String = "clsExpCommonMethods"

    Public Shared Function Get_ExpenseTypes(Optional ByVal IncludeMedical As Boolean = False, Optional ByVal IncludeTraining As Boolean = False, _
                                            Optional ByVal iFlag As Boolean = False, _
                                            Optional ByVal iList As String = "List", Optional ByVal IncludeMiscellenous As Boolean = False, _
                                            Optional ByVal blnIncludeImprest As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        Try

            If iFlag Then
                strQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If


            strQ &= "  SELECT '" & enExpenseType.EXP_LEAVE & "' AS Id , @EXP_LEAVE  AS Name "
            objDataOperation.AddParameter("@EXP_LEAVE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Leave"))



            'SHANI (06 JUN 2015) -- Start
            'Enhancement : Changes in C & R module given by Mr.Andrew.
            If IncludeMiscellenous = True Then
                strQ &= " UNION SELECT '" & enExpenseType.EXP_MISCELLANEOUS & "' AS Id, @EXP_MISCELLANEOUS  AS Name "
                objDataOperation.AddParameter("@EXP_MISCELLANEOUS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Miscellaneous"))
            End If
            'SHANI (06 JUN 2015) -- End 

            If IncludeMedical = True Then
                strQ &= " UNION SELECT '" & enExpenseType.EXP_MEDICAL & "' AS Id, @EXP_MEDICAL  AS Name "
                objDataOperation.AddParameter("@EXP_MEDICAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Medical"))
            End If

            'S.SANDEEP [09-OCT-2018] -- START
            'Uncommented the Commented Part
            If IncludeTraining = True Then
                strQ &= " UNION SELECT '" & enExpenseType.EXP_TRAINING & "' AS Id, @EXP_TRAINING  AS Name "
                objDataOperation.AddParameter("@EXP_TRAINING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Training"))
            End If
            'S.SANDEEP [09-OCT-2018] -- END


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            If blnIncludeImprest = True Then
                strQ &= " UNION SELECT '" & enExpenseType.EXP_IMPREST & "' AS Id, @EXP_IMPREST  AS Name "
                objDataOperation.AddParameter("@EXP_IMPREST", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Imprest"))
            End If
            'Pinkal (11-Sep-2019) -- End


            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_ExpenseTypes; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Shared Function Get_UoM(Optional ByVal iFlag As Boolean = False, Optional ByVal iList As String = "List") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        Try

            If iFlag Then
                strQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            strQ &= " SELECT '" & enExpUoM.UOM_QTY & "', @UOM_QTY  AS Name UNION " & _
                    " SELECT '" & enExpUoM.UOM_AMOUNT & "', @UOM_AMOUNT  AS Name  "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Select"))
            objDataOperation.AddParameter("@UOM_QTY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Quantity"))
            objDataOperation.AddParameter("@UOM_AMOUNT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Amount"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_UoM; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Leave")
			Language.setMessage(mstrModuleName, 3, "Medical")
			Language.setMessage(mstrModuleName, 4, "Training")
			Language.setMessage(mstrModuleName, 5, "Select")
			Language.setMessage(mstrModuleName, 6, "Quantity")
			Language.setMessage(mstrModuleName, 7, "Amount")
            Language.setMessage(mstrModuleName, 8, "Miscellaneous")
			Language.setMessage(mstrModuleName, 9, "Imprest")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
