﻿'************************************************************************************************************************************
'Class Name : clsclaim_process_Tran.vb
'Purpose    :
'Date       :11/11/2014
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports Aruti.Data


''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsclaim_process_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsclaim_process_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintCrprocesstranunkid As Integer
    Private mintCrapprovaltranunkid As Integer
    Private mintCrtranunkid As Integer
    Private mintCrmasterunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintPeriodId As Integer
    Private mintExpenseunkid As Integer
    Private mintSecrouteunkid As Integer
    Private mintCostingunkid As Integer
    Private mdecUnitprice As Decimal
    Private mdblQuantity As Double
    Private mdecAmount As Decimal
    Private mintQtyTranheadunkid As Integer = -1
    Private mintAmtTranheadunkid As Integer = -1
    Private mdecProcessedrate As Decimal
    Private mdecProcessedAmt As Decimal
    Private mstrExpense_Remark As String = String.Empty
    Private mintApproveremployeeunkid As Integer
    Private mintCrapproverunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mblnIsposted As Boolean
    Private mblnIsprocessed As Boolean
    Private mintProcessuserunkid As Integer
    Private mdtProcessdatetime As Date
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientID As String = ""
    Private mstrWebhostName As String = ""


    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Private mintCountryId As Integer = 0
    Private mintBaseCountryId As Integer = 0
    Private mdecBaseAmount As Decimal = 0
    Private mintExchangeRateId As Integer = 0
    Private mdecExchangeRate As Decimal = 0
    'Pinkal (04-Feb-2019) -- End


#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crprocesstranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Crprocesstranunkid() As Integer
        Get
            Return mintCrprocesstranunkid
        End Get
        Set(ByVal value As Integer)
            mintCrprocesstranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crapprovaltranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Crapprovaltranunkid() As Integer
        Get
            Return mintCrapprovaltranunkid
        End Get
        Set(ByVal value As Integer)
            mintCrapprovaltranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crtranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Crtranunkid() As Integer
        Get
            Return mintCrtranunkid
        End Get
        Set(ByVal value As Integer)
            mintCrtranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crmasterunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Crmasterunkid() As Integer
        Get
            Return mintCrmasterunkid
        End Get
        Set(ByVal value As Integer)
            mintCrmasterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _Periodunkid 
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodId
        End Get
        Set(ByVal value As Integer)
            mintPeriodId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set expenseunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Expenseunkid() As Integer
        Get
            Return mintExpenseunkid
        End Get
        Set(ByVal value As Integer)
            mintExpenseunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set secrouteunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Secrouteunkid() As Integer
        Get
            Return mintSecrouteunkid
        End Get
        Set(ByVal value As Integer)
            mintSecrouteunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set costingunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Costingunkid() As Integer
        Get
            Return mintCostingunkid
        End Get
        Set(ByVal value As Integer)
            mintCostingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unitprice
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Unitprice() As Decimal
        Get
            Return mdecUnitprice
        End Get
        Set(ByVal value As Decimal)
            mdecUnitprice = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set quantity
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Quantity() As Double
        Get
            Return mdblQuantity
        End Get
        Set(ByVal value As Double)
            mdblQuantity = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set amount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Amount() As Decimal
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal)
            mdecAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Qtytranheadunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _QtyTranheadunkid() As Integer
        Get
            Return mintQtyTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintQtyTranheadunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Amttranheadunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AmtTranheadunkid() As Integer
        Get
            Return mintAmtTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintAmtTranheadunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ProcessedRate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ProcessedRate() As Decimal
        Get
            Return mdecProcessedrate
        End Get
        Set(ByVal value As Decimal)
            mdecProcessedrate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Processedamt
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ProcessedAmt() As Decimal
        Get
            Return mdecProcessedAmt
        End Get
        Set(ByVal value As Decimal)
            mdecProcessedAmt = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set expense_remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Expense_Remark() As String
        Get
            Return mstrExpense_Remark
        End Get
        Set(ByVal value As String)
            mstrExpense_Remark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approveremployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approveremployeeunkid() As Integer
        Get
            Return mintApproveremployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintApproveremployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crapproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Crapproverunkid() As Integer
        Get
            Return mintCrapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintCrapproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isposted
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isposted() As Boolean
        Get
            Return mblnIsposted
        End Get
        Set(ByVal value As Boolean)
            mblnIsposted = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isprocessed
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isprocessed() As Boolean
        Get
            Return mblnIsprocessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsprocessed = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processuserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Processuserunkid() As Integer
        Get
            Return mintProcessuserunkid
        End Get
        Set(ByVal value As Integer)
            mintProcessuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processdatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Processdatetime() As Date
        Get
            Return mdtProcessdatetime
        End Get
        Set(ByVal value As Date)
            mdtProcessdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientID = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Webhostname
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebhostName = value
        End Set
    End Property


    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

    ''' <summary>
    ''' Purpose: Get or Set CountryId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _CountryId() As Integer
        Get
            Return mintCountryId
        End Get
        Set(ByVal value As Integer)
            mintCountryId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set BaseCountryId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _BaseCountryId() As Integer
        Get
            Return mintBaseCountryId
        End Get
        Set(ByVal value As Integer)
            mintBaseCountryId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set BaseAmount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _BaseAmount() As Decimal
        Get
            Return mdecBaseAmount
        End Get
        Set(ByVal value As Decimal)
            mdecBaseAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ExchangeRateId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ExchangeRateId() As Integer
        Get
            Return mintExchangeRateId
        End Get
        Set(ByVal value As Integer)
            mintExchangeRateId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ExchangeRate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ExchangeRate() As Decimal
        Get
            Return mdecExchangeRate
        End Get
        Set(ByVal value As Decimal)
            mdecExchangeRate = value
        End Set
    End Property

    'Pinkal (04-Feb-2019) -- End

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal intUnkId As Integer = 0, Optional ByVal xDataOp As clsDataOperation = Nothing)
        'Sohail (03 May 2018) - [intUnkId, xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'Dim objDataOperation As New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        If intUnkId > 0 Then mintCrprocesstranunkid = intUnkId
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT " & _
                      "  crprocesstranunkid " & _
                      ", crapprovaltranunkid " & _
                      ", crtranunkid " & _
                      ", crmasterunkid " & _
                      ", employeeunkid " & _
                      ", periodunkid " & _
                      ", expenseunkid " & _
                      ", secrouteunkid " & _
                      ", costingunkid " & _
                      ", unitprice " & _
                      ", quantity " & _
                      ", amount " & _
                      ", qtytranheadunkid " & _
                      ", amttranheadunkid " & _
                      ", processedrate " & _
                      ", processedamt " & _
                      ", expense_remark " & _
                      ", approveremployeeunkid " & _
                      ", crapproverunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", isposted " & _
                      ", isprocessed " & _
                      ", processuserunkid " & _
                      ", processdatetime " & _
                      ", ISNULL(countryunkid,0) AS countryunkid " & _
                      ", ISNULL(base_countryunkid ,0) AS base_countryunkid " & _
                      ", ISNULL(base_amount,0) AS base_amount " & _
                      ", ISNULL(exchangerateunkid,0) AS exchangerateunkid  " & _
                      ", ISNULL(exchange_rate,0) AS exchange_rate " & _
                     "FROM cmclaim_process_tran " & _
                     "WHERE crprocesstranunkid = @crprocesstranunkid "

            objDataOperation.AddParameter("@crprocesstranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintCrprocessTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintcrprocesstranunkid = CInt(dtRow.Item("crprocesstranunkid"))
                mintcrapprovaltranunkid = CInt(dtRow.Item("crapprovaltranunkid"))
                mintcrtranunkid = CInt(dtRow.Item("crtranunkid"))
                mintCrmasterunkid = CInt(dtRow.Item("crmasterunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintPeriodId = CInt(dtRow.Item("periodunkid"))
                mintexpenseunkid = CInt(dtRow.Item("expenseunkid"))
                mintsecrouteunkid = CInt(dtRow.Item("secrouteunkid"))
                mintCostingunkid = CInt(dtRow.Item("costingunkid"))
                mdecUnitprice = dtRow.Item("unitprice")
                mdblquantity = CDbl(dtRow.Item("quantity"))
                mdecAmount = dtRow.Item("amount")
                mintQtyTranheadunkid = CInt(dtRow.Item("qtytranheadunkid"))
                mintAmtTranheadunkid = CInt(dtRow.Item("amttranheadunkid"))
                mdecProcessedrate = CDec(dtRow.Item("processedrate"))
                mdecProcessedAmt = CDec(dtRow.Item("processedamt"))
                mstrexpense_remark = dtRow.Item("expense_remark").ToString
                mintapproveremployeeunkid = CInt(dtRow.Item("approveremployeeunkid"))
                mintcrapproverunkid = CInt(dtRow.Item("crapproverunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))

                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mstrVoidreason = dtRow.Item("voidreason").ToString
                mblnIsposted = CBool(dtRow.Item("isposted"))
                mblnIsprocessed = CBool(dtRow.Item("isprocessed"))
                mintProcessuserunkid = CInt(dtRow.Item("processuserunkid"))

                If Not IsDBNull(dtRow.Item("processdatetime")) Then
                    mdtProcessdatetime = dtRow.Item("processdatetime")
                End If


                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                mintCountryId = CInt(dtRow.Item("countryunkid"))
                mintBaseCountryId = CInt(dtRow.Item("base_countryunkid"))
                mdecBaseAmount = CDec(dtRow.Item("base_amount"))
                mintExchangeRateId = CInt(dtRow.Item("exchangerateunkid"))
                mdecExchangeRate = CDec(dtRow.Item("exchange_rate"))
                'Pinkal (04-Feb-2019) -- End


                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intIsposted As Integer = -1, Optional ByVal mstrFilter As String = "", Optional ByVal xDataOp As clsDataOperation = Nothing) As DataSet
        'Sohail (03 May 2018) - [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            strQ = "SELECT " & _
                      "  crprocesstranunkid " & _
                      ", crapprovaltranunkid " & _
                      ", crtranunkid " & _
                      ", cmclaim_process_tran.crmasterunkid " & _
                      ", cmclaim_process_tran.employeeunkid " & _
                      ", cmclaim_process_tran.periodunkid " & _
                      ", cmclaim_process_tran.expenseunkid " & _
                      ", cmclaim_process_tran.secrouteunkid " & _
                      ", cmclaim_process_tran.costingunkid " & _
                      ", cmexpense_master.uomunkid " & _
                      ", cmexpense_master.trnheadtype_id " & _
                      ", cmclaim_process_tran.qtytranheadunkid " & _
                      ", cmclaim_process_tran.amttranheadunkid " & _
                      ", cmclaim_request_master.claimrequestno " & _
                      ", ISNULL(hremployee_master.firstname,'') + '  ' + ISNULL(hremployee_master.othername,'')  + '  ' +  ISNULL(hremployee_master.surname,'') AS Employee " & _
                      ", cmclaim_request_master.claimrequestno + '  -  ' + ISNULL(hremployee_master.firstname,'') + '  ' + ISNULL(hremployee_master.othername,'')  + '  ' +  ISNULL(hremployee_master.surname,'') AS Claim " & _
                      ", ISNULL(cmexpense_master.name,'') AS Expense " & _
                      ", ISNULL(cfcommon_master.name,'') AS Sector " & _
                      ", ISNULL(cfcommon_period_tran.period_name,'') AS Period " & _
                      ", ISNULL(prtranhead_master.trnheadname,'') AS Qtytrnhead " & _
                      ", ISNULL(pr.trnheadname,'') AS Amttrnhead " & _
                      ", cmclaim_process_tran.unitprice " & _
                      ", cmclaim_process_tran.quantity " & _
                      ", cmclaim_process_tran.amount " & _
                      ", cmclaim_process_tran.processedrate " & _
                      ", cmclaim_process_tran.processedamt " & _
                      ", expense_remark " & _
                      ", cmclaim_process_tran.approveremployeeunkid " & _
                      ", cmclaim_process_tran.crapproverunkid " & _
                      ", cmclaim_process_tran.userunkid " & _
                      ", cmclaim_process_tran.isvoid " & _
                      ", cmclaim_process_tran.voiduserunkid " & _
                      ", cmclaim_process_tran.voiddatetime " & _
                      ", cmclaim_process_tran.voidreason " & _
                      ", cmclaim_process_tran.isposted " & _
                      ", cmclaim_process_tran.isprocessed " & _
                      ", cmclaim_process_tran.processuserunkid " & _
                      ", cmclaim_process_tran.processdatetime " & _
                      ", ISNULL(cmclaim_process_tran.countryunkid,0) AS countryunkid " & _
                      ", ISNULL(cmclaim_process_tran.base_countryunkid ,0) AS base_countryunkid " & _
                      ", ISNULL(base_amount,0) AS base_amount " & _
                      ", ISNULL(cmclaim_process_tran.exchangerateunkid,0) AS exchangerateunkid  " & _
                      ", ISNULL(cmclaim_process_tran.exchange_rate,0) AS exchange_rate " & _
                      ", ISNULL(cfexchange_rate.currency_sign,'') AS currency_sign " & _
                      ", ISNULL(cmexpense_master.default_costcenterunkid, 0) AS default_costcenterunkid " & _
                      ", ISNULL(pr.roundofftypeid, 200) AS roundofftypeid " & _
                      ", ISNULL(pr.roundoffmodeid, 1) AS roundoffmodeid " & _
                      " FROM cmclaim_process_tran " & _
                      " JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_process_tran.crmasterunkid AND cmclaim_request_master.isvoid = 0 " & _
                      " JOIN hremployee_master ON hremployee_master.employeeunkid = cmclaim_process_tran.employeeunkid " & _
                      " JOIN cmexpense_master ON cmexpense_master.expenseunkid  = cmclaim_process_tran.expenseunkid AND cmexpense_master.isactive = 1 " & _
                      " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmclaim_process_tran.secrouteunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & _
                      " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid  = cmclaim_process_tran.periodunkid " & _
                      " LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = cmclaim_process_tran.qtytranheadunkid AND prtranhead_master.isvoid  = 0 " & _
                      " LEFT JOIN prtranhead_master pr ON pr.tranheadunkid = cmclaim_process_tran.amttranheadunkid AND pr.isvoid  = 0 " & _
                      " LEFT JOIN cfexchange_rate ON cfexchange_rate.exchangerateunkid = ISNULL(cmclaim_process_tran.exchangerateunkid,0) " & _
                      " WHERE cmclaim_process_tran.isprocessed = 0 "
            'Sohail (11 Dec 2020) - [roundoffmodeid]
            'Sohail (03 Dec 2020) - [roundofftypeid]
            'Hemant (06 Jul 2020) -- [ISNULL(cmexpense_master.default_costcenterunkid, 0) AS default_costcenterunkid]

            'Pinkal (04-Feb-2019) -- 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[   ", ISNULL(cmclaim_process_tran.countryunkid,0) AS countryunkid , ISNULL(cmclaim_process_tran.base_countryunkid ,0) AS base_countryunkid , ISNULL(base_amount,0) AS base_amount , ISNULL(cmclaim_process_tran.exchangerateunkid,0) AS exchangerateunkid  , ISNULL(cmclaim_process_tran.exchange_rate,0) AS exchange_rate 

            If blnOnlyActive Then
                strQ &= " AND cmclaim_process_tran.isvoid = 0 "
            End If

            If intIsposted = 0 OrElse intIsposted = 1 Then
                strQ &= " AND cmclaim_process_tran.isposted = " & intIsposted & " "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cmclaim_process_tran) </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function Insert(ByVal drRow As DataRow, Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
    Public Function Insert(ByVal drRow As DataRow, ByVal dtCurrentDateAndTime As DateTime, Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        'Shani(24-Aug-2015) -- End


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrapprovaltranunkid)
            objDataOperation.AddParameter("@crtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("crtranunkid").ToString)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("crmasterunkid").ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("expenseunkid").ToString)
            objDataOperation.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("secrouteunkid").ToString)
            objDataOperation.AddParameter("@costingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("costingunkid").ToString)
            objDataOperation.AddParameter("@unitprice", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, drRow("unitprice").ToString)
            objDataOperation.AddParameter("@quantity", SqlDbType.Float, eZeeDataType.MONEY_SIZE, drRow("quantity").ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, drRow("amount").ToString)
            objDataOperation.AddParameter("@qtytranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQtyTranheadunkid)
            objDataOperation.AddParameter("@amttranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAmtTranheadunkid)
            objDataOperation.AddParameter("@processedrate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecProcessedrate)
            objDataOperation.AddParameter("@processedamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecProcessedAmt)
            objDataOperation.AddParameter("@expense_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, drRow("expense_remark").ToString)

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("approveremployeeunkid").ToString)
            'objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("crapproverunkid").ToString)
            'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("approveremployeeunkid").ToString)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveremployeeunkid)
            objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrapproverunkid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            'Pinkal (04-Feb-2019) -- End
            
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, String.Empty)
            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False.ToString)
            objDataOperation.AddParameter("@processuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
            objDataOperation.AddParameter("@processdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("countryunkid"))
            objDataOperation.AddParameter("@base_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("base_countryunkid"))
            objDataOperation.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("base_amount"))
            objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow("exchangerateunkid"))
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, drRow("exchange_rate"))
            'Pinkal (04-Feb-2019) -- End


            strQ = "INSERT INTO cmclaim_process_tran ( " & _
                      "  crapprovaltranunkid " & _
                      ", crtranunkid " & _
                      ", crmasterunkid " & _
                      ", employeeunkid " & _
                      ", periodunkid " & _
                      ", expenseunkid " & _
                      ", secrouteunkid " & _
                      ", costingunkid " & _
                      ", unitprice " & _
                      ", quantity " & _
                      ", amount " & _
                      ", qtytranheadunkid " & _
                      ", amttranheadunkid " & _
                      ", processedrate " & _
                      ", processedamt " & _
                      ", expense_remark " & _
                      ", approveremployeeunkid " & _
                      ", crapproverunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", isposted " & _
                      ", isprocessed " & _
                      ", processuserunkid " & _
                      ", processdatetime" & _
                      ", countryunkid " & _
                      ", base_countryunkid " & _
                      ", base_amount " & _
                      ", exchangerateunkid " & _
                      ", exchange_rate " & _
                    ") VALUES (" & _
                      "  @crapprovaltranunkid " & _
                      ", @crtranunkid " & _
                      ", @crmasterunkid " & _
                      ", @employeeunkid " & _
                      ", @periodunkid " & _
                      ", @expenseunkid " & _
                      ", @secrouteunkid " & _
                      ", @costingunkid " & _
                      ", @unitprice " & _
                      ", @quantity " & _
                      ", @amount " & _
                      ", @qtytranheadunkid " & _
                      ", @amttranheadunkid " & _
                      ", @processedrate " & _
                      ", @processedamt " & _
                      ", @expense_remark " & _
                      ", @approveremployeeunkid " & _
                      ", @crapproverunkid " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason " & _
                      ", @isposted " & _
                      ", @isprocessed " & _
                      ", @processuserunkid " & _
                      ", @processdatetime" & _
                      ", @countryunkid " & _
                      ", @base_countryunkid " & _
                      ", @base_amount " & _
                      ", @exchangerateunkid " & _
                      ", @exchange_rate " & _
                    "); SELECT @@identity"

            'Pinkal (04-Feb-2019) --  'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[  @countryunkid , @base_countryunkid , @base_amount , @exchangerateunkid , @exchange_rate " & _]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Crprocesstranunkid = dsList.Tables(0).Rows(0).Item(0)


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If InsertAudiTrailForClaimProcessTran(objDataOperation, 1) = False Then
            If InsertAudiTrailForClaimProcessTran(objDataOperation, 1, dtCurrentDateAndTime) = False Then
                'Shani(24-Aug-2015) -- End

                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cmclaim_process_tran) </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function Update(ByVal objDataOperation As clsDataOperation) As Boolean
    Public Function Update(ByVal objDataOperation As clsDataOperation, ByVal dtCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crprocesstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrprocesstranunkid.ToString)
            objDataOperation.AddParameter("@crapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrapprovaltranunkid.ToString)
            objDataOperation.AddParameter("@crtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrtranunkid.ToString)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrmasterunkid.ToString)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseunkid.ToString)
            objDataOperation.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSecrouteunkid.ToString)
            objDataOperation.AddParameter("@costingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostingunkid.ToString)
            objDataOperation.AddParameter("@unitprice", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecUnitprice.ToString)
            objDataOperation.AddParameter("@quantity", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblQuantity.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@qtytranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQtyTranheadunkid)
            objDataOperation.AddParameter("@amttranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAmtTranheadunkid)
            objDataOperation.AddParameter("@processedrate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecProcessedrate)
            objDataOperation.AddParameter("@processedamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecProcessedAmt)
            objDataOperation.AddParameter("@expense_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrExpense_Remark.ToString)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveremployeeunkid.ToString)
            objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrapproverunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsposted.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)
            objDataOperation.AddParameter("@processuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessuserunkid.ToString)
            objDataOperation.AddParameter("@processdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtProcessdatetime <> Nothing, mdtProcessdatetime, DBNull.Value))

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryId)
            objDataOperation.AddParameter("@base_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBaseCountryId)
            objDataOperation.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseAmount)
            objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExchangeRateId)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchangeRate)
            'Pinkal (04-Feb-2019) -- End

            strQ = "UPDATE cmclaim_process_tran SET " & _
                      "  crapprovaltranunkid = @crapprovaltranunkid" & _
                      ", crtranunkid = @crtranunkid" & _
                      ", crmasterunkid = @crmasterunkid" & _
                      ", expenseunkid = @expenseunkid" & _
                      ", secrouteunkid = @secrouteunkid" & _
                      ", costingunkid = @costingunkid" & _
                      ", unitprice = @unitprice" & _
                      ", quantity = @quantity" & _
                      ", amount = @amount" & _
                      ", qtytranheadunkid = @qtytranheadunkid " & _
                      ", amttranheadunkid = @amttranheadunkid " & _
                      ", processedrate = @processedrate " & _
                      ", processedamt = @processedamt " & _
                      ", expense_remark = @expense_remark" & _
                      ", approveremployeeunkid = @approveremployeeunkid" & _
                      ", crapproverunkid = @crapproverunkid" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason" & _
                      ", isposted = @isposted" & _
                      ", isprocessed = @isprocessed" & _
                      ", processuserunkid = @processuserunkid" & _
                      ", processdatetime = @processdatetime " & _
                      ", countryunkid = @countryunkid " & _
                      ", base_countryunkid = @base_countryunkid " & _
                      ", base_amount = @base_amount " & _
                      ", exchangerateunkid = @exchangerateunkid " & _
                     ", exchange_rate = @exchange_rate " & _
                    "WHERE crprocesstranunkid = @crprocesstranunkid "

            'Pinkal (04-Feb-2019) --  'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[ countryunkid = @countryunkid , base_countryunkid = @base_countryunkid , base_amount = @base_amount , exchangerateunkid = @exchangerateunkid , exchange_rate = @exchange_rate 

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If InsertAudiTrailForClaimProcessTran(objDataOperation, 2) = False Then
            If InsertAudiTrailForClaimProcessTran(objDataOperation, 2, dtCurrentDateAndTime) = False Then
                'Shani(24-Aug-2015) -- End

                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cmclaim_process_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "DELETE FROM cmclaim_process_tran " & _
            "WHERE crprocesstranunkid = @crprocesstranunkid "

            objDataOperation.AddParameter("@crprocesstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@crprocesstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  crprocesstranunkid " & _
              ", crapprovaltranunkid " & _
              ", crtranunkid " & _
              ", crmasterunkid " & _
              ", expenseunkid " & _
              ", secrouteunkid " & _
              ", costingunkid " & _
              ", unitprice " & _
              ", quantity " & _
              ", amount " & _
              ", qtytranheadunkid " & _
              ", amttranheadunkid " & _
              ", processedrate " & _
              ", processedamt " & _
              ", expense_remark " & _
              ", approveremployeeunkid " & _
              ", crapproverunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", isposted " & _
              ", isprocessed " & _
              ", processuserunkid " & _
              ", processdatetime " & _
              ",countryunkid " & _
              ",base_countryunkid " & _
              ",base_amount " & _
              ",exchangerateunkid " & _
              ",exchange_rate " & _
             "FROM cmclaim_process_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            'Pinkal (04-Feb-2019) -- 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[", countryunkid ,base_countryunkid , base_amount , exchangerateunkid , exchange_rate "

            If intUnkid > 0 Then
                strQ &= " AND crprocesstranunkid <> @crprocesstranunkid"
            End If



            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@crprocesstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    'Public Function Posting_Unposting_Claim(ByVal blnIsPosted As Boolean, ByVal dtTable As DataTable, ByVal dtCurrentDateAndTime As DateTime) As Boolean

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Try
    '        Dim objDataOperation As New clsDataOperation
    '        objDataOperation.BindTransaction()


    '        strQ = " UPDATE cmclaim_process_tran SET periodunkid = @periodunkid,qtytranheadunkid  = @qtytranheadunkid,amttranheadunkid = @amttranheadunkid ,isposted = @isposted " & _
    '                  " WHERE employeeunkid  = @employeeunkid AND crprocesstranunkid = @crprocesstranunkid AND isvoid = 0 "

    '        For Each dr As DataRow In dtTable.Rows
    '            If CBool(dr("ischange")) = False Then Continue For
    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("periodunkid"))
    '            objDataOperation.AddParameter("@qtytranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("qtytranheadunkid"))
    '            objDataOperation.AddParameter("@amttranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("amttranheadunkid"))
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("employeeunkid"))
    '            objDataOperation.AddParameter("@crprocesstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("crprocesstranunkid"))
    '            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsPosted)
    '            objDataOperation.ExecNonQuery(strQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            _Crprocesstranunkid = CInt(dr("crprocesstranunkid"))


    '            'Shani(24-Aug-2015) -- Start
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'If InsertAudiTrailForClaimProcessTran(objDataOperation, 2) = False Then
    '            If InsertAudiTrailForClaimProcessTran(objDataOperation, 2, dtCurrentDateAndTime) = False Then
    '                'Shani(24-Aug-2015) -- End

    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '        Next

    '        objDataOperation.ReleaseTransaction(True)
    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Posting_Unposting_Claim; Module Name: " & mstrModuleName)
    '        Return False
    '    End Try
    'End Function

    Public Function Posting_Unposting_Claim(ByVal blnIsPosted As Boolean, ByVal xPeriodId As Integer, ByVal xQtyTranHeadId As Integer, ByVal xAmtTranHeadId As Integer, ByVal strClaimProcessIds As String) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
         
            If strClaimProcessIds.Trim.Length > 0 Then

                strQ = "IF OBJECT_ID(N'tempdb..#ClaimProcessIds') IS NOT NULL " & _
                         " DROP TABLE #ClaimProcessIds " & _
                         " CREATE TABLE #ClaimProcessIds (crprocesstranunkid int default(0)) " & _
                         " DECLARE @DATA NVARCHAR(MAX) = '" & strClaimProcessIds & "' " & _
                         " DECLARE @CNT INT " & _
                         " DECLARE @SEP AS CHAR(1) " & _
                         " SET @SEP = ',' " & _
                         " SET @CNT = 1 " & _
                         " WHILE (CHARINDEX(@SEP,@DATA)>0) " & _
                         "    BEGIN " & _
                         "        INSERT INTO #ClaimProcessIds (crprocesstranunkid) " & _
                         "        SELECT DATA = LTRIM(RTRIM(SUBSTRING(@DATA,1,CHARINDEX(@SEP,@DATA)-1))) " & _
                         "        SET @DATA = SUBSTRING(@DATA,CHARINDEX(@SEP,@DATA)+1,LEN(@DATA)) " & _
                         "        SET @CNT = @CNT + 1 " & _
                         "    END " & _
                         " INSERT INTO #ClaimProcessIds (crprocesstranunkid) SELECT DATA = LTRIM(RTRIM(@DATA)) " & _
                         " UPDATE cmclaim_process_tran  SET " & _
                         " periodunkid = @periodunkid, qtytranheadunkid  = @qtytranheadunkid,amttranheadunkid = @amttranheadunkid,isposted = @isposted " & _
                         " FROM " & _
                         "( " & _
                         "    SELECT " & _
                         "        #ClaimProcessIds.crprocesstranunkid " & _
                         "    FROM #ClaimProcessIds " & _
                         "    JOIN cmclaim_process_tran AS CPT ON #ClaimProcessIds.crprocesstranunkid = CPT.crprocesstranunkid " & _
                         "    WHERE CPT.isvoid = 0 " & _
                         " ) AS CP WHERE CP.crprocesstranunkid = cmclaim_process_tran. crprocesstranunkid " & _
                         " INSERT INTO atcmclaim_process_tran (crprocesstranunkid ,crapprovaltranunkid,crtranunkid,crmasterunkid,employeeunkid,periodunkid,expenseunkid,secrouteunkid,costingunkid " & _
                         ", unitprice,quantity,amount,qtytranheadunkid,amttranheadunkid,processedrate, processedamt,expense_remark,approveremployeeunkid,crapproverunkid,userunkid,isposted " & _
                         ", isprocessed,processuserunkid,processdatetime,audittype,audituserunkid,auditdatetime,ip,machine_name,form_name,module_name1,module_name2,module_name3 " & _
                         ",module_name4,module_name5,isweb,countryunkid,base_countryunkid, base_amount, exchangerateunkid, exchange_rate) " & _
                         " SELECT cmclaim_process_tran.crprocesstranunkid ,crapprovaltranunkid,crtranunkid,crmasterunkid,employeeunkid,periodunkid,expenseunkid,secrouteunkid,costingunkid " & _
                         ", unitprice,quantity,amount,qtytranheadunkid,amttranheadunkid,processedrate, processedamt,expense_remark,approveremployeeunkid,crapproverunkid,@userunkid,isposted " & _
                         ", isprocessed,processuserunkid,processdatetime,2,@userunkid,getdate(),@ip,@machine_name,@form_name,@module_name1,@module_name2,@module_name3 " & _
                         ", @module_name4,@module_name5,@isweb,countryunkid,base_countryunkid, base_amount, exchangerateunkid, exchange_rate " & _
                         " FROM #ClaimProcessIds " & _
                         " JOIN cmclaim_process_tran ON #ClaimProcessIds.crprocesstranunkid = cmclaim_process_tran.crprocesstranunkid " & _
                         " WHERE cmclaim_process_tran.isvoid = 0 " & _
                         " " & _
                         " IF OBJECT_ID(N'tempdb..#ClaimProcessIds') IS NOT NULL " & _
                         " DROP TABLE #ClaimProcessIds "

                objDataOperation.ClearParameters()

                If mstrWebClientID.ToString().Length <= 0 Then
                    mstrWebClientID = getIP()
                End If

                If mstrWebhostName.ToString().Length <= 0 Then
                    mstrWebhostName = getHostName()
                End If


                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
                objDataOperation.AddParameter("@qtytranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xQtyTranHeadId)
                objDataOperation.AddParameter("@amttranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAmtTranHeadId)
                objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsPosted)

                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebClientID)
                objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)
                If mstrWebFormName.Trim.Length <= 0 Then
                    objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                    objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                    objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                Else
                    objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                    objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                    objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 5, "WEB"))
                End If
                objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
                objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
                objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
                objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Posting_Unposting_Claim; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function InsertAudiTrailForClaimProcessTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
    Public Function InsertAudiTrailForClaimProcessTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal dtCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try


            strQ = "INSERT INTO atcmclaim_process_tran ( " & _
                     " crprocesstranunkid " & _
                     ",crapprovaltranunkid " & _
                     ", crtranunkid " & _
                     ", crmasterunkid " & _
                     ", employeeunkid " & _
                     ", periodunkid " & _
                     ", expenseunkid " & _
                     ", secrouteunkid " & _
                     ", costingunkid " & _
                     ", unitprice " & _
                     ", quantity " & _
                     ", amount " & _
                     ", qtytranheadunkid " & _
                     ", amttranheadunkid " & _
                     ", processedrate " & _
                     ", processedamt " & _
                     ", expense_remark " & _
                     ", approveremployeeunkid " & _
                     ", crapproverunkid " & _
                     ", userunkid " & _
                     ", isposted " & _
                     ", isprocessed " & _
                     ", processuserunkid " & _
                     ", processdatetime" & _
                     ",audittype " & _
                     ",audituserunkid " & _
                     ",auditdatetime " & _
                     ", ip " & _
                     ",machine_name " & _
                     ",form_name " & _
                     ",module_name1 " & _
                     ",module_name2 " & _
                     ",module_name3 " & _
                     ",module_name4 " & _
                     ",module_name5 " & _
                     ",isweb " & _
                     ", countryunkid " & _
                     ", base_countryunkid " & _
                     ", base_amount " & _
                     ", exchangerateunkid " & _
                     ", exchange_rate " & _
                   ") VALUES (" & _
                     "  @crprocesstranunkid " & _
                     ", @crapprovaltranunkid " & _
                     ", @crtranunkid " & _
                     ", @crmasterunkid " & _
                     ", @employeeunkid " & _
                     ", @periodunkid " & _
                     ", @expenseunkid " & _
                     ", @secrouteunkid " & _
                     ", @costingunkid " & _
                     ", @unitprice " & _
                     ", @quantity " & _
                     ", @amount " & _
                     ", @qtytranheadunkid " & _
                     ", @amttranheadunkid " & _
                     ", @processedrate " & _
                     ", @processedamt " & _
                     ", @expense_remark " & _
                     ", @approveremployeeunkid " & _
                     ", @crapproverunkid " & _
                     ", @userunkid " & _
                     ", @isposted " & _
                     ", @isprocessed " & _
                     ", @processuserunkid " & _
                     ", @processdatetime" & _
                     ", @audittype " & _
                     ", @audituserunkid " & _
                     ", @auditdatetime " & _
                     ", @ip " & _
                     ", @machine_name " & _
                     ", @form_name " & _
                     ", @module_name1 " & _
                     ", @module_name2 " & _
                     ", @module_name3 " & _
                     ", @module_name4 " & _
                     ", @module_name5 " & _
                     ", @isweb " & _
                     ", @countryunkid " & _
                     ", @base_countryunkid " & _
                     ", @base_amount " & _
                     ", @exchangerateunkid " & _
                     ", @exchange_rate " & _
                   "); SELECT @@identity"


            'Pinkal (04-Feb-2019) --  'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[  @countryunkid , @base_countryunkid , @base_amount , @exchangerateunkid , @exchange_rate " & _]



            If mstrWebClientID.ToString().Length <= 0 Then
                mstrWebClientID = getIP()
            End If

            If mstrWebhostName.ToString().Length <= 0 Then
                mstrWebhostName = getHostName()
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crprocesstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrprocesstranunkid)
            objDataOperation.AddParameter("@crapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrapprovaltranunkid)
            objDataOperation.AddParameter("@crtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrtranunkid)
            objDataOperation.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrmasterunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodId)
            objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpenseunkid)
            objDataOperation.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSecrouteunkid)
            objDataOperation.AddParameter("@costingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostingunkid)
            objDataOperation.AddParameter("@unitprice", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecUnitprice)
            objDataOperation.AddParameter("@quantity", SqlDbType.Float, eZeeDataType.MONEY_SIZE, mdblQuantity)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecAmount)
            objDataOperation.AddParameter("@qtytranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQtyTranheadunkid)
            objDataOperation.AddParameter("@amttranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAmtTranheadunkid)
            objDataOperation.AddParameter("@processedrate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecProcessedrate)
            objDataOperation.AddParameter("@processedamt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecProcessedAmt)
            objDataOperation.AddParameter("@expense_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrExpense_Remark)
            objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveremployeeunkid)
            objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrapproverunkid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsposted)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed)
            objDataOperation.AddParameter("@processuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessuserunkid)
            objDataOperation.AddParameter("@processdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtProcessdatetime <> Nothing, mdtProcessdatetime, DBNull.Value))
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            'Shani(24-Aug-2015) -- End

            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebClientID)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 5, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryId)
            objDataOperation.AddParameter("@base_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBaseCountryId)
            objDataOperation.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseAmount)
            objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExchangeRateId)
            objDataOperation.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchangeRate)
            'Pinkal (04-Feb-2019) -- End

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAudiTrailForClaimProcessTran; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeExpenseQty(ByVal intEmployeeId As Integer, ByVal intExpenseId As Integer, ByVal intPeriodId As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Decimal
        'Sohail (03 May 2018) - [xDataOp]
        Dim mdecQty As Decimal = 0
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try


            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim objDataOperation As New clsDataOperation
            Dim objDataOperation As clsDataOperation
            If xDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOp
            End If
            'Sohail (03 May 2018) -- End
            objDataOperation.ClearParameters()

            strQ = " SELECT ISNULL(SUM(quantity),0) AS Qty FROM cmclaim_process_tran " & _
                       " WHERE cmclaim_process_tran.employeeunkid = @employeeunkid " & _
                       " AND cmclaim_process_tran.periodunkid = @periodunkid AND cmclaim_process_tran.isposted = 1 AND isprocessed  = 0  AND cmclaim_process_tran.isvoid = 0 "

            If intExpenseId > 0 Then
                strQ &= " AND cmclaim_process_tran.expenseunkid = @expenseunkid  "
                objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intExpenseId)
            End If
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdecQty = CDec(dsList.Tables(0).Rows(0)("Qty"))
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeExpenseQty; Module Name: " & mstrModuleName)
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return mdecQty
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeExpenseAmount(ByVal intEmployeeId As Integer, ByVal intExpenseId As Integer, ByVal intPeriodId As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Decimal
        'Sohail (03 May 2018) - [xDataOp]
        Dim mdecAmount As Decimal = 0
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim objDataOperation As New clsDataOperation
            Dim objDataOperation As clsDataOperation
            If xDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOp
            End If
            'Sohail (03 May 2018) -- End
            objDataOperation.ClearParameters()


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            'strQ = " SELECT ISNULL(SUM(amount),0) AS Amount FROM cmclaim_process_tran " & _
            '           " WHERE cmclaim_process_tran.employeeunkid = @employeeunkid  " & _
            '           " AND cmclaim_process_tran.periodunkid = @periodunkid AND cmclaim_process_tran.isposted = 1 AND isprocessed  = 0  AND cmclaim_process_tran.isvoid = 0 "

            strQ = " SELECT ISNULL(SUM(amount),0) AS Amount,ISNULL(SUM(base_amount),0) AS Base_Amount FROM cmclaim_process_tran " & _
                       " WHERE cmclaim_process_tran.employeeunkid = @employeeunkid  " & _
                       " AND cmclaim_process_tran.periodunkid = @periodunkid AND cmclaim_process_tran.isposted = 1 AND isprocessed  = 0  AND cmclaim_process_tran.isvoid = 0 "

            'Pinkal (04-Feb-2019) -- End


            If intExpenseId > 0 Then
                strQ &= " AND cmclaim_process_tran.expenseunkid = @expenseunkid  "
                objDataOperation.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intExpenseId)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[Used in Payroll formula due to that we are using base amount for further process.]
                'mdecAmount = CDec(dsList.Tables(0).Rows(0)("Amount"))
                mdecAmount = CDec(dsList.Tables(0).Rows(0)("Base_Amount"))
                'Pinkal (04-Feb-2019) -- End
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeExpenseAmount; Module Name: " & mstrModuleName)
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return mdecAmount
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 5, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class