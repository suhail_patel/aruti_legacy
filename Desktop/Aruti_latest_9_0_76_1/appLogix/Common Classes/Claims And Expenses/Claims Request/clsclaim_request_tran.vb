﻿'************************************************************************************************************************************
'Class Name :clsclaim_request_tran.vb
'Purpose    :
'Date       :01-Mar-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsclaim_request_tran
    Private Shared ReadOnly mstrModuleName As String = "clsclaim_request_tran"
    Dim mstrMessage As String = ""

#Region " Private Variables "

    Private mintClaimRequestTranId As Integer = 0
    Private mintClaimRequestMasterId As Integer = 0
    Private mintStatusId As Integer = 0
    Private mintVisibleId As Integer = -1
    Private mdtTran As DataTable
    Private mblnPaymentApprovalwithLeaveApproval As Boolean = False
    Private mintLeaveTypeId As Integer = 0
    Private mblnLeaveApproverForLeaveType As Boolean = False
    Private mintYearId As Integer = 0
    Private mintLeaveBalanceSetting As Integer = 0
    Private mintLoginemployeeunkid As Integer = 0
    Private mintVoidLoginemployeeunkid As Integer = 0
    Dim dtLeaveApprover As DataTable = Nothing
    'Pinkal (10-Jan-2017) -- Start
    'Enhancement - Working on TRA C&R Module Changes with Leave Module.
    Private mintApproverTranID As Integer = 0
    Private mintApproverID As Integer = 0
    'Pinkal (10-Jan-2017) -- End


#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property


    'Pinkal (10-Jan-2017) -- Start
    'Enhancement - Working on TRA C&R Module Changes with Leave Module.

    ''' <summary>
    ''' Purpose: Get or Set ClaimRequestTranId
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _ClaimRequestTranId() As Integer
        Get
            Return mintClaimRequestTranId
        End Get
        Set(ByVal value As Integer)
            mintClaimRequestTranId = value
        End Set
    End Property

    'Pinkal (10-Jan-2017) -- End


    ''' <summary>
    ''' Purpose: Get or Set ClaimRequestMasterId
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _ClaimRequestMasterId() As Integer
        Get
            Return mintClaimRequestMasterId
        End Get
        Set(ByVal value As Integer)
            mintClaimRequestMasterId = value
            Call Get_Data()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Datatable
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set StatusId
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _StatusId() As Integer
        Set(ByVal value As Integer)
            mintStatusId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set StatusId
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _VisibleId() As Integer
        Set(ByVal value As Integer)
            mintVisibleId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set IsPaymentApprovalwithLeaveApproval
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _IsPaymentApprovalwithLeaveApproval() As Boolean
        Set(ByVal value As Boolean)
            mblnPaymentApprovalwithLeaveApproval = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set LeaveTypeId
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _LeaveTypeId() As Integer
        Get
            Return mintLeaveTypeId
        End Get
        Set(ByVal value As Integer)
            mintLeaveTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set LeaveApproverForLeaveType
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _LeaveApproverForLeaveType() As Boolean
        Get
            Return mblnLeaveApproverForLeaveType
        End Get
        Set(ByVal value As Boolean)
            mblnLeaveApproverForLeaveType = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set YearId
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _YearId() As Integer
        Get
            Return mintYearId
        End Get
        Set(ByVal value As Integer)
            mintYearId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LeaveBalanceSetting
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public Property _LeaveBalanceSetting() As Integer
        Get
            Return mintLeaveBalanceSetting
        End Get
        Set(ByVal value As Integer)
            mintLeaveBalanceSetting = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set LoginEmployeeID
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _LoginEmployeeID() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set VoidLoginEmployeeID
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _VoidLoginEmployeeID() As Integer
        Get
            Return mintVoidLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidLoginemployeeunkid = value
        End Set
    End Property

    Public Property _dtLeaveApprover() As DataTable
        Get
            Return dtLeaveApprover
        End Get
        Set(ByVal value As DataTable)
            dtLeaveApprover = value
        End Set
    End Property

    'Pinkal (10-Jan-2017) -- Start
    'Enhancement - Working on TRA C&R Module Changes with Leave Module.

    ''' <summary>
    ''' Purpose: Get or Set _ApproverTranID
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _ApproverTranID() As Integer
        Get
            Return mintApproverTranID
        End Get
        Set(ByVal value As Integer)
            mintApproverTranID = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _ApproverID
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _ApproverID() As Integer
        Get
            Return mintApproverID
        End Get
        Set(ByVal value As Integer)
            mintApproverID = value
        End Set
    End Property

    'Pinkal (10-Jan-2017) -- End

#End Region

#Region " Constructor "

    Public Sub New()
        mdtTran = New DataTable("List")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("crtranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("crmasterunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("expenseunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("secrouteunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("costingunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("unitprice")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("quantity")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("amount")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("expense_remark")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            'Pinkal (07-Sep-2015) -- Start
            'Enhancement - ADDING EXPENSE REMARK AND SECTOR ROUTE COLUMN FOR TRA AS PER DENNIS MAIL SUBJECT "TRA upgrade" SENT ON 03-Sep-2015.
            dCol = New DataColumn("sector")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            dCol = New DataColumn("costcenterunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("costcentercode")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("glcodeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("glcode")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("gldesc")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isbudgetmandatory")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            'Pinkal (26-Dec-2018) -- End

            'Pinkal (20-Nov-2018) -- End


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            dCol = New DataColumn("ishrExpense")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("countryunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("base_countryunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("base_amount")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("exchangerateunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("exchange_rate")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            'Pinkal (04-Feb-2019) -- End


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            dCol = New DataColumn("isimprest")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)
            'Pinkal (11-Sep-2019) -- End


            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("loginemployeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("expense")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("uom")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidloginemployeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Public & Private Methods "

    Private Sub Get_Data()
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            Dim objData As New clsDataOperation

            StrQ = "SELECT " & _
                   "  cmclaim_request_tran.crtranunkid " & _
                   ", cmclaim_request_tran.crmasterunkid " & _
                   ", cmclaim_request_tran.expenseunkid " & _
                   ", cmclaim_request_tran.secrouteunkid " & _
                   ", cmclaim_request_tran.costingunkid " & _
                   ", cmclaim_request_tran.unitprice " & _
                   ", cmclaim_request_tran.quantity " & _
                   ", cmclaim_request_tran.amount " & _
                   ", cmclaim_request_tran.expense_remark " & _
                   ", cmclaim_request_tran.isvoid " & _
                   ", cmclaim_request_tran.voiduserunkid " & _
                   ", cmclaim_request_tran.voiddatetime " & _
                   ", cmclaim_request_tran.voidreason " & _
                   ", cmclaim_request_tran.loginemployeeunkid " & _
                   ", cmclaim_request_tran.voidloginemployeeunkid " & _
                   ", '' AS AUD " & _
                   ", '' AS GUID " & _
                   ", ISNULL(cmexpense_master.name,'') As expense " & _
                   ", CASE WHEN cmexpense_master.uomunkid = 1 THEN @Qty " & _
                   "       WHEN cmexpense_master.uomunkid = 2 THEN @Amt END AS uom " & _
                   ", ISNULL(cfcommon_master.name,'') AS sector " & _
                   ", ISNULL(cmclaim_request_tran.costcenterunkid,0) AS costcenterunkid " & _
                   ", ISNULL(prcostcenter_master.costcentercode,'') AS costcentercode " & _
                   ", ISNULL(prcostcenter_master.costcentername,'') AS costcentername " & _
                   ", ISNULL(cmexpense_master.glcodeunkid,0) AS glcodeunkid " & _
                   ", ISNULL(praccount_master.account_code,'') AS glcode " & _
                   ", ISNULL(praccount_master.account_name,'') AS glcodeName " & _
                   ", ISNULL(cmexpense_master.description,'') AS gldesc " & _
                   ", ISNULL(cmexpense_master.isbudgetmandatory,0) AS isbudgetmandatory " & _
                   ", ISNULL(cmexpense_master.ishrexpense,0) AS ishrexpense " & _
                   ",ISNULL(cmclaim_request_tran.countryunkid,0) AS countryunkid " & _
                   ",ISNULL(cmclaim_request_tran.base_countryunkid,0) AS base_countryunkid " & _
                   ",ISNULL(cmclaim_request_tran.base_amount,0.00) AS base_amount " & _
                   ",ISNULL(cmclaim_request_tran.exchangerateunkid,0) AS exchangerateunkid " & _
                   ",ISNULL(cmclaim_request_tran.exchange_rate,0.00) AS exchange_rate " & _
                   ", ISNULL(cmexpense_master.isimprest,0) AS isimprest " & _
                   " FROM cmclaim_request_tran " & _
                   "    JOIN cmclaim_request_master ON cmclaim_request_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                   " JOIN cmexpense_master ON cmclaim_request_tran.expenseunkid = cmexpense_master.expenseunkid " & _
                   " LEFT JOIN  cfcommon_master ON cfcommon_master.masterunkid = cmclaim_request_tran.secrouteunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & _
                   " LEFT JOIN praccount_master ON praccount_master.accountunkid = cmexpense_master.glcodeunkid " & _
                   " LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = cmclaim_request_tran.costcenterunkid " & _
                   " WHERE cmclaim_request_tran.crmasterunkid = @crmasterunkid AND cmclaim_request_tran.isvoid = 0  "


            'Pinkal (11-Sep-2019) -- 'Enhancement NMB - Working On Claim Retirement for NMB.[", ISNULL(cmexpense_master.isimprest,0) AS isimprest " & ]

            'Pinkal (04-Feb-2019) -- 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[  ",ISNULL(cmclaim_request_tran.countryunkid,0) AS countryunkid ,ISNULL(cmclaim_request_tran.base_countryunkid,0) AS base_countryunkid ,ISNULL(cmclaim_request_tran.base_amount,0.00) AS base_amount ,ISNULL(cmclaim_request_tran.exchangerateunkid,0) AS exchangerateunkid ,ISNULL(cmclaim_request_tran.exchange_rate,0.00) AS exchange_rate "

            'Pinkal (29-Jan-2019) --   'Enhancement - Working on P2P Integration with Claim. [", ISNULL(cmexpense_master.ishrexpense,0) AS ishrexpense " & _]

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            '   ", ISNULL(cmclaim_request_tran.costcenterunkid,0) AS costcenterunkid   ", ISNULL(cmexpense_master.isbudgetmandatory,0) AS isbudgetmandatory " & _
            objData.ClearParameters()
            'Pinkal (20-Nov-2018) -- End

            objData.AddParameter("@Qty", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 6, "Quantity"))
            objData.AddParameter("@Amt", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 7, "Amount"))

            objData.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimRequestMasterId.ToString)

            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()

            For Each dRow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(dRow)
            Next

            objData = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function Insert(ByVal dtRow As DataRow, ByVal objDataOpr As clsDataOperation, ByVal iUserId As Integer) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOpr.ClearParameters()
            With dtRow
                StrQ = "INSERT INTO cmclaim_request_tran ( " & _
                           "  crmasterunkid " & _
                           ", expenseunkid " & _
                           ", secrouteunkid " & _
                           ", costingunkid " & _
                           ", unitprice " & _
                           ", quantity " & _
                           ", amount " & _
                           ", expense_remark " & _
                           ", isvoid " & _
                           ", voiduserunkid " & _
                           ", voiddatetime " & _
                           ", voidreason " & _
                           ", loginemployeeunkid " & _
                           ", voidloginemployeeunkid " & _
                           ", approvertranunkid " & _
                           ", approverunkid " & _
                           ",costcenterunkid " & _
                           ",countryunkid " & _
                           ",base_countryunkid " & _
                           ",base_amount " & _
                           ",exchangerateunkid " & _
                           ",exchange_rate " & _
                       ") VALUES (" & _
                           "  @crmasterunkid " & _
                           ", @expenseunkid " & _
                           ", @secrouteunkid " & _
                           ", @costingunkid " & _
                           ", @unitprice " & _
                           ", @quantity " & _
                           ", @amount " & _
                           ", @expense_remark " & _
                           ", @isvoid " & _
                           ", @voiduserunkid " & _
                           ", @voiddatetime " & _
                           ", @voidreason " & _
                           ", @loginemployeeunkid " & _
                           ", @voidloginemployeeunkid " & _
                           ", @approvertranunkid " & _
                           ", @approverunkid " & _
                           ",@costcenterunkid " & _
                           ",@countryunkid " & _
                           ",@base_countryunkid " & _
                           ",@base_amount " & _
                           ",@exchangerateunkid " & _
                           ",@exchange_rate " & _
                         "); SELECT @@identity"

                'Pinkal (04-Feb-2019) -- 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[   ",@countryunkid ,@base_countryunkid ,@base_amount ,@exchangerateunkid ,@exchange_rate " & _

                'Pinkal (20-Nov-2018) --  'Enhancement - Working on P2P Integration for NMB.[costcenterunkid]

                'Pinkal (10-Jan-2017) -- 'Enhancement - Working on TRA C&R Module Changes with Leave Module.[approvertranunkid,approverunkid]

                objDataOpr.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimRequestMasterId)
                objDataOpr.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("expenseunkid"))
                objDataOpr.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("secrouteunkid"))
                objDataOpr.AddParameter("@costingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("costingunkid"))
                objDataOpr.AddParameter("@unitprice", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("unitprice"))
                objDataOpr.AddParameter("@quantity", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("quantity"))
                objDataOpr.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amount"))
                objDataOpr.AddParameter("@expense_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("expense_remark"))
                objDataOpr.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                objDataOpr.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                If IsDBNull(.Item("voiddatetime")) = False Then
                    objDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                Else
                    objDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                objDataOpr.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                objDataOpr.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
                objDataOpr.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voidloginemployeeunkid"))


                'Pinkal (10-Jan-2017) -- Start
                'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                objDataOpr.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranID)
                objDataOpr.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverID)
                'Pinkal (10-Jan-2017) -- End


                'Pinkal (20-Nov-2018) -- Start
                'Enhancement - Working on P2P Integration for NMB.
                objDataOpr.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("costcenterunkid"))
                'Pinkal (20-Nov-2018) -- End


                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                objDataOpr.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countryunkid"))
                objDataOpr.AddParameter("@base_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("base_countryunkid"))
                objDataOpr.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("base_amount"))
                objDataOpr.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("exchangerateunkid"))
                objDataOpr.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("exchange_rate"))
                'Pinkal (04-Feb-2019) -- End



                dsList = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

                mintClaimRequestTranId = dsList.Tables(0).Rows(0).Item(0)

                dtRow.Item("crtranunkid") = mintClaimRequestTranId
                mdtTran.AcceptChanges()

                If .Item("crmasterunkid") > 0 Then
                    If clsCommonATLog.Insert_TranAtLog(objDataOpr, "cmclaim_request_master", "crmasterunkid", .Item("crmasterunkid"), "cmclaim_request_tran", "crtranunkid", mintClaimRequestTranId, 2, 1, , iUserId) = False Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If
                Else
                    If clsCommonATLog.Insert_TranAtLog(objDataOpr, "cmclaim_request_master", "crmasterunkid", mintClaimRequestMasterId, "cmclaim_request_tran", "crtranunkid", mintClaimRequestTranId, 2, 1, , iUserId) = False Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Insert", mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    Public Function Update(ByVal dtRow As DataRow, ByVal objDataOpr As clsDataOperation, ByVal iUserId As Integer) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOpr.ClearParameters()
            With dtRow
                StrQ = "UPDATE cmclaim_request_tran SET " & _
                       "  crmasterunkid = @crmasterunkid" & _
                       ", expenseunkid = @expenseunkid" & _
                       ", secrouteunkid = @secrouteunkid" & _
                       ", costingunkid = @costingunkid" & _
                       ", unitprice = @unitprice" & _
                       ", quantity = @quantity" & _
                       ", amount = @amount" & _
                       ", expense_remark = @expense_remark" & _
                       ", isvoid = @isvoid" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voiddatetime = @voiddatetime" & _
                       ", voidreason = @voidreason" & _
                       ", loginemployeeunkid = @loginemployeeunkid " & _
                       ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                       ", approvertranunkid = @approvertranunkid " & _
                       ", approverunkid = @approverunkid " & _
                       ", costcenterunkid = @costcenterunkid " & _
                       ", countryunkid = @countryunkid " & _
                       ", base_countryunkid = @base_countryunkid " & _
                       ", base_amount = @base_amount " & _
                       ", exchangerateunkid = @exchangerateunkid " & _
                       ", exchange_rate = @exchange_rate " & _
                       "WHERE crtranunkid = @crtranunkid "

                'Pinkal (04-Feb-2019) -- 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[countryunkid = @countryunkid,base_countryunkid = @base_countryunkid, base_amount = @base_amount,exchangerateunkid = @exchangerateunkid,exchange_rate = @exchange_rate ]

                'Pinkal (20-Nov-2018) --  'Enhancement - Working on P2P Integration for NMB.[", costcenterunkid = @costcenterunkid " & _]

                objDataOpr.AddParameter("@crtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("crtranunkid"))
                objDataOpr.AddParameter("@crmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("crmasterunkid"))
                objDataOpr.AddParameter("@expenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("expenseunkid"))
                objDataOpr.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("secrouteunkid"))
                objDataOpr.AddParameter("@costingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("costingunkid"))
                objDataOpr.AddParameter("@unitprice", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("unitprice"))
                objDataOpr.AddParameter("@quantity", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("quantity"))
                objDataOpr.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amount"))
                objDataOpr.AddParameter("@expense_remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("expense_remark"))
                objDataOpr.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                objDataOpr.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                If IsDBNull(.Item("voiddatetime")) = False Then
                    objDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                Else
                    objDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                objDataOpr.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                objDataOpr.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
                objDataOpr.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voidloginemployeeunkid"))

                'Pinkal (10-Jan-2017) -- Start
                'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                objDataOpr.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranID)
                objDataOpr.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverID)
                'Pinkal (10-Jan-2017) -- End


                'Pinkal (20-Nov-2018) -- Start
                'Enhancement - Working on P2P Integration for NMB.
                objDataOpr.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("costcenterunkid"))
                'Pinkal (20-Nov-2018) -- End


                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                objDataOpr.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("countryunkid"))
                objDataOpr.AddParameter("@base_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("base_countryunkid"))
                objDataOpr.AddParameter("@base_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("base_amount"))
                objDataOpr.AddParameter("@exchangerateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("exchangerateunkid"))
                objDataOpr.AddParameter("@exchange_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("exchange_rate"))
                'Pinkal (04-Feb-2019) -- End


                Call objDataOpr.ExecNonQuery(StrQ)

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

                If clsCommonATLog.Insert_TranAtLog(objDataOpr, "cmclaim_request_master", "crmasterunkid", .Item("crmasterunkid"), "cmclaim_request_tran", "crtranunkid", .Item("crtranunkid"), 2, 2, , iUserId) = False Then
                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If
            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Update", mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    Public Function Delete(ByVal dtRow As DataRow, ByVal objDataOpr As clsDataOperation, ByVal iUserId As Integer) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOpr.ClearParameters()
            With dtRow
                If .Item("crtranunkid") > 0 Then
                    If clsCommonATLog.Insert_TranAtLog(objDataOpr, "cmclaim_request_master", "crmasterunkid", .Item("crmasterunkid"), "cmclaim_request_tran", "crtranunkid", .Item("crtranunkid"), 2, 3, , iUserId) = False Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If
                End If

                StrQ = "UPDATE cmclaim_request_tran SET " & _
                       "  isvoid = @isvoid" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
                       ", voiddatetime = @voiddatetime" & _
                       ", voidreason = @voidreason " & _
                       "WHERE crtranunkid = @crtranunkid "

                objDataOpr.AddParameter("@crtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("crtranunkid"))
                objDataOpr.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))

                'Pinkal (16-Dec-2014) -- Start
                'Enhancement - Claim & Request For Web.
                objDataOpr.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                objDataOpr.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voidloginemployeeunkid"))
                objDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                objDataOpr.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                'Pinkal (16-Dec-2014) -- End
                Call objDataOpr.ExecNonQuery(StrQ)

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

            End With
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Delete", mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    Public Function Insert_Update_Delete(ByVal objData As clsDataOperation, ByVal iUserId As Integer) As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objData.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                If Insert(mdtTran.Rows(i), objData, iUserId) = False Then
                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If
                            Case "U"
                                If Update(mdtTran.Rows(i), objData, iUserId) = False Then
                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If
                            Case "D"
                                If Delete(mdtTran.Rows(i), objData, iUserId) = False Then
                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Insert_Update_Delete", mstrModuleName)
        Finally
        End Try
    End Function


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Public Function Insert_Update_Approver(ByVal objDataOpr As clsDataOperation, _
    '                                       ByVal iEmployeeId As Integer, _
    '                                       ByVal iExpTypeId As Integer, _
    '                                       ByVal iUserId As Integer, _
    '                                       ByVal iFromModuleID As Integer, _
    '                                       Optional ByVal iNextPriority As Integer = 0, _
    '                                       Optional ByVal iClaimMasterId As Integer = 0) As Boolean
    '    Dim StrQ As String = String.Empty
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Try
    '        Dim dTab As DataTable = Nothing

    '        If mblnPaymentApprovalwithLeaveApproval = False Then
    '            Dim objExpAppr As New clsExpenseApprover_Master
    '            dsList = objExpAppr.GetEmployeeApprovers(iExpTypeId, iEmployeeId, "List", objDataOpr)
    '            If dsList.Tables(0).Rows.Count > 0 Then
    '                Dim iPriority As Integer = 0
    '                If iNextPriority > 0 Then
    '                    iPriority = iNextPriority
    '                    dTab = New DataView(dsList.Tables(0), "crpriority <= '" & iPriority & "'", "crpriority DESC", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    dTab = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
    '                End If
    '            End If

    '        ElseIf mblnPaymentApprovalwithLeaveApproval Then
    '            Dim objapprover As New clsleaveapprover_master
    '            If CBool(mblnLeaveApproverForLeaveType) = True Then
    '                dTab = objapprover.GetEmployeeApprover(-1, iEmployeeId, -1, mintLeaveTypeId, mblnLeaveApproverForLeaveType)
    '            Else
    '                dTab = objapprover.GetEmployeeApprover(-1, iEmployeeId, -1, -1)
    '            End If
    '            objapprover = Nothing
    '        End If

    '        If dTab IsNot Nothing AndAlso dTab.Rows.Count > 0 Then
    '            Dim dtTrans As DataTable = mdtTran.Copy
    '            Dim objClaim_Apprl As New clsclaim_request_approval_tran
    '            objClaim_Apprl._YearId = mintYearId
    '            objClaim_Apprl._LeaveBalanceSetting = mintLeaveBalanceSetting
    '            objClaim_Apprl._DataTable = dtTrans

    '            Dim objExpMst As New clsExpense_Master

    '            For Each dRow As DataRow In dTab.Rows

    '                If mblnPaymentApprovalwithLeaveApproval Then

    '                    Dim mintPriority As Integer = dTab.Compute("MIN(priority)", "1=1")

    '                    'Pinkal (22-Jun-2015) -- Start
    '                    'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.

    '                    If iExpTypeId = enExpenseType.EXP_LEAVE AndAlso iFromModuleID = enExpFromModuleID.FROM_LEAVE Then
    '                        mintVisibleId = -1
    '                    Else
    '                        If mintPriority = CInt(dRow("priority")) Then
    '                            mintVisibleId = 2
    '                        Else
    '                            mintVisibleId = -1
    '                        End If
    '                    End If

    '                    'Pinkal (22-Jun-2015) -- End

    '                    objClaim_Apprl._EmployeeID = iEmployeeId
    '                    If objClaim_Apprl.Insert_Update_ApproverData(dRow("employeeunkid"), dRow("approverunkid"), mintStatusId, mintVisibleId, iUserId, mintClaimRequestMasterId, objDataOpr) = False Then
    '                        exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                Else

    '                    'Pinkal (22-Jun-2015) -- Start
    '                    'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.
    '                    If iExpTypeId = enExpenseType.EXP_LEAVE AndAlso iFromModuleID = enExpFromModuleID.FROM_LEAVE Then
    '                        mintVisibleId = -1
    '                    Else
    '                        Dim mintPriority As Integer = dTab.Compute("MIN(crpriority)", "1=1")
    '                        If mintPriority = CInt(dRow("crpriority")) Then
    '                            mintVisibleId = 2
    '                        Else
    '                            mintVisibleId = -1
    '                        End If
    '                    End If
    '                    'Pinkal (22-Jun-2015) -- End

    '                    objClaim_Apprl._EmployeeID = iEmployeeId
    '                    objClaim_Apprl._VoidLoginEmployeeID = mintVoidLoginemployeeunkid
    '                    '
    '                    If objClaim_Apprl.Insert_Update_ApproverData(dRow("employeeunkid"), dRow("crapproverunkid"), mintStatusId, mintVisibleId, iUserId, mintClaimRequestMasterId, objDataOpr) = False Then
    '                        exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                End If

    '            Next
    '        End If
    '        Return True
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Insert_Update_Approver", mstrModuleName)
    '        Return False
    '    Finally
    '    End Try
    'End Function

    Public Function Insert_Update_Approver(ByVal xDatabaseName As String, ByVal xYearUnkid As Integer _
                                                              , ByVal xCompanyUnkid As Integer, ByVal mdtEmployeeAsonDate As Date _
                                                              , ByVal xIncludeIn_ActiveEmployee As Boolean, ByVal objDataOpr As clsDataOperation _
                                                              , ByVal iEmployeeId As Integer, ByVal iExpTypeId As Integer _
                                                              , ByVal iUserId As Integer, ByVal iFromModuleID As Integer _
                                                              , ByVal dtCurrentDateAndTime As DateTime _
                                                              , Optional ByVal iNextPriority As Integer = 0, Optional ByVal iClaimMasterId As Integer = 0) As Boolean

        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Dim dTab As DataTable = Nothing

            If mblnPaymentApprovalwithLeaveApproval = False Then
                Dim objExpAppr As New clsExpenseApprover_Master
                dsList = objExpAppr.GetEmployeeApprovers(iExpTypeId, iEmployeeId, "List", objDataOpr)
                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim iPriority As Integer = 0
                    If iNextPriority > 0 Then
                        iPriority = iNextPriority
                        dTab = New DataView(dsList.Tables(0), "crpriority <= '" & iPriority & "'", "crpriority DESC", DataViewRowState.CurrentRows).ToTable
                    Else
                        dTab = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                    End If
                End If

            ElseIf mblnPaymentApprovalwithLeaveApproval Then

                If dtLeaveApprover Is Nothing Then

                Dim objapprover As New clsleaveapprover_master
                If CBool(mblnLeaveApproverForLeaveType) = True Then
                    dTab = objapprover.GetEmployeeApprover(xDatabaseName, xYearUnkid, xCompanyUnkid _
                                                                                 , mdtEmployeeAsonDate, xIncludeIn_ActiveEmployee _
                                                                                 , -1, iEmployeeId, -1, mintLeaveTypeId, mblnLeaveApproverForLeaveType, objDataOpr)
                Else
                    dTab = objapprover.GetEmployeeApprover(xDatabaseName, xYearUnkid, xCompanyUnkid _
                                                                                 , mdtEmployeeAsonDate, xIncludeIn_ActiveEmployee _
                                                                                , -1, iEmployeeId, -1, -1, mblnLeaveApproverForLeaveType, objDataOpr)
                End If
                objapprover = Nothing
                Else

                    dTab = dtLeaveApprover.Copy()
                End If

            End If

            If dTab IsNot Nothing AndAlso dTab.Rows.Count > 0 Then
                Dim dtTrans As DataTable = mdtTran.Copy
                Dim objClaim_Apprl As New clsclaim_request_approval_tran
                objClaim_Apprl._YearId = mintYearId
                objClaim_Apprl._LeaveBalanceSetting = mintLeaveBalanceSetting
                objClaim_Apprl._DataTable = dtTrans

                Dim objExpMst As New clsExpense_Master

                For Each dRow As DataRow In dTab.Rows

                    If mblnPaymentApprovalwithLeaveApproval Then

                        Dim mintPriority As Integer = dTab.Compute("MIN(priority)", "1=1")

                        If iExpTypeId = enExpenseType.EXP_LEAVE AndAlso iFromModuleID = enExpFromModuleID.FROM_LEAVE Then
                            mintVisibleId = -1
                        Else
                        If mintPriority = CInt(dRow("priority")) Then
                            mintVisibleId = 2
                        Else
                            mintVisibleId = -1
                        End If
                        End If

                        objClaim_Apprl._EmployeeID = iEmployeeId
                        If objClaim_Apprl.Insert_Update_ApproverData(dRow("employeeunkid"), dRow("approverunkid"), mintStatusId, mintVisibleId, iUserId, dtCurrentDateAndTime, mintClaimRequestMasterId, objDataOpr) = False Then
                            exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                    Else

                        If iExpTypeId = enExpenseType.EXP_LEAVE AndAlso iFromModuleID = enExpFromModuleID.FROM_LEAVE Then
                            mintVisibleId = -1
                            'S.SANDEEP [09-OCT-2018] -- START
                        ElseIf iExpTypeId = enExpenseType.EXP_TRAINING AndAlso iFromModuleID = enExpFromModuleID.FROM_TRAINING Then
                            mintVisibleId = -1
                            'S.SANDEEP [09-OCT-2018] -- END
                        Else
                        Dim mintPriority As Integer = dTab.Compute("MIN(crpriority)", "1=1")
                        If mintPriority = CInt(dRow("crpriority")) Then
                            mintVisibleId = 2
                        Else
                            mintVisibleId = -1
                        End If
                        End If

                        objClaim_Apprl._EmployeeID = iEmployeeId
                        objClaim_Apprl._VoidLoginEmployeeID = mintVoidLoginemployeeunkid
                        If objClaim_Apprl.Insert_Update_ApproverData(dRow("employeeunkid"), dRow("crapproverunkid"), mintStatusId, mintVisibleId, iUserId, dtCurrentDateAndTime, mintClaimRequestMasterId, objDataOpr) = False Then
                            exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                    End If

                Next
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Insert_Update_Approver", mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    Public Function GetAppliedExpenseAmount(ByVal mintRequestMstID As Integer) As Decimal
        Dim mdecamount As Decimal = 0
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation
            StrQ = " SELECT ISNULL(SUM(Amount),0.00) AS Amount FROM cmclaim_request_tran WHERE crmasterunkid = @masterunkid AND isvoid = 0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRequestMstID)
            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mdecamount = CDec(dsList.Tables(0).Rows(0)("Amount"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAppliedExpenseAmount", mstrModuleName)
        End Try
        Return mdecamount
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsExpCommonMethods", 6, "Quantity")
            Language.setMessage("clsExpCommonMethods", 7, "Amount")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class