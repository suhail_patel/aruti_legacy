﻿'************************************************************************************************************************************
'Class Name : clsExApprovalLevel.vb
'Purpose    :
'Date       :05-Feb-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsExApprovalLevel
    Private Shared ReadOnly mstrModuleName As String = "clsExApprovalLevel"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintCrlevelunkid As Integer
    Private mintExpensetypeid As Integer
    Private mstrCrlevelcode As String = String.Empty
    Private mstrCrlevelName As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrCrlevelname1 As String = String.Empty
    Private mstrCrlevelname2 As String = String.Empty
    Private mintCrpriority As Integer

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crlevelunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Crlevelunkid() As Integer
        Get
            Return mintCrlevelunkid
        End Get
        Set(ByVal value As Integer)
            mintCrlevelunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set expensetypeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Expensetypeid() As Integer
        Get
            Return mintExpensetypeid
        End Get
        Set(ByVal value As Integer)
            mintExpensetypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crlevelcode
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Crlevelcode() As String
        Get
            Return mstrCrlevelcode
        End Get
        Set(ByVal value As String)
            mstrCrlevelcode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crlevelname
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Crlevelname() As String
        Get
            Return mstrCrlevelName
        End Get
        Set(ByVal value As String)
            mstrCrlevelName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crlevelname1
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Crlevelname1() As String
        Get
            Return mstrCrlevelname1
        End Get
        Set(ByVal value As String)
            mstrCrlevelname1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crlevelname2
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Crlevelname2() As String
        Get
            Return mstrCrlevelname2
        End Get
        Set(ByVal value As String)
            mstrCrlevelname2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crpriority
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Crpriority() As Integer
        Get
            Return mintCrpriority
        End Get
        Set(ByVal value As Integer)
            mintCrpriority = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  crlevelunkid " & _
              ", expensetypeid " & _
              ", crlevelcode " & _
              ", crlevelname " & _
              ", isactive " & _
              ", crlevelname1 " & _
              ", crlevelname2 " & _
              ", crpriority " & _
             "FROM cmapproverlevel_master " & _
             "WHERE crlevelunkid = @crlevelunkid "

            objDataOperation.AddParameter("@crlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrlevelunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCrlevelunkid = CInt(dtRow.Item("crlevelunkid"))
                mintExpensetypeid = CInt(dtRow.Item("expensetypeid"))
                mstrCrlevelcode = dtRow.Item("crlevelcode").ToString
                mstrCrlevelName = dtRow.Item("crlevelname").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrCrlevelname1 = dtRow.Item("crlevelname1").ToString
                mstrCrlevelname2 = dtRow.Item("crlevelname2").ToString
                mintCrpriority = CInt(dtRow.Item("crpriority"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT " & _
                   "  crlevelunkid " & _
                   ", expensetypeid " & _
                   ", crlevelcode " & _
                   ", crlevelname " & _
                   ", isactive " & _
                   ", crlevelname1 " & _
                   ", crlevelname2 " & _
                   ", crpriority " & _
                   ", CASE WHEN expensetypeid = '" & enExpenseType.EXP_LEAVE & "' THEN @Leave " & _
                   "       WHEN expensetypeid = '" & enExpenseType.EXP_MEDICAL & "' THEN @Medical " & _
                   "       WHEN expensetypeid = '" & enExpenseType.EXP_MISCELLANEOUS & "' THEN @Miscellaneous " & _
                   "       WHEN expensetypeid = '" & enExpenseType.EXP_TRAINING & "' THEN @Training " & _
                   "       WHEN expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest END AS expensetype " & _
                   "FROM cmapproverlevel_master "

            'Pinkal (11-Sep-2019) -- 'Enhancement NMB - Working On Claim Retirement for NMB.[ "       WHEN expensetypeid = '" & enExpenseType.EXP_IMPREST & "' THEN @Imprest END AS expensetype " & _]

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            objDataOperation.AddParameter("@Leave", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 2, "Leave"))
            objDataOperation.AddParameter("@Medical", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 3, "Medical"))
            objDataOperation.AddParameter("@Training", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 4, "Training"))
            objDataOperation.AddParameter("@Miscellaneous", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous"))

            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            objDataOperation.AddParameter("@Imprest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsExpCommonMethods", 9, "Imprest"))
            'Pinkal (11-Sep-2019) -- End


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cmapproverlevel_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCrlevelcode, , mintExpensetypeid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Level Code is already defined for the selected expense category. Please define new Level Code.")
            Return False
        ElseIf isExist(, mstrCrlevelName, mintExpensetypeid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Level Name is already defined for the selected expense category. Please define new Level Name.")
            Return False
        ElseIf isPriorityExist(mintCrpriority, mintExpensetypeid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Level Priority is already assign for the selected expense category. Please assign new Level Priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpensetypeid.ToString)
            objDataOperation.AddParameter("@crlevelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCrlevelcode.ToString)
            objDataOperation.AddParameter("@crlevelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCrlevelName.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@crlevelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCrlevelname1.ToString)
            objDataOperation.AddParameter("@crlevelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCrlevelname2.ToString)
            objDataOperation.AddParameter("@crpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrpriority.ToString)

            strQ = "INSERT INTO cmapproverlevel_master ( " & _
              "  expensetypeid " & _
              ", crlevelcode " & _
              ", crlevelname " & _
              ", isactive " & _
              ", crlevelname1 " & _
              ", crlevelname2 " & _
              ", crpriority" & _
            ") VALUES (" & _
              "  @expensetypeid " & _
              ", @crlevelcode " & _
              ", @crlevelname " & _
              ", @isactive " & _
              ", @crlevelname1 " & _
              ", @crlevelname2 " & _
              ", @crpriority" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCrlevelunkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "cmapproverlevel_master", "crlevelunkid", mintCrlevelunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cmapproverlevel_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrCrlevelcode, , mintExpensetypeid, mintCrlevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Level Code is already defined for the selected expense category. Please define new Level Code.")
            Return False
        ElseIf isExist(, mstrCrlevelName, mintExpensetypeid, mintCrlevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Level Name is already defined for the selected expense category. Please define new Level Name.")
            Return False
        ElseIf isPriorityExist(mintCrpriority, mintExpensetypeid, mintCrlevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Level Priority is already assign for the selected expense category. Please assign new Level Priority.")
            Return False
        End If


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@crlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrlevelunkid.ToString)
            objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpensetypeid.ToString)
            objDataOperation.AddParameter("@crlevelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCrlevelcode.ToString)
            objDataOperation.AddParameter("@crlevelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCrlevelName.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@crlevelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCrlevelname1.ToString)
            objDataOperation.AddParameter("@crlevelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCrlevelname2.ToString)
            objDataOperation.AddParameter("@crpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCrpriority.ToString)

            strQ = "UPDATE cmapproverlevel_master SET " & _
              "  expensetypeid = @expensetypeid" & _
              ", crlevelcode = @crlevelcode" & _
              ", crlevelname = @crlevelname" & _
              ", isactive = @isactive" & _
              ", crlevelname1 = @crlevelname1" & _
              ", crlevelname2 = @crlevelname2" & _
              ", crpriority = @crpriority " & _
            "WHERE crlevelunkid = @crlevelunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "cmapproverlevel_master", mintCrlevelunkid, "crlevelunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "cmapproverlevel_master", "crlevelunkid", mintCrlevelunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cmapproverlevel_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, you cannot delete this Level. Reason : This Level is already linked with some transaction.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE cmapproverlevel_master SET isactive = 0 " & _
                   "WHERE crlevelunkid = @crlevelunkid "

            objDataOperation.AddParameter("@crlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "cmapproverlevel_master", "crlevelunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsTables As DataSet = Nothing
        Dim blnIsUsed As Boolean = False
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "TABLE_NAME AS TableName " & _
                   "FROM INFORMATION_SCHEMA.COLUMNS " & _
                   "WHERE COLUMN_NAME='crlevelunkid' "

            dsTables = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@crlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            For Each dtRow As DataRow In dsTables.Tables("List").Rows
                If dtRow.Item("TableName") = "cmapproverlevel_master" Then Continue For
                strQ = "SELECT crlevelunkid FROM " & dtRow.Item("TableName").ToString & " WHERE crlevelunkid = @crlevelunkid "
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next
            mstrMessage = ""
            Return blnIsUsed
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", _
                            Optional ByVal strName As String = "", _
                            Optional ByVal iExpenseTypeId As Integer = 0, _
                            Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  crlevelunkid " & _
              ", expensetypeid " & _
              ", crlevelcode " & _
              ", crlevelname " & _
              ", isactive " & _
              ", crlevelname1 " & _
              ", crlevelname2 " & _
              ", crpriority " & _
             "FROM cmapproverlevel_master " & _
             "WHERE isactive = 1 "

            If strCode.ToString.Trim.Length > 0 Then
                strQ &= " AND crlevelcode = @code "
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If

            If strName.ToString.Trim.Length > 0 Then
                strQ &= " AND crlevelname = @name "
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If iExpenseTypeId > 0 Then
                strQ &= " AND expensetypeid = @expensetypeid "
                objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iExpenseTypeId)
            End If

            If intUnkid > 0 Then
                strQ &= " AND crlevelunkid <> @crlevelunkid"
                objDataOperation.AddParameter("@crlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isPriorityExist(ByVal mintPriority As Integer, Optional ByVal iExpenseTypeId As Integer = 0, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  crlevelunkid " & _
              ", expensetypeid " & _
              ", crlevelcode " & _
              ", crlevelname " & _
              ", isactive " & _
              ", crlevelname1 " & _
              ", crlevelname2 " & _
              ", crpriority " & _
             "FROM cmapproverlevel_master " & _
             "WHERE crpriority = @crpriority AND isactive = 1 "

            If intUnkid > 0 Then
                strQ &= " AND crlevelunkid <> @crlevelunkid"
            End If

            If iExpenseTypeId > 0 Then
                strQ &= " AND expensetypeid = @expensetypeid "
                objDataOperation.AddParameter("@expensetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iExpenseTypeId)
            End If

            objDataOperation.AddParameter("@crpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority)
            objDataOperation.AddParameter("@crlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isPriorityExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(ByVal iExpenseType As Integer, Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as Id,'' AS Code, ' ' +  @name  as Name UNION "
            End If
            strQ &= "SELECT crlevelunkid AS Id,crlevelcode AS Code,crlevelname as Name FROM cmapproverlevel_master where isactive = 1 " & _
                    " AND expensetypeid = '" & iExpenseType & "' " & _
                    "ORDER BY name "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Level Code is already defined for the selected expense category. Please define new Level Code.")
			Language.setMessage("clsExpCommonMethods", 2, "Leave")
			Language.setMessage("clsExpCommonMethods", 3, "Medical")
			Language.setMessage("clsExpCommonMethods", 4, "Training")
            Language.setMessage("clsExpCommonMethods", 5, "Miscellaneous")
			Language.setMessage(mstrModuleName, 2, "This Level Name is already defined for the selected expense category. Please define new Level Name.")
			Language.setMessage(mstrModuleName, 3, "This Level Priority is already assign for the selected expense category. Please assign new Level Priority.")
			Language.setMessage(mstrModuleName, 4, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
