﻿'************************************************************************************************************************************
'Class Name : clsassignemp_sector.vb
'Purpose    :
'Date       :4/4/2016
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System

''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsassignemp_sector
    Private Shared ReadOnly mstrModuleName As String = "clsassignemp_sector"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mbintCrempsecrouteunkid As Int64
    Private mintEmployeeunkid As Integer
    Private mintSecrouteunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set crempsecrouteunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Crempsecrouteunkid() As Int64
        Get
            Return mbintCrempsecrouteunkid
        End Get
        Set(ByVal value As Int64)
            mbintCrempsecrouteunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set secrouteunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Secrouteunkid() As Integer
        Get
            Return mintSecrouteunkid
        End Get
        Set(ByVal value As Integer)
            mintSecrouteunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webformname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  crempsecrouteunkid " & _
                      ", employeeunkid " & _
                      ", secrouteunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                     "FROM cmassignemp_sector " & _
                     "WHERE crempsecrouteunkid = @crempsecrouteunkid "

            objDataOperation.AddParameter("@crempsecrouteunkid", SqlDbType.BigInt, mbintCrempsecrouteunkid.ToString.Length, mbintCrempsecrouteunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mbintCrempsecrouteunkid = Convert.ToInt64(dtRow.Item("crempsecrouteunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintsecrouteunkid = CInt(dtRow.Item("secrouteunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal xEmployeeID As Integer = -1, Optional ByVal xSectopID As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try

            strQ = "SELECT " & _
                  "     ischeck " & _
                  "    ,Sector " & _
                  "    ,isgrp " & _
                  "    ,sortid " & _
                  "    ,grpid " & _
                  "    ,crempsecrouteunkid " & _
                  "    ,employeeunkid " & _
                  "    ,SectorId " & _
                  "FROM " & _
                  "( " & _
                  "    SELECT DISTINCT " & _
                  "         CAST(0 AS BIT) AS ischeck " & _
                  "        , hremployee_master.employeeunkid " & _
                  "        , -1 SectorId " & _
                  "        , ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' '+ ISNULL(hremployee_master.surname,'') AS Sector " & _
                  "        ,1 AS isgrp " & _
                  "        ,cmassignemp_sector.employeeunkid AS grpid " & _
                  "        ,-1 AS sortid " & _
                  "        ,-1 AS crempsecrouteunkid " & _
                  "    FROM cmassignemp_sector " & _
                  "        JOIN hremployee_master ON cmassignemp_sector.employeeunkid = hremployee_master.employeeunkid " & _
                  "    WHERE cmassignemp_sector.isvoid = 0 "

            If xEmployeeID > 0 Then
                strQ &= " AND cmassignemp_sector.employeeunkid = '" & xEmployeeID & "' "
            End If

            If xSectopID > 0 Then
                strQ &= " AND cmassignemp_sector.secrouteunkid = " & xSectopID
            End If

            strQ &= " UNION ALL " & _
                      "    SELECT " & _
                      "     CAST(0 AS BIT) AS ischeck " & _
                      "      , hremployee_master.employeeunkid " & _
                      "     ,ISNULL(cfcommon_master.masterunkid,'') AS SectorId " & _
                      "     ,ISNULL(cfcommon_master.name,'') AS Sector " & _
                      "     ,0 AS isgrp " & _
                      "     ,cmassignemp_sector.employeeunkid AS grpid " & _
                      "     ,0 AS sortid " & _
                      "     ,cmassignemp_sector.crempsecrouteunkid AS crempsecrouteunkid " & _
                     " FROM cmassignemp_sector " & _
                     " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = cmassignemp_sector.employeeunkid " & _
                     " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmassignemp_sector.secrouteunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & _
                     " WHERE cmassignemp_sector.isvoid= 0 "

            If xEmployeeID > 0 Then
                strQ &= " AND cmassignemp_sector.employeeunkid = " & xEmployeeID
            End If

            If xSectopID > 0 Then
                strQ &= " AND cmassignemp_sector.secrouteunkid = " & xSectopID
            End If

            strQ &= ") AS A WHERE 1 = 1  ORDER BY grpid,sortid,A.Sector "

            objDataOperation.ClearParameters()
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cmassignemp_sector) </purpose>
    Public Function Insert() As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSecrouteunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO cmassignemp_sector ( " & _
                      "  employeeunkid " & _
                      ", secrouteunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @employeeunkid " & _
                      ", @secrouteunkid " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid " & _
                      ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mbintCrempsecrouteunkid = Convert.ToInt64(dsList.Tables(0).Rows(0).Item(0))

            If InsertAudiTrailForEmployeeSector(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cmassignemp_sector) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crempsecrouteunkid", SqlDbType.BigInt, mbintCrempsecrouteunkid.ToString().Length, mbintCrempsecrouteunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSecrouteunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE cmassignemp_sector SET " & _
                      "  employeeunkid = @employeeunkid" & _
                      ", secrouteunkid = @secrouteunkid" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                    "WHERE crempsecrouteunkid = @crempsecrouteunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAudiTrailForEmployeeSector(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cmassignemp_sector) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "Update  cmassignemp_sector SET isvoid = 1,voiduserunkid = @voiduserunkid,voiddatetime = Getdate(),voidreason = @voidreason WHERE crempsecrouteunkid = @crempsecrouteunkid AND isvoid = 0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crempsecrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAudiTrailForEmployeeSector(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intEmployeeId As Integer, ByVal intSectorId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT cmclaim_request_tran.crmasterunkid FROM cmclaim_request_tran " & _
                      " JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_request_tran.crmasterunkid " & _
                      " WHERE cmclaim_request_master.isvoid = 0 And cmclaim_request_tran.isvoid = 0 AND cmclaim_request_master.employeeunkid= @employeeunkid " & _
                      " AND cmclaim_request_tran.secrouteunkid = @sectorunkid " & _
                      " UNION " & _
                      " SELECT cmclaim_approval_tran.crmasterunkid FROM cmclaim_approval_tran " & _
                      " JOIN cmclaim_request_master ON cmclaim_request_master.crmasterunkid = cmclaim_approval_tran.crmasterunkid " & _
                      " where cmclaim_request_master.isvoid = 0 And cmclaim_approval_tran.isvoid = 0 AND cmclaim_request_master.employeeunkid= @employeeunkid " & _
                      " AND cmclaim_approval_tran.secrouteunkid = @sectorunkid"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@sectorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSectorId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 

    'Pinkal (26-Dec-2018) -- Start
    'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    Public Function isExist(ByVal iEmployeeId As Integer, ByVal iSectorRouteId As Integer) As Boolean
        'Pinkal (26-Dec-2018) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  crempsecrouteunkid " & _
                      ", employeeunkid " & _
                      ", secrouteunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                     "FROM cmassignemp_sector " & _
                     "WHERE isvoid = 0 "

            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            'If intUnkid > 0 Then
            '    strQ &= " AND crempsecrouteunkid <> @crempsecrouteunkid"
            'End If
            If iEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid"
            End If
            If iSectorRouteId > 0 Then
                strQ &= " AND secrouteunkid = @iSectorRouteId"
            End If
            objDataOperation.ClearParameters()
            'objDataOperation.AddParameter("@crempsecrouteunkid", SqlDbType.BigInt, intUnkid.ToString().Length, intUnkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeId)
            objDataOperation.AddParameter("@iSectorRouteId", SqlDbType.Int, eZeeDataType.INT_SIZE, iSectorRouteId)
            'Pinkal (26-Dec-2018) -- End
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmployeeForSector(ByVal strDatabaseName As String, _
                                                             ByVal mstrEmployeeAsonDate As String, _
                                                             ByVal xSectorRouteId As Integer) As String
        Dim mstrEmployeeID As String = ""
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim objDataOperation As New clsDataOperation
        Try

            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(mstrEmployeeAsonDate), eZeeDate.convertDate(mstrEmployeeAsonDate), True, , strDatabaseName)

            strQ = " SELECT STUFF((SELECT ',' +  CAST(hremployee_master.employeeunkid AS NVARCHAR(max)) " & _
               " FROM hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            strQ &= " WHERE employeeunkid NOT IN (SELECT  employeeunkid  FROM cmassignemp_sector WHERE isvoid = 0 AND secrouteunkid = @secrouteunkid ) "

            If xDateFilterQry.Trim.Length > 0 Then
                strQ &= xDateFilterQry
            End If

            strQ &= " AND hremployee_master.isapproved = 1 " & _
                     " ORDER BY hremployee_master.employeeunkid FOR XML PATH('')),1,1,'') AS EmployeeID "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xSectorRouteId)

            dsList = objDataOperation.ExecQuery(strQ, "Employees")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mstrEmployeeID = dsList.Tables(0).Rows(0)("EmployeeID").ToString()
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeForSector; Module Name: " & mstrModuleName)
        End Try
        Return mstrEmployeeID
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAudiTrailForEmployeeSector(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "INSERT INTO atcmassignemp_sector ( " & _
                      " crempsecrouteunkid " & _
                      ", employeeunkid " & _
                      ", secrouteunkid " & _
                      ", AuditType " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", module_name1 " & _
                      ", module_name2 " & _
                      ", module_name3 " & _
                      ", module_name4 " & _
                      ", module_name5 " & _
                      ", isweb " & _
                ") VALUES (" & _
                     " @crempsecrouteunkid " & _
                     ", @employeeunkid " & _
                     ", @secrouteunkid " & _
                     ", @auditType " & _
                     ", @audituserunkid " & _
                     ", getdate() " & _
                     ", @ip " & _
                     ", @machine_name " & _
                     ", @form_name " & _
                     ", @module_name1 " & _
                     ", @module_name2 " & _
                     ", @module_name3 " & _
                     ", @module_name4 " & _
                     ", @module_name5 " & _
                     ", @isweb " & _
                "); "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crempsecrouteunkid", SqlDbType.BigInt, mbintCrempsecrouteunkid.ToString().Length, mbintCrempsecrouteunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@secrouteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSecrouteunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAudiTrailForEmployeeSector; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetSectorFromEmployee(ByVal intEmployeeID As Integer, Optional ByVal iFlag As Boolean = False) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Dim objDataOperation As New clsDataOperation
        Try

            If iFlag Then
                strQ = "SELECT 0 AS crempsecrouteunkid, 0 AS employeeunkid,0 AS secrouteunkid, '' AS EmployeeCode, '' AS Employee , @Select AS Sector UNION "
            End If

            strQ &= " SELECT ISNULL(cmassignemp_sector.crempsecrouteunkid ,0) AS crempsecrouteunkid " & _
                      ", ISNULL(cmassignemp_sector.employeeunkid ,0) AS employeeunkid " & _
                      ", ISNULL(cmassignemp_sector.secrouteunkid ,0) AS secrouteunkid " & _
                      ",ISNULL(hremployee_master.employeecode,'') As EmployeeCode	" & _
                      ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') As Employee " & _
                      ",ISNULL(cfcommon_master.name,'') AS Sector " & _
                      " FROM cmassignemp_sector " & _
                      " LEFT Join hremployee_master ON cmassignemp_sector.employeeunkid = hremployee_master.employeeunkid " & _
                      " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = cmassignemp_sector.secrouteunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SECTOR_ROUTE & _
                      " WHERE cmassignemp_sector.isvoid = 0  AND cmassignemp_sector.employeeunkid  =  @EmployeeID "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
            objDataOperation.AddParameter("@EmployeeID", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSectorFromEmployee; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")
			Language.setMessage(mstrModuleName, 2, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class