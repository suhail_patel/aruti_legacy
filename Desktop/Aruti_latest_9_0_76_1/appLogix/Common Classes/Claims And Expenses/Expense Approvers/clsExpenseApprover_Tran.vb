﻿'************************************************************************************************************************************
'Class Name :clsExpenseApprover_Tran.vb
'Purpose    :
'Date       :06-Feb-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsExpenseApprover_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsExpenseApprover_Tran"
    Dim mstrMessage As String = ""

#Region " Private Variables "

    Private mintExpApproverTranId As Integer = 0
    Private mintExpApproverMasterId As Integer = 0
    Private mdtTran As DataTable

    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
    Private mdtEmployeeAsonDate As Date = Nothing
    'Pinkal (24-Aug-2015) -- End


#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ApproverMasterId
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _ExpApproverMasterId() As Integer
        Get
            Return mintExpApproverMasterId
        End Get
        Set(ByVal value As Integer)
            mintExpApproverMasterId = value
            Call Get_Data()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Datatable
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    ''' <summary>
    ''' Purpose: Get or Set Date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _EmployeeAsonDate() As Date
        Get
            Return mdtEmployeeAsonDate
        End Get
        Set(ByVal value As Date)
            mdtEmployeeAsonDate = value
        End Set
    End Property

    'Pinkal (24-Aug-2015) -- End


#End Region

#Region " Constructor "

    Public Sub New()
        mdtTran = New DataTable("List")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("ischeck")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("crapprovertranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("crapproverunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("userunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ecode")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ename")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("edept")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ejob")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Public & Private Methods "

    Private Sub Get_Data()
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            Dim objData As New clsDataOperation

            StrQ = "SELECT " & _
                   "	 CAST(0 AS BIT) AS ischeck " & _
                   "	,cmexpapprover_tran.crapprovertranunkid " & _
                   "	,cmexpapprover_tran.crapproverunkid " & _
                   "	,cmexpapprover_tran.employeeunkid " & _
                   "	,cmexpapprover_tran.userunkid " & _
                   "	,cmexpapprover_tran.isvoid " & _
                   "	,cmexpapprover_tran.voiddatetime " & _
                   "	,cmexpapprover_tran.voidreason " & _
                   "	,cmexpapprover_tran.voiduserunkid " & _
                   "	,'' AS AUD " & _
                   "	,employeecode AS ecode " & _
                   "	,firstname+' '+surname AS ename " & _
                   "	,name AS edept " & _
                   "	,job_name AS ejob " & _
                   "    ,hremployee_master.employeeunkid " & _
                   "FROM cmexpapprover_tran " & _
                   "	JOIN hremployee_master ON cmexpapprover_tran.employeeunkid = hremployee_master.employeeunkid " & _
                   "    LEFT JOIN " & _
                    "   ( " & _
                    "        SELECT " & _
                    "             jobunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                    " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "	JOIN hrjob_master ON  hrjob_master.jobunkid = Jobs.jobunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "        SELECT " & _
                    "         departmentunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEmployeeAsonDate) & "' " & _
                    "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "	JOIN hrdepartment_master ON  hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                   "	JOIN cmexpapprover_master ON cmexpapprover_tran.crapproverunkid = cmexpapprover_master.crapproverunkid " & _
                   "WHERE cmexpapprover_master.crapproverunkid = '" & mintExpApproverMasterId & "' AND cmexpapprover_tran.isvoid = 0 "

            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()

            For Each dRow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(dRow)
            Next

            objData = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Data", mstrModuleName)
        Finally

        End Try
    End Sub

    Public Function Insert_Update_Delete(ByVal objData As clsDataOperation) As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objData.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO cmexpapprover_tran ( " & _
                                           "  crapproverunkid " & _
                                           ", employeeunkid " & _
                                           ", userunkid " & _
                                           ", isvoid " & _
                                           ", voiddatetime " & _
                                           ", voidreason " & _
                                           ", voiduserunkid " & _
                                       ") VALUES (" & _
                                           "  @crapproverunkid " & _
                                           ", @employeeunkid " & _
                                           ", @userunkid " & _
                                           ", @isvoid " & _
                                           ", @voiddatetime " & _
                                           ", @voidreason " & _
                                           ", @voiduserunkid " & _
                                       "); SELECT @@identity "

                                objData.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpApproverMasterId)
                                objData.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))
                                objData.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid"))
                                objData.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objData.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objData.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))


                                dsList = objData.ExecQuery(StrQ, "List")

                                If objData.ErrorMessage <> "" Then
                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                mintExpApproverTranId = dsList.Tables(0).Rows(0).Item(0)

                                If .Item("crapproverunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objData, "cmexpapprover_master", "crapproverunkid", .Item("crapproverunkid"), "cmexpapprover_tran", "crapprovertranunkid", mintExpApproverTranId, 2, 1, , .Item("userunkid")) = False Then
                                        exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else

                                    'Pinkal (01-Mar-2016) -- Start
                                    'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                                    'If clsCommonATLog.Insert_TranAtLog(objData, "cmexpapprover_master", "crapproverunkid", mintExpApproverMasterId, "cmexpapprover_tran", "crapprovertranunkid", mintExpApproverTranId, 2, 1, , .Item("userunkid")) = False Then
                                    '    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    '    Throw exForce
                                    'End If
                                    If clsCommonATLog.Insert_TranAtLog(objData, "cmexpapprover_master", "crapproverunkid", mintExpApproverMasterId, "cmexpapprover_tran", "crapprovertranunkid", mintExpApproverTranId, 1, 1, , .Item("userunkid")) = False Then
                                        exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                        Throw exForce
                                    End If
                                    'Pinkal (01-Mar-2016) -- End
                                End If

                            Case "U"

                                StrQ = "UPDATE cmexpapprover_tran SET " & _
                                       "  crapproverunkid = @crapproverunkid" & _
                                       ", employeeunkid = @employeeunkid" & _
                                       ", userunkid = @userunkid" & _
                                       ", isvoid = @isvoid" & _
                                       ", voiddatetime = @voiddatetime" & _
                                       ", voidreason = @voidreason" & _
                                       ", voiduserunkid = @voiduserunkid " & _
                                       "WHERE crapprovertranunkid = @crapprovertranunkid "

                                objData.AddParameter("@crapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("crapprovertranunkid"))
                                objData.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("crapproverunkid"))
                                objData.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))
                                objData.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid"))
                                objData.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objData.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objData.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))

                                Call objData.ExecNonQuery(StrQ)

                                If objData.ErrorMessage <> "" Then
                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_TranAtLog(objData, "cmexpapprover_master", "crapproverunkid", .Item("crapproverunkid"), "cmexpapprover_tran", "crapprovertranunkid", .Item("crapprovertranunkid"), 2, 2, , .Item("userunkid")) = False Then
                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"

                                If .Item("crapprovertranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objData, "cmexpapprover_master", "crapproverunkid", .Item("crapproverunkid"), "cmexpapprover_tran", "crapprovertranunkid", .Item("crapprovertranunkid"), 2, 3, , .Item("userunkid")) = False Then
                                        exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                                StrQ = "UPDATE cmexpapprover_tran SET " & _
                                       "  isvoid = @isvoid " & _
                                       ", voiddatetime = @voiddatetime " & _
                                       ", voidreason = @voidreason " & _
                                       ", voiduserunkid = @voiduserunkid " & _
                                       "WHERE crapprovertranunkid = @crapprovertranunkid "

                                objData.AddParameter("@crapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("crapprovertranunkid"))
                                objData.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objData.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objData.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))

                                Call objData.ExecNonQuery(StrQ)

                                If objData.ErrorMessage <> "" Then
                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Insert_Update_Delete", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function IsUserMapped(ByVal iUserId As Integer, ByRef sMsg As String) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Dim objData As New clsDataOperation

            StrQ = "SELECT " & _
                   "	 employeecode AS ECode " & _
                   "	,firstname+' '+surname AS EName " & _
                   "FROM hrapprover_usermapping " & _
                   "	JOIN cmexpapprover_master ON hrapprover_usermapping.approverunkid = cmexpapprover_master.crapproverunkid " & _
                   "	JOIN hremployee_master ON cmexpapprover_master.employeeunkid = hremployee_master.employeeunkid " & _
                   "WHERE usertypeid = '" & enUserType.crApprover & "' AND hrapprover_usermapping.userunkid = '" & iUserId & "' "

            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                sMsg = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot map selected user with this employee." & vbCrLf & _
                                           "Reason user is already mapped with [ Code : ") & dsList.Tables(0).Rows(0).Item("ECode") & _
                Language.getMessage(mstrModuleName, 2, " Employee : ") & dsList.Tables(0).Rows(0).Item("EName") & Language.getMessage(mstrModuleName, 3, " ]. Please select new user to map.")
                Return True
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsUserMapped", mstrModuleName)
            Return True
        Finally
        End Try
    End Function


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function Perform_Migration(ByVal iFromApproverUnkid As Integer, ByVal iToApproverUnkid As Integer, ByVal iDataTable As DataTable, ByVal iUserId As Integer, ByVal iToApproverEmpID As Integer) As Boolean
    Public Function Perform_Migration(ByVal iFromApproverUnkid As Integer, ByVal iToApproverUnkid As Integer, ByVal iDataTable As DataTable, ByVal iUserId As Integer, ByVal iToApproverEmpID As Integer, ByVal dtCurrentDateAndTime As DateTime, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'Shani(24-Aug-2015) -- End
        'Gajanan [23-SEP-2019] -- Add {xDataOpr}    

        Dim exForce As Exception
        Dim iblnVoidMaster As Boolean = False


        'Gajanan [23-SEP-2019] -- Start    
        'Enhancement:Enforcement of approval migration From Transfer and recategorization.
        'Dim objDataOperation As New clsDataOperation
        Dim objDataOperation As clsDataOperation
        'Gajanan [23-SEP-2019] -- End


        Dim iMigratedEmpId As String = String.Empty
        Dim strQ As String = ""
        Try
            _ExpApproverMasterId = iFromApproverUnkid
            If mdtTran.Rows.Count = iDataTable.Rows.Count Then iblnVoidMaster = True


            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'objDataOperation.BindTransaction()
            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            End If
            'Gajanan [23-SEP-2019] -- End


            For Each dRow As DataRow In iDataTable.Rows
                mdtTran.Rows.Clear()
                With dRow
                    .Item("AUD") = "D"

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    '.Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    .Item("voiddatetime") = dtCurrentDateAndTime
                    'Shani(24-Aug-2015) -- End

                    .Item("isvoid") = True
                    .Item("voidreason") = Language.getMessage(mstrModuleName, 4, "Migrated to other Approver.")
                    .Item("voiduserunkid") = iUserId
                    .Item("crapprovertranunkid") = dRow.Item("crapprovertranunkid")
                    .Item("crapproverunkid") = iFromApproverUnkid
                End With
                mdtTran.ImportRow(dRow)

                mintExpApproverMasterId = iFromApproverUnkid

                If Insert_Update_Delete(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mdtTran.Rows.Clear()
                With dRow
                    .Item("AUD") = "A"
                    .Item("voiddatetime") = DBNull.Value
                    .Item("isvoid") = False
                    .Item("voidreason") = ""
                    .Item("voiduserunkid") = 0
                    .Item("crapproverunkid") = iToApproverUnkid
                End With
                mdtTran.ImportRow(dRow)

                mintExpApproverMasterId = iToApproverUnkid

                If Insert_Update_Delete(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                'Pinkal (19-Mar-2015) -- Start
                'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.

                Dim objClaim As New clsclaim_request_master
                Dim mstrFormId As String = objClaim.GetApproverPendingClaimForm(iFromApproverUnkid, dRow.Item("employeeunkid").ToString(), objDataOperation)

                If mstrFormId.Trim.Length > 0 Then
                    strQ = " Update cmclaim_approval_tran set approveremployeeunkid  = @newapproveremployeeunkid,crapproverunkid = @newcrapproverunkid  WHERE crmasterunkid in (" & mstrFormId & ") AND crapproverunkid = @oldcrapproverunkid AND cmclaim_approval_tran.isvoid = 0 "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@newapproveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iToApproverEmpID)
                    objDataOperation.AddParameter("@newcrapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iToApproverUnkid)
                    objDataOperation.AddParameter("@oldcrapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iFromApproverUnkid)
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    strQ = "Select ISNULL(crapprovaltranunkid,0) crapprovaltranunkid FROM cmclaim_approval_tran WHERE crapproverunkid = @crapproverunkid AND approveremployeeunkid = @approveremployeeunkid AND crmasterunkid  in (" & mstrFormId & ") AND isvoid = 0"
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iToApproverUnkid)
                    objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iToApproverEmpID)
                    Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    If dsList.Tables(0).Rows.Count > 0 Then

                        For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "cmclaim_approval_tran", "crapprovaltranunkid", dsList.Tables(0).Rows(k)("crapprovaltranunkid").ToString(), False) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        Next

                    End If

                End If

                'Pinkal (19-Mar-2015) -- End


                iMigratedEmpId &= "," & dRow.Item("employeeunkid")
            Next


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            strQ = " SELECT count(*) as Empcount FROM cmexpapprover_tran WHERE crapproverunkid = @crapproverunkid AND isvoid = 0  "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iFromApproverUnkid)
            Dim dsCount As DataSet = objDataOperation.ExecQuery(strQ, "List")
            If dsCount IsNot Nothing AndAlso dsCount.Tables(0).Rows.Count > 0 Then
                If dsCount.Tables(0).Rows(0)("Empcount") <= 0 Then
                    iblnVoidMaster = True
                End If
            Else
                iblnVoidMaster = True
            End If
            'Pinkal (06-Jan-2016) -- End


            If iblnVoidMaster = True Then
                Dim objExApprMaster As New clsExpenseApprover_Master
                objExApprMaster._Isvoid = True

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objExApprMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objExApprMaster._Voiddatetime = dtCurrentDateAndTime
                'Shani(24-Aug-2015) -- End

                objExApprMaster._Voidreason = Language.getMessage(mstrModuleName, 5, "Employee Migrated")
                objExApprMaster._Voiduserunkid = iUserId
                If objExApprMaster.Delete(iFromApproverUnkid, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objExApprMaster = Nothing
            End If

            If iMigratedEmpId.Trim.Length > 0 Then
                iMigratedEmpId = Mid(iMigratedEmpId, 2)
                Dim objMigration As New clsMigration
                objMigration._Migratedemployees = iMigratedEmpId

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objMigration._Migrationdate = ConfigParameter._Object._CurrentDateAndTime
                objMigration._Migrationdate = dtCurrentDateAndTime
                'Shani(24-Aug-2015) -- End

                objMigration._Migrationfromunkid = iFromApproverUnkid
                objMigration._Migrationtounkid = iToApproverUnkid
                objMigration._Usertypeid = enUserType.crApprover
                objMigration._Userunkid = iUserId

                If objMigration.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objMigration = Nothing

            End If

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'Gajanan [23-SEP-2019] -- End

            Return True
        Catch ex As Exception

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'objDataOperation.ReleaseTransaction(False)
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan [23-SEP-2019] -- End

            DisplayError.Show("-1", ex.Message, "Perform_Migration", mstrModuleName)
        Finally
        End Try
    End Function




    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
    Public Function GetApproverTranIdFromEmployeeAndApprover(ByVal ApproverId As Integer, ByVal EmployeeId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = " SELECT ISNULL(crapprovertranunkid,0) as crapprovertranunkid FROM cmexpapprover_tran WHERE crapproverunkid = @crapproverunkid AND employeeunkid = @employeeunkid AND isvoid = 0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, ApproverId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, EmployeeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables("List").Rows(0)("crapprovertranunkid").ToString())
            End If

            Return 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverTranIdFromEmployeeAndApprover; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try



    End Function
    'Gajanan [23-SEP-2019] -- End

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, you cannot map selected user with this employee." & vbCrLf & _
                                           "Reason user is already mapped with [ Code :")
			Language.setMessage(mstrModuleName, 2, " Employee :")
			Language.setMessage(mstrModuleName, 3, " ]. Please select new user to map.")
			Language.setMessage(mstrModuleName, 4, "Migrated to other Approver.")
			Language.setMessage(mstrModuleName, 5, "Employee Migrated")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
