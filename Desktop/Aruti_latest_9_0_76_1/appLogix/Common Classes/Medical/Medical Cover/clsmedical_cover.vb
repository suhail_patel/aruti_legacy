﻿'************************************************************************************************************************************
'Class Name : clsmedical_cover.vb
'Purpose    :
'Date       :20/07/2010
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsmedical_cover
    Private Shared ReadOnly mstrModuleName As String = "clsmedical_cover"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintMedicalcoverunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintDependentsunkid As Integer
    Private mintServiceunkid As Integer
    Private mintMedicalcategoryunkid As Integer
    Private mintCovermasterunkid As Integer
    Private mdecAmount As Decimal
    Private mstrRemark As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    'S.SANDEEP [ 06 OCT 2014 ] -- START
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    'S.SANDEEP [ 06 OCT 2014 ] -- END

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set medicalcoverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Medicalcoverunkid() As Integer
        Get
            Return mintMedicalcoverunkid
        End Get
        Set(ByVal value As Integer)
            mintMedicalcoverunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dependantsunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Dependentsunkid() As Integer
        Get
            Return mintDependentsunkid
        End Get
        Set(ByVal value As Integer)
            mintDependentsunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set serviceunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Serviceunkid() As Integer
        Get
            Return mintServiceunkid
        End Get
        Set(ByVal value As Integer)
            mintServiceunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set medicalcategoryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Medicalcategoryunkid() As Integer
        Get
            Return mintMedicalcategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintMedicalcategoryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set covermasterunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Covermasterunkid() As Integer
        Get
            Return mintCovermasterunkid
        End Get
        Set(ByVal value As Integer)
            mintCovermasterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set amount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Amount() As Decimal 'Sohail (17 Jun 2011)
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal) 'Sohail (17 Jun 2011)
            mdecAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property


    'S.SANDEEP [ 06 OCT 2014 ] -- START
    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _StartDate() As Date
        Get
            Return mdtStartDate
        End Get
        Set(ByVal value As Date)
            mdtStartDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enddate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _EndDate() As Date
        Get
            Return mdtEndDate
        End Get
        Set(ByVal value As Date)
            mdtEndDate = value
        End Set
    End Property
    'S.SANDEEP [ 06 OCT 2014 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  medicalcoverunkid " & _
              ", employeeunkid " & _
              ", dependantsunkid " & _
              ", serviceunkid " & _
              ", medicalcategoryunkid " & _
              ", covermasterunkid " & _
              ", amount " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", startdate " & _
              ", enddate " & _
             "FROM mdmedical_cover " & _
             "WHERE medicalcoverunkid = @medicalcoverunkid "
            'S.SANDEEP [ 06 OCT 2014 ] -- START {startdate,enddate} -- END

            objDataOperation.AddParameter("@medicalcoverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMedicalcoverunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintMedicalcoverunkid = CInt(dtRow.Item("medicalcoverunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintDependentsunkid = CInt(dtRow.Item("dependantsunkid"))
                mintServiceunkid = CInt(dtRow.Item("serviceunkid"))
                mintMedicalcategoryunkid = CInt(dtRow.Item("medicalcategoryunkid"))
                mintCovermasterunkid = CInt(dtRow.Item("covermasterunkid"))
                mdecAmount = CDec(dtRow.Item("amount"))
                mstrRemark = dtRow.Item("remark").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If mdtVoiddatetime <> Nothing Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString

                'S.SANDEEP [ 06 OCT 2014 ] -- START
                If IsDBNull(dtRow.Item("startdate")) Then
                    mdtStartDate = Nothing
                Else
                    mdtStartDate = dtRow.Item("startdate")
                End If

                If IsDBNull(dtRow.Item("enddate")) Then
                    mdtEndDate = Nothing
                Else
                    mdtEndDate = dtRow.Item("enddate")
                End If
                'S.SANDEEP [ 06 OCT 2014 ] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    '''Shani(24-Aug-2015) -- Start
    '''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '''Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    Public Function GetList(ByVal strDatabaseName As String, _
                            ByVal intUserUnkid As Integer, _
                            ByVal intYearUnkid As Integer, _
                            ByVal intCompanyUnkid As Integer, _
                            ByVal dtPeriodStart As Date, _
                            ByVal dtPeriodEnd As Date, _
                            ByVal strUserModeSetting As String, _
                            ByVal blnOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        'Shani(24-Aug-2015) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , strDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkid, strUserModeSetting)
            'Shani(24-Aug-2015) -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            strQ = "SELECT * " & _
                       "INTO #TableDepn " & _
                       "FROM " & _
                       "( " & _
                           "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                           "FROM hrdependant_beneficiaries_status_tran "

            strQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If dtPeriodEnd <> Nothing Then
                strQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
                objDataOperation.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            strQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End

            strQ &= "SELECT " & _
              "  medicalcoverunkid " & _
              ", mdmedical_cover.employeeunkid " & _
              ", isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') as employeename" & _
              ", dependantsunkid " & _
              ", isnull(hrdependants_beneficiaries_tran.first_name,'') + ' ' +  isnull(hrdependants_beneficiaries_tran.last_name,'') as dependents " & _
              ", mdmedical_cover.serviceunkid " & _
              ", m1.mdmastername as service " & _
              ", m1.mdserviceno as serviceno " & _
              ", mdmedical_cover.medicalcategoryunkid " & _
              ", mdmedical_master.mdmastername as category" & _
              ", mdmedical_cover.covermasterunkid " & _
              ", amount " & _
              ", mdmedical_cover.remark " & _
              ", mdmedical_cover.userunkid " & _
              ", mdmedical_cover.isvoid " & _
              ", mdmedical_cover.voiduserunkid " & _
              ", mdmedical_cover.voiddatetime " & _
              ", mdmedical_cover.voidreason " & _
              ", CONVERT(CHAR(8),mdmedical_cover.startdate,112) AS startdate " & _
              ", CONVERT(CHAR(8),mdmedical_cover.enddate,112) AS enddate " & _
             " FROM mdmedical_cover " & _
             " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = mdmedical_cover.employeeunkid " & _
             " LEFT JOIN mdmedical_master on mdmedical_master.mdmasterunkid = mdmedical_cover.medicalcategoryunkid " & _
             " LEFT JOIN mdmedical_master m1 on m1.mdmasterunkid = mdmedical_cover.serviceunkid " & _
             " LEFT JOIN hrdependants_beneficiaries_tran on hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = mdmedical_cover.dependantsunkid " & _
             "LEFT JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                    "AND #TableDepn.isactive = 1 " & _
             " AND hrdependants_beneficiaries_tran.isdependant = 1 "
            'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]
            'S.SANDEEP [ 06 OCT 2014 ] -- START {startdate,enddate} -- END


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'Shani(24-Aug-2015) -- End

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If blnOnlyActive Then
            '    strQ &= " WHERE mdmedical_cover.isvoid = 0 "
            'End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'Shani(24-Aug-2015) -- End

            strQ &= " WHERE 1 = 1 "

            If blnOnlyActive Then
                strQ &= " AND mdmedical_cover.isvoid = 0 "
            End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    If blnOnlyActive Then
            '        strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            '    Else
            '        strQ &= "  WHERE hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            '    End If
            'End If

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            'Select Case ConfigParameter._Object._UserAccessModeSetting
            '    Case enAllocation.BRANCH
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.DEPARTMENT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.SECTION
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.UNIT
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.TEAM
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOB_GROUP
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        End If
            '    Case enAllocation.JOBS
            '        If UserAccessLevel._AccessLevel.Length > 0 Then
            '            strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
            '        End If
            'End Select

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'strQ &= UserAccessLevel._AccessLevelFilterString
            ''S.SANDEEP [ 01 JUNE 2012 ] -- END


            ''S.SANDEEP [ 04 FEB 2012 ] -- END

            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    'Sohail (06 Jan 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    'strQ &= " AND hremployee_master.isactive = 1 "
            '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    'Sohail (06 Jan 2012) -- End
            'End If
            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry & " "
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry & " "
                End If
            End If
            'Shani(24-Aug-2015) -- End

            strQ &= "Order by mdmedical_cover.medicalcategoryunkid"

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            strQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (mdmedical_cover) </purpose>
    Public Function Insert(ByVal mstrEmployeeunkid As String) As Boolean

        If mstrEmployeeunkid.Length <= 0 Then
            Return True
        End If

        If isExist(mintMedicalcategoryunkid, mintEmployeeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Employee is already assigned to this cover. Please assign new Cover.")
            Return False
        End If


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = "INSERT INTO mdmedical_cover ( " & _
              "  employeeunkid " & _
              ", dependantsunkid " & _
              ", serviceunkid " & _
              ", medicalcategoryunkid " & _
              ", covermasterunkid " & _
              ", amount " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", startdate " & _
              ", enddate " & _
            ") VALUES (" & _
              "  @employeeunkid " & _
              ", @dependantsunkid " & _
              ", @serviceunkid " & _
              ", @medicalcategoryunkid " & _
              ", @covermasterunkid " & _
              ", @amount " & _
              ", @remark " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @startdate " & _
              ", @enddate " & _
            "); SELECT @@identity"
            'S.SANDEEP [ 06 OCT 2014 ] -- START {startdate,enddate} -- END


            Dim arEmpID As String() = mstrEmployeeunkid.Split(",")

            For i As Integer = 0 To arEmpID.Length - 1

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, arEmpID(i).ToString())
                objDataOperation.AddParameter("@dependantsunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependentsunkid.ToString)
                objDataOperation.AddParameter("@serviceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintServiceunkid.ToString)
                objDataOperation.AddParameter("@medicalcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMedicalcategoryunkid.ToString)
                objDataOperation.AddParameter("@covermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCovermasterunkid.ToString)
                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                'S.SANDEEP [ 06 OCT 2014 ] -- START
                If mdtStartDate <> Nothing Then
                    objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartDate)
                Else
                    objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If

                If mdtEndDate <> Nothing Then
                    objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEndDate)
                Else
                    objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                'S.SANDEEP [ 06 OCT 2014 ] -- END


                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintMedicalcoverunkid = dsList.Tables(0).Rows(0).Item(0)

                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "mdmedical_cover", "medicalcoverunkid", mintMedicalcoverunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (mdmedical_cover) </purpose>
    Public Function Update(ByVal mstrEmployeeunkid As String) As Boolean

        If mstrEmployeeunkid.Length <= 0 Then
            Return True
        End If

        If isExist(mintMedicalcategoryunkid, mintEmployeeunkid, mintMedicalcoverunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Employee is already assigned to this cover. Please assign new Cover.")
            Return False
        End If


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try


            strQ = "UPDATE mdmedical_cover SET " & _
              "  employeeunkid = @employeeunkid" & _
              ", dependantsunkid = @dependantsunkid" & _
              ", serviceunkid = @serviceunkid" & _
              ", medicalcategoryunkid = @medicalcategoryunkid" & _
              ", covermasterunkid = @covermasterunkid" & _
              ", amount = @amount" & _
              ", remark = @remark" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", startdate = @startdate " & _
              ", enddate = @enddate " & _
            "WHERE medicalcoverunkid = @medicalcoverunkid "
            'S.SANDEEP [ 06 OCT 2014 ] -- START {startdate,enddate} -- END

            Dim arEmpID As String() = mstrEmployeeunkid.Split(",")

            For i As Integer = 0 To arEmpID.Length - 1

                objDataOperation.ClearParameters()
                'S.SANDEEP [ 06 OCT 2014 ] -- START
                'objDataOperation.AddParameter("@medicalcoverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, arEmpID(i).ToString)
                'objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)

                objDataOperation.AddParameter("@medicalcoverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMedicalcoverunkid)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, arEmpID(i).ToString)
                'S.SANDEEP [ 06 OCT 2014 ] -- END
                objDataOperation.AddParameter("@dependantsunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependentsunkid.ToString)
                objDataOperation.AddParameter("@serviceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintServiceunkid.ToString)
                objDataOperation.AddParameter("@medicalcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMedicalcategoryunkid.ToString)
                objDataOperation.AddParameter("@covermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCovermasterunkid.ToString)
                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                'S.SANDEEP [ 06 OCT 2014 ] -- START
                If mdtStartDate <> Nothing Then
                    objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartDate)
                Else
                    objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If

                If mdtEndDate <> Nothing Then
                    objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEndDate)
                Else
                    objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                'S.SANDEEP [ 06 OCT 2014 ] -- END

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If clsCommonATLog.IsTableDataUpdate("atcommon_log", "mdmedical_cover", mintMedicalcoverunkid, "medicalcoverunkid", 2) Then
                    If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "mdmedical_cover", "medicalcoverunkid", mintMedicalcoverunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

            Next

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (mdmedical_cover) </purpose>
    Public Function Delete(ByVal mstrCoverunkid As String) As Boolean

        If mstrCoverunkid.Trim.Length <= 0 Then Return True

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "Update mdmedical_cover set isvoid = 1,voiddatetime = @voiddatetime,voidreason=@voidreson,voiduserunkid = @voiduserunkid " & _
            "WHERE medicalcoverunkid = @medicalcoverunkid "

            Dim arCoverID As String() = mstrCoverunkid.Trim.Split(",")

            For i As Integer = 0 To arCoverID.Length - 1

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@voidreson", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                objDataOperation.AddParameter("@medicalcoverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, arCoverID(i).ToString())

                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "mdmedical_cover", "medicalcoverunkid", arCoverID(i)) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@medicalcoverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intMedicalCategoryunkid As Integer, Optional ByVal intEmployeeunkid As Integer = -1, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  medicalcoverunkid " & _
              ", employeeunkid " & _
              ", dependantsunkid " & _
              ", serviceunkid " & _
              ", medicalcategoryunkid " & _
              ", covermasterunkid " & _
              ", amount " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", startdate " & _
              ", enddate " & _
              " FROM mdmedical_cover " & _
              " WHERE medicalcategoryunkid = @medicalcategoryunkid " & _
              " AND isvoid = 0 "
            'S.SANDEEP [ 06 OCT 2014 ] -- START {startdate,enddate} -- END

            objDataOperation.ClearParameters()

            If intEmployeeunkid > 0 Then
                strQ &= " AND employeeunkid=@employeeunkid and isnull(dependantsunkid,0)= 0 "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            End If


            If intUnkid > 0 Then
                strQ &= " AND medicalcoverunkid <> @medicalcoverunkid"
                objDataOperation.AddParameter("@medicalcoverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@medicalcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMedicalCategoryunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Employee is already assigned to this cover. Please assign new Cover.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class


'Public Class clsmedical_cover
'    Private Shared Readonly mstrModuleName As String = "clsmedical_cover"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "
'    Private mintMedicalcoverunkid As Integer
'    Private mintEmployeeunkid As Integer
'    Private mintDependentsunkid As Integer
'    Private mintServiceunkid As Integer
'    ' Private mintMembershiptypeunkid As Integer
'    '  Private mstrMembershipno As String = String.Empty
'    Private mintMedicalcategoryunkid As Integer
'    Private mintCovermasterunkid As Integer
'    'Sohail (17 Jun 2011) -- Start
'    'Private mdblAmount As Double
'    Private mdecAmount As Decimal
'    'Sohail (17 Jun 2011) -- End
'    Private mstrRemark As String = String.Empty
'    Private mintUserunkid As Integer
'    Private mblnIsvoid As Boolean
'    Private mintVoiduserunkid As Integer
'    Private mdtVoiddatetime As Date
'    Private mstrVoidreason As String = String.Empty
'#End Region

'#Region " Properties "
'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set medicalcoverunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Medicalcoverunkid() As Integer
'        Get
'            Return mintMedicalcoverunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintMedicalcoverunkid = Value
'            Call getData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set employeeunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Employeeunkid() As Integer
'        Get
'            Return mintEmployeeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintEmployeeunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set dependantsunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Dependentsunkid() As Integer
'        Get
'            Return mintDependentsunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintDependentsunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set serviceunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Serviceunkid() As Integer
'        Get
'            Return mintServiceunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintServiceunkid = value
'        End Set
'    End Property

'    '''' <summary>
'    '''' Purpose: Get or Set membershiptypeunkid
'    '''' Modify By: Pinkal
'    '''' </summary>
'    'Public Property _Membershiptypeunkid() As Integer
'    '    Get
'    '        Return mintMembershiptypeunkid
'    '    End Get
'    '    Set(ByVal value As Integer)
'    '        mintMembershiptypeunkid = Value
'    '    End Set
'    'End Property

'    '''' <summary>
'    '''' Purpose: Get or Set membershipno
'    '''' Modify By: Pinkal
'    '''' </summary>
'    'Public Property _Membershipno() As String
'    '    Get
'    '        Return mstrMembershipno
'    '    End Get
'    '    Set(ByVal value As String)
'    '        mstrMembershipno = Value
'    '    End Set
'    'End Property

'    ''' <summary>
'    ''' Purpose: Get or Set medicalcategoryunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Medicalcategoryunkid() As Integer
'        Get
'            Return mintMedicalcategoryunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintMedicalcategoryunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set covermasterunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Covermasterunkid() As Integer
'        Get
'            Return mintCovermasterunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintCovermasterunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set amount
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Amount() As Decimal 'Sohail (17 Jun 2011)
'        Get
'            Return mdecAmount
'        End Get
'        Set(ByVal value As Decimal) 'Sohail (17 Jun 2011)
'            mdecAmount = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set remark
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Remark() As String
'        Get
'            Return mstrRemark
'        End Get
'        Set(ByVal value As String)
'            mstrRemark = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set userunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Userunkid() As Integer
'        Get
'            Return mintUserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintUserunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isvoid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Isvoid() As Boolean
'        Get
'            Return mblnIsvoid
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsvoid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiduserunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Voiduserunkid() As Integer
'        Get
'            Return mintVoiduserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintVoiduserunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiddatetime
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Voiddatetime() As Date
'        Get
'            Return mdtVoiddatetime
'        End Get
'        Set(ByVal value As Date)
'            mdtVoiddatetime = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voidreason
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Voidreason() As String
'        Get
'            Return mstrVoidreason
'        End Get
'        Set(ByVal value As String)
'            mstrVoidreason = value
'        End Set
'    End Property

'#End Region

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  medicalcoverunkid " & _
'              ", employeeunkid " & _
'              ", dependantsunkid " & _
'              ", serviceunkid " & _
'              ", medicalcategoryunkid " & _
'              ", covermasterunkid " & _
'              ", amount " & _
'              ", remark " & _
'              ", userunkid " & _
'              ", isvoid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime " & _
'              ", voidreason " & _
'             "FROM mdmedical_cover " & _
'             "WHERE medicalcoverunkid = @medicalcoverunkid "

'            objDataOperation.AddParameter("@medicalcoverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMedicalcoverunkid.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            '", membershiptypeunkid " & _
'            '", membershipno " & _


'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintMedicalcoverunkid = CInt(dtRow.Item("medicalcoverunkid"))
'                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
'                mintDependentsunkid = CInt(dtRow.Item("dependantsunkid"))
'                mintServiceunkid = CInt(dtRow.Item("serviceunkid"))
'                '  mintmembershiptypeunkid = CInt(dtRow.Item("membershiptypeunkid"))
'                '  mstrmembershipno = dtRow.Item("membershipno").ToString
'                mintMedicalcategoryunkid = CInt(dtRow.Item("medicalcategoryunkid"))
'                mintCovermasterunkid = CInt(dtRow.Item("covermasterunkid"))
'                mdecAmount = CDec(dtRow.Item("amount"))
'                mstrRemark = dtRow.Item("remark").ToString
'                mintUserunkid = CInt(dtRow.Item("userunkid"))
'                mblnIsvoid = CBool(dtRow.Item("isvoid"))
'                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
'                If mdtVoiddatetime <> Nothing Then
'                    mdtVoiddatetime = dtRow.Item("voiddatetime")
'                End If
'                mstrVoidreason = dtRow.Item("voidreason").ToString
'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub


'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  medicalcoverunkid " & _
'              ", mdmedical_cover.employeeunkid " & _
'              ", isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') as employeename" & _
'              ", dependantsunkid " & _
'              ", isnull(hrdependants_beneficiaries_tran.first_name,'') + ' ' +  isnull(hrdependants_beneficiaries_tran.last_name,'') as dependents " & _
'              ", mdmedical_cover.serviceunkid " & _
'              ", m1.mdmastername as service " & _
'              ", m1.mdserviceno as serviceno " & _
'              ", mdmedical_cover.medicalcategoryunkid " & _
'              ", mdmedical_master.mdmastername as category" & _
'              ", mdmedical_cover.covermasterunkid " & _
'              ", amount " & _
'              ", mdmedical_cover.remark " & _
'              ", mdmedical_cover.userunkid " & _
'              ", mdmedical_cover.isvoid " & _
'              ", mdmedical_cover.voiduserunkid " & _
'              ", mdmedical_cover.voiddatetime " & _
'              ", mdmedical_cover.voidreason " & _
'             " FROM mdmedical_cover " & _
'             " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = mdmedical_cover.employeeunkid " & _
'             " LEFT JOIN mdmedical_master on mdmedical_master.mdmasterunkid = mdmedical_cover.medicalcategoryunkid " & _
'             " LEFT JOIN mdmedical_master m1 on m1.mdmasterunkid = mdmedical_cover.serviceunkid " & _
'             " LEFT JOIN hrdependants_beneficiaries_tran on hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = mdmedical_cover.dependantsunkid " & _
'             " AND hrdependants_beneficiaries_tran.isdependant = 1 "

'            If blnOnlyActive Then
'                strQ &= " WHERE mdmedical_cover.isvoid = 0 "
'            End If


'            'Anjan (24 Jun 2011)-Start
'            'Issue : According to privilege that lower level user should not see superior level employees.
'            If UserAccessLevel._AccessLevel.Length > 0 Then
'                If blnOnlyActive Then
'                    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
'                Else
'                    strQ &= "  WHERE hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
'                End If
'            End If
'            'Anjan (24 Jun 2011)-End 


'            'Pinkal (24-Jun-2011) -- Start
'            'ISSUE : CHECK FOR ACTIVE EMPLOYEE
'            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'                strQ &= " AND hremployee_master.isactive = 1 "
'            End If
'            'Pinkal (24-Jun-2011) -- End




'            '", mdmedical_cover.membershiptypeunkid " & _
'            '", mdmedical_cover.membershipno " & _

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function


'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (mdmedical_cover) </purpose>
'    Public Function Insert(ByVal isdependent As Boolean) As Boolean

'        If isdependent = False Then

'            If isExist(mintMedicalcategoryunkid, mintServiceunkid, mintCovermasterunkid, mintEmployeeunkid) Then
'                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Employee has already assign this cover. Please assign new Cover.")
'                Return False
'            End If

'        Else

'            If isExist(mintMedicalcategoryunkid, mintServiceunkid, mintCovermasterunkid, -1, mintDependentsunkid) Then
'                mstrMessage = Language.getMessage(mstrModuleName, 2, "This Dependants has already assign this cover. Please assign new Cover.")
'                Return False
'            End If

'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
'            objDataOperation.AddParameter("@dependantsunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependentsunkid.ToString)
'            objDataOperation.AddParameter("@serviceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintServiceunkid.ToString)
'            ' objDataOperation.AddParameter("@membershiptypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMembershiptypeunkid.ToString)
'            'objDataOperation.AddParameter("@membershipno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMembershipno.ToString)
'            objDataOperation.AddParameter("@medicalcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMedicalcategoryunkid.ToString)
'            objDataOperation.AddParameter("@covermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCovermasterunkid.ToString)
'            'Sohail (17 Jun 2011) -- Start
'            'objDataOperation.AddParameter("@amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdecAmount.ToString)
'            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
'            'Sohail (17 Jun 2011) -- End
'            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'            strQ = "INSERT INTO mdmedical_cover ( " & _
'              "  employeeunkid " & _
'              ", dependantsunkid " & _
'              ", serviceunkid " & _
'              ", medicalcategoryunkid " & _
'              ", covermasterunkid " & _
'              ", amount " & _
'              ", remark " & _
'              ", userunkid " & _
'              ", isvoid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime " & _
'              ", voidreason" & _
'            ") VALUES (" & _
'              "  @employeeunkid " & _
'              ", @dependantsunkid " & _
'              ", @serviceunkid " & _
'              ", @medicalcategoryunkid " & _
'              ", @covermasterunkid " & _
'              ", @amount " & _
'              ", @remark " & _
'              ", @userunkid " & _
'              ", @isvoid " & _
'              ", @voiduserunkid " & _
'              ", @voiddatetime " & _
'              ", @voidreason" & _
'            "); SELECT @@identity"

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintMedicalcoverunkid = dsList.Tables(0).Rows(0).Item(0)

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (mdmedical_cover) </purpose>
'    Public Function Update(ByVal isdependent As Boolean) As Boolean

'        If isdependent = False Then

'            If isExist(mintMedicalcategoryunkid, mintServiceunkid, mintCovermasterunkid, mintEmployeeunkid, -1, mintMedicalcoverunkid) Then
'                mstrMessage = Language.getMessage(mstrModuleName, 1, "This Employee has already assign this cover. Please assign new Cover.")
'                Return False
'            End If

'        Else

'            If isExist(mintMedicalcategoryunkid, mintServiceunkid, mintCovermasterunkid, -1, mintDependentsunkid, mintMedicalcoverunkid) Then
'                mstrMessage = Language.getMessage(mstrModuleName, 2, "This Dependants has already assign this cover. Please assign new Cover.")
'                Return False
'            End If

'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@medicalcoverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMedicalcoverunkid.ToString)
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
'            objDataOperation.AddParameter("@dependantsunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependentsunkid.ToString)
'            objDataOperation.AddParameter("@serviceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintServiceunkid.ToString)
'            objDataOperation.AddParameter("@medicalcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMedicalcategoryunkid.ToString)
'            objDataOperation.AddParameter("@covermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCovermasterunkid.ToString)
'            'Sohail (17 Jun 2011) -- Start
'            'objDataOperation.AddParameter("@amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdecAmount.ToString)
'            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
'            'Sohail (17 Jun 2011) -- End
'            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'            strQ = "UPDATE mdmedical_cover SET " & _
'              "  employeeunkid = @employeeunkid" & _
'              ", dependantsunkid = @dependantsunkid" & _
'              ", serviceunkid = @serviceunkid" & _
'              ", medicalcategoryunkid = @medicalcategoryunkid" & _
'              ", covermasterunkid = @covermasterunkid" & _
'              ", amount = @amount" & _
'              ", remark = @remark" & _
'              ", userunkid = @userunkid" & _
'              ", isvoid = @isvoid" & _
'              ", voiduserunkid = @voiduserunkid" & _
'              ", voiddatetime = @voiddatetime" & _
'              ", voidreason = @voidreason " & _
'            "WHERE medicalcoverunkid = @medicalcoverunkid "

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (mdmedical_cover) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer) As Boolean
'        'If isUsed(intUnkid) Then
'        '    mstrMessage = "<Message>"
'        '    Return False
'        'End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "Update mdmedical_cover set isvoid = 1,voiddatetime = @voiddatetime,voidreason=@voidreson,voiduserunkid = @voiduserunkid " & _
'            "WHERE medicalcoverunkid = @medicalcoverunkid "

'            objDataOperation.AddParameter("@voidreson", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
'            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            objDataOperation.AddParameter("@medicalcoverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "<Query>"

'            objDataOperation.AddParameter("@medicalcoverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(ByVal intMedicalCategoryunkid As Integer, ByVal intServiceunkid As Integer, ByVal intCoverunkid As Integer, Optional ByVal intEmployeeunkid As Integer = -1, Optional ByVal intdependentsunkid As Integer = -1, Optional ByVal intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  medicalcoverunkid " & _
'              ", employeeunkid " & _
'              ", dependantsunkid " & _
'              ", serviceunkid " & _
'              ", medicalcategoryunkid " & _
'              ", covermasterunkid " & _
'              ", amount " & _
'              ", remark " & _
'              ", userunkid " & _
'              ", isvoid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime " & _
'              ", voidreason " & _
'              " FROM mdmedical_cover " & _
'              " WHERE medicalcategoryunkid = @medicalcategoryunkid " & _
'              " AND serviceunkid = @serviceunkid " & _
'              " AND covermasterunkid = @covermasterunkid AND isvoid = 0 "

'            If intEmployeeunkid > 0 Then
'                strQ &= " AND employeeunkid=@employeeunkid and isnull(dependantsunkid,0)= 0 "
'                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
'            End If

'            If intdependentsunkid > 0 Then
'                strQ &= " AND dependantsunkid=@dependantsunkid"
'                objDataOperation.AddParameter("@dependantsunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intdependentsunkid)
'            End If

'            If intUnkid > 0 Then
'                strQ &= " AND medicalcoverunkid <> @medicalcoverunkid"
'                objDataOperation.AddParameter("@medicalcoverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
'            End If

'            objDataOperation.AddParameter("@medicalcategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMedicalCategoryunkid)
'            objDataOperation.AddParameter("@serviceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intServiceunkid)
'            objDataOperation.AddParameter("@covermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCoverunkid)


'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'End Class