﻿'************************************************************************************************************************************
'Class Name : clsmedical_claim_master.vb
'Purpose    :
'Date       :20/07/2010
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
''' 

Public Class clsmedical_claim_master
    Private Shared ReadOnly mstrModuleName As String = "clsmedical_claim_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintClaimunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintInstituteunkid As Integer
    Private mstrInvoiceNo As String
    Private mdblTotInvoiceAmt As Decimal
    Private mintUserunkid As Integer
    Private mintStatusunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Dim objClaimTran As New clsmedical_claim_Tran


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes
    Private mblnIsFinal As Boolean
    Private mdsTranData As DataSet
    'Pinkal (20-Jan-2012) -- End


    'Pinkal (18-Dec-2012) -- Start
    'Enhancement : TRA Changes
    Private mdtInvoiceDate As Date
    'Pinkal (18-Dec-2012) -- End


#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set claimunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Claimunkid() As Integer
        Get
            Return mintClaimunkid
        End Get
        Set(ByVal value As Integer)
            mintClaimunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set instituteunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Instituteunkid() As Integer
        Get
            Return mintInstituteunkid
        End Get
        Set(ByVal value As Integer)
            mintInstituteunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set invoiceno
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _InvoiceNo() As String
        Get
            Return mstrInvoiceNo
        End Get
        Set(ByVal value As String)
            mstrInvoiceNo = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set totinvoiceamount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _TotalInvoiceAmount() As Decimal
        Get
            Return mdblTotInvoiceAmt
        End Get
        Set(ByVal value As Decimal)
            mdblTotInvoiceAmt = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property


    'Pinkal (20-Jan-2012) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _IsFinal() As Boolean
        Get
            Return mblnIsFinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsFinal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DsTranData
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public ReadOnly Property _DsTranData() As DataSet
        Get
            Return mdsTranData
        End Get
    End Property

    'Pinkal (20-Jan-2012) -- End



    'Pinkal (18-Dec-2012) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set invoicedate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _InvoiceDate() As Date
        Get
            Return mdtInvoiceDate
        End Get
        Set(ByVal value As Date)
            mdtInvoiceDate = value
        End Set
    End Property

    'Pinkal (18-Dec-2012) -- End



#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '          "  claimunkid " & _
            '          ", periodunkid " & _
            '          ", instituteunkid " & _
            '          ", isnull(invoiceno,'') invoiceno " & _
            '          ", isnull(invoice_amt,0.00) invoice_amt" & _
            '          ", userunkid " & _
            '          ", isvoid " & _
            '          ", voiduserunkid " & _
            '          ", voiddatetime " & _
            '          ", voidreason " & _
            '          ", ISNULL(isfinal,0) as isfinal " & _
            '         "FROM mdmedical_claim_master " & _
            '         "WHERE claimunkid = @claimunkid "

            strQ = "SELECT " & _
              "  claimunkid " & _
                      ", periodunkid " & _
                      ", instituteunkid " & _
                      ", isnull(invoiceno,'') invoiceno " & _
                      ", isnull(invoice_amt,0.00) invoice_amt" & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
                      ", ISNULL(isfinal,0) as isfinal " & _
                       ", invoicedate " & _
             "FROM mdmedical_claim_master " & _
             "WHERE claimunkid = @claimunkid "

            'Pinkal (18-Dec-2012) -- End


            objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintClaimunkid = CInt(dtRow.Item("claimunkid"))

                If Not IsDBNull(dtRow.Item("periodunkid")) Then
                    mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                End If
                If Not IsDBNull(dtRow.Item("instituteunkid")) Then
                    mintInstituteunkid = CInt(dtRow.Item("instituteunkid"))
                End If
                If Not IsDBNull(dtRow.Item("invoiceno")) Then
                    mstrInvoiceNo = CStr(dtRow.Item("invoiceno"))
                End If
                If Not IsDBNull(dtRow.Item("invoice_amt")) Then
                    mdblTotInvoiceAmt = CDbl(dtRow.Item("invoice_amt"))
                End If

                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then mdtVoiddatetime = dtRow.Item("voiddatetime")
                mstrVoidreason = dtRow.Item("voidreason").ToString


                'Pinkal (20-Jan-2012) -- Start
                'Enhancement : TRA Changes
                mblnIsFinal = CBool(dtRow.Item("isfinal").ToString())
                'Pinkal (20-Jan-2012) -- End


                'Pinkal (18-Dec-2012) -- Start
                'Enhancement : TRA Changes
                If Not IsDBNull(dtRow.Item("invoicedate")) <> Nothing Then mdtInvoiceDate = dtRow.Item("invoicedate")
                'Pinkal (18-Dec-2012) -- End


                Exit For
            Next


            'Pinkal (19-Nov-2012) -- Start
            'Enhancement : TRA Changes



            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objClaimTran._Claimunkid = mintClaimunkid
            'mdsTranData = objClaimTran.GetClaimTran()
            'Shani(24-Aug-2015) -- End

            'Pinkal (19-Nov-2012) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 

    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True _
                                    , Optional ByVal intUserUnkId As Integer = 0, Optional ByVal mblnMyInvoiceOnly As Boolean = True _
                                    , Optional ByVal mstrFilter As String = "") As DataSet

        'Pinkal (06-Jan-2016) -- Start 'Enhancement - Working on Changes in SS for Leave Module.[Optional ByVal mstrFilter As String = ""]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT DISTINCT " & _
              "  claimunkid " & _
                      ", mdmedical_claim_master.periodunkid " & _
                      ", cfcommon_period_tran.period_name " & _
                      ", mdmedical_claim_master.instituteunkid " & _
                      ", hrinstitute_master.institute_name " & _
                      ", isnull(invoiceno,'') invoiceno" & _
                      ", isnull(invoice_amt,0) invoice_amt " & _
                      ", mdmedical_claim_master.statusunkid " & _
                      ", CASE WHEN mdmedical_claim_master.statusunkid = 1 then @Pending " & _
                      "           WHEN mdmedical_claim_master.statusunkid = 2 then @Exported " & _
                      "           WHEN mdmedical_claim_master.statusunkid = 3 then @PAffected " & _
                      "  END status_name " & _
                      ", mdmedical_claim_master.userunkid " & _
                      ", mdmedical_claim_master.isvoid " & _
                      ", mdmedical_claim_master.voiduserunkid " & _
                      ", mdmedical_claim_master.voiddatetime " & _
                      ", mdmedical_claim_master.voidreason " & _
                     ", ISNULL(mdmedical_claim_master.isfinal,0) isfinal " & _
                   ", mdmedical_claim_master.invoicedate " & _
                     " FROM mdmedical_claim_master " & _
                     " JOIN hrinstitute_master on mdmedical_claim_master.instituteunkid = hrinstitute_master.instituteunkid AND hrinstitute_master.ishospital = 1 " & _
                     " JOIN cfcommon_period_tran on cfcommon_period_tran.periodunkid = mdmedical_claim_master.periodunkid "

            If intUserUnkId <= 0 Then
                intUserUnkId = User._Object._Userunkid
            End If

            If mblnMyInvoiceOnly Then

                If intUserUnkId > 1 Then
                    strQ &= " JOIN mdprovider_mapping on mdprovider_mapping.providerunkid = mdmedical_claim_master.instituteunkid  And mdmedical_claim_master.userunkid = " & intUserUnkId
                End If
            Else

                If intUserUnkId > 1 Then
                    strQ &= " JOIN mdprovider_mapping on mdprovider_mapping.providerunkid = mdmedical_claim_master.instituteunkid  "
                End If

            End If

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If
            'Pinkal (06-Jan-2016) -- End


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Exported", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 280, "Exported"))
            objDataOperation.AddParameter("@PAffected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 281, "Payroll Affected"))
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (mdmedical_claim_master) </purpose>
    Public Function Insert(ByVal mdtran As DataTable) As Boolean
        If isExist(mintInstituteunkid, mstrInvoiceNo) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Invoice No is already define for this Service Provider.Please define new Invoice No.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteunkid.ToString)
            objDataOperation.AddParameter("@invoiceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrInvoiceNo.ToString)
            objDataOperation.AddParameter("@invoice_amt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblTotInvoiceAmt.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFinal.ToString())


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            objDataOperation.AddParameter("@invoicedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtInvoiceDate <> Nothing, mdtInvoiceDate, DBNull.Value))

            'strQ = "INSERT INTO mdmedical_claim_master ( " & _
            '          " periodunkid " & _
            '          ", instituteunkid " & _
            '          ", invoiceno " & _
            '          ", invoice_amt " & _
            '          ", statusunkid " & _
            '          ", userunkid " & _
            '          ", isvoid " & _
            '          ", voiduserunkid " & _
            '          ", voiddatetime " & _
            '          ", voidreason" & _
            '          ", isfinal " & _
            '        ") VALUES (" & _
            '          "  @periodunkid " & _
            '          ", @instituteunkid " & _
            '          ", @invoiceno " & _
            '          ", @invoice_amt " & _
            '          ", @statusunkid " & _
            '          ", @userunkid " & _
            '          ", @isvoid " & _
            '          ", @voiduserunkid " & _
            '          ", @voiddatetime " & _
            '          ", @voidreason" & _
            '          ", @isfinal " & _
            '        "); SELECT @@identity"

            strQ = "INSERT INTO mdmedical_claim_master ( " & _
                      " periodunkid " & _
                      ", instituteunkid " & _
                      ", invoiceno " & _
                      ", invoice_amt " & _
                      ", statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
                      ", isfinal " & _
                    ", invoicedate " & _
            ") VALUES (" & _
                      "  @periodunkid " & _
                      ", @instituteunkid " & _
                      ", @invoiceno " & _
                      ", @invoice_amt " & _
                      ", @statusunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
                      ", @isfinal " & _
                    ", @invoicedate " & _
            "); SELECT @@identity"

            'Pinkal (18-Dec-2012) -- End



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintClaimunkid = dsList.Tables(0).Rows(0).Item(0)

            objClaimTran._Claimunkid = mintClaimunkid
            objClaimTran._DataList = mdtran
            objClaimTran._Userunkid = mintUserunkid


            If objClaimTran._DataList IsNot Nothing Then

                If objClaimTran._DataList.Rows.Count > 0 Then

                    If objClaimTran.InsertUpdateDelete_ClaimTran(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Else


                    'Pinkal (25-APR-2012) -- Start
                    'Enhancement : TRA Changes
                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "mdmedical_claim_master", "claimunkid", mintClaimunkid, "mdmedical_claim_tranfs", "claimtranunkid", -1, 1, 0) = False Then
                    'Pinkal (25-APR-2012) -- End
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "mdmedical_claim_master", "claimunkid", mintClaimunkid, "mdmedical_claim_tranfs", "claimtranunkid", -1, 1, 0, False, mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
                End If

            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (mdmedical_claim_master) </purpose>
    Public Function Update(ByVal mdtran As DataTable) As Boolean
        If isExist(mintInstituteunkid, mstrInvoiceNo, mintClaimunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Invoice No is already define for this Service Provider.Please define new Invoice No.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInstituteunkid.ToString)
            objDataOperation.AddParameter("@invoiceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrInvoiceNo.ToString)
            objDataOperation.AddParameter("@invoice_amt", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblTotInvoiceAmt.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFinal.ToString())

            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            objDataOperation.AddParameter("@invoicedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtInvoiceDate <> Nothing, mdtInvoiceDate, DBNull.Value))

            'strQ = "UPDATE mdmedical_claim_master SET " & _
            '  " periodunkid = @periodunkid " & _
            '  ", instituteunkid = @instituteunkid" & _
            '  ", invoiceno = @invoiceno " & _
            '  ", invoice_amt = @invoice_amt " & _
            '  ", statusunkid = @statusunkid " & _
            '  ", userunkid = @userunkid" & _
            '  ", isvoid = @isvoid" & _
            '  ", voiduserunkid = @voiduserunkid" & _
            '  ", voiddatetime = @voiddatetime" & _
            '  ", voidreason = @voidreason " & _
            '          ", isfinal = @isfinal " & _
            '"WHERE claimunkid = @claimunkid "

            strQ = "UPDATE mdmedical_claim_master SET " & _
              " periodunkid = @periodunkid " & _
              ", instituteunkid = @instituteunkid" & _
              ", invoiceno = @invoiceno " & _
              ", invoice_amt = @invoice_amt " & _
              ", statusunkid = @statusunkid " & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
                      ", isfinal = @isfinal " & _
                       ", invoicedate = @invoicedate " & _
            "WHERE claimunkid = @claimunkid "

            'Pinkal (18-Dec-2012) -- End


            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objClaimTran._Claimunkid = mintClaimunkid
            objClaimTran._DataList = mdtran
            objClaimTran._Userunkid = mintUserunkid

            Dim dt() As DataRow = objClaimTran._DataList.Select("AUD=''")
            If dt.Length = objClaimTran._DataList.Rows.Count Then


                'Pinkal (25-APR-2012) -- Start
                'Enhancement : TRA Changes

                'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "mdmedical_claim_master", mintClaimunkid, "claimunkid", 2) Then
                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "mdmedical_claim_master", "claimunkid", mintClaimunkid, "mdmedical_claim_tran", "claimtranunkid", -1, 2, 0) = False Then
                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '        Throw exForce
                '    End If

                'End If

                If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "mdmedical_claim_master", mintClaimunkid, "claimunkid", 2, objDataOperation) Then
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "mdmedical_claim_master", "claimunkid", mintClaimunkid, "mdmedical_claim_tran", "claimtranunkid", -1, 2, 0, False, mintUserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                End If

                'Pinkal (25-APR-2012) -- End

            Else
                If objClaimTran.InsertUpdateDelete_ClaimTran(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (mdmedical_claim_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal intMedicalClaimtranunkid As Integer = -1) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            'START FOR VOID MEDICAL CLAIM TRAN

            strQ = "Update mdmedical_claim_tran set" & _
                 " isvoid =1,voiddatetime = @voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                  "WHERE claimunkid = @claimunkid "

            objDataOperation.ClearParameters()

            If intMedicalClaimtranunkid > 0 Then
                strQ &= " AND claimtranunkid=@claimtranunkid "
                objDataOperation.AddParameter("@claimtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMedicalClaimtranunkid)
            End If


            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            objDataOperation.ExecNonQuery(strQ)


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'END FOR VOID MEDICAL CLAIM TRAN


                        'START FOR VOID MEDICAL CLAIM MASTER

                        strQ = "Update mdmedical_claim_master set" & _
                               " isvoid =1,voiddatetime = @voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid  " & _
                                "WHERE claimunkid = @claimunkid "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                        objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

                        objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

            'END FOR VOID MEDICAL CLAIM MASTER


            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes

            'If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "mdmedical_claim_master", "claimunkid", intUnkid, "mdmedical_claim_tran", "claimtranunkid", 3, 3) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If


            If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "mdmedical_claim_master", "claimunkid", intUnkid, "mdmedical_claim_tran", "claimtranunkid", 3, 3, "", "", False, mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (25-APR-2012) -- End


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intinstituteunkid As Integer, ByVal strInvoiceno As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = " SELECT " & _
            '          "  claimunkid " & _
            '          ", periodunkid " & _
            '          ", instituteunkid " & _
            '          ", invoiceno " & _
            '          ", invoice_amt " & _
            '          ", userunkid " & _
            '          ", isvoid " & _
            '          ", voiduserunkid " & _
            '          ", voiddatetime " & _
            '          ", voidreason " & _
            '          ", ISNULL(isfinal,0) as isfinal " & _
            '          " FROM mdmedical_claim_master " & _
            '          " WHERE isvoid = 0"

            strQ = "SELECT " & _
              "  claimunkid " & _
                      ", periodunkid " & _
                      ", instituteunkid " & _
                      ", invoiceno " & _
                      ", invoice_amt " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
                     ", ISNULL(isfinal,0) as isfinal " & _
                   ", invoicedate " & _
             "FROM mdmedical_claim_master " & _
                     " WHERE isvoid = 0"


            'Pinkal (18-Dec-2012) -- End


            objDataOperation.ClearParameters()

            If intinstituteunkid > 0 Then
                strQ &= " AND instituteunkid = @instituteunkid"
                objDataOperation.AddParameter("@instituteunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intinstituteunkid)
            End If

            If mstrInvoiceNo.Trim.Length > 0 Then
                strQ &= " AND invoiceno = @invoiceno"
                objDataOperation.AddParameter("@invoiceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strInvoiceno)
            End If

            If intUnkid > 0 Then
                strQ &= " AND claimunkid <> @claimunkid"
                objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetImportFileStructure() As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim objDataOperation As New clsDataOperation
        Try

            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = " SELECT " & _
            '          " '' EmployeeCode " & _
            '          ", '' Employee " & _
            '          ",'' Dependant " & _
            '          ",'' Sicksheet " & _
            '          ",'' Claimno " & _
            '          ",'' ClaimDate " & _
            '          ",' 'ClaimAmount " & _
            '          ",'' Remark "

            strQ = " SELECT " & _
                      " '' EmployeeCode " & _
                      ", '' Employee " & _
                      ",'' Dependant " & _
                      ",'' Sicksheet " & _
                      ",'' Claimno " & _
                      ",'' ClaimDate " & _
                      ",' 'ClaimAmount " & _
                     ",'' Remark " & _
                     ",'' isempexempted "

            'Pinkal (18-Dec-2012) -- End

            dsList = objDataOperation.ExecQuery(strQ, "MedicalClaim")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetFileStructure", mstrModuleName)
        End Try
        Return dsList
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Invoice No is already define for this Service Provider.Please define new Invoice No.")
			Language.setMessage("clsMasterData", 111, "Pending")
			Language.setMessage("clsMasterData", 280, "Exported")
			Language.setMessage("clsMasterData", 281, "Payroll Affected")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class



'Public Class clsmedical_claim_master
'    Private Shared Readonly mstrModuleName As String = "clsmedical_claim_master"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "
'    Private mintClaimunkid As Integer
'    Private mintEmployeeunkid As Integer
'    Private mintYearunkid As Integer
'    Private mintUserunkid As Integer
'    Private mblnIsvoid As Boolean
'    Private mintVoiduserunkid As Integer
'    Private mdtVoiddatetime As Date
'    Private mstrVoidreason As String = String.Empty
'    Dim objClaimTran As New clsmedical_claim_Tran

'#End Region

'#Region " Properties "
'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set claimunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Claimunkid() As Integer
'        Get
'            Return mintClaimunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintClaimunkid = Value
'            Call getData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set employeeunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Employeeunkid() As Integer
'        Get
'            Return mintEmployeeunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintEmployeeunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set yearunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Yearunkid() As Integer
'        Get
'            Return mintYearunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintYearunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set userunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Userunkid() As Integer
'        Get
'            Return mintUserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintUserunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isvoid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Isvoid() As Boolean
'        Get
'            Return mblnIsvoid
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsvoid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiduserunkid
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Voiduserunkid() As Integer
'        Get
'            Return mintVoiduserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintVoiduserunkid = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiddatetime
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Voiddatetime() As Date
'        Get
'            Return mdtVoiddatetime
'        End Get
'        Set(ByVal value As Date)
'            mdtVoiddatetime = Value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voidreason
'    ''' Modify By: Pinkal
'    ''' </summary>
'    Public Property _Voidreason() As String
'        Get
'            Return mstrVoidreason
'        End Get
'        Set(ByVal value As String)
'            mstrVoidreason = Value
'        End Set
'    End Property

'#End Region

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  claimunkid " & _
'              ", employeeunkid " & _
'              ", yearunkid " & _
'              ", userunkid " & _
'              ", isvoid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime " & _
'              ", voidreason " & _
'             "FROM mdmedical_claim_master " & _
'             "WHERE claimunkid = @claimunkid "

'            objDataOperation.AddParameter("@claimunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintClaimUnkId.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintclaimunkid = CInt(dtRow.Item("claimunkid"))
'                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
'                mintYearunkid = CInt(dtRow.Item("yearunkid"))
'                mintuserunkid = CInt(dtRow.Item("userunkid"))
'                mblnisvoid = CBool(dtRow.Item("isvoid"))
'                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
'                If mdtVoiddatetime <> Nothing Then mdtVoiddatetime = dtRow.Item("voiddatetime")
'                mstrVoidreason = dtRow.Item("voidreason").ToString
'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  claimunkid " & _
'              ", employeeunkid " & _
'              ", yearunkid " & _
'              ", userunkid " & _
'              ", isvoid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime " & _
'              ", voidreason " & _
'             "FROM mdmedical_claim_master "

'            If blnOnlyActive Then
'                strQ &= " WHERE isactive = 1 "
'            End If

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (mdmedical_claim_master) </purpose>
'    Public Function Insert(ByVal mdtran As DataTable) As Boolean
'        'If isExist(mstrName) Then
'        '    mstrMessage = "<Message>"
'        '    Return False
'        'End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation
'        objDataOperation.BindTransaction()
'        Try
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
'            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'            strQ = "INSERT INTO mdmedical_claim_master ( " & _
'              "  employeeunkid " & _
'              ", yearunkid " & _
'              ", userunkid " & _
'              ", isvoid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime " & _
'              ", voidreason" & _
'            ") VALUES (" & _
'              "  @employeeunkid " & _
'              ", @yearunkid " & _
'              ", @userunkid " & _
'              ", @isvoid " & _
'              ", @voiduserunkid " & _
'              ", @voiddatetime " & _
'              ", @voidreason" & _
'            "); SELECT @@identity"

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            mintClaimunkid = dsList.Tables(0).Rows(0).Item(0)

'            objClaimTran._Claimunkid = mintClaimunkid
'            objClaimTran._DataList = mdtran
'            objClaimTran.InsertUpdateDelete_ClaimTran(objDataOperation)


'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If
'            objDataOperation.ReleaseTransaction(True)
'            Return True
'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (mdmedical_claim_master) </purpose>
'    Public Function Update(ByVal mdtran As DataTable) As Boolean
'        'If isExist(mstrName, mintClaimunkid) Then
'        '    mstrMessage = "<Message>"
'        '    Return False
'        'End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation
'        objDataOperation.BindTransaction()

'        Try
'            objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClaimunkid.ToString)
'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
'            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
'            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

'            strQ = "UPDATE mdmedical_claim_master SET " & _
'              "  employeeunkid = @employeeunkid" & _
'              ", yearunkid = @yearunkid" & _
'              ", userunkid = @userunkid" & _
'              ", isvoid = @isvoid" & _
'              ", voiduserunkid = @voiduserunkid" & _
'              ", voiddatetime = @voiddatetime" & _
'              ", voidreason = @voidreason " & _
'            "WHERE claimunkid = @claimunkid "

'            objDataOperation.ExecNonQuery(strQ)

'            objClaimTran._Claimunkid = mintClaimunkid
'            objClaimTran._DataList = mdtran
'            objClaimTran.InsertUpdateDelete_ClaimTran(objDataOperation)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If
'            objDataOperation.ReleaseTransaction(True)
'            Return True
'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (mdmedical_claim_master) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal intMedicalClaimtranunkid As Integer = -1) As Boolean
'        'If isUsed(intUnkid) Then
'        '    mstrMessage = "<Message>"
'        '    Return False
'        'End If
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation
'        objDataOperation.BindTransaction()

'        Try

'            'START FOR VOID MEDICAL CLAIM TRAN

'            strQ = "Update mdmedical_claim_tran set" & _
'                 " isvoid =1,voiddatetime = @voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
'                  "WHERE claimunkid = @claimunkid "

'            objDataOperation.ClearParameters()

'            If intMedicalClaimtranunkid > 0 Then
'                strQ &= " AND claimtranunkid=@claimtranunkid "
'                objDataOperation.AddParameter("@claimtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intMedicalClaimtranunkid)
'            End If


'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
'            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
'            objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            objDataOperation.ExecNonQuery(strQ)

'            'END FOR VOID MEDICAL CLAIM TRAN


'            strQ = "select count(*) as Recordcount From mdmedical_claim_tran WHERE claimunkid = @claimunkid AND isvoid = 0 "
'            objDataOperation.ClearParameters()
'            objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
'            dsList = objDataOperation.ExecQuery(strQ, "Count")


'            If dsList IsNot Nothing Then
'                If dsList.Tables("Count").Rows.Count > 0 Then
'                    If CInt(dsList.Tables("Count").Rows(0)("Recordcount")) = 0 Then
'                        'START FOR VOID MEDICAL CLAIM MASTER

'                        strQ = "Update mdmedical_claim_master set" & _
'                               " isvoid =1,voiddatetime = @voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid  " & _
'                                "WHERE claimunkid = @claimunkid "

'                        objDataOperation.ClearParameters()
'                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
'                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
'                        objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'                        objDataOperation.ExecNonQuery(strQ)

'                        'END FOR VOID MEDICAL CLAIM MASTER
'                    End If
'                End If
'            End If

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If
'            objDataOperation.ReleaseTransaction(True)
'            Return True
'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            StrQ = "<Query>"

'            objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.tables(0).rows.count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Pinkal
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  claimunkid " & _
'              ", employeeunkid " & _
'              ", yearunkid " & _
'              ", userunkid " & _
'              ", isvoid " & _
'              ", voiduserunkid " & _
'              ", voiddatetime " & _
'              ", voidreason " & _
'             "FROM mdmedical_claim_master " & _
'             "WHERE name = @name " & _
'             "AND code = @code "

'            If intUnkid > 0 Then
'                strQ &= " AND claimunkid <> @claimunkid"
'            End If

'            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
'            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
'            objDataOperation.AddParameter("@claimunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.tables(0).rows.count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'End Class