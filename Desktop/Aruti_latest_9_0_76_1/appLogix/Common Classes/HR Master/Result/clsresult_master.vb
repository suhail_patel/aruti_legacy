﻿'************************************************************************************************************************************
'Class Name : clsresult_master.vb
'Purpose    : All Result Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :26/06/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsresult_master
    Private Shared ReadOnly mstrModuleName As String = "clsresult_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintResultunkid As Integer
    Private mstrResultcode As String = String.Empty
    Private mstrResultname As String = String.Empty
    Private mintResultgroupunkid As Integer
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrResultname1 As String = String.Empty
    Private mstrResultname2 As String = String.Empty


    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes
    Private mintResultLevel As Integer
    'Pinkal (12-Oct-2011) -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set resultunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Resultunkid() As Integer
        Get
            Return mintResultunkid
        End Get
        Set(ByVal value As Integer)
            mintResultunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set resultcode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Resultcode() As String
        Get
            Return mstrResultcode
        End Get
        Set(ByVal value As String)
            mstrResultcode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set resultname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Resultname() As String
        Get
            Return mstrResultname
        End Get
        Set(ByVal value As String)
            mstrResultname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set resultgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Resultgroupunkid() As Integer
        Get
            Return mintResultgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintResultgroupunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set resultname1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Resultname1() As String
        Get
            Return mstrResultname1
        End Get
        Set(ByVal value As String)
            mstrResultname1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set resultname2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Resultname2() As String
        Get
            Return mstrResultname2
        End Get
        Set(ByVal value As String)
            mstrResultname2 = Value
        End Set
    End Property



    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set result level
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ResultLevel() As Integer
        Get
            Return mintResultLevel
        End Get
        Set(ByVal value As Integer)
            mintResultLevel = value
        End Set
    End Property

    'Pinkal (12-Oct-2011) -- End


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes

            ' strQ = "SELECT " & _
            ' "  resultunkid " & _
            ' ", resultcode " & _
            ' ", resultname " & _
            ' ", resultgroupunkid " & _
            ' ", description " & _
            ' ", isactive " & _
            ' ", resultname1 " & _
            ' ", resultname2 " & _
            '"FROM hrresult_master " & _
            '"WHERE resultunkid = @resultunkid "

            strQ = "SELECT " & _
              "  resultunkid " & _
              ", resultcode " & _
              ", resultname " & _
              ", resultgroupunkid " & _
              ", ISNULL(result_level,'') result_level " & _
              ", description " & _
              ", isactive " & _
              ", resultname1 " & _
              ", resultname2 " & _
             "FROM hrresult_master " & _
             "WHERE resultunkid = @resultunkid "

            'Pinkal (12-Oct-2011) -- End

            objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintResultunkid = CInt(dtRow.Item("resultunkid"))
                mstrResultcode = dtRow.Item("resultcode").ToString
                mstrResultname = dtRow.Item("resultname").ToString
                mintResultgroupunkid = CInt(dtRow.Item("resultgroupunkid"))
                mstrDescription = dtRow.Item("description").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrResultname1 = dtRow.Item("resultname1").ToString
                mstrResultname2 = dtRow.Item("resultname2").ToString

                If Not IsDBNull(dtRow.Item("result_level")) Then
                    mintResultLevel = CInt(dtRow.Item("result_level").ToString())
                End If

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes

            ' strQ = "SELECT " & _
            ' "  resultunkid " & _
            ' ", resultcode " & _
            ' ", resultname " & _
            ' ", cfcommon_master.name" & _
            ' ", resultgroupunkid " & _
            ' ", description " & _
            ' ", hrresult_master.isactive " & _
            ' ", resultname1 " & _
            ' ", resultname2 " & _
            '"FROM hrresult_master " & _
            '" LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = hrresult_master.resultgroupunkid "


            'Anjan [19 January 2016] -- Start
            'strQ = "SELECT " & _
            '  "  resultunkid " & _
            '  ", resultcode " & _
            '  ", resultname " & _
            '  ", cfcommon_master.name" & _
            '  ", resultgroupunkid " & _
            '  ", ISNULL(result_level,'') result_level " & _
            '  ", description " & _
            '  ", hrresult_master.isactive " & _
            '  ", resultname1 " & _
            '  ", resultname2 " & _
            ' "FROM hrresult_master " & _
            ' " LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = hrresult_master.resultgroupunkid  AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RESULT_GROUP

            strQ = "SELECT " & _
              "  hrresult_master.resultunkid " & _
              ", hrresult_master.resultcode " & _
              ", hrresult_master.resultname " & _
              ", cfcommon_master.name" & _
              ", hrresult_master.resultgroupunkid " & _
              ", ISNULL(result_level,'') result_level " & _
              ", hrresult_master.description " & _
              ", hrresult_master.isactive " & _
              ", hrresult_master.resultname1 " & _
              ", hrresult_master.resultname2 " & _
             "FROM hrresult_master " & _
             " LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = hrresult_master.resultgroupunkid  AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.RESULT_GROUP
            'Anjan [19 January 2016] -- End

            

            'Pinkal (12-Oct-2011) -- End

            If blnOnlyActive Then
                strQ &= " WHERE hrresult_master.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrresult_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrResultcode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Result Code is already defined. Please define new Result Code.")
            Return False

            'Anjan (17 Jan 2012)-Start
            'ENHANCEMENT : TRA COMMENTS
            'ElseIf isExist("", mstrResultname) Then
        ElseIf isExist("", mstrResultname, , mintResultgroupunkid) Then
            'Anjan (17 Jan 2012)-End 
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Result Name is already defined. Please define new Result Name.")
            Return False


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes

        ElseIf isExist("", "", , mintResultgroupunkid, mintResultLevel) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "This Result Level is already defined to selected result Group. Please define new Result Level.")
            Return False

            'Pinkal (12-Oct-2011) -- End

        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@resultcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrresultcode.ToString)
            objDataOperation.AddParameter("@resultname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrresultname.ToString)
            objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintresultgroupunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@resultname1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrresultname1.ToString)
            objDataOperation.AddParameter("@resultname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrResultname2.ToString)


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            objDataOperation.AddParameter("@result_level", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultLevel.ToString)



            'StrQ = "INSERT INTO hrresult_master ( " & _
            '  "  resultcode " & _
            '  ", resultname " & _
            '  ", resultgroupunkid " & _
            '  ", description " & _
            '  ", isactive " & _
            '  ", resultname1 " & _
            '  ", resultname2" & _
            '") VALUES (" & _
            '  "  @resultcode " & _
            '  ", @resultname " & _
            '  ", @resultgroupunkid " & _
            '  ", @description " & _
            '  ", @isactive " & _
            '  ", @resultname1 " & _
            '  ", @resultname2" & _
            '"); SELECT @@identity"

            strQ = "INSERT INTO hrresult_master ( " & _
              "  resultcode " & _
              ", resultname " & _
              ", resultgroupunkid " & _
                      ", result_level " & _
              ", description " & _
              ", isactive " & _
              ", resultname1 " & _
              ", resultname2" & _
            ") VALUES (" & _
              "  @resultcode " & _
              ", @resultname " & _
              ", @resultgroupunkid " & _
                      ", @result_level " & _
              ", @description " & _
              ", @isactive " & _
              ", @resultname1 " & _
              ", @resultname2" & _
            "); SELECT @@identity"

            'Pinkal (12-Oct-2011) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintResultunkid = dsList.Tables(0).Rows(0).Item(0)

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrresult_master", "resultunkid", mintResultunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End


            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrresult_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrResultcode, "", mintResultunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Result Code is already defined. Please define new Result Code.")
            Return False
        ElseIf isExist("", mstrResultname, mintResultunkid, mintResultgroupunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Result Name is already defined. Please define new Result Name.")
            Return False

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes

        ElseIf isExist("", "", mintResultunkid, mintResultgroupunkid, mintResultLevel) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "This Result Level is already defined to selected result Group. Please define new Result Level.")
            Return False

            'Pinkal (12-Oct-2011) -- End


        End If


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@resultunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintresultunkid.ToString)
            objDataOperation.AddParameter("@resultcode", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrresultcode.ToString)
            objDataOperation.AddParameter("@resultname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrresultname.ToString)
            objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintresultgroupunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@resultname1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrresultname1.ToString)
            objDataOperation.AddParameter("@resultname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrResultname2.ToString)

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            objDataOperation.AddParameter("@result_level", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultLevel.ToString)


            'strQ = "UPDATE hrresult_master SET " & _
            '  "  resultcode = @resultcode" & _
            '  ", resultname = @resultname" & _
            '  ", resultgroupunkid = @resultgroupunkid" & _
            '  ", description = @description" & _
            '  ", isactive = @isactive" & _
            '  ", resultname1 = @resultname1" & _
            '  ", resultname2 = @resultname2 " & _
            '"WHERE resultunkid = @resultunkid "


            strQ = "UPDATE hrresult_master SET " & _
              "  resultcode = @resultcode" & _
              ", resultname = @resultname" & _
              ", resultgroupunkid = @resultgroupunkid" & _
                      ", result_level = @result_level" & _
              ", description = @description" & _
              ", isactive = @isactive" & _
              ", resultname1 = @resultname1" & _
              ", resultname2 = @resultname2 " & _
              ", Syncdatetime  = NULL " & _
            "WHERE resultunkid = @resultunkid " 'Sohail (28 Mar 2012) - [Syncdatetime]

            'Pinkal (12-Oct-2011) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrresult_master", mintResultunkid, "resultunkid", 2) Then

                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrresult_master", "resultunkid", mintResultunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrresult_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete selected Result Code. Reason: This Result Code is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            'strQ = "DELETE FROM hrresult_master " & _
            '"WHERE resultunkid = @resultunkid "

            strQ = "UPDATE hrresult_master " & _
                    " SET isactive = 0 " & _
                    "WHERE resultunkid = @resultunkid "


            objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrresult_master", "resultunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT TABLE_NAME AS TableName " & _
                         ",COLUMN_NAME " & _
                    "FROM INFORMATION_SCHEMA.COLUMNS " & _
                    "WHERE COLUMN_NAME IN ('resultunkid','resultcodeunkid' )"

          
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            For Each dtRow As DataRow In dsList.Tables("List").Rows

                If dtRow.Item("TableName") = "hrresult_master" Then Continue For

                strQ = "SELECT " & dtRow.Item("COLUMN_NAME") & " FROM " & dtRow.Item("TableName") & " WHERE " & dtRow.Item("COLUMN_NAME") & " = @resultunkid "

                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1, Optional ByVal intGroupId As Integer = -1, Optional ByVal intLevel As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '        "  resultunkid " & _
            '        ", resultcode " & _
            '        ", resultname " & _
            '        ", resultgroupunkid " & _
            '        ", description " & _
            '        ", isactive " & _
            '        ", resultname1 " & _
            '        ", resultname2 " & _
            '       "FROM hrresult_master " & _
            '       "WHERE 1 = 1"

            strQ = "SELECT " & _
              "  resultunkid " & _
              ", resultcode " & _
              ", resultname " & _
              ", resultgroupunkid " & _
                    ", result_level " & _
              ", description " & _
              ", isactive " & _
              ", resultname1 " & _
              ", resultname2 " & _
             "FROM hrresult_master " & _
             "WHERE 1 = 1"

            'Pinkal (12-Oct-2011) -- End

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End

            If strCode.Length > 0 Then
                strQ &= " AND resultcode = @resultcode "
            End If

            If strName.Length > 0 Then
                strQ &= " AND resultname = @resultname "
            End If

            If intUnkid > 0 Then
                strQ &= " AND resultunkid <> @resultunkid"
            End If

            objDataOperation.ClearParameters()

            If intGroupId > 0 AndAlso intLevel >= 0 Then
                strQ &= " AND resultgroupunkid = @resultgroupunkid AND result_level = @result_level"
                objDataOperation.AddParameter("@result_level", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevel)

            End If

            If intGroupId > 0 Then
                strQ &= " AND resultgroupunkid = @resultgroupunkid "
            End If

            objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGroupId)
            objDataOperation.AddParameter("@resultcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@resultname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(Optional ByVal strList As String = "List", _
                                 Optional ByVal blnFlag As Boolean = False, _
                                 Optional ByVal intGrpId As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            If blnFlag = True Then
                strQ = "SELECT 0 AS Id, @Select As Name UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            End If
            strQ &= "SELECT resultunkid As Id, resultname As Name FROM hrresult_master WHERE isactive = 1 "

            If intGrpId > 0 Then
                strQ &= "AND resultgroupunkid = @resultgroupunkid "
                objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGrpId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        End Try
    End Function

    'S.SANDEEP [ 26 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    'S.SANDEEP [ 14 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Function GetResultUnkid(ByVal StrResultName As String) As Boolean
    Public Function GetResultUnkid(ByVal StrResultName As String) As Integer
        'S.SANDEEP [ 14 AUG 2013 ] -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   " resultunkid " & _
                   "FROM hrresult_master " & _
                   "WHERE isactive = 1 AND resultname = @Rname "

            objDataOperation.AddParameter("@Rname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrResultName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("resultunkid")
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetResultUnkid", mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 26 APRIL 2012 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Result Code is already defined. Please define new Result Code.")
			Language.setMessage(mstrModuleName, 2, "This Result Name is already defined. Please define new Result Name.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "This Result Level is already defined to selected result Group. Please define new Result Level.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class