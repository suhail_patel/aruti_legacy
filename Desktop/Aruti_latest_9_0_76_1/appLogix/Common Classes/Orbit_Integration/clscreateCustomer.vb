﻿Imports System.ComponentModel
Imports Newtonsoft.Json.Serialization

Public Class clscreateCustomer
    'Private _channelId As String = String.Empty
    'Private _serviceChannelCode As String = String.Empty
    'Private _transmissionTime As String = String.Empty
    Private _referenceNo As String = String.Empty
    Private _credentials As New credentials
    Private _businessUnitId As String = String.Empty
    Private _addressLine1 As String = String.Empty
    Private _addressState As String = String.Empty
    Private _addressCity As String = String.Empty
    Private _addressPropertyTypeId As String = String.Empty
    Private _countryOfBirthCd As String = String.Empty
    Private _custCountryCd As String = String.Empty
    Private _customerCategory As String = String.Empty
    Private _customerSegmentCd As String = String.Empty
    Private _customerType As String = String.Empty
    Private _firstName As String = String.Empty
    Private _lastName As String = String.Empty
    Private _gender As String = String.Empty
    Private _maritalStatus As String = String.Empty
    Private _mainBusinessUnitId As String = String.Empty
    Private _openingReasonCode As String = String.Empty
    Private _operationCurrencyCd As String = String.Empty
    Private _residentCountryCd As String = String.Empty
    Private _residentFlag As String = String.Empty
    Private _strDateOfBirth As String = String.Empty
    Private _strFromDate As String = String.Empty
    Private _nationalityId As String = String.Empty
    Private _countryOfBirthId As String = String.Empty
    Private _countryId As String = String.Empty
    Private _countryOfResidenceId As String = String.Empty
    Private _addressTypeId As String = String.Empty
    Private _addressCountryId As String = String.Empty
    Private _titleId As String = String.Empty
    Private _supplementaryRelationshipOfficerId As String = String.Empty
    Private _riskCode As String = String.Empty
    Private _industryId As String = String.Empty
    Private _identificationsList As New identificationsList
    Private _contactsList As New contactsList
    Private _imageList As New List(Of imageList)

    '<DefaultValue("")> _
    'Public Property channelId() As String
    '    Get
    '        Return _channelId
    '    End Get
    '    Set(ByVal value As String)
    '        _channelId = value
    '    End Set
    'End Property

    '<DefaultValue("")> _
    'Public Property serviceChannelCode() As String
    '    Get
    '        Return _serviceChannelCode
    '    End Get
    '    Set(ByVal value As String)
    '        _serviceChannelCode = value
    '    End Set
    'End Property

    '<DefaultValue("")> _
    'Public Property transmissionTime() As String
    '    Get
    '        Return _transmissionTime
    '    End Get
    '    Set(ByVal value As String)
    '        _transmissionTime = value
    '    End Set
    'End Property

    <DefaultValue("")> _
    Public Property referenceNo() As String
        Get
            Return _referenceNo
        End Get
        Set(ByVal value As String)
            _referenceNo = value
        End Set
    End Property

    Public Property credentials() As credentials
        Get
            Return _credentials
        End Get
        Set(ByVal value As credentials)
            _credentials = value
        End Set
    End Property

    <DefaultValue("")> _
    Public Property businessUnitId() As String
        Get
            Return _businessUnitId
        End Get
        Set(ByVal value As String)
            _businessUnitId = value
        End Set
    End Property

    <DefaultValue("")> _
    Public Property addressLine1() As String
        Get
            Return _addressLine1
        End Get
        Set(ByVal value As String)
            _addressLine1 = value
        End Set
    End Property

    <DefaultValue("")> _
    Public Property addressState() As String
        Get
            Return _addressState
        End Get
        Set(ByVal value As String)
            _addressState = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property addressCity() As String
        Get
            Return _addressCity
        End Get
        Set(ByVal value As String)
            _addressCity = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property addressPropertyTypeId() As String
        Get
            Return _addressPropertyTypeId
        End Get
        Set(ByVal value As String)
            _addressPropertyTypeId = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property countryOfBirthCd() As String
        Get
            Return _countryOfBirthCd
        End Get
        Set(ByVal value As String)
            _countryOfBirthCd = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property custCountryCd() As String
        Get
            Return _custCountryCd
        End Get
        Set(ByVal value As String)
            _custCountryCd = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property customerCategory() As String
        Get
            Return _customerCategory
        End Get
        Set(ByVal value As String)
            _customerCategory = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property customerSegmentCd() As String
        Get
            Return _customerSegmentCd
        End Get
        Set(ByVal value As String)
            _customerSegmentCd = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property customerType() As String
        Get
            Return _customerType
        End Get
        Set(ByVal value As String)
            _customerType = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property firstName() As String
        Get
            Return _firstName
        End Get
        Set(ByVal value As String)
            _firstName = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property lastName() As String
        Get
            Return _lastName
        End Get
        Set(ByVal value As String)
            _lastName = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property gender() As String
        Get
            Return _gender
        End Get
        Set(ByVal value As String)
            _gender = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property maritalStatus() As String
        Get
            Return _maritalStatus
        End Get
        Set(ByVal value As String)
            _maritalStatus = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property mainBusinessUnitId() As String
        Get
            Return _mainBusinessUnitId
        End Get
        Set(ByVal value As String)
            _mainBusinessUnitId = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property openingReasonCode() As String
        Get
            Return _openingReasonCode
        End Get
        Set(ByVal value As String)
            _openingReasonCode = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property operationCurrencyCd() As String
        Get
            Return _operationCurrencyCd
        End Get
        Set(ByVal value As String)
            _operationCurrencyCd = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property residentCountryCd() As String
        Get
            Return _residentCountryCd
        End Get
        Set(ByVal value As String)
            _residentCountryCd = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property residentFlag() As String
        Get
            Return _residentFlag
        End Get
        Set(ByVal value As String)
            _residentFlag = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property strDateOfBirth() As String
        Get
            Return _strDateOfBirth
        End Get
        Set(ByVal value As String)
            _strDateOfBirth = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property strFromDate() As String
        Get
            Return _strFromDate
        End Get
        Set(ByVal value As String)
            _strFromDate = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property nationalityId() As String
        Get
            Return _nationalityId
        End Get
        Set(ByVal value As String)
            _nationalityId = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property countryOfBirthId() As String
        Get
            Return _countryOfBirthId
        End Get
        Set(ByVal value As String)
            _countryOfBirthId = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property countryId() As String
        Get
            Return _countryId
        End Get
        Set(ByVal value As String)
            _countryId = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property countryOfResidenceId() As String
        Get
            Return _countryOfResidenceId
        End Get
        Set(ByVal value As String)
            _countryOfResidenceId = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property addressTypeId() As String
        Get
            Return _addressTypeId
        End Get
        Set(ByVal value As String)
            _addressTypeId = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property addressCountryId() As String
        Get
            Return _addressCountryId
        End Get
        Set(ByVal value As String)
            _addressCountryId = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property titleId() As String
        Get
            Return _titleId
        End Get
        Set(ByVal value As String)
            _titleId = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property supplementaryRelationshipOfficerId() As String
        Get
            Return _supplementaryRelationshipOfficerId
        End Get
        Set(ByVal value As String)
            _supplementaryRelationshipOfficerId = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property riskCode() As String
        Get
            Return _riskCode
        End Get
        Set(ByVal value As String)
            _riskCode = value
        End Set
    End Property

    <DefaultValue("")> _
    Public Property industryId() As String
        Get
            Return _industryId
        End Get
        Set(ByVal value As String)
            _industryId = value
        End Set
    End Property

    Public Property identificationsList() As identificationsList
        Get
            Return _identificationsList
        End Get
        Set(ByVal value As identificationsList)
            _identificationsList = value
        End Set
    End Property

    Public Property contactsList() As contactsList
        Get
            Return _contactsList
        End Get
        Set(ByVal value As contactsList)
            _contactsList = value
        End Set
    End Property

    Public Property imageList() As List(Of imageList)
        Get
            Return _imageList
        End Get
        Set(ByVal value As List(Of imageList))
            _imageList = value
        End Set
    End Property

End Class

Public Class identificationsList
    Private _identityTypeId As String = String.Empty
    Private _identityType As String = String.Empty
    Private _identityNumber As String = String.Empty
    Private _countryOfIssue As String = String.Empty
    Private _cityOfIssue As String = String.Empty
    Private _strIssueDate As String = String.Empty
    Private _strExpiryDate As String = String.Empty
    Private _verifiedFlag As String = String.Empty
    <DefaultValue("")> _
    Public Property identityTypeId() As String
        Get
            Return _identityTypeId
        End Get
        Set(ByVal value As String)
            _identityTypeId = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property identityType() As String
        Get
            Return _identityType
        End Get
        Set(ByVal value As String)
            _identityType = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property identityNumber() As String
        Get
            Return _identityNumber
        End Get
        Set(ByVal value As String)
            _identityNumber = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property countryOfIssue() As String
        Get
            Return _countryOfIssue
        End Get
        Set(ByVal value As String)
            _countryOfIssue = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property cityOfIssue() As String
        Get
            Return _cityOfIssue
        End Get
        Set(ByVal value As String)
            _cityOfIssue = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property strIssueDate() As String
        Get
            Return _strIssueDate
        End Get
        Set(ByVal value As String)
            _strIssueDate = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property strExpiryDate() As String
        Get
            Return _strExpiryDate
        End Get
        Set(ByVal value As String)
            _strExpiryDate = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property verifiedFlag() As String
        Get
            Return _verifiedFlag
        End Get
        Set(ByVal value As String)
            _verifiedFlag = value
        End Set
    End Property

End Class

Public Class contactsList
    Private _contactModeTypeId As String = String.Empty
    Private _contactMode As String = String.Empty
    Private _contactDetails As String = String.Empty
    <DefaultValue("")> _
    Public Property contactModeTypeId() As String
        Get
            Return _contactModeTypeId
        End Get
        Set(ByVal value As String)
            _contactModeTypeId = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property contactMode() As String
        Get
            Return _contactMode
        End Get
        Set(ByVal value As String)
            _contactMode = value
        End Set
    End Property
    <DefaultValue("")> _
    Public Property contactDetails() As String
        Get
            Return _contactDetails
        End Get
        Set(ByVal value As String)
            _contactDetails = value
        End Set
    End Property

End Class

Public Class imageList
    Private _imageType As String = String.Empty
    Private _binaryImage As String = String.Empty
    <DefaultValue("")> _
    Public Property imageType() As String
        Get
            Return _imageType
        End Get
        Set(ByVal value As String)
            _imageType = value
        End Set
    End Property

    <DefaultValue("")> _
    Public Property binaryImage() As String
        Get
            Return _binaryImage
        End Get
        Set(ByVal value As String)
            _binaryImage = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal imageType As String, ByVal binaryImage As String)
        _imageType = imageType
        _binaryImage = binaryImage
    End Sub

End Class

