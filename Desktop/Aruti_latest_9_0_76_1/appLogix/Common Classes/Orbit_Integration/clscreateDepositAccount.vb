﻿Public Class clscreateDepositAccount
    Private _referenceNo As String = String.Empty
    Private _credentials As New credentials
    Private _accountTitle As String = String.Empty
    Private _primaryCustomerNumber As String = String.Empty
    Private _productCode As String = String.Empty
    Private _strOpeningDate As String = String.Empty

    Public Property referenceNo() As String
        Get
            Return _referenceNo
        End Get
        Set(ByVal value As String)
            _referenceNo = value
        End Set
    End Property

    Public Property credentials() As credentials
        Get
            Return _credentials
        End Get
        Set(ByVal value As credentials)
            _credentials = value
        End Set
    End Property

    Public Property accountTitle() As String
        Get
            Return _accountTitle
        End Get
        Set(ByVal value As String)
            _accountTitle = value
        End Set
    End Property

    Public Property primaryCustomerNumber() As String
        Get
            Return _primaryCustomerNumber
        End Get
        Set(ByVal value As String)
            _primaryCustomerNumber = value
        End Set
    End Property

    Public Property productCode() As String
        Get
            Return _productCode
        End Get
        Set(ByVal value As String)
            _productCode = value
        End Set
    End Property

    Public Property strOpeningDate() As String
        Get
            Return _strOpeningDate
        End Get
        Set(ByVal value As String)
            _strOpeningDate = value
        End Set
    End Property

End Class
