﻿'************************************************************************************************************************************
'Class Name : clstlscreening_stages_tran.vb
'Purpose    :
'Date       :13-Oct-2020
'Written By :Hemant
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clstlscreening_stages_tran
    Private Shared ReadOnly mstrModuleName As String = "clstlscreening_stages_tran"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation


#Region " Private variables "

    Private mintStagetranunkid As Integer
    Private mintCycleunkid As Integer
    Private mintProcessmstunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintScreenermstunkid As Integer
    Private mintStageunkid As Integer
    Private mdtTransactiondate As Date
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property


    ''' <summary>
    ''' Purpose: Get or Set stagetranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Stagetranunkid(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer
        Get
            Return mintStagetranunkid
        End Get
        Set(ByVal value As Integer)
            mintStagetranunkid = value
            Call GetData(xDataOpr)
        End Set
    End Property

    Public Property _Processmstunkid() As Integer
        Get
            Return mintProcessmstunkid
        End Get
        Set(ByVal value As Integer)
            mintProcessmstunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cycleunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Cycleunkid() As Integer
        Get
            Return mintCycleunkid
        End Get
        Set(ByVal value As Integer)
            mintCycleunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Public Property _Screenermstunkid() As Integer
        Get
            Return mintScreenermstunkid
        End Get
        Set(ByVal value As Integer)
            mintScreenermstunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stageunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Stageunkid() As Integer
        Get
            Return mintStageunkid
        End Get
        Set(ByVal value As Integer)
            mintStageunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property



#End Region

    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  stagetranunkid " & _
              ", processmstunkid " & _
              ", cycleunkid " & _
              ", employeeunkid " & _
              ", screenermstunkid " & _
              ", stageunkid " & _
              ", transactiondate " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM " & mstrDatabaseName & "..tlscreening_stages_tran " & _
             "WHERE stagetranunkid = @stagetranunkid "

            objDataOperation.AddParameter("@stagetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStagetranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintStagetranunkid = CInt(dtRow.Item("stagetranunkid"))
                mintCycleunkid = CInt(dtRow.Item("cycleunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintStageunkid = CBool(dtRow.Item("stageunkid"))
                mdtTransactiondate = CDate(dtRow.Item("transactiondate"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    Public Function GetList(ByVal strTableName As String, _
                            Optional ByVal intcycleunkid As Integer = -1, _
                            Optional ByVal intemployeeunkid As Integer = -1, _
                            Optional ByVal intscreenermstunkid As Integer = -1) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  tlscreening_stages_tran.stagetranunkid " & _
              ", tlscreening_stages_tran.processmstunkid " & _
              ", tlscreening_stages_tran.cycleunkid " & _
              ", isnull(tlstages_master.stage_name,null) as stage_name " & _
              ", tlscreening_stages_tran.employeeunkid " & _
              ", tlscreening_stages_tran.screenermstunkid " & _
              ", tlscreening_stages_tran.stageunkid " & _
              ", tlscreening_stages_tran.transactiondate " & _
              ", tlscreening_stages_tran.voiduserunkid " & _
              ", tlscreening_stages_tran.voiddatetime " & _
              ", tlscreening_stages_tran.voidreason " & _
              ", ISNULL(tlcycle_master.name, '') as period " & _
              ", isnull(tlratings_master.color,'#ffffff') as color " & _
             "FROM " & mstrDatabaseName & "..tlscreening_stages_tran " & _
             " LEFT JOIN tlcycle_master ON tlcycle_master.cycleunkid = tlscreening_stages_tran.cycleunkid " & _
             " LEFT JOIN tlstages_master ON tlstages_master.stageunkid = tlscreening_stages_tran.stageunkid and tlstages_master.isactive = 1 " & _
             "LEFT JOIN tlratings_master ON tlratings_master.stageunkid = tlscreening_stages_tran.stageunkid and tlratings_master.cycleunkid= @cycleunkid " & _
             "WHERE tlscreening_stages_tran.isvoid = 0 "

            If intcycleunkid > 0 Then
                strQ &= "and tlscreening_stages_tran.cycleunkid  = @cycleunkid "
                objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intcycleunkid)
            End If

            If intemployeeunkid > 0 Then
                strQ &= "and tlscreening_stages_tran.employeeunkid  = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intemployeeunkid)
            End If

            If intscreenermstunkid > 0 Then
                strQ &= "and tlscreening_stages_tran.screenermstunkid  = @screenermstunkid "
                objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intscreenermstunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function SaveStageTran(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim exForce As Exception
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            If isExist(mintCycleunkid, mintEmployeeunkid, mintScreenermstunkid, -1, objDataOperation) Then
                If Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                If Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True

        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCycleunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@processmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessmstunkid)
            objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScreenermstunkid)
            objDataOperation.AddParameter("@stageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStageunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

            strQ = "INSERT INTO " & mstrDatabaseName & "..tlscreening_stages_tran ( " & _
              " processmstunkid " & _
              ", cycleunkid " & _
              ", employeeunkid " & _
              ", screenermstunkid " & _
              ", stageunkid " & _
              ", transactiondate " & _
              ", isvoid " & _
            ") VALUES (" & _
              " @processmstunkid " & _
              ", @cycleunkid " & _
              ", @employeeunkid " & _
              ", @screenermstunkid " & _
              ", @stageunkid " & _
              ", getDate() " & _
              ", @isvoid " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintStagetranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD, mintStagetranunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@stageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStageunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCycleunkid.ToString)
            objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScreenermstunkid)
            objDataOperation.AddParameter("@processmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcessmstunkid)
  
            strQ = "UPDATE " & mstrDatabaseName & "..tlscreening_stages_tran SET " & _
              " stageunkid = @stageunkid" & _
              ", transactiondate = getdate() " & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              "WHERE screenermstunkid = @screenermstunkid " & _
              " and cycleunkid = @cycleunkid " & _
              " and employeeunkid = @employeeunkid "& _
              " and processmstunkid = @processmstunkid "

            Call objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT, mintScreenermstunkid, mintProcessmstunkid, -1, mintCycleunkid, mintEmployeeunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Delete(ByVal strProcessmstunkid As String, Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal intScreenerMstUnkId As Integer = -1) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE " & mstrDatabaseName & "..tlscreening_stages_tran SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE processmstunkid in (" & strProcessmstunkid & ") "

            If intScreenerMstUnkId > 0 Then
                strQ &= " AND screenermstunkid = @screenermstunkid "
                objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intScreenerMstUnkId)
            End If

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, -1, strProcessmstunkid, intScreenerMstUnkId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@stagetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isExist(ByVal intCycleId As Integer, _
                            ByVal intEmployeeId As Integer, _
                            ByVal intScreenermstunkid As Integer, _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  stagetranunkid " & _
              ", processmstunkid " & _
              ", cycleunkid " & _
              ", employeeunkid " & _
              ", screenermstunkid " & _
              ", stageunkid " & _
              ", transactiondate " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM " & mstrDatabaseName & "..tlscreening_stages_tran " & _
             "WHERE isvoid = 0 " & _
             " AND cycleunkid  = @cycleunkid " & _
             " AND employeeunkid = @employeeunkid " & _
             " AND screenermstunkid = @screenermstunkid "

            If intUnkid > 0 Then
                strQ &= " AND stagetranunkid <> @stagetranunkid"
            End If

            objDataOperation.AddParameter("@stagetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCycleId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intScreenermstunkid)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, _
                                      ByVal eAuditType As enAuditType, _
                                      Optional ByVal intStagetranunkid As Integer = -1, _
                                      Optional ByVal strProcessmstunkid As String = "", _
                                      Optional ByVal intScreenermstunkid As Integer = -1, _
                                      Optional ByVal intCycleunkid As Integer = -1, _
                                      Optional ByVal intEmployeeunkid As Integer = -1 _
                                      ) As Boolean
        Dim StrQ As String = ""

        Try
            StrQ = "INSERT INTO attlscreening_stages_tran ( " & _
                    "  tranguid " & _
                    ", stagetranunkid " & _
                    ", processmstunkid " & _
                    ", cycleunkid " & _
                    ", employeeunkid " & _
                    ", screenermstunkid " & _
                    ", stageunkid " & _
                    ", transactiondate " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb" & _
                    ") SELECT " & _
                    "  LOWER(NEWID()) " & _
                    ", stagetranunkid " & _
                    ", processmstunkid " & _
                    ", cycleunkid " & _
                    ", employeeunkid " & _
                    ", screenermstunkid " & _
                    ", stageunkid " & _
                    ", getDate() " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb" & _
                    " FROM tlscreening_stages_tran Where 1=1 "

            objDataOperation.ClearParameters()


            If intStagetranunkid > 0 Then
                StrQ &= " AND tlscreening_stages_tran.stagetranunkid = @stagetranunkid "
                objDataOperation.AddParameter("@stagetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStagetranunkid)
            End If

            If strProcessmstunkid.Length > 0 Then
                StrQ &= " AND tlscreening_stages_tran.processmstunkid in (" & strProcessmstunkid & ") "
            End If

            If intCycleunkid > 0 Then
                StrQ &= " AND tlscreening_stages_tran.cycleunkid = @cycleunkid "
                objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCycleunkid)
            End If

            If intScreenermstunkid > 0 Then
                StrQ &= " AND tlscreening_stages_tran.screenermstunkid = @screenermstunkid "
                objDataOperation.AddParameter("@screenermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intScreenermstunkid)
            End If

            If intEmployeeunkid > 0 Then
                StrQ &= " AND tlscreening_stages_tran.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            End If

            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetOtherScreenerData(ByVal strTableName As String, _
                                         ByVal intCycleunkid As Integer, _
                                         ByVal intEmployeeunkid As Integer, _
                                         ByVal intCurrentScreenerid As Integer, _
                                         Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()


        Try
            strQ = "SELECT " & _
                     "CASE " & _
                          "WHEN ISNULL(usermst.firstname + usermst.lastname, '') = '' THEN ISNULL(usermst.username, '') " & _
                          "ELSE ISNULL(usermst.firstname + ' ' +  usermst.lastname, '') " & _
                     "END AS screener " & _
                     ",stage.stage_name " & _
                     ",stage_tran.processmstunkid " & _
                     ",screener.screenermstunkid " & _
                     ",tlresult.totalpoint " & _
                     ",stage_tran.employeeunkid " & _
                "FROM " & mstrDatabaseName & "..tlscreening_stages_tran AS stage_tran " & _
                "JOIN " & mstrDatabaseName & "..tlscreener_master AS screener " & _
                     "ON stage_tran.screenermstunkid = screener.screenermstunkid " & _
                     " and screener.isvoid=0 " & _
                     " and screener.isactive=1 " & _
                  " and screener.cycleunkid = @cycleunkid " & _
                "JOIN hrmsConfiguration..cfuser_master AS usermst " & _
                     "ON usermst.userunkid = screener.mapuserunkid " & _
                "JOIN " & mstrDatabaseName & "..tlstages_master AS stage " & _
                     "ON stage_tran.stageunkid = stage.stageunkid " & _
                "JOIN (SELECT Sum(tlscreening_process_tran.result) as totalpoint,tlscreening_process_tran.processmstunkid,screenermstunkid from tlscreening_process_tran " & _
                     "where isvoid = 0 GROUP by processmstunkid,screenermstunkid) as tlresult " & _
                     "on tlresult.processmstunkid = stage_tran.processmstunkid and tlresult.screenermstunkid = stage_tran.screenermstunkid " & _
                "WHERE stage_tran.isvoid = 0 and stage_tran.cycleunkid = @cycleunkid " & _
                "AND stage_tran.employeeunkid = @employeeunkid "

            If intCurrentScreenerid > 0 Then
                strQ &= "AND stage_tran.screenermstunkid not in ( " & intCurrentScreenerid & " ) "
            End If


            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCycleunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetProcessListfromMaxScreening(ByVal strTableName As String, _
                                         ByVal intCycleunkid As Integer, _
                                         ByVal intStageunkid As Integer, _
                                         Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim intMaxCount As Integer
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objtlsettings_master As New clstlsettings_master

        Dim mdicSetting As Dictionary(Of clstlsettings_master.enTalentConfiguration, String) = objtlsettings_master.GetSettingFromPeriod(intCycleunkid)
        If IsNothing(mdicSetting) = False Then

            If mdicSetting.ContainsKey(clstlsettings_master.enTalentConfiguration.MIN_SCREENER_REQ) Then
                intMaxCount = CInt(mdicSetting(clstlsettings_master.enTalentConfiguration.MIN_SCREENER_REQ))
            End If

        End If

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()


        Try
            strQ = "SELECT " & _
                     "tlscreening_stages_tran.processmstunkid " & _
                     ",COUNT(*) " & _
                    "FROM " & mstrDatabaseName & "..tlscreening_stages_tran  " & _
                    " LEFT JOIN " & mstrDatabaseName & "..tlscreening_process_master " & _
                        "  ON tlscreening_process_master.processmstunkid = tlscreening_stages_tran.processmstunkid " & _
                        " AND tlscreening_process_master.isvoid = 0 " & _
                    "WHERE tlscreening_stages_tran.isvoid = 0 " & _
                        " AND tlscreening_process_master.cycleunkid  = @cycleunkid " & _
                        " AND tlscreening_process_master.stageunkid = @stageunkid " & _
                        " AND tlscreening_process_master.isapproved = 0 " & _
                    " GROUP BY tlscreening_stages_tran.processmstunkid " & _
                    " HAVING COUNT(*) >= " & intMaxCount

            objDataOperation.AddParameter("@cycleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCycleunkid)
            objDataOperation.AddParameter("@stageunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStageunkid)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetProcessListfromMaxScreening; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

End Class
