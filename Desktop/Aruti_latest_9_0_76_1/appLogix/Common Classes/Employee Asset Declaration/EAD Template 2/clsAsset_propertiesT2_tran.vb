﻿'************************************************************************************************************************************
'Class Name : clsAsset_propertiesT2_tran.vb
'Purpose    :
'Date       :08/10/2019
'Written By :Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clsAsset_propertiesT2_tran
    Private Const mstrModuleName = "clsAsset_propertiesT2_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAssetpropertiest2tranunkid As Integer
    Private mintAssetdeclarationt2unkid As Integer
    Private mstrAssetName As String = String.Empty
    Private mstrLocation As String = String.Empty
    Private mstrRegistrationTitleNo As String = String.Empty
    Private mintCountryunkid As Integer
    Private mintCurrencyUnkId As Integer
    Private mintBasecurrencyunkid As Integer
    Private mdecBaseexchangerate As Decimal
    Private mdecExchangerate As Decimal
    Private mdecEstimatedvalue As Decimal
    Private mdecBaseestimatedvalue As Decimal
    Private mstrFundssource As String = String.Empty
    Private mstrAssetlocation As String = String.Empty
    Private mstrAssetuse As String = String.Empty
    Private mblnIsfinalsaved As Boolean
    Private mdtTransactiondate As DateTime
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private minAuditUserid As Integer = 0
    Private minAuditDate As DateTime
    Private minloginemployeeunkid As Integer = 0
    Private minClientIp As String = ""
    Private mstrHostName As String = ""
    Private mstrFormName As String = String.Empty
    Private blnIsFromWeb As Boolean = False

    Private mdtTable As DataTable
    Private mstrDatabaseName As String = ""
    'Hemant (15 Nov 2018) -- Start
    'Enhancement : Changes for NMB Requirement
    Private mdtAcquisition_date As DateTime
    'Hemant (15 Nov 2018) -- End

#End Region

#Region " Properties "
    Public Property _Datasource() As DataTable
        Get
            Return mdtTable
        End Get
        Set(ByVal value As DataTable)
            mdtTable = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetpropertiest2tranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Assetpropertiest2tranunkid() As Integer
        Get
            Return mintAssetpropertiest2tranunkid
        End Get
        Set(ByVal value As Integer)
            mintAssetpropertiest2tranunkid = value
            'Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetdeclarationt2unkid
    ''' Modify By: Hemant
    ''' </summary>
    ''' Public Property _Assetdeclarationt2unkid() As Integer
    Public Property _Assetdeclarationt2unkid(ByVal xDatabase As String) As Integer
        Get
            Return mintAssetdeclarationt2unkid
        End Get
        Set(ByVal value As Integer)
            mintAssetdeclarationt2unkid = value

            Call GetData(xDatabase)
        End Set
    End Property



    ''' <summary>
    ''' Purpose: Get or Set asset_name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Asset_Name() As String
        Get
            Return mstrAssetName
        End Get
        Set(ByVal value As String)
            mstrAssetName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set location
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Location() As String
        Get
            Return mstrLocation
        End Get
        Set(ByVal value As String)
            mstrLocation = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set registration_title_no
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Registration_Title_No() As String
        Get
            Return mstrRegistrationTitleNo
        End Get
        Set(ByVal value As String)
            mstrRegistrationTitleNo = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set currencyunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Currencyunkid() As Integer
        Get
            Return mintCurrencyunkid
        End Get
        Set(ByVal value As Integer)
            mintCurrencyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set basecurrencyunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Basecurrencyunkid() As Integer
        Get
            Return mintBasecurrencyunkid
        End Get
        Set(ByVal value As Integer)
            mintBasecurrencyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set baseexchangerate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Baseexchangerate() As Decimal
        Get
            Return mdecBaseexchangerate
        End Get
        Set(ByVal value As Decimal)
            mdecBaseexchangerate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set exchangerate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Exchangerate() As Decimal
        Get
            Return mdecExchangerate
        End Get
        Set(ByVal value As Decimal)
            mdecExchangerate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set estimated_value
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Estimated_Value() As Decimal
        Get
            Return mdecEstimatedvalue
        End Get
        Set(ByVal value As Decimal)
            mdecEstimatedvalue = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set baseestimated_value
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Baseestimated_Value() As Decimal
        Get
            Return mdecBaseestimatedvalue
        End Get
        Set(ByVal value As Decimal)
            mdecBaseestimatedvalue = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set funds_source
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Funds_Source() As String
        Get
            Return mstrFundssource
        End Get
        Set(ByVal value As String)
            mstrFundssource = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set asset_location
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Asset_Location() As String
        Get
            Return mstrAssetlocation
        End Get
        Set(ByVal value As String)
            mstrAssetlocation = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set asset_use
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Asset_Use() As String
        Get
            Return mstrAssetuse
        End Get
        Set(ByVal value As String)
            mstrAssetuse = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinalsaved
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isfinalsaved() As Boolean
        Get
            Return mblnIsfinalsaved
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinalsaved = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set acquisition_date
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Acquisition_Date() As Date
        Get
            Return mdtAcquisition_Date
        End Get
        Set(ByVal value As Date)
            mdtAcquisition_Date = Value
        End Set
    End Property


    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _AuditUserid() As Integer
        Get
            Return minAuditUserid
        End Get
        Set(ByVal value As Integer)
            minAuditUserid = value
        End Set
    End Property

    Public Property _AuditDate() As DateTime
        Get
            Return minAuditDate
        End Get
        Set(ByVal value As DateTime)
            minAuditDate = value
        End Set
    End Property

    Public Property _ClientIp() As String
        Get
            Return minClientIp
        End Get
        Set(ByVal value As String)
            minClientIp = value
        End Set
    End Property

    Public Property _Loginemployeeunkid() As Integer
        Get
            Return minloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            minloginemployeeunkid = value
        End Set
    End Property

    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public Property _IsFromWeb() As Boolean
        Get
            Return blnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            blnIsFromWeb = value
        End Set
    End Property


    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

#End Region

#Region " Constructor "
    Public Sub New()
        mdtTable = New DataTable("Properties")

        Try
            mdtTable.Columns.Add("assetpropertiest2tranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("assetdeclarationt2unkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("asset_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("location", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("registration_title_no", System.Type.GetType("System.String"))
            mdtTable.Columns.Add("countryunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("currencyunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("basecurrencyunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("baseexchangerate", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("exchangerate", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("estimated_value", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("baseestimated_value", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("funds_source", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("asset_location", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("asset_use", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("isfinalsaved", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("transactiondate", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTable.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            mdtTable.Columns.Add("acquisition_date", System.Type.GetType("System.DateTime"))
            'Hemant (15 Nov 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

    ''' <purpose> Assign all Property variable </purpose>   
    ''' Public Sub GetData()
    Public Sub GetData(ByVal xDatabase As String)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim drRow As DataRow

        objDataOperation = New clsDataOperation


        mstrDatabaseName = xDatabase

        Try
            strQ = "SELECT " & _
              "  assetpropertiest2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", asset_name " & _
              ", location " & _
              ", registration_title_no " & _
              ", ISNULL(countryunkid, 0) AS countryunkid " & _
              ", ISNULL(currencyunkid, 0) AS currencyunkid " & _
              ", ISNULL(basecurrencyunkid, 0) AS basecurrencyunkid " & _
              ", ISNULL(baseexchangerate, 0) AS baseexchangerate " & _
              ", ISNULL(exchangerate, 0) AS exchangerate " & _
              ", ISNULL(estimated_value, 0) AS estimated_value " & _
              ", ISNULL(baseestimated_value, 0) AS baseestimated_value " & _
              ", funds_source " & _
              ", asset_location " & _
              ", asset_use " & _
              ", isfinalsaved " & _
              ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' AS AUD " & _
              ", acquisition_date " & _
             "FROM " & mstrDatabaseName & "..hrasset_propertiesT2_tran " & _
             "WHERE ISNULL(isvoid, 0 ) = 0 " & _
             "AND assetdeclarationt2unkid = @assetdeclarationt2unkid "

            'Hemant (15 Nov 2018) -- [acquisition_date]
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTable.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                drRow = mdtTable.NewRow()
                drRow.Item("assetpropertiest2tranunkid") = CInt(dtRow.Item("assetpropertiest2tranunkid"))
                drRow.Item("assetdeclarationt2unkid") = CInt(dtRow.Item("assetdeclarationt2unkid"))
                drRow.Item("asset_name") = dtRow.Item("asset_name").ToString
                drRow.Item("location") = dtRow.Item("location")
                drRow.Item("registration_title_no") = dtRow.Item("registration_title_no")
                drRow.Item("countryunkid") = CInt(dtRow.Item("countryunkid"))
                drRow.Item("currencyunkid") = CInt(dtRow.Item("currencyunkid"))
                drRow.Item("basecurrencyunkid") = CInt(dtRow.Item("basecurrencyunkid"))
                drRow.Item("baseexchangerate") = CDec(dtRow.Item("baseexchangerate"))
                drRow.Item("exchangerate") = dtRow.Item("exchangerate")
                drRow.Item("estimated_value") = dtRow.Item("estimated_value")
                drRow.Item("baseestimated_value") = dtRow.Item("baseestimated_value")

                drRow.Item("funds_source") = dtRow.Item("funds_source")
                drRow.Item("asset_location") = dtRow.Item("asset_location")

                drRow.Item("asset_use") = dtRow.Item("asset_use")
                drRow.Item("isfinalsaved") = CBool(dtRow.Item("isfinalsaved"))
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    drRow.Item("transactiondate") = DBNull.Value
                Else
                    drRow.Item("transactiondate") = dtRow.Item("transactiondate")
                End If
                drRow.Item("userunkid") = CInt(dtRow.Item("userunkid"))
                drRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                drRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                drRow.Item("voiddatetime") = dtRow.Item("voiddatetime")
                drRow.Item("voidreason") = dtRow.Item("voidreason").ToString

                drRow.Item("AUD") = dtRow.Item("AUD").ToString
                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                drRow.Item("acquisition_date") = dtRow.Item("acquisition_date")
                'Hemant (15 Nov 2018) -- End

                mdtTable.Rows.Add(drRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'Hemant (14 Nov 2018) -- Start
    'Enhancement : Changes for Bind Transactions
    Public Sub GetData(ByVal xDatabase As String, ByVal intAssetdeclarationt2Unkid As Integer, ByVal xDataOp As clsDataOperation)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim drRow As DataRow

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        mintAssetdeclarationt2unkid = intAssetdeclarationt2Unkid

        mstrDatabaseName = xDatabase

        Try
            strQ = "SELECT " & _
              "  assetpropertiest2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", asset_name " & _
              ", location " & _
              ", registration_title_no " & _
              ", ISNULL(countryunkid, 0) AS countryunkid " & _
              ", ISNULL(currencyunkid, 0) AS currencyunkid " & _
              ", ISNULL(basecurrencyunkid, 0) AS basecurrencyunkid " & _
              ", ISNULL(baseexchangerate, 0) AS baseexchangerate " & _
              ", ISNULL(exchangerate, 0) AS exchangerate " & _
              ", ISNULL(estimated_value, 0) AS estimated_value " & _
              ", ISNULL(baseestimated_value, 0) AS baseestimated_value " & _
              ", funds_source " & _
              ", asset_location " & _
              ", asset_use " & _
              ", isfinalsaved " & _
              ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' AS AUD " & _
               ", acquisition_date " & _
             "FROM " & mstrDatabaseName & "..hrasset_propertiesT2_tran " & _
             "WHERE ISNULL(isvoid, 0 ) = 0 " & _
             "AND assetdeclarationt2unkid = @assetdeclarationt2unkid "

            'Hemant (15 Nov 2018) -- [acquisition_date]
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTable.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                drRow = mdtTable.NewRow()
                drRow.Item("assetpropertiest2tranunkid") = CInt(dtRow.Item("assetpropertiest2tranunkid"))
                drRow.Item("assetdeclarationt2unkid") = CInt(dtRow.Item("assetdeclarationt2unkid"))
                drRow.Item("asset_name") = dtRow.Item("asset_name").ToString
                drRow.Item("location") = dtRow.Item("location")
                drRow.Item("registration_title_no") = dtRow.Item("registration_title_no")
                drRow.Item("countryunkid") = CInt(dtRow.Item("countryunkid"))
                drRow.Item("currencyunkid") = CInt(dtRow.Item("currencyunkid"))
                drRow.Item("basecurrencyunkid") = CInt(dtRow.Item("basecurrencyunkid"))
                drRow.Item("baseexchangerate") = CDec(dtRow.Item("baseexchangerate"))
                drRow.Item("exchangerate") = dtRow.Item("exchangerate")
                drRow.Item("estimated_value") = dtRow.Item("estimated_value")
                drRow.Item("baseestimated_value") = dtRow.Item("baseestimated_value")

                drRow.Item("funds_source") = dtRow.Item("funds_source")
                drRow.Item("asset_location") = dtRow.Item("asset_location")

                drRow.Item("asset_use") = dtRow.Item("asset_use")
                drRow.Item("isfinalsaved") = CBool(dtRow.Item("isfinalsaved"))
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    drRow.Item("transactiondate") = DBNull.Value
                Else
                    drRow.Item("transactiondate") = dtRow.Item("transactiondate")
                End If
                drRow.Item("userunkid") = CInt(dtRow.Item("userunkid"))
                drRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                drRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                drRow.Item("voiddatetime") = dtRow.Item("voiddatetime")
                drRow.Item("voidreason") = dtRow.Item("voidreason").ToString

                drRow.Item("AUD") = dtRow.Item("AUD").ToString

                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                drRow.Item("acquisition_date") = dtRow.Item("acquisition_date")
                'Hemant (15 Nov 2018) -- End

                mdtTable.Rows.Add(drRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub
    'Hemant (14 Nov 2018) -- End

    'Gajanan (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Public Sub GetDataByUnkId(ByVal xDatabase As String, ByVal intAssetPropertiesT2tranUnkid As Integer, ByVal xDataOp As clsDataOperation)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()


        mstrDatabaseName = xDatabase

        Try
            strQ = "SELECT " & _
               "  assetpropertiest2tranunkid " & _
               ", assetdeclarationt2unkid " & _
               ", asset_name " & _
               ", location " & _
               ", registration_title_no " & _
               ", ISNULL(countryunkid, 0) AS countryunkid " & _
               ", ISNULL(currencyunkid, 0) AS currencyunkid " & _
               ", ISNULL(basecurrencyunkid, 0) AS basecurrencyunkid " & _
               ", ISNULL(baseexchangerate, 0) AS baseexchangerate " & _
               ", ISNULL(exchangerate, 0) AS exchangerate " & _
               ", ISNULL(estimated_value, 0) AS estimated_value " & _
               ", ISNULL(baseestimated_value, 0) AS baseestimated_value " & _
               ", funds_source " & _
               ", asset_location " & _
               ", asset_use " & _
               ", isfinalsaved " & _
               ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
               ", userunkid " & _
               ", isvoid " & _
               ", voiduserunkid " & _
               ", voiddatetime " & _
               ", voidreason " & _
               ", '' AS AUD " & _
                ", acquisition_date " & _
              "FROM " & mstrDatabaseName & "..hrasset_propertiesT2_tran " & _
              "WHERE assetpropertiest2tranunkid = @assetpropertiest2tranunkid "

            objDataOperation.AddParameter("@assetpropertiest2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetPropertiesT2tranUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAssetpropertiest2tranunkid = CInt(dtRow.Item("assetpropertiest2tranunkid"))
                mintAssetdeclarationt2unkid = CInt(dtRow.Item("assetdeclarationt2unkid"))
                mstrAssetName = dtRow.Item("asset_name").ToString
                mstrLocation = dtRow.Item("location")
                mstrRegistrationTitleNo = dtRow.Item("registration_title_no")
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mintCurrencyUnkId = CInt(dtRow.Item("currencyunkid"))
                mintBasecurrencyunkid = CInt(dtRow.Item("basecurrencyunkid"))
                mdecBaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                mdecExchangerate = dtRow.Item("exchangerate")
                mdecEstimatedvalue = dtRow.Item("estimated_value")
                mdecBaseestimatedvalue = dtRow.Item("baseestimated_value")

                mstrFundssource = dtRow.Item("funds_source")
                mstrAssetlocation = dtRow.Item("asset_location")

                mstrAssetuse = dtRow.Item("asset_use")
                mblnIsfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    mdtTransactiondate = Nothing
                Else
                    mdtTransactiondate = dtRow.Item("transactiondate")
                End If
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mdtAcquisition_date = dtRow.Item("acquisition_date")

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDataByUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub
    'Gajanan (07 Dec 2018) -- End

    Public Function InserByDataTable(ByRef blnChildTableChanged As Boolean, ByVal dtOld As DataTable, ByRef decBankTotal As Decimal, ByVal xCurrentDatetTime As DateTime, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim strUnkIDs As String = ""
        Dim decTotal As Decimal = 0

        Try
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
            objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()
            'Hemant (14 Nov 2018) -- End

            For Each dtRow As DataRow In mdtTable.Rows

                mintAssetpropertiest2tranunkid = CInt(dtRow.Item("assetpropertiest2tranunkid"))
                mstrAssetName = dtRow.Item("asset_name").ToString
                mstrLocation = dtRow.Item("location")
                mstrRegistrationTitleNo = dtRow.Item("registration_title_no").ToString
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mintCurrencyUnkId = CInt(dtRow.Item("currencyunkid"))
                mintBasecurrencyunkid = CInt(dtRow.Item("basecurrencyunkid"))
                mdecBaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                mdecExchangerate = CDec(dtRow.Item("exchangerate"))
                mdecEstimatedvalue = CDec(dtRow.Item("estimated_value"))
                mdecBaseestimatedvalue = CDec(dtRow.Item("baseestimated_value"))
                mstrFundssource = dtRow.Item("funds_source").ToString
                mstrAssetlocation = dtRow.Item("asset_location").ToString
                mstrAssetuse = dtRow.Item("asset_use").ToString
                mblnIsfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                mdtTransactiondate = dtRow.Item("transactiondate")
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtVoiddatetime = Nothing
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                If dtRow.Item("acquisition_date") IsNot DBNull.Value Then
                    mdtAcquisition_date = Convert.ToDateTime(dtRow.Item("acquisition_date"))
                End If
                'Hemant (15 Nov 2018) -- End

                If mintAssetpropertiest2tranunkid <= 0 Then

                    blnChildTableChanged = True

                    'Hemant (14 Nov 2018) -- Start
                    'Enhancement : Changes for Bind Transactions
                    'If Insert() = False Then
                    If Insert(objDataOperation) = False Then
                        'Hemant (14 Nov 2018) -- End
                        Return False
                    End If
                Else
                    If strUnkIDs.Trim = "" Then
                        strUnkIDs = mintAssetpropertiest2tranunkid.ToString
                    Else
                        strUnkIDs &= "," & mintAssetpropertiest2tranunkid.ToString
                    End If
                    'Hemant (14 Nov 2018) -- Start
                    'Enhancement : Changes for Bind Transactions
                    'If dtRow.Item("AUD").ToString() = "U" AndAlso Update(blnChildTableChanged) = False Then
                    If dtRow.Item("AUD").ToString() = "U" AndAlso Update(blnChildTableChanged, objDataOperation) = False Then
                        'Hemant (14 Nov 2018) -- End
                        Return False
                    End If
                End If

            Next


            If dtOld IsNot Nothing AndAlso dtOld.Rows.Count > 0 Then
                Dim dRow() As DataRow
                If strUnkIDs.Trim <> "" Then
                    dRow = dtOld.Select("assetpropertiest2tranunkid NOT IN (" & strUnkIDs & ") ")
                Else
                    dRow = dtOld.Select()
                End If

                For Each dtRow In dRow

                    mintAssetpropertiest2tranunkid = CInt(dtRow.Item("assetpropertiest2tranunkid"))
                    mstrAssetName = dtRow.Item("asset_name").ToString
                    mstrLocation = dtRow.Item("location")
                    mstrRegistrationTitleNo = dtRow.Item("registration_title_no").ToString
                    mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                    mintCurrencyUnkId = CInt(dtRow.Item("currencyunkid"))
                    mintBasecurrencyunkid = CInt(dtRow.Item("basecurrencyunkid"))
                    mdecBaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                    mdecExchangerate = CDec(dtRow.Item("exchangerate"))
                    mdecEstimatedvalue = CDec(dtRow.Item("estimated_value"))
                    mdecBaseestimatedvalue = CDec(dtRow.Item("baseestimated_value"))
                    mstrFundssource = dtRow.Item("funds_source").ToString
                    mstrAssetlocation = dtRow.Item("asset_location").ToString
                    mstrAssetuse = dtRow.Item("asset_use").ToString
                    mblnIsfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                    mdtTransactiondate = dtRow.Item("transactiondate")
                    mblnIsvoid = CBool(dtRow.Item("isvoid"))
                    mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                    mdtVoiddatetime = Nothing
                    mstrVoidreason = dtRow.Item("voidreason").ToString
                    'Hemant (15 Nov 2018) -- Start
                    'Enhancement : Changes for NMB Requirement
                    If dtRow.Item("acquisition_date") IsNot DBNull.Value Then
                        mdtAcquisition_date = Convert.ToDateTime(dtRow.Item("acquisition_date"))
                    End If
                    'Hemant (15 Nov 2018) -- End

                    'Hemant (14 Nov 2018) -- Start
                    'Enhancement : Changes for Bind Transactions
                    'If Void(CInt(dtRow.Item("assetpropertiest2tranunkid")), mintUserunkid, xCurrentDatetTime, "") = False Then
                    If Void(CInt(dtRow.Item("assetpropertiest2tranunkid")), mintUserunkid, xCurrentDatetTime, "", objDataOperation) = False Then
                        'Hemant (14 Nov 2018) -- End
                        Return False
                    End If
                Next
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InserByDataTable; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function

    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intAssetDeclarationt2unkID As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            Dim objExRate As New clsExchangeRate
            Dim dsExRate As DataSet = objExRate.GetList("Currency", True, False, 0, 0, False, Nothing, True)
            Dim dicExRate As Dictionary(Of Integer, String) = (From p In dsExRate.Tables(0) Select New With {.Id = CInt(p.Item("countryunkid")), .Name = p.Item("currency_sign").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Name)
            'Sohail (07 Dec 2018) -- End

            strQ = "SELECT " & _
              "  hrasset_propertiesT2_tran.assetpropertiest2tranunkid " & _
              ", hrasset_propertiesT2_tran.assetdeclarationt2unkid " & _
              ", hrasset_propertiesT2_tran.asset_name " & _
              ", hrasset_propertiesT2_tran.location " & _
              ", hrasset_propertiesT2_tran.registration_title_no " & _
              ", ISNULL(hrasset_propertiesT2_tran.countryunkid, 0) AS countryunkid " & _
              ", ISNULL(hrasset_propertiesT2_tran.currencyunkid, 0) AS currencyunkid " & _
              ", ISNULL(hrasset_propertiesT2_tran.basecurrencyunkid, 0) AS basecurrencyunkid " & _
              ", ISNULL(hrasset_propertiesT2_tran.baseexchangerate, 0) AS baseexchangerate " & _
              ", ISNULL(hrasset_propertiesT2_tran.exchangerate, 0) AS exchangerate " & _
              ", ISNULL(hrasset_propertiesT2_tran.estimated_value, 0) AS estimated_value " & _
              ", ISNULL(hrasset_propertiesT2_tran.baseestimated_value, 0) AS baseestimated_value " & _
              ", hrasset_propertiesT2_tran.funds_source " & _
              ", hrasset_propertiesT2_tran.asset_location " & _
              ", hrasset_propertiesT2_tran.asset_use " & _
              ", hrasset_propertiesT2_tran.isfinalsaved " & _
              ", hrasset_propertiesT2_tran.transactiondate " & _
              ", hrasset_propertiesT2_tran.userunkid " & _
              ", hrasset_propertiesT2_tran.isvoid " & _
              ", hrasset_propertiesT2_tran.voiduserunkid " & _
              ", hrasset_propertiesT2_tran.voiddatetime " & _
              ", hrasset_propertiesT2_tran.voidreason " & _
              ", hrasset_propertiesT2_tran.acquisition_date "

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            strQ &= ", CASE hrasset_propertiesT2_tran.countryunkid "
            For Each pair In dicExRate
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS currency_sign "
            'Sohail (07 Dec 2018) -- End

            strQ &= "FROM hrasset_propertiesT2_tran " & _
             "WHERE ISNULL(hrasset_propertiesT2_tran.isvoid, 0 ) = 0 "
            'Hemant (15 Nov 2018) -- [acquisition_date]

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'If intAssetDeclarationt2unkID > 0 Then
            '    strQ &= " AND assetdeclarationt2unkid = @assetdeclarationt2unkid "
            '    objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2unkID)
            'End If
            If intAssetDeclarationt2unkID > 0 Then
                strQ &= " AND hrasset_propertiesT2_tran.assetdeclarationt2unkid = @assetdeclarationt2unkid "
                objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2unkID)
            Else
                strQ &= " AND 1 = 2 "
            End If
            'Sohail (07 Dec 2018) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrasset_propertiesT2_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal objAD_MasterT2 As clsAssetdeclaration_masterT2 = Nothing) As Boolean
        'Gajanan (07 Dec 2018) - [objAD_MasterT2]
        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Bind Transactions
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Gajanan (07 Dec 2018) -- Start
            objDataOperation.BindTransaction()
            'Gajanan (07 Dec 2018) -- End           
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End

        Try
            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If mintAssetdeclarationt2unkid <= 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If objAD_MasterT2.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintAssetdeclarationt2unkid = intNewUnkId
                End If
            ElseIf mintAssetdeclarationt2unkid > 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                If objAD_MasterT2.Update(enAction.EDIT_ONE, objDataOperation) = False Then
                    Return False
                End If
            End If

            objDataOperation.ClearParameters()
            'Gajanan (07 Dec 2018) -- End

            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)
            objDataOperation.AddParameter("@asset_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssetName.ToString)
            objDataOperation.AddParameter("@location", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLocation.ToString)
            objDataOperation.AddParameter("@registration_title_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRegistrationTitleNo.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyUnkId.ToString)
            objDataOperation.AddParameter("@basecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasecurrencyunkid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
            objDataOperation.AddParameter("@exchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchangerate.ToString)
            objDataOperation.AddParameter("@estimated_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEstimatedvalue.ToString)
            objDataOperation.AddParameter("@baseestimated_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseestimatedvalue.ToString)

            objDataOperation.AddParameter("@funds_source", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundssource.ToString)
            objDataOperation.AddParameter("@asset_location", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssetlocation.ToString)


            objDataOperation.AddParameter("@asset_use", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssetuse.ToString)
            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            If mdtAcquisition_date = Nothing Then
                objDataOperation.AddParameter("@acquisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@acquisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAcquisition_date.ToString)
            End If
            'Hemant (15 Nov 2018) -- End


            strQ = "INSERT INTO hrasset_propertiesT2_tran ( " & _
              "  assetdeclarationt2unkid " & _
              ", asset_name " & _
              ", location " & _
              ", registration_title_no " & _
              ", countryunkid " & _
              ", currencyunkid " & _
              ", basecurrencyunkid " & _
              ", baseexchangerate " & _
              ", exchangerate " & _
              ", estimated_value " & _
              ", baseestimated_value " & _
              ", funds_source " & _
              ", asset_location " & _
              ", asset_use " & _
              ", isfinalsaved " & _
              ", transactiondate" & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
               ", acquisition_date " & _
            ") VALUES (" & _
              "  @assetdeclarationt2unkid " & _
              ", @asset_name " & _
              ", @location " & _
              ", @registration_title_no " & _
              ", @countryunkid " & _
              ", @currencyunkid " & _
              ", @basecurrencyunkid " & _
              ", @baseexchangerate " & _
              ", @exchangerate " & _
              ", @estimated_value " & _
              ", @baseestimated_value" & _
              ", @funds_source " & _
              ", @asset_location " & _
              ", @asset_use " & _
              ", @isfinalsaved " & _
              ", @transactiondate" & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @acquisition_date " & _
            "); SELECT @@identity"

            'Hemant (15 Nov 2018) -- [acquisition_date]
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAssetpropertiest2tranunkid = dsList.Tables(0).Rows(0).Item(0)


            'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", mintAssetdeclarationt2unkid, "hrasset_propertiesT2_tran", "assetpropertiest2tranunkid", mintAssetpropertiest2tranunkid, 1, 1, , mintUserunkid) = False Then
            '    Return False
            'End If

            If Insert_AtTranLog(objDataOperation, 1) = False Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Sohail (07 Dec 2018) -- End
                Return False
            End If
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function


    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrasset_propertiesT2_tran) </purpose>
    Public Function Update(ByRef blnChildTableChanged As Boolean, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal objAD_MasterT2 As clsAssetdeclaration_masterT2 = Nothing) As Boolean
        'Sohail (07 Dec 2018) - [objAD_MasterT2]
        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Bind Transactions
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Gajanan (07 Dec 2018) -- Start
            objDataOperation.BindTransaction()
            'Gajanan (07 Dec 2018) -- End           
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End


        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If mintAssetdeclarationt2unkid <= 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If objAD_MasterT2.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintAssetdeclarationt2unkid = intNewUnkId
                End If
            ElseIf mintAssetdeclarationt2unkid > 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                If objAD_MasterT2.Update(enAction.EDIT_ONE, objDataOperation) = False Then
                    Return False
                End If
            End If
            objDataOperation.ClearParameters()
            'Sohail (07 Dec 2018) -- End
            objDataOperation.AddParameter("@assetpropertiest2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetpropertiest2tranunkid.ToString)
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)
            objDataOperation.AddParameter("@asset_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssetName.ToString)
            objDataOperation.AddParameter("@location", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLocation.ToString)
            objDataOperation.AddParameter("@registration_title_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRegistrationTitleNo.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyUnkId.ToString)
            objDataOperation.AddParameter("@basecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasecurrencyunkid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
            objDataOperation.AddParameter("@exchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchangerate.ToString)
            objDataOperation.AddParameter("@estimated_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEstimatedvalue.ToString)
            objDataOperation.AddParameter("@baseestimated_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseestimatedvalue.ToString)
            objDataOperation.AddParameter("@funds_source", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundssource.ToString)
            objDataOperation.AddParameter("@asset_location", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssetlocation.ToString)


            objDataOperation.AddParameter("@asset_use", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssetuse.ToString)
            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            If mdtAcquisition_date = Nothing Then
                objDataOperation.AddParameter("@acquisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@acquisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAcquisition_date.ToString)
            End If
            'Hemant (15 Nov 2018) -- End

            strQ = "UPDATE hrasset_propertiesT2_tran SET " & _
              "  assetdeclarationt2unkid = @assetdeclarationt2unkid" & _
              ", asset_name = @asset_name" & _
              ", location = @location" & _
              ", registration_title_no = @registration_title_no" & _
              ", countryunkid = @countryunkid" & _
              ", currencyunkid = @currencyunkid " & _
              ", basecurrencyunkid = @basecurrencyunkid" & _
              ", baseexchangerate = @baseexchangerate" & _
              ", exchangerate = @exchangerate" & _
              ", estimated_value = @estimated_value" & _
              ", baseestimated_value = @baseestimated_value" & _
              ", funds_source = @funds_source" & _
              ", asset_location = @asset_location" & _
              ", asset_use = @asset_use " & _
              ", isfinalsaved = @isfinalsaved " & _
              ", transactiondate = @transactiondate " & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", acquisition_date = @acquisition_date " & _
            "WHERE assetpropertiest2tranunkid = @assetpropertiest2tranunkid "

            'Hemant (15 Nov 2018) -- [acquisition_date]
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", mintAssetdeclarationt2unkid, "hrasset_propertiesT2_tran", "assetpropertiest2tranunkid", mintAssetpropertiest2tranunkid, 2, 2, , mintUserunkid) = False Then
            '    Return False
            'End If

            If IsTableDataUpdate(mintAssetpropertiest2tranunkid, objDataOperation) = False Then
            If Insert_AtTranLog(objDataOperation, 2) = False Then
                    'Sohail (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    'Sohail (07 Dec 2018) -- End
                Return False
            End If
            End If

            blnChildTableChanged = True
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function

    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrasset_propertiesT2_tran) </purpose>
    Public Function Void(ByVal intUnkid As Integer, _
                         ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Bind Transactions
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Gajanan (07 Dec 2018) -- Start
            objDataOperation.BindTransaction()
            'Gajanan (07 Dec 2018) -- End            
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End

        Try

            'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", mintAssetdeclarationt2unkid, "hrasset_propertiesT2_tran", "assetpropertiest2tranunkid", intUnkid, 2, 3, False, ) = False Then

            '    Return False
            'End If

            strQ = "UPDATE hrasset_propertiesT2_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE assetpropertiest2tranunkid = @assetpropertiest2tranunkid "

            objDataOperation.AddParameter("@assetpropertiest2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If Insert_AtTranLog(objDataOperation, 3) = False Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Sohail (07 Dec 2018) -- End
                Return False
            End If
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function

    Public Function VoidByAssetDeclarationt2unkID(ByVal intAssetDeclarationt2UnkID As Integer, _
                                        ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, ByVal intParentAuditType As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Bind Transactions
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End

        Try

            'If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", intAssetDeclarationt2UnkID, "hrasset_propertiesT2_tran", "assetpropertiest2tranunkid", intParentAuditType, 3, , " ISNULL(isvoid, 0) = 0 ", , intVoidUserID) = False Then

            '    Return False
            'End If

            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            strQ = "INSERT INTO athrasset_propertiesT2_tran ( " & _
                     "  assetpropertiest2tranunkid " & _
                      ", assetdeclarationt2unkid " & _
                      ", asset_name " & _
                      ", location " & _
                      ", registration_title_no " & _
                      ", countryunkid " & _
                      ", currencyunkid " & _
                      ", basecurrencyunkid " & _
                      ", baseexchangerate " & _
                      ", exchangerate " & _
                      ", estimated_value " & _
                      ", baseestimated_value " & _
                      ", funds_source " & _
                      ", asset_location " & _
                      ", asset_use " & _
                      ", isfinalsaved " & _
                      ", transactiondate" & _
                     ", auditdatetime " & _
                     ", audittype " & _
                     ", audituserunkid " & _
                     ", loginemployeeunkid " & _
                     ", ip " & _
                     ", host " & _
                     ", form_name " & _
                     ", isweb " & _
                     ", acquisition_date ) " & _
                 "SELECT " & _
                     "  assetpropertiest2tranunkid " & _
                      ", assetdeclarationt2unkid " & _
                      ", asset_name " & _
                      ", location " & _
                      ", registration_title_no " & _
                      ", countryunkid " & _
                      ", currencyunkid " & _
                      ", basecurrencyunkid " & _
                      ", baseexchangerate " & _
                      ", exchangerate " & _
                      ", estimated_value " & _
                      ", baseestimated_value " & _
                      ", funds_source " & _
                      ", asset_location " & _
                      ", asset_use " & _
                      ", isfinalsaved " & _
                      ", transactiondate" & _
                     ", GETDATE() " & _
                     ", 3 " & _
                     ", @audituserunkid " & _
                     ", @loginemployeeunkid " & _
                     ", @ip " & _
                     ", @host " & _
                     ", @form_name " & _
                     ", @isweb " & _
                     ", acquisition_date " & _
             "FROM hrasset_propertiesT2_tran " & _
                         "WHERE isvoid = 0 " & _
                         "AND assetdeclarationt2unkid = @assetdeclarationt2unkid "

            'Hemant (15 Nov 2018) -- [acquisition_date]
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)

            Call objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ClearParameters()
            'Hemant (14 Nov 2018) -- End

            strQ = "UPDATE hrasset_propertiesT2_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE assetdeclarationt2unkid = @assetdeclarationt2unkid "

            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'If Insert_AtTranLog(objDataOperation, 3) = False Then
            '    Return False
            'End If
            'Hemant (14 Nov 2018) -- End

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidByAssetDeclarationt2UnkID; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@assetpropertiest2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  assetpropertiest2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", asset_name " & _
              ", location " & _
              ", registration_title_no " & _
              ", ISNULL(countryunkid, 0) AS countryunkid " & _
              ", ISNULL(currencyunkid, 0) AS currencyunkid " & _
              ", ISNULL(basecurrencyunkid, 0) AS basecurrencyunkid " & _
              ", ISNULL(baseexchangerate, 0) AS baseexchangerate " & _
              ", ISNULL(exchangerate, 0) AS exchangerate " & _
              ", ISNULL(estimated_value, 0) AS estimated_value " & _
              ", ISNULL(baseestimated_value, 0) AS baseestimated_value " & _
              ", funds_source " & _
              ", asset_location " & _
              ", asset_use " & _
              ", isfinalsaved " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", acquisition_date " & _
             "FROM hrasset_propertiesT2_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            'Hemant (15 Nov 2018) -- [acquisition_date]
            If intUnkid > 0 Then
                strQ &= " AND assetpropertiest2tranunkid <> @assetpropertiest2tranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@assetpropertiest2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (22 Nov 2018) -- Start
    'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
    'Public Function getClientsList(Optional ByVal strListName As String = "List", Optional ByVal blnFlag As Boolean = False) As DataSet
    '    Dim strQ As String = ""
    '    Try
    '        Dim objDataOperation As New clsDataOperation

    '        If blnFlag Then
    '            strQ = "SELECT @Select As Name, 0 As id UNION "
    '        End If

    '        strQ &= "SELECT @Supplier As Name, 1 As id " & _
    '                "UNION SELECT @Borrower As Name, 2 As id " & _
    '                "UNION SELECT @Other As Name, 3 As id " & _
    '                "Order By id "

    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 72, "Select"))
    '        objDataOperation.AddParameter("@Supplier", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 73, "Supplier"))
    '        objDataOperation.AddParameter("@Borrower", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 74, "Borrower"))
    '        objDataOperation.AddParameter("@Other", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 74, "Other"))

    '        Return objDataOperation.ExecQuery(strQ, strListName)

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "[getGenderList]")
    '    End Try
    'End Function
    'Sohail (22 Nov 2018) -- End

    Public Function Insert_AtTranLog(ByVal objDoOps As clsDataOperation, ByVal AuditType As Integer)
        Dim StrQ As String = String.Empty
        Dim extForce As Exception
        Dim dsList As New DataSet

        Try
            objDoOps.ClearParameters()
            objDoOps.AddParameter("@assetpropertiest2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetpropertiest2tranunkid.ToString)
            objDoOps.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)
            objDoOps.AddParameter("@asset_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssetName.ToString)
            objDoOps.AddParameter("@location", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLocation.ToString)
            objDoOps.AddParameter("@registration_title_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRegistrationTitleNo.ToString)
            objDoOps.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDoOps.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyUnkId.ToString)
            objDoOps.AddParameter("@basecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasecurrencyunkid.ToString)
            objDoOps.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
            objDoOps.AddParameter("@exchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchangerate.ToString)
            objDoOps.AddParameter("@estimated_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEstimatedvalue.ToString)
            objDoOps.AddParameter("@baseestimated_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseestimatedvalue.ToString)

            objDoOps.AddParameter("@funds_source", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFundssource.ToString)
            objDoOps.AddParameter("@asset_location", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssetlocation.ToString)


            objDoOps.AddParameter("@asset_use", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssetuse.ToString)
            objDoOps.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            If mdtTransactiondate = Nothing Then
                objDoOps.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDoOps.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            objDoOps.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDoOps.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minAuditUserid)
            objDoOps.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid)
            objDoOps.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDoOps.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDoOps.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDoOps.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)
            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            If mdtAcquisition_date = Nothing Then
                objDoOps.AddParameter("@acquisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDoOps.AddParameter("@acquisition_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAcquisition_date.ToString)
            End If
            'Hemant (15 Nov 2018) -- End


            StrQ = "INSERT INTO athrasset_propertiesT2_tran ( " & _
              "  assetpropertiest2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", asset_name " & _
              ", location " & _
              ", registration_title_no " & _
              ", countryunkid " & _
              ", currencyunkid " & _
              ", basecurrencyunkid " & _
              ", baseexchangerate " & _
              ", exchangerate " & _
              ", estimated_value " & _
              ", baseestimated_value " & _
              ", funds_source " & _
              ", asset_location " & _
              ", asset_use " & _
              ", isfinalsaved " & _
              ", transactiondate" & _
               ", auditdatetime " & _
                     ", audittype " & _
                     ", audituserunkid " & _
                     ", loginemployeeunkid " & _
                     ", ip " & _
                     ", host " & _
                     ", form_name " & _
                     ", isweb " & _
              ", acquisition_date " & _
            ") VALUES (" & _
              "  @assetpropertiest2tranunkid " & _
              ", @assetdeclarationt2unkid " & _
              ", @asset_name " & _
              ", @location " & _
              ", @registration_title_no " & _
              ", @countryunkid " & _
              ", @currencyunkid " & _
              ", @basecurrencyunkid " & _
              ", @baseexchangerate " & _
              ", @exchangerate " & _
              ", @estimated_value " & _
              ", @baseestimated_value" & _
              ", @funds_source " & _
              ", @asset_location " & _
              ", @asset_use " & _
              ", @isfinalsaved " & _
              ", @transactiondate" & _
              ", GETDATE() " & _
                     ", @audittype " & _
                     ", @audituserunkid " & _
                     ", @loginemployeeunkid " & _
                     ", @ip " & _
                     ", @host " & _
                     ", @form_name " & _
              ", @isweb " & _
             ", @acquisition_date ) "

            'Hemant (15 Nov 2018) -- [acquisition_date]
            dsList = objDoOps.ExecQuery(StrQ, "List")

            If objDoOps.ErrorMessage <> "" Then
                extForce = New Exception(objDoOps.ErrorNumber & ": " & objDoOps.ErrorMessage)
                Throw extForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
        End Try
        Return True

    End Function

    Public Function IsTableDataUpdate(ByVal unkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'strQ = "select TOP 1 * from athrasset_propertiesT2_tran where assetpropertiest2tranunkid = @assetpropertiest2tranunkid and assetdeclarationt2unkid = @assetdeclarationt2unkid and audittype = 2 ORDER BY auditdatetime DESC"
            strQ = "select TOP 1 * from athrasset_propertiesT2_tran where assetpropertiest2tranunkid = @assetpropertiest2tranunkid and assetdeclarationt2unkid = @assetdeclarationt2unkid and audittype <> 3 ORDER BY auditdatetime DESC"
            'Gajanan (07 Dec 2018) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetpropertiest2tranunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, unkid)
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dr As DataRow In dsList.Tables(0).Rows
                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                If dr("acquisition_date") Is DBNull.Value Then
                    'Hemant (15 Nov 2018) -- End
                    Return False
                ElseIf dr("asset_name").ToString() = mstrAssetName AndAlso dr("location").ToString() = mstrLocation AndAlso _
                   dr("registration_title_no").ToString() = mstrRegistrationTitleNo AndAlso dr("countryunkid").ToString() = mintCountryunkid _
                   AndAlso dr("estimated_value").ToString() = mdecEstimatedvalue AndAlso dr("funds_source").ToString() = mstrFundssource _
                   AndAlso dr("asset_location").ToString() = mstrAssetlocation AndAlso dr("asset_use").ToString() = mstrAssetuse _
                   AndAlso dr("location").ToString() = mstrLocation AndAlso CDate(dr("acquisition_date")) = mdtAcquisition_date Then
                    'Hemant (15 Nov 2018) -- [acquisition_date ]
                    Return True
                Else
                    Return False
                End If
            Next
            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

End Class