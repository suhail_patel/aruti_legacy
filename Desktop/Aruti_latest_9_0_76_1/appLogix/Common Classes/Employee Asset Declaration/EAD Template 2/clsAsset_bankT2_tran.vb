﻿'************************************************************************************************************************************
'Class Name : clsAsset_bankT2_tran.vb
'Purpose    :
'Date       :08/10/2019
'Written By :Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clsAsset_bankT2_tran
    Private Const mstrModuleName = "clsAsset_bankT2_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAssetbanktrant2unkid As Integer
    Private mintAssetdeclarationt2unkid As Integer
    Private mstrBankName As String = String.Empty
    Private mstrAccountype As String = String.Empty
    Private mstrAccountNo As String = String.Empty
    Private mdecAmount As Decimal
    Private mdecBaseamount As Decimal
    Private mintCountryunkid As Integer
    Private mintCurrencyUnkId As Integer
    Private mintBasecurrencyUnkid As Integer
    Private mdecBaseexchangerate As Decimal
    Private mdecExchangerate As Decimal
    Private mstrDepositssource As String = String.Empty
    Private mblnIsfinalsaved As Boolean
    Private mdtTransactiondate As DateTime
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private minAuditUserid As Integer = 0
    Private minAuditDate As DateTime
    Private minloginemployeeunkid As Integer = 0
    Private minClientIp As String = ""
    Private mstrHostName As String = ""
    Private mstrFormName As String = String.Empty
    Private blnIsFromWeb As Boolean = False

    Private mdtTable As DataTable
    Private mstrDatabaseName As String = ""
    'Hemant (19 Nov 2018) -- Start
    'Enhancement : Changes for NMB Requirement
    Private mintAccountTypeUnkid As Integer
    'Hemant (19 Nov 2018) -- End
    'Sohail (30 Nov 2018) -- Start
    'NMB Enhancement - On employee declaration, bank account and dependant bank fields there should be a free text box named "specify" for user to input other account names in 75.1.
    Private mstrSpecify As String = String.Empty
    'Sohail (30 Nov 2018) -- End

#End Region

#Region " Properties "
    Public Property _Datasource() As DataTable
        Get
            Return mdtTable
        End Get
        Set(ByVal value As DataTable)
            mdtTable = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetbankt2tranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Assetbanktrant2unkid() As Integer
        Get
            Return mintAssetbanktrant2unkid
        End Get
        Set(ByVal value As Integer)
            mintAssetbanktrant2unkid = value
            'Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetdeclarationt2unkid
    ''' </summary>    
    Public Property _Assetdeclarationt2unkid(ByVal xDatabase As String) As Integer
        Get
            Return mintAssetdeclarationt2unkid
        End Get
        Set(ByVal value As Integer)
            mintAssetdeclarationt2unkid = value
            Call GetData(xDatabase)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set bank_name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Bank_Name() As String
        Get
            Return mstrBankName
        End Get
        Set(ByVal value As String)
            mstrBankName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set account_type
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Account_Type() As String
        Get
            Return mstrAccountype
        End Get
        Set(ByVal value As String)
            mstrAccountype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set account_no
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Account_No() As String
        Get
            Return mstrAccountNo
        End Get
        Set(ByVal value As String)
            mstrAccountNo = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set currencyunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Currencyunkid() As Integer
        Get
            Return mintCurrencyunkid
        End Get
        Set(ByVal value As Integer)
            mintCurrencyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set basecurrencyunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Basecurrencyunkid() As Integer
        Get
            Return mintBasecurrencyunkid
        End Get
        Set(ByVal value As Integer)
            mintBasecurrencyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set baseexchangerate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Baseexchangerate() As Decimal
        Get
            Return mdecBaseexchangerate
        End Get
        Set(ByVal value As Decimal)
            mdecBaseexchangerate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set exchangerate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Exchangerate() As Decimal
        Get
            Return mdecExchangerate
        End Get
        Set(ByVal value As Decimal)
            mdecExchangerate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set deposits_source
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Deposits_Source() As String
        Get
            Return mstrDepositssource
        End Get
        Set(ByVal value As String)
            mstrDepositssource = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinalsaved
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isfinalsaved() As Boolean
        Get
            Return mblnIsfinalsaved
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinalsaved = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set amount
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Amount() As Decimal
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal)
            mdecAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set baseamount
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Baseamount() As Decimal
        Get
            Return mdecBaseamount
        End Get
        Set(ByVal value As Decimal)
            mdecBaseamount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accounttypeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Accounttypeunkid() As Integer
        Get
            Return mintAccounttypeunkid
        End Get
        Set(ByVal value As Integer)
            mintAccounttypeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set specify
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Specify() As String
        Get
            Return mstrSpecify
        End Get
        Set(ByVal value As String)
            mstrSpecify = Value
        End Set
    End Property


    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _AuditUserid() As Integer
        Get
            Return minAuditUserid
        End Get
        Set(ByVal value As Integer)
            minAuditUserid = value
        End Set
    End Property

    Public Property _AuditDate() As DateTime
        Get
            Return minAuditDate
        End Get
        Set(ByVal value As DateTime)
            minAuditDate = value
        End Set
    End Property

    Public Property _ClientIp() As String
        Get
            Return minClientIp
        End Get
        Set(ByVal value As String)
            minClientIp = value
        End Set
    End Property

    Public Property _Loginemployeeunkid() As Integer
        Get
            Return minloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            minloginemployeeunkid = value
        End Set
    End Property

    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public Property _IsFromWeb() As Boolean
        Get
            Return blnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            blnIsFromWeb = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

#End Region

#Region " Constructor "
    Public Sub New()
        mdtTable = New DataTable("BankAsset")

        Try
            mdtTable.Columns.Add("assetbankt2tranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("assetdeclarationt2unkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("bank_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("account_type", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("account_no", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("amount", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("baseamount", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("countryunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("currencyunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("basecurrencyunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("baseexchangerate", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("exchangerate", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("deposits_source", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("isfinalsaved", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("transactiondate", System.Type.GetType("System.DateTime"))
            mdtTable.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTable.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            'Hemant (19 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            mdtTable.Columns.Add("accounttypeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            'Hemant (19 Nov 2018) -- End
            'Sohail (30 Nov 2018) -- Start
            'NMB Enhancement - On employee declaration, bank account and dependant bank fields there should be a free text box named "specify" for user to input other account names in 75.1.
            mdtTable.Columns.Add("specify", System.Type.GetType("System.String")).DefaultValue = ""
            'Sohail (30 Nov 2018) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region


    Public Sub GetData(ByVal xDatabase As String)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim drRow As DataRow

        objDataOperation = New clsDataOperation


        mstrDatabaseName = xDatabase


        Try
            strQ = "SELECT " & _
              "  assetbankt2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", bank_name " & _
              ", account_type " & _
              ", account_no " & _
              ", ISNULL(amount, 0) AS amount " & _
              ", ISNULL(baseamount, 0) AS baseamount " & _
              ", ISNULL(countryunkid, 0) AS countryunkid " & _
              ", ISNULL(currencyunkid, 0) AS currencyunkid " & _
              ", ISNULL(basecurrencyunkid, 0) AS basecurrencyunkid " & _
              ", ISNULL(baseexchangerate, 0) AS baseexchangerate " & _
              ", ISNULL(exchangerate, 0) AS exchangerate " & _
              ", deposits_source " & _
              ", isfinalsaved " & _
              ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' AS AUD " & _
              ", ISNULL(accounttypeunkid, 0) AS accounttypeunkid " & _
              ", ISNULL(specify, '') AS specify " & _
             "FROM " & mstrDatabaseName & "..hrasset_bankT2_tran " & _
             "WHERE ISNULL(isvoid, 0 ) = 0 " & _
             "AND assetdeclarationt2unkid = @assetdeclarationt2unkid "
            'Sohail (30 Nov 2018) - [specify]
            'Hemant (19 Nov 2018) -- [accounttypeunkid]
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTable.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                drRow = mdtTable.NewRow()
                drRow.Item("assetbankt2tranunkid") = CInt(dtRow.Item("assetbankt2tranunkid"))
                drRow.Item("assetdeclarationt2unkid") = CInt(dtRow.Item("assetdeclarationt2unkid"))
                drRow.Item("bank_name") = dtRow.Item("bank_name").ToString
                drRow.Item("account_type") = dtRow.Item("account_type").ToString
                drRow.Item("account_no") = dtRow.Item("account_no").ToString
                drRow.Item("amount") = CDec(dtRow.Item("amount"))
                drRow.Item("baseamount") = CDec(dtRow.Item("baseamount"))
                drRow.Item("countryunkid") = CInt(dtRow.Item("countryunkid"))
                drRow.Item("currencyunkid") = CInt(dtRow.Item("currencyunkid"))
                drRow.Item("basecurrencyunkid") = CInt(dtRow.Item("basecurrencyunkid"))
                drRow.Item("baseexchangerate") = CDec(dtRow.Item("baseexchangerate"))
                drRow.Item("exchangerate") = CDec(dtRow.Item("exchangerate"))
                drRow.Item("deposits_source") = dtRow.Item("deposits_source").ToString
                drRow.Item("isfinalsaved") = CBool(dtRow.Item("isfinalsaved"))
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    drRow.Item("transactiondate") = DBNull.Value
                Else
                    drRow.Item("transactiondate") = dtRow.Item("transactiondate")
                End If
                drRow.Item("userunkid") = CInt(dtRow.Item("userunkid"))
                drRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                drRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                drRow.Item("voiddatetime") = dtRow.Item("voiddatetime")
                drRow.Item("voidreason") = dtRow.Item("voidreason").ToString
                drRow.Item("AUD") = dtRow.Item("AUD").ToString

                'Hemant (19 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                drRow.Item("accounttypeunkid") = CInt(dtRow.Item("accounttypeunkid"))
                'Hemant (19 Nov 2018) -- End
                'Sohail (30 Nov 2018) -- Start
                'NMB Enhancement - On employee declaration, bank account and dependant bank fields there should be a free text box named "specify" for user to input other account names in 75.1.
                drRow.Item("specify") = dtRow.Item("specify").ToString
                'Sohail (30 Nov 2018) -- End

                mdtTable.Rows.Add(drRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'Hemant (14 Nov 2018) -- Start
    'Enhancement : Changes for Bind Transactions
    Public Sub GetData(ByVal xDatabase As String, ByVal intAssetdeclarationt2Unkid As Integer, ByVal xDataOp As clsDataOperation)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim drRow As DataRow

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        mintAssetdeclarationt2unkid = intAssetdeclarationt2Unkid

        mstrDatabaseName = xDatabase


        Try
            strQ = "SELECT " & _
              "  assetbankt2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", bank_name " & _
              ", account_type " & _
              ", account_no " & _
              ", ISNULL(amount, 0) AS amount " & _
              ", ISNULL(baseamount, 0) AS baseamount " & _
              ", ISNULL(countryunkid, 0) AS countryunkid " & _
              ", ISNULL(currencyunkid, 0) AS currencyunkid " & _
              ", ISNULL(basecurrencyunkid, 0) AS basecurrencyunkid " & _
              ", ISNULL(baseexchangerate, 0) AS baseexchangerate " & _
              ", ISNULL(exchangerate, 0) AS exchangerate " & _
              ", deposits_source " & _
              ", isfinalsaved " & _
              ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' AS AUD " & _
              ", ISNULL(accounttypeunkid, 0) AS accounttypeunkid " & _
              ", ISNULL(specify, '') AS specify " & _
             "FROM " & mstrDatabaseName & "..hrasset_bankT2_tran " & _
             "WHERE ISNULL(isvoid, 0 ) = 0 " & _
             "AND assetdeclarationt2unkid = @assetdeclarationt2unkid "
            'Sohail (30 Nov 2018) - [specify]
            'Hemant (19 Nov 2018) -- [accounttypeunkid]
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTable.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                drRow = mdtTable.NewRow()
                drRow.Item("assetbankt2tranunkid") = CInt(dtRow.Item("assetbankt2tranunkid"))
                drRow.Item("assetdeclarationt2unkid") = CInt(dtRow.Item("assetdeclarationt2unkid"))
                drRow.Item("bank_name") = dtRow.Item("bank_name").ToString
                drRow.Item("account_type") = dtRow.Item("account_type").ToString
                drRow.Item("account_no") = dtRow.Item("account_no").ToString
                drRow.Item("amount") = CDec(dtRow.Item("amount"))
                drRow.Item("baseamount") = CDec(dtRow.Item("baseamount"))
                drRow.Item("countryunkid") = CInt(dtRow.Item("countryunkid"))
                drRow.Item("currencyunkid") = CInt(dtRow.Item("currencyunkid"))
                drRow.Item("basecurrencyunkid") = CInt(dtRow.Item("basecurrencyunkid"))
                drRow.Item("baseexchangerate") = CDec(dtRow.Item("baseexchangerate"))
                drRow.Item("exchangerate") = CDec(dtRow.Item("exchangerate"))
                drRow.Item("deposits_source") = dtRow.Item("deposits_source").ToString
                drRow.Item("isfinalsaved") = CBool(dtRow.Item("isfinalsaved"))
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    drRow.Item("transactiondate") = DBNull.Value
                Else
                    drRow.Item("transactiondate") = dtRow.Item("transactiondate")
                End If
                drRow.Item("userunkid") = CInt(dtRow.Item("userunkid"))
                drRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                drRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                drRow.Item("voiddatetime") = dtRow.Item("voiddatetime")
                drRow.Item("voidreason") = dtRow.Item("voidreason").ToString
                drRow.Item("AUD") = dtRow.Item("AUD").ToString

                'Hemant (19 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                drRow.Item("accounttypeunkid") = CInt(dtRow.Item("accounttypeunkid"))
                'Hemant (19 Nov 2018) -- End
                'Sohail (30 Nov 2018) -- Start
                'NMB Enhancement - On employee declaration, bank account and dependant bank fields there should be a free text box named "specify" for user to input other account names in 75.1.
                drRow.Item("specify") = dtRow.Item("specify").ToString
                'Sohail (30 Nov 2018) -- End

                mdtTable.Rows.Add(drRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Sub
    'Hemant (14 Nov 2018) -- End

    'Sohail (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Public Sub GetDataByUnkId(ByVal xDatabase As String, ByVal intAssetBankT2TranUnkid As Integer, ByVal xDataOp As clsDataOperation)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()


        mstrDatabaseName = xDatabase

        Try
            strQ = "SELECT " & _
              "  assetbankt2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", bank_name " & _
              ", account_type " & _
              ", account_no " & _
              ", ISNULL(amount, 0) AS amount " & _
              ", ISNULL(baseamount, 0) AS baseamount " & _
              ", ISNULL(countryunkid, 0) AS countryunkid " & _
              ", ISNULL(currencyunkid, 0) AS currencyunkid " & _
              ", ISNULL(basecurrencyunkid, 0) AS basecurrencyunkid " & _
              ", ISNULL(baseexchangerate, 0) AS baseexchangerate " & _
              ", ISNULL(exchangerate, 0) AS exchangerate " & _
              ", deposits_source " & _
              ", isfinalsaved " & _
              ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' AS AUD " & _
              ", ISNULL(accounttypeunkid, 0) AS accounttypeunkid " & _
              ", ISNULL(specify, '') AS specify " & _
             "FROM " & mstrDatabaseName & "..hrasset_bankT2_tran " & _
             "WHERE assetbankt2tranunkid = @assetbankt2tranunkid "

            objDataOperation.AddParameter("@assetbankt2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetBankT2TranUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAssetbanktrant2unkid = CInt(dtRow.Item("assetbankt2tranunkid"))
                mintAssetdeclarationt2unkid = CInt(dtRow.Item("assetdeclarationt2unkid"))
                mstrBankName = dtRow.Item("bank_name").ToString
                mstrAccountype = dtRow.Item("account_type").ToString
                mstrAccountNo = dtRow.Item("account_no").ToString
                mdecAmount = CDec(dtRow.Item("amount"))
                mdecBaseamount = CDec(dtRow.Item("baseamount"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mintCurrencyUnkId = CInt(dtRow.Item("currencyunkid"))
                mintBasecurrencyUnkid = CInt(dtRow.Item("basecurrencyunkid"))
                mdecBaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                mdecExchangerate = CDec(dtRow.Item("exchangerate"))
                mstrDepositssource = dtRow.Item("deposits_source").ToString
                mblnIsfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    mdtTransactiondate = Nothing
                Else
                    mdtTransactiondate = dtRow.Item("transactiondate")
                End If
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintAccountTypeUnkid = CInt(dtRow.Item("accounttypeunkid"))
                mstrSpecify = dtRow.Item("specify").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDataByUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub
    'Sohail (07 Dec 2018) -- End

    Public Function InserByDataTable(ByRef blnChildTableChanged As Boolean, ByVal dtOld As DataTable, ByRef decBankTotal As Decimal, ByVal xCurrentDatetTime As DateTime, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim strUnkIDs As String = ""
        Dim decTotal As Decimal = 0

        Try
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
            objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()
            'Hemant (14 Nov 2018) -- End

            For Each dtRow As DataRow In mdtTable.Rows

                mintAssetbanktrant2unkid = CInt(dtRow.Item("assetbankt2tranunkid"))
                mstrBankName = dtRow.Item("bank_name").ToString
                mstrAccountype = dtRow.Item("account_type").ToString
                mstrAccountNo = dtRow.Item("account_no").ToString
                mdecAmount = CDec(dtRow.Item("amount"))
                mdecBaseamount = CDec(dtRow.Item("baseamount"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mintCurrencyUnkId = CInt(dtRow.Item("currencyunkid"))
                mintBasecurrencyUnkid = CInt(dtRow.Item("basecurrencyunkid"))
                mdecBaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                mdecExchangerate = CDec(dtRow.Item("exchangerate"))
                mstrDepositssource = dtRow.Item("deposits_source").ToString
                mblnIsfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                mdtTransactiondate = dtRow.Item("transactiondate")
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtVoiddatetime = Nothing
                mstrVoidreason = dtRow.Item("voidreason").ToString

                'Hemant (19 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                mintAccountTypeUnkid = dtRow.Item("accounttypeunkid").ToString
                'Hemant (19 Nov 2018) -- End
                'Sohail (30 Nov 2018) -- Start
                'NMB Enhancement - On employee declaration, bank account and dependant bank fields there should be a free text box named "specify" for user to input other account names in 75.1.
                mstrSpecify = dtRow.Item("specify").ToString
                'Sohail (30 Nov 2018) -- End

                If mintAssetbanktrant2unkid <= 0 Then

                    blnChildTableChanged = True

                    'Hemant (14 Nov 2018) -- Start
                    'Enhancement : Changes for Bind Transactions
                    'If Insert() = False Then
                    If Insert(objDataOperation) = False Then
                        'Hemant (14 Nov 2018) -- End
                        Return False
                    End If
                Else
                    If strUnkIDs.Trim = "" Then
                        strUnkIDs = mintAssetbanktrant2unkid.ToString
                    Else
                        strUnkIDs &= "," & mintAssetbanktrant2unkid.ToString
                    End If
                    'Hemant (14 Nov 2018) -- Start
                    'Enhancement : Changes for Bind Transactions
                    'If dtRow.Item("AUD").ToString() = "U" AndAlso Update(blnChildTableChanged) = False Then
                    If dtRow.Item("AUD").ToString() = "U" AndAlso Update(blnChildTableChanged, objDataOperation) = False Then
                        'Hemant (14 Nov 2018) -- End
                        Return False
                    End If
                End If

            Next

            decBankTotal = decTotal

            If dtOld IsNot Nothing AndAlso dtOld.Rows.Count > 0 Then
                Dim dRow() As DataRow
                If strUnkIDs.Trim <> "" Then
                    dRow = dtOld.Select("assetbankt2tranunkid NOT IN (" & strUnkIDs & ") ")
                Else
                    dRow = dtOld.Select()
                End If

                For Each dtRow In dRow

                    mintAssetbanktrant2unkid = CInt(dtRow.Item("assetbankt2tranunkid"))
                    mstrBankName = dtRow.Item("bank_name").ToString
                    mstrAccountype = dtRow.Item("account_type").ToString
                    mstrAccountNo = dtRow.Item("account_no").ToString
                    mdecAmount = CDec(dtRow.Item("amount"))
                    mdecBaseamount = CDec(dtRow.Item("baseamount"))
                    mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                    mintCurrencyUnkId = CInt(dtRow.Item("currencyunkid"))
                    mintBasecurrencyUnkid = CInt(dtRow.Item("basecurrencyunkid"))
                    mdecBaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                    mdecExchangerate = CDec(dtRow.Item("exchangerate"))
                    mstrDepositssource = dtRow.Item("deposits_source").ToString
                    mblnIsfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                    mdtTransactiondate = dtRow.Item("transactiondate")
                    mblnIsvoid = CBool(dtRow.Item("isvoid"))
                    mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                    mdtVoiddatetime = Nothing
                    mstrVoidreason = dtRow.Item("voidreason").ToString

                    'Hemant (19 Nov 2018) -- Start
                    'Enhancement : Changes for NMB Requirement
                    mintAccountTypeUnkid = dtRow.Item("accounttypeunkid").ToString
                    'Hemant (19 Nov 2018) -- End
                    'Sohail (30 Nov 2018) -- Start
                    'NMB Enhancement - On employee declaration, bank account and dependant bank fields there should be a free text box named "specify" for user to input other account names in 75.1.
                    mstrSpecify = dtRow.Item("specify").ToString
                    'Sohail (30 Nov 2018) -- End

                    'Hemant (14 Nov 2018) -- Start
                    'Enhancement : Changes for Bind Transactions
                    'If Void(CInt(dtRow.Item("assetbankt2tranunkid")), mintUserunkid, xCurrentDatetTime, "") = False Then
                    If Void(CInt(dtRow.Item("assetbankt2tranunkid")), mintUserunkid, xCurrentDatetTime, "", objDataOperation) = False Then
                        'Hemant (14 Nov 2018) -- End
                        Return False
                    End If
                Next
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InserByDataTable; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function

    Public Function GetList(ByVal strTableName As String, Optional ByVal intAssetDeclarationt2UnkID As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            Dim objExRate As New clsExchangeRate
            Dim dsExRate As DataSet = objExRate.GetList("Currency", True, False, 0, 0, False, Nothing, True)
            Dim dicExRate As Dictionary(Of Integer, String) = (From p In dsExRate.Tables(0) Select New With {.Id = CInt(p.Item("countryunkid")), .Name = p.Item("currency_sign").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Name)
            'Sohail (07 Dec 2018) -- End

            strQ = "SELECT " & _
              "  hrasset_bankT2_tran.assetbankt2tranunkid " & _
              ", hrasset_bankT2_tran.assetdeclarationt2unkid " & _
              ", hrasset_bankT2_tran.bank_name " & _
              ", hrasset_bankT2_tran.account_type " & _
              ", hrasset_bankT2_tran.account_no " & _
              ", ISNULL(hrasset_bankT2_tran.amount, 0) AS amount " & _
              ", ISNULL(hrasset_bankT2_tran.baseamount, 0) AS baseamount " & _
              ", ISNULL(hrasset_bankT2_tran.countryunkid, 0) AS countryunkid " & _
              ", ISNULL(hrasset_bankT2_tran.currencyunkid, 0) AS currencyunkid " & _
              ", ISNULL(hrasset_bankT2_tran.basecurrencyunkid, 0) AS basecurrencyunkid " & _
              ", ISNULL(hrasset_bankT2_tran.baseexchangerate, 0) AS baseexchangerate " & _
              ", ISNULL(hrasset_bankT2_tran.exchangerate, 0) AS exchangerate " & _
              ", hrasset_bankT2_tran.deposits_source " & _
              ", hrasset_bankT2_tran.isfinalsaved " & _
              ", hrasset_bankT2_tran.transactiondate " & _
              ", hrasset_bankT2_tran.userunkid " & _
              ", hrasset_bankT2_tran.isvoid " & _
              ", hrasset_bankT2_tran.voiduserunkid " & _
              ", hrasset_bankT2_tran.voiddatetime " & _
              ", hrasset_bankT2_tran.voidreason " & _
              ", ISNULL(hrasset_bankT2_tran.accounttypeunkid, 0) AS accounttypeunkid " & _
              ", ISNULL(cfbankacctype_master.accounttype_name, '') AS accounttype_name " & _
              ", ISNULL(hrasset_bankT2_tran.specify, '') AS specify "

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            strQ &= ", CASE hrasset_bankT2_tran.countryunkid "
            For Each pair In dicExRate
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS currency_sign "
            'Sohail (07 Dec 2018) -- End

            strQ &= "FROM hrasset_bankT2_tran " & _
            "LEFT JOIN hrmsConfiguration..cfbankacctype_master ON cfbankacctype_master.accounttypeunkid = hrasset_bankT2_tran.accounttypeunkid " & _
             "WHERE ISNULL(hrasset_bankT2_tran.isvoid, 0 ) = 0 " & _
             "AND cfbankacctype_master.isactive = 1 "
            'Sohail (07 Dec 2018) - [LEFT JOIN hrmsConfiguration..cfbankacctype_master, isactive]
            'Sohail (30 Nov 2018) - [specify]
            'Hemant (19 Nov 2018) -- [accounttypeunkid]

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'If intAssetDeclarationt2UnkID > 0 Then
            '    strQ &= " AND assetdeclarationt2unkid = @assetdeclarationt2unkid "
            '    objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            'End If
            If intAssetDeclarationt2UnkID > 0 Then
                strQ &= " AND hrasset_bankT2_tran.assetdeclarationt2unkid = @assetdeclarationt2unkid "
                objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            Else
                strQ &= " AND 1 = 2 "
            End If
            'Sohail (07 Dec 2018) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrasset_bankT2_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal objAD_MasterT2 As clsAssetdeclaration_masterT2 = Nothing) As Boolean
        'Gajanan (07 Dec 2018) - [objAD_MasterT2]
        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Bind Transactions
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Gajanan (07 Dec 2018) -- Start
            objDataOperation.BindTransaction()
            'Gajanan (07 Dec 2018) -- End
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End

        Try
            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If mintAssetdeclarationt2unkid <= 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If objAD_MasterT2.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintAssetdeclarationt2unkid = intNewUnkId
                End If
            ElseIf mintAssetdeclarationt2unkid > 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                If objAD_MasterT2.Update(enAction.EDIT_ONE, objDataOperation) = False Then
                    Return False
                End If
            End If

            objDataOperation.ClearParameters()
            'Gajanan (07 Dec 2018) -- End



            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)
            objDataOperation.AddParameter("@bank_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBankName.ToString)
            objDataOperation.AddParameter("@account_type", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountype.ToString)
            objDataOperation.AddParameter("@account_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountNo.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@baseamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseamount.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyUnkId.ToString)
            objDataOperation.AddParameter("@basecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasecurrencyUnkid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
            objDataOperation.AddParameter("@exchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchangerate.ToString)
            objDataOperation.AddParameter("@deposits_source", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDepositssource.ToString)
            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Hemant (19 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountTypeUnkid.ToString)
            'Hemant (19 Nov 2018) -- End
            'Sohail (30 Nov 2018) -- Start
            'NMB Enhancement - On employee declaration, bank account and dependant bank fields there should be a free text box named "specify" for user to input other account names in 75.1.
            objDataOperation.AddParameter("@specify", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSpecify.ToString)
            'Sohail (30 Nov 2018) -- End

            strQ = "INSERT INTO hrasset_bankT2_tran ( " & _
              "  assetdeclarationt2unkid " & _
              ", bank_name " & _
              ", account_type " & _
              ", account_no " & _
              ", amount " & _
              ", baseamount " & _
              ", countryunkid " & _
              ", currencyunkid " & _
              ", basecurrencyunkid " & _
              ", baseexchangerate " & _
              ", exchangerate " & _
              ", deposits_source " & _
              ", isfinalsaved " & _
              ", transactiondate" & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", accounttypeunkid" & _
              ", specify" & _
            ") VALUES (" & _
              "  @assetdeclarationt2unkid " & _
              ", @bank_name " & _
              ", @account_type " & _
              ", @account_no " & _
              ", @amount " & _
              ", @baseamount " & _
              ", @countryunkid " & _
              ", @currencyunkid " & _
              ", @basecurrencyunkid " & _
              ", @baseexchangerate " & _
              ", @exchangerate " & _
              ", @deposits_source " & _
              ", @isfinalsaved " & _
              ", @transactiondate" & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @accounttypeunkid" & _
              ", @specify" & _
            "); SELECT @@identity"
            'Sohail (30 Nov 2018) - [specify]
            'Hemant (19 Nov 2018) -- [accounttypeunkid]
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAssetbanktrant2unkid = dsList.Tables(0).Rows(0).Item(0)

            'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", mintAssetdeclarationt2unkid, "hrasset_bankT2_tran", "assetbankt2tranunkid", mintAssetbanktrant2unkid, 1, 1, , mintUserunkid) = False Then
            '    Return False
            'End If

            If Insert_AtTranLog(objDataOperation, 1) = False Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Sohail (07 Dec 2018) -- End
                Return False
            End If
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrasset_bankT2_tran) </purpose>
    Public Function Update(ByRef blnChildTableChanged As Boolean, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal objAD_MasterT2 As clsAssetdeclaration_masterT2 = Nothing) As Boolean
        'Sohail (07 Dec 2018) - [objAD_MasterT2]
        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Bind Transactions
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Gajanan (07 Dec 2018) -- Start
            objDataOperation.BindTransaction()
            'Gajanan (07 Dec 2018) -- End            
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End

        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If mintAssetdeclarationt2unkid <= 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If objAD_MasterT2.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintAssetdeclarationt2unkid = intNewUnkId
                End If
            ElseIf mintAssetdeclarationt2unkid > 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                If objAD_MasterT2.Update(enAction.EDIT_ONE, objDataOperation) = False Then
                    Return False
                End If
            End If
            objDataOperation.ClearParameters()
            'Sohail (07 Dec 2018) -- End
            objDataOperation.AddParameter("@assetbankt2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetbanktrant2unkid.ToString)
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)
            objDataOperation.AddParameter("@bank_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBankName.ToString)
            objDataOperation.AddParameter("@account_type", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountype.ToString)
            objDataOperation.AddParameter("@account_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountNo.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@baseamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseamount.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyUnkId.ToString) 'Sohail (06 Apr 2012)
            objDataOperation.AddParameter("@basecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasecurrencyUnkid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
            objDataOperation.AddParameter("@exchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchangerate.ToString)
            objDataOperation.AddParameter("@deposits_source", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDepositssource.ToString)
            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Hemant (19 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountTypeUnkid.ToString)
            'Hemant (19 Nov 2018) -- End
            'Sohail (30 Nov 2018) -- Start
            'NMB Enhancement - On employee declaration, bank account and dependant bank fields there should be a free text box named "specify" for user to input other account names in 75.1.
            objDataOperation.AddParameter("@specify", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSpecify.ToString)
            'Sohail (30 Nov 2018) -- End

            strQ = "UPDATE hrasset_bankT2_tran SET " & _
              "  assetdeclarationt2unkid = @assetdeclarationt2unkid" & _
              ", bank_name = @bank_name" & _
              ", account_type = @account_type" & _
              ", account_no = @account_no" & _
              ", amount = @amount" & _
              ", baseamount = @baseamount" & _
              ", countryunkid = @countryunkid" & _
              ", currencyunkid = @currencyunkid " & _
              ", basecurrencyunkid = @basecurrencyunkid" & _
              ", baseexchangerate = @baseexchangerate" & _
              ", exchangerate = @exchangerate" & _
              ", deposits_source = @deposits_source" & _
              ", isfinalsaved = @isfinalsaved " & _
              ", transactiondate = @transactiondate " & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", accounttypeunkid = @accounttypeunkid " & _
              ", specify = @specify " & _
            "WHERE assetbankt2tranunkid = @assetbankt2tranunkid "
            'Sohail (30 Nov 2018) - [specify]
            'Hemant (19 Nov 2018) -- [accounttypeunkid]
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", mintAssetdeclarationt2unkid, "hrasset_bankT2_tran", "assetbankt2tranunkid", mintAssetbanktrant2unkid, 2, 2, , mintUserunkid) = False Then
            '    Return False
            'End If
            If IsTableDataUpdate(mintAssetbanktrant2unkid, objDataOperation) = False Then
            If Insert_AtTranLog(objDataOperation, 2) = False Then
                    'Sohail (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    'Sohail (07 Dec 2018) -- End
                Return False
            End If
            End If

            blnChildTableChanged = True
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrasset_bankT2_tran) </purpose>
    Public Function Void(ByVal intUnkid As Integer, _
                         ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Bind Transactions
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Gajanan (07 Dec 2018) -- Start
            objDataOperation.BindTransaction()
            'Gajanan (07 Dec 2018) -- End          
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End

        Try

            'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", mintAssetdeclarationt2unkid, "hrasset_bankT2_tran", "assetbankt2tranunkid", intUnkid, 2, 3, False, ) = False Then
            '    Return False
            'End If

            strQ = "UPDATE hrasset_bankT2_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE assetbankt2tranunkid = @assetbankt2tranunkid "

            objDataOperation.AddParameter("@assetbankt2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If Insert_AtTranLog(objDataOperation, 3) = False Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Sohail (07 Dec 2018) -- End
                Return False
            End If
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function

    Public Function VoidByAssetDeclarationt2UnkID(ByVal intAssetDeclarationt2UnkID As Integer, _
                                        ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, ByVal intParentAuditType As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Bind Transactions
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End


        Try

            'If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", intAssetDeclarationt2UnkID, "hrasset_bankT2_tran", "assetbankt2tranunkid", intParentAuditType, 3, , " ISNULL(isvoid, 0) = 0 ", , intVoidUserID) = False Then

            '    Return False
            'End If
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            strQ = "INSERT INTO athrasset_bankT2_tran ( " & _
                     "  assetbankt2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", bank_name " & _
              ", account_type " & _
              ", account_no " & _
              ", amount " & _
              ", baseamount " & _
              ", countryunkid " & _
              ", currencyunkid " & _
              ", basecurrencyunkid " & _
              ", baseexchangerate " & _
              ", exchangerate " & _
              ", deposits_source " & _
              ", isfinalsaved " & _
              ", transactiondate" & _
              ", auditdatetime " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", loginemployeeunkid " & _
              ", ip " & _
              ", host " & _
              ", form_name " & _
              ", isweb " & _
              ", accounttypeunkid " & _
              ", specify " & _
              " )" & _
             "SELECT " & _
                    "  assetbankt2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", bank_name " & _
              ", account_type " & _
              ", account_no " & _
              ", amount " & _
              ", baseamount " & _
              ", countryunkid " & _
              ", currencyunkid " & _
              ", basecurrencyunkid " & _
              ", baseexchangerate " & _
              ", exchangerate " & _
              ", deposits_source " & _
              ", isfinalsaved " & _
              ", transactiondate" & _
              ", GETDATE() " & _
              ", 3 " & _
              ", @audituserunkid " & _
              ", @loginemployeeunkid " & _
              ", @ip " & _
              ", @host " & _
              ", @form_name " & _
              ", @isweb " & _
              ", accounttypeunkid " & _
              ", specify " & _
             "FROM hrasset_bankT2_tran " & _
              "WHERE isvoid = 0 " & _
               "AND assetdeclarationt2unkid = @assetdeclarationt2unkid "
            'Sohail (30 Nov 2018) - [specify, @accounttypeunkid=accounttypeunkid]
            'Hemant (19 Nov 2018) -- [accounttypeunkid]
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)

            Call objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ClearParameters()
            'Hemant (14 Nov 2018) -- End
            strQ = "UPDATE hrasset_bankT2_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE assetdeclarationt2unkid = @assetdeclarationt2unkid "

            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'If Insert_AtTranLog(objDataOperation, 3) = False Then
            '    Return False
            'End If
            'Hemant (14 Nov 2018) -- End

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidByAssetDeclarationt2UnkID; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Bind Transactions
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function
    
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@assetbankt2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  assetbankt2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", bank_name " & _
              ", account_type " & _
              ", account_no " & _
              ", amount " & _
              ", baseamount " & _
              ", isfinalsaved " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(currencyunkid, 0) AS currencyunkid " & _
              ", ISNULL(accounttypeunkid, 0) AS accounttypeunkid " & _
              ", ISNULL(specify, '') AS specify " & _
             "FROM hrasset_bankT2_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "
            'Sohail (30 Nov 2018) - [specify]

            If intUnkid > 0 Then
                strQ &= " AND assetbankt2tranunkid <> @assetbankt2tranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@assetbankt2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Insert_AtTranLog(ByVal objDoOps As clsDataOperation, ByVal AuditType As Integer)
        Dim StrQ As String = String.Empty
        Dim extForce As Exception
        Dim dsList As New DataSet

        Try
            objDoOps.ClearParameters()
            objDoOps.AddParameter("@assetbankt2tranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetbanktrant2unkid.ToString)
            objDoOps.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid.ToString)
            objDoOps.AddParameter("@bank_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBankName.ToString)
            objDoOps.AddParameter("@account_type", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountype.ToString)
            objDoOps.AddParameter("@account_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountNo.ToString)
            objDoOps.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDoOps.AddParameter("@baseamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseamount.ToString)
            objDoOps.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDoOps.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrencyUnkId.ToString) 'Sohail (06 Apr 2012)
            objDoOps.AddParameter("@basecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBasecurrencyUnkid.ToString)
            objDoOps.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBaseexchangerate.ToString)
            objDoOps.AddParameter("@exchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExchangerate.ToString)
            objDoOps.AddParameter("@deposits_source", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDepositssource.ToString)
            objDoOps.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            If mdtTransactiondate = Nothing Then
                objDoOps.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDoOps.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            objDoOps.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDoOps.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minAuditUserid)
            objDoOps.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid)
            objDoOps.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDoOps.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDoOps.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDoOps.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)

            'Hemant (19 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            objDoOps.AddParameter("@accounttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountTypeUnkid.ToString)
            'Hemant (19 Nov 2018) -- End
            'Sohail (30 Nov 2018) -- Start
            'NMB Enhancement - On employee declaration, bank account and dependant bank fields there should be a free text box named "specify" for user to input other account names in 75.1.
            objDoOps.AddParameter("@specify", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrSpecify)
            'Sohail (30 Nov 2018) -- End

            StrQ = "INSERT INTO athrasset_bankT2_tran ( " & _
              "  assetbankt2tranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", bank_name " & _
              ", account_type " & _
              ", account_no " & _
              ", amount " & _
              ", baseamount " & _
              ", countryunkid " & _
              ", currencyunkid " & _
              ", basecurrencyunkid " & _
              ", baseexchangerate " & _
              ", exchangerate " & _
              ", deposits_source " & _
              ", isfinalsaved " & _
              ", transactiondate" & _
              ", auditdatetime " & _
                     ", audittype " & _
                     ", audituserunkid " & _
                     ", loginemployeeunkid " & _
                     ", ip " & _
                     ", host " & _
                     ", form_name " & _
                     ", isweb " & _
              ", accounttypeunkid " & _
              ", specify " & _
            ") VALUES (" & _
              "  @assetbankt2tranunkid " & _
              ", @assetdeclarationt2unkid " & _
              ", @bank_name " & _
              ", @account_type " & _
              ", @account_no " & _
              ", @amount " & _
              ", @baseamount " & _
              ", @countryunkid " & _
              ", @currencyunkid " & _
              ", @basecurrencyunkid " & _
              ", @baseexchangerate " & _
              ", @exchangerate " & _
              ", @deposits_source " & _
              ", @isfinalsaved " & _
              ", @transactiondate" & _
              ", GETDATE() " & _
                     ", @audittype " & _
                     ", @audituserunkid " & _
                     ", @loginemployeeunkid " & _
                     ", @ip " & _
                     ", @host " & _
                     ", @form_name " & _
              ", @isweb " & _
            ", @accounttypeunkid  " & _
            ", @specify " & _
            "  ) "
            'Sohail (30 Nov 2018) - [specify]
            'Hemant (19 Nov 2018) -- [accounttypeunkid]
            dsList = objDoOps.ExecQuery(StrQ, "List")

            If objDoOps.ErrorMessage <> "" Then
                extForce = New Exception(objDoOps.ErrorNumber & ": " & objDoOps.ErrorMessage)
                Throw extForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
        End Try
        Return True

    End Function

    Public Function IsTableDataUpdate(ByVal unkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'strQ = "select TOP 1 * from athrasset_bankT2_tran where assetbankt2tranunkid = @assetbankt2tranunkid and assetdeclarationt2unkid = @assetdeclarationt2unkid and audittype =2 ORDER BY auditdatetime DESC"
            strQ = "select TOP 1 * from athrasset_bankT2_tran where assetbankt2tranunkid = @assetbankt2tranunkid and assetdeclarationt2unkid = @assetdeclarationt2unkid and audittype <> 3 ORDER BY auditdatetime DESC"
            'Gajanan (07 Dec 2018) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetbankt2tranunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, unkid)
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dr As DataRow In dsList.Tables(0).Rows
                'Hemant (19 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                If dr("accounttypeunkid") Is DBNull.Value Then
                    Return False
                    'Hemant (19 Nov 2018) -- End
                    'Sohail (30 Nov 2018) -- Start
                    'NMB Enhancement - On employee declaration, bank account and dependant bank fields there should be a free text box named "specify" for user to input other account names in 75.1.
                ElseIf IsDBNull(dr("specify")) = True Then
                    Return False
                    'Sohail (30 Nov 2018) -- End
                ElseIf dr("bank_name").ToString() = mstrBankName AndAlso dr("account_type").ToString() = mstrAccountype AndAlso _
                   dr("account_no").ToString() = mstrAccountNo AndAlso dr("deposits_source").ToString() = mstrDepositssource _
                        AndAlso dr("countryunkid").ToString() = mintCountryunkid AndAlso dr("accounttypeunkid").ToString() = mintAccountTypeUnkid AndAlso dr("specify").ToString() = mstrSpecify Then
                    'Sohail (30 Nov 2018) - [specify]
                    'Hemant (19 Nov 2018) -- [accounttypeunkid] 
                    Return True
                Else
                    Return False
                End If
            Next
            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

End Class