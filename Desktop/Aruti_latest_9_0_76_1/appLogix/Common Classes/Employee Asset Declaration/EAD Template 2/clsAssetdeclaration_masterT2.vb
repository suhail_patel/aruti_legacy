﻿'************************************************************************************************************************************
'Class Name : clsAssetdeclaration_masterT2.vb
'Purpose    :
'Date       :06/10/2019
'Written By :Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Text
Imports System
Imports System.Web

''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clsAssetdeclaration_masterT2
    Private Const mstrModuleName As String = "clsAssetdeclaration_masterT2"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAssetdeclaration2unkid As Integer
    Private mintEmployeeunkid As Integer
    Private mstrTin_No As String
 
    Private mdecCashExistingBusiness As Decimal
    Private mdtFinyear_Start As Date
    Private mdtFinyear_End As Date
    Private mblnIsfinalsaved As Boolean
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdtTransactiondate As Date 
    Private mdtSavedate As Date
    Private mdtFinalsavedate As Date
    Private mdtUnlockfinalsavedate As Date
    Private mdtBusinessDealings As DataTable = Nothing
    Private mdtStaffOwnerShip As DataTable = Nothing
    Private mdtBankAccounts As DataTable = Nothing
    Private mdtRealProperties As DataTable = Nothing
    Private mdtLiabilities As DataTable = Nothing
    Private mdtRelatives As DataTable = Nothing
    'Private mintLoginemployeeunkid As Integer = -1
    'Gajanan (06 Oct 2018) -- Start
    'Enhancement : Implementing New Module of Asset Declaration Template 2
    Private mdtDependantBusiness As DataTable = Nothing
    Private mdtDependantShare As DataTable = Nothing
    Private mdtDependantBank As DataTable = Nothing
    Private mdtDependantProperties As DataTable = Nothing
    'Gajanan(06 Oct 2018) -- End


    Private mintVoidloginemployeeunkid As Integer = -1
    Private mstrDatabaseName As String = ""

    Private minAuditUserid As Integer = 0
    Private minAuditDate As DateTime
    Private minloginemployeeunkid As Integer = 0
    Private minClientIp As String = ""
    Private mstrHostName As String = ""
    Private mstrFormName As String = String.Empty
    Private blnIsFromWeb As Boolean = False

    'Hemant (05 Dec 2018) -- Start
    'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
    Private mintFinancialYearunkid As Integer
    'Hemant (05 Dec 2018) -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetdeclarationt2unkid
    ''' Modify By: Hemant
    ''' </summary>

    Public Property _Assetdeclaration2unkid(ByVal xDatabaseName As String) As Integer
        Get
            Return mintAssetdeclaration2unkid
        End Get
        Set(ByVal value As Integer)
            mintAssetdeclaration2unkid = value
            Call GetData(xDatabaseName)

        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Tin_No
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Tin_No() As String
        Get
            Return mstrTin_No
        End Get
        Set(ByVal value As String)
            mstrTin_No = value
        End Set
    End Property


    'Hemant (05 Dec 2018) -- Start
    'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
    Public Property _FinancialYearunkid() As Integer
        Get
            Return mintFinancialYearunkid
        End Get
        Set(ByVal value As Integer)
            mintFinancialYearunkid = value
        End Set
    End Property
    'Hemant (05 Dec 2018) -- End



    Public Property _Finyear_Start() As Date
        Get
            Return mdtFinyear_Start
        End Get
        Set(ByVal value As Date)
            mdtFinyear_Start = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set finyear_end
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Finyear_End() As Date
        Get
            Return mdtFinyear_End
        End Get
        Set(ByVal value As Date)
            mdtFinyear_End = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinalsaved
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isfinalsaved() As Boolean
        Get
            Return mblnIsfinalsaved
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinalsaved = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TransactionDate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property


    Public Property _VoidLoginEmployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set savedate
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Savedate() As Date
        Get
            Return mdtSavedate
        End Get
        Set(ByVal value As Date)
            mdtSavedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set finalsavedate
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Finalsavedate() As Date
        Get
            Return mdtFinalsavedate
        End Get
        Set(ByVal value As Date)
            mdtFinalsavedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unlockfinalsavedate
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Unlockfinalsavedate() As Date
        Get
            Return mdtUnlockfinalsavedate
        End Get
        Set(ByVal value As Date)
            mdtUnlockfinalsavedate = value
        End Set
    End Property


    Public Property _Datasource_Business() As DataTable
        Get
            Return mdtBusinessDealings
        End Get
        Set(ByVal value As DataTable)
            mdtBusinessDealings = value
        End Set
    End Property

    Public WriteOnly Property _Datasource_StaffOwnerShip() As DataTable
        Set(ByVal value As DataTable)
            mdtStaffOwnerShip = value
        End Set
    End Property

    Public WriteOnly Property _Datasource_BankAccounts() As DataTable
        Set(ByVal value As DataTable)
            mdtBankAccounts = value
        End Set
    End Property

    Public WriteOnly Property _Datasource_RealProperties() As DataTable
        Set(ByVal value As DataTable)
            mdtRealProperties = value
        End Set
    End Property

    Public WriteOnly Property _Datasource_Liabilities() As DataTable
        Set(ByVal value As DataTable)
            mdtLiabilities = value
        End Set
    End Property

    Public WriteOnly Property _Datasource_Relatives() As DataTable
        Set(ByVal value As DataTable)
            mdtRelatives = value
        End Set
    End Property

    'Gajanan (06 Oct 2018) -- Start
    'Enhancement : Implementing New Module of Asset Declaration Template 2
    Public WriteOnly Property _Datasource_DependantBusiness() As DataTable
        Set(ByVal value As DataTable)
            mdtDependantBusiness = value
        End Set
    End Property

    Public WriteOnly Property _Datasource_DependantShare() As DataTable
        Set(ByVal value As DataTable)
            mdtDependantShare = value
        End Set
    End Property

    Public WriteOnly Property _Datasource_DependantBank() As DataTable
        Set(ByVal value As DataTable)
            mdtDependantBank = value
        End Set
    End Property

    Public WriteOnly Property _Datasource_DependantProperties() As DataTable
        Set(ByVal value As DataTable)
            mdtDependantProperties = value
        End Set
    End Property
    'Gajanan(06 Oct 2018) -- End

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    Public Property _AuditUserid() As Integer
        Get
            Return minAuditUserid
        End Get
        Set(ByVal value As Integer)
            minAuditUserid = value
        End Set
    End Property

    Public Property _AuditDate() As DateTime
        Get
            Return minAuditDate
        End Get
        Set(ByVal value As DateTime)
            minAuditDate = value
        End Set
    End Property

    Public Property _ClientIp() As String
        Get
            Return minClientIp
        End Get
        Set(ByVal value As String)
            minClientIp = value
        End Set
    End Property

    Public Property _Loginemployeeunkid() As Integer
        Get
            Return minloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            minloginemployeeunkid = value
        End Set
    End Property

    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public Property _IsFromWeb() As Boolean
        Get
            Return blnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            blnIsFromWeb = value
        End Set
    End Property

    'Hemant (15 Nov 2018) -- Start
    'Enhancement : Changes for NMB Requirement
    ''' <summary>
    ''' Purpose: Get or Set isfinalsaved
    ''' Modify By: Hemant
    ''' </summary>
    Private mblnIsAcknowledged As Boolean
    Public Property _IsAcknowledged() As Boolean
        Get
            Return mblnIsAcknowledged
        End Get
        Set(ByVal value As Boolean)
            mblnIsAcknowledged = value
        End Set
    End Property
    'Hemant (15 Nov 2018) -- End

    'Hemant (02 Feb 2022) -- Start            
    'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
    Private lstWebEmail As List(Of clsEmailCollection) = Nothing
    Public ReadOnly Property _WebEmailList() As List(Of clsEmailCollection)
        Get
            Return lstWebEmail
        End Get
    End Property
    'Hemant (02 Feb 2022) -- End

#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(ByVal xDatabaseName As String)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        mstrDatabaseName = xDatabaseName

        Try
            strQ = "SELECT " & _
              "  assetdeclarationt2unkid " & _
              ", employeeunkid " & _
              ", Tin_No " & _
              ", finyear_start " & _
              ", finyear_end " & _
              ", isfinalsaved " & _
              ", ISNULL(transactiondate, GETDATE()) AS transactiondate " & _
              ", savedate " & _
              ", finalsavedate " & _
              ", unlockfinalsavedate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(isacknowledged, 0 ) AS isacknowledged " & _
             "FROM " & mstrDatabaseName & "..hrassetdeclarationT2_master " & _
             "WHERE assetdeclarationt2unkid = @assetdeclarationt2unkid "

            'Hemant (15 Nov 2018) -- [isacknowledged]
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclaration2unkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAssetdeclaration2unkid = CInt(dtRow.Item("assetdeclarationt2unkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mstrTin_No = dtRow.Item("Tin_No").ToString
                mdtFinyear_Start = dtRow.Item("finyear_start")
                mdtFinyear_End = dtRow.Item("finyear_end")
                mblnIsfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mdtTransactiondate = dtRow.Item("transactiondate")

                If IsDBNull(dtRow.Item("savedate")) = True Then
                    mdtSavedate = Nothing
                Else
                    mdtSavedate = dtRow.Item("savedate")
                End If
                If IsDBNull(dtRow.Item("finalsavedate")) = True Then
                    mdtFinalsavedate = Nothing
                Else
                    mdtFinalsavedate = dtRow.Item("finalsavedate")
                End If
                If IsDBNull(dtRow.Item("unlockfinalsavedate")) = True Then
                    mdtUnlockfinalsavedate = Nothing
                Else
                    mdtUnlockfinalsavedate = dtRow.Item("unlockfinalsavedate")
                End If

                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                mblnIsAcknowledged = CBool(dtRow.Item("isacknowledged"))
                'Hemant (15 Nov 2018) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    Public Shared Function GetList(ByVal xUserUnkid As Integer, _
                                   ByVal xCompanyUnkid As Integer, _
                                   ByVal xUserModeSetting As String, _
                                   ByVal xOnlyApproved As Boolean, _
                                   ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                   ByVal strTableName As String, _
                                   ByVal dicDatabaseNames As Dictionary(Of String, String), _
                                   ByVal dicDatabaseEndDate As Dictionary(Of String, String), _
                                   ByVal dicDatabasYearUnkId As Dictionary(Of String, Integer), _
                                   Optional ByVal intAssetDeclaration2UnkID As Integer = 0, _
                                   Optional ByVal intEmployeeUnkID As Integer = 0, _
                                   Optional ByVal blnNotIncludeFinalSaved As Boolean = False, _
                                   Optional ByVal strEmployeeAsOnDate As String = "", _
                                   Optional ByVal strUserAccessLevelFilterString As String = "", _
                                   Optional ByVal strCarRegno As String = "", _
                                   Optional ByVal strFilter As String = "", _
                                   Optional ByVal strOrderByFields As String = "", _
                                   Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                                   Optional ByVal xFinStartDate As DateTime = Nothing, _
                                   Optional ByVal xFinEndDate As DateTime = Nothing, _
                                   Optional ByVal blnIncludeClosedYear As Boolean = False _
                                   ) As DataSet
        'Hemant (11 Feb 2019) -- [blnIncludeClosedYear]
        'Hemant (05 Dec 2018) -- [xFinStartDate,xFinEndDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim strInnerQ As String = ""

        objDataOperation = New clsDataOperation

        Try
            If dicDatabaseNames Is Nothing OrElse dicDatabaseNames.Count <= 0 Then
                dicDatabaseNames = New Dictionary(Of String, String)
                dicDatabaseNames.Add(FinancialYear._Object._DatabaseName, FinancialYear._Object._FinancialYear_Name)
            End If

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""

            strQ = "SELECT  A.assetdeclarationt2unkid " & _
                          ", A.employeeunkid " & _
                          ", A.employeename " & _
                          ", A.noofemployment " & _
                          ", A.position " & _
                          ", A.centerforwork " & _
                          ", A.workstation " & _
                          ", A.finyear_start " & _
                          ", A.Tin_No " & _
                          ", A.finyear_end " & _
                          ", A.isfinalsaved " & _
                          ", A.userunkid " & _
                          ", A.isvoid " & _
                          ", A.voiduserunkid " & _
                          ", A.voiddatetime " & _
                          ", A.voidreason " & _
                          ", A.transactiondate " & _
                          ", A.savedate " & _
                          ", A.finalsavedate " & _
                          ", A.unlockfinalsavedate " & _
                          ", A.isacknowledged " & _
                          ", A.databasename " & _
                          ", A.yearname " & _
                    "FROM    (  "


            For Each key As KeyValuePair(Of String, String) In dicDatabaseNames
                Dim intYearId As Integer = 0
                Dim dtPeriodStart As Date = Nothing
                Dim dtPeriodEnd As Date = Nothing



                If dicDatabaseEndDate.ContainsKey(key.Key) AndAlso dicDatabasYearUnkId.ContainsKey(key.Key) Then
                    intYearId = dicDatabasYearUnkId(key.Key)
                    dtPeriodStart = eZeeDate.convertDate(dicDatabaseEndDate(key.Key).ToString().Split("|")(0))
                    dtPeriodEnd = eZeeDate.convertDate(dicDatabaseEndDate(key.Key).ToString().Split("|")(1))
                End If

                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodStart, dtPeriodEnd, , , key.Key)
                If blnApplyUserAccessFilter = True Then
                    Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, xOnlyApproved, key.Key, xUserUnkid, xCompanyUnkid, intYearId, xUserModeSetting)
                End If

                strInnerQ &= "UNION ALL " & _
                                  "SELECT  DISTINCT " & _
                                            "hrassetdeclarationT2_master.assetdeclarationt2unkid  " & _
                          ", hrassetdeclarationT2_master.employeeunkid " & _
                                          ", ISNULL(hremployee_master.firstname, '') + ' ' " & _
                                            "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
                                            "+ ISNULL(hremployee_master.surname, '') AS employeename  " & _
                          ", ISNULL(hremployee_master.employeecode, '') AS noofemployment " & _
                             "      , ISNULL(JM.job_name, '') AS position " & _
                             "      , ISNULL(DM.name, '') AS centerforwork " & _
                             "      , ISNULL(CM.name, '') AS workstation " & _
                          ", hrassetdeclarationT2_master.finyear_start " & _
                          ", hrassetdeclarationT2_master.Tin_No " & _
                          ", hrassetdeclarationT2_master.finyear_end " & _
                          ", hrassetdeclarationT2_master.isfinalsaved " & _
                          ", hrassetdeclarationT2_master.userunkid " & _
                          ", hrassetdeclarationT2_master.isvoid " & _
                          ", hrassetdeclarationT2_master.voiduserunkid " & _
                          ", hrassetdeclarationT2_master.voiddatetime " & _
                          ", hrassetdeclarationT2_master.voidreason " & _
                                          ", ISNULL(hrassetdeclarationT2_master.transactiondate " & _
                                                 ", GETDATE()) AS transactiondate " & _
                          ", savedate " & _
                          ", finalsavedate " & _
                          ", unlockfinalsavedate " & _
                          ", ISNULL(isacknowledged, 0 ) AS isacknowledged " & _
                                    ", '" & key.Key & "' AS databasename " & _
                                    ", convert(varchar(4), convert(datetime, finyear_start,112), 112)   + '-' + convert(varchar(4), convert(datetime, finyear_end,112), 112) AS yearname " & _
                                  "FROM      " & key.Key & "..hrassetdeclarationT2_master " & _
                                            "LEFT JOIN " & key.Key & "..hremployee_master ON hrassetdeclarationT2_master.employeeunkid = hremployee_master.employeeunkid " & _
                             "      LEFT JOIN " & _
                             "          ( " & _
                             "              SELECT " & _
                             "                  departmentunkid " & _
                             "                  ,classunkid " & _
                             "                  ,employeeunkid " & _
                             "                  ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                             "              FROM " & key.Key & "..hremployee_transfer_tran " & _
                             "              WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                             "          ) AS Alloc ON Alloc.employeeunkid = " & key.Key & "..hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                             "      LEFT JOIN " & key.Key & "..hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
                             "      LEFT JOIN " & key.Key & "..hrclasses_master AS CM ON Alloc.classunkid = CM.classesunkid " & _
                             "      LEFT JOIN " & _
                             "          ( " & _
                             "              SELECT " & _
                             "                  jobunkid " & _
                             "                  ,employeeunkid " & _
                             "                  ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                             "              FROM " & key.Key & "..hremployee_categorization_tran " & _
                             "              WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
                             "          ) AS Jobs ON Jobs.employeeunkid = " & key.Key & "..hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                             "      LEFT JOIN " & key.Key & "..hrjob_master AS JM ON Jobs.jobunkid = JM.jobunkid "

                'Hemant (15 Nov 2018) -- [isacknowledged]
                If xDateJoinQry.Trim.Length > 0 Then
                    strInnerQ &= xDateJoinQry
                End If

                If xUACQry.Trim.Length > 0 Then
                    strInnerQ &= xUACQry
                End If

                If strCarRegno <> "" Then
                    strInnerQ &= " LEFT JOIN " & key.Key & "..hrasset_vehicles_tran ON hrasset_vehicles_tran.assetdeclarationt2unkid = hrassetdeclarationT2_master.assetdeclarationt2unkid "
                End If

                strInnerQ &= "      WHERE     ISNULL(hrassetdeclarationT2_master.isvoid, 0) = 0 "

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        strInnerQ &= xDateFilterQry
                    End If
                End If

                If xUACFiltrQry.Trim.Length > 0 Then
                    strInnerQ &= " AND " & xUACFiltrQry
                End If

                If strCarRegno <> "" Then
                    strInnerQ &= " AND hrasset_vehicles_tran.regno like '%" & strCarRegno & "%' "
                End If

                If intAssetDeclaration2UnkID > 0 Then
                    strInnerQ &= " AND hrassetdeclarationT2_master.assetdeclarationt2unkid = @assetdeclarationt2unkid "

                End If

                If intEmployeeUnkID > 0 Then
                    strInnerQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
                End If

                If blnNotIncludeFinalSaved = True Then
                    strInnerQ &= " AND hrassetdeclarationT2_master.isfinalsaved = 0 "
                End If

                'Hemant (05 Dec 2018) -- Start
                'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
                'Hemant (11 Feb 2019) -- Start
                'ISSUE : Error Occurs "The Variable @finyear_start has already been declared" while ticked  Include closed Year Transaction 
                'If xFinStartDate <> Nothing Then
                If xFinStartDate <> Nothing AndAlso blnIncludeClosedYear = False Then
                    'Hemant (11 Feb 2019) -- End
                    'Hemant (10 Dec 2018) -- Start
                    'strInnerQ &= " AND CONVERT(CHAR(8),finyear_start,112)  = @finyear_start"
                    strInnerQ &= " AND CONVERT(CHAR(8),finyear_start,112)  >= @finyear_start "
                    'Hemant (11 Feb 2019) -- End
                    'Hemant (10 Dec 2018) -- End
                    'Hemant (11 Feb 2019) -- Start
                    'ISSUE : Error Occurs "The Variable @finyear_start has already been declared" while ticked  Include closed Year Transaction 
                    objDataOperation.AddParameter("@finyear_start", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xFinStartDate).ToString())
                    'Hemant (11 Feb 2019) -- End
                End If
                'Hemant (11 Feb 2019) -- Start
                'ISSUE : Error Occurs "The Variable @finyear_start has already been declared" while ticked  Include closed Year Transaction 
                'If xFinEndDate <> Nothing Then
                If xFinEndDate <> Nothing AndAlso blnIncludeClosedYear = False Then
                    'Hemant (11 Feb 2019) -- End
                    'Hemant (10 Dec 2018) -- Start
                    'strInnerQ &= " AND CONVERT(CHAR(8),finyear_end,112) = @finyear_end"
                    strInnerQ &= " AND CONVERT(CHAR(8),finyear_end,112) <= @finyear_end "
                    'Hemant (10 Dec 2018) -- End
                    'Hemant (11 Feb 2019) -- Start
                    'ISSUE : Error Occurs "The Variable @finyear_start has already been declared" while ticked  Include closed Year Transaction 
                    objDataOperation.AddParameter("@finyear_end", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xFinEndDate).ToString())
                    'Hemant (11 Feb 2019) -- End
                End If
                'Hemant (05 Dec 2018) -- End

                If strFilter.Trim <> "" Then
                    strInnerQ &= " AND " & strFilter
                End If

            Next

            strQ &= strInnerQ.Substring(9)

            strQ &= "        ) AS A "

            strQ &= " ORDER BY A.yearname DESC "

            If strOrderByFields.Trim <> "" Then
                strQ &= ", " & strOrderByFields
            End If

            If intAssetDeclaration2UnkID > 0 Then
                objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclaration2UnkID)
            End If

            If intEmployeeUnkID > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkID)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function InsertData(ByVal xDatabaseName As String, ByVal xCurrentDatetTime As DateTime, ByVal eAction As enAction, Optional ByVal xFinStartDate As DateTime = Nothing, Optional ByVal xFinEndDate As DateTime = Nothing) As Boolean
        'Hemant (05 Dec 2018) -- [xFinStartDate, xFinEndDate]
        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Dim intAssetDeclaration2UnkID As Integer = 0
        Dim blnChildTableChanged As Boolean = False
        Dim blnIsUpdate As Boolean = False
        Dim decBusinessTotal As Decimal = 0
        Dim decShareDividendTotal As Decimal = 0
        Dim decHouseTotal As Decimal = 0
        Dim decPropertiesTotal As Decimal = 0
        Dim decVehicleTotal As Decimal = 0
        Dim decRelativesTotal As Decimal = 0
        Dim decOtherBusinessTotal As Decimal = 0
        Dim decOtherResourceTotal As Decimal = 0
        Dim decDebtTotal As Decimal = 0
        Dim dtOld As DataTable

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            intAssetDeclaration2UnkID = mintAssetdeclaration2unkid

            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'If isExist(mintEmployeeunkid, , intAssetDeclaration2UnkID) = True Then
            'Hemant (05 Dec 2018) -- Start
            'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
            'If isExist(mintEmployeeunkid, , intAssetDeclaration2UnkID, objDataOperation) = True Then
            If isExist(mintEmployeeunkid, , intAssetDeclaration2UnkID, objDataOperation, xFinStartDate, xFinEndDate) = True Then
                'Hemant (05 Dec 2018) -- End
                'Hemant (14 Nov 2018) -- End

                blnIsUpdate = True

                mintAssetdeclaration2unkid = intAssetDeclaration2UnkID

                'Hemant (14 Nov 2018) -- Start
                'Enhancement : Changes for Asset Declaration template2
                'If Update(eAction) = False Then
                If Update(eAction, objDataOperation) = False Then
                    'Hemant (14 Nov 2018) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            Else
                'Hemant (14 Nov 2018) -- Start
                'Enhancement : Changes for Asset Declaration template2
                'blnFlag = Insert()
                blnFlag = Insert(objDataOperation)
                'Hemant (14 Nov 2018) -- End
                If blnFlag = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            End If


            If mdtBusinessDealings IsNot Nothing Then
                Dim objAssetbusinessdealT2tran As New clsAsset_business_dealT2_tran
                'Hemant (14 Nov 2018) -- Start
                'Enhancement : Changes for Asset Declaration template2
                'objAssetbusinessdealT2tran._Assetdeclarationt2unkid(xDatabaseName) = mintAssetdeclaration2unkid
                objAssetbusinessdealT2tran.GetData(xDatabaseName, mintAssetdeclaration2unkid, objDataOperation)
                'Hemant (14 Nov 2018) -- End
                dtOld = objAssetbusinessdealT2tran._Datasource
                objAssetbusinessdealT2tran._Userunkid = mintUserunkid
                objAssetbusinessdealT2tran._Datasource = mdtBusinessDealings
                objAssetbusinessdealT2tran._AuditUserId = minAuditUserid
                objAssetbusinessdealT2tran._AuditDate = minAuditDate
                objAssetbusinessdealT2tran._ClientIP = minClientIp
                objAssetbusinessdealT2tran._Loginemployeeunkid = minloginemployeeunkid
                objAssetbusinessdealT2tran._HostName = mstrHostName
                objAssetbusinessdealT2tran._FormName = mstrFormName
                objAssetbusinessdealT2tran._IsFromWeb = blnIsFromWeb


                'Hemant (14 Nov 2018) -- Start
                'Enhancement : Changes for Asset Declaration template2
                'If objAssetbusinessdealT2tran.InserByDataTable(blnChildTableChanged, dtOld, decBusinessTotal, xCurrentDatetTime) = False Then
                If objAssetbusinessdealT2tran.InserByDataTable(blnChildTableChanged, dtOld, decBusinessTotal, xCurrentDatetTime, objDataOperation) = False Then
                    'Hemant (14 Nov 2018) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If



            If mdtStaffOwnerShip IsNot Nothing Then
                Dim objAsset_securitiesT2_tran As New clsAsset_securitiesT2_tran
                'Hemant (14 Nov 2018) -- Start
                'Enhancement : Changes for Asset Declaration template2
                'objAsset_securitiesT2_tran._Assetdeclarationt2unkid(xDatabaseName) = mintAssetdeclaration2unkid
                objAsset_securitiesT2_tran.GetData(xDatabaseName, mintAssetdeclaration2unkid, objDataOperation)
                'Hemant (14 Nov 2018) -- End
                dtOld = objAsset_securitiesT2_tran._Datasource
                objAsset_securitiesT2_tran._Userunkid = mintUserunkid
                objAsset_securitiesT2_tran._Datasource = mdtStaffOwnerShip
                objAsset_securitiesT2_tran._AuditUserid = minAuditUserid
                objAsset_securitiesT2_tran._AuditDate = minAuditDate
                objAsset_securitiesT2_tran._ClientIp = minClientIp
                objAsset_securitiesT2_tran._Loginemployeeunkid = minloginemployeeunkid
                objAsset_securitiesT2_tran._HostName = mstrHostName
                objAsset_securitiesT2_tran._FormName = mstrFormName
                objAsset_securitiesT2_tran._IsFromWeb = blnIsFromWeb

                'Hemant (14 Nov 2018) -- Start
                'Enhancement : Changes for Asset Declaration template2
                'If objAsset_securitiesT2_tran.InserByDataTable(blnChildTableChanged, dtOld, decShareDividendTotal, xCurrentDatetTime) = False Then
                If objAsset_securitiesT2_tran.InserByDataTable(blnChildTableChanged, dtOld, decShareDividendTotal, xCurrentDatetTime, objDataOperation) = False Then
                    'Hemant (14 Nov 2018) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            If mdtBankAccounts IsNot Nothing Then
                Dim objAssetBankAccounts As New clsAsset_bankT2_tran


                'Hemant (14 Nov 2018) -- Start
                'Enhancement : Changes for Bind Transactions
                'objAssetBankAccounts._Assetdeclarationt2unkid(xDatabaseName) = mintAssetdeclaration2unkid
                objAssetBankAccounts.GetData(xDatabaseName, mintAssetdeclaration2unkid, objDataOperation)
                'Hemant (14 Nov 2018) -- End

                dtOld = objAssetBankAccounts._Datasource
                objAssetBankAccounts._Userunkid = mintUserunkid
                objAssetBankAccounts._Datasource = mdtBankAccounts
                objAssetBankAccounts._AuditUserid = minAuditUserid
                objAssetBankAccounts._AuditDate = minAuditDate
                objAssetBankAccounts._ClientIp = minClientIp
                objAssetBankAccounts._Loginemployeeunkid = minloginemployeeunkid
                objAssetBankAccounts._HostName = mstrHostName
                objAssetBankAccounts._FormName = mstrFormName
                objAssetBankAccounts._IsFromWeb = blnIsFromWeb


                'Hemant (14 Nov 2018) -- Start
                'Enhancement : Changes for Bind Transactions
                'If objAssetBankAccounts.InserByDataTable(blnChildTableChanged, dtOld, decHouseTotal, xCurrentDatetTime) = False Then
                If objAssetBankAccounts.InserByDataTable(blnChildTableChanged, dtOld, decHouseTotal, xCurrentDatetTime, objDataOperation) = False Then
                    'Hemant (14 Nov 2018) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            If mdtRealProperties IsNot Nothing Then
                Dim objAssetProperties As New clsAsset_propertiesT2_tran

                'Hemant (14 Nov 2018) -- Start
                'Enhancement : Changes for Bind Transactions
                'objAssetProperties._Assetdeclarationt2unkid(xDatabaseName) = mintAssetdeclaration2unkid
                objAssetProperties.GetData(xDatabaseName, mintAssetdeclaration2unkid, objDataOperation)
                'Hemant (14 Nov 2018) -- End

                dtOld = objAssetProperties._Datasource
                objAssetProperties._Userunkid = mintUserunkid
                objAssetProperties._Datasource = mdtRealProperties
                objAssetProperties._AuditUserid = minAuditUserid
                objAssetProperties._AuditDate = minAuditDate
                objAssetProperties._ClientIp = minClientIp
                objAssetProperties._Loginemployeeunkid = minloginemployeeunkid
                objAssetProperties._HostName = mstrHostName
                objAssetProperties._FormName = mstrFormName
                objAssetProperties._IsFromWeb = blnIsFromWeb


                'Hemant (14 Nov 2018) -- Start
                'Enhancement : Changes for Bind Transactions
                'If objAssetProperties.InserByDataTable(blnChildTableChanged, dtOld, decPropertiesTotal, xCurrentDatetTime) = False Then
                If objAssetProperties.InserByDataTable(blnChildTableChanged, dtOld, decPropertiesTotal, xCurrentDatetTime, objDataOperation) = False Then
                    'Hemant (14 Nov 2018) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            If mdtLiabilities IsNot Nothing Then
                Dim objAssetLiabilities As New clsAsset_liabilitiesT2_tran

                'Hemant (14 Nov 2018) -- Start
                'Enhancement : Changes for Bind Transactions
                'objAssetLiabilities._Assetdeclarationt2unkid(xDatabaseName) = mintAssetdeclaration2unkid
                objAssetLiabilities.GetData(xDatabaseName, mintAssetdeclaration2unkid, objDataOperation)
                'Hemant (14 Nov 2018) -- End

                dtOld = objAssetLiabilities._Datasource
                objAssetLiabilities._Userunkid = mintUserunkid
                objAssetLiabilities._Datasource = mdtLiabilities
                objAssetLiabilities._AuditUserid = minAuditUserid
                objAssetLiabilities._AuditDate = minAuditDate
                objAssetLiabilities._ClientIp = minClientIp
                objAssetLiabilities._Loginemployeeunkid = minloginemployeeunkid
                objAssetLiabilities._HostName = mstrHostName
                objAssetLiabilities._FormName = mstrFormName
                objAssetLiabilities._IsFromWeb = blnIsFromWeb

                'Hemant (14 Nov 2018) -- Start
                'Enhancement : Changes for Bind Transactions
                'If objAssetLiabilities.InserByDataTable(blnChildTableChanged, dtOld, decVehicleTotal, xCurrentDatetTime) = False Then
                If objAssetLiabilities.InserByDataTable(blnChildTableChanged, dtOld, decVehicleTotal, xCurrentDatetTime, objDataOperation) = False Then
                    'Hemant (14 Nov 2018) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            If mdtRelatives IsNot Nothing Then
                Dim objAssetRelatives As New clsAsset_relativesT2_tran

                'Hemant (14 Nov 2018) -- Start
                'Enhancement : Changes for Bind Transactions
                'objAssetRelatives._Assetdeclarationt2unkid(xDatabaseName) = mintAssetdeclaration2unkid
                objAssetRelatives.GetData(xDatabaseName, mintAssetdeclaration2unkid, objDataOperation)
                'Hemant (14 Nov 2018) -- End

                dtOld = objAssetRelatives._Datasource
                objAssetRelatives._Userunkid = mintUserunkid
                objAssetRelatives._Datasource = mdtRelatives
                objAssetRelatives._AuditUserid = minAuditUserid
                objAssetRelatives._AuditDate = minAuditDate
                objAssetRelatives._ClientIp = minClientIp
                objAssetRelatives._Loginemployeeunkid = minloginemployeeunkid
                objAssetRelatives._HostName = mstrHostName
                objAssetRelatives._FormName = mstrFormName
                objAssetRelatives._IsFromWeb = blnIsFromWeb

                If objAssetRelatives.InserByDataTable(blnChildTableChanged, dtOld, decRelativesTotal, xCurrentDatetTime) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If


            'Gajanan (06 Oct 2018) -- Start
            'Enhancement : Implementing New Module of Asset Declaration Template 2
            If mdtDependantBusiness IsNot Nothing Then
                Dim objAsset_businessdealT2depn_tran As New clsAsset_businessdealT2depn_tran

                'Gajanan (14 Nov 2018) -- Start
                'Enhancement : Changes for Asset Declaration template2
                'objAsset_businessdealT2depn_tran._Assetdeclarationt2unkid(xDatabaseName) = mintAssetdeclaration2unkid
                objAsset_businessdealT2depn_tran.GetData(xDatabaseName, mintAssetdeclaration2unkid, objDataOperation)
                'Gajanan (14 Nov 2018) -- End

                dtOld = objAsset_businessdealT2depn_tran._Datasource
                objAsset_businessdealT2depn_tran._Userunkid = mintUserunkid
                objAsset_businessdealT2depn_tran._Datasource = mdtDependantBusiness
                objAsset_businessdealT2depn_tran._AuditUserid = minAuditUserid
                objAsset_businessdealT2depn_tran._AuditDate = minAuditDate
                objAsset_businessdealT2depn_tran._ClientIp = minClientIp
                objAsset_businessdealT2depn_tran._Loginemployeeunkid = minloginemployeeunkid
                objAsset_businessdealT2depn_tran._HostName = mstrHostName
                objAsset_businessdealT2depn_tran._FormName = mstrFormName
                objAsset_businessdealT2depn_tran._IsFromWeb = blnIsFromWeb

                'Gajanan (14 Nov 2018) -- Start
                'Enhancement : Changes for Asset Declaration template2
                'If objAsset_businessdealT2depn_tran.InserByDataTable(blnChildTableChanged, dtOld, xCurrentDatetTime) = False Then
                If objAsset_businessdealT2depn_tran.InserByDataTable(blnChildTableChanged, dtOld, xCurrentDatetTime, objDataOperation) = False Then
                    'Gajanan (14 Nov 2018) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If


            If mdtDependantShare IsNot Nothing Then
                Dim objAsset_securitiesT2depn_tran As New clsAsset_securitiesT2depn_tran

                'Gajanan (14 Nov 2018) -- Start
                'Enhancement : Changes for Asset Declaration template2
                'objAsset_securitiesT2depn_tran._Assetdeclarationt2unkid(xDatabaseName) = mintAssetdeclaration2unkid
                objAsset_securitiesT2depn_tran.GetData(xDatabaseName, mintAssetdeclaration2unkid, objDataOperation)
                'Gajanan (14 Nov 2018) -- End

                dtOld = objAsset_securitiesT2depn_tran._Datasource
                objAsset_securitiesT2depn_tran._Userunkid = mintUserunkid
                objAsset_securitiesT2depn_tran._Datasource = mdtDependantShare
                objAsset_securitiesT2depn_tran._AuditUserid = minAuditUserid
                objAsset_securitiesT2depn_tran._AuditDate = minAuditDate
                objAsset_securitiesT2depn_tran._ClientIp = minClientIp
                objAsset_securitiesT2depn_tran._Loginemployeeunkid = minloginemployeeunkid
                objAsset_securitiesT2depn_tran._HostName = mstrHostName
                objAsset_securitiesT2depn_tran._FormName = mstrFormName
                objAsset_securitiesT2depn_tran._IsFromWeb = blnIsFromWeb

                'Gajanan (14 Nov 2018) -- Start
                'Enhancement : Changes for Asset Declaration template2
                'If objAsset_securitiesT2depn_tran.InserByDataTable(blnChildTableChanged, dtOld, xCurrentDatetTime) = False Then
                If objAsset_securitiesT2depn_tran.InserByDataTable(blnChildTableChanged, dtOld, xCurrentDatetTime, objDataOperation) = False Then
                    'Gajanan (14 Nov 2018) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            If mdtDependantBank IsNot Nothing Then
                Dim objAsset_bankT2depn_tran As New clsAsset_bankT2depn_tran

                'Gajanan (14 Nov 2018) -- Start
                'Enhancement : Changes for Asset Declaration template2
                'objAsset_bankT2depn_tran._Assetdeclarationt2unkid(xDatabaseName) = mintAssetdeclaration2unkid
                objAsset_bankT2depn_tran.GetData(xDatabaseName, mintAssetdeclaration2unkid, objDataOperation)
                'Gajanan (14 Nov 2018) -- End

                dtOld = objAsset_bankT2depn_tran._Datasource
                objAsset_bankT2depn_tran._Userunkid = mintUserunkid
                objAsset_bankT2depn_tran._Datasource = mdtDependantBank
                objAsset_bankT2depn_tran._AuditUserid = minAuditUserid
                objAsset_bankT2depn_tran._AuditDate = minAuditDate
                objAsset_bankT2depn_tran._ClientIp = minClientIp
                objAsset_bankT2depn_tran._Loginemployeeunkid = minloginemployeeunkid
                objAsset_bankT2depn_tran._HostName = mstrHostName
                objAsset_bankT2depn_tran._FormName = mstrFormName
                objAsset_bankT2depn_tran._IsFromWeb = blnIsFromWeb

                'Gajanan (14 Nov 2018) -- Start
                'Enhancement : Changes for Asset Declaration template2
                'If objAsset_bankT2depn_tran.InserByDataTable(blnChildTableChanged, dtOld, xCurrentDatetTime) = False Then
                If objAsset_bankT2depn_tran.InserByDataTable(blnChildTableChanged, dtOld, xCurrentDatetTime, objDataOperation) = False Then
                    'Gajanan (14 Nov 2018) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If


            If mdtDependantProperties IsNot Nothing Then
                Dim objAsset_propertiesT2depn_tran As New clsAsset_propertiesT2depn_tran

                'Gajanan (14 Nov 2018) -- Start
                'Enhancement : Changes for Asset Declaration template2
                'objAsset_propertiesT2depn_tran._Assetdeclarationt2unkid(xDatabaseName) = mintAssetdeclaration2unkid
                objAsset_propertiesT2depn_tran.GetData(xDatabaseName, mintAssetdeclaration2unkid, objDataOperation)
                'Gajanan (14 Nov 2018) -- End

                dtOld = objAsset_propertiesT2depn_tran._Datasource
                objAsset_propertiesT2depn_tran._Userunkid = mintUserunkid
                objAsset_propertiesT2depn_tran._Datasource = mdtDependantProperties
                objAsset_propertiesT2depn_tran._AuditUserid = minAuditUserid
                objAsset_propertiesT2depn_tran._AuditDate = minAuditDate
                objAsset_propertiesT2depn_tran._ClientIp = minClientIp
                objAsset_propertiesT2depn_tran._Loginemployeeunkid = minloginemployeeunkid
                objAsset_propertiesT2depn_tran._HostName = mstrHostName
                objAsset_propertiesT2depn_tran._FormName = mstrFormName
                objAsset_propertiesT2depn_tran._IsFromWeb = blnIsFromWeb

                'Gajanan (14 Nov 2018) -- Start
                'Enhancement : Changes for Asset Declaration template2
                'If objAsset_propertiesT2depn_tran.InserByDataTable(blnChildTableChanged, dtOld, xCurrentDatetTime) = False Then
                If objAsset_propertiesT2depn_tran.InserByDataTable(blnChildTableChanged, dtOld, xCurrentDatetTime, objDataOperation) = False Then
                    'Gajanan (14 Nov 2018) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If
            'Gajanan(06 Oct 2018) -- End


            '******     UPDATE ALL CHILD TABLE TOTAL IN ASSET_DECLARATION_MASTER CLASS    *****
            '----------------------------------------------------------------------------------

            'Me._Assetdeclaration2unkid(xDatabaseName) = mintAssetdeclaration2unkid

            'If Update(eAction) = False Then
            '    objDataOperation.ReleaseTransaction(False)
            '    Return False
            'End If

            ''---------------------------
            'If blnIsUpdate = False Then
            '    If blnChildTableChanged = False Then
            '        If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", mintAssetdeclaration2unkid, "", "", 0, 1, 0) = False Then
            '            objDataOperation.ReleaseTransaction(False)
            '            Return False
            '        End If
            '    End If
            'Else
            '    If blnChildTableChanged = False Then
            '        If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrassetdeclarationT2_master", intAssetDeclaration2UnkID, "assetdeclarationt2unkid", 2, objDataOperation) = True Then
            '            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", intAssetDeclaration2UnkID, "", "", 0, 2, 0) = False Then
            '                objDataOperation.ReleaseTransaction(False)
            '                Return False
            '            End If
            '        End If
            '    End If
            'End If


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassetdeclarationT2_master) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByRef intNewUnkId As Integer = 0) As Boolean
        'Sohail (07 Dec 2018) - [intNewUnkId]
        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            objDataOperation.BindTransaction()
            'Sohail (07 Dec 2018) -- End
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End


        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            intNewUnkId = 0
            Dim intUnkId As Integer = 0
            If isExist(mintEmployeeunkid, 0, intUnkId, objDataOperation, mdtFinyear_Start, mdtFinyear_End) = True Then
                If intUnkId > 0 Then
                    mintAssetdeclaration2unkid = intUnkId
                    intNewUnkId = mintAssetdeclaration2unkid
                    If Update(enAction.EDIT_ONE, objDataOperation) = False Then
                        Return False
                    Else
                        Return True
                    End If
                End If
            End If
            objDataOperation.ClearParameters()
            'Sohail (07 Dec 2018) -- End

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@tin_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTin_No.ToString)
            objDataOperation.AddParameter("@finyear_start", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFinyear_Start.ToString)
            objDataOperation.AddParameter("@finyear_end", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFinyear_End.ToString)
            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            If mdtSavedate = Nothing Then
                objDataOperation.AddParameter("@savedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@savedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSavedate.ToString)
            End If
            If mdtFinalsavedate = Nothing Then
                objDataOperation.AddParameter("@finalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@finalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFinalsavedate.ToString)
            End If
            If mdtUnlockfinalsavedate = Nothing Then
                objDataOperation.AddParameter("@unlockfinalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@unlockfinalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtUnlockfinalsavedate.ToString)
            End If
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            objDataOperation.AddParameter("@isacknowledged", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAcknowledged.ToString)
            'Hemant (15 Nov 2018) -- End

            strQ = "INSERT INTO hrassetdeclarationT2_master ( " & _
              "  employeeunkid " & _
              ",  tin_no " & _
              ", finyear_start " & _
              ", finyear_end " & _
              ", isfinalsaved " & _
              ", transactiondate" & _
              ", savedate " & _
              ", finalsavedate " & _
              ", unlockfinalsavedate" & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", loginemployeeunkid" & _
              ", voidloginemployeeunkid" & _
              ", isacknowledged " & _
            ") VALUES (" & _
              "  @employeeunkid " & _
              ", @tin_no " & _
              ", @finyear_start " & _
              ", @finyear_end " & _
              ", @isfinalsaved " & _
              ", @transactiondate" & _
              ", @savedate " & _
              ", @finalsavedate " & _
              ", @unlockfinalsavedate" & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @loginemployeeunkid" & _
              ", @voidloginemployeeunkid" & _
              ", @isacknowledged " & _
            "); SELECT @@identity"

            'Hemant (15 Nov 2018) -- [isacknowledged]
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'objDataOperation.ReleaseTransaction(False)
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Sohail (07 Dec 2018) -- End
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAssetdeclaration2unkid = dsList.Tables(0).Rows(0).Item(0)

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            intNewUnkId = mintAssetdeclaration2unkid
            'Sohail (07 Dec 2018) -- End

            If Insert_AtTranLog(objDataOperation, 1) = False Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Sohail (07 Dec 2018) -- End
                Return False
            End If

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Sohail (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Sohail (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassetdeclarationT2_master) </purpose>
    Public Function Update(ByVal eAction As enAction, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (14 Nov 2018) -- [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            objDataOperation.BindTransaction()
            'Sohail (07 Dec 2018) -- End
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End

        Try
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclaration2unkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@Tin_No", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTin_No.ToString)
            objDataOperation.AddParameter("@finyear_start", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFinyear_Start.ToString)
            objDataOperation.AddParameter("@finyear_end", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFinyear_End.ToString)
            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            If mdtTransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            If mdtSavedate = Nothing Then
                objDataOperation.AddParameter("@savedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@savedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSavedate.ToString)
            End If
            If mdtFinalsavedate = Nothing Then
                objDataOperation.AddParameter("@finalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@finalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFinalsavedate.ToString)
            End If
            If mdtUnlockfinalsavedate = Nothing Then
                objDataOperation.AddParameter("@unlockfinalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@unlockfinalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtUnlockfinalsavedate.ToString)
            End If

            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            objDataOperation.AddParameter("@isacknowledged", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAcknowledged.ToString)
            'Hemant (15 Nov 2018) -- End


            strQ = "UPDATE hrassetdeclarationT2_master SET " & _
              "  employeeunkid = @employeeunkid" & _
              ", tin_no = @Tin_No " & _
              ", finyear_start = @finyear_start" & _
              ", finyear_end = @finyear_end" & _
              ", isfinalsaved = @isfinalsaved " & _
              ", transactiondate = @transactiondate " & _
              ", savedate = @savedate" & _
              ", finalsavedate = @finalsavedate" & _
              ", unlockfinalsavedate = @unlockfinalsavedate " & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", loginemployeeunkid = @loginemployeeunkid " & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
              ", isacknowledged = @isacknowledged " & _
            "WHERE assetdeclarationt2unkid = @assetdeclarationt2unkid "

            'Hemant (15 Nov 2018) -- [isacknowledged]
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If blnInsertLog = True Then
            '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", mintAssetdeclaration2unkid, "", "", 0, 2, 0, , mintUserunkid) = False Then
            '        Return False
            '    End If
            'End If 

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'If eAction = enAction.EDIT_ONE Then
            If IsTableDataUpdate(mintAssetdeclaration2unkid, objDataOperation) = False Then
                If Insert_AtTranLog(objDataOperation, 2) = False Then
                    'Sohail (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    'Sohail (07 Dec 2018) -- End
                    Return False
                End If
            End If
            'End If
            'Sohail (07 Dec 2018) -- End


            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Sohail (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Sohail (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()

            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassetdeclarationT2_master) </purpose>
    Public Function Void(ByVal intUnkid As Integer, _
                         ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnChildExist As Boolean = False

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If VoidAssetDeclarationMaster(intUnkid, intVoidUserID, dtVoidDateTime, strVoidReason) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If


            Dim objAssetbusinessdealT2tran As New clsAsset_business_dealT2_tran
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Changes for Bind Transactions
            'dsList = objAssetbusinessdealT2tran.GetList("Business", intUnkid)
            'If dsList.Tables("Business").Rows.Count > 0 Then
            If (objDataOperation.RecordCount("select 1 FROM hrasset_businessdealT2_tran WHERE assetdeclarationt2unkid = " & intUnkid & " AND isvoid = 0   ")) > 0 Then
                blnChildExist = True
                objAssetbusinessdealT2tran._AuditUserId = minAuditUserid
                objAssetbusinessdealT2tran._AuditDate = minAuditDate
                objAssetbusinessdealT2tran._ClientIP = minClientIp
                objAssetbusinessdealT2tran._Loginemployeeunkid = minloginemployeeunkid
                objAssetbusinessdealT2tran._HostName = mstrHostName
                objAssetbusinessdealT2tran._FormName = mstrFormName
                objAssetbusinessdealT2tran._IsFromWeb = blnIsFromWeb
                'If objAssetbusinessdealT2tran.VoidByAssetDeclarationt2unkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3) = False Then
                If objAssetbusinessdealT2tran.VoidByAssetDeclarationt2unkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    'Hemant (14 Nov 2018) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

            End If

            Dim objAssetsecurities As New clsAsset_securitiesT2_tran
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Changes for Bind Transactions
            'dsList = objAssetBankAccounts.GetList("House", intUnkid)
            'If dsList.Tables("House").Rows.Count > 0 Then
            If (objDataOperation.RecordCount("select 1 FROM hrasset_securitiesT2_tran WHERE assetdeclarationt2unkid = " & intUnkid & " AND isvoid = 0   ")) > 0 Then
                blnChildExist = True
                objAssetsecurities._AuditUserid = minAuditUserid
                objAssetsecurities._AuditDate = minAuditDate
                objAssetsecurities._ClientIp = minClientIp
                objAssetsecurities._Loginemployeeunkid = minloginemployeeunkid
                objAssetsecurities._HostName = mstrHostName
                objAssetsecurities._FormName = mstrFormName
                objAssetsecurities._IsFromWeb = blnIsFromWeb
                'If objAssetBankAccounts.VoidByAssetDeclarationt2UnkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3) = False Then
                If objAssetsecurities.VoidByAssetDeclarationt2UnkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    'Hemant (14 Nov 2018) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            Dim objAssetBankAccounts As New clsAsset_bankT2_tran
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Changes for Bind Transactions
            'dsList = objAssetBankAccounts.GetList("House", intUnkid)
            'If dsList.Tables("House").Rows.Count > 0 Then
            If (objDataOperation.RecordCount("select 1 FROM hrasset_bankT2_tran WHERE assetdeclarationt2unkid = " & intUnkid & " AND isvoid = 0   ")) > 0 Then
                blnChildExist = True
                objAssetBankAccounts._AuditUserid = minAuditUserid
                objAssetBankAccounts._AuditDate = minAuditDate
                objAssetBankAccounts._ClientIp = minClientIp
                objAssetBankAccounts._Loginemployeeunkid = minloginemployeeunkid
                objAssetBankAccounts._HostName = mstrHostName
                objAssetBankAccounts._FormName = mstrFormName
                objAssetBankAccounts._IsFromWeb = blnIsFromWeb
                'If objAssetBankAccounts.VoidByAssetDeclarationt2UnkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3) = False Then
                If objAssetBankAccounts.VoidByAssetDeclarationt2UnkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    'Hemant (14 Nov 2018) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            Dim objAssetProperties As New clsAsset_propertiesT2_tran
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Changes for Bind Transactions
            'dsList = objAssetProperties.GetList("Park", intUnkid)
            'If dsList.Tables("Park").Rows.Count > 0 Then
            If (objDataOperation.RecordCount("select 1 FROM hrasset_propertiesT2_tran WHERE assetdeclarationt2unkid = " & intUnkid & " AND isvoid = 0   ")) > 0 Then
                blnChildExist = True
                objAssetProperties._AuditUserid = minAuditUserid
                objAssetProperties._AuditDate = minAuditDate
                objAssetProperties._ClientIp = minClientIp
                objAssetProperties._Loginemployeeunkid = minloginemployeeunkid
                objAssetProperties._HostName = mstrHostName
                objAssetProperties._FormName = mstrFormName
                objAssetProperties._IsFromWeb = blnIsFromWeb
                'If objAssetProperties.VoidByAssetDeclarationt2unkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3) = False Then
                If objAssetProperties.VoidByAssetDeclarationt2unkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    'Hemant (14 Nov 2018) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If


            Dim objAssetLiabilities As New clsAsset_liabilitiesT2_tran
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Changes for Bind Transactions
            'dsList = objAssetLiabilities.GetList("Vehicle", intUnkid)
            'If dsList.Tables("Vehicle").Rows.Count > 0 Then
            If (objDataOperation.RecordCount("select 1 FROM hrasset_liabilitiesT2_tran WHERE assetdeclarationt2unkid = " & intUnkid & " AND isvoid = 0   ")) > 0 Then
                blnChildExist = True

                objAssetLiabilities._AuditUserid = minAuditUserid
                objAssetLiabilities._AuditDate = minAuditDate
                objAssetLiabilities._ClientIp = minClientIp
                objAssetLiabilities._Loginemployeeunkid = minloginemployeeunkid
                objAssetLiabilities._HostName = mstrHostName
                objAssetLiabilities._FormName = mstrFormName
                objAssetLiabilities._IsFromWeb = blnIsFromWeb
                'If objAssetLiabilities.VoidByAssetDeclarationt2unkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3) = False Then
                If objAssetLiabilities.VoidByAssetDeclarationt2unkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    'Hemant (14 Nov 2018) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If


            Dim objAssetRelatives As New clsAsset_relativesT2_tran
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Changes for Bind Transactions
            'dsList = objAssetRelatives.GetList("Relatives", intUnkid)
            'If dsList.Tables("Relatives").Rows.Count > 0 Then
            If (objDataOperation.RecordCount("select 1 FROM hrasset_relativesT2_tran WHERE assetdeclarationt2unkid = " & intUnkid & " AND isvoid = 0   ")) > 0 Then
                blnChildExist = True

                objAssetRelatives._AuditUserid = minAuditUserid
                objAssetRelatives._AuditDate = minAuditDate
                objAssetRelatives._ClientIp = minClientIp
                objAssetRelatives._Loginemployeeunkid = minloginemployeeunkid
                objAssetRelatives._HostName = mstrHostName
                objAssetRelatives._FormName = mstrFormName
                objAssetRelatives._IsFromWeb = blnIsFromWeb
                'If objAssetRelatives.VoidByAssetDeclarationt2unkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3) = False Then
                If objAssetRelatives.VoidByAssetDeclarationt2unkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    'Hemant (14 Nov 2018) -- End
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If


  'Gajanan (06 Oct 2018) -- Start
            'Enhancement : Implementing New Module of Asset Declaration Template 2
            Dim objAssetDeptBank As New clsAsset_bankT2depn_tran
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Changes for Bind Transactions
            'dsList = objAssetDeptBank.GetList("DeptBank", intUnkid)
            'If dsList.Tables("DeptBank").Rows.Count > 0 Then
            If (objDataOperation.RecordCount("select 1 FROM hrasset_bankT2depn_tran WHERE assetdeclarationt2unkid = " & intUnkid & " AND isvoid = 0   ")) > 0 Then
                'Hemant (14 Nov 2018) -- End
                blnChildExist = True

                objAssetDeptBank._AuditUserid = minAuditUserid
                objAssetDeptBank._AuditDate = minAuditDate
                objAssetDeptBank._ClientIp = minClientIp
                objAssetDeptBank._Loginemployeeunkid = minloginemployeeunkid
                objAssetDeptBank._HostName = mstrHostName
                objAssetDeptBank._FormName = mstrFormName
                objAssetDeptBank._IsFromWeb = blnIsFromWeb


                If objAssetDeptBank.VoidByAssetDeclarationt2UnkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            Dim objAssetDeptShare As New clsAsset_securitiesT2depn_tran
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Changes for Bind Transactions
            'dsList = objAssetDeptShare.GetList("DeptShare", intUnkid)
            'If dsList.Tables("DeptShare").Rows.Count > 0 Then
            If (objDataOperation.RecordCount("select 1 FROM hrasset_securitiesT2depn_tran WHERE assetdeclarationt2unkid = " & intUnkid & " AND isvoid = 0   ")) > 0 Then
                'Hemant (14 Nov 2018) -- End
                blnChildExist = True

                objAssetDeptShare._AuditUserid = minAuditUserid
                objAssetDeptShare._AuditDate = minAuditDate
                objAssetDeptShare._ClientIp = minClientIp
                objAssetDeptShare._Loginemployeeunkid = minloginemployeeunkid
                objAssetDeptShare._HostName = mstrHostName
                objAssetDeptShare._FormName = mstrFormName
                objAssetDeptShare._IsFromWeb = blnIsFromWeb


                If objAssetDeptShare.VoidByAssetDeclarationt2UnkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            Dim objAssetDeptBussiness As New clsAsset_businessdealT2depn_tran
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Changes for Bind Transactions
            'dsList = objAssetDeptBussiness.GetList("DeptBusiness", intUnkid)
            'If dsList.Tables("DeptBusiness").Rows.Count > 0 Then
            If (objDataOperation.RecordCount("select 1 FROM hrasset_businessdealT2depn_tran WHERE assetdeclarationt2unkid = " & intUnkid & " AND isvoid = 0   ")) > 0 Then
                'Hemant (14 Nov 2018) -- End
                blnChildExist = True

                objAssetDeptBussiness._AuditUserid = minAuditUserid
                objAssetDeptBussiness._AuditDate = minAuditDate
                objAssetDeptBussiness._ClientIp = minClientIp
                objAssetDeptBussiness._Loginemployeeunkid = minloginemployeeunkid
                objAssetDeptBussiness._HostName = mstrHostName
                objAssetDeptBussiness._FormName = mstrFormName
                objAssetDeptBussiness._IsFromWeb = blnIsFromWeb


                If objAssetDeptBussiness.VoidByAssetDeclarationt2UnkID(mintAssetdeclaration2unkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            Dim objAssetDeptProperty As New clsAsset_propertiesT2depn_tran
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Changes for Bind Transactions
            'dsList = objAssetDeptProperty.GetList("DeptProperty", intUnkid)
            'If dsList.Tables("DeptProperty").Rows.Count > 0 Then
            If (objDataOperation.RecordCount("select 1 FROM hrasset_propertiesT2depn_tran WHERE assetdeclarationt2unkid = " & intUnkid & " AND isvoid = 0   ")) > 0 Then
                'Hemant (14 Nov 2018) -- End
                blnChildExist = True

                objAssetDeptProperty._AuditUserid = minAuditUserid
                objAssetDeptProperty._AuditDate = minAuditDate
                objAssetDeptProperty._ClientIp = minClientIp
                objAssetDeptProperty._Loginemployeeunkid = minloginemployeeunkid
                objAssetDeptProperty._HostName = mstrHostName
                objAssetDeptProperty._FormName = mstrFormName
                objAssetDeptProperty._IsFromWeb = blnIsFromWeb

                If objAssetDeptProperty.VoidByAssetDeclarationt2UnkID(intUnkid, intVoidUserID, dtVoidDateTime, "", 3, objDataOperation) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            'Gajanan(06 Oct 2018) -- End


            'If blnChildExist = False Then

            '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", intUnkid, "", "", 0, 3, 0, , intVoidUserID) = False Then

            '        objDataOperation.ReleaseTransaction(False)
            '        Return False
            '    End If
            'End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Private Function VoidAssetDeclarationMaster(ByVal intAssetDeclaration2UnkID As Integer, _
                                        ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean
       
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "UPDATE hrassetdeclarationT2_master SET " & _
             "  isvoid = 1 " & _
             ", voiduserunkid = @voiduserunkid" & _
             ", voiddatetime = @voiddatetime" & _
             ", voidreason = @voidreason " & _
             ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
           "WHERE assetdeclarationt2unkid = @assetdeclarationt2unkid "

            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclaration2UnkID)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If Insert_AtTranLog(objDataOperation, 3) = False Then
                Return False
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidAssetDeclarationMaster; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intEmpUnkID As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByRef intAssetDeclaration2UnkID As Integer = 0, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal xFinStartDate As DateTime = Nothing, Optional ByVal xFinEndDate As DateTime = Nothing) As Boolean
        'Hemant (05 Dec 2018) -- [xFinStartDate, xFinEndDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Hemant (14 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Hemant (14 Nov 2018) -- End

        Try
            strQ = "SELECT " & _
              "  assetdeclarationt2unkid " & _
              ", employeeunkid " & _
              ", tin_no " & _
              ", finyear_start " & _
              ", finyear_end " & _
              ", isfinalsaved " & _
              ", ISNULL(transactiondate, GETDATE()) AS transactiondate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(isacknowledged, 0 ) AS isacknowledged " & _
             "FROM hrassetdeclarationT2_master " & _
             "WHERE ISNULL(isvoid, 0) = 0 " & _
             "AND employeeunkid = @employeeunkid "

            'Hemant (15 Nov 2018) -- [isacknowledged]
            If intUnkid > 0 Then
                strQ &= " AND assetdeclarationt2unkid <> @assetdeclarationt2unkid"
                objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            'Hemant (05 Dec 2018) -- Start
            'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
            If xFinStartDate <> Nothing Then
                'Hemant (10 Dec 2018) -- Start
                'strQ &= " AND CONVERT(CHAR(8),finyear_start,112)  = @finyear_start"
                strQ &= " AND CONVERT(CHAR(8),finyear_start,112)  >= @finyear_start"
                'Hemant (10 Dec 2018) -- End
                objDataOperation.AddParameter("@finyear_start", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, xFinStartDate)
            End If
            If xFinEndDate <> Nothing Then
                'Hemant (10 Dec 2018) -- Start
                'strQ &= " AND CONVERT(CHAR(8),finyear_end,112) = @finyear_end"
                strQ &= " AND CONVERT(CHAR(8),finyear_end,112) <= @finyear_end"
                'Hemant (10 Dec 2018) -- End
                objDataOperation.AddParameter("@finyear_end", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, xFinEndDate)
            End If
            'Hemant (05 Dec 2018) -- End


            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intAssetDeclaration2UnkID = CInt(dsList.Tables(0).Rows(0).Item("assetdeclarationt2unkid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (14 Nov 2018) -- End
        End Try
    End Function
    Public Function Insert_AtTranLog(ByVal objDoOps As clsDataOperation, ByVal Audittype As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDoOps.ClearParameters()
            objDoOps.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclaration2unkid.ToString)
            objDoOps.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDoOps.AddParameter("@tin_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTin_No.ToString)
            objDoOps.AddParameter("@finyear_start", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFinyear_Start.ToString)
            objDoOps.AddParameter("@finyear_end", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFinyear_End.ToString)
            objDoOps.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinalsaved.ToString)
            If mdtTransactiondate = Nothing Then
                objDoOps.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDoOps.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            End If
            If mdtSavedate = Nothing Then
                objDoOps.AddParameter("@savedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDoOps.AddParameter("@savedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtSavedate.ToString)
            End If
            If mdtFinalsavedate = Nothing Then
                objDoOps.AddParameter("@finalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDoOps.AddParameter("@finalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtFinalsavedate.ToString)
            End If
            If mdtUnlockfinalsavedate = Nothing Then
                objDoOps.AddParameter("@unlockfinalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDoOps.AddParameter("@unlockfinalsavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtUnlockfinalsavedate.ToString)
            End If

            objDoOps.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.NAME_SIZE, Audittype)
            objDoOps.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minAuditUserid)
            'objDoOps.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, minAuditDate)
            objDoOps.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid)
            objDoOps.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDoOps.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDoOps.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDoOps.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)
            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            objDoOps.AddParameter("@isacknowledged", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAcknowledged.ToString)
            'Hemant (15 Nov 2018) -- End

            strQ = "INSERT INTO athrassetdeclarationT2_master ( " & _
              "  assetdeclarationt2unkid " & _
              ", employeeunkid " & _
              ", tin_no " & _
              ", finyear_start " & _
              ", finyear_end " & _
              ", isfinalsaved " & _
              ", transactiondate" & _
              ", savedate " & _
              ", finalsavedate " & _
              ", unlockfinalsavedate" & _
              ", auditdatetime " & _
                     ", audittype " & _
                     ", audituserunkid " & _
                     ", loginemployeeunkid " & _
                     ", ip " & _
                     ", host " & _
                     ", form_name " & _
                     ", isweb " & _
              ", isacknowledged " & _
            ") VALUES (" & _
              "  @assetdeclarationt2unkid " & _
              ", @employeeunkid " & _
              ", @tin_no " & _
              ", @finyear_start " & _
              ", @finyear_end " & _
              ", @isfinalsaved " & _
              ", @transactiondate" & _
              ", @savedate " & _
              ", @finalsavedate " & _
              ", @unlockfinalsavedate" & _
              ", GETDATE() " & _
                     ", @audittype " & _
                     ", @audituserunkid " & _
                     ", @loginemployeeunkid " & _
                     ", @ip " & _
                     ", @host " & _
                     ", @form_name " & _
              ", @isweb " & _
              ", @isacknowledged ) "

            'Hemant (15 Nov 2018) -- [isacknowledged]
            objDoOps.ExecNonQuery(strQ)

            If objDoOps.ErrorMessage <> "" Then
                exForce = New Exception(objDoOps.ErrorNumber & ": " & objDoOps.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
        End Try

        Return True
    End Function

    Public Function GetAssetDeclarationidFromEmployee(ByVal intEmpUnkID As Integer, _
                                                      ByVal xFinStartDate As DateTime, _
                                                      ByVal xFinEndDate As DateTime, _
                                                      Optional ByVal xDataOp As clsDataOperation = Nothing) As DataSet
            'Hemant (03 May 2021) -- [xFinStartDate,xFinEndDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()


        Try
            strQ = "SELECT " & _
              "  assetdeclarationt2unkid " & _
              ", employeeunkid " & _
             "FROM hrassetdeclarationT2_master " & _
             "WHERE ISNULL(isvoid, 0) = 0 " & _
             "AND employeeunkid = @employeeunkid "

            'Hemant (03 May 2021) -- Start
            'ISSUE : Asset Declaration Report Showing First Year Declaration Data.
            If xFinStartDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),finyear_start,112)  >= @finyear_start "
                objDataOperation.AddParameter("@finyear_start", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xFinStartDate).ToString())
            End If

            If xFinEndDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),finyear_end,112) <= @finyear_end "
                objDataOperation.AddParameter("@finyear_end", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xFinEndDate).ToString())
            End If
            'Hemant (15 Apr 2021) -- End

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Hemant (19 Nov 2018) -- Start
    'Enhancement : Changes for NMB Requirement for Email Notification After Final Saved In Asset Declaration 
    Public Sub SendMailToUserForFinalSaved(ByVal strEmployeeUnkId As String _
                                           , ByVal strNotifyAssetDeclarationFinalSavedUserIDs As String _
                                           , ByVal intCompanyUnkId As Integer _
                                           , ByVal intYearUnkId As Integer _
                                           , ByVal strDatabaseName As String _
                                           , ByVal xPeriodStart As Date _
                                           , ByVal xPeriodEnd As Date _
                                           , ByVal xUserModeSetting As String _
                                           , ByVal xOnlyApproved As Boolean _
                                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                           , Optional ByVal iLoginTypeId As Integer = -1 _
                                           , Optional ByVal iLoginEmployeeId As Integer = -1 _
                                           , Optional ByVal iUserId As Integer = -1 _
                                           , Optional ByVal blnFinalSaved As Boolean = False _
                                           , Optional ByVal xFinStart As Date = Nothing _
                                           , Optional ByVal xFinEnd As Date = Nothing _
                                )
        'Hemant (23 Nov 2018) -- [xFinStart, xFinEnd]
        Dim strFinalSavedNofificationArrayIDs() As String
        Dim strEmployeeName As String
        Dim strFinalSaveByName As String
        Dim strMessage As New StringBuilder
        Dim objMaster As New clsMasterData
        Dim objUser As New clsUserAddEdit
        Dim objEmp As New clsEmployee_Master
        Dim objMail As New clsSendMail

        'Hemant (23 Nov 2018) -- Start
        'Enhancement : Changes As per Rutta Request for UAT 
        Dim strEmployeeEmail As String
        Dim objCommon As New clsCommon_Master
        Dim strEmployeeTitle As String
        'Hemant (23 Nov 2018) -- End

        Try
            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            If HttpContext.Current IsNot Nothing Then
                lstWebEmail = New List(Of clsEmailCollection)
            End If
            'Hemant (02 Feb 2022) -- End

            If strNotifyAssetDeclarationFinalSavedUserIDs.Trim <> "" AndAlso blnFinalSaved = True Then
                If iLoginEmployeeId <= 0 Then
                    objUser._Userunkid = CInt(iUserId)
                    strFinalSaveByName = objUser._Firstname & " " & objUser._Lastname
                Else
                    objEmp._Employeeunkid(xPeriodStart) = Convert.ToInt32(iLoginEmployeeId)
                    strFinalSaveByName = objEmp._Firstname & " " & objEmp._Surname
                End If

                'Hemant (23 Nov 2018) -- Start
                'Enhancement : Changes As per Rutta Request for UAT 
                objUser = New clsUserAddEdit

                objEmp._Employeeunkid(xPeriodStart) = Convert.ToInt32(strEmployeeUnkId)
                objCommon._Masterunkid = objEmp._Titalunkid
                strEmployeeTitle = objCommon._Name
                'strEmployeeName = strEmployeeTitle & " " & objEmp._Firstname & " " & objEmp._Surname
                strEmployeeName = strEmployeeTitle & " " & objEmp._Firstname & " " & objEmp._Surname

                strEmployeeEmail = objEmp._Email
                If strEmployeeEmail IsNot Nothing AndAlso strEmployeeEmail.Length > 0 Then
                    strMessage = New StringBuilder

                    objMail._Subject = Language.getMessage(mstrModuleName, 1, "Employee Declaration Notification")

                    strMessage.Append("<HTML> <BODY style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")

                    strMessage.Append(Language.getMessage(mstrModuleName, 2, "Dear") & " <B>" & strEmployeeName & "</B>, <BR><BR>")

                    'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 4, "This is to notify you that your FY :") & xFinStart.Date.Year & " - " & xFinEnd.Date.Year & " ")
                    strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 8, "This is to notify that your declaration has been submitted successfully,") & "<BR><BR>")
                    strMessage.Append(Language.getMessage(mstrModuleName, 5, "Thank you, Compliance "))
                    'strMessage.Append(Language.getMessage(mstrModuleName, 6, " has been submitted to compliance "))
                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'strMessage.Append("<BR><BR><BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B>&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</B>")
                    strMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                    'Gajanan [27-Mar-2019] -- End
                    strMessage.Append("</BODY></HTML>")

                    objMail._Message = strMessage.ToString
                    objMail._ToEmail = strEmployeeEmail

                    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                    If mstrFormName.Trim.Length > 0 Then
                        objMail._Form_Name = mstrFormName
                    End If
                    objMail._LogEmployeeUnkid = iLoginEmployeeId
                    objMail._OperationModeId = iLoginTypeId
                    objMail._UserUnkid = 0
                    Dim objUsr As New clsUserAddEdit
                    objUsr._Userunkid = 0
                    objMail._SenderAddress = IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)
                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT

                    'Hemant (02 Feb 2022) -- Start            
                    'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                    'gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, "", "", 0, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                    If HttpContext.Current Is Nothing Then
                    gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, "", "", 0, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                    Else
                        lstWebEmail.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, "", "", 0, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                    End If
                    'Hemant (02 Feb 2022) -- End
                    objUsr = Nothing
                End If
                'Hemant (23 Nov 2018) -- End
                strFinalSavedNofificationArrayIDs = strNotifyAssetDeclarationFinalSavedUserIDs.Split(",")
                For Each strNofificationID As String In strFinalSavedNofificationArrayIDs
                    Dim strAppEmpIDs As String = objMaster.GetUserAccessLevelEmployees(strDatabaseName, CInt(strNofificationID), intYearUnkId, intCompanyUnkId, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee)
                    If (strAppEmpIDs.Split(",").Contains(strEmployeeUnkId)) = True Then

                        strMessage = New StringBuilder


                        'Hemant (23 Nov 2018) -- Start
                        'Enhancement : Changes As per Rutta Request for UAT 
                        'objUser = New clsUserAddEdit

                        'objEmp._Employeeunkid(xPeriodStart) = Convert.ToInt32(strEmployeeUnkId)
                        'strEmployeeName = objEmp._Firstname & " " & objEmp._Surname
                        'Hemant (23 Nov 2018) -- End



                        objUser._Userunkid = CInt(strNofificationID)
                        If objUser._Email.Trim = "" Then Continue For


                        'Gajanan (28 Nov 2018) -- Start
                        'objMail._Subject = Language.getMessage(mstrModuleName, 1, "Employee Declaration Notification")
                        objMail._Subject = Language.getMessage(mstrModuleName, 1, "Employee Declaration Notification") & "-" & strEmployeeName & "(" & objEmp._Employeecode & ")"
                        'Gajanan (28 Nov 2018) -- End

                        strMessage.Append("<HTML> <BODY style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")

                        strMessage.Append(Language.getMessage(mstrModuleName, 2, "Dear") & " <B>" & objUser._Firstname & " " & objUser._Lastname & "</B>, <BR><BR>")

                        'strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 4, "This is to notify you that your FY :") & xFinStart.Date.Year & " - " & xFinEnd.Date.Year & " ")
                        strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 4, "This is to notify you that the declaration for") & " <B>" & strEmployeeName & "</B> ")
                        'strMessage.Append(Language.getMessage(mstrModuleName, 3, "Declaration of ") & "<B>" & strEmployeeName & "</B> ")
                        strMessage.Append(Language.getMessage(mstrModuleName, 6, "has been submitted.") & "<BR><BR>")
                        strMessage.Append(Language.getMessage(mstrModuleName, 3, "Thank you."))
                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language
                        'strMessage.Append("<BR><BR><BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B>&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</B>")
                        strMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                        'Gajanan [27-Mar-2019] -- End
                        strMessage.Append("</BODY></HTML>")

                        objMail._Message = strMessage.ToString
                        objMail._ToEmail = objUser._Email

                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        If mstrFormName.Trim.Length > 0 Then
                            objMail._Form_Name = mstrFormName
                        End If
                        objMail._LogEmployeeUnkid = iLoginEmployeeId
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = Convert.ToInt32(strNofificationID)
                        Dim objUsr As New clsUserAddEdit
                        objUsr._Userunkid = Convert.ToInt32(strNofificationID)
                        objMail._SenderAddress = IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT

                        'Hemant (02 Feb 2022) -- Start            
                        'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                        'gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, "", "", Convert.ToInt32(strNofificationID), iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                        If HttpContext.Current Is Nothing Then
                        gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, "", "", Convert.ToInt32(strNofificationID), iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                        Else
                            lstWebEmail.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, "", "", Convert.ToInt32(strNofificationID), iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                        End If
                        'Hemant (02 Feb 2022) -- End
                        objUsr = Nothing
                    Else
                        Continue For
                    End If


                Next
            End If

            'Hemant (23 Nov 2018) -- Start
            'Enhancement : Changes As per Rutta Request for UAT 
            If strNotifyAssetDeclarationFinalSavedUserIDs.Trim <> "" AndAlso blnFinalSaved = False Then
                strFinalSavedNofificationArrayIDs = strNotifyAssetDeclarationFinalSavedUserIDs.Split(",")
                        objUser = New clsUserAddEdit
                        objEmp._Employeeunkid(xPeriodStart) = Convert.ToInt32(strEmployeeUnkId)
                        strEmployeeName = objEmp._Firstname & " " & objEmp._Surname
                For Each strNofificationID As String In strFinalSavedNofificationArrayIDs
                    Dim strAppEmpIDs As String = objMaster.GetUserAccessLevelEmployees(strDatabaseName, CInt(strNofificationID), intYearUnkId, intCompanyUnkId, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee)
                    If (strAppEmpIDs.Split(",").Contains(strEmployeeUnkId)) = True Then

                        strMessage = New StringBuilder

                        objUser._Userunkid = CInt(strNofificationID)
                        If objUser._Email.Trim = "" Then Continue For

                        objMail._Subject = Language.getMessage(mstrModuleName, 1, "Employee Declaration Notification")

                        strMessage.Append("<HTML> <BODY style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")

                        strMessage.Append(Language.getMessage(mstrModuleName, 2, "Dear") & " <B>" & objUser._Firstname & " " & objUser._Lastname & "</B>, <BR><BR>")

                        strMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 4, "This is to notify you that the declaration for ") & "  <B>" & strEmployeeName & "</B>  ")
                        strMessage.Append(Language.getMessage(mstrModuleName, 7, "has been Unlocked "))
                        'Gajanan [27-Mar-2019] -- Start
                        'Enhancement - Change Email Language
                        'strMessage.Append("<BR><BR><BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B>&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</B>")
                        strMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                        'Gajanan [27-Mar-2019] -- End
                        strMessage.Append("</BODY></HTML>")

                        objMail._Message = strMessage.ToString
                        objMail._ToEmail = objUser._Email

                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        If mstrFormName.Trim.Length > 0 Then
                            objMail._Form_Name = mstrFormName
                        End If
                        objMail._LogEmployeeUnkid = iLoginEmployeeId
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = Convert.ToInt32(strNofificationID)
                        Dim objUsr As New clsUserAddEdit
                        objUsr._Userunkid = Convert.ToInt32(strNofificationID)
                        objMail._SenderAddress = IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT

                        'Hemant (02 Feb 2022) -- Start            
                        'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                        'gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, "", "", Convert.ToInt32(strNofificationID), iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                        If HttpContext.Current Is Nothing Then
                        gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, "", "", Convert.ToInt32(strNofificationID), iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                        Else
                            lstWebEmail.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, "", "", Convert.ToInt32(strNofificationID), iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                        End If
                        'Hemant (02 Feb 2022) -- End
                        objUsr = Nothing
                    Else
                        Continue For
                    End If


                Next
            End If
            'Hemant (23 Nov 2018) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SendMailToUserForFinalSaved", mstrModuleName)
        Finally
            objUser = Nothing
            objMail = Nothing
        End Try
    End Sub

    Public Shared Sub SetControlCaptions()
        Try
            'List page controls
            Language.setLanguage("frmEmpAssetDeclarationListT2")
            Language._Object.setCaption("lblEmployee", "Employee")

            'Add Edit page controls
            Language.setLanguage("frmEmpAssetDeclarationT2")
            Language._Object.setCaption("lblCenterforWork", "Functions")
            Language._Object.setCaption("lblWorkStation", "Work Station")

        Catch ex As Exception

        End Try
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Public Function IsTableDataUpdate(ByVal unkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If


            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'strQ = "select TOP 1 * from athrassetdeclarationT2_master where assetdeclarationt2unkid = @assetdeclarationt2unkid and audittype = 2 ORDER BY auditdatetime DESC"
            strQ = "select TOP 1 * from athrassetdeclarationT2_master where assetdeclarationt2unkid = @assetdeclarationt2unkid and audittype in (1, 2) ORDER BY auditdatetime DESC"
            'Hemant (07 Feb 2019) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dr As DataRow In dsList.Tables(0).Rows

                'Hemant (07 Feb 2019) -- Start
                'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
                'If dr("employeeunkid").ToString() = mintEmployeeunkid AndAlso dr("tin_no").ToString() = mstrTin_No _
                '                      AndAlso CDate(dr("finyear_start")) = mdtFinyear_Start AndAlso CDate(dr("finyear_end")) = mdtFinyear_End _
                '                      AndAlso dr("isfinalsaved").ToString() = mblnIsfinalsaved AndAlso CDate(dr("transactiondate")) = mdtTransactiondate _
                '                      AndAlso CDate(dr("savedate")) = mdtSavedate AndAlso CDate(dr("finalsavedate")) = mdtFinalsavedate _
                '                      AndAlso dr("unlockfinalsavedate").ToString() = mdtUnlockfinalsavedate AndAlso dr("isacknowledged").ToString() = mblnIsAcknowledged Then

                If dr("employeeunkid").ToString() = mintEmployeeunkid AndAlso dr("tin_no").ToString() = mstrTin_No _
                       AndAlso CDate(dr("finyear_start")) = mdtFinyear_Start AndAlso CDate(dr("finyear_end")) = mdtFinyear_End _
                       AndAlso dr("isfinalsaved").ToString() = mblnIsfinalsaved AndAlso CDate(dr("transactiondate")) = mdtTransactiondate _
                  AndAlso ((IsDBNull(dr.Item("finalsavedate")) = True AndAlso mdtFinalsavedate = Nothing) OrElse (IsDBNull(dr.Item("finalsavedate")) = False AndAlso eZeeDate.convertDate(dr("finalsavedate")) = eZeeDate.convertDate(mdtFinalsavedate))) _
                  AndAlso ((IsDBNull(dr.Item("unlockfinalsavedate")) = True AndAlso mdtUnlockfinalsavedate = Nothing) OrElse (IsDBNull(dr.Item("unlockfinalsavedate")) = False AndAlso eZeeDate.convertDate(dr("unlockfinalsavedate")) = eZeeDate.convertDate(mdtUnlockfinalsavedate))) _
                  AndAlso dr("isacknowledged").ToString() = mblnIsAcknowledged Then
                    'Hemant (07 Feb 2019) -- End
                    Return True
                Else
                    Return False
                End If
            Next
            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee Declaration Notification")
			Language.setMessage(mstrModuleName, 2, "Dear")
			Language.setMessage(mstrModuleName, 3, "Thank you.")
			Language.setMessage(mstrModuleName, 4, "This is to notify you that the declaration for")
			Language.setMessage(mstrModuleName, 5, "Thank you, Compliance")
			Language.setMessage(mstrModuleName, 6, "has been submitted.")
			Language.setMessage(mstrModuleName, 7, "has been Unlocked")
			Language.setMessage(mstrModuleName, 8, "This is to notify that your declaration has been submitted successfully,")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class