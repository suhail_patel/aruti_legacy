﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language
Imports eZee.Common.eZeeForm 'S.SANDEEP [ 28 JAN 2014 ] -- START -- END
Imports Microsoft.Exchange.WebServices.Data 'Sohail (30 Nov 2017)
Imports System.Web 'Sohail (26 Feb 2018)
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.IO
'Hemant (01 Feb 2022) -- Start
'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
Imports System.Security.Authentication
'Hemant (01 Feb 2022) -- End

#End Region

Public Class clsSendMail

#Region " Private Variables "
    Private Shared ReadOnly mstrModuleName As String = "clsSendMail"
    Private mstrToEmail As String = ""
    Private mstrMessage As String = ""
    Private mstrSubject As String = ""
    Private mstrAttachedFiles As String = ""
    'S.SANDEEP [26 JUL 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    Private mstrCCAddress As String = ""
    Private mstrBCCAddress As String = ""
    'S.SANDEEP [26 JUL 2016] -- END

    'S.SANDEEP [ 28 JAN 2014 ] -- START
    Private mstrFormName As String = String.Empty
    Private mintLogEmployeeUnkid As Integer = -1
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private mintUserUnkid As Integer = 0
    Private mintOperationModeId As Integer = 0
    Private mintModuleRefId As Integer = 0
    Private mstrSenderAddress As String = String.Empty

    'S.SANDEEP [05-Apr-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
    Private Content As Dictionary(Of String, Byte())
    'S.SANDEEP [05-Apr-2018] -- END

    Public Enum enAT_VIEW_TYPE
        ASSESSMENT_MGT = 1
        LEAVE_MGT = 2
        EMPLOYEE_MGT = 3
        PAYROLL_MGT = 4
        RECRUITMENT_MGT = 5
        'Pinkal (13-Jul-2015) -- Start
        'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
        CLAIMREQUEST_MGT = 6
        'Pinkal (13-Jul-2015) -- End

        'S.SANDEEP [24 MAY 2016] -- Start
        'Email Notification
        DISCIPLINE_MGT = 7
        'S.SANDEEP [24 MAY 2016] -- End

        'Shani (21-Jul-2016) -- Start
        'Enhancement - Create New Loan Notification 
        LOAN_MGT = 8
        'Shani (21-Jul-2016) -- End

        'Gajanan [13-AUG-2018] -- Start
        'Enhancement - Implementing Grievance Module.
        GRIEVANCE_MGT = 9
        'Gajanan(13-AUG-2018) -- End

        'S.SANDEEP [09-OCT-2018] -- START
        TRAININGREQUISITION_MGT = 10
        'S.SANDEEP [09-OCT-2018] -- END


        'Gajanan [19-OCT-2019] -- Start    
        'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
        Configuration = 11
        'Gajanan [19-OCT-2019] -- End

        'Pinkal (11-Sep-2019) -- Start
        'Enhancement NMB - Working On Claim Retirement for NMB.
        CLAIM_RETIREMENT_MGT = 12
        'Pinkal (11-Sep-2019) -- End


        'Pinkal (07-Nov-2019) -- Start
        'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
        OTREQUISITION_MGT = 13
        'Pinkal (07-Nov-2019) -- End


        'Pinkal (27-Oct-2020) -- Start
        'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.
        TNA_MGT = 14
        'Pinkal (27-Oct-2020) -- End

        'Hemant (03 Dec 2020) -- Start
        'Enhancement : Email Notifications for Talent Pipeline
        TALENT_MGT = 15
        'Hemant (03 Dec 2020) -- End

        'Hemant (15 Dec 2020) -- Start
        SUCCESSION_MGT = 16
        'Hemant (15 Dec 2020) -- End

        PDP_MGT = 17

    End Enum
    'S.SANDEEP [ 28 JAN 2014 ] -- END

    'Hemant (01 Feb 2022) -- Start
    'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.  
    Public Const _Tls12 As SslProtocols = CType(&HC00, SslProtocols)
    Public Const Tls12 As System.Net.SecurityProtocolType = CType(_Tls12, System.Net.SecurityProtocolType)
    'Hemant (01 Feb 2022) -- End


#End Region

#Region " Properties "
    Public Property _ToEmail() As String
        Get
            Return mstrToEmail
        End Get
        Set(ByVal value As String)
            mstrToEmail = value
        End Set
    End Property

    Public Property _Message() As String
        Get
            Return mstrMessage
        End Get
        Set(ByVal value As String)
            mstrMessage = value
        End Set
    End Property

    Public Property _Subject() As String
        Get
            Return mstrSubject
        End Get
        Set(ByVal value As String)
            mstrSubject = value
        End Set
    End Property

    Public Property _AttachedFiles() As String
        Get
            Return mstrAttachedFiles
        End Get
        Set(ByVal value As String)
            mstrAttachedFiles = value
        End Set
    End Property

    'S.SANDEEP [26 JUL 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    Public Property _CCAddress() As String
        Get
            Return mstrCCAddress
        End Get
        Set(ByVal value As String)
            mstrCCAddress = value
        End Set
    End Property

    Public Property _BCCAddress() As String
        Get
            Return mstrBCCAddress
        End Get
        Set(ByVal value As String)
            mstrBCCAddress = value
        End Set
    End Property
    'S.SANDEEP [26 JUL 2016] -- END


    'S.SANDEEP [ 28 JAN 2014 ] -- START
    Public WriteOnly Property _Form_Name() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _LogEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkid() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property

    Public WriteOnly Property _OperationModeId() As Integer
        Set(ByVal value As Integer)
            mintOperationModeId = value
        End Set
    End Property

    Public WriteOnly Property _ModuleRefId() As Integer
        Set(ByVal value As Integer)
            mintModuleRefId = value
        End Set
    End Property

    Public WriteOnly Property _SenderAddress() As String
        Set(ByVal value As String)
            mstrSenderAddress = value
        End Set
    End Property
    'S.SANDEEP [ 28 JAN 2014 ] -- END

    'S.SANDEEP [05-Apr-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
    Public WriteOnly Property _ImageContents() As Dictionary(Of String, Byte())
        Set(ByVal value As Dictionary(Of String, Byte()))
            Content = value
        End Set
    End Property
    'S.SANDEEP [05-Apr-2018] -- END

#End Region

#Region " Private Function "

    'Sandeep [ 25 APRIL 2011 ] -- Start
    'Public Function SendMail() As String

    'S.SANDEEP [21-SEP-2018] -- START
    'ISSUE/ENHANCEMENT : {#0002552}
    'Public Function SendMail(ByVal intCompanyUnkId As Integer, Optional ByVal blnIsPaySlip As Boolean = False, Optional ByVal StrPath As String = "") As String
    '    'Sandeep [ 25 APRIL 2011 ] -- End 
    '    'Sohail (30 Nov 2017) -- Start
    '    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '    'Dim objCompany As New clsCompany_Master 'Sohail (26 Feb 2018) 
    '    'Sohail (13 Dec 2017) -- Start
    '    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '    'objCompany._FromSendEmail = True 'Sohail (26 Feb 2018) 
    '    'Sohail (13 Dec 2017) -- End
    '    'objCompany._Companyunkid = intCompanyUnkId 'Sohail (26 Feb 2018) 
    '    'Sohail (30 Nov 2017) -- End

    '    'Sohail (26 Feb 2018) -- Start
    '    'Issue : There is already an open datareader associated with this command which must be closed first. in 70.1.
    '    Dim strSenderAddress As String = String.Empty
    '    Dim intEmail_Type As Integer = 0
    '    Dim strMailserverip As String = String.Empty
    '    Dim intMailserverport As Integer = 0
    '    Dim strUsername As String = String.Empty
    '    Dim strPassword As String = String.Empty
    '    Dim blnIsloginssl As Boolean = False
    '    Dim blnIsCertificateAuthorization As Boolean = False
    '    Dim strEWS_Domain As String = String.Empty
    '    Dim strEWS_URL As String = String.Empty
    '    'Sohail (26 Feb 2018) -- End

    '    'S.SANDEEP [21-SEP-2018] -- START
    '    'ISSUE/ENHANCEMENT : {#0002552}
    '    Dim blnByPassProxy As Boolean = False
    '    'S.SANDEEP [21-SEP-2018] -- END

    '    Try
    '        If mstrToEmail <> "" Then

    '            'Sohail (26 Feb 2018) -- Start
    '            'Issue : There is already an open datareader associated with this command which must be closed first. in 70.1.
    '            If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Session IsNot Nothing Then
    '                strSenderAddress = HttpContext.Current.Session("Senderaddress").ToString
    '                intEmail_Type = CInt(HttpContext.Current.Session("Email_Type"))
    '                strMailserverip = HttpContext.Current.Session("Mailserverip").ToString
    '                intMailserverport = CInt(HttpContext.Current.Session("Mailserverport"))
    '                strUsername = HttpContext.Current.Session("SenderUsername").ToString
    '                strPassword = HttpContext.Current.Session("SenderUserPassword").ToString
    '                blnIsloginssl = CBool(HttpContext.Current.Session("Isloginssl"))
    '                blnIsCertificateAuthorization = CBool(HttpContext.Current.Session("IsCertificateAuthorization"))
    '                strEWS_Domain = HttpContext.Current.Session("EWS_Domain").ToString
    '                strEWS_URL = HttpContext.Current.Session("EWS_URL").ToString
    '                'S.SANDEEP [21-SEP-2018] -- START
    '                'ISSUE/ENHANCEMENT : {#0002552}
    '                blnByPassProxy = CBool(HttpContext.Current.Session("ByPassProxy"))
    '                'S.SANDEEP [21-SEP-2018] -- END
    '            Else
    '                strSenderAddress = Company._Object._Senderaddress
    '                intEmail_Type = Company._Object._Email_Type
    '                strMailserverip = Company._Object._Mailserverip
    '                intMailserverport = Company._Object._Mailserverport
    '                strUsername = Company._Object._Username
    '                strPassword = Company._Object._Password
    '                blnIsloginssl = Company._Object._Isloginssl
    '                blnIsCertificateAuthorization = Company._Object._IsCertificateAuthorization
    '                strEWS_Domain = Company._Object._EWS_Domain
    '                strEWS_URL = Company._Object._EWS_URL
    '                'S.SANDEEP [21-SEP-2018] -- START
    '                'ISSUE/ENHANCEMENT : {#0002552}
    '                blnByPassProxy = Company._Object._Isbypassproxy
    '                'S.SANDEEP [21-SEP-2018] -- END
    '            End If
    '            'Sohail (26 Feb 2018) -- End

    '            'Sohail (30 Nov 2017) -- Start
    '            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '            'Sohail (26 Feb 2018) -- Start
    '            'Issue : There is already an open datareader associated with this command which must be closed first. in 70.1.
    '            'If objCompany._Email_Type = 0 Then '0 = SMTP, 1 = EWS
    '            If intEmail_Type = 0 Then '0 = SMTP, 1 = EWS
    '                'Sohail (26 Feb 2018) -- End
    '                'Sohail (30 Nov 2017) -- End

    '                'Sohail (30 Nov 2017) -- Start
    '                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '                'Dim objMail As New System.Net.Mail.MailMessage(Company._Object._Senderaddress, mstrToEmail)
    '                'Sohail (26 Feb 2018) -- Start
    '                'Issue : There is already an open datareader associated with this command which must be closed first. in 70.1.
    '                'Dim objMail As New System.Net.Mail.MailMessage(objCompany._Senderaddress, mstrToEmail)
    '                Dim objMail As New System.Net.Mail.MailMessage(strSenderAddress, mstrToEmail)
    '                'Sohail (26 Feb 2018) -- End
    '                'Sohail (30 Nov 2017) -- End


    '                'S.SANDEEP [05-Apr-2018] -- START
    '                'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
    '                If mstrMessage.Contains("<img ") AndAlso Content IsNot Nothing Then
    '                    Dim lscr As List(Of LinkedResource) = New List(Of LinkedResource)()
    '                    For Each iKey As String In Content.Keys
    '                        Dim fl As New IO.FileInfo(iKey)
    '                        Dim contentPath = Path.GetDirectoryName(iKey)
    '                        If mstrMessage.Contains("src=""" & contentPath & "/" & fl.Name & """") Then
    '                            Dim inline As New LinkedResource(contentPath & "/" & fl.Name, MediaTypeNames.Image.Jpeg)
    '                            inline.ContentId = Guid.NewGuid().ToString()
    '                            mstrMessage = mstrMessage.Replace("src=""" & contentPath & "/" & fl.Name & """", "src=""cid:" & inline.ContentId & """")
    '                            lscr.Add(inline)
    '                            ' avHtml.LinkedResources.Add(inline)
    '                        End If
    '                    Next
    '                    Dim avHtml As AlternateView = AlternateView.CreateAlternateViewFromString(mstrMessage, Nothing, MediaTypeNames.Text.Html)
    '                    For i As Integer = 0 To lscr.Count - 1
    '                        avHtml.LinkedResources.Add(lscr(i))
    '                    Next
    '                    objMail.AlternateViews.Add(avHtml)
    '                End If
    '                'S.SANDEEP [05-Apr-2018] -- END

    '                objMail.Body = mstrMessage
    '                objMail.Subject = mstrSubject
    '                'S.SANDEEP [26 JUL 2016] -- START
    '                'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    '                If mstrCCAddress.Trim.Length > 0 Then
    '                    objMail.CC.Add(mstrCCAddress)
    '                End If

    '                If mstrBCCAddress.Trim.Length > 0 Then
    '                    objMail.Bcc.Add(mstrBCCAddress)
    '                End If
    '                'S.SANDEEP [26 JUL 2016] -- END

    '                If mstrAttachedFiles <> "" Then
    '                    Dim strAtt() As String
    '                    strAtt = mstrAttachedFiles.Split(",")
    '                    If strAtt.Length > 0 Then
    '                        For i As Integer = 0 To strAtt.Length - 1
    '                            'If IO.File.Exists(gobjParam._ExportReportPath & "\" & strAtt(i)) Then objMail.Attachments.Add(New System.Net.Mail.Attachment(gobjParam._ExportReportPath & "\" & strAtt(i)))

    '                            'Sandeep [ 25 APRIL 2011 ] -- Start
    '                            'If IO.File.Exists(ConfigParameter._Object._ExportReportPath & "\" & strAtt(i)) Then objMail.Attachments.Add(New System.Net.Mail.Attachment(ConfigParameter._Object._ExportReportPath & "\" & strAtt(i)))
    '                            If blnIsPaySlip = True And StrPath.Length > 0 Then
    '                                If IO.File.Exists(StrPath & "\" & strAtt(i)) Then objMail.Attachments.Add(New System.Net.Mail.Attachment(StrPath & "\" & strAtt(i)))
    '                            Else
    '                                'S.SANDEEP [ 18 FEB 2012 ] -- START
    '                                'ENHANCEMENT : TRA CHANGES
    '                                'If IO.File.Exists(ConfigParameter._Object._ExportReportPath & "\" & strAtt(i)) Then objMail.Attachments.Add(New System.Net.Mail.Attachment(ConfigParameter._Object._ExportReportPath & "\" & strAtt(i)))

    '                                'S.SANDEEP [26 JUL 2016] -- START
    '                                'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    '                                'If IO.File.Exists(strAtt(i)) Then objMail.Attachments.Add(New System.Net.Mail.Attachment(strAtt(i)))
    '                                If strAtt(i).ToString.StartsWith("http://") = False Then
    '                                    If IO.File.Exists(strAtt(i)) Then objMail.Attachments.Add(New System.Net.Mail.Attachment(strAtt(i)))
    '                                Else
    '                                    Dim imgData As Byte() = Nothing
    '                                    Dim strWritePath As String = System.IO.Path.GetTempPath()
    '                                    Dim filename As String = ""
    '                                    'S.SANDEEP [25 JUL 2016] -- START
    '                                    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    '                                    filename = System.IO.Path.GetFileName(strAtt(i).ToString)
    '                                    Dim FolderName As String = strAtt(i).ToString.Split("/")(strAtt(i).ToString.Split("/").Length - 2)
    '                                    Dim strError As String = ""
    '                                    imgData = clsFileUploadDownload.DownloadFile(strAtt(i).ToString, filename, FolderName, strError, strAtt(i).ToString.Substring(0, strAtt(i).ToString.IndexOf("/uploadimage") + 1))
    '                                    'S.SANDEEP [25 JUL 2016] -- END
    '                                    'S.SANDEEP [25 JUL 2016] -- START
    '                                    'Using client As New Net.WebClient()
    '                                    '    imgData = client.DownloadData(strAtt(i).ToString)
    '                                    '    System.IO.File.WriteAllBytes(strWritePath & filename, imgData)
    '                                    'End Using
    '                                    If imgData IsNot Nothing Then
    '                                        System.IO.File.WriteAllBytes(strWritePath & filename, imgData)
    '                                        strWritePath = strWritePath & filename
    '                                        objMail.Attachments.Add(New System.Net.Mail.Attachment(strWritePath))
    '                                    End If
    '                                End If
    '                                'S.SANDEEP [25 JUL 2016] -- END

    '                                'S.SANDEEP [ 18 FEB 2012 ] -- END
    '                            End If
    '                            'Sandeep [ 25 APRIL 2011 ] -- End 
    '                        Next
    '                    End If
    '                End If
    '                objMail.DeliveryNotificationOptions = Net.Mail.DeliveryNotificationOptions.OnSuccess Or Net.Mail.DeliveryNotificationOptions.OnFailure
    '                'Sohail (30 Nov 2017) -- Start
    '                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '                'objMail.Headers.Add("Disposition-Notification-To", Company._Object._Senderaddress)
    '                'objMail.Headers.Add("Return-Receipt-To", Company._Object._Senderaddress)
    '                'Sohail (26 Feb 2018) -- Start
    '                'Issue : There is already an open datareader associated with this command which must be closed first. in 70.1.
    '                'objMail.Headers.Add("Disposition-Notification-To", objCompany._Senderaddress)
    '                'objMail.Headers.Add("Return-Receipt-To", objCompany._Senderaddress)
    '                objMail.Headers.Add("Disposition-Notification-To", strSenderAddress)
    '                objMail.Headers.Add("Return-Receipt-To", strSenderAddress)
    '                'Sohail (26 Feb 2018) -- End
    '                'Sohail (30 Nov 2017) -- End
    '                objMail.IsBodyHtml = True
    '                Dim SmtpMail As New System.Net.Mail.SmtpClient()
    '                'Sohail (30 Nov 2017) -- Start
    '                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '                'SmtpMail.Host = Company._Object._Mailserverip  'gobjParam._MailServer
    '                'SmtpMail.Port = Company._Object._Mailserverport   'gobjParam._MailPort
    '                'If Company._Object._Username <> "" Then SmtpMail.Credentials = New System.Net.NetworkCredential(Company._Object._Username, Company._Object._Password)
    '                'SmtpMail.EnableSsl = Company._Object._Isloginssl         'gobjParam._IsEnableSSL
    '                'Sohail (26 Feb 2018) -- Start
    '                'Issue : There is already an open datareader associated with this command which must be closed first. in 70.1.
    '                'SmtpMail.Host = objCompany._Mailserverip  'gobjParam._MailServer
    '                'SmtpMail.Port = objCompany._Mailserverport   'gobjParam._MailPort
    '                'If objCompany._Username <> "" Then SmtpMail.Credentials = New System.Net.NetworkCredential(objCompany._Username, objCompany._Password)
    '                'SmtpMail.EnableSsl = objCompany._Isloginssl
    '                SmtpMail.Host = strMailserverip  'gobjParam._MailServer
    '                SmtpMail.Port = intMailserverport   'gobjParam._MailPort
    '                If strUsername <> "" Then SmtpMail.Credentials = New System.Net.NetworkCredential(strUsername, strPassword)
    '                SmtpMail.EnableSsl = blnIsloginssl
    '                'Sohail (26 Feb 2018) -- End
    '                'Sohail (30 Nov 2017) -- End

    '                'S.SANDEEP [11-AUG-2017] -- START
    '                'ISSUE/ENHANCEMENT : CERTIFICATE AUTHENTICATION IN EMAIL SENDING
    '                'Sohail (30 Nov 2017) -- Start
    '                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '                'If Company._Object._IsCertificateAuthorization Then
    '                'Sohail (26 Feb 2018) -- Start
    '                'Issue : There is already an open datareader associated with this command which must be closed first. in 70.1.
    '                'If objCompany._IsCertificateAuthorization Then
    '                If blnIsCertificateAuthorization Then
    '                    'Sohail (26 Feb 2018) -- End
    '                    'Sohail (30 Nov 2017) -- End
    '                    System.Net.ServicePointManager.ServerCertificateValidationCallback = Function(s As Object, certificate As System.Security.Cryptography.X509Certificates.X509Certificate, chain As System.Security.Cryptography.X509Certificates.X509Chain, sslPolicyErrors As System.Net.Security.SslPolicyErrors) True
    '                End If
    '                'S.SANDEEP [11-AUG-2017] -- END




    '                SmtpMail.Send(objMail)
    '                'S.SANDEEP [ 28 JAN 2014 ] -- START

    '                'Sohail (30 Nov 2017) -- Start
    '                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '                'Sohail (26 Feb 2018) -- Start
    '                'Issue : There is already an open datareader associated with this command which must be closed first. in 70.1.
    '                'ElseIf objCompany._Email_Type = 1 Then '0 = SMTP, 1 = EWS

    '                'S.SANDEEP [05-Apr-2018] -- START
    '                'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
    '                objMail.Dispose()
    '                'S.SANDEEP [05-Apr-2018] -- END


    '            ElseIf intEmail_Type = 1 Then '0 = SMTP, 1 = EWS
    '                'Sohail (26 Feb 2018) -- End

    '                'Creating the ExchangeService Object 
    '                Dim objService As New ExchangeService()

    '                'Seting Credentials to be used
    '                'Sohail (26 Feb 2018) -- Start
    '                'Issue : There is already an open datareader associated with this command which must be closed first. in 70.1.
    '                'objService.Credentials = New WebCredentials(objCompany._Username, objCompany._Password, objCompany._EWS_Domain)
    '                objService.Credentials = New WebCredentials(strUsername, strPassword, strEWS_Domain)
    '                'Sohail (26 Feb 2018) -- End

    '                'Setting the EWS Uri
    '                'Sohail (26 Feb 2018) -- Start
    '                'Issue : There is already an open datareader associated with this command which must be closed first. in 70.1.
    '                'objService.Url = New Uri(objCompany._EWS_URL)
    '                objService.Url = New Uri(strEWS_URL)
    '                'Sohail (26 Feb 2018) -- End

    '                'Creating an Email Service and passing in the service
    '                Dim objMessage As New EmailMessage(objService)

    '                'Setting Email message properties
    '                objMessage.Subject = mstrSubject
    '                objMessage.Body = mstrMessage
    '                objMessage.ToRecipients.Add(mstrToEmail)
    '                If mstrCCAddress.Trim.Length > 0 Then
    '                    objMessage.CcRecipients.Add(mstrCCAddress)
    '                End If
    '                If mstrBCCAddress.Trim.Length > 0 Then
    '                    objMessage.BccRecipients.Add(mstrBCCAddress)
    '                End If


    '                'S.SANDEEP [05-Apr-2018] -- START
    '                'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
    '                If mstrMessage.Contains("<img ") AndAlso Content IsNot Nothing Then
    '                    For Each iKey As String In Content.Keys
    '                        Dim fl As New IO.FileInfo(iKey)
    '                        Dim contentPath = Path.GetDirectoryName(iKey)
    '                        If mstrMessage.Contains("src=""" & contentPath & "/" & fl.Name & """") Then
    '                            objMessage.Attachments.AddFileAttachment(fl.Name, iKey)
    '                            objMessage.Attachments(0).IsInline = True
    '                            objMessage.Attachments(0).ContentId = fl.Name
    '                        End If
    '                    Next
    '                End If
    '                'S.SANDEEP [05-Apr-2018] -- END

    '                If mstrAttachedFiles <> "" Then
    '                    Dim strAtt() As String
    '                    strAtt = mstrAttachedFiles.Split(",")
    '                    If strAtt.Length > 0 Then
    '                        For i As Integer = 0 To strAtt.Length - 1

    '                            If blnIsPaySlip = True And StrPath.Length > 0 Then
    '                                If IO.File.Exists(StrPath & "\" & strAtt(i)) Then objMessage.Attachments.AddFileAttachment(StrPath & "\" & strAtt(i))
    '                            Else
    '                                If strAtt(i).ToString.StartsWith("http://") = False Then
    '                                    If IO.File.Exists(strAtt(i)) Then objMessage.Attachments.AddFileAttachment(strAtt(i))
    '                                Else
    '                                    Dim imgData As Byte() = Nothing
    '                                    Dim strWritePath As String = System.IO.Path.GetTempPath()
    '                                    Dim filename As String = ""

    '                                    filename = System.IO.Path.GetFileName(strAtt(i).ToString)
    '                                    Dim FolderName As String = strAtt(i).ToString.Split("/")(strAtt(i).ToString.Split("/").Length - 2)
    '                                    Dim strError As String = ""
    '                                    imgData = clsFileUploadDownload.DownloadFile(strAtt(i).ToString, filename, FolderName, strError, strAtt(i).ToString.Substring(0, strAtt(i).ToString.IndexOf("/uploadimage") + 1))

    '                                    If imgData IsNot Nothing Then
    '                                        System.IO.File.WriteAllBytes(strWritePath & filename, imgData)
    '                                        strWritePath = strWritePath & filename
    '                                        objMessage.Attachments.AddFileAttachment(strWritePath)
    '                                    End If
    '                                End If

    '                            End If
    '                        Next
    '                    End If
    '                End If

    '                'Sending the email and saving a copy.
    '                'objMessage.Send()
    '                objMessage.SendAndSaveCopy()

    '            End If
    '            'Sohail (30 Nov 2017) -- End

    '            If mintModuleRefId > 0 Then
    '                'Sohail (30 Nov 2017) -- Start
    '                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '                'Call Insert_Email_Tran_Log()
    '                'Sohail (26 Feb 2018) -- Start
    '                'Issue : There is already an open datareader associated with this command which must be closed first. in 70.1.
    '                'Call Insert_Email_Tran_Log(objCompany._Senderaddress)
    '                Call Insert_Email_Tran_Log(strSenderAddress)
    '                'Sohail (26 Feb 2018) -- End
    '                'Sohail (30 Nov 2017) -- End
    '            End If



    '            'S.SANDEEP [ 28 JAN 2014 ] -- END
    '            Return ""
    '        Else
    '            Return Language.getMessage(mstrModuleName, 1, "Recipient email address should not be blank.")
    '        End If
    '    Catch ex As Exception
    '        'S.SANDEEP [ 28 JAN 2014 ] -- START

    '        'S.SANDEEP [ 18 JUL 2014 ] -- START
    '        'Call Insert_Email_Tran_Log(ex.Message)
    '        If mintModuleRefId > 0 Then
    '            'Sohail (30 Nov 2017) -- Start
    '            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '            'Call Insert_Email_Tran_Log(ex.Message)
    '            'Sohail (26 Feb 2018) -- Start
    '            'Issue : There is already an open datareader associated with this command which must be closed first. in 70.1.
    '            'Call Insert_Email_Tran_Log(objCompany._Senderaddress, ex.Message)
    '            Call Insert_Email_Tran_Log(strSenderAddress, ex.Message)
    '            'Sohail (26 Feb 2018) -- End
    '            'Sohail (30 Nov 2017) -- End
    '        End If
    '        'S.SANDEEP [ 18 JUL 2014 ] -- END


    '        'S.SANDEEP [ 28 JAN 2014 ] -- END
    '        Return ex.Message
    '    Finally

    '    End Try
    'End Function

    Public Function SendMail(ByVal intCompanyUnkId As Integer, Optional ByVal blnIsPaySlip As Boolean = False, Optional ByVal StrPath As String = "") As String
        Dim strSenderAddress As String = String.Empty
        Dim intEmail_Type As Integer = 0
        Dim strMailserverip As String = String.Empty
        Dim intMailserverport As Integer = 0
        Dim strUsername As String = String.Empty
        Dim strPassword As String = String.Empty
        Dim blnIsloginssl As Boolean = False
        Dim blnIsCertificateAuthorization As Boolean = False
        Dim strEWS_Domain As String = String.Empty
        Dim strEWS_URL As String = String.Empty
        Dim blnByPassProxy As Boolean = False
        'Sohail (18 Jul 2019) -- Start
        'Internal Enhancement - Ref #  - 76.1 - Show Sender Name instead of sender email in mail box.
        Dim strSenderName As String = String.Empty
        'Sohail (18 Jul 2019) -- End
        'Hemant (01 Feb 2022) -- Start
        'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
        Dim intProtocolId As Integer = 0
        'Hemant (01 Feb 2022) -- End
        Try
            'S.SANDEEP |05-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            Dim objConfig As New clsConfigOptions
            Dim blnValue As Boolean = True

            'Pinkal (13-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'blnValue = objConfig.GetKeyValue(intCompanyUnkId, "ShowArutisignature")
            Dim mstrArutisignature As String = ""
            mstrArutisignature = objConfig.GetKeyValue(intCompanyUnkId, "ShowArutisignature")
            blnValue = CBool(IIf(mstrArutisignature.ToString().Length > 0, mstrArutisignature.ToString(), True))
            'Pinkal (13-Mar-2019) -- End


            If blnValue = False Then
                If mstrMessage.Trim.Length > 0 Then
                    mstrMessage = mstrMessage.Replace("""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""", "")
                    mstrMessage = mstrMessage.Replace("&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;", "")
		    mstrMessage = mstrMessage.Replace("&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;", "")
                    mstrMessage = mstrMessage.Replace("""POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.""", "")
                    mstrMessage = mstrMessage.Replace("&quot;POWERED BY ARUTI HR &; PAYROLL MANAGEMENT SOFTWARE.&quot;", "") 'Hemant (08 June 2019)
                    mstrMessage = mstrMessage.Replace("'POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.'", "")  'Hemant (06 Aug 2019)
                End If
            End If
            objConfig = Nothing
            'S.SANDEEP |05-MAR-2019| -- END

            If mstrToEmail <> "" Then

                If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Session IsNot Nothing Then
                    strSenderAddress = HttpContext.Current.Session("Senderaddress").ToString
                    intEmail_Type = CInt(HttpContext.Current.Session("Email_Type"))
                    strMailserverip = HttpContext.Current.Session("Mailserverip").ToString
                    intMailserverport = CInt(HttpContext.Current.Session("Mailserverport"))
                    strUsername = HttpContext.Current.Session("SenderUsername").ToString
                    strPassword = HttpContext.Current.Session("SenderUserPassword").ToString
                    blnIsloginssl = CBool(HttpContext.Current.Session("Isloginssl"))
                    blnIsCertificateAuthorization = CBool(HttpContext.Current.Session("IsCertificateAuthorization"))
                    strEWS_Domain = HttpContext.Current.Session("EWS_Domain").ToString
                    strEWS_URL = HttpContext.Current.Session("EWS_URL").ToString
                    blnByPassProxy = CBool(HttpContext.Current.Session("ByPassProxy"))
                    'Sohail (18 Jul 2019) -- Start
                    'Internal Enhancement - Ref #  - 76.1 - Show Sender Name instead of sender email in mail box.
                    strSenderName = HttpContext.Current.Session("SenderName").ToString
                    'Sohail (18 Jul 2019) -- End
                    'Hemant (01 Feb 2022) -- Start
                    'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
                    intProtocolId = CInt(HttpContext.Current.Session("protocolunkid"))
                    'Hemant (01 Feb 2022) -- End

                Else
                    strSenderAddress = Company._Object._Senderaddress
                    intEmail_Type = Company._Object._Email_Type
                    strMailserverip = Company._Object._Mailserverip
                    intMailserverport = Company._Object._Mailserverport
                    strUsername = Company._Object._Username
                    strPassword = Company._Object._Password
                    blnIsloginssl = Company._Object._Isloginssl
                    blnIsCertificateAuthorization = Company._Object._IsCertificateAuthorization
                    strEWS_Domain = Company._Object._EWS_Domain
                    strEWS_URL = Company._Object._EWS_URL
                    blnByPassProxy = Company._Object._Isbypassproxy
                    'Sohail (18 Jul 2019) -- Start
                    'Internal Enhancement - Ref #  - 76.1 - Show Sender Name instead of sender email in mail box.
                    strSenderName = Company._Object._Sendername
                    'Sohail (18 Jul 2019) -- End
                    'Hemant (01 Feb 2022) -- Start
                    'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
                    intProtocolId = Company._Object._Protocolunkid
                    'Hemant (01 Feb 2022) -- End
                End If
                If intEmail_Type = 0 Then '0 = SMTP, 1 = EWS
                    'Sohail (18 Jul 2019) -- Start
                    'Internal Enhancement - Ref #  - 76.1 - Show Sender Name instead of sender email in mail box.
                    'Dim objMail As New System.Net.Mail.MailMessage(strSenderAddress, mstrToEmail)
                    Dim strSenderDispName As String = strSenderName
                    If strSenderDispName.Trim.Length <= 0 Then
                        strSenderDispName = strSenderAddress
                    End If
                    Dim objMail As New System.Net.Mail.MailMessage(New MailAddress(strSenderAddress, strSenderDispName), New MailAddress(mstrToEmail))
                    'Sohail (18 Jul 2019) -- End
                    If mstrMessage.Contains("<img ") AndAlso Content IsNot Nothing Then
                        Dim lscr As List(Of LinkedResource) = New List(Of LinkedResource)()
                        For Each iKey As String In Content.Keys
                            Dim fl As New IO.FileInfo(iKey)
                            'S.SANDEEP |14-FEB-2019| -- START
                            'ISSUE/ENHANCEMENT : IMAGE NOT DISPLAYED
                            Dim iPath As String = System.IO.Path.GetTempPath()
                            iPath = iPath & "\" & fl.Directory.Name
                            'S.SANDEEP |14-FEB-2019| -- END
                            Dim contentPath = Path.GetDirectoryName(iKey)
                            If mstrMessage.Contains("src=""" & contentPath & "/" & fl.Name & """") Then
                                'S.SANDEEP |14-FEB-2019| -- START
                                'ISSUE/ENHANCEMENT : IMAGE NOT DISPLAYED
                                'Dim inline As New LinkedResource(contentPath & "/" & fl.Name, MediaTypeNames.Image.Jpeg)
                                Dim inline As New LinkedResource(iPath & "/" & fl.Name, MediaTypeNames.Image.Jpeg)
                                'S.SANDEEP |14-FEB-2019| -- END
                                inline.ContentId = Guid.NewGuid().ToString()
                                mstrMessage = mstrMessage.Replace("src=""" & contentPath & "/" & fl.Name & """", "src=""cid:" & inline.ContentId & """")
                                lscr.Add(inline)
                            End If
                        Next
                        Dim avHtml As AlternateView = AlternateView.CreateAlternateViewFromString(mstrMessage, Nothing, MediaTypeNames.Text.Html)
                        For i As Integer = 0 To lscr.Count - 1
                            avHtml.LinkedResources.Add(lscr(i))
                        Next
                        objMail.AlternateViews.Add(avHtml)
                    End If
                    objMail.Body = mstrMessage
                    objMail.Subject = mstrSubject
                    If mstrCCAddress.Trim.Length > 0 Then
                        objMail.CC.Add(mstrCCAddress)
                    End If

                    If mstrBCCAddress.Trim.Length > 0 Then
                        objMail.Bcc.Add(mstrBCCAddress)
                    End If

                    If mstrAttachedFiles <> "" Then
                        Dim strAtt() As String
                        strAtt = mstrAttachedFiles.Split(",")
                        If strAtt.Length > 0 Then
                            For i As Integer = 0 To strAtt.Length - 1
                                If blnIsPaySlip = True And StrPath.Length > 0 Then
                                    If IO.File.Exists(StrPath & "\" & strAtt(i)) Then objMail.Attachments.Add(New System.Net.Mail.Attachment(StrPath & "\" & strAtt(i)))
                                Else
                                    If strAtt(i).ToString.StartsWith("http://") = False Then
                                        If IO.File.Exists(strAtt(i)) Then objMail.Attachments.Add(New System.Net.Mail.Attachment(strAtt(i)))
                                    Else
                                        Dim imgData As Byte() = Nothing
                                        Dim strWritePath As String = System.IO.Path.GetTempPath()
                                        Dim filename As String = ""
                                        filename = System.IO.Path.GetFileName(strAtt(i).ToString)
                                        Dim FolderName As String = strAtt(i).ToString.Split("/")(strAtt(i).ToString.Split("/").Length - 2)
                                        Dim strError As String = ""
                                        imgData = clsFileUploadDownload.DownloadFile(strAtt(i).ToString, filename, FolderName, strError, strAtt(i).ToString.Substring(0, strAtt(i).ToString.IndexOf("/uploadimage") + 1))
                                        If imgData IsNot Nothing Then
                                            System.IO.File.WriteAllBytes(strWritePath & filename, imgData)
                                            strWritePath = strWritePath & filename
                                            objMail.Attachments.Add(New System.Net.Mail.Attachment(strWritePath))
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    End If
                    objMail.DeliveryNotificationOptions = Net.Mail.DeliveryNotificationOptions.OnSuccess Or Net.Mail.DeliveryNotificationOptions.OnFailure
                    objMail.Headers.Add("Disposition-Notification-To", strSenderAddress)
                    objMail.Headers.Add("Return-Receipt-To", strSenderAddress)
                    objMail.IsBodyHtml = True
                    Dim SmtpMail As New System.Net.Mail.SmtpClient()
                    SmtpMail.Host = strMailserverip  'gobjParam._MailServer
                    SmtpMail.Port = intMailserverport   'gobjParam._MailPort
                    If strUsername <> "" Then SmtpMail.Credentials = New System.Net.NetworkCredential(strUsername, strPassword)
                    SmtpMail.EnableSsl = blnIsloginssl

                    If blnByPassProxy Then
                        System.Net.ServicePointManager.Expect100Continue = False
                    End If

                    If blnIsCertificateAuthorization Then
                        System.Net.ServicePointManager.ServerCertificateValidationCallback = Function(s As Object, certificate As System.Security.Cryptography.X509Certificates.X509Certificate, chain As System.Security.Cryptography.X509Certificates.X509Chain, sslPolicyErrors As System.Net.Security.SslPolicyErrors) True
                    End If

                    'Hemant (01 Feb 2022) -- Start
                    'ISSUE/ENHANCEMENT : OLD-546 - FFK - Provide TLS 1.2 Protocol type in Company Email Configuration.
                    If SmtpMail.Port = 587 Then
                        If intProtocolId = enAuthenticationProtocol.TLS Then
                            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls
                        ElseIf intProtocolId = enAuthenticationProtocol.SSL3 Then
                            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Ssl3
                        ElseIf intProtocolId = enAuthenticationProtocol.TLS12 Then
                            System.Net.ServicePointManager.SecurityProtocol = Tls12
                        Else
                        End If
                    End If
                    'Hemant (01 Feb 2022) -- End

                    SmtpMail.Send(objMail)
                    objMail.Dispose()

                ElseIf intEmail_Type = 1 Then '0 = SMTP, 1 = EWS

                    Dim objService As New ExchangeService()
                    objService.Credentials = New WebCredentials(strUsername, strPassword, strEWS_Domain)
                    objService.Url = New Uri(strEWS_URL)
                    Dim objMessage As New EmailMessage(objService)

                    'Setting Email message properties
                    objMessage.Subject = mstrSubject
                    objMessage.Body = mstrMessage
                    objMessage.ToRecipients.Add(mstrToEmail)
                    If mstrCCAddress.Trim.Length > 0 Then
                        objMessage.CcRecipients.Add(mstrCCAddress)
                    End If
                    If mstrBCCAddress.Trim.Length > 0 Then
                        objMessage.BccRecipients.Add(mstrBCCAddress)
                    End If

                    If mstrMessage.Contains("<img ") AndAlso Content IsNot Nothing Then
                        For Each iKey As String In Content.Keys
                            Dim fl As New IO.FileInfo(iKey)
                            'S.SANDEEP |14-FEB-2019| -- START
                            'ISSUE/ENHANCEMENT : IMAGE NOT DISPLAYED
                            Dim iPath As String = System.IO.Path.GetTempPath()
                            iPath = iPath & "\" & fl.Directory.Name
                            'S.SANDEEP |14-FEB-2019| -- END
                            Dim contentPath = Path.GetDirectoryName(iKey)
                            If mstrMessage.Contains("src=""" & contentPath & "/" & fl.Name & """") Then
                                'S.SANDEEP |14-FEB-2019| -- START
                                'ISSUE/ENHANCEMENT : IMAGE NOT DISPLAYED
                                'objMessage.Attachments.AddFileAttachment(fl.Name, iKey)
                                objMessage.Attachments.AddFileAttachment(fl.Name, iPath & "\" & fl.Name)
                                'S.SANDEEP |14-FEB-2019| -- END
                                objMessage.Attachments(0).IsInline = True
                                objMessage.Attachments(0).ContentId = fl.Name
                            End If
                        Next
                    End If

                    If mstrAttachedFiles <> "" Then
                        Dim strAtt() As String
                        strAtt = mstrAttachedFiles.Split(",")
                        If strAtt.Length > 0 Then
                            For i As Integer = 0 To strAtt.Length - 1

                                If blnIsPaySlip = True And StrPath.Length > 0 Then
                                    If IO.File.Exists(StrPath & "\" & strAtt(i)) Then objMessage.Attachments.AddFileAttachment(StrPath & "\" & strAtt(i))
                                Else
                                    If strAtt(i).ToString.StartsWith("http://") = False Then
                                        If IO.File.Exists(strAtt(i)) Then objMessage.Attachments.AddFileAttachment(strAtt(i))
                                    Else
                                        Dim imgData As Byte() = Nothing
                                        Dim strWritePath As String = System.IO.Path.GetTempPath()
                                        Dim filename As String = ""

                                        filename = System.IO.Path.GetFileName(strAtt(i).ToString)
                                        Dim FolderName As String = strAtt(i).ToString.Split("/")(strAtt(i).ToString.Split("/").Length - 2)
                                        Dim strError As String = ""
                                        imgData = clsFileUploadDownload.DownloadFile(strAtt(i).ToString, filename, FolderName, strError, strAtt(i).ToString.Substring(0, strAtt(i).ToString.IndexOf("/uploadimage") + 1))

                                        If imgData IsNot Nothing Then
                                            System.IO.File.WriteAllBytes(strWritePath & filename, imgData)
                                            strWritePath = strWritePath & filename
                                            objMessage.Attachments.AddFileAttachment(strWritePath)
                                        End If
                                    End If

                                End If
                            Next
                        End If
                    End If

                    objMessage.SendAndSaveCopy()

                End If
                'Sohail (30 Nov 2017) -- End

                If mintModuleRefId > 0 Then
                    Call Insert_Email_Tran_Log(strSenderAddress)
                End If

                Return ""
            Else
                Return Language.getMessage(mstrModuleName, 1, "Recipient email address should not be blank.")
            End If
        Catch ex As Exception
            If mintModuleRefId > 0 Then
                'S.SANDEEP |05-FEB-2022| -- START
                'ISSUE : REASON CATCHING
                'Call Insert_Email_Tran_Log(strSenderAddress, ex.Message)
                Dim StrMsg As String = ""
                StrMsg = ex.Message
                If ex.InnerException IsNot Nothing Then
                    StrMsg &= "; InnerException : " & ex.InnerException.Message & _
                    "; StackTrace : " & ex.StackTrace
                End If
                Call Insert_Email_Tran_Log(strSenderAddress, StrMsg)
                'S.SANDEEP |05-FEB-2022| -- END                
            End If
            Return ex.Message
        Finally

        End Try
    End Function

    'S.SANDEEP [21-SEP-2018] -- END
    

    'S.SANDEEP [ 28 JAN 2014 ] -- START

    'Pinkal (02-Jan-2015) -- Start
    'Enhancement - INCLUDING EMAIL TRAN LOG WHEN SENDING EMAIL IN WEB FOR FORGET PASSWORD LINK.
    'Private Sub Insert_Email_Tran_Log(Optional ByVal strReason As String = "")
    Public Sub Insert_Email_Tran_Log(ByVal strSenderaddress As String, Optional ByVal strReason As String = "")
        'Sohail (30 Nov 2017) - [strSenderaddress]
        'Pinkal (02-Jan-2015) -- End

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            Dim iTableName As String = String.Empty
            Select Case mintModuleRefId
                Case enAT_VIEW_TYPE.ASSESSMENT_MGT
                    iTableName = "atassess_email_ntf_tran"
                Case enAT_VIEW_TYPE.LEAVE_MGT
                    iTableName = "atleave_email_ntf_tran"
                Case enAT_VIEW_TYPE.EMPLOYEE_MGT
                    iTableName = "atemployee_email_ntf_tran"
                Case enAT_VIEW_TYPE.PAYROLL_MGT
                    iTableName = "atpayroll_email_ntf_tran"
                Case enAT_VIEW_TYPE.RECRUITMENT_MGT
                    iTableName = "atrecruit_email_ntf_tran"

                    'Pinkal (13-Jul-2015) -- Start
                    'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
                Case enAT_VIEW_TYPE.CLAIMREQUEST_MGT
                    iTableName = "atclaimrequest_email_ntf_tran"
                    'Pinkal (13-Jul-2015) -- End

                    'S.SANDEEP [24 MAY 2016] -- Start
                    'Email Notification
                Case enAT_VIEW_TYPE.DISCIPLINE_MGT
                    iTableName = "atdiscipline_email_ntf_tran"
                    'S.SANDEEP [24 MAY 2016] -- End

                    'Shani (21-Jul-2016) -- Start
                    'Enhancement - Create New Loan Notification 
                Case enAT_VIEW_TYPE.LOAN_MGT
                    iTableName = "atloan_email_ntf_tran"
                    'Shani (21-Jul-2016) -- End

                Case enAT_VIEW_TYPE.GRIEVANCE_MGT
                    iTableName = "atgregrievance_email_ntf_tran"

                    'S.SANDEEP [09-OCT-2018] -- START
                Case enAT_VIEW_TYPE.TRAININGREQUISITION_MGT
                    iTableName = "attrainingRequisition_email_ntf_tran"
                    'S.SANDEEP [09-OCT-2018] -- END

                    'Gajanan [19-OCT-2019] -- Start    
                    'Enhancement:Email Notification email will be sent to selected user when user is created/edited from User creation screen.
                Case enAT_VIEW_TYPE.Configuration
                    iTableName = "hrmsConfiguration..atconfig_email_nft_tran"
                    'Gajanan [19-OCT-2019] -- End


                    'Pinkal (11-Sep-2019) -- Start
                    'Enhancement NMB - Working On Claim Retirement for NMB.
                Case enAT_VIEW_TYPE.CLAIM_RETIREMENT_MGT
                    iTableName = "atclaimretirement_email_ntf_tran"
                    'Pinkal (11-Sep-2019) -- End


                    'Pinkal (07-Nov-2019) -- Start
                    'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
                Case enAT_VIEW_TYPE.OTREQUISITION_MGT
                    iTableName = "atotrequisition_email_ntf_tran"
                    'Pinkal (07-Nov-2019) -- End


                    'Pinkal (27-Oct-2020) -- Start
                    'Enhancement St.Jude -   Working on Automatic Attendance download and send Report to Manager.
                Case enAT_VIEW_TYPE.TNA_MGT
                    iTableName = "attna_email_ntf_tran"
                    'Pinkal (27-Oct-2020) -- End

                    'Hemant (03 Dec 2020) -- Start
                    'Enhancement : Email Notifications for Talent Pipeline
                Case enAT_VIEW_TYPE.TALENT_MGT
                    iTableName = "attalent_email_ntf_tran"
                    'Hemant (03 Dec 2020) -- End

                    'Hemant (15 Dec 2020) -- Start
                Case enAT_VIEW_TYPE.SUCCESSION_MGT
                    iTableName = "atsuccession_email_ntf_tran"
                    'Hemant (15 Dec 2020) -- End

                Case enAT_VIEW_TYPE.PDP_MGT
                    iTableName = "atpdp_email_ntf_tran"


            End Select

            StrQ = "INSERT INTO " & iTableName & " " & _
                   "( " & _
                   "	 row_guid " & _
                   "	,sender_address " & _
                   "	,recipient_address " & _
                   "	,send_datetime " & _
                   "	,sent_data " & _
                   "	,subject " & _
                   "	,ip " & _
                   "	,host " & _
                   "	,form_name " & _
                   "	,module_name1 " & _
                   "	,module_name2 " & _
                   "	,module_name3 " & _
                   "	,module_name4 " & _
                   "	,module_name5 " & _
                   "	,operationmodeid " & _
                   "	,userunkid " & _
                   "	,loginemployeeunkid " & _
                   "	,fail_reason " & _
                   "	,filename " & _
                   "	,gateway " & _
                   ", isweb " & _
                   ") " & _
                   "VALUES " & _
                   "( " & _
                   "	 @row_guid " & _
                   "	,@sender_address " & _
                   "	,@recipient_address " & _
                   "	, getdate() " & _
                   "	,@sent_data " & _
                   "	,@subject " & _
                   "	,@ip " & _
                   "	,@host " & _
                   "	,@form_name " & _
                   "	,@module_name1 " & _
                   "	,@module_name2 " & _
                   "	,@module_name3 " & _
                   "	,@module_name4 " & _
                   "	,@module_name5 " & _
                   "	,@operationmodeid " & _
                   "	,@userunkid " & _
                   "	,@loginemployeeunkid " & _
                   "	,@fail_reason "
            If mstrAttachedFiles.Trim.Length > 0 Then
                StrQ &= ",'" & mstrAttachedFiles & "' "
            Else
                StrQ &= ",'' "
            End If
            StrQ &= "	,@gateway " & _
                    ", @isweb " & _
                    ") "
            'Sohail (17 Dec 2014) - [isweb]

            objDataOperation.AddParameter("@row_guid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString)
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'objDataOperation.AddParameter("@sender_address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSenderAddress)
            objDataOperation.AddParameter("@sender_address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strSenderaddress)
            'Sohail (30 Nov 2017) -- End
            objDataOperation.AddParameter("@recipient_address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrToEmail)

            'Pinkal (03-Nov-2014) -- Start
            'Enhancement -  GIVEN ERROR DATA READER IS ALREADY OPEN. WHEN ERROR WAS COMING.
            'objDataOperation.AddParameter("@send_datetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            'Pinkal (03-Nov-2014) -- End

            objDataOperation.AddParameter("@sent_data", SqlDbType.VarChar, mstrMessage.Length, mstrMessage)
            objDataOperation.AddParameter("@subject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSubject)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP(), mstrWebClientIP))
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName(), mstrWebHostName))
            If mstrFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name)
                'Sohail (17 Dec 2014) -- Start
                'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                'Sohail (17 Dec 2014) -- End
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
                'Sohail (17 Dec 2014) -- Start
                'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 8, "WEB"))
                'Sohail (17 Dec 2014) -- End
            End If
            'objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrModuleName1) 'Sohail (17 Dec 2014)
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrModuleName5)
            objDataOperation.AddParameter("@operationmodeid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mintOperationModeId)
            'Nilay (27 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'NOTES:ERROR - There is already an open DataReader associated with this Command which must be close first.
            'REASON : User._Object._Userunkid is used so remove it
            'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(mintUserUnkid = 0, User._Object._Userunkid.ToString, mintUserUnkid))
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserUnkid)
            'Nilay (27 Feb 2017) -- End

            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid)
            objDataOperation.AddParameter("@fail_reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strReason)
            'Sohail (30 Nov 2017) -- Start
            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            'objDataOperation.AddParameter("@gateway", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Company._Object._Senderaddress)
            objDataOperation.AddParameter("@gateway", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strSenderaddress)
            'Sohail (30 Nov 2017) -- End

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'DisplayError.Show("-1", ex.Message, "Insert_Email_Tran_Log", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: Insert_Email_Tran_Log; Module Name: " & mstrModuleName)
            'Pinkal (13-Aug-2020) -- End
        Finally
            objDataOperation = Nothing
        End Try
    End Sub

    Public Function Generate_Log_List(ByVal iViewType As clsSendMail.enAT_VIEW_TYPE, _
                                      ByVal iDate1 As Date, _
                                      ByVal iDate2 As Date, _
                                      ByVal iViewById As Integer, _
                                      ByVal iFilterId As Integer, _
                                      ByVal iScreenName As String, _
                                      ByVal IP_Addr As String, _
                                      ByVal iMachineName As String, _
                                      ByVal iSbuject As String) As DataTable
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim iTableName As String = String.Empty
        Try
            Select Case iViewType
                Case enAT_VIEW_TYPE.ASSESSMENT_MGT
                    iTableName = "atassess_email_ntf_tran"
                Case enAT_VIEW_TYPE.LEAVE_MGT
                    iTableName = "atleave_email_ntf_tran"
                Case enAT_VIEW_TYPE.EMPLOYEE_MGT
                    iTableName = "atemployee_email_ntf_tran"
                Case enAT_VIEW_TYPE.PAYROLL_MGT
                    iTableName = "atpayroll_email_ntf_tran"
                Case enAT_VIEW_TYPE.RECRUITMENT_MGT
                    iTableName = "atrecruit_email_ntf_tran"

                    'Pinkal (13-Jul-2015) -- Start
                    'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
                Case enAT_VIEW_TYPE.CLAIMREQUEST_MGT
                    iTableName = "atclaimrequest_email_ntf_tran"
                    'Pinkal (13-Jul-2015) -- End

                    'S.SANDEEP [24 MAY 2016] -- Start
                    'Email Notification
                Case enAT_VIEW_TYPE.DISCIPLINE_MGT
                    iTableName = "atdiscipline_email_ntf_tran"
                    'S.SANDEEP [24 MAY 2016] -- End

                    'Shani (21-Jul-2016) -- Start
                    'Enhancement - Create New Loan Notification 
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'Case enAT_VIEW_TYPE.CLAIMREQUEST_MGT
                Case enAT_VIEW_TYPE.LOAN_MGT
                    'Sohail (30 Nov 2017) -- End
                    iTableName = "atloan_email_ntf_tran"
                    'Shani (21-Jul-2016) -- End

                    'Pinkal (07-Nov-2019) -- Start
                    'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
                Case enAT_VIEW_TYPE.CLAIM_RETIREMENT_MGT
                    iTableName = "atclaimretirement_email_ntf_tran"

                Case enAT_VIEW_TYPE.OTREQUISITION_MGT
                    iTableName = "atotrequisition_email_ntf_tran"
                    'Pinkal (07-Nov-2019) -- End

            End Select

            StrQ = "SELECT " & _
                   "	 CAST(0 AS BIT) AS iCol " & _
                   "	,CASE WHEN operationmodeid = 2 THEN " & _
                   "			ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') " & _
                   "		  WHEN operationmodeid IN (1,3) THEN " & _
                   "			ISNULL(username,'') END AS 'Logged In' " & _
                   "	,CASE WHEN @LangId = 0 THEN cflanguage.language " & _
                   "		  WHEN @LangId = 1 THEN cflanguage.language1 " & _
                   "		  WHEN @LangId = 2 THEN cflanguage.language2 END AS 'Screen Name' " & _
                   "	,sender_address AS 'Sender' " & _
                   "	,recipient_address AS 'Recipient' " & _
                   "	,CONVERT(CHAR(8),send_datetime,112) AS 'Send Date' " & _
                   "	,CONVERT(CHAR(8),send_datetime,108) AS 'Send Time' " & _
                   "	,ip AS 'IP Address' " & _
                   "	,host AS 'Machine Name' " & _
                   "	,CASE WHEN operationmodeid = 1 THEN @DES " & _
                   "		  WHEN operationmodeid = 2 THEN @ESS " & _
                   "		  WHEN operationmodeid = 3 THEN @MSS END AS 'Operation' " & _
                   "	,fail_reason AS 'Fail Reason' " & _
                   "	,filename AS 'File Attached' " & _
                   "	,subject AS iSub "
            If iViewType = enAT_VIEW_TYPE.EMPLOYEE_MGT Then
                StrQ &= "	,CASE WHEN form_name = 'Aruti Login Page' AND module_name1 = 'WEB' THEN @Password ELSE sent_data END AS iCon "
            Else
                StrQ &= "	,sent_data AS iCon "
            End If


            StrQ &= "	,row_guid AS iRG " & _
                   "FROM " & iTableName & " " & _
                   "	LEFT JOIN cflanguage ON cflanguage.controlname = " & iTableName & ".form_name " & _
                   "	LEFT JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid  = " & iTableName & ".userunkid " & _
                   "	LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = " & iTableName & ".loginemployeeunkid " & _
                   "WHERE CONVERT(CHAR(8),send_datetime,112) BETWEEN '" & eZeeDate.convertDate(iDate1).ToString & "' AND '" & eZeeDate.convertDate(iDate2).ToString & "' "

            If iViewById > 0 Then
                StrQ &= " AND " & iTableName & ".operationmodeid = '" & iViewById & "' "
            End If

            If iFilterId > 0 Then
                StrQ &= " AND CASE WHEN operationmodeid = 2 THEN " & iTableName & ".loginemployeeunkid WHEN operationmodeid IN (1,3) THEN " & iTableName & ".userunkid END = '" & iFilterId & "' "
            End If

            If iScreenName.Trim.Length > 0 Then
                StrQ &= " AND CASE WHEN @LangId = 0 THEN cflanguage.language " & _
                        "		   WHEN @LangId = 1 THEN cflanguage.language1 " & _
                        "		   WHEN @LangId = 2 THEN cflanguage.language2 END LIKE '%" & iScreenName & "%' "
            End If

            If IP_Addr.Trim.Length > 0 Then
                StrQ &= " AND ip LIKE '%" & IP_Addr & "%' "
            End If

            If iMachineName.Trim.Length > 0 Then
                StrQ &= " AND host LIKE '%" & iMachineName & "%' "
            End If

            If iSbuject.Trim.Length > 0 Then
                StrQ &= " AND subject LIKE '%" & iSbuject & "%' "
            End If

            objDataOperation.AddParameter("@LangId", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Languageunkid)
            objDataOperation.AddParameter("@DES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmATEmail_LogView", 105, "Desktop"))
            objDataOperation.AddParameter("@ESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmATEmail_LogView", 106, "Employee Self Service"))
            objDataOperation.AddParameter("@MSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("frmATEmail_LogView", 107, "Manager Self Service"))
            'S.SANDEEP [07 DEC 2016] -- START
            objDataOperation.AddParameter("@Password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1000, "Password sent to,") & " " & "#value" & " " & Language.getMessage(mstrModuleName, 1001, "for the request of forget password."))
            '***********************************************************************
            '"	,sent_data AS iCon " -------------------------------------------- REMOVED

            '//////////////////////////////////////// ADDED //////////////////////////////////////////////
            'If iViewType = enAT_VIEW_TYPE.EMPLOYEE_MGT Then
            '    StrQ &= "	,CASE WHEN form_name = 'Aruti Login Page' AND module_name1 = 'WEB' THEN @Password ELSE sent_data END AS iCon "
            'Else
            '    StrQ &= "	,sent_data AS iCon "
            'End If
            '//////////////////////////////////////// ADDED //////////////////////////////////////////////
            '***********************************************************************
            'S.SANDEEP [07 DEC 2016] -- END


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 20 JUN 2014 ] -- START
            For Each iRow As DataRow In dsList.Tables(0).Rows
                iRow.Item("Send Date") = eZeeDate.convertDate(iRow.Item("Send Date").ToString).ToShortDateString
                'S.SANDEEP [01 DEC 2016] -- START
                If iRow.Item("iCon").ToString.Contains("#value") Then
                    iRow.Item("iCon") = iRow.Item("iCon").ToString.Replace("#value", "<FONT COLOR = 'Blue'><B>" & iRow.Item("Recipient").ToString & "</font></B>")
                End If
                'S.SANDEEP [01 DEC 2016] -- END
            Next
            'S.SANDEEP [ 20 JUN 2014 ] -- END


            Return dsList.Tables(0)

        Catch ex As Exception

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'DisplayError.Show("-1", ex.Message, "Generate_Log_List", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: Generate_Log_List; Module Name: " & mstrModuleName)
            'Pinkal (13-Aug-2020) -- End
            Return Nothing
        Finally
        End Try
    End Function

    Public Function getComboList_AT(ByVal iFlag As Boolean, Optional ByVal iList As String = "iList") As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception : Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Try
            If iFlag = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            StrQ &= "SELECT '" & enAT_VIEW_TYPE.ASSESSMENT_MGT & "' AS Id, @ASSESSMENT_MGT AS Name UNION " & _
                    "SELECT '" & enAT_VIEW_TYPE.LEAVE_MGT & "' AS Id, @LEAVE_MGT AS Name UNION " & _
                    "SELECT '" & enAT_VIEW_TYPE.EMPLOYEE_MGT & "' AS Id, @EMPLOYEE_MGT AS Name UNION " & _
                    "SELECT '" & enAT_VIEW_TYPE.PAYROLL_MGT & "' AS Id, @PAYROLL_MGT AS Name UNION " & _
                    "SELECT '" & enAT_VIEW_TYPE.RECRUITMENT_MGT & "' AS Id, @RECRUITMENT_MGT AS Name "


            'Pinkal (13-Jul-2015) -- Start
            'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
            StrQ &= " UNION SELECT '" & enAT_VIEW_TYPE.CLAIMREQUEST_MGT & "' AS Id, @CLAIMREQUEST_MGT AS Name "
            'Pinkal (13-Jul-2015) -- End

            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            StrQ &= " UNION SELECT '" & enAT_VIEW_TYPE.DISCIPLINE_MGT & "' AS Id, @DISCIPLINE_MGT AS Name "
            'S.SANDEEP |01-OCT-2019| -- END


            'Pinkal (07-Nov-2019) -- Start
            'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
            StrQ &= " UNION SELECT '" & enAT_VIEW_TYPE.CLAIM_RETIREMENT_MGT & "' AS Id, @CLAIMRETIREMENT_MGT AS Name "
            StrQ &= " UNION SELECT '" & enAT_VIEW_TYPE.OTREQUISITION_MGT & "' AS Id, @OTREQUISITION_MGT AS Name "
            'Pinkal (07-Nov-2019) -- End



            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
            objDataOperation.AddParameter("@ASSESSMENT_MGT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Employee Assessment"))
            objDataOperation.AddParameter("@LEAVE_MGT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Leave Managment"))
            objDataOperation.AddParameter("@EMPLOYEE_MGT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Employee Managment"))
            objDataOperation.AddParameter("@PAYROLL_MGT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Payroll Managment"))
            objDataOperation.AddParameter("@RECRUITMENT_MGT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Recruitment Managment"))


            'Pinkal (13-Jul-2015) -- Start
            'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
            objDataOperation.AddParameter("@CLAIMREQUEST_MGT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Claim and Request Managment"))
            'Pinkal (13-Jul-2015) -- End

            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            objDataOperation.AddParameter("@DISCIPLINE_MGT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Discipline Managment"))
            'S.SANDEEP |01-OCT-2019| -- END


            'Pinkal (07-Nov-2019) -- Start
            'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
            objDataOperation.AddParameter("@CLAIMRETIREMENT_MGT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Claim Retirement Managment"))
            objDataOperation.AddParameter("@OTREQUISITION_MGT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "OT Requisition Managment"))
            'Pinkal (07-Nov-2019) -- End



            dsList = objDataOperation.ExecQuery(StrQ, iList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'DisplayError.Show("-1", ex.Message, "getComboList_AT", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: getComboList_AT; Module Name: " & mstrModuleName)
            'Pinkal (13-Aug-2020) -- End
            Return Nothing
        Finally
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Delete_Email_Log(ByVal iViewType As clsSendMail.enAT_VIEW_TYPE, _
                                     ByVal iDate1 As Date, ByVal iDate2 As Date) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim iTableName As String = String.Empty
        Try
            Select Case iViewType
                Case enAT_VIEW_TYPE.ASSESSMENT_MGT
                    iTableName = "atassess_email_ntf_tran"
                Case enAT_VIEW_TYPE.LEAVE_MGT
                    iTableName = "atleave_email_ntf_tran"
                Case enAT_VIEW_TYPE.EMPLOYEE_MGT
                    iTableName = "atemployee_email_ntf_tran"
                Case enAT_VIEW_TYPE.PAYROLL_MGT
                    iTableName = "atpayroll_email_ntf_tran"
                Case enAT_VIEW_TYPE.RECRUITMENT_MGT
                    iTableName = "atrecruit_email_ntf_tran"

                    'Pinkal (13-Jul-2015) -- Start
                    'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
                Case enAT_VIEW_TYPE.CLAIMREQUEST_MGT
                    iTableName = "atclaimrequest_email_ntf_tran"
                    'Pinkal (13-Jul-2015) -- End

                    'S.SANDEEP [24 MAY 2016] -- Start
                    'Email Notification
                Case enAT_VIEW_TYPE.DISCIPLINE_MGT
                    iTableName = "atdiscipline_email_ntf_tran"
                    'S.SANDEEP [24 MAY 2016] -- End

                    'Shani (21-Jul-2016) -- Start
                    'Enhancement - Create New Loan Notification 
                Case enAT_VIEW_TYPE.LOAN_MGT
                    iTableName = "atloan_email_ntf_tran"
                    'Shani (21-Jul-2016) -- End


                    'Pinkal (07-Nov-2019) -- Start
                    'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
                Case enAT_VIEW_TYPE.CLAIM_RETIREMENT_MGT
                    iTableName = "atclaimretirement_email_ntf_tran"

                Case enAT_VIEW_TYPE.OTREQUISITION_MGT
                    iTableName = "atotrequisition_email_ntf_tran"
                    'Pinkal (07-Nov-2019) -- End


            End Select

            StrQ = "DELETE FROM " & iTableName & " WHERE CONVERT(CHAR(8),send_datetime,112) BETWEEN '" & eZeeDate.convertDate(iDate1).ToString & "' AND '" & eZeeDate.convertDate(iDate2).ToString & "' "

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'DisplayError.Show("-1", ex.Message, "Delete_Email_Log", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: getComboList_AT; Module Name: " & mstrModuleName)
            'Pinkal (13-Aug-2020) -- End
        Finally
        End Try
    End Function
    'S.SANDEEP [ 28 JAN 2014 ] -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
			Language.setMessage("frmATEmail_LogView", 105, "Desktop")
			Language.setMessage("frmATEmail_LogView", 106, "Employee Self Service")
			Language.setMessage("frmATEmail_LogView", 107, "Manager Self Service")
            Language.setMessage(mstrModuleName, 1, "Recipient email address should not be blank.")
            Language.setMessage(mstrModuleName, 2, "Select")
            Language.setMessage(mstrModuleName, 3, "Employee Assessment")
            Language.setMessage(mstrModuleName, 4, "Leave Managment")
            Language.setMessage(mstrModuleName, 5, "Employee Managment")
            Language.setMessage(mstrModuleName, 6, "Payroll Managment")
            Language.setMessage(mstrModuleName, 7, "Recruitment Managment")
            Language.setMessage(mstrModuleName, 8, "WEB")
            Language.setMessage(mstrModuleName, 9, "Claim and Request Managment")
			Language.setMessage(mstrModuleName, 10, "Discipline Managment")
			Language.setMessage(mstrModuleName, 1000, "Password sent to,")
			Language.setMessage(mstrModuleName, 1001, "for the request of forget password.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
