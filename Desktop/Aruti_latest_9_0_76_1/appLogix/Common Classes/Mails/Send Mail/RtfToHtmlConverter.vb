﻿Public NotInheritable Class RtfToHtmlConverter

    Private Sub New()
    End Sub

    Public Shared Function RtfToHtml(ByVal rtf As String, Optional ByVal contentUriPrefix As String = Nothing, Optional ByVal asFullDocument As Boolean = True) As HtmlResult
        Dim xamlStream = RtfToXamlConverter.RtfToXamlPackage(rtf)
        Dim htmlConverter = New XamlToHtmlConverter With {._AsFullDocument = asFullDocument, .ContentUriPrefix = contentUriPrefix}

        Return htmlConverter.ConvertXamlToHtml(xamlStream)
    End Function

End Class
