﻿'************************************************************************************************************************************
'Class Name : clsLetterFields.vb
'Purpose    :
'Date       :07/07/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master
Imports Aruti.Data.Language
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsLetterFields
    Private Shared ReadOnly mstrModuleName As String = "clsLetterFields"
    'S.SANDEEP [ 02 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 18 : On letter template, applicant fields, provide parameter to pick employee reporting-to. (For internal vacancies))
    'Private mintVacancyUnkid As Integer = 0
    'Public WriteOnly Property _VacancyUnkid() As Integer
    '    Set(ByVal value As Integer)
    '        mintVacancyUnkid = value
    '    End Set
    'End Property
    Private mstrVacancyUnkid As String = 0
    Public WriteOnly Property _VacancyUnkid() As String
        Set(ByVal value As String)
            mstrVacancyUnkid = value
        End Set
    End Property
    'Hemant (07 Oct 2019) -- End
    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 13 : On batch interview scheduling, system should send calendar invites to people selected as interviewers for that particular batch.  The date and time of the calendar should be picked from the interview date and time set)
    Private mintBatchscheduleUnkid As Integer = 0
    Public WriteOnly Property _BatchscheduleUnkid() As Integer
        Set(ByVal value As Integer)
            mintBatchscheduleUnkid = value
        End Set
    End Property
    'Hemant (07 Oct 2019) -- End


    'S.SANDEEP [ 02 MAY 2012 ] -- END

    'S.SANDEEP [05-Apr-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
    Private mCompany_Logo As Byte() = Nothing
    'S.SANDEEP [05-Apr-2018] -- END

    Public Function GetList(ByVal intModuleRefId As Integer, Optional ByVal strListName As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objdataOperation As New clsDataOperation
        Try
            Select Case intModuleRefId
                Case enImg_Email_RefId.Employee_Module '1  'Employee
                    StrQ = "SELECT " & _
                             " '' AS Shift " & _
                             ",'' AS Title " & _
                             ",'' AS EmpCode " & _
                             ",'' AS FirstName " & _
                             ",'' AS SurName " & _
                             ",'' AS OtherName " & _
                             ",'' AS Email " & _
                             ",'' AS BirthDate " & _
                             ",'' AS [Completed birth Age] " & _
                             ",'' AS [Running birth Age] " & _
                             ",'' AS AnniversaryDate " & _
                             ",'' AS EmpAddress1 " & _
                             ",'' AS EmpAddress2 " & _
                             ",'' AS City " & _
                             ",'' AS State " & _
                             ",'' AS Country " & _
                             ",'' AS SuspendedFrom " & _
                             ",'' AS SuspendedTo " & _
                             ",'' AS ProbationFrom " & _
                             ",'' AS ProbationTo " & _
                             ",'' AS TerminationDate " & _
                             ",'' AS RetirementDate " & _
                             ",'' AS ConfirmationDate " & _
                             ",'' AS EmploymentLeavingDate " & _
                             ",'' AS ApponintmentDate " & _
                             ",'' AS Branch " & _
                             ",'' AS DepartmentGroup " & _
                             ",'' AS Department " & _
                             ",'' AS SectionGroup " & _
                             ",'' AS Section " & _
                             ",'' AS UnitGroup " & _
                             ",'' AS Unit " & _
                             ",'' AS Team " & _
                             ",'' AS JobGroup " & _
                             ",'' AS Job " & _
                             ",'' AS ClassGroup " & _
                             ",'' AS Class " & _
                             ",'' AS CostCenter " & _
                             ",'' AS OldBranch " & _
                             ",'' AS OldDepartmentGroup " & _
                             ",'' AS OldDepartment " & _
                             ",'' AS OldSectionGroup " & _
                             ",'' AS OldSection " & _
                             ",'' AS OldUnitGroup " & _
                             ",'' AS OldUnit " & _
                             ",'' AS OldTeam " & _
                             ",'' AS OldJobGroup " & _
                             ",'' AS OldJob " & _
                             ",'' AS OldClassGroup " & _
                             ",'' AS OldClass " & _
                             ",'' AS OldCostCenter " & _
                             ",'' AS OldGradeGroup " & _
                             ",'' AS OldGrade " & _
                             ",'' AS OldGradeLevel " & _
                             ",'' AS OldSalary " & _
                             ",'' AS CurrentGradeGroup " & _
                             ",'' AS CurrentGrade " & _
                             ",'' AS CurrentGradeLevel " & _
                             ",'' AS CurrentSalary " & _
                             ",'' AS NextGradeGroup " & _
                             ",'' AS NextGrade " & _
                             ",'' AS NextGradeLevel " & _
                             ",'' AS NextSalary "
                    'Nilay (16-Apr-2016) -- [Completed birth Age, Running birth Age]
                    'S.SANDEEP [ 07 MAY 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    StrQ &= ",'' AS UserName " & _
                            ",'' AS Password "
                    'S.SANDEEP [ 07 MAY 2012 ] -- END

                    'S.SANDEEP [ 15 MAR 2014 ] -- START
                    StrQ &= ",'' AS ReportingCode " & _
                            ",'' AS ReportingFirstname " & _
                            ",'' AS ReportingOthername " & _
                            ",'' AS ReportingSurname "
                    'S.SANDEEP [ 15 MAR 2014 ] -- END

                    'S.SANDEEP [05-Apr-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
                    StrQ &= ",'' AS Gender " & _
                            ",'' AS [Reporting To Job] " & _
                            ",'' AS [Key Duties And Responsibilities] " & _
                            ",'' AS [Employment Period] "
                    'S.SANDEEP [05-Apr-2018] -- END

                    'S.SANDEEP |09-APR-2019| -- START
                    StrQ &= ",'' AS YearOfService "
                    'S.SANDEEP |09-APR-2019| -- END

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    ''''''''''''''''''''''''''' REMOVED
                    '   ",'' AS OldReportToJobGroup " & _
                    '   ",'' AS OldReportToJob " & _
                    '   ",'' AS ReportToJobGroup " & _
                    '   ",'' AS ReportToJob " & _
                    'S.SANDEEP [04 JUN 2015] -- END

    				'Gajanan [28-AUG-2019] -- Start      
    				'ENHANCEMENT : Additional Data Fields(EOC Effective Date and latest Reinstatement dates) required in Letter Templates  are required.
                    StrQ &= ",'' as [Reinstatement Date]" & _
                            ",'' as [EOC Effective Date]"
    				'Gajanan [28-AUG-2019] -- End      

                Case enImg_Email_RefId.Applicant_Module '2  'Applicant


                    'S.SANDEEP [ 04 MAY 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'StrQ = "SELECT " & _
                    '       " '' AS Title " & _
                    '       ",'' AS Firstname " & _
                    '       ",'' AS Surname " & _
                    '       ",'' AS Othername " & _
                    '       ",'' AS Gender " & _
                    '       ",'' AS Email " & _
                    '       ",'' AS Applicant_Address1 " & _
                    '       ",'' AS Applicant_Address2 " & _
                    '       ",'' AS Applicant_City " & _
                    '       ",'' AS Applicant_State " & _
                    '       ",'' AS Applicant_Country " & _
                    '       ",'' AS Mobileno " & _
                    '       ",'' AS BirthDate " & _
                    '       ",'' AS AnniversaryDate " & _
                    '       ",'' AS Vacancy " & _
                    '       ",'' AS Appreferenceno "

                    StrQ = "SELECT " & _
                           " '' AS Title " & _
                           ",'' AS Firstname " & _
                           ",'' AS Surname " & _
                           ",'' AS Othername " & _
                           ",'' AS Gender " & _
                           ",'' AS Email " & _
                           ",'' AS Applicant_Address1 " & _
                           ",'' AS Applicant_Address2 " & _
                           ",'' AS Applicant_City " & _
                           ",'' AS Applicant_State " & _
                           ",'' AS Applicant_Country " & _
                           ",'' AS Mobileno " & _
                           ",'' AS BirthDate " & _
                           ",'' AS [Completed birth Age] " & _
                           ",'' AS [Running birth Age] " & _
                           ",'' AS AnniversaryDate " & _
                           ",'' AS Appreferenceno " & _
                           ",'' AS Vacancy_Without_Dates " & _
                           ",'' AS Vacancy_With_Dates " & _
                           ",'' AS Branch " & _
                           ",'' AS Department_Group " & _
                           ",'' AS Job_Department " & _
                           ",'' AS Section " & _
                           ",'' AS Unit " & _
                           ",'' AS Job_Group " & _
                           ",'' AS Job_Name " & _
                           ",'' AS Employment_Type " & _
                           ",'' AS Grade_Group " & _
                           ",'' AS Grade " & _
                           ",'' AS Pay_Type " & _
                           ",'' AS Shift_Type " & _
                           ",'' AS Pay_Range_From " & _
                           ",'' AS Pay_Range_To "
                    'Nilay (16-Apr-2016) -- [Completed birth Age, Running birth Age]
                    'S.SANDEEP [ 04 MAY 2012 ] -- END

                    'Hemant (07 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 17 : On letter template, applicant fields, provide parameters for interview date,  interview time, interview venue. These should pick items as set on Batch Scheduling screen.)
                    StrQ &= ",'' AS InterviewDate " & _
                            ",'' AS InterviewTime " & _
                            ",'' AS InterviewVenue "
                    'Hemant (07 Oct 2019) -- End
                    'Hemant (07 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 18 : On letter template, applicant fields, provide parameter to pick employee reporting-to. (For internal vacancies))
                    StrQ &= ",'' AS ReportingTo "
                    'Hemant (07 Oct 2019) -- End

                Case enImg_Email_RefId.Leave_Module '3  'Leave

                    'Pinkal (18-Sep-2012) -- Start
                    'Enhancement : TRA Changes

                    StrQ = "SELECT " & _
                            " '' AS Title " & _
                            ",'' AS EmpCode " & _
                            ",'' AS Firstname " & _
                            ",'' AS Surname " & _
                            ",'' AS Othername " & _
                            ",'' AS Email " & _
                            ",'' AS Job " & _
                            ",'' AS ClassGroup " & _
                            ",'' AS Class " & _
                            ",'' AS Formno " & _
                            ",'' AS Leavename " & _
                            ",'' AS Applydate " & _
                            ",'' AS Startdate " & _
                            ",'' AS Enddate " & _
                            ",'' AS Days " & _
                            ",'' AS Addressonleave " & _
                            ",'' AS Remark " & _
                            ",'' AS Status "

                    'Pinkal (18-Sep-2012) -- End


                    'Pinkal (3-Aug-2013) -- Start
                    'Enhancement : TRA Changes

                    StrQ &= ", '' AS [Approved Start Date] " & _
                            ", '' AS [Approved End Date] " & _
                            ", 0.00 AS [Approved Days] " & _
                            ", '' AS [Accrue Start Date] " & _
                            ", '' AS [Accrue End Date] " & _
                            ", 0.00 AS [Leave Accrue] " & _
                            ", 0.00 AS [Leave Balance] " & _
                            ", 0.00 AS [Leave Issued] "

                    'Pinkal (3-Aug-2013) -- End




                Case enImg_Email_RefId.Payroll_Module '4  'Payroll
                    StrQ = ""
                Case enImg_Email_RefId.Discipline_Module '7 'Discipline 'S.SANDEEP [ 20 APRIL 2012 ]
                    'S.SANDEEP |08-JAN-2019| -- START
                    'StrQ = "SELECT " & _
                    '        " '' AS OffenceCategory " & _
                    '        ",'' AS DisciplinaryOffence " & _
                    '        ",'' AS StatementofOffence " & _
                    '        ",'' AS EmployeeTitle " & _
                    '        ",'' AS EmployeeGender " & _
                    '        ",'' AS EmployeeFirstname " & _
                    '        ",'' AS EmployeeMiddlename " & _
                    '        ",'' AS EmployeeSurname " & _
                    '        ",'' AS Email " & _
                    '        ",'' AS EmpAddress1 " & _
                    '        ",'' AS EmpAddress2 " & _
                    '        ",'' AS City " & _
                    '        ",'' AS State " & _
                    '        ",'' AS Country " & _
                    '        ",'' AS JobGroup " & _
                    '        ",'' AS Job " & _
                    '        ",'' AS Department " & _
                    '        ",'' AS EmpRegion " & _
                    '        ",'' AS WorkStation " & _
                    '        ",'' AS TransactionDate " & _
                    '        ",'' AS CommitteeName " & _
                    '        ",'' AS DisciplinaryAction " & _
                    '        ",'' AS ActionFromDate " & _
                    '        ",'' AS ActionToDate " & _
                    '        ",'' AS CaseNo " & _
                    '        ",'' AS CourtName " & _
                    '        ",'' AS FinalResolutionStep " & _
                    '        ",'' AS FinalDisciplineStatus "
                    ''S.SANDEEP [ 01 JUNE 2012 ] -- START
                    ''ENHANCEMENT : TRA DISCIPLINE CHANGES
                    'StrQ &= ",'' AS InterdictDate " & _
                    '        ",'' AS Discipline_Ref_No "
                    ''S.SANDEEP [ 01 JUNE 2012 ] -- END
                    StrQ = "SELECT " & _
                            " '' AS EmployeeCode " & _
                            ",'' AS EmployeeTitle " & _
                            ",'' AS EmployeeGender " & _
                            ",'' AS EmployeeFirstname " & _
                            ",'' AS EmployeeMiddlename " & _
                            ",'' AS EmployeeSurname " & _
                            ",'' AS Email " & _
                            ",'' AS EmployeeBranch " & _
                            ",'' AS EmployeeDepartmentGroup " & _
                            ",'' AS EmployeeDepartment " & _
                            ",'' AS EmployeeSectionGroup " & _
                            ",'' AS EmployeeSection " & _
                            ",'' AS EmployeeUnitGroup " & _
                            ",'' AS EmployeeUnit " & _
                            ",'' AS EmployeeTeam " & _
                            ",'' AS EmployeeClassGroup " & _
                            ",'' AS EmployeeClass " & _
                            ",'' AS EmployeeJobGroup " & _
                            ",'' AS EmployeeJob " & _
                            ",'' AS EmployeeGradeGroup " & _
                            ",'' AS EmployeeGrade " & _
                            ",'' AS EmployeeGradeLevel " & _
                            ",'' AS EmployeeCostCenter " & _
                            ",'' AS ReportingCode " & _
                            ",'' AS ReportingFirstname " & _
                            ",'' AS ReportingSurname " & _
                            ",'' AS ReportingOthername " & _
                            ",'' AS ChargeStatus " & _
                            ",'' AS ReferenceNo " & _
                            ",'' AS ChargeDate " & _
                            ",'' AS InterdictionDate " & _
                            ",'' AS ChargeDescription " & _
                            ",'' AS ChargeCategory " & _
                            ",'' AS TotalNoOfCount " & _
                            ",'' AS NotificationDate " & _
                            ",'' AS HearingDate " & _
                            ",'' AS HearingTime " & _
                            ",'' AS HearingVenue " & _
                            ",'' AS HearingRemark " & _
                            ",'' AS HearingMembers "
                    'S.SANDEEP |08-JAN-2019| -- END




                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                    '----------------------------------------- REMOVED
                    '",'' AS ReportToJobGroup " & _
                    '",'' AS ReportToJob " & _

                    'S.SANDEEP [04 JUN 2015] -- END


                    'S.SANDEEP [18-AUG-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#290}
                Case enImg_Email_RefId.PerformanceAssessment_Employee
                    StrQ = "SELECT " & _
                           " '' AS Title " & _
                           ",'' AS EmpCode " & _
                           ",'' AS FirstName " & _
                           ",'' AS SurName " & _
                           ",'' AS OtherName " & _
                           ",'' AS Email " & _
                           ",'' AS ApponintmentDate " & _
                           ",'' AS PeriodName " & _
                           ",'' AS StartDate " & _
                           ",'' AS EndDate "

                    'S.SANDEEP |08-JAN-2019| -- START
                    StrQ &= ",'' AS LockedAfterDays " & _
                            ",'' AS LockedBeforeDays " & _
                            ",'' AS PlanningLockDate " & _
                            ",'' AS AssessmentLockDate "
                    'S.SANDEEP |08-JAN-2019| -- END
                    
                Case enImg_Email_RefId.PerformanceAssessment_Assessor
                    StrQ = "SELECT " & _
                           " '' AS Title " & _
                           ",'' AS AssessorCode " & _
                           ",'' AS AssessorFirstName " & _
                           ",'' AS AssessorSurName " & _
                           ",'' AS AssessorOtherName " & _
                           ",'' AS AssessorEmail " & _
                           ",'' AS PeriodName " & _
                           ",'' AS StartDate " & _
                           ",'' AS EndDate "
                    'S.SANDEEP |08-JAN-2019| -- START
                    StrQ &= ",'' AS LockedAfterDays " & _
                            ",'' AS LockedBeforeDays " & _
                            ",'' AS PlanningLockDate " & _
                            ",'' AS AssessmentLockDate "
                    'S.SANDEEP |08-JAN-2019| -- END

                Case enImg_Email_RefId.PerformanceAssessment_Reviewer
                    StrQ = "SELECT " & _
                           " '' AS Title " & _
                           ",'' AS ReviewerCode " & _
                           ",'' AS ReviewerFirstName " & _
                           ",'' AS ReviewerSurName " & _
                           ",'' AS ReviewerOtherName " & _
                           ",'' AS ReviewerEmail " & _
                           ",'' AS PeriodName " & _
                           ",'' AS StartDate " & _
                           ",'' AS EndDate "

                    'S.SANDEEP |08-JAN-2019| -- START
                    StrQ &= ",'' AS LockedAfterDays " & _
                            ",'' AS LockedBeforeDays " & _
                            ",'' AS PlanningLockDate " & _
                            ",'' AS AssessmentLockDate "
                    'S.SANDEEP |08-JAN-2019| -- END

                    'S.SANDEEP [18-AUG-2018] -- END



                    'Pinkal (16-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.
                Case enImg_Email_RefId.Leave_Planner_Module

                    StrQ = "SELECT " & _
                               "   ''  AS Title " & _
                               "   ,'' AS EmpCode " & _
                               "   ,'' AS Firstname " & _
                               "   ,'' AS Surname " & _
                               "   ,'' AS Othername " & _
                               "   ,'' AS Email " & _
                               "   ,'' AS Job " & _
                               "   ,'' AS ClassGroup " & _
                               "   ,'' AS Class " & _
                               "   ,'' AS JobGroup " & _
                               "   ,'' AS Branch " & _
                               "   ,'' AS DepartmentGroup " & _
                               "   ,'' AS Department " & _
                               "   ,'' AS SectionGroup " & _
                               "   ,'' AS Section " & _
                               "   ,'' AS UnitGroup " & _
                               "   ,'' AS Unit " & _
                               "   ,'' AS Team " & _
                               "   ,'' AS Leavename " & _
                               "   ,'' AS Startdate " & _
                               "   ,'' AS Enddate " & _
                               "   , 0.00 AS Days "
                    'Pinkal (16-Oct-2018) -- End


                    'Gajanan [13-July-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Enhancement
                Case enImg_Email_RefId.Grievance

                    StrQ = "SELECT " & _
                            " '' AS [Raised Employee EmpCode] " & _
                            ",'' AS [Raised Employee FirstName] " & _
                            ",'' AS [Raised Employee SurName] " & _
                            ",'' AS [Raised Employee OtherName] " & _
                            ",'' AS [Raised Employee Email] " & _
                            ",'' AS [Raised Employee Branch] " & _
                            ",'' AS [Raised Employee DepartmentGroup] " & _
                            ",'' AS [Raised Employee Department] " & _
                            ",'' AS [Raised Employee SectionGroup] " & _
                            ",'' AS [Raised Employee Section] " & _
                            ",'' AS [Raised Employee UnitGroup] " & _
                            ",'' AS [Raised Employee Unit] " & _
                            ",'' AS [Raised Employee Team] " & _
                            ",'' AS [Raised Employee JobGroup] " & _
                            ",'' AS [Raised Employee Job] " & _
                            ",'' AS [Raised Employee ClassGroup] " & _
                            ",'' AS [Raised Employee Class] " & _
                            ",'' AS [Raised Employee CostCenter] " & _
                            ",'' AS [Against Employee EmpCode] " & _
                            ",'' AS [Against Employee FirstName] " & _
                            ",'' AS [Against EmployeeSurName] " & _
                            ",'' AS [Against EmployeeOtherName] " & _
                            ",'' AS [Against Employee Email] " & _
                            ",'' AS [Against Employee Branch] " & _
                            ",'' AS [Against Employee DepartmentGroup] " & _
                            ",'' AS [Against Employee Department] " & _
                            ",'' AS [Against Employee SectionGroup] " & _
                            ",'' AS [Against Employee Section] " & _
                            ",'' AS [Against Employee UnitGroup] " & _
                            ",'' AS [Against Employee Unit] " & _
                            ",'' AS [Against Employee Team] " & _
                            ",'' AS [Against Employee JobGroup] " & _
                            ",'' AS [Against Employee Job] " & _
                            ",'' AS [Against Employee ClassGroup] " & _
                            ",'' AS [Against Employee Class] " & _
                            ",'' AS [Against Employee CostCenter] " & _
                            ",'' AS [Grievance Reference No] " & _
                            ",'' AS [Grievance Description] " & _
                            ",'' AS [Grievance Reliefwanted] " & _
                            ",'' AS UserName "

                    'Gajanan [13-July-2019] -- End

                    'Gajanan [19-July-2019] -- ADD  [Grievance Reference No],[Grievance Description],[Grievance Reliefwanted]

                    'Hemant (07 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 13 : On batch interview scheduling, system should send calendar invites to people selected as interviewers for that particular batch.  The date and time of the calendar should be picked from the interview date and time set)
                Case enImg_Email_RefId.Batch_Scheduling

                    StrQ = "SELECT " & _
                               "   ''  AS Title " & _
                               "   ,'' AS InterviewerEmpCode " & _
                               "   ,'' AS InterviewerFirstname " & _
                               "   ,'' AS InterviewerSurname " & _
                               "   ,'' AS VacancyName " & _
                               "   ,'' AS VacancyOpeningDate " & _
                               "   ,'' AS VacancyClosingDate " & _
                               "   ,'' AS InterviewStartDate " & _
                               "   ,'' AS InterviewCloseDate " & _
                               "   ,'' AS InterviewerIntviewType " & _
                               "   ,'' AS InterviewerLevel " & _
                               "   ,'' AS OtherInterviewerName " & _
                               "   ,'' AS OtherCompany " & _
                               "   ,'' AS OtherDepartment " & _
                               "   ,'' AS OtherContactNo " & _
                               "   ,'' AS BatchCode " & _
                               "   ,'' AS BatchName " & _
                               "   ,'' AS InterviewDate " & _
                               "   ,'' AS InterviewTime " & _
                               "   ,'' AS VacancyInterviewType " & _
                               "   ,'' AS ResultGroup " & _
                               "   ,'' AS Location " & _
                               "   ,'' AS Description " & _
                               "   ,'' AS CancelRemark "
                    'Hemant (07 Oct 2019) -- End

                    'Hemant (30 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
                Case enImg_Email_RefId.Applicant_Job_Vacancy

                    StrQ = "SELECT " & _
                           " '' AS Title " & _
                           ",'' AS Firstname " & _
                           ",'' AS Surname " & _
                           ",'' AS Othername " & _
                           ",'' AS Gender " & _
                           ",'' AS Email " & _
                           ",'' AS Applicant_Address1 " & _
                           ",'' AS Applicant_Address2 " & _
                           ",'' AS Applicant_City " & _
                           ",'' AS Applicant_State " & _
                           ",'' AS Applicant_Country " & _
                           ",'' AS Mobileno " & _
                           ",'' AS BirthDate " & _
                           "/*,'' AS [Completed birth Age] " & _
                           ",'' AS [Running birth Age] " & _
                           ",'' AS AnniversaryDate " & _
                           ",'' AS Appreferenceno " & _
                           ",'' AS Vacancy_Without_Dates " & _
                           ",'' AS Vacancy_With_Dates " & _
                           ",'' AS Branch " & _
                           ",'' AS Department_Group " & _
                           ",'' AS Job_Department " & _
                           ",'' AS Section " & _
                           ",'' AS Unit " & _
                           ",'' AS Job_Group " & _
                           ",'' AS Job_Name " & _
                           ",'' AS Employment_Type " & _
                           ",'' AS Grade_Group " & _
                           ",'' AS Grade " & _
                           ",'' AS Pay_Type " & _
                           ",'' AS Shift_Type*/ " & _
                           ",'' AS VacancyName " & _
                           ",'' AS Pay_Range_From " & _
                           ",'' AS Pay_Range_To " & _
                           ",'' AS VacancyOpeningDate " & _
                           ",'' AS VacancyClosingDate " & _
                           ",'' AS experience " & _
                           ",'' AS noofposition " & _
                           ",'' AS responsibilities_duties " & _
                           ",'' AS remark " & _
                           ",'' AS SkillRequired " & _
                           ",'' AS QualificationRequired " & _
                           ",'' AS Externallink " & _
                           ",'' AS Unsubscribelink "
                    'Hemant (30 Oct 2019) -- End

                    'Sohail (17 Feb 2021) -- Start
                    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                Case enImg_Email_RefId.Loan_Balance

                    StrQ = "SELECT " & _
                           "  '' AS Firstname " & _
                           ", '' AS Surname " & _
                           ", '' AS Othername " & _
                           ", '' AS Email " & _
                           ", '' AS LoanVoucher_No " & _
                           ", '' AS LoanCode " & _
                           ", '' AS LoanScheme " & _
                           ", '' AS Loan_Amount " & _
                           ", '' AS CSign " & _
                           ", '' AS Installments " & _
                           ", '' AS EMI_Amount " & _
                           ", '' AS Interest_Rate " & _
                           ", '' AS Topup_Amount " & _
                           ", '' AS Total_Topup " & _
                           ", '' AS LoanBalance " 
                    'Sohail (17 Feb 2021) -- End


            End Select
            If StrQ.Length > 0 Then
                StrQ &= ",'' As Today " & _
                        ",'' As CompanyCode " & _
                        ",'' As CompanyName " & _
                        ",'' As Address1 " & _
                        ",'' As Address2 " & _
                        ",'' As C_City " & _
                        ",'' As C_State " & _
                        ",'' As C_Country " & _
                        ",'' As Phone1 " & _
                        ",'' As Phone2 " & _
                        ",'' As Phone3 " & _
                        ",'' As Fax " & _
                        ",'' As C_Email " & _
                        ",'' As Website " & _
                        ",'' As RegNo1 " & _
                        ",'' As RegNo2 " & _
                        ",'' As TinNo " & _
                        ",'' As Reference "

                'S.SANDEEP [05-Apr-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
                StrQ &= ",'' AS CompanyLogo "
                'S.SANDEEP [05-Apr-2018] -- END

                dsList = objdataOperation.ExecQuery(StrQ, strListName)

                If objdataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objdataOperation.ErrorNumber & ": " & objdataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'Sandeep [ 02 Oct 2010 ] -- End 

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            exForce = Nothing
        End Try
    End Function

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function GetEmployeeData(ByVal strEmpId As String, ByVal intModuleRefId As Integer, Optional ByVal strListName As String = "List") As DataSet
    Private Function CompanyData(ByVal intCompanyId As Integer) As String
        Dim StrValue As String = String.Empty
        Try
            Dim objCompany As New clsCompany_Master
            objCompany._Companyunkid = intCompanyId

            StrValue = "    ,'" & Now.Date & "' As Today " & _
                       "    ,'" & objCompany._Code & "' As CompanyCode " & _
                       "    ,'" & objCompany._Name & "' As CompanyName " & _
                       "    ,'" & objCompany._Address1 & "' As Address1 " & _
                       "    ,'" & objCompany._Address2 & "' As Address2 " & _
                       "    ,'" & objCompany._City_Name & "' As C_City " & _
                       "    ,'" & objCompany._State_Name & "' As C_State " & _
                       "    ,'" & objCompany._Country_Name & "' As C_Country " & _
                       "    ,'" & objCompany._Phone1 & "' As Phone1 " & _
                       "    ,'" & objCompany._Phone2 & "' As Phone2 " & _
                       "    ,'" & objCompany._Phone3 & "' As Phone3 " & _
                       "    ,'" & objCompany._Fax & "' As Fax " & _
                       "    ,'" & objCompany._Email & "' As C_Email " & _
                       "    ,'" & objCompany._Website & "' As Website " & _
                       "    ,'" & objCompany._Registerdno & "' As RegNo1 " & _
                       "    ,'" & objCompany._Vatno & "' As RegNo2 " & _
                       "    ,'" & objCompany._Tinno & "' As TinNo " & _
                       "    ,'" & objCompany._Reference & "' As Reference "

            'S.SANDEEP [05-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
            mCompany_Logo = eZeeDataType.image2Data(objCompany._Image)
            'S.SANDEEP [05-Apr-2018] -- END

            objCompany = Nothing
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return StrValue
    End Function

    Public Function GetEmployeeData(ByVal strEmpId As String, _
                                    ByVal intModuleRefId As Integer, _
                                    ByVal mdtAsOnDate As DateTime, _
                                    ByVal intCompanyUnkid As Integer, _
                                    ByVal strDatabaseName As String, _
                                    Optional ByVal strListName As String = "List", _
                                    Optional ByVal mdtTable As DataTable = Nothing _
                                    ) As DataSet 'S.SANDEEP |09-APR-2019| -- START {strDatabaseName} -- END
        'Sohail (17 Feb 2021) - [mdtTable]
        'S.SANDEEP [04 JUN 2015] -- END

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            Dim objdataOperation As New clsDataOperation
            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim mdicIsDataPresent As New Dictionary(Of Integer, Integer)
            'S.SANDEEP [ 28 FEB 2012 ] -- END

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim strCommonData As String = String.Empty
            strCommonData = CompanyData(intCompanyUnkid)
            'S.SANDEEP [04 JUN 2015] -- END

            Select Case intModuleRefId
                Case enImg_Email_RefId.Employee_Module
                    If strEmpId.Trim.Length > 0 Then
                        Dim dTable As DataTable = Nothing
                        For Each StrId As String In strEmpId.Split(",")
                            'S.SANDEEP [ 28 FEB 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            If mdicIsDataPresent.ContainsKey(CInt(StrId)) Then Continue For
                            mdicIsDataPresent.Add(CInt(StrId), CInt(StrId))
                            'S.SANDEEP [ 28 FEB 2012 ] -- END

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'StrQ = "SELECT " & _
                            '             " Shift " & _
                            '             ",Title " & _
                            '             ",EmpCode " & _
                            '             ",FirstName " & _
                            '             ",SurName " & _
                            '             ",OtherName " & _
                            '             ",Email " & _
                            '             ",BirthDate " & _
                            '             ",AnniversaryDate " & _
                            '             ",EmpAddress1 " & _
                            '             ",EmpAddress2 " & _
                            '             ",City " & _
                            '             ",State " & _
                            '             ",Country " & _
                            '             ",SuspendedFrom " & _
                            '             ",SuspendedTo " & _
                            '             ",ProbationFrom " & _
                            '             ",ProbationTo " & _
                            '             ",TerminationDate " & _
                            '             ",RetirementDate " & _
                            '             ",ConfirmationDate " & _
                            '             ",EmploymentLeavingDate " & _
                            '             ",ApponintmentDate " & _
                            '             ",Station AS Branch " & _
                            '             ",DepartmentGroup " & _
                            '             ",Department " & _
                            '             ",SectionGroup " & _
                            '             ",Section " & _
                            '             ",UnitGroup " & _
                            '             ",Unit " & _
                            '             ",Team " & _
                            '             ",JobGroup " & _
                            '             ",Job " & _
                            '             ",ClassGroup " & _
                            '             ",Class " & _
                            '             ",CostCenter " & _
                            '             ",ReportToJobGroup " & _
                            '             ",ReportToJob " & _
                            '             ",OldBranch " & _
                            '             ",OldDepartmentGroup " & _
                            '             ",OldDepartment " & _
                            '             ",OldSectionGroup " & _
                            '             ",OldSection " & _
                            '             ",OldUnitGroup " & _
                            '             ",OldUnit " & _
                            '             ",OldTeam " & _
                            '             ",OldJobGroup " & _
                            '             ",OldJob " & _
                            '             ",OldClassGroup " & _
                            '             ",OldClass " & _
                            '             ",OldCostCenter " & _
                            '             ",OldReportToJobGroup " & _
                            '             ",OldReportToJob " & _
                            '             ",OldGradeGroup " & _
                            '             ",OldGrade " & _
                            '             ",OldGradeLevel " & _
                            '             ",OldSalary " & _
                            '             ",CurrentGradeGroup " & _
                            '             ",CurrentGrade " & _
                            '             ",CurrentGradeLevel " & _
                            '             ",CurrentSalary " & _
                            '             ",NextGradeGroup " & _
                            '             ",NextGrade " & _
                            '             ",NextGradeLevel " & _
                            '             ",NextSalary " & _
                            '             ",UserName " & _
                            '             ",Password " & _
                            '             ",Original_Password " & _
                            '             ",ReportingCode " & _
                            '             ",ReportingFirstname " & _
                            '             ",ReportingSurname " & _
                            '             ",ReportingOthername " & _
                            '             ",'" & Now.Date & "' As Today " & _
                            '             ",'" & Company._Object._Code & "' As CompanyCode " & _
                            '             ",'" & Company._Object._Name & "' As CompanyName " & _
                            '             ",'" & Company._Object._Address1 & "' As Address1 " & _
                            '             ",'" & Company._Object._Address2 & "' As Address2 " & _
                            '             ",'" & Company._Object._City_Name & "' As C_City " & _
                            '             ",'" & Company._Object._State_Name & "' As C_State " & _
                            '             ",'" & Company._Object._Country_Name & "' As C_Country " & _
                            '             ",'" & Company._Object._Phone1 & "' As Phone1 " & _
                            '             ",'" & Company._Object._Phone2 & "' As Phone2 " & _
                            '             ",'" & Company._Object._Phone3 & "' As Phone3 " & _
                            '             ",'" & Company._Object._Fax & "' As Fax " & _
                            '             ",'" & Company._Object._Email & "' As Email " & _
                            '             ",'" & Company._Object._Website & "' As Website " & _
                            '             ",'" & Company._Object._Registerdno & "' As RegNo1 " & _
                            '             ",'" & Company._Object._Vatno & "' As RegNo2 " & _
                            '             ",'" & Company._Object._Tinno & "' As TinNo " & _
                            '             ",'" & Company._Object._Reference & "' As Reference " & _
                            '             ",EmpId " & _
                            '        "FROM " & _
                            '        "( " & _
                            '             "SELECT " & _
                            '                   "ISNULL(tnashift_master.shiftname,'') AS Shift " & _
                            '                  ",ISNULL(cfcommon_master.name,'') AS Title " & _
                            '                  ",ISNULL(hremployee_master.employeecode,'') AS EmpCode " & _
                            '                  ",ISNULL(hremployee_master.firstname,'') AS FirstName " & _
                            '                  ",ISNULL(hremployee_master.surname,'') AS SurName " & _
                            '                  ",ISNULL(hremployee_master.othername,'') AS OtherName " & _
                            '                  ",ISNULL(hremployee_master.email,'') AS Email " & _
                            '                  ",ISNULL(CONVERT(CHAR(8),hremployee_master.birthdate,112),'') AS BirthDate " & _
                            '                  ",ISNULL(CONVERT(CHAR(8),hremployee_master.anniversary_date,112),'') AS AnniversaryDate " & _
                            '                  ",ISNULL(hremployee_master.present_address1,'') AS EmpAddress1 " & _
                            '                  ",ISNULL(hremployee_master.present_address2,'') AS EmpAddress2 " & _
                            '                  ",ISNULL(hrmsConfiguration..cfcity_master .name,'') AS City " & _
                            '                  ",ISNULL(hrmsConfiguration..cfstate_master .name,'') AS State " & _
                            '                  ",ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
                            '                  ",ISNULL(CONVERT(CHAR(8),hremployee_master.suspended_from_date,112),'') AS SuspendedFrom " & _
                            '                  ",ISNULL(CONVERT(CHAR(8),hremployee_master.suspended_to_date,112),'') AS SuspendedTo " & _
                            '                  ",ISNULL(CONVERT(CHAR(8),hremployee_master.probation_from_date,112),'') AS ProbationFrom " & _
                            '                  ",ISNULL(CONVERT(CHAR(8),hremployee_master.probation_to_date,112),'') AS ProbationTo " & _
                            '                  ",ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),'') AS TerminationDate " & _
                            '                  ",ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),'') AS RetirementDate " & _
                            '                  ",ISNULL(CONVERT(CHAR(8),hremployee_master.confirmation_date,112),'') AS ConfirmationDate " & _
                            '                  ",ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112),'') AS EmploymentLeavingDate " & _
                            '                  ",ISNULL(CONVERT(CHAR(8),hremployee_master.appointeddate,112),'') AS ApponintmentDate " & _
                            '                  ",ISNULL(hrstation_master.name,'') AS Station " & _
                            '                  ",ISNULL(hrdepartment_group_master.name,'') AS DepartmentGroup " & _
                            '                  ",ISNULL(hrdepartment_master.name,'') AS Department " & _
                            '                  ",ISNULL(hrsectiongroup_master.name,'') AS SectionGroup " & _
                            '                  ",ISNULL(hrsection_master.name,'') AS Section " & _
                            '                  ",ISNULL(hrunitgroup_master.name,'') AS UnitGroup " & _
                            '                  ",ISNULL(hrunit_master.name,'') AS Unit " & _
                            '                  ",ISNULL(hrteam_master.name,'') AS Team " & _
                            '                  ",ISNULL(hrjobgroup_master.name,'') AS JobGroup " & _
                            '                  ",ISNULL(hrjob_master.job_name,'') AS Job " & _
                            '                  ",ISNULL(hrclassgroup_master.name,'') AS ClassGroup " & _
                            '                  ",ISNULL(hrclasses_master.name,'') AS Class " & _
                            '                  ",ISNULL(prcostcenter_master.costcentername,'') AS CostCenter " & _
                            '                  ",ISNULL(ReportGroup.name,'') AS ReportToJobGroup " & _
                            '                  ",ISNULL(Report.job_name,'') AS ReportToJob " & _
                            '                  ",ISNULL(hremployee_master.displayname,'') AS UserName " & _
                            '                  ",ISNULL(hremployee_master.password,'') AS Password " & _
                            '                  ",'' AS Original_Password " & _
                            '                  ",hremployee_master.employeeunkid AS EmpId " & _
                            '                  ",ISNULL(rEmp.employeecode,'') AS ReportingCode " & _
                            '                  ",ISNULL(rEmp.firstname,'') AS ReportingFirstname " & _
                            '                  ",ISNULL(rEmp.surname,'') AS ReportingSurname " & _
                            '                  ",ISNULL(rEmp.othername,'') AS ReportingOthername " & _
                            '             "FROM hremployee_master " & _
                            '                  "LEFT JOIN hremployee_reportto ON hremployee_master.employeeunkid = hremployee_reportto.employeeunkid AND ishierarchy = 1 AND isvoid = 0 " & _
                            '                  "LEFT JOIN hremployee_master AS rEmp ON hremployee_reportto.reporttoemployeeunkid = rEmp.employeeunkid " & _
                            '                  "LEFT JOIN hrclassgroup_master ON hremployee_master.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
                            '                  "LEFT JOIN hrjobgroup_master ON hremployee_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
                            '                  "LEFT JOIN hrteam_master ON hremployee_master.teamunkid = hrteam_master.teamunkid " & _
                            '                  "LEFT JOIN hrunit_master ON hremployee_master.unitunkid = hrunit_master.unitunkid " & _
                            '                  "LEFT JOIN hrunitgroup_master ON hremployee_master.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
                            '                  "LEFT JOIN hrsectiongroup_master ON hremployee_master.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
                            '                  "LEFT JOIN hrdepartment_group_master ON hremployee_master.deptgroupunkid = hrdepartment_group_master.deptgroupunkid " & _
                            '                  "LEFT JOIN prcostcenter_master ON hremployee_master.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                            '                  "LEFT JOIN hrclasses_master ON hremployee_master.classunkid = hrclasses_master.classesunkid " & _
                            '                  "LEFT JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
                            '                  "LEFT JOIN hrsection_master ON hremployee_master.sectionunkid = hrsection_master.sectionunkid " & _
                            '                  "LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                            '                  "LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid " & _
                            '                  "LEFT JOIN hrmsConfiguration..cfstate_master  ON hremployee_master.present_stateunkid= hrmsConfiguration..cfstate_master .stateunkid " & _
                            '                  "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.present_countryunkid " & _
                            '                  "LEFT JOIN hrmsConfiguration..cfzipcode_master  ON hremployee_master.present_postcodeunkid = hrmsConfiguration..cfzipcode_master .zipcodeunkid " & _
                            '                  "LEFT JOIN hrmsConfiguration..cfcity_master  ON hremployee_master.present_post_townunkid = hrmsConfiguration..cfcity_master .cityunkid " & _
                            '                  "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.titleunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.TITLE & "' " & _
                            '                  "LEFT JOIN tnashift_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
                            '                  "LEFT JOIN hrjob_master AS Report ON hrjob_master.report_tounkid = Report.jobunkid " & _
                            '                  "LEFT JOIN hrjobgroup_master AS ReportGroup ON ReportGroup.jobgroupunkid = Report.jobgroupunkid " & _
                            '             "WHERE hremployee_master.employeeunkid IN (" & CInt(StrId) & ") " & _
                            '        ") AS Main " & _
                            '        "LEFT JOIN " & _
                            '        "( " & _
                            '             "SELECT " & _
                            '              "ISNULL(hrstation_master.name,'') AS OldBranch " & _
                            '             ",ISNULL(hrdepartment_group_master.name,'') AS OldDepartmentGroup " & _
                            '             ",ISNULL(hrdepartment_master.name,'') AS OldDepartment " & _
                            '             ",ISNULL(hrsectiongroup_master.name,'') AS OldSectionGroup " & _
                            '             ",ISNULL(hrsection_master.name,'') AS OldSection " & _
                            '             ",ISNULL(hrunitgroup_master.name,'') AS OldUnitGroup " & _
                            '             ",ISNULL(hrunit_master.name,'') AS OldUnit " & _
                            '             ",ISNULL(hrteam_master.name,'') AS OldTeam " & _
                            '             ",ISNULL(hrjobgroup_master.name,'') AS OldJobGroup " & _
                            '             ",ISNULL(hrjob_master.job_name,'') AS OldJob " & _
                            '             ",ISNULL(hrclassgroup_master.name,'') AS OldClassGroup " & _
                            '             ",ISNULL(hrclasses_master.name,'') AS OldClass " & _
                            '             ",ISNULL(prcostcenter_master.costcentername,'') AS OldCostCenter " & _
                            '             ",ISNULL(ReportGroup.name,'') AS OldReportToJobGroup " & _
                            '             ",ISNULL(Report.job_name,'') AS OldReportToJob " & _
                            '             ",employeeunkid " & _
                            '        "FROM " & _
                            '        "( " & _
                            '              "SELECT " & _
                            '                   "ISNULL(a.stationunkid,hremployee_master.stationunkid) AS stationunkid " & _
                            '                  ",ISNULL(b.deptgroupunkid,hremployee_master.deptgroupunkid) AS deptgroupunkid " & _
                            '                  ",ISNULL(c.departmentunkid,hremployee_master.departmentunkid) AS departmentunkid " & _
                            '                  ",ISNULL(d.sectiongroupunkid,hremployee_master.sectiongroupunkid) AS sectiongroupunkid " & _
                            '                  ",ISNULL(e.sectionunkid,hremployee_master.sectionunkid) AS sectionunkid " & _
                            '                  ",ISNULL(f.unitgroupunkid,hremployee_master.unitgroupunkid) AS unitgroupunkid " & _
                            '                  ",ISNULL(g.unitunkid,hremployee_master.unitunkid) AS unitunkid " & _
                            '                  ",ISNULL(h.teamunkid,hremployee_master.teamunkid) AS teamunkid " & _
                            '                  ",ISNULL(i.jobgroupunkid,hremployee_master.jobgroupunkid) AS jobgroupunkid " & _
                            '                  ",ISNULL(k.jobunkid,hremployee_master.jobunkid) AS jobunkid " & _
                            '                  ",ISNULL(l.classgroupunkid,hremployee_master.classgroupunkid) AS classgroupunkid " & _
                            '                  ",ISNULL(m.classunkid,hremployee_master.classunkid) AS classunkid " & _
                            '                  ",ISNULL(n.costcenterunkid,hremployee_master.costcenterunkid) AS costcenterunkid " & _
                            '                  ",hremployee_master.employeeunkid " & _
                            '             "FROM hremployee_master " & _
                            '             "LEFT JOIN " & _
                            '             "( " & _
                            '                  "SELECT TOP 1 " & _
                            '                        "athremployee_master.employeeunkid " & _
                            '                       ",athremployee_master.stationunkid " & _
                            '                       ",auditdatetime " & _
                            '                  "FROM athremployee_master " & _
                            '                  "JOIN " & _
                            '                  "( " & _
                            '                       "SELECT " & _
                            '                            "employeeunkid,stationunkid " & _
                            '                       "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  ") AS Station ON athremployee_master.employeeunkid = Station.employeeunkid AND athremployee_master.stationunkid <> Station.stationunkid " & _
                            '                  "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  "ORDER BY auditdatetime DESC " & _
                            '             ") AS a ON hremployee_master.employeeunkid = a.employeeunkid " & _
                            '             "LEFT JOIN " & _
                            '             "( " & _
                            '                  "SELECT TOP 1 " & _
                            '                        "athremployee_master.employeeunkid " & _
                            '                       ",athremployee_master.deptgroupunkid " & _
                            '                       ",auditdatetime " & _
                            '                  "FROM athremployee_master " & _
                            '                  "JOIN " & _
                            '                  "( " & _
                            '                       "SELECT " & _
                            '                            "employeeunkid,deptgroupunkid " & _
                            '                       "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  ") AS DeptGrp ON athremployee_master.employeeunkid = DeptGrp.employeeunkid AND athremployee_master.deptgroupunkid <> DeptGrp.deptgroupunkid " & _
                            '                  "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  "ORDER BY auditdatetime DESC " & _
                            '             ")AS b ON hremployee_master.employeeunkid = b.employeeunkid " & _
                            '             "LEFT JOIN " & _
                            '             "( " & _
                            '                  "SELECT TOP 1 " & _
                            '                        "athremployee_master.employeeunkid " & _
                            '                       ",athremployee_master.departmentunkid " & _
                            '                       ",auditdatetime " & _
                            '                  "FROM athremployee_master " & _
                            '                  "JOIN " & _
                            '                  "( " & _
                            '                       "SELECT " & _
                            '                            "employeeunkid,departmentunkid " & _
                            '                       "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  ") AS Dept ON athremployee_master.employeeunkid = Dept.employeeunkid AND athremployee_master.departmentunkid <> Dept.departmentunkid " & _
                            '                  "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  "ORDER BY auditdatetime DESC " & _
                            '             ") AS c ON hremployee_master.employeeunkid=c.employeeunkid " & _
                            '             "LEFT JOIN " & _
                            '             "( " & _
                            '                  "SELECT TOP 1 " & _
                            '                        "athremployee_master.employeeunkid " & _
                            '                       ",athremployee_master.sectiongroupunkid " & _
                            '                       ",auditdatetime " & _
                            '                  "FROM athremployee_master " & _
                            '                  "JOIN " & _
                            '                  "( " & _
                            '                       "SELECT " & _
                            '                            "employeeunkid,sectiongroupunkid " & _
                            '                       "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  ") AS SG ON athremployee_master.employeeunkid = SG.employeeunkid AND athremployee_master.sectiongroupunkid <> SG.sectiongroupunkid " & _
                            '                  "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  "ORDER BY auditdatetime DESC " & _
                            '             ") AS d ON hremployee_master.employeeunkid = d.employeeunkid " & _
                            '             "LEFT JOIN " & _
                            '             "( " & _
                            '                  "SELECT TOP 1 " & _
                            '                        "athremployee_master.employeeunkid " & _
                            '                       ",athremployee_master.sectionunkid " & _
                            '                       ",auditdatetime " & _
                            '                  "FROM athremployee_master " & _
                            '                  "JOIN " & _
                            '                  "( " & _
                            '                       "SELECT " & _
                            '                            "employeeunkid,sectionunkid " & _
                            '                       "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  ") AS Sec ON athremployee_master.employeeunkid = Sec.employeeunkid AND athremployee_master.sectionunkid <> Sec.sectionunkid " & _
                            '                  "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  "ORDER BY auditdatetime DESC " & _
                            '             ")as e ON hremployee_master.employeeunkid = e.employeeunkid " & _
                            '             "LEFT JOIN " & _
                            '             "( " & _
                            '                  "SELECT TOP 1 " & _
                            '                        "athremployee_master.employeeunkid " & _
                            '                       ",athremployee_master.unitgroupunkid " & _
                            '                       ",auditdatetime " & _
                            '                  "FROM athremployee_master " & _
                            '                  "JOIN " & _
                            '                  "( " & _
                            '                       "SELECT " & _
                            '                            "employeeunkid,unitgroupunkid " & _
                            '                       "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  ") AS UG ON athremployee_master.employeeunkid = UG.employeeunkid AND athremployee_master.unitgroupunkid <> UG.unitgroupunkid " & _
                            '                  "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  "ORDER BY auditdatetime DESC " & _
                            '             ")AS f ON hremployee_master.employeeunkid=f.employeeunkid " & _
                            '             "LEFT JOIN " & _
                            '             "( " & _
                            '                  "SELECT TOP 1 " & _
                            '                        "athremployee_master.employeeunkid " & _
                            '                       ",athremployee_master.unitunkid " & _
                            '                       ",auditdatetime " & _
                            '                  "FROM athremployee_master " & _
                            '                  "JOIN " & _
                            '                  "( " & _
                            '                       "SELECT " & _
                            '                            "employeeunkid,unitunkid " & _
                            '                       "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  ") AS Unit ON athremployee_master.employeeunkid = Unit.employeeunkid AND athremployee_master.unitunkid <> Unit.unitunkid " & _
                            '                  "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  "ORDER BY auditdatetime DESC " & _
                            '             ")AS g ON hremployee_master.employeeunkid=g.employeeunkid " & _
                            '             "LEFT JOIN " & _
                            '             "( " & _
                            '                  "SELECT TOP 1 " & _
                            '                        "athremployee_master.employeeunkid " & _
                            '                       ",athremployee_master.teamunkid " & _
                            '                       ",auditdatetime " & _
                            '                  "FROM athremployee_master " & _
                            '                  "JOIN " & _
                            '                  "( " & _
                            '                       "SELECT " & _
                            '                            "employeeunkid,teamunkid " & _
                            '                       "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  ") AS Team ON athremployee_master.employeeunkid = Team.employeeunkid AND athremployee_master.teamunkid <> Team.teamunkid " & _
                            '                  "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  "ORDER BY auditdatetime DESC " & _
                            '             ") AS h ON hremployee_master.employeeunkid=h.employeeunkid " & _
                            '             "LEFT JOIN " & _
                            '             "( " & _
                            '                  "SELECT TOP 1 " & _
                            '                        "athremployee_master.employeeunkid " & _
                            '                       ",athremployee_master.jobgroupunkid " & _
                            '                       ",auditdatetime " & _
                            '                  "FROM athremployee_master " & _
                            '                  "JOIN " & _
                            '                  "( " & _
                            '                       "SELECT " & _
                            '                            "employeeunkid,jobgroupunkid " & _
                            '                       "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  ") AS JG ON athremployee_master.employeeunkid = JG.employeeunkid AND athremployee_master.jobgroupunkid <> JG.jobgroupunkid " & _
                            '                  "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  "ORDER BY auditdatetime DESC " & _
                            '             ") AS i ON hremployee_master.employeeunkid = i.employeeunkid " & _
                            '             "LEFT JOIN " & _
                            '             "( " & _
                            '                  "SELECT TOP 1 " & _
                            '                        "athremployee_master.employeeunkid " & _
                            '                       ",athremployee_master.jobunkid " & _
                            '                       ",auditdatetime " & _
                            '                  "FROM athremployee_master " & _
                            '                  "JOIN " & _
                            '                  "( " & _
                            '                       "SELECT " & _
                            '                            "employeeunkid,jobunkid " & _
                            '                       "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  ") AS Job ON athremployee_master.employeeunkid = Job.employeeunkid AND athremployee_master.jobunkid <> Job.jobunkid " & _
                            '                  "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  "ORDER BY auditdatetime DESC " & _
                            '             ")AS k ON hremployee_master.employeeunkid = k.employeeunkid " & _
                            '             "LEFT JOIN " & _
                            '             "( " & _
                            '                  "SELECT TOP 1 " & _
                            '                        "athremployee_master.employeeunkid " & _
                            '                       ",athremployee_master.classgroupunkid " & _
                            '                       ",auditdatetime " & _
                            '                  "FROM athremployee_master " & _
                            '                  "JOIN " & _
                            '                  "( " & _
                            '                       "SELECT " & _
                            '                            "employeeunkid,classgroupunkid " & _
                            '                       "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  ") AS CG ON athremployee_master.employeeunkid = CG.employeeunkid AND athremployee_master.classgroupunkid <> CG.classgroupunkid " & _
                            '                  "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  "ORDER BY auditdatetime DESC " & _
                            '             ")AS l ON hremployee_master.employeeunkid = l.employeeunkid " & _
                            '             "LEFT JOIN " & _
                            '             "( " & _
                            '                  "SELECT TOP 1 " & _
                            '                        "athremployee_master.employeeunkid " & _
                            '                       ",athremployee_master.classunkid " & _
                            '                       ",auditdatetime " & _
                            '                  "FROM athremployee_master " & _
                            '                  "JOIN " & _
                            '                  "( " & _
                            '                       "SELECT " & _
                            '                            "employeeunkid,classunkid " & _
                            '                       "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  ") AS CLS ON athremployee_master.employeeunkid = CLS.employeeunkid AND athremployee_master.classunkid <> CLS.classunkid " & _
                            '                  "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  "ORDER BY auditdatetime DESC " & _
                            '             ")AS m ON hremployee_master.employeeunkid = m.employeeunkid " & _
                            '             "LEFT JOIN " & _
                            '             "( " & _
                            '                  "SELECT TOP 1 " & _
                            '                        "athremployee_master.employeeunkid " & _
                            '                       ",athremployee_master.costcenterunkid " & _
                            '                       ",auditdatetime " & _
                            '                  "FROM athremployee_master " & _
                            '                  "JOIN " & _
                            '                  "( " & _
                            '                       "SELECT " & _
                            '                            "employeeunkid,costcenterunkid " & _
                            '                       "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  ") AS CC ON athremployee_master.employeeunkid = CC.employeeunkid AND athremployee_master.costcenterunkid <> CC.costcenterunkid " & _
                            '                  "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  "ORDER BY auditdatetime DESC " & _
                            '             ")AS n ON hremployee_master.employeeunkid = n.employeeunkid " & _
                            '             "LEFT JOIN " & _
                            '             "( " & _
                            '                  "SELECT TOP 1 " & _
                            '                        "athremployee_master.employeeunkid " & _
                            '                       ",athremployee_master.gradegroupunkid " & _
                            '                       ",auditdatetime " & _
                            '                  "FROM athremployee_master " & _
                            '                  "JOIN " & _
                            '                  "( " & _
                            '                       "SELECT " & _
                            '                            "employeeunkid,gradegroupunkid " & _
                            '                       "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  ") AS GG ON athremployee_master.employeeunkid = GG.employeeunkid AND athremployee_master.gradegroupunkid <> GG.gradegroupunkid " & _
                            '                  "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
                            '                  "ORDER BY auditdatetime DESC " & _
                            '             ") AS o ON hremployee_master.employeeunkid = o.employeeunkid " & _
                            '             "WHERE hremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
                            '        ") AS OLD " & _
                            '             "LEFT JOIN hrclassgroup_master ON OLD.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
                            '             "LEFT JOIN hrjobgroup_master ON OLD.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
                            '             "LEFT JOIN hrteam_master ON OLD.teamunkid = hrteam_master.teamunkid " & _
                            '             "LEFT JOIN hrunit_master ON OLD.unitunkid = hrunit_master.unitunkid " & _
                            '             "LEFT JOIN hrunitgroup_master ON OLD.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
                            '             "LEFT JOIN hrsectiongroup_master ON OLD.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
                            '             "LEFT JOIN hrdepartment_group_master ON OLD.deptgroupunkid = hrdepartment_group_master.deptgroupunkid " & _
                            '             "LEFT JOIN prcostcenter_master ON OLD.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                            '             "LEFT JOIN hrclasses_master ON OLD.classunkid = hrclasses_master.classesunkid " & _
                            '             "LEFT JOIN hrjob_master ON OLD.jobunkid = hrjob_master.jobunkid " & _
                            '             "LEFT JOIN hrsection_master ON OLD.sectionunkid = hrsection_master.sectionunkid " & _
                            '             "LEFT JOIN hrdepartment_master ON OLD.departmentunkid = hrdepartment_master.departmentunkid " & _
                            '             "LEFT JOIN hrstation_master ON OLD.stationunkid = hrstation_master.stationunkid " & _
                            '             "LEFT JOIN hrjob_master AS Report ON hrjob_master.report_tounkid = Report.jobunkid " & _
                            '             "LEFT JOIN hrjobgroup_master AS ReportGroup ON ReportGroup.jobgroupunkid = Report.jobgroupunkid " & _
                            '        "WHERE 1 = 1 " & _
                            '        ") AS old_Allocation ON old_Allocation.employeeunkid = Main.EmpId " & _
                            '        "LEFT JOIN " & _
                            '        "( " & _
                            '             "SELECT TOP 1 " & _
                            '                   "employeeunkid " & _
                            '                  ",ISNULL(hrgradegroup_master.name,'') AS CurrentGradeGroup " & _
                            '                  ",ISNULL(hrgrade_master.name,'') AS CurrentGrade " & _
                            '                  ",ISNULL(hrgradelevel_master.name,'') AS CurrentGradeLevel " & _
                            '                  ",CAST(newscale AS NVARCHAR(MAX)) AS CurrentSalary " & _
                            '             "FROM prsalaryincrement_tran " & _
                            '                  "JOIN hrgradegroup_master ON prsalaryincrement_tran.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                            '                  "JOIN hrgrade_master ON prsalaryincrement_tran.gradeunkid = hrgrade_master.gradeunkid " & _
                            '                  "JOIN hrgradelevel_master ON prsalaryincrement_tran.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
                            '             "WHERE prsalaryincrement_tran.employeeunkid = '" & CInt(StrId) & "' AND CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) <= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & "' " & _
                            '             "AND prsalaryincrement_tran.isapproved = 1 " & _
                            '             "ORDER BY prsalaryincrement_tran.incrementdate DESC " & _
                            '        ")AS CGrades ON CGrades.employeeunkid = Main.EmpId " & _
                            '        "LEFT JOIN " & _
                            '        "( " & _
                            '             "SELECT TOP 1 " & _
                            '         "employeeunkid " & _
                            '        ",ISNULL(hrgradegroup_master.name,'') AS OldGradeGroup " & _
                            '        ",ISNULL(hrgrade_master.name,'') AS OldGrade " & _
                            '        ",ISNULL(hrgradelevel_master.name,'') AS OldGradeLevel " & _
                            '        ",CAST(newscale AS NVARCHAR(MAX)) AS OldSalary " & _
                            '        "FROM " & _
                            '        "( " & _
                            '             "SELECT TOP 2 " & _
                            '                   "A.gradegroupunkid " & _
                            '                  ",a.gradeunkid " & _
                            '                  ",a.gradelevelunkid " & _
                            '                  ",a.newscale " & _
                            '                  ",a.incrementdate " & _
                            '                  ",a.employeeunkid " & _
                            '             "FROM hremployee_master " & _
                            '             "LEFT JOIN " & _
                            '             "( " & _
                            '                  "SELECT " & _
                            '                        "gradegroupunkid " & _
                            '                       ",gradeunkid " & _
                            '                       ",gradelevelunkid " & _
                            '                       ",newscale " & _
                            '                       ",employeeunkid " & _
                            '                       ",incrementdate " & _
                            '                  "FROM prsalaryincrement_tran " & _
                            '                  "WHERE prsalaryincrement_tran.employeeunkid = '" & CInt(StrId) & "' AND CONVERT(CHAR(8),prsalaryincrement_tran.incrementdate,112) <= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & "' " & _
                            '                  "AND prsalaryincrement_tran.isapproved = 1 " & _
                            '             ") AS A ON dbo.hremployee_master.employeeunkid = A.employeeunkid " & _
                            '             "WHERE hremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
                            '             "ORDER BY a.incrementdate DESC " & _
                            '             ") AS B " & _
                            '             "JOIN hrgradegroup_master ON B.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                            '             "JOIN hrgrade_master ON B.gradeunkid = hrgrade_master.gradeunkid " & _
                            '             "JOIN hrgradelevel_master ON B.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
                            '             "ORDER BY b.incrementdate ASC " & _
                            '        ") AS OGrades ON OGrades.employeeunkid = Main.EmpId " & _
                            '        "LEFT JOIN " & _
                            '        "( " & _
                            '             "SELECT TOP 1 " & _
                            '                   "employeeunkid " & _
                            '                  ",ISNULL(hrgradegroup_master.name,'') AS NextGradeGroup " & _
                            '                  ",ISNULL(hrgrade_master.name,'') AS NextGrade " & _
                            '                  ",ISNULL(hrgradelevel_master.name,'') AS NextGradeLevel " & _
                            '                  ",CAST(newscale AS NVARCHAR(MAX)) AS NextSalary " & _
                            '             "FROM prsalaryincrement_tran " & _
                            '                  "JOIN hrgradegroup_master ON prsalaryincrement_tran.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                            '                  "JOIN hrgrade_master ON prsalaryincrement_tran.gradeunkid = hrgrade_master.gradeunkid " & _
                            '                  "JOIN hrgradelevel_master ON prsalaryincrement_tran.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
                            '             "WHERE employeeunkid = '" & CInt(StrId) & "' AND CONVERT(CHAR(8),incrementdate,112) > '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime) & "' " & _
                            '             "AND prsalaryincrement_tran.isapproved = 1 " & _
                            '        ") AS FGrades ON FGrades.employeeunkid = Main.EmpId "

                            StrQ = "DECLARE @OTrfDate AS CHAR(8),@OCatDate AS CHAR(8),@GrdsDate AS CHAR(8),@OCCRDate AS CHAR(8) " & _
                                   "SET @OTrfDate = (SELECT CONVERT(CHAR(8),(SELECT (MAX(effectivedate) - 1) FROM hremployee_transfer_tran WHERE isvoid = 0 AND employeeunkid = '" & CInt(StrId) & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' ) ,112)) " & _
                                   "SET @OCatDate = (SELECT CONVERT(CHAR(8),(SELECT (MAX(effectivedate) - 1) FROM hremployee_categorization_tran WHERE isvoid = 0 AND employeeunkid = '" & CInt(StrId) & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' ) ,112)) " & _
                                   "SET @OCCRDate = (SELECT CONVERT(CHAR(8),(SELECT (MAX(effectivedate) - 1) FROM hremployee_cctranhead_tran WHERE isvoid = 0 AND employeeunkid = '" & CInt(StrId) & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' ) ,112)) " & _
                                   "SET @GrdsDate = (SELECT CONVERT(CHAR(8),(SELECT (MAX(incrementdate) - 1) FROM prsalaryincrement_tran WHERE isvoid = 0 AND isapproved = 1 AND employeeunkid = '" & CInt(StrId) & "' AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' ) ,112)) " & _
                                   "SELECT " & _
                                   "     ISNULL(tnashift_master.shiftname,'') AS Shift " & _
                                   "    ,ISNULL(cfcommon_master.name,'') AS Title " & _
                                   "    ,hremployee_master.employeecode AS EmpCode " & _
                                   "    ,hremployee_master.firstname AS FirstName " & _
                                   "    ,hremployee_master.surname AS SurName " & _
                                   "    ,hremployee_master.othername AS OtherName " & _
                                   "    ,LTRIM(RTRIM(hremployee_master.email)) AS Email " & _
                                   "    ,ISNULL(CONVERT(CHAR(8),hremployee_master.birthdate,112),'') AS BirthDate " & _
                                   "    ,CASE WHEN ISNULL(hremployee_master.birthdate,'') = '' THEN '' " & _
                                   "          ELSE CAST((CAST(CONVERT(char(8), GETDATE(), 112) AS int) - CAST(CONVERT(char(8), hremployee_master.birthdate, 112) AS int)) / 10000 AS nvarchar(max)) " & _
                                   "     END AS [Completed birth Age] " & _
                                   "    ,CASE WHEN ISNULL(hremployee_master.birthdate,'') = '' THEN '' " & _
                                   "          ELSE CAST((CAST(CONVERT(char(8), GETDATE(), 112) AS int) - CAST(CONVERT(char(8), hremployee_master.birthdate, 112) AS int)) / 10000 + 1 AS nvarchar(max)) " & _
                                   "     END AS [Running birth Age] " & _
                                   "    ,ISNULL(CONVERT(CHAR(8),hremployee_master.anniversary_date,112),'') AS AnniversaryDate " & _
                                   "    ,ISNULL(hremployee_master.present_address1,'') AS EmpAddress1 " & _
                                   "    ,ISNULL(hremployee_master.present_address2,'') AS EmpAddress2 " & _
                                   "    ,ISNULL(hrmsConfiguration..cfcity_master .name,'') AS City " & _
                                   "    ,ISNULL(hrmsConfiguration..cfstate_master .name,'') AS State " & _
                                   "    ,ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
                                   "    ,ISNULL(CONVERT(CHAR(8),SuspDt.date1,112),'') AS SuspendedFrom " & _
                                   "    ,ISNULL(CONVERT(CHAR(8),SuspDt.date2,112),'') AS SuspendedTo " & _
                                   "    ,ISNULL(CONVERT(CHAR(8),PrbDt.date1,112),'') AS ProbationFrom " & _
                                   "    ,ISNULL(CONVERT(CHAR(8),PrbDt.date2,112),'') AS ProbationTo " & _
                                   "    ,ISNULL(CONVERT(CHAR(8),EocDt.date2,112),'') AS TerminationDate " & _
                                   "    ,ISNULL(CONVERT(CHAR(8),Retr.date1,112),'') AS RetirementDate " & _
                                   "    ,ISNULL(CONVERT(CHAR(8),CnfDt.date1,112),'') AS ConfirmationDate " & _
                                   "    ,ISNULL(CONVERT(CHAR(8),EocDt.date1,112),'') AS EmploymentLeavingDate " & _
                                   "    ,ISNULL(CONVERT(CHAR(8),hremployee_master.appointeddate,112),'') AS ApponintmentDate " & _
                                   "    ,ISNULL(CST.name,'') AS Branch " & _
                                   "    ,ISNULL(CDG.name,'') AS DepartmentGroup " & _
                                   "    ,ISNULL(CDT.name,'') AS Department " & _
                                   "    ,ISNULL(CSG.name,'') AS SectionGroup " & _
                                   "    ,ISNULL(CSC.name,'') AS Section " & _
                                   "    ,ISNULL(CUG.name,'') AS UnitGroup " & _
                                   "    ,ISNULL(CUT.name,'') AS Unit " & _
                                   "    ,ISNULL(CTM.name,'') AS Team " & _
                                   "    ,ISNULL(CCG.name,'') AS ClassGroup " & _
                                   "    ,ISNULL(CCM.name,'') AS Class " & _
                                   "    ,ISNULL(OST.name,'') AS OldBranch " & _
                                   "    ,ISNULL(ODG.name,'') AS OldDepartmentGroup " & _
                                   "    ,ISNULL(ODT.name,'') AS OldDepartment " & _
                                   "    ,ISNULL(OSG.name,'') AS OldSectionGroup " & _
                                   "    ,ISNULL(OSC.name,'') AS OldSection " & _
                                   "    ,ISNULL(OUG.name,'') AS OldUnitGroup " & _
                                   "    ,ISNULL(OUM.name,'') AS OldUnit " & _
                                   "    ,ISNULL(OTM.name,'') AS OldTeam " & _
                                   "    ,ISNULL(OCG.name,'') AS OldClassGroup " & _
                                   "    ,ISNULL(OCM.name,'') AS OldClass " & _
                                   "    ,ISNULL(CJG.name,'') AS JobGroup " & _
                                   "    ,ISNULL(CJB.job_name,'') AS Job " & _
                                   "    ,ISNULL(OJG.name,'') AS OldJobGroup " & _
                                   "    ,ISNULL(OJB.job_name,'') AS OldJob " & _
                                   "    ,ISNULL(CGG.name,'') AS CurrentGradeGroup " & _
                                   "    ,ISNULL(CGM.name,'') AS CurrentGrade " & _
                                   "    ,ISNULL(CGL.name,'') AS CurrentGradeLevel " & _
                                   "    ,ISNULL(Grds.Scale, 0) AS CurrentSalary " & _
                                   "    ,ISNULL(OGG.name,'') AS OldGradeGroup " & _
                                   "    ,ISNULL(OGM.name,'') AS OldGrade " & _
                                   "    ,ISNULL(OGL.name,'') AS OldGradeLevel " & _
                                   "    ,ISNULL(OGrds.Scale, 0) AS OldSalary " & _
                                   "    ,ISNULL(NGG.name,'') AS NextGradeGroup " & _
                                   "    ,ISNULL(NGM.name,'') AS NextGrade " & _
                                   "    ,ISNULL(NGL.name,'') AS NextGradeLevel " & _
                                   "    ,ISNULL(NGrds.Scale,'') AS NextSalary " & _
                                   "    ,ISNULL(CCN.costcentername,'') AS CostCenter " & _
                                   "    ,ISNULL(OCN.costcentername,'') AS OldCostCenter " & _
                                   "    ,ISNULL(rEmp.employeecode,'') AS ReportingCode " & _
                                   "    ,ISNULL(rEmp.firstname,'') AS ReportingFirstname " & _
                                   "    ,ISNULL(rEmp.surname,'') AS ReportingSurname " & _
                                   "    ,ISNULL(rEmp.othername,'') AS ReportingOthername " & _
                                   "    ,ISNULL(hremployee_master.displayname,'') AS UserName " & _
                                   "    ,ISNULL(hremployee_master.password,'') AS Password " & _
                                   "    ,'' AS Original_Password " & _
                                   strCommonData & " " & _
                                   "    ,hremployee_master.employeeunkid AS EmpId " & _
                                   "    ,ISNULL(JKR.[Key Duties And Responsibilities],'') AS [Key Duties And Responsibilities] " & _
                                   "    ,ISNULL(CASE WHEN hremployee_master.gender = 1 THEN @Male " & _
                                   "                 WHEN hremployee_master.gender = 2 THEN @Female " & _
                                   "            END,'') AS Gender " & _
                                   "    ,ISNULL(RJB.job_name,'') AS [Reporting To Job] " & _
                                   "    ,CASE WHEN ISNULL(CONVERT(CHAR(8), EocDt.date2, 112),'') = '' OR ISNULL(CONVERT(CHAR(8), EocDt.date1, 112),'') = '' THEN @Permanent " & _
                                   "     ELSE " & _
                                   "        ISNULL(CAST((CAST(CONVERT(CHAR(8),((CASE WHEN ISNULL(EocDt.date1,EocDt.date2) < ISNULL(EocDt.date2,EocDt.date1) THEN ISNULL(EocDt.date2,EocDt.date1) ELSE ISNULL(EocDt.date1,EocDt.date2) END)+1),112) AS INT)-CAST(CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS INT))/10000 AS NVARCHAR(MAX)),'') +' '+ @Years + " & _
                                   "        ISNULL(CAST(((DATEDIFF(MONTH,hremployee_master.appointeddate,((CASE WHEN ISNULL(EocDt.date1,EocDt.date2) < ISNULL(EocDt.date2,EocDt.date1) THEN ISNULL(EocDt.date2,EocDt.date1) ELSE ISNULL(EocDt.date1,EocDt.date2) END)+1))+12)%12 - CASE WHEN DAY(hremployee_master.appointeddate)>DAY(((CASE WHEN ISNULL(EocDt.date1,EocDt.date2) < ISNULL(EocDt.date2,EocDt.date1) THEN ISNULL(EocDt.date2,EocDt.date1) ELSE ISNULL(EocDt.date1,EocDt.date2) END)+1)) THEN 1 ELSE 0 END) AS NVARCHAR(MAX)),'') +' '+ @Months + " & _
                                   "        ISNULL(CAST((DATEDIFF(day,DATEADD(month,((DATEDIFF(MONTH,hremployee_master.appointeddate,((CASE WHEN ISNULL(EocDt.date1,EocDt.date2) < ISNULL(EocDt.date2,EocDt.date1) THEN ISNULL(EocDt.date2,EocDt.date1) ELSE ISNULL(EocDt.date1,EocDt.date2) END)+1))+12)%12 - CASE WHEN DAY(hremployee_master.appointeddate)>DAY(((CASE WHEN ISNULL(EocDt.date1,EocDt.date2) < ISNULL(EocDt.date2,EocDt.date1) THEN ISNULL(EocDt.date2,EocDt.date1) ELSE ISNULL(EocDt.date1,EocDt.date2) END)+1)) THEN 1 ELSE 0 END),DATEADD(YEAR,(CAST(CONVERT(CHAR(8),((CASE WHEN ISNULL(EocDt.date1,EocDt.date2) < ISNULL(EocDt.date2,EocDt.date1) THEN ISNULL(EocDt.date2,EocDt.date1) ELSE ISNULL(EocDt.date1,EocDt.date2) END)+1),112) AS INT)-CAST(CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS INT))/10000,hremployee_master.appointeddate)),((CASE WHEN ISNULL(EocDt.date1,EocDt.date2) < ISNULL(EocDt.date2,EocDt.date1) THEN ISNULL(EocDt.date2,EocDt.date1) ELSE ISNULL(EocDt.date1,EocDt.date2) END)+1))) AS NVARCHAR(MAX)),'') +' '+ @Days " & _
                                   "     END AS [Employment Period] " & _
                                   "    ,'' AS YearOfService " & _
                                   "    ,ISNULL(CONVERT(CHAR(8),EocDt.[EOC Effective Date],112),'') AS [EOC Effective Date] " & _
                                   "    ,ISNULL(CONVERT(CHAR(8),RehDt.[Reinstatement Date],112),'') AS [Reinstatement Date] " & _
                                   "FROM hremployee_master " & _
                                   "    LEFT JOIN hremployee_reportto ON hremployee_master.employeeunkid = hremployee_reportto.employeeunkid AND ishierarchy = 1 AND isvoid = 0 " & _
                                   "    LEFT JOIN hremployee_master AS rEmp ON hremployee_reportto.reporttoemployeeunkid = rEmp.employeeunkid " & _
                                   "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.titleunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.TITLE & "' " & _
                                   "    LEFT JOIN hrmsConfiguration..cfstate_master  ON hremployee_master.present_stateunkid= hrmsConfiguration..cfstate_master .stateunkid " & _
                                   "    LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.present_countryunkid " & _
                                   "    LEFT JOIN hrmsConfiguration..cfzipcode_master  ON hremployee_master.present_postcodeunkid = hrmsConfiguration..cfzipcode_master .zipcodeunkid " & _
                                   "    LEFT JOIN hrmsConfiguration..cfcity_master  ON hremployee_master.present_post_townunkid = hrmsConfiguration..cfcity_master .cityunkid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT TOP 1 " & _
                                   "             gradegroupunkid " & _
                                   "            ,gradeunkid " & _
                                   "            ,gradelevelunkid " & _
                                   "            ,employeeunkid " & _
                                   "            ,CAST(newscale AS NVARCHAR(MAX)) AS Scale " & _
                                   "        FROM prsalaryincrement_tran " & _
                                   "        WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' AND employeeunkid = '" & CInt(StrId) & "' " & _
                                   "        ORDER BY incrementdate DESC, salaryincrementtranunkid DESC " & _
                                   "    ) AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid " & _
                                   "    LEFT JOIN hrgradegroup_master AS CGG ON CGG.gradegroupunkid = Grds.gradegroupunkid " & _
                                   "    LEFT JOIN hrgrade_master AS CGM ON Grds.gradeunkid = CGM.gradeunkid " & _
                                   "    LEFT JOIN hrgradelevel_master AS CGL ON Grds.gradelevelunkid = CGL.gradelevelunkid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT TOP 1 " & _
                                   "             gradegroupunkid " & _
                                   "            ,gradeunkid " & _
                                   "            ,gradelevelunkid " & _
                                   "            ,employeeunkid " & _
                                   "            ,CAST(newscale AS NVARCHAR(MAX)) AS Scale " & _
                                   "        FROM prsalaryincrement_tran " & _
                                   "        WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= @GrdsDate AND employeeunkid = '" & CInt(StrId) & "' " & _
                                   "        ORDER BY incrementdate DESC, salaryincrementtranunkid DESC " & _
                                   "    ) AS OGrds ON OGrds.employeeunkid = hremployee_master.employeeunkid " & _
                                   "    LEFT JOIN hrgradegroup_master AS OGG ON OGG.gradegroupunkid = OGrds.gradegroupunkid " & _
                                   "    LEFT JOIN hrgrade_master AS OGM ON OGrds.gradeunkid = OGM.gradeunkid " & _
                                   "    LEFT JOIN hrgradelevel_master AS OGL ON OGrds.gradelevelunkid = OGL.gradelevelunkid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT TOP 1 " & _
                                   "             gradegroupunkid " & _
                                   "            ,gradeunkid " & _
                                   "            ,gradelevelunkid " & _
                                   "            ,employeeunkid " & _
                                   "            ,CAST(newscale AS NVARCHAR(MAX)) AS Scale " & _
                                   "        FROM prsalaryincrement_tran " & _
                                   "        WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) > '" & eZeeDate.convertDate(mdtAsOnDate) & "' AND employeeunkid = '" & CInt(StrId) & "' " & _
                                   "        ORDER BY incrementdate DESC, salaryincrementtranunkid DESC " & _
                                   "    ) AS NGrds ON NGrds.employeeunkid = hremployee_master.employeeunkid " & _
                                   "    LEFT JOIN hrgradegroup_master AS NGG ON NGG.gradegroupunkid = NGrds.gradegroupunkid " & _
                                   "    LEFT JOIN hrgrade_master AS NGM ON NGrds.gradeunkid = NGM.gradeunkid " & _
                                   "    LEFT JOIN hrgradelevel_master AS NGL ON NGrds.gradelevelunkid = NGL.gradelevelunkid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT TOP 1 " & _
                                   "             date1 " & _
                                   "            ,date2 " & _
                                   "            ,employeeunkid " & _
                                   "        FROM hremployee_dates_tran " & _
                                   "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_SUSPENSION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' AND employeeunkid = '" & CInt(StrId) & "' " & _
                                   "        ORDER BY effectivedate DESC " & _
                                   "    )AS SuspDt ON SuspDt.employeeunkid = hremployee_master.employeeunkid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT TOP 1 " & _
                                   "             date1 " & _
                                   "            ,date2 " & _
                                   "            ,employeeunkid " & _
                                   "        FROM hremployee_dates_tran " & _
                                   "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' AND employeeunkid = '" & CInt(StrId) & "' " & _
                                   "        ORDER BY effectivedate DESC " & _
                                   "    )AS PrbDt ON PrbDt.employeeunkid = hremployee_master.employeeunkid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT TOP 1 " & _
                                   "             date1 " & _
                                   "            ,date2 " & _
                                   "            ,effectivedate as [EOC Effective Date] " & _
                                   "            ,employeeunkid " & _
                                   "        FROM hremployee_dates_tran " & _
                                   "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' AND employeeunkid = '" & CInt(StrId) & "' " & _
                                   "        ORDER BY effectivedate DESC " & _
                                   "    )AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT TOP 1 " & _
                                   "             date1 " & _
                                   "            ,employeeunkid " & _
                                   "        FROM hremployee_dates_tran " & _
                                   "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' AND employeeunkid = '" & CInt(StrId) & "' " & _
                                   "        ORDER BY effectivedate DESC " & _
                                   "    )AS Retr ON Retr.employeeunkid = hremployee_master.employeeunkid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT TOP 1 " & _
                                   "             reinstatment_date as [Reinstatement Date] " & _
                                   "            ,employeeunkid " & _
                                   "        FROM hremployee_rehire_tran " & _
                                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' AND employeeunkid = '" & CInt(StrId) & "' " & _
                                   "        ORDER BY effectivedate DESC " & _
                                   "    )AS RehDt ON RehDt.employeeunkid = hremployee_master.employeeunkid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT TOP 1 " & _
                                   "             date1 " & _
                                   "            ,date2 " & _
                                   "            ,employeeunkid " & _
                                   "        FROM hremployee_dates_tran " & _
                                   "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' AND employeeunkid = '" & CInt(StrId) & "' " & _
                                   "        ORDER BY effectivedate DESC " & _
                                   "    )AS CnfDt ON CnfDt.employeeunkid = hremployee_master.employeeunkid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT TOP 1 " & _
                                   "             employeeunkid AS EmpId " & _
                                   "            ,cctranheadvalueid " & _
                                   "        FROM hremployee_cctranhead_tran " & _
                                   "        WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' AND employeeunkid = '" & CInt(StrId) & "' " & _
                                   "        ORDER BY effectivedate DESC " & _
                                   "    ) AS CC ON CC.EmpId = hremployee_master.employeeunkid " & _
                                   "    LEFT JOIN prcostcenter_master AS CCN ON CCN.costcenterunkid = CC.cctranheadvalueid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT TOP 1 " & _
                                   "             employeeunkid AS EmpId " & _
                                   "            ,cctranheadvalueid " & _
                                   "        FROM hremployee_cctranhead_tran " & _
                                   "        WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @OCCRDate AND employeeunkid = '" & CInt(StrId) & "' " & _
                                   "        ORDER BY effectivedate DESC " & _
                                   "    ) AS OC ON OC.EmpId = hremployee_master.employeeunkid " & _
                                   "    LEFT JOIN prcostcenter_master AS OCN ON OCN.costcenterunkid = OC.cctranheadvalueid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT TOP 1 " & _
                                   "             hremployee_categorization_tran.employeeunkid AS EmpId " & _
                                   "            ,hremployee_categorization_tran.jobgroupunkid " & _
                                   "            ,hremployee_categorization_tran.jobunkid " & _
                                   "        FROM hremployee_categorization_tran " & _
                                   "            JOIN hremployee_reportto ON hremployee_categorization_tran.employeeunkid = hremployee_reportto.reporttoemployeeunkid " & _
                                   "            JOIN hremployee_master ON hremployee_reportto.employeeunkid = hremployee_master.employeeunkid " & _
                                   "        WHERE hremployee_categorization_tran.isvoid = 0 AND hremployee_reportto.isvoid = 0  AND ishierarchy = 1 " & _
                                   "        AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' AND hremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
                                   "        ORDER BY effectivedate DESC " & _
                                   "    ) AS RpCat ON RpCat.EmpId = rEmp.employeeunkid " & _
                                   "    LEFT JOIN hrjobgroup_master AS RJG ON RpCat.jobgroupunkid = RJG.jobgroupunkid " & _
                                   "    LEFT JOIN hrjob_master AS RJB ON RpCat.jobunkid = RJB.jobunkid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT TOP 1 " & _
                                   "             employeeunkid AS EmpId " & _
                                   "            ,jobgroupunkid " & _
                                   "            ,jobunkid " & _
                                   "        FROM hremployee_categorization_tran " & _
                                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' AND employeeunkid = '" & CInt(StrId) & "' " & _
                                   "        ORDER BY effectivedate DESC " & _
                                   "    ) AS ReCat ON ReCat.EmpId = hremployee_master.employeeunkid " & _
                                   "    LEFT JOIN hrjobgroup_master AS CJG ON ReCat.jobgroupunkid = CJG.jobgroupunkid " & _
                                   "    LEFT JOIN hrjob_master AS CJB ON ReCat.jobunkid = CJB.jobunkid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT " & _
                                   "             jobunkid " & _
                                   "            ,REPLACE(ISNULL(STUFF((SELECT '|' + oD.keyduties + '\r\n' " & _
                                   "        FROM hrjobkeyduties_tran AS oD " & _
                                   "        WHERE oD.jobunkid = oJ.jobunkid " & _
                                   "        FOR XML PATH ('')) ,1,0,''),''),'|','> ') AS [Key Duties And Responsibilities] " & _
                                   "        FROM hrjob_master AS oJ " & _
                                   "        GROUP BY oJ.jobunkid " & _
                                   "    ) AS JKR ON JKR.jobunkid = CJB.jobunkid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT  TOP 1 " & _
                                   "             employeeunkid AS EmpId " & _
                                   "            ,jobgroupunkid " & _
                                   "            ,jobunkid " & _
                                   "        FROM hremployee_categorization_tran " & _
                                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @OCatDate AND employeeunkid = '" & CInt(StrId) & "' " & _
                                   "        ORDER BY effectivedate DESC " & _
                                   "    ) AS OReCat ON OReCat.EmpId = hremployee_master.employeeunkid " & _
                                   "    LEFT JOIN hrjobgroup_master AS OJG ON ReCat.jobgroupunkid = OJG.jobgroupunkid " & _
                                   "    LEFT JOIN hrjob_master AS OJB ON ReCat.jobunkid = OJB.jobunkid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT  TOP 1 " & _
                                   "             employeeunkid AS EmpId " & _
                                   "            ,stationunkid " & _
                                   "            ,deptgroupunkid " & _
                                   "            ,departmentunkid " & _
                                   "            ,sectiongroupunkid " & _
                                   "            ,sectionunkid " & _
                                   "            ,unitgroupunkid " & _
                                   "            ,unitunkid " & _
                                   "            ,teamunkid " & _
                                   "            ,classgroupunkid " & _
                                   "            ,classunkid " & _
                                   "        FROM hremployee_transfer_tran " & _
                                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' AND employeeunkid = '" & CInt(StrId) & "' " & _
                                   "        ORDER BY effectivedate DESC " & _
                                   "    ) AS Alloc ON Alloc.EmpId = hremployee_master.employeeunkid " & _
                                   "    LEFT JOIN hrstation_master AS CST ON  CST.stationunkid = Alloc.stationunkid " & _
                                   "    LEFT JOIN hrdepartment_group_master AS CDG ON CDG.deptgroupunkid = Alloc.deptgroupunkid " & _
                                   "    LEFT JOIN hrdepartment_master AS CDT ON Alloc.departmentunkid = CDT.departmentunkid " & _
                                   "    LEFT JOIN hrsectiongroup_master AS CSG ON Alloc.sectiongroupunkid = CSG.sectiongroupunkid " & _
                                   "    LEFT JOIN hrsection_master AS CSC ON Alloc.sectionunkid = CSC.sectionunkid " & _
                                   "    LEFT JOIN hrunitgroup_master AS CUG ON Alloc.unitgroupunkid = CUG.unitgroupunkid " & _
                                   "    LEFT JOIN hrunit_master AS CUT ON Alloc.unitunkid = CUT.unitunkid " & _
                                   "    LEFT JOIN hrteam_master AS CTM ON Alloc.teamunkid = CTM.teamunkid " & _
                                   "    LEFT JOIN hrclassgroup_master AS CCG ON Alloc.classgroupunkid = CCG.classgroupunkid " & _
                                   "    LEFT JOIN hrclasses_master AS CCM ON Alloc.classunkid = CCM.classesunkid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT  TOP 1 " & _
                                   "             employeeunkid AS EmpId " & _
                                   "            ,stationunkid " & _
                                   "            ,deptgroupunkid " & _
                                   "            ,departmentunkid " & _
                                   "            ,sectiongroupunkid " & _
                                   "            ,sectionunkid " & _
                                   "            ,unitgroupunkid " & _
                                   "            ,unitunkid " & _
                                   "            ,teamunkid " & _
                                   "            ,classgroupunkid " & _
                                   "            ,classunkid " & _
                                   "        FROM hremployee_transfer_tran " & _
                                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @OTrfDate AND employeeunkid = '" & CInt(StrId) & "' " & _
                                   "        ORDER BY effectivedate DESC " & _
                                   "    ) AS OAlloc ON OAlloc.EmpId = hremployee_master.employeeunkid " & _
                                   "    LEFT JOIN hrstation_master AS OST ON  OST.stationunkid = OAlloc.stationunkid " & _
                                   "    LEFT JOIN hrdepartment_group_master AS ODG ON ODG.deptgroupunkid = OAlloc.deptgroupunkid " & _
                                   "    LEFT JOIN hrdepartment_master AS ODT ON OAlloc.departmentunkid = ODT.departmentunkid " & _
                                   "    LEFT JOIN hrsectiongroup_master AS OSG ON OAlloc.sectiongroupunkid = OSG.sectiongroupunkid " & _
                                   "    LEFT JOIN hrsection_master AS OSC ON OAlloc.sectionunkid = OSC.sectionunkid " & _
                                   "    LEFT JOIN hrunitgroup_master AS OUG ON OAlloc.unitgroupunkid = OUG.unitgroupunkid " & _
                                   "    LEFT JOIN hrunit_master AS OUM ON OAlloc.unitunkid = OUM.unitunkid " & _
                                   "    LEFT JOIN hrteam_master AS OTM ON OAlloc.teamunkid = OTM.teamunkid " & _
                                   "    LEFT JOIN hrclassgroup_master AS OCG ON OAlloc.classgroupunkid = OCG.classgroupunkid " & _
                                   "    LEFT JOIN hrclasses_master AS OCM ON OAlloc.classunkid = OCM.classesunkid " & _
                                   "    LEFT JOIN " & _
                                   "    ( " & _
                                   "        SELECT TOP 1 " & _
                                   "             employeeunkid AS EmpId " & _
                                   "            ,shiftunkid " & _
                                   "        FROM hremployee_shift_tran " & _
                                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' AND employeeunkid = '" & CInt(StrId) & "' " & _
                                   "        ORDER BY effectivedate DESC " & _
                                   "    ) AS Sft ON Sft.EmpId = hremployee_master.employeeunkid " & _
                                   "    JOIN tnashift_master ON Sft.shiftunkid = tnashift_master.shiftunkid " & _
                                   "WHERE 1 = 1 AND hremployee_master.employeeunkid = '" & CInt(StrId) & "' "

                            'S.SANDEEP [05-Apr-2018] -- START
                            'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102} ---------- ADDED WITH JOINS
                            '"    ,ISNULL(JKR.[Key Duties And Responsibilities],'') AS [Key Duties And Responsibilities] " & _
                            '"    ,ISNULL(CASE WHEN hremployee_master.gender = 1 THEN @Male " & _
                            '"                 WHEN hremployee_master.gender = 2 THEN @Female " & _
                            '"            END,'') AS Gender " & _
                            '"    ,ISNULL(RJB.job_name,'') AS [Reporting To Job] " & _

                            objdataOperation.ClearParameters()
                            objdataOperation.AddParameter("@Male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
                            objdataOperation.AddParameter("@Female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))

                            objdataOperation.AddParameter("@Permanent", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Permanent"))
                            objdataOperation.AddParameter("@Years", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Year(s),"))
                            objdataOperation.AddParameter("@Months", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Month(s) and "))
                            objdataOperation.AddParameter("@Days", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Day(s)"))
                            'S.SANDEEP [05-Apr-2018] -- END



                            'Nilay (16-Apr-2016) -- Start
                            'ENHANCEMENT - 59.1 - Including Completed Age and Running Age columns in Letter Fields
                            'ADDED - [Completed birth Age, Running birth Age]
                            'Nilay (16-Apr-2016) -- End

                            'S.SANDEEP [10 MAR 2016] -- START
                            'REMOVED  ------------------> ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno ; .rno = 1
                            'ADDED    ------------------> TOP 1 ; ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ; ORDER BY effectivedate DESC
                            'S.SANDEEP [10 MAR 2016] -- END


                            'S.SANDEEP [04 JUN 2015] -- END


                            '       'Sohail (24 Sep 2012) - [isapproved]

                            'S.SANDEEP [ 15 MAR 2014 ] -- START
                            ' ADDED FOR COMMENT NUMBER ->  365
                            '",ISNULL(rEmp.employeecode,'') AS ReportingCode " & _
                            '",ISNULL(rEmp.firstname,'') AS ReportingFirstName " & _
                            '",ISNULL(rEmp.surname,'') AS ReportingSurnameCode " & _
                            '",ISNULL(rEmp.othername,'') AS ReportingOtherName " & _
                            '"LEFT JOIN hremployee_reportto ON hremployee_master.employeeunkid = hremployee_reportto.employeeunkid " & _
                            '"LEFT JOIN hremployee_master AS rEmp ON hremployee_reportto.reporttoemployeeunkid = rEmp.employeeunkid " & _
                            'S.SANDEEP [ 15 MAR 2014 ] -- END

                            dsList = objdataOperation.ExecQuery(StrQ, strListName)
                            If objdataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objdataOperation.ErrorNumber & " : " & objdataOperation.ErrorMessage)
                                Throw exForce
                            End If

						    'Gajanan [28-AUG-2019] -- Start      
						    'ENHANCEMENT : Additional Data Fields(EOC Effective Date and latest Reinstatement dates) required in Letter Templates  are required.

							'Add [EOC Effective Date]
							'Add [Reinstatement Date]

						    'Gajanan [28-AUG-2019] -- End      

                            'S.SANDEEP |09-APR-2019| -- START
                            '' AS YearOfService --------------- ADDED
                            Dim intService As Integer = 0
                            Dim objEDate As New clsemployee_dates_tran
                            Dim dsYear As New DataSet
                            dsYear = objEDate.GetEmployeeServiceDays(objdataOperation, intCompanyUnkid, strDatabaseName, StrId, mdtAsOnDate)
                            objEDate = Nothing
                            If dsYear.Tables(0).Rows.Count > 0 Then
                                Dim dblTotDays As Double = (From p In dsYear.Tables(0) Select (CDbl(p.Item("ServiceDays")))).Sum()
                                intService = Int(dblTotDays / 365)
                            End If
                            'S.SANDEEP |09-APR-2019| -- END

                            If dsList.Tables(0).Rows.Count > 0 Then
                                If dTable Is Nothing Then
                                    dTable = dsList.Tables(0).Clone
                                    dTable.Rows.Clear()
                                End If

                                For Each dRow As DataRow In dsList.Tables(0).Rows
                                    If dRow.Item("OldSalary").ToString.Trim.Length > 0 Then
                                        dRow.Item("OldSalary") = Format(CDec(dRow.Item("OldSalary")), GUI.fmtCurrency)
                                    End If

                                    If dRow.Item("CurrentSalary").ToString.Trim.Length > 0 Then
                                        dRow.Item("CurrentSalary") = Format(CDec(dRow.Item("CurrentSalary")), GUI.fmtCurrency)
                                    End If

                                    If dRow.Item("NextSalary").ToString.Trim.Length > 0 Then
                                        dRow.Item("NextSalary") = Format(CDec(dRow.Item("NextSalary")), GUI.fmtCurrency)
                                    End If

                                    'S.SANDEEP [ 07 MAY 2012 ] -- START
                                    'ENHANCEMENT : TRA CHANGES
                                    If dRow.Item("Password").ToString.Trim.Length > 0 Then
                                        dRow.Item("Original_Password") = clsSecurity.Decrypt(dRow.Item("Password").ToString, "ezee")
                                    End If
                                    'S.SANDEEP [ 07 MAY 2012 ] -- END

                                    'S.SANDEEP |09-APR-2019| -- START
                                    If intService > 0 Then
                                        dRow.Item("YearOfService") = intService
                                    End If
                                    'S.SANDEEP |09-APR-2019| -- END

                                    dTable.ImportRow(dRow)
                                Next
                            End If

                            'S.SANDEEP [28 SEP 2016] -- START
                            'ENHANCEMENT : ISSUE WHEN ROWS ARE NOT THERE FOR GIVEN EMPLOYEE OBJECT REFRENCE WAS THROWN
                            If dsList.Tables(0).Rows.Count > 0 Then
                            dsList.Tables.RemoveAt(0)
                            dsList.Tables.Add(dTable.Copy)
                            End If
                            'S.SANDEEP [28 SEP 2016] -- END
                        Next
                    End If
                Case enImg_Email_RefId.Applicant_Module
                    'S.SANDEEP [ 02 MAY 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'StrQ = "SELECT " & _
                    '        "	 ISNULL(cfcommon_master.name,'') AS Title " & _
                    '        "	,ISNULL(rcapplicant_master.firstname,'') AS Firstname " & _
                    '        "	,ISNULL(rcapplicant_master.surname,'') AS Surname " & _
                    '        "	,ISNULL(rcapplicant_master.othername,'') AS Othername " & _
                    '        "	,ISNULL(rcapplicant_master.gender,'') AS Gender " & _
                    '        "	,ISNULL(rcapplicant_master.email,'') AS Email " & _
                    '        "	,ISNULL(rcapplicant_master.present_address1,'') AS Applicant_Address1 " & _
                    '        "	,ISNULL(rcapplicant_master.present_address2,'') AS Applicant_Address2 " & _
                    '        "	,ISNULL(hrmsConfiguration..cfcity_master.name,'') AS Applicant_City " & _
                    '        "	,ISNULL(hrmsConfiguration..cfstate_master.name,'') AS Applicant_State " & _
                    '        "	,ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Applicant_Country " & _
                    '        "	,ISNULL(rcapplicant_master.present_mobileno,'') AS Mobileno " & _
                    '        "	,ISNULL(CONVERT(CHAR(8),rcapplicant_master.birth_date,112),'') AS BirthDate " & _
                    '        "	,ISNULL(CONVERT(CHAR(8),rcapplicant_master.anniversary_date,112),'') AS AnniversaryDate " & _
                    '        "	,ISNULL(rcvacancy_master.vacancytitle,'') AS Vacancy " & _
                    '        "	,'" & Now.Date & "' As Today " & _
                    '        "	,'" & Company._Object._Name & "' As CompanyName " & _
                    '        "	,'" & Company._Object._Address1 & "' As Address1 " & _
                    '        "	,'" & Company._Object._Address2 & "' As Address2 " & _
                    '        "   ,'" & Company._Object._City_Name & "' As C_City " & _
                    '        "   ,'" & Company._Object._State_Name & "' As C_State " & _
                    '        "   ,'" & Company._Object._Country_Name & "' As C_Country " & _
                    '        "	,'" & Company._Object._Phone1 & "' As Phone1 " & _
                    '        "	,'" & Company._Object._Phone2 & "' As Phone2 " & _
                    '        "	,'" & Company._Object._Phone3 & "' As Phone3 " & _
                    '        "	,'" & Company._Object._Fax & "' As Fax " & _
                    '        "	,'" & Company._Object._Email & "' As Email " & _
                    '        "	,'" & Company._Object._Website & "' As Website " & _
                    '        "	,'" & Company._Object._Registerdno & "' As RegNo1 " & _
                    '        "	,'" & Company._Object._Vatno & "' As RegNo2 " & _
                    '        "	,'" & Company._Object._Tinno & "' As TinNo " & _
                    '        "	,'" & Company._Object._Reference & "' As Reference " & _
                    '        "	,rcapplicant_master.applicantunkid AS EmpId " & _
                    '        "	,rcapplicant_master.applicant_code AS EmpCode " & _
                    '        "   ,ISNULL(referenceno,'') AS Appreferenceno " & _
                    '        "FROM rcapplicant_master " & _
                    '        "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicant_master.titleunkid " & _
                    '        "	LEFT JOIN hrmsConfiguration..cfstate_master ON hrmsConfiguration..cfstate_master.stateunkid = rcapplicant_master.present_stateunkid " & _
                    '        "	LEFT JOIN hrmsConfiguration..cfcity_master ON hrmsConfiguration..cfcity_master.cityunkid = rcapplicant_master.present_post_townunkid " & _
                    '        "	LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = rcapplicant_master.present_countryunkid " & _
                    '        "	LEFT JOIN rcvacancy_master ON rcapplicant_master.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                    '        "WHERE rcapplicant_master.applicantunkid IN ( " & strEmpId & ") "


                    'S.SANDEEP [ 04 MAY 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'StrQ = "SELECT " & _
                    '        "	 ISNULL(cfcommon_master.name,'') AS Title " & _
                    '        "	,ISNULL(rcapplicant_master.firstname,'') AS Firstname " & _
                    '        "	,ISNULL(rcapplicant_master.surname,'') AS Surname " & _
                    '        "	,ISNULL(rcapplicant_master.othername,'') AS Othername " & _
                    '        "	,ISNULL(rcapplicant_master.gender,'') AS Gender " & _
                    '        "	,ISNULL(rcapplicant_master.email,'') AS Email " & _
                    '        "	,ISNULL(rcapplicant_master.present_address1,'') AS Applicant_Address1 " & _
                    '        "	,ISNULL(rcapplicant_master.present_address2,'') AS Applicant_Address2 " & _
                    '        "	,ISNULL(hrmsConfiguration..cfcity_master.name,'') AS Applicant_City " & _
                    '        "	,ISNULL(hrmsConfiguration..cfstate_master.name,'') AS Applicant_State " & _
                    '        "	,ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Applicant_Country " & _
                    '        "	,ISNULL(rcapplicant_master.present_mobileno,'') AS Mobileno " & _
                    '        "	,ISNULL(CONVERT(CHAR(8),rcapplicant_master.birth_date,112),'') AS BirthDate " & _
                    '        "	,ISNULL(CONVERT(CHAR(8),rcapplicant_master.anniversary_date,112),'') AS AnniversaryDate " & _
                    '        "   ,ISNULL(VAC.vacancy, '') AS Vacancy " & _
                    '        "   ,ISNULL(VAC.ODate,'') AS ODate " & _
                    '        "   ,ISNULL(VAC.CDate,'') AS CDate " & _
                    '        "	,'" & Now.Date & "' As Today " & _
                    '        "	,'" & Company._Object._Name & "' As CompanyName " & _
                    '        "	,'" & Company._Object._Address1 & "' As Address1 " & _
                    '        "	,'" & Company._Object._Address2 & "' As Address2 " & _
                    '        "   ,'" & Company._Object._City_Name & "' As C_City " & _
                    '        "   ,'" & Company._Object._State_Name & "' As C_State " & _
                    '        "   ,'" & Company._Object._Country_Name & "' As C_Country " & _
                    '        "	,'" & Company._Object._Phone1 & "' As Phone1 " & _
                    '        "	,'" & Company._Object._Phone2 & "' As Phone2 " & _
                    '        "	,'" & Company._Object._Phone3 & "' As Phone3 " & _
                    '        "	,'" & Company._Object._Fax & "' As Fax " & _
                    '        "	,'" & Company._Object._Email & "' As Email " & _
                    '        "	,'" & Company._Object._Website & "' As Website " & _
                    '        "	,'" & Company._Object._Registerdno & "' As RegNo1 " & _
                    '        "	,'" & Company._Object._Vatno & "' As RegNo2 " & _
                    '        "	,'" & Company._Object._Tinno & "' As TinNo " & _
                    '        "	,'" & Company._Object._Reference & "' As Reference " & _
                    '        "	,rcapplicant_master.applicantunkid AS EmpId " & _
                    '        "	,rcapplicant_master.applicant_code AS EmpCode " & _
                    '        "   ,ISNULL(referenceno,'') AS Appreferenceno " & _
                    '        "FROM rcapplicant_master " & _
                    '        "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicant_master.titleunkid " & _
                    '        "	LEFT JOIN hrmsConfiguration..cfstate_master ON hrmsConfiguration..cfstate_master.stateunkid = rcapplicant_master.present_stateunkid " & _
                    '        "	LEFT JOIN hrmsConfiguration..cfcity_master ON hrmsConfiguration..cfcity_master.cityunkid = rcapplicant_master.present_post_townunkid " & _
                    '        "	LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = rcapplicant_master.present_countryunkid " & _
                    '        "   LEFT JOIN " & _
                    '        "   ( " & _
                    '        "	    SELECT " & _
                    '        "	         rcapp_vacancy_mapping.applicantunkid " & _
                    '        "	        ,rcapp_vacancy_mapping.vacancyunkid " & _
                    '        "	        ,cfcommon_master.name AS vacancy " & _
                    '        "	        ,CONVERT(CHAR(8), openingdate, 112) AS ODate " & _
                    '        "	        ,CONVERT(CHAR(8), closingdate, 112) AS CDate " & _
                    '        "       FROM rcapp_vacancy_mapping " & _
                    '        "	        JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                    '        "	        JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.VACANCY_MASTER & " " & _
                    '        "       WHERE rcapp_vacancy_mapping.isactive = 1 AND rcapp_vacancy_mapping.vacancyunkid = " & mintVacancyUnkid & " " & _
                    '        "   )AS VAC ON rcapplicant_master.applicantunkid = VAC.applicantunkid " & _
                    '        "WHERE rcapplicant_master.applicantunkid IN ( " & strEmpId & ") "


                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ = "SELECT " & _
                    '        "	 ISNULL(cfcommon_master.name,'') AS Title " & _
                    '        "	,ISNULL(rcapplicant_master.firstname,'') AS Firstname " & _
                    '        "	,ISNULL(rcapplicant_master.surname,'') AS Surname " & _
                    '        "	,ISNULL(rcapplicant_master.othername,'') AS Othername " & _
                    '        "	,ISNULL(rcapplicant_master.gender,'') AS Gender " & _
                    '        "	,ISNULL(rcapplicant_master.email,'') AS Email " & _
                    '        "	,ISNULL(rcapplicant_master.present_address1,'') AS Applicant_Address1 " & _
                    '        "	,ISNULL(rcapplicant_master.present_address2,'') AS Applicant_Address2 " & _
                    '        "	,ISNULL(hrmsConfiguration..cfcity_master.name,'') AS Applicant_City " & _
                    '        "	,ISNULL(hrmsConfiguration..cfstate_master.name,'') AS Applicant_State " & _
                    '        "	,ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Applicant_Country " & _
                    '        "	,ISNULL(rcapplicant_master.present_mobileno,'') AS Mobileno " & _
                    '        "	,ISNULL(CONVERT(CHAR(8),rcapplicant_master.birth_date,112),'') AS BirthDate " & _
                    '        "	,ISNULL(CONVERT(CHAR(8),rcapplicant_master.anniversary_date,112),'') AS AnniversaryDate " & _
                    '        "   ,ISNULL(VAC.vacancy, '') AS Vacancy_Without_Dates " & _
                    '        "   ,ISNULL(VAC.vacancy, '') AS Vacancy_With_Dates " & _
                    '        "   ,ISNULL(VAC.ODate,'') AS ODate " & _
                    '        "   ,ISNULL(VAC.CDate,'') AS CDate " & _
                    '        "   ,ISNULL(Branch,'') AS Branch " & _
                    '        "   ,ISNULL(Department_Group,'') AS Department_Group " & _
                    '        "   ,ISNULL(Job_Department,'') AS Job_Department " & _
                    '        "   ,ISNULL(Section,'') AS Section " & _
                    '        "   ,ISNULL(Unit,'') AS Unit " & _
                    '        "   ,ISNULL(Job_Group,'') AS Job_Group " & _
                    '        "   ,ISNULL(Job_Name,'') AS Job_Name " & _
                    '        "   ,ISNULL(Employment_Type,'') AS Employment_Type " & _
                    '        "   ,ISNULL(Grade_Group,'') AS Grade_Group " & _
                    '        "   ,ISNULL(Grade,'') AS Grade " & _
                    '        "   ,ISNULL(Pay_Type,'') AS Pay_Type " & _
                    '        "   ,ISNULL(Shift_Type,'') AS Shift_Type " & _
                    '        "   ,ISNULL(Pay_Range_From,0) AS Pay_Range_From " & _
                    '        "   ,ISNULL(Pay_Range_To,0) AS Pay_Range_To " & _
                    '        "	,'" & Now.Date & "' As Today " & _
                    '        "   ,'" & Company._Object._Code & "' As CompanyCode " & _
                    '        "	,'" & Company._Object._Name & "' As CompanyName " & _
                    '        "	,'" & Company._Object._Address1 & "' As Address1 " & _
                    '        "	,'" & Company._Object._Address2 & "' As Address2 " & _
                    '        "   ,'" & Company._Object._City_Name & "' As C_City " & _
                    '        "   ,'" & Company._Object._State_Name & "' As C_State " & _
                    '        "   ,'" & Company._Object._Country_Name & "' As C_Country " & _
                    '        "	,'" & Company._Object._Phone1 & "' As Phone1 " & _
                    '        "	,'" & Company._Object._Phone2 & "' As Phone2 " & _
                    '        "	,'" & Company._Object._Phone3 & "' As Phone3 " & _
                    '        "	,'" & Company._Object._Fax & "' As Fax " & _
                    '        "	,'" & Company._Object._Email & "' As Email " & _
                    '        "	,'" & Company._Object._Website & "' As Website " & _
                    '        "	,'" & Company._Object._Registerdno & "' As RegNo1 " & _
                    '        "	,'" & Company._Object._Vatno & "' As RegNo2 " & _
                    '        "	,'" & Company._Object._Tinno & "' As TinNo " & _
                    '        "	,'" & Company._Object._Reference & "' As Reference " & _
                    '        "	,rcapplicant_master.applicantunkid AS EmpId " & _
                    '        "	,rcapplicant_master.applicant_code AS EmpCode " & _
                    '        "   ,ISNULL(referenceno,'') AS Appreferenceno " & _
                    '        "FROM rcapplicant_master " & _
                    '        "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicant_master.titleunkid " & _
                    '        "	LEFT JOIN hrmsConfiguration..cfstate_master ON hrmsConfiguration..cfstate_master.stateunkid = rcapplicant_master.present_stateunkid " & _
                    '        "	LEFT JOIN hrmsConfiguration..cfcity_master ON hrmsConfiguration..cfcity_master.cityunkid = rcapplicant_master.present_post_townunkid " & _
                    '        "	LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = rcapplicant_master.present_countryunkid " & _
                    '        "   LEFT JOIN " & _
                    '        "   ( " & _
                    '        "	    SELECT " & _
                    '        "	         rcapp_vacancy_mapping.applicantunkid " & _
                    '        "	        ,rcapp_vacancy_mapping.vacancyunkid " & _
                    '        "	        ,cfcommon_master.name AS vacancy " & _
                    '        "	        ,CONVERT(CHAR(8), openingdate, 112) AS ODate " & _
                    '        "	        ,CONVERT(CHAR(8), closingdate, 112) AS CDate " & _
                    '        "           ,ISNULL(hrstation_master.name,'') AS Branch " & _
                    '        "           ,ISNULL(hrdepartment_group_master.name,'') AS Department_Group " & _
                    '        "           ,ISNULL(hrdepartment_master.name,'') AS Job_Department " & _
                    '        "           ,ISNULL(hrsection_master.name,'') AS Section " & _
                    '        "           ,ISNULL(hrunit_master.name,'') AS Unit " & _
                    '        "           ,ISNULL(hrjobgroup_master.name,'') AS Job_Group " & _
                    '        "           ,ISNULL(hrjob_master.job_name,'') AS Job_Name " & _
                    '        "           ,ISNULL(EMPL.name,'') AS Employment_Type " & _
                    '        "           ,ISNULL(hrgradegroup_master.name,'') AS Grade_Group " & _
                    '        "           ,ISNULL(hrgrade_master.name,'') AS Grade " & _
                    '        "           ,ISNULL(PAYTYP.name,'') AS Pay_Type " & _
                    '        "           ,ISNULL(SFTTYP.name,'') AS Shift_Type " & _
                    '        "           ,ISNULL(pay_from,0) AS Pay_Range_From " & _
                    '        "           ,ISNULL(pay_to,0) AS Pay_Range_To " & _
                    '        "       FROM rcapp_vacancy_mapping " & _
                    '        "	        JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                    '        "           LEFT JOIN cfcommon_master AS SFTTYP ON dbo.rcvacancy_master.shifttypeunkid = SFTTYP.masterunkid AND SFTTYP.mastertype = 22 " & _
                    '        "           LEFT JOIN cfcommon_master AS PAYTYP ON rcvacancy_master.paytypeunkid = PAYTYP.masterunkid AND PAYTYP.mastertype = 17 " & _
                    '        "           LEFT JOIN hrgrade_master ON rcvacancy_master.gradeunkid = hrgrade_master.gradeunkid " & _
                    '        "           LEFT JOIN hrgradegroup_master ON rcvacancy_master.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                    '        "           LEFT JOIN cfcommon_master AS EMPL ON dbo.rcvacancy_master.employeementtypeunkid = EMPL.masterunkid AND EMPL.mastertype = 8 " & _
                    '        "           LEFT JOIN hrjob_master ON rcvacancy_master.jobunkid = hrjob_master.jobunkid " & _
                    '        "           LEFT JOIN hrjobgroup_master ON rcvacancy_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
                    '        "           LEFT JOIN hrunit_master ON rcvacancy_master.unitunkid = hrunit_master.unitunkid " & _
                    '        "           LEFT JOIN hrsection_master ON rcvacancy_master.sectionunkid = hrsection_master.sectionunkid " & _
                    '        "           LEFT JOIN hrdepartment_master ON rcvacancy_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                    '        "           LEFT JOIN hrdepartment_group_master ON rcvacancy_master.deptgroupunkid = hrdepartment_group_master.deptgroupunkid " & _
                    '        "           LEFT JOIN hrstation_master ON rcvacancy_master.stationunkid = hrstation_master.stationunkid " & _
                    '        "	        JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.VACANCY_MASTER & " " & _
                    '        "       WHERE rcapp_vacancy_mapping.isactive = 1 AND rcapp_vacancy_mapping.vacancyunkid = " & mintVacancyUnkid & " " & _
                    '        "   )AS VAC ON rcapplicant_master.applicantunkid = VAC.applicantunkid " & _
                    '        "WHERE rcapplicant_master.applicantunkid IN ( " & strEmpId & ") "

                    StrQ = "SELECT " & _
                            "	 ISNULL(cfcommon_master.name,'') AS Title " & _
                            "	,ISNULL(rcapplicant_master.firstname,'') AS Firstname " & _
                            "	,ISNULL(rcapplicant_master.surname,'') AS Surname " & _
                            "	,ISNULL(rcapplicant_master.othername,'') AS Othername " & _
                            "	,ISNULL(rcapplicant_master.gender,'') AS Gender " & _
                            "	,ISNULL(LTRIM(RTRIM(rcapplicant_master.email)),'') AS Email " & _
                            "	,ISNULL(rcapplicant_master.present_address1,'') AS Applicant_Address1 " & _
                            "	,ISNULL(rcapplicant_master.present_address2,'') AS Applicant_Address2 " & _
                            "	,ISNULL(hrmsConfiguration..cfcity_master.name,'') AS Applicant_City " & _
                            "	,ISNULL(hrmsConfiguration..cfstate_master.name,'') AS Applicant_State " & _
                            "	,ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Applicant_Country " & _
                            "	,ISNULL(rcapplicant_master.present_mobileno,'') AS Mobileno " & _
                            "	,ISNULL(CONVERT(CHAR(8),rcapplicant_master.birth_date,112),'') AS BirthDate " & _
                            "   ,CASE WHEN ISNULL(rcapplicant_master.birth_date,'') = '' THEN '' " & _
                            "         ELSE CAST((CAST(CONVERT(char(8), GETDATE(), 112) AS int) - CAST(CONVERT(char(8), rcapplicant_master.birth_date, 112) AS int)) / 10000 AS nvarchar(max)) " & _
                            "    END AS [Completed birth Age] " & _
                            "   ,CASE WHEN ISNULL(rcapplicant_master.birth_date,'') = '' THEN '' " & _
                            "         ELSE CAST((CAST(CONVERT(char(8), GETDATE(), 112) AS int) - CAST(CONVERT(char(8), rcapplicant_master.birth_date, 112) AS int)) / 10000 + 1 AS nvarchar(max)) " & _
                            "    END AS [Running birth Age] " & _
                            "	,ISNULL(CONVERT(CHAR(8),rcapplicant_master.anniversary_date,112),'') AS AnniversaryDate " & _
                            "   ,ISNULL(VAC.vacancy, '') AS Vacancy_Without_Dates " & _
                            "   ,ISNULL(VAC.vacancy, '') AS Vacancy_With_Dates " & _
                            "   ,ISNULL(VAC.ODate,'') AS ODate " & _
                            "   ,ISNULL(VAC.CDate,'') AS CDate " & _
                            "   ,ISNULL(Branch,'') AS Branch " & _
                            "   ,ISNULL(Department_Group,'') AS Department_Group " & _
                            "   ,ISNULL(Job_Department,'') AS Job_Department " & _
                            "   ,ISNULL(Section,'') AS Section " & _
                            "   ,ISNULL(Unit,'') AS Unit " & _
                            "   ,ISNULL(Job_Group,'') AS Job_Group " & _
                            "   ,ISNULL(Job_Name,'') AS Job_Name " & _
                            "   ,ISNULL(Employment_Type,'') AS Employment_Type " & _
                            "   ,ISNULL(Grade_Group,'') AS Grade_Group " & _
                            "   ,ISNULL(Grade,'') AS Grade " & _
                            "   ,ISNULL(Pay_Type,'') AS Pay_Type " & _
                            "   ,ISNULL(Shift_Type,'') AS Shift_Type " & _
                            "   ,ISNULL(Pay_Range_From,0) AS Pay_Range_From " & _
                            "   ,ISNULL(Pay_Range_To,0) AS Pay_Range_To " & _
                            strCommonData & " " & _
                            "	,rcapplicant_master.applicantunkid AS EmpId " & _
                            "	,rcapplicant_master.applicant_code AS EmpCode " & _
                            "   ,ISNULL(referenceno,'') AS Appreferenceno " & _
                            "   ,ISNULL(CONVERT(CHAR(8), rcbatchschedule_master.interviewdate, 112), '')  AS InterviewDate " & _
                            "   ,ISNULL(CONVERT(NVARCHAR(5),rcbatchschedule_master.interviewtime,108), '')  AS InterviewTime " & _
                            "   ,rcbatchschedule_master.location AS InterviewVenue " & _
                            "   ,ISNULL(VAC.isexternalvacancy,0) AS IsExternalVacancy " & _
                            "   ,ISNULL(rcapplicant_master.employeecode,'') as EmployeeCode " & _
                            "   ,'' as ReportingTo " & _
                            "FROM rcapplicant_master " & _
                            "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicant_master.titleunkid " & _
                            "	LEFT JOIN hrmsConfiguration..cfstate_master ON hrmsConfiguration..cfstate_master.stateunkid = rcapplicant_master.present_stateunkid " & _
                            "	LEFT JOIN hrmsConfiguration..cfcity_master ON hrmsConfiguration..cfcity_master.cityunkid = rcapplicant_master.present_post_townunkid " & _
                            "	LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = rcapplicant_master.present_countryunkid " & _
                            "   LEFT JOIN " & _
                            "   ( " & _
                            "	    SELECT " & _
                            "	         rcapp_vacancy_mapping.applicantunkid " & _
                            "	        ,rcapp_vacancy_mapping.vacancyunkid " & _
                            "	        ,cfcommon_master.name AS vacancy " & _
                            "	        ,CONVERT(CHAR(8), openingdate, 112) AS ODate " & _
                            "	        ,CONVERT(CHAR(8), closingdate, 112) AS CDate " & _
                            "           ,ISNULL(hrstation_master.name,'') AS Branch " & _
                            "           ,ISNULL(hrdepartment_group_master.name,'') AS Department_Group " & _
                            "           ,ISNULL(hrdepartment_master.name,'') AS Job_Department " & _
                            "           ,ISNULL(hrsection_master.name,'') AS Section " & _
                            "           ,ISNULL(hrunit_master.name,'') AS Unit " & _
                            "           ,ISNULL(hrjobgroup_master.name,'') AS Job_Group " & _
                            "           ,ISNULL(hrjob_master.job_name,'') AS Job_Name " & _
                            "           ,ISNULL(EMPL.name,'') AS Employment_Type " & _
                            "           ,ISNULL(hrgradegroup_master.name,'') AS Grade_Group " & _
                            "           ,ISNULL(hrgrade_master.name,'') AS Grade " & _
                            "           ,ISNULL(PAYTYP.name,'') AS Pay_Type " & _
                            "           ,ISNULL(SFTTYP.name,'') AS Shift_Type " & _
                            "           ,ISNULL(pay_from,0) AS Pay_Range_From " & _
                            "           ,ISNULL(pay_to,0) AS Pay_Range_To " & _
                            "           ,ISNULL(rcvacancy_master.isexternalvacancy,0) as isexternalvacancy " & _
                            "       FROM rcapp_vacancy_mapping " & _
                            "	        JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                            "           LEFT JOIN cfcommon_master AS SFTTYP ON dbo.rcvacancy_master.shifttypeunkid = SFTTYP.masterunkid AND SFTTYP.mastertype = 22 " & _
                            "           LEFT JOIN cfcommon_master AS PAYTYP ON rcvacancy_master.paytypeunkid = PAYTYP.masterunkid AND PAYTYP.mastertype = 17 " & _
                            "           LEFT JOIN hrgrade_master ON rcvacancy_master.gradeunkid = hrgrade_master.gradeunkid " & _
                            "           LEFT JOIN hrgradegroup_master ON rcvacancy_master.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
                            "           LEFT JOIN cfcommon_master AS EMPL ON dbo.rcvacancy_master.employeementtypeunkid = EMPL.masterunkid AND EMPL.mastertype = 8 " & _
                            "           LEFT JOIN hrjob_master ON rcvacancy_master.jobunkid = hrjob_master.jobunkid " & _
                            "           LEFT JOIN hrjobgroup_master ON rcvacancy_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
                            "           LEFT JOIN hrunit_master ON rcvacancy_master.unitunkid = hrunit_master.unitunkid " & _
                            "           LEFT JOIN hrsection_master ON rcvacancy_master.sectionunkid = hrsection_master.sectionunkid " & _
                            "           LEFT JOIN hrdepartment_master ON rcvacancy_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                            "           LEFT JOIN hrdepartment_group_master ON rcvacancy_master.deptgroupunkid = hrdepartment_group_master.deptgroupunkid " & _
                            "           LEFT JOIN hrstation_master ON rcvacancy_master.stationunkid = hrstation_master.stationunkid " & _
                            "	        JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & " " & _
                            "       WHERE ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 AND rcapp_vacancy_mapping.vacancyunkid IN ( " & mstrVacancyUnkid & " ) " & _
                            "   )AS VAC ON rcapplicant_master.applicantunkid = VAC.applicantunkid " & _
                            "   LEFT JOIN rcapplicant_batchschedule_tran ON rcapplicant_batchschedule_tran.applicantunkid = rcapplicant_master.applicantunkid " & _
                            "   LEFT JOIN rcbatchschedule_master ON rcbatchschedule_master.batchscheduleunkid = rcapplicant_batchschedule_tran.batchscheduleunkid " & _
                            "WHERE rcapplicant_master.applicantunkid IN ( " & strEmpId & ") "
                    'Hemant (07 Oct 2019) -- [interviewdate, interviewtime, InterviewVenue, isexternalvacancy, employeecode ]
                    'Sohail (09 Oct 2018) - [rcapp_vacancy_mapping.isactive = 1]=[ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0]
                    'Nilay (16-Apr-2016) -- Start
                    'ENHANCEMENT - 59.1 - Including Completed Age and Running Age columns in Letter Fields
                    'ADDED -- [Completed birth Age, Running birth Age]
                    'Nilay (16-Apr-2016) -- End

                    'S.SANDEEP [04 JUN 2015] -- END


                    'S.SANDEEP [ 04 MAY 2012 ] -- END



                    'S.SANDEEP [ 02 MAY 2012 ] -- END

                    dsList = objdataOperation.ExecQuery(StrQ, strListName)
                    If objdataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objdataOperation.ErrorNumber & " : " & objdataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Case enImg_Email_RefId.Leave_Module


                    'Pinkal (18-Sep-2012) -- Start
                    'Enhancement : TRA Changes



                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ = "SELECT " & _
                    '         " hremployee_master.employeeunkid AS EmpId " & _
                    '         ", ISNULL(cfcommon_master.name,'') AS Title " & _
                    '         ",ISNULL(hremployee_master.employeecode,'') AS EmpCode " & _
                    '         ",ISNULL(hremployee_master.firstName,'') AS Firstname " & _
                    '         ",ISNULL(hremployee_master.surName,'') AS Surname " & _
                    '         ",ISNULL(hremployee_master.otherName,'') AS Othername " & _
                    '         ",ISNULL(hremployee_master.email,'') AS Email " & _
                    '         ",ISNULL(hrjob_master.Job_name,'') AS Job " & _
                    '         ",ISNULL(hrclassgroup_master.name,'') AS ClassGroup " & _
                    '         ",ISNULL(hrclasses_master.name,'') AS Class " & _
                    '         ",'" & Now & "' As Today " & _
                    '         ",'" & Company._Object._Code & "' As CompanyCode " & _
                    '         ",'" & Company._Object._Name & "' As CompanyName " & _
                    '         ",'" & Company._Object._Address1 & "' As Address1 " & _
                    '         ",'" & Company._Object._Address2 & "' As Address2 " & _
                    '         ",'" & Company._Object._City_Name & "' As C_City " & _
                    '         ",'" & Company._Object._State_Name & "' As C_State " & _
                    '         ",'" & Company._Object._Country_Name & "' As C_Country " & _
                    '         ",'" & Company._Object._Phone1 & "' As Phone1 " & _
                    '         ",'" & Company._Object._Phone2 & "' As Phone2 " & _
                    '         ",'" & Company._Object._Phone3 & "' As Phone3 " & _
                    '         ",'" & Company._Object._Fax & "' As Fax " & _
                    '         ",'" & Company._Object._Email & "' As Email " & _
                    '         ",'" & Company._Object._Website & "' As Website " & _
                    '         ",'" & Company._Object._Registerdno & "' As RegNo1 " & _
                    '         ",'" & Company._Object._Vatno & "' As RegNo2 " & _
                    '         ",'" & Company._Object._Tinno & "' As TinNo " & _
                    '         ",'" & Company._Object._Reference & "' As Reference " & _
                    '         ",lvleaveform.Formno " & _
                    '         ",Leavename " & _
                    '         ",CONVERT(CHAR(8),lvleaveform.Applydate,112) AS Applydate " & _
                    '         ",CONVERT(CHAR(8),lvleaveform.Startdate,112) AS Startdate " & _
                    '         ",CONVERT(CHAR(8),lvleaveform.Returndate,112) AS Enddate " & _
                    '         ", ISNULL((SELECT SUM(dayfraction) FROM lvleaveday_fraction WHERE lvleaveday_fraction.formunkid = lvleaveform.formunkid " & _
                    '         " AND lvleaveform.isvoid = 0 AND lvleaveday_fraction.isvoid = 0 AND leavedate BETWEEN lvleaveform.startdate AND lvleaveform.returndate AND ISNULL(lvleaveday_fraction.approverunkid,-1) <=0),0.00) Days " & _
                    '         ",ISNULL(lvleaveform.Addressonleave,'') AS Addressonleave " & _
                    '         ",ISNULL(lvleaveform.Remark,'') AS Remark " & _
                    '         ", CASE WHEN lvleaveform.statusunkid = 1 then @Approve " & _
                    '         "         WHEN lvleaveform.statusunkid = 2 then @Pending  " & _
                    '         "         WHEN lvleaveform.statusunkid = 3 then @Reject  " & _
                    '         "         WHEN lvleaveform.statusunkid = 4 then @ReSchedule " & _
                    '         "         WHEN lvleaveform.statusunkid = 6 then @Cancel " & _
                    '         "         WHEN lvleaveform.statusunkid = 7 then @Issued " & _
                    '         "        END as Status"

                    StrQ = "SELECT " & _
                           " hremployee_master.employeeunkid AS EmpId " & _
                           ",ISNULL(cfcommon_master.name,'') AS Title " & _
                           ",ISNULL(hremployee_master.employeecode,'') AS EmpCode " & _
                           ",ISNULL(hremployee_master.firstName,'') AS Firstname " & _
                           ",ISNULL(hremployee_master.surName,'') AS Surname " & _
                           ",ISNULL(hremployee_master.otherName,'') AS Othername " & _
                           ",ISNULL(LTRIM(RTRIM(hremployee_master.email)),'') AS Email " & _
                           ",ISNULL(CJB.Job_name,'') AS Job " & _
                           ",ISNULL(CCG.name,'') AS ClassGroup " & _
                           ",ISNULL(CCM.name,'') AS Class " & _
                           strCommonData & " " & _
                           ",lvleaveform.Formno " & _
                           ",Leavename " & _
                           ",CONVERT(CHAR(8),lvleaveform.Applydate,112) AS Applydate " & _
                           ",CONVERT(CHAR(8),lvleaveform.Startdate,112) AS Startdate " & _
                           ",CONVERT(CHAR(8),lvleaveform.Returndate,112) AS Enddate " & _
                           ", ISNULL((SELECT SUM(dayfraction) FROM lvleaveday_fraction WHERE lvleaveday_fraction.formunkid = lvleaveform.formunkid " & _
                           " AND lvleaveform.isvoid = 0 AND lvleaveday_fraction.isvoid = 0 AND leavedate BETWEEN lvleaveform.startdate AND lvleaveform.returndate AND ISNULL(lvleaveday_fraction.approverunkid,-1) <=0),0.00) Days " & _
                           ",ISNULL(lvleaveform.Addressonleave,'') AS Addressonleave " & _
                           ",ISNULL(lvleaveform.Remark,'') AS Remark " & _
                           ", CASE WHEN lvleaveform.statusunkid = 1 then @Approve " & _
                           "         WHEN lvleaveform.statusunkid = 2 then @Pending  " & _
                           "         WHEN lvleaveform.statusunkid = 3 then @Reject  " & _
                           "         WHEN lvleaveform.statusunkid = 4 then @ReSchedule " & _
                           "         WHEN lvleaveform.statusunkid = 6 then @Cancel " & _
                           "         WHEN lvleaveform.statusunkid = 7 then @Issued " & _
                           "        END as Status"
                    'S.SANDEEP [04 JUN 2015] -- END



                    'Pinkal (3-Aug-2013) -- Start
                    'Enhancement : TRA Changes

                    StrQ &= ",CONVERT(CHAR(8),lvleaveform.approve_stdate,112) AS 'Approved Start Date' " & _
                            ",CONVERT(CHAR(8),lvleaveform.approve_eddate,112) AS 'Approved End Date' " & _
                            ",ISNULL(lvleaveform.approve_days,0.00) AS 'Approved Days' " & _
                            ",CONVERT(CHAR(8),lvleavebalance_tran.startdate,112) AS 'Accrue Start Date' " & _
                            ",CONVERT(CHAR(8),lvleavebalance_tran.enddate,112) AS 'Accrue End Date' " & _
                            ",ISNULL(lvleavebalance_tran.accrue_amount,0.00) AS 'Leave Accrue' " & _
                            ",(ISNULL(lvleavebalance_tran.accrue_amount,0.00) - ISNULL(lvleavebalance_tran.issue_amount,0.00)) AS 'Leave Balance' " & _
                            ",ISNULL(lvleaveIssue_master.issuecount,0.00) AS 'Leave Issued' "

                    'Pinkal (3-Aug-2013) -- End


                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ &= "  FROM hremployee_master " & _
                    '        "       LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.titleunkid " & _
                    '        "       LEFT JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
                    '        "       LEFT JOIN hrclassgroup_master ON hremployee_master.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
                    '        "       LEFT JOIN hrclasses_master ON hremployee_master.classunkid = hrclasses_master.classesunkid " & _
                    '        "       LEFT JOIN hrmsConfiguration..cfstate_master  ON hremployee_master.present_stateunkid= hrmsConfiguration..cfstate_master .stateunkid " & _
                    '        "       LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.present_countryunkid " & _
                    '        "       LEFT JOIN hrmsConfiguration..cfzipcode_master  ON hremployee_master.present_postcodeunkid = hrmsConfiguration..cfzipcode_master .zipcodeunkid " & _
                    '        "       LEFT JOIN hrmsConfiguration..cfcity_master  ON hremployee_master.present_post_townunkid = hrmsConfiguration..cfcity_master .cityunkid " & _
                    '        "       JOIN lvleaveform ON lvleaveform.employeeunkid = hremployee_master.employeeunkid AND lvleaveform.isvoid = 0" & _
                    '        "       JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid "

                    StrQ &= "  FROM hremployee_master " & _
                            "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.titleunkid " & _
                            "    LEFT JOIN " & _
                            "    ( " & _
                            "        SELECT " & _
                            "             employeeunkid AS EmpId " & _
                            "            ,jobgroupunkid " & _
                            "            ,jobunkid " & _
                            "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "        FROM hremployee_categorization_tran " & _
                            "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                            "    ) AS ReCat ON ReCat.EmpId = hremployee_master.employeeunkid AND ReCat.rno = 1 " & _
                            "    LEFT JOIN hrjob_master AS CJB ON ReCat.jobunkid = CJB.jobunkid " & _
                            "       LEFT JOIN " & _
                            "       ( " & _
                            "           SELECT " & _
                            "                employeeunkid AS EmpId " & _
                            "               ,stationunkid " & _
                            "               ,deptgroupunkid " & _
                            "               ,departmentunkid " & _
                            "               ,sectiongroupunkid " & _
                            "               ,sectionunkid " & _
                            "               ,unitgroupunkid " & _
                            "               ,unitunkid " & _
                            "               ,teamunkid " & _
                            "               ,classgroupunkid " & _
                            "               ,classunkid " & _
                            "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "           FROM hremployee_transfer_tran " & _
                            "           WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                            "       ) AS Alloc ON Alloc.EmpId = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                            "       LEFT JOIN hrclassgroup_master AS CCG ON Alloc.classgroupunkid = CCG.classgroupunkid " & _
                            "       LEFT JOIN hrclasses_master AS CCM ON Alloc.classunkid = CCM.classesunkid " & _
                            "       LEFT JOIN hrmsConfiguration..cfstate_master  ON hremployee_master.present_stateunkid= hrmsConfiguration..cfstate_master .stateunkid " & _
                            "       LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.present_countryunkid " & _
                            "       LEFT JOIN hrmsConfiguration..cfzipcode_master  ON hremployee_master.present_postcodeunkid = hrmsConfiguration..cfzipcode_master .zipcodeunkid " & _
                            "       LEFT JOIN hrmsConfiguration..cfcity_master  ON hremployee_master.present_post_townunkid = hrmsConfiguration..cfcity_master .cityunkid " & _
                            "       JOIN lvleaveform ON lvleaveform.employeeunkid = hremployee_master.employeeunkid AND lvleaveform.isvoid = 0" & _
                            "       JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid "

                    'S.SANDEEP [04 JUN 2015] -- END

                    'Pinkal (3-Aug-2013) -- Start
                    'Enhancement : TRA Changes
                    StrQ &= " LEFT JOIN lvleaveIssue_master on lvleaveIssue_master.formunkid = lvleaveform.formunkid and lvleaveIssue_master.isvoid = 0 " & _
                                 " LEFT JOIN lvleavebalance_tran ON lvleavebalance_tran.employeeunkid = lvleaveform.employeeunkid AND lvleavebalance_tran.leavetypeunkid = lvleaveform.leavetypeunkid AND lvleavebalance_tran.isvoid = 0 "
                    'Pinkal (3-Aug-2013) -- End


                    StrQ &= "	WHERE  lvleaveform.formunkid IN ( " & strEmpId & " ) "


                    objdataOperation.ClearParameters()
                    objdataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
                    objdataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
                    objdataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
                    objdataOperation.AddParameter("@ReSchedule", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 113, "Re-Scheduled"))
                    objdataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))
                    objdataOperation.AddParameter("@Issued", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 277, "Issued"))



                    dsList = objdataOperation.ExecQuery(StrQ, strListName)

                    If objdataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objdataOperation.ErrorNumber & " : " & objdataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Pinkal (18-Sep-2012) -- End

                Case enImg_Email_RefId.Payroll_Module
                Case enImg_Email_RefId.Dependants_Beneficiaries
                Case enImg_Email_RefId.Medical_Module

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ = " SELECT " & _
                    '           "	 ISNULL(tnashift_master.shiftname,'') AS Shift " & _
                    '           "	,ISNULL(cfcommon_master.name,'') AS Title " & _
                    '           "	,ISNULL(hremployee_master.employeecode,'') AS EmpCode " & _
                    '           "	,ISNULL(hremployee_master.firstname,'') AS FirstName " & _
                    '           "	,ISNULL(hremployee_master.surname,'') AS SurName " & _
                    '           "	,ISNULL(hremployee_master.othername,'') AS OtherName " & _
                    '           "	,ISNULL(hremployee_master.email,'') AS Email " & _
                    '           "	,ISNULL(CONVERT(CHAR(8),hremployee_master.birthdate,112),'') AS BirthDate " & _
                    '           "	,ISNULL(CONVERT(CHAR(8),hremployee_master.anniversary_date,112),'') AS AnniversaryDate " & _
                    '           "	,ISNULL(hremployee_master.present_address1,'') AS EmpAddress1 " & _
                    '           "	,ISNULL(hremployee_master.present_address2,'') AS EmpAddress2 " & _
                    '           "	,ISNULL(hrmsConfiguration..cfcity_master .name,'') AS City " & _
                    '           "	,ISNULL(hrmsConfiguration..cfstate_master .name,'') AS State " & _
                    '           "	,ISNULL(hrmsConfiguration..cfcountry_master.country_name1,'') AS Country " & _
                    '           "	,ISNULL(hrstation_master.name,'') AS Branch " & _
                    '           "	,ISNULL(hrdepartment_master.name,'') AS Department " & _
                    '           "	,ISNULL(hrsection_master.name,'') AS Section " & _
                    '           "	,ISNULL(hrjob_master.job_name,'') AS Job " & _
                    '           "	,ISNULL(hrgrade_master.name,'') AS Grade " & _
                    '           "	,ISNULL(hrgradelevel_master.name,'') AS GradeLevel " & _
                    '           "	,ISNULL(hrclasses_master.name,'') AS Class " & _
                    '           "	,ISNULL(prcostcenter_master.costcentername,'') AS CostCenter " & _
                    '           "	,ISNULL(CONVERT(CHAR(8),hremployee_master.suspended_from_date,112),'') AS SuspendedFrom " & _
                    '           "	,ISNULL(CONVERT(CHAR(8),hremployee_master.suspended_to_date,112),'') AS SuspendedTo " & _
                    '           "	,ISNULL(CONVERT(CHAR(8),hremployee_master.probation_from_date,112),'') AS ProbationFrom " & _
                    '           "	,ISNULL(CONVERT(CHAR(8),hremployee_master.probation_to_date,112),'') AS ProbationTo " & _
                    '           "    ,ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),'') AS TerminationDate " & _
                    '           "    ,ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),'') AS RetirementDate " & _
                    '           "    ,ISNULL(CONVERT(CHAR(8),hremployee_master.confirmation_date,112),'') AS ConfirmationDate " & _
                    '           "    ,ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112),'') AS EmploymentLeavingDate " & _
                    '           "    ,ISNULL(CONVERT(CHAR(8),hremployee_master.appointeddate,112),'') AS ApponintmentDate " & _
                    '           "	,'" & Now.Date & "' As Today " & _
                    '           "    ,'" & Company._Object._Code & "' As CompanyCode " & _
                    '           "	,'" & Company._Object._Name & "' As CompanyName " & _
                    '           "	,'" & Company._Object._Address1 & "' As Address1 " & _
                    '           "	,'" & Company._Object._Address2 & "' As Address2 " & _
                    '           "    ,'" & Company._Object._City_Name & "' As C_City " & _
                    '           "    ,'" & Company._Object._State_Name & "' As C_State " & _
                    '           "    ,'" & Company._Object._Country_Name & "' As C_Country " & _
                    '           "	,'" & Company._Object._Phone1 & "' As Phone1 " & _
                    '           "	,'" & Company._Object._Phone2 & "' As Phone2 " & _
                    '           "	,'" & Company._Object._Phone3 & "' As Phone3 " & _
                    '           "	,'" & Company._Object._Fax & "' As Fax " & _
                    '           "	,'" & Company._Object._Email & "' As Email " & _
                    '           "	,'" & Company._Object._Website & "' As Website " & _
                    '           "	,'" & Company._Object._Registerdno & "' As RegNo1 " & _
                    '           "	,'" & Company._Object._Vatno & "' As RegNo2 " & _
                    '           "	,'" & Company._Object._Tinno & "' As TinNo " & _
                    '           "	,'" & Company._Object._Reference & "' As Reference " & _
                    '           "	,hremployee_master.employeeunkid AS EmpId " & _
                    '           "	,mdmedical_sicksheet.sicksheetunkid AS sicksheetunkid " & _
                    '           "	,mdmedical_sicksheet.sicksheetdate AS sicksheetdate " & _
                    '           "    ,ISNULL(mdmedical_sicksheet.instituteunkid,0) instituteunkid " & _
                    '           "    ,ISNULL(hrinstitute_master.institute_email,'') institute_email " & _
                    '           "  FROM hremployee_master " & _
                    '           "	LEFT JOIN prcostcenter_master ON hremployee_master.costcenterunkid = prcostcenter_master.costcenterunkid " & _
                    '           "	LEFT JOIN hrclasses_master ON hremployee_master.classunkid = hrclasses_master.classesunkid " & _
                    '           "	LEFT JOIN hrgradelevel_master ON hremployee_master.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
                    '           "	LEFT JOIN hrgrade_master ON hremployee_master.gradeunkid = hrgrade_master.gradeunkid " & _
                    '           "	LEFT JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
                    '           "	LEFT JOIN hrsection_master ON hremployee_master.sectionunkid = hrsection_master.sectionunkid " & _
                    '           "	LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                    '           "	LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid " & _
                    '           "	LEFT JOIN hrmsConfiguration..cfstate_master  ON hremployee_master.present_stateunkid= hrmsConfiguration..cfstate_master .stateunkid " & _
                    '           "	LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.present_countryunkid " & _
                    '           "	LEFT JOIN hrmsConfiguration..cfzipcode_master  ON hremployee_master.present_postcodeunkid = hrmsConfiguration..cfzipcode_master .zipcodeunkid " & _
                    '           "	LEFT JOIN hrmsConfiguration..cfcity_master  ON hremployee_master.present_post_townunkid = hrmsConfiguration..cfcity_master .cityunkid " & _
                    '           "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.titleunkid AND mastertype = " & enCommonMaster.TITLE & _
                    '           "	LEFT JOIN tnashift_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
                    '           "    JOIN mdmedical_sicksheet ON mdmedical_sicksheet.employeeunkid = hremployee_master.employeeunkid AND mdmedical_sicksheet.isvoid = 0 " & _
                    '           "    JOIN hrinstitute_master ON mdmedical_sicksheet.instituteunkid = hrinstitute_master.instituteunkid AND ishospital=1 AND hrinstitute_master.isactive = 1 " & _
                    '           "    WHERE mdmedical_sicksheet.sicksheetunkid IN ( " & strEmpId & " ) "

                    StrQ = " SELECT " & _
                           "	 ISNULL(tnashift_master.shiftname,'') AS Shift " & _
                           "	,ISNULL(cfcommon_master.name,'') AS Title " & _
                           "	,ISNULL(hremployee_master.employeecode,'') AS EmpCode " & _
                           "	,ISNULL(hremployee_master.firstname,'') AS FirstName " & _
                           "	,ISNULL(hremployee_master.surname,'') AS SurName " & _
                           "	,ISNULL(hremployee_master.othername,'') AS OtherName " & _
                           "	,ISNULL(LTRIM(RTRIM(hremployee_master.email)),'') AS Email " & _
                           "	,ISNULL(CONVERT(CHAR(8),hremployee_master.birthdate,112),'') AS BirthDate " & _
                           "    ,CASE WHEN ISNULL(hremployee_master.birthdate,'') = '' THEN '' " & _
                           "          ELSE CAST((CAST(CONVERT(char(8), GETDATE(), 112) AS int) - CAST(CONVERT(char(8), hremployee_master.birthdate, 112) AS int)) / 10000 AS nvarchar(max)) " & _
                           "     END AS [Completed birth Age] " & _
                           "    ,CASE WHEN ISNULL(hremployee_master.birthdate,'') = '' THEN '' " & _
                           "          ELSE CAST((CAST(CONVERT(char(8), GETDATE(), 112) AS int) - CAST(CONVERT(char(8), hremployee_master.birthdate, 112) AS int)) / 10000 + 1 AS nvarchar(max)) " & _
                           "     END AS [Running birth Age] " & _
                           "	,ISNULL(CONVERT(CHAR(8),hremployee_master.anniversary_date,112),'') AS AnniversaryDate " & _
                           "	,ISNULL(hremployee_master.present_address1,'') AS EmpAddress1 " & _
                           "	,ISNULL(hremployee_master.present_address2,'') AS EmpAddress2 " & _
                           "	,ISNULL(hrmsConfiguration..cfcity_master .name,'') AS City " & _
                           "	,ISNULL(hrmsConfiguration..cfstate_master .name,'') AS State " & _
                           "	,ISNULL(hrmsConfiguration..cfcountry_master.country_name1,'') AS Country " & _
                           "	,ISNULL(CST.name,'') AS Branch " & _
                           "	,ISNULL(CDT.name,'') AS Department " & _
                           "	,ISNULL(CSC.name,'') AS Section " & _
                           "	,ISNULL(CJB.job_name,'') AS Job " & _
                           "	,ISNULL(CGM.name,'') AS Grade " & _
                           "	,ISNULL(CGL.name,'') AS GradeLevel " & _
                           "	,ISNULL(CCM.name,'') AS Class " & _
                           "	,ISNULL(CCN.costcentername,'') AS CostCenter " & _
                           "	,ISNULL(CONVERT(CHAR(8),SuspDt.date1,112),'') AS SuspendedFrom " & _
                           "	,ISNULL(CONVERT(CHAR(8),SuspDt.date2,112),'') AS SuspendedTo " & _
                           "	,ISNULL(CONVERT(CHAR(8),PrbDt.date1,112),'') AS ProbationFrom " & _
                           "	,ISNULL(CONVERT(CHAR(8),PrbDt.date2,112),'') AS ProbationTo " & _
                           "    ,ISNULL(CONVERT(CHAR(8),EocDt.date2,112),'') AS TerminationDate " & _
                           "    ,ISNULL(CONVERT(CHAR(8),Retr.date1,112),'') AS RetirementDate " & _
                           "    ,ISNULL(CONVERT(CHAR(8),CnfDt.date1,112),'') AS ConfirmationDate " & _
                           "    ,ISNULL(CONVERT(CHAR(8),EocDt.date1,112),'') AS EmploymentLeavingDate " & _
                           "    ,ISNULL(CONVERT(CHAR(8),hremployee_master.appointeddate,112),'') AS ApponintmentDate " & _
                           strCommonData & " " & _
                           "	,hremployee_master.employeeunkid AS EmpId " & _
                           "	,mdmedical_sicksheet.sicksheetunkid AS sicksheetunkid " & _
                           "	,mdmedical_sicksheet.sicksheetdate AS sicksheetdate " & _
                           "    ,ISNULL(mdmedical_sicksheet.instituteunkid,0) instituteunkid " & _
                           "    ,ISNULL(hrinstitute_master.institute_email,'') institute_email " & _
                           "  FROM hremployee_master " & _
                           "    LEFT JOIN " & _
                           "    ( " & _
                           "        SELECT " & _
                           "             employeeunkid AS EmpId " & _
                           "            ,cctranheadvalueid " & _
                           "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                           "        FROM hremployee_cctranhead_tran " & _
                           "        WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                           "    ) AS CC ON CC.EmpId = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                           "    LEFT JOIN prcostcenter_master AS CCN ON CCN.costcenterunkid = CC.cctranheadvalueid " & _
                           "    LEFT JOIN " & _
                           "    ( " & _
                           "        SELECT " & _
                           "             date1 " & _
                           "            ,date2 " & _
                           "            ,employeeunkid " & _
                           "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                           "        FROM hremployee_dates_tran " & _
                           "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_SUSPENSION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                           "    )AS SuspDt ON SuspDt.employeeunkid = hremployee_master.employeeunkid AND SuspDt.rno = 1 " & _
                           "    LEFT JOIN " & _
                           "    ( " & _
                           "        SELECT " & _
                           "             date1 " & _
                           "            ,date2 " & _
                           "            ,employeeunkid " & _
                           "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                           "        FROM hremployee_dates_tran " & _
                           "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                           "    )AS PrbDt ON PrbDt.employeeunkid = hremployee_master.employeeunkid AND PrbDt.rno = 1 " & _
                           "    LEFT JOIN " & _
                           "    ( " & _
                           "        SELECT " & _
                           "             date1 " & _
                           "            ,date2 " & _
                           "            ,employeeunkid " & _
                           "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                           "        FROM hremployee_dates_tran " & _
                           "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                           "    )AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid AND EocDt.rno = 1 " & _
                           "    LEFT JOIN " & _
                           "    ( " & _
                           "        SELECT " & _
                           "             date1 " & _
                           "            ,employeeunkid " & _
                           "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                           "        FROM hremployee_dates_tran " & _
                           "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                           "    )AS Retr ON Retr.employeeunkid = hremployee_master.employeeunkid AND Retr.rno = 1 " & _
                           "    LEFT JOIN " & _
                           "    ( " & _
                           "        SELECT " & _
                           "             date1 " & _
                           "            ,employeeunkid " & _
                           "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                           "        FROM hremployee_dates_tran " & _
                           "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                           "    )AS CnfDt ON CnfDt.employeeunkid = hremployee_master.employeeunkid AND CnfDt.rno = 1 " & _
                           "    LEFT JOIN " & _
                           "    ( " & _
                           "        SELECT " & _
                           "             gradeunkid " & _
                           "            ,gradelevelunkid " & _
                           "            ,employeeunkid " & _
                           "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                           "        FROM prsalaryincrement_tran " & _
                           "        WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                           "    ) AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                           "    LEFT JOIN hrgrade_master AS CGM ON Grds.gradeunkid = CGM.gradeunkid " & _
                           "    LEFT JOIN hrgradelevel_master AS CGL ON Grds.gradelevelunkid = CGL.gradelevelunkid " & _
                           "    LEFT JOIN " & _
                           "    ( " & _
                           "        SELECT " & _
                           "             employeeunkid AS EmpId " & _
                           "            ,jobgroupunkid " & _
                           "            ,jobunkid " & _
                           "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                           "        FROM hremployee_categorization_tran " & _
                           "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                           "    ) AS ReCat ON ReCat.EmpId = hremployee_master.employeeunkid AND ReCat.rno = 1 " & _
                           "    LEFT JOIN hrjob_master AS CJB ON ReCat.jobunkid = CJB.jobunkid " & _
                           "    LEFT JOIN " & _
                           "    ( " & _
                           "        SELECT " & _
                           "             employeeunkid AS EmpId " & _
                           "            ,stationunkid " & _
                           "            ,departmentunkid " & _
                           "            ,sectionunkid " & _
                           "            ,classunkid " & _
                           "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                           "        FROM hremployee_transfer_tran " & _
                           "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                           "    ) AS Alloc ON Alloc.EmpId = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                           "    LEFT JOIN hrstation_master AS CST ON  CST.stationunkid = Alloc.stationunkid " & _
                           "    LEFT JOIN hrdepartment_master AS CDT ON Alloc.departmentunkid = CDT.departmentunkid " & _
                           "    LEFT JOIN hrsection_master AS CSC ON Alloc.sectionunkid = CSC.sectionunkid " & _
                           "    LEFT JOIN hrclasses_master AS CCM ON Alloc.classunkid = CCM.classesunkid " & _
                           "	LEFT JOIN hrmsConfiguration..cfstate_master  ON hremployee_master.present_stateunkid= hrmsConfiguration..cfstate_master .stateunkid " & _
                           "	LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.present_countryunkid " & _
                           "	LEFT JOIN hrmsConfiguration..cfzipcode_master  ON hremployee_master.present_postcodeunkid = hrmsConfiguration..cfzipcode_master .zipcodeunkid " & _
                           "	LEFT JOIN hrmsConfiguration..cfcity_master  ON hremployee_master.present_post_townunkid = hrmsConfiguration..cfcity_master .cityunkid " & _
                           "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.titleunkid AND mastertype = " & enCommonMaster.TITLE & _
                           "	LEFT JOIN tnashift_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
                           "    JOIN mdmedical_sicksheet ON mdmedical_sicksheet.employeeunkid = hremployee_master.employeeunkid AND mdmedical_sicksheet.isvoid = 0 " & _
                           "    JOIN hrinstitute_master ON mdmedical_sicksheet.instituteunkid = hrinstitute_master.instituteunkid AND ishospital=1 AND hrinstitute_master.isactive = 1 " & _
                           "    WHERE mdmedical_sicksheet.sicksheetunkid IN ( " & strEmpId & " ) "

                    'Nilay (16-Apr-2016) -- Start
                    'ENHANCEMENT - 59.1 - Including Completed Age and Running Age columns in Letter Fields
                    'ADDED -- [Completed birth Age, Running birth Age]
                    'Nilay (16-Apr-2016) -- End

                    'S.SANDEEP [04 JUN 2015] -- END

                    dsList = objdataOperation.ExecQuery(StrQ, strListName)
                    If objdataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objdataOperation.ErrorNumber & " : " & objdataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Case enImg_Email_RefId.Discipline_Module 'S.SANDEEP [ 20 APRIL 2012 ]

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'StrQ = "SELECT " & _
                    '        "	 ISNULL(cfcommon_master.name,'') AS OffenceCategory " & _
                    '        "	,ISNULL(hrdisciplinetype_master.name,'') AS DisciplinaryOffence " & _
                    '        "	,ISNULL(hrdiscipline_file.incident,'') AS StatementofOffence " & _
                    '        "	,ISNULL(Title.name,'') AS EmployeeTitle " & _
                    '        "	,CASE WHEN gender = 1 THEN @MALE " & _
                    '        "		  WHEN gender = 2 THEN @FEMALE " & _
                    '        "	 ELSE '' END AS  EmployeeGender " & _
                    '        "	,ISNULL(firstname,'') AS EmployeeFirstname " & _
                    '        "	,ISNULL(othername,'') AS EmployeeMiddlename " & _
                    '        "	,ISNULL(surname,'') AS EmployeeSurname " & _
                    '        "   ,ISNULL(hremployee_master.email,'') AS Email " & _
                    '        "	,ISNULL(present_address1,'') AS EmpAddress1 " & _
                    '        "	,ISNULL(present_address2,'') AS EmpAddress2 " & _
                    '        "	,ISNULL(hrmsConfiguration..cfcity_master .name,'') AS City " & _
                    '        "	,ISNULL(hrmsConfiguration..cfstate_master .name,'') AS State " & _
                    '        "	,ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
                    '        "	,ISNULL(hrjobgroup_master.name,'') AS JobGroup " & _
                    '        "	,ISNULL(hrjob_master.job_name,'') AS Job " & _
                    '        "	,ISNULL(hrdepartment_master.name,'') AS Department " & _
                    '        "	,ISNULL(hrclassgroup_master.name,'') AS EmpRegion " & _
                    '        "	,ISNULL(hrclasses_master.name,'') AS WorkStation " & _
                    '        "	,ISNULL(ReportGroup.name,'') AS ReportToJobGroup " & _
                    '        "	,ISNULL(Report.job_name,'') AS ReportToJob " & _
                    '        "	,ISNULL(CONVERT(CHAR(8),hrdiscipline_file.trandate,112),'') AS TransactionDate " & _
                    '        "	,ISNULL(CommitteeName,'') AS CommitteeName " & _
                    '        "	,ISNULL(DisciplinaryAction,'') AS DisciplinaryAction " & _
                    '        "	,ISNULL(ActionFromDate,'') AS ActionFromDate " & _
                    '        "	,ISNULL(ActionToDate,'') AS ActionToDate " & _
                    '        "	,ISNULL(CaseNo,'') AS CaseNo " & _
                    '        "	,ISNULL(CourtName,'') AS CourtName " & _
                    '        "	,ISNULL(FinalResolutionStep,'') AS FinalResolutionStep " & _
                    '        "	,ISNULL(Finaldisciplinestatus,'') AS FinalDisciplineStatus " & _
                    '        "   ,hremployee_master.employeeunkid AS EmpId " & _
                    '        "   ,ISNULL(CONVERT(CHAR(8),hrdiscipline_file.interdictdate,112),'') AS InterdictDate " & _
                    '        "   ,ISNULL(hrdiscipline_file.reference_no,'') AS Discipline_Ref_No " & _
                    '        "	,'" & Now.Date & "' As Today " & _
                    '        "   ,'" & Company._Object._Code & "' As CompanyCode " & _
                    '        "	,'" & Company._Object._Name & "' As CompanyName " & _
                    '        "	,'" & Company._Object._Address1 & "' As Address1 " & _
                    '        "	,'" & Company._Object._Address2 & "' As Address2 " & _
                    '        "   ,'" & Company._Object._City_Name & "' As C_City " & _
                    '        "   ,'" & Company._Object._State_Name & "' As C_State " & _
                    '        "   ,'" & Company._Object._Country_Name & "' As C_Country " & _
                    '        "	,'" & Company._Object._Phone1 & "' As Phone1 " & _
                    '        "	,'" & Company._Object._Phone2 & "' As Phone2 " & _
                    '        "	,'" & Company._Object._Phone3 & "' As Phone3 " & _
                    '        "	,'" & Company._Object._Fax & "' As Fax " & _
                    '        "	,'" & Company._Object._Email & "' As Email " & _
                    '        "	,'" & Company._Object._Website & "' As Website " & _
                    '        "	,'" & Company._Object._Registerdno & "' As RegNo1 " & _
                    '        "	,'" & Company._Object._Vatno & "' As RegNo2 " & _
                    '        "	,'" & Company._Object._Tinno & "' As TinNo " & _
                    '        "	,'" & Company._Object._Reference & "' As Reference " & _
                    '        "FROM hremployee_master " & _
                    '        "	LEFT JOIN hrclasses_master ON hremployee_master.classunkid = hrclasses_master.classesunkid " & _
                    '        "	LEFT JOIN hrclassgroup_master ON hremployee_master.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
                    '        "	LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                    '        "	LEFT JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
                    '        "	LEFT JOIN hrjobgroup_master ON hremployee_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
                    '        "	LEFT JOIN hrmsConfiguration..cfstate_master  ON hremployee_master.present_stateunkid= hrmsConfiguration..cfstate_master.stateunkid " & _
                    '        "	LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.present_countryunkid " & _
                    '        "	LEFT JOIN hrmsConfiguration..cfzipcode_master  ON hremployee_master.present_postcodeunkid = hrmsConfiguration..cfzipcode_master.zipcodeunkid " & _
                    '        "	LEFT JOIN hrmsConfiguration..cfcity_master  ON hremployee_master.present_post_townunkid = hrmsConfiguration..cfcity_master .cityunkid " & _
                    '        "	JOIN hrdiscipline_file ON hremployee_master.employeeunkid = hrdiscipline_file.involved_employeeunkid " & _
                    '        "	JOIN hrdisciplinetype_master ON hrdiscipline_file.disciplinetypeunkid = hrdisciplinetype_master.disciplinetypeunkid " & _
                    '        "	LEFT JOIN cfcommon_master ON hrdisciplinetype_master.offencecategoryunkid = cfcommon_master.masterunkid AND  mastertype = 36 " & _
                    '        "	LEFT JOIN cfcommon_master AS Title ON hremployee_master.titleunkid = Title.masterunkid AND  Title.mastertype = 24 " & _
                    '        "	LEFT JOIN hrjob_master AS Report ON hrjob_master.report_tounkid = Report.jobunkid " & _
                    '        "	LEFT JOIN hrjobgroup_master AS ReportGroup ON ReportGroup.jobgroupunkid = Report.jobgroupunkid " & _
                    '        "	LEFT JOIN " & _
                    '        "	( " & _
                    '        "		SELECT " & _
                    '        "			 CommitteeName AS CommitteeName " & _
                    '        "			,DisciplinaryAction AS DisciplinaryAction " & _
                    '        "			,CaseNo AS CaseNo " & _
                    '        "			,CourtName AS CourtName " & _
                    '        "			,resolutionstep AS FinalResolutionStep " & _
                    '        "			,DisciplineStatus AS Finaldisciplinestatus " & _
                    '        "			,ActionFromDate AS ActionFromDate " & _
                    '        "			,ActionToDate AS ActionToDate " & _
                    '        "			,DFId AS DFId " & _
                    '        "		FROM " & _
                    '        "		( " & _
                    '        "			SELECT " & _
                    '        "				 ISNULL(cfcommon_master.name,'') AS CommitteeName " & _
                    '        "				,disciplinefileunkid AS DFId " & _
                    '        "				,ISNULL(hraction_reason_master.reason_action,'') AS DisciplinaryAction " & _
                    '        "				,ISNULL(caseno,'') AS CaseNo " & _
                    '        "				,ISNULL(courtname,'') AS CourtName " & _
                    '        "				,resolutionunkid " & _
                    '        "				,resolutionstep " & _
                    '        "				,ISNULL(hrdisciplinestatus_master.name,'') AS DisciplineStatus " & _
                    '        "				,ISNULL(CONVERT(CHAR(8),action_start_date,112),'') AS ActionFromDate " & _
                    '        "				,ISNULL(CONVERT(CHAR(8),action_end_date,112),'') AS ActionToDate " & _
                    '        "				,ROW_NUMBER() OVER(PARTITION BY disciplinefileunkid ORDER BY resolutionunkid DESC) AS Nos " & _
                    '        "			FROM hrdiscipline_resolutions " & _
                    '        "				LEFT JOIN hrdisciplinestatus_master ON hrdiscipline_resolutions.disciplinestatusunkid = hrdisciplinestatus_master.disciplinestatusunkid " & _
                    '        "				JOIN hraction_reason_master ON actionreasonunkid = hrdiscipline_resolutions.disciplinaryactionunkid AND isreason = 0 " & _
                    '        "				LEFT JOIN cfcommon_master ON hrdiscipline_resolutions.committeemasterunkid = cfcommon_master.masterunkid AND  cfcommon_master.mastertype = 35 " & _
                    '        "		)AS A " & _
                    '        "		WHERE A.Nos = 1 " & _
                    '        "	) AS B ON B.DFId = hrdiscipline_file.disciplinefileunkid " & _
                    '        "WHERE hrdiscipline_file.disciplinefileunkid IN (" & strEmpId & ") "

                    'S.SANDEEP |08-JAN-2019| -- START
                    'StrQ = "SELECT " & _
                    '        "	 ISNULL(cfcommon_master.name,'') AS OffenceCategory " & _
                    '        "	,ISNULL(hrdisciplinetype_master.name,'') AS DisciplinaryOffence " & _
                    '        "	,ISNULL(hrdiscipline_file.incident,'') AS StatementofOffence " & _
                    '        "	,ISNULL(Title.name,'') AS EmployeeTitle " & _
                    '        "	,CASE WHEN gender = 1 THEN @MALE " & _
                    '        "		  WHEN gender = 2 THEN @FEMALE " & _
                    '        "	 ELSE '' END AS  EmployeeGender " & _
                    '        "	,ISNULL(firstname,'') AS EmployeeFirstname " & _
                    '        "	,ISNULL(othername,'') AS EmployeeMiddlename " & _
                    '        "	,ISNULL(surname,'') AS EmployeeSurname " & _
                    '        "   ,ISNULL(LTRIM(RTRIM(hremployee_master.email)),'') AS Email " & _
                    '        "	,ISNULL(present_address1,'') AS EmpAddress1 " & _
                    '        "	,ISNULL(present_address2,'') AS EmpAddress2 " & _
                    '        "	,ISNULL(hrmsConfiguration..cfcity_master .name,'') AS City " & _
                    '        "	,ISNULL(hrmsConfiguration..cfstate_master .name,'') AS State " & _
                    '        "	,ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
                    '        "	,ISNULL(CJG.name,'') AS JobGroup " & _
                    '        "	,ISNULL(CJB.job_name,'') AS Job " & _
                    '        "	,ISNULL(CDT.name,'') AS Department " & _
                    '        "	,ISNULL(CCG.name,'') AS EmpRegion " & _
                    '        "	,ISNULL(CCM.name,'') AS WorkStation " & _
                    '        "	,ISNULL(CONVERT(CHAR(8),hrdiscipline_file.trandate,112),'') AS TransactionDate " & _
                    '        "	,ISNULL(CommitteeName,'') AS CommitteeName " & _
                    '        "	,ISNULL(DisciplinaryAction,'') AS DisciplinaryAction " & _
                    '        "	,ISNULL(ActionFromDate,'') AS ActionFromDate " & _
                    '        "	,ISNULL(ActionToDate,'') AS ActionToDate " & _
                    '        "	,ISNULL(CaseNo,'') AS CaseNo " & _
                    '        "	,ISNULL(CourtName,'') AS CourtName " & _
                    '        "	,ISNULL(FinalResolutionStep,'') AS FinalResolutionStep " & _
                    '        "	,ISNULL(Finaldisciplinestatus,'') AS FinalDisciplineStatus " & _
                    '        "   ,hremployee_master.employeeunkid AS EmpId " & _
                    '        "   ,ISNULL(CONVERT(CHAR(8),hrdiscipline_file.interdictdate,112),'') AS InterdictDate " & _
                    '        "   ,ISNULL(hrdiscipline_file.reference_no,'') AS Discipline_Ref_No " & _
                    '        strCommonData & " " & _
                    '        "FROM hremployee_master " & _
                    '        "   LEFT JOIN " & _
                    '        "   ( " & _
                    '        "       SELECT " & _
                    '        "            employeeunkid AS EmpId " & _
                    '        "           ,jobgroupunkid " & _
                    '        "           ,jobunkid " & _
                    '        "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '        "       FROM hremployee_categorization_tran " & _
                    '        "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                    '        "   ) AS ReCat ON ReCat.EmpId = hremployee_master.employeeunkid AND ReCat.rno = 1 " & _
                    '        "   LEFT JOIN hrjobgroup_master AS CJG ON ReCat.jobgroupunkid = CJG.jobgroupunkid " & _
                    '        "   LEFT JOIN hrjob_master AS CJB ON ReCat.jobunkid = CJB.jobunkid " & _
                    '        "   LEFT JOIN " & _
                    '        "   ( " & _
                    '        "       SELECT " & _
                    '        "            employeeunkid AS EmpId " & _
                    '        "           ,departmentunkid " & _
                    '        "           ,classgroupunkid " & _
                    '        "           ,classunkid " & _
                    '        "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '        "       FROM hremployee_transfer_tran " & _
                    '        "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                    '        "   ) AS Alloc ON Alloc.EmpId = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    '        "   LEFT JOIN hrdepartment_master AS CDT ON Alloc.departmentunkid = CDT.departmentunkid " & _
                    '        "   LEFT JOIN hrclassgroup_master AS CCG ON Alloc.classgroupunkid = CCG.classgroupunkid " & _
                    '        "   LEFT JOIN hrclasses_master AS CCM ON Alloc.classunkid = CCM.classesunkid " & _
                    '        "	LEFT JOIN hrmsConfiguration..cfstate_master  ON hremployee_master.present_stateunkid= hrmsConfiguration..cfstate_master.stateunkid " & _
                    '        "	LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.present_countryunkid " & _
                    '        "	LEFT JOIN hrmsConfiguration..cfzipcode_master  ON hremployee_master.present_postcodeunkid = hrmsConfiguration..cfzipcode_master.zipcodeunkid " & _
                    '        "	LEFT JOIN hrmsConfiguration..cfcity_master  ON hremployee_master.present_post_townunkid = hrmsConfiguration..cfcity_master .cityunkid " & _
                    '        "	JOIN hrdiscipline_file ON hremployee_master.employeeunkid = hrdiscipline_file.involved_employeeunkid " & _
                    '        "	JOIN hrdisciplinetype_master ON hrdiscipline_file.disciplinetypeunkid = hrdisciplinetype_master.disciplinetypeunkid " & _
                    '        "	LEFT JOIN cfcommon_master ON hrdisciplinetype_master.offencecategoryunkid = cfcommon_master.masterunkid AND  mastertype = 36 " & _
                    '        "	LEFT JOIN cfcommon_master AS Title ON hremployee_master.titleunkid = Title.masterunkid AND  Title.mastertype = 24 " & _
                    '        "	LEFT JOIN " & _
                    '        "	( " & _
                    '        "		SELECT " & _
                    '        "			 CommitteeName AS CommitteeName " & _
                    '        "			,DisciplinaryAction AS DisciplinaryAction " & _
                    '        "			,CaseNo AS CaseNo " & _
                    '        "			,CourtName AS CourtName " & _
                    '        "			,resolutionstep AS FinalResolutionStep " & _
                    '        "			,DisciplineStatus AS Finaldisciplinestatus " & _
                    '        "			,ActionFromDate AS ActionFromDate " & _
                    '        "			,ActionToDate AS ActionToDate " & _
                    '        "			,DFId AS DFId " & _
                    '        "		FROM " & _
                    '        "		( " & _
                    '        "			SELECT " & _
                    '        "				 ISNULL(cfcommon_master.name,'') AS CommitteeName " & _
                    '        "				,disciplinefileunkid AS DFId " & _
                    '        "				,ISNULL(hraction_reason_master.reason_action,'') AS DisciplinaryAction " & _
                    '        "				,ISNULL(caseno,'') AS CaseNo " & _
                    '        "				,ISNULL(courtname,'') AS CourtName " & _
                    '        "				,resolutionunkid " & _
                    '        "				,resolutionstep " & _
                    '        "				,ISNULL(hrdisciplinestatus_master.name,'') AS DisciplineStatus " & _
                    '        "				,ISNULL(CONVERT(CHAR(8),action_start_date,112),'') AS ActionFromDate " & _
                    '        "				,ISNULL(CONVERT(CHAR(8),action_end_date,112),'') AS ActionToDate " & _
                    '        "				,ROW_NUMBER() OVER(PARTITION BY disciplinefileunkid ORDER BY resolutionunkid DESC) AS Nos " & _
                    '        "			FROM hrdiscipline_resolutions " & _
                    '        "				LEFT JOIN hrdisciplinestatus_master ON hrdiscipline_resolutions.disciplinestatusunkid = hrdisciplinestatus_master.disciplinestatusunkid " & _
                    '        "				JOIN hraction_reason_master ON actionreasonunkid = hrdiscipline_resolutions.disciplinaryactionunkid AND isreason = 0 " & _
                    '        "				LEFT JOIN cfcommon_master ON hrdiscipline_resolutions.committeemasterunkid = cfcommon_master.masterunkid AND  cfcommon_master.mastertype = 35 " & _
                    '        "		)AS A " & _
                    '        "		WHERE A.Nos = 1 " & _
                    '        "	) AS B ON B.DFId = hrdiscipline_file.disciplinefileunkid " & _
                    '        "WHERE hrdiscipline_file.disciplinefileunkid IN (" & strEmpId & ") "
                    StrQ = "SELECT " & _
                              "hremployee_master.employeecode AS EmployeeCode " & _
                            "	,ISNULL(Title.name,'') AS EmployeeTitle " & _
                             ",CASE WHEN hremployee_master.gender = 1 THEN @MALE WHEN hremployee_master.gender = 2 THEN @FEMALE ELSE '' END AS EmployeeGender " & _
                             ",hremployee_master.firstname AS EmployeeFirstname " & _
                             ",hremployee_master.othername AS EmployeeMiddlename " & _
                             ",hremployee_master.surname AS EmployeeSurname " & _
                             ",hremployee_master.email AS Email " & _
                             ",ISNULL(CST.name,'') AS EmployeeBranch " & _
                             ",ISNULL(CDG.name,'') AS EmployeeDepartmentGroup " & _
                             ",ISNULL(CDT.name,'') AS EmployeeDepartment " & _
                             ",ISNULL(CSG.name,'') AS EmployeeSectionGroup " & _
                             ",ISNULL(CSC.name,'') AS EmployeeSection " & _
                             ",ISNULL(CUG.name,'') AS EmployeeUnitGroup " & _
                             ",ISNULL(CUT.name,'') AS EmployeeUnit " & _
                             ",ISNULL(CTM.name,'') AS EmployeeTeam " & _
                             ",ISNULL(CCG.name,'') AS EmployeeClassGroup " & _
                             ",ISNULL(CCM.name,'') AS EmployeeClass " & _
                             ",ISNULL(CJG.name,'') AS EmployeeJobGroup " & _
                             ",ISNULL(CJB.job_name,'') AS EmployeeJob " & _
                             ",ISNULL(CGG.name,'') AS EmployeeGradeGroup " & _
                             ",ISNULL(CGM.name,'') AS EmployeeGrade " & _
                             ",ISNULL(CGL.name,'') AS EmployeeGradeLevel " & _
                             ",ISNULL(CCN.costcentername,'') AS EmployeeCostCenter " & _
                             ",ISNULL(rEmp.employeecode,'') AS ReportingCode " & _
                             ",ISNULL(rEmp.firstname,'') AS ReportingFirstname " & _
                             ",ISNULL(rEmp.surname,'') AS ReportingSurname " & _
                             ",ISNULL(rEmp.othername,'') AS ReportingOthername " & _
                             ",ISNULL(A.Charge_Status,'') AS ChargeStatus " & _
                             ",hrdiscipline_file_master.reference_no AS ReferenceNo " & _
                             ",ISNULL(CONVERT(NVARCHAR(8),hrdiscipline_file_master.chargedate,112),'') AS ChargeDate " & _
                             ",ISNULL(CONVERT(NVARCHAR(8),hrdiscipline_file_master.interdictiondate,112),'') AS InterdictionDate " & _
                             ",hrdiscipline_file_master.charge_description AS ChargeDescription " & _
                             ",ISNULL(A.Category,'') AS ChargeCategory " & _
                             ",ISNULL(Cnt.NoOfCount,0) AS TotalNoOfCount " & _
                             ",ISNULL(CONVERT(NVARCHAR(8),hrdiscipline_file_master.notification_date,112),'') AS NotificationDate " & _
                             ",ISNULL(Hear.HearingDate,'') AS HearingDate " & _
                             ",ISNULL(Hear.HearingTime,'') AS HearingTime " & _
                             ",ISNULL(Hear.HearingVenue,'') AS HearingVenue " & _
                             ",ISNULL(Hear.HearingRemark,'') AS HearingRemark " & _
                             ",ISNULL(Hear.HearingMembers,'') AS HearingMembers " & _
                            strCommonData & " " & _
                            "FROM hremployee_master " & _
                             "LEFT JOIN hremployee_reportto ON hremployee_master.employeeunkid = hremployee_reportto.employeeunkid AND ishierarchy = 1 AND isvoid = 0 " & _
                             "LEFT JOIN hremployee_master AS rEmp ON hremployee_reportto.reporttoemployeeunkid = rEmp.employeeunkid " & _
                             "LEFT JOIN " & _
                             "( " & _
                                  "SELECT " & _
                                        "employeeunkid AS EmpId " & _
                                       ",stationunkid " & _
                                       ",deptgroupunkid " & _
                                       ",departmentunkid " & _
                                       ",sectiongroupunkid " & _
                                       ",sectionunkid " & _
                                       ",unitgroupunkid " & _
                                       ",unitunkid " & _
                                       ",teamunkid " & _
                                       ",classgroupunkid " & _
                                       ",classunkid " & _
                                       ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "FROM hremployee_transfer_tran " & _
                                  "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                             ") AS Alloc ON Alloc.EmpId = hremployee_master.employeeunkid AND Alloc.rno = 1  " & _
                             "LEFT JOIN hrstation_master AS CST ON  CST.stationunkid = Alloc.stationunkid " & _
                             "LEFT JOIN hrdepartment_group_master AS CDG ON CDG.deptgroupunkid = Alloc.deptgroupunkid " & _
                             "LEFT JOIN hrdepartment_master AS CDT ON Alloc.departmentunkid = CDT.departmentunkid " & _
                             "LEFT JOIN hrsectiongroup_master AS CSG ON Alloc.sectiongroupunkid = CSG.sectiongroupunkid " & _
                             "LEFT JOIN hrsection_master AS CSC ON Alloc.sectionunkid = CSC.sectionunkid " & _
                             "LEFT JOIN hrunitgroup_master AS CUG ON Alloc.unitgroupunkid = CUG.unitgroupunkid " & _
                             "LEFT JOIN hrunit_master AS CUT ON Alloc.unitunkid = CUT.unitunkid " & _
                             "LEFT JOIN hrteam_master AS CTM ON Alloc.teamunkid = CTM.teamunkid " & _
                             "LEFT JOIN hrclassgroup_master AS CCG ON Alloc.classgroupunkid = CCG.classgroupunkid " & _
                             "LEFT JOIN hrclasses_master AS CCM ON Alloc.classunkid = CCM.classesunkid " & _
                            "   LEFT JOIN " & _
                            "   ( " & _
                            "       SELECT " & _
                            "            employeeunkid AS EmpId " & _
                            "           ,jobgroupunkid " & _
                            "           ,jobunkid " & _
                            "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                            "       FROM hremployee_categorization_tran " & _
                            "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                            "   ) AS ReCat ON ReCat.EmpId = hremployee_master.employeeunkid AND ReCat.rno = 1 " & _
                            "   LEFT JOIN hrjobgroup_master AS CJG ON ReCat.jobgroupunkid = CJG.jobgroupunkid " & _
                            "   LEFT JOIN hrjob_master AS CJB ON ReCat.jobunkid = CJB.jobunkid " & _
                            "   LEFT JOIN " & _
                            "   ( " & _
                            "       SELECT " & _
                                        "gradegroupunkid " & _
                                       ",gradeunkid " & _
                                       ",gradelevelunkid " & _
                                       ",employeeunkid " & _
                                       ",CAST(newscale AS NVARCHAR(MAX)) AS Scale " & _
                                       ",ROW_NUMBER()OVER(PARTITION BY prsalaryincrement_tran.employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC) AS rno " & _
                                  "FROM prsalaryincrement_tran " & _
                                  "WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                             ") AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1  " & _
                             "LEFT JOIN hrgradegroup_master AS CGG ON CGG.gradegroupunkid = Grds.gradegroupunkid " & _
                             "LEFT JOIN hrgrade_master AS CGM ON Grds.gradeunkid = CGM.gradeunkid " & _
                             "LEFT JOIN hrgradelevel_master AS CGL ON Grds.gradelevelunkid = CGL.gradelevelunkid " & _
                             "LEFT JOIN " & _
                             "( " & _
                                  "SELECT " & _
                            "            employeeunkid AS EmpId " & _
                                       ",cctranheadvalueid " & _
                            "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                  "FROM hremployee_cctranhead_tran " & _
                                  "WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                             ") AS CC ON CC.EmpId = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                             "LEFT JOIN prcostcenter_master AS CCN ON CCN.costcenterunkid = CC.cctranheadvalueid " & _
                            "	LEFT JOIN cfcommon_master AS Title ON hremployee_master.titleunkid = Title.masterunkid AND  Title.mastertype = 24 " & _
                             "JOIN hrdiscipline_file_master ON hrdiscipline_file_master.involved_employeeunkid = hremployee_master.employeeunkid " & _
                            "	LEFT JOIN " & _
                            "	( " & _
                            "		SELECT " & _
                                        "hrdiscipline_file_tran.disciplinefileunkid " & _
                                       ",COUNT(1) AS NoOfCount " & _
                                  "FROM hrdiscipline_file_tran " & _
                                  "WHERE hrdiscipline_file_tran.isvoid = 0 " & _
                                  "GROUP BY hrdiscipline_file_tran.disciplinefileunkid " & _
                             ") AS Cnt ON Cnt.disciplinefileunkid = hrdiscipline_file_master.disciplinefileunkid " & _
                             "LEFT JOIN " & _
                             "( " & _
                                  "SELECT " & _
                                       " CASE WHEN isexternal = 0 THEN @INTERNAL WHEN isexternal = 1 THEN @EXTERNAL END AS Category " & _
                                       ",disciplinefileunkid AS FileUnkid " & _
                                       ",disciplinestatusunkid AS StatusId " & _
                                       ",statustranunkid AS StatusTranId " & _
                                       ",CASE WHEN isexternal = 0 THEN 1 WHEN isexternal = 1 THEN 2 END AS CategoryId " & _
                                       ",Charge_Status " & _
                            "		FROM " & _
                            "		( " & _
                            "			SELECT " & _
                                             "hrdiscipline_status_tran.isexternal " & _
                                            ",hrdiscipline_status_tran.disciplinefileunkid " & _
                                            ",hrdiscipline_status_tran.disciplinestatusunkid " & _
                                            ",hrdiscipline_status_tran.statustranunkid " & _
                                            ",hrdisciplinestatus_master.name AS Charge_Status " & _
                                            ",ROW_NUMBER() OVER(PARTITION BY hrdiscipline_status_tran.disciplinefileunkid ORDER BY hrdiscipline_status_tran.statustranunkid DESC) AS RNO " & _
                                       "FROM hrdiscipline_status_tran " & _
                                            "JOIN hrdisciplinestatus_master ON hrdiscipline_status_tran.disciplinestatusunkid = hrdisciplinestatus_master.disciplinestatusunkid " & _
                                       "WHERE isvoid = 0 " & _
                                  ") AS Category " & _
                                  "WHERE RNO = 1 " & _
                             ") AS A ON A.FileUnkid = hrdiscipline_file_master.disciplinefileunkid " & _
                             "LEFT JOIN " & _
                             "( " & _
                                  "SELECT " & _
                                        "CONVERT(NVARCHAR(8),hearing_date,112) AS HearingDate " & _
                                       ",CONVERT(NVARCHAR(5),hearning_time,108) AS HearingTime " & _
                                       ",hearning_venue AS HearingVenue " & _
                                       ",remark AS HearingRemark " & _
                                       ",member.members AS HearingMembers " & _
                                       ",disciplinefileunkid " & _
                                  "FROM hrhearing_schedule_master " & _
                                  "LEFT JOIN " & _
                                  "( " & _
                                       "SELECT " & _
                                                 "hearingschedulemasterunkid " & _
                                            ",STUFF((SELECT ',' + CASE WHEN c1.employeeunkid > 0 THEN e1.firstname+' '+e1.surname ELSE c1.ex_name END " & _
                                                      "FROM hrhearing_schedule_tran t1 " & _
                                                           "JOIN hrdiscipline_committee AS c1 ON c1.committeetranunkid = t1.committeetranunkid " & _
                                                           "LEFT JOIN hremployee_master AS e1 ON c1.employeeunkid = e1.employeeunkid " & _
                                                      "WHERE t1.hearingschedulemasterunkid = t2.hearingschedulemasterunkid AND t1.isvoid = 0 " & _
                                                      "FOR XML PATH ('')) ,1, 1, '') AS members " & _
                                       "FROM hrhearing_schedule_tran AS t2 " & _
                                       "WHERE t2.isvoid = 0 GROUP BY t2.hearingschedulemasterunkid " & _
                                  ") AS member ON member.hearingschedulemasterunkid = hrhearing_schedule_master.hearingschedulemasterunkid " & _
                                  "WHERE isvoid = 0 AND status = 1 " & _
                             ") AS Hear ON Hear.disciplinefileunkid = hrdiscipline_file_master.disciplinefileunkid " & _
                        "WHERE hrdiscipline_file_master.isvoid = 0 AND hrdiscipline_file_master.disciplinefileunkid = '" & CInt(strEmpId) & "' "
                    'S.SANDEEP |08-JAN-2019| -- END

                    'S.SANDEEP [04 JUN 2015] -- END`



                    objdataOperation.AddParameter("@MALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 73, "Male"))
                    objdataOperation.AddParameter("@FEMALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 74, "Female"))
                    objdataOperation.AddParameter("@INTERNAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsDiscipline_file_master", 3, "Internal"))
                    objdataOperation.AddParameter("@EXTERNAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsDiscipline_file_master", 4, "External"))

                    dsList = objdataOperation.ExecQuery(StrQ, strListName)

                    If objdataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objdataOperation.ErrorNumber & " : " & objdataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    '",ISNULL(CONVERT(NVARCHAR(8),hrdiscipline_file_master.chargedate,112),'') AS ChargeDate "
                    '",ISNULL(CONVERT(NVARCHAR(8),hrdiscipline_file_master.interdictiondate,112),'') AS InterdictionDate "
                    '",ISNULL(CONVERT(NVARCHAR(8),hrdiscipline_file_master.notification_date,112),'') AS NotificationDate "

                    'Pinkal (16-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.

                Case enImg_Email_RefId.Leave_Planner_Module

                    StrQ = "SELECT " & _
                          "     hremployee_master.employeeunkid AS EmpId " & _
                          "    ,ISNULL(cfcommon_master.name,'') AS Title " & _
                          "    ,ISNULL(hremployee_master.employeecode,'') AS EmpCode " & _
                          "    ,ISNULL(hremployee_master.firstName,'') AS Firstname " & _
                          "    ,ISNULL(hremployee_master.surName,'') AS Surname " & _
                          "    ,ISNULL(hremployee_master.otherName,'') AS Othername " & _
                          "    ,ISNULL(LTRIM(RTRIM(hremployee_master.email)),'') AS Email " & _
                          "    ,ISNULL(CJB.Job_name,'') AS Job " & _
                          "    ,ISNULL(CCG.name,'') AS ClassGroup " & _
                          "    ,ISNULL(CCM.name,'') AS Class " & _
                          "    ,ISNULL(JGM.name,'') AS JobGroup " & _
                          "    ,ISNULL(BM.name,'') AS Branch " & _
                          "    ,ISNULL(DGM.name,'') AS DepartmentGroup " & _
                          "    ,ISNULL(DM.name,'') AS Department " & _
                          "    ,ISNULL(SGM.name,'') AS SectionGroup " & _
                          "    ,ISNULL(SM.name,'') AS Section " & _
                          "    ,ISNULL(UGM.name,'') AS UnitGroup " & _
                          "    ,ISNULL(UM.name,'') AS Unit " & _
                          "    ,ISNULL(TM.name,'') AS Team " & _
                          "    ,Leavename " & _
                          "    ,CONVERT(CHAR(8),lvleaveplanner.startdate,112) AS Startdate " & _
                          "    ,CONVERT(CHAR(8),lvleaveplanner.stopdate,112) AS Enddate " & _
                          "    , ISNULL((SELECT SUM(dayfraction) FROM lvplannerday_fraction WHERE lvplannerday_fraction.leaveplannerunkid = lvleaveplanner.leaveplannerunkid " & _
                          "      AND lvleaveplanner.isvoid = 0 AND lvplannerday_fraction.isvoid = 0 AND leavedate BETWEEN lvleaveplanner.startdate AND lvleaveplanner.stopdate),0.00) Days " & _
                          " FROM hremployee_master " & _
                          "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.titleunkid " & _
                          "    LEFT JOIN " & _
                          "    ( " & _
                          "        SELECT " & _
                          "             employeeunkid AS EmpId " & _
                          "            ,jobgroupunkid " & _
                          "            ,jobunkid " & _
                          "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                          "        FROM hremployee_categorization_tran " & _
                          "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                          "    ) AS ReCat ON ReCat.EmpId = hremployee_master.employeeunkid AND ReCat.rno = 1 " & _
                          "    LEFT JOIN hrjob_master AS CJB ON ReCat.jobunkid = CJB.jobunkid " & _
                          "    LEFT JOIN hrjobgroup_master AS JGM ON ReCat.jobgroupunkid = JGM.jobgroupunkid " & _
                          "    LEFT JOIN " & _
                          "    ( " & _
                          "           SELECT " & _
                          "                employeeunkid AS EmpId " & _
                          "               ,stationunkid " & _
                          "               ,deptgroupunkid " & _
                          "               ,departmentunkid " & _
                          "               ,sectiongroupunkid " & _
                          "               ,sectionunkid " & _
                          "               ,unitgroupunkid " & _
                          "               ,unitunkid " & _
                          "               ,teamunkid " & _
                          "               ,classgroupunkid " & _
                          "               ,classunkid " & _
                          "               ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                          "           FROM hremployee_transfer_tran " & _
                          "           WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                          "    ) AS Alloc ON Alloc.EmpId = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                          "    LEFT JOIN hrclassgroup_master AS CCG ON Alloc.classgroupunkid = CCG.classgroupunkid " & _
                          "    LEFT JOIN hrclasses_master AS CCM ON Alloc.classunkid = CCM.classesunkid " & _
                          "    LEFT JOIN hrstation_master AS BM ON Alloc.stationunkid = BM.stationunkid " & _
                          "    LEFT JOIN hrdepartment_group_master AS DGM ON Alloc.deptgroupunkid = DGM.deptgroupunkid " & _
                          "    LEFT JOIN hrdepartment_master AS DM ON Alloc.departmentunkid = DM.departmentunkid " & _
                          "    LEFT JOIN hrsectiongroup_master AS SGM ON Alloc.sectiongroupunkid = SGM.sectiongroupunkid " & _
                          "    LEFT JOIN hrsection_master AS SM ON Alloc.sectionunkid = SM.sectionunkid " & _
                          "    LEFT JOIN hrunitgroup_master AS UGM ON Alloc.unitgroupunkid = UGM.unitgroupunkid " & _
                          "    LEFT JOIN hrunit_master AS UM ON Alloc.unitunkid = UM.unitunkid " & _
                          "    LEFT JOIN hrteam_master AS TM ON Alloc.teamunkid = TM.teamunkid " & _
                          "    LEFT JOIN hrmsConfiguration..cfstate_master  ON hremployee_master.present_stateunkid= hrmsConfiguration..cfstate_master .stateunkid " & _
                          "    LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.present_countryunkid " & _
                          "    LEFT JOIN hrmsConfiguration..cfzipcode_master  ON hremployee_master.present_postcodeunkid = hrmsConfiguration..cfzipcode_master .zipcodeunkid " & _
                          "    LEFT JOIN hrmsConfiguration..cfcity_master  ON hremployee_master.present_post_townunkid = hrmsConfiguration..cfcity_master .cityunkid " & _
                          "   JOIN lvleaveplanner ON lvleaveplanner.employeeunkid = hremployee_master.employeeunkid AND lvleaveplanner.isvoid = 0" & _
                          "   JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveplanner.leavetypeunkid " & _
                          "   WHERE lvleaveplanner.leaveplannerunkid  IN (" & strEmpId & ")"


                    objdataOperation.ClearParameters()
                    dsList = objdataOperation.ExecQuery(StrQ, strListName)

                    If objdataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objdataOperation.ErrorNumber & " : " & objdataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    'Pinkal (16-Oct-2018) -- End

                    'Gajanan [13-July-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Enhancement
                Case enImg_Email_RefId.Grievance
                    StrQ = "SELECT " & _
                           "ISNULL(GM_RaisedEmp.employeecode, '') AS [Raised Employee EmpCode] " & _
                           ",ISNULL(GM_RaisedEmp.firstName, '') AS [Raised Employee FirstName] " & _
                           ",ISNULL(GM_RaisedEmp.surname, '') AS [Raised Employee SurName] " & _
                           ",ISNULL(GM_RaisedEmp.otherName, '') AS [Raised Employee OtherName] " & _
                           ",ISNULL(LTRIM(RTRIM(GM_RaisedEmp.email)), '') AS [Raised Employee Email] " & _
                           ",ISNULL(RaisedEmpBM.name, '') AS [Raised Employee Branch] " & _
                           ",ISNULL(RaisedEmpDGM.name, '') AS [Raised Employee DepartmentGroup] " & _
                           ",ISNULL(RaisedEmpDM.name, '') AS [Raised Employee Department] " & _
                           ",ISNULL(RaisedEmpSGM.name, '') AS [Raised Employee SectionGroup] " & _
                           ",ISNULL(RaisedEmpSM.name, '') AS [Raised Employee Section] " & _
                           ",ISNULL(RaisedEmpUGM.name, '') AS [Raised Employee UnitGroup] " & _
                           ",ISNULL(RaisedEmpUM.name, '') AS [Raised Employee Unit] " & _
                           ",ISNULL(RaisedEmpTM.name, '') AS [Raised Employee Team] " & _
                           ",ISNULL(RaisedEmpJGM.name, '') AS [Raised Employee JobGroup] " & _
                           ",ISNULL(RaisedEmpCJB.Job_name, '') AS [Raised Employee Job] " & _
                           ",ISNULL(RaisedEmpCCG.name, '') AS [Raised Employee ClassGroup] " & _
                           ",ISNULL(RaisedEmpCCM.name, '') AS [Raised Employee Class] " & _
                           ",ISNULL(RaisedEmpCCN.costcentername, '') AS [Raised Employee CostCenter] " & _
                           " " & _
                           ",ISNULL(GM_AgainstEmp.employeecode, '') AS [Against Employee EmpCode] " & _
                           ",ISNULL(GM_AgainstEmp.firstName, '') AS [Against Employee FirstName] " & _
                           ",ISNULL(GM_AgainstEmp.surname, '') AS [Against Employee SurName] " & _
                           ",ISNULL(GM_AgainstEmp.otherName, '') AS [Against Employee OtherName] " & _
                           ",ISNULL(LTRIM(RTRIM(GM_AgainstEmp.email)), '') AS [Against Employee Email] " & _
                           ",ISNULL(AgainstEmpBM.name, '') AS [Against Employee Branch] " & _
                           ",ISNULL(AgainstEmpDGM.name, '') AS [Against Employee DepartmentGroup] " & _
                           ",ISNULL(AgainstEmpDM.name, '') AS [Against Employee Department] " & _
                           ",ISNULL(AgainstEmpSGM.name, '') AS [Against Employee SectionGroup] " & _
                           ",ISNULL(AgainstEmpSM.name, '') AS [Against Employee Section] " & _
                           ",ISNULL(AgainstEmpUGM.name, '') AS [Against Employee UnitGroup] " & _
                           ",ISNULL(AgainstEmpUM.name, '') AS [Against Employee Unit] " & _
                           ",ISNULL(AgainstEmpTM.name, '') AS [Against Employee Team] " & _
                           ",ISNULL(AgainstEmpJGM.name, '') AS [Against Employee JobGroup] " & _
                           ",ISNULL(AgainstEmpCJB.Job_name, '') AS [Against Employee Job] " & _
                           ",ISNULL(AgainstEmpCCG.name, '') AS [Against Employee ClassGroup] " & _
                           ",ISNULL(AgainstEmpCCM.name, '') AS [Against Employee Class] " & _
                           ",ISNULL(AgainstEmpCCN.costcentername, '') AS [Against Employee CostCenter] " & _
                           ",gregrievance_master.grievancerefno AS [Grievance Reference No] " & _
                           ",gregrievance_master.grievance_description AS [Grievance Description] " & _
                           ",gregrievance_master.grievance_reliefwanted AS [Grievance Reliefwanted] " & _
                           strCommonData & " " & _
                        "FROM hremployee_master AS GM_RaisedEmp " & _
                        "LEFT JOIN gregrievance_master " & _
                             "ON gregrievance_master.fromemployeeunkid = GM_RaisedEmp.employeeunkid " & _
                        "LEFT JOIN hremployee_master AS GM_AgainstEmp " & _
                             "ON GM_AgainstEmp.employeeunkid = gregrievance_master.againstemployeeunkid " & _
                        "LEFT JOIN (SELECT " & _
                                  "employeeunkid AS RaisedEmpId " & _
                                ",jobgroupunkid AS RaisedEmpjobgroupunkid " & _
                                ",jobunkid AS RaisedEmpjobunkid " & _
                                ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                             "FROM hremployee_categorization_tran " & _
                             "WHERE isvoid = 0 " & _
                             "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "') AS RaisedEmpReCat " & _
                             "ON RaisedEmpReCat.RaisedEmpId = GM_RaisedEmp.employeeunkid " & _
                                  "AND RaisedEmpReCat.rno = 1 " & _
                        "LEFT JOIN (SELECT " & _
                                  "employeeunkid AS AgainstEmpId " & _
                                ",jobgroupunkid AS AgainstEmpjobgroupunkid " & _
                                ",jobunkid AS AgainstEmpjobunkid " & _
                                ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                             "FROM hremployee_categorization_tran " & _
                             "WHERE isvoid = 0 " & _
                             "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "') AS AgainstEmpReCat " & _
                             "ON AgainstEmpReCat.AgainstEmpId = GM_AgainstEmp.employeeunkid " & _
                                  "AND AgainstEmpReCat.rno = 1 " & _
                        "LEFT JOIN hrjob_master AS RaisedEmpCJB " & _
                             "ON RaisedEmpReCat.RaisedEmpjobunkid = RaisedEmpCJB.jobunkid " & _
                        "LEFT JOIN hrjob_master AS AgainstEmpCJB " & _
                             "ON AgainstEmpReCat.AgainstEmpjobunkid = AgainstEmpCJB.jobunkid " & _
                        "LEFT JOIN hrjobgroup_master AS RaisedEmpJGM " & _
                             "ON RaisedEmpReCat.RaisedEmpjobgroupunkid = RaisedEmpJGM.jobgroupunkid " & _
                        "LEFT JOIN hrjobgroup_master AS AgainstEmpJGM " & _
                             "ON AgainstEmpReCat.AgainstEmpjobgroupunkid = AgainstEmpJGM.jobgroupunkid " & _
                        "LEFT JOIN (SELECT " & _
                                    "employeeunkid AS EmpId " & _
                                    ",stationunkid " & _
                                    ",deptgroupunkid " & _
                                    ",departmentunkid " & _
                                    ",sectiongroupunkid " & _
                                    ",sectionunkid " & _
                                    ",unitgroupunkid " & _
                                    ",unitunkid " & _
                                    ",teamunkid " & _
                                    ",classgroupunkid " & _
                                    ",classunkid " & _
                                    ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                             "FROM hremployee_transfer_tran " & _
                             "WHERE isvoid = 0 " & _
                             "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "') AS RaisedEmpAlloc " & _
                             "ON RaisedEmpAlloc.EmpId = GM_RaisedEmp.employeeunkid " & _
                                  "AND RaisedEmpAlloc.rno = 1 " & _
                        "LEFT JOIN (SELECT " & _
                                    "employeeunkid AS EmpId " & _
                                    ",stationunkid " & _
                                    ",deptgroupunkid " & _
                                    ",departmentunkid " & _
                                    ",sectiongroupunkid " & _
                                    ",sectionunkid " & _
                                    ",unitgroupunkid " & _
                                    ",unitunkid " & _
                                    ",teamunkid " & _
                                    ",classgroupunkid " & _
                                    ",classunkid " & _
                                    ",ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                             "FROM hremployee_transfer_tran " & _
                             "WHERE isvoid = 0 " & _
                             "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "') AS AgainstEmpAlloc " & _
                             "ON AgainstEmpAlloc.EmpId = GM_AgainstEmp.employeeunkid " & _
                                  "AND AgainstEmpAlloc.rno = 1 " & _
                        "LEFT JOIN hrclassgroup_master AS RaisedEmpCCG " & _
                             "ON RaisedEmpAlloc.classgroupunkid = RaisedEmpCCG.classgroupunkid " & _
                        "LEFT JOIN hrclasses_master AS RaisedEmpCCM " & _
                             "ON RaisedEmpAlloc.classunkid = RaisedEmpCCM.classesunkid " & _
                        "LEFT JOIN hrstation_master AS RaisedEmpBM " & _
                             "ON RaisedEmpAlloc.stationunkid = RaisedEmpBM.stationunkid " & _
                        "LEFT JOIN hrdepartment_group_master AS RaisedEmpDGM " & _
                             "ON RaisedEmpAlloc.deptgroupunkid = RaisedEmpDGM.deptgroupunkid " & _
                        "LEFT JOIN hrdepartment_master AS RaisedEmpDM " & _
                             "ON RaisedEmpAlloc.departmentunkid = RaisedEmpDM.departmentunkid " & _
                        "LEFT JOIN hrsectiongroup_master AS RaisedEmpSGM " & _
                             "ON RaisedEmpAlloc.sectiongroupunkid = RaisedEmpSGM.sectiongroupunkid " & _
                        "LEFT JOIN hrsection_master AS RaisedEmpSM " & _
                             "ON RaisedEmpAlloc.sectionunkid = RaisedEmpSM.sectionunkid " & _
                        "LEFT JOIN hrunitgroup_master AS RaisedEmpUGM " & _
                             "ON RaisedEmpAlloc.unitgroupunkid = RaisedEmpUGM.unitgroupunkid " & _
                        "LEFT JOIN hrunit_master AS RaisedEmpUM " & _
                             "ON RaisedEmpAlloc.unitunkid = RaisedEmpUM.unitunkid " & _
                        "LEFT JOIN hrteam_master AS RaisedEmpTM " & _
                             "ON RaisedEmpAlloc.teamunkid = RaisedEmpTM.teamunkid " & _
                        "LEFT JOIN (SELECT TOP 1 " & _
                                  "employeeunkid AS EmpId " & _
                                ",cctranheadvalueid " & _
                             "FROM hremployee_cctranhead_tran " & _
                             "WHERE isvoid = 0 " & _
                             "AND istransactionhead = 0 " & _
                             "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                             "ORDER BY effectivedate DESC) AS RaisedEmpCC " & _
                             "ON RaisedEmpCC.EmpId = GM_RaisedEmp.employeeunkid " & _
                        "LEFT JOIN prcostcenter_master AS RaisedEmpCCN " & _
                             "ON RaisedEmpCCN.costcenterunkid = RaisedEmpCC.cctranheadvalueid " & _
                        "LEFT JOIN hrclassgroup_master AS AgainstEmpCCG " & _
                             "ON AgainstEmpAlloc.classgroupunkid = AgainstEmpCCG.classgroupunkid " & _
                        "LEFT JOIN hrclasses_master AS AgainstEmpCCM " & _
                             "ON AgainstEmpAlloc.classunkid = AgainstEmpCCM.classesunkid " & _
                        "LEFT JOIN hrstation_master AS AgainstEmpBM " & _
                             "ON AgainstEmpAlloc.stationunkid = AgainstEmpBM.stationunkid " & _
                        "LEFT JOIN hrdepartment_group_master AS AgainstEmpDGM " & _
                             "ON AgainstEmpAlloc.deptgroupunkid = AgainstEmpDGM.deptgroupunkid " & _
                        "LEFT JOIN hrdepartment_master AS AgainstEmpDM " & _
                             "ON AgainstEmpAlloc.departmentunkid = AgainstEmpDM.departmentunkid " & _
                        "LEFT JOIN hrsectiongroup_master AS AgainstEmpSGM " & _
                             "ON AgainstEmpAlloc.sectiongroupunkid = AgainstEmpSGM.sectiongroupunkid " & _
                        "LEFT JOIN hrsection_master AS AgainstEmpSM " & _
                             "ON AgainstEmpAlloc.sectionunkid = AgainstEmpSM.sectionunkid " & _
                        "LEFT JOIN hrunitgroup_master AS AgainstEmpUGM " & _
                             "ON AgainstEmpAlloc.unitgroupunkid = AgainstEmpUGM.unitgroupunkid " & _
                        "LEFT JOIN hrunit_master AS AgainstEmpUM " & _
                             "ON AgainstEmpAlloc.unitunkid = AgainstEmpUM.unitunkid " & _
                        "LEFT JOIN hrteam_master AS AgainstEmpTM " & _
                             "ON AgainstEmpAlloc.teamunkid = AgainstEmpTM.teamunkid " & _
                        "LEFT JOIN (SELECT TOP 1 " & _
                                  "employeeunkid AS EmpId " & _
                                ",cctranheadvalueid " & _
                             "FROM hremployee_cctranhead_tran " & _
                             "WHERE isvoid = 0 " & _
                             "AND istransactionhead = 0 " & _
                             "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                             "ORDER BY effectivedate DESC) AS AgainstEmpCC " & _
                             "ON RaisedEmpCC.EmpId = GM_AgainstEmp.employeeunkid " & _
                        "LEFT JOIN prcostcenter_master AS AgainstEmpCCN " & _
                             "ON AgainstEmpCCN.costcenterunkid = AgainstEmpCC.cctranheadvalueid " & _
                        "WHERE gregrievance_master.grievancerefno IN ('" & strEmpId & "') "

                    'Gajanan [19-July-2019] -- ADD  [Grievance Reference No],[Grievance Description],[Grievance Reliefwanted]

                    'StrQ = " SELECT " & _
                    '   "	ISNULL(hremployee_master.employeecode,'') AS EmpCode " & _
                    '   "	,ISNULL(hremployee_master.firstname,'') AS FirstName " & _
                    '   "	,ISNULL(hremployee_master.surname,'') AS SurName " & _
                    '   "	,ISNULL(hremployee_master.othername,'') AS OtherName " & _
                    '   "	,ISNULL(LTRIM(RTRIM(hremployee_master.email)),'') AS Email " & _
                    '   "	,ISNULL(CST.name,'') AS Branch " & _
                    '   "	,ISNULL(CDT.name,'') AS Department " & _
                    '   "	,ISNULL(CSC.name,'') AS Section " & _
                    '   "	,ISNULL(CJB.job_name,'') AS Job " & _
                    '   "	,ISNULL(CGM.name,'') AS Grade " & _
                    '   "	,ISNULL(CGL.name,'') AS GradeLevel " & _
                    '   "	,ISNULL(CCM.name,'') AS Class " & _
                    '   "	,ISNULL(CCN.costcentername,'') AS CostCenter " & _
                    '   strCommonData & " " & _
                    '   "	,hremployee_master.employeeunkid AS EmpId " & _
                    '   "    ,ISNULL(GM.grievancerefno,'') as GrievanceRefNo " & _
                    '   "    ,ISNULL(GM.grievance_description,'') as GrievanceDescription" & _
                    '   "    ,ISNULL(GM.grievance_reliefwanted,'') as GrievanceReliefwanted" & _
                    '   "  FROM hremployee_master " & _
                    '   "    LEFT JOIN " & _
                    '   "    ( " & _
                    '   "        SELECT " & _
                    '   "             employeeunkid AS EmpId " & _
                    '   "            ,cctranheadvalueid " & _
                    '   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '   "        FROM hremployee_cctranhead_tran " & _
                    '   "        WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                    '   "    ) AS CC ON CC.EmpId = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                    '   "    LEFT JOIN prcostcenter_master AS CCN ON CCN.costcenterunkid = CC.cctranheadvalueid " & _
                    '   "    LEFT JOIN " & _
                    '   "    ( " & _
                    '   "        SELECT " & _
                    '   "             date1 " & _
                    '   "            ,date2 " & _
                    '   "            ,employeeunkid " & _
                    '   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '   "        FROM hremployee_dates_tran " & _
                    '   "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_SUSPENSION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                    '   "    )AS SuspDt ON SuspDt.employeeunkid = hremployee_master.employeeunkid AND SuspDt.rno = 1 " & _
                    '   "    LEFT JOIN " & _
                    '   "    ( " & _
                    '   "        SELECT " & _
                    '   "             date1 " & _
                    '   "            ,date2 " & _
                    '   "            ,employeeunkid " & _
                    '   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '   "        FROM hremployee_dates_tran " & _
                    '   "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                    '   "    )AS PrbDt ON PrbDt.employeeunkid = hremployee_master.employeeunkid AND PrbDt.rno = 1 " & _
                    '   "    LEFT JOIN " & _
                    '   "    ( " & _
                    '   "        SELECT " & _
                    '   "             date1 " & _
                    '   "            ,date2 " & _
                    '   "            ,employeeunkid " & _
                    '   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '   "        FROM hremployee_dates_tran " & _
                    '   "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                    '   "    )AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid AND EocDt.rno = 1 " & _
                    '   "    LEFT JOIN " & _
                    '   "    ( " & _
                    '   "        SELECT " & _
                    '   "             date1 " & _
                    '   "            ,employeeunkid " & _
                    '   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '   "        FROM hremployee_dates_tran " & _
                    '   "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                    '   "    )AS Retr ON Retr.employeeunkid = hremployee_master.employeeunkid AND Retr.rno = 1 " & _
                    '   "    LEFT JOIN " & _
                    '   "    ( " & _
                    '   "        SELECT " & _
                    '   "             date1 " & _
                    '   "            ,employeeunkid " & _
                    '   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '   "        FROM hremployee_dates_tran " & _
                    '   "        WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                    '   "    )AS CnfDt ON CnfDt.employeeunkid = hremployee_master.employeeunkid AND CnfDt.rno = 1 " & _
                    '   "    LEFT JOIN " & _
                    '   "    ( " & _
                    '   "        SELECT " & _
                    '   "             gradeunkid " & _
                    '   "            ,gradelevelunkid " & _
                    '   "            ,employeeunkid " & _
                    '   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                    '   "        FROM prsalaryincrement_tran " & _
                    '   "        WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                    '   "    ) AS Grds ON Grds.employeeunkid = hremployee_master.employeeunkid AND Grds.rno = 1 " & _
                    '   "    LEFT JOIN hrgrade_master AS CGM ON Grds.gradeunkid = CGM.gradeunkid " & _
                    '   "    LEFT JOIN hrgradelevel_master AS CGL ON Grds.gradelevelunkid = CGL.gradelevelunkid " & _
                    '   "    LEFT JOIN " & _
                    '   "    ( " & _
                    '   "        SELECT " & _
                    '   "             employeeunkid AS EmpId " & _
                    '   "            ,jobgroupunkid " & _
                    '   "            ,jobunkid " & _
                    '   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '   "        FROM hremployee_categorization_tran " & _
                    '   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                    '   "    ) AS ReCat ON ReCat.EmpId = hremployee_master.employeeunkid AND ReCat.rno = 1 " & _
                    '   "    LEFT JOIN hrjob_master AS CJB ON ReCat.jobunkid = CJB.jobunkid " & _
                    '   "    LEFT JOIN " & _
                    '   "    ( " & _
                    '   "        SELECT " & _
                    '   "             employeeunkid AS EmpId " & _
                    '   "            ,stationunkid " & _
                    '   "            ,departmentunkid " & _
                    '   "            ,sectionunkid " & _
                    '   "            ,classunkid " & _
                    '   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '   "        FROM hremployee_transfer_tran " & _
                    '   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtAsOnDate) & "' " & _
                    '   "    ) AS Alloc ON Alloc.EmpId = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    '   "    LEFT JOIN hrstation_master AS CST ON  CST.stationunkid = Alloc.stationunkid " & _
                    '   "    LEFT JOIN hrdepartment_master AS CDT ON Alloc.departmentunkid = CDT.departmentunkid " & _
                    '   "    LEFT JOIN hrsection_master AS CSC ON Alloc.sectionunkid = CSC.sectionunkid " & _
                    '   "    LEFT JOIN hrclasses_master AS CCM ON Alloc.classunkid = CCM.classesunkid " & _
                    '   "    LEFT JOIN gregrievance_master AS GM ON GM.fromemployeeunkid = hremployee_master.employeeunkid" & _
                    '   "    WHERE GM.grievancerefno IN ( " & strEmpId & " ) "

                    dsList = objdataOperation.ExecQuery(StrQ, strListName)

                    If objdataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objdataOperation.ErrorNumber & " : " & objdataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    'Gajanan [13-July-2019] -- End

                    'Hemant (07 Oct 2019) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 13 : On batch interview scheduling, system should send calendar invites to people selected as interviewers for that particular batch.  The date and time of the calendar should be picked from the interview date and time set)
                Case enImg_Email_RefId.Batch_Scheduling

                    StrQ = "SELECT " & _
                               "     ISNULL(rcbatchschedule_interviewer_tran.batchscheduleinterviewertranunkid,0) AS BatchScheduleInterviewerTranUnkId " & _
                               "     ,ISNULL(rcbatchschedule_interviewer_tran.vacancyunkid ,0) AS VacancyUnkId " & _
                               "     ,ISNULL(VAC.name,'') AS VacancyName " & _
                               "     ,ISNULL(CONVERT(CHAR(8), rcvacancy_master.openingdate, 112),'') AS VacancyOpeningDate " & _
                               "     ,ISNULL(CONVERT(CHAR(8), rcvacancy_master.closingdate, 112),'') AS VacancyClosingDate " & _
                               "     ,ISNULL(CONVERT(CHAR(8), rcvacancy_master.interview_startdate, 112),'') AS InterviewStartDate " & _
                               "     ,ISNULL(CONVERT(CHAR(8), rcvacancy_master.interview_closedate, 112),'') AS InterviewCloseDate " & _
                               "     ,ISNULL(rcbatchschedule_interviewer_tran.interviewtypeunkid,0) AS InterviewerInterviewtypeUnkId " & _
                               "     ,ISNULL(InterviewerIntvType.name, '') AS InterviewerIntviewType " & _
                               "     ,ISNULL(rcbatchschedule_interviewer_tran.interviewer_level, '') AS InterviewerLevel " & _
                               "     ,ISNULL(rcbatchschedule_interviewer_tran.interviewerunkid, 0) AS InterviewerUnkId " & _
                               "     ,ISNULL(EmpTitle.name , '') AS Title " & _
                               "     ,ISNULL(hremployee_master.employeecode , '') AS InterviewerEmpCode " & _
                               "     ,ISNULL(hremployee_master.firstname , '') AS InterviewerFirstname " & _
                               "     ,ISNULL(hremployee_master.surname , '') AS InterviewerSurname " & _
                               "     ,ISNULL(rcbatchschedule_interviewer_tran.otherinterviewer_name,'') AS OtherInterviewerName " & _
                               "     ,ISNULL(rcbatchschedule_interviewer_tran.othercompany, '') AS OtherCompany " & _
                               "     ,ISNULL(rcbatchschedule_interviewer_tran.otherdepartment, '') AS OtherDepartment " & _
                               "     ,ISNULL(rcbatchschedule_interviewer_tran.othercontact_no, '') AS OtherContactNo " & _
                               "     ,ISNULL(rcbatchschedule_master.batchcode, '') AS BatchCode " & _
                               "     ,ISNULL(rcbatchschedule_master.batchname , '') AS BatchName " & _
                               "     ,ISNULL(CONVERT(CHAR(8), rcbatchschedule_master.interviewdate, 112),'') AS InterviewDate " & _
                               "     ,ISNULL(CONVERT(NVARCHAR(5),rcbatchschedule_master.interviewtime,108), '')  AS InterviewTime " & _
                               "     ,ISNULL(rcbatchschedule_master.interviewtypeunkid, 0) AS VacancyInterviewTypeUnkId " & _
                               "     ,ISNULL(VacIntvType.name, '') AS VacancyInterviewType " & _
                               "     ,ISNULL(rcbatchschedule_master.resultgroupunkid, 0) AS ResultGroupUnkId " & _
                               "     ,ISNULL(ResultGrp.name, '') AS ResultGroup " & _
                               "     ,ISNULL(rcbatchschedule_master.location, '') AS Location " & _
                               "     ,ISNULL(rcbatchschedule_master.description, '') AS Description " & _
                               "     ,ISNULL(rcbatchschedule_master.cancel_remark, '') AS CancelRemark " & _
                                strCommonData & " " & _
                            " FROM rcbatchschedule_interviewer_tran " & _
                            "     LEFT JOIN rcbatchschedule_master ON rcbatchschedule_master.batchscheduleunkid = rcbatchschedule_interviewer_tran.batchscheduleunkid " & _
                            "     LEFT JOIN rcvacancy_master on rcvacancy_master.vacancyunkid = rcbatchschedule_interviewer_tran.vacancyunkid AND rcvacancy_master.isvoid = 0 " & _
                            "     LEFT JOIN cfcommon_master VAC on VAC.masterunkid = rcvacancy_master.vacancytitle AND VAC.mastertype = '" & clsCommon_Master.enCommonMaster.VACANCY_MASTER & "' " & _
                            "     LEFT JOIN hremployee_master on hremployee_master.employeeunkid = rcbatchschedule_interviewer_tran.interviewerunkid" & _
                            "     LEFT JOIN cfcommon_master EmpTitle on EmpTitle.masterunkid = hremployee_master.titleunkid AND EmpTitle.mastertype = '" & clsCommon_Master.enCommonMaster.TITLE & "'  " & _
                            "     LEFT JOIN cfcommon_master VacIntvType on VacIntvType.masterunkid = rcbatchschedule_master.interviewtypeunkid and VacIntvType.mastertype = '" & clsCommon_Master.enCommonMaster.INTERVIEW_TYPE & "'" & _
                            "     LEFT JOIN cfcommon_master InterviewerIntvType on InterviewerIntvType.masterunkid = rcbatchschedule_interviewer_tran.interviewtypeunkid and InterviewerIntvType.mastertype = '" & clsCommon_Master.enCommonMaster.INTERVIEW_TYPE & "'" & _
                            "     LEFT JOIN cfcommon_master ResultGrp on ResultGrp.masterunkid = rcbatchschedule_master.resultgroupunkid and ResultGrp.mastertype = '" & clsCommon_Master.enCommonMaster.RESULT_GROUP & "'" & _
                            "WHERE rcbatchschedule_interviewer_tran.isvoid = 0 " & _
                            "AND rcbatchschedule_master.isvoid = 0 " & _
                            "AND rcbatchschedule_interviewer_tran.batchscheduleunkid = " & mintBatchscheduleUnkid & " "


                    objdataOperation.ClearParameters()
                    dsList = objdataOperation.ExecQuery(StrQ, strListName)

                    If objdataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objdataOperation.ErrorNumber & " : " & objdataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    'Hemant (07 Oct 2019) -- End

                    'Sohail (17 Feb 2021) -- Start
                    'NMB Enhancement : OLD-322 : Addition of Loan Parameters Field on Letter Templates.
                Case enImg_Email_RefId.Loan_Balance
                    If mdtTable IsNot Nothing Then
                        dsList = New DataSet
                        dsList.Tables.Add(mdtTable.Copy)

                        If dsList.Tables(0).Columns.Contains("employeeunkid") = True Then
                            dsList.Tables(0).Columns("employeeunkid").ColumnName = "EmpId"
                        End If

                        Dim arr() As String = strCommonData.Split(",")
                        Dim strValue As String = ""
                        For Each strFld In arr
                            Dim arrFld() As String = Microsoft.VisualBasic.Split(strFld, " As ", , CompareMethod.Text)
                            If arrFld.Length = 2 Then
                                With dsList.Tables(0)
                                    If .Columns.Contains(arrFld(1).Trim) = False Then
                                        strValue += arrFld(0).Trim.Replace("'", "")
                                        Dim dtCol As New DataColumn(arrFld(1).Trim, System.Type.GetType("System.String"))
                                        dtCol.AllowDBNull = False
                                        dtCol.DefaultValue = strValue
                                        .Columns.Add(dtCol)
                                    End If
                                End With
                                strValue = ""
                            Else
                                If strValue.Trim <> "" AndAlso arrFld(0).Trim.Replace("'", "") <> "" Then
                                    strValue += ", "
                                End If
                                strValue += arrFld(0).Trim.Replace("'", "")
                            End If
                        Next
                    End If
                    'Sohail (17 Feb 2021) -- End

            End Select

            If dsList.Tables(0).Rows.Count > 0 Then
                Select Case intModuleRefId
                    Case enImg_Email_RefId.Employee_Module, enImg_Email_RefId.Medical_Module
                        For Each dtRow As DataRow In dsList.Tables(0).Rows
                            If dtRow.Item("BirthDate").ToString.Trim.Length > 0 Then
                                dtRow.Item("BirthDate") = eZeeDate.convertDate(dtRow.Item("BirthDate").ToString).ToShortDateString
                            End If
                            If dtRow.Item("AnniversaryDate").ToString.Trim.Length > 0 Then
                                dtRow.Item("AnniversaryDate") = eZeeDate.convertDate(dtRow.Item("AnniversaryDate").ToString).ToShortDateString
                            End If
                            If dtRow.Item("SuspendedFrom").ToString.Trim.Length > 0 Then
                                dtRow.Item("SuspendedFrom") = eZeeDate.convertDate(dtRow.Item("SuspendedFrom").ToString).ToShortDateString
                            End If
                            If dtRow.Item("SuspendedTo").ToString.Trim.Length > 0 Then
                                dtRow.Item("SuspendedTo") = eZeeDate.convertDate(dtRow.Item("SuspendedTo").ToString).ToShortDateString
                            End If
                            If dtRow.Item("ProbationFrom").ToString.Trim.Length > 0 Then
                                dtRow.Item("ProbationFrom") = eZeeDate.convertDate(dtRow.Item("ProbationFrom").ToString).ToShortDateString
                            End If
                            If dtRow.Item("ProbationTo").ToString.Trim.Length > 0 Then
                                dtRow.Item("ProbationTo") = eZeeDate.convertDate(dtRow.Item("ProbationTo").ToString).ToShortDateString
                            End If
                            If dtRow.Item("TerminationDate").ToString.Trim.Length > 0 Then
                                dtRow.Item("TerminationDate") = eZeeDate.convertDate(dtRow.Item("TerminationDate").ToString).ToShortDateString
                            End If
                            If dtRow.Item("RetirementDate").ToString.Trim.Length > 0 Then
                                dtRow.Item("RetirementDate") = eZeeDate.convertDate(dtRow.Item("RetirementDate").ToString).ToShortDateString
                            End If
                            If dtRow.Item("EmploymentLeavingDate").ToString.Trim.Length > 0 Then
                                dtRow.Item("EmploymentLeavingDate") = eZeeDate.convertDate(dtRow.Item("EmploymentLeavingDate").ToString).ToShortDateString
                            End If
                            If dtRow.Item("ApponintmentDate").ToString.Trim.Length > 0 Then
                                dtRow.Item("ApponintmentDate") = eZeeDate.convertDate(dtRow.Item("ApponintmentDate").ToString).ToShortDateString
                            End If
                            If dtRow.Item("ConfirmationDate").ToString.Trim.Length > 0 Then
                                dtRow.Item("ConfirmationDate") = eZeeDate.convertDate(dtRow.Item("ConfirmationDate").ToString).ToShortDateString
                            End If

						    'Gajanan [28-AUG-2019] -- Start      
						    'ENHANCEMENT : Additional Data Fields(EOC Effective Date and latest Reinstatement dates) required in Letter Templates  are required.
                            If dtRow.Item("EOC Effective Date").ToString.Trim.Length > 0 Then
                                dtRow.Item("EOC Effective Date") = eZeeDate.convertDate(dtRow.Item("EOC Effective Date").ToString).ToShortDateString
                            End If

                            If dtRow.Item("Reinstatement Date").ToString.Trim.Length > 0 Then
                                dtRow.Item("Reinstatement Date") = eZeeDate.convertDate(dtRow.Item("Reinstatement Date").ToString).ToShortDateString
                            End If
    						'Gajanan [28-AUG-2019] -- End      
                        Next
                    Case enImg_Email_RefId.Applicant_Module
                        For Each dtRow As DataRow In dsList.Tables(0).Rows
                            If dtRow.Item("BirthDate").ToString.Trim.Length > 0 Then
                                dtRow.Item("BirthDate") = eZeeDate.convertDate(dtRow.Item("BirthDate").ToString).ToShortDateString
                            End If
                            If dtRow.Item("AnniversaryDate").ToString.Trim.Length > 0 Then
                                dtRow.Item("AnniversaryDate") = eZeeDate.convertDate(dtRow.Item("AnniversaryDate").ToString).ToShortDateString
                            End If
                            If dtRow.Item("Appreferenceno").ToString.Trim.Length > 0 Then
                                dtRow.Item("Appreferenceno") = clsCrypto.Dicrypt(dtRow.Item("Appreferenceno").ToString)
                            Else
                                dtRow.Item("Appreferenceno") = ""
                            End If

                            'S.SANDEEP [ 02 MAY 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            If dtRow.Item("Vacancy_With_Dates").ToString.Trim.Length > 0 Then
                                If dtRow.Item("ODate").ToString.Trim.Length > 0 AndAlso dtRow.Item("CDate").ToString.Trim.Length > 0 Then
                                    dtRow.Item("Vacancy_With_Dates") = dtRow.Item("Vacancy_With_Dates").ToString.Trim & " (" & eZeeDate.convertDate(dtRow.Item("ODate").ToString).ToShortDateString & " - " & _
                                                            eZeeDate.convertDate(dtRow.Item("CDate").ToString).ToShortDateString & ")"
                                    dtRow.Item("Vacancy_Without_Dates") = Trim(dtRow.Item("Vacancy_Without_Dates"))
                                End If
                            End If
                            'S.SANDEEP [ 02 MAY 2012 ] -- END

                            'Hemant (07 Oct 2019) -- Start
                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 17 : On letter template, applicant fields, provide parameters for interview date,  interview time, interview venue. These should pick items as set on Batch Scheduling screen.)
                            If dtRow.Item("interviewdate").ToString.Trim.Length > 0 Then
                                dtRow.Item("interviewdate") = eZeeDate.convertDate(dtRow.Item("interviewdate").ToString).ToShortDateString
                            End If
                            'Hemant (07 Oct 2019) -- End
                            'Hemant (07 Oct 2019) -- Start
                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 18 : On letter template, applicant fields, provide parameter to pick employee reporting-to. (For internal vacancies))
                            If CBool(dtRow.Item("isexternalvacancy")) = False Then
                                Dim intEmployeeUnkId As Integer
                                Dim objEmp As New clsEmployee_Master
                                Dim objReportingTo As New clsReportingToEmployee
                                intEmployeeUnkId = objEmp.GetEmployeeUnkidFromEmpCode(dtRow.Item("EmployeeCode").ToString)
                                objReportingTo._EmployeeUnkid(ConfigParameter._Object._CurrentDateAndTime) = intEmployeeUnkId
                                Dim dt As DataTable = objReportingTo._RDataTable
                                Dim DefaultReportList As List(Of DataRow) = (From p In dt.AsEnumerable() Where (CBool(p.Item("ishierarchy")) = True) Select (p)).ToList
                                If DefaultReportList.Count > 0 Then
                                    dtRow.Item("ReportingTo") = DefaultReportList(0).Item("ename").ToString
                                End If
                            End If
                            'Hemant (07 Oct 2019) -- End
                        Next
                    Case enImg_Email_RefId.Leave_Module

                        For Each dtRow As DataRow In dsList.Tables(0).Rows
                            If dtRow.Item("Applydate").ToString.Trim.Length > 0 Then
                                dtRow.Item("Applydate") = eZeeDate.convertDate(dtRow.Item("Applydate").ToString).ToShortDateString
                            End If
                            If dtRow.Item("Startdate").ToString.Trim.Length > 0 Then
                                dtRow.Item("Startdate") = eZeeDate.convertDate(dtRow.Item("Startdate").ToString).ToShortDateString
                            End If
                            If dtRow.Item("Enddate").ToString.Trim.Length > 0 Then
                                dtRow.Item("Enddate") = eZeeDate.convertDate(dtRow.Item("Enddate").ToString).ToShortDateString
                            End If


                            'Pinkal (3-Aug-2013) -- Start
                            'Enhancement : TRA Changes

                            If dtRow.Item("Approved Start Date").ToString.Trim.Length > 0 Then
                                dtRow.Item("Approved Start Date") = eZeeDate.convertDate(dtRow.Item("Approved Start Date").ToString).ToShortDateString
                            End If

                            If dtRow.Item("Approved End Date").ToString.Trim.Length > 0 Then
                                dtRow.Item("Approved End Date") = eZeeDate.convertDate(dtRow.Item("Approved End Date").ToString).ToShortDateString
                            End If

                            If dtRow.Item("Accrue Start Date").ToString.Trim.Length > 0 Then
                                dtRow.Item("Accrue Start Date") = eZeeDate.convertDate(dtRow.Item("Accrue Start Date").ToString).ToShortDateString
                            End If

                            If dtRow.Item("Accrue End Date").ToString.Trim.Length > 0 Then
                                dtRow.Item("Accrue End Date") = eZeeDate.convertDate(dtRow.Item("Accrue End Date").ToString).ToShortDateString
                            End If

                            dtRow.Item("Approved Days") = CDec(dtRow.Item("Approved Days")).ToString("#0.00")

                            'Pinkal (3-Aug-2013) -- End

                        Next

                    Case enImg_Email_RefId.Payroll_Module
                    Case enImg_Email_RefId.Dependants_Beneficiaries
                    Case enImg_Email_RefId.Discipline_Module 'S.SANDEEP [ 20 APRIL 2012 ]
                        For Each dtRow As DataRow In dsList.Tables(0).Rows

                            'S.SANDEEP |08-JAN-2019| -- START
                            'If dtRow.Item("TransactionDate").ToString.Trim.Length > 0 Then
                            '    dtRow.Item("TransactionDate") = eZeeDate.convertDate(dtRow.Item("TransactionDate").ToString).ToShortDateString
                            'End If

                            'If dtRow.Item("ActionFromDate").ToString.Trim.Length > 0 Then
                            '    dtRow.Item("ActionFromDate") = eZeeDate.convertDate(dtRow.Item("ActionFromDate").ToString).ToShortDateString
                            'End If

                            'If dtRow.Item("ActionToDate").ToString.Trim.Length > 0 Then
                            '    dtRow.Item("ActionToDate") = eZeeDate.convertDate(dtRow.Item("ActionToDate").ToString).ToShortDateString
                            'End If

                            ''S.SANDEEP [ 01 JUNE 2012 ] -- START
                            ''ENHANCEMENT : TRA DISCIPLINE CHANGES
                            'If dtRow.Item("InterdictDate").ToString.Trim.Length > 0 Then
                            '    dtRow.Item("InterdictDate") = eZeeDate.convertDate(dtRow.Item("InterdictDate").ToString).ToShortDateString
                            'End If
                            ''S.SANDEEP [ 01 JUNE 2012 ] -- END


                            If dtRow.Item("ChargeDate").ToString.Trim.Length > 0 Then
                                dtRow.Item("ChargeDate") = eZeeDate.convertDate(dtRow.Item("ChargeDate").ToString).ToShortDateString
                            End If

                            If dtRow.Item("InterdictionDate").ToString.Trim.Length > 0 Then
                                dtRow.Item("InterdictionDate") = eZeeDate.convertDate(dtRow.Item("InterdictionDate").ToString).ToShortDateString
                            End If

                            If dtRow.Item("NotificationDate").ToString.Trim.Length > 0 Then
                                dtRow.Item("NotificationDate") = eZeeDate.convertDate(dtRow.Item("NotificationDate").ToString).ToShortDateString
                            End If
                            'S.SANDEEP |08-JAN-2019| -- END


                        Next

                        'Hemant (12 Nov 2019) -- Start
                    Case enImg_Email_RefId.Batch_Scheduling
                        For Each dtRow As DataRow In dsList.Tables(0).Rows
                            If dtRow.Item("VacancyOpeningDate").ToString.Trim.Length > 0 Then
                                dtRow.Item("VacancyOpeningDate") = eZeeDate.convertDate(dtRow.Item("VacancyOpeningDate").ToString).ToShortDateString
                            End If

                            If dtRow.Item("VacancyClosingDate").ToString.Trim.Length > 0 Then
                                dtRow.Item("VacancyClosingDate") = eZeeDate.convertDate(dtRow.Item("VacancyClosingDate").ToString).ToShortDateString
                            End If

                            If dtRow.Item("InterviewStartDate").ToString.Trim.Length > 0 Then
                                dtRow.Item("InterviewStartDate") = eZeeDate.convertDate(dtRow.Item("InterviewStartDate").ToString).ToShortDateString
                            End If

                            If dtRow.Item("InterviewCloseDate").ToString.Trim.Length > 0 Then
                                dtRow.Item("InterviewCloseDate") = eZeeDate.convertDate(dtRow.Item("InterviewCloseDate").ToString).ToShortDateString
                            End If

                            If dtRow.Item("InterviewDate").ToString.Trim.Length > 0 Then
                                dtRow.Item("InterviewDate") = eZeeDate.convertDate(dtRow.Item("InterviewDate").ToString).ToShortDateString
                            End If
                        Next
                        'Hemant (12 Nov 2019) -- End

                End Select
            End If


            'S.SANDEEP [05-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#216|#ARUTI-102}
            If dsList.Tables(0).Columns.Contains("CompanyLogo") = True Then
                dsList.Tables(0).Columns.Remove("CompanyLogo")
            End If
            Dim dCol As New DataColumn
            With dCol
                .DataType = GetType(Byte())
                .ColumnName = "CompanyLogo"
                .DefaultValue = mCompany_Logo
            End With
            dsList.Tables(0).Columns.Add(dCol)
            'S.SANDEEP [05-Apr-2018] -- END


            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeData; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 73, "Male")
            Language.setMessage("clsMasterData", 74, "Female")
            Language.setMessage("clsMasterData", 110, "Approved")
            Language.setMessage("clsMasterData", 111, "Pending")
            Language.setMessage("clsMasterData", 112, "Rejected")
            Language.setMessage("clsMasterData", 113, "Re-Scheduled")
            Language.setMessage("clsMasterData", 115, "Cancelled")
            Language.setMessage("clsMasterData", 277, "Issued")
            Language.setMessage(mstrModuleName, 1, "Permanent")
            Language.setMessage(mstrModuleName, 2, "Year(s),")
            Language.setMessage(mstrModuleName, 3, "Month(s),")
            Language.setMessage(mstrModuleName, 4, "Day(s)")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Public Class clsLetterFields1
'    Private Shared Readonly mstrModuleName As String = "clsLetterFields"
'    Private objDataOperation As clsDataOperation

'    Public Function GetList(ByVal intModuleRefId As Integer, Optional ByVal strListName As String = "List") As DataSet
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Dim dsList As New DataSet
'        objDataOperation = New clsDataOperation
'        Try
'            Select Case intModuleRefId
'                Case 1  'Employee
'                    'S.SANDEEP [ 23 JAN 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    'StrQ = "SELECT " & _
'                    '          " '' AS Shift " & _
'                    '          ",'' AS Title " & _
'                    '          ",'' AS EmpCode " & _
'                    '          ",'' AS FirstName " & _
'                    '          ",'' AS SurName " & _
'                    '          ",'' AS OtherName " & _
'                    '          ",'' AS Email " & _
'                    '          ",'' AS BirthDate " & _
'                    '          ",'' AS AnniversaryDate " & _
'                    '          ",'' AS EmpAddress1 " & _
'                    '          ",'' AS EmpAddress2 " & _
'                    '          ",'' AS City " & _
'                    '          ",'' AS State " & _
'                    '          ",'' AS Country " & _
'                    '          ",'' AS Station " & _
'                    '          ",'' AS Department " & _
'                    '          ",'' AS Section " & _
'                    '          ",'' AS Job " & _
'                    '          ",'' AS Grade " & _
'                    '          ",'' AS GradeLevel " & _
'                    '          ",'' AS Class " & _
'                    '          ",'' AS CostCenter " & _
'                    '          ",'' AS SuspendedFrom " & _
'                    '          ",'' AS SuspendedTo " & _
'                    '          ",'' AS ProbationFrom " & _
'                    '          ",'' AS ProbationTo " & _
'                    '          ",'' AS TerminationFrom " & _
'                    '          ",'' AS TerminationTo "
'                    StrQ = "SELECT " & _
'                             " '' AS Shift " & _
'                             ",'' AS Title " & _
'                             ",'' AS EmpCode " & _
'                             ",'' AS FirstName " & _
'                             ",'' AS SurName " & _
'                             ",'' AS OtherName " & _
'                             ",'' AS Email " & _
'                             ",'' AS BirthDate " & _
'                             ",'' AS AnniversaryDate " & _
'                             ",'' AS EmpAddress1 " & _
'                             ",'' AS EmpAddress2 " & _
'                             ",'' AS City " & _
'                             ",'' AS State " & _
'                             ",'' AS Country " & _
'                             ",'' AS SuspendedFrom " & _
'                             ",'' AS SuspendedTo " & _
'                             ",'' AS ProbationFrom " & _
'                             ",'' AS ProbationTo " & _
'                             ",'' AS TerminationDate " & _
'                             ",'' AS RetirementDate " & _
'                             ",'' AS ConfirmationDate " & _
'                             ",'' AS EmploymentLeavingDate " & _
'                             ",'' AS ApponintmentDate " & _
'                             ",'' AS Station " & _
'                             ",'' AS DepartmentGroup " & _
'                             ",'' AS Department " & _
'                             ",'' AS SectionGroup " & _
'                             ",'' AS Section " & _
'                             ",'' AS UnitGroup " & _
'                             ",'' AS Unit " & _
'                             ",'' AS Team " & _
'                             ",'' AS JobGroup " & _
'                             ",'' AS Job " & _
'                             ",'' AS ClassGroup " & _
'                             ",'' AS Class " & _
'                             ",'' AS CostCenter " & _
'                             ",'' AS ReportToJobGroup " & _
'                             ",'' AS ReportToJob " & _
'                             ",'' AS OldBranch " & _
'                             ",'' AS OldDepartmentGroup " & _
'                             ",'' AS OldDepartment " & _
'                             ",'' AS OldSectionGroup " & _
'                             ",'' AS OldSection " & _
'                             ",'' AS OldUnitGroup " & _
'                             ",'' AS OldUnit " & _
'                             ",'' AS OldTeam " & _
'                             ",'' AS OldJobGroup " & _
'                             ",'' AS OldJob " & _
'                             ",'' AS OldClassGroup " & _
'                             ",'' AS OldClass " & _
'                             ",'' AS OldCostCenter " & _
'                             ",'' AS OldReportToJobGroup " & _
'                             ",'' AS OldReportToJob " & _
'                             ",'' AS OldGradeGroup " & _
'                             ",'' AS OldGrade " & _
'                             ",'' AS OldGradeLevel " & _
'                             ",'' AS OldSalary " & _
'                             ",'' AS CurrentGradeGroup " & _
'                             ",'' AS CurrentGrade " & _
'                             ",'' AS CurrentGradeLevel " & _
'                             ",'' AS CurrentSalary " & _
'                             ",'' AS NextGradeGroup " & _
'                             ",'' AS NextGrade " & _
'                             ",'' AS NextGradeLevel " & _
'                             ",'' AS NextSalary "
'                    'S.SANDEEP [ 23 JAN 2012 ] -- END



'                Case 2  'Applicant
'                    StrQ = "SELECT " & _
'                                " '' AS Title " & _
'                                ",'' AS Firstname " & _
'                                ",'' AS Surname " & _
'                                ",'' AS Othername " & _
'                                ",'' AS Gender " & _
'                                ",'' AS Email " & _
'                                ",'' AS Applicant_Address1 " & _
'                                ",'' AS Applicant_Address2 " & _
'                                ",'' AS Applicant_City " & _
'                                ",'' AS Applicant_State " & _
'                                ",'' AS Applicant_Country " & _
'                                ",'' AS Mobileno " & _
'                                ",'' AS BirthDate " & _
'                                ",'' AS AnniversaryDate " & _
'                                ",'' AS Vacancy "
'                Case 3  'Leave
'                    StrQ = ""
'                Case 4  'Payroll
'                    StrQ = ""
'            End Select
'            'Sandeep [ 02 Oct 2010 ] -- Start
'            'StrQ &= ",'' As Today " & _
'            '                        ",'' As CompanyName " & _
'            '                        ",'' As Address1 " & _
'            '                        ",'' As Address2 " & _
'            '                        ",'' As City " & _
'            '                        ",'' As State " & _
'            '                        ",'' As Country " & _
'            '                        ",'' As Phone1 " & _
'            '                        ",'' As Phone2 " & _
'            '                        ",'' As Phone3 " & _
'            '                        ",'' As Fax " & _
'            '                        ",'' As Email " & _
'            '                        ",'' As Website " & _
'            '                        ",'' As RegNo1 " & _
'            '                        ",'' As RegNo2 " & _
'            '                        ",'' As TinNo " & _
'            '                        ",'' As Reference "

'            'dsList = objDataOperation.ExecQuery(StrQ, strListName)

'            'If objDataOperation.ErrorMessage <> "" Then
'            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'            '    Throw exForce
'            'End If
'            If StrQ.Length > 0 Then

'                'Sandeep [ 07 APRIL 2011 ] -- Start
'                'StrQ &= ",'' As Today " & _
'                '        ",'' As CompanyName " & _
'                '        ",'' As Address1 " & _
'                '        ",'' As Address2 " & _
'                '        ",'' As City " & _
'                '        ",'' As State " & _
'                '        ",'' As Country " & _
'                '        ",'' As Phone1 " & _
'                '        ",'' As Phone2 " & _
'                '        ",'' As Phone3 " & _
'                '        ",'' As Fax " & _
'                '        ",'' As Email " & _
'                '        ",'' As Website " & _
'                '        ",'' As RegNo1 " & _
'                '        ",'' As RegNo2 " & _
'                '        ",'' As TinNo " & _
'                '        ",'' As Reference "
'                StrQ &= ",'' As Today " & _
'                        ",'' As CompanyName " & _
'                        ",'' As Address1 " & _
'                        ",'' As Address2 " & _
'                        ",'' As C_City " & _
'                        ",'' As C_State " & _
'                        ",'' As C_Country " & _
'                        ",'' As Phone1 " & _
'                        ",'' As Phone2 " & _
'                        ",'' As Phone3 " & _
'                        ",'' As Fax " & _
'                        ",'' As Email " & _
'                        ",'' As Website " & _
'                        ",'' As RegNo1 " & _
'                        ",'' As RegNo2 " & _
'                        ",'' As TinNo " & _
'                        ",'' As Reference "
'                'Sandeep [ 07 APRIL 2011 ] -- End 

'                dsList = objDataOperation.ExecQuery(StrQ, strListName)

'                If objDataOperation.ErrorMessage <> "" Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If
'            End If
'            'Sandeep [ 02 Oct 2010 ] -- End 

'            Return dsList

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetList", mstrModuleName)
'            Return Nothing
'        Finally
'            If dsList IsNot Nothing Then dsList.Dispose()
'            dsList = Nothing
'            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
'            objDataOperation = Nothing
'            exForce = Nothing
'        End Try
'    End Function

'    Public Function GetEmployeeData(ByVal strEmpId As String, ByVal intModuleRefId As Integer, Optional ByVal strListName As String = "List") As DataSet
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Dim dsList As New DataSet

'        Dim objCity As New clscity_master
'        Dim objState As New clsstate_master
'        Dim objCountry As New clsMasterData
'        Dim dsCountry As New DataSet

'        objCity._Cityunkid = Company._Object._Cityunkid
'        objState._Stateunkid = Company._Object._Stateunkid
'        dsCountry = objCountry.getCountryList("Country", False, Company._Object._Countryunkid)
'        Dim mstrCountry As String = String.Empty
'        If dsCountry.Tables(0).Rows.Count > 0 Then
'            mstrCountry = dsCountry.Tables(0).Rows(0)("country_name")
'        Else
'            mstrCountry = ""
'        End If
'        dsCountry = Nothing
'        objCountry = Nothing

'        objDataOperation = New clsDataOperation
'        Try
'            Select Case intModuleRefId
'                Case 1     'Employee
'                    'S.SANDEEP [ 23 JAN 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES  
'                    'StrQ = "SELECT " & _
'                    '   "	 ISNULL(tnashift_master.shiftname,'') AS Shift " & _
'                    '   "	,ISNULL(cfcommon_master.name,'') AS Title " & _
'                    '   "	,ISNULL(hremployee_master.employeecode,'') AS EmpCode " & _
'                    '   "	,ISNULL(hremployee_master.firstname,'') AS FirstName " & _
'                    '   "	,ISNULL(hremployee_master.surname,'') AS SurName " & _
'                    '   "	,ISNULL(hremployee_master.othername,'') AS OtherName " & _
'                    '   "	,ISNULL(hremployee_master.email,'') AS Email " & _
'                    '   "	,ISNULL(CONVERT(CHAR(8),hremployee_master.birthdate,112),'') AS BirthDate " & _
'                    '   "	,ISNULL(CONVERT(CHAR(8),hremployee_master.anniversary_date,112),'') AS AnniversaryDate " & _
'                    '   "	,ISNULL(hremployee_master.present_address1,'') AS EmpAddress1 " & _
'                    '   "	,ISNULL(hremployee_master.present_address2,'') AS EmpAddress2 " & _
'                    '   "	,ISNULL(hrmsConfiguration..cfcity_master .name,'') AS City " & _
'                    '   "	,ISNULL(hrmsConfiguration..cfstate_master .name,'') AS State " & _
'                    '   "	,ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
'                    '   "	,ISNULL(hrstation_master.name,'') AS Station " & _
'                    '   "	,ISNULL(hrdepartment_master.name,'') AS Department " & _
'                    '   "	,ISNULL(hrsection_master.name,'') AS Section " & _
'                    '   "	,ISNULL(hrjob_master.job_name,'') AS Job " & _
'                    '   "	,ISNULL(hrgrade_master.name,'') AS Grade " & _
'                    '   "	,ISNULL(hrgradelevel_master.name,'') AS GradeLevel " & _
'                    '   "	,ISNULL(hrclasses_master.name,'') AS Class " & _
'                    '   "	,ISNULL(prcostcenter_master.costcentername,'') AS CostCenter " & _
'                    '   "	,ISNULL(CONVERT(CHAR(8),hremployee_master.suspended_from_date,112),'') AS SuspendedFrom " & _
'                    '   "	,ISNULL(CONVERT(CHAR(8),hremployee_master.suspended_to_date,112),'') AS SuspendedTo " & _
'                    '   "	,ISNULL(CONVERT(CHAR(8),hremployee_master.probation_from_date,112),'') AS ProbationFrom " & _
'                    '   "	,ISNULL(CONVERT(CHAR(8),hremployee_master.probation_to_date,112),'') AS ProbationTo " & _
'                    '   "	,ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),'') AS TerminationFrom " & _
'                    '   "	,ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),'') AS TerminationTo " & _
'                    '   "	,'" & Now.Date & "' As Today " & _
'                    '   "	,'" & Company._Object._Name & "' As CompanyName " & _
'                    '   "	,'" & Company._Object._Address1 & "' As Address1 " & _
'                    '   "	,'" & Company._Object._Address2 & "' As Address2 " & _
'                    '   "	,'" & objCity._Name & "' As C_City " & _
'                    '   "	,'" & objState._Name & "' As C_State " & _
'                    '   "	,'" & mstrCountry & "' As C_Country " & _
'                    '   "	,'" & Company._Object._Phone1 & "' As Phone1 " & _
'                    '   "	,'" & Company._Object._Phone2 & "' As Phone2 " & _
'                    '   "	,'" & Company._Object._Phone3 & "' As Phone3 " & _
'                    '   "	,'" & Company._Object._Fax & "' As Fax " & _
'                    '   "	,'" & Company._Object._Email & "' As Email " & _
'                    '   "	,'" & Company._Object._Website & "' As Website " & _
'                    '   "	,'" & Company._Object._Registerdno & "' As RegNo1 " & _
'                    '   "	,'" & Company._Object._Vatno & "' As RegNo2 " & _
'                    '   "	,'" & Company._Object._Tinno & "' As TinNo " & _
'                    '   "	,'" & Company._Object._Reference & "' As Reference " & _
'                    '   "	,hremployee_master.employeeunkid AS EmpId " & _
'                    '   "FROM hremployee_master " & _
'                    '   "	LEFT JOIN prcostcenter_master ON hremployee_master.costcenterunkid = prcostcenter_master.costcenterunkid " & _
'                    '   "	LEFT JOIN hrclasses_master ON hremployee_master.classunkid = hrclasses_master.classesunkid " & _
'                    '   "	LEFT JOIN hrgradelevel_master ON hremployee_master.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
'                    '   "	LEFT JOIN hrgrade_master ON hremployee_master.gradeunkid = hrgrade_master.gradeunkid " & _
'                    '   "	LEFT JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                    '   "	LEFT JOIN hrsection_master ON hremployee_master.sectionunkid = hrsection_master.sectionunkid " & _
'                    '   "	LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                    '   "	LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid " & _
'                    '   "	LEFT JOIN hrmsConfiguration..cfstate_master  ON hremployee_master.present_stateunkid= hrmsConfiguration..cfstate_master .stateunkid " & _
'                    '   "	LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.present_countryunkid " & _
'                    '   "	LEFT JOIN hrmsConfiguration..cfzipcode_master  ON hremployee_master.present_postcodeunkid = hrmsConfiguration..cfzipcode_master .zipcodeunkid " & _
'                    '   "	LEFT JOIN hrmsConfiguration..cfcity_master  ON hremployee_master.present_post_townunkid = hrmsConfiguration..cfcity_master .cityunkid " & _
'                    '   "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.titleunkid AND mastertype = " & enCommonMaster.TITLE & _
'                    '   "	LEFT JOIN tnashift_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
'                    '   "WHERE ISNULL(hremployee_master.isactive,0) = 1 " & _
'                    '   "	AND hremployee_master.employeeunkid IN ( " & strEmpId & " ) "
'                    If strEmpId.Trim.Length > 0 Then
'                        Dim dTable As DataTable = Nothing
'                        For Each StrId As String In strEmpId.Split(",")
'                            StrQ = "SELECT " & _
'                                         " Shift " & _
'                                         ",Title " & _
'                                         ",EmpCode " & _
'                                         ",FirstName " & _
'                                         ",SurName " & _
'                                         ",OtherName " & _
'                                         ",Email " & _
'                                         ",BirthDate " & _
'                                         ",AnniversaryDate " & _
'                                         ",EmpAddress1 " & _
'                                         ",EmpAddress2 " & _
'                                         ",City " & _
'                                         ",State " & _
'                                         ",Country " & _
'                                         ",SuspendedFrom " & _
'                                         ",SuspendedTo " & _
'                                         ",ProbationFrom " & _
'                                         ",ProbationTo " & _
'                                         ",TerminationDate " & _
'                                         ",RetirementDate " & _
'                                         ",ConfirmationDate " & _
'                                         ",EmploymentLeavingDate " & _
'                                         ",ApponintmentDate " & _
'                                         ",Station " & _
'                                         ",DepartmentGroup " & _
'                                         ",Department " & _
'                                         ",SectionGroup " & _
'                                         ",Section " & _
'                                         ",UnitGroup " & _
'                                         ",Unit " & _
'                                         ",Team " & _
'                                         ",JobGroup " & _
'                                         ",Job " & _
'                                         ",ClassGroup " & _
'                                         ",Class " & _
'                                         ",CostCenter " & _
'                                         ",ReportToJobGroup " & _
'                                         ",ReportToJob " & _
'                                         ",OldBranch " & _
'                                         ",OldDepartmentGroup " & _
'                                         ",OldDepartment " & _
'                                         ",OldSectionGroup " & _
'                                         ",OldSection " & _
'                                         ",OldUnitGroup " & _
'                                         ",OldUnit " & _
'                                         ",OldTeam " & _
'                                         ",OldJobGroup " & _
'                                         ",OldJob " & _
'                                         ",OldClassGroup " & _
'                                         ",OldClass " & _
'                                         ",OldCostCenter " & _
'                                         ",OldReportToJobGroup " & _
'                                         ",OldReportToJob " & _
'                                         ",OldGradeGroup " & _
'                                         ",OldGrade " & _
'                                         ",OldGradeLevel " & _
'                                         ",OldSalary " & _
'                                         ",CurrentGradeGroup " & _
'                                         ",CurrentGrade " & _
'                                         ",CurrentGradeLevel " & _
'                                         ",CurrentSalary " & _
'                                         ",NextGradeGroup " & _
'                                         ",NextGrade " & _
'                                         ",NextGradeLevel " & _
'                                         ",NextSalary " & _
'                                         ",'" & Now.Date & "' As Today " & _
'                                         ",'" & Company._Object._Name & "' As CompanyName " & _
'                                         ",'" & Company._Object._Address1 & "' As Address1 " & _
'                                         ",'" & Company._Object._Address2 & "' As Address2 " & _
'                                         ",'" & objCity._Name & "' As C_City " & _
'                                         ",'" & objState._Name & "' As C_State " & _
'                                         ",'" & mstrCountry & "' As C_Country " & _
'                                         ",'" & Company._Object._Phone1 & "' As Phone1 " & _
'                                         ",'" & Company._Object._Phone2 & "' As Phone2 " & _
'                                         ",'" & Company._Object._Phone3 & "' As Phone3 " & _
'                                         ",'" & Company._Object._Fax & "' As Fax " & _
'                                         ",'" & Company._Object._Email & "' As Email " & _
'                                         ",'" & Company._Object._Website & "' As Website " & _
'                                         ",'" & Company._Object._Registerdno & "' As RegNo1 " & _
'                                         ",'" & Company._Object._Vatno & "' As RegNo2 " & _
'                                         ",'" & Company._Object._Tinno & "' As TinNo " & _
'                                         ",'" & Company._Object._Reference & "' As Reference " & _
'                                         ",EmpId " & _
'                                    "FROM " & _
'                                    "( " & _
'                                         "SELECT " & _
'                                               "ISNULL(tnashift_master.shiftname,'') AS Shift " & _
'                                              ",ISNULL(cfcommon_master.name,'') AS Title " & _
'                                              ",ISNULL(hremployee_master.employeecode,'') AS EmpCode " & _
'                                              ",ISNULL(hremployee_master.firstname,'') AS FirstName " & _
'                                              ",ISNULL(hremployee_master.surname,'') AS SurName " & _
'                                              ",ISNULL(hremployee_master.othername,'') AS OtherName " & _
'                                              ",ISNULL(hremployee_master.email,'') AS Email " & _
'                                              ",ISNULL(CONVERT(CHAR(8),hremployee_master.birthdate,112),'') AS BirthDate " & _
'                                              ",ISNULL(CONVERT(CHAR(8),hremployee_master.anniversary_date,112),'') AS AnniversaryDate " & _
'                                              ",ISNULL(hremployee_master.present_address1,'') AS EmpAddress1 " & _
'                                              ",ISNULL(hremployee_master.present_address2,'') AS EmpAddress2 " & _
'                                              ",ISNULL(hrmsConfiguration..cfcity_master .name,'') AS City " & _
'                                              ",ISNULL(hrmsConfiguration..cfstate_master .name,'') AS State " & _
'                                              ",ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Country " & _
'                                              ",ISNULL(CONVERT(CHAR(8),hremployee_master.suspended_from_date,112),'') AS SuspendedFrom " & _
'                                              ",ISNULL(CONVERT(CHAR(8),hremployee_master.suspended_to_date,112),'') AS SuspendedTo " & _
'                                              ",ISNULL(CONVERT(CHAR(8),hremployee_master.probation_from_date,112),'') AS ProbationFrom " & _
'                                              ",ISNULL(CONVERT(CHAR(8),hremployee_master.probation_to_date,112),'') AS ProbationTo " & _
'                                              ",ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),'') AS TerminationDate " & _
'                                              ",ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),'') AS RetirementDate " & _
'                                              ",ISNULL(CONVERT(CHAR(8),hremployee_master.confirmation_date,112),'') AS ConfirmationDate " & _
'                                              ",ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112),'') AS EmploymentLeavingDate " & _
'                                              ",ISNULL(CONVERT(CHAR(8),hremployee_master.appointeddate,112),'') AS ApponintmentDate " & _
'                                              ",ISNULL(hrstation_master.name,'') AS Station " & _
'                                              ",ISNULL(hrdepartment_group_master.name,'') AS DepartmentGroup " & _
'                                              ",ISNULL(hrdepartment_master.name,'') AS Department " & _
'                                              ",ISNULL(hrsectiongroup_master.name,'') AS SectionGroup " & _
'                                              ",ISNULL(hrsection_master.name,'') AS Section " & _
'                                              ",ISNULL(hrunitgroup_master.name,'') AS UnitGroup " & _
'                                              ",ISNULL(hrunit_master.name,'') AS Unit " & _
'                                              ",ISNULL(hrteam_master.name,'') AS Team " & _
'                                              ",ISNULL(hrjobgroup_master.name,'') AS JobGroup " & _
'                                              ",ISNULL(hrjob_master.job_name,'') AS Job " & _
'                                              ",ISNULL(hrclassgroup_master.name,'') AS ClassGroup " & _
'                                              ",ISNULL(hrclasses_master.name,'') AS Class " & _
'                                              ",ISNULL(prcostcenter_master.costcentername,'') AS CostCenter " & _
'                                              ",ISNULL(ReportGroup.name,'') AS ReportToJobGroup " & _
'                                              ",ISNULL(Report.job_name,'') AS ReportToJob " & _
'                                              ",hremployee_master.employeeunkid AS EmpId " & _
'                                         "FROM hremployee_master " & _
'                                              "LEFT JOIN hrclassgroup_master ON hremployee_master.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
'                                              "LEFT JOIN hrjobgroup_master ON hremployee_master.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
'                                              "LEFT JOIN hrteam_master ON hremployee_master.teamunkid = hrteam_master.teamunkid " & _
'                                              "LEFT JOIN hrunit_master ON hremployee_master.unitunkid = hrunit_master.unitunkid " & _
'                                              "LEFT JOIN hrunitgroup_master ON hremployee_master.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
'                                              "LEFT JOIN hrsectiongroup_master ON hremployee_master.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
'                                              "LEFT JOIN hrdepartment_group_master ON hremployee_master.deptgroupunkid = hrdepartment_group_master.deptgroupunkid " & _
'                                              "LEFT JOIN prcostcenter_master ON hremployee_master.costcenterunkid = prcostcenter_master.costcenterunkid " & _
'                                              "LEFT JOIN hrclasses_master ON hremployee_master.classunkid = hrclasses_master.classesunkid " & _
'                                              "LEFT JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                                              "LEFT JOIN hrsection_master ON hremployee_master.sectionunkid = hrsection_master.sectionunkid " & _
'                                              "LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                                              "LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid " & _
'                                              "LEFT JOIN hrmsConfiguration..cfstate_master  ON hremployee_master.present_stateunkid= hrmsConfiguration..cfstate_master .stateunkid " & _
'                                              "LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.present_countryunkid " & _
'                                              "LEFT JOIN hrmsConfiguration..cfzipcode_master  ON hremployee_master.present_postcodeunkid = hrmsConfiguration..cfzipcode_master .zipcodeunkid " & _
'                                              "LEFT JOIN hrmsConfiguration..cfcity_master  ON hremployee_master.present_post_townunkid = hrmsConfiguration..cfcity_master .cityunkid " & _
'                                              "LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.titleunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.TITLE & "' " & _
'                                              "LEFT JOIN tnashift_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
'                                              "LEFT JOIN hrjob_master AS Report ON hrjob_master.report_tounkid = Report.jobunkid " & _
'                                              "LEFT JOIN hrjobgroup_master AS ReportGroup ON ReportGroup.jobgroupunkid = Report.jobgroupunkid " & _
'                                         "WHERE ISNULL(hremployee_master.isactive,0) = 1 AND hremployee_master.employeeunkid IN (" & CInt(StrId) & ") " & _
'                                    ") AS Main " & _
'                                    "LEFT JOIN " & _
'                                    "( " & _
'                                         "SELECT " & _
'                                          "ISNULL(hrstation_master.name,'') AS OldBranch " & _
'                                         ",ISNULL(hrdepartment_group_master.name,'') AS OldDepartmentGroup " & _
'                                         ",ISNULL(hrdepartment_master.name,'') AS OldDepartment " & _
'                                         ",ISNULL(hrsectiongroup_master.name,'') AS OldSectionGroup " & _
'                                         ",ISNULL(hrsection_master.name,'') AS OldSection " & _
'                                         ",ISNULL(hrunitgroup_master.name,'') AS OldUnitGroup " & _
'                                         ",ISNULL(hrunit_master.name,'') AS OldUnit " & _
'                                         ",ISNULL(hrteam_master.name,'') AS OldTeam " & _
'                                         ",ISNULL(hrjobgroup_master.name,'') AS OldJobGroup " & _
'                                         ",ISNULL(hrjob_master.job_name,'') AS OldJob " & _
'                                         ",ISNULL(hrclassgroup_master.name,'') AS OldClassGroup " & _
'                                         ",ISNULL(hrclasses_master.name,'') AS OldClass " & _
'                                         ",ISNULL(prcostcenter_master.costcentername,'') AS OldCostCenter " & _
'                                         ",ISNULL(ReportGroup.name,'') AS OldReportToJobGroup " & _
'                                         ",ISNULL(Report.job_name,'') AS OldReportToJob " & _
'                                         ",employeeunkid " & _
'                                    "FROM " & _
'                                    "( " & _
'                                          "SELECT " & _
'                                               "ISNULL(a.stationunkid,hremployee_master.stationunkid) AS stationunkid " & _
'                                              ",ISNULL(b.deptgroupunkid,hremployee_master.deptgroupunkid) AS deptgroupunkid " & _
'                                              ",ISNULL(c.departmentunkid,hremployee_master.departmentunkid) AS departmentunkid " & _
'                                              ",ISNULL(d.sectiongroupunkid,hremployee_master.sectiongroupunkid) AS sectiongroupunkid " & _
'                                              ",ISNULL(e.sectionunkid,hremployee_master.sectionunkid) AS sectionunkid " & _
'                                              ",ISNULL(f.unitgroupunkid,hremployee_master.unitgroupunkid) AS unitgroupunkid " & _
'                                              ",ISNULL(g.unitunkid,hremployee_master.unitunkid) AS unitunkid " & _
'                                              ",ISNULL(h.teamunkid,hremployee_master.teamunkid) AS teamunkid " & _
'                                              ",ISNULL(i.jobgroupunkid,hremployee_master.jobgroupunkid) AS jobgroupunkid " & _
'                                              ",ISNULL(k.jobunkid,hremployee_master.jobunkid) AS jobunkid " & _
'                                              ",ISNULL(l.classgroupunkid,hremployee_master.classgroupunkid) AS classgroupunkid " & _
'                                              ",ISNULL(m.classunkid,hremployee_master.classunkid) AS classunkid " & _
'                                              ",ISNULL(n.costcenterunkid,hremployee_master.costcenterunkid) AS costcenterunkid " & _
'                                              ",hremployee_master.employeeunkid " & _
'                                         "FROM hremployee_master " & _
'                                         "LEFT JOIN " & _
'                                         "( " & _
'                                              "SELECT TOP 1 " & _
'                                                    "athremployee_master.employeeunkid " & _
'                                                   ",athremployee_master.stationunkid " & _
'                                                   ",auditdatetime " & _
'                                              "FROM athremployee_master " & _
'                                              "JOIN " & _
'                                              "( " & _
'                                                   "SELECT " & _
'                                                        "employeeunkid,stationunkid " & _
'                                                   "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
'                                              ") AS Station ON athremployee_master.employeeunkid = Station.employeeunkid AND athremployee_master.stationunkid <> Station.stationunkid " & _
'                                              "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
'                                              "ORDER BY auditdatetime DESC " & _
'                                         ") AS a ON hremployee_master.employeeunkid = a.employeeunkid " & _
'                                         "LEFT JOIN " & _
'                                         "( " & _
'                                              "SELECT TOP 1 " & _
'                                                    "athremployee_master.employeeunkid " & _
'                                                   ",athremployee_master.deptgroupunkid " & _
'                                                   ",auditdatetime " & _
'                                              "FROM athremployee_master " & _
'                                              "JOIN " & _
'                                              "( " & _
'                                                   "SELECT " & _
'                                                        "employeeunkid,deptgroupunkid " & _
'                                                   "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
'                                              ") AS DeptGrp ON athremployee_master.employeeunkid = DeptGrp.employeeunkid AND athremployee_master.deptgroupunkid <> DeptGrp.deptgroupunkid " & _
'                                              "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
'                                              "ORDER BY auditdatetime DESC " & _
'                                         ")AS b ON hremployee_master.employeeunkid = b.employeeunkid " & _
'                                         "LEFT JOIN " & _
'                                         "( " & _
'                                              "SELECT TOP 1 " & _
'                                                    "athremployee_master.employeeunkid " & _
'                                                   ",athremployee_master.departmentunkid " & _
'                                                   ",auditdatetime " & _
'                                              "FROM athremployee_master " & _
'                                              "JOIN " & _
'                                              "( " & _
'                                                   "SELECT " & _
'                                                        "employeeunkid,departmentunkid " & _
'                                                   "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
'                                              ") AS Dept ON athremployee_master.employeeunkid = Dept.employeeunkid AND athremployee_master.departmentunkid <> Dept.departmentunkid " & _
'                                              "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
'                                              "ORDER BY auditdatetime DESC " & _
'                                         ") AS c ON hremployee_master.employeeunkid=c.employeeunkid " & _
'                                         "LEFT JOIN " & _
'                                         "( " & _
'                                              "SELECT TOP 1 " & _
'                                                    "athremployee_master.employeeunkid " & _
'                                                   ",athremployee_master.sectiongroupunkid " & _
'                                                   ",auditdatetime " & _
'                                              "FROM athremployee_master " & _
'                                              "JOIN " & _
'                                              "( " & _
'                                                   "SELECT " & _
'                                                        "employeeunkid,sectiongroupunkid " & _
'                                                   "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
'                                              ") AS SG ON athremployee_master.employeeunkid = SG.employeeunkid AND athremployee_master.sectiongroupunkid <> SG.sectiongroupunkid " & _
'                                              "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
'                                              "ORDER BY auditdatetime DESC " & _
'                                         ") AS d ON hremployee_master.employeeunkid = d.employeeunkid " & _
'                                         "LEFT JOIN " & _
'                                         "( " & _
'                                              "SELECT TOP 1 " & _
'                                                    "athremployee_master.employeeunkid " & _
'                                                   ",athremployee_master.sectionunkid " & _
'                                                   ",auditdatetime " & _
'                                              "FROM athremployee_master " & _
'                                              "JOIN " & _
'                                              "( " & _
'                                                   "SELECT " & _
'                                                        "employeeunkid,sectionunkid " & _
'                                                   "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
'                                              ") AS Sec ON athremployee_master.employeeunkid = Sec.employeeunkid AND athremployee_master.sectionunkid <> Sec.sectionunkid " & _
'                                              "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
'                                              "ORDER BY auditdatetime DESC " & _
'                                         ")as e ON hremployee_master.employeeunkid = e.employeeunkid " & _
'                                         "LEFT JOIN " & _
'                                         "( " & _
'                                              "SELECT TOP 1 " & _
'                                                    "athremployee_master.employeeunkid " & _
'                                                   ",athremployee_master.unitgroupunkid " & _
'                                                   ",auditdatetime " & _
'                                              "FROM athremployee_master " & _
'                                              "JOIN " & _
'                                              "( " & _
'                                                   "SELECT " & _
'                                                        "employeeunkid,unitgroupunkid " & _
'                                                   "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
'                                              ") AS UG ON athremployee_master.employeeunkid = UG.employeeunkid AND athremployee_master.unitgroupunkid <> UG.unitgroupunkid " & _
'                                              "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
'                                              "ORDER BY auditdatetime DESC " & _
'                                         ")AS f ON hremployee_master.employeeunkid=f.employeeunkid " & _
'                                         "LEFT JOIN " & _
'                                         "( " & _
'                                              "SELECT TOP 1 " & _
'                                                    "athremployee_master.employeeunkid " & _
'                                                   ",athremployee_master.unitunkid " & _
'                                                   ",auditdatetime " & _
'                                              "FROM athremployee_master " & _
'                                              "JOIN " & _
'                                              "( " & _
'                                                   "SELECT " & _
'                                                        "employeeunkid,unitunkid " & _
'                                                   "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
'                                              ") AS Unit ON athremployee_master.employeeunkid = Unit.employeeunkid AND athremployee_master.unitunkid <> Unit.unitunkid " & _
'                                              "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
'                                              "ORDER BY auditdatetime DESC " & _
'                                         ")AS g ON hremployee_master.employeeunkid=g.employeeunkid " & _
'                                         "LEFT JOIN " & _
'                                         "( " & _
'                                              "SELECT TOP 1 " & _
'                                                    "athremployee_master.employeeunkid " & _
'                                                   ",athremployee_master.teamunkid " & _
'                                                   ",auditdatetime " & _
'                                              "FROM athremployee_master " & _
'                                              "JOIN " & _
'                                              "( " & _
'                                                   "SELECT " & _
'                                                        "employeeunkid,teamunkid " & _
'                                                   "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
'                                              ") AS Team ON athremployee_master.employeeunkid = Team.employeeunkid AND athremployee_master.teamunkid <> Team.teamunkid " & _
'                                              "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
'                                              "ORDER BY auditdatetime DESC " & _
'                                         ") AS h ON hremployee_master.employeeunkid=h.employeeunkid " & _
'                                         "LEFT JOIN " & _
'                                         "( " & _
'                                              "SELECT TOP 1 " & _
'                                                    "athremployee_master.employeeunkid " & _
'                                                   ",athremployee_master.jobgroupunkid " & _
'                                                   ",auditdatetime " & _
'                                              "FROM athremployee_master " & _
'                                              "JOIN " & _
'                                              "( " & _
'                                                   "SELECT " & _
'                                                        "employeeunkid,jobgroupunkid " & _
'                                                   "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
'                                              ") AS JG ON athremployee_master.employeeunkid = JG.employeeunkid AND athremployee_master.jobgroupunkid <> JG.jobgroupunkid " & _
'                                              "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
'                                              "ORDER BY auditdatetime DESC " & _
'                                         ") AS i ON hremployee_master.employeeunkid = i.employeeunkid " & _
'                                         "LEFT JOIN " & _
'                                         "( " & _
'                                              "SELECT TOP 1 " & _
'                                                    "athremployee_master.employeeunkid " & _
'                                                   ",athremployee_master.jobunkid " & _
'                                                   ",auditdatetime " & _
'                                              "FROM athremployee_master " & _
'                                              "JOIN " & _
'                                              "( " & _
'                                                   "SELECT " & _
'                                                        "employeeunkid,jobunkid " & _
'                                                   "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
'                                              ") AS Job ON athremployee_master.employeeunkid = Job.employeeunkid AND athremployee_master.jobunkid <> Job.jobunkid " & _
'                                              "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
'                                              "ORDER BY auditdatetime DESC " & _
'                                         ")AS k ON hremployee_master.employeeunkid = k.employeeunkid " & _
'                                         "LEFT JOIN " & _
'                                         "( " & _
'                                              "SELECT TOP 1 " & _
'                                                    "athremployee_master.employeeunkid " & _
'                                                   ",athremployee_master.classgroupunkid " & _
'                                                   ",auditdatetime " & _
'                                              "FROM athremployee_master " & _
'                                              "JOIN " & _
'                                              "( " & _
'                                                   "SELECT " & _
'                                                        "employeeunkid,classgroupunkid " & _
'                                                   "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
'                                              ") AS CG ON athremployee_master.employeeunkid = CG.employeeunkid AND athremployee_master.classgroupunkid <> CG.classgroupunkid " & _
'                                              "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
'                                              "ORDER BY auditdatetime DESC " & _
'                                         ")AS l ON hremployee_master.employeeunkid = l.employeeunkid " & _
'                                         "LEFT JOIN " & _
'                                         "( " & _
'                                              "SELECT TOP 1 " & _
'                                                    "athremployee_master.employeeunkid " & _
'                                                   ",athremployee_master.classunkid " & _
'                                                   ",auditdatetime " & _
'                                              "FROM athremployee_master " & _
'                                              "JOIN " & _
'                                              "( " & _
'                                                   "SELECT " & _
'                                                        "employeeunkid,classunkid " & _
'                                                   "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
'                                              ") AS CLS ON athremployee_master.employeeunkid = CLS.employeeunkid AND athremployee_master.classunkid <> CLS.classunkid " & _
'                                              "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
'                                              "ORDER BY auditdatetime DESC " & _
'                                         ")AS m ON hremployee_master.employeeunkid = m.employeeunkid " & _
'                                         "LEFT JOIN " & _
'                                         "( " & _
'                                              "SELECT TOP 1 " & _
'                                                    "athremployee_master.employeeunkid " & _
'                                                   ",athremployee_master.costcenterunkid " & _
'                                                   ",auditdatetime " & _
'                                              "FROM athremployee_master " & _
'                                              "JOIN " & _
'                                              "( " & _
'                                                   "SELECT " & _
'                                                        "employeeunkid,costcenterunkid " & _
'                                                   "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
'                                              ") AS CC ON athremployee_master.employeeunkid = CC.employeeunkid AND athremployee_master.costcenterunkid <> CC.costcenterunkid " & _
'                                              "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
'                                              "ORDER BY auditdatetime DESC " & _
'                                         ")AS n ON hremployee_master.employeeunkid = n.employeeunkid " & _
'                                         "LEFT JOIN " & _
'                                         "( " & _
'                                              "SELECT TOP 1 " & _
'                                                    "athremployee_master.employeeunkid " & _
'                                                   ",athremployee_master.gradegroupunkid " & _
'                                                   ",auditdatetime " & _
'                                              "FROM athremployee_master " & _
'                                              "JOIN " & _
'                                              "( " & _
'                                                   "SELECT " & _
'                                                        "employeeunkid,gradegroupunkid " & _
'                                                   "FROM hremployee_master WHERE employeeunkid = '" & CInt(StrId) & "' " & _
'                                              ") AS GG ON athremployee_master.employeeunkid = GG.employeeunkid AND athremployee_master.gradegroupunkid <> GG.gradegroupunkid " & _
'                                              "WHERE athremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
'                                              "ORDER BY auditdatetime DESC " & _
'                                         ") AS o ON hremployee_master.employeeunkid = o.employeeunkid " & _
'                                         "WHERE hremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
'                                    ") AS OLD " & _
'                                         "LEFT JOIN hrclassgroup_master ON OLD.classgroupunkid = hrclassgroup_master.classgroupunkid " & _
'                                         "LEFT JOIN hrjobgroup_master ON OLD.jobgroupunkid = hrjobgroup_master.jobgroupunkid " & _
'                                         "LEFT JOIN hrteam_master ON OLD.teamunkid = hrteam_master.teamunkid " & _
'                                         "LEFT JOIN hrunit_master ON OLD.unitunkid = hrunit_master.unitunkid " & _
'                                         "LEFT JOIN hrunitgroup_master ON OLD.unitgroupunkid = hrunitgroup_master.unitgroupunkid " & _
'                                         "LEFT JOIN hrsectiongroup_master ON OLD.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid " & _
'                                         "LEFT JOIN hrdepartment_group_master ON OLD.deptgroupunkid = hrdepartment_group_master.deptgroupunkid " & _
'                                         "LEFT JOIN prcostcenter_master ON OLD.costcenterunkid = prcostcenter_master.costcenterunkid " & _
'                                         "LEFT JOIN hrclasses_master ON OLD.classunkid = hrclasses_master.classesunkid " & _
'                                         "LEFT JOIN hrjob_master ON OLD.jobunkid = hrjob_master.jobunkid " & _
'                                         "LEFT JOIN hrsection_master ON OLD.sectionunkid = hrsection_master.sectionunkid " & _
'                                         "LEFT JOIN hrdepartment_master ON OLD.departmentunkid = hrdepartment_master.departmentunkid " & _
'                                         "LEFT JOIN hrstation_master ON OLD.stationunkid = hrstation_master.stationunkid " & _
'                                         "LEFT JOIN hrjob_master AS Report ON hrjob_master.report_tounkid = Report.jobunkid " & _
'                                         "LEFT JOIN hrjobgroup_master AS ReportGroup ON ReportGroup.jobgroupunkid = Report.jobgroupunkid " & _
'                                    "WHERE 1 = 1 " & _
'                                    ") AS old_Allocation ON old_Allocation.employeeunkid = Main.EmpId " & _
'                                    "LEFT JOIN " & _
'                                    "( " & _
'                                         "SELECT TOP 1 " & _
'                                               "employeeunkid " & _
'                                              ",ISNULL(hrgradegroup_master.name,'') AS CurrentGradeGroup " & _
'                                              ",ISNULL(hrgrade_master.name,'') AS CurrentGrade " & _
'                                              ",ISNULL(hrgradelevel_master.name,'') AS CurrentGradeLevel " & _
'                                              ",newscale AS CurrentSalary " & _
'                                         "FROM prsalaryincrement_tran " & _
'                                              "JOIN hrgradegroup_master ON prsalaryincrement_tran.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
'                                              "JOIN hrgrade_master ON prsalaryincrement_tran.gradeunkid = hrgrade_master.gradeunkid " & _
'                                              "JOIN hrgradelevel_master ON prsalaryincrement_tran.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
'                                         "WHERE prsalaryincrement_tran.employeeunkid = '" & CInt(StrId) & "' AND prsalaryincrement_tran.incrementdate <= '" & ConfigParameter._Object._CurrentDateAndTime.Date & "' " & _
'                                         "ORDER BY prsalaryincrement_tran.incrementdate DESC " & _
'                                    ")AS CGrades ON CGrades.employeeunkid = Main.EmpId " & _
'                                    "LEFT JOIN " & _
'                                    "( " & _
'                                         "SELECT TOP 1 " & _
'                                     "employeeunkid " & _
'                                    ",ISNULL(hrgradegroup_master.name,'') AS OldGradeGroup " & _
'                                    ",ISNULL(hrgrade_master.name,'') AS OldGrade " & _
'                                    ",ISNULL(hrgradelevel_master.name,'') AS OldGradeLevel " & _
'                                    ",newscale AS OldSalary " & _
'                                    "FROM " & _
'                                    "( " & _
'                                         "SELECT TOP 2 " & _
'                                               "A.gradegroupunkid " & _
'                                              ",a.gradeunkid " & _
'                                              ",a.gradelevelunkid " & _
'                                              ",a.newscale " & _
'                                              ",a.incrementdate " & _
'                                              ",a.employeeunkid " & _
'                                         "FROM hremployee_master " & _
'                                         "LEFT JOIN " & _
'                                         "( " & _
'                                              "SELECT " & _
'                                                    "gradegroupunkid " & _
'                                                   ",gradeunkid " & _
'                                                   ",gradelevelunkid " & _
'                                                   ",newscale " & _
'                                                   ",employeeunkid " & _
'                                                   ",incrementdate " & _
'                                              "FROM prsalaryincrement_tran " & _
'                                              "WHERE prsalaryincrement_tran.employeeunkid = '" & CInt(StrId) & "' AND prsalaryincrement_tran.incrementdate <= '" & ConfigParameter._Object._CurrentDateAndTime.Date & "' " & _
'                                         ") AS A ON dbo.hremployee_master.employeeunkid = A.employeeunkid " & _
'                                         "WHERE hremployee_master.employeeunkid = '" & CInt(StrId) & "' " & _
'                                         "ORDER BY a.incrementdate DESC " & _
'                                         ") AS B " & _
'                                         "JOIN hrgradegroup_master ON B.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
'                                         "JOIN hrgrade_master ON B.gradeunkid = hrgrade_master.gradeunkid " & _
'                                         "JOIN hrgradelevel_master ON B.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
'                                         "ORDER BY b.incrementdate ASC " & _
'                                    ") AS OGrades ON OGrades.employeeunkid = Main.EmpId " & _
'                                    "LEFT JOIN " & _
'                                    "( " & _
'                                         "SELECT TOP 1 " & _
'                                               "employeeunkid " & _
'                                              ",ISNULL(hrgradegroup_master.name,'') AS NextGradeGroup " & _
'                                              ",ISNULL(hrgrade_master.name,'') AS NextGrade " & _
'                                              ",ISNULL(hrgradelevel_master.name,'') AS NextGradeLevel " & _
'                                              ",newscale AS NextSalary " & _
'                                         "FROM prsalaryincrement_tran " & _
'                                              "JOIN hrgradegroup_master ON prsalaryincrement_tran.gradegroupunkid = hrgradegroup_master.gradegroupunkid " & _
'                                              "JOIN hrgrade_master ON prsalaryincrement_tran.gradeunkid = hrgrade_master.gradeunkid " & _
'                                              "JOIN hrgradelevel_master ON prsalaryincrement_tran.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
'                                         "WHERE employeeunkid = '" & CInt(StrId) & "' AND incrementdate > '" & ConfigParameter._Object._CurrentDateAndTime.Date & "' " & _
'                                    ") AS FGrades ON FGrades.employeeunkid = Main.EmpId "

'                            dsList = objDataOperation.ExecQuery(StrQ, strListName)
'                            If objDataOperation.ErrorMessage <> "" Then
'                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                Throw exForce
'                            End If
'                            If dsList.Tables(0).Rows.Count > 0 Then
'                                If dTable Is Nothing Then
'                                    dTable = dsList.Tables(0).Clone
'                                    dTable.Rows.Clear()
'                                End If
'                                For Each dRow As DataRow In dsList.Tables(0).Rows
'                                    dTable.ImportRow(dRow)
'                                Next
'                                dsList.Tables.RemoveAt(0)
'                                dsList.Tables.Add(dTable.Copy)
'                            End If
'                        Next
'                    End If
'                    'S.SANDEEP [ 23 JAN 2012 ] -- END

'                Case 2     'Applicant

'                    'Pinkal (12-Oct-2011) -- Start

'                    StrQ = "SELECT " & _
'                                "	 ISNULL(cfcommon_master.name,'') AS Title " & _
'                                "	,ISNULL(rcapplicant_master.firstname,'') AS Firstname " & _
'                                "	,ISNULL(rcapplicant_master.surname,'') AS Surname " & _
'                                "	,ISNULL(rcapplicant_master.othername,'') AS Othername " & _
'                                "	,ISNULL(rcapplicant_master.gender,'') AS Gender " & _
'                                "	,ISNULL(rcapplicant_master.email,'') AS Email " & _
'                                "	,ISNULL(rcapplicant_master.present_address1,'') AS Applicant_Address1 " & _
'                                "	,ISNULL(rcapplicant_master.present_address2,'') AS Applicant_Address2 " & _
'                                "	,ISNULL(hrmsConfiguration..cfcity_master.name,'') AS Applicant_City " & _
'                                "	,ISNULL(hrmsConfiguration..cfstate_master.name,'') AS Applicant_State " & _
'                                "	,ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS Applicant_Country " & _
'                                "	,ISNULL(rcapplicant_master.present_mobileno,'') AS Mobileno " & _
'                                "	,ISNULL(CONVERT(CHAR(8),rcapplicant_master.birth_date,112),'') AS BirthDate " & _
'                                "	,ISNULL(CONVERT(CHAR(8),rcapplicant_master.anniversary_date,112),'') AS AnniversaryDate " & _
'                                "	,ISNULL(rcvacancy_master.vacancytitle,'') AS Vacancy " & _
'                                "	,'" & Now.Date & "' As Today " & _
'                                "	,'" & Company._Object._Name & "' As CompanyName " & _
'                                "	,'" & Company._Object._Address1 & "' As Address1 " & _
'                                "	,'" & Company._Object._Address2 & "' As Address2 " & _
'                                "	,'" & objCity._Name & "' As C_City " & _
'                                "	,'" & objState._Name & "' As C_State " & _
'                                "	,'" & mstrCountry & "' As C_Country " & _
'                                "	,'" & Company._Object._Phone1 & "' As Phone1 " & _
'                                "	,'" & Company._Object._Phone2 & "' As Phone2 " & _
'                                "	,'" & Company._Object._Phone3 & "' As Phone3 " & _
'                                "	,'" & Company._Object._Fax & "' As Fax " & _
'                                "	,'" & Company._Object._Email & "' As Email " & _
'                                "	,'" & Company._Object._Website & "' As Website " & _
'                                "	,'" & Company._Object._Registerdno & "' As RegNo1 " & _
'                                "	,'" & Company._Object._Vatno & "' As RegNo2 " & _
'                                "	,'" & Company._Object._Tinno & "' As TinNo " & _
'                                "	,'" & Company._Object._Reference & "' As Reference " & _
'                                "	,rcapplicant_master.applicantunkid AS EmpId " & _
'                                "	,rcapplicant_master.applicant_code AS EmpCode " & _
'                                "FROM rcapplicant_master " & _
'                                "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = rcapplicant_master.titleunkid " & _
'                                "	LEFT JOIN hrmsConfiguration..cfstate_master ON hrmsConfiguration..cfstate_master.stateunkid = rcapplicant_master.present_stateunkid " & _
'                                "	LEFT JOIN hrmsConfiguration..cfcity_master ON hrmsConfiguration..cfcity_master.cityunkid = rcapplicant_master.present_post_townunkid " & _
'                                "	LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = rcapplicant_master.present_countryunkid " & _
'                                "	LEFT JOIN rcvacancy_master ON rcapplicant_master.vacancyunkid = rcvacancy_master.vacancyunkid " & _
'                                "WHERE rcapplicant_master.applicantunkid IN ( " & strEmpId & ") "

'                    dsList = objDataOperation.ExecQuery(StrQ, strListName)

'                    'Pinkal (12-Oct-2011) -- End

'                Case 3     'Leave
'                Case 4     'Payroll

'                Case 5  'DEPENDANTS & BENEFICIARIES

'                    'Pinkal (20-Jan-2012) -- Start
'                    'Enhancement : TRA Changes

'                Case 6     'Medical

'                    StrQ = " SELECT " & _
'                               "	 ISNULL(tnashift_master.shiftname,'') AS Shift " & _
'                               "	,ISNULL(cfcommon_master.name,'') AS Title " & _
'                               "	,ISNULL(hremployee_master.employeecode,'') AS EmpCode " & _
'                               "	,ISNULL(hremployee_master.firstname,'') AS FirstName " & _
'                               "	,ISNULL(hremployee_master.surname,'') AS SurName " & _
'                               "	,ISNULL(hremployee_master.othername,'') AS OtherName " & _
'                               "	,ISNULL(hremployee_master.email,'') AS Email " & _
'                               "	,ISNULL(CONVERT(CHAR(8),hremployee_master.birthdate,112),'') AS BirthDate " & _
'                               "	,ISNULL(CONVERT(CHAR(8),hremployee_master.anniversary_date,112),'') AS AnniversaryDate " & _
'                               "	,ISNULL(hremployee_master.present_address1,'') AS EmpAddress1 " & _
'                               "	,ISNULL(hremployee_master.present_address2,'') AS EmpAddress2 " & _
'                               "	,ISNULL(hrmsConfiguration..cfcity_master .name,'') AS City " & _
'                               "	,ISNULL(hrmsConfiguration..cfstate_master .name,'') AS State " & _
'                               "	,ISNULL(hrmsConfiguration..cfcountry_master.country_name1,'') AS Country " & _
'                               "	,ISNULL(hrstation_master.name,'') AS Station " & _
'                               "	,ISNULL(hrdepartment_master.name,'') AS Department " & _
'                               "	,ISNULL(hrsection_master.name,'') AS Section " & _
'                               "	,ISNULL(hrjob_master.job_name,'') AS Job " & _
'                               "	,ISNULL(hrgrade_master.name,'') AS Grade " & _
'                               "	,ISNULL(hrgradelevel_master.name,'') AS GradeLevel " & _
'                               "	,ISNULL(hrclasses_master.name,'') AS Class " & _
'                               "	,ISNULL(prcostcenter_master.costcentername,'') AS CostCenter " & _
'                               "	,ISNULL(CONVERT(CHAR(8),hremployee_master.suspended_from_date,112),'') AS SuspendedFrom " & _
'                               "	,ISNULL(CONVERT(CHAR(8),hremployee_master.suspended_to_date,112),'') AS SuspendedTo " & _
'                               "	,ISNULL(CONVERT(CHAR(8),hremployee_master.probation_from_date,112),'') AS ProbationFrom " & _
'                               "	,ISNULL(CONVERT(CHAR(8),hremployee_master.probation_to_date,112),'') AS ProbationTo " & _
'                               "	,ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),'') AS TerminationFrom " & _
'                               "	,ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),'') AS TerminationTo " & _
'                               "	,'" & Now.Date & "' As Today " & _
'                               "	,'" & Company._Object._Name & "' As CompanyName " & _
'                               "	,'" & Company._Object._Address1 & "' As Address1 " & _
'                               "	,'" & Company._Object._Address2 & "' As Address2 " & _
'                               "	,'" & objCity._Name & "' As C_City " & _
'                               "	,'" & objState._Name & "' As C_State " & _
'                               "	,'" & mstrCountry & "' As C_Country " & _
'                               "	,'" & Company._Object._Phone1 & "' As Phone1 " & _
'                               "	,'" & Company._Object._Phone2 & "' As Phone2 " & _
'                               "	,'" & Company._Object._Phone3 & "' As Phone3 " & _
'                               "	,'" & Company._Object._Fax & "' As Fax " & _
'                               "	,'" & Company._Object._Email & "' As Email " & _
'                               "	,'" & Company._Object._Website & "' As Website " & _
'                               "	,'" & Company._Object._Registerdno & "' As RegNo1 " & _
'                               "	,'" & Company._Object._Vatno & "' As RegNo2 " & _
'                               "	,'" & Company._Object._Tinno & "' As TinNo " & _
'                               "	,'" & Company._Object._Reference & "' As Reference " & _
'                               "	,hremployee_master.employeeunkid AS EmpId " & _
'                               "	,mdmedical_sicksheet.sicksheetunkid AS sicksheetunkid " & _
'                               "	,mdmedical_sicksheet.sicksheetdate AS sicksheetdate " & _
'                               "  FROM hremployee_master " & _
'                               "	LEFT JOIN prcostcenter_master ON hremployee_master.costcenterunkid = prcostcenter_master.costcenterunkid " & _
'                               "	LEFT JOIN hrclasses_master ON hremployee_master.classunkid = hrclasses_master.classesunkid " & _
'                               "	LEFT JOIN hrgradelevel_master ON hremployee_master.gradelevelunkid = hrgradelevel_master.gradelevelunkid " & _
'                               "	LEFT JOIN hrgrade_master ON hremployee_master.gradeunkid = hrgrade_master.gradeunkid " & _
'                               "	LEFT JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                               "	LEFT JOIN hrsection_master ON hremployee_master.sectionunkid = hrsection_master.sectionunkid " & _
'                               "	LEFT JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                               "	LEFT JOIN hrstation_master ON hremployee_master.stationunkid = hrstation_master.stationunkid " & _
'                               "	LEFT JOIN hrmsConfiguration..cfstate_master  ON hremployee_master.present_stateunkid= hrmsConfiguration..cfstate_master .stateunkid " & _
'                               "	LEFT JOIN hrmsConfiguration..cfcountry_master ON hrmsConfiguration..cfcountry_master.countryunkid = hremployee_master.present_countryunkid " & _
'                               "	LEFT JOIN hrmsConfiguration..cfzipcode_master  ON hremployee_master.present_postcodeunkid = hrmsConfiguration..cfzipcode_master .zipcodeunkid " & _
'                               "	LEFT JOIN hrmsConfiguration..cfcity_master  ON hremployee_master.present_post_townunkid = hrmsConfiguration..cfcity_master .cityunkid " & _
'                               "	LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_master.titleunkid AND mastertype = " & enCommonMaster.TITLE & _
'                               "	LEFT JOIN tnashift_master ON hremployee_master.shiftunkid = tnashift_master.shiftunkid " & _
'                               "    JOIN mdmedical_sicksheet on mdmedical_sicksheet.employeeunkid = hremployee_master.employeeunkid " & _
'                               "    WHERE ISNULL(hremployee_master.isactive,0) = 1 " & _
'                               "	AND mdmedical_sicksheet.sicksheetunkid IN ( " & strEmpId & " ) "


'                    'Pinkal (20-Jan-2012) -- End
'                    dsList = objDataOperation.ExecQuery(StrQ, strListName)

'            End Select




'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Select Case intModuleRefId
'                Case 1, 6  'EMPLOYEE,'MEDICAL
'                    For Each dtRow As DataRow In dsList.Tables(0).Rows
'                        If dtRow.Item("BirthDate").ToString.Trim.Length > 0 Then
'                            dtRow.Item("BirthDate") = eZeeDate.convertDate(dtRow.Item("BirthDate").ToString).ToShortDateString
'                        End If
'                        If dtRow.Item("AnniversaryDate").ToString.Trim.Length > 0 Then
'                            dtRow.Item("AnniversaryDate") = eZeeDate.convertDate(dtRow.Item("AnniversaryDate").ToString).ToShortDateString
'                        End If
'                        If dtRow.Item("SuspendedFrom").ToString.Trim.Length > 0 Then
'                            dtRow.Item("SuspendedFrom") = eZeeDate.convertDate(dtRow.Item("SuspendedFrom").ToString).ToShortDateString
'                        End If
'                        If dtRow.Item("SuspendedTo").ToString.Trim.Length > 0 Then
'                            dtRow.Item("SuspendedTo") = eZeeDate.convertDate(dtRow.Item("SuspendedTo").ToString).ToShortDateString
'                        End If
'                        If dtRow.Item("ProbationFrom").ToString.Trim.Length > 0 Then
'                            dtRow.Item("ProbationFrom") = eZeeDate.convertDate(dtRow.Item("ProbationFrom").ToString).ToShortDateString
'                        End If
'                        If dtRow.Item("ProbationTo").ToString.Trim.Length > 0 Then
'                            dtRow.Item("ProbationTo") = eZeeDate.convertDate(dtRow.Item("ProbationTo").ToString).ToShortDateString
'                        End If
'                        If dtRow.Item("ProbationTo").ToString.Trim.Length > 0 Then
'                            dtRow.Item("ProbationTo") = eZeeDate.convertDate(dtRow.Item("ProbationTo").ToString).ToShortDateString
'                        End If
'                        If dtRow.Item("TerminationDate").ToString.Trim.Length > 0 Then
'                            dtRow.Item("TerminationDate") = eZeeDate.convertDate(dtRow.Item("TerminationDate").ToString).ToShortDateString
'                        End If
'                        If dtRow.Item("RetirementDate").ToString.Trim.Length > 0 Then
'                            dtRow.Item("RetirementDate") = eZeeDate.convertDate(dtRow.Item("RetirementDate").ToString).ToShortDateString
'                        End If
'                        If dtRow.Item("EmploymentLeavingDate").ToString.Trim.Length > 0 Then
'                            dtRow.Item("EmploymentLeavingDate") = eZeeDate.convertDate(dtRow.Item("EmploymentLeavingDate").ToString).ToShortDateString
'                        End If
'                        If dtRow.Item("ApponintmentDate").ToString.Trim.Length > 0 Then
'                            dtRow.Item("ApponintmentDate") = eZeeDate.convertDate(dtRow.Item("ApponintmentDate").ToString).ToShortDateString
'                        End If

'                        If dtRow.Item("ConfirmationDate").ToString.Trim.Length > 0 Then
'                            dtRow.Item("ConfirmationDate") = eZeeDate.convertDate(dtRow.Item("ConfirmationDate").ToString).ToShortDateString
'                        End If

'                    Next
'                Case 2  'APPLICANT
'                    For Each dtRow As DataRow In dsList.Tables(0).Rows
'                        If dtRow.Item("BirthDate").ToString.Trim.Length > 0 Then
'                            dtRow.Item("BirthDate") = eZeeDate.convertDate(dtRow.Item("BirthDate").ToString).ToShortDateString
'                        End If
'                        If dtRow.Item("AnniversaryDate").ToString.Trim.Length > 0 Then
'                            dtRow.Item("AnniversaryDate") = eZeeDate.convertDate(dtRow.Item("AnniversaryDate").ToString).ToShortDateString
'                        End If
'                    Next
'                Case 3  'LEAVE
'                Case 4  'PAYROLL
'                Case 5  'DEPENDANT & BENEFICIATIES
'            End Select



'            Return dsList

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetEmployeeData", mstrModuleName)
'            Return Nothing
'        Finally
'            If dsList IsNot Nothing Then dsList.Dispose()
'            dsList = Nothing
'            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
'            objDataOperation = Nothing
'            exForce = Nothing
'            objCity = Nothing
'            objState = Nothing
'        End Try
'    End Function
'End Class
