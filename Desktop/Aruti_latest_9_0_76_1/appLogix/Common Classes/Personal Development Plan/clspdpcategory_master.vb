﻿'************************************************************************************************************************************
'Class Name : clspdpcategory_master
'Purpose    :
'Date       : 12-Jan-2021
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clspdpcategory_master
    Private Shared ReadOnly mstrModuleName As String = "clspdpcategory_master"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation


#Region " Private variables "
    Private mintCategoryunkid As Integer
    Private mstrCategory As String = String.Empty
    Private mintCategoryTypeId As Integer
    Private mintSortOrder As Integer
    Private mblnIsactive As Boolean = True
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""
    'S.SANDEEP |03-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : PDP_PM_LINKING
    Private mblnIsincludeinpm As Boolean = False
    Private mblnIsmandatory As Boolean = False
    'S.SANDEEP |03-MAY-2021| -- END
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categoryunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Categoryunkid() As Integer
        Get
            Return mintCategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintCategoryunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set category
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Category() As String
        Get
            Return mstrCategory
        End Get
        Set(ByVal value As String)
            mstrCategory = value
        End Set
    End Property

    Public Property _CategoryTypeId() As Integer
        Get
            Return mintCategoryTypeId
        End Get
        Set(ByVal value As Integer)
            mintCategoryTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sortorder
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _SortOrder() As Integer
        Get
            Return mintSortOrder
        End Get
        Set(ByVal value As Integer)
            mintSortOrder = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property


    'S.SANDEEP |03-MAY-2021| -- START
    'ISSUE/ENHANCEMENT : PDP_PM_LINKING
    Public Property _Isincludeinpm() As Boolean
        Get
            Return mblnIsincludeinpm
        End Get
        Set(ByVal value As Boolean)
            mblnIsincludeinpm = value
        End Set
    End Property

    Public Property _Ismandatory() As Boolean
        Get
            Return mblnIsmandatory
        End Get
        Set(ByVal value As Boolean)
            mblnIsmandatory = value
        End Set
    End Property
    'S.SANDEEP |03-MAY-2021| -- END

#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  categoryunkid " & _
              ", category " & _
              ", viewtype " & _
              ", sortorder " & _
              ", isactive " & _
              ", isincludeinpm " & _
              ", ismandatory " & _
             "FROM " & mstrDatabaseName & "..pdpcategory_master " & _
             "WHERE categoryunkid = @categoryunkid "
            'S.SANDEEP |03-MAY-2021| -- START {isincludeinpm,ismandatory} -- END

            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCategoryunkid = CInt(dtRow.Item("categoryunkid"))
                mstrCategory = dtRow.Item("category").ToString
                mintSortOrder = CInt(dtRow.Item("sortorder"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mintCategoryTypeId = CInt(dtRow.Item("viewtype"))
                'S.SANDEEP |03-MAY-2021| -- START
                'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                mblnIsincludeinpm = CBool(dtRow.Item("isincludeinpm"))
                mblnIsmandatory = CBool(dtRow.Item("ismandatory"))
                'S.SANDEEP |03-MAY-2021| -- END
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    'Pinkal (12-Dec-2020) -- Start
    'Enhancement  -  Working on Talent Issue which is given by Andrew.
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    Public Function GetList(ByVal strTableName As String, _
                            Optional ByVal blnOnlyActive As Boolean = True, _
                            Optional ByVal strFilter As String = "", _
                            Optional ByVal blnIsFromPM As Boolean = False) As DataSet
        'Pinkal (12-Dec-2020) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  categoryunkid " & _
              ", category " & _
              ", pdpcategory_master.viewtype as viewtypeid " & _
                   ", CASE WHEN pdpcategory_master.viewtype  = " & CInt(enPDPCustomItemViewType.Table) & " then @table " & _
                   "       WHEN pdpcategory_master.viewtype  = " & CInt(enPDPCustomItemViewType.List) & " then @list " & _
                   "  ELSE '' END ViewType " & _
              ", pdpcategory_master.sortorder " & _
              ", pdpcategory_master.isactive " & _
                   ", pdpcategory_master.isincludeinpm " & _
                   ", pdpcategory_master.ismandatory " & _
                   ", CASE WHEN orgcategory = 'Performance Goals' THEN 1 ELSE 0 END AS cdel "
            If blnIsFromPM Then
                strQ &= ", CAST('9000' + CAST(categoryunkid AS NVARCHAR(MAX)) AS INT) AS Id "
            End If
            strQ &= "FROM pdpcategory_master " & _
                    " WHERE pdpcategory_master.isactive = 1 "
            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter & " "
            End If
            strQ &= "ORDER BY pdpcategory_master.sortorder "
            'S.SANDEEP |03-MAY-2021| -- START {isincludeinpm,ismandatory} -- END

            objDataOperation.AddParameter("@table", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Table"))
            objDataOperation.AddParameter("@list", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "List"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pdpcategory_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCategory, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This category is already defined. Please define new category.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@category", SqlDbType.NVarChar, mstrCategory.Trim.Length, mstrCategory.ToString)
            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSortOrder.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@viewtype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryTypeId.ToString())
            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            objDataOperation.AddParameter("@isincludeinpm", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsincludeinpm.ToString)
            objDataOperation.AddParameter("@ismandatory", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsmandatory.ToString)
            'S.SANDEEP |03-MAY-2021| -- END



            strQ = "INSERT INTO " & mstrDatabaseName & "..pdpcategory_master ( " & _
              " category " & _
              ",viewtype " & _
              ", sortorder " & _
              ", isactive" & _
              ", isincludeinpm " & _
              ", ismandatory " & _
            ") VALUES (" & _
              "  @category " & _
              ", @viewtype " & _
              ", @sortorder " & _
              ", @isactive" & _
              ", @isincludeinpm " & _
              ", @ismandatory " & _
            "); SELECT @@identity"
            'S.SANDEEP |03-MAY-2021| -- START {isincludeinpm,ismandatory} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCategoryunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (pdpcategory_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrCategory, mintCategoryunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This category is already defined. Please define new category.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryunkid.ToString)
            objDataOperation.AddParameter("@category", SqlDbType.NVarChar, mstrCategory.Trim.Length, mstrCategory.ToString)
            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSortOrder.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@viewtype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryTypeId.ToString)
            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            objDataOperation.AddParameter("@isincludeinpm", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsincludeinpm.ToString)
            objDataOperation.AddParameter("@ismandatory", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsmandatory.ToString)
            'S.SANDEEP |03-MAY-2021| -- END


            strQ = "UPDATE " & mstrDatabaseName & "..pdpcategory_master SET " & _
              " category = @category" & _
              ", sortorder = @sortorder" & _
              ", isactive = @isactive " & _
              ", viewtype = @viewtype " & _
              ", isincludeinpm = @isincludeinpm " & _
              ", ismandatory = @ismandatory " & _
            "WHERE categoryunkid = @categoryunkid "
            'S.SANDEEP |03-MAY-2021| -- START {isincludeinpm,ismandatory} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (pdpcategory_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        _Categoryunkid = intUnkid
        mblnIsactive = False

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE " & mstrDatabaseName & "..pdpcategory_master SET " & _
                   " isactive = 0 " & _
                   "WHERE categoryunkid = @categoryunkid "

            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try

            Dim Tables() As String = {"pdpitem_master", "pdpcategory_item_mapping", "pdpitemdatatran"}
            For Each value As String In Tables
                Select Case value
                    Case "pdpitemdatatran"
                        strQ = "SELECT categoryunkid FROM " & value & " WHERE categoryunkid = @categoryunkid and isvoid = 0 "

                    Case "pdpitem_master"
                        strQ = "SELECT categoryunkid FROM " & value & " WHERE categoryunkid = @categoryunkid and isactive = 1 "

                    Case "pdpcategory_item_mapping"
                        strQ = "SELECT categoryid FROM " & value & " WHERE categoryid = @categoryunkid and isvoid=0 "
                    Case Else
                End Select
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

                dsList = objDataOperation.ExecQuery(strQ, "Used")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    Return True
                    Exit For
                End If

            Next
            Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCategory As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "  categoryunkid " & _
                    ", category " & _
                    ", sortorder " & _
                    ", isactive " & _
                    ", isincludeinpm " & _
                    ", ismandatory " & _
                    "FROM " & mstrDatabaseName & "..pdpcategory_master " & _
                    "WHERE isactive = 1 " & _
                    " AND category = @category "
            'S.SANDEEP |03-MAY-2021| -- START {isincludeinpm,ismandatory} -- END

            If intUnkid > 0 Then
                strQ &= " AND categoryunkid <> @categoryunkid"
            End If

            objDataOperation.AddParameter("@category", SqlDbType.NVarChar, strcategory.Trim.Length, strcategory)
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isSortOrderExist(ByVal intSortOrder As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOper As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOper IsNot Nothing Then
            objDataOperation = xDataOper
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()


        Try
            strQ = "SELECT " & _
                    "  categoryunkid " & _
                    ", category " & _
                    ", sortorder " & _
                    ", isactive " & _
                    "FROM pdpcategory_master " & _
                    "WHERE isactive = 1 " & _
                    " AND sortorder = @sortorder "

            If intUnkid > 0 Then
                strQ &= " and categoryunkid <> @categoryunkid "
                objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If


            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, intSortOrder)
            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isSortOrderExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOper Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function getListForCombo(Optional ByVal strTableName As String = "List", _
                                    Optional ByVal mblFlag As Boolean = False, _
                                    Optional ByVal xDataOper As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOper IsNot Nothing Then
            objDataOperation = xDataOper
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as categoryunkid, ' ' +  @Select  as category, 0 AS isincludeinpm, 0 AS ismandatory UNION "
                'S.SANDEEP |03-MAY-2021| -- START {isincludeinpm,ismandatory} -- END
            End If

            strQ &= "SELECT " & _
              "  categoryunkid " & _
              ", category " & _
                    ", isincludeinpm " & _
                    ", ismandatory " & _
             "FROM " & mstrDatabaseName & "..pdpcategory_master " & _
             "WHERE pdpcategory_master.isactive = 1 "
            'S.SANDEEP |03-MAY-2021| -- START {isincludeinpm,ismandatory} -- END

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Select")

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getListForCombo; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOper Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO " & mstrDatabaseName & "..atpdpcategory_master ( " & _
                    "  tranguid " & _
                    ", categoryunkid " & _
                    ", category " & _
                    ", viewtype " & _
                    ", sortorder " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb" & _
                    ", isincludeinpm " & _
                    ", ismandatory " & _
                  ") VALUES (" & _
                    "  LOWER(NEWID()) " & _
                    ", @categoryunkid " & _
                    ", @category " & _
                    ", @viewtype " & _
                    ", @sortorder " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb" & _
                    ", @isincludeinpm " & _
                    ", @ismandatory " & _
                  ")"
            'S.SANDEEP |03-MAY-2021| -- START {isincludeinpm,ismandatory} -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryunkid.ToString)
            objDataOperation.AddParameter("@category", SqlDbType.NVarChar, mstrCategory.Trim.Length, mstrCategory.ToString)
            objDataOperation.AddParameter("@viewtype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryTypeId.ToString)
            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSortOrder.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            objDataOperation.AddParameter("@isincludeinpm", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsincludeinpm.ToString)
            objDataOperation.AddParameter("@ismandatory", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsmandatory.ToString)
            'S.SANDEEP |03-MAY-2021| -- END

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function isPDPTransectionStarted(ByVal intUnkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                       "pdpcategory_master.viewtype " & _
                       ",pdpitemdatatran.categoryunkid " & _
                    "FROM pdpitemdatatran " & _
                    "LEFT JOIN pdpcategory_master " & _
                    "ON pdpitemdatatran.categoryunkid = pdpcategory_master.categoryunkid WHERE pdpcategory_master.categoryunkid = @categoryunkid "

            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "Used")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


End Class
