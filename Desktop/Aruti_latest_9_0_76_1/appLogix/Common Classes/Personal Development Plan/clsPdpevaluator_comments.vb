﻿Imports eZeeCommonLib

Public Class clsPdpevaluator_comments
    Private Const mstrModuleName = "clsPdpevaluator_comments"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintCommenttranunkid As Integer
    Private mintPdpgoalsmstunkid As Integer
    Private mintEvaluatortypeid As Integer
    Private mintEvaluatorunkid As Integer
    Private mintReviewermstunkid As Integer

    Private mdtCommentdatetime As Date
    Private mstrComments As String = String.Empty
    Private mintEmployeeunkid As Integer
    Private mdtAttachment As DataTable
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintUserunkid As Integer
    Private mintVoidloginemployeeunkid As Integer

    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mintloginemployeeunkid As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set commenttranunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Commenttranunkid() As Integer
        Get
            Return mintCommenttranunkid
        End Get
        Set(ByVal value As Integer)
            mintCommenttranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pdpgoalsmstunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Pdpgoalsmstunkid() As Integer
        Get
            Return mintPdpgoalsmstunkid
        End Get
        Set(ByVal value As Integer)
            mintPdpgoalsmstunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set evaluatortypeid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Evaluatortypeid() As Integer
        Get
            Return mintEvaluatortypeid
        End Get
        Set(ByVal value As Integer)
            mintEvaluatortypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set evaluatorunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Evaluatorunkid() As Integer
        Get
            Return mintEvaluatorunkid
        End Get
        Set(ByVal value As Integer)
            mintEvaluatorunkid = value
        End Set
    End Property


    Public Property _Reviewermstunkid() As Integer
        Get
            Return mintReviewermstunkid
        End Get
        Set(ByVal value As Integer)
            mintReviewermstunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set commentdatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Commentdatetime() As Date
        Get
            Return mdtCommentdatetime
        End Get
        Set(ByVal value As Date)
            mdtCommentdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set comments
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Comments() As String
        Get
            Return mstrComments
        End Get
        Set(ByVal value As String)
            mstrComments = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Public Property _DtAttachment() As DataTable
        Get
            Return mdtAttachment
        End Get
        Set(ByVal value As DataTable)
            mdtAttachment = value
        End Set
    End Property



    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintloginemployeeunkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property
#End Region

    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  commenttranunkid " & _
              ", pdpgoalsmstunkid " & _
              ", evaluatortypeid " & _
              ", evaluatorunkid " & _
              ", reviewermstunkid " & _
              ", commentdatetime " & _
              ", comments " & _
              ", employeeunkid " & _
              ", isvoid " & _
             "FROM pdpevaluator_comments " & _
             "WHERE commenttranunkid = @commenttranunkid "

            objDataOperation.AddParameter("@commenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCommenttranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCommenttranunkid = CInt(dtRow.Item("commenttranunkid"))
                mintPdpgoalsmstunkid = CInt(dtRow.Item("pdpgoalsmstunkid"))
                mintEvaluatortypeid = CInt(dtRow.Item("evaluatortypeid"))
                mintEvaluatorunkid = CInt(dtRow.Item("evaluatorunkid"))
                mintReviewermstunkid = CInt(dtRow.Item("reviewermstunkid"))
                mdtCommentdatetime = dtRow.Item("commentdatetime")
                mstrComments = dtRow.Item("comments").ToString
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intPDPGoalunkid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  commenttranunkid " & _
              ", pdpgoalsmstunkid " & _
              ", evaluatortypeid " & _
              ", evaluatorunkid " & _
              ", reviewermstunkid " & _
              ", commentdatetime " & _
              ", comments " & _
              ", employeeunkid " & _
              ", isvoid " & _
             "FROM pdpevaluator_comments WHERE 1=1 "

            If blnOnlyActive Then
                strQ &= " and isactive = 1 "
            End If


            If intPDPGoalunkid > 0 Then
                strQ &= " and pdpgoalsmstunkid = @pdpgoalsmstunkid "
                objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPDPGoalunkid)
            End If




            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function isExist(Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  commenttranunkid " & _
              ", pdpgoalsmstunkid " & _
              ", evaluatortypeid " & _
              ", evaluatorunkid " & _
              ", commentdatetime " & _
              ", comments " & _
              ", employeeunkid " & _
              ", isvoid " & _
             " FROM pdpevaluator_comments " & _
             " WHERE pdpgoalsmstunkid = @pdpgoalsmstunkid and isvoid = 0 " & _
             " AND commenttranunkid = @commenttranunkid  "


            If mintEvaluatorunkid <= 0 AndAlso mintReviewermstunkid > 0 Then
                strQ &= " and reviewermstunkid=@reviewermstunkid "
                objDataOperation.AddParameter("@reviewermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReviewermstunkid)
            Else
                strQ &= " and evaluatorunkid=@evaluatorunkid "
                objDataOperation.AddParameter("@evaluatorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEvaluatorunkid)
            End If


            objDataOperation.AddParameter("@commenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpgoalsmstunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpgoalsmstunkid.ToString)
            objDataOperation.AddParameter("@evaluatortypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEvaluatortypeid.ToString)
            objDataOperation.AddParameter("@evaluatorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEvaluatorunkid.ToString)
            objDataOperation.AddParameter("@reviewermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReviewermstunkid.ToString)
            objDataOperation.AddParameter("@commentdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCommentdatetime.ToString)
            objDataOperation.AddParameter("@comments", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrComments.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

            strQ = "INSERT INTO pdpevaluator_comments ( " & _
              "  pdpgoalsmstunkid " & _
              ", evaluatortypeid " & _
              ", evaluatorunkid " & _
              ", reviewermstunkid " & _
              ", commentdatetime " & _
              ", comments " & _
              ", employeeunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", userunkid " & _
              ", voidloginemployeeunkid" & _
            ") VALUES (" & _
              "  @pdpgoalsmstunkid " & _
              ", @evaluatortypeid " & _
              ", @evaluatorunkid " & _
              ", @reviewermstunkid " & _
              ", getDate() " & _
              ", @comments " & _
              ", @employeeunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @userunkid " & _
              ", @voidloginemployeeunkid" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCommenttranunkid = dsList.Tables(0).Rows(0).Item(0)

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@commenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCommenttranunkid.ToString)
            objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpgoalsmstunkid.ToString)
            objDataOperation.AddParameter("@evaluatortypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEvaluatortypeid.ToString)
            objDataOperation.AddParameter("@evaluatorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEvaluatorunkid.ToString)
            objDataOperation.AddParameter("@reviewermstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReviewermstunkid.ToString)
            objDataOperation.AddParameter("@comments", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrComments.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            If mdtCommentdatetime = Nothing Then
                objDataOperation.AddParameter("@commentdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@commentdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCommentdatetime)
            End If


            strQ = "UPDATE pdpevaluator_comments SET " & _
              "  pdpgoalsmstunkid = @pdpgoalsmstunkid" & _
              ", evaluatortypeid = @evaluatortypeid" & _
              ", evaluatorunkid = @evaluatorunkid " & _
              ", reviewermstunkid = @reviewermstunkid " & _
              ", commentdatetime = @commentdatetime " & _
              ", comments = @comments" & _
              ", employeeunkid = @employeeunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason" & _
              ", userunkid = @userunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
            "WHERE commenttranunkid = @commenttranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function Delete(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@commenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCommenttranunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE pdpevaluator_comments SET " & _
              " isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE commenttranunkid = @commenttranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, mintCommenttranunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function DeleteAll(ByVal intPdpgoalsmstunkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPdpgoalsmstunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE pdpevaluator_comments SET " & _
              " isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE pdpgoalsmstunkid = @pdpgoalsmstunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, -1, intPdpgoalsmstunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function SaveEvalutorCommentData(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            If isExist(mintCommenttranunkid, objDataOperation) Then
                If Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If InsertAuditTrails(objDataOperation, enAuditType.EDIT, mintCommenttranunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                If Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If InsertAuditTrails(objDataOperation, enAuditType.ADD, mintCommenttranunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            Dim objDocument As New clsScan_Attach_Documents

            If mdtAttachment IsNot Nothing AndAlso mdtAttachment.Rows.Count > 0 Then
                Dim dtTab As DataTable = Nothing
                Dim row As DataRow() = mdtAttachment.Select("form_name = '" & mstrFormName & "'")
                If row.Length > 0 Then
                    row.ToList.ForEach(Function(x) UpdateRowValue(x, mintCommenttranunkid, 0))
                    dtTab = row.CopyToDataTable()
                    objDocument._Datatable = dtTab
                    objDocument.InsertUpdateDelete_Documents(objDataOperation)
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            objDocument = Nothing


            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: SaveEvalutorCommentData; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function UpdateEvalutorCommentAttachmentData(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            Dim objDocument As New clsScan_Attach_Documents

            If mdtAttachment IsNot Nothing AndAlso mdtAttachment.Rows.Count > 0 Then
                Dim dtTab As DataTable = Nothing
                Dim row As DataRow() = mdtAttachment.Select("form_name = '" & mstrFormName & "'")
                If row.Length > 0 Then
                    row.ToList.ForEach(Function(x) UpdateRowValue(x, mintCommenttranunkid, 0))
                    dtTab = row.CopyToDataTable()
                    objDocument._Datatable = dtTab
                    objDocument.InsertUpdateDelete_Documents(objDataOperation)
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            objDocument = Nothing


            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateEvalutorCommentAttachmentData; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, _
                                      ByVal eAuditType As enAuditType, _
                                      ByVal intunkid As Integer, _
                                      Optional ByVal intPdpgoalsmstunkid As Integer = -1) As Boolean

        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO " & mstrDatabaseName & "..atpdpevaluator_comments ( " & _
                    "  tranguid " & _
                    ", commenttranunkid " & _
                    ", pdpgoalsmstunkid " & _
                    ", evaluatortypeid " & _
                    ", evaluatorunkid " & _
                    ", reviewermstunkid " & _
                    ", commentdatetime " & _
                    ", comments " & _
                    ", employeeunkid " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", loginemployeeunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb" & _
                    ") select " & _
                    "  LOWER(NEWID()) " & _
                    ", commenttranunkid " & _
                    ", pdpgoalsmstunkid " & _
                    ", evaluatortypeid " & _
                    ", evaluatorunkid " & _
                    ", reviewermstunkid " & _
                    ", commentdatetime " & _
                    ", comments " & _
                    ", employeeunkid " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", @loginemployeeunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb " & _
                    "  From " & mstrDatabaseName & "..pdpevaluator_comments WHERE 1=1  "

            objDataOperation.ClearParameters()

            If intunkid > 0 Then
                StrQ &= " and commenttranunkid=@commenttranunkid "
                objDataOperation.AddParameter("@commenttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intunkid)
            End If


            If intPdpgoalsmstunkid > 0 Then
                StrQ &= " and pdpgoalsmstunkid=@pdpgoalsmstunkid "
                objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPdpgoalsmstunkid)
            End If

            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@loginemployeeunkid ", SqlDbType.Int, eZeeDataType.INT_SIZE, mintloginemployeeunkid)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetActionPlanCommentData(ByVal xDatabaseName As String, _
                                     ByVal xUserUnkid As Integer, _
                                     ByVal xYearUnkid As Integer, _
                                     ByVal xCompanyUnkid As Integer, _
                                     ByVal xPeriodStart As DateTime, _
                                     ByVal xPeriodEnd As DateTime, _
                                     ByVal xUserModeSetting As String, _
                                     ByVal xOnlyApproved As Boolean, _
                                     ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                     ByVal xNoImageString As String, _
                                     Optional ByVal intActionPlanItemUnkId As Integer = -1, _
                                     Optional ByVal strListName As String = "List", _
                                     Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False, _
                                     Optional ByVal strFilterQuery As String = "", _
                                     Optional ByVal blnReinstatementDate As Boolean = False, _
                                     Optional ByVal blnIncludeAccessFilterQry As Boolean = True, _
                                     Optional ByVal blnAddApprovalCondition As Boolean = True, _
                                     Optional ByVal strTempTableSelectQry As String = "", _
                                     Optional ByVal strTempTableJoinQry As String = "", _
                                     Optional ByVal strTempTableDropQry As String = "") As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation

        Try

            Dim xDateJoinQry, xDataFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDataFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, xPeriodStart, xPeriodEnd, blnReinstatementDate, blnExcludeTermEmp_PayProcess, xDatabaseName)

            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , blnAddApprovalCondition)

            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Dim strDBName As String = ""
            Dim currDB As String = ""
            If System.Web.HttpContext.Current IsNot Nothing Then
                currDB = System.Web.HttpContext.Current.Session("Database_Name").ToString
            Else
                currDB = eZeeDatabase.current_database()
            End If
            If currDB <> xDatabaseName Then
                strDBName = " " & xDatabaseName & ".."
            End If

            strQ = strTempTableSelectQry

            strQ &= "SELECT " & _
                       "cfuser_master.username AS CommentBy " & _
                       ",cfuser_master.userunkid AS CommentById " & _
                       ",'' AS job_name " & _
                       ",'' AS department " & _
                       ",NULL AS empImage " & _
                       ",'' AS empBaseImage " & _
                       ",pdpevaluator_comments.pdpgoalsmstunkid " & _
                       ",pdpevaluator_comments.evaluatorunkid " & _
                       ",pdpevaluator_comments.reviewermstunkid " & _
                       ",pdpevaluator_comments.commentdatetime " & _
                       ",pdpevaluator_comments.comments " & _
                       ",pdpevaluator_comments.evaluatortypeid " & _
                       ",pdpevaluator_comments.commenttranunkid " & _
                    "FROM pdpevaluator_comments " & _
                    "LEFT JOIN pdpreviewer_master " & _
                         "ON pdpreviewer_master.reviewermstunkid = pdpevaluator_comments.reviewermstunkid " & _
                    "LEFT JOIN hrmsConfiguration..cfuser_master " & _
                         "ON cfuser_master.userunkid = pdpreviewer_master.mapuserunkid " & _
                    "WHERE 1 = 1 " & _
                    "AND pdpevaluator_comments.isvoid = 0 "

            If intActionPlanItemUnkId > 0 Then
            strQ &= " AND pdpevaluator_comments.pdpgoalsmstunkid = @pdpgoalsmstunkid "
            End If


            strQ &= "And pdpevaluator_comments.reviewermstunkid > 0 " & _
                    "UNION ALL " & _
                    "SELECT " & _
                     "CASE " & _
                          "WHEN evaluatortypeid = " & clspdpsettings_master.enPDPEvalutorTypeId.OTHER & " THEN CASE " & _
                                    "WHEN ISNULL(cmtuser.firstname, '') + ' ' + ISNULL(cmtuser.lastname, '') = '' THEN cmtuser.username " & _
                                    "ELSE ISNULL(cmtuser.firstname, '') + ' ' + ISNULL(cmtuser.lastname, '') " & _
                               "END " & _
                          "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') " & _
                     "END AS CommentBy " & _
                   ",hremployee_master.employeeunkid AS CommentById " & _
                   ",ISNULL(job.job_name,'') AS job_name " & _
                   ",ISNULL(dept.name,'') AS department " & _
                   ",CASE " & _
                          "WHEN evaluatortypeid =  " & clspdpsettings_master.enPDPEvalutorTypeId.OTHER & "  THEN " & _
                                    "userImage.photo " & _
                          "ELSE empImage.photo " & _
                   "END AS empImage " & _
                   ",'' AS empBaseImage " & _
                   ",pdpevaluator_comments.pdpgoalsmstunkid " & _
                   ",pdpevaluator_comments.evaluatorunkid " & _
                   ",pdpevaluator_comments.reviewermstunkid " & _
                   ",pdpevaluator_comments.commentdatetime " & _
                   ",pdpevaluator_comments.comments " & _
                   ",pdpevaluator_comments.evaluatortypeid " & _
                   ",pdpevaluator_comments.commenttranunkid " & _
                   " FROM " & strDBName & "..pdpevaluator_comments " & _
                   " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = pdpevaluator_comments.evaluatorunkid " & _
                   "LEFT JOIN hrmsConfiguration..cfuser_master AS cmtuser " & _
                   " ON cmtuser.userunkid = pdpevaluator_comments.evaluatorunkid " & _
                   " AND pdpevaluator_comments.evaluatortypeid = " & clspdpsettings_master.enPDPEvalutorTypeId.OTHER & " "

            strQ &= strTempTableJoinQry


            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         stationunkid " & _
                    "        ,deptgroupunkid " & _
                    "        ,departmentunkid " & _
                    "        ,sectiongroupunkid " & _
                    "        ,sectionunkid " & _
                    "        ,unitgroupunkid " & _
                    "        ,unitunkid " & _
                    "        ,teamunkid " & _
                    "        ,classgroupunkid " & _
                    "        ,classunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS T ON T.employeeunkid = hremployee_master.employeeunkid AND T.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobgroupunkid " & _
                    "        ,jobunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         cctranheadvalueid AS costcenterunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM " & strDBName & "hremployee_cctranhead_tran " & _
                    "    WHERE isvoid = 0 AND istransactionhead = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                    ") AS C ON C.employeeunkid = hremployee_master.employeeunkid AND C.Rno = 1 "




            '********************** DATA FOR DATES CONDITION ************************' --- START
            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If


            If blnIncludeAccessFilterQry = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If


            strQ &= "LEFT JOIN arutiimages..hremployee_images as empImage ON empImage.employeeunkid = hremployee_master.employeeunkid and empImage.isvoid = 0  " & _
                    "LEFT JOIN arutiimages..hremployee_images AS userImage ON userImage.employeeunkid = cmtuser.employeeunkid AND userImage.isvoid = 0 " & _
                    "LEFT JOIN (SELECT " & _
                              "job_name " & _
                            ",jobunkid " & _
                    "FROM hrjob_master) AS job " & _
                    "ON job.jobunkid = J.jobunkid " & _
                    "LEFT JOIN (SELECT " & _
                              "name " & _
                            ",departmentunkid " & _
                    "FROM hrdepartment_master) AS dept " & _
                    "ON dept.departmentunkid = T.departmentunkid "

            strQ &= " WHERE 1=1 AND pdpevaluator_comments.isvoid = 0 "


            If intActionPlanItemUnkId > 0 Then
                strQ &= " and pdpevaluator_comments.pdpgoalsmstunkid = @pdpgoalsmstunkid "
            End If


            If intActionPlanItemUnkId > 0 Then
                objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intActionPlanItemUnkId)
            End If


            If xIncludeIn_ActiveEmployee = False Then
                If xDataFilterQry.Trim.Length > 0 Then
                    strQ &= xDataFilterQry
                End If
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodStart))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            End If


            If strFilterQuery.Trim <> "" Then
                strQ &= " AND " & strFilterQuery & " "
            End If



            strQ &= strTempTableDropQry


            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dfView As DataView = dsList.Tables(0).DefaultView
            dfView.Sort = "commentdatetime desc"
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dfView.ToTable)
            dsList.Tables(0).AsEnumerable().Cast(Of DataRow).ToList().ForEach(Function(x) convertImageToBase64(x, xNoImageString))

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetTalentPoolDetailList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing
            exForce = Nothing
        End Try
    End Function

    Private Function convertImageToBase64(ByVal dr As DataRow, ByVal strNoimage As String) As Boolean
        Try
            If IsNothing(dr.Field(Of Byte())("empImage")) = False Then
                Dim bytes As Byte() = dr.Field(Of Byte())("empImage")
                Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                dr("empBaseImage") = "data:image/png;base64," & base64String
            Else
                dr("empBaseImage") = "data:image/png;base64," & strNoimage
            End If
            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeList", mstrModuleName)
        End Try
    End Function

    Private Function UpdateRowValue(ByVal dr As DataRow, ByVal intValue As Integer, ByVal intUserId As Integer, Optional ByVal strAUDValue As String = "") As Boolean
        Try
            dr("transactionunkid") = intValue
            dr("userunkid") = intUserId
            dr("filesize") = dr("filesize_kb")
            If strAUDValue.Trim.Length > 0 Then dr("AUD") = strAUDValue
            dr.AcceptChanges()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateRowValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
End Class