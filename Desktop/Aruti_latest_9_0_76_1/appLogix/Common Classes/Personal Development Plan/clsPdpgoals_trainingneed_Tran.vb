﻿Imports eZeeCommonLib
Public Class clsPdpgoals_trainingneed_Tran
    Private Const mstrModuleName = "clsPdpgoals_trainingneed_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintTrainingneedtranunkid As Integer
    Private mintPdpgoalsmstunkid As Integer
    Private mintTrainingcourseunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintUserunkid As Integer
    Private mintVoidloginemployeeunkid As Integer
    Private mblnIsFromCompetency As Boolean = False
    Private mblnIsRecommended As Boolean = False

    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mdtAuditDate As DateTime
    Private mintloginemployeeunkid As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Gajanan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingneedtranunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Trainingneedtranunkid() As Integer
        Get
            Return mintTrainingneedtranunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingneedtranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pdpgoalsmstunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Pdpgoalsmstunkid() As Integer
        Get
            Return mintPdpgoalsmstunkid
        End Get
        Set(ByVal value As Integer)
            mintPdpgoalsmstunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingcourseunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Trainingcourseunkid() As Integer
        Get
            Return mintTrainingcourseunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingcourseunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    Public Property _IsFromCompetency() As Boolean
        Get
            Return mblnIsFromCompetency
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromCompetency = value
        End Set
    End Property

    Public Property _IsRecommended() As Boolean
        Get
            Return mblnIsRecommended
        End Get
        Set(ByVal value As Boolean)
            mblnIsRecommended = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public Property _AuditDate() As DateTime
        Get
            Return mdtAuditDate
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintloginemployeeunkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property
#End Region

    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  trainingneedtranunkid " & _
              ", pdpgoalsmstunkid " & _
              ", trainingcourseunkid " & _
              ", employeeunkid " & _
              ", isvoid " & _
              ", isfromcompetency " & _
              ", isrecommended " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", userunkid " & _
              ", voidloginemployeeunkid " & _
             "FROM pdpgoals_trainingneed_tran " & _
             "WHERE trainingneedtranunkid = @trainingneedtranunkid "

            objDataOperation.AddParameter("@trainingneedtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedtranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTrainingneedtranunkid = CInt(dtRow.Item("trainingneedtranunkid"))
                mintPdpgoalsmstunkid = CInt(dtRow.Item("pdpgoalsmstunkid"))
                mintTrainingcourseunkid = CInt(dtRow.Item("trainingcourseunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mblnIsFromCompetency = CBool(dtRow.Item("isfromcompetency"))
                mblnIsRecommended = CBool(dtRow.Item("isrecommended"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtVoiddatetime = dtRow.Item("voiddatetime")
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintVoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  trainingneedtranunkid " & _
              ", pdpgoalsmstunkid " & _
              ", trainingcourseunkid " & _
              ", employeeunkid " & _
              ", isvoid " & _
              ", isrecommended " & _
              ", isfromcompetency " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", userunkid " & _
              ", voidloginemployeeunkid " & _
             "FROM pdpgoals_trainingneed_tran "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function isExist(Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT " & _
              "  trainingneedtranunkid " & _
              ", pdpgoalsmstunkid " & _
              ", trainingcourseunkid " & _
              ", employeeunkid " & _
              ", isvoid " & _
             "FROM pdpgoals_trainingneed_tran " & _
             "WHERE pdpgoalsmstunkid = @pdpgoalsmstunkid and employeeunkid=@employeeunkid and trainingcourseunkid=@trainingcourseunkid and isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND trainingneedtranunkid = @trainingneedtranunkid"
                objDataOperation.AddParameter("@trainingneedtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            objDataOperation.AddParameter("@trainingcourseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingcourseunkid)
            objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpgoalsmstunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpgoalsmstunkid.ToString)
            objDataOperation.AddParameter("@trainingcourseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingcourseunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@isfromcompetency", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromCompetency.ToString)
            objDataOperation.AddParameter("@isrecommended", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsRecommended.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

            strQ = "INSERT INTO pdpgoals_trainingneed_tran ( " & _
              "  pdpgoalsmstunkid " & _
              ", trainingcourseunkid " & _
              ", employeeunkid " & _
              ", isvoid " & _
              ", isfromcompetency " & _
              ", isrecommended " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", userunkid " & _
              ", voidloginemployeeunkid" & _
            ") VALUES (" & _
              "  @pdpgoalsmstunkid " & _
              ", @trainingcourseunkid " & _
              ", @employeeunkid " & _
              ", @isvoid " & _
              ", @isfromcompetency " & _
              ", @isrecommended " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @userunkid " & _
              ", @voidloginemployeeunkid" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTrainingneedtranunkid = dsList.Tables(0).Rows(0).Item(0)

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@trainingneedtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingneedtranunkid.ToString)
            objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPdpgoalsmstunkid.ToString)
            objDataOperation.AddParameter("@trainingcourseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingcourseunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@isfromcompetency", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromCompetency.ToString)
            objDataOperation.AddParameter("@isrecommended", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsRecommended.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

            strQ = "UPDATE pdpgoals_trainingneed_tran SET " & _
              "  pdpgoalsmstunkid = @pdpgoalsmstunkid" & _
              ", trainingcourseunkid = @trainingcourseunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", isvoid = @isvoid" & _
              ", isfromcompetency = @isfromcompetency" & _
              ", isrecommended = @isrecommended" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason" & _
              ", userunkid = @userunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
            "WHERE trainingneedtranunkid = @trainingneedtranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function Delete(ByVal intTrainingneedtranunkid As Integer, ByVal intTrainingcourseunkid As Integer, Optional ByVal intPdpgoalsmstunkid As Integer = 0, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim objDepartmentaltrainingneed_master As New clsDepartmentaltrainingneed_master 

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE pdpgoals_trainingneed_tran SET " & _
              " isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE 1=1 and trainingneedtranunkid = @trainingneedtranunkid "

            objDataOperation.AddParameter("@trainingneedtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingneedtranunkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, intTrainingneedtranunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Gajanan [14-Apr-2021] -- Start

            dsList = objDepartmentaltrainingneed_master.GetTrainingNeedListForOtherModule(intPdpgoalsmstunkid, intTrainingcourseunkid, CInt(enModuleReference.PDP), -1, -1, False, objDataOperation)
            If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For Each drrow As DataRow In dsList.Tables(0).Rows
                    objDepartmentaltrainingneed_master._Isvoid = True
                    objDepartmentaltrainingneed_master._Voiduserunkid = mintVoiduserunkid
                    objDepartmentaltrainingneed_master._Voidloginemployeeunkid = mintVoidloginemployeeunkid
                    objDepartmentaltrainingneed_master._Voiddatetime = mdtVoiddatetime
                    objDepartmentaltrainingneed_master._Voidreason = mstrVoidreason

                    objDepartmentaltrainingneed_master._AuditUserId = mintAuditUserId
                    objDepartmentaltrainingneed_master._AuditDate = mdtAuditDate
                    objDepartmentaltrainingneed_master._Loginemployeeunkid = mintloginemployeeunkid
                    objDepartmentaltrainingneed_master._HostName = mstrHostName
                    objDepartmentaltrainingneed_master._ClientIP = mstrClientIP
                    objDepartmentaltrainingneed_master._Isweb = mblnIsWeb
                    objDepartmentaltrainingneed_master._FormName = mstrFormName

                    If objDepartmentaltrainingneed_master.Void(CInt(drrow("departmentaltrainingneedunkid")), objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If
            'Gajanan [14-Apr-2021] -- End

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function DeleteAll(ByVal intPdpgoalsmstunkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPdpgoalsmstunkid)

            strQ = "UPDATE pdpgoals_trainingneed_tran SET " & _
              " isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE pdpgoalsmstunkid = @pdpgoalsmstunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, -1, intPdpgoalsmstunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: DeleteAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function SaveTrainingData(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            If isExist(mintTrainingneedtranunkid, objDataOperation) Then
                If Update(xDataOpr) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If InsertAuditTrails(objDataOperation, enAuditType.EDIT, mintTrainingneedtranunkid, -1) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                If Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If InsertAuditTrails(objDataOperation, enAuditType.ADD, mintTrainingneedtranunkid, -1) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If



            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: SaveTrainingData; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, _
                                      ByVal eAuditType As enAuditType, _
                                      ByVal intunkid As Integer, _
                                     Optional ByVal intPdpgoalsmstunkid As Integer = -1) As Boolean

        Dim StrQ As String = ""
        Try

            StrQ = "INSERT INTO " & mstrDatabaseName & "..atpdpgoals_trainingneed_tran ( " & _
                    "  tranguid " & _
                    ", trainingneedtranunkid " & _
                    ", pdpgoalsmstunkid " & _
                    ", trainingcourseunkid " & _
                    ", employeeunkid " & _
                    ", isfromcompetency " & _
                    ", isrecommended " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", loginemployeeunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb" & _
                    ") select " & _
                    "  LOWER(NEWID()) " & _
                    ", trainingneedtranunkid " & _
                    ", pdpgoalsmstunkid " & _
                    ", trainingcourseunkid " & _
                    ", employeeunkid " & _
                    ", isfromcompetency " & _
                    ", isrecommended " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", @loginemployeeunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb " & _
                    "  From " & mstrDatabaseName & "..pdpgoals_trainingneed_tran WHERE 1=1  "

            objDataOperation.ClearParameters()


            If intunkid > 0 Then
                StrQ &= " and trainingneedtranunkid=@trainingneedtranunkid "
                objDataOperation.AddParameter("@trainingneedtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intunkid)
            End If

            If intPdpgoalsmstunkid > 0 Then
                StrQ &= " and pdpgoalsmstunkid=@pdpgoalsmstunkid "
                objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPdpgoalsmstunkid)
            End If

            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@loginemployeeunkid ", SqlDbType.Int, eZeeDataType.INT_SIZE, mintloginemployeeunkid)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetActionPlanTraining(ByVal strTableName As String, ByVal intpdpGoalUnkid As Integer, ByVal intEmployeeUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT " & _
                   " pdpgoals_trainingneed_tran.* " & _
                   ",cfcommon_master.name " & _
                   ",trdepartmentaltrainingneed_master.statusunkid " & _
                   ",CASE " & _
                          "WHEN trdepartmentaltrainingneed_master.statusunkid = " & clsDepartmentaltrainingneed_master.enApprovalStatus.Pending & " THEN @Pending " & _
                          "WHEN trdepartmentaltrainingneed_master.statusunkid = " & clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromDeptTrainingNeed & " THEN @SubmittedForApprovalFromDeptTrainingNeed " & _
                          "WHEN trdepartmentaltrainingneed_master.statusunkid = " & clsDepartmentaltrainingneed_master.enApprovalStatus.TentativeApproved & " THEN @TentativeApproved " & _
                          "WHEN trdepartmentaltrainingneed_master.statusunkid = " & clsDepartmentaltrainingneed_master.enApprovalStatus.SubmittedForApprovalFromTrainingBacklog & " THEN @SubmittedForApprovalFromTrainingBacklog " & _
                          "WHEN trdepartmentaltrainingneed_master.statusunkid = " & clsDepartmentaltrainingneed_master.enApprovalStatus.AskedForReviewAsPerAmountSet & " THEN @AskedForReviewAsPerAmountSet " & _
                          "WHEN trdepartmentaltrainingneed_master.statusunkid = " & clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved & " THEN @FinalApproved " & _
                          "WHEN trdepartmentaltrainingneed_master.statusunkid = " & clsDepartmentaltrainingneed_master.enApprovalStatus.Rejected & " THEN @Reject " & _
                     "END AS StatusName " & _
                     ",pdpgoals_trainingneed_tran.isrecommended " & _
                    "FROM pdpgoals_trainingneed_tran " & _
                "LEFT JOIN cfcommon_master " & _
                     "ON cfcommon_master.masterunkid = pdpgoals_trainingneed_tran.trainingcourseunkid " & _
                    "and mastertype = " & CInt(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER) & " " & _
                "LEFT JOIN trdepartmentaltrainingneed_master " & _
                     "ON trdepartmentaltrainingneed_master.moduletranunkid = pdpgoals_trainingneed_tran.pdpgoalsmstunkid " & _
                          "AND trdepartmentaltrainingneed_master.trainingcourseunkid = pdpgoals_trainingneed_tran.trainingcourseunkid " & _
                          "AND trdepartmentaltrainingneed_master.isvoid = 0 " & _
                          "AND trdepartmentaltrainingneed_master.moduleid IN (" & CInt(enModuleReference.PDP) & ", " & CInt(enModuleReference.Succession) & ", " & CInt(enModuleReference.Talent) & ") " & _
                    "WHERE pdpgoals_trainingneed_tran.pdpgoalsmstunkid = @pdpgoalsmstunkid " & _
                "AND pdpgoals_trainingneed_tran.employeeunkid = @employeeunkid " & _
                "AND pdpgoals_trainingneed_tran.isvoid = 0 "


            objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intpdpGoalUnkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkid)

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsDepartmentaltrainingneed_master", 12, "Pending"))
            objDataOperation.AddParameter("@SubmittedForApprovalFromDeptTrainingNeed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsDepartmentaltrainingneed_master", 13, "Submitted For Approval From Departmental Training Need"))
            objDataOperation.AddParameter("@TentativeApproved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsDepartmentaltrainingneed_master", 14, "Tentative Approved"))
            objDataOperation.AddParameter("@SubmittedForApprovalFromTrainingBacklog", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsDepartmentaltrainingneed_master", 15, "Submitted For Approval From Training Backlog"))
            objDataOperation.AddParameter("@FinalApproved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsDepartmentaltrainingneed_master", 16, "Final Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsDepartmentaltrainingneed_master", 17, "Rejected"))
            objDataOperation.AddParameter("@AskedForReviewAsPerAmountSet", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsDepartmentaltrainingneed_master", 18, "Asked For Review As Per Amount Set"))


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetActionPlanTraining; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetRecommendedTraining(ByVal strTableName As String, ByVal intpdpFormUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            strQ &= "IF OBJECT_ID('tempdb..#competencyList') IS NOT NULL " & _
                   "DROP TABLE #competencyList " & _
                         "CREATE TABLE #competencyList ( " & _
                           "itemtranunkid int " & _
                           ",fieldvalue  NVARCHAR(MAX) " & _
                           ",CompetencyValue int " & _
                         "); " & _
                   "INSERT INTO #competencyList " & _
                        "SELECT " & _
                             "itemtranunkid " & _
                           ",ISNULL(hrassess_competencies_master.name, '') AS fieldvalue " & _
                           ",ISNULL(pdpitemdatatran.fieldvalue, 0) AS CompetencyValue " & _
                        "FROM pdpitemdatatran " & _
                        "LEFT JOIN hrassess_competencies_master " & _
                             "ON CONVERT(NVARCHAR(MAX), pdpitemdatatran.fieldvalue) = hrassess_competencies_master.competenciesunkid " & _
                        "WHERE isvoid = 0 " & _
                        "AND iscompetencyselection = 1 and pdpitemdatatran.pdpformunkid = @pdpformunkid "

            strQ &= "SELECT " & _
                   " pdpgoals_trainingneed_tran.trainingneedtranunkid " & _
                   ",cfcommon_master.name " & _
                   ",CASE " & _
                         "WHEN ISNULL(pdpgoals_master.itemunkid,0) > 0 THEN CASE " & _
                         "WHEN ISNULL(pdpitemdatatran.iscompetencyselection, 0) > 0 THEN ISNULL(#competencyList.fieldvalue, '') " & _
                         "ELSE pdpitemdatatran.fieldvalue " & _
                   "END " & _
                         "ELSE pdpgoals_master.goal_name " & _
                   "END AS goal_name " & _
                   ",pdpgoals_master.goal_description " & _
                   ",0 as isgrp " & _
                   ",pdpgoals_master.pdpgoalsmstunkid " & _
                   ",pdpgoals_trainingneed_tran.trainingcourseunkid " & _
                   ",pdpgoals_trainingneed_tran.isfromcompetency " & _
                   ",ISNULL(pdpgoals_master.itemunkid,0) as itemunkid " & _
                   "FROM pdpgoals_trainingneed_tran " & _
                   "LEFT JOIN pdpgoals_master " & _
                       "ON pdpgoals_master.pdpgoalsmstunkid = pdpgoals_trainingneed_tran.pdpgoalsmstunkid " & _
                   "LEFT JOIN pdpitemdatatran " & _
                       "ON pdpitemdatatran.itemtranunkid = pdpgoals_master.itemunkid and pdpitemdatatran.isvoid = 0 " & _
                   "LEFT JOIN cfcommon_master " & _
                       "ON cfcommon_master.masterunkid = pdpgoals_trainingneed_tran.trainingcourseunkid " & _
                       "and mastertype = " & CInt(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER) & " " & _
                   "LEFT JOIN #competencyList " & _
                       "ON pdpitemdatatran.itemtranunkid = #competencyList.itemtranunkid " & _
                   "WHERE pdpgoals_trainingneed_tran.isvoid = 0 " & _
                   "AND pdpgoals_trainingneed_tran.isrecommended = 0 and pdpgoals_master.pdpformunkid = @pdpformunkid "

            objDataOperation.AddParameter("@pdpformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intpdpFormUnkid)


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetActionPlanTraining; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function UpdateTrainingRecommendedStatus(ByVal intTrainingneedtranunkid As Integer, ByVal blnIsrecommended As Boolean, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE pdpgoals_trainingneed_tran SET " & _
              " isrecommended = @isrecommended " & _
              " WHERE trainingneedtranunkid = @trainingneedtranunkid "

            objDataOperation.AddParameter("@isrecommended", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsrecommended)
            objDataOperation.AddParameter("@trainingneedtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTrainingneedtranunkid)

            Call objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT, intTrainingneedtranunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdatePDPFormStatus; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function


    

    Public Function GetDistinctTraining(ByVal strTableName As String, ByVal intpdpGoalUnkid As Integer, Optional ByVal blnAddSelect As Boolean = False, Optional ByVal xDataOpr As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            If blnAddSelect = True Then
                strQ &= "SELECT  0 AS masterunkid, ' ' + @select AS name UNION "
                objDataOperation.AddParameter("@select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            End If

            strQ &= "SELECT  DISTINCT cfcommon_master.masterunkid, cfcommon_master.name " & _
                    "FROM pdpgoals_trainingneed_tran " & _
                    "LEFT join cfcommon_master on cfcommon_master.masterunkid = pdpgoals_trainingneed_tran.trainingcourseunkid " & _
                    "WHERE pdpgoals_trainingneed_tran.pdpgoalsmstunkid = @pdpgoalsmstunkid " & _
                    "AND pdpgoals_trainingneed_tran.isvoid = 0 " & _
                    "AND cfcommon_master.mastertype = " & CInt(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER) & " "


            objDataOperation.AddParameter("@pdpgoalsmstunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intpdpGoalUnkid)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDistinctTraining; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

End Class