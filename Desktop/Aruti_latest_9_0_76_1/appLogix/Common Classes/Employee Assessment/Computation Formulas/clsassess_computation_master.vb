﻿'************************************************************************************************************************************
'Class Name : clsassess_computation_tran.vb
'Purpose    :
'Date       :02-Jan-2015
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_computation_master
    Private Shared ReadOnly mstrModuleName As String = "clsassess_computation_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintComputationunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintFormula_Typeid As Integer
    Private mstrComputation_Formula As String = String.Empty
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mblnIsNormalized As Boolean = False

    'Shani (24-May-2016) -- Start
    Private mdtComputeValue As DataTable = Nothing
    'Shani (24-May-2016) -- End


#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set computationunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Computationunkid() As Integer
        Get
            Return mintComputationunkid
        End Get
        Set(ByVal value As Integer)
            mintComputationunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formula_typeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Formula_Typeid() As Integer
        Get
            Return mintFormula_Typeid
        End Get
        Set(ByVal value As Integer)
            mintFormula_Typeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set computation_formula
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Computation_Formula() As String
        Get
            Return mstrComputation_Formula
        End Get
        Set(ByVal value As String)
            mstrComputation_Formula = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  computationunkid " & _
              ", periodunkid " & _
              ", formula_typeid " & _
              ", computation_formula " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrassess_computation_master " & _
             "WHERE computationunkid = @computationunkid "

            objDataOperation.AddParameter("@computationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintComputationunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintComputationunkid = CInt(dtRow.Item("computationunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintFormula_Typeid = CInt(dtRow.Item("formula_typeid"))
                mstrComputation_Formula = dtRow.Item("computation_formula").ToString
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intPeriodId As Integer = 0) As DataSet 'S.SANDEEP [17 APR 2015] -- START {intPeriodId} -- END
        'Public Function GetList(ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "     CM.computationunkid " & _
                   "    ,CM.periodunkid " & _
                   "    ,CM.formula_typeid " & _
                   "    ,CM.computation_formula " & _
                   "    ,CM.isvoid " & _
                   "    ,CM.voiduserunkid " & _
                   "    ,CM.voiddatetime " & _
                   "    ,CM.voidreason " & _
                   "    ,cfcommon_period_tran.period_name AS PName " & _
                   "    ,cfcommon_period_tran.statusid AS PStatId " & _
                   "    ,cfcommon_period_tran.start_date " & _
                   "    ,'' AS Formula_Defined " & _
                   "    ,CASE WHEN formula_typeid = " & enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE & " THEN @BSC_EMP_TOTAL_SCORE " & _
                   "          WHEN formula_typeid = " & enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE & " THEN @BSC_ASR_TOTAL_SCORE " & _
                   "          WHEN formula_typeid = " & enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE & " THEN @BSC_REV_TOTAL_SCORE " & _
                   "          WHEN formula_typeid = " & enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE & " THEN @CMP_EMP_TOTAL_SCORE " & _
                   "          WHEN formula_typeid = " & enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE & " THEN @CMP_ASR_TOTAL_SCORE " & _
                   "          WHEN formula_typeid = " & enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE & " THEN @CMP_REV_TOTAL_SCORE " & _
                   "          WHEN formula_typeid = " & enAssess_Computation_Formulas.EMP_OVERALL_SCORE & " THEN @EMP_OVERALL_SCORE " & _
                   "          WHEN formula_typeid = " & enAssess_Computation_Formulas.ASR_OVERALL_SCORE & " THEN @ASR_OVERALL_SCORE " & _
                   "          WHEN formula_typeid = " & enAssess_Computation_Formulas.REV_OVERALL_SCORE & " THEN @REV_OVERALL_SCORE " & _
                   "          WHEN formula_typeid = " & enAssess_Computation_Formulas.POST_TO_PAYROLL_VALUE & " THEN @POST_TO_PAYROLL_VALUE " & _
                   "          WHEN formula_typeid = " & enAssess_Computation_Formulas.FINAL_RESULT_SCORE & " THEN @FINAL_RESULT_SCORE " & _
                   "          WHEN formula_typeid = " & enAssess_Computation_Formulas.AVG_FINAL_RESULT_SCORE & " THEN @AVG_FINAL_RESULT_SCORE " & _
                   "     END AS Formula_Type " & _
                   "FROM hrassess_computation_master AS CM " & _
                   "    JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = CM.periodunkid " & _
                   "WHERE CM.isvoid = 0 "
            'S.SANDEEP [29-NOV-2017] -- START {REF-ID # 40} <AVG_FINAL_RESULT_SCORE> -- END

            'S.SANDEEP [04 JUN 2015] -- START
            '"    ,cfcommon_period_tran.start_date " & _
            'S.SANDEEP [04 JUN 2015] -- END

            'S.SANDEEP [17 APR 2015] -- START
            If intPeriodId > 0 Then
                strQ &= " AND CM.periodunkid = '" & intPeriodId & "' "
            End If
            'S.SANDEEP [17 APR 2015] -- END

            objDataOperation.AddParameter("@BSC_EMP_TOTAL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 616, "BSC Employee Total Score"))
            objDataOperation.AddParameter("@BSC_ASR_TOTAL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 617, "BSC Assessor Total Score"))
            objDataOperation.AddParameter("@BSC_REV_TOTAL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 618, "BSC Reviewer Total Score"))
            objDataOperation.AddParameter("@CMP_EMP_TOTAL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 619, "Competence Employee Total Score"))
            objDataOperation.AddParameter("@CMP_ASR_TOTAL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 620, "Competence Assessor Total Score"))
            objDataOperation.AddParameter("@CMP_REV_TOTAL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 621, "Competence Reviewer Total Score"))
            objDataOperation.AddParameter("@EMP_OVERALL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 622, "Employee Overall Score"))
            objDataOperation.AddParameter("@ASR_OVERALL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 623, "Assessor Overall Score"))
            objDataOperation.AddParameter("@REV_OVERALL_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 624, "Reviewer Overall Score"))
            objDataOperation.AddParameter("@POST_TO_PAYROLL_VALUE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 625, "Post To Payroll Value"))
            'S.SANDEEP [19 FEB 2015] -- START
            objDataOperation.AddParameter("@FINAL_RESULT_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 636, "Final Result Score"))
            'S.SANDEEP [19 FEB 2015] -- END

            'S.SANDEEP [29-NOV-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 40
            objDataOperation.AddParameter("@AVG_FINAL_RESULT_SCORE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 821, "Avg. Of Final Score"))
            'S.SANDEEP [29-NOV-2017] -- END


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [19 FEB 2015] -- START
            'If dsList.Tables(0).Rows.Count Then
            '    Dim objCMaster As New clsMasterData
            '    Dim dsComp As New DataSet
            '    dsComp = objCMaster.getComboListForAssessmentComputationVariables("List")
            '    objCMaster = Nothing
            '    For Each xRow As DataRow In dsList.Tables(0).Rows
            '        Dim xArr() As String = xRow.Item("computation_formula").ToString.Split("#")
            '        If xArr.Length > 0 Then
            '            For idx As Integer = 0 To xArr.Length - 1
            '                If IsNumeric(xArr(idx)) Then
            '                    Dim xtmp() As DataRow = dsComp.Tables(0).Select("Id = '" & xArr(idx).ToString & "'")
            '                    If xtmp.Length > 0 Then xRow.Item("Formula_Defined") &= xtmp(0).Item("Name")
            '                Else
            '                    xRow.Item("Formula_Defined") &= xArr(idx).ToString
            '                End If
            '            Next
            '        End If
            '    Next
            'End If
            If dsList.Tables(0).Rows.Count Then
                Dim xStrFormulaString, xTemp, xOperand, xStrExpression As String
                Dim dsComp As New DataSet
                Dim dsFrml As New DataSet
                Dim objCMaster As New clsMasterData
                dsComp = objCMaster.getComboListForAssessmentComputationVariables("List")
                dsFrml = objCMaster.getComboListForAssessmentComputationFunction("List")
                objCMaster = Nothing
                Dim objCmptTran As New clsassess_computation_tran

                For Each xRow As DataRow In dsList.Tables(0).Rows
                    xStrFormulaString = xRow.Item("computation_formula")
                    xTemp = "" : xOperand = "" : xStrExpression = ""

                    For i As Integer = 1 To xStrFormulaString.Length

                        If Mid(xStrFormulaString, i, 1) = "+" OrElse _
                           Mid(xStrFormulaString, i, 1) = "-" OrElse _
                           Mid(xStrFormulaString, i, 1) = "*" OrElse _
                           Mid(xStrFormulaString, i, 1) = "/" OrElse _
                           Mid(xStrFormulaString, i, 1) = "(" OrElse _
                           Mid(xStrFormulaString, i, 1) = ")" Then

                            If xTemp.Trim.Length > 0 Then
                                xStrExpression += xOperand & xTemp
                            End If
                            xTemp = Trim(Mid(xStrFormulaString, i, 1))

                            If xTemp = "+" Then
                                If xOperand.Trim.Length > 0 Then
                                    xOperand += "("
                                End If
                                xOperand += xTemp
                                xStrExpression += xOperand
                                xTemp = "" : xOperand = ""

                            ElseIf xTemp = "-" Then
                                If xOperand.Trim.Length > 0 Then
                                    xOperand += "("
                                End If
                                xOperand += xTemp
                                xStrExpression += xOperand
                                xTemp = "" : xOperand = ""

                            ElseIf xTemp = "*" Then
                                If xOperand.Trim.Length > 0 Then
                                    xOperand += "("
                                End If
                                xOperand += xTemp
                                xStrExpression += xOperand
                                xTemp = "" : xOperand = ""

                            ElseIf xTemp = "/" Then
                                If xOperand.Trim.Length > 0 Then
                                    xOperand += "("
                                End If
                                xOperand += xTemp
                                xStrExpression += xOperand
                                xTemp = "" : xOperand = ""

                            ElseIf xTemp = "(" Then
                                xOperand = xTemp
                                xStrExpression += xOperand
                                xTemp = "" : xOperand = ""

                            ElseIf xTemp = ")" Then
                                xOperand += xTemp
                                xStrExpression += xOperand
                                xTemp = "" : xOperand = ""

                            Else
                                xOperand += xOperand
                                xStrExpression += xOperand
                                xTemp = "" : xOperand = ""

                            End If
                        ElseIf Mid(xStrFormulaString, i, 1) <> "#" Then
                            xTemp += Trim(Mid(xStrFormulaString, i, 1))
                        Else
                            Dim xFrmlId As Integer = -1
                            If xTemp <> "" Then
                                xFrmlId = objCmptTran.PredefineFormula(CInt(xTemp))
                                Dim xtmp() As DataRow = Nothing
                                'S.SANDEEP [27-NOV-2017] -- START
                                If xFrmlId <= 0 AndAlso xTemp.StartsWith("1000") Then xFrmlId = enAssess_Functions_Mode.PRE_DEF_FORMULA
                                'S.SANDEEP [27-NOV-2017] -- END
                                If xFrmlId > 0 Then
                                    xtmp = dsFrml.Tables(0).Select("Id = '" & xTemp.ToString.Substring(4) & "'")
                                Else
                                    xtmp = dsComp.Tables(0).Select("Id = '" & xTemp.ToString & "'")
                                End If
                                If xtmp.Length > 0 Then xStrExpression &= xtmp(0).Item("Name").ToString
                                xOperand = "" : xTemp = ""
                            End If
                        End If
                    Next

                    If xTemp.Trim.Length > 0 Then
                        xStrExpression += xOperand & xTemp
                    End If

                    xRow.Item("Formula_Defined") &= xStrExpression
                    xStrExpression = ""
                Next
            End If
            'S.SANDEEP [19 FEB 2015] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_computation_master) </purpose>
    Public Function Insert(ByVal dtFormula As DataTable, ByVal intUserUnkid As Integer) As Boolean 'S.SANDEEP [04 JUN 2015] -- START -- END
        'Public Function Insert(ByVal dtFormula As DataTable) As Boolean
        If isExist(mintPeriodunkid, mintFormula_Typeid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, formula is already defined for the selected period. Please define new formula.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@formula_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormula_Typeid.ToString)
            objDataOperation.AddParameter("@computation_formula", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrComputation_Formula.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO hrassess_computation_master ( " & _
                       "  periodunkid " & _
                       ", formula_typeid " & _
                       ", computation_formula " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason" & _
                   ") VALUES (" & _
                       "  @periodunkid " & _
                       ", @formula_typeid " & _
                       ", @computation_formula " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason" & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintComputationunkid = dsList.Tables(0).Rows(0).Item(0)

            If dtFormula IsNot Nothing Then
                Dim objCTran As New clsassess_computation_tran
                objCTran._DataOperation = objDataOperation
                objCTran._Computationunkid = mintComputationunkid
                objCTran._DataTable = dtFormula.Copy

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If objCTran.InsertUpdateDelete_Computation_Tran(intUserUnkid) = False Then
                    'If objCTran.InsertUpdateDelete_Computation_Tran(User._Object._Userunkid) = False Then
                    'S.SANDEEP [04 JUN 2015] -- END
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objCTran = Nothing
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_computation_master) </purpose>
    Public Function Update(ByVal dtFormula As DataTable, ByVal intUserUnkid As Integer) As Boolean 'S.SANDEEP [04 JUN 2015] -- START -- END
        'Public Function Update(ByVal dtFormula As DataTable) As Boolean

        If isExist(mintPeriodunkid, mintFormula_Typeid, mintComputationunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, formula is already defined for the selected period. Please define new formula.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@computationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintComputationunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@formula_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormula_Typeid.ToString)
            objDataOperation.AddParameter("@computation_formula", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrComputation_Formula.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE hrassess_computation_master SET " & _
                   "  periodunkid = @periodunkid" & _
                   ", formula_typeid = @formula_typeid" & _
                   ", computation_formula = @computation_formula" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE computationunkid = @computationunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dtFormula IsNot Nothing Then
                Dim objCTran As New clsassess_computation_tran
                objCTran._DataOperation = objDataOperation
                objCTran._Computationunkid = mintComputationunkid
                objCTran._DataTable = dtFormula.Copy

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                If objCTran.InsertUpdateDelete_Computation_Tran(intUserUnkid) = False Then
                    'If objCTran.InsertUpdateDelete_Computation_Tran(User._Object._Userunkid) = False Then
                    'S.SANDEEP [04 JUN 2015] -- END
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objCTran = Nothing
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_computation_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal intUserUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrassess_computation_master SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE computationunkid = @computationunkid "

            objDataOperation.AddParameter("@computationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList = clsCommonATLog.GetChildList(objDataOperation, "hrassess_computation_tran", "computationunkid", intUnkid)
            For Each xRow As DataRow In dsList.Tables(0).Rows
                objDataOperation.ClearParameters()
                strQ = "UPDATE hrassess_computation_tran SET " & _
                       "  isvoid = @isvoid" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voiddatetime = @voiddatetime" & _
                       ", voidreason = @voidreason " & _
                       "WHERE computationtranunkid = '" & xRow.Item("computationtranunkid") & "'"

                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_computation_master", "computationunkid", intUnkid, "hrassess_computation_tran", "computationtranunkid", xRow.Item("computationtranunkid"), 3, 3, , intUserUnkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal xComputationId As Integer, ByVal xFormulaTypeId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'strQ = "SELECT 1 FROM hrgoals_analysis_tran WHERE formula_used = @Formula AND isvoid = 0 UNION " & _
            '       "SELECT 1 FROM hrcompetency_analysis_tran WHERE formula_used = @Formula AND isvoid = 0 "

            'objDataOperation.AddParameter("@Formula", SqlDbType.NVarChar, xFormula.Length, xFormula)

            'dsList = objDataOperation.ExecQuery(strQ, "List")

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'Return dsList.Tables(0).Rows.Count > 0

            'Shani (24-May-2016) -- Start
            'Return False
            If xFormulaTypeId = enAssess_Computation_Formulas.FINAL_RESULT_SCORE Then
                strQ = "SELECT 1 FROM hrassess_compute_score_master WHERE isvoid = 0 "
            Else
                strQ = "SELECT 1 FROM hrassess_compute_score_tran WHERE isvoid = 0 AND computationunkid = '" & xComputationId & "' "
            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
            'Shani (24-May-2016) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iPeriodunkid As Integer, ByVal iFormula_Typeid As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  computationunkid " & _
                   ", periodunkid " & _
                   ", formula_typeid " & _
                   ", computation_formula " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", voidreason " & _
                   "FROM hrassess_computation_master " & _
                   "WHERE periodunkid = @periodunkid " & _
                   "AND formula_typeid = @formula_typeid " & _
                   "AND isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND computationunkid <> @computationunkid"
            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, iPeriodunkid)
            objDataOperation.AddParameter("@formula_typeid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, iFormula_Typeid)
            objDataOperation.AddParameter("@computationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function Process_Assessment_Formula(ByVal iComputationFormula As Integer, _
    '                                           ByVal iPeriodId As Integer, _
    '                                           ByVal iItemUnkid As Integer, _
    '                                           ByVal iEmployeeId As Integer, _
    '                                           ByVal iScoringOptionId As Integer, _
    '                                           ByVal iScore As Decimal, _
    '                                           ByVal iAMode As enAssessmentMode, _
    '                                           ByRef iFormulaString As String, _
    '                                           Optional ByVal iAssessGrpId As Integer = 0, _
    '                                           Optional ByVal iDataTab As DataTable = Nothing) As Decimal
    Public Function Process_Assessment_Formula(ByVal iComputationFormula As Integer, _
                                               ByVal iPeriodId As Integer, _
                                               ByVal iItemUnkid As Integer, _
                                               ByVal iEmployeeId As Integer, _
                                               ByVal iScoringOptionId As Integer, _
                                               ByVal iScore As Decimal, _
                                               ByVal iAMode As enAssessmentMode, _
                                               ByVal iEmployeeAsOnDate As DateTime, _
                                               ByVal xIsUsedAgreedScore As Boolean, _
                                               ByRef iFormulaString As String, _
                                               Optional ByVal iAssessGrpId As Integer = 0, _
                                               Optional ByVal iDataTab As DataTable = Nothing, _
                                               Optional ByVal blnIsSelfAssignCompetencies As Boolean = False) As Decimal

        'Shani (23-Nov-2016) -- [ByVal xIsUsedAgreedScore As Boolean, _]
        'S.SANDEEP [08 Jan 2016] -- START {blnIsSelfAssignCompetencies} -- END

        'S.SANDEEP [04 JUN 2015] -- END

        Dim xScoreValue As Double = 0
        Dim StrQ As String = String.Empty
        Try
            Dim xExpression As String = String.Empty
            Dim xFormulaString As String = String.Empty

            Using objDo As New clsDataOperation
                StrQ = "SELECT " & _
                       "    @formula = computation_formula " & _
                       "FROM hrassess_computation_master " & _
                       "WHERE periodunkid = '" & iPeriodId & "' AND formula_typeid = '" & iComputationFormula & "' AND isvoid = 0"

                objDo.AddParameter("@formula", SqlDbType.NVarChar, 4000, xFormulaString, ParameterDirection.InputOutput)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                xFormulaString = objDo.GetParameterValue("@formula")
                iFormulaString = xFormulaString
            End Using

            If xFormulaString.Trim.Length > 0 Then

                'S.SANDEEP [17 APR 2015] -- START
                mblnIsNormalized = False
                'S.SANDEEP [17 APR 2015] -- END


                While mblnIsNormalized = False
                    xFormulaString = Generate_Nested_Expression(xFormulaString, iPeriodId)
                End While

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'xExpression = Get_Formula_Expression(xFormulaString, iPeriodId, iItemUnkid, iEmployeeId, iScoringOptionId, iScore, iAMode, iAssessGrpId, iDataTab)

                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                'xExpression = Get_Formula_Expression(xFormulaString, iPeriodId, iItemUnkid, iEmployeeId, iScoringOptionId, iScore, iAMode, iEmployeeAsOnDate, iAssessGrpId, iDataTab, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START {blnIsSelfAssignCompetencies} -- END
                xExpression = Get_Formula_Expression(xFormulaString, iPeriodId, iItemUnkid, iEmployeeId, iScoringOptionId, iScore, iAMode, iEmployeeAsOnDate, xIsUsedAgreedScore, iAssessGrpId, iDataTab, blnIsSelfAssignCompetencies)
                'Shani (23-Nov123-2016-2016) -- End


                'S.SANDEEP [04 JUN 2015] -- END


                If xExpression.Trim.Length > 0 Then
                    Dim objFormula As New clsFomulaEvaluate
                    xScoreValue = objFormula.Eval(xExpression)
                    If xScoreValue > 0 Then
                        'S.SANDEEP [17 APR 2015] -- START
                        'xScoreValue = CDbl(xScoreValue).ToString("########0.#0")

                        'S.SANDEEP |09-DEC-2019| -- START
                        'ISSUE/ENHANCEMENT : NORMALIZED FORMULA
                        'xScoreValue = CDbl(xScoreValue).ToString("######################0.#0")
                        xScoreValue = CDbl(xScoreValue).ToString("#######################0.###0")
                        'S.SANDEEP |09-DEC-2019| -- END

                        'S.SANDEEP [17 APR 2015] -- END
                    End If
                    objFormula = Nothing
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Process_Assessment_Formula", mstrModuleName)
        Finally
        End Try
        Return xScoreValue
    End Function

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function Get_Formula_Expression(ByVal xStrFormulaString As String, _
    '                                        ByVal iPeriodId As Integer, _
    '                                        ByVal iItemUnkid As Integer, _
    '                                        ByVal iEmployeeId As Integer, _
    '                                        ByVal iScoringOptionId As Integer, _
    '                                        ByVal iScore As Decimal, _
    '                                        ByVal iAMode As enAssessmentMode, _
    '                                        Optional ByVal iAssessGrpId As Integer = 0, _
    '                                        Optional ByVal iDataTab As DataTable = Nothing) As String
    Private Function Get_Formula_Expression(ByVal xStrFormulaString As String, _
                                            ByVal iPeriodId As Integer, _
                                            ByVal iItemUnkid As Integer, _
                                            ByVal iEmployeeId As Integer, _
                                            ByVal iScoringOptionId As Integer, _
                                            ByVal iScore As Decimal, _
                                            ByVal iAMode As enAssessmentMode, _
                                            ByVal iEmployeeAsOnDate As DateTime, _
                                            ByVal xIsUsedAgreedScore As Boolean, _
                                            Optional ByVal iAssessGrpId As Integer = 0, _
                                            Optional ByVal iDataTab As DataTable = Nothing, _
                                            Optional ByVal blnIsSelfAssignCompetencies As Boolean = False) As String
        'Shani (23-Nov-2016) -- [ByVal xIsUsedAgreedScore As Boolean, _]
        'S.SANDEEP [08 Jan 2016] -- START {blnIsSelfAssignCompetencies} -- END
        'S.SANDEEP [04 JUN 2015] -- END


        Dim xStrExpression As String = String.Empty
        Dim xOperand As String = String.Empty
        Dim xTemp As String = ""
        Dim xFinalValue As Decimal = 0
        Try
            For i As Integer = 1 To xStrFormulaString.Length
                If Mid(xStrFormulaString, i, 1) = "+" OrElse _
                   Mid(xStrFormulaString, i, 1) = "-" OrElse _
                   Mid(xStrFormulaString, i, 1) = "*" OrElse _
                   Mid(xStrFormulaString, i, 1) = "/" OrElse _
                   Mid(xStrFormulaString, i, 1) = "(" OrElse _
                   Mid(xStrFormulaString, i, 1) = ")" Then

                    If xTemp.Trim.Length > 0 Then
                        xStrExpression += xOperand & xTemp
                    End If

                    xTemp = Trim(Mid(xStrFormulaString, i, 1))

                    If xTemp = "+" Then
                        If xOperand.Trim.Length > 0 Then
                            xOperand += "("
                        End If
                        xOperand += xTemp
                        xStrExpression += xOperand
                        xTemp = "" : xOperand = ""

                    ElseIf xTemp = "-" Then
                        If xOperand.Trim.Length > 0 Then
                            xOperand += "("
                        End If
                        xOperand += xTemp
                        xStrExpression += xOperand
                        xTemp = "" : xOperand = ""

                    ElseIf xTemp = "*" Then
                        If xOperand.Trim.Length > 0 Then
                            xOperand += "("
                        End If
                        xOperand += xTemp
                        xStrExpression += xOperand
                        xTemp = "" : xOperand = ""

                    ElseIf xTemp = "/" Then
                        If xOperand.Trim.Length > 0 Then
                            xOperand += "("
                        End If
                        xOperand += xTemp
                        xStrExpression += xOperand
                        xTemp = "" : xOperand = ""

                    ElseIf xTemp = "(" Then
                        xOperand = xTemp
                        xStrExpression += xOperand
                        xTemp = "" : xOperand = ""

                    ElseIf xTemp = ")" Then
                        xOperand += xTemp
                        xStrExpression += xOperand
                        xTemp = "" : xOperand = ""

                    Else
                        xOperand += xOperand
                        xStrExpression += xOperand
                        xTemp = "" : xOperand = ""

                    End If
                ElseIf Mid(xStrFormulaString, i, 1) <> "#" Then
                    xTemp += Trim(Mid(xStrFormulaString, i, 1))
                Else
                    Dim iFrmlId As Integer = -1
                    If xTemp <> "" Then

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'xFinalValue = Get_Computation_Variable_Value(CInt(xTemp), iPeriodId, iItemUnkid, iEmployeeId, iScoringOptionId, iScore, iAMode, iAssessGrpId, iDataTab)

                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        'xFinalValue = Get_Computation_Variable_Value(CInt(xTemp), iPeriodId, iItemUnkid, iEmployeeId, iScoringOptionId, iScore, iAMode, iEmployeeAsOnDate, iAssessGrpId, iDataTab, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START {blnIsSelfAssignCompetencies} -- END
                        xFinalValue = Get_Computation_Variable_Value(CInt(xTemp), iPeriodId, iItemUnkid, iEmployeeId, iScoringOptionId, iScore, iAMode, iEmployeeAsOnDate, xIsUsedAgreedScore, iAssessGrpId, iDataTab, blnIsSelfAssignCompetencies)
                        'Shani (23-Nov123-2016-2016) -- End


                        'S.SANDEEP [04 JUN 2015] -- END

                        'S.SANDEEP [17 APR 2015] -- START
                        If iAssessGrpId > 0 Then
                            If IsValid_Computation_Type_Id(CInt(xTemp), True) Then
                                xFinalValue = 0
                            End If
                        Else
                            If IsValid_Computation_Type_Id(CInt(xTemp), False) Then
                                xFinalValue = 0
                            End If
                        End If
                        'S.SANDEEP [17 APR 2015] -- END
                        xStrExpression += xOperand & xFinalValue.ToString
                        xOperand = "" : xTemp = ""
                    End If
                End If
            Next

            If xTemp.Trim.Length > 0 Then
                xStrExpression += xOperand & xTemp
                xOperand = ""
            End If

            xOperand = xOperand.Replace("+", "").Replace("-", "").Replace("*", "").Replace("/", "")
            xStrExpression += xOperand

            Return xStrExpression.Replace("()", "")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Formula_Expression", mstrModuleName)
        Finally
        End Try
        Return xStrExpression
    End Function

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Get_Computation_Variable_Value(ByVal xComputationVariable As Integer, _
                                                    ByVal xPeriodId As Integer, _
                                                    ByVal xItemUnkid As Integer, _
                                                    ByVal xEmployeeId As Integer, _
                                                    ByVal xScoringOptionId As Integer, _
                                                    ByVal xScore As Decimal, _
                                                    ByVal xAMode As enAssessmentMode, _
                                                    ByVal xEmployeeAsOnDate As DateTime, _
                                                    ByVal xIsUsedAgreedScore As Boolean, _
                                                    Optional ByVal xAssessGrpId As Integer = 0, _
                                                    Optional ByVal xDataTab As DataTable = Nothing, _
                                                    Optional ByVal blnIsSelfAssignCompetencies As Boolean = False) As Decimal

        'Shani (23-Nov-2016) -- [ByVal xIsUsedAgreedScore As Boolean, _]
        'S.SANDEEP [08 Jan 2016] -- START {blnIsSelfAssignCompetencies} -- END

        'Private Function Get_Computation_Variable_Value(ByVal xComputationVariable As Integer, _
        '                                            ByVal xPeriodId As Integer, _
        '                                            ByVal xItemUnkid As Integer, _
        '                                            ByVal xEmployeeId As Integer, _
        '                                            ByVal xScoringOptionId As Integer, _
        '                                            ByVal xScore As Decimal, _
        '                                            ByVal xAMode As enAssessmentMode, _
        '                                            Optional ByVal xAssessGrpId As Integer = 0, _
        '                                            Optional ByVal xDataTab As DataTable = Nothing) As Decimal
        'S.SANDEEP [04 JUN 2015] -- END

        Dim StrQ As String = String.Empty
        Dim exForce As Exception

        Dim xLinkedFieldId, xExOrder, xMappingUnkid As Integer
        Dim xBSC_OverAllWeight, xComp_OverAllWeight, xBSC_Ratio, xComp_Ratio, xValue As Decimal

        xValue = 0 : xBSC_OverAllWeight = 0 : xComp_OverAllWeight = 0 : xBSC_Ratio = 0 : xComp_Ratio = 0
        xLinkedFieldId = 0 : xExOrder = 0 : xMappingUnkid = 0

        Dim xTableName, xColName, xParentCol, xParentTabName As String
        xTableName = "" : xColName = "" : xParentCol = "" : xParentTabName = ""

        Dim xStrGroupIds As String = String.Empty
        Dim objAssessGrp As New clsassess_group_master

        'S.SANDEEP [04 JUN 2015] -- START
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        'xStrGroupIds = objAssessGrp.GetCSV_AssessGroupIds(xEmployeeId)

        'S.SANDEEP [19 DEC 2016] -- START
        'xStrGroupIds = objAssessGrp.GetCSV_AssessGroupIds(xEmployeeId, xEmployeeAsOnDate)
        xStrGroupIds = objAssessGrp.GetCSV_AssessGroupIds(xEmployeeId, xEmployeeAsOnDate, xPeriodId)
        'S.SANDEEP [19 DEC 2016] -- END

        'S.SANDEEP [04 JUN 2015] -- END

        objAssessGrp = Nothing


        'S.SANDEEP [14 MAR 2016] -- START
        Dim intAllocRefId As Integer = 0
        Dim objRatio As New clsassess_ratio_master
        intAllocRefId = objRatio.GetAllocReferenceId(xPeriodId)
        'S.SANDEEP [14 MAR 2016] -- END


        Try
            Dim objFMapping As New clsAssess_Field_Mapping
            xLinkedFieldId = objFMapping.Get_Map_FieldId(xPeriodId)
            xMappingUnkid = objFMapping.Get_MappingUnkId(xPeriodId)
            If xMappingUnkid > 0 Then
                objFMapping._Mappingunkid = xMappingUnkid
                xBSC_OverAllWeight = objFMapping._Weight
            End If
            objFMapping = Nothing
            Dim objFMaster As New clsAssess_Field_Master
            xExOrder = objFMaster.Get_Field_ExOrder(xLinkedFieldId)
            objFMaster = Nothing
            Select Case xExOrder
                Case enWeight_Types.WEIGHT_FIELD1
                    xTableName = "hrassess_empfield1_master"
                    xColName = "empfield1unkid"
                    xParentCol = "empfield1unkid"
                    xParentTabName = "hrassess_empfield1_master"
                Case enWeight_Types.WEIGHT_FIELD2
                    xTableName = "hrassess_empfield2_master"
                    xColName = "empfield2unkid"
                    xParentCol = "empfield1unkid"
                    xParentTabName = "hrassess_empfield1_master"
                Case enWeight_Types.WEIGHT_FIELD3
                    xTableName = "hrassess_empfield3_master"
                    xColName = "empfield3unkid"
                    xParentCol = "empfield2unkid"
                    xParentTabName = "hrassess_empfield2_master"
                Case enWeight_Types.WEIGHT_FIELD4
                    xTableName = "hrassess_empfield4_master"
                    xColName = "empfield4unkid"
                    xParentCol = "empfield3unkid"
                    xParentTabName = "hrassess_empfield3_master"
                Case enWeight_Types.WEIGHT_FIELD5
                    xTableName = "hrassess_empfield5_master"
                    xColName = "empfield5unkid"
                    xParentCol = "empfield4unkid"
                    xParentTabName = "hrassess_empfield4_master"
            End Select
            Using objDataOp As New clsDataOperation
                Select Case xComputationVariable
                    Case enAssess_Computation_Types.BSC_ITEM_WEIGHT
                        StrQ = "SELECT @xval = weight FROM " & xTableName & " WITH (NOLOCK) " & _
                               "WHERE isvoid = 0 AND employeeunkid = '" & xEmployeeId & "' AND " & xColName & " = '" & xItemUnkid & "' "

                        objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)

                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                            xValue = 0
                        Else
                            xValue = objDataOp.GetParameterValue("@xval")
                        End If
                    Case enAssess_Computation_Types.BSC_ITEM_EMP_SCORE_VALUE
                        Select Case xAMode
                            Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                                StrQ = "SELECT @xval = CAST(GT.result AS DECIMAL(36,2)) " & _
                                       "FROM hrevaluation_analysis_master AS EM WITH (NOLOCK) " & _
                                       "    JOIN hrgoals_analysis_tran AS GT WITH (NOLOCK) ON EM.analysisunkid = GT.analysisunkid " & _
                                       "WHERE EM.isvoid = 0 AND GT.isvoid = 0 AND EM.selfemployeeunkid = '" & xEmployeeId & "' " & _
                                       "AND EM.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND GT." & xColName & " = '" & xItemUnkid & "' AND EM.periodunkid = '" & xPeriodId & "'"
                                'S.SANDEEP |21-AUG-2019| -- START
                                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                                'REMOVED -------- @xval = GT.result
                                'ADDED ---------- @xval = CAST(GT.result AS DECIMAL(36,2))
                                'S.SANDEEP |21-AUG-2019| -- END

                                objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                                objDataOp.ExecNonQuery(StrQ)

                                If objDataOp.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                    Throw exForce
                                End If

                                If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                                    xValue = 0
                                Else
                                    xValue = objDataOp.GetParameterValue("@xval")
                                End If

                            Case Else
                                xValue = xScore
                        End Select
                    Case enAssess_Computation_Types.BSC_ITEM_ASR_SCORE_VALUE
                        Select Case xAMode
                            Case enAssessmentMode.SELF_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                                StrQ = "SELECT @xval = CAST(" & IIf(xIsUsedAgreedScore, "GT.agreedscore", "GT.result") & " AS DECIMAL(36,2)) " & _
                                       "FROM hrevaluation_analysis_master AS EM WITH (NOLOCK) " & _
                                       "    JOIN hrgoals_analysis_tran AS GT WITH (NOLOCK) ON EM.analysisunkid = GT.analysisunkid " & _
                                       "WHERE EM.isvoid = 0 AND GT.isvoid = 0 AND EM.assessedemployeeunkid = '" & xEmployeeId & "' " & _
                                       "AND EM.assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND GT." & xColName & " = '" & xItemUnkid & "' AND EM.periodunkid = '" & xPeriodId & "'"

                                'S.SANDEEP |21-AUG-2019| -- START
                                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                                'REMOVED -------- @xval = " & IIf(xIsUsedAgreedScore, "GT.agreedscore", "GT.result") & "
                                'ADDED ---------- @xval = CAST(" & IIf(xIsUsedAgreedScore, "GT.agreedscore", "GT.result") & " AS DECIMAL(36,2))
                                'S.SANDEEP |21-AUG-2019| -- END

                                'Shani (23-Nov-2016)--[GT.result]-->[& IIf(xIsUsedAgreedScore, "GT.agreedscore", "GT.result")]


                                objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                                objDataOp.ExecNonQuery(StrQ)

                                If objDataOp.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                    Throw exForce
                                End If

                                If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                                    xValue = 0
                                Else
                                    xValue = objDataOp.GetParameterValue("@xval")
                                End If

                            Case Else
                                xValue = xScore
                        End Select
                    Case enAssess_Computation_Types.BSC_ITEM_REV_SCORE_VALUE
                        Select Case xAMode
                            Case enAssessmentMode.SELF_ASSESSMENT, enAssessmentMode.APPRAISER_ASSESSMENT
                                StrQ = "SELECT @xval = CAST(GT.result AS DECIMAL(36,2)) " & _
                                       "FROM hrevaluation_analysis_master AS EM WITH (NOLOCK) " & _
                                       "    JOIN hrgoals_analysis_tran AS GT WITH (NOLOCK) ON EM.analysisunkid = GT.analysisunkid " & _
                                       "WHERE EM.isvoid = 0 AND GT.isvoid = 0 AND EM.assessedemployeeunkid = '" & xEmployeeId & "' " & _
                                       "AND EM.assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND GT." & xColName & " = '" & xItemUnkid & "' AND EM.periodunkid = '" & xPeriodId & "'"

                                'S.SANDEEP |21-AUG-2019| -- START
                                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                                'REMOVED -------- @xval = GT.result
                                'ADDED ---------- @xval = CAST(GT.result AS DECIMAL(36,2))
                                'S.SANDEEP |21-AUG-2019| -- END

                                objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                                objDataOp.ExecNonQuery(StrQ)

                                If objDataOp.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                    Throw exForce
                                End If

                                If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                                    xValue = 0
                                Else
                                    xValue = objDataOp.GetParameterValue("@xval")
                                End If

                            Case Else
                                xValue = xScore
                        End Select
                    Case enAssess_Computation_Types.BSC_PARENT_FIELD_WEIGHT
                        StrQ = "SELECT @xval = SUM(weight) " & _
                               "FROM " & xTableName & " AS A1 WITH (NOLOCK) " & _
                               "WHERE " & xParentCol & " IN " & _
                               "(SELECT " & xParentCol & " FROM " & xTableName & " AS A2 WITH (NOLOCK) WHERE isvoid = 0 AND " & xColName & " = '" & xItemUnkid & "') "

                        objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                            xValue = 0
                        Else
                            xValue = objDataOp.GetParameterValue("@xval")
                        End If

                    Case enAssess_Computation_Types.BSC_ITEM_MAX_SCORE_VALUE
                        Select Case xScoringOptionId
                            Case enScoringOption.SC_WEIGHTED_BASED
                                StrQ = "SELECT @xval = weight FROM " & xTableName & " WITH (NOLOCK) " & _
                                       "WHERE isvoid = 0 AND employeeunkid = '" & xEmployeeId & "' AND " & xColName & " = '" & xItemUnkid & "' "

                                objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)

                                objDataOp.ExecNonQuery(StrQ)

                                If objDataOp.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                    Throw exForce
                                End If

                                If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                                    xValue = 0
                                Else
                                    xValue = objDataOp.GetParameterValue("@xval")
                                End If
                            Case enScoringOption.SC_SCALE_BASED
                                StrQ = "SELECT " & _
                                       " @xval = MAX(scale) " & _
                                       "FROM hrassess_scalemapping_tran WITH (NOLOCK) " & _
                                       "    JOIN hrassess_scale_master WITH (NOLOCK) ON hrassess_scale_master.scalemasterunkid = hrassess_scalemapping_tran.scalemasterunkid " & _
                                       "WHERE isvoid = 0 AND isactive = 1 AND hrassess_scale_master.periodunkid = '" & xPeriodId & "' AND hrassess_scalemapping_tran.periodunkid = '" & xPeriodId & "' " & _
                                       "AND perspectiveunkid IN " & _
                                       "( " & _
                                       "    SELECT TOP 1 " & _
                                       "        CASE WHEN ISNULL(hrassess_empfield1_master.perspectiveunkid,0) <=0 THEN hrassess_coyfield1_master.perspectiveunkid ELSE hrassess_empfield1_master.perspectiveunkid END AS PerId " & _
                                       "    FROM hrassess_empfield1_master WITH (NOLOCK) " & _
                                       "        LEFT JOIN hrassess_owrfield1_master WITH (NOLOCK) ON hrassess_empfield1_master.owrfield1unkid = hrassess_owrfield1_master.owrfield1unkid " & _
                                       "        LEFT JOIN hrassess_coyfield1_master WITH (NOLOCK) ON hrassess_coyfield1_master.coyfield1unkid = hrassess_owrfield1_master.coyfield1unkid " & _
                                       "        LEFT JOIN hrassess_empfield2_master WITH (NOLOCK) ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid " & _
                                       "        LEFT JOIN hrassess_empfield3_master WITH (NOLOCK) ON hrassess_empfield3_master.empfield2unkid = hrassess_empfield2_master.empfield2unkid " & _
                                       "        LEFT JOIN hrassess_empfield4_master WITH (NOLOCK) ON hrassess_empfield4_master.empfield3unkid = hrassess_empfield3_master.empfield3unkid " & _
                                       "        LEFT JOIN hrassess_empfield5_master WITH (NOLOCK) ON hrassess_empfield5_master.empfield4unkid = hrassess_empfield4_master.empfield4unkid " & _
                                       "    WHERE hrassess_empfield1_master.employeeunkid = '" & xEmployeeId & "' AND hrassess_empfield1_master.periodunkid = '" & xPeriodId & "' AND hrassess_empfield1_master.isvoid = 0 " & _
                                       "        AND " & xTableName & "." & xColName & " = '" & xItemUnkid & "' " & _
                                       ")"

                                objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)

                                objDataOp.ExecNonQuery(StrQ)

                                If objDataOp.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                    Throw exForce
                                End If

                                If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                                    xValue = 0
                                Else
                                    xValue = objDataOp.GetParameterValue("@xval")
                                End If
                        End Select
                    Case enAssess_Computation_Types.BSC_OVERALL_WEIGHT
                        xValue = xBSC_OverAllWeight

                    Case enAssess_Computation_Types.CMP_ITEM_WEIGHT
                        StrQ = "SELECT " & _
                                   "  @xval = CT.weight " & _
                                   "FROM hrassess_competence_assign_tran AS CT WITH (NOLOCK) " & _
                                   "    JOIN hrassess_competence_assign_master AS CM WITH (NOLOCK) ON CM.assigncompetenceunkid = CT.assigncompetenceunkid " & _
                                   "WHERE CT.isvoid = 0 AND CM.periodunkid = '" & xPeriodId & "' AND CM.isvoid = 0 AND CM.assessgroupunkid = '" & xAssessGrpId & "' " & _
                                   "AND CT.competenciesunkid = '" & xItemUnkid & "' "

                        'S.SANDEEP [08 Jan 2016] -- START
                        If blnIsSelfAssignCompetencies = True Then
                            StrQ &= " AND CM.employeeunkid = '" & xEmployeeId & "' "
                        End If
                        'S.SANDEEP [08 Jan 2016] -- END

                        objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)

                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                            xValue = 0
                        Else
                            xValue = objDataOp.GetParameterValue("@xval")
                        End If

                    Case enAssess_Computation_Types.CMP_ITEM_EMP_SCORE_VALUE
                        Select Case xAMode
                            Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                                StrQ = "SELECT " & _
                                       "  @xval = CAST(ISNULL(CAT.result,0) AS DECIMAL(36,2)) " & _
                                       "FROM hrevaluation_analysis_master AS EM WITH (NOLOCK) " & _
                                       "    JOIN hrcompetency_analysis_tran AS CAT WITH (NOLOCK) ON CAT.analysisunkid = EM.analysisunkid " & _
                                       "WHERE CAT.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = '" & xPeriodId & "' AND EM.selfemployeeunkid = '" & xEmployeeId & "' " & _
                                       "AND EM.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND CAT.competenciesunkid = '" & xItemUnkid & "' AND CAT.assessgroupunkid = '" & xAssessGrpId & "' "

                                'S.SANDEEP |21-AUG-2019| -- START
                                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                                'REMOVED -------- @xval = ISNULL(CAT.result,0)
                                'ADDED ---------- @xval = CAST(ISNULL(CAT.result,0) AS DECIMAL(36,2))
                                'S.SANDEEP |21-AUG-2019| -- END

                                objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                                objDataOp.ExecNonQuery(StrQ)

                                If objDataOp.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                    Throw exForce
                                End If

                                If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                                    xValue = 0
                                Else
                                    xValue = objDataOp.GetParameterValue("@xval")
                                End If

                            Case Else
                                xValue = xScore
                        End Select
                    Case enAssess_Computation_Types.CMP_ITEM_ASR_SCORE_VALUE
                        Select Case xAMode
                            Case enAssessmentMode.SELF_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                                StrQ = "SELECT " & _
                                       "  @xval = CAST(ISNULL( " & IIf(xIsUsedAgreedScore, "CAT.agreedscore", "CAT.result") & " ,0) AS DECIMAL(36,2)) " & _
                                       "FROM hrevaluation_analysis_master AS EM WITH (NOLOCK) " & _
                                       "    JOIN hrcompetency_analysis_tran AS CAT WITH (NOLOCK) ON CAT.analysisunkid = EM.analysisunkid " & _
                                       "WHERE CAT.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = '" & xPeriodId & "' AND EM.assessedemployeeunkid = '" & xEmployeeId & "' " & _
                                       "AND EM.assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND CAT.competenciesunkid = '" & xItemUnkid & "' AND CAT.assessgroupunkid = '" & xAssessGrpId & "' "
                                'S.SANDEEP |21-AUG-2019| -- START
                                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                                'REMOVED -------- @xval = " & ISNULL( " & IIf(xIsUsedAgreedScore, "CAT.agreedscore", "CAT.result") & " ,0)
                                'ADDED ---------- @xval = CAST(" & IIf(xIsUsedAgreedScore, "CAT.agreedscore", "CAT.result") & " AS DECIMAL(36,2))
                                'S.SANDEEP |21-AUG-2019| -- END


                                'Shani (23-Nov-2016) --[CAT.result]-->[IIf(xIsUsedAgreedScore, "CAT.agreedscore", "CAT.result")]
                                'Shani (24-May-2016) -- [EM.selfemployeeunkid -- >EM.assessedemployeeunkid]


                                objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                                objDataOp.ExecNonQuery(StrQ)

                                If objDataOp.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                    Throw exForce
                                End If

                                If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                                    xValue = 0
                                Else
                                    xValue = objDataOp.GetParameterValue("@xval")
                                End If

                            Case Else
                                xValue = xScore
                        End Select

                    Case enAssess_Computation_Types.CMP_ITEM_REV_SCORE_VALUE
                        Select Case xAMode
                            Case enAssessmentMode.SELF_ASSESSMENT, enAssessmentMode.APPRAISER_ASSESSMENT
                                StrQ = "SELECT " & _
                                       "  @xval = CAST(ISNULL(CAT.result,0) AS DECIMAL(36,2)) " & _
                                       "FROM hrevaluation_analysis_master AS EM WITH (NOLOCK) " & _
                                       "    JOIN hrcompetency_analysis_tran AS CAT WITH (NOLOCK) ON CAT.analysisunkid = EM.analysisunkid " & _
                                       "WHERE CAT.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = '" & xPeriodId & "' AND EM.assessedemployeeunkid = '" & xEmployeeId & "' " & _
                                       "AND EM.assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND CAT.competenciesunkid = '" & xItemUnkid & "' AND CAT.assessgroupunkid = '" & xAssessGrpId & "' "

                                'S.SANDEEP |21-AUG-2019| -- START
                                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                                'REMOVED -------- @xval = ISNULL(CAT.result,0)
                                'ADDED ---------- @xval = CAST(ISNULL(CAT.result,0) AS DECIMAL(36,2))
                                'S.SANDEEP |21-AUG-2019| -- END

                                'Shani (24-May-2016) -- [EM.selfemployeeunkid -- >EM.assessedemployeeunkid]

                                objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                                objDataOp.ExecNonQuery(StrQ)

                                If objDataOp.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                    Throw exForce
                                End If

                                If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                                    xValue = 0
                                Else
                                    xValue = objDataOp.GetParameterValue("@xval")
                                End If

                            Case Else
                                xValue = xScore
                        End Select

                    Case enAssess_Computation_Types.CMP_CATEGORY_WEIGHT

                        'S.SANDEEP [15 FEB 2016] -- START
                        Dim StrTableName As String = ""
                        Dim objCP As New clsassess_competencies_master
                        StrTableName = objCP.GetCompetencyTableName(xPeriodId, objDataOp)
                        objCP = Nothing

                        'StrQ = "SELECT " & _
                        '       "  @xval = ISNULL(SUM(CAT.weight),0) " & _
                        '       "FROM hrassess_competence_assign_tran AS CAT " & _
                        '       "    JOIN hrassess_competence_assign_master AS CAM ON CAM.assigncompetenceunkid = CAT.assigncompetenceunkid " & _
                        '       "    JOIN hrassess_competencies_master AS CM ON CAT.competenciesunkid = CM.competenciesunkid " & _
                        '       "WHERE CAM.isvoid = 0 AND CAT.isvoid = 0 AND CAM.assessgroupunkid = '" & xAssessGrpId & "' AND CM.competence_categoryunkid " & _
                        '       "IN " & _
                        '       "( " & _
                        '       "    SELECT " & _
                        '       "        CM.competence_categoryunkid " & _
                        '       "    FROM hrassess_competence_assign_tran AS CAT " & _
                        '       "        JOIN hrassess_competence_assign_master AS CAM ON CAM.assigncompetenceunkid = CAT.assigncompetenceunkid " & _
                        '       "        JOIN hrassess_competencies_master AS CM ON CAT.competenciesunkid = CM.competenciesunkid " & _
                        '       "    WHERE CAM.isvoid = 0 AND CAT.isvoid = 0 AND CAM.assessgroupunkid = '" & xAssessGrpId & "' AND CAT.competenciesunkid = '" & xItemUnkid & "' AND CAM.periodunkid = '" & xPeriodId & "' "

                        StrQ = "SELECT " & _
                               "  @xval = ISNULL(SUM(CAT.weight),0) " & _
                               "FROM hrassess_competence_assign_tran AS CAT WITH (NOLOCK) " & _
                               "    JOIN hrassess_competence_assign_master AS CAM WITH (NOLOCK) ON CAM.assigncompetenceunkid = CAT.assigncompetenceunkid " & _
                               "    JOIN " & StrTableName & " AS CM ON CAT.competenciesunkid = CM.competenciesunkid " & _
                               "WHERE CAM.isvoid = 0 AND CAT.isvoid = 0 AND CAM.assessgroupunkid = '" & xAssessGrpId & "' AND CM.competence_categoryunkid " & _
                               "IN " & _
                               "( " & _
                               "    SELECT " & _
                               "        CM.competence_categoryunkid " & _
                               "    FROM hrassess_competence_assign_tran AS CAT WITH (NOLOCK) " & _
                               "        JOIN hrassess_competence_assign_master AS CAM WITH (NOLOCK) ON CAM.assigncompetenceunkid = CAT.assigncompetenceunkid " & _
                               "        JOIN " & StrTableName & " AS CM ON CAT.competenciesunkid = CM.competenciesunkid " & _
                               "    WHERE CAM.isvoid = 0 AND CAT.isvoid = 0 AND CAM.assessgroupunkid = '" & xAssessGrpId & "' AND CAT.competenciesunkid = '" & xItemUnkid & "' AND CAM.periodunkid = '" & xPeriodId & "' "
                        'S.SANDEEP [15 FEB 2016] -- END

                        
                        'S.SANDEEP [06 Jan 2016] -- START
                        If blnIsSelfAssignCompetencies = True Then
                            StrQ &= " AND CAM.employeeunkid = '" & xEmployeeId & "' "
                        End If
                        'S.SANDEEP [06 Jan 2016] -- END

                        StrQ &= ") AND CAM.periodunkid = '" & xPeriodId & "' "

                        'S.SANDEEP [06 Jan 2016] -- START
                        If blnIsSelfAssignCompetencies = True Then
                            StrQ &= " AND CAM.employeeunkid = '" & xEmployeeId & "' "
                        End If
                        'S.SANDEEP [06 Jan 2016] -- END

                        objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                            xValue = 0
                        Else
                            xValue = objDataOp.GetParameterValue("@xval")
                        End If

                    Case enAssess_Computation_Types.CMP_ITEM_MAX_SCORE_VALUE
                        Select Case xScoringOptionId
                            Case enScoringOption.SC_WEIGHTED_BASED
                                'S.SANDEEP [15 JAN 2016] -- START
                                Dim StrTableName As String = ""
                                Dim objCP As New clsassess_competencies_master
                                StrTableName = objCP.GetCompetencyTableName(xPeriodId, objDataOp)
                                objCP = Nothing

                                'StrQ = "SELECT " & _
                                '       "  @xval = CAT.weight " & _
                                '       "FROM hrassess_competence_assign_tran AS CAT " & _
                                '       "    JOIN hrassess_competence_assign_master AS CAM ON CAM.assigncompetenceunkid = CAT.assigncompetenceunkid " & _
                                '       "    JOIN hrassess_competencies_master AS CM ON CAT.competenciesunkid = CM.competenciesunkid " & _
                                '       "WHERE CAM.isvoid = 0 AND CAT.isvoid = 0 AND CAM.assessgroupunkid = '" & xAssessGrpId & "' AND CAT.competenciesunkid = '" & xItemUnkid & "' AND CAM.periodunkid = '" & xPeriodId & "' "

                                StrQ = "SELECT " & _
                                       "  @xval = CAT.weight " & _
                                       "FROM hrassess_competence_assign_tran AS CAT WITH (NOLOCK) " & _
                                       "    JOIN hrassess_competence_assign_master AS CAM WITH (NOLOCK) ON CAM.assigncompetenceunkid = CAT.assigncompetenceunkid " & _
                                       "    JOIN " & StrTableName & " AS CM ON CAT.competenciesunkid = CM.competenciesunkid " & _
                                       "WHERE CAM.isvoid = 0 AND CAT.isvoid = 0 AND CAM.assessgroupunkid = '" & xAssessGrpId & "' AND CAT.competenciesunkid = '" & xItemUnkid & "' AND CAM.periodunkid = '" & xPeriodId & "' "
                                'S.SANDEEP [15 JAN 2016] -- END
                                

                                

                                'S.SANDEEP [06 Jan 2016] -- START
                                If blnIsSelfAssignCompetencies = True Then
                                    StrQ &= " AND CAM.employeeunkid = '" & xEmployeeId & "' "
                                End If
                                'S.SANDEEP [06 Jan 2016] -- END

                                objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)

                                objDataOp.ExecNonQuery(StrQ)

                                If objDataOp.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                    Throw exForce
                                End If

                                If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                                    xValue = 0
                                Else
                                    xValue = objDataOp.GetParameterValue("@xval")
                                End If
                            Case enScoringOption.SC_SCALE_BASED

                                'S.SANDEEP [15 FEB 2016] -- START
                                Dim StrTableName As String = ""
                                Dim objCP As New clsassess_competencies_master
                                StrTableName = objCP.GetCompetencyTableName(xPeriodId, objDataOp)
                                objCP = Nothing

                                'StrQ = "SELECT " & _
                                '       "  @xval = MAX(scale) " & _
                                '       "FROM hrassess_scale_master WHERE periodunkid = '" & xPeriodId & "' AND isactive = 1 " & _
                                '       "AND scalemasterunkid IN " & _
                                '       "( " & _
                                '       "    SELECT " & _
                                '       "        CM.scalemasterunkid " & _
                                '       "    FROM hrassess_competence_assign_tran AS CAT " & _
                                '       "        JOIN hrassess_competence_assign_master AS CAM ON CAM.assigncompetenceunkid = CAT.assigncompetenceunkid " & _
                                '       "        JOIN hrassess_competencies_master AS CM ON CAT.competenciesunkid = CM.competenciesunkid " & _
                                '       "    WHERE CAM.isvoid = 0 AND CAT.isvoid = 0 AND CAM.assessgroupunkid = '" & xAssessGrpId & "' AND CAT.competenciesunkid = '" & xItemUnkid & "' AND CAM.periodunkid = '" & xPeriodId & "' "

                                StrQ = "SELECT " & _
                                       "  @xval = MAX(scale) " & _
                                       "FROM hrassess_scale_master WITH (NOLOCK) WHERE periodunkid = '" & xPeriodId & "' AND isactive = 1 " & _
                                       "AND scalemasterunkid IN " & _
                                       "( " & _
                                       "    SELECT " & _
                                       "        CM.scalemasterunkid " & _
                                       "    FROM hrassess_competence_assign_tran AS CAT WITH (NOLOCK) " & _
                                       "        JOIN hrassess_competence_assign_master AS CAM WITH (NOLOCK) ON CAM.assigncompetenceunkid = CAT.assigncompetenceunkid " & _
                                       "        JOIN " & StrTableName & " AS CM ON CAT.competenciesunkid = CM.competenciesunkid " & _
                                       "    WHERE CAM.isvoid = 0 AND CAT.isvoid = 0 AND CAM.assessgroupunkid = '" & xAssessGrpId & "' AND CAT.competenciesunkid = '" & xItemUnkid & "' AND CAM.periodunkid = '" & xPeriodId & "' "
                                'S.SANDEEP [15 FEB 2016] -- END

                                

                                'S.SANDEEP [06 Jan 2016] -- START
                                If blnIsSelfAssignCompetencies = True Then
                                    StrQ &= " AND CAM.employeeunkid = '" & xEmployeeId & "' "
                                End If
                                'S.SANDEEP [06 Jan 2016] -- END

                                StrQ &= ")"

                                objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)

                                objDataOp.ExecNonQuery(StrQ)

                                If objDataOp.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                    Throw exForce
                                End If

                                If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                                    xValue = 0
                                Else
                                    xValue = objDataOp.GetParameterValue("@xval")
                                End If
                        End Select

                    Case enAssess_Computation_Types.CMP_ASSESSMENT_GROUP_WEIGHT
                        StrQ = "SELECT " & _
                               " @xval = SUM(weight) " & _
                               "FROM hrassess_group_master WITH (NOLOCK) " & _
                               "WHERE assessgroupunkid = '" & xAssessGrpId & "' AND isactive = 1 "

                        objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                            xValue = 0
                        Else
                            xValue = objDataOp.GetParameterValue("@xval")
                        End If

                    Case enAssess_Computation_Types.CMP_OVERALL_WEIGHT
                        StrQ = "SELECT " & _
                               " @xval = SUM(weight) " & _
                               "FROM hrassess_group_master WITH (NOLOCK) " & _
                               "WHERE assessgroupunkid IN(" & xStrGroupIds & ") AND isactive = 1 "

                        objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                            xValue = 0
                        Else
                            xValue = objDataOp.GetParameterValue("@xval")
                        End If

                    Case enAssess_Computation_Types.BSC_PARENT_ITEMS_ALL_PERSPECTIVE_COUNT
                        StrQ = "SELECT " & _
                               " @xval = COUNT(" & xParentCol & ") " & _
                               "FROM " & xParentTabName & " WITH (NOLOCK) " & _
                               "WHERE employeeunkid = '" & xEmployeeId & "' AND periodunkid = '" & xPeriodId & "' AND isvoid = 0 "

                        objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                            xValue = 0
                        Else
                            xValue = objDataOp.GetParameterValue("@xval")
                        End If

                    Case enAssess_Computation_Types.BSC_LINKED_ITEMS_ALL_PERSPECTIVE_COUNT
                        StrQ = "SELECT " & _
                               " @xval = COUNT(" & xColName & ") " & _
                               "FROM " & xTableName & " WITH (NOLOCK) " & _
                               "WHERE employeeunkid = '" & xEmployeeId & "' AND periodunkid = '" & xPeriodId & "' AND isvoid = 0 "

                        objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                            xValue = 0
                        Else
                            xValue = objDataOp.GetParameterValue("@xval")
                        End If

                    Case enAssess_Computation_Types.CMP_ASSESSMENT_GROUP_COUNT
                        Dim xGrp() As String = xStrGroupIds.Split(",")
                        Dim xLastGrp As String = String.Empty
                        If xGrp.Length > 0 Then
                            For i As Integer = 0 To xGrp.Length - 1
                                If xLastGrp <> xGrp(i).ToString.Trim Then
                                    xValue += 1
                                    xLastGrp = xGrp(i).ToString.Trim
                                End If
                            Next
                        End If

                    Case enAssess_Computation_Types.CMP_ITEMS_PER_CATEGORY_COUNT
                        StrQ = "SELECT " & _
                               " @xval = COUNT(DISTINCT CT.competenciesunkid) " & _
                               "FROM hrassess_competence_assign_tran AS CT WITH (NOLOCK) " & _
                               "    JOIN hrassess_competence_assign_master AS CM WITH (NOLOCK) ON CM.assigncompetenceunkid = CT.assigncompetenceunkid " & _
                               "WHERE CM.isvoid = 0 AND CT.isvoid = 0 AND CM.assessgroupunkid = '" & xAssessGrpId & "' AND CM.periodunkid = '" & xPeriodId & "' "

                        'S.SANDEEP [06 Jan 2016] -- START
                        If blnIsSelfAssignCompetencies = True Then
                            StrQ &= " AND CM.employeeunkid = '" & xEmployeeId & "' "
                        End If
                        'S.SANDEEP [06 Jan 2016] -- END

                        objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                            xValue = 0
                        Else
                            xValue = objDataOp.GetParameterValue("@xval")
                        End If

                    Case enAssess_Computation_Types.CMP_CATEGORY_PER_GROUP_COUNT

                        'S.SANDEEP [15 FEB 2016] -- START
                        Dim StrTableName As String = ""
                        Dim objCP As New clsassess_competencies_master
                        StrTableName = objCP.GetCompetencyTableName(xPeriodId, objDataOp)
                        objCP = Nothing
                        'StrQ = "SELECT " & _
                        '       " @xval = COUNT(DISTINCT COM.competence_categoryunkid) " & _
                        '       "FROM hrassess_competence_assign_tran AS CT " & _
                        '       "    JOIN hrassess_competence_assign_master AS CM ON CM.assigncompetenceunkid = CT.assigncompetenceunkid " & _
                        '       "    JOIN hrassess_competencies_master AS COM ON COM.competenciesunkid = CT.competenciesunkid " & _
                        '       "WHERE CM.isvoid = 0 AND CT.isvoid = 0 AND CM.assessgroupunkid '" & xAssessGrpId & "' AND CM.periodunkid = '" & xPeriodId & "' "

                        StrQ = "SELECT " & _
                               " @xval = COUNT(DISTINCT COM.competence_categoryunkid) " & _
                               "FROM hrassess_competence_assign_tran AS CT WITH (NOLOCK) " & _
                               "    JOIN hrassess_competence_assign_master AS CM WITH (NOLOCK) ON CM.assigncompetenceunkid = CT.assigncompetenceunkid " & _
                               "    JOIN " & StrTableName & " AS COM ON COM.competenciesunkid = CT.competenciesunkid " & _
                               "WHERE CM.isvoid = 0 AND CT.isvoid = 0 AND CM.assessgroupunkid = '" & xAssessGrpId & "' AND CM.periodunkid = '" & xPeriodId & "' "
                        'S.SANDEEP [15 FEB 2016] -- END

                        

                        'S.SANDEEP [06 Jan 2016] -- START
                        If blnIsSelfAssignCompetencies = True Then
                            StrQ &= " AND CM.employeeunkid = '" & xEmployeeId & "' "
                        End If
                        'S.SANDEEP [06 Jan 2016] -- END

                        objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                            xValue = 0
                        Else
                            xValue = objDataOp.GetParameterValue("@xval")
                        End If

                    Case enAssess_Computation_Types.CMP_ITEMS_ALL_CATEGORY_GROUP_COUNT
                        StrQ = "SELECT " & _
                               " @xval = COUNT(DISTINCT CT.competenciesunkid) " & _
                               "FROM hrassess_competence_assign_tran AS CT WITH (NOLOCK) " & _
                               "    JOIN hrassess_competence_assign_master AS CM WITH (NOLOCK) ON CM.assigncompetenceunkid = CT.assigncompetenceunkid " & _
                               "WHERE CM.isvoid = 0 AND CT.isvoid = 0 AND CM.assessgroupunkid IN(" & xStrGroupIds & ") AND CM.periodunkid = '" & xPeriodId & "' "

                        'S.SANDEEP [06 Jan 2016] -- START
                        If blnIsSelfAssignCompetencies = True Then
                            StrQ &= " AND CM.employeeunkid = '" & xEmployeeId & "' "
                        End If
                        'S.SANDEEP [06 Jan 2016] -- END

                        objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                            xValue = 0
                        Else
                            xValue = objDataOp.GetParameterValue("@xval")
                        End If

                    Case enAssess_Computation_Types.CMP_CATEGORY_ALL_GROUP_COUNT

                        'S.SANDEEP [15 FEB 2016] -- START
                        Dim StrTableName As String = ""
                        Dim objCP As New clsassess_competencies_master
                        StrTableName = objCP.GetCompetencyTableName(xPeriodId, objDataOp)
                        objCP = Nothing
                        'StrQ = "SELECT " & _
                        '       " @xval = COUNT(DISTINCT COM.competence_categoryunkid) " & _
                        '       "FROM hrassess_competence_assign_tran AS CT " & _
                        '       "    JOIN hrassess_competence_assign_master AS CM ON CM.assigncompetenceunkid = CT.assigncompetenceunkid " & _
                        '       "    JOIN hrassess_competencies_master AS COM ON COM.competenciesunkid = CT.competenciesunkid " & _
                        '       "WHERE CM.isvoid = 0 AND CT.isvoid = 0 AND CM.assessgroupunkid IN(" & xStrGroupIds & ") AND CM.periodunkid = '" & xPeriodId & "' "

                        StrQ = "SELECT " & _
                               " @xval = COUNT(DISTINCT COM.competence_categoryunkid) " & _
                               "FROM hrassess_competence_assign_tran AS CT WITH (NOLOCK) " & _
                               "    JOIN hrassess_competence_assign_master AS CM WITH (NOLOCK) ON CM.assigncompetenceunkid = CT.assigncompetenceunkid " & _
                               "    JOIN " & StrTableName & " AS COM ON COM.competenciesunkid = CT.competenciesunkid " & _
                               "WHERE CM.isvoid = 0 AND CT.isvoid = 0 AND CM.assessgroupunkid IN(" & xStrGroupIds & ") AND CM.periodunkid = '" & xPeriodId & "' "
                        'S.SANDEEP [15 FEB 2016] -- END
                        

                        'S.SANDEEP [06 Jan 2016] -- START
                        If blnIsSelfAssignCompetencies = True Then
                            StrQ &= " AND CM.employeeunkid  = '" & xEmployeeId & "' "
                        End If
                        'S.SANDEEP [06 Jan 2016] -- END

                        objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                            xValue = 0
                        Else
                            xValue = objDataOp.GetParameterValue("@xval")
                        End If

                    Case enAssess_Computation_Types.BSC_RATIO_VALUE

                        'S.SANDEEP [14 MAR 2016] -- START
                        'StrQ = "SELECT @xval = bsc_weight FROM hrassessment_ratio " & _
                        '       "WHERE periodunkid = '" & xPeriodId & "' AND isactive = 1 " & _
                        '       "AND jobgroupunkid IN (SELECT jobgroupunkid FROM hremployee_master WHERE employeeunkid = '" & xEmployeeId & "') "
                        Dim StrQJoin As String = ""
                        Dim StrQInnr As String = ""
                        Dim StrQColName As String = ""

                        Select Case intAllocRefId
                            Case enAllocation.JOBS, enAllocation.JOB_GROUP
                                StrQInnr = "    SELECT " & _
                                           "         jobgroupunkid " & _
                                           "        ,jobunkid " & _
                                           "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                           "        ,employeeunkid " & _
                                           "    FROM hremployee_categorization_tran WITH (NOLOCK) " & _
                                           "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                                           "        AND hremployee_categorization_tran.employeeunkid = '" & xEmployeeId & "' "

                                Select Case intAllocRefId
                                    Case enAllocation.JOB_GROUP
                                        StrQColName = " A.jobgroupunkid "
                                    Case enAllocation.JOBS
                                        StrQColName = " A.jobunkid "
                                End Select

                            Case enAllocation.COST_CENTER
                            Case enAllocation.EMPLOYEE
                            Case Else
                                StrQInnr = "SELECT " & _
                                           "     employeeunkid " & _
                                           "    ,stationunkid " & _
                                           "    ,deptgroupunkid " & _
                                           "    ,departmentunkid " & _
                                           "    ,sectiongroupunkid " & _
                                           "    ,sectionunkid " & _
                                           "    ,unitgroupunkid " & _
                                           "    ,unitunkid " & _
                                           "    ,teamunkid " & _
                                           "    ,classgroupunkid " & _
                                           "    ,classunkid " & _
                                           "    ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                           "FROM hremployee_transfer_tran WITH (NOLOCK) " & _
                                           "WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                                           " AND hremployee_transfer_tran.employeeunkid = '" & xEmployeeId & "' "

                                Select Case intAllocRefId
                                    Case enAllocation.BRANCH
                                        StrQColName = " A.stationunkid "
                                    Case enAllocation.DEPARTMENT_GROUP
                                        StrQColName = " A.deptgroupunkid "
                                    Case enAllocation.DEPARTMENT
                                        StrQColName = " A.departmentunkid "
                                    Case enAllocation.SECTION_GROUP
                                        StrQColName = " A.sectiongroupunkid "
                                    Case enAllocation.SECTION
                                        StrQColName = " A.sectionunkid "
                                    Case enAllocation.UNIT_GROUP
                                        StrQColName = " A.unitgroupunkid "
                                    Case enAllocation.UNIT
                                        StrQColName = " A.unitunkid "
                                    Case enAllocation.TEAM
                                        StrQColName = " A.teamunkid "
                                    Case enAllocation.CLASS_GROUP
                                        StrQColName = " A.classgroupunkid "
                                    Case enAllocation.CLASSES
                                        StrQColName = " A.classunkid "
                                End Select

                        End Select

                        StrQJoin = " JOIN hrassess_ratio_tran WITH (NOLOCK) ON " & StrQColName & " = hrassess_ratio_tran.allocationid " & _
                                   " JOIN hrassess_ratio_master WITH (NOLOCK) ON hrassess_ratio_master.ratiounkid = hrassess_ratio_tran.ratiounkid AND hrassess_ratio_master.periodunkid = '" & xPeriodId & "' "


                        StrQ = "SELECT " & _
                               "     @xval = bsc_weight " & _
                               "FROM " & _
                               "( " & _
                               " " & StrQInnr & " " & _
                               ") AS A " & _
                               " " & StrQJoin & " " & _
                               "WHERE A.rno = 1 AND hrassess_ratio_master.allocrefunkid = '" & intAllocRefId & "' " & _
                               "    AND hrassess_ratio_master.isvoid = 0 AND hrassess_ratio_tran.isvoid = 0 "

                        'S.SANDEEP [14 MAR 2016] -- END

                        

                        objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                            xValue = 0
                        Else
                            xValue = objDataOp.GetParameterValue("@xval")
                            'S.SANDEEP |09-JUL-2019| -- START
                            'ISSUE/ENHANCEMENT : PA CHANGES
                            If xValue > 0 Then xValue = (xValue / 100)
                            'S.SANDEEP |09-JUL-2019| -- END
                        End If

                    Case enAssess_Computation_Types.CMP_RATIO_VALUE

                        'S.SANDEEP [14 MAR 2016] -- START
                        'StrQ = "SELECT @xval = ge_weight FROM hrassessment_ratio " & _
                        '       "WHERE periodunkid = '" & xPeriodId & "' AND isactive = 1 " & _
                        '       "AND jobgroupunkid IN (SELECT jobgroupunkid FROM hremployee_master WHERE employeeunkid = '" & xEmployeeId & "') "

                        Dim StrQJoin As String = ""
                        Dim StrQInnr As String = ""
                        Dim StrQColName As String = ""

                        Select Case intAllocRefId
                            Case enAllocation.JOBS, enAllocation.JOB_GROUP
                                StrQInnr = "    SELECT " & _
                                           "         jobgroupunkid " & _
                                           "        ,jobunkid " & _
                                           "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                           "        ,employeeunkid " & _
                                           "    FROM hremployee_categorization_tran WITH (NOLOCK) " & _
                                           "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                                           "        AND hremployee_categorization_tran.employeeunkid = '" & xEmployeeId & "' "

                                Select Case intAllocRefId
                                    Case enAllocation.JOB_GROUP
                                        StrQColName = " A.jobgroupunkid "
                                    Case enAllocation.JOBS
                                        StrQColName = " A.jobunkid "
                                End Select

                            Case enAllocation.COST_CENTER
                            Case enAllocation.EMPLOYEE
                            Case Else
                                StrQInnr = "SELECT " & _
                                           "     employeeunkid " & _
                                           "    ,stationunkid " & _
                                           "    ,deptgroupunkid " & _
                                           "    ,departmentunkid " & _
                                           "    ,sectiongroupunkid " & _
                                           "    ,sectionunkid " & _
                                           "    ,unitgroupunkid " & _
                                           "    ,unitunkid " & _
                                           "    ,teamunkid " & _
                                           "    ,classgroupunkid " & _
                                           "    ,classunkid " & _
                                           "    ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                           "FROM hremployee_transfer_tran WITH (NOLOCK) " & _
                                           "WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                                           " AND hremployee_transfer_tran.employeeunkid = '" & xEmployeeId & "' "

                                Select Case intAllocRefId
                                    Case enAllocation.BRANCH
                                        StrQColName = " A.stationunkid "
                                    Case enAllocation.DEPARTMENT_GROUP
                                        StrQColName = " A.deptgroupunkid "
                                    Case enAllocation.DEPARTMENT
                                        StrQColName = " A.departmentunkid "
                                    Case enAllocation.SECTION_GROUP
                                        StrQColName = " A.sectiongroupunkid "
                                    Case enAllocation.SECTION
                                        StrQColName = " A.sectionunkid "
                                    Case enAllocation.UNIT_GROUP
                                        StrQColName = " A.unitgroupunkid "
                                    Case enAllocation.UNIT
                                        StrQColName = " A.unitunkid "
                                    Case enAllocation.TEAM
                                        StrQColName = " A.teamunkid "
                                    Case enAllocation.CLASS_GROUP
                                        StrQColName = " A.classgroupunkid "
                                    Case enAllocation.CLASSES
                                        StrQColName = " A.classunkid "
                                End Select

                        End Select

                        StrQJoin = " JOIN hrassess_ratio_tran WITH (NOLOCK) ON " & StrQColName & " = hrassess_ratio_tran.allocationid " & _
                                   " JOIN hrassess_ratio_master WITH (NOLOCK) ON hrassess_ratio_master.ratiounkid = hrassess_ratio_tran.ratiounkid AND hrassess_ratio_master.periodunkid = '" & xPeriodId & "' "


                        StrQ = "SELECT " & _
                               "     @xval = ge_weight " & _
                               "FROM " & _
                               "( " & _
                               " " & StrQInnr & " " & _
                               ") AS A " & _
                               " " & StrQJoin & " " & _
                               "WHERE A.rno = 1 AND hrassess_ratio_master.allocrefunkid = '" & intAllocRefId & "' " & _
                               "    AND hrassess_ratio_master.isvoid = 0 AND hrassess_ratio_tran.isvoid = 0 "
                        'S.SANDEEP [14 MAR 2016] -- END

                        

                        objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                            xValue = 0
                        Else
                            xValue = objDataOp.GetParameterValue("@xval")
                            'S.SANDEEP |09-JUL-2019| -- START
                            'ISSUE/ENHANCEMENT : PA CHANGES
                            If xValue > 0 Then xValue = (xValue / 100)
                            'S.SANDEEP |09-JUL-2019| -- END
                        End If

                        'S.SANDEEP [21 JAN 2015] -- START
                    Case enAssess_Computation_Types.BSC_TOT_EMP_ROW_SCORE_RATIO
                        If xDataTab Is Nothing Then
                            StrQ = "SELECT " & _
                                   " @xval = CAST(SUM(GT.result * GT.item_weight)/SUM(GT.item_weight*(CASE WHEN @ScoreId = 2 THEN GT.max_scale ELSE GT.item_weight END)) AS DECIMAL(36,2)) " & _
                                   "FROM hrgoals_analysis_tran GT WITH (NOLOCK) " & _
                                   "  JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = GT.analysisunkid " & _
                                   "WHERE EM.isvoid = 0 AND GT.isvoid = 0 AND EM.periodunkid = '" & xPeriodId & "' AND EM.selfemployeeunkid = '" & xEmployeeId & "' AND EM.assessmodeid = " & xAMode

                            'S.SANDEEP |21-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                            'REMOVED -------- @xval = SUM(GT.result * GT.item_weight)/SUM(GT.item_weight*(CASE WHEN @ScoreId = 2 THEN GT.max_scale ELSE GT.item_weight END))
                            'ADDED ---------- @xval = CAST(SUM(GT.result * GT.item_weight)/SUM(GT.item_weight*(CASE WHEN @ScoreId = 2 THEN GT.max_scale ELSE GT.item_weight END)) AS DECIMAL(36,2))
                            'S.SANDEEP |21-AUG-2019| -- END

                            objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                            objDataOp.AddParameter("@ScoreId", SqlDbType.Int, eZeeDataType.INT_SIZE, xScoringOptionId)

                            objDataOp.ExecNonQuery(StrQ)

                            If objDataOp.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                Throw exForce
                            End If

                            If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                                xValue = 0
                            Else
                                xValue = objDataOp.GetParameterValue("@xval")
                            End If
                        Else
                            xValue = Process_Summary_Value(xScoringOptionId, xDataTab, xAMode)
                        End If

                    Case enAssess_Computation_Types.BSC_TOT_ASR_ROW_SCORE_RATIO
                        If xDataTab Is Nothing Then

                            'Shani (23-Nov-2016) -- Start
                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                            'StrQ = "SELECT " & _
                            '       " @xval = SUM(GT.result * GT.item_weight)/SUM(GT.item_weight*(CASE WHEN @ScoreId = 2 THEN GT.max_scale ELSE GT.item_weight END)) " & _
                            '       "FROM hrgoals_analysis_tran GT " & _
                            '       "  JOIN hrevaluation_analysis_master AS EM ON EM.analysisunkid = GT.analysisunkid " & _
                            '       "WHERE EM.isvoid = 0 AND GT.isvoid = 0 AND EM.periodunkid = '" & xPeriodId & "' AND EM.assessedemployeeunkid = '" & xEmployeeId & "' AND EM.assessmodeid = " & xAMode

                            StrQ = "SELECT " & _
                                   " @xval = CAST(SUM(" & IIf(xIsUsedAgreedScore, "GT.agreedscore", "GT.result") & " * GT.item_weight)/SUM(GT.item_weight*(CASE WHEN @ScoreId = 2 THEN GT.max_scale ELSE GT.item_weight END)) AS DECIMAL(36,2)) " & _
                                   "FROM hrgoals_analysis_tran GT WITH (NOLOCK) " & _
                                   "  JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = GT.analysisunkid " & _
                                   "WHERE EM.isvoid = 0 AND GT.isvoid = 0 AND EM.periodunkid = '" & xPeriodId & "' AND EM.assessedemployeeunkid = '" & xEmployeeId & "' AND EM.assessmodeid = " & xAMode

                            'S.SANDEEP |21-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                            'REMOVED -------- @xval = SUM(" & IIf(xIsUsedAgreedScore, "GT.agreedscore", "GT.result") & " * GT.item_weight)/SUM(GT.item_weight*(CASE WHEN @ScoreId = 2 THEN GT.max_scale ELSE GT.item_weight END))
                            'ADDED ---------- @xval = CAST(SUM(" & IIf(xIsUsedAgreedScore, "GT.agreedscore", "GT.result") & " * GT.item_weight)/SUM(GT.item_weight*(CASE WHEN @ScoreId = 2 THEN GT.max_scale ELSE GT.item_weight END)) AS DECIMAL(36,2))
                            'S.SANDEEP |21-AUG-2019| -- END

                            'Shani (23-Nov123-2016-2016) -- End
                            objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                            objDataOp.AddParameter("@ScoreId", SqlDbType.Int, eZeeDataType.INT_SIZE, xScoringOptionId)

                            objDataOp.ExecNonQuery(StrQ)

                            If objDataOp.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                Throw exForce
                            End If

                            If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                                xValue = 0
                            Else
                                xValue = objDataOp.GetParameterValue("@xval")
                            End If
                        Else
                            xValue = Process_Summary_Value(xScoringOptionId, xDataTab, xAMode)
                        End If

                    Case enAssess_Computation_Types.BSC_TOT_REV_ROW_SCORE_RATIO
                        If xDataTab Is Nothing Then
                            StrQ = "SELECT " & _
                                   " @xval = CAST(SUM(GT.result * GT.item_weight)/SUM(GT.item_weight*(CASE WHEN @ScoreId = 2 THEN GT.max_scale ELSE GT.item_weight END)) AS DECIMAL(36,2)) " & _
                                   "FROM hrgoals_analysis_tran GT WITH (NOLOCK) " & _
                                   "  JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = GT.analysisunkid " & _
                                   "WHERE EM.isvoid = 0 AND GT.isvoid = 0 AND EM.periodunkid = '" & xPeriodId & "' AND EM.assessedemployeeunkid = '" & xEmployeeId & "' AND EM.assessmodeid = " & xAMode

                            'S.SANDEEP |21-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                            'REMOVED -------- @xval = SUM(GT.result * GT.item_weight)/SUM(GT.item_weight*(CASE WHEN @ScoreId = 2 THEN GT.max_scale ELSE GT.item_weight END))
                            'ADDED ---------- @xval = CAST(SUM(GT.result * GT.item_weight)/SUM(GT.item_weight*(CASE WHEN @ScoreId = 2 THEN GT.max_scale ELSE GT.item_weight END)) AS DECIMAL(36,2))
                            'S.SANDEEP |21-AUG-2019| -- END

                            objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                            objDataOp.AddParameter("@ScoreId", SqlDbType.Int, eZeeDataType.INT_SIZE, xScoringOptionId)

                            objDataOp.ExecNonQuery(StrQ)

                            If objDataOp.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                Throw exForce
                            End If

                            If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                                xValue = 0
                            Else
                                xValue = objDataOp.GetParameterValue("@xval")
                            End If
                        Else
                            xValue = Process_Summary_Value(xScoringOptionId, xDataTab, xAMode)
                        End If


                    Case enAssess_Computation_Types.CMP_TOT_EMP_ROW_SCORE_RATIO
                        If xDataTab Is Nothing Then
                            StrQ = "SELECT " & _
                                   "    @xval = CAST(CASE WHEN ISNULL(SUM(CT.result*CT.item_weight),0)<=0 THEN 0 ELSE ISNULL(SUM(CT.result*CT.item_weight),0)/ISNULL(SUM(CT.item_weight*(CASE WHEN @ScoreId = 2 THEN CT.max_scale ELSE CT.item_weight END)),0) END AS DECIMAL(36,2)) " & _
                                   "FROM hrcompetency_analysis_tran AS CT WITH (NOLOCK) " & _
                                   "JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = CT.analysisunkid " & _
                                   "WHERE CT.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = '" & xPeriodId & "' AND EM.selfemployeeunkid = '" & xEmployeeId & "' AND EM.assessmodeid = '" & xAMode & "' AND CT.assessgroupunkid = '" & xAssessGrpId & "' "

                            'S.SANDEEP |21-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                            'REMOVED -------- @xval = CASE WHEN ISNULL(SUM(CT.result*CT.item_weight),0)<=0 THEN 0 ELSE ISNULL(SUM(CT.result*CT.item_weight),0)/ISNULL(SUM(CT.item_weight*(CASE WHEN @ScoreId = 2 THEN CT.max_scale ELSE CT.item_weight END)),0) END
                            'ADDED ---------- @xval = CAST(CASE WHEN ISNULL(SUM(CT.result*CT.item_weight),0)<=0 THEN 0 ELSE ISNULL(SUM(CT.result*CT.item_weight),0)/ISNULL(SUM(CT.item_weight*(CASE WHEN @ScoreId = 2 THEN CT.max_scale ELSE CT.item_weight END)),0) END AS DECIMAL(36,2))
                            'S.SANDEEP |21-AUG-2019| -- END

                            objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                            objDataOp.AddParameter("@ScoreId", SqlDbType.Int, eZeeDataType.INT_SIZE, xScoringOptionId)

                            objDataOp.ExecNonQuery(StrQ)

                            If objDataOp.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                Throw exForce
                            End If

                            If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                                xValue = 0
                            Else
                                xValue = objDataOp.GetParameterValue("@xval")
                            End If
                        Else
                            xValue = Process_Summary_Value(xScoringOptionId, xDataTab, xAMode)
                        End If


                    Case enAssess_Computation_Types.CMP_TOT_ASR_ROW_SCORE_RATIO
                        If xDataTab Is Nothing Then

                            'Shani (23-Nov-2016) -- Start
                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...

                            'StrQ = "SELECT " & _
                            '       "    @xval = CASE WHEN ISNULL(SUM(CT.result*CT.item_weight),0)<=0 THEN 0 " & _
                            '       "            ELSE ISNULL(SUM(CT.result*CT.item_weight),0)/ISNULL(SUM(CT.item_weight*(CASE WHEN @ScoreId = 2 THEN CT.max_scale ELSE CT.item_weight END)),0) END " & _
                            '       "FROM hrcompetency_analysis_tran AS CT " & _
                            '       "JOIN hrevaluation_analysis_master AS EM ON EM.analysisunkid = CT.analysisunkid " & _
                            '       "WHERE CT.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = '" & xPeriodId & "' AND EM.assessedemployeeunkid = '" & xEmployeeId & "' AND EM.assessmodeid = '" & xAMode & "' AND CT.assessgroupunkid = '" & xAssessGrpId & "' "


                            StrQ = "SELECT " & _
                                   "    @xval = CAST(CASE WHEN ISNULL(SUM( " & IIf(xIsUsedAgreedScore, "CT.agreedscore", "CT.result") & " *CT.item_weight),0)<=0 THEN 0 ELSE ISNULL(SUM(" & IIf(xIsUsedAgreedScore, "CT.agreedscore", "CT.result") & "*CT.item_weight),0)/ISNULL(SUM(CT.item_weight*(CASE WHEN @ScoreId = 2 THEN CT.max_scale ELSE CT.item_weight END)),0) END AS DECIMAL(36,2)) " & _
                                   "FROM hrcompetency_analysis_tran AS CT WITH (NOLOCK) " & _
                                   "JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = CT.analysisunkid " & _
                                   "WHERE CT.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = '" & xPeriodId & "' AND EM.assessedemployeeunkid = '" & xEmployeeId & "' AND EM.assessmodeid = '" & xAMode & "' AND CT.assessgroupunkid = '" & xAssessGrpId & "' "
                            'S.SANDEEP |21-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                            'REMOVED -------- @xval = CASE WHEN ISNULL(SUM( " & IIf(xIsUsedAgreedScore, "CT.agreedscore", "CT.result") & " *CT.item_weight),0)<=0 THEN 0 ELSE ISNULL(SUM(" & IIf(xIsUsedAgreedScore, "CT.agreedscore", "CT.result") & "*CT.item_weight),0)/ISNULL(SUM(CT.item_weight*(CASE WHEN @ScoreId = 2 THEN CT.max_scale ELSE CT.item_weight END)),0) END
                            'ADDED ---------- @xval = CAST(CASE WHEN ISNULL(SUM( " & IIf(xIsUsedAgreedScore, "CT.agreedscore", "CT.result") & " *CT.item_weight),0)<=0 THEN 0 ELSE ISNULL(SUM(" & IIf(xIsUsedAgreedScore, "CT.agreedscore", "CT.result") & "*CT.item_weight),0)/ISNULL(SUM(CT.item_weight*(CASE WHEN @ScoreId = 2 THEN CT.max_scale ELSE CT.item_weight END)),0) END AS DECIMAL(36,2))
                            'S.SANDEEP |21-AUG-2019| -- END

                            'Shani (23-Nov123-2016-2016) -- End

                            objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                            objDataOp.AddParameter("@ScoreId", SqlDbType.Int, eZeeDataType.INT_SIZE, xScoringOptionId)

                            objDataOp.ExecNonQuery(StrQ)

                            If objDataOp.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                Throw exForce
                            End If

                            If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                                xValue = 0
                            Else
                                xValue = objDataOp.GetParameterValue("@xval")
                            End If
                        Else
                            xValue = Process_Summary_Value(xScoringOptionId, xDataTab, xAMode)
                        End If


                    Case enAssess_Computation_Types.CMP_TOT_REV_ROW_SCORE_RATIO
                        If xDataTab Is Nothing Then
                            StrQ = "SELECT " & _
                                   "    @xval = CAST(CASE WHEN ISNULL(SUM(CT.result*CT.item_weight),0)<=0 THEN 0 ELSE ISNULL(SUM(CT.result*CT.item_weight),0)/ISNULL(SUM(CT.item_weight*(CASE WHEN @ScoreId = 2 THEN CT.max_scale ELSE CT.item_weight END)),0) END AS DECIMAL(36,2)) " & _
                                   "FROM hrcompetency_analysis_tran AS CT WITH (NOLOCK) " & _
                                   "JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = CT.analysisunkid " & _
                                   "WHERE CT.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = '" & xPeriodId & "' AND EM.assessedemployeeunkid = '" & xEmployeeId & "' AND EM.assessmodeid = '" & xAMode & "' AND CT.assessgroupunkid = '" & xAssessGrpId & "' "
                            'S.SANDEEP |21-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                            'REMOVED -------- @xval = CASE WHEN ISNULL(SUM(CT.result*CT.item_weight),0)<=0 THEN 0 ELSE ISNULL(SUM(CT.result*CT.item_weight),0)/ISNULL(SUM(CT.item_weight*(CASE WHEN @ScoreId = 2 THEN CT.max_scale ELSE CT.item_weight END)),0) END
                            'ADDED ---------- @xval = CAST(CASE WHEN ISNULL(SUM(CT.result*CT.item_weight),0)<=0 THEN 0 ELSE ISNULL(SUM(CT.result*CT.item_weight),0)/ISNULL(SUM(CT.item_weight*(CASE WHEN @ScoreId = 2 THEN CT.max_scale ELSE CT.item_weight END)),0) END AS DECIMAL(36,2))
                            'S.SANDEEP |21-AUG-2019| -- END

                            objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)
                            objDataOp.AddParameter("@ScoreId", SqlDbType.Int, eZeeDataType.INT_SIZE, xScoringOptionId)

                            objDataOp.ExecNonQuery(StrQ)

                            If objDataOp.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                Throw exForce
                            End If

                            If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                                xValue = 0
                            Else
                                xValue = objDataOp.GetParameterValue("@xval")
                            End If
                        Else
                            xValue = Process_Summary_Value(xScoringOptionId, xDataTab, xAMode)
                        End If


                        'S.SANDEEP [21 JAN 2015] -- END

                        'S.SANDEEP [29-NOV-2017] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 40
                    Case enAssess_Computation_Types.TOT_FINAL_SCORE_ALL_PERIOD  '31
                        'S.SANDEEP [28-Feb-2018] -- START
                        'ISSUE/ENHANCEMENT : {#0002069}
                               'StrQ = "declare @yearid int,@csvprdids nvarchar(max) " & _
                               '"set @yearid = isnull((select yearunkid from cfcommon_period_tran where periodunkid = '" & xPeriodId & "'),0) " & _
                               '"set @csvprdids = isnull((select isnull(stuff((select ','+convert(nvarchar(max),periodunkid) " & _
                               '"    from cfcommon_period_tran where yearunkid = @yearid and isactive = 1 and modulerefid = 5 " & _
                               '"    for xml path('')),1,1,''),'')),'') " & _
                               '"select " & _
                               '"    @xval = sum(csm.finaloverallscore) " & _
                               '"from hrassess_compute_score_master as csm " & _
                               '"where csm.assessmodeid = 1 and csm.isvoid = 0 " & _
                               '"and csm.periodunkid in (@csvprdids) and csm.employeeunkid  = '" & xEmployeeId & "' "

                        StrQ = "declare @yearid int,@csvprdids nvarchar(max) " & _
                               "set @yearid = isnull((select yearunkid from cfcommon_period_tran WITH (NOLOCK) where periodunkid = '" & xPeriodId & "'),0) " & _
                               "set @csvprdids = isnull((select isnull(stuff((select ','+convert(nvarchar(max),periodunkid) " & _
                               "    from cfcommon_period_tran WITH (NOLOCK) where yearunkid = @yearid and isactive = 1 and modulerefid = 5 " & _
                               "    for xml path('')),1,1,''),'')),'') " & _
                               "select " & _
                               "    @xval = sum(csm.finaloverallscore) " & _
                               "from hrassess_compute_score_master as csm WITH (NOLOCK) " & _
                               "where csm.assessmodeid = 1 and csm.isvoid = 0 " & _
                               "and csm.periodunkid in (select periodunkid from cfcommon_period_tran WITH (NOLOCK) where yearunkid = @yearid and isactive = 1 and modulerefid = 5) and csm.employeeunkid  = '" & xEmployeeId & "' "
                        'S.SANDEEP [28-Feb-2018] -- END
                        

                        objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)

                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                            xValue = 0
                        Else
                            xValue = objDataOp.GetParameterValue("@xval")
                        End If

                    Case enAssess_Computation_Types.TOT_ASSES_PERIOD_COUNT  '32

                        StrQ = "declare @yearid int " & _
                               "set @yearid = isnull((select yearunkid from cfcommon_period_tran WITH (NOLOCK) where periodunkid = '" & xPeriodId & "'),0) " & _
                               "select " & _
                               "   @xval = count(periodunkid) " & _
                               "from cfcommon_period_tran WITH (NOLOCK) " & _
                               "where yearunkid = @yearid and isactive = 1 and modulerefid = 5 "

                        objDataOp.AddParameter("@xval", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xValue, ParameterDirection.InputOutput)

                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        If IsDBNull(objDataOp.GetParameterValue("@xval")) Then
                            xValue = 0
                        Else
                            xValue = objDataOp.GetParameterValue("@xval")
                        End If

                        'S.SANDEEP [29-NOV-2017] -- END
                End Select
            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Computation_Variable_Value", mstrModuleName)
        Finally
        End Try
        Return xValue
    End Function

    'S.SANDEEP [21 JAN 2015] -- START
    Private Function Process_Summary_Value(ByVal iScoringOptionId As Integer, ByVal iDataTable As DataTable, ByVal iAssessMode As enAssessmentMode) As Decimal
        Dim mdecValue As Decimal = 0
        Try
            Dim xRowScore, xMaxScore As Decimal
            xRowScore = 0 : xMaxScore = 0
            Dim xScore As String = "iScore"
            Dim xIWeight As String = "item_weight"
            Dim xMaxScale As String = "max_scale"
            If iDataTable IsNot Nothing Then
                Select Case iAssessMode
                    Case enAssessmentMode.SELF_ASSESSMENT
                        If iDataTable.Columns.Contains("escore") = True Then
                            xScore = "escore"
                        End If
                        If iDataTable.Columns.Contains("eitem_weight") = True Then
                            xIWeight = "eitem_weight"
                        End If
                        If iDataTable.Columns.Contains("emax_scale") = True Then
                            xMaxScale = "emax_scale"
                        End If
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        If iDataTable.Columns.Contains("ascore") = True Then
                            xScore = "ascore"
                        End If
                        If iDataTable.Columns.Contains("aitem_weight") = True Then
                            xIWeight = "aitem_weight"
                        End If
                        If iDataTable.Columns.Contains("amax_scale") = True Then
                            xMaxScale = "amax_scale"
                        End If
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        If iDataTable.Columns.Contains("rscore") = True Then
                            xScore = "rscore"
                        End If
                        If iDataTable.Columns.Contains("ritem_weight") = True Then
                            xIWeight = "ritem_weight"
                        End If
                        If iDataTable.Columns.Contains("emax_scale") = True Then
                            xMaxScale = "rmax_scale"
                        End If
                End Select
            End If
            For Each xRow As DataRow In iDataTable.Rows

                If iDataTable.Columns.Contains("AUD") = True Then
                    If xRow.Item("AUD") = "D" Then Continue For
                End If

                xRowScore += CDec(xRow.Item(xScore) * xRow.Item(xIWeight))
                xMaxScore += CDec(IIf(iScoringOptionId = enScoringOption.SC_WEIGHTED_BASED, xRow.Item(xIWeight), xRow.Item(xMaxScale)) * xRow.Item(xIWeight))
            Next
            If xMaxScore <= 0 Or xRowScore <= 0 Then
                mdecValue = 0
            Else
                mdecValue = xRowScore / xMaxScore
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Process_Summary_Value", mstrModuleName)
        Finally
        End Try
        Return mdecValue
    End Function

    Public Function Get_Computation_TranVariables(ByVal xPeriodId As Integer, ByVal xFormulaTypeId As Integer) As DataTable
        Dim StrQ As String = String.Empty
        Dim xTranVariable As DataTable = Nothing
        Try
            Using objDo As New clsDataOperation
                Dim dsList As New DataSet

                'S.SANDEEP |16-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                'StrQ = "SELECT " & _
                '       "    ACT.computation_typeid " & _                
                '       "FROM hrassess_computation_tran AS ACT " & _
                '       "    JOIN hrassess_computation_master AS ACM ON ACM.computationunkid = ACT.computationunkid " & _
                '       "WHERE ACM.isvoid = 0 AND ACT.isvoid = 0 AND ACM.periodunkid = @xPeriodId AND ACM.formula_typeid = @xFormulaTypeId "

                StrQ = "SELECT " & _
                       "    ACT.computation_typeid " & _
                       "   ,ACM.formula_typeid " & _
                       "   ,CAST(0 AS BIT) AS blnSummry " & _
                       "FROM hrassess_computation_tran AS ACT WITH (NOLOCK) " & _
                       "    JOIN hrassess_computation_master AS ACM WITH (NOLOCK) ON ACM.computationunkid = ACT.computationunkid " & _
                       "WHERE ACM.isvoid = 0 AND ACT.isvoid = 0 AND ACM.periodunkid = @xPeriodId "

                If xFormulaTypeId > 0 Then
                    StrQ &= " AND ACM.formula_typeid = @xFormulaTypeId "
                End If
                'S.SANDEEP |16-JUL-2019| -- END
                

                objDo.AddParameter("@xPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
                objDo.AddParameter("@xFormulaTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, xFormulaTypeId)

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                'S.SANDEEP |09-DEC-2019| -- START
                'ISSUE/ENHANCEMENT : NORMALIZED FORMULA
                If dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("computation_typeid").ToString().StartsWith("1000")).Count > 0 Then
                    Dim strSummaryValue As String = String.Empty
                    strSummaryValue = enAssess_Computation_Types.BSC_TOT_EMP_ROW_SCORE_RATIO & "," & _
                                      enAssess_Computation_Types.BSC_TOT_ASR_ROW_SCORE_RATIO & "," & _
                                      enAssess_Computation_Types.BSC_TOT_REV_ROW_SCORE_RATIO & "," & _
                                      enAssess_Computation_Types.CMP_TOT_EMP_ROW_SCORE_RATIO & "," & _
                                      enAssess_Computation_Types.CMP_TOT_ASR_ROW_SCORE_RATIO & "," & _
                                      enAssess_Computation_Types.CMP_TOT_REV_ROW_SCORE_RATIO

                    Dim iList As IEnumerable(Of DataRow) = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("computation_typeid").ToString().StartsWith("1000")).ToList
                    If iList IsNot Nothing AndAlso iList.Count > 0 Then
                        For Each iRow In iList
                            Dim strNumber As String = iRow("computation_typeid").ToString().Replace("1000", "")
                            Dim iTemp() As DataRow = dsList.Tables(0).Select("formula_typeid = '" & strNumber & "' AND computation_typeid IN (" & strSummaryValue & ")")
                            If iTemp IsNot Nothing AndAlso iTemp.Length > 0 Then
                                Dim iAdd As DataRow = dsList.Tables(0).NewRow()
                                iAdd("computation_typeid") = iTemp(0)("computation_typeid")
                                iAdd("formula_typeid") = iRow("formula_typeid")
                                iAdd("blnSummry") = True
                                iRow("blnSummry") = True
                                dsList.Tables(0).Rows.Add(iAdd) : dsList.Tables(0).AcceptChanges()
                            End If
                        Next
                    End If
                End If
                'S.SANDEEP |09-DEC-2019| -- END

                xTranVariable = dsList.Tables(0).Copy
            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Computation_TranVariables", mstrModuleName)
        Finally
        End Try
        Return xTranVariable
    End Function
    'S.SANDEEP [21 JAN 2015] -- END

    'S.SANDEEP [19 FEB 2015] -- START
    Private Function Generate_Nested_Expression(ByVal xFormula_String As String, ByVal xPeriodId As Integer) As String
        Dim xNestedFormula As String = String.Empty
        Dim xOperand As String = String.Empty
        Dim xTemp As String = ""
        Dim xChildFormula As String = String.Empty
        Dim objComp_Tran As New clsassess_computation_tran
        Dim blnNotNormalized As Boolean = False
        Try
            For i As Integer = 1 To xFormula_String.Length
                If Mid(xFormula_String, i, 1) = "+" OrElse _
                   Mid(xFormula_String, i, 1) = "-" OrElse _
                   Mid(xFormula_String, i, 1) = "*" OrElse _
                   Mid(xFormula_String, i, 1) = "/" OrElse _
                   Mid(xFormula_String, i, 1) = "(" OrElse _
                   Mid(xFormula_String, i, 1) = ")" Then

                    If xTemp.Trim.Length > 0 Then
                        xNestedFormula += xOperand & xTemp
                    End If

                    xTemp = Trim(Mid(xFormula_String, i, 1))

                    If xTemp = "+" Then
                        If xOperand.Trim.Length > 0 Then
                            xOperand += "("
                        End If
                        xOperand += xTemp
                        xNestedFormula += xOperand
                        xTemp = "" : xOperand = ""

                    ElseIf xTemp = "-" Then
                        If xOperand.Trim.Length > 0 Then
                            xOperand += "("
                        End If
                        xOperand += xTemp
                        xNestedFormula += xOperand
                        xTemp = "" : xOperand = ""

                    ElseIf xTemp = "*" Then
                        If xOperand.Trim.Length > 0 Then
                            xOperand += "("
                        End If
                        xOperand += xTemp
                        xNestedFormula += xOperand
                        xTemp = "" : xOperand = ""

                    ElseIf xTemp = "/" Then
                        If xOperand.Trim.Length > 0 Then
                            xOperand += "("
                        End If
                        xOperand += xTemp
                        xNestedFormula += xOperand
                        xTemp = "" : xOperand = ""

                    ElseIf xTemp = "(" Then
                        xOperand = xTemp
                        xNestedFormula += xOperand
                        xTemp = "" : xOperand = ""

                    ElseIf xTemp = ")" Then
                        xOperand += xTemp
                        xNestedFormula += xOperand
                        xTemp = "" : xOperand = ""

                    Else
                        xOperand += xOperand
                        xNestedFormula += xOperand
                        xTemp = "" : xOperand = ""
                    End If
                ElseIf Mid(xFormula_String, i, 1) <> "#" Then
                    xTemp += Trim(Mid(xFormula_String, i, 1))
                Else
                    Dim xFrmlId As Integer = -1
                    If xTemp <> "" Then
                        xFrmlId = objComp_Tran.PredefineFormula(CInt(xTemp))
                        If xFrmlId > 0 Then
                            blnNotNormalized = True
                            Using objDo As New clsDataOperation
                                Dim StrQ As String = ""
                                StrQ = "SELECT " & _
                                       "    @formula = computation_formula " & _
                                       "FROM hrassess_computation_master " & _
                                       "WHERE periodunkid = '" & xPeriodId & "' AND formula_typeid = '" & CInt(xTemp.ToString.Substring(4)) & "' AND isvoid = 0"

                                objDo.AddParameter("@formula", SqlDbType.NVarChar, 4000, xChildFormula, ParameterDirection.InputOutput)

                                objDo.ExecNonQuery(StrQ)

                                If objDo.ErrorMessage <> "" Then
                                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                                End If

                                xChildFormula = objDo.GetParameterValue("@formula")
                            End Using

                            xNestedFormula += xOperand & "(" & xChildFormula & ")"
                            xTemp = "" : xOperand = ""
                        Else
                            If xTemp.Trim.Length > 0 Then
                                xNestedFormula += xOperand & "#" & xTemp & "#"
                                xOperand = "" : xTemp = ""
                            End If
                        End If
                    End If
                End If
            Next
            If xTemp.Trim.Length > 0 Then
                xNestedFormula += xOperand & xTemp
                xOperand = "" : xTemp = ""
            End If
            If blnNotNormalized = False Then
                mblnIsNormalized = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Generate_Nested_Expression", mstrModuleName)
        Finally
        End Try
        Return xNestedFormula
    End Function

    Public Function IsRecursiveFormula(ByVal xComputationTypeId As Integer, _
                                       ByVal xFormulaTypeId As Integer, _
                                       ByVal xPeriodId As Integer, _
                                       ByVal xFuncTypeId As Integer) As String
        Dim xMsg As String = String.Empty
        Dim StrQ As String = ""
        Try
            Using objDo As New clsDataOperation
                Select Case xFuncTypeId
                    Case enAssess_Functions_Mode.COMPUTATION_VARIABLES
                        StrQ = ""
                    Case enAssess_Functions_Mode.PRE_DEF_FORMULA
                        StrQ = ""
                End Select
            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsRecursiveFormula", mstrModuleName)
        Finally
        End Try
        Return xMsg
    End Function


    'S.SANDEEP [17 APR 2015] -- START
    Private Function IsValid_Computation_Type_Id(ByVal xCTypeId As Integer, ByVal IsItemTypeOfBSC As Boolean) As Boolean
        Dim blnFlag As Boolean = False
        Try
            If IsItemTypeOfBSC = True Then
                Select Case xCTypeId
                    Case enAssess_Computation_Types.BSC_ITEM_WEIGHT, enAssess_Computation_Types.BSC_ITEM_EMP_SCORE_VALUE, enAssess_Computation_Types.BSC_ITEM_ASR_SCORE_VALUE, enAssess_Computation_Types.BSC_ITEM_REV_SCORE_VALUE, enAssess_Computation_Types.BSC_PARENT_FIELD_WEIGHT, enAssess_Computation_Types.BSC_ITEM_MAX_SCORE_VALUE, enAssess_Computation_Types.BSC_OVERALL_WEIGHT, _
                         enAssess_Computation_Types.BSC_PARENT_ITEMS_ALL_PERSPECTIVE_COUNT, enAssess_Computation_Types.BSC_LINKED_ITEMS_ALL_PERSPECTIVE_COUNT, enAssess_Computation_Types.BSC_RATIO_VALUE, enAssess_Computation_Types.BSC_TOT_EMP_ROW_SCORE_RATIO, enAssess_Computation_Types.BSC_TOT_ASR_ROW_SCORE_RATIO, enAssess_Computation_Types.BSC_TOT_REV_ROW_SCORE_RATIO
                        blnFlag = True
                End Select
            Else
                Select Case xCTypeId
                    Case enAssess_Computation_Types.CMP_ITEM_WEIGHT, enAssess_Computation_Types.CMP_ITEM_EMP_SCORE_VALUE, enAssess_Computation_Types.CMP_ITEM_ASR_SCORE_VALUE, enAssess_Computation_Types.CMP_ITEM_REV_SCORE_VALUE, enAssess_Computation_Types.CMP_CATEGORY_WEIGHT, enAssess_Computation_Types.CMP_ITEM_MAX_SCORE_VALUE, enAssess_Computation_Types.CMP_ASSESSMENT_GROUP_WEIGHT, enAssess_Computation_Types.CMP_OVERALL_WEIGHT, _
                         enAssess_Computation_Types.CMP_ASSESSMENT_GROUP_COUNT, enAssess_Computation_Types.CMP_ITEMS_PER_CATEGORY_COUNT, enAssess_Computation_Types.CMP_CATEGORY_PER_GROUP_COUNT, enAssess_Computation_Types.CMP_ITEMS_ALL_CATEGORY_GROUP_COUNT, enAssess_Computation_Types.CMP_CATEGORY_ALL_GROUP_COUNT, enAssess_Computation_Types.CMP_RATIO_VALUE, enAssess_Computation_Types.CMP_TOT_EMP_ROW_SCORE_RATIO, enAssess_Computation_Types.CMP_TOT_ASR_ROW_SCORE_RATIO, enAssess_Computation_Types.CMP_TOT_REV_ROW_SCORE_RATIO
                        blnFlag = True
                End Select
            End If

            Return blnFlag
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid_Computation_Type_Id", mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [17 APR 2015] -- END
    'S.SANDEEP [19 FEB 2015] -- END


    'Shani (24-May-2016) -- Start

    Public Function GetFormulaStrings(ByVal xPeriodIds As Integer) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim dsFormula As DataSet = Nothing
        Dim StrQ As String = String.Empty
        Dim xFormulaString As String = String.Empty
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT " & _
                       "     computationunkid " & _
                       "    ,computation_formula " & _
                       "    ,formula_typeid " & _
                       "    ,'' AS formula_string " & _
                       "FROM hrassess_computation_master " & _
                       "WHERE periodunkid = '" & xPeriodIds & "' AND isvoid = 0"

                dsFormula = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If
            End Using


            For Each dRow As DataRow In dsFormula.Tables(0).Rows
                xFormulaString = ""
                If dRow.Item("computation_formula").ToString.Trim.Length > 0 Then
                    mblnIsNormalized = False
                    xFormulaString = dRow.Item("computation_formula")

                    While mblnIsNormalized = False
                        xFormulaString = Generate_Nested_Expression(xFormulaString, xPeriodIds)
                    End While
                End If
                dRow.Item("formula_string") = xFormulaString
            Next
            dtTable = dsFormula.Tables(0).Copy
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetFormulaStrings", mstrModuleName)
        Finally
        End Try
        Return dtTable
    End Function

    'Shani (23-Nov-2016) -- Start
    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
    'Public Sub Get_All_Compution_value(ByVal xPeriodId As Integer, ByVal xScoringOptionId As Integer, ByVal xStrCsvEmployee As String, ByVal dtEmployeeAsOnDate As Date)
    Public Sub Get_All_Compution_value(ByVal xPeriodId As Integer, ByVal xScoringOptionId As Integer, ByVal xStrCsvEmployee As String, ByVal dtEmployeeAsOnDate As Date, ByVal blnUsedAgreedScore As Boolean, ByVal blnIsSelfAssignCompetencies As Boolean)
        'Shani (23-Nov123-2016-2016) -- End
        Dim StrQ As String = String.Empty

        Dim xLinkedFieldId, xExOrder As Integer
        xLinkedFieldId = 0 : xExOrder = 0

        Dim xTableName, xColName, xParentCol, xParentTabName As String
        xTableName = "" : xColName = "" : xParentCol = "" : xParentTabName = ""

        Try
            Dim objFMapping As New clsAssess_Field_Mapping
            xLinkedFieldId = objFMapping.Get_Map_FieldId(xPeriodId)
            objFMapping = Nothing

            Dim objFMaster As New clsAssess_Field_Master
            xExOrder = objFMaster.Get_Field_ExOrder(xLinkedFieldId)
            objFMaster = Nothing
            If xExOrder <= 0 Then Exit Sub
            Select Case xExOrder
                Case enWeight_Types.WEIGHT_FIELD1
                    xTableName = "hrassess_empfield1_master"
                    xColName = "empfield1unkid"
                    xParentCol = "empfield1unkid"
                    xParentTabName = "hrassess_empfield1_master"
                Case enWeight_Types.WEIGHT_FIELD2
                    xTableName = "hrassess_empfield2_master"
                    xColName = "empfield2unkid"
                    xParentCol = "empfield1unkid"
                    xParentTabName = "hrassess_empfield1_master"
                Case enWeight_Types.WEIGHT_FIELD3
                    xTableName = "hrassess_empfield3_master"
                    xColName = "empfield3unkid"
                    xParentCol = "empfield2unkid"
                    xParentTabName = "hrassess_empfield2_master"
                Case enWeight_Types.WEIGHT_FIELD4
                    xTableName = "hrassess_empfield4_master"
                    xColName = "empfield4unkid"
                    xParentCol = "empfield3unkid"
                    xParentTabName = "hrassess_empfield3_master"
                Case enWeight_Types.WEIGHT_FIELD5
                    xTableName = "hrassess_empfield5_master"
                    xColName = "empfield5unkid"
                    xParentCol = "empfield4unkid"
                    xParentTabName = "hrassess_empfield4_master"
            End Select

            Using objDataOp As New clsDataOperation
                StrQ = "      SELECT " & _
                       "           weight AS iweight " & _
                       "          ," & xColName & " AS Itemids " & _
                       "          ,0 AS assessmodeid " & _
                       "          ," & enAssess_Computation_Types.BSC_ITEM_WEIGHT & " AS compute_type " & _
                       "          ,1 AS isBsc " & _
                       "          ,employeeunkid AS employeeunkid " & _
                       "          ,-1 AS AssGrpIds " & _
                       "     FROM " & xTableName & " WITH (NOLOCK) " & _
                       "     WHERE isvoid = 0 AND periodunkid = '" & xPeriodId & "' AND employeeunkid IN (" & xStrCsvEmployee & ") " & _
                       " UNION ALL " & _
                       "    SELECT " & _
                       "         iweight " & _
                       "        ,Itemids " & _
                       "        ,assessmodeid " & _
                       "        ,compute_type " & _
                       "        ,isBsc " & _
                       "        ,employeeunkid " & _
                       "        ,AssGrpIds " & _
                       "    FROM " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             CAST(CASE WHEN EM.assessmodeid =  " & enAssessmentMode.APPRAISER_ASSESSMENT & "  THEN " & IIf(blnUsedAgreedScore, "GT.agreedscore ", "GT.result ") & " ELSE GT.result END AS DECIMAL(36,2)) AS iweight " & _
                       "            ,GT." & xColName & " AS Itemids " & _
                       "            ,EM.assessmodeid AS assessmodeid " & _
                       "            ,CASE WHEN EM.assessmodeid = " & enAssessmentMode.SELF_ASSESSMENT & " THEN " & enAssess_Computation_Types.BSC_ITEM_EMP_SCORE_VALUE & " " & _
                       "                  WHEN EM.assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & " THEN " & enAssess_Computation_Types.BSC_ITEM_ASR_SCORE_VALUE & " " & _
                       "                  WHEN EM.assessmodeid = " & enAssessmentMode.REVIEWER_ASSESSMENT & " THEN " & enAssess_Computation_Types.BSC_ITEM_REV_SCORE_VALUE & " " & _
                       "             END AS compute_type " & _
                       "            ,1 AS isBsc " & _
                       "            ,CASE WHEN EM.assessmodeid = " & enAssessmentMode.SELF_ASSESSMENT & " THEN EM.selfemployeeunkid " & _
                       "                  ELSE EM.assessedemployeeunkid " & _
                       "             END AS employeeunkid " & _
                       "            ,-1 AS AssGrpIds " & _
                       "        FROM hrevaluation_analysis_master AS EM WITH (NOLOCK) " & _
                       "            JOIN hrgoals_analysis_tran AS GT WITH (NOLOCK) ON EM.analysisunkid = GT.analysisunkid " & _
                       "        WHERE EM.isvoid = 0 AND GT.isvoid = 0 AND EM.periodunkid = '" & xPeriodId & "' " & _
                       "    ) AS A WHERE A.employeeunkid IN (" & xStrCsvEmployee & ") " & _
                       " UNION ALL  " & _
                       "     SELECT " & _
                       "           A2.iweight AS iweight " & _
                       "          ,A1." & xColName & " AS Itemids " & _
                       "          ,0 AS assessmodeid " & _
                       "          ," & enAssess_Computation_Types.BSC_PARENT_FIELD_WEIGHT & " AS compute_type " & _
                       "          ,1 AS isBsc " & _
                       "          ,A1.employeeunkid AS employeeunkid " & _
                       "          ,-1 AS AssGrpIds " & _
                       "     FROM " & xTableName & " AS A1 WITH (NOLOCK) " & _
                       "          JOIN " & _
                       "          ( " & _
                       "               SELECT " & _
                       "                    sum(weight) AS iweight " & _
                       "                    ," & xParentCol & " " & _
                       "               FROM " & xTableName & " WITH (NOLOCK) " & _
                       "               WHERE periodunkid = '" & xPeriodId & "' " & _
                       "               GROUP BY " & xParentCol & " " & _
                       "          ) AS A2 ON A2." & xParentCol & " = A1." & xParentCol & " " & _
                       "     WHERE A1.periodunkid = '" & xPeriodId & "' AND A1.employeeunkid IN (" & xStrCsvEmployee & ") "
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'REMOVED -------- @xval = CASE WHEN EM.assessmodeid =  " & enAssessmentMode.APPRAISER_ASSESSMENT & "  THEN " & IIf(blnUsedAgreedScore, "GT.agreedscore ", "GT.result ") & " ELSE GT.result END AS iweight
                'ADDED ---------- @xval = CAST(CASE WHEN EM.assessmodeid =  " & enAssessmentMode.APPRAISER_ASSESSMENT & "  THEN " & IIf(blnUsedAgreedScore, "GT.agreedscore ", "GT.result ") & " ELSE GT.result END AS DECIMAL(36,2)) AS iweight
                'S.SANDEEP |21-AUG-2019| -- END

                If xScoringOptionId = enScoringOption.SC_SCALE_BASED Then
                    StrQ &= "UNION ALL " & _
                            "     SELECT " & _
                            "           MAX(hrassess_scale_master.scale) AS iweight " & _
                            "          ," & xTableName & "." & xColName & " AS Itemids " & _
                            "          ,0 AS assessmodeid " & _
                            "          ," & enAssess_Computation_Types.BSC_ITEM_MAX_SCORE_VALUE & " AS compute_type " & _
                            "          ,1 AS isBsc " & _
                            "          ," & xTableName & ".employeeunkid AS employeeunkid " & _
                            "          ,-1 AS AssGrpIds " & _
                            "     FROM hrassess_scalemapping_tran WITH (NOLOCK) " & _
                            "          JOIN hrassess_scale_master WITH (NOLOCK) ON hrassess_scale_master.scalemasterunkid = hrassess_scalemapping_tran.scalemasterunkid " & _
                            "          JOIN hrassess_empfield1_master WITH (NOLOCK) ON hrassess_empfield1_master.perspectiveunkid = hrassess_scalemapping_tran.perspectiveunkid " & _
                            "          LEFT JOIN hrassess_owrfield1_master WITH (NOLOCK) ON hrassess_empfield1_master.owrfield1unkid = hrassess_owrfield1_master.owrfield1unkid " & _
                            "          LEFT JOIN hrassess_coyfield1_master WITH (NOLOCK) ON hrassess_coyfield1_master.coyfield1unkid = hrassess_owrfield1_master.coyfield1unkid " & _
                            "          LEFT JOIN hrassess_empfield2_master WITH (NOLOCK) ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid " & _
                            "          LEFT JOIN hrassess_empfield3_master WITH (NOLOCK) ON hrassess_empfield3_master.empfield2unkid = hrassess_empfield2_master.empfield2unkid " & _
                            "          LEFT JOIN hrassess_empfield4_master WITH (NOLOCK) ON hrassess_empfield4_master.empfield3unkid = hrassess_empfield3_master.empfield3unkid " & _
                            "          LEFT JOIN hrassess_empfield5_master WITH (NOLOCK) ON hrassess_empfield5_master.empfield4unkid = hrassess_empfield4_master.empfield4unkid " & _
                            "     WHERE hrassess_scalemapping_tran.isvoid  = 0 AND " & _
                            "            hrassess_scale_master.isactive = 1 AND " & _
                            "            " & xTableName & ".isvoid = 0 AND " & xTableName & ".periodunkid = '" & xPeriodId & "' AND " & xTableName & ".employeeunkid IN (" & xStrCsvEmployee & ")" & _
                            "     GROUP BY " & xTableName & "." & xColName & "," & xTableName & ".employeeunkid "
                End If
                '[ ADDED EMPLOYEE ID VALUE IN BELOW UNION OF CMP_ITEM_WEIGHT] -- SANDEEP
                StrQ &= "UNION ALL " & _
                        "     SELECT DISTINCT " & _
                        "           CT.weight AS iweight " & _
                        "          ,CT.competenciesunkid AS Itemids " & _
                        "          ,0 AS assessmodeid " & _
                        "          ," & enAssess_Computation_Types.CMP_ITEM_WEIGHT & " AS compute_type " & _
                        "          ,0 AS isBsc " & _
                        "          ,ISNULL(CASE WHEN EM.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' THEN EM.selfemployeeunkid ELSE EM.assessedemployeeunkid END,CM.employeeunkid) AS employeeunkid " & _
                        "          ,CM.assessgroupunkid AS AssGrpIds " & _
                        "     FROM hrassess_competence_assign_tran AS CT WITH (NOLOCK) " & _
                        "          JOIN hrassess_competence_assign_master AS CM WITH (NOLOCK) ON CM.assigncompetenceunkid = CT.assigncompetenceunkid " & _
                        "          JOIN hrcompetency_analysis_tran CAT WITH (NOLOCK) ON CM.assessgroupunkid = CAT.assessgroupunkid AND CT.competenciesunkid = CAT.competenciesunkid " & _
                        "          JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = CAT.analysisunkid AND CM.periodunkid = EM.periodunkid "
                If blnIsSelfAssignCompetencies Then
                    StrQ &= "               AND ISNULL(CASE WHEN EM.assessmodeid = '1' THEN EM.selfemployeeunkid ELSE EM.assessedemployeeunkid END, CM.employeeunkid)  = CM.employeeunkid "
                End If
                StrQ &= "     WHERE CT.isvoid = 0 AND CM.isvoid = 0 AND CM.periodunkid = '" & xPeriodId & "' AND EM.isvoid = 0 "
                If blnIsSelfAssignCompetencies Then
                    StrQ &= " AND CM.employeeunkid IN (" & xStrCsvEmployee & ") "
                End If
                StrQ &= "UNION ALL " & _
                        "    SELECT " & _
                        "         iweight " & _
                        "        ,Itemids " & _
                        "        ,assessmodeid " & _
                        "        ,compute_type " & _
                        "        ,isBsc " & _
                        "        ,employeeunkid " & _
                        "        ,AssGrpIds " & _
                        "    FROM " & _
                        "    ( " & _
                        "       SELECT " & _
                        "            CAST(ISNULL(CASE WHEN EM.assessmodeid =  " & enAssessmentMode.APPRAISER_ASSESSMENT & "  THEN " & IIf(blnUsedAgreedScore, "CAT.agreedscore ", "CAT.result ") & " ELSE CAT.result END,0) AS DECIMAL(36,2)) AS iweight " & _
                        "           ,cat.competenciesunkid AS Itemids " & _
                        "           ,EM.assessmodeid AS assessmodeid " & _
                        "           ,CASE WHEN EM.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' THEN '" & enAssess_Computation_Types.CMP_ITEM_EMP_SCORE_VALUE & "' " & _
                        "                 WHEN EM.assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' THEN '" & enAssess_Computation_Types.CMP_ITEM_ASR_SCORE_VALUE & "' " & _
                        "                 WHEN EM.assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' THEN '" & enAssess_Computation_Types.CMP_ITEM_REV_SCORE_VALUE & "' " & _
                        "            END AS compute_type " & _
                        "           ,0 AS isBsc " & _
                        "           ,CASE WHEN EM.assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' THEN EM.selfemployeeunkid " & _
                        "                 ELSE EM.assessedemployeeunkid " & _
                        "            END AS employeeunkid " & _
                        "            ,cat.assessgroupunkid AS AssGrpIds " & _
                        "       FROM hrevaluation_analysis_master AS EM WITH (NOLOCK) " & _
                        "           JOIN hrcompetency_analysis_tran AS CAT WITH (NOLOCK) ON CAT.analysisunkid = EM.analysisunkid " & _
                        "       WHERE CAT.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = '" & xPeriodId & "' " & _
                        "   ) AS A WHERE A.employeeunkid IN (" & xStrCsvEmployee & ") " & _
                        "UNION ALL " & _
                        "     SELECT " & _
                        "         A.iweight AS iweight " & _
                        "        ,hrassess_competence_assign_tran.competenciesunkid AS Itemids " & _
                        "        ,0 AS assessmodeid " & _
                        "        ," & enAssess_Computation_Types.CMP_CATEGORY_WEIGHT & " AS compute_type " & _
                        "        ,0 AS isBsc " & _
                        "        ,0 AS employeeunkid " & _
                        "        ,hrassess_competence_assign_master.assessgroupunkid AS AssGrpIds " & _
                        "     FROM hrassess_competence_assign_master WITH (NOLOCK) " & _
                        "        JOIN hrassess_competence_assign_tran WITH (NOLOCK) ON hrassess_competence_assign_tran.assigncompetenceunkid = hrassess_competence_assign_master.assigncompetenceunkid " & _
                        "        JOIN " & _
                        "        ( " & _
                        "            SELECT " & _
                        "                ISNULL(SUM(CAT.weight),0) AS iweight " & _
                        "                ,CM.competenciesunkid " & _
                        "            FROM hrassess_competence_assign_tran AS CAT WITH (NOLOCK) " & _
                        "                JOIN hrassess_competence_assign_master AS CAM WITH (NOLOCK) ON CAM.assigncompetenceunkid = CAT.assigncompetenceunkid " & _
                        "                JOIN hrassess_competencies_master AS CM WITH (NOLOCK) ON CAT.competenciesunkid = CM.competenciesunkid " & _
                        "            WHERE CAM.isvoid = 0 AND CAT.isvoid = 0 AND " & _
                        "            CM.competence_categoryunkid IN " & _
                        "            ( " & _
                        "               SELECT CM.competence_categoryunkid " & _
                        "               FROM hrassess_competence_assign_tran AS CAT WITH (NOLOCK) " & _
                        "                    JOIN hrassess_competence_assign_master AS CAM WITH (NOLOCK) ON CAM.assigncompetenceunkid = CAT.assigncompetenceunkid " & _
                        "                    JOIN hrassess_competencies_master AS CM WITH (NOLOCK) ON CAT.competenciesunkid = CM.competenciesunkid " & _
                        "               WHERE CAM.isvoid = 0 AND CAT.isvoid = 0 " & _
                        "                AND CAM.periodunkid = '" & xPeriodId & "' " & _
                        "            ) " & _
                        "          GROUP BY cm.competenciesunkid,CAM.employeeunkid " & _
                        "     ) AS A ON A.competenciesunkid = hrassess_competence_assign_tran.competenciesunkid "
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'REMOVED -------- @xval = ISNULL(CASE WHEN EM.assessmodeid =  " & enAssessmentMode.APPRAISER_ASSESSMENT & "  THEN " & IIf(blnUsedAgreedScore, "CAT.agreedscore ", "CAT.result ") & " ELSE CAT.result END,0) AS iweight
                'ADDED ---------- @xval = CAST(ISNULL(CASE WHEN EM.assessmodeid =  " & enAssessmentMode.APPRAISER_ASSESSMENT & "  THEN " & IIf(blnUsedAgreedScore, "CAT.agreedscore ", "CAT.result ") & " ELSE CAT.result END,0) AS DECIMAL(36,2)) AS iweight
                'S.SANDEEP |21-AUG-2019| -- END

                If xScoringOptionId = enScoringOption.SC_SCALE_BASED Then
                    StrQ &= "UNION ALL " & _
                            "     " & _
                            "     SELECT " & _
                            "           MAX(scale) AS iweight " & _
                            "          ,CM.competenciesunkid AS Itemids " & _
                            "          ,0 AS assessmodeid " & _
                            "          ," & enAssess_Computation_Types.CMP_ITEM_MAX_SCORE_VALUE & " AS compute_type " & _
                            "          ,0 AS isBsc " & _
                            "          ,0 AS employeeunkid " & _
                            "          ,CAM.assessgroupunkid AS AssGrpIds " & _
                            "     FROM hrassess_scale_master WITH (NOLOCK) " & _
                            "          JOIN hrassess_competencies_master AS CM WITH (NOLOCK) ON hrassess_scale_master.scalemasterunkid = CM.scalemasterunkid " & _
                            "          JOIN hrassess_competence_assign_tran AS CAT WITH (NOLOCK) ON CAT.competenciesunkid = CM.competenciesunkid " & _
                            "          JOIN hrassess_competence_assign_master AS CAM WITH (NOLOCK) ON CAM.assigncompetenceunkid = CAT.assigncompetenceunkid " & _
                            "     WHERE hrassess_scale_master.isactive = 1 and cm.isactive = 1 and CAT.isvoid = 0 AND CAM.isvoid = 0 " & _
                            "               AND hrassess_scale_master.periodunkid = '" & xPeriodId & "' " & _
                            "     GROUP BY CM.competenciesunkid,CAM.assessgroupunkid "
                End If
                StrQ &= "UNION ALL " & _
                        "     SELECT " & _
                        "          SUM(weight) AS iweight " & _
                        "          ,0 AS Itemids " & _
                        "          ,0 AS assessmodeid " & _
                        "          ," & enAssess_Computation_Types.CMP_ASSESSMENT_GROUP_WEIGHT & " AS compute_type " & _
                        "          ,0 AS isBsc " & _
                        "          ,0 AS employeeunkid " & _
                        "          ,assessgroupunkid AS AssGrpIds " & _
                        "     FROM hrassess_group_master WITH (NOLOCK) " & _
                        "     WHERE isactive = 1 " & _
                        "     GROUP BY assessgroupunkid " & _
                        " UNION ALL " & _
                        "     SELECT " & _
                        "          COUNT(" & xParentTabName & "." & xParentCol & ") AS iweight " & _
                        "          ,0 AS Itemids " & _
                        "          ,0 AS assessmodeid " & _
                        "          ," & enAssess_Computation_Types.BSC_PARENT_ITEMS_ALL_PERSPECTIVE_COUNT & " AS compute_type " & _
                        "          ,1 AS isBsc " & _
                        "          ," & xParentTabName & ".employeeunkid AS employeeunkid " & _
                        "          ,0 AS AssGrpIds " & _
                        "     FROM " & xParentTabName & " WITH (NOLOCK) " & _
                        "     WHERE isvoid = 0 AND periodunkid = '" & xPeriodId & "' AND " & xParentTabName & ".employeeunkid IN (" & xStrCsvEmployee & ")" & _
                        "     GROUP BY " & xParentTabName & ".employeeunkid " & _
                        " UNION ALL " & _
                        "     SELECT " & _
                        "          COUNT(" & xTableName & "." & xColName & ") AS iweight " & _
                        "          ,0 AS Itemids " & _
                        "          ,0 AS assessmodeid " & _
                        "          ," & enAssess_Computation_Types.BSC_LINKED_ITEMS_ALL_PERSPECTIVE_COUNT & " AS compute_type " & _
                        "          ,1 AS isBsc " & _
                        "          ," & xTableName & ".employeeunkid AS employeeunkid " & _
                        "          ,0 AS AssGrpIds " & _
                        "     FROM " & xTableName & " WITH (NOLOCK) " & _
                        "     WHERE isvoid = 0 AND periodunkid = '" & xPeriodId & "' AND " & xTableName & ".employeeunkid IN (" & xStrCsvEmployee & ")" & _
                        "     GROUP BY " & xTableName & ".employeeunkid " & _
                        " UNION ALL  " & _
                        "     SELECT " & _
                        "          COUNT(DISTINCT CT.competenciesunkid) AS iweight " & _
                        "          ,0 AS Itemids " & _
                        "          ,0 AS assessmodeid " & _
                        "          ," & enAssess_Computation_Types.CMP_ASSESSMENT_GROUP_COUNT & " AS compute_type " & _
                        "          ,0 AS isBsc " & _
                        "          ," & IIf(blnIsSelfAssignCompetencies, "CM.employeeunkid", "0") & " AS employeeunkid " & _
                        "          ,CM.assessgroupunkid AS AssGrpIds " & _
                        "     FROM hrassess_competence_assign_tran AS CT WITH (NOLOCK) " & _
                        "          JOIN hrassess_competence_assign_master AS CM WITH (NOLOCK) ON CM.assigncompetenceunkid = CT.assigncompetenceunkid " & _
                        "     WHERE CM.isvoid = 0 AND CT.isvoid = 0 " & _
                        "           AND CM.periodunkid = '" & xPeriodId & "' " & _
                        "     GROUP BY CM.assessgroupunkid " & IIf(blnIsSelfAssignCompetencies, ",CM.employeeunkid", "") & " " & _
                        " UNION ALL  " & _
                        "     SELECT " & _
                        "           A.iWeight AS iweight " & _
                        "          ,hrassess_competencies_master.competenciesunkid AS Itemids " & _
                        "          ,0 AS assessmodeid " & _
                        "          ," & enAssess_Computation_Types.CMP_ITEMS_PER_CATEGORY_COUNT & " AS compute_type " & _
                        "          ,0 AS isBsc " & _
                        "          ," & IIf(blnIsSelfAssignCompetencies, "A.employeeunkid", "0") & " AS employeeunkid " & _
                        "          ,0 AS AssGrpIds " & _
                        "     FROM hrassess_competencies_master WITH (NOLOCK) " & _
                        "          JOIN " & _
                        "          ( " & _
                        "               SELECT " & _
                        "                    COUNT(DISTINCT CT.competenciesunkid) AS iWeight " & _
                        "                    ,hrassess_competencies_master.competence_categoryunkid " & _
                        "                    " & IIf(blnIsSelfAssignCompetencies, ",CM.employeeunkid", "") & " " & _
                        "               FROM hrassess_competence_assign_tran AS CT WITH (NOLOCK) " & _
                        "                    JOIN hrassess_competence_assign_master AS CM WITH (NOLOCK) ON CM.assigncompetenceunkid = CT.assigncompetenceunkid " & _
                        "                    JOIN hrassess_competencies_master WITH (NOLOCK) ON hrassess_competencies_master.competenciesunkid = CT.competenciesunkid " & _
                        "               WHERE CM.isvoid = 0 AND CT.isvoid = 0 AND CM.periodunkid = '" & xPeriodId & "' " & _
                        "               GROUP BY hrassess_competencies_master.competence_categoryunkid " & IIf(blnIsSelfAssignCompetencies, ",CM.employeeunkid", "") & " " & _
                        "          ) AS A ON a.competence_categoryunkid = hrassess_competencies_master.competence_categoryunkid " & _
                        " UNION ALL " & _
                        "     SELECT " & _
                        "           COUNT(DISTINCT COM.competence_categoryunkid) AS iweight " & _
                        "          ,0 AS Itemids " & _
                        "          ,0 AS assessmodeid " & _
                        "          ," & enAssess_Computation_Types.CMP_CATEGORY_PER_GROUP_COUNT & " AS compute_type " & _
                        "          ,0 AS isBsc " & _
                        "          ," & IIf(blnIsSelfAssignCompetencies, "CM.employeeunkid", "0") & " AS employeeunkid " & _
                        "          ,CM.assessgroupunkid AS AssGrpIds " & _
                        "     FROM hrassess_competence_assign_tran AS CT WITH (NOLOCK) " & _
                        "          JOIN hrassess_competence_assign_master AS CM WITH (NOLOCK) ON CM.assigncompetenceunkid = CT.assigncompetenceunkid " & _
                        "          JOIN hrassess_competencies_master AS COM WITH (NOLOCK) ON COM.competenciesunkid = CT.competenciesunkid " & _
                        "     WHERE CM.isvoid = 0 AND CT.isvoid = 0 AND CM.periodunkid = '" & xPeriodId & "' " & _
                        "     GROUP BY CM.assessgroupunkid " & IIf(blnIsSelfAssignCompetencies, ",CM.employeeunkid", "") & " " & _
                        " UNION ALL " & _
                        "    SELECT " & _
                        "         iweight " & _
                        "        ,Itemids " & _
                        "        ,assessmodeid " & _
                        "        ,compute_type " & _
                        "        ,isBsc " & _
                        "        ,employeeunkid " & _
                        "        ,AssGrpIds " & _
                        "    FROM " & _
                        "    ( " & _
                        "       SELECT " & _
                        "            CAST(CASE WHEN SUM(CASE WHEN EM.assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & " THEN " & IIf(blnUsedAgreedScore, "GT.agreedscore", "GT.result") & " ELSE GT.result END * GT.item_weight) = 0 THEN 0 " & _
                        "                 WHEN SUM(GT.item_weight*(CASE WHEN " & xScoringOptionId & " = 2 THEN GT.max_scale ELSE GT.item_weight END)) = 0 THEN 0 " & _
                        "                 ELSE SUM(CASE WHEN EM.assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & " THEN " & IIf(blnUsedAgreedScore, "GT.agreedscore", "GT.result") & " ELSE GT.result END * GT.item_weight)/SUM(GT.item_weight*(CASE WHEN " & xScoringOptionId & " = 2 THEN GT.max_scale ELSE GT.item_weight END)) " & _
                        "                 END AS DECIMAL(36,2)) AS iweight " & _
                        "          ,0 AS Itemids " & _
                        "          ,EM.assessmodeid AS assessmodeid " & _
                        "          ,CASE WHEN EM.assessmodeid = " & enAssessmentMode.SELF_ASSESSMENT & " THEN " & enAssess_Computation_Types.BSC_TOT_EMP_ROW_SCORE_RATIO & " " & _
                        "                WHEN EM.assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & " THEN " & enAssess_Computation_Types.BSC_TOT_ASR_ROW_SCORE_RATIO & " " & _
                        "                WHEN EM.assessmodeid = " & enAssessmentMode.REVIEWER_ASSESSMENT & " THEN " & enAssess_Computation_Types.BSC_TOT_REV_ROW_SCORE_RATIO & " " & _
                        "           END AS compute_type " & _
                        "          ,1 AS isBsc " & _
                        "          ,CASE WHEN EM.assessmodeid = " & enAssessmentMode.SELF_ASSESSMENT & " THEN EM.selfemployeeunkid " & _
                        "                 ELSE EM.assessedemployeeunkid " & _
                        "           END AS employeeunkid " & _
                        "          ,0 AS AssGrpIds " & _
                        "       FROM hrgoals_analysis_tran GT WITH (NOLOCK) " & _
                        "           JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = GT.analysisunkid " & _
                        "       WHERE EM.isvoid = 0 AND GT.isvoid = 0 AND EM.periodunkid = '" & xPeriodId & "' " & _
                        "       GROUP BY EM.assessmodeid,EM.selfemployeeunkid,EM.assessedemployeeunkid " & _
                        "   ) AS A WHERE A.employeeunkid IN (" & xStrCsvEmployee & ") " & _
                        " UNION ALL " & _
                        "    SELECT " & _
                        "         iweight " & _
                        "        ,Itemids " & _
                        "        ,assessmodeid " & _
                        "        ,compute_type " & _
                        "        ,isBsc " & _
                        "        ,employeeunkid " & _
                        "        ,AssGrpIds " & _
                        "    FROM " & _
                        "    ( " & _
                        "       SELECT " & _
                        "           CAST(CASE WHEN SUM(CASE WHEN EM.assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & " THEN " & IIf(blnUsedAgreedScore, "CT.agreedscore", "CT.result") & " ELSE CT.result END * CT.item_weight) = 0 THEN 0 " & _
                        "                WHEN SUM(CT.item_weight*(CASE WHEN " & xScoringOptionId & " = 2 THEN CT.max_scale ELSE CT.item_weight END)) = 0 THEN 0 " & _
                        "                ELSE SUM(CASE WHEN EM.assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & " THEN " & IIf(blnUsedAgreedScore, "CT.agreedscore", "CT.result") & " ELSE CT.result END * CT.item_weight)/SUM(CT.item_weight*(CASE WHEN " & xScoringOptionId & " = 2 THEN CT.max_scale ELSE CT.item_weight END)) " & _
                        "                END AS DECIMAL(36,2)) AS iweight " & _
                        "          ,0 AS Itemids " & _
                        "          ,EM.assessmodeid AS assessmodeid " & _
                        "          ,CASE WHEN EM.assessmodeid = " & enAssessmentMode.SELF_ASSESSMENT & "  THEN " & enAssess_Computation_Types.CMP_TOT_EMP_ROW_SCORE_RATIO & " " & _
                        "                WHEN EM.assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & " THEN " & enAssess_Computation_Types.CMP_TOT_ASR_ROW_SCORE_RATIO & " " & _
                        "                WHEN EM.assessmodeid = " & enAssessmentMode.REVIEWER_ASSESSMENT & "  THEN " & enAssess_Computation_Types.CMP_TOT_REV_ROW_SCORE_RATIO & " " & _
                        "           END AS compute_type " & _
                        "          ,0 AS isBsc " & _
                        "          ,CASE WHEN EM.assessmodeid = " & enAssessmentMode.SELF_ASSESSMENT & " THEN EM.selfemployeeunkid " & _
                        "                 ELSE EM.assessedemployeeunkid " & _
                        "           END AS employeeunkid " & _
                        "          ,CT.assessgroupunkid AS AssGrpIds " & _
                        "       FROM hrcompetency_analysis_tran AS CT WITH (NOLOCK) " & _
                        "          JOIN hrevaluation_analysis_master AS EM WITH (NOLOCK) ON EM.analysisunkid = CT.analysisunkid " & _
                        "       WHERE CT.isvoid = 0 AND EM.isvoid = 0 AND EM.periodunkid = '" & xPeriodId & "' " & _
                        "       GROUP BY  EM.assessmodeid,EM.selfemployeeunkid,EM.assessedemployeeunkid,CT.assessgroupunkid " & _
                        "   ) AS A WHERE A.employeeunkid IN (" & xStrCsvEmployee & ") "
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'REMOVED -------- 
                '"            CASE WHEN SUM(CASE WHEN EM.assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & " THEN " & IIf(blnUsedAgreedScore, "GT.agreedscore", "GT.result") & " ELSE GT.result END * GT.item_weight) = 0 THEN 0 " & _
                '"                 WHEN SUM(GT.item_weight*(CASE WHEN " & xScoringOptionId & " = 2 THEN GT.max_scale ELSE GT.item_weight END)) = 0 THEN 0 " & _
                '"                 ELSE SUM(CASE WHEN EM.assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & " THEN " & IIf(blnUsedAgreedScore, "GT.agreedscore", "GT.result") & " ELSE GT.result END * GT.item_weight)/SUM(GT.item_weight*(CASE WHEN " & xScoringOptionId & " = 2 THEN GT.max_scale ELSE GT.item_weight END)) " & _
                '"           END AS iweight " & _
                'ADDED ---------- 
                '"            CAST(CASE WHEN SUM(CASE WHEN EM.assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & " THEN " & IIf(blnUsedAgreedScore, "GT.agreedscore", "GT.result") & " ELSE GT.result END * GT.item_weight) = 0 THEN 0 " & _
                '"                      WHEN SUM(GT.item_weight*(CASE WHEN " & xScoringOptionId & " = 2 THEN GT.max_scale ELSE GT.item_weight END)) = 0 THEN 0 " & _
                '"                 ELSE SUM(CASE WHEN EM.assessmodeid = " & enAssessmentMode.APPRAISER_ASSESSMENT & " THEN " & IIf(blnUsedAgreedScore, "GT.agreedscore", "GT.result") & " ELSE GT.result END * GT.item_weight)/SUM(GT.item_weight*(CASE WHEN " & xScoringOptionId & " = 2 THEN GT.max_scale ELSE GT.item_weight END)) " & _
                '"                 END AS DECIMAL(36,2))AS iweight " & _
                'S.SANDEEP |21-AUG-2019| -- END

                Dim dsList As DataSet = objDataOp.ExecQuery(StrQ, "List")

                If objDataOp.ErrorMessage <> "" Then
                    Throw New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                End If

                If dsList IsNot Nothing Then
                    mdtComputeValue = dsList.Tables(0).Copy
                End If

                'S.SANDEEP [20-JUL-2017] -- START
                'ISSUE/ENHANCEMENT : SUB QURRY
                'StrQ = "DECLARE @allocId AS INT " & _
                '       "SET @allocId = ISNULL((SELECT allocrefunkid FROM hrassess_ratio_master WHERE periodunkid = '" & xPeriodId & "' AND isvoid = 0),0) " & _
                StrQ = "DECLARE @allocId AS INT " & _
                       " SET @allocId = ISNULL((SELECT DISTINCT allocrefunkid FROM hrassess_ratio_master WHERE periodunkid = '" & xPeriodId & "' AND isvoid = 0),0) "
                'S.SANDEEP [20-JUL-2017] -- END
                StrQ &= "    SELECT  DISTINCT " & _
                       "         hrassess_ratio_master.allocrefunkid " & _
                       "        ,hrassess_ratio_master.bsc_weight " & _
                       "        ,hrassess_ratio_master.ge_weight " & _
                       "        ,ISNULL(ISNULL(ETRF.TrfEmpId,ERECAT.CatEmpId),0) AS TrfEmpId " & _
                       "    FROM hrassess_ratio_master WITH (NOLOCK) " & _
                       "        JOIN cfcommon_period_tran WITH (NOLOCK) ON cfcommon_period_tran.periodunkid = hrassess_ratio_master.periodunkid " & _
                       "        JOIN hrassess_ratio_tran WITH (NOLOCK) ON hrassess_ratio_master.ratiounkid = hrassess_ratio_tran.ratiounkid " & _
                       "    LEFT JOIN " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             Trf.TrfEmpId " & _
                       "            ,allocid " & _
                       "            ,atId " & _
                       "        FROM " & _
                       "        ( " & _
                       "            SELECT " & _
                       "                 ETT.employeeunkid AS TrfEmpId " & _
                       "                ,CASE WHEN @allocId = 1  THEN ISNULL(ETT.stationunkid,0) " & _
                       "                      WHEN @allocId = 2  THEN ISNULL(ETT.deptgroupunkid,0) " & _
                       "                      WHEN @allocId = 3  THEN ISNULL(ETT.departmentunkid,0) " & _
                       "                      WHEN @allocId = 4  THEN ISNULL(ETT.sectiongroupunkid,0) " & _
                       "                      WHEN @allocId = 5  THEN ISNULL(ETT.sectionunkid,0) " & _
                       "                      WHEN @allocId = 6  THEN ISNULL(ETT.unitgroupunkid,0) " & _
                       "                      WHEN @allocId = 7  THEN ISNULL(ETT.unitunkid,0) " & _
                       "                      WHEN @allocId = 8  THEN ISNULL(ETT.teamunkid,0) " & _
                       "                      WHEN @allocId = 13 THEN ISNULL(ETT.classgroupunkid,0) " & _
                       "                      WHEN @allocId = 14 THEN ISNULL(ETT.classunkid,0) " & _
                       "                 END AS allocid " & _
                       "               ,ROW_NUMBER()OVER(PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC) AS Rno " & _
                       "               ,@allocId AS atId " & _
                       "            FROM hremployee_transfer_tran AS ETT WITH (NOLOCK) " & _
                       "            WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEmployeeAsOnDate) & "' AND ETT.employeeunkid IN (" & xStrCsvEmployee & ") " & _
                       "        ) AS Trf WHERE Trf.Rno = 1 " & _
                       ") AS ETRF ON ETRF.atId = @allocId AND ETRF.TrfEmpId IN (" & xStrCsvEmployee & ") AND ETRF.allocid = allocationid " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "         Cat.CatEmpId " & _
                       "        ,allocid " & _
                       "        ,atId " & _
                       "    FROM " & _
                       "    ( " & _
                       "        SELECT " & _
                       "             ECT.employeeunkid AS CatEmpId " & _
                       "            ,CASE WHEN @allocId = 9  THEN ISNULL(ECT.jobgroupunkid,0) " & _
                       "                  WHEN @allocId = 10 THEN ISNULL(ECT.jobunkid,0) " & _
                       "             END AS allocid " & _
                       "            ,ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                       "            ,@allocId AS atId " & _
                       "        FROM hremployee_categorization_tran AS ECT WITH (NOLOCK) " & _
                       "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEmployeeAsOnDate) & "' AND ECT.employeeunkid IN (" & xStrCsvEmployee & ") " & _
                       "    ) AS Cat WHERE Cat.Rno = 1 " & _
                       ") AS ERECAT ON ERECAT.atId = @allocId AND ERECAT.CatEmpId IN (" & xStrCsvEmployee & ") AND ERECAT.allocid = allocationid " & _
                       "WHERE hrassess_ratio_master.isvoid = 0 AND hrassess_ratio_master.isvoid = 0 AND hrassess_ratio_master.periodunkid = '" & xPeriodId & "' " & _
                       " AND ISNULL(ISNULL(ETRF.TrfEmpId,ERECAT.CatEmpId),0) > 0 "

                'S.SANDEEP |09-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : PA CHANGES
                ' ---- ADDED 
                '1.  AND ISNULL(ISNULL(ETRF.TrfEmpId,ERECAT.CatEmpId),0) > 0 
                '2.  AND ERECAT.allocid = allocationid
                '3.  AND ETRF.allocid = allocationid
                '4.  ,ISNULL(ISNULL(ETRF.TrfEmpId,ERECAT.CatEmpId),0) AS TrfEmpId
                ' ---- REMOVED
                '1.  ,ETRF.TrfEmpId
                'S.SANDEEP |09-JUL-2019| -- END

                dsList = objDataOp.ExecQuery(StrQ, "List")

                If objDataOp.ErrorMessage <> "" Then
                    Throw New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                End If

                If dsList.Tables("List").Rows.Count > 0 Then
                    For Each mrow As DataRow In dsList.Tables("List").Rows
                        If mrow.Item("bsc_weight") > 0 Then
                            Dim xRow As DataRow = mdtComputeValue.NewRow()
                            With xRow
                                .Item("iweight") = mrow.Item("bsc_weight")
                                .Item("Itemids") = 0
                                .Item("assessmodeid") = 0
                                .Item("compute_type") = enAssess_Computation_Types.BSC_RATIO_VALUE
                                .Item("isBsc") = "1"
                                .Item("employeeunkid") = mrow.Item("TrfEmpId")
                                .Item("AssGrpIds") = 0
                            End With
                            mdtComputeValue.Rows.Add(xRow)
                        End If
                        If mrow.Item("ge_weight") > 0 Then
                            Dim xRow As DataRow = mdtComputeValue.NewRow()
                            With xRow
                                .Item("iweight") = mrow.Item("ge_weight")
                                .Item("Itemids") = 0
                                .Item("assessmodeid") = 0
                                .Item("compute_type") = enAssess_Computation_Types.CMP_RATIO_VALUE
                                .Item("isBsc") = "0"
                                .Item("employeeunkid") = mrow.Item("TrfEmpId")
                                .Item("AssGrpIds") = 0
                            End With
                            mdtComputeValue.Rows.Add(xRow)
                        End If
                    Next
                End If

                'S.SANDEEP [29-NOV-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 40

                'S.SANDEEP [28-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0002069}
                'StrQ = "declare @yearid int,@csvprdids nvarchar(max) " & _
                '      "set @yearid = isnull((select yearunkid from cfcommon_period_tran where periodunkid = '" & xPeriodId & "'),0) " & _
                '      "set @csvprdids = isnull((select isnull(stuff((select ','+convert(nvarchar(max),periodunkid) " & _
                '      "    from cfcommon_period_tran where yearunkid = @yearid and isactive = 1 and modulerefid = 5 " & _
                '      "    for xml path('')),1,1,''),'')),'') " & _
                '      "select " & _
                '      "     sum(csm.finaloverallscore) as finalsum " & _
                '      "    ,csm.employeeunkid " & _
                '      "from hrassess_compute_score_master as csm " & _
                '      "where csm.assessmodeid = 1 and csm.isvoid = 0 " & _
                '      "and csm.periodunkid in (@csvprdids) and csm.employeeunkid in (" & xStrCsvEmployee & ") " & _
                '      "group by csm.employeeunkid "

                StrQ = "declare @yearid int " & _
                       "set @yearid = isnull((select yearunkid from cfcommon_period_tran where periodunkid = '" & xPeriodId & "'),0) " & _
                       "select " & _
                       "     sum(csm.finaloverallscore) as finalsum " & _
                       "    ,csm.employeeunkid " & _
                       "from hrassess_compute_score_master as csm WITH (NOLOCK) " & _
                       "where csm.assessmodeid = 1 and csm.isvoid = 0 " & _
                       "and csm.periodunkid in (SELECT periodunkid FROM cfcommon_period_tran WHERE yearunkid = @yearid AND isactive = 1 AND modulerefid = 5) and csm.employeeunkid in (" & xStrCsvEmployee & ") " & _
                       "group by csm.employeeunkid "
                'S.SANDEEP [28-Feb-2018] -- END
               

                dsList = objDataOp.ExecQuery(StrQ, "List")

                If objDataOp.ErrorMessage <> "" Then
                    Throw New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                End If

                If dsList.Tables("List").Rows.Count > 0 Then
                    For Each mrow As DataRow In dsList.Tables("List").Rows
                        If mrow.Item("finalsum") > 0 Then
                            Dim xRow As DataRow = mdtComputeValue.NewRow()
                            With xRow
                                .Item("iweight") = mrow.Item("finalsum")
                                .Item("Itemids") = 0
                                .Item("assessmodeid") = 0
                                .Item("compute_type") = enAssess_Computation_Types.TOT_FINAL_SCORE_ALL_PERIOD
                                .Item("isBsc") = "0"
                                .Item("employeeunkid") = mrow.Item("employeeunkid")
                                .Item("AssGrpIds") = 0
                            End With
                            mdtComputeValue.Rows.Add(xRow)
                        End If
                    Next
                End If

                If xStrCsvEmployee.Trim.Length > 0 Then
                    StrQ = "DECLARE @words VARCHAR (MAX) " & _
                             "SET @words = '" & xStrCsvEmployee & "' " & _
                            "DECLARE @split TABLE(word VARCHAR(MAX)) " & _
                            "DECLARE @word VARCHAR(64), @start INT, @end INT, @stop INT " & _
                            "SELECT @words = @words + ',', @start = 1, @stop = len(@words)+1 " & _
                            "WHILE @start < @stop begin " & _
                                "SELECT " & _
                                    "@end   = CHARINDEX(',',@words,@start) " & _
                                  ", @word  = RTRIM(LTRIM(SUBSTRING(@words,@start,@end-@start))) " & _
                                  ", @start = @end + 1 " & _
                                "INSERT @split VALUES (@word) " & _
                            "END " & _
                            "declare @yearid int " & _
                            "set @yearid = isnull((select yearunkid from cfcommon_period_tran WITH (NOLOCK) where periodunkid = '" & xPeriodId & "'),0) " & _
                            "select " & _
                             "count(periodunkid) as pcount " & _
                            ",word  as emplid " & _
                            "from cfcommon_period_tran " & _
                            "join @split on 1 = 1 " & _
                            "where yearunkid = @yearid and isactive = 1 and modulerefid = 5 " & _
                            "group by word order by word asc "

                    dsList = objDataOp.ExecQuery(StrQ, "List")

                    If objDataOp.ErrorMessage <> "" Then
                        Throw New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                    End If

                    If dsList.Tables("List").Rows.Count > 0 Then
                        For Each mrow As DataRow In dsList.Tables("List").Rows
                            If mrow.Item("pcount") > 0 Then
                                Dim xRow As DataRow = mdtComputeValue.NewRow()
                                With xRow
                                    .Item("iweight") = mrow.Item("pcount")
                                    .Item("Itemids") = 0
                                    .Item("assessmodeid") = 0
                                    .Item("compute_type") = enAssess_Computation_Types.TOT_ASSES_PERIOD_COUNT
                                    .Item("isBsc") = "0"
                                    .Item("employeeunkid") = mrow.Item("emplid")
                                    .Item("AssGrpIds") = 0
                                End With
                                mdtComputeValue.Rows.Add(xRow)
                            End If
                        Next
                    End If
                End If
                'S.SANDEEP [29-NOV-2017] -- END
            End Using

            'S.SANDEEP [29-NOV-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 40
            If mdtComputeValue IsNot Nothing Then
                Dim xCol As New DataColumn
                With xCol
                    .ColumnName = "periodunkid"
                    .DataType = GetType(System.Int32)
                    .DefaultValue = xPeriodId
                End With
                mdtComputeValue.Columns.Add(xCol)
            End If
            'S.SANDEEP [29-NOV-2017] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_All_Compution_value; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Function Get_All_Formula_Expression(ByVal xStrFormulaString As String, _
                                               ByVal iItemUnkid As Integer, _
                                               ByVal iEmployeeId As Integer, _
                                               ByVal iScoringOptionId As Integer, _
                                               ByVal iScore As Decimal, _
                                               ByVal iAMode As enAssessmentMode, _
                                               ByVal iBSC_OverAllWeight As Decimal, _
                                               ByVal istrGrps As String, _
                                               ByVal iSelfAssignCompetencies As Boolean, _
                                               Optional ByVal iAssessGrpId As Integer = 0, _
                                               Optional ByRef blnIsSummaryUsed As Boolean = False) As Decimal
        'S.SANDEEP |26-AUG-2019| -- START [blnIsSummaryUsed] -- END
        'Shani (23-Nov-2016) -- [ByVal iSelfAssignCompetencies As Boolean, _]

        Dim xStrExpression As String = String.Empty
        Dim xOperand As String = String.Empty
        Dim xTemp As String = ""
        Dim xFinalValue As Decimal = 0
        Dim xScoreValue As Decimal = 0
        Try
            For i As Integer = 1 To xStrFormulaString.Length
                If Mid(xStrFormulaString, i, 1) = "+" OrElse _
                   Mid(xStrFormulaString, i, 1) = "-" OrElse _
                   Mid(xStrFormulaString, i, 1) = "*" OrElse _
                   Mid(xStrFormulaString, i, 1) = "/" OrElse _
                   Mid(xStrFormulaString, i, 1) = "(" OrElse _
                   Mid(xStrFormulaString, i, 1) = ")" Then

                    If xTemp.Trim.Length > 0 Then
                        xStrExpression += xOperand & xTemp
                    End If

                    xTemp = Trim(Mid(xStrFormulaString, i, 1))

                    If xTemp = "+" Then
                        If xOperand.Trim.Length > 0 Then
                            xOperand += "("
                        End If
                        xOperand += xTemp
                        xStrExpression += xOperand
                        xTemp = "" : xOperand = ""

                    ElseIf xTemp = "-" Then
                        If xOperand.Trim.Length > 0 Then
                            xOperand += "("
                        End If
                        xOperand += xTemp
                        xStrExpression += xOperand
                        xTemp = "" : xOperand = ""

                    ElseIf xTemp = "*" Then
                        If xOperand.Trim.Length > 0 Then
                            xOperand += "("
                        End If
                        xOperand += xTemp
                        xStrExpression += xOperand
                        xTemp = "" : xOperand = ""

                    ElseIf xTemp = "/" Then
                        If xOperand.Trim.Length > 0 Then
                            xOperand += "("
                        End If
                        xOperand += xTemp
                        xStrExpression += xOperand
                        xTemp = "" : xOperand = ""

                    ElseIf xTemp = "(" Then
                        xOperand = xTemp
                        xStrExpression += xOperand
                        xTemp = "" : xOperand = ""

                    ElseIf xTemp = ")" Then
                        xOperand += xTemp
                        xStrExpression += xOperand
                        xTemp = "" : xOperand = ""

                    Else
                        xOperand += xOperand
                        xStrExpression += xOperand
                        xTemp = "" : xOperand = ""

                    End If
                ElseIf Mid(xStrFormulaString, i, 1) <> "#" Then
                    xTemp += Trim(Mid(xStrFormulaString, i, 1))
                Else
                    Dim iFrmlId As Integer = -1
                    If xTemp <> "" Then

                        'S.SANDEEP |26-AUG-2019| -- START
                        'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                        Select Case CInt(xTemp)
                            Case enAssess_Computation_Types.BSC_TOT_EMP_ROW_SCORE_RATIO, enAssess_Computation_Types.BSC_TOT_ASR_ROW_SCORE_RATIO, enAssess_Computation_Types.BSC_TOT_REV_ROW_SCORE_RATIO
                                If iAssessGrpId <= 0 Then blnIsSummaryUsed = True
                            Case enAssess_Computation_Types.CMP_TOT_EMP_ROW_SCORE_RATIO, enAssess_Computation_Types.CMP_TOT_ASR_ROW_SCORE_RATIO, enAssess_Computation_Types.CMP_TOT_REV_ROW_SCORE_RATIO
                                If iAssessGrpId > 0 Then blnIsSummaryUsed = True
                        End Select
                        'S.SANDEEP |26-AUG-2019| -- END

                        xFinalValue = Get_Computation_All_Variable_Value(CInt(xTemp), iItemUnkid, iEmployeeId, _
                                                                         iScoringOptionId, iScore, iAMode, _
                                                                         iBSC_OverAllWeight, istrGrps, iSelfAssignCompetencies, iAssessGrpId)
                        'Shani (23-Nov-2016) -- [iSelfAssignCompetencies]
                        If iAssessGrpId > 0 Then
                            If IsValid_Computation_Type_Id(CInt(xTemp), True) Then
                                xFinalValue = 0
                            End If
                        Else
                            If IsValid_Computation_Type_Id(CInt(xTemp), False) Then
                                xFinalValue = 0
                            End If
                        End If
                        xStrExpression += xOperand & xFinalValue.ToString
                        xOperand = "" : xTemp = ""
                    End If
                End If
            Next

            If xTemp.Trim.Length > 0 Then
                xStrExpression += xOperand & xTemp
                xOperand = ""
            End If

            xOperand = xOperand.Replace("+", "").Replace("-", "").Replace("*", "").Replace("/", "")
            xStrExpression += xOperand

            xStrExpression = xStrExpression.Replace("()", "")


            If xStrExpression.Trim.Length > 0 Then
                Dim objFormula As New clsFomulaEvaluate
                xScoreValue = objFormula.Eval(xStrExpression)
                If xScoreValue > 0 Then
                    xScoreValue = CDbl(xScoreValue).ToString("###########0.###0")
                End If
                objFormula = Nothing
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Formula_Expression", mstrModuleName)
        Finally
        End Try
        Return xScoreValue
    End Function

    Private Function Get_Computation_All_Variable_Value(ByVal xComputationVariable As Integer, _
                                                        ByVal xItemUnkid As Integer, _
                                                        ByVal xEmployeeId As Integer, _
                                                        ByVal xScoringOptionId As Integer, _
                                                        ByVal xScore As Decimal, _
                                                        ByVal xAMode As enAssessmentMode, _
                                                        ByVal xBSC_OverAllWeight As Decimal, _
                                                        ByVal xStrGroupIds As String, _
                                                        ByVal xSelfAssignCompetencies As Boolean, _
                                                        Optional ByVal xAssessGrpId As Integer = 0) As Decimal

        'Shani (23-Nov-2016) -- [ByVal xSelfAssignCompetencies As Boolean, _]
        Dim xValue As Decimal = 0
        Dim xstrgrp() As String = Nothing
        Try
            If xStrGroupIds.Trim.Length > 0 Then
                xstrgrp = xStrGroupIds.Split(",")
            End If

            Using objDataOp As New clsDataOperation
                Select Case xComputationVariable
                    Case enAssess_Computation_Types.BSC_ITEM_WEIGHT
                        Dim xVar = mdtComputeValue.AsEnumerable() _
                                .Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                           AndAlso x.Field(Of Integer)("Itemids") = xItemUnkid _
                                           AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.BSC_ITEM_WEIGHT) _
                               .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If
                    Case enAssess_Computation_Types.BSC_ITEM_EMP_SCORE_VALUE
                        'S.SANDEEP |09-DEC-2019| -- START
                        'ISSUE/ENHANCEMENT : NORMALIZED FORMULA
                        'Select Case xAMode
                        '    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        '        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                        '                                                        And x.Field(Of Integer)("Itemids") = xItemUnkid _
                        '                                                        And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT _
                        '                                                        And x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.BSC_ITEM_EMP_SCORE_VALUE) _
                        '                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray

                        '        If xVar.Count > 0 Then
                        '            xValue = xVar.First
                        '        End If
                        '    Case Else
                        '        xValue = xScore
                        'End Select
                        If xScore > 0 Then
                            xValue = xScore
                        Else
                                Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                                And x.Field(Of Integer)("Itemids") = xItemUnkid _
                                                                                And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT _
                                                                                And x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.BSC_ITEM_EMP_SCORE_VALUE) _
                                                                    .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray

                                If xVar.Count > 0 Then
                                    xValue = xVar.First
                                End If
                        End If
                        'S.SANDEEP |09-DEC-2019| -- END                        
                    Case enAssess_Computation_Types.BSC_ITEM_ASR_SCORE_VALUE
                        'S.SANDEEP |09-DEC-2019| -- START
                        'ISSUE/ENHANCEMENT : NORMALIZED FORMULA
                        'Select Case xAMode
                        '    Case enAssessmentMode.SELF_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        '        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                        '                                                        And x.Field(Of Integer)("Itemids") = xItemUnkid _
                        '                                                        And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT _
                        '                                                        And x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.BSC_ITEM_ASR_SCORE_VALUE) _
                        '                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                        '        If xVar.Count > 0 Then
                        '            xValue = xVar.First
                        '        End If
                        '    Case Else
                        '        xValue = xScore
                        'End Select
                        If xScore > 0 Then
                                xValue = xScore
                        Else
                                Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                                And x.Field(Of Integer)("Itemids") = xItemUnkid _
                                                                                And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT _
                                                                                And x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.BSC_ITEM_ASR_SCORE_VALUE) _
                                                                    .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                                If xVar.Count > 0 Then
                                    xValue = xVar.First
                                End If
                        End If
                        'S.SANDEEP |09-DEC-2019| -- END
                    Case enAssess_Computation_Types.BSC_ITEM_REV_SCORE_VALUE
                        'S.SANDEEP |09-DEC-2019| -- START
                        'ISSUE/ENHANCEMENT : NORMALIZED FORMULA
                        'Select Case xAMode
                        '    Case enAssessmentMode.SELF_ASSESSMENT, enAssessmentMode.APPRAISER_ASSESSMENT
                        '        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                        '                                                        And x.Field(Of Integer)("Itemids") = xItemUnkid _
                        '                                                        And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT _
                        '                                                        And x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.BSC_ITEM_REV_SCORE_VALUE) _
                        '                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray

                        '        If xVar.Count > 0 Then
                        '            xValue = xVar.First
                        '        End If
                        '    Case Else
                        '        xValue = xScore
                        'End Select
                        If xScore > 0 Then
                                xValue = xScore
                        Else
                                Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                                And x.Field(Of Integer)("Itemids") = xItemUnkid _
                                                                                And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT _
                                                                                And x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.BSC_ITEM_REV_SCORE_VALUE) _
                                                                    .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray

                                If xVar.Count > 0 Then
                                    xValue = xVar.First
                                End If
                        End If
                        'S.SANDEEP |09-DEC-2019| -- END
                    Case enAssess_Computation_Types.BSC_PARENT_FIELD_WEIGHT
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("Itemids") = xItemUnkid _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.BSC_PARENT_FIELD_WEIGHT) _
                                                            .Select(Function(x) x.Field(Of Decimal)("iWeight")).ToArray
                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If
                    Case enAssess_Computation_Types.BSC_ITEM_MAX_SCORE_VALUE
                        Select Case xScoringOptionId
                            Case enScoringOption.SC_WEIGHTED_BASED
                                Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                                AndAlso x.Field(Of Integer)("Itemids") = xItemUnkid _
                                                                                AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.BSC_ITEM_WEIGHT) _
                                                                    .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                                If xVar.Count > 0 Then
                                    xValue = xVar.First
                                End If
                            Case enScoringOption.SC_SCALE_BASED
                                Dim xVar = mdtComputeValue.AsEnumerable() _
                                            .Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                       AndAlso x.Field(Of Integer)("Itemids") = xItemUnkid _
                                                       AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.BSC_ITEM_MAX_SCORE_VALUE) _
                                           .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                                If xVar.Count > 0 Then
                                    xValue = xVar.First
                                End If
                        End Select
                    Case enAssess_Computation_Types.BSC_OVERALL_WEIGHT
                        xValue = xBSC_OverAllWeight

                    Case enAssess_Computation_Types.CMP_ITEM_WEIGHT
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                        AndAlso x.Field(Of Integer)("Itemids") = xItemUnkid _
                                                                        AndAlso x.Field(Of Integer)("AssGrpIds") = xAssessGrpId _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ITEM_WEIGHT) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray

                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If
                    Case enAssess_Computation_Types.CMP_ITEM_EMP_SCORE_VALUE
                        'S.SANDEEP |09-DEC-2019| -- START
                        'ISSUE/ENHANCEMENT : NORMALIZED FORMULA
                        'Select Case xAMode
                        '    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        '        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                        '                                                        AndAlso x.Field(Of Integer)("AssGrpIds") = xAssessGrpId _
                        '                                                        AndAlso x.Field(Of Integer)("Itemids") = xItemUnkid _
                        '                                                        AndAlso x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT _
                        '                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ITEM_EMP_SCORE_VALUE) _
                        '                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                        '        If xVar.Count > 0 Then
                        '            xValue = xVar.First
                        '        End If
                        '    Case Else
                        '        xValue = xScore
                        'End Select
                        If xScore > 0 Then
                            xValue = xScore
                        Else
                                Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                                AndAlso x.Field(Of Integer)("AssGrpIds") = xAssessGrpId _
                                                                                AndAlso x.Field(Of Integer)("Itemids") = xItemUnkid _
                                                                                AndAlso x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT _
                                                                                AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ITEM_EMP_SCORE_VALUE) _
                                                                    .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                                If xVar.Count > 0 Then
                                    xValue = xVar.First
                                End If
                        End If
                        'S.SANDEEP |09-DEC-2019| -- END                        
                    Case enAssess_Computation_Types.CMP_ITEM_ASR_SCORE_VALUE
                        'S.SANDEEP |09-DEC-2019| -- START
                        'ISSUE/ENHANCEMENT : NORMALIZED FORMULA
                        'Select Case xAMode
                        '    Case enAssessmentMode.SELF_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        '        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                        '                                                        AndAlso x.Field(Of Integer)("AssGrpIds") = xAssessGrpId _
                        '                                                        AndAlso x.Field(Of Integer)("Itemids") = xItemUnkid _
                        '                                                        AndAlso x.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT _
                        '                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ITEM_ASR_SCORE_VALUE) _
                        '                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray

                        '        If xVar.Count > 0 Then
                        '            xValue = xVar.First
                        '        End If
                        '    Case Else
                        '        xValue = xScore
                        'End Select
                        If xScore > 0 Then
                                xValue = xScore
                        Else
                                Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                                AndAlso x.Field(Of Integer)("AssGrpIds") = xAssessGrpId _
                                                                                AndAlso x.Field(Of Integer)("Itemids") = xItemUnkid _
                                                                                AndAlso x.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT _
                                                                                AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ITEM_ASR_SCORE_VALUE) _
                                                                    .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray

                                If xVar.Count > 0 Then
                                    xValue = xVar.First
                                End If
                        End If                        
                        'S.SANDEEP |09-DEC-2019| -- END
                    Case enAssess_Computation_Types.CMP_ITEM_REV_SCORE_VALUE
                        'S.SANDEEP |09-DEC-2019| -- START
                        'ISSUE/ENHANCEMENT : NORMALIZED FORMULA
                        'Select Case xAMode
                        '    Case enAssessmentMode.SELF_ASSESSMENT, enAssessmentMode.APPRAISER_ASSESSMENT
                        '        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                        '                                                        AndAlso x.Field(Of Integer)("AssGrpIds") = xAssessGrpId _
                        '                                                        AndAlso x.Field(Of Integer)("Itemids") = xItemUnkid _
                        '                                                        AndAlso x.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT _
                        '                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ITEM_REV_SCORE_VALUE) _
                        '                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray

                        '        If xVar.Count > 0 Then
                        '            xValue = xVar.First
                        '        End If
                        '    Case Else
                        '        xValue = xScore
                        'End Select
                        If xScore > 0 Then
                                xValue = xScore
                        Else
                                Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                                AndAlso x.Field(Of Integer)("AssGrpIds") = xAssessGrpId _
                                                                                AndAlso x.Field(Of Integer)("Itemids") = xItemUnkid _
                                                                                AndAlso x.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT _
                                                                                AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ITEM_REV_SCORE_VALUE) _
                                                                    .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray

                                If xVar.Count > 0 Then
                                    xValue = xVar.First
                                End If
                        End If
                        'S.SANDEEP |09-DEC-2019| -- END
                    Case enAssess_Computation_Types.CMP_CATEGORY_WEIGHT
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("Itemids") = xItemUnkid _
                                                                        AndAlso x.Field(Of Integer)("AssGrpIds") = xAssessGrpId _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_CATEGORY_WEIGHT) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If
                    Case enAssess_Computation_Types.CMP_ITEM_MAX_SCORE_VALUE
                        Select Case xScoringOptionId
                            Case enScoringOption.SC_WEIGHTED_BASED
                                Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                                AndAlso x.Field(Of Integer)("Itemids") = xItemUnkid _
                                                                                AndAlso x.Field(Of Integer)("AssGrpIds") = xAssessGrpId _
                                                                                AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.BSC_ITEM_WEIGHT) _
                                                                    .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                                If xVar.Count > 0 Then
                                    xValue = xVar.First
                                End If
                            Case enScoringOption.SC_SCALE_BASED
                                Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("Itemids") = xItemUnkid _
                                                                                AndAlso x.Field(Of Integer)("AssGrpIds") = xAssessGrpId _
                                                                                AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ITEM_MAX_SCORE_VALUE) _
                                                                    .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                                If xVar.Count > 0 Then
                                    xValue = xVar.First
                                End If
                        End Select
                        'xValue = xVar
                    Case enAssess_Computation_Types.CMP_ASSESSMENT_GROUP_WEIGHT
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("AssGrpIds") = xAssessGrpId _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ASSESSMENT_GROUP_WEIGHT) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray

                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If
                    Case enAssess_Computation_Types.CMP_OVERALL_WEIGHT
                        Dim xGrp As String() = xstrgrp
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) xGrp.Contains(x.Field(Of Integer)("AssGrpIds").ToString) _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ASSESSMENT_GROUP_WEIGHT) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).Sum

                        xValue = xVar
                    Case enAssess_Computation_Types.BSC_PARENT_ITEMS_ALL_PERSPECTIVE_COUNT
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.BSC_PARENT_ITEMS_ALL_PERSPECTIVE_COUNT) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray

                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If
                    Case enAssess_Computation_Types.BSC_LINKED_ITEMS_ALL_PERSPECTIVE_COUNT
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.BSC_LINKED_ITEMS_ALL_PERSPECTIVE_COUNT) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray

                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If
                    Case enAssess_Computation_Types.CMP_ASSESSMENT_GROUP_COUNT
                        Dim xGrp() As String = xStrGroupIds.Split(",")
                        Dim xLastGrp As String = String.Empty
                        If xGrp.Length > 0 Then
                            For i As Integer = 0 To xGrp.Length - 1
                                If xLastGrp <> xGrp(i).ToString.Trim Then
                                    xValue += 1
                                    xLastGrp = xGrp(i).ToString.Trim
                                End If
                            Next
                        End If

                    Case enAssess_Computation_Types.CMP_ITEMS_PER_CATEGORY_COUNT

                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        'Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("Itemids") = xItemUnkid _
                        '                                                AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ITEMS_PER_CATEGORY_COUNT) _
                        '                                    .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                        If xSelfAssignCompetencies Then
                            Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("Itemids") = xItemUnkid _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ITEMS_PER_CATEGORY_COUNT _
                                                                        AndAlso x.Field(Of Integer)("employeeunkid") = xEmployeeId) _
                                                                .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                            If xVar.Count > 0 Then
                                xValue = xVar.First
                            End If
                        Else
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("Itemids") = xItemUnkid _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ITEMS_PER_CATEGORY_COUNT) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If

                        End If
                        'Shani (23-Nov123-2016-2016) -- End

                    Case enAssess_Computation_Types.CMP_CATEGORY_PER_GROUP_COUNT
                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        'Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("AssGrpIds") = xAssessGrpId _
                        '                                                AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_CATEGORY_PER_GROUP_COUNT) _
                        '                                    .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray

                        If xSelfAssignCompetencies Then
                            Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("AssGrpIds") = xAssessGrpId _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_CATEGORY_PER_GROUP_COUNT _
                                                                        AndAlso x.Field(Of Integer)("employeeunkid") = xEmployeeId) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                            If xVar.Count > 0 Then
                                xValue = xVar.First
                            End If
                        Else
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("AssGrpIds") = xAssessGrpId _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_CATEGORY_PER_GROUP_COUNT) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If
                        End If
                        'Shani (23-Nov123-2016-2016) -- End

                        
                    Case enAssess_Computation_Types.CMP_ITEMS_ALL_CATEGORY_GROUP_COUNT

                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        'Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) xstrgrp.Contains(x.Field(Of Integer)("AssGrpIds").ToString) _
                        '                                                AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ASSESSMENT_GROUP_COUNT) _
                        '                                    .Select(Function(x) x.Field(Of Double)("iWeight")).Sum

                        If xSelfAssignCompetencies Then
                            Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) xstrgrp.Contains(x.Field(Of Integer)("AssGrpIds").ToString) _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ASSESSMENT_GROUP_COUNT _
                                                                        AndAlso x.Field(Of Integer)("employeeunkid") = xEmployeeId) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).Sum
                            xValue = xVar
                        Else
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) xstrgrp.Contains(x.Field(Of Integer)("AssGrpIds").ToString) _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ASSESSMENT_GROUP_COUNT) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).Sum
                        xValue = xVar
                        End If
                        'Shani (23-Nov123-2016-2016) -- End

                    Case enAssess_Computation_Types.CMP_CATEGORY_ALL_GROUP_COUNT
                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        'Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) xstrgrp.Contains(x.Field(Of Integer)("AssGrpIds").ToString) _
                        '                                                AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_CATEGORY_PER_GROUP_COUNT) _
                        '                                    .Select(Function(x) x.Field(Of Double)("iWeight")).Sum

                        If xSelfAssignCompetencies Then
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) xstrgrp.Contains(x.Field(Of Integer)("AssGrpIds").ToString) _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ASSESSMENT_GROUP_COUNT _
                                                                        AndAlso x.Field(Of Integer)("employeeunkid") = xEmployeeId) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).Sum
                            xValue = xVar
                        Else
                            Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) xstrgrp.Contains(x.Field(Of Integer)("AssGrpIds").ToString) _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_ASSESSMENT_GROUP_COUNT) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).Sum
                        xValue = xVar
                        End If
                        'Shani (23-Nov123-2016-2016) -- End

                    Case enAssess_Computation_Types.BSC_RATIO_VALUE
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.BSC_RATIO_VALUE) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If

                        'S.SANDEEP [20-JUL-2017] -- START
                        'ISSUE/ENHANCEMENT : RATIO CONVERTING TO %
                        If xValue > 0 Then xValue = (xValue / 100)
                        'S.SANDEEP [20-JUL-2017] -- END


                    Case enAssess_Computation_Types.CMP_RATIO_VALUE
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_RATIO_VALUE) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If

                        'S.SANDEEP [20-JUL-2017] -- START
                        'ISSUE/ENHANCEMENT : RATIO CONVERTING TO %
                        If xValue > 0 Then xValue = (xValue / 100)
                        'S.SANDEEP [20-JUL-2017] -- END

                    Case enAssess_Computation_Types.BSC_TOT_EMP_ROW_SCORE_RATIO
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.BSC_TOT_EMP_ROW_SCORE_RATIO _
                                                                        AndAlso x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray

                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If

                        
                    Case enAssess_Computation_Types.BSC_TOT_ASR_ROW_SCORE_RATIO
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.BSC_TOT_ASR_ROW_SCORE_RATIO _
                                                                        AndAlso x.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray

                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If
                    Case enAssess_Computation_Types.BSC_TOT_REV_ROW_SCORE_RATIO
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.BSC_TOT_REV_ROW_SCORE_RATIO _
                                                                        AndAlso x.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray


                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If
                    Case enAssess_Computation_Types.CMP_TOT_EMP_ROW_SCORE_RATIO
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_TOT_EMP_ROW_SCORE_RATIO _
                                                                        AndAlso x.Field(Of Integer)("AssGrpIds") = xAssessGrpId _
                                                                        AndAlso x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray


                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If
                    Case enAssess_Computation_Types.CMP_TOT_ASR_ROW_SCORE_RATIO
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_TOT_ASR_ROW_SCORE_RATIO _
                                                                        AndAlso x.Field(Of Integer)("AssGrpIds") = xAssessGrpId _
                                                                        AndAlso x.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray


                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If
                    Case enAssess_Computation_Types.CMP_TOT_REV_ROW_SCORE_RATIO
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.CMP_TOT_REV_ROW_SCORE_RATIO _
                                                                        AndAlso x.Field(Of Integer)("AssGrpIds") = xAssessGrpId _
                                                                        AndAlso x.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If

                        'S.SANDEEP [29-NOV-2017] -- START
                        'ISSUE/ENHANCEMENT : REF-ID # 40
                    Case enAssess_Computation_Types.TOT_FINAL_SCORE_ALL_PERIOD
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.TOT_FINAL_SCORE_ALL_PERIOD) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If

                        If xValue <= 0 Then
                            If mdtComputeValue.Rows.Count > 0 Then
                                xValue = Get_Computation_Variable_Value(CInt(enAssess_Computation_Types.TOT_FINAL_SCORE_ALL_PERIOD), _
                                                               CInt(mdtComputeValue.Rows(0)("periodunkid")), 0, _
                                                               xEmployeeId, xScoringOptionId, 0, enAssessmentMode.SELF_ASSESSMENT, _
                                                               Now.Date, False)
                            End If
                        End If

                    Case enAssess_Computation_Types.TOT_ASSES_PERIOD_COUNT
                        Dim xVar = mdtComputeValue.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = xEmployeeId _
                                                                        AndAlso x.Field(Of Integer)("compute_type") = enAssess_Computation_Types.TOT_ASSES_PERIOD_COUNT) _
                                                            .Select(Function(x) x.Field(Of Double)("iWeight")).ToArray
                        If xVar.Count > 0 Then
                            xValue = xVar.First
                        End If

                        If xValue <= 0 Then
                            If mdtComputeValue.Rows.Count > 0 Then
                                xValue = Get_Computation_Variable_Value(CInt(enAssess_Computation_Types.TOT_ASSES_PERIOD_COUNT), _
                                                               CInt(mdtComputeValue.Rows(0)("periodunkid")), 0, _
                                                               xEmployeeId, xScoringOptionId, 0, enAssessmentMode.SELF_ASSESSMENT, _
                                                               Now.Date, False)
                            End If
                        End If
                        'S.SANDEEP [29-NOV-2017] -- END
                End Select
            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Computation_Variable_Value", mstrModuleName)
        Finally
        End Try
        Return xValue
    End Function
    'Shani (24-May-2016) -- End

    'S.SANDEEP [29-NOV-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 40
    Public Function ComputeAvgFinalScore(ByVal xFormulaType As Integer, ByVal xFormulaExpr As String, ByVal intPeriodId As Integer, ByVal intEmployeeId As Integer, ByVal intScoreTypeId As Integer, ByVal intComputeMstId As Integer) As Decimal
        Try
            Dim xValue As Decimal = 0
            xValue = Get_All_Formula_Expression(xFormulaExpr, 0, intEmployeeId, intScoreTypeId, 0, enAssessmentMode.SELF_ASSESSMENT, 0, "", False, 0)

            Dim StrQ As String = String.Empty
            Using objDo As New clsDataOperation
                Dim intTransactionId As Integer = 0
                Dim strMasterId As String = ""
                StrQ = "SELECT " & _
                       "    @TranId = ISNULL(CST.computescoretranunkid,0) " & _
                       "    @MstrId = ISNULL(CAST(CST.computescoremasterunkid AS NVARCHAR(MAX)),'0') " & _
                       "FROM hrassess_compute_score_master AS CSM WITH (NOLOCK) " & _
                       "    JOIN hrassess_compute_score_tran AS CST WITH (NOLOCK) ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                       "    JOIN hrassess_computation_master AS CM WITH (NOLOCK) ON CM.computationunkid = CST.computationunkid " & _
                       "WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.employeeunkid = @employeeunkid AND CSM.periodunkid = @periodunkid " & _
                       "AND CM.formula_typeid = @formula_typeid AND CM.isvoid = 0 "

                objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                objDo.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
                objDo.AddParameter("@formula_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xFormulaType)
                objDo.AddParameter("@TranId", SqlDbType.Int, eZeeDataType.INT_SIZE, intTransactionId, ParameterDirection.InputOutput)
                objDo.AddParameter("@MstrId", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strMasterId, ParameterDirection.InputOutput)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                If IsDBNull(objDo.GetParameterValue("@TranId")) = False Then
                    intTransactionId = objDo.GetParameterValue("@TranId")
                End If
                If IsDBNull(objDo.GetParameterValue("@MstrId")) = False Then
                    strMasterId = objDo.GetParameterValue("@MstrId")
                End If

                objDo.ClearParameters()
                If strMasterId <= 0 Then
                    

                End If
                If strMasterId <> "0" Then
                    Dim StrQI, StrQU As String

                    StrQI = "" : StrQU = ""

                    StrQI = "INSERT INTO hrassess_compute_score_tran " & _
                            "(computescoremasterunkid,computationunkid,formula_value,isvoid,voiduserunkid,voiddatetime,voidreason,competency_value) " & _
                            "VALUES " & _
                            "(@computescoremasterunkid,@computationunkid,@formula_value,@isvoid,@voiduserunkid,@voiddatetime,@voidreason,@competency_value) "

                    StrQU = "UPDATE hrassess_compute_score_tran SET " & _
                            "    computescoremasterunkid = @computescoremasterunkid " & _
                            "   ,computationunkid = @computationunkid " & _
                            "   ,formula_value = formula_value " & _
                            "   ,competency_value = @competency_value " & _
                            "WHERE computescoretranunkid = @computescoretranunkid "

                    If intTransactionId > 0 Then
                        StrQ = StrQU

                    Else
                        StrQ = StrQI
                    End If
                End If

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ComputeAvgFinalScore; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [29-NOV-2017] -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, formula is already defined for the selected period. Please define new formula.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class