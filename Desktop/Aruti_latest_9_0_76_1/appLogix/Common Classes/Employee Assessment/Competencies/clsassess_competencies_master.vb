﻿'************************************************************************************************************************************
'Class Name : clsassess_competencies_master.vb
'Purpose    :
'Date       :10-Jun-2014
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsassess_competencies_master
    Private Shared ReadOnly mstrModuleName As String = "clsassess_competencies_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    'S.SANDEEP |29-MAR-2021| -- START
    'ISSUE/ENHANCEMENT : Competencies Training
    Private objCmptTraining As New clsassess_competencies_trainingcourses_tran
    'S.SANDEEP |29-MAR-2021| -- END

#Region " Private variables "

    Private mintCompetenciesunkid As Integer = 0
    Private mintCompetence_Categoryunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mintScalemasterunkid As Integer = 0
    Private mstrName As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
    Private mblnIsactive As Boolean = True
    'S.SANDEEP [21 NOV 2015] -- START
    Private mintCompetenceGroupUnkid As Integer = 0
    'S.SANDEEP [21 NOV 2015] -- END

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set competenciesunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Competenciesunkid(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer 'S.SANDEEP [15 FEB 2016] -- START {xDataOpr} -- END
        Get
            Return mintCompetenciesunkid
        End Get
        Set(ByVal value As Integer)
            mintCompetenciesunkid = value
            Call GetData(xDataOpr) 'S.SANDEEP [15 FEB 2016] -- START {xDataOpr} -- END
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set competence_categoryunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Competence_Categoryunkid() As Integer
        Get
            Return mintCompetence_Categoryunkid
        End Get
        Set(ByVal value As Integer)
            mintCompetence_Categoryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set scalemasterunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Scalemasterunkid() As Integer
        Get
            Return mintScalemasterunkid
        End Get
        Set(ByVal value As Integer)
            mintScalemasterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    'S.SANDEEP [21 NOV 2015] -- START
    Public Property _CompetenceGroupUnkid() As Integer
        Get
            Return mintCompetenceGroupUnkid
        End Get
        Set(ByVal value As Integer)
            mintCompetenceGroupUnkid = value
        End Set
    End Property
    'S.SANDEEP [21 NOV 2015] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing) 'S.SANDEEP [15 FEB 2016] -- START {xDataOpr} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'S.SANDEEP [15 FEB 2016] -- START
        'objDataOperation = New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [15 FEB 2016] -- END
        Try
            strQ = "SELECT " & _
              "  competenciesunkid " & _
              ", competence_categoryunkid " & _
              ", periodunkid " & _
              ", scalemasterunkid " & _
              ", name " & _
              ", description " & _
              ", name1 " & _
              ", name2 " & _
              ", isactive " & _
                  ", competencegroupunkid " & _
             "FROM hrassess_competencies_master " & _
                 "WHERE competenciesunkid = @competenciesunkid " 'S.SANDEEP [21 NOV 2015] -- START {competencegroupunkid} --END


            objDataOperation.AddParameter("@competenciesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompetenciesunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCompetenciesunkid = CInt(dtRow.Item("competenciesunkid"))
                mintCompetence_Categoryunkid = CInt(dtRow.Item("competence_categoryunkid"))
                mintScalemasterunkid = CInt(dtRow.Item("scalemasterunkid"))
                mstrName = dtRow.Item("name").ToString
                mstrDescription = dtRow.Item("description").ToString
                mstrName1 = dtRow.Item("name1").ToString
                mstrName2 = dtRow.Item("name2").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                'S.SANDEEP [21 NOV 2015] -- START
                mintCompetenceGroupUnkid = CInt(dtRow.Item("competencegroupunkid"))
                'S.SANDEEP [21 NOV 2015] -- END
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [15 FEB 2016] -- START
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [15 FEB 2016] -- END
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  hrassess_competencies_master.competenciesunkid " & _
                   ", hrassess_competencies_master.competence_categoryunkid " & _
                   ", hrassess_competencies_master.scalemasterunkid " & _
                   ", hrassess_competencies_master.name " & _
                   ", hrassess_competencies_master.description " & _
                   ", hrassess_competencies_master.name1 " & _
                   ", hrassess_competencies_master.name2 " & _
                   ", hrassess_competencies_master.isactive " & _
                   ", cfcommon_master.name AS CCategory " & _
                   ", ISNULL(SG.name,'') AS Score " & _
                   ", hrassess_competencies_master.periodunkid " & _
                   ", cfcommon_period_tran.period_name AS pname " & _
                   ", hrassess_competencies_master.competencegroupunkid " & _
                   ", ISNULL(CG.name,'') AS competence_group " & _
                   "FROM hrassess_competencies_master " & _
                   " JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = hrassess_competencies_master.periodunkid " & _
                   " JOIN cfcommon_master ON hrassess_competencies_master.competence_categoryunkid = cfcommon_master.masterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES & "' " & _
                   " LEFT JOIN cfcommon_master AS SG ON hrassess_competencies_master.scalemasterunkid = SG.masterunkid AND SG.mastertype = '" & clsCommon_Master.enCommonMaster.ASSESSMENT_SCALE_GROUP & "' " & _
                   " LEFT JOIN cfcommon_master AS CG ON hrassess_competencies_master.competencegroupunkid = CG.masterunkid AND CG.mastertype = '" & clsCommon_Master.enCommonMaster.COMPETENCE_GROUP & "'"
            'S.SANDEEP [21 NOV 2015] -- START {hrassess_competencies_master.competencegroupunkid} -- END


            If blnOnlyActive Then
                strQ &= " WHERE hrassess_competencies_master.isactive = 1 "
            End If


            ''S.SANDEEP [21 NOV 2015] -- START
            'objDataOperation.AddParameter("@NA", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Not Assigned"))
            ''S.SANDEEP [21 NOV 2015] -- END

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_competencies_master) </purpose>
    Public Function Insert(ByVal dtCourses As DataTable) As Boolean 'S.SANDEEP |29-MAR-2021| -- START {dtCourses} -- END

        'S.SANDEEP [21 NOV 2015] -- START
        If isExist(mintCompetence_Categoryunkid, mstrName, mintPeriodunkid, mintCompetenceGroupUnkid) Then
            'If isExist(mintCompetence_Categoryunkid, mstrName, mintPeriodunkid) Then
            'S.SANDEEP [21 NOV 2015] -- END
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Name is already defined for the selected category. Please define new Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@competence_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompetence_Categoryunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@scalemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScalemasterunkid.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, mstrName.Length, mstrName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Length, mstrDescription.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, mstrName1.Length, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, mstrName2.Length, mstrName2.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            'S.SANDEEP [21 NOV 2015] -- START
            objDataOperation.AddParameter("@competencegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompetenceGroupUnkid.ToString)
            'S.SANDEEP [21 NOV 2015] -- END


            strQ = "INSERT INTO hrassess_competencies_master ( " & _
                       "  competence_categoryunkid " & _
                       ", scalemasterunkid " & _
                       ", periodunkid " & _
                       ", name " & _
                       ", description " & _
                       ", name1 " & _
                       ", name2 " & _
                       ", isactive" & _
                       ", competencegroupunkid " & _
                   ") VALUES (" & _
                       "  @competence_categoryunkid " & _
                       ", @scalemasterunkid " & _
                       ", @periodunkid" & _
                       ", @name " & _
                       ", @description " & _
                       ", @name1 " & _
                       ", @name2 " & _
                       ", @isactive" & _
                       ", @competencegroupunkid " & _
                   "); SELECT @@identity" 'S.SANDEEP [21 NOV 2015] -- START {competencegroupunkid} --END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCompetenciesunkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_competencies_master", "competenciesunkid", mintCompetenciesunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP |29-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Training
            If dtCourses IsNot Nothing Then
                objCmptTraining._mDataOperation = objDataOperation
                objCmptTraining._Competenciesunkid = mintCompetenciesunkid
                objCmptTraining._DataTable = dtCourses.Copy
                If objCmptTraining.InsertUpdateDelete_Competencies_TrainingCourses() = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'S.SANDEEP |29-MAR-2021| -- END
            

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_competencies_master) </purpose>
    Public Function Update(ByVal dtCourses As DataTable, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean 'S.SANDEEP |29-MAR-2021| -- START {dtCourses} -- END 'S.SANDEEP [30 JAN 2016] -- START {} -- END
        'S.SANDEEP [21 NOV 2015] -- START
        If isExist(mintCompetence_Categoryunkid, mstrName, mintPeriodunkid, mintCompetenceGroupUnkid, mintCompetenciesunkid) Then
            'If isExist(mintCompetence_Categoryunkid, mstrName, mintPeriodunkid, mintCompetenciesunkid) Then
            'S.SANDEEP [21 NOV 2015] -- END
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Name is already defined for the selected category. Please define new Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'S.SANDEEP [15 FEB 2016] -- START
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        objDataOperation.BindTransaction()
        'S.SANDEEP [15 FEB 2016] -- END
        
        Try
            objDataOperation.AddParameter("@competenciesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompetenciesunkid.ToString)
            objDataOperation.AddParameter("@competence_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompetence_Categoryunkid.ToString)
            objDataOperation.AddParameter("@scalemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScalemasterunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, mstrName.Length, mstrName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Length, mstrDescription.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, mstrName1.Length, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, mstrName2.Length, mstrName2.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            'S.SANDEEP [21 NOV 2015] -- START
            objDataOperation.AddParameter("@competencegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompetenceGroupUnkid.ToString)
            'S.SANDEEP [21 NOV 2015] -- END

            strQ = "UPDATE hrassess_competencies_master SET " & _
                      "  competence_categoryunkid = @competence_categoryunkid" & _
                      ", scalemasterunkid = @scalemasterunkid" & _
                      ", periodunkid = @periodunkid " & _
                      ", name = @name" & _
                      ", description = @description" & _
                      ", name1 = @name1" & _
                      ", name2 = @name2" & _
                      ", isactive = @isactive " & _
                      ", competencegroupunkid = @competencegroupunkid " & _
                    "WHERE competenciesunkid = @competenciesunkid " 'S.SANDEEP [21 NOV 2015] -- START {competencegroupunkid} --END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_competencies_master", mintCompetenciesunkid, "competenciesunkid", 2, objDataOperation) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_competencies_master", "competenciesunkid", mintCompetenciesunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP |29-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Training
            If dtCourses IsNot Nothing Then
                objCmptTraining._mDataOperation = objDataOperation
                objCmptTraining._Competenciesunkid = mintCompetenciesunkid
                objCmptTraining._DataTable = dtCourses.Copy
                If objCmptTraining.InsertUpdateDelete_Competencies_TrainingCourses() = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'S.SANDEEP |29-MAR-2021| -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [15 FEB 2016] -- START
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [15 FEB 2016] -- END
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_competencies_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal dtCourses As DataTable) As Boolean 'S.SANDEEP |29-MAR-2021| -- START {dtCourses} -- END
        mstrMessage = ""
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, you cannot do delete operation. Reason : Selected competency is already linked with some transaction.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        'S.SANDEEP [22 SEP 2015] -- START
        objDataOperation.BindTransaction()
        'S.SANDEEP [22 SEP 2015] -- END

        Try
            strQ = "UPDATE hrassess_competencies_master " & _
                   " SET isactive = 0 " & _
                   "WHERE competenciesunkid = @competenciesunkid "

            objDataOperation.AddParameter("@competenciesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [22 SEP 2015] -- START
            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_competencies_master", "competenciesunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP |29-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Training
            If dtCourses IsNot Nothing Then
                objCmptTraining._mDataOperation = objDataOperation
                objCmptTraining._Competenciesunkid = mintCompetenciesunkid
                objCmptTraining._DataTable = dtCourses.Copy
                If objCmptTraining.InsertUpdateDelete_Competencies_TrainingCourses() = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'S.SANDEEP |29-MAR-2021| -- END

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [22 SEP 2015] -- END


            Return True
        Catch ex As Exception
            'S.SANDEEP [22 SEP 2015] -- START
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [22 SEP 2015] -- END
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'S.SANDEEP [22 SEP 2015] -- START
            'strQ = "SELECT 1 FROM hrcompetency_analysis_tran WHERE isvoid = 0 UNION " & _
            '       "SELECT 1 FROM hrassess_competence_assign_tran WHERE isvoid = 0 "

            strQ = "SELECT 1 FROM hrcompetency_analysis_tran WHERE isvoid = 0 AND competenciesunkid = @competenciesunkid UNION " & _
                   "SELECT 1 FROM hrassess_competence_assign_tran WHERE isvoid = 0 AND competenciesunkid = @competenciesunkid "
            'S.SANDEEP [22 SEP 2015] -- END
            

            objDataOperation.AddParameter("@competenciesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iCategoryId As Integer, _
                            ByVal strName As String, _
                            ByVal iPeriodId As Integer, _
                            ByVal iCompetenceGroupUnkid As Integer, _
                            Optional ByVal intUnkid As Integer = -1) As Boolean
        'Public Function isExist(ByVal iCategoryId As Integer, ByVal strName As String, ByVal iPeriodId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean 'S.SANDEEP [21 NOV 2015] -- START {} -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  competenciesunkid " & _
              ", competence_categoryunkid " & _
              ", scalemasterunkid " & _
              ", periodunkid " & _
              ", name " & _
              ", description " & _
              ", name1 " & _
              ", name2 " & _
              ", isactive " & _
                   ", competencegroupunkid " & _
             "FROM hrassess_competencies_master " & _
             "WHERE name = @name " & _
             "AND competence_categoryunkid = @competence_categoryunkid AND isactive = 1 "
            'S.SANDEEP [21 NOV 2015] -- START {competencegroupunkid} --END

            If iPeriodId > 0 Then
                strQ &= " AND periodunkid = @periodunkid"
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)
            End If

            'S.SANDEEP [21 NOV 2015] -- START
            If iCompetenceGroupUnkid > 0 Then
                strQ &= " AND competencegroupunkid = @competencegroupunkid"
                objDataOperation.AddParameter("@competencegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCompetenceGroupUnkid)
            End If
            'S.SANDEEP [21 NOV 2015] -- END

            If intUnkid > 0 Then
                strQ &= " AND competenciesunkid <> @competenciesunkid"
            End If

            objDataOperation.AddParameter("@competence_categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCategoryId)

            'Shani(06-Feb-2016) -- Start
            'PA Changes Given By Glory for CCBRT
            'objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, strName.Length, strName)
            'Shani(06-Feb-2016) -- End

            objDataOperation.AddParameter("@competenciesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(ByVal iCategoryId As Integer, ByVal iPeriodId As Integer, Optional ByVal iAddSelect As Boolean = False, Optional ByVal iList As String = "List") As DataSet
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            If iAddSelect = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name,0 AS CatId UNION "
            End If
            StrQ &= "SELECT " & _
                        "  competenciesunkid AS Id " & _
                        ", name AS Name " & _
                        ", competence_categoryunkid  AS CatId " & _
                    "FROM hrassess_competencies_master " & _
                    "WHERE isactive = 1 AND competence_categoryunkid = '" & iCategoryId & "' AND periodunkid = '" & iPeriodId & "' "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetCompetenciesForShortlisting(ByVal iAssessGrpId As Integer, ByVal iPeriod As Integer) As DataTable
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Dim dsList As New DataSet
        Try
            StrQ &= "SELECT hrassess_competencies_master.competenciesunkid AS Id " & _
                        "   ,hrassess_competencies_master.name AS Name " & _
                        "FROM hrassess_competencies_master " & _
                        "   JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrassess_competencies_master.competenciesunkid " & _
                        "   JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                        "WHERE hrassess_competence_assign_master.periodunkid = '" & iPeriod & "' and hrassess_competence_assign_master.assessgroupunkid IN(" & iAssessGrpId & ") " & _
                        "   AND hrassess_competence_assign_master.isvoid = 0 AND hrassess_competence_assign_tran.isvoid = 0 "
            
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getAssigned_Competencies_List", mstrModuleName)
        Finally
        End Try
        Return dsList.Tables(0)
    End Function

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function getAssigned_Competencies_List(ByVal iEmployeeId As Integer, _
                                                  ByVal iPeriod As Integer, _
                                                  ByVal iEmployeeAsOnDate As DateTime, _
                                                  Optional ByVal iAddSelect As Boolean = False, _
                                                  Optional ByVal blnSelfAssign As Boolean = False) As DataSet 'S.SANDEEP [29 JAN 2015] -- START {blnSelfAssign} -- END

        'Public Function getAssigned_Competencies_List(ByVal iEmployeeId As Integer, _
        '                                          ByVal iPeriod As Integer, _
        '                                          Optional ByVal iAddSelect As Boolean = False, _
        '                                          Optional ByVal blnSelfAssign As Boolean = False) As DataSet 'S.SANDEEP [29 JAN 2015] -- START {blnSelfAssign} -- END
        'S.SANDEEP [04 JUN 2015] -- END

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Dim dsList As New DataSet
        Try
            Dim iStringVal As String = String.Empty
            Dim objGMaster As New clsassess_group_master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'iStringVal = objGMaster.GetCSV_AssessGroupIds(iEmployeeId)

            'S.SANDEEP [19 DEC 2016] -- START
            'iStringVal = objGMaster.GetCSV_AssessGroupIds(iEmployeeId, iEmployeeAsOnDate)
            iStringVal = objGMaster.GetCSV_AssessGroupIds(iEmployeeId, iEmployeeAsOnDate, iPeriod)
            'S.SANDEEP [19 DEC 2016] -- END

            'S.SANDEEP [04 JUN 2015] -- END

            objGMaster = Nothing
            If iStringVal.Trim.Length > 0 Then
                If iAddSelect = True Then
                    StrQ = "SELECT 0 AS Id, @Select As Name UNION "
                End If
                StrQ &= "SELECT hrassess_competencies_master.competenciesunkid AS Id " & _
                        "   ,hrassess_competencies_master.name AS Name " & _
                        "FROM hrassess_competencies_master " & _
                        "   JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrassess_competencies_master.competenciesunkid " & _
                        "   JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                        "WHERE hrassess_competence_assign_master.periodunkid = '" & iPeriod & "' and hrassess_competence_assign_master.assessgroupunkid IN(" & iStringVal & ") " & _
                        "   AND hrassess_competence_assign_master.isvoid = 0 AND hrassess_competence_assign_tran.isvoid = 0 "

                'S.SANDEEP [29 JAN 2015] -- START
                If blnSelfAssign = True Then
                    StrQ &= "AND hrassess_competence_assign_master.employeeunkid = @employeeunkid "
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeId)
                End If
                'S.SANDEEP [29 JAN 2015] -- END

            Else
                StrQ = "SELECT 0 AS Id, @Select As Name "
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getAssigned_Competencies_List", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'S.SANDEEP [21 NOV 2015] -- START
    Public Function Grouped_Competencies(ByVal iAssessGrpId As Integer, _
                                         ByVal iPeriodId As Integer, _
                                         ByVal iMode As Integer, _
                                         Optional ByVal iList As String = "List", _
                                         Optional ByVal iEmployeeId As Integer = 0, _
                                         Optional ByVal blnCompetencyByJob As Boolean = False, _
                                         Optional ByVal dtEmployeeAsOnDate As Date = Nothing) As DataTable 'S.SANDEEP [23 DEC 2015] -- START {blnCompetencyByJob} -- END

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim mdtFinal As DataTable = Nothing
        Dim dsList As New DataSet

        'S.SANDEEP [28 DEC 2015] -- START
        Dim objJobCompetencies As New clsJobCompetencies_Tran
        Dim strCSVCategoryUnkids As String = ""
        'S.SANDEEP [28 DEC 2015] -- END

        objDataOperation = New clsDataOperation
        Try
            'S.SANDEEP [23 DEC 2015] -- START
            Dim intJobUnkid As Integer = 0
            'S.SANDEEP [29 DEC 2015] -- START
            'If iMode = 1 Then
            If blnCompetencyByJob = True Then
                'S.SANDEEP [29 DEC 2015] -- END

                If iEmployeeId > 0 Then
                    Dim objRecategorize As New clsemployee_categorization_Tran
                    dsList = objRecategorize.Get_Current_Job(dtEmployeeAsOnDate, iEmployeeId)
                    objRecategorize = Nothing
                    If dsList.Tables(0).Rows.Count > 0 Then
                        intJobUnkid = dsList.Tables(0).Rows(0)("jobunkid")
                    End If
                    objJobCompetencies._Jobunkid = intJobUnkid
                    If objJobCompetencies._mdtCompetenciesTran.Rows.Count <= 0 Then
                        mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, there are no competencies mapped with the selected employee's Job.")
                        Return Nothing
                    Else
                        strCSVCategoryUnkids = String.Join(",", objJobCompetencies._mdtCompetenciesTran.AsEnumerable().Select(Function(x) x.Field(Of Integer)("competence_categoryunkid").ToString()).ToArray())
                    End If
                    dsList = New DataSet
                End If
            End If
            'S.SANDEEP [23 DEC 2015] -- END


            If iList.Trim.Length <= 0 Then iList = "List"

            mdtFinal = New DataTable(iList)

            mdtFinal.Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtFinal.Columns.Add("competence", System.Type.GetType("System.String")).DefaultValue = ""
            mdtFinal.Columns.Add("scale", System.Type.GetType("System.String")).DefaultValue = ""
            mdtFinal.Columns.Add("weight", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtFinal.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtFinal.Columns.Add("competenciesunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("assigncompetencetranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("assigncompetenceunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("PGrId", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFinal.Columns.Add("IsPGrp", System.Type.GetType("System.Boolean")).DefaultValue = 0
            mdtFinal.Columns.Add("linkcol", System.Type.GetType("System.String")).DefaultValue = Language.getMessage(mstrModuleName, 5, "What is this?")

            StrQ = "SELECT " & _
                   "     @CGRP + ' : ' + name AS competence " & _
                   "    ,masterunkid AS GrpId " & _
                   "    ,CAST(1 AS BIT) AS IsGrp " & _
                   "    ,masterunkid AS PGrId " & _
                   "    ,CAST(1 AS BIT) AS IsPGrp " & _
                   "FROM cfcommon_master WHERE isactive = 1 AND mastertype = '" & clsCommon_Master.enCommonMaster.COMPETENCE_GROUP & "' " & _
                   " UNION SELECT @CGRP + ' : ' + @Default AS competence, 0 AS GrpId,CAST(1 AS BIT) AS IsGrp,0 AS PGrId,CAST(1 AS BIT) AS IsPGrp "

            objDataOperation.AddParameter("@Default", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Default"))
            objDataOperation.AddParameter("@CGRP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Competence Group"))

            Dim dsMst As New DataSet

            dsMst = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            StrQ = "SELECT "

            If iMode = 1 Then
                StrQ &= "  ischeck "
            ElseIf iMode = 2 Then
                StrQ &= "  CASE WHEN assigncompetencetranunkid > 0 THEN 1 ELSE 0 END AS ischeck "
            End If

            StrQ &= ", competence " & _
                    ", scale "

            If iMode = 1 Then
                StrQ &= ", CAST(weight AS DECIMAL(10,2)) AS weight "
            ElseIf iMode = 2 Then
                StrQ &= ",CASE WHEN A.IsGrp = 1 THEN ISNULL(CAST(iTotal AS DECIMAL(10,2)),0) ELSE CAST(weight AS DECIMAL(10,2)) END AS weight "
            End If

            StrQ &= ", GrpId " & _
                    ", IsGrp " & _
                    ", competenciesunkid " & _
                    ", assigncompetencetranunkid " & _
                    ", assigncompetenceunkid " & _
                    ", PGrId " & _
                    "FROM " & _
                    "( " & _
                    "		SELECT DISTINCT " & _
                    "			  CAST(0 AS BIT) AS ischeck " & _
                    "			, cfcommon_master.name AS competence " & _
                    "			, '' AS scale " & _
                    "			, masterunkid AS GrpId " & _
                    "			, CAST(1 AS BIT) AS IsGrp " & _
                    "			, 0 AS competenciesunkid " & _
                    "			, 0 AS weight " & _
                    "           , 0 AS assigncompetencetranunkid " & _
                    "           , 0 AS assigncompetenceunkid " & _
                    "           , competencegroupunkid AS PGrId " & _
                    "		FROM cfcommon_master " & _
                    "           LEFT JOIN hrassess_competencies_master ON competence_categoryunkid = cfcommon_master.masterunkid AND hrassess_competencies_master.isactive = 1 " & _
                    "		WHERE cfcommon_master.isactive = 1 AND mastertype = '" & clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES & "' "

            'S.SANDEEP [28 DEC 2015] -- START
            If strCSVCategoryUnkids.Trim.Length > 0 Then
                StrQ &= "AND cfcommon_master.masterunkid IN( " & strCSVCategoryUnkids & ") "
            End If
            'S.SANDEEP [28 DEC 2015] -- END

            StrQ &= "	UNION ALL " & _
                    "		SELECT "

            'S.SANDEEP [23 DEC 2015] -- START
            If intJobUnkid > 0 Then
                StrQ &= "	CASE WHEN hrjobcompetencies_tran.competenciesunkid > 0 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS ischeck "
            Else
                StrQ &= "	CAST(0 AS BIT) AS ischeck "
            End If
            'S.SANDEEP [23 DEC 2015] -- END

            StrQ &= "			, SPACE(5) + hrassess_competencies_master.name AS competence " & _
                    "			, ISNULL(cfcommon_master.name,'') AS scale " & _
                    "			, hrassess_competencies_master.competence_categoryunkid AS GrpId " & _
                    "			, CAST(0 AS BIT) AS IsGrp " & _
                    "			, hrassess_competencies_master.competenciesunkid "
            If iMode = 1 Then   'ADD
                StrQ &= "			, 0 AS weight " & _
                        "           , 0 AS assigncompetencetranunkid " & _
                        "           , 0 AS assigncompetenceunkid "

            ElseIf iMode = 2 Then   'EDIT
                StrQ &= " , ISNULL(weight,0) AS weight " & _
                        "           , ISNULL(assigncompetencetranunkid,0) AS assigncompetencetranunkid " & _
                        " , ISNULL(assigncompetenceunkid,0) AS assigncompetenceunkid "
            End If
            StrQ &= "		,hrassess_competencies_master.competencegroupunkid AS PGrId " & _
                    " FROM hrassess_competencies_master "

            'S.SANDEEP [23 DEC 2015] -- START
            If intJobUnkid > 0 Then
                StrQ &= " LEFT JOIN hrjobcompetencies_tran ON hrassess_competencies_master.competenciesunkid = hrjobcompetencies_tran.competenciesunkid " & _
                        " AND hrjobcompetencies_tran.isvoid = 0 AND hrjobcompetencies_tran.jobunkid = '" & intJobUnkid & "' "

                'S.SANDEEP [31 DEC 2015] -- START
                '" AND hrjobcompetencies_tran.isvoid = 0 AND hrjobcompetencies_tran.jobunkid = '" & intJobUnkid & "' " -- ADDED
                'S.SANDEEP [31 DEC 2015] -- END

            End If
            'S.SANDEEP [23 DEC 2015] -- END

            If iMode = 2 Then
                StrQ &= "LEFT JOIN " & _
                        "( " & _
                        "    SELECT " & _
                        "         hrassess_competence_assign_tran.competenciesunkid AS CmpId " & _
                        "        ,assigncompetencetranunkid " & _
                        "        ,hrassess_competence_assign_tran.weight " & _
                        "        ,hrassess_competence_assign_master.assigncompetenceunkid " & _
                        "    FROM hrassess_competence_assign_tran " & _
                        "        JOIN hrassess_competence_assign_master ON hrassess_competence_assign_tran.assigncompetenceunkid = hrassess_competence_assign_master.assigncompetenceunkid " & _
                        "    WHERE hrassess_competence_assign_master.isvoid = 0 AND hrassess_competence_assign_master.assessgroupunkid = '" & iAssessGrpId & "' " & _
                        "        AND hrassess_competence_assign_master.periodunkid = '" & iPeriodId & "' " & _
                        "        AND hrassess_competence_assign_tran.isvoid = 0 "
                If iEmployeeId > 0 Then
                    StrQ &= "  AND hrassess_competence_assign_master.employeeunkid = '" & iEmployeeId & "' "
                End If
                StrQ &= ") AS C ON C.CmpId = hrassess_competencies_master.competenciesunkid "
            End If

            StrQ &= " LEFT JOIN cfcommon_master ON hrassess_competencies_master.scalemasterunkid = cfcommon_master.masterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.ASSESSMENT_SCALE_GROUP & "' " & _
                    "		WHERE hrassess_competencies_master.isactive = 1 AND hrassess_competencies_master.periodunkid = '" & iPeriodId & "'"

            'S.SANDEEP [28 DEC 2015] -- START
            If strCSVCategoryUnkids.Trim.Length > 0 Then
                StrQ &= "AND hrassess_competencies_master.competence_categoryunkid IN( " & strCSVCategoryUnkids & ") "
            End If
            'S.SANDEEP [28 DEC 2015] -- END


            StrQ &= ") AS A "
            If iMode = 2 Then
                StrQ &= "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        competence_categoryunkid AS iGrpId " & _
                        "       ,1 AS iIsGrp " & _
                        "       ,ISNULL(SUM(hrassess_competence_assign_tran.weight),0) AS iTotal " & _
                        "   FROM hrassess_competence_assign_tran " & _
                        "       JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                        "       JOIN hrassess_competencies_master ON hrassess_competence_assign_tran.competenciesunkid = hrassess_competencies_master.competenciesunkid " & _
                        "   WHERE hrassess_competencies_master.isactive = 1 AND hrassess_competence_assign_tran.isvoid = 0 AND hrassess_competence_assign_master.assessgroupunkid = '" & iAssessGrpId & "' " & _
                        "       AND hrassess_competence_assign_master.periodunkid = '" & iPeriodId & "' "
                If iEmployeeId > 0 Then
                    StrQ &= " AND hrassess_competence_assign_master.employeeunkid = '" & iEmployeeId & "' "
                End If

                'S.SANDEEP [28 DEC 2015] -- START
                If strCSVCategoryUnkids.Trim.Length > 0 Then
                    StrQ &= "AND competence_categoryunkid IN( " & strCSVCategoryUnkids & ") "
                End If
                'S.SANDEEP [28 DEC 2015] -- END

                StrQ &= "GROUP BY competence_categoryunkid " & _
                        ") AS B ON A.GrpId = B.iGrpId AND A.IsGrp = B.iIsGrp "
            End If

            StrQ &= " ORDER BY A.GrpId,A.IsGrp DESC,A.competence "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsMst.Tables("List").Rows.Count > 0 Then
                For Each dr As DataRow In dsMst.Tables("List").Rows
                    Dim drow() As DataRow = dsList.Tables("List").Select("PGrId = '" & dr("PGrId") & "'", "GrpId,IsGrp DESC,competence")
                    If drow.Length > 0 Then
                        Dim dfrow As DataRow = mdtFinal.NewRow
                        dfrow.Item("competence") = dr("competence")
                        dfrow.Item("GrpId") = dr("GrpId")
                        dfrow.Item("IsGrp") = dr("IsGrp")
                        dfrow.Item("PGrId") = dr("PGrId")
                        dfrow.Item("IsPGrp") = dr("IsPGrp")
                        mdtFinal.Rows.Add(dfrow)

                        For ridx As Integer = 0 To drow.Length - 1
                            dfrow = mdtFinal.NewRow

                            dfrow.Item("ischeck") = drow(ridx)("ischeck")
                            dfrow.Item("competence") = drow(ridx)("competence")
                            dfrow.Item("scale") = drow(ridx)("scale")
                            dfrow.Item("weight") = drow(ridx)("weight")
                            dfrow.Item("GrpId") = drow(ridx)("GrpId")
                            dfrow.Item("IsGrp") = drow(ridx)("IsGrp")
                            dfrow.Item("competenciesunkid") = drow(ridx)("competenciesunkid")
                            dfrow.Item("assigncompetencetranunkid") = drow(ridx)("assigncompetencetranunkid")
                            dfrow.Item("assigncompetenceunkid") = drow(ridx)("assigncompetenceunkid")
                            dfrow.Item("PGrId") = dr("PGrId")
                            dfrow.Item("IsPGrp") = False

                            mdtFinal.Rows.Add(dfrow)
                        Next
                    End If
                Next
            Else
            mdtFinal = dsList.Tables(0).Copy
            End If
            'Shani(24-Feb-2016) -- Start
            Dim strids As String = ""
            For Each dRow As DataRow In mdtFinal.Select("IsGrp = true AND IsPGrp = false ")
                If mdtFinal.Select("GrpId = " & dRow("GrpId") & " AND IsGrp = false AND IsPGrp = false").Length <= 0 Then
                    strids &= "," & dRow("GrpId")
                End If
            Next

            'Shani(01-MAR-2016) -- Start
            'Enhancement
            'strids = strids.Substring(1, strids.Length - 1)
            'mdtFinal = New DataView(mdtFinal, "GrpId NOT IN (" & strids & ")", "", DataViewRowState.CurrentRows).ToTable
            If strids.Trim.Length > 0 Then
            strids = strids.Substring(1, strids.Length - 1)
            mdtFinal = New DataView(mdtFinal, "GrpId NOT IN (" & strids & ")", "", DataViewRowState.CurrentRows).ToTable
            End If
            'Shani(01-MAR-2016) -- End

            strids = ""
            For Each dRow As DataRow In mdtFinal.Select("IsPGrp = TRUE ")
                If mdtFinal.Select("PGrId = " & dRow("PGrId") & " AND IsPGrp = FALSE").Length <= 0 Then
                    strids &= "," & dRow("PGrId")
                End If
            Next

            'Shani(01-MAR-2016) -- Start
            'Enhancement
            'strids = strids.Substring(1, strids.Length - 1)
            'mdtFinal = New DataView(mdtFinal, "PGrId NOT IN (" & strids & ")", "", DataViewRowState.CurrentRows).ToTable
            If strids.Trim.Length > 0 Then
            strids = strids.Substring(1, strids.Length - 1)
            mdtFinal = New DataView(mdtFinal, "PGrId NOT IN (" & strids & ")", "", DataViewRowState.CurrentRows).ToTable
            End If

            'Shani(01-MAR-2016) -- End

            'Shani(24-Feb-2016) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Grouped_Competencies", mstrModuleName)
        Finally
        End Try
        Return mdtFinal
    End Function

    'Public Function Grouped_Competencies(ByVal iAssessGrpId As Integer, _
    '                                     ByVal iPeriodId As Integer, _
    '                                     ByVal iMode As Integer, _
    '                                     Optional ByVal iList As String = "List", _
    '                                     Optional ByVal iEmployeeId As Integer = 0) As DataTable 'S.SANDEEP [29 JAN 2015] -- START {iEmployeeId} -- END
    '    'Public Function Grouped_Competencies(ByVal iAssessGrpId As Integer, ByVal iPeriodId As Integer, ByVal iMode As Integer, Optional ByVal iList As String = "List") As DataTable
    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Dim mdtFinal As DataTable = Nothing
    '    Dim dsList As New DataSet
    '    objDataOperation = New clsDataOperation
    '    Try
    '        'S.SANDEEP [ 17 DEC 2014 ] -- START
    '        'If iList.Trim.Length <= 0 Then iList = "List"
    '        'StrQ = "SELECT "
    '        'If iMode = 1 Then
    '        '    StrQ &= "  ischeck "
    '        'ElseIf iMode = 2 Then
    '        '    StrQ &= "  CASE WHEN assigncompetencetranunkid > 0 THEN 1 ELSE 0 END AS ischeck "
    '        'End If
    '        'StrQ &= ", competence " & _
    '        '        ", scale "
    '        'If iMode = 1 Then
    '        '    StrQ &= ", CAST(weight AS DECIMAL(10,2)) AS weight "
    '        'ElseIf iMode = 2 Then
    '        '    StrQ &= ",CASE WHEN A.IsGrp = 1 THEN ISNULL(CAST(iTotal AS DECIMAL(10,2)),0) ELSE CAST(weight AS DECIMAL(10,2)) END AS weight "
    '        'End If
    '        'StrQ &= ", GrpId " & _
    '        '        ", IsGrp " & _
    '        '        ", competenciesunkid " & _
    '        '        ", assigncompetencetranunkid " & _
    '        '        ", assigncompetenceunkid " & _
    '        '        "FROM " & _
    '        '        "( " & _
    '        '        "		SELECT " & _
    '        '        "			  CAST(0 AS BIT) AS ischeck " & _
    '        '        "			, name AS competence " & _
    '        '        "			, '' AS scale " & _
    '        '        "			, masterunkid AS GrpId " & _
    '        '        "			, CAST(1 AS BIT) AS IsGrp " & _
    '        '        "			, 0 AS competenciesunkid " & _
    '        '        "			, 0 AS weight " & _
    '        '        "           , 0 AS assigncompetencetranunkid " & _
    '        '        "           , 0 AS assigncompetenceunkid " & _
    '        '        "		FROM cfcommon_master " & _
    '        '        "		WHERE isactive = 1 AND mastertype = '" & clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES & "' " & _
    '        '        "	UNION ALL " & _
    '        '        "		SELECT " & _
    '        '        "			  CAST(0 AS BIT) AS ischeck " & _
    '        '        "			, SPACE(5) + hrassess_competencies_master.name AS competence " & _
    '        '        "			, ISNULL(cfcommon_master.name,'') AS scale " & _
    '        '        "			, competence_categoryunkid AS GrpId " & _
    '        '        "			, CAST(0 AS BIT) AS IsGrp " & _
    '        '        "			, hrassess_competencies_master.competenciesunkid "
    '        'If iMode = 1 Then   'ADD
    '        '    StrQ &= "			, 0 AS weight " & _
    '        '            "           , 0 AS assigncompetencetranunkid " & _
    '        '            "           , 0 AS assigncompetenceunkid "
    '        'ElseIf iMode = 2 Then   'EDIT
    '        '    StrQ &= "			, ISNULL(hrassess_competence_assign_tran.weight,0) AS weight " & _
    '        '            "           , ISNULL(assigncompetencetranunkid,0) AS assigncompetencetranunkid " & _
    '        '            "           , ISNULL(hrassess_competence_assign_master.assigncompetenceunkid,0) AS assigncompetenceunkid "
    '        'End If
    '        'StrQ &= "		FROM hrassess_competencies_master "
    '        'If iMode = 2 Then
    '        '    StrQ &= "            LEFT JOIN hrassess_competence_assign_tran ON hrassess_competencies_master.competenciesunkid = hrassess_competence_assign_tran.competenciesunkid AND hrassess_competence_assign_tran.isvoid = 0 " & _
    '        '            "            LEFT JOIN hrassess_competence_assign_master ON hrassess_competence_assign_tran.assigncompetenceunkid = hrassess_competence_assign_master.assigncompetenceunkid AND hrassess_competence_assign_master.isvoid = 0 " & _
    '        '            "                AND hrassess_competence_assign_master.assessgroupunkid = '" & iAssessGrpId & "' AND hrassess_competence_assign_master.periodunkid = '" & iPeriodId & "' "
    '        'End If
    '        'StrQ &= "		    JOIN cfcommon_master ON hrassess_competencies_master.scalemasterunkid = cfcommon_master.masterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.ASSESSMENT_SCALE_GROUP & "' " & _
    '        '        "		WHERE hrassess_competencies_master.isactive = 1 AND hrassess_competencies_master.periodunkid = '" & iPeriodId & "') AS A "
    '        'If iMode = 2 Then
    '        '    StrQ &= "LEFT JOIN " & _
    '        '            "( " & _
    '        '            "   SELECT " & _
    '        '            "        competence_categoryunkid AS iGrpId " & _
    '        '            "       ,1 AS iIsGrp " & _
    '        '            "       ,ISNULL(SUM(hrassess_competence_assign_tran.weight),0) AS iTotal " & _
    '        '            "   FROM hrassess_competence_assign_tran " & _
    '        '            "       JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
    '        '            "       JOIN hrassess_competencies_master ON hrassess_competence_assign_tran.competenciesunkid = hrassess_competencies_master.competenciesunkid " & _
    '        '            "   WHERE hrassess_competencies_master.isactive = 1 AND hrassess_competence_assign_tran.isvoid = 0 AND hrassess_competence_assign_master.assessgroupunkid = '" & iAssessGrpId & "' " & _
    '        '            "       AND hrassess_competence_assign_master.periodunkid = '" & iPeriodId & "' " & _
    '        '            "GROUP BY competence_categoryunkid " & _
    '        '            ") AS B ON A.GrpId = B.iGrpId AND A.IsGrp = B.iIsGrp "
    '        'End If
    '        If iList.Trim.Length <= 0 Then iList = "List"
    '        StrQ = "SELECT "
    '        If iMode = 1 Then
    '            StrQ &= "  ischeck "
    '        ElseIf iMode = 2 Then
    '            StrQ &= "  CASE WHEN assigncompetencetranunkid > 0 THEN 1 ELSE 0 END AS ischeck "
    '        End If
    '        StrQ &= ", competence " & _
    '                ", scale "
    '        If iMode = 1 Then
    '            StrQ &= ", CAST(weight AS DECIMAL(10,2)) AS weight "
    '        ElseIf iMode = 2 Then
    '            StrQ &= ",CASE WHEN A.IsGrp = 1 THEN ISNULL(CAST(iTotal AS DECIMAL(10,2)),0) ELSE CAST(weight AS DECIMAL(10,2)) END AS weight "
    '        End If
    '        StrQ &= ", GrpId " & _
    '                ", IsGrp " & _
    '                ", competenciesunkid " & _
    '                ", assigncompetencetranunkid " & _
    '                ", assigncompetenceunkid " & _
    '                "FROM " & _
    '                "( " & _
    '                "		SELECT " & _
    '                "			  CAST(0 AS BIT) AS ischeck " & _
    '                "			, name AS competence " & _
    '                "			, '' AS scale " & _
    '                "			, masterunkid AS GrpId " & _
    '                "			, CAST(1 AS BIT) AS IsGrp " & _
    '                "			, 0 AS competenciesunkid " & _
    '                "			, 0 AS weight " & _
    '                "           , 0 AS assigncompetencetranunkid " & _
    '                "           , 0 AS assigncompetenceunkid " & _
    '                "		FROM cfcommon_master " & _
    '                "		WHERE isactive = 1 AND mastertype = '" & clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES & "' " & _
    '                "	UNION ALL " & _
    '                "		SELECT " & _
    '                "			  CAST(0 AS BIT) AS ischeck " & _
    '                "			, SPACE(5) + hrassess_competencies_master.name AS competence " & _
    '                "			, ISNULL(cfcommon_master.name,'') AS scale " & _
    '                "			, competence_categoryunkid AS GrpId " & _
    '                "			, CAST(0 AS BIT) AS IsGrp " & _
    '                "			, hrassess_competencies_master.competenciesunkid "
    '        If iMode = 1 Then   'ADD
    '            StrQ &= "			, 0 AS weight " & _
    '                    "           , 0 AS assigncompetencetranunkid " & _
    '                    "           , 0 AS assigncompetenceunkid "

    '            'S.SANDEEP [09 FEB 2015] -- START
    '            'ElseIf iMode = 2 Then   'EDIT
    '            '    StrQ &= "			, ISNULL(hrassess_competence_assign_tran.weight,0) AS weight " & _
    '            '            "           , ISNULL(assigncompetencetranunkid,0) AS assigncompetencetranunkid " & _
    '            '            "           , ISNULL(hrassess_competence_assign_master.assigncompetenceunkid,0) AS assigncompetenceunkid "
    '            'End If
    '            'StrQ &= "		FROM hrassess_competencies_master "
    '            'If iMode = 2 Then
    '            '    StrQ &= "            JOIN hrassess_competence_assign_tran ON hrassess_competencies_master.competenciesunkid = hrassess_competence_assign_tran.competenciesunkid AND hrassess_competence_assign_tran.isvoid = 0 " & _
    '            '            "            JOIN hrassess_competence_assign_master ON hrassess_competence_assign_tran.assigncompetenceunkid = hrassess_competence_assign_master.assigncompetenceunkid AND hrassess_competence_assign_master.isvoid = 0 " & _
    '            '            "                AND hrassess_competence_assign_master.assessgroupunkid = '" & iAssessGrpId & "' AND hrassess_competence_assign_master.periodunkid = '" & iPeriodId & "' "
    '        ElseIf iMode = 2 Then   'EDIT
    '            StrQ &= " , ISNULL(weight,0) AS weight " & _
    '                    "           , ISNULL(assigncompetencetranunkid,0) AS assigncompetencetranunkid " & _
    '                    " , ISNULL(assigncompetenceunkid,0) AS assigncompetenceunkid "
    '        End If
    '        StrQ &= "		FROM hrassess_competencies_master "
    '        If iMode = 2 Then
    '            StrQ &= "LEFT JOIN " & _
    '                    "( " & _
    '                    "    SELECT " & _
    '                    "         competenciesunkid AS CmpId " & _
    '                    "        ,assigncompetencetranunkid " & _
    '                    "        ,hrassess_competence_assign_tran.weight " & _
    '                    "        ,hrassess_competence_assign_master.assigncompetenceunkid " & _
    '                    "    FROM hrassess_competence_assign_tran " & _
    '                    "        JOIN hrassess_competence_assign_master ON hrassess_competence_assign_tran.assigncompetenceunkid = hrassess_competence_assign_master.assigncompetenceunkid " & _
    '                    "    WHERE hrassess_competence_assign_master.isvoid = 0 AND hrassess_competence_assign_master.assessgroupunkid = '" & iAssessGrpId & "' " & _
    '                    "        AND hrassess_competence_assign_master.periodunkid = '" & iPeriodId & "' " & _
    '                    "        AND hrassess_competence_assign_tran.isvoid = 0 "
    '            If iEmployeeId > 0 Then
    '                StrQ &= "  AND hrassess_competence_assign_master.employeeunkid = '" & iEmployeeId & "' "
    '            End If
    '            StrQ &= ") AS C ON C.CmpId = competenciesunkid "
    '            'S.SANDEEP [09 FEB 2015] -- END
    '        End If

    '        'S.SANDEEP [23 APR 2015] -- START
    '        'StrQ &= "		    JOIN cfcommon_master ON hrassess_competencies_master.scalemasterunkid = cfcommon_master.masterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.ASSESSMENT_SCALE_GROUP & "' " & _
    '        '                    "		WHERE hrassess_competencies_master.isactive = 1 AND hrassess_competencies_master.periodunkid = '" & iPeriodId & "'"

    '        StrQ &= " LEFT JOIN cfcommon_master ON hrassess_competencies_master.scalemasterunkid = cfcommon_master.masterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.ASSESSMENT_SCALE_GROUP & "' " & _
    '                "		WHERE hrassess_competencies_master.isactive = 1 AND hrassess_competencies_master.periodunkid = '" & iPeriodId & "'"
    '        'S.SANDEEP [23 APR 2015] -- END


    '        'S.SANDEEP [09 FEB 2015] -- START
    '        'If iMode = 2 Then
    '        '    StrQ &= "UNION ALL " & _
    '        '            "   SELECT " & _
    '        '            "        CAST(0 AS BIT) AS ischeck " & _
    '        '            "       ,SPACE(5) + hrassess_competencies_master.name AS competence " & _
    '        '            "       ,ISNULL(cfcommon_master.name, '') AS scale " & _
    '        '            "       ,competence_categoryunkid AS GrpId " & _
    '        '            "       ,CAST(0 AS BIT) AS IsGrp " & _
    '        '            "       ,hrassess_competencies_master.competenciesunkid " & _
    '        '            "       ,ISNULL(hrassess_competence_assign_tran.weight, 0) AS weight " & _
    '        '            "       ,ISNULL(assigncompetencetranunkid, 0) AS assigncompetencetranunkid " & _
    '        '            "       ,ISNULL(hrassess_competence_assign_master.assigncompetenceunkid, 0) AS assigncompetenceunkid " & _
    '        '            "   FROM hrassess_competencies_master " & _
    '        '            "       LEFT JOIN hrassess_competence_assign_tran ON hrassess_competencies_master.competenciesunkid = hrassess_competence_assign_tran.competenciesunkid AND hrassess_competence_assign_tran.isvoid = 0 " & _
    '        '            "       LEFT JOIN hrassess_competence_assign_master ON hrassess_competence_assign_tran.assigncompetenceunkid = hrassess_competence_assign_master.assigncompetenceunkid " & _
    '        '            "           AND hrassess_competence_assign_master.isvoid = 0 AND hrassess_competence_assign_master.assessgroupunkid = '" & iAssessGrpId & "' AND hrassess_competence_assign_master.periodunkid = '" & iPeriodId & "' "
    '        '    'S.SANDEEP [29 JAN 2015] -- START
    '        '    If iEmployeeId > 0 Then
    '        '        StrQ &= " AND hrassess_competence_assign_master.employeeunkid = '" & iEmployeeId & "' "
    '        '    End If
    '        '    'S.SANDEEP [29 JAN 2015] -- END
    '        '    StrQ &= "       JOIN cfcommon_master ON hrassess_competencies_master.scalemasterunkid = cfcommon_master.masterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.ASSESSMENT_SCALE_GROUP & "' " & _
    '        '            "   WHERE hrassess_competencies_master.isactive = 1 AND hrassess_competencies_master.periodunkid = '" & iPeriodId & "' " & _
    '        '            "       AND ISNULL(assigncompetencetranunkid,0) = 0 AND ISNULL(hrassess_competence_assign_master.assigncompetenceunkid,0) = 0 "
    '        'End If
    '        'S.SANDEEP [09 FEB 2015] -- END


    '        StrQ &= ") AS A "
    '        If iMode = 2 Then
    '            StrQ &= "LEFT JOIN " & _
    '                    "( " & _
    '                    "   SELECT " & _
    '                    "        competence_categoryunkid AS iGrpId " & _
    '                    "       ,1 AS iIsGrp " & _
    '                    "       ,ISNULL(SUM(hrassess_competence_assign_tran.weight),0) AS iTotal " & _
    '                    "   FROM hrassess_competence_assign_tran " & _
    '                    "       JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
    '                    "       JOIN hrassess_competencies_master ON hrassess_competence_assign_tran.competenciesunkid = hrassess_competencies_master.competenciesunkid " & _
    '                    "   WHERE hrassess_competencies_master.isactive = 1 AND hrassess_competence_assign_tran.isvoid = 0 AND hrassess_competence_assign_master.assessgroupunkid = '" & iAssessGrpId & "' " & _
    '                    "       AND hrassess_competence_assign_master.periodunkid = '" & iPeriodId & "' "
    '            'S.SANDEEP [29 JAN 2015] -- START
    '            If iEmployeeId > 0 Then
    '                StrQ &= " AND hrassess_competence_assign_master.employeeunkid = '" & iEmployeeId & "' "
    '            End If
    '            'S.SANDEEP [29 JAN 2015] -- END
    '            StrQ &= "GROUP BY competence_categoryunkid " & _
    '                    ") AS B ON A.GrpId = B.iGrpId AND A.IsGrp = B.iIsGrp "
    '        End If
    '        'S.SANDEEP [ 17 DEC 2014 ] -- END


    '        StrQ &= " ORDER BY A.GrpId,A.IsGrp DESC,A.competence "

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mdtFinal = dsList.Tables(0).Copy

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "Grouped_Competencies", mstrModuleName)
    '    Finally
    '    End Try
    '    Return mdtFinal
    'End Function
    'S.SANDEEP [21 NOV 2015] -- END
    

    Public Function LoadPerDefinedLibrary(ByVal xFilterTypeId As Integer, ByVal xFilterString As String) As DataTable
        Dim xDataTable As DataTable = Nothing
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT " & _
                   "     CASE WHEN IsGrp = 1 THEN Item + ' (' + CAST(RowNum AS NVARCHAR(MAX)) + ')' ELSE Item END AS Item  " & _
                   "    ,IsGrp" & _
                   "    ,GrpName" & _
                   "    ,[check]" & _
                   "    ,RowNum " & _
                   "FROM " & _
                   "(" & _
                   "    SELECT " & _
                   "         A.Item " & _
                   "        ,A.IsGrp " & _
                   "        ,A.GrpName " & _
                   "        ,CAST(0 AS BIT) AS [check] " & _
                   "        ,SUM(1) OVER(PARTITION BY GrpName) - 1 AS RowNum " & _
                   "    FROM " & _
                   "    ( " & _
                   "        SELECT " & _
                   "            DISTINCT category_name AS Item,CAST(1 AS BIT) IsGrp, category_name AS GrpName " & _
                   "        FROM hrassess_competencies_library " & _
                   "    UNION " & _
                   "        SELECT " & _
                   "             SPACE(5)+competencies AS Item " & _
                   "            ,CAST(0 AS BIT) IsGrp " & _
                   "            ,category_name AS GrpName " & _
                   "        FROM hrassess_competencies_library " & _
                   "    )AS A WHERE 1 = 1 "
            Select Case xFilterTypeId
                Case 0  'BY GROUP
                    StrQ &= "AND A.GrpName LIKE '%" & xFilterString.Replace("'", "''") & "%' " 'DISPLAYS GROUP WITH ALL ITEMS
                Case 1  'BY COMPETENCIES
                    StrQ &= "AND A.Item LIKE '%" & xFilterString.Replace("'", "''") & "%' OR A.IsGrp = 1 " 'INCLUDES THE GROUP WITH ITEMS
            End Select
            StrQ &= ") AS B WHERE 1 = 1 "
            Select Case xFilterTypeId
                Case 1  'BY COMPETENCIES
                    StrQ &= "AND RowNum > 0 "   'DISPLAY ONLY THOSE GROUPS HAVING DATA
            End Select
            StrQ &= "ORDER BY B.GrpName,B.IsGrp DESC "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            xDataTable = dsList.Tables(0).Copy

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "LoadPerDefinedLibrary", mstrModuleName)
        Finally
        End Try
        Return xDataTable
    End Function

    'S.SANDEEP [15 FEB 2016] -- START
    Public Function GetCompetencyTableName(ByVal intPeriodId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As String
        Dim StrTableName As String = "hrassess_competencies_master"
        Dim StrQ As String = ""
        Dim objDo As clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDo = xDataOpr
        Else
            objDo = New clsDataOperation
        End If
        objDo.ClearParameters()
        Try
            StrQ = "SELECT 1 FROM hrassess_competencies_master WITH (NOLOCK) WHERE isactive = 1 AND hrassess_competencies_master.periodunkid = '" & intPeriodId & "' "

            If objDo.RecordCount(StrQ) <= 0 Then
                StrTableName = "hrassess_movedcompetencies_master"
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:GetCompetencyTableName; Module Name: " & mstrModuleName)
        Finally
            If xDataOpr Is Nothing Then objDo = Nothing
        End Try
        Return StrTableName
    End Function
    'S.SANDEEP [15 FEB 2016] -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, This Name is already defined for the selected category. Please define new Name.")
            Language.setMessage(mstrModuleName, 2, "Select")
            Language.setMessage(mstrModuleName, 3, "Sorry, you cannot do delete operation. Reason : Selected competency is already linked with some transaction.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
