﻿'************************************************************************************************************************************
'Class Name : clsassess_competencies_trainingcourses_tran.vb
'Purpose    :
'Date       :28-Mar-2021
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_competencies_trainingcourses_tran
    Private Shared ReadOnly mstrModuleName As String = "clsassess_competencies_trainingcourses_tran"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintTrainingcoursetranunkid As Integer
    Private mintCompetenciesunkid As Integer
    Private mObjDataOpr As clsDataOperation
    Private mdtTran As DataTable

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Competenciesunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Competenciesunkid() As Integer
        Get
            Return mintCompetenciesunkid
        End Get
        Set(ByVal value As Integer)
            mintCompetenciesunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mObjDataOpr
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _mDataOperation() As clsDataOperation
        Get
            Return mObjDataOpr
        End Get
        Set(ByVal value As clsDataOperation)
            mObjDataOpr = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdtTran
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("List") : Dim dCol As DataColumn

            dCol = New DataColumn
            dCol.ColumnName = "trainingcoursetranunkid"
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "competenciesunkid"
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "masterunkid"
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "isvoid"
            dCol.DataType = GetType(System.Boolean)
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voiduserunkid"
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voiddatetime"
            dCol.DataType = GetType(System.DateTime)
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voidreason"
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "AUD"
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "GUID"
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "auditdatetime"
            dCol.DataType = GetType(System.DateTime)
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "audituserunkid"
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "audittype"
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "ip"
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "host"
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "formname"
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "isweb"
            dCol.DataType = GetType(System.Boolean)
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "cname"
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    Private Sub GetData()
        Dim StrQ As String = ""
        Dim exForce As Exception = Nothing
        Dim dsList As New DataSet
        Dim objDataOperation As clsDataOperation
        If mObjDataOpr IsNot Nothing Then
            objDataOperation = mObjDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            StrQ = "SELECT " & _
                   "     hrassess_competencies_trainingcourses_tran.trainingcoursetranunkid " & _
                   "    ,hrassess_competencies_trainingcourses_tran.competenciesunkid " & _
                   "    ,hrassess_competencies_trainingcourses_tran.masterunkid " & _
                   "    ,hrassess_competencies_trainingcourses_tran.isvoid " & _
                   "    ,hrassess_competencies_trainingcourses_tran.voiduserunkid " & _
                   "    ,hrassess_competencies_trainingcourses_tran.voiddatetime " & _
                   "    ,hrassess_competencies_trainingcourses_tran.voidreason " & _
                   "    ,'' AS AUD " & _
                   "    ,'' AS GUID " & _
                   "    ,NULL AS auditdatetime " & _
                   "    ,0 AS audituserunkid " & _
                   "    ,0 AS audittype " & _
                   "    ,'' AS ip " & _
                   "    ,'' AS host " & _
                   "    ,'' AS formname " & _
                   "    ,CAST(0 AS BIT) AS isweb " & _
                   "    ,cfcommon_master.name AS cname " & _
                   "FROM hrassess_competencies_trainingcourses_tran " & _
                   "    JOIN cfcommon_master ON cfcommon_master.masterunkid = hrassess_competencies_trainingcourses_tran.masterunkid " & _
                   "WHERE isvoid = 0 AND competenciesunkid = @competenciesunkid "

            objDataOperation.AddParameter("@competenciesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompetenciesunkid)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtTran IsNot Nothing Then mdtTran.Rows.Clear()
            For Each xRow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(xRow)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            If mObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    Public Function InsertUpdateDelete_Competencies_TrainingCourses() As Boolean
        Dim StrQIn As String = ""
        Dim StrQAt As String = ""
        Dim StrQUd As String = ""
        Dim exForce As Exception = Nothing
        Dim dsList As New DataSet
        Dim objDataOperation As clsDataOperation
        If mObjDataOpr IsNot Nothing Then
            objDataOperation = mObjDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        Try
            StrQIn = "INSERT INTO hrassess_competencies_trainingcourses_tran ( " & _
                     "   competenciesunkid " & _
                     "  ,masterunkid " & _
                     "  ,isvoid " & _
                     "  ,voiduserunkid " & _
                     "  ,voiddatetime " & _
                     "  ,voidreason " & _
                     ") VALUES (" & _
                     "   @competenciesunkid " & _
                     "  ,@masterunkid " & _
                     "  ,@isvoid " & _
                     "  ,@voiduserunkid " & _
                     "  ,@voiddatetime " & _
                     "  ,@voidreason " & _
                     ") ; SELECT @@identity "

            StrQUd = "UPDATE hrassess_competencies_trainingcourses_tran SET " & _
                     "   isvoid = @isvoid " & _
                     "  ,voiduserunkid = @voiduserunkid " & _
                     "  ,voiddatetime = @voiddatetime " & _
                     "  ,voidreason = @voidreason " & _
                     "WHERE trainingcoursetranunkid = @trainingcoursetranunkid "

            StrQAt = "INSERT INTO athrassess_competencies_trainingcourses_tran ( " & _
                     "   tranguid " & _
                     "  ,trainingcoursetranunkid " & _
                     "  ,competenciesunkid " & _
                     "  ,masterunkid " & _
                     "  ,auditdatetime " & _
                     "  ,audituserunkid " & _
                     "  ,audittype " & _
                     "  ,ip " & _
                     "  ,host " & _
                     "  ,formname " & _
                     "  ,isweb " & _
                     ") VALUES (" & _
                     "   @tranguid " & _
                     "  ,@trainingcoursetranunkid " & _
                     "  ,@competenciesunkid " & _
                     "  ,@masterunkid " & _
                     "  ,@auditdatetime " & _
                     "  ,@audituserunkid " & _
                     "  ,@audittype " & _
                     "  ,@ip " & _
                     "  ,@host " & _
                     "  ,@formname " & _
                     "  ,@isweb " & _
                     ") "

            For Each iRow As DataRow In mdtTran.Rows
                objDataOperation.ClearParameters()

                With iRow
                    objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Guid.NewGuid.ToString())
                    objDataOperation.AddParameter("@competenciesunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(mintCompetenciesunkid <= 0, .Item("competenciesunkid"), mintCompetenciesunkid))
                    objDataOperation.AddParameter("@masterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("masterunkid"))
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))
                    objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("auditdatetime"))
                    objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("audituserunkid"))
                    objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("audittype"))
                    objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("ip"))
                    objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("host"))
                    objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("formname"))
                    objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isweb"))

                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"

                                dsList = objDataOperation.ExecQuery(StrQIn, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintTrainingcoursetranunkid = dsList.Tables(0).Rows(0).Item(0)

                                objDataOperation.AddParameter("@trainingcoursetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(mintTrainingcoursetranunkid <= 0, .Item("trainingcoursetranunkid"), mintTrainingcoursetranunkid))


                                objDataOperation.ExecQuery(StrQAt, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"

                                If .Item("trainingcoursetranunkid") > 0 Then
                                    objDataOperation.AddParameter("@trainingcoursetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainingcoursetranunkid"))

                                    objDataOperation.ExecQuery(StrQUd, "List")

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                    objDataOperation.ExecQuery(StrQAt, "List")

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If


                        End Select
                    End If
                End With
            Next

            If mObjDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If mObjDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_Competencies_TrainingCourses; Module Name: " & mstrModuleName)
        Finally
            If mObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

End Class
