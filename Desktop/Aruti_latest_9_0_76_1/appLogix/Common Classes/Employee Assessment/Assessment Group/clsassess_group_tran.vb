﻿'************************************************************************************************************************************
'Class Name : clsassess_group_tran.vb
'Purpose    :
'Date       :05/01/2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsassess_group_tran

    Private Shared ReadOnly mstrModuleName As String = "clsassess_group_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

    Public Property _Message() As String
        Get
            Return mstrMessage
        End Get
        Set(ByVal value As String)
            mstrMessage = value
        End Set
    End Property


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_group_tran) </purpose>
    Public Function Insert(ByVal objDataOperation As clsDataOperation, ByVal intAssessGrpUnkid As Integer, ByVal strAllocationIds As String, ByVal intReferenceUnkid As Integer, ByVal dtEffDate As Date) As Boolean 'S.SANDEEP [20 Jan 2016] -- START {dtEffDate} -- END
        'Public Function Insert(ByVal objDataOperation As clsDataOperation, ByVal intAssessGrpUnkid As Integer, ByVal strAllocationIds As String, ByVal intReferenceUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mintAssessgroupTranUnkId As Integer = -1
        Dim dsAllocated As New DataSet
        Dim blnFlagIsInserted As Boolean = False
        'S.SANDEEP [ 19 JULY 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Dim blnIsUpdate As Boolean = False
        'S.SANDEEP [ 19 JULY 2012 ] -- END
        Try
            dsAllocated = GetAllocations(objDataOperation, intAssessGrpUnkid)

            If dsAllocated.Tables(0).Rows.Count > 0 Then
                'S.SANDEEP [ 19 JULY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                blnIsUpdate = True
                'S.SANDEEP [ 19 JULY 2012 ] -- END
                Dim dtTemp() As DataRow = dsAllocated.Tables(0).Select("allocationunkid NOT IN(" & strAllocationIds & ")")
                If dtTemp.Length > 0 Then
                    For i As Integer = 0 To dtTemp.Length - 1
                        'S.SANDEEP [20 Jan 2016] -- START
                        If isAllocationUsed(objDataOperation, dtTemp(i)("allocationunkid"), intReferenceUnkid, dtEffDate) = False Then
                            'If isAllocationUsed(objDataOperation, dtTemp(i)("allocationunkid"), intReferenceUnkid) = False Then
                            'S.SANDEEP [20 Jan 2016] -- END
                            Call Delete(objDataOperation, dtTemp(i)("assessgrouptranunkid"))
                            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_group_master", "assessgroupunkid", intAssessGrpUnkid, "hrassess_group_tran", "assessgrouptranunkid", dtTemp(i)("assessgrouptranunkid"), 2, 3) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        Else
                            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, You cannot unassign some allocation(s). Reason : They are already linked with some transaction(s).")
                        End If
                    Next
                End If
            End If

            For Each StrId As String In strAllocationIds.Split(",")

                If isExist(objDataOperation, intAssessGrpUnkid, StrId) = True Then Continue For


                objDataOperation.ClearParameters() : mintAssessgroupTranUnkId = -1

                objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessGrpUnkid.ToString)
                objDataOperation.AddParameter("@allocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, StrId)
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)

                strQ = "INSERT INTO hrassess_group_tran ( " & _
                        "  assessgroupunkid " & _
                        ", allocationunkid " & _
                        ", isactive" & _
                        ") VALUES (" & _
                        "  @assessgroupunkid " & _
                        ", @allocationunkid " & _
                        ", @isactive" & _
                        "); SELECT @@identity"

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintAssessgroupTranUnkId = dsList.Tables(0).Rows(0).Item(0)

                If blnFlagIsInserted = False Then blnFlagIsInserted = True


                'S.SANDEEP [ 19 JULY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_group_master", "assessgroupunkid", intAssessGrpUnkid, "hrassess_group_tran", "assessgrouptranunkid", mintAssessgroupTranUnkId, 2, 1) = False Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If
                If blnIsUpdate = False Then
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_group_master", "assessgroupunkid", intAssessGrpUnkid, "hrassess_group_tran", "assessgrouptranunkid", mintAssessgroupTranUnkId, 1, 1) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Else
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_group_master", "assessgroupunkid", intAssessGrpUnkid, "hrassess_group_tran", "assessgrouptranunkid", mintAssessgroupTranUnkId, 2, 1) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                End If
                'S.SANDEEP [ 19 JULY 2012 ] -- END

                

            Next

            If blnFlagIsInserted = False Then
                If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrassess_group_master", intAssessGrpUnkid, "assessgroupunkid", 2) Then
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_group_master", "assessgroupunkid", intAssessGrpUnkid, "hrassess_group_tran", "assessgrouptranunkid", -1, 2, 0) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_group_tran) </purpose>
    Public Function Delete(ByVal objDataOperation As clsDataOperation, ByVal intAssessGrpTranId As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            strQ = "UPDATE hrassess_group_tran " & _
                   " SET isactive = 0 " & _
                   "WHERE assessgrouptranunkid = @assessgrouptranunkid "

            objDataOperation.AddParameter("@assessgrouptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessGrpTranId)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose></purpose>
    Private Function isExist(ByVal objDataOperation As clsDataOperation, ByVal intAssessGrpId As Integer, ByVal intAllocationId As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        Try
            strQ = " SELECT * FROM hrassess_group_tran WHERE assessgroupunkid = '" & intAssessGrpId & "' AND allocationunkid = '" & intAllocationId & "' AND isactive = 1 "


            If objDataOperation.RecordCount(strQ) <= 0 Then
                blnFlag = False
            Else
                blnFlag = True
            End If


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose></purpose>
    Private Function GetAllocations(ByVal objDataOperation As clsDataOperation, ByVal intAssessGrpId As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ &= "SELECT " & _
                    " assessgrouptranunkid " & _
                    ",assessgroupunkid " & _
                    ",allocationunkid " & _
                    ",isactive " & _
                    "FROM hrassess_group_tran " & _
                    "WHERE assessgroupunkid = '" & intAssessGrpId & "' AND isactive = 1 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAllocations; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose></purpose>
    Private Function isAllocationUsed(ByVal objDataOperation As clsDataOperation, _
                                      ByVal intAllocationId As Integer, _
                                      ByVal intReferenceId As Integer, _
                                      ByVal dtEffDate As Date) As Boolean 'S.SANDEEP [20 Jan 2016] -- START {dtEffDate} -- END
        'Private Function isAllocationUsed(ByVal objDataOperation As clsDataOperation, ByVal intAllocationId As Integer, ByVal intReferenceId As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        Try
            'S.SANDEEP [20 Jan 2016] -- START
            'strQ = "SELECT " & _
            '       "	analysisunkid " & _
            '       "FROM hrassess_analysis_master " & _
            '       "	JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
            '       "	JOIN hrassess_group_tran ON hrassess_group_master.assessgroupunkid = hrassess_group_tran.assessgroupunkid " & _
            '       "	LEFT JOIN hremployee_master AS Assess ON hrassess_analysis_master.assessedemployeeunkid = Assess.employeeunkid AND hrassess_analysis_master.assessmodeid ='" & enAssessmentMode.APPRAISER_ASSESSMENT & "' " & _
            '       "	LEFT JOIN hremployee_master AS Self ON hrassess_analysis_master.selfemployeeunkid = Self.employeeunkid AND hrassess_analysis_master.assessmodeid ='" & enAssessmentMode.SELF_ASSESSMENT & "' " & _
            '       "WHERE hrassess_group_tran.allocationunkid = '" & intAllocationId & "' " & _
            '       "AND hrassess_analysis_master.isvoid = 0 "
            'Select Case intReferenceId
            '    Case enAllocation.BRANCH
            '        strQ &= " AND(Self.stationunkid = '" & intAllocationId & "' OR Assess.stationunkid = '" & intAllocationId & "') "
            '    Case enAllocation.DEPARTMENT_GROUP
            '        strQ &= " AND(Self.deptgroupunkid = '" & intAllocationId & "' OR Assess.deptgroupunkid = '" & intAllocationId & "') "
            '    Case enAllocation.DEPARTMENT
            '        strQ &= " AND(Self.departmentunkid = '" & intAllocationId & "' OR Assess.departmentunkid = '" & intAllocationId & "') "
            '    Case enAllocation.SECTION_GROUP
            '        strQ &= " AND(Self.sectiongroupunkid = '" & intAllocationId & "' OR Assess.sectiongroupunkid = '" & intAllocationId & "') "
            '    Case enAllocation.SECTION
            '        strQ &= " AND(Self.sectionunkid = '" & intAllocationId & "' OR Assess.sectionunkid = '" & intAllocationId & "') "
            '    Case enAllocation.UNIT_GROUP
            '        strQ &= " AND(Self.unitgroupunkid = '" & intAllocationId & "' OR Assess.unitgroupunkid = '" & intAllocationId & "') "
            '    Case enAllocation.UNIT
            '        strQ &= " AND(Self.unitunkid = '" & intAllocationId & "' OR Assess.unitunkid = '" & intAllocationId & "') "
            '    Case enAllocation.TEAM
            '        strQ &= " AND(Self.teamunkid = '" & intAllocationId & "' OR Assess.teamunkid = '" & intAllocationId & "') "
            '    Case enAllocation.JOB_GROUP
            '        strQ &= " AND(Self.jobgroupunkid = '" & intAllocationId & "' OR Assess.jobgroupunkid = '" & intAllocationId & "') "
            '    Case enAllocation.JOBS
            '        strQ &= " AND(Self.jobunkid = '" & intAllocationId & "' OR Assess.jobunkid = '" & intAllocationId & "') "
            'End Select

            strQ = "SELECT DISTINCT " & _
                         "hrevaluation_analysis_master.analysisunkid " & _
                    "FROM hrevaluation_analysis_master " & _
                    "JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "JOIN hrassess_group_master ON hrcompetency_analysis_tran.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
                   "	JOIN hrassess_group_tran ON hrassess_group_master.assessgroupunkid = hrassess_group_tran.assessgroupunkid " & _
                    "LEFT JOIN hremployee_master AS Assess ON hrevaluation_analysis_master.assessedemployeeunkid = Assess.employeeunkid AND hrevaluation_analysis_master.assessmodeid = 2 " & _
                    "LEFT JOIN " & _
                    "( " & _
                         "SELECT " & _
                               "stationunkid " & _
                              ",deptgroupunkid " & _
                              ",departmentunkid " & _
                              ",sectiongroupunkid " & _
                              ",sectionunkid " & _
                              ",unitgroupunkid " & _
                              ",unitunkid " & _
                              ",teamunkid " & _
                              ",classgroupunkid " & _
                              ",classunkid " & _
                              ",employeeunkid " & _
                              ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "FROM hremployee_transfer_tran " & _
                         "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEffDate) & "' " & _
                    ") AS A_Alloc ON A_Alloc.employeeunkid = Assess.employeeunkid AND A_Alloc.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                         "SELECT " & _
                               "jobunkid " & _
                              ",jobgroupunkid " & _
                              ",employeeunkid " & _
                              ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "FROM hremployee_categorization_tran " & _
                         "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEffDate) & "' " & _
                    ") AS A_Jobs ON A_Jobs.employeeunkid = Assess.employeeunkid AND A_Jobs.rno = 1 " & _
                    "LEFT JOIN hremployee_master AS Self ON hrevaluation_analysis_master.selfemployeeunkid = Self.employeeunkid AND hrevaluation_analysis_master.assessmodeid = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                         "SELECT " & _
                               "stationunkid " & _
                              ",deptgroupunkid " & _
                              ",departmentunkid " & _
                              ",sectiongroupunkid " & _
                              ",sectionunkid " & _
                              ",unitgroupunkid " & _
                              ",unitunkid " & _
                              ",teamunkid " & _
                              ",classgroupunkid " & _
                              ",classunkid " & _
                              ",employeeunkid " & _
                              ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "FROM hremployee_transfer_tran " & _
                         "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEffDate) & "' " & _
                    ") AS S_Alloc ON S_Alloc.employeeunkid = Self.employeeunkid AND S_Alloc.rno = 1 " & _
                    "LEFT JOIN " & _
                    "( " & _
                         "SELECT " & _
                               "jobunkid " & _
                              ",jobgroupunkid " & _
                              ",employeeunkid " & _
                              ",ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                         "FROM hremployee_categorization_tran " & _
                         "WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEffDate) & "' " & _
                    ") AS S_Jobs ON S_Jobs.employeeunkid = Self.employeeunkid AND S_Jobs.rno = 1 " & _
                   "WHERE hrassess_group_tran.allocationunkid = '" & intAllocationId & "' " & _
                    "AND hrevaluation_analysis_master.isvoid = 0 "

            Select intReferenceId
                Case enAllocation.BRANCH
                    strQ &= " AND(S_Alloc.stationunkid = '" & intAllocationId & "' OR A_Alloc.stationunkid = '" & intAllocationId & "') "
                Case enAllocation.DEPARTMENT_GROUP
                    strQ &= " AND(S_Alloc.deptgroupunkid = '" & intAllocationId & "' OR A_Alloc.deptgroupunkid = '" & intAllocationId & "') "
                Case enAllocation.DEPARTMENT
                    strQ &= " AND(S_Alloc.departmentunkid = '" & intAllocationId & "' OR A_Alloc.departmentunkid = '" & intAllocationId & "') "
                Case enAllocation.SECTION_GROUP
                    strQ &= " AND(S_Alloc.sectiongroupunkid = '" & intAllocationId & "' OR A_Alloc.sectiongroupunkid = '" & intAllocationId & "') "
                Case enAllocation.SECTION
                    strQ &= " AND(S_Alloc.sectionunkid = '" & intAllocationId & "' OR A_Alloc.sectionunkid = '" & intAllocationId & "') "
                Case enAllocation.UNIT_GROUP
                    strQ &= " AND(S_Alloc.unitgroupunkid = '" & intAllocationId & "' OR A_Alloc.unitgroupunkid = '" & intAllocationId & "') "
                Case enAllocation.UNIT
                    strQ &= " AND(S_Alloc.unitunkid = '" & intAllocationId & "' OR A_Alloc.unitunkid = '" & intAllocationId & "') "
                Case enAllocation.TEAM
                    strQ &= " AND(S_Alloc.teamunkid = '" & intAllocationId & "' OR A_Alloc.teamunkid = '" & intAllocationId & "') "
                Case enAllocation.JOB_GROUP
                    strQ &= " AND(S_Jobs.jobgroupunkid = '" & intAllocationId & "' OR A_Jobs.jobgroupunkid = '" & intAllocationId & "') "
                Case enAllocation.JOBS
                    strQ &= " AND(S_Jobs.jobunkid = '" & intAllocationId & "' OR A_Jobs.jobunkid = '" & intAllocationId & "') "
            End Select

            'S.SANDEEP [20 Jan 2016] -- END



            If objDataOperation.RecordCount(strQ) <= 0 Then
                blnFlag = False
            Else
                blnFlag = True
            End If


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isAllocationUsed; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, You cannot unassign some allocation(s). Reason : They are already linked with some transaction(s).")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
