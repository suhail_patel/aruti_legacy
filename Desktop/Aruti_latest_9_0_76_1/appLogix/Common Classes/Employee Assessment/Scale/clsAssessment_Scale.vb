﻿'************************************************************************************************************************************
'Class Name : clsScale.vb
'Purpose    :
'Date       :28/03/2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsAssessment_Scale
    Private Shared ReadOnly mstrModuleName As String = "clsAssessment_Scale"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintScaletranunkid As Integer = 0
    Private mintScalemasterunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mDecScale As Decimal = 0
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    'S.SANDEEP [11-OCT-2018] -- START
    Private mdecPctFrom As Decimal = 0
    Private mdecPctTo As Decimal = 0
    'S.SANDEEP [11-OCT-2018] -- END

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set scaletranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Scaletranunkid() As Integer
        Get
            Return mintScaletranunkid
        End Get
        Set(ByVal value As Integer)
            mintScaletranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set scalemasterunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Scalemasterunkid() As Integer
        Get
            Return mintScalemasterunkid
        End Get
        Set(ByVal value As Integer)
            mintScalemasterunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set scale
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Scale() As Decimal
        Get
            Return mDecScale
        End Get
        Set(ByVal value As Decimal)
            mDecScale = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    'S.SANDEEP [11-OCT-2018] -- START
    Public Property _PercentFrom() As Decimal
        Get
            Return mdecPctFrom
        End Get
        Set(ByVal value As Decimal)
            mdecPctFrom = value
        End Set
    End Property
    Public Property _PercentTo() As Decimal
        Get
            Return mdecPctTo
        End Get
        Set(ByVal value As Decimal)
            mdecPctTo = value
        End Set
    End Property
    'S.SANDEEP [11-OCT-2018] -- END


#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  scaletranunkid " & _
                   ", scalemasterunkid " & _
                   ", periodunkid " & _
                   ", scale " & _
                   ", description " & _
                   ", isactive " & _
                   ", ISNULL(pct_from,0) AS pct_from " & _
                   ", ISNULL(pct_to,0) AS pct_to " & _
                   "FROM hrassess_scale_master " & _
                   "WHERE scaletranunkid = @scaletranunkid "
            'S.SANDEEP [11-OCT-2018] -- START {pct_from,pct_to} -- END

            objDataOperation.AddParameter("@scaletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScaletranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintScaletranunkid = CInt(dtRow.Item("scaletranunkid"))
                mintScalemasterunkid = CInt(dtRow.Item("scalemasterunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mDecScale = dtRow.Item("scale")
                mstrDescription = dtRow.Item("description").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                'S.SANDEEP [11-OCT-2018] -- START
                mdecPctFrom = CInt(dtRow.Item("pct_from"))
                mdecPctTo = CInt(dtRow.Item("pct_to"))
                'S.SANDEEP [11-OCT-2018] -- END
                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  hrassess_scale_master.scaletranunkid " & _
                   ", hrassess_scale_master.scalemasterunkid " & _
                   ", hrassess_scale_master.scale " & _
                   ", hrassess_scale_master.description " & _
                   ", hrassess_scale_master.isactive " & _
                   ", cfcommon_master.name AS name " & _
                   ", hrassess_scale_master.periodunkid " & _
                   ", cfcommon_period_tran.period_name AS pname " & _
                   ", cfcommon_period_tran.statusid " & _
                   ", ISNULL(hrassess_scale_master.pct_from,0) AS pct_from " & _
                   ", ISNULL(hrassess_scale_master.pct_to,0) AS pct_to " & _
                   "FROM hrassess_scale_master " & _
                   " JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = hrassess_scale_master.periodunkid AND modulerefid = '" & enModuleReference.Assessment & "' " & _
                   " JOIN cfcommon_master ON hrassess_scale_master.scalemasterunkid = cfcommon_master.masterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.ASSESSMENT_SCALE_GROUP & "' " & _
                   "WHERE 1=1 "
            'S.SANDEEP [11-OCT-2018] -- START {pct_from,pct_to} -- END

            'Shani(18-Feb-2016) -- [cfcommon_period_tran.statusid, WHERE 1=1 ]

            'Shani(18-Feb-2016) -- Start
            '
            'If blnOnlyActive Then
            '    strQ &= " WHERE hrassess_scale_master.isactive = 1 "
            'End If
            If blnOnlyActive Then
                strQ &= " AND hrassess_scale_master.isactive = 1 "
            End If

            strQ &= " AND cfcommon_period_tran.statusid = 1"

            'Shani(18-Feb-2016) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal iPeriodId As Integer, ByVal iScaleMasterId As Integer, Optional ByVal blnAddNA As Boolean = False) As DataSet 'S.SANDEEP |18-JAN-2020| -- START {blnAddNA} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            'S.SANDEEP |18-JAN-2020| -- START
            'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
            If blnAddNA Then
            strQ = "SELECT " & _
                    "  0 AS scaletranunkid " & _
                    ", 0 AS scalemasterunkid " & _
                    ", @NA AS scale " & _
                    ", '' AS description " & _
                    ", 0 AS isactive " & _
                    ", '' AS name " & _
                    ", 0 AS periodunkid " & _
                    ", '' AS pname " & _
                    ", 0 AS pct_from " & _
                    ", 0 AS pct_to " & _
                    "UNION ALL "
                objDataOperation.AddParameter("@NA", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "-NA-"))
            End If
            'S.SANDEEP |18-JAN-2020| -- END

            strQ &= "SELECT " & _
                   "  hrassess_scale_master.scaletranunkid " & _
                   ", hrassess_scale_master.scalemasterunkid " & _
                    ", CAST(hrassess_scale_master.scale AS NVARCHAR(MAX)) AS scale " & _
                   ", hrassess_scale_master.description " & _
                   ", hrassess_scale_master.isactive " & _
                   ", cfcommon_master.name AS name " & _
                   ", hrassess_scale_master.periodunkid " & _
                   ", cfcommon_period_tran.period_name AS pname " & _
                   ", ISNULL(hrassess_scale_master.pct_from,0) AS pct_from " & _
                   ", ISNULL(hrassess_scale_master.pct_to,0) AS pct_to " & _
                   "FROM hrassess_scale_master " & _
                   " JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = hrassess_scale_master.periodunkid AND modulerefid = '" & enModuleReference.Assessment & "' " & _
                   " JOIN cfcommon_master ON hrassess_scale_master.scalemasterunkid = cfcommon_master.masterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.ASSESSMENT_SCALE_GROUP & "' " & _
                   " WHERE hrassess_scale_master.isactive = 1 AND hrassess_scale_master.scalemasterunkid = '" & iScaleMasterId & "' " & _
                   " AND hrassess_scale_master.periodunkid = '" & iPeriodId & "' "
            'S.SANDEEP [11-OCT-2018] -- START {pct_from,pct_to} -- END

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_scale_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mintPeriodunkid, mDecScale, "", mintScalemasterunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Scale is already defined. Please define new scale.")
            Return False
        End If

        If isExist(mintPeriodunkid, -1, mstrDescription, mintScalemasterunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, name is already defined. Please define new name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@scalemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScalemasterunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@scale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecScale.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Length, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            'S.SANDEEP [11-OCT-2018] -- START
            objDataOperation.AddParameter("@pct_from", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPctFrom.ToString)
            objDataOperation.AddParameter("@pct_to", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPctTo.ToString)
            'S.SANDEEP [11-OCT-2018] -- END

            strQ = "INSERT INTO hrassess_scale_master ( " & _
                       "  scalemasterunkid " & _
                   "    , periodunkid" & _
                   "    , scale " & _
                   "    , description " & _
                   "    , isactive " & _
                   "    , pct_from " & _
                   "    , pct_to " & _
                   ") VALUES (" & _
                       "  @scalemasterunkid " & _
                   "    , @periodunkid " & _
                   "    , @scale " & _
                   "    , @description " & _
                   "    , @isactive " & _
                   "    , @pct_from " & _
                   "    , @pct_to " & _
                   "); SELECT @@identity"
            'S.SANDEEP [11-OCT-2018] -- START {pct_from,pct_to} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintScaletranunkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_scale_master", "scaletranunkid", mintScaletranunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_scale_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mintPeriodunkid, mDecScale, "", mintScalemasterunkid, mintScaletranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Scale is already defined. Please define new scale.")
            Return False
        End If

        If isExist(mintPeriodunkid, -1, mstrDescription, mintScalemasterunkid, mintScaletranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, name is already defined. Please define new name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@scaletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScaletranunkid.ToString)
            objDataOperation.AddParameter("@scalemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScalemasterunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@scale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecScale.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Length, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            'S.SANDEEP [11-OCT-2018] -- START
            objDataOperation.AddParameter("@pct_from", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPctFrom.ToString)
            objDataOperation.AddParameter("@pct_to", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPctTo.ToString)
            'S.SANDEEP [11-OCT-2018] -- END

            strQ = "UPDATE hrassess_scale_master SET " & _
                    "  scalemasterunkid = @scalemasterunkid" & _
                    ", periodunkid = @periodunkid " & _
                    ", scale = @scale" & _
                    ", description = @description" & _
                    ", isactive = @isactive " & _
                    ", pct_from = @pct_from " & _
                    ", pct_to = @pct_to " & _
                   "WHERE scaletranunkid = @scaletranunkid "
            'S.SANDEEP [11-OCT-2018] -- START {pct_from,pct_to} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_scale_master", mintScaletranunkid, "scaletranunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_scale_master", "scaletranunkid", mintScaletranunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_scale_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            StrQ = "UPDATE hrassess_scale_master SET isactive = 0 " & _
                   "WHERE scaletranunkid = @scaletranunkid "

            objDataOperation.AddParameter("@scaletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_scale_master", "scaletranunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False
        Dim dsUsed As New DataSet
        objDataOperation = New clsDataOperation

        Try
            mstrMessage = ""

            StrQ = "SELECT TABLE_NAME AS TableName FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'scaletranunkid' AND TABLE_NAME NOT IN('hrassess_scale_master') "

            objDataOperation.AddParameter("@scaletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("List").Rows
                StrQ = "SELECT scaletranunkid FROM " & dtRow.Item("TableName").ToString & " WHERE scaletranunkid = @scaletranunkid  "

                dsUsed = objDataOperation.ExecQuery(StrQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsUsed.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next
            Return blnIsUsed
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iPeriodId As Integer, Optional ByVal iScale As Decimal = -1, Optional ByVal iname As String = "", Optional ByVal iGroupId As Integer = 0, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
                    "  scaletranunkid " & _
                    ", scalemasterunkid " & _
                    ", scale " & _
                    ", description " & _
                    ", isactive " & _
                    ", periodunkid " & _
                    ", ISNULL(pct_from,0) AS pct_from " & _
                    ", ISNULL(pct_to,0) AS pct_to " & _
                    "FROM hrassess_scale_master " & _
                    "WHERE isactive = 1 "
            'S.SANDEEP [11-OCT-2018] -- START {pct_from,pct_to} -- END

            If iPeriodId > 0 Then
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)
                StrQ &= " AND periodunkid = @periodunkid "
            End If

            If iGroupId > 0 Then
                objDataOperation.AddParameter("@scalemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iGroupId)
                StrQ &= " AND scalemasterunkid = @scalemasterunkid "
            End If

            If IsNumeric(iScale) Then
                objDataOperation.AddParameter("@scale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iScale)
                StrQ &= " AND scale = @scale "
            End If

            If iname.Trim.Length > 0 Then
                objDataOperation.AddParameter("@description", SqlDbType.NVarChar, iname.Length, iname)
                StrQ &= " AND description = @description "
            End If

            If intUnkid > 0 Then
                StrQ &= " AND scaletranunkid <> @scaletranunkid"
            End If


            objDataOperation.AddParameter("@scaletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetScaleDetails(ByVal iScale As Decimal, ByVal iPeriodId As Integer, ByRef iScaleUnkid As Integer, ByRef iname As String)
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT scaletranunkid,scale,description,ISNULL(pct_from,0) AS pct_from,ISNULL(pct_to,0) AS pct_to " & _
                   "FROM hrassess_scale_master WHERE isactive = 1 AND scale = @scale AND periodunkid = @periodunkid "
            'S.SANDEEP [11-OCT-2018] -- START {pct_from,pct_to} -- END

            objDataOperation.AddParameter("@scale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iScale)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                iScaleUnkid = dsList.Tables("List").Rows(0).Item("scaletranunkid")
                iname = dsList.Tables("List").Rows(0).Item("description")
            Else
                iScaleUnkid = 0 : iname = ""
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetScaleDetails; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' Shani (26-Sep-2016) -- Start
    ''' Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    ''' Public Function getComboList(ByVal iList As String, Optional ByVal iAddSelect As Boolean = False, Optional ByVal iAddname As Boolean = False) As DataSet
    Public Function getComboList(ByVal iList As String, ByVal iPeriodUnkid As Integer, Optional ByVal iAddSelect As Boolean = False, Optional ByVal iAddname As Boolean = False) As DataSet
        'Shani (26-Sep-2016) -- End
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            If iAddSelect = True Then
                StrQ = "SELECT 0 AS Id,@Select AS Name UNION "
            End If

            StrQ &= "SELECT scaletranunkid AS Id "

            If iAddname = True Then
                StrQ &= ", CAST(scale AS NVARCHAR(MAX))+' - '+ description AS Name "
            Else
                StrQ &= ", CAST(scale AS NVARCHAR(MAX)) AS Name "
            End If

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'StrQ &= "FROM hrassess_scale_master WHERE isactive = 1 "
            StrQ &= "FROM hrassess_scale_master WHERE isactive = 1 AND periodunkid = '" & iPeriodUnkid & "'"
            'Shani (26-Sep-2016) -- End


            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, iList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    'Public Function getComboList_ScaleGroup(ByVal iPeriod As Integer, ByVal iList As String, Optional ByVal iAddSelect As Boolean = False) As DataSet
    Public Function getComboList_ScaleGroup(ByVal iPeriod As Integer, ByVal iList As String, Optional ByVal iAddSelect As Boolean = False, Optional ByVal iAddName As Boolean = False) As DataSet
        'Shani (26-Sep-2016) -- End
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            If iAddSelect = True Then
                StrQ = "SELECT 0 AS Id,@Select AS Name UNION "
            End If

            StrQ &= "SELECT DISTINCT " & _
                    "    hrassess_scale_master.scalemasterunkid AS Id " & _
                    "   ,cfcommon_master.name AS Name " & _
                    "FROM hrassess_scale_master WITH (NOLOCK) " & _
                    "   JOIN cfcommon_master WITH (NOLOCK) ON cfcommon_master.masterunkid = hrassess_scale_master.scalemasterunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.ASSESSMENT_SCALE_GROUP & "' " & _
                    "WHERE cfcommon_master.isactive = 1 and hrassess_scale_master.isactive = 1 AND periodunkid = '" & iPeriod & "' "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, iList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    Public Function getScale(ByVal iScaleUnkId As Integer) As Decimal
        Dim iScale As Decimal = 0
        Dim StrQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            StrQ &= "SELECT " & _
                    "    ISNULL(hrassess_scale_master.scale,0) AS scale " & _
                    "   ,ISNULL(hrassess_scale_master.pct_from,0) AS pct_from " & _
                    "   ,ISNULL(hrassess_scale_master.pct_to,0) AS pct_to " & _
                    "FROM hrassess_scale_master " & _
                    "WHERE hrassess_scale_master.isactive = 1 AND scaletranunkid = '" & iScaleUnkId & "' "
            'S.SANDEEP [11-OCT-2018] -- START {pct_from,pct_to} -- END

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                iScale = CDec(dsList.Tables(0).Rows(0)("scale"))
            End If

            Return iScale
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function
    'Shani (26-Sep-2016) -- End

    'S.SANDEEP [11-OCT-2018] -- START
    Public Function getPecentageDefined(ByVal intPeriodId As Integer, ByVal intScaleMasterId As String, ByVal objDo As clsDataOperation, ByRef iList As List(Of Integer)) As DataSet
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT " & _
                   "   ISNULL(hrassess_scale_master.scale,0) AS scale " & _
                   "  ,ISNULL(hrassess_scale_master.pct_from,0) AS pct_from " & _
                   "  ,ISNULL(hrassess_scale_master.pct_to,0) AS pct_to " & _
                   "  ,hrassess_scale_master.scalemasterunkid " & _
                   "FROM hrassess_scale_master WITH (NOLOCK) " & _
                   "WHERE hrassess_scale_master.isactive = 1 " & _
                   "  AND hrassess_scale_master.periodunkid = '" & intPeriodId & "' " & _
                   "  AND hrassess_scale_master.scalemasterunkid IN (" & intScaleMasterId & ") "

            dsList = objDo.ExecQuery(StrQ, "List")

            If objDo.ErrorMessage <> "" Then
                exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                iList = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Decimal)("pct_to") > 0).Select(Function(x) x.Field(Of Integer)("scalemasterunkid")).Distinct().ToList()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getPecentageDefined; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function
    Public Function getLastPecentageDefined(ByVal intPeriodId As Integer, ByVal intScaleMasterId As Integer) As String
        Dim iScale As String = ""
        Dim StrQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT " & _
                   "      ISNULL(hrassess_scale_master.scale,0) AS scale " & _
                   "     ,ISNULL(hrassess_scale_master.pct_from,0) AS pct_from " & _
                   "     ,ISNULL(hrassess_scale_master.pct_to,0) AS pct_to " & _
                   "FROM hrassess_scale_master WITH (NOLOCK) " & _
                   "WHERE hrassess_scale_master.isactive = 1 " & _
                   "     AND hrassess_scale_master.periodunkid = @periodunkid " & _
                   "     AND hrassess_scale_master.scalemasterunkid = @scalemasterunkid " & _
                   "ORDER BY pct_to DESC "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            objDataOperation.AddParameter("@scalemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intScaleMasterId)

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                iScale = dsList.Tables(0).Rows(0).Item("pct_from").ToString() & " - " & dsList.Tables(0).Rows(0).Item("pct_to").ToString()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return iScale
    End Function

    Public Function getScale(ByVal mdecPctCompleted As Decimal, ByVal intPeriodId As Integer, ByVal intScaleMasterId As Integer) As Decimal
        Dim iScale As Decimal = 0
        Dim StrQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT " & _
                    "  ISNULL(hrassess_scale_master.scale,0) AS scale " & _
                    " ,ISNULL(hrassess_scale_master.pct_from,0) AS pct_from " & _
                    " ,ISNULL(hrassess_scale_master.pct_to,0) AS pct_to " & _
                    "FROM hrassess_scale_master WITH (NOLOCK) " & _
                    "WHERE hrassess_scale_master.isactive = 1 " & _
                    " AND hrassess_scale_master.periodunkid @periodunkid " & _
                    " AND hrassess_scale_master.scalemasterunkid @scalemasterunkid " & _
                    " AND ISNULL(hrassess_scale_master.pct_from,0) <= @pct " & _
                    " AND ISNULL(hrassess_scale_master.pct_to,0) >= @pct "

            objDataOperation.AddParameter("@pct", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPctCompleted)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            objDataOperation.AddParameter("@scalemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intScaleMasterId)

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                iScale = CDec(dsList.Tables(0).Rows(0)("scale"))
            End If

            Return iScale
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getScale; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    Public Function IsValidPercentageValue(ByVal decPercent As Decimal, ByVal intPeriodId As Integer, ByVal intScaleMasterId As Integer) As String
        Dim iScale As Decimal = 0
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim strMsg As String = ""
        objDataOperation = New clsDataOperation
        Try

            StrQ = "SELECT " & _
                   "      ISNULL(hrassess_scale_master.scale,0) AS scale " & _
                   "     ,ISNULL(hrassess_scale_master.pct_from,0) AS pct_from " & _
                   "     ,ISNULL(hrassess_scale_master.pct_to,0) AS pct_to " & _
                   "FROM hrassess_scale_master WITH (NOLOCK) " & _
                   "WHERE hrassess_scale_master.isactive = 1 " & _
                   "     AND hrassess_scale_master.periodunkid = @periodunkid " & _
                   "     AND hrassess_scale_master.scalemasterunkid = @scalemasterunkid " & _
                   "ORDER BY pct_to DESC "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            objDataOperation.AddParameter("@scalemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intScaleMasterId)

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                If decPercent < CDec(dsList.Tables(0).Rows(0).Item("pct_to")) Then
                    strMsg = Language.getMessage(mstrModuleName, 4, "Sorry, Percentage should be greater than Previous Percentage.")
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsValidPercentageValue; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strMsg
    End Function
    'S.SANDEEP [11-OCT-2018] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, Scale is already defined. Please define new scale.")
            Language.setMessage(mstrModuleName, 2, "Sorry, name is already defined. Please define new name.")
			Language.setMessage(mstrModuleName, 3, "Select")
            Language.setMessage(mstrModuleName, 4, "Sorry, Percentage should be greater than Previous Percentage.")
            Language.setMessage(mstrModuleName, 5, "-NA-")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
