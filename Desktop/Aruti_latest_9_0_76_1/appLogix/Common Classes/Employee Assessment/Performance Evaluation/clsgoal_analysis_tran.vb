﻿'************************************************************************************************************************************
'Class Name :clsgoal_analysis_tran.vb
'Purpose    :
'Date       :04-Aug-2014
'Written By :Sandeep J. Sharma
'Modified   :ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsgoal_analysis_tran
    Private Shared ReadOnly mstrModuleName As String = "clsgoal_analysis_tran"
    Dim mstrMessage As String = ""

#Region " Private Variables "

    Private mintAnalysisTranId As Integer = -1
    Private mintAnalysisUnkid As Integer = -1
    Private mdtTran As DataTable
    Private objDataOperation As clsDataOperation


    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Private ianalysistranunkid As Integer = -1
    Private ianalysisunkid As Integer = -1
    Private iperspectiveunkid As Integer = -1
    Private iempfield1unkid As Integer = -1
    Private iempfield2unkid As Integer = -1
    Private iempfield3unkid As Integer = -1
    Private iempfield4unkid As Integer = -1
    Private iempfield5unkid As Integer = -1
    Private iresult As Decimal = 0
    Private iremark As String = String.Empty
    Private iisvoid As Boolean = False
    Private ivoiduserunkid As Integer = -1
    Private ivoiddatetime As DateTime = Nothing
    Private ivoidreason As String = String.Empty
    Private iitem_weight As Decimal = 0
    Private imax_scale As Decimal = 0
    Private iagreedscore As Decimal = 0
    'S.SANDEEP |18-JAN-2020| -- END

#End Region

#Region " Properties "

    Public Property _AnalysisUnkid() As Integer
        Get
            Return mintAnalysisUnkid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisUnkid = value
            Call GetAnalysisTran()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDataOperation = value
        End Set
    End Property


    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Public Property _ianalysistranunkid() As Integer
        Get
            Return ianalysistranunkid
        End Get
        Set(ByVal value As Integer)
            ianalysistranunkid = value
        End Set
    End Property
    Public Property _ianalysisunkid() As Integer
        Get
            Return ianalysisunkid
        End Get
        Set(ByVal value As Integer)
            ianalysisunkid = value
        End Set
    End Property
    Public Property _iperspectiveunkid() As Integer
        Get
            Return iperspectiveunkid
        End Get
        Set(ByVal value As Integer)
            iperspectiveunkid = value
        End Set
    End Property
    Public Property _iempfield1unkid() As Integer
        Get
            Return iempfield1unkid
        End Get
        Set(ByVal value As Integer)
            iempfield1unkid = value
        End Set
    End Property
    Public Property _iempfield2unkid() As Integer
        Get
            Return iempfield2unkid
        End Get
        Set(ByVal value As Integer)
            iempfield2unkid = value
        End Set
    End Property
    Public Property _iempfield3unkid() As Integer
        Get
            Return iempfield3unkid
        End Get
        Set(ByVal value As Integer)
            iempfield3unkid = value
        End Set
    End Property
    Public Property _iempfield4unkid() As Integer
        Get
            Return iempfield4unkid
        End Get
        Set(ByVal value As Integer)
            iempfield4unkid = value
        End Set
    End Property
    Public Property _iempfield5unkid() As Integer
        Get
            Return iempfield5unkid
        End Get
        Set(ByVal value As Integer)
            iempfield5unkid = value
        End Set
    End Property
    Public Property _iresult() As Decimal
        Get
            Return iresult
        End Get
        Set(ByVal value As Decimal)
            iresult = value
        End Set
    End Property
    Public Property _iremark() As String
        Get
            Return iremark
        End Get
        Set(ByVal value As String)
            iremark = value
        End Set
    End Property
    Public Property _iisvoid() As Boolean
        Get
            Return iisvoid
        End Get
        Set(ByVal value As Boolean)
            iisvoid = value
        End Set
    End Property
    Public Property _ivoiduserunkid() As Integer
        Get
            Return ivoiduserunkid
        End Get
        Set(ByVal value As Integer)
            ivoiduserunkid = value
        End Set
    End Property
    Public Property _ivoiddatetime() As DateTime
        Get
            Return ivoiddatetime
        End Get
        Set(ByVal value As DateTime)
            ivoiddatetime = value
        End Set
    End Property
    Public Property _ivoidreason() As String
        Get
            Return ivoidreason
        End Get
        Set(ByVal value As String)
            ivoidreason = value
        End Set
    End Property
    Public Property _iitem_weight() As Decimal
        Get
            Return iitem_weight
        End Get
        Set(ByVal value As Decimal)
            iitem_weight = value
        End Set
    End Property
    Public Property _imax_scale() As Decimal
        Get
            Return imax_scale
        End Get
        Set(ByVal value As Decimal)
            imax_scale = value
        End Set
    End Property
    Public Property _iagreedscore() As Decimal
        Get
            Return iagreedscore
        End Get
        Set(ByVal value As Decimal)
            iagreedscore = value
        End Set
    End Property
    'S.SANDEEP |18-JAN-2020| -- END

#End Region

#Region " Contructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("Evaluation")
            mdtTran.Columns.Add("analysistranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("analysisunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("perspectiveunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("empfield1unkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("empfield2unkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("empfield3unkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("empfield4unkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("empfield5unkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("result", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTran.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            'S.SANDEEP [21 JAN 2015] -- START
            ''S.SANDEEP [ 01 JAN 2015 ] -- START
            'mdtTran.Columns.Add("computed_value", System.Type.GetType("System.Decimal")).DefaultValue = 0
            'mdtTran.Columns.Add("formula_used", System.Type.GetType("System.String")).DefaultValue = ""
            ''S.SANDEEP [ 01 JAN 2015 ] -- END
            mdtTran.Columns.Add("item_weight", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTran.Columns.Add("max_scale", System.Type.GetType("System.Decimal")).DefaultValue = 0

            '****************************************** COMPUTATION PURPOSE
            mdtTran.Columns.Add("iPeriodId", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("iItemUnkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("iEmployeeId", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("iScore", System.Type.GetType("System.Decimal")).DefaultValue = 0
            '****************************************** COMPUTATION PURPOSE

            'Shani (23-Nov-2016) -- Start
            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            mdtTran.Columns.Add("agreed_score", System.Type.GetType("System.Decimal")).DefaultValue = 0
            'Shani (23-Nov-2016) -- End


            'S.SANDEEP [21 JAN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        Finally
        End Try
    End Sub

    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Public Sub New(ByVal _analysistranunkid As Integer, _
                   ByVal _analysisunkid As Integer, _
                   ByVal _perspectiveunkid As Integer, _
                   ByVal _empfield1unkid As Integer, _
                   ByVal _empfield2unkid As Integer, _
                   ByVal _empfield3unkid As Integer, _
                   ByVal _empfield4unkid As Integer, _
                   ByVal _empfield5unkid As Integer, _
                   ByVal _result As Decimal, _
                   ByVal _remark As String, _
                   ByVal _isvoid As Boolean, _
                   ByVal _voiduserunkid As Integer, _
                   ByVal _voiddatetime As DateTime, _
                   ByVal _voidreason As String, _
                   ByVal _item_weight As Decimal, _
                   ByVal _max_scale As Decimal, _
                   ByVal _agreedscore As Decimal)

        ianalysistranunkid = _analysistranunkid
        ianalysisunkid = _analysisunkid
        iperspectiveunkid = _perspectiveunkid
        iempfield1unkid = _empfield1unkid
        iempfield2unkid = _empfield2unkid
        iempfield3unkid = _empfield3unkid
        iempfield4unkid = _empfield4unkid
        iempfield5unkid = _empfield5unkid
        iresult = _result
        iremark = _remark
        iisvoid = _isvoid
        ivoiduserunkid = _voiduserunkid
        ivoiddatetime = _voiddatetime
        ivoidreason = _voidreason
        iitem_weight = _item_weight
        imax_scale = _max_scale
        iagreedscore = _agreedscore
    End Sub
    'S.SANDEEP |18-JAN-2020| -- END

#End Region

#Region " Private/Public Methods "

    Private Sub GetAnalysisTran()
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDataOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            End If

            StrQ = "SELECT " & _
                   "     analysistranunkid " & _
                   "    ,analysisunkid " & _
                   "    ,perspectiveunkid " & _
                   "    ,empfield1unkid " & _
                   "    ,empfield2unkid " & _
                   "    ,empfield3unkid " & _
                   "    ,empfield4unkid " & _
                   "    ,empfield5unkid " & _
                   "    ,result " & _
                   "    ,remark " & _
                   "    ,isvoid " & _
                   "    ,voiduserunkid " & _
                   "    ,voiddatetime " & _
                   "    ,voidreason " & _
                   "    ,item_weight " & _
                   "    ,max_scale " & _
                   "    ,agreedscore " & _
                   "FROM hrgoals_analysis_tran " & _
                   "WHERE analysisunkid = @analysisunkid " & _
                   "AND isvoid = 0 " 'S.SANDEEP [ 01 JAN 2015 ] -- START {computed_value,formula_used} -- END

            'Shani (23-Nov-2016) -- [agreedscore]
            'S.SANDEEP [21 JAN 2015] -- START {REMOVED : (computed_value,formula_used) ; ADDED : (item_weight,max_scale)} -- END

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisUnkid.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [21 JAN 2015] -- START
            Dim xLinkedFieldId, xExOrder As Integer

            Dim objAnalysis As New clsevaluation_analysis_master
            Dim objFMapping As New clsAssess_Field_Mapping
            Dim objFMaster As New clsAssess_Field_Master
            objAnalysis._Analysisunkid = mintAnalysisUnkid
            xLinkedFieldId = objFMapping.Get_Map_FieldId(objAnalysis._Periodunkid)
            xExOrder = objFMaster.Get_Field_ExOrder(xLinkedFieldId)
            objFMaster = Nothing : objFMapping = Nothing
            'S.SANDEEP [21 JAN 2015] -- END

            mdtTran.Clear()
            For Each dtRow As DataRow In dsList.Tables("List").Rows
                mdtTran.ImportRow(dtRow)
                'S.SANDEEP [21 JAN 2015] -- START
                mdtTran.Rows(mdtTran.Rows.Count - 1).Item("iPeriodId") = objAnalysis._Periodunkid
                Select Case xExOrder
                    Case enWeight_Types.WEIGHT_FIELD1
                        mdtTran.Rows(mdtTran.Rows.Count - 1).Item("iItemUnkid") = dtRow.Item("empfield1unkid")
                    Case enWeight_Types.WEIGHT_FIELD2
                        mdtTran.Rows(mdtTran.Rows.Count - 1).Item("iItemUnkid") = dtRow.Item("empfield2unkid")
                    Case enWeight_Types.WEIGHT_FIELD3
                        mdtTran.Rows(mdtTran.Rows.Count - 1).Item("iItemUnkid") = dtRow.Item("empfield3unkid")
                    Case enWeight_Types.WEIGHT_FIELD4
                        mdtTran.Rows(mdtTran.Rows.Count - 1).Item("iItemUnkid") = dtRow.Item("empfield4unkid")
                    Case enWeight_Types.WEIGHT_FIELD5
                        mdtTran.Rows(mdtTran.Rows.Count - 1).Item("iItemUnkid") = dtRow.Item("empfield5unkid")
                End Select
                Select Case objAnalysis._Assessmodeid
                    Case enAssessmentMode.SELF_ASSESSMENT
                        mdtTran.Rows(mdtTran.Rows.Count - 1).Item("iEmployeeId") = objAnalysis._Selfemployeeunkid
                    Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                        mdtTran.Rows(mdtTran.Rows.Count - 1).Item("iEmployeeId") = objAnalysis._Assessedemployeeunkid
                End Select
                mdtTran.Rows(mdtTran.Rows.Count - 1).Item("iScore") = dtRow.Item("result")
                'S.SANDEEP [21 JAN 2015] -- END
                'Shani (23-Nov-2016) -- Start
                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                mdtTran.Rows(mdtTran.Rows.Count - 1).Item("agreed_score") = dtRow.Item("agreedscore")
                'Shani (23-Nov-2016) -- End
            Next
            'S.SANDEEP [21 JAN 2015] -- START
            objAnalysis = Nothing
            'S.SANDEEP [21 JAN 2015] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAnalysisTran", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function InsertUpdateDelete_AnalysisTran(Optional ByVal intUserUnkid As Integer = 0) As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO hrgoals_analysis_tran ( " & _
                                        "  analysisunkid " & _
                                        ", perspectiveunkid " & _
                                        ", empfield1unkid " & _
                                        ", empfield2unkid " & _
                                        ", empfield3unkid " & _
                                        ", empfield4unkid " & _
                                        ", empfield5unkid " & _
                                        ", result " & _
                                        ", remark " & _
                                        ", isvoid " & _
                                        ", voiduserunkid " & _
                                        ", voiddatetime " & _
                                        ", voidreason " & _
                                        ", item_weight " & _
                                        ", max_scale " & _
                                        ", agreedscore " & _
                                      ") VALUES (" & _
                                        "  @analysisunkid " & _
                                        ", @perspectiveunkid " & _
                                        ", @empfield1unkid " & _
                                        ", @empfield2unkid " & _
                                        ", @empfield3unkid " & _
                                        ", @empfield4unkid " & _
                                        ", @empfield5unkid " & _
                                        ", @result " & _
                                        ", @remark " & _
                                        ", @isvoid " & _
                                        ", @voiduserunkid " & _
                                        ", @voiddatetime " & _
                                        ", @voidreason " & _
                                        ", @item_weight " & _
                                        ", @max_scale " & _
                                        ", @agreedscore " & _
                                      "); SELECT @@identity" 'S.SANDEEP [ 01 JAN 2015 ] -- START {computed_value,formula_used} -- END
                                'Shani (23-Nov-2016) -- [agreedscore]
                                'S.SANDEEP [21 JAN 2015] -- START {REMOVED : (computed_value,formula_used)  ; ADDED : (item_weight,max_scale)} -- END

                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisUnkid)
                                objDataOperation.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("perspectiveunkid"))
                                objDataOperation.AddParameter("@empfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("empfield1unkid"))
                                objDataOperation.AddParameter("@empfield2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("empfield2unkid"))
                                objDataOperation.AddParameter("@empfield3unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("empfield3unkid"))
                                objDataOperation.AddParameter("@empfield4unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("empfield4unkid"))
                                objDataOperation.AddParameter("@empfield5unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("empfield5unkid"))
                                objDataOperation.AddParameter("@result", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("result"))
                                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, .Item("remark").ToString.Length, .Item("remark"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))

                                'S.SANDEEP [21 JAN 2015] -- START
                                ''S.SANDEEP [ 01 JAN 2015 ] -- START
                                'objDataOperation.AddParameter("@computed_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("computed_value"))
                                'objDataOperation.AddParameter("@formula_used", SqlDbType.NVarChar, .Item("formula_used").ToString.Length, .Item("formula_used"))
                                ''S.SANDEEP [ 01 JAN 2015 ] -- END
                                objDataOperation.AddParameter("@item_weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("item_weight"))
                                objDataOperation.AddParameter("@max_scale", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("max_scale"))
                                'S.SANDEEP [21 JAN 2015] -- END

                                'Shani (23-Nov-2016) -- Start
                                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                objDataOperation.AddParameter("@agreedscore", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("agreed_score"))
                                'Shani (23-Nov-2016) -- End


                                Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintAnalysisTranId = dsList.Tables(0).Rows(0).Item(0)

                                If .Item("analysisunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrgoals_analysis_tran", "analysistranunkid", mintAnalysisTranId, 2, 1, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", mintAnalysisUnkid, "hrgoals_analysis_tran", "analysistranunkid", mintAnalysisTranId, 1, 1, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                            Case "U"
                                StrQ = "UPDATE hrgoals_analysis_tran SET " & _
                                        "  analysisunkid = @analysisunkid " & _
                                        ", perspectiveunkid = @perspectiveunkid" & _
                                        ", empfield1unkid = @empfield1unkid" & _
                                        ", empfield2unkid = @empfield2unkid" & _
                                        ", empfield3unkid = @empfield3unkid" & _
                                        ", empfield4unkid = @empfield4unkid" & _
                                        ", empfield5unkid = @empfield5unkid" & _
                                        ", result = @result" & _
                                        ", remark = @remark" & _
                                        ", isvoid = @isvoid" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voidreason = @voidreason " & _
                                        ", item_weight = @item_weight " & _
                                        ", max_scale = @max_scale " & _
                                        ", agreedscore = @agreedscore " & _
                                      "WHERE analysistranunkid = @analysistranunkid " 'S.SANDEEP [ 01 JAN 2015 ] -- START {computed_value,formula_used} -- END

                                'Shani (23-Nov-2016) -- [agreedscore]
                                'S.SANDEEP [21 JAN 2015] -- START {REMOVED : (computed_value,formula_used) ; ADDED : (item_weight,max_scale)} -- END

                                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid"))
                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysisunkid"))
                                objDataOperation.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("perspectiveunkid"))
                                objDataOperation.AddParameter("@empfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("empfield1unkid"))
                                objDataOperation.AddParameter("@empfield2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("empfield2unkid"))
                                objDataOperation.AddParameter("@empfield3unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("empfield3unkid"))
                                objDataOperation.AddParameter("@empfield4unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("empfield4unkid"))
                                objDataOperation.AddParameter("@empfield5unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("empfield5unkid"))
                                objDataOperation.AddParameter("@result", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("result"))
                                objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, .Item("remark").ToString.Length, .Item("remark"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = True Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, .Item("voidreason"))

                                'S.SANDEEP [21 JAN 2015] -- START
                                ''S.SANDEEP [ 01 JAN 2015 ] -- START
                                'objDataOperation.AddParameter("@computed_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("computed_value"))
                                'objDataOperation.AddParameter("@formula_used", SqlDbType.NVarChar, .Item("formula_used").ToString.Length, .Item("formula_used"))
                                ''S.SANDEEP [ 01 JAN 2015 ] -- END
                                objDataOperation.AddParameter("@item_weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("item_weight"))
                                objDataOperation.AddParameter("@max_scale", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("max_scale"))
                                'S.SANDEEP [21 JAN 2015] -- END
                                'Shani (23-Nov-2016) -- Start
                                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                                objDataOperation.AddParameter("@agreedscore", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, .Item("agreed_score"))
                                'Shani (23-Nov-2016) -- End

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrgoals_analysis_tran", "analysistranunkid", .Item("analysistranunkid"), 2, 2, , intUserUnkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                            Case "D"
                                If .Item("analysistranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrgoals_analysis_tran", "analysistranunkid", .Item("analysistranunkid"), 2, 3, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                                StrQ = "UPDATE hrgoals_analysis_tran SET " & _
                                        "  isvoid = @isvoid" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voidreason = @voidreason " & _
                                      "WHERE analysistranunkid = @analysistranunkid "

                                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_AnalysisTran", mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Public Function InsertUpdate(ByVal objDataOperation As clsDataOperation, ByVal intUserUnkid As Integer) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        objDataOperation.ClearParameters()
        Try
            StrQ = "IF NOT EXISTS(SELECT * FROM hrgoals_analysis_tran WITH (NOLOCK) WHERE isvoid = 0 AND analysisunkid = @analysisunkid AND perspectiveunkid = @perspectiveunkid AND empfield1unkid = @empfield1unkid AND empfield2unkid = @empfield2unkid AND empfield3unkid = @empfield3unkid AND empfield4unkid = @empfield4unkid AND empfield5unkid = @empfield5unkid) " & _
                   "BEGIN " & _
                   "    INSERT INTO hrgoals_analysis_tran ( " & _
                   "      analysisunkid " & _
                   "    , perspectiveunkid " & _
                   "    , empfield1unkid " & _
                   "    , empfield2unkid " & _
                   "    , empfield3unkid " & _
                   "    , empfield4unkid " & _
                   "    , empfield5unkid " & _
                   "    , result " & _
                   "    , remark " & _
                   "    , isvoid " & _
                   "    , voiduserunkid " & _
                   "    , voiddatetime " & _
                   "    , voidreason " & _
                   "    , item_weight " & _
                   "    , max_scale " & _
                   "    , agreedscore " & _
                   "    ) VALUES (" & _
                   "      @analysisunkid " & _
                   "    , @perspectiveunkid " & _
                   "    , @empfield1unkid " & _
                   "    , @empfield2unkid " & _
                   "    , @empfield3unkid " & _
                   "    , @empfield4unkid " & _
                   "    , @empfield5unkid " & _
                   "    , @result " & _
                   "    , @remark " & _
                   "    , @isvoid " & _
                   "    , @voiduserunkid " & _
                   "    , @voiddatetime " & _
                   "    , @voidreason " & _
                   "    , @item_weight " & _
                   "    , @max_scale " & _
                   "    , @agreedscore " & _
                   "    ); SELECT @@identity,1 AS atype " & _
                   "END " & _
                   "ELSE " & _
                   "BEGIN " & _
                   "    DECLARE @analysistranunkid INT " & _
                   "    SET @analysistranunkid = (SELECT analysistranunkid FROM hrgoals_analysis_tran WITH (NOLOCK) WHERE isvoid = 0 AND analysisunkid = @analysisunkid AND perspectiveunkid = @perspectiveunkid AND empfield1unkid = @empfield1unkid AND empfield2unkid = @empfield2unkid AND empfield3unkid = @empfield3unkid AND empfield4unkid = @empfield4unkid AND empfield5unkid = @empfield5unkid) " & _
                   "    UPDATE hrgoals_analysis_tran SET " & _
                   "      analysisunkid = @analysisunkid " & _
                   "    , perspectiveunkid = @perspectiveunkid" & _
                   "    , empfield1unkid = @empfield1unkid" & _
                   "    , empfield2unkid = @empfield2unkid" & _
                   "    , empfield3unkid = @empfield3unkid" & _
                   "    , empfield4unkid = @empfield4unkid" & _
                   "    , empfield5unkid = @empfield5unkid" & _
                   "    , result = @result" & _
                   "    , remark = @remark" & _
                   "    , isvoid = @isvoid" & _
                   "    , voiduserunkid = @voiduserunkid" & _
                   "    , voiddatetime = @voiddatetime" & _
                   "    , voidreason = @voidreason " & _
                   "    , item_weight = @item_weight " & _
                   "    , max_scale = @max_scale " & _
                   "    , agreedscore = @agreedscore " & _
                   "    WHERE analysistranunkid = @analysistranunkid " & _
                   "    ;SELECT @analysistranunkid,2 AS atype " & _
                   "END "


            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, ianalysisunkid)
            objDataOperation.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iperspectiveunkid)
            objDataOperation.AddParameter("@empfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iempfield1unkid)
            objDataOperation.AddParameter("@empfield2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iempfield2unkid)
            objDataOperation.AddParameter("@empfield3unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iempfield3unkid)
            objDataOperation.AddParameter("@empfield4unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iempfield4unkid)
            objDataOperation.AddParameter("@empfield5unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iempfield5unkid)
            objDataOperation.AddParameter("@result", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iresult)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, iremark.ToString.Length, iremark)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, iisvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, ivoiduserunkid)
            If ivoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ivoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, ivoidreason)
            objDataOperation.AddParameter("@item_weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iitem_weight)
            objDataOperation.AddParameter("@max_scale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, imax_scale)
            objDataOperation.AddParameter("@agreedscore", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iagreedscore)

            Dim dsList As New DataSet
            Dim intAuditTypeId As Integer = 0

            If ianalysistranunkid <= 0 Then
                dsList = objDataOperation.ExecQuery(StrQ, "List")
            Else
                objDataOperation.ExecNonQuery(StrQ)
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If ianalysistranunkid <= 0 Then
                ianalysistranunkid = dsList.Tables(0).Rows(0).Item(0)
                intAuditTypeId = dsList.Tables(0).Rows(0).Item(1)
            End If

            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrevaluation_analysis_master", "analysisunkid", ianalysisunkid, "hrgoals_analysis_tran", "analysistranunkid", ianalysistranunkid, 2, intAuditTypeId, , intUserUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdate; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |18-JAN-2020| -- END

#End Region

End Class
