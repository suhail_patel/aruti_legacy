﻿'************************************************************************************************************************************
'Class Name : clsAssessor_tran.vb
'Purpose    :
'Date       :07/10/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class clsAssessor_tran

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "clsAssessor_tran"
    Private mstrMessage As String = ""
    Private mintAssessorEmployeeId As Integer = 0
    Private mdtTran As DataTable
    Private objUsrMapping As New clsapprover_Usermapping

    'Shani(01-MAR-2016) -- Start
    'Enhancement :PA External Approver Flow
    Private mblnIsExternalAssessor As Boolean = False
    'Shani(01-MAR-2016) -- End

    'Shani (07-Dec-2016) -- Start
    'optimization -  optimization to Assessor/Reviewer Add/edit Screen (TRA)
    Private strEmpAsOnDate As String = String.Empty
    'Shani (07-Dec-2016) -- End

    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Private mintAssessorMasterId As Integer = 0
    'S.SANDEEP |18-JAN-2020| -- END

#End Region

    'S.SANDEEP [27 DEC 2016] -- START
    'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
    Public Enum enOperationType
        Overwrite = 1
        Void = 2
    End Enum
    'S.SANDEEP [27 DEC 2016] -- END

#Region " Properties "

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    'Shani(01-MAR-2016) -- Start
    'Enhancement :PA External Approver Flow
    'Public Property _AssessorEmployeeId() As Integer
    '    Get
    '        Return mintAssessorEmployeeId
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintAssessorEmployeeId = value
    '        Call Get_Data()
    '    End Set
    'End Property
    Public Property _AssessorEmployeeId(ByVal blnIsExternalAssessor As Boolean, Optional ByVal eValue As clsAssessor.enAssessorType = clsAssessor.enAssessorType.ALL_DATA) As Integer
        Get
            Return mintAssessorEmployeeId
        End Get
        Set(ByVal value As Integer)
            mintAssessorEmployeeId = value
            mblnIsExternalAssessor = blnIsExternalAssessor
            Call Get_Data(eValue)
        End Set
    End Property
    'Shani(01-MAR-2016)-- End

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    'Shani (07-Dec-2016) -- Start
    'optimization -  optimization to Assessor/Reviewer Add/edit Screen (TRA)
    Public WriteOnly Property _EmployeeAsOnDate() As String
        Set(ByVal value As String)
            strEmpAsOnDate = value
        End Set
    End Property
    'Shani (07-Dec-2016) -- End

    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Public WriteOnly Property _AssessorMasterId() As Integer
        Set(ByVal value As Integer)
            mintAssessorMasterId = value
            Call Get_Data(clsAssessor.enAssessorType.ALL_DATA)
        End Set
    End Property
    'S.SANDEEP |18-JAN-2020| -- END

#End Region

#Region " Constructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("List")
            Dim dCol As DataColumn

            dCol = New DataColumn
            dCol.ColumnName = "assessortranunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "assessormasterunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "stationunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "departmentunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "employeeunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "isvoid"
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voiddatetime"
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voiduserunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voidreason"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "AUD"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "GUID"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "ischeck"
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "employeecode"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "name"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "department"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "job"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "isreviewer"
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "arId"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            dCol = New DataColumn
            dCol.ColumnName = "visibletypeid"
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)
            'S.SANDEEP [27 DEC 2016] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Public & Private Methods "

    Private Sub Get_Data(ByVal eValue As clsAssessor.enAssessorType)
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            Dim objData As New clsDataOperation

            'Shani (07-Dec-2016) -- Start
            'optimization -  optimization to Assessor/Reviewer Add/edit Screen (TRA)
            'StrQ = "SELECT " & _
            '        "	 hrassessor_tran.assessortranunkid " & _
            '        "   ,hrassessor_tran.assessormasterunkid " & _
            '        "   ,ISNULL(hrassessor_tran.stationunkid,0) AS stationunkid " & _
            '        "   ,hrassessor_tran.departmentunkid " & _
            '        "   ,hrassessor_tran.employeeunkid " & _
            '        "   ,hrassessor_tran.isvoid " & _
            '        "   ,hrassessor_tran.voiddatetime " & _
            '        "   ,hrassessor_tran.voiduserunkid " & _
            '        "   ,hrassessor_tran.voidreason " & _
            '        "	,CAST(0 AS BIT) AS ischeck " & _
            '        "	,employeecode " & _
            '        "	,firstname+' '+ surname AS name " & _
            '        "	,hrdepartment_master.name AS Department " & _
            '        "	,job_name AS Job " & _
            '        "	,hremployee_master.employeeunkid " & _
            '        "   ,hrassessor_master.employeeunkid AS arId " & _
            '        "	,isreviewer " & _
            '        "	,'' AS AUD " & _
            '        "FROM hrassessor_tran " & _
            '        "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
            '        "	JOIN hremployee_master ON hrassessor_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '        "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '        "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '        "WHERE hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
            '        "	AND hrassessor_master.employeeunkid = '" & mintAssessorEmployeeId & "' AND hrassessor_master.isexternalapprover = @isexternalapprover "


            'S.SANDEEP [29-NOV-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 101
            Dim xDateJoinQry, xDataFilter As String
            xDateJoinQry = "" : xDataFilter = ""
            Aruti.Data.modGlobal.GetDatesFilterString(xDateJoinQry, xDataFilter, eZeeDate.convertDate(strEmpAsOnDate), eZeeDate.convertDate(strEmpAsOnDate), True, False, "", "")
            'S.SANDEEP [29-NOV-2017] -- END

            StrQ = "SELECT " & _
                    "	 hrassessor_tran.assessortranunkid " & _
                    "   ,hrassessor_tran.assessormasterunkid " & _
                    "   ,ISNULL(hrassessor_tran.stationunkid,0) AS stationunkid " & _
                   "    ,ISNULL(hrassessor_tran.departmentunkid,0) AS departmentunkid " & _
                    "   ,hrassessor_tran.employeeunkid " & _
                    "   ,hrassessor_tran.isvoid " & _
                    "   ,hrassessor_tran.voiddatetime " & _
                    "   ,hrassessor_tran.voiduserunkid " & _
                    "   ,hrassessor_tran.voidreason " & _
                    "	,CAST(0 AS BIT) AS ischeck " & _
                    "	,employeecode " & _
                    "	,firstname+' '+ surname AS name " & _
                    "	,hrdepartment_master.name AS Department " & _
                    "	,job_name AS Job " & _
                    "	,hremployee_master.employeeunkid " & _
                    "   ,hrassessor_master.employeeunkid AS arId " & _
                    "	,isreviewer " & _
                    "	,'' AS AUD " & _
                    "   ,hrassessor_tran.visibletypeid " & _
                    "FROM hrassessor_tran WITH (NOLOCK) " & _
                    "	JOIN hrassessor_master WITH (NOLOCK) ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                    "	JOIN hremployee_master WITH (NOLOCK) ON hrassessor_tran.employeeunkid = hremployee_master.employeeunkid "
            If xDateJoinQry.Trim().Length > 0 Then
                StrQ &= " " & xDateJoinQry & " "
            End If
            StrQ &= "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             Trf.TrfEmpId " & _
                   "            ,ISNULL(Trf.departmentunkid,0) AS departmentunkid " & _
                   "            ,ISNULL(Trf.EfDt,'') AS EfDt " & _
                   "        FROM " & _
                   "        ( " & _
                   "            SELECT " & _
                   "                 ETT.employeeunkid AS TrfEmpId " & _
                   "                ,ISNULL(ETT.departmentunkid,0) AS departmentunkid " & _
                   "                ,CONVERT(CHAR(8),ETT.effectivedate,112) AS EfDt " & _
                   "                ,ROW_NUMBER()OVER(PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC) AS Rno " & _
                   "            FROM hremployee_transfer_tran AS ETT WITH (NOLOCK) " & _
                   "            WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmpAsOnDate & "' " & _
                   "        ) AS Trf WHERE Trf.Rno = 1 " & _
                   "    ) AS ETRF ON ETRF.TrfEmpId = hremployee_master.employeeunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             Cat.CatEmpId " & _
                   "            ,Cat.jobunkid " & _
                   "            ,Cat.CEfDt " & _
                   "        FROM " & _
                   "        ( " & _
                   "            SELECT " & _
                   "                 ECT.employeeunkid AS CatEmpId " & _
                   "                ,ECT.jobunkid " & _
                   "                ,CONVERT(CHAR(8),ECT.effectivedate,112) AS CEfDt " & _
                   "                ,ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                   "            FROM hremployee_categorization_tran AS ECT WITH (NOLOCK) " & _
                   "            WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmpAsOnDate & "' " & _
                   "        ) AS Cat WHERE Cat.Rno = 1 " & _
                   "    ) AS ERECAT ON ERECAT.CatEmpId = hremployee_master.employeeunkid " & _
                   "    LEFT JOIN hrjob_master WITH (NOLOCK) ON ERECAT.jobunkid = hrjob_master.jobunkid AND hrjob_master.isactive = 1 " & _
                   "    LEFT JOIN hrdepartment_master WITH (NOLOCK) ON ETRF.departmentunkid = hrdepartment_master.departmentunkid AND hrdepartment_master.isactive = 1 " & _
                    "WHERE 1 = 1 "
            If mintAssessorMasterId > 0 Then
                StrQ &= "	AND hrassessor_master.assessormasterunkid = '" & mintAssessorMasterId & "' "
            Else
                StrQ &= "	AND hrassessor_master.employeeunkid = '" & mintAssessorEmployeeId & "' AND hrassessor_master.isexternalapprover = @isexternalapprover "
            End If
            StrQ &= "   AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
                    "   AND hrassessor_master.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' " & _
                    "   AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' "

            'S.SANDEEP |18-JAN-2020| -- START
            'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
            '-- ADDED - "	AND hrassessor_master.assessormasterunkid = '" & mintAssessorMasterId & "' "
            'S.SANDEEP |18-JAN-2020| -- END


            'S.SANDEEP [29-NOV-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 101
            If xDataFilter.Trim().Length > 0 Then
                StrQ &= " " & xDataFilter & " "
            End If
            'S.SANDEEP [29-NOV-2017] -- END



            'S.SANDEEP [27 DEC 2016] -- START {visibletypeid}-- END



            'Shani (07-Dec-2016) -- End
            If eValue = clsAssessor.enAssessorType.ASSESSOR Then
                StrQ &= "   AND hrassessor_master.isreviewer = 0 "
            ElseIf eValue = clsAssessor.enAssessorType.REVIEWER Then
                StrQ &= "   AND hrassessor_master.isreviewer = 1 "
            End If

            StrQ &= "ORDER BY firstname+' '+ surname "

            'Shani(01-MAR-2016) -- Start
            'Enhancement :PA External Approver Flow
            '[ADD--> AND hrassessor_master.isexternalapprover = @isexternalapprover ]
            objData.ClearParameters()
            objData.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalAssessor)
            'Shani(01-MAR-2016)-- End

            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()

            For Each dRow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(dRow)
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function Insert_Update_Delete(ByVal objData As clsDataOperation, ByVal iAssessorMasterId As Integer) As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objData.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO hrassessor_tran ( " & _
                                            "  assessormasterunkid " & _
                                                                ", stationunkid " & _
                                            ", departmentunkid " & _
                                            ", employeeunkid" & _
                                            ", isvoid " & _
                                            ", voiddatetime " & _
                                            ", voiduserunkid " & _
                                            ", voidreason" & _
                                            ", visibletypeid " & _
                                       ") VALUES (" & _
                                            "  @assessormasterunkid " & _
                                                                ", @stationunkid " & _
                                            ", @departmentunkid " & _
                                            ", @employeeunkid" & _
                                            ", @isvoid " & _
                                            ", @voiddatetime " & _
                                            ", @voiduserunkid " & _
                                            ", @voidreason" & _
                                            ", @visibletypeid " & _
                                       "); SELECT @@identity "
                                objData.ClearParameters()
                                objData.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iAssessorMasterId.ToString)
                                objData.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("stationunkid").ToString)
                                objData.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("departmentunkid").ToString)
                                objData.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objData.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objData.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objData.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                'S.SANDEEP [27 DEC 2016] -- START
                                'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
                                objData.AddParameter("@visibletypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("visibletypeid"))
                                'S.SANDEEP [27 DEC 2016] -- END

                                dsList = objData.ExecQuery(StrQ, "List")

                                If objData.ErrorMessage <> "" Then
                                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                Dim iAssessorTranId As Integer = CInt(dsList.Tables(0).Rows(0)(0))

                                If clsCommonATLog.Insert_AtLog(objData, 1, "hrassessor_tran", "assessortranunkid", iAssessorTranId) = False Then
                                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                                    Throw exForce
                                End If


                                'S.SANDEEP [27 DEC 2016] -- START
                                'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
                            Case "U"
                                StrQ = "UPDATE hrassessor_tran " & _
                                       "SET assessormasterunkid = @assessormasterunkid " & _
                                       "    ,stationunkid = @stationunkid " & _
                                       "    ,departmentunkid = @departmentunkid " & _
                                       "    ,employeeunkid = @employeeunkid " & _
                                       "    ,isvoid = @isvoid " & _
                                       "    ,voiddatetime = @voiddatetime " & _
                                       "    ,voiduserunkid = @voiduserunkid " & _
                                       "    ,voidreason = @voidreason " & _
                                       "    ,visibletypeid = @visibletypeid " & _
                                       "WHERE assessortranunkid = @assessortranunkid "

                                objData.AddParameter("@assessortranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("assessortranunkid").ToString)
                                objData.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("assessormasterunkid").ToString)
                                objData.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("stationunkid").ToString)
                                objData.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("departmentunkid").ToString)
                                objData.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objData.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objData.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objData.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objData.AddParameter("@visibletypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("visibletypeid"))

                                objData.ExecNonQuery(StrQ)

                                If objData.ErrorMessage <> "" Then
                                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_AtLog(objData, 2, "hrassessor_tran", "assessortranunkid", .Item("assessortranunkid")) = False Then
                                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [27 DEC 2016] -- END

                            Case "D"

                                If clsCommonATLog.Insert_AtLog(objData, 3, "hrassessor_tran", "assessortranunkid", .Item("assessortranunkid")) = False Then
                                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                StrQ = "UPDATE hrassessor_tran SET " & _
                                         " isvoid = @isvoid " & _
                                        ",voiddatetime = @voiddatetime " & _
                                        ",voiduserunkid = @voiduserunkid " & _
                                        ",voidreason = @voidreason " & _
                                        ",visibletypeid = @visibletypeid " & _
                                       "WHERE assessortranunkid = @assessortranunkid "
                                objData.ClearParameters()
                                objData.AddParameter("@assessortranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("assessortranunkid").ToString)
                                objData.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                'S.SANDEEP [ 01 NOV 2013 ] -- START
                                'objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                'S.SANDEEP [ 01 NOV 2013 ] -- END
                                objData.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objData.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                'S.SANDEEP [27 DEC 2016] -- START
                                'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
                                objData.AddParameter("@visibletypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("visibletypeid"))
                                'S.SANDEEP [27 DEC 2016] -- END


                                Call objData.ExecNonQuery(StrQ)

                                If objData.ErrorMessage <> "" Then
                                    exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_Update_Delete; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP [ 01 JAN 2015 ] -- START

    'Shani(01-MAR-2016) -- Start
    'Enhancement :PA External Approver Flow
    'Public Function Get_AssignedEmp(ByVal iEmployeeId As Integer, ByVal blnIsAssessor As Boolean) As String
    'S.SANDEEP [27 DEC 2016] -- START
    'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
    'Public Function Get_AssignedEmp(ByVal iEmployeeId As Integer, ByVal blnIsAssessor As Boolean, ByVal blnIsExternalAssessor As Boolean) As String
    Public Function Get_AssignedEmp(ByVal iEmployeeId As Integer, ByVal blnIsAssessor As Boolean, _
                                    ByVal blnIsExternalAssessor As Boolean, ByVal strVisibilityTypeIds As String) As String
        'S.SANDEEP [27 DEC 2016] -- END


        'Shani(01-MAR-2016) -- End

        'Public Function Get_AssignedEmp(ByVal iEmployeeId As Integer) As String
        'S.SANDEEP [ 01 JAN 2015 ] -- END
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim sValue As String = String.Empty
        Try
            Dim objData As New clsDataOperation

            'S.SANDEEP [ 05 NOV 2014 ] -- START
            'StrQ = "SELECT ISNULL(STUFF((SELECT DISTINCT ',' + CAST(hrassessor_tran.employeeunkid AS NVARCHAR(MAX)) " & _
            '                   " FROM hrassessor_tran " & _
            '                   "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
            '                   " WHERE hrassessor_master.employeeunkid = '" & iEmployeeId & "' AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
            '                   "FOR XML PATH('')),1,1,''),'') AS aValue "

            'S.SANDEEP [ 01 JAN 2015 ] -- START
            'StrQ = "SELECT ISNULL(STUFF((SELECT DISTINCT ',' + CAST(hrassessor_tran.employeeunkid AS NVARCHAR(MAX)) " & _
            '       "FROM hrassessor_tran " & _
            '       "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
            '       " WHERE hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
            '       "FOR XML PATH('')),1,1,''),'') AS aValue "

            'Shani(01-MAR-2016) -- Start
            'Enhancement :PA External Approver Flow
            'StrQ = "SELECT ISNULL(STUFF((SELECT DISTINCT ',' + CAST(hrassessor_tran.employeeunkid AS NVARCHAR(MAX)) " & _
            '       "FROM hrassessor_tran " & _
            '       "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
            '       " WHERE hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 "
            'If blnIsAssessor = True Then
            '    StrQ &= "AND isreviewer = 0 "
            'Else
            '    StrQ &= "AND isreviewer = 1 "
            'End If
            'StrQ &= "FOR XML PATH('')),1,1,''),'') AS aValue "
            ''S.SANDEEP [ 01 JAN 2015 ] -- END
            ''S.SANDEEP [ 05 NOV 2014 ] -- END

            'Shani (29-Nov-2016) -- Start
            'Enhancement - 
            'StrQ = "SELECT ISNULL(STUFF(( " & _
            '       "    SELECT " & _
            '       "        ',' + CAST(employeeunkid AS NVARCHAR(MAX)) " & _
            '       "    FROM " & _
            '       "    ( " & _
            '       "        SELECT DISTINCT " & _
            '       "            CAST(hrassessor_tran.employeeunkid AS NVARCHAR(MAX)) AS employeeunkid " & _
            '       "FROM hrassessor_tran " & _
            '       "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
            '       "        WHERE hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0  AND isreviewer =  " & IIf(blnIsAssessor, 0, 1) & " " & _
            '       "        UNION ALL " & _
            '       "        SELECT DISTINCT " & _
            '       "            CAST(hrassessor_tran.employeeunkid AS NVARCHAR(MAX)) AS employeeunkid " & _
            '       "        FROM hrassessor_tran " & _
            '       "            JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
            '       "        WHERE hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0  AND hrassessor_master.employeeunkid = '" & iEmployeeId & "' " & _
            '       "        AND isexternalapprover = " & IIf(blnIsExternalAssessor, 1, 0) & " " & _
            '       "    ) As A " & _
            '       "FOR XML PATH ('')), 1, 1, ''), '') AS aValue "

            StrQ = "SELECT ISNULL(STUFF(( " & _
                   "    SELECT " & _
                   "        ',' + CAST(employeeunkid AS NVARCHAR(MAX)) " & _
                   "    FROM " & _
                   "    ( " & _
                   "        SELECT DISTINCT " & _
                   "            CAST(hrassessor_tran.employeeunkid AS NVARCHAR(MAX)) AS employeeunkid " & _
                   "FROM hrassessor_tran WITH (NOLOCK) " & _
                   "	JOIN hrassessor_master WITH (NOLOCK) ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                   "        WHERE hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0  AND isreviewer =  " & IIf(blnIsAssessor, 0, 1) & " #VALUE# " & _
                   "    AND hrassessor_tran.visibletypeid IN(" & strVisibilityTypeIds & ")) As A " & _
                   "FOR XML PATH ('')), 1, 1, ''), '') AS aValue "
            'Shani (29-Nov-2016) -- End

            'Shani(01-MAR-2016) -- End


            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            If iEmployeeId > 0 Then
                StrQ = StrQ.Replace("#VALUE#", " AND hrassessor_master.employeeunkid = '" & iEmployeeId & "'")
            Else
                StrQ = StrQ.Replace("#VALUE#", " ")
            End If
            'Gajanan [23-SEP-2019] -- End

            dsList = objData.ExecQuery(StrQ, "List")

            If dsList.Tables("List").Rows.Count > 0 Then
                sValue = dsList.Tables("List").Rows(0).Item("aValue")
            End If

            Return sValue
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_AssignedEmp", mstrModuleName)
            Return ""
        Finally
        End Try
    End Function

    Public Function IsAssessorPresent(ByVal iEmpId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim StrMsg As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT 1 FROM hrassessor_tran WITH (NOLOCK) " & _
                   " JOIN hrassessor_master WITH (NOLOCK) ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                   "WHERE hrassessor_tran.employeeunkid = '" & iEmpId & "' AND hrassessor_tran.isvoid = 0 " & _
                   " AND isreviewer = 0 AND hrassessor_master.isvoid = 0 "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & "  : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count <= 0 Then
                StrMsg = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot do submit for approval operation. Reason : No Assessor assigned to to this employee.")
            End If

            Return StrMsg
        Catch ex As Exception
            Throw New Exception(objDataOperation.ErrorNumber & "  : " & objDataOperation.ErrorMessage)
        Finally
        End Try
    End Function


    'Shani(01-MAR-2016) -- Start
    'Enhancement :PA External Approver Flow
    '    Public Function Import_Assesor_Reviewe_Access(ByVal iAccessEmpId As Integer, _
    '                                                  ByVal iDepartmentId As Integer, _
    '                                                  ByVal iAREmpId As Integer, _
    '                                                  ByVal blnIsReviewer As Boolean, _
    '                                                  ByRef iMessage As String) As Integer
    '        Dim StrQ As String = String.Empty
    '        Dim objDataOperation As clsDataOperation
    '        Dim iRefValue As Integer = -1
    '        Dim iAssessorMasterId As Integer = 0
    '        Dim dsList As New DataSet
    '        Dim exForce As Exception
    '        Dim objOption As New clsPassowdOptions
    '        Try
    '            objDataOperation = New clsDataOperation

    '            iRefValue = -1

    '            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER MASTER */--/* START */
    '            If blnIsReviewer = False Then
    '                StrQ = "SELECT assessormasterunkid FROM hrassessor_master WHERE isvoid = 0 AND employeeunkid = '" & iAREmpId & "' AND isreviewer = 0 "
    '            Else
    '                StrQ = "SELECT assessormasterunkid FROM hrassessor_master WHERE isvoid = 0 AND employeeunkid = '" & iAREmpId & "' AND isreviewer = 1 "
    '            End If

    '            dsList = objDataOperation.ExecQuery(StrQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If dsList.Tables("List").Rows.Count > 0 Then
    '                iAssessorMasterId = dsList.Tables("List").Rows(0).Item("assessormasterunkid")
    '                Dim iCnt As Integer = 0
    '                iCnt = objDataOperation.RecordCount("SELECT * FROM hrassessor_tran WHERE assessormasterunkid = '" & iAssessorMasterId & "' AND employeeunkid = '" & iAccessEmpId & "'")

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If
    '                If iCnt > 0 Then
    '                    iRefValue = 0
    '                    GoTo final
    '                End If
    '            Else
    '                StrQ = "INSERT INTO hrassessor_master " & _
    '                       " (employeeunkid,userunkid,isreviewer,isvoid,voiddatetime,voiduserunkid,voidreason) " & _
    '                       "SELECT '" & iAREmpId & "','" & User._Object._Userunkid & "','" & IIf(blnIsReviewer = True, "True", "False") & "','False',NULL,-1,'' " & _
    '                       " WHERE NOT EXISTS (SELECT employeeunkid FROM hrassessor_master WHERE isvoid = 0 AND isreviewer = " & IIf(blnIsReviewer = True, 1, 0) & " AND employeeunkid = '" & iAREmpId & "');SELECT @@IDENTITY; "

    '                dsList = objDataOperation.ExecQuery(StrQ, "List")

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If

    '                iAssessorMasterId = dsList.Tables("List").Rows(0).Item(0)

    '                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassessor_master", "assessormasterunkid", iAssessorMasterId) = False Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If
    '            End If
    '            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER MASTER */--/* END */

    '            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER USER MAPPING */--/* START */
    '            If objOption._IsEmployeeAsUser = True Then
    '                If iAssessorMasterId > 0 Then
    '                    StrQ = "SELECT ISNULL(userunkid,0) AS UsrId " & _
    '                           "FROM hremployee_master " & _
    '                           "JOIN hrmsConfiguration..cfuser_master ON hremployee_master.displayname = hrmsConfiguration..cfuser_master.username " & _
    '                           "WHERE hremployee_master.employeeunkid = '" & iAREmpId & "' "

    '                    dsList = objDataOperation.ExecQuery(StrQ, "Usr")

    '                    If objDataOperation.ErrorMessage <> "" Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If

    '                    If dsList.Tables("Usr").Rows.Count > 0 Then
    '                        If dsList.Tables("Usr").Rows(0).Item("UsrId") > 0 Then

    '                            Dim iCnt As Integer = 0
    '                            iCnt = objDataOperation.RecordCount("SELECT * FROM hrapprover_usermapping WHERE approverunkid = '" & iAssessorMasterId & "' AND userunkid = '" & dsList.Tables("Usr").Rows(0).Item("UsrId") & "' AND usertypeid = '" & enUserType.Assessor & "'")

    '                            If iCnt <= 0 Then
    '                                StrQ = "INSERT INTO hrapprover_usermapping " & _
    '                                   "(approverunkid,userunkid,usertypeid) " & _
    '                                   "SELECT '" & iAssessorMasterId & "', '" & dsList.Tables("Usr").Rows(0).Item("UsrId") & "','" & enUserType.Assessor & "' " & _
    '                                   "WHERE NOT EXISTS (SELECT approverunkid FROM hrapprover_usermapping WHERE usertypeid = '" & enUserType.Assessor & "' AND approverunkid = '" & iAssessorMasterId & "');SELECT @@IDENTITY; "

    '                                dsList = objDataOperation.ExecQuery(StrQ, "uList")

    '                                If objDataOperation.ErrorMessage <> "" Then
    '                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                                    Throw exForce
    '                                End If
    '                                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrapprover_usermapping", "mappingunkid", dsList.Tables("uList").Rows(0).Item(0)) = False Then
    '                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                                    Throw exForce
    '                                End If
    '                            End If
    '                        End If
    '                    End If
    '                End If
    '            End If
    '            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER USER MAPPING */--/* END */

    '            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER ACCESS */--/* START */
    '            If iAssessorMasterId > 0 Then
    '                StrQ = "INSERT INTO hrassessor_tran " & _
    '                       "(assessormasterunkid,stationunkid,departmentunkid,employeeunkid,isvoid,voiddatetime,voiduserunkid,voidreason) " & _
    '                       "SELECT '" & iAssessorMasterId & "', 0, '" & iDepartmentId & "', '" & iAccessEmpId & "','False',NULL,-1,''" & _
    '                       "WHERE NOT EXISTS (SELECT assessortranunkid FROM hrassessor_tran WHERE assessormasterunkid = '" & iAssessorMasterId & "' AND employeeunkid = '" & iAccessEmpId & "');SELECT @@IDENTITY; "

    '                dsList = objDataOperation.ExecQuery(StrQ, "lst")

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If

    '                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassessor_tran", "assessortranunkid", dsList.Tables("lst").Rows(0).Item(0)) = False Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If
    '            End If
    '            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER ACCESS */--/* END */

    'final:      If iRefValue = -1 Then iRefValue = 1

    '            Return iRefValue
    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "Import_Assesor_Reviewe_Access", mstrModuleName)
    '            iMessage = ex.Message
    '            Return iRefValue
    '        Finally
    '            objDataOperation = Nothing
    '        End Try
    '    End Function
    Public Function Import_Assesor_Reviewe_Access(ByVal iAccessEmpId As Integer, _
                                                  ByVal iDepartmentId As Integer, _
                                                  ByVal iAREmpId As Integer, _
                                                  ByVal blnIsReviewer As Boolean, _
                                                  ByRef iMessage As String, _
                                                  ByVal blnIsExternal As Boolean) As Integer 'Shani(01-MAR-2016) -- [blnIsExternal]

        Dim StrQ As String = String.Empty
        Dim objDataOperation As clsDataOperation
        Dim iRefValue As Integer = -1
        Dim iAssessorMasterId As Integer = 0
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim objOption As New clsPassowdOptions
        Try
            objDataOperation = New clsDataOperation

            iRefValue = -1

            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER MASTER */--/* START */
            If blnIsReviewer = False Then
                StrQ = "SELECT assessormasterunkid FROM hrassessor_master WHERE isvoid = 0 AND employeeunkid = '" & iAREmpId & "' AND isreviewer = 0 "
            Else
                StrQ = "SELECT assessormasterunkid FROM hrassessor_master WHERE isvoid = 0 AND employeeunkid = '" & iAREmpId & "' AND isreviewer = 1 "
            End If

            StrQ &= " AND isexternalapprover = @isexternalapprover "
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsExternal)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                iAssessorMasterId = dsList.Tables("List").Rows(0).Item("assessormasterunkid")
                Dim iCnt As Integer = 0
                iCnt = objDataOperation.RecordCount("SELECT * FROM hrassessor_tran WHERE assessormasterunkid = '" & iAssessorMasterId & "' AND employeeunkid = '" & iAccessEmpId & "'")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If iCnt > 0 Then
                    iRefValue = 0
                    GoTo final
                End If
            Else

                StrQ = "INSERT INTO hrassessor_master " & _
                       " (employeeunkid,userunkid,isreviewer,isvoid,voiddatetime,voiduserunkid,voidreason,isexternalapprover,visibletypeid) " & _
                       "SELECT '" & iAREmpId & "','" & User._Object._Userunkid & "','" & IIf(blnIsReviewer = True, "True", "False") & "','False',NULL,-1,'',@isexternalapprover,'" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' " & _
                       " WHERE NOT EXISTS (SELECT employeeunkid FROM hrassessor_master WHERE isvoid = 0 AND isreviewer = " & IIf(blnIsReviewer = True, 1, 0) & " AND employeeunkid = '" & iAREmpId & "' AND isexternalapprover = @isexternalapprover );SELECT @@IDENTITY; "
                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                iAssessorMasterId = dsList.Tables("List").Rows(0).Item(0)

                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassessor_master", "assessormasterunkid", iAssessorMasterId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If
            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER MASTER */--/* END */

            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER USER MAPPING */--/* START */

            'S.SANDEEP |25-MAR-2019| -- START
            'If objOption._IsEmployeeAsUser = True Then
            '    If iAssessorMasterId > 0 Then

            '        If blnIsExternal = True Then
            '            StrQ = "SELECT ISNULL(userunkid,0) AS UsrId " & _
            '                   "FROM hrmsConfiguration..cfuser_master " & _
            '                   "WHERE userunkid = '" & iAREmpId & "' "
            '        Else
            '            StrQ = "SELECT ISNULL(userunkid,0) AS UsrId " & _
            '                   "FROM hremployee_master " & _
            '                   "JOIN hrmsConfiguration..cfuser_master ON hremployee_master.displayname = hrmsConfiguration..cfuser_master.username " & _
            '                   "WHERE hremployee_master.employeeunkid = '" & iAREmpId & "' "
            '        End If

            '        dsList = objDataOperation.ExecQuery(StrQ, "Usr")

            '        If objDataOperation.ErrorMessage <> "" Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '        If dsList.Tables("Usr").Rows.Count > 0 Then
            '            If dsList.Tables("Usr").Rows(0).Item("UsrId") > 0 Then

            '                Dim iCnt As Integer = 0
            '                iCnt = objDataOperation.RecordCount("SELECT * FROM hrapprover_usermapping WHERE approverunkid = '" & iAssessorMasterId & "' AND userunkid = '" & dsList.Tables("Usr").Rows(0).Item("UsrId") & "' AND usertypeid = '" & enUserType.Assessor & "'")

            '                If iCnt <= 0 Then
            '                    StrQ = "INSERT INTO hrapprover_usermapping " & _
            '                       "(approverunkid,userunkid,usertypeid) " & _
            '                       "SELECT '" & iAssessorMasterId & "', '" & dsList.Tables("Usr").Rows(0).Item("UsrId") & "','" & enUserType.Assessor & "' " & _
            '                       "WHERE NOT EXISTS (SELECT approverunkid FROM hrapprover_usermapping WHERE usertypeid = '" & enUserType.Assessor & "' AND approverunkid = '" & iAssessorMasterId & "');SELECT @@IDENTITY; "

            '                    dsList = objDataOperation.ExecQuery(StrQ, "uList")

            '                    If objDataOperation.ErrorMessage <> "" Then
            '                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '                        Throw exForce
            '                    End If
            '                    If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrapprover_usermapping", "mappingunkid", dsList.Tables("uList").Rows(0).Item(0)) = False Then
            '                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '                        Throw exForce
            '                    End If
            '                End If
            '            End If
            '        End If
            '    End If
            'End If
                If iAssessorMasterId > 0 Then

                    If blnIsExternal = True Then
                        StrQ = "SELECT ISNULL(userunkid,0) AS UsrId " & _
                               "FROM hrmsConfiguration..cfuser_master " & _
                               "WHERE userunkid = '" & iAREmpId & "' "
                    Else
                        StrQ = "SELECT ISNULL(userunkid,0) AS UsrId " & _
                               "FROM hremployee_master " & _
                               "JOIN hrmsConfiguration..cfuser_master ON hremployee_master.displayname = hrmsConfiguration..cfuser_master.username " & _
                               "WHERE hremployee_master.employeeunkid = '" & iAREmpId & "' "
                    End If

                    dsList = objDataOperation.ExecQuery(StrQ, "Usr")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    If dsList.Tables("Usr").Rows.Count > 0 Then
                        If dsList.Tables("Usr").Rows(0).Item("UsrId") > 0 Then

                            Dim iCnt As Integer = 0
                            iCnt = objDataOperation.RecordCount("SELECT * FROM hrapprover_usermapping WHERE approverunkid = '" & iAssessorMasterId & "' AND userunkid = '" & dsList.Tables("Usr").Rows(0).Item("UsrId") & "' AND usertypeid = '" & enUserType.Assessor & "'")

                            If iCnt <= 0 Then
                                StrQ = "INSERT INTO hrapprover_usermapping " & _
                                   "(approverunkid,userunkid,usertypeid) " & _
                                   "SELECT '" & iAssessorMasterId & "', '" & dsList.Tables("Usr").Rows(0).Item("UsrId") & "','" & enUserType.Assessor & "' " & _
                                   "WHERE NOT EXISTS (SELECT approverunkid FROM hrapprover_usermapping WHERE usertypeid = '" & enUserType.Assessor & "' AND approverunkid = '" & iAssessorMasterId & "');SELECT @@IDENTITY; "

                                dsList = objDataOperation.ExecQuery(StrQ, "uList")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrapprover_usermapping", "mappingunkid", dsList.Tables("uList").Rows(0).Item(0)) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            End If
                        End If
                    End If
                End If

            'S.SANDEEP |25-MAR-2019| -- END


            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER USER MAPPING */--/* END */

            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER ACCESS */--/* START */
            If iAssessorMasterId > 0 Then
                StrQ = "INSERT INTO hrassessor_tran " & _
                       "(assessormasterunkid,stationunkid,departmentunkid,employeeunkid,isvoid,voiddatetime,voiduserunkid,voidreason,visibletypeid) " & _
                       "SELECT '" & iAssessorMasterId & "', 0, '" & iDepartmentId & "', '" & iAccessEmpId & "','False',NULL,-1,'','" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' " & _
                       "WHERE NOT EXISTS (SELECT assessortranunkid FROM hrassessor_tran WHERE assessormasterunkid = '" & iAssessorMasterId & "' AND employeeunkid = '" & iAccessEmpId & "');SELECT @@IDENTITY; "

                dsList = objDataOperation.ExecQuery(StrQ, "lst")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassessor_tran", "assessortranunkid", dsList.Tables("lst").Rows(0).Item(0)) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER ACCESS */--/* END */

final:      If iRefValue = -1 Then iRefValue = 1

            Return iRefValue
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Import_Assesor_Reviewe_Access", mstrModuleName)
            iMessage = ex.Message
            Return iRefValue
        Finally
            objDataOperation = Nothing
        End Try
    End Function
    'Shani(01-MAR-2016) -- End


    'Shani(01-MAR-2016) -- Start
    'Enhancement :PA External Approver Flow
    'Public Function isUsed(ByVal IsAssessor As Boolean, ByVal iEmplId As Integer, ByVal iAseRevEmpId As Integer) As Boolean
    Public Function isUsed(ByVal IsAssessor As Boolean, ByVal iEmplId As Integer, ByVal iAseRevMstId As Integer) As Boolean
        'Shani(01-MAR-2016)-- End

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iCnt As Integer = -1
        Try
            Dim objDataOperation As New clsDataOperation

            'S.SANDEEP [20 Jan 2016] -- START
            'If IsAssessor = True Then
            '    StrQ = "SELECT 1 FROM hrassess_analysis_master WHERE assessedemployeeunkid = '" & iEmplId & "' AND assessoremployeeunkid = '" & iAseRevEmpId & "' "
            'Else
            '    StrQ = "SELECT 1 FROM hrassess_analysis_master WHERE assessedemployeeunkid = '" & iEmplId & "' AND reviewerunkid = '" & iAseRevEmpId & "' "
            'End If

            'Shani(01-MAR-2016) -- Start
            'Enhancement :PA External Approver Flow
            'If IsAssessor = True Then
            '    StrQ = "SELECT 1 FROM hrevaluation_analysis_master WHERE assessedemployeeunkid = '" & iEmplId & "' AND assessoremployeeunkid = '" & iAseRevEmpId & "' "
            'Else
            '    StrQ = "SELECT 1 FROM hrevaluation_analysis_master WHERE assessedemployeeunkid = '" & iEmplId & "' AND reviewerunkid = '" & iAseRevEmpId & "' "
            'End If
            StrQ = "SELECT 1 FROM hrevaluation_analysis_master WHERE assessedemployeeunkid = '" & iEmplId & "' AND assessormasterunkid = '" & iAseRevMstId & "' "
            'Shani(01-MAR-2016)-- End

            'S.SANDEEP [20 Jan 2016] -- END



            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt > 0 Then
                Return True
            End If

            'S.SANDEEP [20 Jan 2016] -- START
            'If IsAssessor = True Then
            '    StrQ = "SELECT 1 FROM hrbsc_analysis_master WHERE assessedemployeeunkid = '" & iEmplId & "' AND assessoremployeeunkid = '" & iAseRevEmpId & "' "
            'Else
            '    StrQ = "SELECT 1 FROM hrbsc_analysis_master WHERE assessedemployeeunkid = '" & iEmplId & "' AND reviewerunkid = '" & iAseRevEmpId & "' "
            'End If

            'iCnt = objDataOperation.RecordCount(StrQ)

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'If iCnt > 0 Then
            '    Return True
            'End If
            'S.SANDEEP [20 Jan 2016] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isUsed", mstrModuleName)
        Finally
        End Try
    End Function

    'Shani(01-MAR-2016) -- Start
    'Enhancement :PA External Approver Flow
    'Public Function IsUserMapped(ByVal iAssessor As Boolean, ByVal iUserId As Integer, ByRef sMsg As String) As Boolean
    Public Function IsUserMapped(ByVal iAssessor As Boolean, ByVal iUserId As Integer, ByRef sMsg As String, ByVal blnIsExternalApprover As Boolean, ByVal enARVisibilityId As clsAssessor.enARVisibilityTypeId) As Boolean
        'Shani(01-MAR-2016)-- End
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Dim objData As New clsDataOperation

            'Shani (29-Nov-2016) -- Start
            'Enhancement - 



            'StrQ = "SELECT " & _
            '       "	 CASE WHEN isexternalapprover = 0 THEN hremployee_master.employeecode ELSE '' END  AS ECode " & _
            '       "	,CASE WHEN isexternalapprover = 0 THEN hremployee_master.firstname+' '+hremployee_master.surname ELSE cfuser_master.username END  AS EName " & _
            '       "    ,isexternalapprover " & _
            '       "FROM hrapprover_usermapping " & _
            '       "	JOIN hrassessor_master ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
            '       "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
            '       "	JOIN hrmsConfiguration..cfuser_master ON hrassessor_master.employeeunkid = cfuser_master.userunkid " & _
            '       "WHERE usertypeid = '" & enUserType.Assessor & "' AND hrapprover_usermapping.userunkid = '" & iUserId & "' " & _
            '       " AND hrassessor_master.isvoid = 0 " 'S.SANDEEP [ 18 NOV 2014 ] -- START {hrassessor_master.isvoid = 0} -- END

            'S.SANDEEP [22-JUN-2017] -- START
            'StrQ = "SELECT " & _
            '       "	 hremployee_master.employeecode AS ECode " & _
            '       "	,hremployee_master.firstname+' '+hremployee_master.surname AS EName " & _
            '       "    ,isexternalapprover " & _
            '       "FROM hrapprover_usermapping " & _
            '       "	JOIN hrassessor_master ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
            '       "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
            '       "WHERE usertypeid = '" & enUserType.Assessor & "' AND hrapprover_usermapping.userunkid = '" & iUserId & "' " & _
            '       "    AND hrassessor_master.isvoid = 0 AND isexternalapprover = 0 AND isreviewer = " & IIf(iAssessor, 0, 1) & " " & _            
            '       "UNION ALL " & _
            '       "SELECT " & _
            '       "	 '' AS ECode " & _
            '       "	,cfuser_master.username AS EName " & _
            '       "    ,isexternalapprover " & _
            '       "FROM hrapprover_usermapping " & _
            '       "	JOIN hrassessor_master ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
            '       "	JOIN hrmsConfiguration..cfuser_master ON hrassessor_master.employeeunkid = cfuser_master.userunkid " & _
            '       "WHERE usertypeid = '" & enUserType.Assessor & "' AND hrapprover_usermapping.userunkid = '" & iUserId & "' " & _
            '       " AND hrassessor_master.isvoid = 0 AND isexternalapprover = 1 AND isreviewer = " & IIf(iAssessor, 0, 1) & " "

            StrQ = "SELECT " & _
                   "	 hremployee_master.employeecode AS ECode " & _
                   "	,hremployee_master.firstname+' '+hremployee_master.surname AS EName " & _
                   "    ,isexternalapprover " & _
                   "FROM hrapprover_usermapping " & _
                   "	JOIN hrassessor_master ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                   "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
                   "WHERE usertypeid = '" & enUserType.Assessor & "' AND hrapprover_usermapping.userunkid = '" & iUserId & "' " & _
                   "    AND hrassessor_master.isvoid = 0 AND isexternalapprover = 0 AND isreviewer = " & IIf(iAssessor, 0, 1) & " " & _
                   "    AND hrassessor_master.visibletypeid = @visibletypeid " & _
                   "UNION ALL " & _
                   "SELECT " & _
                   "	 '' AS ECode " & _
                   "	,cfuser_master.username AS EName " & _
                   "    ,isexternalapprover " & _
                   "FROM hrapprover_usermapping " & _
                   "	JOIN hrassessor_master ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                   "	JOIN hrmsConfiguration..cfuser_master ON hrassessor_master.employeeunkid = cfuser_master.userunkid " & _
                   "WHERE usertypeid = '" & enUserType.Assessor & "' AND hrapprover_usermapping.userunkid = '" & iUserId & "' " & _
                   " AND hrassessor_master.isvoid = 0 AND isexternalapprover = 1 AND isreviewer = " & IIf(iAssessor, 0, 1) & " " & _
                   "    AND hrassessor_master.visibletypeid = @visibletypeid "
            'S.SANDEEP [22-JUN-2017] -- END
            

            'Shani (29-Nov-2016) -- End
            'If iAssessor = True Then
            '    StrQ &= " AND isreviewer = 0 "
            'Else
            '    StrQ &= " AND isreviewer = 1 "
            'End If

            'S.SANDEEP [22-JUN-2017] -- START
            objData.AddParameter("@visibletypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, enARVisibilityId)
            'S.SANDEEP [22-JUN-2017] -- END

            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then


                'Shani(01-MAR-2016) -- Start
                'Enhancement :PA External Approver Flow
                'sMsg = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot map selected user with this employee." & vbCrLf & _
                '                           "Reason user is already mapped with [ Code : ") & dsList.Tables(0).Rows(0).Item("ECode") & _
                'Language.getMessage(mstrModuleName, 2, " Employee : ") & dsList.Tables(0).Rows(0).Item("EName") & Language.getMessage(mstrModuleName, 3, " ]. Please select new user to map.")
                ''If blnIsExternalApprover = False Then
                ''    sMsg = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot map selected user with this employee." & vbCrLf & _
                ''                           "Reason user is already mapped with [ Code : ") & dsList.Tables(0).Rows(0).Item("ECode") & _
                ''                           Language.getMessage(mstrModuleName, 2, " Employee : ") & dsList.Tables(0).Rows(0).Item("EName") & Language.getMessage(mstrModuleName, 3, " ]. Please select new user to map.")
                ''Else
                ''    Dim StrCaption As String = ""
                ''    If iAssessor = True Then
                ''        StrCaption = Language.getMessage(mstrModuleName, 8, "assessor")
                ''    Else
                ''        StrCaption = Language.getMessage(mstrModuleName, 9, "reviewer")
                ''    End If

                ''    sMsg = Language.getMessage(mstrModuleName, 10, "Sorry, you cannot make selected user as") & " " & StrCaption & " " & Language.getMessage(mstrModuleName, 11, ".Reason selected user is already mappeed as ") & " " & StrCaption & " " & _
                ''           Language.getMessage(mstrModuleName, 12, " with [ Code : ") & dsList.Tables(0).Rows(0).Item("ECode") & _
                ''           Language.getMessage(mstrModuleName, 2, " Employee : ") & dsList.Tables(0).Rows(0).Item("EName") & Language.getMessage(mstrModuleName, 13, " ]. Please select new user.")
                ''End If

                If CBool(dsList.Tables(0).Rows(0)("isexternalapprover")) = False Then
                    sMsg = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot map selected user with this employee." & vbCrLf & _
                                               "Reason user is already mapped with [ Code : ") & dsList.Tables(0).Rows(0).Item("ECode") & _
                    Language.getMessage(mstrModuleName, 2, " Employee : ") & dsList.Tables(0).Rows(0).Item("EName") & Language.getMessage(mstrModuleName, 3, " ]. Please select new user to map.")
                Else
                    Dim StrCaption As String = ""
                    If iAssessor = True Then
                        StrCaption = Language.getMessage(mstrModuleName, 8, "assessor")
                    Else
                        StrCaption = Language.getMessage(mstrModuleName, 9, "reviewer")
                    End If

                    sMsg = Language.getMessage(mstrModuleName, 10, "Sorry, you cannot make selected user as") & " " & StrCaption & " " & Language.getMessage(mstrModuleName, 11, ".Reason selected user is already mappeed as ") & " " & StrCaption & " " & _
                           Language.getMessage(mstrModuleName, 12, " with [ username : ") & dsList.Tables(0).Rows(0).Item("EName") & Language.getMessage(mstrModuleName, 13, " ]. Please select new user.")
                End If


                'Shani(01-MAR-2016)-- End

                Return True
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsUserMapped", mstrModuleName)
            Return True
        Finally
        End Try
    End Function

    'S.SANDEEP [27 DEC 2016] -- START
    'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
    Public Function PerformOperationType(ByVal eOprType As enOperationType, _
                                         ByVal eAssessMode As enAssessmentMode, _
                                         ByVal intFormARUnkid As Integer, _
                                         ByVal intEmployeeUnkid As Integer, _
                                         ByVal objDataOperation As clsDataOperation, _
                                         ByVal iToARUnkid As Integer, ByVal iToAREmpId As Integer, _
                                         ByVal iUserId As Integer, _
                                         ByVal strWebfrmName As String, _
                                         ByVal iblnIsAssessor As Boolean) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objEvalMst As New clsevaluation_analysis_master
        Try
            StrQ = "SELECT " & _
                   "     EM.analysisunkid " & _
                   "    ,EM.periodunkid " & _
                   "FROM hrevaluation_analysis_master AS EM WITH (NOLOCK) " & _
                   "    JOIN cfcommon_period_tran AS CP WITH (NOLOCK) ON CP.periodunkid = EM.periodunkid " & _
                   "WHERE EM.isvoid = 0 AND CP.isactive = 1 AND CP.statusid = 1 AND EM.assessormasterunkid = @assessormasterunkid " & _
                   "AND EM.assessedemployeeunkid = @assessedemployeeunkid AND EM.assessmodeid = @assessmodeid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormARUnkid)
            objDataOperation.AddParameter("@assessedemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkid)
            objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAssessMode)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables("List").Rows
                    objDataOperation.ClearParameters()
                    Select Case eOprType
                        Case enOperationType.Overwrite
                            objEvalMst._Analysisunkid(objDataOperation) = dRow.Item("analysisunkid")
                            objEvalMst._Assessormasterunkid = iToARUnkid
                            If eAssessMode = enAssessmentMode.APPRAISER_ASSESSMENT Then
                                objEvalMst._Assessoremployeeunkid = iToAREmpId
                            ElseIf eAssessMode = enAssessmentMode.REVIEWER_ASSESSMENT Then
                                objEvalMst._Reviewerunkid = iToAREmpId
                            End If

                            'S.SANDEEP [27-APR-2017] -- START
                            'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                            'If objEvalMst.Update(Nothing, Nothing, Nothing, objDataOperation) = False Then

                            'S.SANDEEP |01-JAN-2020| -- START
                            'ISSUE/ENHANCEMENT : PREVINTING COMPUTATION VOID FROM MIGRATION IF OPERATION TYPE IS OVERWRITE EXISTING ASSESSOR
                            'If objEvalMst.Update(Nothing, Nothing, Nothing, 0, Nothing, False, False, objDataOperation) = False Then
                            If objEvalMst.Update(Nothing, Nothing, Nothing, 0, Nothing, False, False, objDataOperation, False) = False Then
                                'S.SANDEEP |01-JAN-2020| -- END

                                'S.SANDEEP [27-APR-2017] -- END
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        Case enOperationType.Void
                            objEvalMst._Isvoid = True
                            objEvalMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            If iblnIsAssessor = True Then
                                objEvalMst._Voidreason = Language.getMessage(mstrModuleName, 5, "Migrated to other Assessor.")
                            Else
                                objEvalMst._Voidreason = Language.getMessage(mstrModuleName, 6, "Migrated to other Reviewer.")
                            End If
                            objEvalMst._Voiduserunkid = iUserId
                            objEvalMst._WebFrmName = strWebfrmName
                            If objEvalMst.Delete(dRow.Item("analysisunkid"), eAssessMode, dRow.Item("periodunkid"), objDataOperation) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                    End Select
                Next
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: PerformOperationType; Module Name: " & mstrModuleName)
        Finally
            objEvalMst = Nothing
        End Try
    End Function
    'Public Function Perform_Migration(ByVal iFromARUnkid As Integer, _
    '                                  ByVal mDtTable As DataTable, _
    '                                  ByVal iToARUnkid As Integer, _
    '                                  ByVal iblnIsAssessor As Boolean, _
    '                                  ByVal iFrmAREmpId As Integer, _
    '                                  ByVal iUserId As Integer, _
    '                                  ByVal blnExternalAssessor As Boolean, _
    '                                  ByVal strEmployeeAsOnDate As String) As Boolean

    '    'Shani (07-Dec-2016) -- [strEmployeeAsOnDate]


    '    'Shani(01-MAR-2016) -- [blnExternal]
    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Dim iblnVoidMaster As Boolean = False
    '    Dim objDataOperation As New clsDataOperation
    '    Dim mDtOld As DataTable
    '    Dim iMigratedEmpId As String = String.Empty
    '    Try

    '        'Shani(29-Nov-2016) -- Start
    '        'Issue :  Fix for TRA Assessor Migration issue. 
    '        'Shani(01-MAR-2016) -- Start
    '        'Enhancement :PA External Approver Flow
    '        '_AssessorEmployeeId = iFrmAREmpId
    '        Dim evalue As clsAssessor.enAssessorType = clsAssessor.enAssessorType.ALL_DATA
    '        If iblnIsAssessor Then
    '            evalue = clsAssessor.enAssessorType.ASSESSOR
    '        Else
    '            evalue = clsAssessor.enAssessorType.REVIEWER
    '        End If
    '        _AssessorEmployeeId(blnExternalAssessor, evalue) = iFrmAREmpId
    '        'Shani(01-MAR-2016)-- End
    '        'Shani(29-Nov-2016)-- End

    '        If iblnIsAssessor = True Then
    '            mDtOld = New DataView(mdtTran, "isreviewer = 0", "", DataViewRowState.CurrentRows).ToTable.Copy
    '        Else
    '            mDtOld = New DataView(mdtTran, "isreviewer = 1", "", DataViewRowState.CurrentRows).ToTable.Copy
    '        End If

    '        If mDtOld.Rows.Count = mDtTable.Rows.Count Then iblnVoidMaster = True

    '        objDataOperation.BindTransaction()

    '        For Each dRow As DataRow In mDtTable.Rows
    '            mdtTran.Rows.Clear()
    '            With dRow
    '                .Item("AUD") = "D"
    '                .Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
    '                .Item("isvoid") = True
    '                If iblnIsAssessor = True Then
    '                    .Item("voidreason") = Language.getMessage(mstrModuleName, 5, "Migrated to other Assessor.")
    '                Else
    '                    .Item("voidreason") = Language.getMessage(mstrModuleName, 6, "Migrated to other Reviewer.")
    '                End If
    '                .Item("voiduserunkid") = iUserId
    '            End With
    '            mdtTran.ImportRow(dRow)

    '            If Insert_Update_Delete(objDataOperation, iFromARUnkid) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            mdtTran.Rows.Clear()
    '            With dRow
    '                .Item("AUD") = "A"
    '                .Item("voiddatetime") = DBNull.Value
    '                .Item("isvoid") = False
    '                .Item("voidreason") = ""
    '                .Item("voiduserunkid") = 0
    '            End With
    '            mdtTran.ImportRow(dRow)

    '            If Insert_Update_Delete(objDataOperation, iToARUnkid) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If
    '            iMigratedEmpId &= "," & dRow.Item("employeeunkid")
    '        Next
    '        If iblnVoidMaster = True Then
    '            Dim objAssessor As New clsAssessor
    '            objAssessor._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '            objAssessor._Voidreason = Language.getMessage(mstrModuleName, 7, "Employee Migrated")
    '            objAssessor._Voiduserunkid = iUserId
    '            objAssessor._Isvoid = True
    '            If objAssessor.Delete(iFromARUnkid, objDataOperation) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If
    '            objAssessor = Nothing
    '        End If

    '        If iMigratedEmpId.Trim.Length > 0 Then
    '            iMigratedEmpId = Mid(iMigratedEmpId, 2)
    '            Dim objMigration As New clsMigration

    '            objMigration._Migratedemployees = iMigratedEmpId
    '            objMigration._Migrationdate = ConfigParameter._Object._CurrentDateAndTime
    '            objMigration._Migrationfromunkid = iFromARUnkid
    '            objMigration._Migrationtounkid = iToARUnkid
    '            objMigration._Usertypeid = enUserType.Assessor
    '            objMigration._Userunkid = iUserId

    '            If objMigration.Insert(objDataOperation) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            objMigration = Nothing
    '        End If

    '        objDataOperation.ReleaseTransaction(True)
    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        DisplayError.Show("-1", ex.Message, "Perform_Migration", mstrModuleName)
    '    Finally
    '    End Try
    'End Function
    Public Function Perform_Migration(ByVal iFromARUnkid As Integer, _
                                      ByVal mDtTable As DataTable, _
                                      ByVal iToARUnkid As Integer, _
                                      ByVal iblnIsAssessor As Boolean, _
                                      ByVal iFrmAREmpId As Integer, _
                                      ByVal iUserId As Integer, _
                                      ByVal blnExternalAssessor As Boolean, _
                                      ByVal strEmployeeAsOnDate As String, _
                                      ByVal eOprType As enOperationType, _
                                      ByVal eAssessMode As enAssessmentMode, _
                                      ByVal iToArEmpId As Integer, _
                                      ByVal strWebfrmName As String, _
                                      Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean


        'Gajanan [23-SEP-2019] -- Add {xDataOpr}    
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iblnVoidMaster As Boolean = False


        'Gajanan [23-SEP-2019] -- Start    
        'Enhancement:Enforcement of approval migration From Transfer and recategorization.
        'Dim objDataOperation As New clsDataOperation
        Dim objDataOperation As clsDataOperation
        'Gajanan [23-SEP-2019] -- End

        Dim mDtOld As DataTable
        Dim iMigratedEmpId As String = String.Empty
        Try

            Dim evalue As clsAssessor.enAssessorType = clsAssessor.enAssessorType.ALL_DATA
            If iblnIsAssessor Then
                evalue = clsAssessor.enAssessorType.ASSESSOR
            Else
                evalue = clsAssessor.enAssessorType.REVIEWER
            End If
            _AssessorEmployeeId(blnExternalAssessor, evalue) = iFrmAREmpId

            If iblnIsAssessor = True Then
                mDtOld = New DataView(mdtTran, "isreviewer = 0", "", DataViewRowState.CurrentRows).ToTable.Copy
            Else
                mDtOld = New DataView(mdtTran, "isreviewer = 1", "", DataViewRowState.CurrentRows).ToTable.Copy
            End If

            If mDtOld.Rows.Count = mDtTable.Rows.Count Then iblnVoidMaster = True


            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'objDataOperation.BindTransaction()
            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            End If
            'Gajanan [23-SEP-2019] -- End


            For Each dRow As DataRow In mDtTable.Rows
                mdtTran.Rows.Clear()
                With dRow
                    .Item("AUD") = "U"
                    .Item("visibletypeid") = clsAssessor.enARVisibilityTypeId.MIGRATE


                    'Gajanan [23-SEP-2019] -- Start    
                    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                    If CInt(.Item("assessormasterunkid")) <= 0 Then
                        .Item("assessormasterunkid") = iFromARUnkid
                    End If
                    'Gajanan [23-SEP-2019] -- End

                End With
                mdtTran.ImportRow(dRow)

                If Insert_Update_Delete(objDataOperation, iFromARUnkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If PerformOperationType(eOprType, eAssessMode, iFromARUnkid, dRow.Item("employeeunkid"), objDataOperation, iToARUnkid, iToArEmpId, iUserId, strWebfrmName, iblnIsAssessor) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mdtTran.Rows.Clear()
                With dRow
                    .Item("AUD") = "A"
                    .Item("visibletypeid") = clsAssessor.enARVisibilityTypeId.VISIBLE
                End With
                mdtTran.ImportRow(dRow)

                If Insert_Update_Delete(objDataOperation, iToARUnkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                iMigratedEmpId &= "," & dRow.Item("employeeunkid")
            Next
            If iblnVoidMaster = True Then
                StrQ = "UPDATE hrassessor_master SET visibletypeid = '" & clsAssessor.enARVisibilityTypeId.MIGRATE & "' WHERE assessormasterunkid = '" & iFromARUnkid & "' "

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrassessor_master", iFromARUnkid, "assessormasterunkid", 2) Then
                    If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassessor_master", "assessormasterunkid", iFromARUnkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

            End If

            If iMigratedEmpId.Trim.Length > 0 Then
                iMigratedEmpId = Mid(iMigratedEmpId, 2)
                Dim objMigration As New clsMigration

                objMigration._Migratedemployees = iMigratedEmpId
                objMigration._Migrationdate = ConfigParameter._Object._CurrentDateAndTime
                objMigration._Migrationfromunkid = iFromARUnkid
                objMigration._Migrationtounkid = iToARUnkid
                objMigration._Usertypeid = enUserType.Assessor
                objMigration._Userunkid = iUserId

                If objMigration.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objMigration = Nothing
            End If

            StrQ = ""


            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'Gajanan [23-SEP-2019] -- End

            Return True
        Catch ex As Exception

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'objDataOperation.ReleaseTransaction(False)
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan [23-SEP-2019] -- End

            DisplayError.Show("-1", ex.Message, "Perform_Migration", mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [27 DEC 2016] -- END



    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
    Public Function CheckEmployeeInsert(ByVal intEmpId As Integer, ByVal blnIsReviewer As Boolean, ByVal intAREmpId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As DataSet = Nothing
        Try
            StrQ = "SELECT " & _
                   "  AM.employeeunkid " & _
                   "FROM hrassessor_tran AS AT " & _
                   "  JOIN hrassessor_master AS AM ON AM.assessormasterunkid = AT.assessormasterunkid " & _
                   "WHERE AT.isvoid = 0 AND AT.visibletypeid = 1 AND AM.isvoid = 0 AND AM.visibletypeid = 1 " & _
                   "AND AT.employeeunkid = '" & intEmpId & "' "

            'S.SANDEEP [16-NOV-2018] -- START
            'If blnIsReviewer = False Then
            '    StrQ = " AND isreviewer = 1 "
            'Else
            '    StrQ = " AND isreviewer = 0 "
            'End If

            If blnIsReviewer = False Then
                StrQ &= " AND isreviewer = 1 "
            Else
                StrQ &= " AND isreviewer = 0 "
            End If
            'S.SANDEEP [16-NOV-2018] -- END

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            Dim intExistEmpId As Integer = 0
            If dsList.Tables(0).Rows.Count > 0 Then
                intExistEmpId = dsList.Tables(0).Rows(0)("employeeunkid")
            End If

            If intExistEmpId = intAREmpId Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CheckEmployeeInsert; Module Name: " & mstrModuleName)
            Return True
        Finally
        End Try
    End Function
    'S.SANDEEP [01-OCT-2018] -- END


    'Shani(01-MAR-2016) -- Start
    'Enhancement :PA External Approver Flow
    Public Function CheckEmployeeInsert(ByVal intEmpId As Integer, ByVal blnIsReviewer As Boolean) As Boolean
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As DataSet = Nothing
        Try
            StrQ = "SELECT " & _
                   "    hrassessor_tran.employeeunkid " & _
                   "FROM hrassessor_tran " & _
                   "    JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid AND hrassessor_master.isvoid = 0 " & _
                   "WHERE hrassessor_tran.isvoid = 0  " & _
                   "    AND hrassessor_tran.employeeunkid = '" & intEmpId & "' " & _
                   "    AND hrassessor_master.isreviewer = @isreviewer "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isreviewer", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsReviewer)

            dsList = objDataOperation.ExecQuery(StrQ, "List")
            If dsList.Tables("List").Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckEmployeeInsert", mstrModuleName)
            Return True
        End Try
    End Function
    'Shani(01-MAR-2016) -- End


    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
    Public Function GetEmployeeAssessorReviewer(ByVal mintApproverType As enApprFilterTypeId, ByVal intEmployeeId As Integer, ByVal xPeriodStart As DateTime, ByVal xPeriodEnd As DateTime, ByVal xDatabaseName As String) As DataSet

        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        Try

            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()

            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)


            Dim strFinalQ As String = ""
            Dim strAllcJoin As String = ""
            Dim dTemp As DataSet = Nothing



            strAllcJoin = "LEFT JOIN " & _
                               "( " & _
                               "    SELECT " & _
                               "         departmentunkid " & _
                               "        ,employeeunkid " & _
                               "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                          "    FROM #DBNAME#hremployee_transfer_tran " & _
                               "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                               ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                          "JOIN #DBNAME#hrdepartment_master ON Alloc.departmentunkid = hrdepartment_master.departmentunkid " & _
                               "LEFT JOIN " & _
                               "( " & _
                               "    SELECT " & _
                               "         jobunkid " & _
                               "        ,jobgroupunkid " & _
                               "        ,employeeunkid " & _
                               "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                          "    FROM #DBNAME#hremployee_categorization_tran " & _
                               "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                               ") AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                          "JOIN #DBNAME#hrjob_master ON Jobs.jobunkid = hrjob_master.jobunkid "


            Select Case mintApproverType


                Case enApprFilterTypeId.APRL_ASSESSOR



                    strFinalQ = "SELECT " & _
                                "    assessormasterunkid " & _
                                "    ,employeeunkid " & _
                                "    ,Employee " & _
                                "    ,Department " & _
                                "    ,Job " & _
                                "    ,@Assessor as ApproverType " & _
                                "    ,isexternalapprover " & _
                                "    ,isreviewer " & _
                                "FROM " & _
                                "    ( "

                    StrQ = "            SELECT " & _
                           "                 #ASS_CODE# AS [Employee Code] " & _
                           "                ,#ASS_NAME# AS Employee " & _
                           "                ,#ASS_DEPT# AS Department " & _
                           "                ,#ASS_JOB# AS Job " & _
                           "                ,hrassessor_master.assessormasterunkid " & _
                           "                ,hrassessor_master.isexternalapprover " & _
                           "                ,hrassessor_master.employeeunkid " & _
                           "                ,hrassessor_master.isreviewer " & _
                           "            FROM hrassessor_master " & _
                           "                JOIN hrassessor_tran ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
                           "                #ASS_JOIN# " & _
                           "                #ASS_ALLC_JOIN# " & _
                           "            WHERE hrassessor_tran.employeeunkid = @employeeunkid AND hrassessor_master.isvoid = 0 AND " & _
                           "                isreviewer = 0 AND hrassessor_tran.isvoid = 0 AND hrassessor_master.isexternalapprover = #EXVAL# AND hrassessor_master.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' "

                    StrQ &= " AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' "

                    strFinalQ &= StrQ
                    strFinalQ = strFinalQ.Replace("#ASS_CODE#", "hremployee_master.employeecode")
                    strFinalQ = strFinalQ.Replace("#ASS_NAME#", "hremployee_master.firstname+' '+ hremployee_master.surname")
                    strFinalQ = strFinalQ.Replace("#ASS_DEPT#", "hrdepartment_master.name")
                    strFinalQ = strFinalQ.Replace("#ASS_JOB#", "hrjob_master.job_name")
                    strFinalQ = strFinalQ.Replace("#ASS_JOIN#", "JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid ")
                    strFinalQ = strFinalQ.Replace("#ASS_ALLC_JOIN#", strAllcJoin)
                    strFinalQ = strFinalQ.Replace("#EXVAL#", "0")
                    strFinalQ = strFinalQ.Replace("#DBNAME#", "")

                    dTemp = (New clsAssessor).GetExternalAssessorReviewerList(objDataOperation, "List", , , intEmployeeId, , clsAssessor.enAssessorType.ASSESSOR)

                    For Each dRow As DataRow In dTemp.Tables(0).Rows
                        strFinalQ &= " UNION ALL " & StrQ
                        If dRow.Item("companyunkid") <= 0 AndAlso dRow.Item("DBName").ToString.Trim.Length <= 0 Then
                            strFinalQ = strFinalQ.Replace("#ASS_CODE#", "''")
                            strFinalQ = strFinalQ.Replace("#ASS_NAME#", "cfuser_master.username")
                            strFinalQ = strFinalQ.Replace("#ASS_DEPT#", "''")
                            strFinalQ = strFinalQ.Replace("#ASS_JOB#", "''")
                            strFinalQ = strFinalQ.Replace("#ASS_JOIN#", "JOIN hrmsConfiguration..cfuser_master ON hrassessor_master.employeeunkid = cfuser_master.userunkid ")
                            strFinalQ = strFinalQ.Replace("#ASS_ALLC_JOIN#", "")
                            strFinalQ = strFinalQ.Replace("#EXVAL#", "1 AND cfuser_master.companyunkid = '" & dRow.Item("companyunkid") & "' ")
                            strFinalQ = strFinalQ.Replace("#DBNAME#", "")
                        Else
                            strFinalQ = strFinalQ.Replace("#ASS_CODE#", "hremployee_master.employeecode")
                            strFinalQ = strFinalQ.Replace("#ASS_NAME#", "hremployee_master.firstname+' '+ hremployee_master.surname")
                            strFinalQ = strFinalQ.Replace("#ASS_DEPT#", "hrdepartment_master.name")
                            strFinalQ = strFinalQ.Replace("#ASS_JOB#", "hrjob_master.job_name")
                            strFinalQ = strFinalQ.Replace("#ASS_JOIN#", "JOIN hrmsConfiguration..cfuser_master ON hrassessor_master.employeeunkid = cfuser_master.userunkid " & _
                                                                        "JOIN #DBNAME#hremployee_master ON cfuser_master.employeeunkid = hremployee_master.employeeunkid ")
                            strFinalQ = strFinalQ.Replace("#ASS_ALLC_JOIN#", strAllcJoin)
                            strFinalQ = strFinalQ.Replace("#EXVAL#", "1 AND cfuser_master.companyunkid = '" & dRow.Item("companyunkid") & "' ")
                            strFinalQ = strFinalQ.Replace("#DBNAME#", dRow.Item("DBName") & "..")
                        End If
                    Next


                Case enApprFilterTypeId.APRL_REVIEWER

                    strFinalQ = "SELECT " & _
                                "    assessormasterunkid " & _
                                "    ,employeeunkid " & _
                                "    ,Employee " & _
                                "    ,Department " & _
                                "    ,Job " & _
                                "    ,@Reviewer as ApproverType " & _
                                "    ,isexternalapprover " & _
                                "    ,isreviewer " & _
                                "FROM " & _
                                "    ( "

                    StrQ = "SELECT " & _
                           "     #REV_CODE# AS [Employee Code] " & _
                           "    ,#REV_NAME# AS Employee " & _
                           "    ,#REV_DEPT# AS Department " & _
                           "    ,#REV_JOB# AS Job " & _
                           "    ,hrassessor_master.assessormasterunkid " & _
                           "    ,hrassessor_master.isexternalapprover " & _
                           "    ,hrassessor_master.employeeunkid " & _
                           "    ,hrassessor_master.isreviewer " & _
                           "    FROM hrassessor_master " & _
                           "    JOIN hrassessor_tran ON hrassessor_master.assessormasterunkid = hrassessor_tran.assessormasterunkid " & _
                           "    #REV_JOIN# " & _
                           "    #REV_ALLC_JOIN# " & _
                           "WHERE hrassessor_tran.employeeunkid = @employeeunkid AND hrassessor_master.isvoid = 0 AND isreviewer = 1 " & _
                           "    AND hrassessor_tran.isvoid = 0 AND hrassessor_master.isexternalapprover = #EXVAL# AND hrassessor_master.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' "

                    StrQ &= " AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' "


                    strFinalQ &= StrQ
                    strFinalQ = strFinalQ.Replace("#REV_CODE#", "hremployee_master.employeecode")
                    strFinalQ = strFinalQ.Replace("#REV_NAME#", "hremployee_master.firstname+' '+ hremployee_master.surname")
                    strFinalQ = strFinalQ.Replace("#REV_DEPT#", "hrdepartment_master.name")
                    strFinalQ = strFinalQ.Replace("#REV_JOB#", "hrjob_master.job_name")
                    strFinalQ = strFinalQ.Replace("#REV_JOIN#", "JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid ")
                    strFinalQ = strFinalQ.Replace("#REV_ALLC_JOIN#", strAllcJoin)
                    strFinalQ = strFinalQ.Replace("#EXVAL#", "0")
                    strFinalQ = strFinalQ.Replace("#DBNAME#", "")

                    dTemp = (New clsAssessor).GetExternalAssessorReviewerList(objDataOperation, "List", , , intEmployeeId, , clsAssessor.enAssessorType.REVIEWER)

                    For Each dRow As DataRow In dTemp.Tables(0).Rows
                        strFinalQ &= " UNION ALL " & StrQ
                        If dRow.Item("companyunkid") <= 0 AndAlso dRow.Item("DBName").ToString.Trim.Length <= 0 Then
                            strFinalQ = strFinalQ.Replace("#REV_CODE#", "''")
                            strFinalQ = strFinalQ.Replace("#REV_NAME#", "cfuser_master.username")
                            strFinalQ = strFinalQ.Replace("#REV_DEPT#", "''")
                            strFinalQ = strFinalQ.Replace("#REV_JOB#", "''")
                            strFinalQ = strFinalQ.Replace("#REV_JOIN#", "JOIN hrmsConfiguration..cfuser_master ON hrassessor_master.employeeunkid = cfuser_master.userunkid ")
                            strFinalQ = strFinalQ.Replace("#REV_ALLC_JOIN#", "")
                            strFinalQ = strFinalQ.Replace("#EXVAL#", "1 AND cfuser_master.companyunkid = '" & dRow.Item("companyunkid") & "' ")
                            strFinalQ = strFinalQ.Replace("#DBNAME#", "")
                        Else
                            strFinalQ = strFinalQ.Replace("#REV_CODE#", "hremployee_master.employeecode")
                            strFinalQ = strFinalQ.Replace("#REV_NAME#", "hremployee_master.firstname+' '+ hremployee_master.surname")
                            strFinalQ = strFinalQ.Replace("#REV_DEPT#", "hrdepartment_master.name")
                            strFinalQ = strFinalQ.Replace("#REV_JOB#", "hrjob_master.job_name")
                            strFinalQ = strFinalQ.Replace("#REV_JOIN#", "JOIN hrmsConfiguration..cfuser_master ON hrassessor_master.employeeunkid = cfuser_master.userunkid " & _
                                                                        "JOIN #DBNAME#hremployee_master ON cfuser_master.employeeunkid = hremployee_master.employeeunkid ")
                            strFinalQ = strFinalQ.Replace("#REV_ALLC_JOIN#", strAllcJoin)
                            strFinalQ = strFinalQ.Replace("#EXVAL#", "1 AND cfuser_master.companyunkid = '" & dRow.Item("companyunkid") & "' ")
                            strFinalQ = strFinalQ.Replace("#DBNAME#", dRow.Item("DBName") & "..")
                        End If
                    Next
            End Select

            strFinalQ &= ")AS AR"

            objDataOperation.AddParameter("@Assessor", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 37, "Assessor"))
            objDataOperation.AddParameter("@Reviewer", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 38, "Reviewer"))

            StrQ = strFinalQ


            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeReporting", mstrModuleName)
        End Try
        Return dsList
    End Function

    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Public Function Perform_Migration(ByVal intFromARId As Integer, _
                                      ByVal intToARId As Integer, _
                                      ByVal iList As Dictionary(Of Integer, Integer), _
                                      ByVal blnIsAssessor As Boolean, _
                                      ByVal iUserId As Integer, _
                                      ByVal strEmpAsOnDate As String, _
                                      ByVal eType As enOperationType, _
                                      ByVal eMode As enAssessmentMode, _
                                      ByVal strIP As String, _
                                      ByVal strHost As String, _
                                      ByVal strFormName As String, _
                                      Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iblnVoidMaster As Boolean = False
        Dim objDataOperation As clsDataOperation = Nothing
        Dim strMigratedEmpIds As String = String.Empty
        Dim strVoidedATranIds As String = String.Empty
        Dim objAssessorMaster As New clsAssessor
        Try
            If iList.Count > 0 Then
                objAssessorMaster._Assessormasterunkid = intToARId

                strMigratedEmpIds = String.Join(",", iList.Select(Function(x) x.Value.ToString()).ToArray())
                strVoidedATranIds = String.Join(",", iList.Select(Function(x) x.Key.ToString()).ToArray())
                _AssessorMasterId = intFromARId
                If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                    Dim oList As List(Of Integer) = mdtTran.AsEnumerable().Select(Function(x) x.Field(Of Integer)("assessortranunkid")).ToList()
                    If oList.Count = iList.Keys.Count Then iblnVoidMaster = True
                    Dim oTable As DataTable = Nothing
                    oTable = New DataView(mdtTran, "assessortranunkid IN (" & strVoidedATranIds & ")", "", DataViewRowState.CurrentRows).ToTable.Copy()
                    mdtTran.Rows.Clear()
                    If xDataOpr IsNot Nothing Then
                        objDataOperation = xDataOpr
                    Else
                        objDataOperation = New clsDataOperation
                        objDataOperation.BindTransaction()
                    End If

                    For Each iKey As Integer In iList.Keys
                        Dim dtmp() As DataRow = oTable.Select("assessortranunkid = '" & iKey & "'")
                        If dtmp.Length > 0 Then
                            mdtTran.Rows.Clear()
                            dtmp(0).Item("AUD") = "U"
                            dtmp(0).Item("visibletypeid") = clsAssessor.enARVisibilityTypeId.MIGRATE
                            If CInt(dtmp(0).Item("assessormasterunkid")) <= 0 Then
                                dtmp(0).Item("assessormasterunkid") = intFromARId
                            End If
                            mdtTran.ImportRow(dtmp(0))
                            If Insert_Update_Delete(objDataOperation, intFromARId) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            If PerformOperationType(eType, eMode, intFromARId, iList(iKey), objDataOperation, intToARId, objAssessorMaster._EmployeeId, iUserId, strFormName, blnIsAssessor) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                            mdtTran.Rows.Clear()
                            dtmp(0).Item("AUD") = "A"
                            dtmp(0).Item("visibletypeid") = clsAssessor.enARVisibilityTypeId.VISIBLE
                            dtmp(0).Item("assessormasterunkid") = intToARId
                            mdtTran.ImportRow(dtmp(0))

                            If Insert_Update_Delete(objDataOperation, intToARId) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If
                    Next
                    If iblnVoidMaster = True Then
                        StrQ = "UPDATE hrassessor_master SET visibletypeid = '" & clsAssessor.enARVisibilityTypeId.MIGRATE & "' WHERE assessormasterunkid = '" & intFromARId & "' "
                        objDataOperation.ExecNonQuery(StrQ)
                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                        If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrassessor_master", intFromARId, "assessormasterunkid", 2) Then
                            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassessor_master", "assessormasterunkid", intFromARId) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If
                    End If
                End If
                If strMigratedEmpIds.Trim.Length > 0 Then
                    Dim objMigration As New clsMigration
                    objMigration._Migratedemployees = strMigratedEmpIds
                    objMigration._Migrationdate = ConfigParameter._Object._CurrentDateAndTime
                    objMigration._Migrationfromunkid = intFromARId
                    objMigration._Migrationtounkid = intToARId
                    objMigration._Usertypeid = enUserType.Assessor
                    objMigration._Userunkid = iUserId
                    If objMigration.Insert(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objMigration = Nothing
                End If
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(True)
                End If
            End If
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Perform_Migration; Module Name: " & mstrModuleName)
        Finally
            objAssessorMaster = Nothing
        End Try
    End Function
    'S.SANDEEP |18-JAN-2020| -- END

    Public Function GetApproverTranIdFromEmployeeAndApprover(ByVal ApproverId As Integer, ByVal EmployeeId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = " SELECT ISNULL(assessortranunkid,0) as assessortranunkid FROM hrassessor_tran WITH (NOLOCK) WHERE assessormasterunkid = @assessormasterunkid AND employeeunkid = @employeeunkid AND isvoid = 0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, ApproverId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, EmployeeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables("List").Rows(0)("assessortranunkid").ToString())
            End If

            Return 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverTranIdFromEmployeeAndApprover; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Public Function GetApproverData(ByVal ApproverId As Integer, _
    '                                Optional ByVal eValue As clsAssessor.enAssessorType = clsAssessor.enAssessorType.ALL_DATA, _
    '                                Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer

    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Dim dsList As New DataSet
    '    Try
    '        Dim objData As New clsDataOperation

    '        Dim xDateJoinQry, xDataFilter As String
    '        xDateJoinQry = "" : xDataFilter = ""
    '        Aruti.Data.modGlobal.GetDatesFilterString(xDateJoinQry, xDataFilter, eZeeDate.convertDate(strEmpAsOnDate), eZeeDate.convertDate(strEmpAsOnDate), True, False, "", "")

    '        StrQ = "SELECT " & _
    '                "	 hrassessor_tran.assessortranunkid " & _
    '                "   ,hrassessor_tran.assessormasterunkid " & _
    '                "   ,ISNULL(hrassessor_tran.stationunkid,0) AS stationunkid " & _
    '               "    ,ISNULL(hrassessor_tran.departmentunkid,0) AS departmentunkid " & _
    '                "   ,hrassessor_tran.employeeunkid " & _
    '                "   ,hrassessor_tran.isvoid " & _
    '                "   ,hrassessor_tran.voiddatetime " & _
    '                "   ,hrassessor_tran.voiduserunkid " & _
    '                "   ,hrassessor_tran.voidreason " & _
    '                "	,CAST(0 AS BIT) AS ischeck " & _
    '                "	,employeecode " & _
    '                "	,firstname+' '+ surname AS name " & _
    '                "	,hrdepartment_master.name AS Department " & _
    '                "	,job_name AS Job " & _
    '                "	,hremployee_master.employeeunkid " & _
    '                "   ,hrassessor_master.employeeunkid AS arId " & _
    '                "	,isreviewer " & _
    '                "	,'' AS AUD " & _
    '                "   ,hrassessor_tran.visibletypeid " & _
    '                "FROM hrassessor_tran " & _
    '                "	JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
    '                "	JOIN hremployee_master ON hrassessor_tran.employeeunkid = hremployee_master.employeeunkid "
    '        If xDateJoinQry.Trim().Length > 0 Then
    '            StrQ &= " " & xDateJoinQry & " "
    '        End If
    '        StrQ &= "    LEFT JOIN " & _
    '               "    ( " & _
    '               "        SELECT " & _
    '               "             Trf.TrfEmpId " & _
    '               "            ,ISNULL(Trf.departmentunkid,0) AS departmentunkid " & _
    '               "            ,ISNULL(Trf.EfDt,'') AS EfDt " & _
    '               "        FROM " & _
    '               "        ( " & _
    '               "            SELECT " & _
    '               "                 ETT.employeeunkid AS TrfEmpId " & _
    '               "                ,ISNULL(ETT.departmentunkid,0) AS departmentunkid " & _
    '               "                ,CONVERT(CHAR(8),ETT.effectivedate,112) AS EfDt " & _
    '               "                ,ROW_NUMBER()OVER(PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC) AS Rno " & _
    '               "            FROM hremployee_transfer_tran AS ETT " & _
    '               "            WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmpAsOnDate & "' " & _
    '               "        ) AS Trf WHERE Trf.Rno = 1 " & _
    '               "    ) AS ETRF ON ETRF.TrfEmpId = hremployee_master.employeeunkid " & _
    '               "    LEFT JOIN " & _
    '               "    ( " & _
    '               "        SELECT " & _
    '               "             Cat.CatEmpId " & _
    '               "            ,Cat.jobunkid " & _
    '               "            ,Cat.CEfDt " & _
    '               "        FROM " & _
    '               "        ( " & _
    '               "            SELECT " & _
    '               "                 ECT.employeeunkid AS CatEmpId " & _
    '               "                ,ECT.jobunkid " & _
    '               "                ,CONVERT(CHAR(8),ECT.effectivedate,112) AS CEfDt " & _
    '               "                ,ROW_NUMBER()OVER(PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
    '               "            FROM hremployee_categorization_tran AS ECT " & _
    '               "            WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & strEmpAsOnDate & "' " & _
    '               "        ) AS Cat WHERE Cat.Rno = 1 " & _
    '               "    ) AS ERECAT ON ERECAT.CatEmpId = hremployee_master.employeeunkid " & _
    '               "    LEFT JOIN hrjob_master ON ERECAT.jobunkid = hrjob_master.jobunkid AND hrjob_master.isactive = 1 " & _
    '               "    LEFT JOIN hrdepartment_master ON ETRF.departmentunkid = hrdepartment_master.departmentunkid AND hrdepartment_master.isactive = 1 " & _
    '                "WHERE 1 = 1 " & _
    '                "	AND hrassessor_master.employeeunkid = '" & mintAssessorEmployeeId & "' AND hrassessor_master.isexternalapprover = @isexternalapprover " & _
    '                "   AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
    '                "   AND hrassessor_master.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' " & _
    '                "   AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' "

    '        If xDataFilter.Trim().Length > 0 Then
    '            StrQ &= " " & xDataFilter & " "
    '        End If

    '        If eValue = clsAssessor.enAssessorType.ASSESSOR Then
    '            StrQ &= "   AND hrassessor_master.isreviewer = 0 "
    '        ElseIf eValue = clsAssessor.enAssessorType.REVIEWER Then
    '            StrQ &= "   AND hrassessor_master.isreviewer = 1 "
    '        End If

    '        StrQ &= "ORDER BY firstname+' '+ surname "

    '        objData.ClearParameters()
    '        objData.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalAssessor)

    '        dsList = objData.ExecQuery(StrQ, "List")

    '        If objData.ErrorMessage <> "" Then
    '            exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mdtTran.Rows.Clear()

    '        For Each dRow As DataRow In dsList.Tables(0).Rows
    '            mdtTran.ImportRow(dRow)
    '        Next

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    Finally
    '    End Try

    'End Function

    'Gajanan [23-SEP-2019] -- End
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, you cannot map selected user with this employee." & vbCrLf & _
                                                    "Reason user is already mapped with [ Code :")
            Language.setMessage(mstrModuleName, 2, " Employee :")
            Language.setMessage(mstrModuleName, 3, " ]. Please select new user to map.")
            Language.setMessage(mstrModuleName, 4, "Sorry, you cannot do submit for approval operation. Reason : No Assessor assigned to to this employee.")
            Language.setMessage(mstrModuleName, 5, "Migrated to other Assessor.")
            Language.setMessage(mstrModuleName, 6, "Migrated to other Reviewer.")
            Language.setMessage(mstrModuleName, 7, "Employee Migrated")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Public Class clsAssessor_tran

'#Region " Private Variables "

'    Private Shared ReadOnly mstrModuleName As String = "clsAssessor_tran"
'    Dim mstrMessage As String = ""

'#End Region

'#Region " Property Variables "

'    Private mintAssessorMasterUnkId As Integer
'    Private mstrEmployeeIds As String = String.Empty

'#End Region

'#Region " Properties "

'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set assessormasterunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Assessormasterunkid() As Integer
'        Get
'            Return mintAssessorMasterUnkId
'        End Get
'        Set(ByVal value As Integer)
'            mintAssessorMasterUnkId = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set mstrEmployeeIds
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _EmployeeIds() As String
'        Get
'            Return mstrEmployeeIds
'        End Get
'        Set(ByVal value As String)
'            mstrEmployeeIds = value
'        End Set
'    End Property

'#End Region

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetData() As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                       "  hrassessor_tran.assessortranunkid " & _
'                       ", hrassessor_tran.assessormasterunkid " & _
'                       ", hrassessor_tran.departmentunkid " & _
'                       ", hrdepartment_master.name as departmentname " & _
'                       ", hrassessor_tran.employeeunkid " & _
'                       ", isnull(hremployee_master.employeecode,'') as employeecode " & _
'                       ", isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') as employeename " & _
'                       " FROM hrassessor_tran " & _
'                       " LEFT JOIN hrdepartment_master on hrdepartment_master.departmentunkid = hrassessor_tran.departmentunkid " & _
'                       " LEFT JOIN hremployee_master on hremployee_master.employeeunkid = hrassessor_tran.employeeunkid " & _
'                       " WHERE assessormasterunkid = @assessormasterunkid AND hrassessor_tran.isvoid = 0 "

'            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessorMasterUnkId.ToString)
'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetData", mstrModuleName)
'        End Try
'        Return dsList
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, ByVal blnIsReviewer As Boolean) As DataSet
'        Dim StrQ As String = ""
'        Dim dsList As New DataSet
'        Dim exForce As Exception
'        Dim objDataOperation As New clsDataOperation
'        Try

'            'S.SANDEEP [ 25 JULY 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'StrQ = "SELECT " & _
'            '            "    hrassessor_tran.assessormasterunkid " & _
'            '            "   ,hrdepartment_master.departmentunkid " & _
'            '            "   ,ISNULL(hremployee_master.firstname, '') + ' ' " & _
'            '            "    + ISNULL(hremployee_master.othername, '') + ' ' " & _
'            '            "    + ISNULL(hremployee_master.surname, '') AS assessor " & _
'            '            "   ,hrdepartment_master.name AS Department " & _
'            '            "FROM hrassessor_tran " & _
'            '            "    JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'            '            "    JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
'            '           "    JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'            '           "WHERE 1 = 1 AND hrassessor_tran.isvoid = 0 AND hrassessor_master.isvoid = 0 "
'            StrQ = "SELECT DISTINCT " & _
'                        "    hrassessor_tran.assessormasterunkid " & _
'                        "   ,hrdepartment_master.departmentunkid " & _
'                        "   ,ISNULL(hremployee_master.firstname, '') + ' ' " & _
'                        "    + ISNULL(hremployee_master.othername, '') + ' ' " & _
'                        "    + ISNULL(hremployee_master.surname, '') AS assessor " & _
'                        "   ,hrdepartment_master.name AS Department " & _
'                       "   ,ISNULL(Usr.username,'') AS usermapped " & _
'                        "FROM hrassessor_tran " & _
'                        "    JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'                       "    LEFT JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid AND usertypeid = '" & enUserType.Assessor & "'  " & _
'                       "    LEFT JOIN hrmsConfiguration..cfuser_master AS Usr ON dbo.hrapprover_usermapping.userunkid = Usr.userunkid " & _
'                        "    JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
'                        "    JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                        "WHERE 1 = 1 AND hrassessor_tran.isvoid = 0 AND hrassessor_master.isvoid = 0 "
'            'S.SANDEEP [ 25 JULY 2013 ] -- END



'            If blnIsReviewer Then
'                StrQ &= " AND hrassessor_master.isreviewer = 1 "
'            Else
'                StrQ &= " AND hrassessor_master.isreviewer = 0 "
'            End If
'            If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
'                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (12 May 2012) - [empl_enddate]

'                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'            End If



'            'S.SANDEEP [ 04 FEB 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    StrQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
'            'End If

'            'S.SANDEEP [ 01 JUNE 2012 ] -- START
'            'ENHANCEMENT : TRA DISCIPLINE CHANGES
'            'Select Case ConfigParameter._Object._UserAccessModeSetting
'            '    Case enAllocation.BRANCH
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            StrQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            StrQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            StrQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            StrQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            StrQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            StrQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            StrQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.TEAM
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            StrQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOB_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            StrQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOBS
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            StrQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
'            '        End If
'            'End Select
'            StrQ &= UserAccessLevel._AccessLevelFilterString
'            'S.SANDEEP [ 01 JUNE 2012 ] -- END


'            'S.SANDEEP [ 04 FEB 2012 ] -- END




'            'S.SANDEEP [ 25 JULY 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'StrQ &= "GROUP BY  hrassessor_tran.assessormasterunkid " & _
'            '       ",hrdepartment_master.departmentunkid " & _
'            '       ",ISNULL(hremployee_master.firstname, '') + ' ' " & _
'            '       "+ ISNULL(hremployee_master.othername, '') + ' ' " & _
'            '       "+ ISNULL(hremployee_master.surname, '') " & _
'            '       ",hrdepartment_master.name "
'            'S.SANDEEP [ 25 JULY 2013 ] -- END

'            dsList = objDataOperation.ExecQuery(StrQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Function

'    Public Function Insert(ByVal mdTable As DataTable, ByVal enMode As enAssessmentMode, ByVal intAssessRevId As Integer) As Boolean
'        Dim StrQ As String = String.Empty
'        Dim dsList As New DataSet
'        Dim exForce As Exception
'        Dim objDataOperation As New clsDataOperation
'        Try
'            objDataOperation.BindTransaction()

'            'StrQ = "SELECT assessortranunkid,assessormasterunkid,departmentunkid,employeeunkid	FROM hrassessor_tran WHERE assessormasterunkid = '" & mintAssessorMasterUnkId & "' "
'            'objDataOperation.ExecQuery(StrQ, "List")
'            Dim dList As DataSet = clsCommonATLog.GetChildList(objDataOperation, "hrassessor_tran", "assessormasterunkid", mintAssessorMasterUnkId)

'            If dList.Tables(0).Rows.Count > 0 Then
'                Dim dRow() As DataRow = dList.Tables(0).Select("employeeunkid NOT IN(" & mstrEmployeeIds & ") AND assessormasterunkid = '" & mintAssessorMasterUnkId & "'")
'                If dRow.Length > 0 Then
'                    For i As Integer = 0 To dRow.Length - 1
'                        If isUsed(objDataOperation, enMode, dRow(i)("employeeunkid"), intAssessRevId) = False Then
'                            Call DeleteAssessorAccess(objDataOperation, dRow(i)("assessortranunkid"))
'                        Else
'                            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot remove some of the employee(s) access, as assessment is already done for some employees.")
'                        End If
'                    Next
'                End If
'            End If



'            'S.SANDEEP [ 18 APRIL 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'StrQ = "INSERT INTO hrassessor_tran ( " & _
'            '            "  assessormasterunkid " & _
'            '            ", departmentunkid " & _
'            '            ", employeeunkid" & _
'            '       ") VALUES (" & _
'            '            "  @assessormasterunkid " & _
'            '            ", @departmentunkid " & _
'            '            ", @employeeunkid" & _
'            '       "); SELECT @@identity"

'            StrQ = "INSERT INTO hrassessor_tran ( " & _
'                        "  assessormasterunkid " & _
'                        ", departmentunkid " & _
'                        ", employeeunkid" & _
'                        ", isvoid " & _
'                        ", voiddatetime " & _
'                        ", voiduserunkid " & _
'                        ", voidreason" & _
'                   ") VALUES (" & _
'                        "  @assessormasterunkid " & _
'                        ", @departmentunkid " & _
'                        ", @employeeunkid" & _
'                        ", @isvoid " & _
'                        ", @voiddatetime " & _
'                        ", @voiduserunkid " & _
'                        ", @voidreason" & _
'                   "); SELECT @@identity"
'            'S.SANDEEP [ 18 APRIL 2012 ] -- END





'            'Pinkal (12-Jun-2012) -- Start
'            'Enhancement : TRA Changes
'            'Dim dTRow() As DataRow = mdTable.Select("IsCheck = true and IsGrp = false")
'            Dim dTRow() As DataRow = mdTable.Select("IsCheck = true")
'            'Pinkal (12-Jun-2012) -- End

'            If dTRow.Length > 0 Then
'                For i As Integer = 0 To dTRow.Length - 1
'                    If isExist(objDataOperation, dTRow(i)("EmpId"), mintAssessorMasterUnkId) = False Then

'                                objDataOperation.ClearParameters()
'                        objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessorMasterUnkId.ToString)
'                        objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dTRow(i)("GrpId"))
'                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dTRow(i)("EmpId"))
'                        'S.SANDEEP [ 18 APRIL 2012 ] -- START
'                        'ENHANCEMENT : TRA CHANGES
'                        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
'                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
'                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
'                        'S.SANDEEP [ 18 APRIL 2012 ] -- END

'                        dsList = objDataOperation.ExecQuery(StrQ, "List")

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                        Dim mintassessortranunkid As Integer = CInt(dsList.Tables(0).Rows(0)(0))

'                        If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassessor_tran", "assessortranunkid", mintassessortranunkid) = False Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                    End If
'                            Next
'            End If

'            objDataOperation.ReleaseTransaction(True)

'            Return True

'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Function

'    Private Function isUsed(ByVal objDataOperation As clsDataOperation, ByVal enMode As enAssessmentMode, ByVal intEmpId As Integer, ByVal intAssessRevUnkid As Integer) As Boolean
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Dim iCnt As Integer = -1
'        Dim blnFlag As Boolean = False
'        Try
'            Select Case enMode
'                Case enAssessmentMode.APPRAISER_ASSESSMENT
'                    StrQ = "Select * From hrassess_analysis_master where assessedemployeeunkid = '" & intEmpId & "' AND assessoremployeeunkid = '" & intAssessRevUnkid & "' "
'                Case enAssessmentMode.REVIEWER_ASSESSMENT
'                    StrQ = "Select * From hrassess_analysis_master where assessedemployeeunkid = '" & intEmpId & "' AND reviewerunkid = '" & intAssessRevUnkid & "' "
'            End Select

'            iCnt = objDataOperation.RecordCount(StrQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'                End If

'            If iCnt > 0 Then
'                blnFlag = True
'            Else
'                blnFlag = False
'            End If

'            Return blnFlag

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Function

'    Private Function DeleteAssessorAccess(ByVal objDataOperation As clsDataOperation, ByVal intAssessorTranId As Integer, Optional ByVal blnIsMigration As Boolean = False) As Boolean
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Try
'            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassessor_tran", "assessortranunkid", intAssessorTranId) = False Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'                    End If

'            'S.SANDEEP [ 18 APRIL 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'StrQ = "DELETE FROM hrassessor_tran WHERE assessortranunkid = '" & intAssessorTranId & "' "
'            StrQ = "UPDATE hrassessor_tran SET " & _
'                   " isvoid = 1 " & _
'                   ",voiddatetime = @voiddatetime " & _
'                   ",voiduserunkid = @voiduserunkid " & _
'                   ",voidreason = @voidreason " & _
'                   "WHERE assessortranunkid = '" & intAssessorTranId & "' "

'                    objDataOperation.ClearParameters()
'            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
'            If blnIsMigration Then
'                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Migration"))
'            Else
'                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Un-Assigned"))
'                    End If
'            'S.SANDEEP [ 18 APRIL 2012 ] -- END


'            objDataOperation.ExecNonQuery(StrQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If

'                Return True

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: DeleteAssessorAccess; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Function

'    Public Function isExist(ByVal objDataOperation As clsDataOperation, ByVal intEmpId As Integer, ByVal intAssessorId As Integer) As Boolean
'        Dim StrQ As String = String.Empty
'        Dim dsList As New DataSet
'        Dim exForce As Exception
'        Try
'            StrQ = "SELECT " & _
'                        "  assessortranunkid " & _
'                        ", assessormasterunkid " & _
'                        ", departmentunkid " & _
'                        ", employeeunkid " & _
'                        "FROM hrassessor_tran " & _
'                        "WHERE employeeunkid = @EmpId " & _
'                        " AND assessormasterunkid = @AssessorId AND hrassessor_tran.isvoid = 0 "

'            objDataOperation.ClearParameters()
'            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId.ToString)
'            objDataOperation.AddParameter("@AssessorId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorId.ToString)

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables(0).Rows.Count > 0 Then
'                Return True
'            Else
'                Return False
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
'        End Try
'    End Function

'    Public Sub Delete(ByVal intAssessorunkid As Integer)
'        Dim StrQ As String = ""
'        Dim exForce As Exception
'        Dim objDataOperation As New clsDataOperation
'        Try
'            objDataOperation.BindTransaction()

'            'Pinkal (12-Oct-2011) -- Start
'            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'            StrQ = "select isnull(mappingunkid,0) mappingunkid  From hrapprover_usermapping where approverunkid = @approverunkid AND usertypeid = " & enUserType.Assessor
'            objDataOperation.ClearParameters()
'            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorunkid.ToString)
'            Dim dtList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

'            If dtList IsNot Nothing AndAlso dtList.Tables(0).Rows.Count > 0 Then

'                If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrapprover_usermapping", "mappingunkid", CInt(dtList.Tables(0).Rows(0)("mappingunkid"))) = False Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If

'            End If

'            'Pinkal (12-Oct-2011) -- End

'            StrQ = "Delete from hrapprover_usermapping where approverunkid = @approverunkid"
'            objDataOperation.ClearParameters()
'            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorunkid)
'            objDataOperation.ExecNonQuery(StrQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If


'            'Pinkal (12-Oct-2011) -- Start
'            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
'            StrQ = "select isnull(assessortranunkid,0) assessortranunkid  From hrassessor_tran where assessormasterunkid = @assessormasterunkid "
'            objDataOperation.ClearParameters()
'            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorunkid.ToString)
'            dtList = objDataOperation.ExecQuery(StrQ, "List")

'            If dtList IsNot Nothing AndAlso dtList.Tables(0).Rows.Count > 0 Then

'                For Each dr As DataRow In dtList.Tables(0).Rows

'                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassessor_tran", "assessortranunkid", CInt(dr("assessortranunkid"))) = False Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If

'                Next

'            End If

'            'Pinkal (12-Oct-2011) -- End


'            StrQ = "Delete from hrassessor_tran WHERE assessormasterunkid = @assessormasterunkid"
'            objDataOperation.ClearParameters()
'            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorunkid)
'            objDataOperation.ExecNonQuery(StrQ)


'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            objDataOperation.ReleaseTransaction(True)

'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            DisplayError.Show("-1", ex.Message, "Delete", mstrModuleName)
'        End Try
'    End Sub

'    'S.SANDEEP [ 18 APRIL 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public Function Perform_Migration(ByVal dtTable As DataTable, ByVal intUnkid As Integer) As Boolean
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Dim blnVoidMaster As Boolean = False
'        Dim dsInsert As DataSet
'        Dim mstrEmpIds As String = String.Empty
'        Dim objDataOperation As New clsDataOperation
'        Try
'            Dim dsList As New DataSet
'            mintAssessorMasterUnkId = dtTable.Rows(0)("assessormasterunkid")
'            dsList = GetData()
'            If dsList.Tables(0).Rows.Count = dtTable.Rows.Count Then
'                blnVoidMaster = True
'            End If

'            objDataOperation.BindTransaction()

'            For Each dRow As DataRow In dtTable.Rows
'                If DeleteAssessorAccess(objDataOperation, dRow.Item("assessortranunkid"), True) = True Then

'                    objDataOperation.ClearParameters()
'                    objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
'                    objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dRow.Item("departmentunkid").ToString)
'                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dRow.Item("employeeunkid").ToString)
'                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
'                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
'                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")

'                    StrQ = "INSERT INTO hrassessor_tran ( " & _
'                            "  assessormasterunkid " & _
'                            ", departmentunkid " & _
'                            ", employeeunkid " & _
'                            ", isvoid " & _
'                            ", voiddatetime " & _
'                            ", voiduserunkid " & _
'                            ", voidreason" & _
'                          ") VALUES (" & _
'                            "  @assessormasterunkid " & _
'                            ", @departmentunkid " & _
'                            ", @employeeunkid " & _
'                            ", @isvoid " & _
'                            ", @voiddatetime " & _
'                            ", @voiduserunkid " & _
'                            ", @voidreason" & _
'                          "); SELECT @@identity"

'                    dsInsert = objDataOperation.ExecQuery(StrQ, "List")

'                    If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If

'                    Dim mintassessortranunkid As Integer = CInt(dsInsert.Tables(0).Rows(0)(0))

'                    If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassessor_tran", "assessortranunkid", mintassessortranunkid) = False Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If
'                End If

'                mstrEmpIds &= "," & dRow.Item("employeeunkid").ToString
'            Next

'            If mstrEmpIds.Trim.Length > 0 Then
'                mstrEmpIds = Mid(mstrEmpIds, 2)
'            End If

'            If blnVoidMaster = True Then

'                StrQ = "UPDATE hrassessor_master SET " & _
'                       " isvoid = 1 " & _
'                       ",voiddatetime = @voiddatetime " & _
'                       ",voiduserunkid = @voiduserunkid " & _
'                       ",voidreason = @voidreason " & _
'                       "WHERE assessormasterunkid = @assessormasterunkid "

'                objDataOperation.ClearParameters()
'                objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessorMasterUnkId)
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
'                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
'                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 3, "Migration"))

'                objDataOperation.ExecNonQuery(StrQ)

'                If objDataOperation.ErrorMessage <> "" Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If

'                If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassessor_master", "assessormasterunkid", mintAssessorMasterUnkId) = False Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If

'                StrQ = "SELECT ISNULL(mappingunkid,0) mappingunkid FROM hrapprover_usermapping WHERE approverunkid = @approverunkid and Usertypeid =" & enUserType.Assessor

'                objDataOperation.ClearParameters()
'                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessorMasterUnkId)
'                dsList = objDataOperation.ExecQuery(StrQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If

'                If dsList.Tables(0).Rows.Count > 0 Then
'                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrapprover_usermapping", "mappingunkid", CInt(dsList.Tables(0).Rows(0)("mappingunkid"))) = False Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If
'                End If

'            End If

'            Dim objMigration As New clsMigration

'            objMigration._Migratedemployees = mstrEmpIds
'            objMigration._Migrationdate = ConfigParameter._Object._CurrentDateAndTime
'            objMigration._Migrationfromunkid = mintAssessorMasterUnkId
'            objMigration._Migrationtounkid = intUnkid
'            objMigration._Usertypeid = enUserType.Assessor
'            objMigration._Userunkid = User._Object._Userunkid

'            If objMigration.Insert(objDataOperation) = False Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            objMigration = Nothing

'            objDataOperation.ReleaseTransaction(True)
'            Return True
'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            Throw New Exception(ex.Message & "; Procedure Name: Perform_Migration; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Function

'    Public Function IsEmployeeExists(ByVal intEmpId As Integer, ByVal intMatchUnkid As Integer) As Boolean
'        Dim objDataOperation As New clsDataOperation
'        Try
'            Return isExist(objDataOperation, intEmpId, intMatchUnkid)
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: IsEmployeeExists; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Function
'    'S.SANDEEP [ 18 APRIL 2012 ] -- END


'    'S.SANDEEP [ 17 JULY 2013 ] -- START
'    'ENHANCEMENT : OTHER CHANGES
'    Public Function Import_Assesor_Reviewe_Access(ByVal iAccessEmpId As Integer, _
'                                                  ByVal iDepartmentId As Integer, _
'                                                  ByVal iAREmpId As Integer, _
'                                                  ByVal blnIsReviewer As Boolean, _
'                                                  ByRef iMessage As String) As Integer
'        Dim StrQ As String = String.Empty
'        Dim objDataOperation As clsDataOperation
'        Dim iRefValue As Integer = -1
'        Dim iAssessorMasterId As Integer = 0
'        Dim dsList As New DataSet
'        Dim exForce As Exception
'        Dim objOption As New clsPassowdOptions
'        Try
'            objDataOperation = New clsDataOperation

'            iRefValue = -1

'            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER MASTER */--/* START */
'            If blnIsReviewer = False Then
'                StrQ = "SELECT assessormasterunkid FROM hrassessor_master WHERE isvoid = 0 AND employeeunkid = '" & iAREmpId & "' AND isreviewer = 0 "
'            Else
'                StrQ = "SELECT assessormasterunkid FROM hrassessor_master WHERE isvoid = 0 AND employeeunkid = '" & iAREmpId & "' AND isreviewer = 1 "
'            End If

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables("List").Rows.Count > 0 Then
'                iAssessorMasterId = dsList.Tables("List").Rows(0).Item("assessormasterunkid")
'                Dim iCnt As Integer = 0
'                iCnt = objDataOperation.RecordCount("SELECT * FROM hrassessor_tran WHERE assessormasterunkid = '" & iAssessorMasterId & "' AND employeeunkid = '" & iAccessEmpId & "'")

'                If objDataOperation.ErrorMessage <> "" Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If
'                If iCnt > 0 Then
'                    iRefValue = 0
'                    GoTo final
'                End If
'            Else
'                StrQ = "INSERT INTO hrassessor_master " & _
'                       " (employeeunkid,userunkid,isreviewer,isvoid,voiddatetime,voiduserunkid,voidreason) " & _
'                       "SELECT '" & iAREmpId & "','" & User._Object._Userunkid & "','" & IIf(blnIsReviewer = True, "True", "False") & "','False',NULL,-1,'' " & _
'                       " WHERE NOT EXISTS (SELECT employeeunkid FROM hrassessor_master WHERE isvoid = 0 AND isreviewer = " & IIf(blnIsReviewer = True, 1, 0) & " AND employeeunkid = '" & iAREmpId & "');SELECT @@IDENTITY; "

'                dsList = objDataOperation.ExecQuery(StrQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If

'                iAssessorMasterId = dsList.Tables("List").Rows(0).Item(0)

'                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassessor_master", "assessormasterunkid", iAssessorMasterId) = False Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If
'            End If
'            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER MASTER */--/* END */

'            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER USER MAPPING */--/* START */
'            If objOption._IsEmployeeAsUser = True Then
'                If iAssessorMasterId > 0 Then
'                    StrQ = "SELECT ISNULL(userunkid,0) AS UsrId " & _
'                           "FROM hremployee_master " & _
'                           "JOIN hrmsConfiguration..cfuser_master ON hremployee_master.displayname = hrmsConfiguration..cfuser_master.username " & _
'                           "WHERE hremployee_master.employeeunkid = '" & iAREmpId & "' "

'                    dsList = objDataOperation.ExecQuery(StrQ, "Usr")

'                    If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If

'                    If dsList.Tables("Usr").Rows.Count > 0 Then
'                        If dsList.Tables("Usr").Rows(0).Item("UsrId") > 0 Then

'                            Dim iCnt As Integer = 0
'                            iCnt = objDataOperation.RecordCount("SELECT * FROM hrapprover_usermapping WHERE approverunkid = '" & iAssessorMasterId & "' AND userunkid = '" & dsList.Tables("Usr").Rows(0).Item("UsrId") & "' AND usertypeid = '" & enUserType.Assessor & "'")

'                            If iCnt <= 0 Then
'                                StrQ = "INSERT INTO hrapprover_usermapping " & _
'                                   "(approverunkid,userunkid,usertypeid) " & _
'                                   "SELECT '" & iAssessorMasterId & "', '" & dsList.Tables("Usr").Rows(0).Item("UsrId") & "','" & enUserType.Assessor & "' " & _
'                                   "WHERE NOT EXISTS (SELECT approverunkid FROM hrapprover_usermapping WHERE usertypeid = '" & enUserType.Assessor & "' AND approverunkid = '" & iAssessorMasterId & "');SELECT @@IDENTITY; "

'                                dsList = objDataOperation.ExecQuery(StrQ, "uList")

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If
'                                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrapprover_usermapping", "mappingunkid", dsList.Tables("uList").Rows(0).Item(0)) = False Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If
'                            End If
'                        End If
'                    End If
'                End If
'            End If
'            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER USER MAPPING */--/* END */

'            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER ACCESS */--/* START */
'            If iAssessorMasterId > 0 Then
'                StrQ = "INSERT INTO hrassessor_tran " & _
'                       "(assessormasterunkid,stationunkid,departmentunkid,employeeunkid,isvoid,voiddatetime,voiduserunkid,voidreason) " & _
'                       "SELECT '" & iAssessorMasterId & "', 0, '" & iDepartmentId & "', '" & iAccessEmpId & "','False',NULL,-1,''" & _
'                       "WHERE NOT EXISTS (SELECT assessortranunkid FROM hrassessor_tran WHERE assessormasterunkid = '" & iAssessorMasterId & "' AND employeeunkid = '" & iAccessEmpId & "');SELECT @@IDENTITY; "

'                dsList = objDataOperation.ExecQuery(StrQ, "lst")

'                If objDataOperation.ErrorMessage <> "" Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If

'                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassessor_tran", "assessortranunkid", dsList.Tables("lst").Rows(0).Item(0)) = False Then
'                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                    Throw exForce
'                End If
'            End If
'            '/* CHECKING AND INSERTING ASSESSOR/REVIEWER ACCESS */--/* END */

'final:      If iRefValue = -1 Then iRefValue = 1

'            Return iRefValue
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Import_Assesor_Reviewe_Access", mstrModuleName)
'            iMessage = ex.Message
'            Return iRefValue
'        Finally
'            objDataOperation = Nothing
'        End Try
'    End Function
'    'S.SANDEEP [ 17 JULY 2013 ] -- END


'    'S.SANDEEP [ 02 AUG 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public Function IsAssessorPresent(ByVal iEmpId As Integer) As String
'        Dim StrQ As String = String.Empty
'        Dim StrMsg As String = String.Empty
'        Dim objDataOperation As New clsDataOperation
'        Dim dsList As New DataSet
'        Try
'            StrQ = "SELECT 1 FROM hrassessor_tran " & _
'                   " JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'                   "WHERE hrassessor_tran.employeeunkid = '" & iEmpId & "' AND hrassessor_tran.isvoid = 0 " & _
'                   " AND isreviewer = 0 AND hrassessor_master.isvoid = 0 "

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                Throw New Exception(objDataOperation.ErrorNumber & "  : " & objDataOperation.ErrorMessage)
'            End If

'            If dsList.Tables(0).Rows.Count <= 0 Then
'                StrMsg = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot do submit for approval operation. Reason : No Assessor assigned to to this employee.")
'            End If

'            Return StrMsg
'        Catch ex As Exception
'            Throw New Exception(objDataOperation.ErrorNumber & "  : " & objDataOperation.ErrorMessage)
'        Finally
'        End Try
'    End Function
'    'S.SANDEEP [ 02 AUG 2013 ] -- END


'End Class
