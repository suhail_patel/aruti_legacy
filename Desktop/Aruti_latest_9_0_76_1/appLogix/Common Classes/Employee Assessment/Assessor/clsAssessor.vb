﻿'************************************************************************************************************************************
'Class Name : clsAssessor.vb
'Purpose    :
'Date       :07/10/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class clsAssessor

#Region " Private Variables "
    Private Shared ReadOnly mstrModuleName As String = "clsAssessor"
#End Region

#Region " Property Variables "

    Private mintAssessormasterunkid As Integer
    Private mintEmployeeId As Integer = -1
    Private mintUserunkid As Integer = -1
    Private mstrassessormasterIds As String = String.Empty
    Private mstrMessage As String = String.Empty
    Private mblnIsreviewer As Boolean = False
    Private mblnIsvoid As Boolean = False
    Private mdtVoiddatetime As Date = Nothing
    Private mintVoiduserunkid As Integer = -1
    Private mstrVoidreason As String = String.Empty
    Private mintMappedUserId As Integer = 0
    Private mintMappingUnkid As Integer = 0
    Private mstrEmployeeName As String = String.Empty

    'Shani(01-MAR-2016) -- Start
    'Enhancement :PA External Approver Flow
    Private mblnIsExternalAssessorReviewer As Boolean = False
    'Shani(01-MAR-2016) -- End

    'S.SANDEEP [27 DEC 2016] -- START
    'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
    Private mintVisibletypeid As Integer = enARVisibilityTypeId.VISIBLE
    'S.SANDEEP [27 DEC 2016] -- END

#End Region

    'Shani(01-MAR-2016) -- Start
    'Enhancement  :PA External Approver Flow
    Public Enum enAssessorType
        ASSESSOR = 0
        REVIEWER = 1
        ALL_DATA = 2
    End Enum
    'Shani(01-MAR-2016) -- End

    'S.SANDEEP [27 DEC 2016] -- START
    'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
    Public Enum enARVisibilityTypeId
        NOT_VISIBLE = 0
        VISIBLE = 1
        MIGRATE = 2
        SWAPPED = 3
    End Enum
    'S.SANDEEP [27 DEC 2016] -- END

#Region " Properties "


    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.


    'Public Property _Assessormasterunkid() As Integer
    '    Get
    '        Return mintAssessormasterunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintAssessormasterunkid = value
    '        Call GetData()
    '    End Set
    'End Property

    Public Property _Assessormasterunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintAssessormasterunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessormasterunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property
    'Gajanan [23-SEP-2019] -- End

    Public Property _EmployeeId() As Integer
        Get
            Return mintEmployeeId
        End Get
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _AssessormasterIds() As String
        Get
            Return mstrassessormasterIds
        End Get
        Set(ByVal value As String)
            mstrassessormasterIds = value
        End Set
    End Property

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _Isreviewer() As Boolean
        Get
            Return mblnIsreviewer
        End Get
        Set(ByVal value As Boolean)
            mblnIsreviewer = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public Property _MappedUserId() As Integer
        Get
            Return mintMappedUserId
        End Get
        Set(ByVal value As Integer)
            mintMappedUserId = value
        End Set
    End Property

    Public Property _MappingUnkid() As Integer
        Get
            Return mintMappingUnkid
        End Get
        Set(ByVal value As Integer)
            mintMappingUnkid = value
        End Set
    End Property

    Public ReadOnly Property _EmployeeName() As String
        Get
            Return mstrEmployeeName
        End Get
    End Property

    'Shani(01-MAR-2016) -- Start
    'Enhancement :PA External Approver Flow
    Public Property _ExternalAssessorReviewer() As Boolean
        Get
            Return mblnIsExternalAssessorReviewer
        End Get
        Set(ByVal value As Boolean)
            mblnIsExternalAssessorReviewer = value
        End Set
    End Property
    'Shani(01-MAR-2016) -- End

    'S.SANDEEP [27 DEC 2016] -- START
    'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
    Public Property _Visibletypeid() As Integer
        Get
            Return mintVisibletypeid
        End Get
        Set(ByVal value As Integer)
            mintVisibletypeid = value
        End Set
    End Property
    'S.SANDEEP [27 DEC 2016] -- END

#End Region

    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
    'Public Sub GetData()
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        'Gajanan [23-SEP-2019] -- End
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception

        'Gajanan [23-SEP-2019] -- Start    
        'Enhancement:Enforcement of approval migration From Transfer and recategorization.
        'Dim objDataOperation As New clsDataOperation
        Dim objDataOperation As clsDataOperation
        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()
        'Gajanan [23-SEP-2019] -- End

        Try

            'Shani(01-MAR-2016) -- Start
            'Enhancement :PA External Approver Flow
            'StrQ = "SELECT " & _
            '       "  hrassessor_master.assessormasterunkid " & _
            '       " ,hrassessor_master.employeeunkid " & _
            '       " ,hrassessor_master.userunkid " & _
            '       " ,hrassessor_master.isreviewer " & _
            '       " ,hrassessor_master.isvoid " & _
            '       " ,hrassessor_master.voiddatetime " & _
            '       " ,hrassessor_master.voiduserunkid " & _
            '       " ,hrassessor_master.voidreason " & _
            '       " ,ISNULL(mappingunkid,0) AS mappingunkid " & _
            '       " ,ISNULL(hrapprover_usermapping.userunkid,0) AS mapuserid " & _
            '       " ,employeecode + ' - ' + firstname+' '+surname AS employee " & _
            '       "FROM hrassessor_master " & _
            '       " LEFT JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
            '       " LEFT JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid AND usertypeid = '" & enUserType.Assessor & "' " & _
            '       "WHERE hrassessor_master.isvoid = 0 AND assessormasterunkid = '" & mintAssessormasterunkid & "' "

            dsList = GetExternalAssessorReviewerList(objDataOperation, "List", mintAssessormasterunkid)
            Dim StrDBName As String = String.Empty : Dim intCompanyId As Integer = -1
            If dsList.Tables("List").Rows.Count > 0 Then
                StrDBName = dsList.Tables("List").Rows(0)("DBName")
                intCompanyId = dsList.Tables("List").Rows(0)("companyunkid")
            End If
            dsList = New DataSet

            StrQ = "SELECT " & _
                   "  hrassessor_master.assessormasterunkid " & _
                   " ,hrassessor_master.employeeunkid " & _
                   " ,hrassessor_master.userunkid " & _
                   " ,hrassessor_master.isreviewer " & _
                   " ,hrassessor_master.isvoid " & _
                   " ,hrassessor_master.voiddatetime " & _
                   " ,hrassessor_master.voiduserunkid " & _
                   " ,hrassessor_master.voidreason " & _
                   " ,ISNULL(mappingunkid,0) AS mappingunkid " & _
                   " ,ISNULL(hrapprover_usermapping.userunkid,0) AS mapuserid " & _
                   "     ,#EMPL_DATA# AS employee " & _
                   "     ,isexternalapprover " & _
                   " ,visibletypeid " & _
                   "FROM hrassessor_master " & _
                   "     #USER_JOIN# " & _
                   "     #EMPL_JOIN# " & _
                   " LEFT JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid AND usertypeid = '" & enUserType.Assessor & "' " & _
                   "WHERE hrassessor_master.isvoid = 0 AND assessormasterunkid = '" & mintAssessormasterunkid & "' " 'S.SANDEEP [27 DEC 2016] -- START {visibletypeid} -- END


            If StrDBName.Trim.Length > 0 AndAlso intCompanyId > 0 Then
                StrQ = StrQ.Replace("#USER_JOIN#", "JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = hrassessor_master.employeeunkid ")
                StrQ = StrQ.Replace("#EMPL_JOIN#", "LEFT JOIN #DName#hremployee_master ON #TBL_NAME#.employeeunkid = hremployee_master.employeeunkid ")
                StrQ = StrQ.Replace("#TBL_NAME#", "UEmp")
                StrQ = StrQ.Replace("#EMPL_DATA#", "hremployee_master.employeecode + ' - ' + hremployee_master.firstname+' '+hremployee_master.surname")
            ElseIf StrDBName.Trim.Length <= 0 AndAlso intCompanyId <> -1 Then
                StrQ = StrQ.Replace("#USER_JOIN#", "JOIN hrmsConfiguration..cfuser_master AS UEmp ON UEmp.userunkid = hrassessor_master.employeeunkid ")
                StrQ = StrQ.Replace("#EMPL_JOIN#", "")
                StrQ = StrQ.Replace("#EMPL_DATA#", "ISNULL(UEmp.username,'')")
            Else
                StrQ = StrQ.Replace("#USER_JOIN#", "")
                StrQ = StrQ.Replace("#EMPL_JOIN#", "LEFT JOIN #DName#hremployee_master ON #TBL_NAME#.employeeunkid = hremployee_master.employeeunkid ")
                StrQ = StrQ.Replace("#TBL_NAME#", "hrassessor_master")
                StrQ = StrQ.Replace("#EMPL_DATA#", "hremployee_master.employeecode + ' - ' + hremployee_master.firstname+' '+hremployee_master.surname")
            End If
            StrQ = StrQ.Replace("#DName#", IIf(StrDBName = "", "", StrDBName & ".."))

            'Shani(01-MAR-2016)-- End

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAssessormasterunkid = dtRow.Item("assessormasterunkid")
                mintEmployeeId = dtRow.Item("employeeunkid")
                mintUserunkid = dtRow.Item("userunkid")
                mblnIsreviewer = CBool(dtRow.Item("isreviewer"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason")
                mintMappedUserId = dtRow.Item("mapuserid")
                mintMappingUnkid = dtRow.Item("mappingunkid")
                mstrEmployeeName = dtRow.Item("employee")

                'Shani(01-MAR-2016) -- Start
                'Enhancement :PA External Approver Flow
                mblnIsExternalAssessorReviewer = CBool(dtRow.Item("isexternalapprover"))
                'Shani(01-MAR-2016)-- End

                'S.SANDEEP [27 DEC 2016] -- START
                'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
                mintVisibletypeid = dtRow.Item("visibletypeid")
                'S.SANDEEP [27 DEC 2016] -- END


                Exit For
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetData", mstrModuleName)
        Finally
            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
            'Gajanan [23-SEP-2019] -- End
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassessor_master) </purpose>
    Public Function Insert(ByVal mAssessor As DataTable, ByVal mReviewer As DataTable) As Boolean
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation

        'Shani(01-MAR-2016) -- Start
        'Enhancement :PA External Approver Flow
        If IsExist(mintEmployeeId, mblnIsreviewer, mblnIsExternalAssessorReviewer) = True Then
            mstrMessage = Language.getMessage(mstrModuleName, 6, "Sorry, this assessor/reviewer is already defined. Please define new assessor/reviewer.")
            Return False
        End If
        'Shani(01-MAR-2016) -- End

        Try
            objDataOperation.BindTransaction()
            StrQ = "INSERT INTO hrassessor_master ( " & _
                        "  employeeunkid " & _
                        ", userunkid " & _
                        ", isreviewer" & _
                       ", isvoid " & _
                       ", voiddatetime " & _
                       ", voiduserunkid " & _
                       ", voidreason" & _
                       ", isexternalapprover " & _
                       ", visibletypeid " & _
                   ") VALUES (" & _
                        "  @employeeunkid " & _
                        ", @userunkid " & _
                        ", @isreviewer" & _
                       ", @isvoid " & _
                       ", @voiddatetime " & _
                       ", @voiduserunkid " & _
                       ", @voidreason" & _
                       ", @isexternalapprover " & _
                       ", @visibletypeid " & _
                   "); SELECT @@identity" 'Shani(01-MAR-2016) -- [isexternalapprover]-- End
            'S.SANDEEP [27 DEC 2016] -- START {visibletypeid}-- END

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@isreviewer", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsreviewer.ToString)

            'Shani(01-MAR-2016) -- Start
            'Enhancement  :PA External Approver Flow
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalAssessorReviewer.ToString)
            'Shani(01-MAR-2016)-- End

            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            objDataOperation.AddParameter("@visibletypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibletypeid)
            'S.SANDEEP [27 DEC 2016] -- END


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAssessormasterunkid = CInt(dsList.Tables(0).Rows(0)(0))

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassessor_master", "assessormasterunkid", mintAssessormasterunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mintAssessormasterunkid > 0 Then
                Dim objATran As New clsAssessor_tran
                If mAssessor IsNot Nothing Then
                    If mAssessor.Rows.Count > 0 Then
                        objATran._DataTable = mAssessor
                        If objATran.Insert_Update_Delete(objDataOperation, mintAssessormasterunkid) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        End If
                    End If
                End If

                If mReviewer IsNot Nothing Then
                    If mReviewer.Rows.Count > 0 Then
                        objATran._DataTable = mReviewer
                        If objATran.Insert_Update_Delete(objDataOperation, mintAssessormasterunkid) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        End If
                    End If
                End If

                Dim objUsrMapp As New clsapprover_Usermapping

                objUsrMapp._Approverunkid = mintAssessormasterunkid
                objUsrMapp._AuditUserunkid = mintUserunkid
                objUsrMapp._UserTypeid = enUserType.Assessor
                objUsrMapp._Userunkid = mintMappedUserId

                If objUsrMapp.Insert(objDataOperation) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "Insert", mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassessor_master) </purpose>
    Public Function Update(ByVal mAssessor As DataTable, ByVal mReviewer As DataTable) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            objDataOperation.BindTransaction()

            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessormasterunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@isreviewer", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsreviewer.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            'Shani(01-MAR-2016) -- Start
            'Enhancement :PA External Approver Flow
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalAssessorReviewer.ToString)
            'Shani(01-MAR-2016)-- End

            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            objDataOperation.AddParameter("@visibletypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibletypeid)
            'S.SANDEEP [27 DEC 2016] -- END



            strQ = "UPDATE hrassessor_master SET " & _
                      "  employeeunkid = @employeeunkid" & _
                      ", userunkid = @userunkid" & _
                      ", isreviewer = @isreviewer" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                      ", isexternalapprover = @isexternalapprover " & _
                      ", visibletypeid = @visibletypeid " & _
                   "WHERE assessormasterunkid = @assessormasterunkid " 'Shani(01-MAR-2016) -- [isexternalapprover] -- END
            'S.SANDEEP [27 DEC 2016] -- START {visibletypeid} -- END


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrassessor_master", mintAssessormasterunkid, "assessormasterunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassessor_master", "assessormasterunkid", mintAssessormasterunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            Dim objATran As New clsAssessor_tran
            If mAssessor IsNot Nothing Then
                If mAssessor.Rows.Count > 0 Then
                    objATran._DataTable = mAssessor
                    If objATran.Insert_Update_Delete(objDataOperation, mintAssessormasterunkid) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    End If
                End If
            End If

            If mReviewer IsNot Nothing Then
                If mReviewer.Rows.Count > 0 Then
                    objATran._DataTable = mReviewer
                    If objATran.Insert_Update_Delete(objDataOperation, mintAssessormasterunkid) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    End If
                End If
            End If


            'S.SANDEEP [ 16 JAN 2014 ] -- START
            Dim objUserMapping As New clsapprover_Usermapping
            objUserMapping.GetData(enUserType.Assessor, mintAssessormasterunkid, -1, -1)
            objUserMapping._Approverunkid = mintAssessormasterunkid
            objUserMapping._AuditUserunkid = mintUserunkid
            objUserMapping._UserTypeid = enUserType.Assessor
            objUserMapping._Userunkid = mintMappedUserId
            If objUserMapping._Mappingunkid > 0 Then
                If objUserMapping.Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                If objUserMapping.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'S.SANDEEP [ 16 JAN 2014 ] -- END



            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Update", mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> DELETE FROM Database Table (hrassessor_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOpr
        End If
        Try
            objDataOperation.BindTransaction()

            StrQ = "UPDATE hrassessor_master SET " & _
                   "  isvoid = @isvoid " & _
                   " ,voiddatetime = @voiddatetime " & _
                   " ,voidreason = @voidreason " & _
                   " ,voiduserunkid = @voiduserunkid " & _
                   " ,visibletypeid = @visibletypeid " & _
                   "WHERE assessormasterunkid = @assessormasterunkid "
            'S.SANDEEP [27 DEC 2016] -- START {visibletypeid} -- END



            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            'S.SANDEEP [27 DEC 2016] -- START
            'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
            objDataOperation.AddParameter("@visibletypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, enARVisibilityTypeId.NOT_VISIBLE)
            'S.SANDEEP [27 DEC 2016] -- END

            Call objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassessor_master", "assessormasterunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList = clsCommonATLog.GetChildList(objDataOperation, "hrassessor_tran", "assessormasterunkid", intUnkid)

            StrQ = "UPDATE hrassessor_tran SET " & _
                   "  isvoid = @isvoid " & _
                   " ,voiddatetime = @voiddatetime " & _
                   " ,voidreason = @voidreason " & _
                   " ,voiduserunkid = @voiduserunkid " & _
                   " ,visibletypeid = @visibletypeid " & _
                   "WHERE assessortranunkid = @assessortranunkid "
            'S.SANDEEP [27 DEC 2016] -- START {visibletypeid} -- END

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                    objDataOperation.AddParameter("@assessortranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("assessortranunkid"))
                    'S.SANDEEP [27 DEC 2016] -- START
                    'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
                    objDataOperation.AddParameter("@visibletypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, enARVisibilityTypeId.NOT_VISIBLE)
                    'S.SANDEEP [27 DEC 2016] -- END


                    Call objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassessor_tran", "assessortranunkid", dtRow.Item("assessortranunkid")) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next
            End If

            StrQ = "SELECT mappingunkid FROM hrapprover_usermapping WHERE approverunkid = '" & intUnkid & "' AND usertypeid = '" & enUserType.Assessor & "' "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows

                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrapprover_usermapping", "mappingunkid", dtRow.Item("mappingunkid")) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    StrQ = "DELETE FROM hrapprover_usermapping WHERE mappingunkid = '" & dtRow.Item("mappingunkid") & "' "

                    Call objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next
            End If


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "Delete", mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> CHECKING IF DATA IS USED FROM Database Table (hrassessor_master) </purpose>
    Public Function isUsed(ByVal iUnkid As Integer) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim iCnt As Integer = -1
        Try
            mstrMessage = ""

            'S.SANDEEP [18 Jan 2016] -- START
            'StrQ = "SELECT 1 FROM hrobjective_status_tran WHERE assessormasterunkid = '" & iUnkid & "'"
            StrQ = "SELECT 1 FROM hrassess_empstatus_tran WHERE assessormasterunkid = '" & iUnkid & "'"
            'S.SANDEEP [18 Jan 2016] -- END


            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt > 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this assessor/reviewer." & vbCrLf & _
                                                  "Reason : This assessor/reviewer has already approved/rejected bsc plan of some employee(s).")
                Return True
            End If

            'S.SANDEEP [18 Jan 2016] -- START
            'StrQ = "SELECT 1 FROM hrbsc_analysis_master WHERE assessormasterunkid = '" & iUnkid & "' AND isvoid = 0 "
            StrQ = "SELECT 1 FROM hrevaluation_analysis_master WHERE assessormasterunkid = '" & iUnkid & "' AND isvoid = 0 "
            'S.SANDEEP [18 Jan 2016] -- END

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt > 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this assessor/reviewer." & vbCrLf & _
                                                  "Reason : This assessor/reviewer has already done Assessment of some employee(s).")
                Return True
            End If

            'S.SANDEEP [20 Jan 2016] -- START
            'StrQ = "SELECT 1 FROM hrassess_analysis_master WHERE assessormasterunkid = '" & iUnkid & "' AND isvoid = 0 "

            'iCnt = objDataOperation.RecordCount(StrQ)

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'If iCnt > 0 Then
            '    mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, you cannot delete this assessor/reviewer." & vbCrLf & _
            '                                      "Reason : This assessor/reviewer has already done General Assessment of some employee(s).")
            '    Return True
            'End If
            'S.SANDEEP [20 Jan 2016] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isUsed", mstrModuleName)
        Finally
        End Try
    End Function


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            ByVal IsReviewer As Boolean, _
                            ByVal strVisibletypeids As String, _
                            Optional ByVal blnFlag As Boolean = False, _
                            Optional ByVal strFilterString As String = "", _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True) As DataSet
        'S.SANDEEP [27 DEC 2016] -- START {strVisibletypeids} -- END


        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            'S.SANDEEP [15 NOV 2016] -- START

            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            'S.SANDEEP [15 NOV 2016] -- END
            'Gajanan [11-Dec-2019] -- End


            Using objDo As New clsDataOperation

                'Shani(01-MAR-2016) -- Start
                'Enhancement :PA External Approver Flow
                'If blnFlag Then
                '    StrQ = "SELECT 0 AS assessormasterunkid,0 AS employeeunkid,0 AS userunkid,'' as employeecode,'" & IsReviewer & "' AS isreviewer,@Select AS assessorname,'' AS AssessIds,'' AS Department,0 AS DeptId,'' AS usermapped,0 AS mapuserunkid UNION "
                'End If

                'StrQ &= "SELECT " & _
                '        "    hrassessor_master.assessormasterunkid " & _
                '        "   ,hrassessor_master.employeeunkid " & _
                '        "   ,hrassessor_master.userunkid " & _
                '        "   ,hremployee_master.employeecode " & _
                '        "   ,isreviewer as isreviewer " & _
                '        "   ,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS assessorname "
                'If IsReviewer Then
                '    StrQ &= "   ,(SELECT STUFF((SELECT ',' + CAST(s.employeeunkid AS NVARCHAR(50)) FROM hrassessor_master s WHERE s.isreviewer = 1 AND s.isvoid = 0 ORDER BY s.assessormasterunkid FOR XML PATH('')),1,1,'')) AS AssessIds "
                'Else
                '    StrQ &= "   ,(SELECT STUFF((SELECT ',' + CAST(s.employeeunkid AS NVARCHAR(50)) FROM hrassessor_master s WHERE s.isreviewer = 0 AND s.isvoid = 0 ORDER BY s.assessormasterunkid FOR XML PATH('')),1,1,'')) AS AssessIds "
                'End If
                'StrQ &= "   ,hrdepartment_master.name AS Department " & _
                '        "   ,hrdepartment_master.departmentunkid AS DeptId " & _
                '        "   ,ISNULL(Usr.username,'') AS usermapped " & _
                '        "   ,ISNULL(hrapprover_usermapping.userunkid,0) AS mapuserunkid " & _
                '        "FROM hrassessor_master " & _
                '        "   JOIN hremployee_master ON hremployee_master.employeeunkid = hrassessor_master.employeeunkid " & _
                '        "   JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                '        "   LEFT JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
                '        "   LEFT JOIN hrmsConfiguration..cfuser_master AS Usr ON hrapprover_usermapping.userunkid = Usr.userunkid "

                ''If xDateJoinQry.Trim.Length > 0 Then
                ''    StrQ &= xDateJoinQry
                ''End If

                ''If xUACQry.Trim.Length > 0 Then
                ''   StrQ &= xUACQry
                ''End If

                ''If xAdvanceJoinQry.Trim.Length > 0 Then
                ''   StrQ &= xAdvanceJoinQry
                ''  End If

                'StrQ &= "WHERE 1 = 1 AND hrassessor_master.isvoid = 0 "

                'If IsReviewer Then
                '    StrQ &= " AND isreviewer = 1 "
                'Else
                '    StrQ &= " AND isreviewer = 0 "
                'End If

                ''If blnApplyUserAccessFilter = True Then
                ''    If xUACFiltrQry.Trim.Length > 0 Then
                ''       StrQ &= " AND " & xUACFiltrQry & " "
                ''    End If
                ''End If

                '' If xIncludeIn_ActiveEmployee = False Then
                ''     If xDateFilterQry.Trim.Length > 0 Then
                ''        StrQ &= xDateFilterQry & " "
                ''    End If
                '' End If

                'If strFilterString.Trim.Length > 0 Then
                '    StrQ &= "AND " & strFilterString
                'End If

                'objDo.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

                'dsList = objDo.ExecQuery(StrQ, strTableName)

                'If objDo.ErrorMessage <> "" Then
                '    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                '    Throw exForce
                'End If

                Dim StrDataJoin As String = ""

                'Gajanan [11-Dec-2019] -- Start   
                'Enhancement:Worked On December Cut Over Enhancement For NMB
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrDataJoin &= xAdvanceJoinQry
                End If
                'Gajanan [11-Dec-2019] -- End
                'S.SANDEEP [17-JUL-2017] -- END

                'S.SANDEEP [17-JUL-2017] -- START
                'ISSUE/ENHANCEMENT : WRONG DEPARTMENT COMING 
                StrDataJoin &= " LEFT JOIN " & _
                                            " ( " & _
                                            "    SELECT " & _
                                            "        departmentunkid " & _
                                            "        ,sectionunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM #DName#hremployee_transfer_tran WITH (NOLOCK) " & _
                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                                            " ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                                            " LEFT JOIN #DName#hrdepartment_master WITH (NOLOCK) on hrdepartment_master.departmentunkid = Alloc.departmentunkid "

			    



                Dim StrQFinalQry As String = String.Empty
                Dim StrQCommon As String = ""
                If blnFlag Then
                    StrQCommon = "SELECT 0 AS assessormasterunkid,0 AS employeeunkid,0 AS userunkid,'' as employeecode,'" & IsReviewer & "' AS isreviewer,@Select AS assessorname,'' AS AssessIds,'' AS Department,0 AS DeptId,'' AS usermapped,0 AS mapuserunkid,CAST(0 AS BIT) AS isexternalapprover,'' AS IsExAR,'" & enARVisibilityTypeId.VISIBLE & "' AS visibletypeid UNION "
                End If

                StrQ &= "SELECT " & _
                        "    hrassessor_master.assessormasterunkid " & _
                        "   ,hrassessor_master.employeeunkid " & _
                        "   ,hrassessor_master.userunkid " & _
                        "   ,#EMPL_CODE# AS employeecode " & _
                        "   ,isreviewer as isreviewer " & _
                        "   ,#ASSR_NAME# AS assessorname "

                If IsReviewer Then
                    StrQ &= "   ,(SELECT STUFF((SELECT ',' + CAST(s.employeeunkid AS NVARCHAR(50)) FROM hrassessor_master s WITH (NOLOCK) WHERE s.isreviewer = 1 AND s.isvoid = 0 ORDER BY s.assessormasterunkid FOR XML PATH('')),1,1,'')) AS AssessIds "
                Else
                    StrQ &= "   ,(SELECT STUFF((SELECT ',' + CAST(s.employeeunkid AS NVARCHAR(50)) FROM hrassessor_master s WITH (NOLOCK) WHERE s.isreviewer = 0 AND s.isvoid = 0 ORDER BY s.assessormasterunkid FOR XML PATH('')),1,1,'')) AS AssessIds "
                End If

                StrQ &= "   ,#DEPT_NAME# AS Department " & _
                        "   ,#DEPT_ID# AS DeptId " & _
                        "   ,ISNULL(Usr.username,'') AS usermapped " & _
                        "   ,ISNULL(hrapprover_usermapping.userunkid,0) AS mapuserunkid " & _
                        "   ,isexternalapprover " & _
                        "   ,CASE WHEN isexternalapprover = 1 THEN @Yes ELSE @No END AS IsExAR " & _
                        "   ,hrassessor_master.visibletypeid " & _
                        "FROM hrassessor_master WITH (NOLOCK) " & _
                        "   #EMPL_DATA_JOIN# " & _
                        "   LEFT JOIN hrapprover_usermapping WITH (NOLOCK) ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
                        "   LEFT JOIN hrmsConfiguration..cfuser_master AS Usr WITH (NOLOCK) ON hrapprover_usermapping.userunkid = Usr.userunkid "

                StrQ &= "WHERE 1 = 1 AND hrassessor_master.isvoid = 0 "

                If IsReviewer Then
                    StrQ &= " AND isreviewer = 1 "
                Else
                    StrQ &= " AND isreviewer = 0 "
                End If
                StrQ &= " AND hrassessor_master.isexternalapprover = #ExValue# "

                If strFilterString.Trim.Length > 0 Then
                    StrQ &= "AND " & strFilterString
                End If

                'S.SANDEEP [27 DEC 2016] -- START
                'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
                If strVisibletypeids.Trim.Length > 0 Then
                    StrQ &= " AND hrassessor_master.visibletypeid IN (" & strVisibletypeids & ")"
                End If
                'S.SANDEEP [27 DEC 2016] -- END


                StrQFinalQry = StrQCommon
                StrQFinalQry &= StrQ

                StrQFinalQry = StrQFinalQry.Replace("#EMPL_CODE#", "hremployee_master.employeecode ")
                StrQFinalQry = StrQFinalQry.Replace("#ASSR_NAME#", "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') ")
                StrQFinalQry = StrQFinalQry.Replace("#DEPT_NAME#", "hrdepartment_master.name ")
                StrQFinalQry = StrQFinalQry.Replace("#DEPT_ID#", "hrdepartment_master.departmentunkid ")
                StrQFinalQry = StrQFinalQry.Replace("#ExValue#", "0")

                'S.SANDEEP [17-JUL-2017] -- START
                'ISSUE/ENHANCEMENT : WRONG DEPARTMENT COMING
                'StrQFinalQry = StrQFinalQry.Replace("#EMPL_DATA_JOIN#", " JOIN hremployee_master ON hremployee_master.employeeunkid = hrassessor_master.employeeunkid " & _
                '                                                        " JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid ")
                StrQFinalQry = StrQFinalQry.Replace("#EMPL_DATA_JOIN#", " JOIN hremployee_master WITH (NOLOCK) ON hremployee_master.employeeunkid = hrassessor_master.employeeunkid " & _
                                                                         StrDataJoin)
                StrQFinalQry = StrQFinalQry.Replace("#DName#", "")
                'S.SANDEEP [17-JUL-2017] -- END


                

                objDo.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
                objDo.AddParameter("@Yes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Yes"))
                objDo.AddParameter("@No", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "No"))

                dsList = objDo.ExecQuery(StrQFinalQry, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

                Dim dsExAR As New DataSet

                'Shani(01-MAR-2016) -- Start
                'Enhancement  :PA External Approver Flow
                'dsExAR = GetExternalAssessorReviewerList(objDo, "List")
                dsExAR = GetExternalAssessorReviewerList(objDo, "List", , , , , IIf(IsReviewer, enAssessorType.REVIEWER, enAssessorType.ASSESSOR))
                'Shani(01-MAR-2016)-- End


                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

                For Each dr In dsExAR.Tables("List").Rows
                    Dim dstmp As New DataSet

                    StrQFinalQry = StrQ

                    If dr("DBName").ToString.Trim.Length <= 0 AndAlso dr("companyunkid") <= 0 Then
                        StrQFinalQry = StrQFinalQry.Replace("#EMPL_CODE#", "'' ")
                        StrQFinalQry = StrQFinalQry.Replace("#ASSR_NAME#", "ISNULL(UEmp.username,'') ")
                        StrQFinalQry = StrQFinalQry.Replace("#DEPT_NAME#", "'' ")
                        StrQFinalQry = StrQFinalQry.Replace("#DEPT_ID#", "0 ")
                        StrQFinalQry = StrQFinalQry.Replace("#ExValue#", "1")
                        StrQFinalQry = StrQFinalQry.Replace("#EMPL_DATA_JOIN#", " JOIN hrmsConfiguration..cfuser_master AS UEmp WITH (NOLOCK) ON UEmp.userunkid = hrassessor_master.employeeunkid ")
                    Else

                        StrQFinalQry = StrQFinalQry.Replace("#EMPL_CODE#", "hremployee_master.employeecode ")
                        StrQFinalQry = StrQFinalQry.Replace("#ASSR_NAME#", "ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') ")
                        StrQFinalQry = StrQFinalQry.Replace("#DEPT_NAME#", "hrdepartment_master.name ")
                        StrQFinalQry = StrQFinalQry.Replace("#DEPT_ID#", "hrdepartment_master.departmentunkid ")
                        StrQFinalQry = StrQFinalQry.Replace("#ExValue#", "1")
                        StrQFinalQry = StrQFinalQry.Replace("#EMPL_DATA_JOIN#", " JOIN hrmsConfiguration..cfuser_master AS UEmp WITH (NOLOCK) ON UEmp.userunkid = hrassessor_master.employeeunkid " & _
                                                                                " JOIN #DName#hremployee_master WITH (NOLOCK) ON hremployee_master.employeeunkid = UEmp.employeeunkid " & _
                                                                                StrDataJoin)
                        StrQFinalQry = StrQFinalQry.Replace("#DName#", dr("DBName") & "..")
                    End If

                    StrQFinalQry &= " AND UEmp.companyunkid = '" & dr("companyunkid") & "' "

                    objDo.ClearParameters()
                    objDo.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
                    objDo.AddParameter("@Yes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Yes"))
                    objDo.AddParameter("@No", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "No"))

                    dstmp = objDo.ExecQuery(StrQFinalQry, "List")

                    If objDo.ErrorMessage <> "" Then
                        exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList.Tables.Count <= 0 Then
                        dsList.Tables.Add(dstmp.Tables(0).Copy)
                    Else
                        dsList.Tables(0).Merge(dstmp.Tables(0), True)
                    End If
                Next
                'Shani(01-MAR-2016)-- End
            End Using

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

        'Shani(01-MAR-2016) -- Start
        'Enhancement :PA External Approver Flow
        If dsList IsNot Nothing Then
            'assessormasterunkid
            Dim dTemp As New DataSet
            dTemp.Tables.Add(New DataView(dsList.Tables(0), "", "assessormasterunkid", DataViewRowState.CurrentRows).ToTable)
            dsList = dTemp
            dsList.Tables(0).TableName = strTableName
        End If
        'Shani(01-MAR-2016) -- End

        Return dsList
    End Function

    'Public Function GetList(ByVal strTableName As String, _
    '                        ByVal IsReviewer As Boolean, _
    '                        Optional ByVal blnFlag As Boolean = False, _
    '                        Optional ByVal strIncludeInactiveEmployee As String = "", _
    '                        Optional ByVal strEmployeeAsOnDate As String = "", _
    '                        Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation
    '    Try
    '        If blnFlag Then
    '            strQ = "SELECT 0 AS assessormasterunkid,0 AS employeeunkid,0 AS userunkid,'' as employeecode,'" & IsReviewer & "' AS isreviewer,@Select AS assessorname,'' AS AssessIds,'' AS Department,0 AS DeptId,'' AS usermapped,0 AS mapuserunkid UNION "
    '        End If 'Shani [ 10 DEC 2014 ] -- START {mapuserunkid} -- END
    '        strQ &= "SELECT " & _
    '                " hrassessor_master.assessormasterunkid " & _
    '                ",hrassessor_master.employeeunkid " & _
    '                ",hrassessor_master.userunkid " & _
    '                ",hremployee_master.employeecode " & _
    '                ",isreviewer as isreviewer " & _
    '                ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS assessorname "
    '        If IsReviewer Then
    '            strQ &= ",(SELECT STUFF((SELECT ',' + CAST(s.employeeunkid AS NVARCHAR(50)) FROM hrassessor_master s WHERE s.isreviewer = 1 AND s.isvoid = 0 ORDER BY s.assessormasterunkid FOR XML PATH('')),1,1,'')) AS AssessIds "
    '        Else
    '            strQ &= ",(SELECT STUFF((SELECT ',' + CAST(s.employeeunkid AS NVARCHAR(50)) FROM hrassessor_master s WHERE s.isreviewer = 0 AND s.isvoid = 0 ORDER BY s.assessormasterunkid FOR XML PATH('')),1,1,'')) AS AssessIds "
    '        End If
    '        strQ &= ",hrdepartment_master.name AS Department " & _
    '                ",hrdepartment_master.departmentunkid AS DeptId " & _
    '                ",ISNULL(Usr.username,'') AS usermapped " & _
    '                ",ISNULL(hrapprover_usermapping.userunkid,'') AS mapuserunkid "
    '        strQ &= "FROM hrassessor_master " & _
    '                "   JOIN hremployee_master ON hremployee_master.employeeunkid = hrassessor_master.employeeunkid " & _
    '                "   JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
    '                "   LEFT JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid " & _
    '                " AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
    '                "   LEFT JOIN hrmsConfiguration..cfuser_master AS Usr ON hrapprover_usermapping.userunkid = Usr.userunkid " & _
    '                "WHERE 1 = 1 AND hrassessor_master.isvoid = 0 "
    '        'Shani [ 10 DEC 2014 ] -- START {mapuserunkid} -- END

    '        If IsReviewer Then
    '            strQ &= " AND isreviewer = 1 "
    '        Else
    '            strQ &= " AND isreviewer = 0 "
    '        End If

    '        If strIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '        End If

    '        If CBool(strIncludeInactiveEmployee) = False Then
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                    " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '        End If

    '        If strUserAccessLevelFilterString.Trim.Length <= 0 Then
    '            'Sohail (08 May 2015) -- Start
    '            'Enhancement - Get User Access Filter string from new employee transfer table and employee categorization table.
    '            'strUserAccessLevelFilterString = UserAccessLevel._AccessLevelFilterString
    '            'strUserAccessLevelFilterString = NewAccessLevelFilterString()
    '            'Sohail (08 May 2015) -- End
    '        End If

    '        'strQ &= strUserAccessLevelFilterString


    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    'S.SANDEEP [04 JUN 2015] -- END


    'Shani(01-MAR-2016) -- Start
    'Enhancement :PA External Approver Flow
    'Public Function GetAssessorMasterId(ByVal iEmpId As Integer, ByVal IsRewiver As Boolean) As Integer
    Public Function GetAssessorMasterId(ByVal iEmpId As Integer, ByVal IsRewiver As Boolean, ByVal blnIsExternal As Boolean) As Integer
        'Shani(01-MAR-2016)-- End

        Dim StrQ As String = String.Empty
        Dim iValue As Integer = 0
        Dim dsList As New DataSet
        Try
            Dim objDataOperation As New clsDataOperation

            StrQ = "SELECT assessormasterunkid FROM hrassessor_master WHERE isvoid = 0 AND employeeunkid = '" & iEmpId & "' "

            If IsRewiver = True Then
                StrQ &= " AND isreviewer = 1 "
            Else
                StrQ &= " AND isreviewer = 0 "
            End If

            'Shani(01-MAR-2016) -- Start
            'Enhancement :PA External Approver Flow
            StrQ &= " AND isexternalapprover = @isexternalapprover "
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsExternal)
            'Shani(01-MAR-2016)-- End

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                iValue = dsList.Tables("List").Rows(0).Item("assessormasterunkid")
            End If

            Return iValue

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAssessorMasterId", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function IsEmpMapped(ByVal iAssessorMasterId As Integer, ByVal iEmpId As Integer, ByVal intVisibilityTypeId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Try
            Dim objDataOperation As New clsDataOperation

            StrQ = "SELECT 1 FROM hrassessor_tran WHERE isvoid = 0 AND assessormasterunkid = '" & iAssessorMasterId & "' AND employeeunkid = '" & iEmpId & "' " & _
                   " AND visibletypeid = '" & intVisibilityTypeId & "'"

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                blnFlag = True
            End If

            Return blnFlag

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsEmpMapped", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function IsMappedToReviewer(ByVal iEmpId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim iCnt As Integer = 0
        Dim iFlag As Boolean = False
        Try
            Using objDataOperation As New clsDataOperation
                'S.SANDEEP |04-SEP-2019| -- START
                'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                'iCnt = objDataOperation.RecordCount("SELECT 1 FROM hrassessor_tran " & _
                '               "    JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                '                                    "WHERE isreviewer = 1 AND hrassessor_tran.employeeunkid = '" & iEmpId & "' AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 ")


                iCnt = objDataOperation.RecordCount("SELECT 1 FROM hrassessor_tran " & _
                               "    JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                                                    "WHERE isreviewer = 1 AND hrassessor_tran.employeeunkid = '" & iEmpId & "' AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
                                                    "AND hrassessor_master.visibletypeid = " & CInt(clsAssessor.enARVisibilityTypeId.VISIBLE) & " AND hrassessor_tran.visibletypeid = " & CInt(clsAssessor.enARVisibilityTypeId.VISIBLE))
                'S.SANDEEP |04-SEP-2019| -- END

                
                If iCnt > 0 Then
                    iFlag = True
                End If
            End Using
            Return iFlag
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsMappedToReviewer", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Get_CSV_Employee(ByVal iUserId As Integer, ByVal iReviewer As Boolean) As String
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim iValue As String = String.Empty
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            If iReviewer = True Then
                StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(hrassessor_tran.employeeunkid AS NVARCHAR(MAX)) FROM hrassessor_tran " & _
                   " JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                   " JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid " & _
                   "WHERE usertypeid = '" & enUserType.Assessor & "' AND isreviewer = 1 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 AND hrapprover_usermapping.userunkid = '" & iUserId & "' FOR XML PATH('')),1,1,''),0) AS cEmp "
            Else
                StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(hrassessor_tran.employeeunkid AS NVARCHAR(MAX)) FROM hrassessor_tran " & _
                   " JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                   " JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid " & _
                   "WHERE usertypeid = '" & enUserType.Assessor & "' AND isreviewer = 0 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 AND hrapprover_usermapping.userunkid = '" & iUserId & "' FOR XML PATH('')),1,1,''),0) AS cEmp "
            End If

            'S.SANDEEP [ 16 JUL 2014 ] -- START
            'AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 ---------- ADDED
            'S.SANDEEP [ 16 JUL 2014 ] -- END


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                iValue = dsList.Tables(0).Rows(0)("cEmp")
            End If

            Return iValue
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_CSV_Employee", mstrModuleName)
            Return ""
        Finally
        End Try
    End Function

    'Shani(01-MAR-2016) -- Start
    'Enhancement :PA External Approver Flow

    Public Function GetEmailNotificationList(ByVal intEmployeeId As Integer, _
                                             ByVal blnIsReviewer As Boolean, _
                                             Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                                             Optional ByVal strListName As String = "List") As DataSet
        Dim objDataOperation As clsDataOperation
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsNtfList As DataSet = Nothing

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            Dim dsList As New DataSet
            StrQ = "SELECT DISTINCT " & _
                   "     ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                   "    ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                   "    ,hrassessor_master.isexternalapprover " & _
                   "FROM hrassessor_master WITH (NOLOCK) " & _
                   "    JOIN hrmsConfiguration..cfuser_master WITH (NOLOCK) ON cfuser_master.userunkid = hrassessor_master.employeeunkid " & _
                   "    JOIN hrassessor_tran WITH (NOLOCK) ON hrassessor_tran.assessormasterunkid  = hrassessor_master.assessormasterunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran WITH (NOLOCK) ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                   "WHERE hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 AND hrassessor_master.isexternalapprover = 1 AND hrassessor_tran.employeeunkid = @EmployeeId AND hrassessor_master.isreviewer = @isreviewer " & _
                   "    AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' AND hrassessor_master.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' " & _
                   "UNION " & _
                   "SELECT DISTINCT " & _
                   "     ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
                   "    ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                   "    ,hrassessor_master.isexternalapprover " & _
                   "FROM hrassessor_master WITH (NOLOCK) " & _
                   "    JOIN hremployee_master WITH (NOLOCK) ON hremployee_master.employeeunkid = hrassessor_master.employeeunkid " & _
                   "    JOIN hrassessor_tran WITH (NOLOCK) ON hrassessor_tran.assessormasterunkid  = hrassessor_master.assessormasterunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran WITH (NOLOCK) ON cffinancial_year_tran.companyunkid = hremployee_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                   "WHERE hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 AND hrassessor_master.isexternalapprover = 0 AND hrassessor_tran.employeeunkid = @EmployeeId AND hrassessor_master.isreviewer = @isreviewer " & _
                   "    AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' AND hrassessor_master.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' "
            'S.SANDEEP [27 DEC 2016] -- START {visibletypeid} -- END


            objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@isreviewer", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsReviewer)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            StrQ = "SELECT " & _
                   "     #EMPL_NAME# AS Ename " & _
                   "    ,#EMP_EMAIL# AS Email " & _
                   "    ,hrassessor_master.assessormasterunkid " & _
                   "    ,hrapprover_usermapping.userunkid " & _
                   "    ,hrassessor_master.isexternalapprover " & _
                   "    ,ISNULL(hremp.firstname,'')+' '+ISNULL(hremp.surname,'') AS eename " & _
                   "    ,ISNULL(hremp.email,'') AS eemail " & _
                   "FROM hrassessor_master WITH (NOLOCK) " & _
                   "    #EMPL_JOIN# " & _
                   "    JOIN hrassessor_tran WITH (NOLOCK) ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                   "    JOIN hremployee_master AS hremp WITH (NOLOCK) ON hremp.employeeunkid = hrassessor_tran.employeeunkid " & _
                   "    JOIN hrapprover_usermapping WITH (NOLOCK) ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                   "WHERE hrassessor_master.isvoid = 0 AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
                   "AND hrassessor_tran.employeeunkid = @EmployeeId AND hrassessor_master.isreviewer = @isreviewer AND hrassessor_tran.isvoid = 0 " & _
                   "AND hrassessor_master.isexternalapprover = #EXVALUE# AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' AND hrassessor_master.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' " 'S.SANDEEP [27 DEC 2016] -- START {visibletypeid} -- END

            'S.SANDEEP |18-JAN-2019| -- START
            Dim oStQ As String = String.Empty
            oStQ = StrQ
            oStQ = oStQ.Replace("#EMPL_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') ")
            oStQ = oStQ.Replace("#EMP_EMAIL#", "ISNULL(hremployee_master.email,'') ")
            oStQ = oStQ.Replace("#EMPL_JOIN#", "JOIN hremployee_master WITH (NOLOCK) ON hremployee_master.employeeunkid = hrassessor_master.employeeunkid ")
            oStQ = oStQ.Replace("#EXVALUE#", "0")

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@isreviewer", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsReviewer)

            dsNtfList = objDataOperation.ExecQuery(oStQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP |18-JAN-2019| -- END

            For Each dr In dsList.Tables(0).Rows
                Dim dstmp As New DataSet
                Dim StrDBName As String : Dim StrFinalQry As String = "" : StrDBName = ""
                StrDBName = dr("DName")
                StrFinalQry = StrQ
                If StrDBName.Trim.Length <= 0 And dr("companyunkid") <= 0 Then
                    StrFinalQry = StrFinalQry.Replace("#EMPL_NAME#", "CASE WHEN ISNULL(Usr.firstname,'')+' '+ISNULL(Usr.lastname,'') = ' ' THEN ISNULL(Usr.username,'') ELSE ISNULL(Usr.firstname,'')+' '+ISNULL(Usr.lastname,'') END ")
                    StrFinalQry = StrFinalQry.Replace("#EMP_EMAIL#", "ISNULL(Usr.email,'') ")
                    StrFinalQry = StrFinalQry.Replace("#EMPL_JOIN#", "JOIN hrmsConfiguration..cfuser_master AS Usr WITH (NOLOCK) ON Usr.userunkid = hrassessor_master.employeeunkid ")
                Else
                    If dr("isexternalapprover") = False Then
                        StrFinalQry = StrFinalQry.Replace("#EMPL_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') ")
                        StrFinalQry = StrFinalQry.Replace("#EMP_EMAIL#", "ISNULL(hremployee_master.email,'') ")
                        'Shani (28-Apr-2016) -- Start
                        'StrFinalQry = StrFinalQry.Replace("#EMPL_JOIN#", "JOIN " & StrDBName & "..hremployee_master ON hremployee_master.employeeunkid = hrassessor_master.employeeunkid ")
                        StrFinalQry = StrFinalQry.Replace("#EMPL_JOIN#", "JOIN hremployee_master WITH (NOLOCK) ON hremployee_master.employeeunkid = hrassessor_master.employeeunkid ")
                        'Shani (28-Apr-2016) -- End

                    Else
                        StrFinalQry = StrFinalQry.Replace("#EMPL_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                        StrFinalQry = StrFinalQry.Replace("#EMP_EMAIL#", "CASE WHEN ISNULL(hremployee_master.email,'') = '' THEN ISNULL(cfuser_master.email,'') ELSE ISNULL(hremployee_master.email,'') END ")
                        StrFinalQry = StrFinalQry.Replace("#EMPL_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master WITH (NOLOCK) ON cfuser_master.userunkid = hrassessor_master.employeeunkid " & _
                                                                       "LEFT JOIN " & StrDBName & "..hremployee_master WITH (NOLOCK) ON hremployee_master.employeeunkid = cfuser_master.employeeunkid AND hremployee_master.isapproved = 1 ")
                    End If
                End If

                'Gajanan [26-Feb-2019] -- Start
                'Enhancement - Email Language Changes For NMB.
                'If dr("isexternalapprover") = True Then
                '    StrFinalQry = StrFinalQry.Replace("#EXVALUE#", "1")
                'Else
                '    StrFinalQry = StrFinalQry.Replace("#EXVALUE#", "0")
                'End If
                    StrFinalQry = StrFinalQry.Replace("#EXVALUE#", "1")
                'Gajanan [26-Feb-2019] -- End

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@EmployeeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
                objDataOperation.AddParameter("@isreviewer", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsReviewer)

                dstmp = objDataOperation.ExecQuery(StrFinalQry, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsNtfList Is Nothing Then dsNtfList = New DataSet

                If dsNtfList.Tables.Count <= 0 Then
                    dsNtfList.Tables.Add(dstmp.Tables(0).Copy)
                Else
                    dsNtfList.Tables(0).Merge(dstmp.Tables(0), True)
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:GetEmailNotificationList ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsNtfList
    End Function


    Public Function GetExternalAssessorReviewerList(Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                                                    Optional ByVal StrListName As String = "List", _
                                                    Optional ByVal intAsRMstId As Integer = 0, _
                                                    Optional ByVal intAsEmpId As Integer = 0, _
                                                    Optional ByVal intEmpId As Integer = 0, _
                                                    Optional ByVal intUserId As Integer = 0, _
                                                    Optional ByVal eAssrType As enAssessorType = enAssessorType.ALL_DATA) As DataSet
        Dim objDataOperation As clsDataOperation
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsExternal As New DataSet

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            If StrListName.Trim.Length <= 0 Then StrListName = "List"

            'StrQ = "SELECT DISTINCT " & _
            '       "     ISNULL(cffinancial_year_tran.database_name,'') AS DName " & _
            '       "    ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
            '       "FROM hrassessor_master " & _
            '       "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = hrassessor_master.employeeunkid " & _
            '       "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
            '       "WHERE hrassessor_master.isvoid = 0 AND hrassessor_master.isexternalapprover = 1 "
            StrQ = "SELECT DISTINCT " & _
                   "     cfuser_master.companyunkid " & _
                   "    ,ISNULL(cffinancial_year_tran.database_name,'') AS DBName " & _
                   "    ,ISNULL(EffDt.key_value,CONVERT(CHAR(8),GETDATE(),112)) AS EMPASONDATE "
            If intAsRMstId > 0 Then
                StrQ &= "    ,hrassessor_master.assessormasterunkid "
            Else
                StrQ &= "    ,0 AS assessormasterunkid "
            End If
            StrQ &= "    ,cffinancial_year_tran.yearunkid " & _
                    "    ,ISNULL(UC.key_value,'') AS ModeSet " & _
                    "    ,hrassessor_master.isexternalapprover " & _
                    "    ,hrassessor_master.isreviewer " & _
                    "FROM hrassessor_master WITH (NOLOCK) " & _
                    "    JOIN hrapprover_usermapping WITH (NOLOCK) ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
                    "    JOIN hrmsConfiguration..cfuser_master WITH (NOLOCK) on cfuser_master.userunkid = hrassessor_master.employeeunkid " & _
                    "    JOIN hrassessor_tran WITH (NOLOCK) ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid AND hrassessor_tran.isvoid = 0 " & _
                    "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran WITH (NOLOCK) ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid AND cffinancial_year_tran.isclosed = 0 " & _
                    "    LEFT JOIN hrmsConfiguration..cfconfiguration AS EffDt WITH (NOLOCK) ON EffDt.companyunkid =cfuser_master.companyunkid AND  UPPER(EffDt.key_name) = 'EMPLOYEEASONDATE' " & _
                    "    LEFT JOIN hrmsConfiguration..cfconfiguration AS UC WITH (NOLOCK) ON UC.companyunkid =cfuser_master.companyunkid AND  UPPER(UC.key_name) = 'USERACCESSMODESETTING' " & _
                    "WHERE hrassessor_master.isexternalapprover = 1 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' AND hrassessor_master.visibletypeid = '" & clsAssessor.enARVisibilityTypeId.VISIBLE & "' " 'S.SANDEEP [27 DEC 2016] -- START {visibletypeid} -- END

            'Shani (24-Oct-2016) -- ADD -- [hrassessor_master.isreviewer]

            If intAsRMstId > 0 Then
                StrQ &= " AND hrassessor_master.assessormasterunkid = '" & intAsRMstId & "' "
            End If

            Select Case eAssrType
                Case enAssessorType.ASSESSOR
                    StrQ &= " AND hrassessor_master.isreviewer = 0 "
                Case enAssessorType.REVIEWER
                    StrQ &= " AND hrassessor_master.isreviewer = 1 "
            End Select

            If intAsEmpId > 0 Then
                StrQ &= " AND hrassessor_master.employeeunkid = '" & intAsEmpId & "' "
            End If

            If intEmpId > 0 Then
                StrQ &= " AND hrassessor_tran.employeeunkid = '" & intEmpId & "' "
            End If

            If intUserId > 0 Then
                StrQ &= " AND hrapprover_usermapping.userunkid = '" & intUserId & "' "
            End If
            dsExternal = objDataOperation.ExecQuery(StrQ, StrListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:GetExternalAssessorReviewerList ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsExternal
    End Function

    Public Function IsExist(ByVal intAsrEmpId As Integer, _
                            ByVal blnReviewer As Boolean, _
                            ByVal blnExternal As Integer, _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim objDataOperation As clsDataOperation
        Dim StrQ As String = String.Empty
        Dim blnFlag As Boolean = False
        Dim iCnt As Integer = -1
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try

            'S.SANDEEP [22-JUN-2017] -- START
            'StrQ = "SELECT " & _
            '       "    hrassessor_master.assessormasterunkid " & _
            '       "FROM hrassessor_master WHERE hrassessor_master.isvoid = 0 " & _
            '       "AND hrassessor_master.employeeunkid = @EmpId AND hrassessor_master.isreviewer = @isreviewer " & _
            '       "AND hrassessor_master.isexternalapprover  = @isexternalapprover "

            StrQ = "SELECT " & _
                   "    hrassessor_master.assessormasterunkid " & _
                   "FROM hrassessor_master WITH (NOLOCK) WHERE hrassessor_master.isvoid = 0 " & _
                   "AND hrassessor_master.employeeunkid = @EmpId AND hrassessor_master.isreviewer = @isreviewer " & _
                   "AND hrassessor_master.isexternalapprover  = @isexternalapprover AND hrassessor_master.visibletypeid = @visibletypeid "
            'S.SANDEEP [22-JUN-2017] -- END

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAsrEmpId)
            objDataOperation.AddParameter("@isreviewer", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnReviewer)
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnExternal)
            'S.SANDEEP [22-JUN-2017] -- START
            objDataOperation.AddParameter("@visibletypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, clsAssessor.enARVisibilityTypeId.VISIBLE)
            'S.SANDEEP [22-JUN-2017] -- END

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If iCnt > 0 Then blnFlag = True

        Catch ex As Exception
            blnFlag = True
            Throw New Exception(ex.Message & "; Procedure Name:IsExist ; Module Name: " & mstrModuleName)
        Finally
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return blnFlag
    End Function

    'Shani(01-MAR-2016)-- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "Select")
            Language.setMessage(mstrModuleName, 3, "Sorry, you cannot delete this assessor/reviewer." & vbCrLf & _
                                                           "Reason : This assessor/reviewer has already approved/rejected bsc plan of some employee(s).")
            Language.setMessage(mstrModuleName, 4, "Sorry, you cannot delete this assessor/reviewer." & vbCrLf & _
                                                           "Reason : This assessor/reviewer has already done Assessment of some employee(s).")
            Language.setMessage(mstrModuleName, 6, "Sorry, this assessor/reviewer is already defined. Please define new assessor/reviewer.")
            Language.setMessage(mstrModuleName, 7, "No")
            Language.setMessage(mstrModuleName, 8, "Yes")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Public Class clsAssessor

'#Region " Private Variables "
'    Private Shared ReadOnly mstrModuleName As String = "clsAssessor"
'#End Region

'#Region " Property Variables "

'    Private mintEmployeeId As Integer = -1
'    Private mintUserunkid As Integer = -1
'    Private mstrassessormasterIds As String = String.Empty
'    Private mstrMessage As String = String.Empty
'    Private mblnIsreviewer As Boolean = False
'    'S.SANDEEP [ 18 APRIL 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private mblnIsvoid As Boolean = False
'    Private mdtVoiddatetime As Date = Nothing
'    Private mintVoiduserunkid As Integer = -1
'    Private mstrVoidreason As String = String.Empty
'    'S.SANDEEP [ 18 APRIL 2012 ] -- END

'#End Region

'#Region " Properties "

'    Public Property _EmployeeId() As Integer
'        Get
'            Return mintEmployeeId
'        End Get
'        Set(ByVal value As Integer)
'            mintEmployeeId = value
'        End Set
'    End Property

'    Public Property _Userunkid() As Integer
'        Get
'            Return mintUserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintUserunkid = value
'        End Set
'    End Property

'    Public Property _AssessormasterIds() As String
'        Get
'            Return mstrassessormasterIds
'        End Get
'        Set(ByVal value As String)
'            mstrassessormasterIds = value
'        End Set
'    End Property

'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    Public Property _Isreviewer() As Boolean
'        Get
'            Return mblnIsreviewer
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsreviewer = value
'        End Set
'    End Property

'    'S.SANDEEP [ 18 APRIL 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    ''' <summary>
'    ''' Purpose: Get or Set isvoid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Isvoid() As Boolean
'        Get
'            Return mblnIsvoid
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsvoid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiddatetime
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voiddatetime() As Date
'        Get
'            Return mdtVoiddatetime
'        End Get
'        Set(ByVal value As Date)
'            mdtVoiddatetime = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voiduserunkid
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voiduserunkid() As Integer
'        Get
'            Return mintVoiduserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintVoiduserunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set voidreason
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public Property _Voidreason() As String
'        Get
'            Return mstrVoidreason
'        End Get
'        Set(ByVal value As String)
'            mstrVoidreason = value
'        End Set
'    End Property
'    'S.SANDEEP [ 18 APRIL 2012 ] -- END

'#End Region

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (hrassessor_master) </purpose>
'    Public Function Insert() As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Dim objDataOperation As New clsDataOperation
'        Try
'            objDataOperation.BindTransaction()

'            'S.SANDEEP [ 18 APRIL 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'strQ = "INSERT INTO hrassessor_master ( " & _
'            '           "  employeeunkid " & _
'            '           ", userunkid " & _
'            '           ", isreviewer" & _
'            '      ") VALUES (" & _
'            '           "  @employeeunkid " & _
'            '           ", @userunkid " & _
'            '           ", @isreviewer" & _
'            '      "); SELECT @@identity"


'            strQ = "INSERT INTO hrassessor_master ( " & _
'                        "  employeeunkid " & _
'                        ", userunkid" & _
'                        ", isreviewer" & _
'                       ", isvoid " & _
'                       ", voiddatetime " & _
'                       ", voiduserunkid " & _
'                       ", voidreason" & _
'                   ") VALUES (" & _
'                        "  @employeeunkid " & _
'                        ", @userunkid" & _
'                        ", @isreviewer" & _
'                       ", @isvoid " & _
'                       ", @voiddatetime " & _
'                       ", @voiduserunkid " & _
'                       ", @voidreason" & _
'                   "); SELECT @@identity"
'            'S.SANDEEP [ 18 APRIL 2012 ] -- END

'            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
'            If mdtVoiddatetime = Nothing Then
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'            Else
'                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'            End If
'            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
'            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
'            'S.SANDEEP [ 18 APRIL 2012 ] -- END

'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
'                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
'            objDataOperation.AddParameter("@isreviewer", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsreviewer.ToString)

'                    dsList = objDataOperation.ExecQuery(strQ, "List")

'                    If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If

'                    Dim mintUnkID As Integer = CInt(dsList.Tables(0).Rows(0)(0))

'                    If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassessor_master", "assessormasterunkid", mintUnkID) = False Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If


'            objDataOperation.ReleaseTransaction(True)

'            Return True


'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> DELETE FROM Database Table (hrassessor_master) </purpose>
'    Public Function Delete() As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Dim IsExist As Boolean = False
'        Dim objDataOperation As New clsDataOperation
'        objDataOperation.BindTransaction()
'        Try

'            If mstrassessormasterIds.Length > 0 Then
'                For Each strIds As String In mstrassessormasterIds.Split(",")

'                    'S.SANDEEP [ 25 JULY 2013 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    'strQ = "Select isnull(assessoremployeeunkid,0) as assessorunkid  From hrassess_analysis_master where assessoremployeeunkid = @assessorunkid AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "
'                    strQ = "Select isnull(assessoremployeeunkid,0) as assessorunkid  From hrassess_analysis_master where assessormasterunkid = @assessorunkid AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "
'                    'S.SANDEEP [ 25 JULY 2013 ] -- END

'                    objDataOperation.ClearParameters()
'                    objDataOperation.AddParameter("@assessorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strIds.ToString)

'                    dsList = objDataOperation.ExecQuery(strQ, "List")

'                    If dsList.Tables("List").Rows.Count > 0 Then IsExist = True : Continue For

'                    If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If

'                    'S.SANDEEP [ 25 JULY 2013 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    'strQ = "Select isnull(reviewerunkid,0) as assessorunkid  From hrassess_analysis_master where reviewerunkid = @assessorunkid AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "
'                    strQ = "Select isnull(reviewerunkid,0) as assessorunkid  From hrassess_analysis_master where assessormasterunkid = @assessorunkid AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "
'                    'S.SANDEEP [ 25 JULY 2013 ] -- END

'                    objDataOperation.ClearParameters()
'                    objDataOperation.AddParameter("@assessorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strIds.ToString)

'                    dsList = objDataOperation.ExecQuery(strQ, "List")

'                    If dsList.Tables("List").Rows.Count > 0 Then IsExist = True : Continue For

'                    If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If

'                    'S.SANDEEP [ 25 JULY 2013 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    'strQ = "Select isnull(assessoremployeeunkid,0) as assessorunkid  From hrbsc_analysis_master where assessoremployeeunkid = @assessorunkid AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "
'                    strQ = "Select isnull(assessoremployeeunkid,0) as assessorunkid  From hrbsc_analysis_master where assessormasterunkid = @assessorunkid AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "
'                    'S.SANDEEP [ 25 JULY 2013 ] -- END


'                    objDataOperation.ClearParameters()
'                    objDataOperation.AddParameter("@assessorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strIds.ToString)

'                    dsList = objDataOperation.ExecQuery(strQ, "List")

'                    If dsList.Tables("List").Rows.Count > 0 Then IsExist = True : Continue For

'                    If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If

'                    'S.SANDEEP [ 25 JULY 2013 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    'strQ = "Select isnull(reviewerunkid,0) as assessorunkid  From hrbsc_analysis_master where reviewerunkid = @assessorunkid AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "
'                    strQ = "Select isnull(reviewerunkid,0) as assessorunkid  From hrbsc_analysis_master where assessormasterunkid = @assessorunkid AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "
'                    'S.SANDEEP [ 25 JULY 2013 ] -- END



'                    objDataOperation.ClearParameters()
'                    objDataOperation.AddParameter("@assessorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strIds.ToString)

'                    dsList = objDataOperation.ExecQuery(strQ, "List")

'                    If dsList.Tables("List").Rows.Count > 0 Then IsExist = True : Continue For

'                    If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If

'                    strQ = "select isnull(mappingunkid,0) mappingunkid  From hrapprover_usermapping where approverunkid = @approverunkid AND usertypeid = " & enUserType.Assessor

'                    objDataOperation.ClearParameters()
'                    objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strIds.ToString)

'                    Dim dtList As DataSet = objDataOperation.ExecQuery(strQ, "List")

'                    If dtList IsNot Nothing AndAlso dtList.Tables(0).Rows.Count > 0 Then
'                        If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrapprover_usermapping", "mappingunkid", CInt(dtList.Tables(0).Rows(0)("mappingunkid"))) = False Then
'                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                            Throw exForce
'                        End If
'                    End If


'                    strQ = "SELECT ISNULL(mappingunkid,0) mappingunkid FROM hrapprover_usermapping WHERE approverunkid = @approverunkid and Usertypeid =" & enUserType.Assessor

'                    objDataOperation.ClearParameters()
'                    objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strIds)
'                    dsList = objDataOperation.ExecQuery(strQ, "List")

'                    If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If


'                    'S.SANDEEP [ 19 JULY 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    If dtList IsNot Nothing AndAlso dtList.Tables(0).Rows.Count > 0 Then
'                        If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrapprover_usermapping", "mappingunkid", CInt(dsList.Tables(0).Rows(0)("mappingunkid"))) = False Then
'                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                            Throw exForce
'                        End If
'                    End If
'                    'S.SANDEEP [ 19 JULY 2012 ] -- END

'                    strQ = "Delete From hrapprover_usermapping where approverunkid = @approverunkid"

'                    objDataOperation.ClearParameters()
'                    objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strIds.ToString)

'                    objDataOperation.ExecNonQuery(strQ)

'                    If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If

'                    dtList = Nothing

'                    strQ = "select isnull(assessortranunkid,0) assessortranunkid  From hrassessor_tran where assessormasterunkid = @assessormasterunkid "

'                    objDataOperation.ClearParameters()
'                    objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strIds.ToString)

'                    dtList = objDataOperation.ExecQuery(strQ, "List")

'                    If dtList IsNot Nothing AndAlso dtList.Tables(0).Rows.Count > 0 Then
'                        For Each dr As DataRow In dtList.Tables(0).Rows
'                            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassessor_tran", "assessortranunkid", CInt(dr("assessortranunkid"))) = False Then
'                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                                Throw exForce
'                            End If
'                        Next
'                    End If


'                    'S.SANDEEP [ 18 APRIL 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    'strQ = "Delete From hrassessor_tran where assessormasterunkid = @assessormasterunkid"
'                    strQ = "UPDATE hrassessor_tran SET " & _
'                           " isvoid = 1 " & _
'                           ",voiddatetime = @voiddatetime " & _
'                           ",voiduserunkid = @voiduserunkid " & _
'                           ",voidreason = @voidreason " & _
'                           "WHERE assessormasterunkid = @assessormasterunkid"
'                    'S.SANDEEP [ 18 APRIL 2012 ] -- END


'                    objDataOperation.ClearParameters()
'                    objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strIds.ToString)
'                    'S.SANDEEP [ 18 APRIL 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
'                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
'                    'S.SANDEEP [ 18 APRIL 2012 ] -- END

'                    objDataOperation.ExecNonQuery(strQ)

'                    If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If

'                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassessor_master", "assessormasterunkid", CInt(strIds)) = False Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If



'                    'S.SANDEEP [ 18 APRIL 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    'strQ = "DELETE FROM hrassessor_master WHERE assessormasterunkid = @assessormasterunkid "
'                    strQ = "UPDATE hrassessor_master SET " & _
'                           " isvoid = 1 " & _
'                           ",voiddatetime = @voiddatetime " & _
'                           ",voiduserunkid = @voiduserunkid " & _
'                           ",voidreason = @voidreason " & _
'                           "WHERE assessormasterunkid = @assessormasterunkid "
'                    'S.SANDEEP [ 18 APRIL 2012 ] -- END

'                    objDataOperation.ClearParameters()
'                    objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strIds.ToString)
'                    'S.SANDEEP [ 18 APRIL 2012 ] -- START
'                    'ENHANCEMENT : TRA CHANGES
'                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
'                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
'                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
'                    'S.SANDEEP [ 18 APRIL 2012 ] -- END

'                    objDataOperation.ExecNonQuery(strQ)

'                    If objDataOperation.ErrorMessage <> "" Then
'                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        Throw exForce
'                    End If
'                Next
'            End If

'            mstrMessage = ""

'            If IsExist Then
'                mstrMessage = Language.getMessage(mstrModuleName, 1, "You can't delete some Assessor(s). Reason :Some of the Assessor(s) is already in used.")
'            End If

'            objDataOperation.ReleaseTransaction(True)

'            Return True

'        Catch ex As Exception
'            objDataOperation.ReleaseTransaction(False)
'            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
'            Return False
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(ByVal intEmployeeId As Integer, ByVal blnIsReviewer As Boolean) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception
'        Dim objDataOperation As New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                        "  assessormasterunkid " & _
'                        ", employeeunkid " & _
'                        ", userunkid " & _
'                       "FROM hrassessor_master " & _
'                       "WHERE employeeunkid = @EmpId AND hrassessor_master.isvoid = 0"

'            If blnIsReviewer Then
'                strQ &= " AND isreviewer = 1 "
'            Else
'                strQ &= " AND isreviewer = 0 "
'            End If

'            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If
'            If dsList.Tables("List").Rows.Count > 0 Then
'                Return True
'            Else
'                Return False
'            End If
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'            Return True
'        End Try
'    End Function

'#Region " List "

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, _
'                            ByVal IsReviewer As Boolean, _
'                            Optional ByVal blnFlag As Boolean = False, _
'                            Optional ByVal strIncludeInactiveEmployee As String = "", _
'                            Optional ByVal strEmployeeAsOnDate As String = "", _
'                            Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
'        'Public Function GetList(ByVal strTableName As String, ByVal IsReviewer As Boolean, Optional ByVal blnFlag As Boolean = False) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        Dim objDataOperation As New clsDataOperation
'        Try
'            If blnFlag Then
'                strQ = "SELECT 0 AS assessormasterunkid,0 AS employeeunkid,0 AS userunkid,'' as employeecode,'" & IsReviewer & "' AS isreviewer,@Select AS assessorname,'' AS AssessIds UNION "
'            End If

'            strQ &= "SELECT " & _
'                    " hrassessor_master.assessormasterunkid " & _
'                    ",hrassessor_master.employeeunkid " & _
'                    ",hrassessor_master.userunkid " & _
'                    ",hremployee_master.employeecode " & _
'                    ",isreviewer as isreviewer " & _
'                    ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS assessorname "
'            If IsReviewer Then
'                strQ &= ",(SELECT STUFF((SELECT ',' + CAST(s.employeeunkid AS NVARCHAR(50)) FROM hrassessor_master s WHERE s.isreviewer = 1 AND s.isvoid = 0 ORDER BY s.assessormasterunkid FOR XML PATH('')),1,1,'')) AS AssessIds "
'            Else
'                strQ &= ",(SELECT STUFF((SELECT ',' + CAST(s.employeeunkid AS NVARCHAR(50)) FROM hrassessor_master s WHERE s.isreviewer = 0 AND s.isvoid = 0 ORDER BY s.assessormasterunkid FOR XML PATH('')),1,1,'')) AS AssessIds "
'            End If
'            strQ &= "FROM hrassessor_master " & _
'                    "   JOIN hremployee_master ON hremployee_master.employeeunkid = hrassessor_master.employeeunkid " & _
'                    "   LEFT JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid " & _
'                    " AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
'                    "WHERE 1 = 1 AND hrassessor_master.isvoid = 0 "

'            ' "	JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid " & _ AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " " AND hrapprover_usermapping.userunkid = " & User._Object._Userunkid

'            If IsReviewer Then
'                strQ &= " AND isreviewer = 1 "
'            Else
'                strQ &= " AND isreviewer = 0 "
'            End If


'            'S.SANDEEP [ 27 APRIL 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then

'            '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

'            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'            'End If



'            'S.SANDEEP [ 16 MAY 2012 ] -- START
'            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
'            If strUserAccessLevelFilterString = "" Then
'                strQ &= UserAccessLevel._AccessLevelFilterString
'            Else
'                strQ &= strUserAccessLevelFilterString
'            End If
'            'S.SANDEEP [ 16 MAY 2012 ] -- END


'            ''S.SANDEEP [ 04 FEB 2012 ] -- START
'            ''ENHANCEMENT : TRA CHANGES
'            ''If UserAccessLevel._AccessLevel.Length > 0 Then
'            ''    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
'            ''End If
'            'Select Case ConfigParameter._Object._UserAccessModeSetting
'            '    Case enAllocation.BRANCH
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.DEPARTMENT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.SECTION
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.UNIT
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.TEAM
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOB_GROUP
'            '        If UserAccessLevel._AccessLevel.Length > 0 Then
'            '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
'            '        End If
'            '    Case enAllocation.JOBS
'            'If UserAccessLevel._AccessLevel.Length > 0 Then
'            '    strQ &= "  AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ")  "
'            'End If
'            'End Select

'            If strIncludeInactiveEmployee.Trim.Length <= 0 Then
'                strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
'            End If

'            If CBool(strIncludeInactiveEmployee) = False Then
'                strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (12 May 2012) - [empl_enddate]

'                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
'                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
'            End If


'            'S.SANDEEP [ 27 APRIL 2012 ] -- END



'            'S.SANDEEP [ 04 FEB 2012 ] -- END

'            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'            Return Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetCSVAssessorIds() As String
'        Dim strQ As String = String.Empty
'        Dim exForce As Exception
'        Dim dsList As DataSet = Nothing
'        Dim objDataOperation As New clsDataOperation
'        Dim strAssessorIds As String = ""
'        Try
'            strQ = "SELECT STUFF((SELECT ',' + CAST(s.employeeunkid AS NVARCHAR(50)) FROM hrassessor_master s WHERE s.isreviewer = 0 AND s.isvoid = 0 ORDER BY s.employeeunkid FOR XML PATH('')),1,1,'') AS AId "

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables(0).Rows.Count > 0 Then
'                strAssessorIds = dsList.Tables(0).Rows(0)("AId")
'            End If

'            Return strAssessorIds

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetCSVAssessorIds; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Function

'            'S.SANDEEP [ 23 JAN 2012 ] -- START
'            'ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
'    Public Function GetReviewerAccess(ByVal StrTabName As String, ByVal intEmpId As Integer, ByVal IsCheckReq As Boolean, ByVal intAssessorId As Integer, Optional ByVal isSameQuery As Boolean = False, Optional ByVal mstrAllocationFilter As String = "") As DataTable
'        Dim StrQ As String = String.Empty
'        Dim dTable As DataTable
'        Dim dMList As New DataSet
'        Dim dTList As New DataSet
'        Dim objDataOperation As New clsDataOperation
'        Try
'            dTable = New DataTable(StrTabName)

'            If IsCheckReq Then
'                dTable.Columns.Add("IsCheck", System.Type.GetType("System.Boolean")).DefaultValue = False
'            End If

'            dTable.Columns.Add("Employee", System.Type.GetType("System.String")).DefaultValue = ""

'            'Pinkal (12-Jun-2012) -- Start
'            'Enhancement : TRA Changes
'            'dTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
'            'Pinkal (12-Jun-2012) -- End
'            dTable.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1
'            dTable.Columns.Add("EmpId", System.Type.GetType("System.Int32")).DefaultValue = -1

'            If IsCheckReq = False Then
'                StrQ = "SELECT " & _
'                           "DISTINCT name ,hrdepartment_master.departmentunkid " & _
'                       "FROM hrdepartment_master " & _
'                           "JOIN hrassessor_tran ON hrdepartment_master.departmentunkid = hrassessor_tran.departmentunkid " & _
'                       "WHERE assessormasterunkid = '" & intAssessorId & "' AND hrdepartment_master.isactive  = 1 "

'                dMList = objDataOperation.ExecQuery(StrQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                End If

'                StrQ = "SELECT " & _
'                         "hrassessor_tran.departmentunkid " & _
'                         ",ISNULL(hremployee_master.employeecode,'')+' - '+ " & _
'                         " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EName " & _
'                         ",hremployee_master.employeeunkid as EmpId " & _
'                       "FROM hrassessor_tran " & _
'                       "    JOIN hremployee_master ON hrassessor_tran.employeeunkid = hremployee_master.employeeunkid " & _
'                       "WHERE assessormasterunkid = '" & intAssessorId & "' " & _
'                       " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate AND hrassessor_tran.isvoid = 0 " & _
'                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (12 May 2012) - [empl_enddate]

'                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

'                dTList = objDataOperation.ExecQuery(StrQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                End If



'                Dim dRow As DataRow = Nothing
'                'For Each dMRow As DataRow In dMList.Tables(0).Rows
'                '    dRow = dTable.NewRow
'                '    dRow.Item("Employee") = dMRow.Item("name")

'                '    'Pinkal (12-Jun-2012) -- Start
'                '    'Enhancement : TRA Changes
'                '    'dRow.Item("IsGrp") = True
'                '    'Pinkal (12-Jun-2012) -- End

'                '    dRow.Item("GrpId") = dMRow.Item("departmentunkid")
'                '    dRow.Item("EmpId") = -1
'                '    dTable.Rows.Add(dRow)
'                '    Dim dFilter As DataTable = New DataView(dTList.Tables(0), "departmentunkid = '" & dMRow.Item("departmentunkid") & "'", "", DataViewRowState.CurrentRows).ToTable
'                '    For Each dTRow As DataRow In dFilter.Rows
'                '        dRow = dTable.NewRow
'                '        dRow.Item("Employee") = Space(5) & dTRow.Item("EName")

'                '        'Pinkal (12-Jun-2012) -- Start
'                '        'Enhancement : TRA Changes
'                '        'dRow.Item("IsGrp") = False
'                '        'Pinkal (12-Jun-2012) -- End
'                '        dRow.Item("GrpId") = dMRow.Item("departmentunkid")
'                '        dRow.Item("EmpId") = dTRow.Item("EmpId")
'                '        dTable.Rows.Add(dRow)
'                '    Next
'                'Next

'                For Each dMRow As DataRow In dTList.Tables(0).Rows
'                    dRow = dTable.NewRow
'                    dRow.Item("Employee") = dMRow.Item("EName")
'                    dRow.Item("GrpId") = dMRow.Item("departmentunkid")
'                    dRow.Item("EmpId") = dMRow.Item("EmpId")
'                    dTable.Rows.Add(dRow)
'                Next


'            Else

'                Dim StrEmpFilter As String = String.Empty
'                Dim StrAssignedRev As String = String.Empty

'                If isSameQuery = False Then

'                    If intEmpId > 0 Then
'                        StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(hrassessor_tran.employeeunkid AS NVARCHAR(50)) " & _
'                               "FROM hrassessor_tran " & _
'                               "    JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'                               "WHERE hrassessor_master.employeeunkid = '" & intEmpId & "' AND isreviewer = 0 AND hrassessor_tran.isvoid = 0 AND hrassessor_master.isvoid = 0 " & _
'                               "ORDER BY hrassessor_tran.employeeunkid FOR XML PATH('')),1,1,''),'') AS AssessIds "

'                        dMList = objDataOperation.ExecQuery(StrQ, "List")

'                        If objDataOperation.ErrorMessage <> "" Then
'                            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        End If

'                        StrEmpFilter = dMList.Tables(0).Rows(0)("AssessIds").ToString
'                    End If

'                    If intEmpId > 0 Then
'                        StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(hrassessor_tran.employeeunkid AS NVARCHAR(50)) " & _
'                               "FROM hrassessor_tran " & _
'                               "    JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'                               "WHERE hrassessor_master.employeeunkid <> '" & intEmpId & "' AND isreviewer = 1 AND hrassessor_tran.isvoid = 0 AND hrassessor_master.isvoid = 0 " & _
'                               "ORDER BY hrassessor_tran.employeeunkid FOR XML PATH('')),1,1,''),'') AS AssessIds "

'                        dMList = objDataOperation.ExecQuery(StrQ, "List")

'                        If objDataOperation.ErrorMessage <> "" Then
'                            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                        End If

'                        StrAssignedRev = dMList.Tables(0).Rows(0)("AssessIds").ToString
'                    End If


'                Else
'                    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(hrassessor_tran.employeeunkid AS NVARCHAR(50)) " & _
'                           "FROM hrassessor_tran " & _
'                           "    JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'                           "WHERE hrassessor_master.employeeunkid = '" & intEmpId & "' AND isreviewer = 1 AND hrassessor_tran.isvoid = 0 AND hrassessor_master.isvoid = 0 " & _
'                           "ORDER BY hrassessor_tran.employeeunkid FOR XML PATH('')),1,1,''),'') AS AssessIds "

'                    dMList = objDataOperation.ExecQuery(StrQ, "List")

'                    If objDataOperation.ErrorMessage <> "" Then
'                        Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                    End If

'                    StrEmpFilter = dMList.Tables(0).Rows(0)("AssessIds").ToString
'                End If


'                StrQ = "SELECT name, departmentunkid FROM hrdepartment_master WHERE isactive = 1 "

'                dMList = objDataOperation.ExecQuery(StrQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                End If

'                StrQ = "SELECT " & _
'                       "  departmentunkid AS DeptId " & _
'                       " ,employeecode AS ECode " & _
'                       " ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
'                       " ,employeeunkid AS EmpId " & _
'                       "FROM hremployee_master " & _
'                       " WHERE 1 = 1 "

'                If StrEmpFilter.Trim.Length > 0 Then
'                    StrQ &= " AND employeeunkid NOT IN(" & intEmpId.ToString & "," & StrEmpFilter & ") "
'                Else
'                    StrQ &= " AND employeeunkid NOT IN(" & intEmpId.ToString & ") "
'                End If
'                If StrAssignedRev.Trim.Length > 0 Then
'                    StrQ &= " AND employeeunkid NOT IN(" & intEmpId.ToString & "," & StrAssignedRev & ") "
'                End If
'                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
'                        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "


'                'Pinkal (12-Jun-2012) -- Start
'                'Enhancement : TRA Changes
'                If mstrAllocationFilter.Trim.Length > 0 Then
'                    StrQ &= " AND  " & mstrAllocationFilter
'                End If
'                'Pinkal (12-Jun-2012) -- End


'                StrQ &= " ORDER BY departmentunkid "

'                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
'                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

'                dTList = objDataOperation.ExecQuery(StrQ, "List")

'                If objDataOperation.ErrorMessage <> "" Then
'                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                End If
'                Dim dRow As DataRow = Nothing

'                'Pinkal (12-Jun-2012) -- Start
'                'Enhancement : TRA Changes

'                StrQ = "SELECT employeeunkid,departmentunkid FROM hrassessor_tran WHERE assessormasterunkid = '" & intAssessorId & "'  AND isvoid = 0 "
'                Dim dsRAccess As DataSet = objDataOperation.ExecQuery(StrQ, "List")

'                'For Each dMRow As DataRow In dMList.Tables(0).Rows
'                '    Dim dFilter As DataTable = New DataView(dTList.Tables(0), "DeptId = '" & dMRow.Item("departmentunkid") & "'", "", DataViewRowState.CurrentRows).ToTable
'                '    Dim dRF As DataTable = New DataView(dsRAccess.Tables(0), "departmentunkid = '" & dMRow.Item("departmentunkid") & "'", "", DataViewRowState.CurrentRows).ToTable

'                '    dRow = dTable.NewRow
'                '    dRow.Item("Employee") = dMRow.Item("name")


'                '    'Pinkal (12-Jun-2012) -- Start
'                '    'Enhancement : TRA Changes
'                '    'dRow.Item("IsGrp") = True
'                '    'Pinkal (12-Jun-2012) -- End


'                '    dRow.Item("GrpId") = dMRow.Item("departmentunkid")
'                '    dRow.Item("EmpId") = -1
'                '    If dRF.Rows.Count > 0 AndAlso dFilter.Rows.Count > 0 Then
'                '        If dRF.Rows.Count = dFilter.Rows.Count Then
'                '            dRow.Item("IsCheck") = True
'                '        End If
'                '    End If
'                '    dTable.Rows.Add(dRow)

'                '    For Each dTRow As DataRow In dFilter.Rows
'                '        dRow = dTable.NewRow
'                '        Dim dRFow() As DataRow = dRF.Select("employeeunkid = '" & dTRow.Item("EmpId") & "'")
'                '        If dRFow.Length > 0 Then
'                '            dRow.Item("IsCheck") = True
'                '        End If

'                '        'Pinkal (12-Jun-2012) -- Start
'                '        'Enhancement : TRA Changes

'                '        ' dRow.Item("Employee") = Space(5) & dTRow.Item("ECode") & " - " & dTRow.Item("EName")
'                '        'dRow.Item("IsGrp") = False
'                '        dRow.Item("Employee") = dTRow.Item("ECode") & " - " & dTRow.Item("EName")


'                '        'Pinkal (12-Jun-2012) -- End

'                '        dRow.Item("GrpId") = dMRow.Item("departmentunkid")
'                '        dRow.Item("EmpId") = dTRow.Item("EmpId")
'                '        dTable.Rows.Add(dRow)
'                '    Next
'                'Next

'                Dim drEmp As DataRow() = Nothing
'                For Each dMRow As DataRow In dTList.Tables(0).Rows
'                    dRow = dTable.NewRow
'                    dRow.Item("Employee") = dMRow.Item("ECode") & " - " & dMRow.Item("EName")
'                    dRow.Item("GrpId") = dMRow.Item("DeptId")
'                    dRow.Item("EmpId") = dMRow.Item("EmpId")
'                    drEmp = dsRAccess.Tables(0).Select("employeeunkid = " & dMRow.Item("EmpId"))
'                    If drEmp.Length > 0 Then
'                        dRow.Item("IsCheck") = True
'                    End If
'                    dTable.Rows.Add(dRow)
'                Next

'                'Pinkal (12-Jun-2012) -- End

'            End If

'            Return dTable
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetReviewerAccess; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Function
'            'S.SANDEEP [ 23 JAN 2012 ] -- END

'    'S.SANDEEP [ 13 AUG 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public Function Get_CSV_Employee(ByVal iUserId As Integer, ByVal iReviewer As Boolean) As String
'        Dim StrQ As String = String.Empty
'        Dim dsList As New DataSet
'        Dim iValue As String = String.Empty
'        Dim exForce As Exception
'        Try
'            Dim objDataOperation As New clsDataOperation

'            If iReviewer = True Then
'                StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(hrassessor_tran.employeeunkid AS NVARCHAR(MAX)) FROM hrassessor_tran " & _
'                   " JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'                   " JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid " & _
'                   "WHERE usertypeid = '" & enUserType.Assessor & "' AND isreviewer = 1 AND hrapprover_usermapping.userunkid = '" & iUserId & "' FOR XML PATH('')),1,1,''),0) AS cEmp "
'            Else
'                StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(hrassessor_tran.employeeunkid AS NVARCHAR(MAX)) FROM hrassessor_tran " & _
'                   " JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
'                   " JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid " & _
'                   "WHERE usertypeid = '" & enUserType.Assessor & "' AND isreviewer = 0 AND hrapprover_usermapping.userunkid = '" & iUserId & "' FOR XML PATH('')),1,1,''),0) AS cEmp "
'            End If

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables(0).Rows.Count > 0 Then
'                iValue = dsList.Tables(0).Rows(0)("cEmp")
'            End If

'            Return iValue
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Get_CSV_Employee", mstrModuleName)
'            Return ""
'        Finally
'        End Try
'    End Function
'    'S.SANDEEP [ 13 AUG 2013 ] -- END

'    'S.SANDEEP [ 14 AUG 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Public Function GetAssessorMasterId(ByVal iEmpId As Integer, ByVal IsRewiver As Boolean) As Integer
'        Dim StrQ As String = String.Empty
'        Dim iValue As Integer = 0
'        Dim dsList As New DataSet
'        Try
'            Dim objDataOperation As New clsDataOperation

'            StrQ = "SELECT assessormasterunkid FROM hrassessor_master WHERE isvoid = 0 AND employeeunkid = '" & iEmpId & "' "

'            If IsRewiver = True Then
'                StrQ &= " AND isreviewer = 1 "
'            Else
'                StrQ &= " AND isreviewer = 0 "
'            End If

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'            End If

'            If dsList.Tables("List").Rows.Count > 0 Then
'                iValue = dsList.Tables("List").Rows(0).Item("assessormasterunkid")
'            End If

'            Return iValue

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetAssessorMasterId", mstrModuleName)
'        Finally
'        End Try
'    End Function

'    Public Function IsEmpMapped(ByVal iAssessorMasterId As Integer, ByVal iEmpId As Integer) As Boolean
'        Dim StrQ As String = String.Empty
'        Dim dsList As New DataSet
'        Dim blnFlag As Boolean = False
'        Try
'            Dim objDataOperation As New clsDataOperation

'            StrQ = "SELECT 1 FROM hrassessor_tran WHERE isvoid = 0 AND assessormasterunkid = '" & iAssessorMasterId & "' AND employeeunkid = '" & iEmpId & "'"

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'            End If

'            If dsList.Tables("List").Rows.Count > 0 Then
'                blnFlag = True
'            End If

'            Return blnFlag

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "IsEmpMapped", mstrModuleName)
'        Finally
'        End Try
'    End Function
'    'S.SANDEEP [ 14 AUG 2013 ] -- END

'#End Region

'End Class