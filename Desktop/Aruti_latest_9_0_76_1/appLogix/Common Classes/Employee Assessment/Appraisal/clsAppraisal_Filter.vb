﻿'************************************************************************************************************************************
'Class Name : clsApprisal_Filter.vb
'Purpose    :
'Date       :08-Feb-12
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsAppraisal_Filter
    Private Shared ReadOnly mstrModuleName As String = "clsAppraisal_Filter"
    Private mintShortlistunkid As Integer
    Private mdtTran As DataTable
    Dim mstrMessage As String = ""
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'S.SANDEEP [ 22 OCT 2013 ] -- START
    Private mintAppointmentTypeId As Integer = 0
    Private mdtDate1 As DateTime = Nothing
    Private mdtDate2 As DateTime = Nothing
    'S.SANDEEP [ 22 OCT 2013 ] -- END


#Region " Properties "

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _Shortlistunkid() As Integer
        Get
            Return mintShortlistunkid
        End Get
        Set(ByVal value As Integer)
            mintShortlistunkid = value
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'S.SANDEEP [ 22 OCT 2013 ] -- START
    Public WriteOnly Property _AppointmentTypeId() As Integer
        Set(ByVal value As Integer)
            mintAppointmentTypeId = value
        End Set
    End Property

    Public WriteOnly Property _Date1() As DateTime
        Set(ByVal value As DateTime)
            mdtDate1 = value
        End Set
    End Property

    Public WriteOnly Property _Date2() As DateTime
        Set(ByVal value As DateTime)
            mdtDate2 = value
        End Set
    End Property
    'S.SANDEEP [ 22 OCT 2013 ] -- END

#End Region

#Region " ENUMS "

    Public Enum Filter_Refid
        TOTAL_BSC_SCORE = 1
        TOTAL_GE_SCORE = 2
        BSC_PERSPECTIVE = 3
        GENEAL_EVALUATION = 4
        OVERALL_SCORE = 5 'S.SANDEEP [ 14 AUG 2013 ] -- START -- END
    End Enum

    Public Enum Operater_ModeId
        OP_AND = 1
        OP_OR = 2
    End Enum

#End Region

#Region " Constructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("DataTable")
            Dim dCol As DataColumn

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "filterunkid"
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "shortlistunkid"
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "filter_refid"
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "conditionunkid"
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "referenceunkid"
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "issummary"
            dCol.DefaultValue = False
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "operator"
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "isself_score"
            dCol.DefaultValue = False
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "isassessor_score"
            dCol.DefaultValue = False
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "isreviewer_score"
            dCol.DefaultValue = False
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "AUD"
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "GUID"
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            'dCol.Caption = Language.getMessage(mstrModuleName, 5, "Filter Group")
            dCol.Caption = ""
            dCol.ColumnName = "filter_grp"
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = Language.getMessage(mstrModuleName, 1, "Filter Criteria")
            dCol.ColumnName = "filter_criteria"
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = Language.getMessage(mstrModuleName, 2, "Value")
            dCol.ColumnName = "filter_value"
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = Language.getMessage(mstrModuleName, 3, "Condition")
            dCol.ColumnName = "contition"
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = Language.getMessage(mstrModuleName, 4, "Operation")
            dCol.ColumnName = "operation"
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "col_tag"
            dCol.DefaultValue = ""
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            'S.SANDEEP [19 FEB 2015] -- START
            dCol = New DataColumn
            dCol.Caption = ""
            dCol.ColumnName = "filtertypeid"
            dCol.DefaultValue = -1
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)
            'S.SANDEEP [19 FEB 2015] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & " Procedure : New, Class : clsApprisal_Filter " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Methods & Functions "

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function Insert_Filter(ByVal objDataOperation As clsDataOperation, ByVal dEmployee As DataTable, ByVal intUserId As Integer) As Boolean
        'Public Function Insert_Filter(ByVal objDataOperation As clsDataOperation, ByVal dEmployee As DataTable) As Boolean
        'S.SANDEEP [04 JUN 2015] -- END

        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Try
            If mdtTran Is Nothing Then Return True

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO hrapps_filter ( " & _
                                        "  shortlistunkid " & _
                                        ", filter_refid " & _
                                        ", filter_value " & _
                                        ", conditionunkid " & _
                                        ", referenceunkid " & _
                                        ", issummary " & _
                                        ", operator " & _
                                        ", userunkid " & _
                                        ", isvoid " & _
                                        ", voiduserunkid " & _
                                        ", voiddatetime " & _
                                        ", voidreason " & _
                                        ", isself_score " & _
                                        ", isassessor_score " & _
                                        ", isreviewer_score" & _
                                      ") VALUES (" & _
                                        "  @shortlistunkid " & _
                                        ", @filter_refid " & _
                                        ", @filter_value " & _
                                        ", @conditionunkid " & _
                                        ", @referenceunkid " & _
                                        ", @issummary " & _
                                        ", @operator " & _
                                        ", @userunkid " & _
                                        ", @isvoid " & _
                                        ", @voiduserunkid " & _
                                        ", @voiddatetime " & _
                                        ", @voidreason " & _
                                        ", @isself_score " & _
                                        ", @isassessor_score " & _
                                        ", @isreviewer_score" & _
                                      "); SELECT @@identity"


                                objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShortlistunkid.ToString)
                                objDataOperation.AddParameter("@filter_refid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("filter_refid").ToString)
                                objDataOperation.AddParameter("@filter_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("filter_value").ToString)
                                objDataOperation.AddParameter("@conditionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("conditionunkid").ToString)
                                objDataOperation.AddParameter("@referenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("referenceunkid").ToString)
                                objDataOperation.AddParameter("@issummary", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("issummary").ToString)
                                objDataOperation.AddParameter("@operator", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("operator").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")

                                objDataOperation.AddParameter("@isself_score", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isself_score").ToString)
                                objDataOperation.AddParameter("@isassessor_score", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isassessor_score").ToString)
                                objDataOperation.AddParameter("@isreviewer_score", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isreviewer_score").ToString)



                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                Dim mintFilterUnkId As Integer = dsList.Tables(0).Rows(0).Item(0)

                                If .Item("filterunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrapps_shortlist_master", "shortlistunkid", .Item("shortlistunkid"), "hrapps_filter", "filterunkid", mintFilterUnkId, 2, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrapps_shortlist_master", "shortlistunkid", mintShortlistunkid, "hrapps_filter", "filterunkid", mintFilterUnkId, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                                Dim objFinalEmployee As New clsAppraisal_Final_Emp

                                objFinalEmployee._ShortListUnkid = mintShortlistunkid
                                objFinalEmployee._FilterUnkid = mintFilterUnkId
                                objFinalEmployee._DataTable = New DataView(dEmployee, "EGUID  ='" & .Item("GUID").ToString() & "'", "", DataViewRowState.CurrentRows).ToTable
                                objFinalEmployee._Userunkid = mintUserunkid

                                If objFinalEmployee.Insert_ShortListedEmployee(objDataOperation) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & " Procedure : Insert_Filter " & mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP [19 FEB 2015] -- START
    Public Function GetFilteredEmployee(ByVal dtFilter As DataTable, _
                                        ByVal intPeriodId As Integer, _
                                        ByVal xFilter As enApprFilterTypeId, _
                                        ByVal xScorId As Integer, _
                                        ByVal xEmployeeAsOnDate As DateTime, _
                                        ByVal blnIsSelfAssignCompetencies As Boolean, _
                                        ByVal blnUsedAgreedScore As Boolean, _
                                        ByVal blnIsCalibrationSettingActive As Boolean) As DataSet 'S.SANDEEP |27-MAY-2019| -- START {blnIsCalibrationSettingActive} -- END

        'Shani (23-Nov-2016) -- ADD [blnUsedAgreedScore]
        'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
        Dim mdTable As DataTable
        Dim dsEmployee As New DataSet
        Try
            mdTable = New DataTable("List")
            mdTable.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).Caption = ""
            mdTable.Columns.Add("employeecode", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 6, "Code")
            mdTable.Columns.Add("ENAME", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 7, "Employee")
            mdTable.Columns.Add("Date", System.Type.GetType("System.DateTime")).Caption = Language.getMessage(mstrModuleName, 9, "Appointed Date")
            mdTable.Columns.Add("EMAIL", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 8, "Email")
            mdTable.Columns.Add("F_RefId", System.Type.GetType("System.Int32")).Caption = ""
            mdTable.Columns.Add("EGUID", System.Type.GetType("System.String")).Caption = ""
            mdTable.Columns.Add("JOB_TITLE", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 10, "Job Title")
            mdTable.Columns.Add("DEPT", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 11, "Department")
            mdTable.Columns.Add("SCORE", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 12, "Overall Score")
            mdTable.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = "A"
            mdTable.Columns.Add("tscore", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdTable.Columns("SCORE").ExtendedProperties.Add("xScore", "xScore")

            If dtFilter.Rows.Count > 0 Then
                Dim dsData As New DataSet
                Dim dtTemp() As DataRow = Nothing
                dtTemp = dtFilter.Select("issummary = true AND AUD <> 'D' AND filter_refid = '" & Filter_Refid.OVERALL_SCORE & "'")
                If dtTemp.Length > 0 Then
                    Dim intOperationMode As Integer = -1
                    For i As Integer = 0 To dtTemp.Length - 1
                        If intOperationMode <= 0 AndAlso dtTemp(i)("operator") > 0 Then
                            intOperationMode = dtTemp(i)("operator")
                        End If

                        'S.SANDEEP |27-MAY-2019| -- START
                        'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
                        'dsData = GetNewOverallFilter(dtTemp(i), intPeriodId, xFilter, xScorId, xEmployeeAsOnDate, blnIsSelfAssignCompetencies, blnUsedAgreedScore)
                        dsData = GetNewOverallFilter(dtTemp(i), intPeriodId, xFilter, xScorId, xEmployeeAsOnDate, blnIsSelfAssignCompetencies, blnUsedAgreedScore, blnIsCalibrationSettingActive)
                        'S.SANDEEP |27-MAY-2019| -- END

                        'Shani (23-Nov-2016) -- [blnUsedAgreedScore]

                        'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

                        If intOperationMode > 0 Then
                            If dsData.Tables(0).Rows.Count <= 0 AndAlso intOperationMode = Operater_ModeId.OP_AND Then
                                If mdTable IsNot Nothing AndAlso mdTable.Rows.Count > 0 Then mdTable.Rows.Clear()
                                Exit For
                            End If
                        End If


                        For Each drRow As DataRow In dsData.Tables(0).Rows
                            Dim dtRow As DataRow = mdTable.NewRow

                            dtRow.Item("employeeunkid") = drRow.Item("employeeunkid")
                            dtRow.Item("employeecode") = drRow.Item("employeecode")
                            dtRow.Item("ENAME") = drRow.Item("ENAME")
                            dtRow.Item("Date") = eZeeDate.convertDate(drRow.Item("appointeddate").ToString)
                            dtRow.Item("EMAIL") = drRow.Item("EMAIL")
                            dtRow.Item("F_RefId") = drRow.Item("F_RefId")
                            dtRow.Item("EGUID") = drRow.Item("EGUID")
                            dtRow.Item("JOB_TITLE") = drRow.Item("JOB_TITLE")
                            dtRow.Item("DEPT") = drRow.Item("DEPT")
                            dtRow.Item("SCORE") = drRow.Item("SCORE")
                            If drRow.Item("SCORE").ToString.Trim.Length > 0 Then
                                dtRow.Item("tscore") = drRow.Item("SCORE")
                            End If

                            mdTable.Rows.Add(dtRow)
                        Next
                    Next
                End If
                dtTemp = dtFilter.Select("issummary = true AND AUD <>'D' AND filter_refid <> '" & Filter_Refid.OVERALL_SCORE & "'")
                If dtTemp.Length > 0 Then
                    Dim intOperationMode As Integer = -1
                    For i As Integer = 0 To dtTemp.Length - 1
                        If intOperationMode <= 0 AndAlso dtTemp(i)("operator") > 0 Then
                            intOperationMode = dtTemp(i)("operator")
                        End If

                        dsData = GetNewSummaryFilter(dtTemp(i), intPeriodId, xFilter, xScorId, xEmployeeAsOnDate, blnIsSelfAssignCompetencies, blnUsedAgreedScore)
                        'Shani (23-Nov-2016) -- [blnUsedAgreedScore]

                        'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

                        If intOperationMode > 0 Then
                            If dsData.Tables(0).Rows.Count <= 0 AndAlso intOperationMode = Operater_ModeId.OP_AND Then
                                If mdTable IsNot Nothing AndAlso mdTable.Rows.Count > 0 Then mdTable.Rows.Clear()
                                Exit For
                            End If
                        End If


                        For Each drRow As DataRow In dsData.Tables(0).Rows
                            Dim dtRow As DataRow = mdTable.NewRow

                            dtRow.Item("employeeunkid") = drRow.Item("employeeunkid")
                            dtRow.Item("employeecode") = drRow.Item("employeecode")
                            dtRow.Item("ENAME") = drRow.Item("ENAME")
                            dtRow.Item("Date") = eZeeDate.convertDate(drRow.Item("appointeddate").ToString)
                            dtRow.Item("EMAIL") = drRow.Item("EMAIL")
                            dtRow.Item("F_RefId") = drRow.Item("F_RefId")
                            dtRow.Item("EGUID") = drRow.Item("EGUID")
                            dtRow.Item("JOB_TITLE") = drRow.Item("JOB_TITLE")
                            dtRow.Item("DEPT") = drRow.Item("DEPT")
                            dtRow.Item("SCORE") = drRow.Item("SCORE")
                            If drRow.Item("SCORE").ToString.Trim.Length > 0 Then
                                dtRow.Item("tscore") = drRow.Item("SCORE")
                            End If

                            mdTable.Rows.Add(dtRow)
                        Next
                    Next
                End If

                'S.SANDEEP [04 MAR 2015] -- START
                dtTemp = dtFilter.Select("issummary = false AND AUD <>'D' AND filter_refid = '" & Filter_Refid.GENEAL_EVALUATION & "'")

                For i As Integer = 0 To dtTemp.Length - 1
                    dsData = GetNewDetailFilter(dtTemp(i), intPeriodId, xFilter)
                    Dim xStrGUID As String = ""
                    Dim dR() As DataRow = Nothing
                    For Each drRow As DataRow In dsData.Tables(0).Rows
                        If xStrGUID <> drRow.Item("EGUID") Then
                            Dim dtRow As DataRow = mdTable.NewRow
                            dtRow.Item("employeeunkid") = drRow.Item("employeeunkid")
                            dtRow.Item("employeecode") = drRow.Item("employeecode")
                            dtRow.Item("ENAME") = drRow.Item("ENAME")
                            dtRow.Item("Date") = eZeeDate.convertDate(drRow.Item("appointeddate").ToString)
                            dtRow.Item("EMAIL") = drRow.Item("EMAIL")
                            dtRow.Item("F_RefId") = drRow.Item("F_RefId")
                            dtRow.Item("EGUID") = drRow.Item("EGUID")
                            dtRow.Item("JOB_TITLE") = drRow.Item("JOB_TITLE")
                            dtRow.Item("DEPT") = drRow.Item("DEPT")
                            dtRow.Item("SCORE") = drRow.Item("SCORE")
                            dR = mdTable.Select("employeeunkid ='" & drRow.Item("employeeunkid") & "'")
                            If dR.Length <= 0 Then
                                If drRow.Item("SCORE").ToString.Trim.Length > 0 Then
                                    dtRow.Item("tscore") = drRow.Item("SCORE")
                                End If
                            End If
                            mdTable.Rows.Add(dtRow)
                            xStrGUID = drRow.Item("EGUID")
                        End If
                    Next
                Next
                'S.SANDEEP [04 MAR 2015] -- END

            End If

            dsEmployee.Tables.Add(mdTable)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetFilteredEmployee", mstrModuleName)
        Finally
        End Try
        Return dsEmployee
    End Function
    'S.SANDEEP [19 FEB 2015] -- END


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function GetFilteredEmployee(ByVal dtFilter As DataTable, ByVal intPeriodId As Integer, ByVal blnConsiderItemWeightAsNumber As Boolean, ByVal blnIsSelfAssignCompetencies As Boolean) As DataSet
        'Public Function GetFilteredEmployee(ByVal dtFilter As DataTable, ByVal intPeriodId As Integer) As DataSet
        'S.SANDEEP [04 JUN 2015] -- END

        Dim mdTable As DataTable
        Dim dsEmployee As New DataSet
        Try

            mdTable = New DataTable("List")

            mdTable.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).Caption = ""
            mdTable.Columns.Add("employeecode", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 6, "Code")
            mdTable.Columns.Add("ENAME", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 7, "Employee")
            mdTable.Columns.Add("Date", System.Type.GetType("System.DateTime")).Caption = Language.getMessage(mstrModuleName, 9, "Appointed Date")
            mdTable.Columns.Add("EMAIL", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 8, "Email")
            mdTable.Columns.Add("F_RefId", System.Type.GetType("System.Int32")).Caption = ""
            mdTable.Columns.Add("EGUID", System.Type.GetType("System.String")).Caption = ""
            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mdTable.Columns.Add("JOB_TITLE", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 10, "Job Title")
            mdTable.Columns.Add("DEPT", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 11, "Department")
            mdTable.Columns.Add("SCORE", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 12, "Overall Score")
            mdTable.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = "A"
            'S.SANDEEP [ 14 AUG 2013 ] -- END

            If dtFilter.Rows.Count > 0 Then

                Dim dsData As New DataSet
                Dim dtTemp() As DataRow = Nothing

                'S.SANDEEP [ 14 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                dtTemp = dtFilter.Select("issummary = true AND AUD <> 'D' AND filter_refid = '" & Filter_Refid.OVERALL_SCORE & "'")
                If dtTemp.Length > 0 Then
                    Dim intOperationMode As Integer = -1
                    For i As Integer = 0 To dtTemp.Length - 1
                        If intOperationMode <= 0 AndAlso dtTemp(i)("operator") > 0 Then
                            intOperationMode = dtTemp(i)("operator")
                        End If

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'dsData = Get_Overall_Filter(dtTemp(i), intPeriodId)
                        dsData = Get_Overall_Filter(dtTemp(i), intPeriodId, blnConsiderItemWeightAsNumber, blnIsSelfAssignCompetencies)
                        'S.SANDEEP [04 JUN 2015] -- END


                        If intOperationMode > 0 Then
                            If dsData.Tables(0).Rows.Count <= 0 AndAlso intOperationMode = Operater_ModeId.OP_AND Then Return dsData
                        End If

                        For Each drRow As DataRow In dsData.Tables(0).Rows
                            Dim dtRow As DataRow = mdTable.NewRow

                            dtRow.Item("employeeunkid") = drRow.Item("employeeunkid")
                            dtRow.Item("employeecode") = drRow.Item("ECODE")
                            dtRow.Item("ENAME") = drRow.Item("ENAME")
                            dtRow.Item("Date") = eZeeDate.convertDate(drRow.Item("appointeddate").ToString)
                            dtRow.Item("EMAIL") = drRow.Item("EMAIL")
                            dtRow.Item("F_RefId") = drRow.Item("F_RefId")
                            dtRow.Item("EGUID") = drRow.Item("EGUID")
                            dtRow.Item("JOB_TITLE") = drRow.Item("JOB_TITLE")
                            dtRow.Item("DEPT") = drRow.Item("DEPT")
                            dtRow.Item("SCORE") = drRow.Item("Overall_Score")

                            mdTable.Rows.Add(dtRow)
                        Next

                    Next
                End If
                'S.SANDEEP [ 14 AUG 2013 ] -- END


                'S.SANDEEP [ 14 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                dtTemp = dtFilter.Select("issummary = true AND AUD <>'D' AND filter_refid <> '" & Filter_Refid.OVERALL_SCORE & "'")
                'S.SANDEEP [ 14 AUG 2013 ] -- END

                If dtTemp.Length > 0 Then

                    Dim intOperationMode As Integer = -1

                    For i As Integer = 0 To dtTemp.Length - 1

                        If intOperationMode <= 0 AndAlso dtTemp(i)("operator") > 0 Then
                            intOperationMode = dtTemp(i)("operator")
                        End If

                        'S.SANDEEP [04 JUN 2015] -- START
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'dsData = GetSummaryFilter(dtTemp(i), intPeriodId)
                        dsData = GetSummaryFilter(dtTemp(i), intPeriodId, blnConsiderItemWeightAsNumber, blnIsSelfAssignCompetencies)
                        'S.SANDEEP [04 JUN 2015] -- END


                        If intOperationMode > 0 Then
                            If dsData.Tables(0).Rows.Count <= 0 AndAlso intOperationMode = Operater_ModeId.OP_AND Then Exit For
                        End If

                        For Each drRow As DataRow In dsData.Tables(0).Rows
                            Dim dtRow As DataRow = mdTable.NewRow

                            dtRow.Item("employeeunkid") = drRow.Item("employeeunkid")
                            dtRow.Item("employeecode") = drRow.Item("ECODE")
                            dtRow.Item("ENAME") = drRow.Item("ENAME")
                            'S.SANDEEP [ 14 AUG 2013 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            'dtRow.Item("Date") = eZeeDate.convertDate(drRow.Item("appointeddate").ToString).ToShortDateString
                            dtRow.Item("Date") = eZeeDate.convertDate(drRow.Item("appointeddate").ToString)
                            'S.SANDEEP [ 14 AUG 2013 ] -- END
                            dtRow.Item("EMAIL") = drRow.Item("EMAIL")
                            dtRow.Item("F_RefId") = drRow.Item("F_RefId")
                            dtRow.Item("EGUID") = drRow.Item("EGUID")
                            'S.SANDEEP [ 14 AUG 2013 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            dtRow.Item("JOB_TITLE") = drRow.Item("JOB_TITLE")
                            dtRow.Item("DEPT") = drRow.Item("DEPT")
                            dtRow.Item("SCORE") = drRow.Item("TotalScore")
                            'S.SANDEEP [ 14 AUG 2013 ] -- END

                            mdTable.Rows.Add(dtRow)
                        Next
                    Next
                End If

                dtTemp = dtFilter.Select("issummary = false AND AUD <> 'D' AND filter_refid = '" & Filter_Refid.BSC_PERSPECTIVE & "'")
                Dim intGEFilterOperator As Integer = -1
                If dtTemp.Length > 0 Then
                    Dim intOperationMode As Integer = -1

                    For i As Integer = 0 To dtTemp.Length - 1

                        If intOperationMode <= 0 AndAlso dtTemp(i)("operator") > 0 Then
                            intOperationMode = dtTemp(i)("operator")
                        End If

                        dsData = GetBSC_PerspectiveFilter(dtTemp(i), intPeriodId)

                        If intOperationMode > 0 Then
                            If dsData.Tables(0).Rows.Count <= 0 AndAlso intOperationMode = Operater_ModeId.OP_AND Then
                                If dtTemp(dtTemp.Length - 1).Item("operator") > 0 Then
                                    intGEFilterOperator = dtTemp(dtTemp.Length - 1).Item("operator")
                                End If
                                Exit For
                            End If
                        End If

                        For Each drRow As DataRow In dsData.Tables(0).Rows
                            Dim dtRow As DataRow = mdTable.NewRow

                            dtRow.Item("employeeunkid") = drRow.Item("employeeunkid")
                            dtRow.Item("employeecode") = drRow.Item("ECODE")
                            dtRow.Item("ENAME") = drRow.Item("ENAME")
                            'S.SANDEEP [ 14 AUG 2013 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            'dtRow.Item("Date") = eZeeDate.convertDate(drRow.Item("appointeddate").ToString).ToShortDateString
                            dtRow.Item("Date") = eZeeDate.convertDate(drRow.Item("appointeddate").ToString)
                            'S.SANDEEP [ 14 AUG 2013 ] -- END
                            dtRow.Item("EMAIL") = drRow.Item("EMAIL")
                            dtRow.Item("F_RefId") = drRow.Item("F_RefId")
                            dtRow.Item("EGUID") = drRow.Item("EGUID")

                            'S.SANDEEP [ 14 AUG 2013 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                            dtRow.Item("JOB_TITLE") = drRow.Item("JOB_TITLE")
                            dtRow.Item("DEPT") = drRow.Item("DEPT")
                            dtRow.Item("SCORE") = drRow.Item("Total")
                            'S.SANDEEP [ 14 AUG 2013 ] -- END

                            mdTable.Rows.Add(dtRow)
                        Next
                    Next
                End If

                If intGEFilterOperator = -1 Or intGEFilterOperator = 2 Then
                    dtTemp = dtFilter.Select("issummary = false AND AUD <>'D' AND filter_refid = '" & Filter_Refid.GENEAL_EVALUATION & "'")

                    If dtTemp.Length > 0 Then
                        Dim intOperationMode As Integer = -1
                        For i As Integer = 0 To dtTemp.Length - 1
                            If intOperationMode <= 0 AndAlso dtTemp(i)("operator") > 0 Then
                                intOperationMode = dtTemp(i)("operator")
                            End If

                            'S.SANDEEP [04 JUN 2015] -- START
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'dsData = Get_GeneralFilter(dtTemp(i), intPeriodId)
                            dsData = Get_GeneralFilter(dtTemp(i), intPeriodId, blnConsiderItemWeightAsNumber, blnIsSelfAssignCompetencies)
                            'S.SANDEEP [04 JUN 2015] -- END


                            If intOperationMode > 0 Then
                                If dsData.Tables(0).Rows.Count <= 0 AndAlso intOperationMode = Operater_ModeId.OP_AND Then Exit For
                            End If

                            For Each drRow As DataRow In dsData.Tables(0).Rows
                                Dim dtRow As DataRow = mdTable.NewRow

                                dtRow.Item("employeeunkid") = drRow.Item("employeeunkid")
                                dtRow.Item("employeecode") = drRow.Item("ECODE")
                                dtRow.Item("ENAME") = drRow.Item("ENAME")
                                'S.SANDEEP [ 14 AUG 2013 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'dtRow.Item("Date") = eZeeDate.convertDate(drRow.Item("appointeddate").ToString).ToShortDateString
                                dtRow.Item("Date") = eZeeDate.convertDate(drRow.Item("appointeddate").ToString)
                                'S.SANDEEP [ 14 AUG 2013 ] -- END
                                dtRow.Item("EMAIL") = drRow.Item("EMAIL")
                                dtRow.Item("F_RefId") = drRow.Item("F_RefId")
                                dtRow.Item("EGUID") = drRow.Item("EGUID")

                                'S.SANDEEP [ 14 AUG 2013 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                dtRow.Item("JOB_TITLE") = drRow.Item("JOB_TITLE")
                                dtRow.Item("DEPT") = drRow.Item("DEPT")
                                dtRow.Item("SCORE") = drRow.Item("Total")
                                'S.SANDEEP [ 14 AUG 2013 ] -- END

                                mdTable.Rows.Add(dtRow)
                            Next
                        Next

                    End If
                End If


            End If

            dsEmployee.Tables.Add(mdTable)
            Return dsEmployee

        Catch ex As Exception
            Throw New Exception(ex.Message & " Procedure : GetFilteredEmployee; Modulename : " & mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Get_GeneralFilter(ByVal dRow As DataRow, ByVal iPeriodId As Integer, ByVal blnConsiderItemWeightAsNumber As Boolean, ByVal blnIsSelfAssignCompetencies As Boolean) As DataSet
        'Private Function Get_GeneralFilter(ByVal dRow As DataRow, ByVal iPeriodId As Integer) As DataSet
        'S.SANDEEP [04 JUN 2015] -- END
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOpetation As New clsDataOperation
        Dim dsList As New DataSet
        Try
            StrQ = " SELECT employeeunkid,ECODE,ENAME,appointeddate,EMAIL,F_RefId,EGUID,JOB_TITLE,DEPT,Total " & _
                   " FROM (" & _
                   "    SELECT " & _
                   "         employeeunkid " & _
                   "        ,employeecode AS ECODE " & _
                   "        ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
                   "        ,CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
                   "        ,ISNULL(email,'') AS EMAIL " & _
                   "        ,'" & dRow.Item("filter_refid") & "' AS F_RefId " & _
                   "        ,'" & dRow.Item("GUID") & "' AS EGUID " & _
                   "        ,ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
                   "        ,ISNULL(hrdepartment_master.name,'') AS DEPT "

            If CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/3 AS DECIMAL(10,2)) AS Total "
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0))/2 AS DECIMAL(10,2)) AS Total "
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(R_GA.R_GA,0))/2 AS DECIMAL(10,2)) AS Total "
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= ",CAST((ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/2 AS DECIMAL(10,2)) AS Total "
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= ",CAST(ISNULL(S_GA.S_GA,0) AS DECIMAL(10,2)) AS Total "
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= ",CAST(ISNULL(A_GA.A_GA,0) AS DECIMAL(10,2)) AS Total "
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= ",CAST(ISNULL(R_GA.R_GA,0) AS DECIMAL(10,2)) AS Total "
            End If

            'S.SANDEEP |21-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
            'StrQ &= "FROM hremployee_master " & _
            '        "   JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '        "   JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '        "   JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            selfemployeeunkid AS EmpId " & _
            '        "           ,hrcompetency_analysis_tran.competenciesunkid " & _
            '        "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS S_GA " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
            '        "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
            '        "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "           AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
            '        "       GROUP BY selfemployeeunkid,hrcompetency_analysis_tran.competenciesunkid " & _
            '        "   )AS S_GA ON S_GA.EmpId = hremployee_master.employeeunkid " & _
            '        "   JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            assessedemployeeunkid AS EmpId " & _
            '        "           ,hrcompetency_analysis_tran.competenciesunkid " & _
            '        "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) / COUNT(DISTINCT assessormasterunkid)  AS A_GA " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
            '        "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
            '        "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
            '        "       GROUP BY assessedemployeeunkid,hrcompetency_analysis_tran.competenciesunkid " & _
            '        "   )AS A_GA ON A_GA.EmpId = hremployee_master.employeeunkid AND S_GA.competenciesunkid = A_GA.competenciesunkid " & _
            '        "   JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            assessedemployeeunkid AS EmpId " & _
            '        "           ,hrcompetency_analysis_tran.competenciesunkid " & _
            '        "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS R_GA " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
            '        "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
            '        "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'  AND iscommitted = 1 " & _
            '        "       GROUP BY assessedemployeeunkid,hrcompetency_analysis_tran.competenciesunkid " & _
            '        "   )AS R_GA ON R_GA.EmpId = hremployee_master.employeeunkid AND S_GA.competenciesunkid = R_GA.competenciesunkid "
            StrQ &= "FROM hremployee_master " & _
                    "   JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                    "   JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
                    "   JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            selfemployeeunkid AS EmpId " & _
                    "           ,hrcompetency_analysis_tran.competenciesunkid " & _
                    "           ,CAST(SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS DECIMAL(36,2)) AS S_GA " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                    "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                    "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "           AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
                    "       GROUP BY selfemployeeunkid,hrcompetency_analysis_tran.competenciesunkid " & _
                    "   )AS S_GA ON S_GA.EmpId = hremployee_master.employeeunkid " & _
                    "   JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            assessedemployeeunkid AS EmpId " & _
                    "           ,hrcompetency_analysis_tran.competenciesunkid " & _
                    "           ,CAST(SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) / COUNT(DISTINCT assessormasterunkid) AS DECIMAL(36,2)) AS A_GA " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                    "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                    "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
                    "       GROUP BY assessedemployeeunkid,hrcompetency_analysis_tran.competenciesunkid " & _
                    "   )AS A_GA ON A_GA.EmpId = hremployee_master.employeeunkid AND S_GA.competenciesunkid = A_GA.competenciesunkid " & _
                    "   JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            assessedemployeeunkid AS EmpId " & _
                    "           ,hrcompetency_analysis_tran.competenciesunkid " & _
                    "           ,CAST(SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS DECIMAL(36,2)) AS R_GA " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                    "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                    "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'  AND iscommitted = 1 " & _
                    "       GROUP BY assessedemployeeunkid,hrcompetency_analysis_tran.competenciesunkid " & _
                    "   )AS R_GA ON R_GA.EmpId = hremployee_master.employeeunkid AND S_GA.competenciesunkid = R_GA.competenciesunkid "
            'S.SANDEEP |21-AUG-2019| -- END

            Select Case mintAppointmentTypeId
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                    End If
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    If mdtDate1 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                    End If
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    If mdtDate1 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                    End If
            End Select

            If CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= " AND S_GA.competenciesunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/3 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= " AND S_GA.competenciesunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= " AND S_GA.competenciesunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST((ISNULL(S_GA.S_GA,0) + ISNULL(R_GA.R_GA,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= " AND A_GA.competenciesunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST((ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= " AND S_GA.competenciesunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST(ISNULL(S_GA.S_GA,0) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= " AND A_GA.competenciesunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST(ISNULL(A_GA.A_GA,0) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= " AND R_GA.competenciesunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST(ISNULL(R_GA.R_GA,0) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
            End If

            StrQ &= ") AS A WHERE 1 = 1 "


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOpetation.AddParameter("@WeightAsNumber", SqlDbType.Bit, eZeeDataType.BIT_SIZE, ConfigParameter._Object._ConsiderItemWeightAsNumber)
            objDataOpetation.AddParameter("@WeightAsNumber", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnConsiderItemWeightAsNumber)
            'S.SANDEEP [04 JUN 2015] -- END


            dsList = objDataOpetation.ExecQuery(StrQ, "List")

            If objDataOpetation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpetation.ErrorNumber & " : " & objDataOpetation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_GeneralFilter", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Private Function GetBSC_PerspectiveFilter(ByVal dRow As DataRow, ByVal iPeriodId As Integer) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOpetation As New clsDataOperation
        Dim dsList As New DataSet
        Try
            StrQ = " SELECT employeeunkid,ECODE,ENAME,appointeddate,EMAIL,F_RefId,EGUID,JOB_TITLE,DEPT,Total " & _
                   " FROM (" & _
                   "    SELECT " & _
                   "         employeeunkid " & _
                   "        ,employeecode AS ECODE " & _
                   "        ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
                   "        ,CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
                   "        ,ISNULL(email,'') AS EMAIL " & _
                   "        ,'" & dRow.Item("filter_refid") & "' AS F_RefId " & _
                   "        ,'" & dRow.Item("GUID") & "' AS EGUID " & _
                   "        ,ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
                   "        ,ISNULL(hrdepartment_master.name,'') AS DEPT "

            If CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= ",CAST((ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/3 AS DECIMAL(10,2)) AS Total "
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= ",CAST((ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0))/2 AS DECIMAL(10,2)) AS Total "
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= ",CAST((ISNULL(S_BSC.S_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 AS DECIMAL(10,2)) AS Total "
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= ",CAST((ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 AS DECIMAL(10,2)) AS Total "
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= ",CAST(ISNULL(S_BSC.S_BSC,0) AS DECIMAL(10,2)) AS Total "
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= ",CAST(ISNULL(A_BSC.A_BSC,0) AS DECIMAL(10,2)) AS Total "
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= ",CAST(ISNULL(R_BSC.R_BSC,0) AS DECIMAL(10,2)) AS Total "
            End If
            'S.SANDEEP |21-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
            'StrQ &= "FROM hremployee_master " & _
            '        "   JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '        "   JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '        "   JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            selfemployeeunkid AS EmpId " & _
            '        "           ,perspectiveunkid AS Pid " & _
            '        "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS S_BSC " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "           AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
            '        "       GROUP BY selfemployeeunkid,perspectiveunkid " & _
            '        "   )AS S_BSC ON S_BSC.EmpId = hremployee_master.employeeunkid " & _
            '        "   JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            assessedemployeeunkid AS EmpId " & _
            '        "           ,perspectiveunkid " & _
            '        "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) / COUNT(DISTINCT assessormasterunkid)  AS A_BSC " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
            '        "       GROUP BY assessedemployeeunkid,perspectiveunkid " & _
            '        "   )AS A_BSC ON A_BSC.EmpId = hremployee_master.employeeunkid AND S_BSC.Pid = A_BSC.perspectiveunkid " & _
            '        "   JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            assessedemployeeunkid AS EmpId " & _
            '        "           ,perspectiveunkid " & _
            '        "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS R_BSC " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
            '        "       GROUP BY assessedemployeeunkid,perspectiveunkid " & _
            '        "   )AS R_BSC ON R_BSC.EmpId = hremployee_master.employeeunkid AND S_BSC.Pid = R_BSC.perspectiveunkid "
            StrQ &= "FROM hremployee_master " & _
                    "   JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                    "   JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
                    "   JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            selfemployeeunkid AS EmpId " & _
                    "           ,perspectiveunkid AS Pid " & _
                    "           ,CAST(SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS DECIMAL(36,2)) AS S_BSC " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "           AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
                    "       GROUP BY selfemployeeunkid,perspectiveunkid " & _
                    "   )AS S_BSC ON S_BSC.EmpId = hremployee_master.employeeunkid " & _
                    "   JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            assessedemployeeunkid AS EmpId " & _
                    "           ,perspectiveunkid " & _
                    "           ,CAST(SUM(ISNULL(hrgoals_analysis_tran.result,0)) / COUNT(DISTINCT assessormasterunkid) AS DECIMAL(36,2)) AS A_BSC " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
                    "       GROUP BY assessedemployeeunkid,perspectiveunkid " & _
                    "   )AS A_BSC ON A_BSC.EmpId = hremployee_master.employeeunkid AND S_BSC.Pid = A_BSC.perspectiveunkid " & _
                    "   JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            assessedemployeeunkid AS EmpId " & _
                    "           ,perspectiveunkid " & _
                    "           ,CAST(SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS DECIMAL(36,2)) AS R_BSC " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
                    "       GROUP BY assessedemployeeunkid,perspectiveunkid " & _
                    "   )AS R_BSC ON R_BSC.EmpId = hremployee_master.employeeunkid AND S_BSC.Pid = R_BSC.perspectiveunkid "
            'S.SANDEEP |21-AUG-2019| -- END
            Select Case mintAppointmentTypeId
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                    End If
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    If mdtDate1 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                    End If
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    If mdtDate1 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                    End If
            End Select
            If CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= " AND S_BSC.Pid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST((ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/3 AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "'"
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= " AND S_BSC.Pid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST((ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0))/2 AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "'"
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= " AND S_BSC.Pid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST((ISNULL(S_BSC.S_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "'"
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= " AND A_BSC.perspectiveunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST((ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "'"
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= " AND S_BSC.Pid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST(ISNULL(S_BSC.S_BSC,0) AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "'"
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= " AND A_BSC.perspectiveunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST(ISNULL(A_BSC.A_BSC,0) AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "'"
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= " AND R_BSC.perspectiveunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST(ISNULL(R_BSC.R_BSC,0) AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "'"
            End If

            StrQ &= ") AS A WHERE 1 = 1 "

            dsList = objDataOpetation.ExecQuery(StrQ, "List")

            If objDataOpetation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpetation.ErrorNumber & " : " & objDataOpetation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetBSC_PerspectiveFilter", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function GetSummaryFilter(ByVal dRow As DataRow, ByVal iPeriodId As Integer, ByVal blnConsiderItemWeightAsNumber As Boolean, ByVal blnIsSelfAssignCompetencies As Boolean) As DataSet
        'Private Function GetSummaryFilter(ByVal dRow As DataRow, ByVal iPeriodId As Integer) As DataSet
        'S.SANDEEP [04 JUN 2015] -- END
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOpetation As New clsDataOperation
        Dim dsList As New DataSet
        Try
            StrQ = " SELECT employeeunkid,ECODE,ENAME,appointeddate,EMAIL,F_RefId,EGUID,JOB_TITLE,DEPT,TotalScore " & _
                   " FROM (" & _
                   "    SELECT " & _
                   "         employeeunkid " & _
                   "        ,employeecode AS ECODE " & _
                   "        ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
                   "        ,CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
                   "        ,ISNULL(email,'') AS EMAIL " & _
                   "        ,'" & dRow.Item("filter_refid") & "' AS F_RefId " & _
                   "        ,'" & dRow.Item("GUID") & "' AS EGUID " & _
                   "        ,ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
                   "        ,ISNULL(hrdepartment_master.name,'') AS DEPT "

            Select Case CInt(dRow.Item("filter_refid"))
                Case Filter_Refid.TOTAL_BSC_SCORE
                    If CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                        StrQ &= ", CAST((ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/3 AS DECIMAL(10,2)) AS TotalScore "
                    ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                        StrQ &= ",CAST((ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0))/2 AS DECIMAL(10,2)) AS TotalScore "
                    ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                        StrQ &= ",CAST((ISNULL(S_BSC.S_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 AS DECIMAL(10,2)) AS TotalScore "
                    ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                        StrQ &= ",CAST((ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 AS DECIMAL(10,2)) AS TotalScore "
                    ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                        StrQ &= ",CAST(ISNULL(S_BSC.S_BSC,0) AS DECIMAL(10,2)) AS TotalScore "
                    ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                        StrQ &= ",CAST(ISNULL(A_BSC.A_BSC,0) AS DECIMAL(10,2)) AS TotalScore "
                    ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                        StrQ &= ",CAST(ISNULL(R_BSC.R_BSC,0) AS DECIMAL(10,2)) AS TotalScore "
                    End If
                Case Filter_Refid.TOTAL_GE_SCORE
                    If CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                        StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/3 AS DECIMAL(10,2)) AS TotalScore "
                    ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                        StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0))/2 AS DECIMAL(10,2)) AS TotalScore "
                    ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                        StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(R_GA.R_GA,0))/2 AS DECIMAL(10,2)) AS TotalScore "
                    ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                        StrQ &= ",CAST((ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/2 AS DECIMAL(10,2)) AS TotalScore "
                    ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                        StrQ &= ",CAST(ISNULL(S_GA.S_GA,0) AS DECIMAL(10,2)) AS TotalScore "
                    ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                        StrQ &= ",CAST(ISNULL(A_GA.A_GA,0) AS DECIMAL(10,2)) AS TotalScore "
                    ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                        StrQ &= ",CAST(ISNULL(R_GA.R_GA,0) AS DECIMAL(10,2)) AS TotalScore "
                    End If
            End Select
            'S.SANDEEP |21-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
            'StrQ &= "FROM hremployee_master " & _
            '        "   JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '        "   JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '        "   LEFT JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            selfemployeeunkid AS EmpId " & _
            '        "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS S_GA " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
            '        "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
            '        "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "           AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
            '        "       GROUP BY selfemployeeunkid " & _
            '        "   )AS S_GA ON S_GA.EmpId = hremployee_master.employeeunkid " & _
            '        "   LEFT JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            selfemployeeunkid AS EmpId " & _
            '        "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS S_BSC " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "       AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
            '        "       GROUP BY selfemployeeunkid " & _
            '        "   )AS S_BSC ON S_BSC.EmpId = hremployee_master.employeeunkid " & _
            '        "   LEFT JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            assessedemployeeunkid AS EmpId " & _
            '        "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) / COUNT(DISTINCT assessormasterunkid)  AS A_GA " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
            '        "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
            '        "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
            '        "       GROUP BY assessedemployeeunkid " & _
            '        "   )AS A_GA ON A_GA.EmpId = hremployee_master.employeeunkid " & _
            '        "   LEFT JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            assessedemployeeunkid AS EmpId " & _
            '        "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) / COUNT(DISTINCT assessormasterunkid)  AS A_BSC " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
            '        "       GROUP BY assessedemployeeunkid " & _
            '        "   )AS A_BSC ON A_BSC.EmpId = hremployee_master.employeeunkid " & _
            '        "   LEFT JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            assessedemployeeunkid AS EmpId " & _
            '        "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS R_GA " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
            '        "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
            '        "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
            '        "       GROUP BY assessedemployeeunkid " & _
            '        "   )AS R_GA ON R_GA.EmpId = hremployee_master.employeeunkid " & _
            '        "   LEFT JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            assessedemployeeunkid AS EmpId " & _
            '        "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS R_BSC " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
            '        "       GROUP BY assessedemployeeunkid " & _
            '        "   )AS R_BSC ON R_BSC.EmpId = hremployee_master.employeeunkid " & _
            '        "WHERE 1 = 1 "
            StrQ &= "FROM hremployee_master " & _
                    "   JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                    "   JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            selfemployeeunkid AS EmpId " & _
                    "           ,CAST(SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS DECIMAL(36,2)) AS S_GA " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                    "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                    "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "           AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
                    "       GROUP BY selfemployeeunkid " & _
                    "   )AS S_GA ON S_GA.EmpId = hremployee_master.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            selfemployeeunkid AS EmpId " & _
                    "           ,CAST(SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS DECIMAL(36,2)) AS S_BSC " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "       AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
                    "       GROUP BY selfemployeeunkid " & _
                    "   )AS S_BSC ON S_BSC.EmpId = hremployee_master.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            assessedemployeeunkid AS EmpId " & _
                    "           ,CAST(SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) / COUNT(DISTINCT assessormasterunkid) AS DECIMAL(36,2)) AS A_GA " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                    "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                    "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
                    "       GROUP BY assessedemployeeunkid " & _
                    "   )AS A_GA ON A_GA.EmpId = hremployee_master.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            assessedemployeeunkid AS EmpId " & _
                    "           ,CAST(SUM(ISNULL(hrgoals_analysis_tran.result,0)) / COUNT(DISTINCT assessormasterunkid) AS DECIMAL(36,2)) AS A_BSC " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
                    "       GROUP BY assessedemployeeunkid " & _
                    "   )AS A_BSC ON A_BSC.EmpId = hremployee_master.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            assessedemployeeunkid AS EmpId " & _
                    "           ,CAST(SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS DECIMAL(36,2)) AS R_GA " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                    "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                    "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
                    "       GROUP BY assessedemployeeunkid " & _
                    "   )AS R_GA ON R_GA.EmpId = hremployee_master.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            assessedemployeeunkid AS EmpId " & _
                    "           ,CAST(SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS DECIMAL(36,2)) AS R_BSC " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
                    "       GROUP BY assessedemployeeunkid " & _
                    "   )AS R_BSC ON R_BSC.EmpId = hremployee_master.employeeunkid " & _
                    "WHERE 1 = 1 "
            'S.SANDEEP |21-AUG-2019| -- END
            Select Case mintAppointmentTypeId
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                    End If
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    If mdtDate1 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                    End If
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    If mdtDate1 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                    End If
            End Select

            Select Case CInt(dRow.Item("filter_refid"))
                Case Filter_Refid.TOTAL_BSC_SCORE
                    If CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                        StrQ &= " AND CAST((ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/3 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
                    ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                        StrQ &= " AND CAST((ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
                    ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                        StrQ &= " AND CAST((ISNULL(S_BSC.S_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
                    ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                        StrQ &= " AND CAST((ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
                    ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                        StrQ &= " AND CAST(ISNULL(S_BSC.S_BSC,0) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
                    ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                        StrQ &= " AND CAST(ISNULL(A_BSC.A_BSC,0) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
                    ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                        StrQ &= " AND CAST(ISNULL(R_BSC.R_BSC,0) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
                    End If
                Case Filter_Refid.TOTAL_GE_SCORE
                    If CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                        StrQ &= " AND CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/3 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
                    ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                        StrQ &= " AND CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
                    ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                        StrQ &= " AND CAST((ISNULL(S_GA.S_GA,0) + ISNULL(R_GA.R_GA,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
                    ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                        StrQ &= " AND CAST((ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
                    ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                        StrQ &= " AND CAST(ISNULL(S_GA.S_GA,0) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
                    ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                        StrQ &= " AND CAST(ISNULL(A_GA.A_GA,0) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
                    ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                        StrQ &= " AND CAST(ISNULL(R_GA.R_GA,0) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
                    End If
            End Select

            StrQ &= ") AS A WHERE 1 = 1 "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOpetation.AddParameter("@WeightAsNumber", SqlDbType.Bit, eZeeDataType.BIT_SIZE, ConfigParameter._Object._ConsiderItemWeightAsNumber)
            objDataOpetation.AddParameter("@WeightAsNumber", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnConsiderItemWeightAsNumber)
            'S.SANDEEP [04 JUN 2015] -- END


            dsList = objDataOpetation.ExecQuery(StrQ, "List")

            If objDataOpetation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpetation.ErrorNumber & " : " & objDataOpetation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetSummaryFilter", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private Function Get_Overall_Filter(ByVal dRow As DataRow, ByVal iPeriodId As Integer, ByVal blnConsiderItemWeightAsNumber As Boolean, ByVal blnIsSelfAssignCompetencies As Boolean) As DataSet
        'Private Function Get_Overall_Filter(ByVal dRow As DataRow, ByVal iPeriodId As Integer) As DataSet
        'S.SANDEEP [04 JUN 2015] -- END

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Try
            StrQ = " SELECT employeeunkid,ECODE,ENAME,appointeddate,EMAIL,F_RefId,EGUID,JOB_TITLE,DEPT,Overall_Score " & _
                   " FROM (" & _
                   "    SELECT " & _
                   "         employeeunkid " & _
                   "        ,employeecode AS ECODE " & _
                   "        ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
                   "        ,CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
                   "        ,ISNULL(email,'') AS EMAIL " & _
                   "        ,'" & dRow.Item("filter_refid") & "' AS F_RefId " & _
                   "        ,'" & dRow.Item("GUID") & "' AS EGUID " & _
                   "        ,ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
                   "        ,ISNULL(hrdepartment_master.name,'') AS DEPT "

            If CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/3 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/3 *(bsc_weight/100) AS DECIMAL(10,2)) AS Overall_Score "
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Overall_Score "
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Overall_Score "
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= ",CAST((ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Overall_Score "
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= ",CAST((ISNULL(S_GA.S_GA,0)) * (ge_weight/100) + (ISNULL(S_BSC.S_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Overall_Score "
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= ",CAST((ISNULL(A_GA.A_GA,0)) * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Overall_Score "
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= ",CAST((ISNULL(R_GA.R_GA,0)) * (ge_weight/100)+(ISNULL(R_BSC.R_BSC,0))*(bsc_weight/100) AS DECIMAL(10,2)) AS Overall_Score "
            End If
            'S.SANDEEP |21-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
            'StrQ &= "FROM hremployee_master " & _
            '        "   JOIN hrassessment_ratio ON hremployee_master.jobgroupunkid = hrassessment_ratio.jobgroupunkid AND periodunkid = '" & iPeriodId & "' AND hrassessment_ratio.isactive = 1 " & _
            '        "   JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
            '        "   JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
            '        "   LEFT JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            selfemployeeunkid AS EmpId " & _
            '        "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS S_GA " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
            '        "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
            '        "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "           AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
            '        "       GROUP BY selfemployeeunkid " & _
            '        "   )AS S_GA ON S_GA.EmpId = hremployee_master.employeeunkid " & _
            '        "   LEFT JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            selfemployeeunkid AS EmpId " & _
            '        "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS S_BSC " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "       AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
            '        "       GROUP BY selfemployeeunkid " & _
            '        "   )AS S_BSC ON S_BSC.EmpId = hremployee_master.employeeunkid " & _
            '        "   LEFT JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            assessedemployeeunkid AS EmpId " & _
            '        "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) / COUNT(DISTINCT assessormasterunkid)  AS A_GA " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
            '        "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
            '        "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
            '        "       GROUP BY assessedemployeeunkid " & _
            '        "   )AS A_GA ON A_GA.EmpId = hremployee_master.employeeunkid " & _
            '        "   LEFT JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            assessedemployeeunkid AS EmpId " & _
            '        "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) / COUNT(DISTINCT assessormasterunkid)  AS A_BSC " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
            '        "       GROUP BY assessedemployeeunkid " & _
            '        "   )AS A_BSC ON A_BSC.EmpId = hremployee_master.employeeunkid " & _
            '        "   LEFT JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            assessedemployeeunkid AS EmpId " & _
            '        "           ,SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS R_GA " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
            '        "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
            '        "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
            '        "       GROUP BY assessedemployeeunkid " & _
            '        "   )AS R_GA ON R_GA.EmpId = hremployee_master.employeeunkid " & _
            '        "   LEFT JOIN " & _
            '        "   ( " & _
            '        "       SELECT " & _
            '        "            assessedemployeeunkid AS EmpId " & _
            '        "           ,SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS R_BSC " & _
            '        "       FROM hrevaluation_analysis_master " & _
            '        "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
            '        "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
            '        "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
            '        "       GROUP BY assessedemployeeunkid " & _
            '        "   )AS R_BSC ON R_BSC.EmpId = hremployee_master.employeeunkid " & _
            '        "WHERE 1 = 1 "
            StrQ &= "FROM hremployee_master " & _
                    "   JOIN hrassessment_ratio ON hremployee_master.jobgroupunkid = hrassessment_ratio.jobgroupunkid AND periodunkid = '" & iPeriodId & "' AND hrassessment_ratio.isactive = 1 " & _
                    "   JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                    "   JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            selfemployeeunkid AS EmpId " & _
                    "           ,CAST(SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS DECIMAL(36,2)) AS S_GA " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                    "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                    "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "           AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
                    "       GROUP BY selfemployeeunkid " & _
                    "   )AS S_GA ON S_GA.EmpId = hremployee_master.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            selfemployeeunkid AS EmpId " & _
                    "           ,CAST(SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS DECIMAL(36,2)) AS S_BSC " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "       AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
                    "       GROUP BY selfemployeeunkid " & _
                    "   )AS S_BSC ON S_BSC.EmpId = hremployee_master.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            assessedemployeeunkid AS EmpId " & _
                    "           ,CAST(SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) / COUNT(DISTINCT assessormasterunkid) AS DECIMAL(36,2)) AS A_GA " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                    "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                    "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
                    "       GROUP BY assessedemployeeunkid " & _
                    "   )AS A_GA ON A_GA.EmpId = hremployee_master.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            assessedemployeeunkid AS EmpId " & _
                    "           ,CAST(SUM(ISNULL(hrgoals_analysis_tran.result,0)) / COUNT(DISTINCT assessormasterunkid) AS DECIMAL(36,2))  AS A_BSC " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "           AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
                    "       GROUP BY assessedemployeeunkid " & _
                    "   )AS A_BSC ON A_BSC.EmpId = hremployee_master.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            assessedemployeeunkid AS EmpId " & _
                    "           ,CAST(SUM(ISNULL((hrcompetency_analysis_tran.result * (CASE WHEN @WeightAsNumber = CAST(1 AS BIT) THEN hrcompetency_analysis_tran.result ELSE (hrassess_competence_assign_tran.weight)/100 END)) * (hrassess_group_master.weight/100),0)) AS DECIMAL(36,2)) AS R_GA " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "           JOIN hrassess_competence_assign_tran ON hrassess_competence_assign_tran.competenciesunkid = hrcompetency_analysis_tran.competenciesunkid " & _
                    "           JOIN hrassess_competence_assign_master ON hrassess_competence_assign_master.assigncompetenceunkid = hrassess_competence_assign_tran.assigncompetenceunkid " & _
                    "               AND hrassess_competence_assign_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "           JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrcompetency_analysis_tran.assessgroupunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
                    "       GROUP BY assessedemployeeunkid " & _
                    "   )AS R_GA ON R_GA.EmpId = hremployee_master.employeeunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            assessedemployeeunkid AS EmpId " & _
                    "           ,CAST(SUM(ISNULL(hrgoals_analysis_tran.result,0)) AS DECIMAL(36,2)) AS R_BSC " & _
                    "       FROM hrevaluation_analysis_master " & _
                    "           JOIN hrgoals_analysis_tran ON hrgoals_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                    "       WHERE hrevaluation_analysis_master.isvoid = 0 AND hrgoals_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.periodunkid = '" & iPeriodId & "' " & _
                    "           AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
                    "       GROUP BY assessedemployeeunkid " & _
                    "   )AS R_BSC ON R_BSC.EmpId = hremployee_master.employeeunkid " & _
                    "WHERE 1 = 1 "
            'S.SANDEEP |21-AUG-2019| -- END
            Select Case mintAppointmentTypeId
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                    End If
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    If mdtDate1 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                    End If
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    If mdtDate1 <> Nothing Then
                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                    End If
            End Select
            If CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/3 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/3 *(bsc_weight/100)) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0))/2 *(bsc_weight/100)) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0)+ ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100)) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= " AND CAST(((ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100)) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0)) * (ge_weight/100) + (ISNULL(S_BSC.S_BSC,0)) * (bsc_weight/100)) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
                StrQ &= " AND CAST(((ISNULL(A_GA.A_GA,0)) * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0)) * (bsc_weight/100)) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
                StrQ &= " AND CAST(((ISNULL(R_GA.R_GA,0)) * (ge_weight/100)+(ISNULL(R_BSC.R_BSC,0))*(bsc_weight/100)) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
            End If
            StrQ &= ") AS A WHERE 1 = 1 AND Overall_Score > 0 "

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@WeightAsNumber", SqlDbType.Bit, eZeeDataType.BIT_SIZE, ConfigParameter._Object._ConsiderItemWeightAsNumber)
            objDataOperation.AddParameter("@WeightAsNumber", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnConsiderItemWeightAsNumber)
            'S.SANDEEP [04 JUN 2015] -- END


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Overall_Filter", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'S.SANDEEP [19 FEB 2015] -- START
    Private Function GetNewOverallFilter(ByVal xtmp As DataRow, _
                                         ByVal xPeriodId As Integer, _
                                         ByVal xFilterBy As Integer, _
                                         ByVal xScoringId As Integer, _
                                         ByVal xEmployeeAsOnDate As DateTime, _
                                         ByVal blnIsSelfAssignCompetencies As Boolean, _
                                         ByVal blnUsedAgreedScore As Boolean, _
                                         ByVal mblnIsCalibrationSettingActive As Boolean) As DataSet 'Shani (23-Nov-2016) -- [blnUsedAgreedScore]
        'S.SANDEEP |27-MAY-2019| -- START {mblnIsCalibrationSettingActive} -- END
        Dim StrQ As String = String.Empty
        Dim dsEmpllst As New DataSet
        Dim dsAsrRevlst As New DataSet
        Dim dsFinalEmp As New DataSet
        Try
            'S.SANDEEP |24-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
            Dim mDecRoundingFactor As Decimal = 0
            mDecRoundingFactor = GetRoundingFactor(xPeriodId)
            'S.SANDEEP |24-DEC-2019| -- END

            Using objDo As New clsDataOperation

                'S.SANDEEP [04-AUG-2017] -- START
                'ISSUE/ENHANCEMENT : CHANGE THE SCORE PICKING METHOD
                '    StrQ = "SELECT " & _
                '               "     employeeunkid " & _
                '               "    ,employeecode " & _
                '               "    ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
                '               "    ,CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
                '               "    ,ISNULL(email,'') AS EMAIL " & _
                '               "    ,ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
                '               "    ,ISNULL(hrdepartment_master.name,'') AS DEPT " & _
                '               "    ,0 AS SCORE " & _
                '               "    ,'" & xtmp.Item("filter_refid") & "' AS F_RefId " & _
                '               "    ,'" & xtmp.Item("GUID") & "' AS EGUID " & _
                '               "    ,CAST(0 AS BIT) AS isvalid " & _
                '               "FROM hremployee_master " & _
                '               "    JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
                '               "    JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
                '               "WHERE 1 = 1 " & _
                '               " AND employeeunkid " & _
                '               "IN " & _
                '               "( " & _
                '               "   SELECT " & _
                '               "       A.employeeid " & _
                '               "   FROM " & _
                '               "   ( " & _
                '               "      SELECT selfemployeeunkid AS employeeid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' " & _
                '               "      UNION SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' " & _
                '               "      UNION SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' " & _
                '               "   ) AS A " & _
                '               ")"

                '    Select Case mintAppointmentTypeId
                '        Case enAD_Report_Parameter.APP_DATE_FROM
                '            If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                '                StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                '            End If
                '        Case enAD_Report_Parameter.APP_DATE_BEFORE
                '            If mdtDate1 <> Nothing Then
                '                StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                '            End If
                '        Case enAD_Report_Parameter.APP_DATE_AFTER
                '            If mdtDate1 <> Nothing Then
                '                StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                '            End If
                '    End Select

                '    dsEmpllst = objDo.ExecQuery(StrQ, "List")

                '    If objDo.ErrorMessage <> "" Then
                '        Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                '    End If

                '    StrQ = "SELECT " & _
                '           "     selfemployeeunkid AS EmpId " & _
                '           "    ,assessormasterunkid " & _
                '           "    ,assessmodeid " & _
                '           "FROM hrevaluation_analysis_master " & _
                '           "WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' " & _
                '           "UNION " & _
                '           "SELECT " & _
                '           "     assessedemployeeunkid AS EmpId " & _
                '           "    ,assessormasterunkid " & _
                '           "    ,assessmodeid " & _
                '           "FROM hrevaluation_analysis_master " & _
                '           "WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' " & _
                '           "UNION " & _
                '           "SELECT " & _
                '           "     assessedemployeeunkid AS EmpId " & _
                '           "    ,assessormasterunkid " & _
                '           "    ,assessmodeid " & _
                '           "FROM hrevaluation_analysis_master " & _
                '           "WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "

                '    dsAsrRevlst = objDo.ExecQuery(StrQ, "List")

                '    If objDo.ErrorMessage <> "" Then
                '        Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                '    End If

                'End Using

                'If dsEmpllst.Tables(0).Rows.Count > 0 Then
                '    Dim xTotalScore As Decimal = 0
                '    Dim xEvaluation As New clsevaluation_analysis_master
                '    For Each xRow As DataRow In dsEmpllst.Tables(0).Rows
                '        Select Case xFilterBy
                '            Case enApprFilterTypeId.APRL_OVER_ALL

                '                'Shani (23-Nov-2016) -- Start
                '                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                'xTotalScore = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, xScoringId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , , True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                xTotalScore = xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                '                                                        IsBalanceScoreCard:=True, _
                '                                                        xScoreOptId:=xScoringId, _
                '                                                        xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
                '                                                        xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                        xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                        xPeriodId:=xPeriodId, _
                '                                                        xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                        xOnlyCommited:=True, _
                '                                                        xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                'Shani (23-Nov-2016) -- End


                '                If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
                '                    Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "'")
                '                    If xAsrRev.Length > 0 Then

                '                        'Shani (23-Nov-2016) -- Start
                '                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                        'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, True, xScoringId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                        xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                '                                                                 IsBalanceScoreCard:=True, _
                '                                                                 xScoreOptId:=xScoringId, _
                '                                                                 xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
                '                                                                 xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                                 xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                                 xPeriodId:=xPeriodId, _
                '                                                                 xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                 xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
                '                                                                 xOnlyCommited:=True, _
                '                                                                 xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                        'Shani (23-Nov-2016) -- End


                '                    End If
                '                End If
                '                If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
                '                    Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'")
                '                    If xAsrRev.Length > 0 Then

                '                        'Shani (23-Nov-2016) -- Start
                '                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                        'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, True, xScoringId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                        xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                '                                                                 IsBalanceScoreCard:=True, _
                '                                                                 xScoreOptId:=xScoringId, _
                '                                                                 xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
                '                                                                 xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                                 xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                                 xPeriodId:=xPeriodId, _
                '                                                                 xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                 xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
                '                                                                 xOnlyCommited:=True, _
                '                                                                 xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                        'Shani (23-Nov-2016) -- End


                '                    End If
                '                End If

                '                'Shani (23-Nov-2016) -- Start
                '                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, xScoringId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , , True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                '                                                         IsBalanceScoreCard:=False, _
                '                                                         xScoreOptId:=xScoringId, _
                '                                                         xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
                '                                                         xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                         xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                         xPeriodId:=xPeriodId, _
                '                                                         xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                         xOnlyCommited:=True, _
                '                                                         xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                'Shani (23-Nov-2016) -- End


                '                If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
                '                    Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "'")
                '                    If xAsrRev.Length > 0 Then

                '                        'Shani (23-Nov-2016) -- Start
                '                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                        'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, False, xScoringId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                        xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                '                                                                 IsBalanceScoreCard:=False, _
                '                                                                 xScoreOptId:=xScoringId, _
                '                                                                 xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
                '                                                                 xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                                 xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                                 xPeriodId:=xPeriodId, _
                '                                                                 xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                 xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
                '                                                                 xOnlyCommited:=True, _
                '                                                                 xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                        'Shani (23-Nov-2016) -- End


                '                    End If
                '                End If
                '                If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
                '                    Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'")
                '                    If xAsrRev.Length > 0 Then

                '                        'Shani (23-Nov-2016) -- Start
                '                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                        'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, False, xScoringId, enAssess_Computation_Formulas.FINAL_RESULT_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                        xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                '                                                                 IsBalanceScoreCard:=False, _
                '                                                                 xScoreOptId:=xScoringId, _
                '                                                                 xCompute_Formula:=enAssess_Computation_Formulas.FINAL_RESULT_SCORE, _
                '                                                                 xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                                 xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                                 xPeriodId:=xPeriodId, _
                '                                                                 xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                 xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
                '                                                                 xOnlyCommited:=True, _
                '                                                                 xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                        'Shani (23-Nov-2016) -- End


                '                    End If
                '                End If

                '                Call Set_Valid_Filter(xRow, xTotalScore, xtmp)

                '            Case enApprFilterTypeId.APRL_EMPLOYEE

                '                'Shani (23-Nov-2016) -- Start
                '                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                'xTotalScore = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, xScoringId, enAssess_Computation_Formulas.EMP_OVERALL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , , True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, xScoringId, enAssess_Computation_Formulas.EMP_OVERALL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , , True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END

                '                xTotalScore = xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                '                                                        IsBalanceScoreCard:=True, _
                '                                                        xScoreOptId:=xScoringId, _
                '                                                        xCompute_Formula:=enAssess_Computation_Formulas.EMP_OVERALL_SCORE, _
                '                                                        xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                        xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                        xPeriodId:=xPeriodId, _
                '                                                        xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                        xOnlyCommited:=True, _
                '                                                        xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)

                '                xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                '                                                         IsBalanceScoreCard:=False, _
                '                                                         xScoreOptId:=xScoringId, _
                '                                                         xCompute_Formula:=enAssess_Computation_Formulas.EMP_OVERALL_SCORE, _
                '                                                         xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                         xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                         xPeriodId:=xPeriodId, _
                '                                                         xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                         xOnlyCommited:=True, _
                '                                                         xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                'Shani (23-Nov-2016) -- End
                '                Call Set_Valid_Filter(xRow, xTotalScore, xtmp)

                '            Case enApprFilterTypeId.APRL_ASSESSOR

                '                If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
                '                    Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "'")
                '                    If xAsrRev.Length > 0 Then
                '                        'Shani (23-Nov-2016) -- Start
                '                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                        'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, True, xScoringId, enAssess_Computation_Formulas.ASR_OVERALL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                        xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                '                                                                 IsBalanceScoreCard:=True, _
                '                                                                 xScoreOptId:=xScoringId, _
                '                                                                 xCompute_Formula:=enAssess_Computation_Formulas.ASR_OVERALL_SCORE, _
                '                                                                 xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                                 xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                                 xPeriodId:=xPeriodId, _
                '                                                                 xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                 xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
                '                                                                 xOnlyCommited:=True, _
                '                                                                 xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                        'Shani (23-Nov-2016) -- End
                '                    End If
                '                End If

                '                If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
                '                    Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "'")
                '                    If xAsrRev.Length > 0 Then

                '                        'Shani (23-Nov-2016) -- Start
                '                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                        'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, False, xScoringId, enAssess_Computation_Formulas.ASR_OVERALL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                        xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                '                                                                 IsBalanceScoreCard:=False, _
                '                                                                 xScoreOptId:=xScoringId, _
                '                                                                 xCompute_Formula:=enAssess_Computation_Formulas.ASR_OVERALL_SCORE, _
                '                                                                 xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                                 xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                                 xPeriodId:=xPeriodId, _
                '                                                                 xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                 xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
                '                                                                 xOnlyCommited:=True, _
                '                                                                 xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                        'Shani (23-Nov-2016) -- End


                '                    End If
                '                End If

                '                Call Set_Valid_Filter(xRow, xTotalScore, xtmp)

                '            Case enApprFilterTypeId.APRL_REVIEWER
                '                If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
                '                    Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'")
                '                    If xAsrRev.Length > 0 Then

                '                        'Shani (23-Nov-2016) -- Start
                '                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                        'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, True, xScoringId, enAssess_Computation_Formulas.REV_OVERALL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                        xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                '                                                                 IsBalanceScoreCard:=True, _
                '                                                                 xScoreOptId:=xScoringId, _
                '                                                                 xCompute_Formula:=enAssess_Computation_Formulas.REV_OVERALL_SCORE, _
                '                                                                 xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                                 xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                                 xPeriodId:=xPeriodId, _
                '                                                                 xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                 xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
                '                                                                 xOnlyCommited:=True, _
                '                                                                 xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                        'Shani (23-Nov-2016) -- End


                '                    End If
                '                End If
                '                If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
                '                    Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'")
                '                    If xAsrRev.Length > 0 Then

                '                        'Shani (23-Nov-2016) -- Start
                '                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                        'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, False, xScoringId, enAssess_Computation_Formulas.REV_OVERALL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                        xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                '                                                                 IsBalanceScoreCard:=False, _
                '                                                                 xScoreOptId:=xScoringId, _
                '                                                                 xCompute_Formula:=enAssess_Computation_Formulas.REV_OVERALL_SCORE, _
                '                                                                 xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                                 xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                                 xPeriodId:=xPeriodId, _
                '                                                                 xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                 xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
                '                                                                 xOnlyCommited:=True, _
                '                                                                 xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                        'Shani (23-Nov-2016) -- End


                '                    End If
                '                End If

                '                Call Set_Valid_Filter(xRow, xTotalScore, xtmp)

                '        End Select
                '    Next
                '    xEvaluation = Nothing
                'End If

                'Dim xtab As DataTable = Nothing
                'xtab = New DataView(dsEmpllst.Tables(0), "isvalid = 1", "", DataViewRowState.CurrentRows).ToTable
                'dsFinalEmp.Tables.Add(xtab)

                'S.SANDEEP |27-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
                'StrQ = "SELECT DISTINCT " & _
                '       "     hremployee_master.employeeunkid " & _
                '       "    ,employeecode " & _
                '       "    ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
                '       "    ,CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
                '       "    ,ISNULL(email,'') AS EMAIL " & _
                '       "    ,ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
                '       "    ,ISNULL(hrdepartment_master.name,'') AS DEPT " & _
                '       "    ,CASE WHEN @FilterTypeId = '" & CInt(enApprFilterTypeId.APRL_OVER_ALL) & "' THEN ISNULL(os.finaloverallscore,0) " & _
                '       "          WHEN @FilterTypeId = '" & CInt(enApprFilterTypeId.APRL_EMPLOYEE) & "' THEN ISNULL(ems.formula_value,0) " & _
                '       "          WHEN @FilterTypeId = '" & CInt(enApprFilterTypeId.APRL_ASSESSOR) & "' THEN ISNULL(ars.formula_value,0) " & _
                '       "          WHEN @FilterTypeId = '" & CInt(enApprFilterTypeId.APRL_REVIEWER) & "' THEN ISNULL(rvs.formula_value,0) " & _
                '       "     END AS SCORE " & _
                '       "    ,'" & xtmp.Item("filter_refid") & "' AS F_RefId " & _
                '       "    ,'" & xtmp.Item("GUID") & "' AS EGUID " & _
                '       "    ,CAST(1 AS BIT) AS isvalid " & _
                '       "FROM hremployee_master " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT " & _
                '       "         employeeunkid " & _
                '       "        ,jobunkid " & _
                '       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                '       "    FROM hremployee_categorization_tran " & _
                '       "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                '       ") AS cc ON cc.employeeunkid = hremployee_master.employeeunkid AND cc.xno = 1 " & _
                '       "JOIN hrjob_master ON cc.jobunkid = hrjob_master.jobunkid " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "   SELECT " & _
                '       "         employeeunkid " & _
                '       "        ,departmentunkid " & _
                '       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                '       "    FROM hremployee_transfer_tran " & _
                '       "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                '       ") AS ac ON ac.employeeunkid = hremployee_master.employeeunkid AND ac.xno = 1 " & _
                '       "JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = ac.departmentunkid " & _
                '       "JOIN " & _
                '       "( " & _
                '       "    SELECT DISTINCT " & _
                '       "        CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeid " & _
                '       "    FROM hrevaluation_analysis_master AS EM " & _
                '       "    WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = @periodunkid " & _
                '       ")AS ev ON ev.employeeid = hremployee_master.employeeunkid " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT DISTINCT " & _
                '       "         OVS.periodunkid " & _
                '       "        ,OVS.analysisunkid " & _
                '       "        ,CAST(OVS.finaloverallscore AS DECIMAL(10,2)) AS finaloverallscore " & _
                '       "        ,OVS.employeeunkid " & _
                '       "   FROM " & _
                '       "   ( " & _
                '       "        SELECT " & _
                '       "             CSM.periodunkid " & _
                '       "            ,CSM.analysisunkid " & _
                '       "            ,CSM.finaloverallscore " & _
                '       "            ,ROW_NUMBER()OVER(PARTITION BY CSM.employeeunkid,CSM.periodunkid ORDER BY CSM.computationdate ASC) AS xNo " & _
                '       "            ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "        FROM hrassess_compute_score_master AS CSM " & _
                '       "            JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "        WHERE CSM.isvoid = 0 AND CSM.periodunkid = @periodunkid " & _
                '       "    ) AS OVS WHERE OVS.xNo = 1 " & _
                '       ") AS os ON os.employeeunkid = hremployee_master.employeeunkid " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT DISTINCT " & _
                '       "         CSM.periodunkid " & _
                '       "        ,CSM.analysisunkid " & _
                '       "        ,2 AS FilterTypeId " & _
                '       "        ,CAST((SUM(CST.formula_value)/2) AS DECIMAL(10,2)) AS formula_value " & _
                '       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "    FROM hrassess_compute_score_master AS CSM " & _
                '       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                '       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                '       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 1 AND CM.formula_typeid IN (1,4) " & _
                '       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                '       ") AS ems ON ems.employeeunkid = hremployee_master.employeeunkid " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT DISTINCT " & _
                '       "         CSM.periodunkid " & _
                '       "        ,CSM.analysisunkid " & _
                '       "        ,3 AS FilterTypeId " & _
                '       "        ,CAST((SUM(CST.formula_value)/2) AS DECIMAL(10,2)) AS formula_value " & _
                '       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "    FROM hrassess_compute_score_master AS CSM " & _
                '       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                '       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                '       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 2 AND CM.formula_typeid IN (2,5) " & _
                '       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                '       ") AS ars ON ars.employeeunkid = hremployee_master.employeeunkid " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT DISTINCT " & _
                '       "         CSM.periodunkid " & _
                '       "        ,CSM.analysisunkid " & _
                '       "        ,4 AS FilterTypeId " & _
                '       "        ,CAST((SUM(CST.formula_value)/2) AS DECIMAL(10,2)) AS formula_value " & _
                '       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "    FROM hrassess_compute_score_master AS CSM " & _
                '       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                '       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                '       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 3 AND CM.formula_typeid IN (3,6) " & _
                '       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                '       ") AS rvs ON rvs.employeeunkid = hremployee_master.employeeunkid " & _
                '       "WHERE 1 = 1 "

                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'StrQ = "DECLARE @Setting AS BIT ; SET @Setting = " & IIf(mblnIsCalibrationSettingActive = True, 1, 0) & " " & _
                '       "SELECT DISTINCT " & _
                '       "     hremployee_master.employeeunkid " & _
                '       "    ,employeecode " & _
                '       "    ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
                '       "    ,CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
                '       "    ,ISNULL(email,'') AS EMAIL " & _
                '       "    ,ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
                '       "    ,ISNULL(hrdepartment_master.name,'') AS DEPT " & _
                '       "    ,CASE WHEN @FilterTypeId = '" & CInt(enApprFilterTypeId.APRL_OVER_ALL) & "' THEN ISNULL(os.finaloverallscore,0) " & _
                '       "          WHEN @FilterTypeId = '" & CInt(enApprFilterTypeId.APRL_EMPLOYEE) & "' THEN ISNULL(ems.formula_value,0) " & _
                '       "          WHEN @FilterTypeId = '" & CInt(enApprFilterTypeId.APRL_ASSESSOR) & "' THEN ISNULL(ars.formula_value,0) " & _
                '       "          WHEN @FilterTypeId = '" & CInt(enApprFilterTypeId.APRL_REVIEWER) & "' THEN ISNULL(rvs.formula_value,0) " & _
                '       "     END AS SCORE " & _
                '       "    ,'" & xtmp.Item("filter_refid") & "' AS F_RefId " & _
                '       "    ,'" & xtmp.Item("GUID") & "' AS EGUID " & _
                '       "    ,CAST(1 AS BIT) AS isvalid " & _
                '       "FROM hremployee_master " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT " & _
                '       "         employeeunkid " & _
                '       "        ,jobunkid " & _
                '       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                '       "    FROM hremployee_categorization_tran " & _
                '       "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                '       ") AS cc ON cc.employeeunkid = hremployee_master.employeeunkid AND cc.xno = 1 " & _
                '       "JOIN hrjob_master ON cc.jobunkid = hrjob_master.jobunkid " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "   SELECT " & _
                '       "         employeeunkid " & _
                '       "        ,departmentunkid " & _
                '       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                '       "    FROM hremployee_transfer_tran " & _
                '       "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                '       ") AS ac ON ac.employeeunkid = hremployee_master.employeeunkid AND ac.xno = 1 " & _
                '       "JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = ac.departmentunkid " & _
                '       "JOIN " & _
                '       "( " & _
                '       "    SELECT DISTINCT " & _
                '       "        CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeid " & _
                '       "    FROM hrevaluation_analysis_master AS EM " & _
                '       "    WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = @periodunkid " & _
                '       ")AS ev ON ev.employeeid = hremployee_master.employeeunkid " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT DISTINCT " & _
                '       "         OVS.periodunkid " & _
                '       "        ,OVS.analysisunkid " & _
                '       "        ,CAST(OVS.finaloverallscore AS DECIMAL(10,2)) AS finaloverallscore " & _
                '       "        ,OVS.employeeunkid " & _
                '       "   FROM " & _
                '       "   ( " & _
                '       "        SELECT " & _
                '       "             CSM.periodunkid " & _
                '       "            ,CSM.analysisunkid " & _
                '       "            ,CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed,0) = 1 THEN CSM.calibrated_score " & _
                '       "                  WHEN @Setting = 1 AND ISNULL(CSM.isprocessed,0) = 0 THEN 0 " & _
                '       "                  WHEN @Setting = 0 THEN CSM.finaloverallscore " & _
                '       "             END AS finaloverallscore " & _
                '       "            ,ROW_NUMBER()OVER(PARTITION BY CSM.employeeunkid,CSM.periodunkid ORDER BY CSM.computationdate ASC) AS xNo " & _
                '       "            ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "        FROM hrassess_compute_score_master AS CSM " & _
                '       "            JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "        WHERE CSM.isvoid = 0 AND CSM.periodunkid = @periodunkid " & _
                '       "    ) AS OVS WHERE OVS.xNo = 1 " & _
                '       ") AS os ON os.employeeunkid = hremployee_master.employeeunkid " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT DISTINCT " & _
                '       "         CSM.periodunkid " & _
                '       "        ,CSM.analysisunkid " & _
                '       "        ,2 AS FilterTypeId " & _
                '       "        ,CAST((SUM(CST.formula_value)/2) AS DECIMAL(10,2)) AS formula_value " & _
                '       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "    FROM hrassess_compute_score_master AS CSM " & _
                '       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                '       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                '       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 1 AND CM.formula_typeid IN (1,4) " & _
                '       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                '       ") AS ems ON ems.employeeunkid = hremployee_master.employeeunkid " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT DISTINCT " & _
                '       "         CSM.periodunkid " & _
                '       "        ,CSM.analysisunkid " & _
                '       "        ,3 AS FilterTypeId " & _
                '       "        ,CAST((SUM(CST.formula_value)/2) AS DECIMAL(10,2)) AS formula_value " & _
                '       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "    FROM hrassess_compute_score_master AS CSM " & _
                '       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                '       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                '       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 2 AND CM.formula_typeid IN (2,5) " & _
                '       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                '       ") AS ars ON ars.employeeunkid = hremployee_master.employeeunkid " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT DISTINCT " & _
                '       "         CSM.periodunkid " & _
                '       "        ,CSM.analysisunkid " & _
                '       "        ,4 AS FilterTypeId " & _
                '       "        ,CAST((SUM(CST.formula_value)/2) AS DECIMAL(10,2)) AS formula_value " & _
                '       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "    FROM hrassess_compute_score_master AS CSM " & _
                '       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                '       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                '       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 3 AND CM.formula_typeid IN (3,6) " & _
                '       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                '       ") AS rvs ON rvs.employeeunkid = hremployee_master.employeeunkid " & _
                '       "WHERE 1 = 1 "

                StrQ = "DECLARE @Setting AS BIT ; SET @Setting = " & IIf(mblnIsCalibrationSettingActive = True, 1, 0) & " " & _
                       "SELECT DISTINCT " & _
                       "     hremployee_master.employeeunkid " & _
                       "    ,employeecode " & _
                       "    ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
                       "    ,CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
                       "    ,ISNULL(email,'') AS EMAIL " & _
                       "    ,ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
                       "    ,ISNULL(hrdepartment_master.name,'') AS DEPT " & _
                       "    ,CASE WHEN @FilterTypeId = '" & CInt(enApprFilterTypeId.APRL_OVER_ALL) & "' THEN CAST(ISNULL(os.finaloverallscore,0) AS DECIMAL(36,2)) " & _
                       "          WHEN @FilterTypeId = '" & CInt(enApprFilterTypeId.APRL_EMPLOYEE) & "' THEN CAST(ISNULL(ems.formula_value,0) AS DECIMAL(36,2)) " & _
                       "          WHEN @FilterTypeId = '" & CInt(enApprFilterTypeId.APRL_ASSESSOR) & "' THEN CAST(ISNULL(ars.formula_value,0) AS DECIMAL(36,2)) " & _
                       "          WHEN @FilterTypeId = '" & CInt(enApprFilterTypeId.APRL_REVIEWER) & "' THEN CAST(ISNULL(rvs.formula_value,0) AS DECIMAL(36,2)) " & _
                       "     END AS SCORE " & _
                       "    ,'" & xtmp.Item("filter_refid") & "' AS F_RefId " & _
                       "    ,'" & xtmp.Item("GUID") & "' AS EGUID " & _
                       "    ,CAST(1 AS BIT) AS isvalid " & _
                       "FROM hremployee_master " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "         employeeunkid " & _
                       "        ,jobunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                       "    FROM hremployee_categorization_tran " & _
                       "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                       ") AS cc ON cc.employeeunkid = hremployee_master.employeeunkid AND cc.xno = 1 " & _
                       "JOIN hrjob_master ON cc.jobunkid = hrjob_master.jobunkid " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "   SELECT " & _
                       "         employeeunkid " & _
                       "        ,departmentunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                       "    FROM hremployee_transfer_tran " & _
                       "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                       ") AS ac ON ac.employeeunkid = hremployee_master.employeeunkid AND ac.xno = 1 " & _
                       "JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = ac.departmentunkid " & _
                       "JOIN " & _
                       "( " & _
                       "    SELECT DISTINCT " & _
                       "        CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeid " & _
                       "    FROM hrevaluation_analysis_master AS EM " & _
                       "    WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = @periodunkid " & _
                       ")AS ev ON ev.employeeid = hremployee_master.employeeunkid " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT DISTINCT " & _
                       "         OVS.periodunkid " & _
                       "        ,OVS.analysisunkid " & _
                       "        ,CAST(OVS.finaloverallscore AS DECIMAL(36,2)) AS finaloverallscore " & _
                       "        ,OVS.employeeunkid " & _
                       "   FROM " & _
                       "   ( " & _
                       "        SELECT " & _
                       "             CSM.periodunkid " & _
                       "            ,CSM.analysisunkid " & _
                       "            ,CASE WHEN @Setting = 1 AND ISNULL(CSM.isprocessed,0) = 1 THEN CSM.calibrated_score " & _
                       "                  WHEN @Setting = 1 AND ISNULL(CSM.isprocessed,0) = 0 THEN 0 " & _
                       "                  WHEN @Setting = 0 THEN CSM.finaloverallscore " & _
                       "             END AS finaloverallscore " & _
                       "            ,ROW_NUMBER()OVER(PARTITION BY CSM.employeeunkid,CSM.periodunkid ORDER BY CSM.computationdate ASC) AS xNo " & _
                       "            ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                       "        FROM hrassess_compute_score_master AS CSM " & _
                       "            JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                       "        WHERE CSM.isvoid = 0 AND CSM.periodunkid = @periodunkid " & _
                       "    ) AS OVS WHERE OVS.xNo = 1 " & _
                       ") AS os ON os.employeeunkid = hremployee_master.employeeunkid " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT DISTINCT " & _
                       "         CSM.periodunkid " & _
                       "        ,CSM.analysisunkid " & _
                       "        ,2 AS FilterTypeId " & _
                       "        ,CAST((SUM(CST.formula_value)/2) AS DECIMAL(36,2)) AS formula_value " & _
                       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                       "    FROM hrassess_compute_score_master AS CSM " & _
                       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 1 AND CM.formula_typeid IN (1,4) " & _
                       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                       ") AS ems ON ems.employeeunkid = hremployee_master.employeeunkid " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT DISTINCT " & _
                       "         CSM.periodunkid " & _
                       "        ,CSM.analysisunkid " & _
                       "        ,3 AS FilterTypeId " & _
                       "        ,CAST((SUM(CST.formula_value)/2) AS DECIMAL(36,2)) AS formula_value " & _
                       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                       "    FROM hrassess_compute_score_master AS CSM " & _
                       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 2 AND CM.formula_typeid IN (2,5) " & _
                       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                       ") AS ars ON ars.employeeunkid = hremployee_master.employeeunkid " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT DISTINCT " & _
                       "         CSM.periodunkid " & _
                       "        ,CSM.analysisunkid " & _
                       "        ,4 AS FilterTypeId " & _
                       "        ,CAST((SUM(CST.formula_value)/2) AS DECIMAL(36,2)) AS formula_value " & _
                       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                       "    FROM hrassess_compute_score_master AS CSM " & _
                       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 3 AND CM.formula_typeid IN (3,6) " & _
                       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                       ") AS rvs ON rvs.employeeunkid = hremployee_master.employeeunkid " & _
                       "WHERE 1 = 1 "
                'S.SANDEEP |21-AUG-2019| -- END

                'S.SANDEEP |27-MAY-2019| -- END

                Select Case mintAppointmentTypeId
                    Case enAD_Report_Parameter.APP_DATE_FROM
                        If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                        End If
                    Case enAD_Report_Parameter.APP_DATE_BEFORE
                        If mdtDate1 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                        End If
                    Case enAD_Report_Parameter.APP_DATE_AFTER
                        If mdtDate1 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                        End If
                End Select

                objDo.AddParameter("@FilterTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, xFilterBy)
                objDo.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)

                dsEmpllst = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            End Using

            'S.SANDEEP |24-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
            If dsEmpllst IsNot Nothing AndAlso dsEmpllst.Tables(0).Rows.Count > 0 Then
                dsEmpllst.Tables(0).AsEnumerable().ToList().ForEach(Function(x) PerformingRounding(x, mDecRoundingFactor, "SCORE"))
            End If
            'S.SANDEEP |24-DEC-2019| -- END

            Dim StrFilterString As String = ""
            'CDec(xtmp.Item("filter_value"))
            Select Case xtmp.Item("contition")
                Case ">"
                    StrFilterString = "SCORE > " & CDec(xtmp.Item("filter_value"))
                Case "<"
                    StrFilterString = "SCORE < " & CDec(xtmp.Item("filter_value"))
                Case ">="
                    StrFilterString = "SCORE >= " & CDec(xtmp.Item("filter_value"))
                Case "<="
                    StrFilterString = "SCORE <= " & CDec(xtmp.Item("filter_value"))
                Case "="
                    StrFilterString = "SCORE = " & CDec(xtmp.Item("filter_value"))
            End Select
            If StrFilterString.Trim.Length > 0 Then
                StrFilterString &= " AND SCORE > 0"
            End If

            Dim xtab As DataTable = Nothing
            xtab = New DataView(dsEmpllst.Tables(0), StrFilterString, "", DataViewRowState.CurrentRows).ToTable
            dsFinalEmp.Tables.Add(xtab)

            'S.SANDEEP [04-AUG-2017] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNewOverallFilter", mstrModuleName)
        Finally
        End Try
        Return dsFinalEmp
    End Function

    Private Function GetNewSummaryFilter(ByVal xtmp As DataRow, ByVal xPeriodId As Integer, ByVal xFilterBy As Integer, ByVal xScoringId As Integer, ByVal xEmployeeAsOnDate As DateTime, ByVal blnIsSelfAssignCompetencies As Boolean, ByVal blnUsedAgreedScore As Boolean) As DataSet
        'Shani (23-Nov-2016) -- [blnUsedAgreedScore]
        'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
        Dim StrQ As String = String.Empty
        Dim dsEmpllst As New DataSet
        Dim dsAsrRevlst As New DataSet
        Dim dsFinalEmp As New DataSet
        Try
            'S.SANDEEP |24-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
            Dim mDecRoundingFactor As Decimal = 0
            mDecRoundingFactor = GetRoundingFactor(xPeriodId)
            'S.SANDEEP |24-DEC-2019| -- END

            Using objDo As New clsDataOperation

                'S.SANDEEP [04-AUG-2017] -- START
                'ISSUE/ENHANCEMENT : CHANGE THE SCORE PICKING METHOD
                '    StrQ = "SELECT " & _
                '                           "     employeeunkid " & _
                '                           "    ,employeecode " & _
                '                           "    ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
                '                           "    ,CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
                '                           "    ,ISNULL(email,'') AS EMAIL " & _
                '                           "    ,ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
                '                           "    ,ISNULL(hrdepartment_master.name,'') AS DEPT " & _
                '                           "    ,0 AS SCORE " & _
                '                           "    ,'" & xtmp.Item("filter_refid") & "' AS F_RefId " & _
                '                           "    ,'" & xtmp.Item("GUID") & "' AS EGUID " & _
                '                           "    ,CAST(0 AS BIT) AS isvalid " & _
                '                           "FROM hremployee_master " & _
                '                           "    JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
                '                           "    JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
                '                           "WHERE 1 = 1 " & _
                '                           " AND employeeunkid " & _
                '                           "IN " & _
                '                           "( " & _
                '                           "   SELECT " & _
                '                           "       A.employeeid " & _
                '                           "   FROM " & _
                '                           "   ( " & _
                '                           "      SELECT selfemployeeunkid AS employeeid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' " & _
                '                           "      UNION SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' " & _
                '                           "      UNION SELECT assessedemployeeunkid FROM hrevaluation_analysis_master WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' " & _
                '                           "   ) AS A " & _
                '                           ")"

                '    Select Case mintAppointmentTypeId
                '        Case enAD_Report_Parameter.APP_DATE_FROM
                '            If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                '                StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                '            End If
                '        Case enAD_Report_Parameter.APP_DATE_BEFORE
                '            If mdtDate1 <> Nothing Then
                '                StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                '            End If
                '        Case enAD_Report_Parameter.APP_DATE_AFTER
                '            If mdtDate1 <> Nothing Then
                '                StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                '            End If
                '    End Select
                '    dsEmpllst = objDo.ExecQuery(StrQ, "List")

                '    If objDo.ErrorMessage <> "" Then
                '        Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                '    End If

                '    StrQ = "SELECT " & _
                '           "     selfemployeeunkid AS EmpId " & _
                '           "    ,assessormasterunkid " & _
                '           "    ,assessmodeid " & _
                '           "FROM hrevaluation_analysis_master " & _
                '           "WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' " & _
                '           "UNION " & _
                '           "SELECT " & _
                '           "     assessedemployeeunkid AS EmpId " & _
                '           "    ,assessormasterunkid " & _
                '           "    ,assessmodeid " & _
                '           "FROM hrevaluation_analysis_master " & _
                '           "WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' " & _
                '           "UNION " & _
                '           "SELECT " & _
                '           "     assessedemployeeunkid AS EmpId " & _
                '           "    ,assessormasterunkid " & _
                '           "    ,assessmodeid " & _
                '           "FROM hrevaluation_analysis_master " & _
                '           "WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' "

                '    dsAsrRevlst = objDo.ExecQuery(StrQ, "List")

                '    If objDo.ErrorMessage <> "" Then
                '        Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                '    End If

                'End Using

                'If dsEmpllst.Tables(0).Rows.Count > 0 Then
                '    Dim xTotalScore As Decimal = 0
                '    Dim xEvaluation As New clsevaluation_analysis_master
                '    For Each xRow As DataRow In dsEmpllst.Tables(0).Rows
                '        Select Case xFilterBy
                '            Case enApprFilterTypeId.APRL_OVER_ALL
                '                If xtmp.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.TOTAL_BSC_SCORE Then

                '                    'Shani (23-Nov-2016) -- Start
                '                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                    'xTotalScore = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, xScoringId, enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , , True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                    xTotalScore = xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                '                                                            IsBalanceScoreCard:=True, _
                '                                                            xScoreOptId:=xScoringId, _
                '                                                            xCompute_Formula:=enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                '                                                            xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                            xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                            xPeriodId:=xPeriodId, _
                '                                                            xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                            xOnlyCommited:=True, _
                '                                                            xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                    'Shani (23-Nov-2016) -- End


                '                    If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
                '                        Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "'")
                '                        If xAsrRev.Length > 0 Then

                '                            'Shani (23-Nov-2016) -- Start
                '                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                            'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, True, xScoringId, enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                            xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                '                                                                     IsBalanceScoreCard:=True, _
                '                                                                     xScoreOptId:=xScoringId, _
                '                                                                     xCompute_Formula:=enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
                '                                                                     xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                                     xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                                     xPeriodId:=xPeriodId, _
                '                                                                     xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                     xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
                '                                                                     xOnlyCommited:=True, _
                '                                                                     xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                            'Shani (23-Nov-2016) -- End


                '                        End If
                '                    End If
                '                    If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
                '                        Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'")
                '                        If xAsrRev.Length > 0 Then

                '                            'Shani (23-Nov-2016) -- Start
                '                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                            'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, True, xScoringId, enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                            xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                '                                                                     IsBalanceScoreCard:=True, _
                '                                                                     xScoreOptId:=xScoringId, _
                '                                                                     xCompute_Formula:=enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, _
                '                                                                     xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                                     xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                                     xPeriodId:=xPeriodId, _
                '                                                                     xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                     xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
                '                                                                     xOnlyCommited:=True, _
                '                                                                     xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                            'Shani (23-Nov-2016) -- End


                '                        End If
                '                    End If
                '                End If
                '                If xtmp.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.TOTAL_GE_SCORE Then

                '                    'Shani (23-Nov-2016) -- Start
                '                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                    'xTotalScore = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, xScoringId, enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , , True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                    xTotalScore = xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                '                                                            IsBalanceScoreCard:=False, _
                '                                                            xScoreOptId:=xScoringId, _
                '                                                            xCompute_Formula:=enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                '                                                            xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                            xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                            xPeriodId:=xPeriodId, _
                '                                                            xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                            xOnlyCommited:=True, _
                '                                                            xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                    'Shani (23-Nov-2016) -- End


                '                    If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
                '                        Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "'")
                '                        If xAsrRev.Length > 0 Then

                '                            'Shani (23-Nov-2016) -- Start
                '                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                            'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, False, xScoringId, enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                            xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                '                                                                     IsBalanceScoreCard:=False, _
                '                                                                     xScoreOptId:=xScoringId, _
                '                                                                     xCompute_Formula:=enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
                '                                                                     xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                                     xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                                     xPeriodId:=xPeriodId, _
                '                                                                     xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                     xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
                '                                                                     xOnlyCommited:=True, _
                '                                                                     xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                            'Shani (23-Nov-2016) -- End


                '                        End If
                '                    End If
                '                    If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
                '                        Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'")
                '                        If xAsrRev.Length > 0 Then
                '                            'Shani (23-Nov-2016) -- Start
                '                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                            'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, False, xScoringId, enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                            xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                '                                                                     IsBalanceScoreCard:=False, _
                '                                                                     xScoreOptId:=xScoringId, _
                '                                                                     xCompute_Formula:=enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, _
                '                                                                     xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                                     xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                                     xPeriodId:=xPeriodId, _
                '                                                                     xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                     xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
                '                                                                     xOnlyCommited:=True, _
                '                                                                     xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                            'Shani (23-Nov-2016) -- End
                '                        End If
                '                    End If
                '                End If

                '                Call Set_Valid_Filter(xRow, xTotalScore, xtmp)

                '            Case enApprFilterTypeId.APRL_EMPLOYEE
                '                If xtmp.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.TOTAL_BSC_SCORE Then

                '                    'Shani (23-Nov-2016) -- Start
                '                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                    'xTotalScore = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, True, xScoringId, enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , , True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                    xTotalScore = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
                '                                                            IsBalanceScoreCard:=True, _
                '                                                            xScoreOptId:=xScoringId, _
                '                                                            xCompute_Formula:=enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                '                                                            xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                            xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                            xPeriodId:=xPeriodId, _
                '                                                            xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                            xOnlyCommited:=True, _
                '                                                            xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                    'Shani (23-Nov-2016) -- End


                '                End If
                '                If xtmp.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.TOTAL_GE_SCORE Then

                '                    'Shani (23-Nov-2016) -- Start
                '                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                    'xTotalScore = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, False, xScoringId, enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , , True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                    xTotalScore = xEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
                '                                                            IsBalanceScoreCard:=False, _
                '                                                            xScoreOptId:=xScoringId, _
                '                                                            xCompute_Formula:=enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                '                                                            xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                            xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                            xPeriodId:=xPeriodId, _
                '                                                            xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                            xOnlyCommited:=True, _
                '                                                            xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                    'Shani (23-Nov-2016) -- End


                '                End If

                '                Call Set_Valid_Filter(xRow, xTotalScore, xtmp)

                '            Case enApprFilterTypeId.APRL_ASSESSOR
                '                If xtmp.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.TOTAL_BSC_SCORE Then
                '                    If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
                '                        Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "'")
                '                        If xAsrRev.Length > 0 Then

                '                            'Shani (23-Nov-2016) -- Start
                '                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                            'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, True, xScoringId, enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                            xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                '                                                                     IsBalanceScoreCard:=True, _
                '                                                                     xScoreOptId:=xScoringId, _
                '                                                                     xCompute_Formula:=enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
                '                                                                     xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                                     xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                                     xPeriodId:=xPeriodId, _
                '                                                                     xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                     xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
                '                                                                     xOnlyCommited:=True, _
                '                                                                     xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                            'Shani (23-Nov-2016) -- End


                '                        End If
                '                    End If

                '                End If
                '                If xtmp.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.TOTAL_GE_SCORE Then
                '                    If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
                '                        Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "'")
                '                        If xAsrRev.Length > 0 Then

                '                            'Shani (23-Nov-2016) -- Start
                '                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                            'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, False, xScoringId, enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                            xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                '                                                                     IsBalanceScoreCard:=False, _
                '                                                                     xScoreOptId:=xScoringId, _
                '                                                                     xCompute_Formula:=enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
                '                                                                     xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                                     xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                                     xPeriodId:=xPeriodId, _
                '                                                                     xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                     xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
                '                                                                     xOnlyCommited:=True, _
                '                                                                     xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                            'Shani (23-Nov-2016) -- End


                '                        End If
                '                    End If
                '                End If

                '                Call Set_Valid_Filter(xRow, xTotalScore, xtmp)

                '            Case enApprFilterTypeId.APRL_REVIEWER
                '                If xtmp.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.TOTAL_BSC_SCORE Then
                '                    If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
                '                        Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'")
                '                        If xAsrRev.Length > 0 Then

                '                            'Shani (23-Nov-2016) -- Start
                '                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                            'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, True, xScoringId, enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                            xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                '                                                                     IsBalanceScoreCard:=True, _
                '                                                                     xScoreOptId:=xScoringId, _
                '                                                                     xCompute_Formula:=enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, _
                '                                                                     xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                                     xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                                     xPeriodId:=xPeriodId, _
                '                                                                     xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                     xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
                '                                                                     xOnlyCommited:=True, _
                '                                                                     xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                            'Shani (23-Nov-2016) -- End


                '                        End If
                '                    End If
                '                End If
                '                If xtmp.Item("filter_refid") = clsAppraisal_Filter.Filter_Refid.TOTAL_GE_SCORE Then
                '                    If dsAsrRevlst.Tables(0).Rows.Count > 0 Then
                '                        Dim xAsrRev() As DataRow = dsAsrRevlst.Tables(0).Select("EmpId = '" & xRow.Item("employeeunkid") & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "'")
                '                        If xAsrRev.Length > 0 Then

                '                            'Shani (23-Nov-2016) -- Start
                '                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '                            'xTotalScore += xEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, False, xScoringId, enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, xEmployeeAsOnDate, xRow.Item("employeeunkid"), xPeriodId, , , xAsrRev(0).Item("assessormasterunkid"), True, blnIsSelfAssignCompetencies) 'S.SANDEEP [08 Jan 2016] -- START{blnIsSelfAssignCompetencies} -- END
                '                            xTotalScore += xEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                '                                                                     IsBalanceScoreCard:=False, _
                '                                                                     xScoreOptId:=xScoringId, _
                '                                                                     xCompute_Formula:=enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, _
                '                                                                     xEmployeeAsOnDate:=xEmployeeAsOnDate, _
                '                                                                     xEmployeeId:=xRow.Item("employeeunkid"), _
                '                                                                     xPeriodId:=xPeriodId, _
                '                                                                     xUsedAgreedScore:=blnUsedAgreedScore, _
                '                                                                     xAssessorReviewerId:=xAsrRev(0).Item("assessormasterunkid"), _
                '                                                                     xOnlyCommited:=True, _
                '                                                                     xSelfAssignedCompetencies:=blnIsSelfAssignCompetencies)
                '                            'Shani (23-Nov-2016) -- End


                '                        End If
                '                    End If
                '                End If

                '                Call Set_Valid_Filter(xRow, xTotalScore, xtmp)

                '        End Select
                '    Next
                '    xEvaluation = Nothing
                'End If

                'Dim xtab As DataTable = Nothing
                'xtab = New DataView(dsEmpllst.Tables(0), "isvalid = 1", "", DataViewRowState.CurrentRows).ToTable
                'dsFinalEmp.Tables.Add(xtab)

                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'StrQ = "SELECT DISTINCT " & _
                '       "     hremployee_master.employeeunkid " & _
                '       "    ,employeecode " & _
                '       "    ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
                '       "    ,CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
                '       "    ,ISNULL(email,'') AS EMAIL " & _
                '       "    ,ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
                '       "    ,ISNULL(hrdepartment_master.name,'') AS DEPT " & _
                '       "    ,CASE WHEN @FilterTypeId = 1 AND @Filter_Refid = 1 THEN CAST((ISNULL(SB.formula_value,0)+ISNULL(AB.formula_value,0)+ISNULL(RB.formula_value,0))/3 AS DECIMAL(10,2)) " & _
                '       "          WHEN @FilterTypeId = 1 AND @Filter_Refid = 2 THEN CAST((ISNULL(SC.formula_value,0)+ISNULL(AC.formula_value,0)+ISNULL(RC.formula_value,0))/3 AS DECIMAL(10,2)) " & _
                '       "          WHEN @FilterTypeId = 2 AND @Filter_Refid = 1 THEN ISNULL(SB.formula_value,0) " & _
                '       "          WHEN @FilterTypeId = 2 AND @Filter_Refid = 2 THEN ISNULL(SC.formula_value,0) " & _
                '       "          WHEN @FilterTypeId = 3 AND @Filter_Refid = 1 THEN ISNULL(AB.formula_value,0) " & _
                '       "          WHEN @FilterTypeId = 3 AND @Filter_Refid = 2 THEN ISNULL(AC.formula_value,0) " & _
                '       "          WHEN @FilterTypeId = 4 AND @Filter_Refid = 1 THEN ISNULL(RB.formula_value,0) " & _
                '       "          WHEN @FilterTypeId = 4 AND @Filter_Refid = 2 THEN ISNULL(RC.formula_value,0) " & _
                '       "     END AS SCORE " & _
                '       "    ,'" & xtmp.Item("filter_refid") & "' AS F_RefId " & _
                '       "    ,'" & xtmp.Item("GUID") & "' AS EGUID " & _
                '       "    ,CAST(1 AS BIT) AS isvalid " & _
                '       "FROM hremployee_master " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT " & _
                '       "         employeeunkid " & _
                '       "        ,jobunkid " & _
                '       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                '       "    FROM hremployee_categorization_tran " & _
                '       "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                '       ") AS cc ON cc.employeeunkid = hremployee_master.employeeunkid AND cc.xno = 1 " & _
                '       "JOIN hrjob_master ON cc.jobunkid = hrjob_master.jobunkid " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT " & _
                '       "         employeeunkid " & _
                '       "        ,departmentunkid " & _
                '       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                '       "    FROM hremployee_transfer_tran " & _
                '       "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                '       ") AS al ON al.employeeunkid = hremployee_master.employeeunkid AND al.xno = 1 " & _
                '       "JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = al.departmentunkid " & _
                '       "JOIN " & _
                '       "( " & _
                '       "    SELECT DISTINCT " & _
                '       "        CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeid " & _
                '       "    FROM hrevaluation_analysis_master AS EM " & _
                '       "    WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = @periodunkid " & _
                '       ")AS ev ON ev.employeeid = hremployee_master.employeeunkid " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT DISTINCT " & _
                '       "         CSM.periodunkid " & _
                '       "        ,CSM.analysisunkid " & _
                '       "        ,1 AS FilterTypeId " & _
                '       "        ,CAST((SUM(CST.formula_value)) AS DECIMAL(10,2)) AS formula_value " & _
                '       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "    FROM hrassess_compute_score_master AS CSM " & _
                '       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                '       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                '       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 1 AND CM.formula_typeid IN (1) " & _
                '       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                '       ") AS SB ON SB.employeeunkid = hremployee_master.employeeunkid " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT DISTINCT " & _
                '       "         CSM.periodunkid " & _
                '       "        ,CSM.analysisunkid " & _
                '       "        ,2 AS FilterTypeId " & _
                '       "        ,CAST((SUM(CST.formula_value)) AS DECIMAL(10,2)) AS formula_value " & _
                '       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "    FROM hrassess_compute_score_master AS CSM " & _
                '       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                '       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                '       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 2 AND CM.formula_typeid IN (2) " & _
                '       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                '       ") AS AB ON AB.employeeunkid = hremployee_master.employeeunkid " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT DISTINCT " & _
                '       "         CSM.periodunkid " & _
                '       "        ,CSM.analysisunkid " & _
                '       "        ,1 AS FilterTypeId " & _
                '       "        ,CAST((SUM(CST.formula_value)) AS DECIMAL(10,2)) AS formula_value " & _
                '       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "    FROM hrassess_compute_score_master AS CSM " & _
                '       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                '       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                '       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 3 AND CM.formula_typeid IN (3) " & _
                '       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                '       ") AS RB ON RB.employeeunkid = hremployee_master.employeeunkid " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT DISTINCT " & _
                '       "         CSM.periodunkid " & _
                '       "        ,CSM.analysisunkid " & _
                '       "        ,2 AS FilterTypeId " & _
                '       "        ,CAST((SUM(CST.formula_value)) AS DECIMAL(10,2)) AS formula_value " & _
                '       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "    FROM hrassess_compute_score_master AS CSM " & _
                '       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                '       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                '       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 1 AND CM.formula_typeid IN (4) " & _
                '       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                '       ") AS SC ON SC.employeeunkid = hremployee_master.employeeunkid " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT DISTINCT " & _
                '       "         CSM.periodunkid " & _
                '       "        ,CSM.analysisunkid " & _
                '       "        ,1 AS FilterTypeId " & _
                '       "        ,CAST((SUM(CST.formula_value)) AS DECIMAL(10,2)) AS formula_value " & _
                '       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "    FROM hrassess_compute_score_master AS CSM " & _
                '       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                '       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                '       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 2 AND CM.formula_typeid IN (5) " & _
                '       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                '       ") AS AC ON AC.employeeunkid = hremployee_master.employeeunkid " & _
                '       "LEFT JOIN " & _
                '       "( " & _
                '       "    SELECT DISTINCT " & _
                '       "         CSM.periodunkid " & _
                '       "        ,CSM.analysisunkid " & _
                '       "        ,2 AS FilterTypeId " & _
                '       "        ,CAST((SUM(CST.formula_value)) AS DECIMAL(10,2)) AS formula_value " & _
                '       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                '       "    FROM hrassess_compute_score_master AS CSM " & _
                '       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                '       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                '       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                '       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 3 AND CM.formula_typeid IN (6) " & _
                '       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                '       ") AS RC ON RC.employeeunkid = hremployee_master.employeeunkid " & _
                '       "WHERE 1 = 1 "
                StrQ = "SELECT DISTINCT " & _
                       "     hremployee_master.employeeunkid " & _
                       "    ,employeecode " & _
                       "    ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
                       "    ,CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
                       "    ,ISNULL(email,'') AS EMAIL " & _
                       "    ,ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
                       "    ,ISNULL(hrdepartment_master.name,'') AS DEPT " & _
                       "    ,CASE WHEN @FilterTypeId = 1 AND @Filter_Refid = 1 THEN CAST((ISNULL(SB.formula_value,0)+ISNULL(AB.formula_value,0)+ISNULL(RB.formula_value,0))/3 AS DECIMAL(36,2)) " & _
                       "          WHEN @FilterTypeId = 1 AND @Filter_Refid = 2 THEN CAST((ISNULL(SC.formula_value,0)+ISNULL(AC.formula_value,0)+ISNULL(RC.formula_value,0))/3 AS DECIMAL(36,2)) " & _
                       "          WHEN @FilterTypeId = 2 AND @Filter_Refid = 1 THEN CAST(ISNULL(SB.formula_value,0) AS DECIMAL(36,2)) " & _
                       "          WHEN @FilterTypeId = 2 AND @Filter_Refid = 2 THEN CAST(ISNULL(SC.formula_value,0) AS DECIMAL(36,2)) " & _
                       "          WHEN @FilterTypeId = 3 AND @Filter_Refid = 1 THEN CAST(ISNULL(AB.formula_value,0) AS DECIMAL(36,2)) " & _
                       "          WHEN @FilterTypeId = 3 AND @Filter_Refid = 2 THEN CAST(ISNULL(AC.formula_value,0) AS DECIMAL(36,2)) " & _
                       "          WHEN @FilterTypeId = 4 AND @Filter_Refid = 1 THEN CAST(ISNULL(RB.formula_value,0) AS DECIMAL(36,2)) " & _
                       "          WHEN @FilterTypeId = 4 AND @Filter_Refid = 2 THEN CAST(ISNULL(RC.formula_value,0) AS DECIMAL(36,2)) " & _
                       "     END AS SCORE " & _
                       "    ,'" & xtmp.Item("filter_refid") & "' AS F_RefId " & _
                       "    ,'" & xtmp.Item("GUID") & "' AS EGUID " & _
                       "    ,CAST(1 AS BIT) AS isvalid " & _
                       "FROM hremployee_master " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "         employeeunkid " & _
                       "        ,jobunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                       "    FROM hremployee_categorization_tran " & _
                       "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                       ") AS cc ON cc.employeeunkid = hremployee_master.employeeunkid AND cc.xno = 1 " & _
                       "JOIN hrjob_master ON cc.jobunkid = hrjob_master.jobunkid " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT " & _
                       "         employeeunkid " & _
                       "        ,departmentunkid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xno " & _
                       "    FROM hremployee_transfer_tran " & _
                       "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                       ") AS al ON al.employeeunkid = hremployee_master.employeeunkid AND al.xno = 1 " & _
                       "JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = al.departmentunkid " & _
                       "JOIN " & _
                       "( " & _
                       "    SELECT DISTINCT " & _
                       "        CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeid " & _
                       "    FROM hrevaluation_analysis_master AS EM " & _
                       "    WHERE isvoid = 0 AND iscommitted = 1 AND periodunkid = @periodunkid " & _
                       ")AS ev ON ev.employeeid = hremployee_master.employeeunkid " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT DISTINCT " & _
                       "         CSM.periodunkid " & _
                       "        ,CSM.analysisunkid " & _
                       "        ,1 AS FilterTypeId " & _
                       "        ,CAST((SUM(CST.formula_value)) AS DECIMAL(36,2)) AS formula_value " & _
                       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                       "    FROM hrassess_compute_score_master AS CSM " & _
                       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 1 AND CM.formula_typeid IN (1) " & _
                       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                       ") AS SB ON SB.employeeunkid = hremployee_master.employeeunkid " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT DISTINCT " & _
                       "         CSM.periodunkid " & _
                       "        ,CSM.analysisunkid " & _
                       "        ,2 AS FilterTypeId " & _
                       "        ,CAST((SUM(CST.formula_value)) AS DECIMAL(36,2)) AS formula_value " & _
                       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                       "    FROM hrassess_compute_score_master AS CSM " & _
                       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 2 AND CM.formula_typeid IN (2) " & _
                       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                       ") AS AB ON AB.employeeunkid = hremployee_master.employeeunkid " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT DISTINCT " & _
                       "         CSM.periodunkid " & _
                       "        ,CSM.analysisunkid " & _
                       "        ,1 AS FilterTypeId " & _
                       "        ,CAST((SUM(CST.formula_value)) AS DECIMAL(36,2)) AS formula_value " & _
                       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                       "    FROM hrassess_compute_score_master AS CSM " & _
                       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 3 AND CM.formula_typeid IN (3) " & _
                       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                       ") AS RB ON RB.employeeunkid = hremployee_master.employeeunkid " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT DISTINCT " & _
                       "         CSM.periodunkid " & _
                       "        ,CSM.analysisunkid " & _
                       "        ,2 AS FilterTypeId " & _
                       "        ,CAST((SUM(CST.formula_value)) AS DECIMAL(36,2)) AS formula_value " & _
                       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                       "    FROM hrassess_compute_score_master AS CSM " & _
                       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 1 AND CM.formula_typeid IN (4) " & _
                       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                       ") AS SC ON SC.employeeunkid = hremployee_master.employeeunkid " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT DISTINCT " & _
                       "         CSM.periodunkid " & _
                       "        ,CSM.analysisunkid " & _
                       "        ,1 AS FilterTypeId " & _
                       "        ,CAST((SUM(CST.formula_value)) AS DECIMAL(36,2)) AS formula_value " & _
                       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                       "    FROM hrassess_compute_score_master AS CSM " & _
                       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 2 AND CM.formula_typeid IN (5) " & _
                       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                       ") AS AC ON AC.employeeunkid = hremployee_master.employeeunkid " & _
                       "LEFT JOIN " & _
                       "( " & _
                       "    SELECT DISTINCT " & _
                       "         CSM.periodunkid " & _
                       "        ,CSM.analysisunkid " & _
                       "        ,2 AS FilterTypeId " & _
                       "        ,CAST((SUM(CST.formula_value)) AS DECIMAL(36,2)) AS formula_value " & _
                       "        ,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END AS employeeunkid " & _
                       "    FROM hrassess_compute_score_master AS CSM " & _
                       "        JOIN hrassess_compute_score_tran AS CST ON CSM.computescoremasterunkid = CST.computescoremasterunkid " & _
                       "        JOIN hrevaluation_analysis_master AS EM ON CSM.analysisunkid = EM.analysisunkid AND CSM.periodunkid = EM.periodunkid " & _
                       "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CST.computationunkid " & _
                       "    WHERE CSM.isvoid = 0 AND CST.isvoid = 0 AND CSM.periodunkid = @periodunkid AND EM.assessmodeid = 3 AND CM.formula_typeid IN (6) " & _
                       "    GROUP BY CSM.periodunkid,CSM.analysisunkid,CASE WHEN EM.selfemployeeunkid <= 0 THEN EM.assessedemployeeunkid ELSE EM.selfemployeeunkid END " & _
                       ") AS RC ON RC.employeeunkid = hremployee_master.employeeunkid " & _
                       "WHERE 1 = 1 "
                'S.SANDEEP |21-AUG-2019| -- END

                Select Case mintAppointmentTypeId
                    Case enAD_Report_Parameter.APP_DATE_FROM
                        If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                                    End If
                    Case enAD_Report_Parameter.APP_DATE_BEFORE
                        If mdtDate1 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                                End If
                    Case enAD_Report_Parameter.APP_DATE_AFTER
                        If mdtDate1 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                            End If
                End Select

                objDo.AddParameter("@FilterTypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, xFilterBy)
                objDo.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
                objDo.AddParameter("@Filter_Refid", SqlDbType.Int, eZeeDataType.INT_SIZE, Convert.ToInt32(xtmp.Item("filter_refid")))

                dsEmpllst = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                            End If

            End Using

            'S.SANDEEP |24-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
            If dsEmpllst IsNot Nothing AndAlso dsEmpllst.Tables(0).Rows.Count > 0 Then
                dsEmpllst.Tables(0).AsEnumerable().ToList().ForEach(Function(x) PerformingRounding(x, mDecRoundingFactor, "SCORE"))
            End If
            'S.SANDEEP |24-DEC-2019| -- END

            Dim StrFilterString As String = ""
            Select Case xtmp.Item("contition")
                Case ">"
                    StrFilterString = "SCORE > " & CDec(xtmp.Item("filter_value"))
                Case "<"
                    StrFilterString = "SCORE < " & CDec(xtmp.Item("filter_value"))
                Case ">="
                    StrFilterString = "SCORE >= " & CDec(xtmp.Item("filter_value"))
                Case "<="
                    StrFilterString = "SCORE <= " & CDec(xtmp.Item("filter_value"))
                Case "="
                    StrFilterString = "SCORE = " & CDec(xtmp.Item("filter_value"))
                    End Select
            If StrFilterString.Trim.Length > 0 Then
                StrFilterString &= " AND SCORE > 0"
            End If

            Dim xtab As DataTable = Nothing
            xtab = New DataView(dsEmpllst.Tables(0), StrFilterString, "", DataViewRowState.CurrentRows).ToTable
            dsFinalEmp.Tables.Add(xtab)
            'S.SANDEEP [04-AUG-2017] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNewSummaryFilter", mstrModuleName)
        Finally
        End Try
        Return dsFinalEmp
    End Function

    Private Sub Set_Valid_Filter(ByVal dRow As DataRow, ByVal xScore As Decimal, ByVal dfl As DataRow)
        Try
            If xScore > 0 Then
                dRow.Item("SCORE") = xScore
                Select Case dfl.Item("contition")
                    Case ">"
                        If xScore > CDec(dfl.Item("filter_value")) Then
                            dRow.Item("isvalid") = True
                        End If
                    Case "<"
                        If xScore < CDec(dfl.Item("filter_value")) Then
                            dRow.Item("isvalid") = True
                        End If
                    Case ">="
                        If xScore >= CDec(dfl.Item("filter_value")) Then
                            dRow.Item("isvalid") = True
                        End If
                    Case "<="
                        If xScore <= CDec(dfl.Item("filter_value")) Then
                            dRow.Item("isvalid") = True
                        End If
                    Case "="
                        If xScore = CDec(dfl.Item("filter_value")) Then
                            dRow.Item("isvalid") = True
                        End If
                End Select
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Valid_Filter", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [19 FEB 2015] -- END

    'S.SANDEEP [04 MAR 2015] -- START
    Private Function GetNewDetailFilter(ByVal xtmp As DataRow, ByVal xPeriodId As Integer, ByVal xFilterBy As Integer) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsFinalEmp As New DataSet
        Try
            'S.SANDEEP |24-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
            Dim mDecRoundingFactor As Decimal = 0
            mDecRoundingFactor = GetRoundingFactor(xPeriodId)
            'S.SANDEEP |24-DEC-2019| -- END

            Using objDo As New clsDataOperation
                StrQ = "SELECT " & _
                       "     employeeunkid " & _
                       "    ,employeecode " & _
                       "    ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
                       "    ,CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
                       "    ,ISNULL(email,'') AS EMAIL " & _
                       "    ,ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
                       "    ,ISNULL(hrdepartment_master.name,'') AS DEPT "
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'Select Case xFilterBy
                '    Case enApprFilterTypeId.APRL_OVER_ALL
                '        StrQ &= "    ,ISNULL(SC.SR,0)+ISNULL(AC.AR,0)+ISNULL(RC.RR,0) AS SCORE "
                '    Case enApprFilterTypeId.APRL_EMPLOYEE
                '        StrQ &= "    ,ISNULL(SC.SR,0) AS SCORE "
                '    Case enApprFilterTypeId.APRL_ASSESSOR
                '        StrQ &= "    ,ISNULL(AC.AR,0) AS SCORE "
                '    Case enApprFilterTypeId.APRL_REVIEWER
                '        StrQ &= "    ,ISNULL(RC.RR,0) AS SCORE "
                'End Select
                Select Case xFilterBy
                    Case enApprFilterTypeId.APRL_OVER_ALL
                        StrQ &= "    ,CAST(ISNULL(SC.SR,0)+ISNULL(AC.AR,0)+ISNULL(RC.RR,0) AS DECIMAL(36,2)) AS SCORE "
                    Case enApprFilterTypeId.APRL_EMPLOYEE
                        StrQ &= "    ,CAST(ISNULL(SC.SR,0) AS DECIMAL(36,2)) AS SCORE "
                    Case enApprFilterTypeId.APRL_ASSESSOR
                        StrQ &= "    ,CAST(ISNULL(AC.AR,0) AS DECIMAL(36,2)) AS SCORE "
                    Case enApprFilterTypeId.APRL_REVIEWER
                        StrQ &= "    ,CAST(ISNULL(RC.RR,0) AS DECIMAL(36,2)) AS SCORE "
                End Select
                'S.SANDEEP |21-AUG-2019| -- END

                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'StrQ &= "    ,'" & xtmp.Item("filter_refid") & "' AS F_RefId " & _
                '        "    ,'" & xtmp.Item("GUID") & "' AS EGUID " & _
                '        "FROM hremployee_master " & _
                '        "    JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
                '        "    JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
                '        "    LEFT JOIN " & _
                '        "    ( " & _
                '        "        SELECT " & _
                '        "             selfemployeeunkid AS EmpId " & _
                '        "            ,hrcompetency_analysis_tran.competenciesunkid AS ItemId " & _
                '        "            ,hrcompetency_analysis_tran.result AS SR " & _
                '        "        FROM hrevaluation_analysis_master " & _
                '        "            JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                '        "        WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 " & _
                '        "            AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
                '        "    ) AS SC ON SC.EmpId = employeeunkid " & _
                '        "    LEFT JOIN " & _
                '        "    ( " & _
                '        "        SELECT " & _
                '        "             assessedemployeeunkid AS EmpId " & _
                '        "            ,hrcompetency_analysis_tran.competenciesunkid AS ItemId " & _
                '        "            ,hrcompetency_analysis_tran.result AS AR " & _
                '        "        FROM hrevaluation_analysis_master " & _
                '        "            JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                '        "        WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 " & _
                '        "            AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
                '        "    ) AS AC ON AC.EmpId = employeeunkid " & _
                '        "    LEFT JOIN " & _
                '        "    ( " & _
                '        "        SELECT " & _
                '        "             assessedemployeeunkid AS EmpId " & _
                '        "            ,hrcompetency_analysis_tran.competenciesunkid AS ItemId " & _
                '        "            ,hrcompetency_analysis_tran.result AS RR " & _
                '        "        FROM hrevaluation_analysis_master " & _
                '        "            JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                '        "        WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 " & _
                '        "            AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
                '        "    ) AS RC ON RC.EmpId = employeeunkid " & _
                '        "WHERE 1 = 1 "

                StrQ &= "    ,'" & xtmp.Item("filter_refid") & "' AS F_RefId " & _
                        "    ,'" & xtmp.Item("GUID") & "' AS EGUID " & _
                        "FROM hremployee_master " & _
                        "    JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
                        "    JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "        SELECT " & _
                        "             selfemployeeunkid AS EmpId " & _
                        "            ,hrcompetency_analysis_tran.competenciesunkid AS ItemId " & _
                        "            ,CAST(hrcompetency_analysis_tran.result AS DECIMAL(36,2)) AS SR " & _
                        "        FROM hrevaluation_analysis_master " & _
                        "            JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                        "        WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 " & _
                        "            AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND iscommitted = 1 " & _
                        "    ) AS SC ON SC.EmpId = employeeunkid " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "        SELECT " & _
                        "             assessedemployeeunkid AS EmpId " & _
                        "            ,hrcompetency_analysis_tran.competenciesunkid AS ItemId " & _
                        "            ,CAST(hrcompetency_analysis_tran.result AS DECIMAL(36,2)) AS AR " & _
                        "        FROM hrevaluation_analysis_master " & _
                        "            JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                        "        WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 " & _
                        "            AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND iscommitted = 1 " & _
                        "    ) AS AC ON AC.EmpId = employeeunkid " & _
                        "    LEFT JOIN " & _
                        "    ( " & _
                        "        SELECT " & _
                        "             assessedemployeeunkid AS EmpId " & _
                        "            ,hrcompetency_analysis_tran.competenciesunkid AS ItemId " & _
                        "            ,CAST(hrcompetency_analysis_tran.result AS DECIMAL(36,2)) AS RR " & _
                        "        FROM hrevaluation_analysis_master " & _
                        "            JOIN hrcompetency_analysis_tran ON hrcompetency_analysis_tran.analysisunkid = hrevaluation_analysis_master.analysisunkid " & _
                        "        WHERE hrcompetency_analysis_tran.isvoid = 0 AND hrevaluation_analysis_master.isvoid = 0 " & _
                        "            AND periodunkid = '" & xPeriodId & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND iscommitted = 1 " & _
                        "    ) AS RC ON RC.EmpId = employeeunkid " & _
                        "WHERE 1 = 1 "
                'S.SANDEEP |21-AUG-2019| -- END
                Select Case xFilterBy
                    Case enApprFilterTypeId.APRL_OVER_ALL
                        StrQ &= "  AND SC.ItemId = '" & xtmp.Item("referenceunkid").ToString & "' AND (ISNULL(SC.SR,0)+ISNULL(AC.AR,0)+ISNULL(RC.RR,0))" & xtmp.Item("contition").ToString & " '" & xtmp.Item("filter_value").ToString & "'  "
                    Case enApprFilterTypeId.APRL_EMPLOYEE
                        StrQ &= "  AND SC.ItemId = '" & xtmp.Item("referenceunkid").ToString & "' AND (ISNULL(SC.SR,0))" & xtmp.Item("contition").ToString & " '" & xtmp.Item("filter_value").ToString & "'  "
                    Case enApprFilterTypeId.APRL_ASSESSOR
                        StrQ &= "  AND AC.ItemId = '" & xtmp.Item("referenceunkid").ToString & "' AND (ISNULL(AC.AR,0))" & xtmp.Item("contition").ToString & " '" & xtmp.Item("filter_value").ToString & "'  "
                    Case enApprFilterTypeId.APRL_REVIEWER
                        StrQ &= "  AND RC.ItemId = '" & xtmp.Item("referenceunkid").ToString & "' AND (ISNULL(RC.RR,0))" & xtmp.Item("contition").ToString & " '" & xtmp.Item("filter_value").ToString & "'  "
                End Select

                Select Case mintAppointmentTypeId
                    Case enAD_Report_Parameter.APP_DATE_FROM
                        If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
                        End If
                    Case enAD_Report_Parameter.APP_DATE_BEFORE
                        If mdtDate1 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
                        End If
                    Case enAD_Report_Parameter.APP_DATE_AFTER
                        If mdtDate1 <> Nothing Then
                            StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
                        End If
                End Select

                dsFinalEmp = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            End Using

            'S.SANDEEP |24-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
            If dsFinalEmp IsNot Nothing AndAlso dsFinalEmp.Tables(0).Rows.Count > 0 Then
                dsFinalEmp.Tables(0).AsEnumerable().ToList().ForEach(Function(x) PerformingRounding(x, mDecRoundingFactor, "SCORE"))
            End If
            'S.SANDEEP |24-DEC-2019| -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNewDetailFilter", mstrModuleName)
        Finally
        End Try
        Return dsFinalEmp
    End Function
    'S.SANDEEP [04 MAR 2015] -- END

    'S.SANDEEP |24-DEC-2019| -- START
    'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
    Private Function GetRoundingFactor(ByVal intPeriodId As Integer) As Decimal
        Dim StrQ As String = String.Empty
        Dim iDecRoundingFactor As Decimal = 0
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT TOP 1 " & _
                       "    @val = CAST(c.key_value AS DECIMAL(36,4)) " & _
                       "FROM cfcommon_period_tran cpt " & _
                       "    JOIN hrmsConfiguration..cffinancial_year_tran cyt ON cpt.yearunkid = cyt.yearunkid " & _
                       "    JOIN hrmsConfiguration..cfconfiguration c ON cyt.companyunkid = c.companyunkid " & _
                       "WHERE cpt.modulerefid = 5 AND cpt.periodunkid = '" & intPeriodId & "' AND UPPER(c.[key_name]) = 'PASCORINGROUDINGFACTOR' "

                objDo.ClearParameters()
                objDo.AddParameter("@val", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iDecRoundingFactor, ParameterDirection.InputOutput)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If
                Try
                    iDecRoundingFactor = objDo.GetParameterValue("@val")
                Catch ex As Exception
                    iDecRoundingFactor = 0
                End Try
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetRoundingFactor; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return iDecRoundingFactor
    End Function

    Private Function PerformingRounding(ByVal x As DataRow, ByVal dblFactor As Decimal, ByVal strColName As String) As Boolean
        Try
            If IsDBNull(x(strColName)) = False Then
                x(strColName) = Rounding.BRound(CDec(x(strColName)), dblFactor)
            Else
                x(strColName) = 0
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: PerformingRounding; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |24-DEC-2019| -- END

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Filter Criteria")
            Language.setMessage(mstrModuleName, 2, "Value")
            Language.setMessage(mstrModuleName, 3, "Condition")
            Language.setMessage(mstrModuleName, 4, "Operation")
            Language.setMessage(mstrModuleName, 6, "Code")
            Language.setMessage(mstrModuleName, 7, "Employee")
            Language.setMessage(mstrModuleName, 8, "Email")
            Language.setMessage(mstrModuleName, 9, "Appointed Date")
            Language.setMessage(mstrModuleName, 10, "Job Title")
            Language.setMessage(mstrModuleName, 11, "Department")
            Language.setMessage(mstrModuleName, 12, "Overall Score")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'Public Class clsAppraisal_Filter
'    Private Shared ReadOnly mstrModuleName As String = "clsAppraisal_Filter"
'    Private mintShortlistunkid As Integer
'    Private mdtTran As DataTable
'    Dim mstrMessage As String = ""
'    Private mintUserunkid As Integer
'    Private mblnIsvoid As Boolean
'    Private mintVoiduserunkid As Integer
'    Private mdtVoiddatetime As Date
'    Private mstrVoidreason As String = String.Empty
'    'S.SANDEEP [ 22 OCT 2013 ] -- START
'    Private mintAppointmentTypeId As Integer = 0
'    Private mdtDate1 As DateTime = Nothing
'    Private mdtDate2 As DateTime = Nothing
'    'S.SANDEEP [ 22 OCT 2013 ] -- END


'#Region " Properties "

'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    Public Property _Shortlistunkid() As Integer
'        Get
'            Return mintShortlistunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintShortlistunkid = value
'        End Set
'    End Property

'    Public Property _DataTable() As DataTable
'        Get
'            Return mdtTran
'        End Get
'        Set(ByVal value As DataTable)
'            mdtTran = value
'        End Set
'    End Property

'    Public Property _Userunkid() As Integer
'        Get
'            Return mintUserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintUserunkid = value
'        End Set
'    End Property

'    Public Property _Isvoid() As Boolean
'        Get
'            Return mblnIsvoid
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsvoid = value
'        End Set
'    End Property

'    Public Property _Voiduserunkid() As Integer
'        Get
'            Return mintVoiduserunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintVoiduserunkid = value
'        End Set
'    End Property

'    Public Property _Voiddatetime() As Date
'        Get
'            Return mdtVoiddatetime
'        End Get
'        Set(ByVal value As Date)
'            mdtVoiddatetime = value
'        End Set
'    End Property

'    Public Property _Voidreason() As String
'        Get
'            Return mstrVoidreason
'        End Get
'        Set(ByVal value As String)
'            mstrVoidreason = value
'        End Set
'    End Property

'    'S.SANDEEP [ 22 OCT 2013 ] -- START
'    Public WriteOnly Property _AppointmentTypeId() As Integer
'        Set(ByVal value As Integer)
'            mintAppointmentTypeId = value
'        End Set
'    End Property

'    Public WriteOnly Property _Date1() As DateTime
'        Set(ByVal value As DateTime)
'            mdtDate1 = value
'        End Set
'    End Property

'    Public WriteOnly Property _Date2() As DateTime
'        Set(ByVal value As DateTime)
'            mdtDate2 = value
'        End Set
'    End Property
'    'S.SANDEEP [ 22 OCT 2013 ] -- END

'#End Region

'#Region " ENUMS "

'    Public Enum Filter_Refid
'        TOTAL_BSC_SCORE = 1
'        TOTAL_GE_SCORE = 2
'        BSC_PERSPECTIVE = 3
'        GENEAL_EVALUATION = 4
'        OVERALL_SCORE = 5 'S.SANDEEP [ 14 AUG 2013 ] -- START -- END
'    End Enum

'    Public Enum Operater_ModeId
'        OP_AND = 1
'        OP_OR = 2
'    End Enum

'#End Region

'#Region " Constructor "

'    Public Sub New()
'        Try
'            mdtTran = New DataTable("DataTable")
'            Dim dCol As DataColumn

'            dCol = New DataColumn
'            dCol.Caption = ""
'            dCol.ColumnName = "filterunkid"
'            dCol.DefaultValue = -1
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn
'            dCol.Caption = ""
'            dCol.ColumnName = "shortlistunkid"
'            dCol.DefaultValue = -1
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn
'            dCol.Caption = ""
'            dCol.ColumnName = "filter_refid"
'            dCol.DefaultValue = -1
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn
'            dCol.Caption = ""
'            dCol.ColumnName = "conditionunkid"
'            dCol.DefaultValue = -1
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn
'            dCol.Caption = ""
'            dCol.ColumnName = "referenceunkid"
'            dCol.DefaultValue = -1
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn
'            dCol.Caption = ""
'            dCol.ColumnName = "issummary"
'            dCol.DefaultValue = False
'            dCol.DataType = System.Type.GetType("System.Boolean")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn
'            dCol.Caption = ""
'            dCol.ColumnName = "operator"
'            dCol.DefaultValue = -1
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn
'            dCol.Caption = ""
'            dCol.ColumnName = "isself_score"
'            dCol.DefaultValue = False
'            dCol.DataType = System.Type.GetType("System.Boolean")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn
'            dCol.Caption = ""
'            dCol.ColumnName = "isassessor_score"
'            dCol.DefaultValue = False
'            dCol.DataType = System.Type.GetType("System.Boolean")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn
'            dCol.Caption = ""
'            dCol.ColumnName = "isreviewer_score"
'            dCol.DefaultValue = False
'            dCol.DataType = System.Type.GetType("System.Boolean")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn
'            dCol.Caption = ""
'            dCol.ColumnName = "AUD"
'            dCol.DefaultValue = ""
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn
'            dCol.Caption = ""
'            dCol.ColumnName = "GUID"
'            dCol.DefaultValue = ""
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn
'            'dCol.Caption = Language.getMessage(mstrModuleName, 5, "Filter Group")
'            dCol.Caption = ""
'            dCol.ColumnName = "filter_grp"
'            dCol.DefaultValue = ""
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn
'            dCol.Caption = Language.getMessage(mstrModuleName, 1, "Filter Criteria")
'            dCol.ColumnName = "filter_criteria"
'            dCol.DefaultValue = ""
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn
'            dCol.Caption = Language.getMessage(mstrModuleName, 2, "Value")
'            dCol.ColumnName = "filter_value"
'            dCol.DefaultValue = -1
'            dCol.DataType = System.Type.GetType("System.Decimal")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn
'            dCol.Caption = Language.getMessage(mstrModuleName, 3, "Condition")
'            dCol.ColumnName = "contition"
'            dCol.DefaultValue = ""
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn
'            dCol.Caption = Language.getMessage(mstrModuleName, 4, "Operation")
'            dCol.ColumnName = "operation"
'            dCol.DefaultValue = ""
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn
'            dCol.Caption = ""
'            dCol.ColumnName = "col_tag"
'            dCol.DefaultValue = ""
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'        Catch ex As Exception
'            Throw New Exception(ex.Message & " Procedure : New, Class : clsApprisal_Filter " & mstrModuleName)
'        Finally
'        End Try
'    End Sub

'#End Region

'#Region " Private Methods & Functions "

'    Public Function Insert_Filter(ByVal objDataOperation As clsDataOperation, ByVal dEmployee As DataTable) As Boolean
'        Dim strQ As String = ""
'        Dim dsList As DataSet = Nothing
'        Dim exForce As Exception
'        Try
'            If mdtTran Is Nothing Then Return True

'            For i = 0 To mdtTran.Rows.Count - 1
'                With mdtTran.Rows(i)
'                    objDataOperation.ClearParameters()
'                    If Not IsDBNull(.Item("AUD")) Then
'                        Select Case .Item("AUD")
'                            Case "A"
'                                strQ = "INSERT INTO hrapps_filter ( " & _
'                                        "  shortlistunkid " & _
'                                        ", filter_refid " & _
'                                        ", filter_value " & _
'                                        ", conditionunkid " & _
'                                        ", referenceunkid " & _
'                                        ", issummary " & _
'                                        ", operator " & _
'                                        ", userunkid " & _
'                                        ", isvoid " & _
'                                        ", voiduserunkid " & _
'                                        ", voiddatetime " & _
'                                        ", voidreason " & _
'                                        ", isself_score " & _
'                                        ", isassessor_score " & _
'                                        ", isreviewer_score" & _
'                                      ") VALUES (" & _
'                                        "  @shortlistunkid " & _
'                                        ", @filter_refid " & _
'                                        ", @filter_value " & _
'                                        ", @conditionunkid " & _
'                                        ", @referenceunkid " & _
'                                        ", @issummary " & _
'                                        ", @operator " & _
'                                        ", @userunkid " & _
'                                        ", @isvoid " & _
'                                        ", @voiduserunkid " & _
'                                        ", @voiddatetime " & _
'                                        ", @voidreason " & _
'                                        ", @isself_score " & _
'                                        ", @isassessor_score " & _
'                                        ", @isreviewer_score" & _
'                                      "); SELECT @@identity"


'                                objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShortlistunkid.ToString)
'                                objDataOperation.AddParameter("@filter_refid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("filter_refid").ToString)
'                                objDataOperation.AddParameter("@filter_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("filter_value").ToString)
'                                objDataOperation.AddParameter("@conditionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("conditionunkid").ToString)
'                                objDataOperation.AddParameter("@referenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("referenceunkid").ToString)
'                                objDataOperation.AddParameter("@issummary", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("issummary").ToString)
'                                objDataOperation.AddParameter("@operator", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("operator").ToString)
'                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
'                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
'                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
'                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
'                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")

'                                objDataOperation.AddParameter("@isself_score", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isself_score").ToString)
'                                objDataOperation.AddParameter("@isassessor_score", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isassessor_score").ToString)
'                                objDataOperation.AddParameter("@isreviewer_score", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isreviewer_score").ToString)



'                                dsList = objDataOperation.ExecQuery(strQ, "List")

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                                Dim mintFilterUnkId As Integer = dsList.Tables(0).Rows(0).Item(0)

'                                If .Item("filterunkid") > 0 Then
'                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrapps_shortlist_master", "shortlistunkid", .Item("shortlistunkid"), "hrapps_filter", "filterunkid", mintFilterUnkId, 2, 1) = False Then
'                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                        Throw exForce
'                                    End If
'                                Else
'                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrapps_shortlist_master", "shortlistunkid", mintShortlistunkid, "hrapps_filter", "filterunkid", mintFilterUnkId, 1, 1) = False Then
'                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                        Throw exForce
'                                    End If
'                                End If

'                                Dim objFinalEmployee As New clsAppraisal_Final_Emp

'                                objFinalEmployee._ShortListUnkid = mintShortlistunkid
'                                objFinalEmployee._FilterUnkid = mintFilterUnkId
'                                objFinalEmployee._DataTable = New DataView(dEmployee, "EGUID  ='" & .Item("GUID").ToString() & "'", "", DataViewRowState.CurrentRows).ToTable
'                                objFinalEmployee._Userunkid = mintUserunkid

'                                If objFinalEmployee.Insert_ShortListedEmployee(objDataOperation) = False Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                        End Select
'                    End If
'                End With
'            Next
'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & " Procedure : Insert_Filter " & mstrModuleName)
'        Finally
'        End Try
'    End Function

'    Public Function GetFilteredEmployee(ByVal dtFilter As DataTable, ByVal intPeriodId As Integer) As DataSet
'        Dim mdTable As DataTable
'        Dim dsEmployee As New DataSet
'        Try

'            mdTable = New DataTable("List")

'            mdTable.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).Caption = ""
'            mdTable.Columns.Add("employeecode", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 6, "Code")
'            mdTable.Columns.Add("ENAME", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 7, "Employee")
'            mdTable.Columns.Add("Date", System.Type.GetType("System.DateTime")).Caption = Language.getMessage(mstrModuleName, 9, "Appointed Date")
'            mdTable.Columns.Add("EMAIL", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 8, "Email")
'            mdTable.Columns.Add("F_RefId", System.Type.GetType("System.Int32")).Caption = ""
'            mdTable.Columns.Add("EGUID", System.Type.GetType("System.String")).Caption = ""
'            'S.SANDEEP [ 14 AUG 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            mdTable.Columns.Add("JOB_TITLE", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 10, "Job Title")
'            mdTable.Columns.Add("DEPT", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 11, "Department")
'            mdTable.Columns.Add("SCORE", System.Type.GetType("System.String")).Caption = Language.getMessage(mstrModuleName, 12, "Overall Score")
'            mdTable.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = "A"
'            'S.SANDEEP [ 14 AUG 2013 ] -- END

'            If dtFilter.Rows.Count > 0 Then

'                Dim dsData As New DataSet
'                Dim dtTemp() As DataRow = Nothing

'                'S.SANDEEP [ 14 AUG 2013 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                dtTemp = dtFilter.Select("issummary = true AND AUD <>'D' AND filter_refid = '" & Filter_Refid.OVERALL_SCORE & "'")
'                If dtTemp.Length > 0 Then
'                    Dim intOperationMode As Integer = -1
'                    For i As Integer = 0 To dtTemp.Length - 1
'                        If intOperationMode <= 0 AndAlso dtTemp(i)("operator") > 0 Then
'                            intOperationMode = dtTemp(i)("operator")
'                        End If

'                        dsData = Get_Overall_Filter(dtTemp(i), intPeriodId)

'                        If intOperationMode > 0 Then
'                            If dsData.Tables(0).Rows.Count <= 0 AndAlso intOperationMode = Operater_ModeId.OP_AND Then Return dsData
'                        End If

'                        For Each drRow As DataRow In dsData.Tables(0).Rows
'                            Dim dtRow As DataRow = mdTable.NewRow

'                            dtRow.Item("employeeunkid") = drRow.Item("employeeunkid")
'                            dtRow.Item("employeecode") = drRow.Item("ECODE")
'                            dtRow.Item("ENAME") = drRow.Item("ENAME")
'                            dtRow.Item("Date") = eZeeDate.convertDate(drRow.Item("appointeddate").ToString)
'                            dtRow.Item("EMAIL") = drRow.Item("EMAIL")
'                            dtRow.Item("F_RefId") = drRow.Item("F_RefId")
'                            dtRow.Item("EGUID") = drRow.Item("EGUID")
'                            dtRow.Item("JOB_TITLE") = drRow.Item("JOB_TITLE")
'                            dtRow.Item("DEPT") = drRow.Item("DEPT")
'                            dtRow.Item("SCORE") = drRow.Item("Overall_Score")

'                            mdTable.Rows.Add(dtRow)
'                        Next

'                    Next
'                End If
'                'S.SANDEEP [ 14 AUG 2013 ] -- END


'                'S.SANDEEP [ 14 AUG 2013 ] -- START
'                'ENHANCEMENT : TRA CHANGES
'                dtTemp = dtFilter.Select("issummary = true AND AUD <>'D' AND filter_refid <> '" & Filter_Refid.OVERALL_SCORE & "'")
'                'S.SANDEEP [ 14 AUG 2013 ] -- END

'                If dtTemp.Length > 0 Then

'                    Dim intOperationMode As Integer = -1

'                    For i As Integer = 0 To dtTemp.Length - 1

'                        If intOperationMode <= 0 AndAlso dtTemp(i)("operator") > 0 Then
'                            intOperationMode = dtTemp(i)("operator")
'                        End If

'                        dsData = GetSummaryFilter(dtTemp(i), intPeriodId)

'                        If intOperationMode > 0 Then
'                            If dsData.Tables(0).Rows.Count <= 0 AndAlso intOperationMode = Operater_ModeId.OP_AND Then Exit For
'                        End If

'                        For Each drRow As DataRow In dsData.Tables(0).Rows
'                            Dim dtRow As DataRow = mdTable.NewRow

'                            dtRow.Item("employeeunkid") = drRow.Item("employeeunkid")
'                            dtRow.Item("employeecode") = drRow.Item("ECODE")
'                            dtRow.Item("ENAME") = drRow.Item("ENAME")
'                            'S.SANDEEP [ 14 AUG 2013 ] -- START
'                            'ENHANCEMENT : TRA CHANGES
'                            'dtRow.Item("Date") = eZeeDate.convertDate(drRow.Item("appointeddate").ToString).ToShortDateString
'                            dtRow.Item("Date") = eZeeDate.convertDate(drRow.Item("appointeddate").ToString)
'                            'S.SANDEEP [ 14 AUG 2013 ] -- END
'                            dtRow.Item("EMAIL") = drRow.Item("EMAIL")
'                            dtRow.Item("F_RefId") = drRow.Item("F_RefId")
'                            dtRow.Item("EGUID") = drRow.Item("EGUID")
'                            'S.SANDEEP [ 14 AUG 2013 ] -- START
'                            'ENHANCEMENT : TRA CHANGES
'                            dtRow.Item("JOB_TITLE") = drRow.Item("JOB_TITLE")
'                            dtRow.Item("DEPT") = drRow.Item("DEPT")
'                            dtRow.Item("SCORE") = drRow.Item("TotalScore")
'                            'S.SANDEEP [ 14 AUG 2013 ] -- END

'                            mdTable.Rows.Add(dtRow)
'                        Next
'                    Next
'                End If

'                dtTemp = dtFilter.Select("issummary = false AND AUD <> 'D' AND filter_refid = '" & Filter_Refid.BSC_PERSPECTIVE & "'")
'                Dim intGEFilterOperator As Integer = -1
'                If dtTemp.Length > 0 Then
'                    Dim intOperationMode As Integer = -1

'                    For i As Integer = 0 To dtTemp.Length - 1

'                        If intOperationMode <= 0 AndAlso dtTemp(i)("operator") > 0 Then
'                            intOperationMode = dtTemp(i)("operator")
'                        End If

'                        dsData = GetBSC_PerspectiveFilter(dtTemp(i), intPeriodId)

'                        If intOperationMode > 0 Then
'                            If dsData.Tables(0).Rows.Count <= 0 AndAlso intOperationMode = Operater_ModeId.OP_AND Then
'                                If dtTemp(dtTemp.Length - 1).Item("operator") > 0 Then
'                                    intGEFilterOperator = dtTemp(dtTemp.Length - 1).Item("operator")
'                                End If
'                                Exit For
'                            End If
'                        End If

'                        For Each drRow As DataRow In dsData.Tables(0).Rows
'                            Dim dtRow As DataRow = mdTable.NewRow

'                            dtRow.Item("employeeunkid") = drRow.Item("employeeunkid")
'                            dtRow.Item("employeecode") = drRow.Item("ECODE")
'                            dtRow.Item("ENAME") = drRow.Item("ENAME")
'                            'S.SANDEEP [ 14 AUG 2013 ] -- START
'                            'ENHANCEMENT : TRA CHANGES
'                            'dtRow.Item("Date") = eZeeDate.convertDate(drRow.Item("appointeddate").ToString).ToShortDateString
'                            dtRow.Item("Date") = eZeeDate.convertDate(drRow.Item("appointeddate").ToString)
'                            'S.SANDEEP [ 14 AUG 2013 ] -- END
'                            dtRow.Item("EMAIL") = drRow.Item("EMAIL")
'                            dtRow.Item("F_RefId") = drRow.Item("F_RefId")
'                            dtRow.Item("EGUID") = drRow.Item("EGUID")

'                            'S.SANDEEP [ 14 AUG 2013 ] -- START
'                            'ENHANCEMENT : TRA CHANGES
'                            dtRow.Item("JOB_TITLE") = drRow.Item("JOB_TITLE")
'                            dtRow.Item("DEPT") = drRow.Item("DEPT")
'                            dtRow.Item("SCORE") = drRow.Item("Total")
'                            'S.SANDEEP [ 14 AUG 2013 ] -- END

'                            mdTable.Rows.Add(dtRow)
'                        Next
'                    Next
'                End If

'                If intGEFilterOperator = -1 Or intGEFilterOperator = 2 Then
'                    dtTemp = dtFilter.Select("issummary = false AND AUD <>'D' AND filter_refid = '" & Filter_Refid.GENEAL_EVALUATION & "'")

'                    If dtTemp.Length > 0 Then
'                        Dim intOperationMode As Integer = -1
'                        For i As Integer = 0 To dtTemp.Length - 1
'                            If intOperationMode <= 0 AndAlso dtTemp(i)("operator") > 0 Then
'                                intOperationMode = dtTemp(i)("operator")
'                            End If

'                            dsData = Get_GeneralFilter(dtTemp(i), intPeriodId)

'                            If intOperationMode > 0 Then
'                                If dsData.Tables(0).Rows.Count <= 0 AndAlso intOperationMode = Operater_ModeId.OP_AND Then Exit For
'                            End If

'                            For Each drRow As DataRow In dsData.Tables(0).Rows
'                                Dim dtRow As DataRow = mdTable.NewRow

'                                dtRow.Item("employeeunkid") = drRow.Item("employeeunkid")
'                                dtRow.Item("employeecode") = drRow.Item("ECODE")
'                                dtRow.Item("ENAME") = drRow.Item("ENAME")
'                                'S.SANDEEP [ 14 AUG 2013 ] -- START
'                                'ENHANCEMENT : TRA CHANGES
'                                'dtRow.Item("Date") = eZeeDate.convertDate(drRow.Item("appointeddate").ToString).ToShortDateString
'                                dtRow.Item("Date") = eZeeDate.convertDate(drRow.Item("appointeddate").ToString)
'                                'S.SANDEEP [ 14 AUG 2013 ] -- END
'                                dtRow.Item("EMAIL") = drRow.Item("EMAIL")
'                                dtRow.Item("F_RefId") = drRow.Item("F_RefId")
'                                dtRow.Item("EGUID") = drRow.Item("EGUID")

'                                'S.SANDEEP [ 14 AUG 2013 ] -- START
'                                'ENHANCEMENT : TRA CHANGES
'                                dtRow.Item("JOB_TITLE") = drRow.Item("JOB_TITLE")
'                                dtRow.Item("DEPT") = drRow.Item("DEPT")
'                                dtRow.Item("SCORE") = drRow.Item("Total")
'                                'S.SANDEEP [ 14 AUG 2013 ] -- END

'                                mdTable.Rows.Add(dtRow)
'                            Next
'                        Next

'                    End If
'                End If


'            End If

'            dsEmployee.Tables.Add(mdTable)
'            Return dsEmployee

'        Catch ex As Exception
'            Throw New Exception(ex.Message & " Procedure : GetFilteredEmployee; Modulename : " & mstrModuleName)
'        Finally
'        End Try
'    End Function

'    'S.SANDEEP [ 05 MARCH 2012 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    'Private Function Get_GeneralFilter(ByVal dRow As DataRow) As DataSet
'    '    Dim StrQ As String = String.Empty
'    '    Dim exForce As Exception
'    '    Dim objDataOpetation As New clsDataOperation
'    '    Dim dsList As New DataSet
'    '    Try
'    '        StrQ = "SELECT " & _
'    '              " employeeunkid " & _
'    '              ",employeecode AS ECODE " & _
'    '              ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
'    '              ",CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
'    '              ",ISNULL(email,'') AS EMAIL " & _
'    '              ",'" & dRow.Item("filter_refid") & "' AS F_RefId " & _
'    '              ",'" & dRow.Item("GUID") & "' AS EGUID "
'    '        If CBool(dRow.Item("isself_score")) = True AndAlso _
'    '           CBool(dRow.Item("isassessor_score")) = True AndAlso _
'    '           CBool(dRow.Item("isreviewer_score")) = True Then
'    '            StrQ &= ",ISNULL(SELF,0) AS SELF " & _
'    '                     ",ISNULL(Assessor,0) AS Assessor " & _
'    '                     ",ISNULL(Result,0) AS Reviewer " & _
'    '                     ",CAST(((ISNULL(SELF,0)+ISNULL(Assessor,0)+ISNULL(Result,0))/3) AS DECIMAL(10,2)) AS Total " & _
'    '                "FROM hremployee_master " & _
'    '                "JOIN " & _
'    '                "( " & _
'    '                     "SELECT " & _
'    '                           "selfemployeeunkid " & _
'    '                          ",Self.assessitemunkid " & _
'    '                          ",Self.SELF " & _
'    '                     "FROM hrassess_analysis_master " & _
'    '                     "JOIN " & _
'    '                     "( " & _
'    '                          "SELECT " & _
'    '                                "analysisunkid " & _
'    '                               ",assessitemunkid " & _
'    '                                  ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'    '                          "FROM hrassess_analysis_tran " & _
'    '                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                          "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'    '                          "GROUP BY analysisunkid,assessitemunkid " & _
'    '                     ") AS Self ON dbo.hrassess_analysis_master.analysisunkid = Self.analysisunkid AND iscommitted = 1 " & _
'    '                     "WHERE assessmodeid = 1 AND isvoid = 0 " & _
'    '                ") AS A ON A.selfemployeeunkid = hremployee_master.employeeunkid " & _
'    '                "JOIN " & _
'    '                "( " & _
'    '                     "SELECT " & _
'    '                           "assessedemployeeunkid " & _
'    '                          ",assessitemunkid AS Aid " & _
'    '                          ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(10,2)) AS Assessor " & _
'    '                     "FROM hrassess_analysis_master " & _
'    '                     "JOIN " & _
'    '                     "( " & _
'    '                          "SELECT " & _
'    '                                "analysisunkid " & _
'    '                               ",assessitemunkid " & _
'    '                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'    '                          "FROM hrassess_analysis_tran " & _
'    '                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                          "GROUP BY  analysisunkid,assessitemunkid " & _
'    '                     ") AS A ON hrassess_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'    '                     "WHERE assessmodeid = 2 " & _
'    '                     "GROUP BY assessitemunkid,assessedemployeeunkid " & _
'    '                ") AS B ON B.assessedemployeeunkid = hremployee_master.employeeunkid AND Aid = A.assessitemunkid " & _
'    '                "JOIN " & _
'    '                "( " & _
'    '                          "SELECT " & _
'    '                           "assessedemployeeunkid " & _
'    '                          ",assessitemunkid AS Rid " & _
'    '                          ",Result " & _
'    '                     "FROM hrassess_analysis_master " & _
'    '                     "JOIN " & _
'    '                     "( " & _
'    '                          "SELECT " & _
'    '                                "analysisunkid " & _
'    '                               ",assessitemunkid " & _
'    '                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'    '                          "FROM hrassess_analysis_tran " & _
'    '                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                          "GROUP BY  analysisunkid,assessitemunkid " & _
'    '                     ") AS A ON hrassess_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'    '                     "WHERE assessmodeid = 3 " & _
'    '                ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid AND Rid = A.assessitemunkid "

'    '            StrQ &= " WHERE 1 = 1 " & _
'    '                    " AND (assessitemunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST(((ISNULL(SELF,0)+ISNULL(Assessor,0)+ISNULL(Result,0))/3) AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'    '        ElseIf CBool(dRow.Item("isself_score")) = True AndAlso _
'    '               CBool(dRow.Item("isassessor_score")) = True AndAlso _
'    '               CBool(dRow.Item("isreviewer_score")) = False Then
'    '            StrQ &= ",ISNULL(SELF,0) AS SELF " & _
'    '                     ",ISNULL(Assessor,0) AS Assessor " & _
'    '                     ",CAST(((ISNULL(SELF,0)+ISNULL(Assessor,0))/2) AS DECIMAL(10,2)) AS Total " & _
'    '                "FROM hremployee_master " & _
'    '                "JOIN " & _
'    '                "( " & _
'    '                     "SELECT " & _
'    '                           "selfemployeeunkid " & _
'    '                          ",Self.assessitemunkid " & _
'    '                          ",Self.SELF " & _
'    '                     "FROM hrassess_analysis_master " & _
'    '                     "JOIN " & _
'    '                     "( " & _
'    '                          "SELECT " & _
'    '                                "analysisunkid " & _
'    '                               ",assessitemunkid " & _
'    '                                  ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'    '                          "FROM hrassess_analysis_tran " & _
'    '                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                          "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'    '                          "GROUP BY analysisunkid,assessitemunkid " & _
'    '                     ") AS Self ON dbo.hrassess_analysis_master.analysisunkid = Self.analysisunkid AND iscommitted = 1 " & _
'    '                     "WHERE assessmodeid = 1 AND isvoid = 0 " & _
'    '                ") AS A ON A.selfemployeeunkid = hremployee_master.employeeunkid " & _
'    '                "JOIN " & _
'    '                "( " & _
'    '                     "SELECT " & _
'    '                           "assessedemployeeunkid " & _
'    '                          ",assessitemunkid AS Aid " & _
'    '                          ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(10,2)) AS Assessor " & _
'    '                     "FROM hrassess_analysis_master " & _
'    '                     "JOIN " & _
'    '                     "( " & _
'    '                          "SELECT " & _
'    '                                "analysisunkid " & _
'    '                               ",assessitemunkid " & _
'    '                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'    '                          "FROM hrassess_analysis_tran " & _
'    '                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                          "GROUP BY  analysisunkid,assessitemunkid " & _
'    '                     ") AS A ON hrassess_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'    '                     "WHERE assessmodeid = 2 " & _
'    '                     "GROUP BY assessitemunkid,assessedemployeeunkid " & _
'    '                ") AS B ON B.assessedemployeeunkid = hremployee_master.employeeunkid AND Aid = A.assessitemunkid "

'    '            StrQ &= " WHERE 1 = 1 " & _
'    '                    " AND (assessitemunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST(((ISNULL(SELF,0)+ISNULL(Assessor,0))/2) AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'    '        ElseIf CBool(dRow.Item("isself_score")) = True AndAlso _
'    '               CBool(dRow.Item("isassessor_score")) = False AndAlso _
'    '               CBool(dRow.Item("isreviewer_score")) = True Then
'    '            StrQ &= ",ISNULL(SELF,0) AS SELF " & _
'    '                    ",ISNULL(Result,0) AS Reviewer " & _
'    '                    ",CAST(((ISNULL(SELF,0)+ISNULL(Result,0))/2) AS DECIMAL(10,2)) AS Total " & _
'    '                "FROM hremployee_master " & _
'    '                "JOIN " & _
'    '                "( " & _
'    '                     "SELECT " & _
'    '                           "selfemployeeunkid " & _
'    '                          ",Self.assessitemunkid " & _
'    '                          ",Self.SELF " & _
'    '                     "FROM hrassess_analysis_master " & _
'    '                     "JOIN " & _
'    '                     "( " & _
'    '                          "SELECT " & _
'    '                                "analysisunkid " & _
'    '                               ",assessitemunkid " & _
'    '                                ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'    '                          "FROM hrassess_analysis_tran " & _
'    '                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                          "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'    '                          "GROUP BY analysisunkid,assessitemunkid " & _
'    '                     ") AS Self ON dbo.hrassess_analysis_master.analysisunkid = Self.analysisunkid AND iscommitted = 1 " & _
'    '                     "WHERE assessmodeid = 1 AND isvoid = 0 " & _
'    '                ") AS A ON A.selfemployeeunkid = hremployee_master.employeeunkid " & _
'    '                "JOIN " & _
'    '                "( " & _
'    '                          "SELECT " & _
'    '                           "assessedemployeeunkid " & _
'    '                          ",assessitemunkid AS Rid " & _
'    '                          ",Result " & _
'    '                     "FROM hrassess_analysis_master " & _
'    '                     "JOIN " & _
'    '                     "( " & _
'    '                          "SELECT " & _
'    '                                "analysisunkid " & _
'    '                               ",assessitemunkid " & _
'    '                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'    '                          "FROM hrassess_analysis_tran " & _
'    '                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                          "GROUP BY  analysisunkid,assessitemunkid " & _
'    '                     ") AS A ON hrassess_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'    '                     "WHERE assessmodeid = 3 " & _
'    '                ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid AND Rid = A.assessitemunkid "

'    '            StrQ &= " WHERE 1 = 1 " & _
'    '                    " AND (assessitemunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST(((ISNULL(SELF,0)+ISNULL(Result,0))/2) AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'    '        ElseIf CBool(dRow.Item("isself_score")) = False AndAlso _
'    '               CBool(dRow.Item("isassessor_score")) = True AndAlso _
'    '               CBool(dRow.Item("isreviewer_score")) = True Then
'    '            StrQ &= ",ISNULL(Assessor,0) AS Assessor " & _
'    '                     ",ISNULL(Result,0) AS Reviewer " & _
'    '                     ",CAST(((ISNULL(Assessor,0)+ISNULL(Result,0))/2) AS DECIMAL(10,2)) AS Total " & _
'    '                "FROM hremployee_master " & _
'    '                "JOIN " & _
'    '                "( " & _
'    '                     "SELECT " & _
'    '                           "assessedemployeeunkid " & _
'    '                          ",assessitemunkid AS Aid " & _
'    '                          ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(10,2)) AS Assessor " & _
'    '                     "FROM hrassess_analysis_master " & _
'    '                     "JOIN " & _
'    '                     "( " & _
'    '                          "SELECT " & _
'    '                                "analysisunkid " & _
'    '                               ",assessitemunkid " & _
'    '                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'    '                          "FROM hrassess_analysis_tran " & _
'    '                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                          "GROUP BY  analysisunkid,assessitemunkid " & _
'    '                     ") AS A ON hrassess_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'    '                     "WHERE assessmodeid = 2 " & _
'    '                     "GROUP BY assessitemunkid,assessedemployeeunkid " & _
'    '                ") AS B ON B.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'    '                "JOIN " & _
'    '                "( " & _
'    '                          "SELECT " & _
'    '                           "assessedemployeeunkid " & _
'    '                          ",assessitemunkid AS Rid " & _
'    '                          ",Result " & _
'    '                     "FROM hrassess_analysis_master " & _
'    '                     "JOIN " & _
'    '                     "( " & _
'    '                          "SELECT " & _
'    '                                "analysisunkid " & _
'    '                               ",assessitemunkid " & _
'    '                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'    '                          "FROM hrassess_analysis_tran " & _
'    '                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                          "GROUP BY  analysisunkid,assessitemunkid " & _
'    '                     ") AS A ON hrassess_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'    '                     "WHERE assessmodeid = 3 " & _
'    '                ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid AND Rid = B.assessitemunkid "

'    '            StrQ &= " WHERE 1 = 1 " & _
'    '                    " AND (assessitemunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST((ISNULL(Assessor,0)+ISNULL(Result,0))/3) AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'    '        ElseIf CBool(dRow.Item("isself_score")) = True AndAlso _
'    '               CBool(dRow.Item("isassessor_score")) = False AndAlso _
'    '               CBool(dRow.Item("isreviewer_score")) = False Then
'    '            StrQ &= ",ISNULL(SELF,0) AS SELF " & _
'    '                    ",ISNULL(SELF,0) AS Total " & _
'    '                "FROM hremployee_master " & _
'    '                "JOIN " & _
'    '                "( " & _
'    '                     "SELECT " & _
'    '                           "selfemployeeunkid " & _
'    '                          ",Self.assessitemunkid " & _
'    '                          ",Self.SELF " & _
'    '                     "FROM hrassess_analysis_master " & _
'    '                     "JOIN " & _
'    '                     "( " & _
'    '                          "SELECT " & _
'    '                                "analysisunkid " & _
'    '                               ",assessitemunkid " & _
'    '                                  ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'    '                          "FROM hrassess_analysis_tran " & _
'    '                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                          "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'    '                          "GROUP BY analysisunkid,assessitemunkid " & _
'    '                     ") AS Self ON dbo.hrassess_analysis_master.analysisunkid = Self.analysisunkid AND iscommitted = 1 " & _
'    '                     "WHERE assessmodeid = 1 AND isvoid = 0 " & _
'    '                ") AS A ON A.selfemployeeunkid = hremployee_master.employeeunkid "

'    '            StrQ &= " WHERE 1 = 1 " & _
'    '                    " AND (assessitemunkid = '" & dRow.Item("referenceunkid").ToString & "' AND ISNULL(SELF,0) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'    '        ElseIf CBool(dRow.Item("isself_score")) = False AndAlso _
'    '               CBool(dRow.Item("isassessor_score")) = True AndAlso _
'    '               CBool(dRow.Item("isreviewer_score")) = False Then
'    '            StrQ &= ",ISNULL(Assessor,0) AS Assessor " & _
'    '                    ",ISNULL(Assessor,0) AS Total " & _
'    '                "FROM hremployee_master " & _
'    '                "JOIN " & _
'    '                "( " & _
'    '                     "SELECT " & _
'    '                           "assessedemployeeunkid " & _
'    '                          ",assessitemunkid AS Aid " & _
'    '                          ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(10,2)) AS Assessor " & _
'    '                     "FROM hrassess_analysis_master " & _
'    '                     "JOIN " & _
'    '                     "( " & _
'    '                          "SELECT " & _
'    '                                "analysisunkid " & _
'    '                               ",assessitemunkid " & _
'    '                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'    '                          "FROM hrassess_analysis_tran " & _
'    '                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                          "GROUP BY  analysisunkid,assessitemunkid " & _
'    '                     ") AS A ON hrassess_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'    '                     "WHERE assessmodeid = 2 " & _
'    '                     "GROUP BY assessitemunkid,assessedemployeeunkid " & _
'    '                ") AS B ON B.assessedemployeeunkid = hremployee_master.employeeunkid "

'    '            StrQ &= " WHERE 1 = 1 " & _
'    '                    " AND (Aid = '" & dRow.Item("referenceunkid").ToString & "' AND ISNULL(Assessor,0) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'    '        ElseIf CBool(dRow.Item("isself_score")) = False AndAlso _
'    '               CBool(dRow.Item("isassessor_score")) = False AndAlso _
'    '               CBool(dRow.Item("isreviewer_score")) = True Then

'    '            StrQ &= ",ISNULL(Result,0) AS Reviewer " & _
'    '                     ",ISNULL(Result,0) AS Total " & _
'    '                "FROM hremployee_master " & _
'    '                "JOIN " & _
'    '                "( " & _
'    '                          "SELECT " & _
'    '                           "assessedemployeeunkid " & _
'    '                          ",assessitemunkid AS Rid " & _
'    '                          ",Result " & _
'    '                     "FROM hrassess_analysis_master " & _
'    '                     "JOIN " & _
'    '                     "( " & _
'    '                          "SELECT " & _
'    '                                "analysisunkid " & _
'    '                               ",assessitemunkid " & _
'    '                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'    '                          "FROM hrassess_analysis_tran " & _
'    '                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                          "GROUP BY  analysisunkid,assessitemunkid " & _
'    '                     ") AS A ON hrassess_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'    '                     "WHERE assessmodeid = 3 " & _
'    '                ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid "

'    '            StrQ &= " WHERE 1 = 1 " & _
'    '                    " AND (Rid = '" & dRow.Item("referenceunkid").ToString & "' AND ISNULL(Result,0) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'    '        End If

'    '        dsList = objDataOpetation.ExecQuery(StrQ, "List")

'    '        If objDataOpetation.ErrorMessage <> "" Then
'    '            exForce = New Exception(objDataOpetation.ErrorNumber & " : " & objDataOpetation.ErrorMessage)
'    '            Throw exForce
'    '        End If

'    '        Return dsList

'    '    Catch ex As Exception
'    '        Throw New Exception(ex.Message & " Procedure : Get_GeneralFilter; Modulename : " & mstrModuleName)
'    '    Finally
'    '    End Try
'    'End Function
'    Private Function Get_GeneralFilter(ByVal dRow As DataRow, ByVal intPeriodUnkid As Integer) As DataSet
'    Dim StrQ As String = String.Empty
'    Dim exForce As Exception
'    Dim objDataOpetation As New clsDataOperation
'    Dim dsList As New DataSet
'    Try
'            'S.SANDEEP [ 14 AUG 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'StrQ = "SELECT " & _
'            '      " employeeunkid " & _
'            '      ",employeecode AS ECODE " & _
'            '      ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
'            '      ",CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
'            '      ",ISNULL(email,'') AS EMAIL " & _
'            '      ",'" & dRow.Item("filter_refid") & "' AS F_RefId " & _
'            '      ",'" & dRow.Item("GUID") & "' AS EGUID "
'        StrQ = "SELECT " & _
'              " employeeunkid " & _
'              ",employeecode AS ECODE " & _
'              ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
'              ",CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
'              ",ISNULL(email,'') AS EMAIL " & _
'              ",'" & dRow.Item("filter_refid") & "' AS F_RefId " & _
'                  ",'" & dRow.Item("GUID") & "' AS EGUID " & _
'                  ",ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
'                  ",ISNULL(hrdepartment_master.name,'') AS DEPT "
'            'S.SANDEEP [ 14 AUG 2013 ] -- END
'        If CBool(dRow.Item("isself_score")) = True AndAlso _
'           CBool(dRow.Item("isassessor_score")) = True AndAlso _
'           CBool(dRow.Item("isreviewer_score")) = True Then
'            StrQ &= ",ISNULL(SELF,0) AS SELF " & _
'                     ",ISNULL(Assessor,0) AS Assessor " & _
'                     ",ISNULL(Result,0) AS Reviewer " & _
'                     ",CAST(((ISNULL(SELF,0)+ISNULL(Assessor,0)+ISNULL(Result,0))/3) AS DECIMAL(10,2)) AS Total " & _
'                "FROM hremployee_master " & _
'                    "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                    "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                "JOIN " & _
'                "( " & _
'                     "SELECT " & _
'                           "selfemployeeunkid " & _
'                          ",Self.assessitemunkid " & _
'                          ",Self.SELF " & _
'                     "FROM hrassess_analysis_master " & _
'                     "JOIN " & _
'                     "( " & _
'                          "SELECT " & _
'                                "analysisunkid " & _
'                                   ",hrassess_analysis_tran.assessitemunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * CAST((weight/100) AS decimal(10,2)) ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'                          "FROM hrassess_analysis_tran " & _
'                                   "JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
'                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                          "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'                              "GROUP BY analysisunkid,hrassess_analysis_tran.assessitemunkid " & _
'                     ") AS Self ON dbo.hrassess_analysis_master.analysisunkid = Self.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND isvoid = 0 AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                ") AS A ON A.selfemployeeunkid = hremployee_master.employeeunkid " & _
'                "JOIN " & _
'                "( " & _
'                     "SELECT " & _
'                           "assessedemployeeunkid " & _
'                          ",assessitemunkid AS Aid " & _
'                          ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(10,2)) AS Assessor " & _
'                     "FROM hrassess_analysis_master " & _
'                     "JOIN " & _
'                     "( " & _
'                          "SELECT " & _
'                                "analysisunkid " & _
'                                   ",hrassess_analysis_tran.assessitemunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * CAST((weight/100) AS decimal(10,2)) ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'                          "FROM hrassess_analysis_tran " & _
'                              "JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
'                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "GROUP BY  analysisunkid,hrassess_analysis_tran.assessitemunkid " & _
'                     ") AS A ON hrassess_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' and isvoid = 0 AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                     "GROUP BY assessitemunkid,assessedemployeeunkid " & _
'                ") AS B ON B.assessedemployeeunkid = hremployee_master.employeeunkid AND Aid = A.assessitemunkid " & _
'                "JOIN " & _
'                "( " & _
'                          "SELECT " & _
'                           "assessedemployeeunkid " & _
'                          ",assessitemunkid AS Rid " & _
'                          ",Result " & _
'                     "FROM hrassess_analysis_master " & _
'                     "JOIN " & _
'                     "( " & _
'                          "SELECT " & _
'                                "analysisunkid " & _
'                                   ",hrassess_analysis_tran.assessitemunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * CAST((weight/100) AS decimal(10,2)) ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'                          "FROM hrassess_analysis_tran " & _
'                                   "JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
'                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "GROUP BY  analysisunkid,hrassess_analysis_tran.assessitemunkid " & _
'                     ") AS A ON hrassess_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' And isvoid = 0 AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid AND Rid = A.assessitemunkid "

'            StrQ &= " WHERE 1 = 1 " & _
'                    " AND (assessitemunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST(((ISNULL(SELF,0)+ISNULL(Assessor,0)+ISNULL(Result,0))/3) AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'        ElseIf CBool(dRow.Item("isself_score")) = True AndAlso _
'               CBool(dRow.Item("isassessor_score")) = True AndAlso _
'               CBool(dRow.Item("isreviewer_score")) = False Then
'            StrQ &= ",ISNULL(SELF,0) AS SELF " & _
'                     ",ISNULL(Assessor,0) AS Assessor " & _
'                     ",CAST(((ISNULL(SELF,0)+ISNULL(Assessor,0))/2) AS DECIMAL(10,2)) AS Total " & _
'                "FROM hremployee_master " & _
'                    "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                    "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                "JOIN " & _
'                "( " & _
'                     "SELECT " & _
'                           "selfemployeeunkid " & _
'                          ",Self.assessitemunkid " & _
'                          ",Self.SELF " & _
'                     "FROM hrassess_analysis_master " & _
'                     "JOIN " & _
'                     "( " & _
'                          "SELECT " & _
'                                "analysisunkid " & _
'                                   ",hrassess_analysis_tran.assessitemunkid " & _
'                                      ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * CAST((weight/100) AS decimal(10,2)) ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'                          "FROM hrassess_analysis_tran " & _
'                                   "JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
'                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                          "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'                              "GROUP BY analysisunkid,hrassess_analysis_tran.assessitemunkid " & _
'                     ") AS Self ON dbo.hrassess_analysis_master.analysisunkid = Self.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND isvoid = 0 AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                ") AS A ON A.selfemployeeunkid = hremployee_master.employeeunkid " & _
'                "JOIN " & _
'                "( " & _
'                     "SELECT " & _
'                           "assessedemployeeunkid " & _
'                          ",assessitemunkid AS Aid " & _
'                          ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(10,2)) AS Assessor " & _
'                     "FROM hrassess_analysis_master " & _
'                     "JOIN " & _
'                     "( " & _
'                          "SELECT " & _
'                                "analysisunkid " & _
'                                   ",hrassess_analysis_tran.assessitemunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * CAST((weight/100) AS decimal(10,2)) ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'                          "FROM hrassess_analysis_tran " & _
'                                   "JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
'                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "GROUP BY  analysisunkid,hrassess_analysis_tran.assessitemunkid " & _
'                     ") AS A ON hrassess_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND isvoid = 0 AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                     "GROUP BY assessitemunkid,assessedemployeeunkid " & _
'                ") AS B ON B.assessedemployeeunkid = hremployee_master.employeeunkid AND Aid = A.assessitemunkid "

'            StrQ &= " WHERE 1 = 1 " & _
'                    " AND (assessitemunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST(((ISNULL(SELF,0)+ISNULL(Assessor,0))/2) AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'        ElseIf CBool(dRow.Item("isself_score")) = True AndAlso _
'               CBool(dRow.Item("isassessor_score")) = False AndAlso _
'               CBool(dRow.Item("isreviewer_score")) = True Then
'            StrQ &= ",ISNULL(SELF,0) AS SELF " & _
'                    ",ISNULL(Result,0) AS Reviewer " & _
'                    ",CAST(((ISNULL(SELF,0)+ISNULL(Result,0))/2) AS DECIMAL(10,2)) AS Total " & _
'                "FROM hremployee_master " & _
'                    "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                    "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                "JOIN " & _
'                "( " & _
'                     "SELECT " & _
'                           "selfemployeeunkid " & _
'                          ",Self.assessitemunkid " & _
'                          ",Self.SELF " & _
'                     "FROM hrassess_analysis_master " & _
'                     "JOIN " & _
'                     "( " & _
'                          "SELECT " & _
'                                "analysisunkid " & _
'                                   ",hrassess_analysis_tran.assessitemunkid " & _
'                                    ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * CAST((weight/100) AS decimal(10,2)) ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'                          "FROM hrassess_analysis_tran " & _
'                                   "JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
'                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                          "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'                              "GROUP BY analysisunkid,hrassess_analysis_tran.assessitemunkid " & _
'                     ") AS Self ON dbo.hrassess_analysis_master.analysisunkid = Self.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND isvoid = 0 AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                ") AS A ON A.selfemployeeunkid = hremployee_master.employeeunkid " & _
'                "JOIN " & _
'                "( " & _
'                          "SELECT " & _
'                           "assessedemployeeunkid " & _
'                          ",assessitemunkid AS Rid " & _
'                          ",Result " & _
'                     "FROM hrassess_analysis_master " & _
'                     "JOIN " & _
'                     "( " & _
'                          "SELECT " & _
'                                "analysisunkid " & _
'                                   ",hrassess_analysis_tran.assessitemunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * CAST((weight/100) AS decimal(10,2)) ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'                          "FROM hrassess_analysis_tran " & _
'                                   "JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
'                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "GROUP BY  analysisunkid,hrassess_analysis_tran.assessitemunkid " & _
'                     ") AS A ON hrassess_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND isvoid = 0 AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid AND Rid = A.assessitemunkid "

'            StrQ &= " WHERE 1 = 1 " & _
'                    " AND (assessitemunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST(((ISNULL(SELF,0)+ISNULL(Result,0))/2) AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'        ElseIf CBool(dRow.Item("isself_score")) = False AndAlso _
'               CBool(dRow.Item("isassessor_score")) = True AndAlso _
'               CBool(dRow.Item("isreviewer_score")) = True Then
'            StrQ &= ",ISNULL(Assessor,0) AS Assessor " & _
'                     ",ISNULL(Result,0) AS Reviewer " & _
'                     ",CAST(((ISNULL(Assessor,0)+ISNULL(Result,0))/2) AS DECIMAL(10,2)) AS Total " & _
'                "FROM hremployee_master " & _
'                    "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                    "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                "JOIN " & _
'                "( " & _
'                     "SELECT " & _
'                           "assessedemployeeunkid " & _
'                          ",assessitemunkid AS Aid " & _
'                          ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(10,2)) AS Assessor " & _
'                     "FROM hrassess_analysis_master " & _
'                     "JOIN " & _
'                     "( " & _
'                          "SELECT " & _
'                                "analysisunkid " & _
'                                   ",hrassess_analysis_tranassessitemunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * CAST((weight/100) AS decimal(10,2)) ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'                          "FROM hrassess_analysis_tran " & _
'                                   "JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
'                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "GROUP BY  analysisunkid,hrassess_analysis_tranassessitemunkid " & _
'                     ") AS A ON hrassess_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND isvoid = 0 AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                     "GROUP BY assessitemunkid,assessedemployeeunkid " & _
'                ") AS B ON B.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'                "JOIN " & _
'                "( " & _
'                          "SELECT " & _
'                           "assessedemployeeunkid " & _
'                          ",assessitemunkid AS Rid " & _
'                          ",Result " & _
'                     "FROM hrassess_analysis_master " & _
'                     "JOIN " & _
'                     "( " & _
'                          "SELECT " & _
'                                "analysisunkid " & _
'                                   ",hrassess_analysis_tran.assessitemunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * CAST((weight/100) AS decimal(10,2)) ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'                          "FROM hrassess_analysis_tran " & _
'                                   "JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
'                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "GROUP BY  analysisunkid,hrassess_analysis_tran.assessitemunkid " & _
'                     ") AS A ON hrassess_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND isvoid = 0 AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid AND Rid = B.assessitemunkid "

'            StrQ &= " WHERE 1 = 1 " & _
'                    " AND (assessitemunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST((ISNULL(Assessor,0)+ISNULL(Result,0))/3) AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'        ElseIf CBool(dRow.Item("isself_score")) = True AndAlso _
'               CBool(dRow.Item("isassessor_score")) = False AndAlso _
'               CBool(dRow.Item("isreviewer_score")) = False Then
'            StrQ &= ",ISNULL(SELF,0) AS SELF " & _
'                    ",ISNULL(SELF,0) AS Total " & _
'                "FROM hremployee_master " & _
'                    "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                    "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                "JOIN " & _
'                "( " & _
'                     "SELECT " & _
'                           "selfemployeeunkid " & _
'                          ",Self.assessitemunkid " & _
'                          ",Self.SELF " & _
'                     "FROM hrassess_analysis_master " & _
'                     "JOIN " & _
'                     "( " & _
'                          "SELECT " & _
'                                "analysisunkid " & _
'                                   ",hrassess_analysis_tran.assessitemunkid " & _
'                                      ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * CAST((weight/100) AS decimal(10,2)) ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'                          "FROM hrassess_analysis_tran " & _
'                                   "JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
'                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                          "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'                              "GROUP BY analysisunkid,hrassess_analysis_tran.assessitemunkid " & _
'                     ") AS Self ON dbo.hrassess_analysis_master.analysisunkid = Self.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND isvoid = 0 AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                ") AS A ON A.selfemployeeunkid = hremployee_master.employeeunkid "

'            StrQ &= " WHERE 1 = 1 " & _
'                    " AND (assessitemunkid = '" & dRow.Item("referenceunkid").ToString & "' AND ISNULL(SELF,0) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'        ElseIf CBool(dRow.Item("isself_score")) = False AndAlso _
'               CBool(dRow.Item("isassessor_score")) = True AndAlso _
'               CBool(dRow.Item("isreviewer_score")) = False Then
'            StrQ &= ",ISNULL(Assessor,0) AS Assessor " & _
'                    ",ISNULL(Assessor,0) AS Total " & _
'                "FROM hremployee_master " & _
'                    "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                    "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                "JOIN " & _
'                "( " & _
'                     "SELECT " & _
'                           "assessedemployeeunkid " & _
'                          ",assessitemunkid AS Aid " & _
'                          ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(10,2)) AS Assessor " & _
'                     "FROM hrassess_analysis_master " & _
'                     "JOIN " & _
'                     "( " & _
'                          "SELECT " & _
'                                "analysisunkid " & _
'                                   ",hrassess_analysis_tran.assessitemunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * CAST((weight/100) AS decimal(10,2)) ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'                          "FROM hrassess_analysis_tran " & _
'                                   "JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
'                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "GROUP BY  analysisunkid,hrassess_analysis_tran.assessitemunkid " & _
'                     ") AS A ON hrassess_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND isvoid = 0 AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                     "GROUP BY assessitemunkid,assessedemployeeunkid " & _
'                ") AS B ON B.assessedemployeeunkid = hremployee_master.employeeunkid "

'            StrQ &= " WHERE 1 = 1 " & _
'                    " AND (Aid = '" & dRow.Item("referenceunkid").ToString & "' AND ISNULL(Assessor,0) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'        ElseIf CBool(dRow.Item("isself_score")) = False AndAlso _
'               CBool(dRow.Item("isassessor_score")) = False AndAlso _
'               CBool(dRow.Item("isreviewer_score")) = True Then

'            StrQ &= ",ISNULL(Result,0) AS Reviewer " & _
'                     ",ISNULL(Result,0) AS Total " & _
'                "FROM hremployee_master " & _
'                    "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                    "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                "JOIN " & _
'                "( " & _
'                          "SELECT " & _
'                           "assessedemployeeunkid " & _
'                          ",assessitemunkid AS Rid " & _
'                          ",Result " & _
'                     "FROM hrassess_analysis_master " & _
'                     "JOIN " & _
'                     "( " & _
'                          "SELECT " & _
'                                "analysisunkid " & _
'                                   ",hrassess_analysis_tran.assessitemunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname * CAST((weight/100) AS decimal(10,2)) ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'                          "FROM hrassess_analysis_tran " & _
'                                   "JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
'                               "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "GROUP BY  analysisunkid,hrassess_analysis_tran.assessitemunkid " & _
'                     ") AS A ON hrassess_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND isvoid = 0 AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid "

'            StrQ &= " WHERE 1 = 1 " & _
'                    " AND (Rid = '" & dRow.Item("referenceunkid").ToString & "' AND ISNULL(Result,0) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'        End If

'            'S.SANDEEP [ 14 AUG 2013 ] -- START -- ADDED
'            'ENHANCEMENT : TRA CHANGES
'            '"	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid "
'            '"	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid "
'            '"  JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid "
'            ' * CAST((weight/100) AS decimal(10,2)) 
'            'S.SANDEEP [ 14 AUG 2013 ] -- END


'            'S.SANDEEP [ 22 OCT 2013 ] -- START
'            Select Case mintAppointmentTypeId
'                Case enAD_Report_Parameter.APP_DATE_FROM
'                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_BEFORE
'                    If mdtDate1 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_AFTER
'                    If mdtDate1 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
'                    End If
'            End Select
'            'S.SANDEEP [ 22 OCT 2013 ] -- END

'        dsList = objDataOpetation.ExecQuery(StrQ, "List")

'        If objDataOpetation.ErrorMessage <> "" Then
'            exForce = New Exception(objDataOpetation.ErrorNumber & " : " & objDataOpetation.ErrorMessage)
'            Throw exForce
'        End If

'        Return dsList

'    Catch ex As Exception
'        Throw New Exception(ex.Message & " Procedure : Get_GeneralFilter; Modulename : " & mstrModuleName)
'    Finally
'    End Try
'End Function
'    'S.SANDEEP [ 05 MARCH 2012 ] -- END

'    Private Function GetBSC_PerspectiveFilter(ByVal dRow As DataRow, ByVal intPeriodUnkid As Integer) As DataSet
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Dim objDataOpetation As New clsDataOperation
'        Dim dsList As New DataSet
'        Try

'            'S.SANDEEP [ 04 MAR 2014 ] -- START
'            ' "AND kpiunkid <= -1 AND targetunkid <= -1 AND initiativeunkid <= 0 "  -- REMOVED
'            Dim iStringValue As String = String.Empty
'            'S.SANDEEP [ 30 MAY 2014 ] -- START
'            'Dim objWSetting As New clsWeight_Setting(True)
'            Dim objWSetting As New clsWeight_Setting(intPeriodUnkid)
'            'S.SANDEEP [ 30 MAY 2014 ] -- END
'            If objWSetting._Weight_Typeid <= 0 Then
'                iStringValue = " AND kpiunkid > 0 AND targetunkid > 0 AND initiativeunkid  > 0  "
'            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD1 Then
'                iStringValue = " AND kpiunkid <= 0 AND targetunkid <= 0 AND initiativeunkid <= 0 "
'            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD2 Then
'                iStringValue = " AND kpiunkid > 0 "
'            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD3 Then
'                iStringValue = " AND targetunkid > 0 "
'            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD5 Then
'                iStringValue = " AND initiativeunkid > 0 "
'            End If
'            'S.SANDEEP [ 04 MAR 2014 ] -- END


'            'S.SANDEEP [ 14 AUG 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'StrQ = "SELECT " & _
'            '       " employeeunkid " & _
'            '       ",employeecode AS ECODE " & _
'            '       ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
'            '       ",CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
'            '       ",ISNULL(email,'') AS EMAIL " & _
'            '       ",'" & dRow.Item("filter_refid") & "' AS F_RefId " & _
'            '       ",'" & dRow.Item("GUID") & "' AS EGUID "

'StrQ = "SELECT " & _
'      " employeeunkid " & _
'      ",employeecode AS ECODE " & _
'      ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
'      ",CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
'      ",ISNULL(email,'') AS EMAIL " & _
'      ",'" & dRow.Item("filter_refid") & "' AS F_RefId " & _
'                   ",'" & dRow.Item("GUID") & "' AS EGUID " & _
'                   ",ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
'                   ",ISNULL(hrdepartment_master.name,'') AS DEPT "
'            'S.SANDEEP [ 14 AUG 2013 ] -- END
'            If CBool(dRow.Item("isself_score")) = True AndAlso _
'               CBool(dRow.Item("isassessor_score")) = True AndAlso _
'               CBool(dRow.Item("isreviewer_score")) = True Then
'                StrQ &= ",ISNULL(SELF,0) AS SELF " & _
'                         ",ISNULL(Assessor,0) AS Assessor " & _
'                         ",ISNULL(Result,0) AS Reviewer " & _
'                         ",CAST(((ISNULL(SELF,0)+ISNULL(Assessor,0)+ISNULL(Result,0))/3) AS DECIMAL(10,2)) AS Total " & _
'                    "FROM hremployee_master " & _
'                    "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                    "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                    "JOIN " & _
'                    "( " & _
'                         "SELECT " & _
'                               "selfemployeeunkid " & _
'                              ",Self.perspectiveunkid " & _
'                              ",Self.SELF " & _
'                         "FROM hrbsc_analysis_master " & _
'                         "JOIN " & _
'                         "( " & _
'                              "SELECT " & _
'                                    "analysisunkid " & _
'                                   ",perspectiveunkid " & _
'                                      ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'                              "FROM hrbsc_analysis_tran " & _
'                                   "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                   iStringValue & " " & _
'                              "GROUP BY analysisunkid,perspectiveunkid " & _
'                         ") AS Self ON dbo.hrbsc_analysis_master.analysisunkid = Self.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = 1 AND isvoid = 0 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                    ") AS A ON A.selfemployeeunkid = hremployee_master.employeeunkid " & _
'                    "JOIN " & _
'                    "( " & _
'                         "SELECT " & _
'                               "assessedemployeeunkid " & _
'                              ",perspectiveunkid AS Aid " & _
'                              ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(10,2)) AS Assessor " & _
'                         "FROM hrbsc_analysis_master " & _
'                         "JOIN " & _
'                         "( " & _
'                              "SELECT " & _
'                                    "analysisunkid " & _
'                                   ",perspectiveunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'                              "FROM hrbsc_analysis_tran " & _
'                                   "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                   iStringValue & " " & _
'                              "GROUP BY  analysisunkid,perspectiveunkid " & _
'                         ") AS A ON hrbsc_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = 2 AND isvoid = 0 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                         "GROUP BY perspectiveunkid,assessedemployeeunkid " & _
'                    ") AS B ON B.assessedemployeeunkid = hremployee_master.employeeunkid AND Aid = A.perspectiveunkid " & _
'                    "JOIN " & _
'                    "( " & _
'                              "SELECT " & _
'                               "assessedemployeeunkid " & _
'                              ",perspectiveunkid AS Rid " & _
'                              ",Result " & _
'                         "FROM hrbsc_analysis_master " & _
'                         "JOIN " & _
'                         "( " & _
'                              "SELECT " & _
'                                    "analysisunkid " & _
'                                   ",perspectiveunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'                              "FROM hrbsc_analysis_tran " & _
'                                   "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                   iStringValue & " " & _
'                              "GROUP BY  analysisunkid,perspectiveunkid " & _
'                         ") AS A ON hrbsc_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = 3 AND isvoid = 0 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                    ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid AND Rid = A.perspectiveunkid "

'                StrQ &= " WHERE 1 = 1 " & _
'                        " AND (perspectiveunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST(((ISNULL(SELF,0)+ISNULL(Assessor,0)+ISNULL(Result,0))/3) AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso _
'                   CBool(dRow.Item("isassessor_score")) = True AndAlso _
'                   CBool(dRow.Item("isreviewer_score")) = False Then
'            StrQ &= ",ISNULL(SELF,0) AS SELF " & _
'                     ",ISNULL(Assessor,0) AS Assessor " & _
'                     ",CAST(((ISNULL(SELF,0)+ISNULL(Assessor,0))/2) AS DECIMAL(10,2)) AS Total " & _
'                "FROM hremployee_master " & _
'                    "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                        "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                "JOIN " & _
'                "( " & _
'                     "SELECT " & _
'                           "selfemployeeunkid " & _
'                          ",Self.perspectiveunkid " & _
'                          ",Self.SELF " & _
'                     "FROM hrbsc_analysis_master " & _
'                     "JOIN " & _
'                     "( " & _
'                          "SELECT " & _
'                                "analysisunkid " & _
'                               ",perspectiveunkid " & _
'                                          ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'                          "FROM hrbsc_analysis_tran " & _
'                               "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                          "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                   iStringValue & " " & _
'                          "GROUP BY analysisunkid,perspectiveunkid " & _
'                     ") AS Self ON dbo.hrbsc_analysis_master.analysisunkid = Self.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = 1 AND isvoid = 0 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                ") AS A ON A.selfemployeeunkid = hremployee_master.employeeunkid " & _
'                "JOIN " & _
'                "( " & _
'                     "SELECT " & _
'                           "assessedemployeeunkid " & _
'                          ",perspectiveunkid AS Aid " & _
'                          ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(10,2)) AS Assessor " & _
'                     "FROM hrbsc_analysis_master " & _
'                     "JOIN " & _
'                     "( " & _
'                          "SELECT " & _
'                                "analysisunkid " & _
'                               ",perspectiveunkid " & _
'                                       ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'                          "FROM hrbsc_analysis_tran " & _
'                               "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                          "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                   iStringValue & " " & _
'                          "GROUP BY  analysisunkid,perspectiveunkid " & _
'                     ") AS A ON hrbsc_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = 2 AND isvoid = 0 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                     "GROUP BY perspectiveunkid,assessedemployeeunkid " & _
'                ") AS B ON B.assessedemployeeunkid = hremployee_master.employeeunkid AND Aid = A.perspectiveunkid "

'                StrQ &= " WHERE 1 = 1 " & _
'                        " AND (perspectiveunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST(((ISNULL(SELF,0)+ISNULL(Assessor,0))/2) AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso _
'                   CBool(dRow.Item("isassessor_score")) = False AndAlso _
'                   CBool(dRow.Item("isreviewer_score")) = True Then
'            StrQ &= ",ISNULL(SELF,0) AS SELF " & _
'                     ",ISNULL(Result,0) AS Reviewer " & _
'                     ",CAST(((ISNULL(SELF,0)+ISNULL(Result,0))/2) AS DECIMAL(10,2)) AS Total " & _
'                "FROM hremployee_master " & _
'                    "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                        "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                "JOIN " & _
'                "( " & _
'                     "SELECT " & _
'                           "selfemployeeunkid " & _
'                          ",Self.perspectiveunkid " & _
'                          ",Self.SELF " & _
'                     "FROM hrbsc_analysis_master " & _
'                     "JOIN " & _
'                     "( " & _
'                          "SELECT " & _
'                                "analysisunkid " & _
'                               ",perspectiveunkid " & _
'                                          ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'                          "FROM hrbsc_analysis_tran " & _
'                               "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                          "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                   iStringValue & " " & _
'                          "GROUP BY analysisunkid,perspectiveunkid " & _
'                     ") AS Self ON dbo.hrbsc_analysis_master.analysisunkid = Self.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = 1 AND isvoid = 0 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                ") AS A ON A.selfemployeeunkid = hremployee_master.employeeunkid " & _
'                "JOIN " & _
'                "( " & _
'                          "SELECT " & _
'                           "assessedemployeeunkid " & _
'                          ",perspectiveunkid AS Rid " & _
'                          ",Result " & _
'                     "FROM hrbsc_analysis_master " & _
'                     "JOIN " & _
'                     "( " & _
'                          "SELECT " & _
'                                "analysisunkid " & _
'                               ",perspectiveunkid " & _
'                                       ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'                          "FROM hrbsc_analysis_tran " & _
'                               "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                          "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                   iStringValue & " " & _
'                          "GROUP BY  analysisunkid,perspectiveunkid " & _
'                     ") AS A ON hrbsc_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = 3 AND isvoid = 0 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid AND Rid = A.perspectiveunkid "

'                StrQ &= " WHERE 1 = 1 " & _
'                        " AND (perspectiveunkid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST(((ISNULL(SELF,0)+ISNULL(Result,0))/2) AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso _
'                   CBool(dRow.Item("isassessor_score")) = True AndAlso _
'                   CBool(dRow.Item("isreviewer_score")) = True Then
'                StrQ &= ",ISNULL(Assessor,0) AS Assessor " & _
'                         ",ISNULL(Result,0) AS Reviewer " & _
'                         ",CAST(((ISNULL(Assessor,0)+ISNULL(Result,0))/3) AS DECIMAL(10,2)) AS Total " & _
'                    "FROM hremployee_master " & _
'                    "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                    "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                    "JOIN " & _
'                    "( " & _
'                         "SELECT " & _
'                               "assessedemployeeunkid " & _
'                              ",perspectiveunkid AS Aid " & _
'                              ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(10,2)) AS Assessor " & _
'                         "FROM hrbsc_analysis_master " & _
'                         "JOIN " & _
'                         "( " & _
'                              "SELECT " & _
'                                    "analysisunkid " & _
'                                   ",perspectiveunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'                              "FROM hrbsc_analysis_tran " & _
'                                   "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                   iStringValue & " " & _
'                              "GROUP BY  analysisunkid,perspectiveunkid " & _
'                         ") AS A ON hrbsc_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = 2 AND isvoid = 0 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                         "GROUP BY perspectiveunkid,assessedemployeeunkid " & _
'                    ") AS B ON B.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'                    "JOIN " & _
'                    "( " & _
'                              "SELECT " & _
'                               "assessedemployeeunkid " & _
'                              ",perspectiveunkid AS Rid " & _
'                              ",Result " & _
'                         "FROM hrbsc_analysis_master " & _
'                         "JOIN " & _
'                         "( " & _
'                              "SELECT " & _
'                                    "analysisunkid " & _
'                                   ",perspectiveunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'                              "FROM hrbsc_analysis_tran " & _
'                                   "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                   iStringValue & " " & _
'                              "GROUP BY  analysisunkid,perspectiveunkid " & _
'                         ") AS A ON hrbsc_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = 3 AND isvoid = 0 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                    ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid AND Rid = B.Aid "

'                StrQ &= " WHERE 1 = 1 " & _
'                        " AND (Aid = '" & dRow.Item("referenceunkid").ToString & "' AND CAST(((ISNULL(Assessor,0)+ISNULL(Result,0))/3) AS DECIMAL(10,2)) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso _
'                   CBool(dRow.Item("isassessor_score")) = False AndAlso _
'                   CBool(dRow.Item("isreviewer_score")) = False Then
'            StrQ &= ",ISNULL(SELF,0) AS SELF " & _
'                     ",ISNULL(SELF,0) AS Total " & _
'                "FROM hremployee_master " & _
'                    "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                        "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                "JOIN " & _
'                "( " & _
'                     "SELECT " & _
'                           "selfemployeeunkid " & _
'                          ",Self.perspectiveunkid " & _
'                          ",Self.SELF " & _
'                     "FROM hrbsc_analysis_master " & _
'                     "JOIN " & _
'                     "( " & _
'                          "SELECT " & _
'                                "analysisunkid " & _
'                               ",perspectiveunkid " & _
'                                          ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'                          "FROM hrbsc_analysis_tran " & _
'                               "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                          "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                   iStringValue & " " & _
'                          "GROUP BY analysisunkid,perspectiveunkid " & _
'                     ") AS Self ON dbo.hrbsc_analysis_master.analysisunkid = Self.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = 1 AND isvoid = 0 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                ") AS A ON A.selfemployeeunkid = hremployee_master.employeeunkid "

'                StrQ &= " WHERE 1 = 1 " & _
'                        " AND (perspectiveunkid = '" & dRow.Item("referenceunkid").ToString & "' AND ISNULL(SELF,0) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso _
'                   CBool(dRow.Item("isassessor_score")) = True AndAlso _
'                   CBool(dRow.Item("isreviewer_score")) = False Then
'                StrQ &= ",ISNULL(Assessor,0) AS Assessor " & _
'                        ",ISNULL(Assessor,0) AS Total " & _
'                    "FROM hremployee_master " & _
'                    "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                    "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                    "JOIN " & _
'                    "( " & _
'                         "SELECT " & _
'                               "assessedemployeeunkid " & _
'                              ",perspectiveunkid AS Aid " & _
'                              ",CAST(SUM(Result)/COUNT(a.analysisunkid)AS DECIMAL(10,2)) AS Assessor " & _
'                         "FROM hrbsc_analysis_master " & _
'                         "JOIN " & _
'                         "( " & _
'                              "SELECT " & _
'                                    "analysisunkid " & _
'                                   ",perspectiveunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'                              "FROM hrbsc_analysis_tran " & _
'                                   "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                   iStringValue & " " & _
'                              "GROUP BY  analysisunkid,perspectiveunkid " & _
'                         ") AS A ON hrbsc_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = 2 AND isvoid = 0 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                         "GROUP BY perspectiveunkid,assessedemployeeunkid " & _
'                    ") AS B ON B.assessedemployeeunkid = hremployee_master.employeeunkid "

'                StrQ &= " WHERE 1 = 1 " & _
'                        " AND (Aid = '" & dRow.Item("referenceunkid").ToString & "' AND ISNULL(Assessor,0) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso _
'                   CBool(dRow.Item("isassessor_score")) = False AndAlso _
'                   CBool(dRow.Item("isreviewer_score")) = True Then

'                StrQ &= ",ISNULL(Result,0) AS Reviewer " & _
'                         ",ISNULL(Result,0) AS Total " & _
'                    "FROM hremployee_master " & _
'                    "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                    "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                    "JOIN " & _
'                    "( " & _
'                              "SELECT " & _
'                               "assessedemployeeunkid " & _
'                              ",perspectiveunkid AS Rid " & _
'                              ",Result " & _
'                         "FROM hrbsc_analysis_master " & _
'                         "JOIN " & _
'                         "( " & _
'                              "SELECT " & _
'                                    "analysisunkid " & _
'                                   ",perspectiveunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS Result " & _
'                              "FROM hrbsc_analysis_tran " & _
'                                   "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                   iStringValue & " " & _
'                              "GROUP BY  analysisunkid,perspectiveunkid " & _
'                         ") AS A ON hrbsc_analysis_master.analysisunkid = A.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE assessmodeid = 3 AND isvoid = 0 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                    ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid "

'                StrQ &= " WHERE 1 = 1 " & _
'                        " AND (Rid = '" & dRow.Item("referenceunkid").ToString & "' AND ISNULL(Result,0) " & " " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "') "

'            End If

'            'S.SANDEEP [ 22 OCT 2013 ] -- START
'            Select Case mintAppointmentTypeId
'                Case enAD_Report_Parameter.APP_DATE_FROM
'                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_BEFORE
'                    If mdtDate1 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_AFTER
'                    If mdtDate1 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
'                    End If
'            End Select
'            'S.SANDEEP [ 22 OCT 2013 ] -- END

'            dsList = objDataOpetation.ExecQuery(StrQ, "List")

'            If objDataOpetation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOpetation.ErrorNumber & " : " & objDataOpetation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList

'        Catch ex As Exception
'            Throw New Exception(ex.Message & " Procedure : GetBSC_PerspectiveFilter; Modulename : " & mstrModuleName)
'        Finally
'        End Try
'    End Function

'    'Private Function GetSummaryFilter(ByVal dRow As DataRow) As DataSet
'    '    Dim StrQ As String = String.Empty
'    '    Dim exForce As Exception
'    '    Dim objDataOpetation As New clsDataOperation
'    '    Dim dsList As New DataSet
'    '    Try
'    '        StrQ = "SELECT " & _
'    '               " employeeunkid " & _
'    '               ",employeecode AS ECODE " & _
'    '               ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
'    '               ",CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
'    '               ",ISNULL(email,'') AS EMAIL " & _
'    '               ",'" & dRow.Item("filter_refid") & "' AS F_RefId " & _
'    '               ",'" & dRow.Item("GUID") & "' AS EGUID "

'    '        Select Case CInt(dRow.Item("filter_refid"))
'    '            Case Filter_Refid.TOTAL_BSC_SCORE
'    '                If CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'    '                    StrQ &= ",ISNULL(SELF,0) SELF " & _
'    '                            ",ISNULL(AssessScore,0) AS ASSESSOR " & _
'    '                            ",ISNULL(RevScore,0) AS REVIEWER " & _
'    '                            ",CAST((ISNULL(SELF,0)+ISNULL(AssessScore,0)+ISNULL(RevScore,0))/3 AS DECIMAL(10,2)) AS TotalScore " & _
'    '                            "FROM hremployee_master " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "selfemployeeunkid " & _
'    '                                      ",SELF " & _
'    '                                 "FROM hrbsc_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS SELF " & _
'    '                                      "FROM hrbsc_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'    '                                           "AND kpiunkid <= -1 AND targetunkid <= -1 AND initiativeunkid <= 0 " & _
'    '                                      "GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 1 " & _
'    '                            ") AS B ON B.selfemployeeunkid = hremployee_master.employeeunkid " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "assessedemployeeunkid " & _
'    '                                      ",CAST(SUM(AssessScore)/COUNT(assessedemployeeunkid) AS DECIMAL(10,2)) AS AssessScore " & _
'    '                                 "FROM hrbsc_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS AssessScore " & _
'    '                                      "FROM hrbsc_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'    '                                           "AND kpiunkid <= -1 AND targetunkid <= -1 AND initiativeunkid <= 0 " & _
'    '                                      "GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 2 GROUP BY assessedemployeeunkid " & _
'    '                            ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "assessedemployeeunkid " & _
'    '                                      ",RevScore " & _
'    '                                 "FROM hrbsc_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS RevScore " & _
'    '                                      "FROM hrbsc_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'    '                                           "AND kpiunkid <= -1 AND targetunkid <= -1 AND initiativeunkid <= 0 " & _
'    '                                      "GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 3 " & _
'    '                            ") AS D ON D.assessedemployeeunkid = hremployee_master.employeeunkid WHERE 1 =1 " & _
'    '                            " AND CAST((ISNULL(SELF,0)+ISNULL(AssessScore,0)+ISNULL(RevScore,0))/3 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "

'    '                ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'    '                    StrQ &= ",ISNULL(SELF,0) SELF " & _
'    '                            ",ISNULL(AssessScore,0) AS ASSESSOR " & _
'    '                            ",CAST((ISNULL(SELF,0)+ISNULL(AssessScore,0))/2 AS DECIMAL(10,2)) AS TotalScore " & _
'    '                            "FROM hremployee_master " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "selfemployeeunkid " & _
'    '                                      ",SELF " & _
'    '                                 "FROM hrbsc_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS SELF " & _
'    '                                      "FROM hrbsc_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'    '                                           "AND kpiunkid <= -1 AND targetunkid <= -1 AND initiativeunkid <= 0 " & _
'    '                                      "GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 1 " & _
'    '                            ") AS B ON B.selfemployeeunkid = hremployee_master.employeeunkid " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "assessedemployeeunkid " & _
'    '                                      ",CAST(SUM(AssessScore)/COUNT(assessedemployeeunkid) AS DECIMAL(10,2)) AS AssessScore " & _
'    '                                 "FROM hrbsc_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS AssessScore " & _
'    '                                      "FROM hrbsc_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'    '                                           "AND kpiunkid <= -1 AND targetunkid <= -1 AND initiativeunkid <= 0 " & _
'    '                                      "GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 2 GROUP BY assessedemployeeunkid " & _
'    '                            ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid  WHERE 1 = 1 " & _
'    '                            " AND CAST((ISNULL(SELF,0)+ISNULL(AssessScore,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'    '                ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'    '                    StrQ &= ",ISNULL(SELF,0) SELF " & _
'    '                            ",ISNULL(RevScore,0) AS REVIEWER " & _
'    '                            ",CAST((ISNULL(SELF,0)+ISNULL(RevScore,0))/2 AS DECIMAL(10,2)) AS TotalScore " & _
'    '                            "FROM hremployee_master " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "selfemployeeunkid " & _
'    '                                      ",SELF " & _
'    '                                 "FROM hrbsc_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS SELF " & _
'    '                                      "FROM hrbsc_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'    '                                           "AND kpiunkid <= -1 AND targetunkid <= -1 AND initiativeunkid <= 0 " & _
'    '                                      "GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 1 " & _
'    '                            ") AS B ON B.selfemployeeunkid = hremployee_master.employeeunkid " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "assessedemployeeunkid " & _
'    '                                      ",RevScore " & _
'    '                                 "FROM hrbsc_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS RevScore " & _
'    '                                      "FROM hrbsc_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'    '                                           "AND kpiunkid <= -1 AND targetunkid <= -1 AND initiativeunkid <= 0 " & _
'    '                                      "GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 3 " & _
'    '                            ") AS D ON D.assessedemployeeunkid = hremployee_master.employeeunkid  WHERE 1 = 1 " & _
'    '                            " AND CAST((ISNULL(SELF,0)+ISNULL(RevScore,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'    '                ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'    '                    StrQ &= ",ISNULL(AssessScore,0) AS ASSESSOR " & _
'    '                            ",ISNULL(RevScore,0) AS REVIEWER " & _
'    '                            ",CAST((ISNULL(AssessScore,0)+ISNULL(RevScore,0))/2 AS DECIMAL(10,2)) AS TotalScore " & _
'    '                            "FROM hremployee_master " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "assessedemployeeunkid " & _
'    '                                      ",CAST(SUM(AssessScore)/COUNT(assessedemployeeunkid) AS DECIMAL(10,2)) AS AssessScore " & _
'    '                                 "FROM hrbsc_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS AssessScore " & _
'    '                                      "FROM hrbsc_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'    '                                           "AND kpiunkid <= -1 AND targetunkid <= -1 AND initiativeunkid <= 0 " & _
'    '                                      "GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 2 GROUP BY assessedemployeeunkid " & _
'    '                            ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "assessedemployeeunkid " & _
'    '                                      ",RevScore " & _
'    '                                 "FROM hrbsc_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS RevScore " & _
'    '                                      "FROM hrbsc_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'    '                                           "AND kpiunkid <= -1 AND targetunkid <= -1 AND initiativeunkid <= 0 " & _
'    '                                      "GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 3 " & _
'    '                            ") AS D ON D.assessedemployeeunkid = hremployee_master.employeeunkid  WHERE 1 = 1 " & _
'    '                            " AND CAST((ISNULL(AssessScore,0)+ISNULL(RevScore,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'    '                ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'    '                    StrQ &= ",ISNULL(SELF,0) SELF " & _
'    '                            ",ISNULL(SELF,0) AS TotalScore " & _
'    '                            "FROM hremployee_master " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "selfemployeeunkid " & _
'    '                                      ",SELF " & _
'    '                                 "FROM hrbsc_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS SELF " & _
'    '                                      "FROM hrbsc_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'    '                                           "AND kpiunkid <= -1 AND targetunkid <= -1 AND initiativeunkid <= 0 " & _
'    '                                      "GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 1 " & _
'    '                            ") AS B ON B.selfemployeeunkid = hremployee_master.employeeunkid  WHERE 1 = 1 " & _
'    '                            " AND ISNULL(SELF,0) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'    '                ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'    '                    StrQ &= ",ISNULL(AssessScore,0) ASSESSOR " & _
'    '                            ",ISNULL(AssessScore,0) AS TotalScore " & _
'    '                            "FROM hremployee_master " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "assessedemployeeunkid " & _
'    '                                      ",CAST(SUM(AssessScore)/COUNT(assessedemployeeunkid) AS DECIMAL(10,2)) AS AssessScore " & _
'    '                                 "FROM hrbsc_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS AssessScore " & _
'    '                                      "FROM hrbsc_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'    '                                           "AND kpiunkid <= -1 AND targetunkid <= -1 AND initiativeunkid <= 0 " & _
'    '                                      "GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 2 GROUP BY assessedemployeeunkid " & _
'    '                            ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid  WHERE 1 = 1 " & _
'    '                            " AND ISNULL(AssessScore,0) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'    '                ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'    '                    StrQ &= ",ISNULL(RevScore,0) REVIEWER " & _
'    '                            ",ISNULL(RevScore,0) AS TotalScore " & _
'    '                            "FROM hremployee_master " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "assessedemployeeunkid " & _
'    '                                      ",RevScore " & _
'    '                                 "FROM hrbsc_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS RevScore " & _
'    '                                      "FROM hrbsc_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'    '                                           "AND kpiunkid <= -1 AND targetunkid <= -1 AND initiativeunkid <= 0 " & _
'    '                                      "GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 3 " & _
'    '                            ") AS D ON D.assessedemployeeunkid = hremployee_master.employeeunkid  WHERE 1 =1 " & _
'    '                            " AND ISNULL(RevScore,0) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'    '                End If
'    '            Case Filter_Refid.TOTAL_GE_SCORE
'    '                If CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'    '                    StrQ &= ",ISNULL(SELF,0) SELF " & _
'    '                             ",ISNULL(AssessScore,0) AS ASSESSOR " & _
'    '                             ",ISNULL(RevScore,0) AS REVIEWER " & _
'    '                             ",CAST((ISNULL(SELF,0)+ISNULL(AssessScore,0)+ISNULL(RevScore,0))/3 AS DECIMAL(10,2)) AS TotalScore " & _
'    '                            "FROM hremployee_master " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "selfemployeeunkid " & _
'    '                                      ",SELF " & _
'    '                                 "FROM hrassess_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS SELF " & _
'    '                                      "FROM hrassess_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'    '                                      "GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid  AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 1 " & _
'    '                            ") AS B ON B.selfemployeeunkid = hremployee_master.employeeunkid " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "assessedemployeeunkid " & _
'    '                                      ",CAST(SUM(AssessScore)/COUNT(assessedemployeeunkid) AS DECIMAL(10,2)) AS AssessScore " & _
'    '                                 "FROM hrassess_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS AssessScore " & _
'    '                                      "FROM hrassess_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 2 GROUP BY assessedemployeeunkid " & _
'    '                            ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "assessedemployeeunkid " & _
'    '                                      ",RevScore " & _
'    '                                 "FROM hrassess_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS RevScore " & _
'    '                                      "FROM hrassess_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 3 " & _
'    '                            ") AS D ON D.assessedemployeeunkid = hremployee_master.employeeunkid WHERE 1 = 1 " & _
'    '                            " AND CAST((ISNULL(SELF,0)+ISNULL(AssessScore,0)+ISNULL(RevScore,0))/3 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "

'    '                ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'    '                    StrQ &= ",ISNULL(SELF,0) SELF " & _
'    '                            ",ISNULL(AssessScore,0) AS ASSESSOR " & _
'    '                            ",CAST((ISNULL(SELF,0)+ISNULL(AssessScore,0))/2 AS DECIMAL(10,2)) AS TotalScore " & _
'    '                            "FROM hremployee_master " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "selfemployeeunkid " & _
'    '                                      ",SELF " & _
'    '                                 "FROM hrassess_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS SELF " & _
'    '                                      "FROM hrassess_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'    '                                      "GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid  AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 1 " & _
'    '                            ") AS B ON B.selfemployeeunkid = hremployee_master.employeeunkid " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "assessedemployeeunkid " & _
'    '                                      ",CAST(SUM(AssessScore)/COUNT(assessedemployeeunkid) AS DECIMAL(10,2)) AS AssessScore " & _
'    '                                 "FROM hrassess_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS AssessScore " & _
'    '                                      "FROM hrassess_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 2 GROUP BY assessedemployeeunkid " & _
'    '                            ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid WHERE 1 = 1 " & _
'    '                            " AND CAST((ISNULL(SELF,0)+ISNULL(AssessScore,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "

'    '                ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'    '                    StrQ &= ",ISNULL(SELF,0) SELF " & _
'    '                            ",ISNULL(RevScore,0) AS REVIEWER " & _
'    '                            ",CAST((ISNULL(SELF,0)+ISNULL(RevScore,0))/2 AS DECIMAL(10,2)) AS TotalScore " & _
'    '                            "FROM hremployee_master " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "selfemployeeunkid " & _
'    '                                      ",SELF " & _
'    '                                 "FROM hrassess_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS SELF " & _
'    '                                      "FROM hrassess_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'    '                                      "GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid  AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 1 " & _
'    '                            ") AS B ON B.selfemployeeunkid = hremployee_master.employeeunkid " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "assessedemployeeunkid " & _
'    '                                      ",RevScore " & _
'    '                                 "FROM hrassess_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS RevScore " & _
'    '                                      "FROM hrassess_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 3 " & _
'    '                            ") AS D ON D.assessedemployeeunkid = hremployee_master.employeeunkid WHERE 1 = 1 " & _
'    '                            " AND CAST((ISNULL(SELF,0)+ISNULL(RevScore,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "

'    '                ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'    '                    StrQ &= ",ISNULL(AssessScore,0) AS ASSESSOR " & _
'    '                            ",ISNULL(RevScore,0) AS REVIEWER " & _
'    '                            ",CAST((ISNULL(AssessScore,0)+ISNULL(RevScore,0))/3 AS DECIMAL(10,2)) AS TotalScore " & _
'    '                            "FROM hremployee_master " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "assessedemployeeunkid " & _
'    '                                      ",CAST(SUM(AssessScore)/COUNT(assessedemployeeunkid) AS DECIMAL(10,2)) AS AssessScore " & _
'    '                                 "FROM hrassess_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS AssessScore " & _
'    '                                      "FROM hrassess_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 2 GROUP BY assessedemployeeunkid " & _
'    '                            ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "assessedemployeeunkid " & _
'    '                                      ",RevScore " & _
'    '                                 "FROM hrassess_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS RevScore " & _
'    '                                      "FROM hrassess_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 3 " & _
'    '                            ") AS D ON D.assessedemployeeunkid = hremployee_master.employeeunkid WHERE 1 = 1 " & _
'    '                            " AND CAST((ISNULL(AssessScore,0)+ISNULL(RevScore,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "

'    '                ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'    '                    StrQ &= ",ISNULL(SELF,0) SELF " & _
'    '                            ",ISNULL(SELF,0) AS TotalScore " & _
'    '                            "FROM hremployee_master " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "selfemployeeunkid " & _
'    '                                      ",SELF " & _
'    '                                 "FROM hrassess_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS SELF " & _
'    '                                      "FROM hrassess_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'    '                                      "GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid  AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 1 " & _
'    '                            ") AS B ON B.selfemployeeunkid = hremployee_master.employeeunkid WHERE 1 = 1 " & _
'    '                            "  AND ISNULL(SELF,0) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "

'    '                ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'    '                    StrQ &= ",ISNULL(AssessScore,0) AS ASSESSOR " & _
'    '                            ",ISNULL(AssessScore,0) AS TotalScore " & _
'    '                            "FROM hremployee_master " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "selfemployeeunkid " & _
'    '                                      ",SELF " & _
'    '                                 "FROM hrassess_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS SELF " & _
'    '                                      "FROM hrassess_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'    '                                      "GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid  AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 1 " & _
'    '                            ") AS B ON B.selfemployeeunkid = hremployee_master.employeeunkid " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "assessedemployeeunkid " & _
'    '                                      ",CAST(SUM(AssessScore)/COUNT(assessedemployeeunkid) AS DECIMAL(10,2)) AS AssessScore " & _
'    '                                 "FROM hrassess_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS AssessScore " & _
'    '                                      "FROM hrassess_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 2 GROUP BY assessedemployeeunkid " & _
'    '                            ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "assessedemployeeunkid " & _
'    '                                      ",RevScore " & _
'    '                                 "FROM hrassess_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS RevScore " & _
'    '                                      "FROM hrassess_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 3 " & _
'    '                            ") AS D ON D.assessedemployeeunkid = hremployee_master.employeeunkid WHERE 1 = 1 " & _
'    '                            " AND ISNULL(AssessScore,0) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "

'    '                ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'    '                    StrQ &= ",ISNULL(RevScore,0) AS REVIEWER " & _
'    '                            ",ISNULL(RevScore,0) AS TotalScore " & _
'    '                            "FROM hremployee_master " & _
'    '                            "JOIN " & _
'    '                            "( " & _
'    '                                 "SELECT " & _
'    '                                       "assessedemployeeunkid " & _
'    '                                      ",RevScore " & _
'    '                                 "FROM hrassess_analysis_master " & _
'    '                                 "JOIN " & _
'    '                                 "( " & _
'    '                                      "SELECT " & _
'    '                                            "analysisunkid " & _
'    '                                           ",ISNULL(SUM(CAST(resultname AS DECIMAL(10,2))),0) AS RevScore " & _
'    '                                      "FROM hrassess_analysis_tran " & _
'    '                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'    '                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY  analysisunkid " & _
'    '                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'    '                                 "WHERE isvoid = 0 AND assessmodeid = 3 " & _
'    '                            ") AS D ON D.assessedemployeeunkid = hremployee_master.employeeunkid WHERE 1 = 1 " & _
'    '                            " AND ISNULL(RevScore,0) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'    '                End If
'    '        End Select

'    '        dsList = objDataOpetation.ExecQuery(StrQ, "List")

'    '        If objDataOpetation.ErrorMessage <> "" Then
'    '            exForce = New Exception(objDataOpetation.ErrorNumber & " : " & objDataOpetation.ErrorMessage)
'    '            Throw exForce
'    '        End If


'    '        Return dsList

'    '    Catch ex As Exception
'    '        Throw New Exception(ex.Message & " Procedure : GetSummaryFilter; Modulename : " & mstrModuleName)
'    '    Finally
'    '    End Try
'    'End Function

'    Private Function GetSummaryFilter(ByVal dRow As DataRow, ByVal intPeriodUnkid As Integer) As DataSet
'    Dim StrQ As String = String.Empty
'    Dim exForce As Exception
'    Dim objDataOpetation As New clsDataOperation
'    Dim dsList As New DataSet
'    Try
'            'S.SANDEEP [ 04 MAR 2014 ] -- START
'            Dim iStringValue As String = String.Empty
'            'S.SANDEEP [ 30 MAY 2014 ] -- START
'            'Dim objWSetting As New clsWeight_Setting(True)
'            Dim objWSetting As New clsWeight_Setting(intPeriodUnkid)
'            'S.SANDEEP [ 30 MAY 2014 ] -- END
'            If objWSetting._Weight_Typeid <= 0 Then
'                iStringValue = " AND kpiunkid > 0 AND targetunkid > 0 AND initiativeunkid  > 0  "
'            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD1 Then
'                iStringValue = " AND kpiunkid <= 0 AND targetunkid <= 0 AND initiativeunkid <= 0 "
'            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD2 Then
'                iStringValue = " AND kpiunkid > 0 "
'            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD3 Then
'                iStringValue = " AND targetunkid > 0 "
'            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD5 Then
'                iStringValue = " AND initiativeunkid > 0 "
'            End If
'            'S.SANDEEP [ 04 MAR 2014 ] -- END


'            'S.SANDEEP [ 14 AUG 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            'StrQ = "SELECT " & _
'            '       " employeeunkid " & _
'            '       ",employeecode AS ECODE " & _
'            '       ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
'            '       ",CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
'            '       ",ISNULL(email,'') AS EMAIL " & _
'            '       ",'" & dRow.Item("filter_refid") & "' AS F_RefId " & _
'            '       ",'" & dRow.Item("GUID") & "' AS EGUID "
'        StrQ = "SELECT " & _
'               " employeeunkid " & _
'               ",employeecode AS ECODE " & _
'               ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
'               ",CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
'               ",ISNULL(email,'') AS EMAIL " & _
'               ",'" & dRow.Item("filter_refid") & "' AS F_RefId " & _
'                   ",'" & dRow.Item("GUID") & "' AS EGUID " & _
'                   ",ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
'                   ",ISNULL(hrdepartment_master.name,'') AS DEPT "
'            'S.SANDEEP [ 14 AUG 2013 ] -- END
'        Select Case CInt(dRow.Item("filter_refid"))
'            Case Filter_Refid.TOTAL_BSC_SCORE
'                If CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'                    StrQ &= ",ISNULL(SELF,0) SELF " & _
'                            ",ISNULL(AssessScore,0) AS ASSESSOR " & _
'                            ",ISNULL(RevScore,0) AS REVIEWER " & _
'                            ",CAST((ISNULL(SELF,0)+ISNULL(AssessScore,0)+ISNULL(RevScore,0))/3 AS DECIMAL(10,2)) AS TotalScore " & _
'                            "FROM hremployee_master " & _
'                                "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                                "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "selfemployeeunkid " & _
'                                      ",SELF " & _
'                                 "FROM hrbsc_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                            "analysisunkid " & _
'                                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'                                      "FROM hrbsc_analysis_tran " & _
'                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                               iStringValue & " " & _
'                                      "GROUP BY  analysisunkid " & _
'                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = 1 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                            ") AS B ON B.selfemployeeunkid = hremployee_master.employeeunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "assessedemployeeunkid " & _
'                                      ",CAST(SUM(AssessScore)/COUNT(assessedemployeeunkid) AS DECIMAL(10,2)) AS AssessScore " & _
'                                 "FROM hrbsc_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                            "analysisunkid " & _
'                                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS AssessScore " & _
'                                      "FROM hrbsc_analysis_tran " & _
'                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                               iStringValue & " " & _
'                                      "GROUP BY  analysisunkid " & _
'                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = 2 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                                     "GROUP BY assessedemployeeunkid " & _
'                            ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "assessedemployeeunkid " & _
'                                      ",RevScore " & _
'                                 "FROM hrbsc_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                            "analysisunkid " & _
'                                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS RevScore " & _
'                                      "FROM hrbsc_analysis_tran " & _
'                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                               iStringValue & " " & _
'                                      "GROUP BY  analysisunkid " & _
'                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = 3 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                            ") AS D ON D.assessedemployeeunkid = hremployee_master.employeeunkid WHERE 1 =1 " & _
'                            " AND CAST((ISNULL(SELF,0)+ISNULL(AssessScore,0)+ISNULL(RevScore,0))/3 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "

'                ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'                    StrQ &= ",ISNULL(SELF,0) SELF " & _
'                            ",ISNULL(AssessScore,0) AS ASSESSOR " & _
'                            ",CAST((ISNULL(SELF,0)+ISNULL(AssessScore,0))/2 AS DECIMAL(10,2)) AS TotalScore " & _
'                            "FROM hremployee_master " & _
'                                "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                                "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "selfemployeeunkid " & _
'                                      ",SELF " & _
'                                 "FROM hrbsc_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                            "analysisunkid " & _
'                                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'                                      "FROM hrbsc_analysis_tran " & _
'                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                               iStringValue & " " & _
'                                      "GROUP BY  analysisunkid " & _
'                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = 1 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                            ") AS B ON B.selfemployeeunkid = hremployee_master.employeeunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "assessedemployeeunkid " & _
'                                      ",CAST(SUM(AssessScore)/COUNT(assessedemployeeunkid) AS DECIMAL(10,2)) AS AssessScore " & _
'                                 "FROM hrbsc_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                            "analysisunkid " & _
'                                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS AssessScore " & _
'                                      "FROM hrbsc_analysis_tran " & _
'                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                               iStringValue & " " & _
'                                      "GROUP BY  analysisunkid " & _
'                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = 2 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                                     "GROUP BY assessedemployeeunkid " & _
'                            ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid  WHERE 1 = 1 " & _
'                            " AND CAST((ISNULL(SELF,0)+ISNULL(AssessScore,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'                ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'                    StrQ &= ",ISNULL(SELF,0) SELF " & _
'                            ",ISNULL(RevScore,0) AS REVIEWER " & _
'                            ",CAST((ISNULL(SELF,0)+ISNULL(RevScore,0))/2 AS DECIMAL(10,2)) AS TotalScore " & _
'                            "FROM hremployee_master " & _
'                                "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                                "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "selfemployeeunkid " & _
'                                      ",SELF " & _
'                                 "FROM hrbsc_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                            "analysisunkid " & _
'                                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'                                      "FROM hrbsc_analysis_tran " & _
'                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                               iStringValue & " " & _
'                                      "GROUP BY  analysisunkid " & _
'                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = 1 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                            ") AS B ON B.selfemployeeunkid = hremployee_master.employeeunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "assessedemployeeunkid " & _
'                                      ",RevScore " & _
'                                 "FROM hrbsc_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                            "analysisunkid " & _
'                                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS RevScore " & _
'                                      "FROM hrbsc_analysis_tran " & _
'                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                               iStringValue & " " & _
'                                      "GROUP BY  analysisunkid " & _
'                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = 3 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                            ") AS D ON D.assessedemployeeunkid = hremployee_master.employeeunkid  WHERE 1 = 1 " & _
'                            " AND CAST((ISNULL(SELF,0)+ISNULL(RevScore,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'                ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'                    StrQ &= ",ISNULL(AssessScore,0) AS ASSESSOR " & _
'                            ",ISNULL(RevScore,0) AS REVIEWER " & _
'                            ",CAST((ISNULL(AssessScore,0)+ISNULL(RevScore,0))/2 AS DECIMAL(10,2)) AS TotalScore " & _
'                            "FROM hremployee_master " & _
'                                "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                                "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "assessedemployeeunkid " & _
'                                      ",CAST(SUM(AssessScore)/COUNT(assessedemployeeunkid) AS DECIMAL(10,2)) AS AssessScore " & _
'                                 "FROM hrbsc_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                            "analysisunkid " & _
'                                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS AssessScore " & _
'                                      "FROM hrbsc_analysis_tran " & _
'                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                               iStringValue & " " & _
'                                      "GROUP BY  analysisunkid " & _
'                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = 2 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                                     "GROUP BY assessedemployeeunkid " & _
'                            ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "assessedemployeeunkid " & _
'                                      ",RevScore " & _
'                                 "FROM hrbsc_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                            "analysisunkid " & _
'                                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS RevScore " & _
'                                      "FROM hrbsc_analysis_tran " & _
'                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                               iStringValue & " " & _
'                                      "GROUP BY  analysisunkid " & _
'                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = 3 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                            ") AS D ON D.assessedemployeeunkid = hremployee_master.employeeunkid  WHERE 1 = 1 " & _
'                            " AND CAST((ISNULL(AssessScore,0)+ISNULL(RevScore,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'                ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'                    StrQ &= ",ISNULL(SELF,0) SELF " & _
'                            ",ISNULL(SELF,0) AS TotalScore " & _
'                            "FROM hremployee_master " & _
'                                "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                                "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "selfemployeeunkid " & _
'                                      ",SELF " & _
'                                 "FROM hrbsc_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                            "analysisunkid " & _
'                                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'                                      "FROM hrbsc_analysis_tran " & _
'                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                               iStringValue & " " & _
'                                      "GROUP BY  analysisunkid " & _
'                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = 1 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                            ") AS B ON B.selfemployeeunkid = hremployee_master.employeeunkid  WHERE 1 = 1 " & _
'                            " AND ISNULL(SELF,0) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'                ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'                    StrQ &= ",ISNULL(AssessScore,0) ASSESSOR " & _
'                            ",ISNULL(AssessScore,0) AS TotalScore " & _
'                            "FROM hremployee_master " & _
'                                "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                                "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "assessedemployeeunkid " & _
'                                      ",CAST(SUM(AssessScore)/COUNT(assessedemployeeunkid) AS DECIMAL(10,2)) AS AssessScore " & _
'                                 "FROM hrbsc_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                            "analysisunkid " & _
'                                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS AssessScore " & _
'                                      "FROM hrbsc_analysis_tran " & _
'                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                               iStringValue & " " & _
'                                      "GROUP BY  analysisunkid " & _
'                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = 2 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                                     "GROUP BY assessedemployeeunkid " & _
'                            ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid  WHERE 1 = 1 " & _
'                            " AND ISNULL(AssessScore,0) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'                ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'                    StrQ &= ",ISNULL(RevScore,0) REVIEWER " & _
'                            ",ISNULL(RevScore,0) AS TotalScore " & _
'                            "FROM hremployee_master " & _
'                                "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                                "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "assessedemployeeunkid " & _
'                                      ",RevScore " & _
'                                 "FROM hrbsc_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                            "analysisunkid " & _
'                                               ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS RevScore " & _
'                                      "FROM hrbsc_analysis_tran " & _
'                                           "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                      "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                               iStringValue & " " & _
'                                      "GROUP BY  analysisunkid " & _
'                                 ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = 3 AND hrbsc_analysis_master.periodunkid = '" & intPeriodUnkid & "' " & _
'                            ") AS D ON D.assessedemployeeunkid = hremployee_master.employeeunkid  WHERE 1 =1 " & _
'                            " AND ISNULL(RevScore,0) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'                End If

'            Case Filter_Refid.TOTAL_GE_SCORE

'                If CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'                    StrQ &= ",ISNULL(SELF,0) SELF " & _
'                             ",ISNULL(AssessScore,0) AS ASSESSOR " & _
'                             ",ISNULL(RevScore,0) AS REVIEWER " & _
'                             ",CAST((ISNULL(SELF,0)+ISNULL(AssessScore,0)+ISNULL(RevScore,0))/3 AS DECIMAL(10,2)) AS TotalScore " & _
'                            "FROM hremployee_master " & _
'                                 "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                                 "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "selfemployeeunkid " & _
'                                          ",ISNULL(SUM(SELF),0) AS SELF " & _
'                                 "FROM hrassess_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                               " hrassess_analysis_tran.analysisunkid " & _
'                                               ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname *  " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS SELF " & _
'                                      "FROM hrassess_analysis_tran " & _
'                                               "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'                                               "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                               "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'                                          "GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid  AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                                     " GROUP BY selfemployeeunkid " & _
'                            ") AS B ON B.selfemployeeunkid = hremployee_master.employeeunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "assessedemployeeunkid " & _
'                                          ",CAST(SUM(AssessScore)/COUNT(DISTINCT assessormasterunkid) AS DECIMAL(10,2)) AS AssessScore " & _
'                                 "FROM hrassess_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                               " hrassess_analysis_tran.analysisunkid " & _
'                                               ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname *  " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS AssessScore " & _
'                                      "FROM hrassess_analysis_tran " & _
'                                               "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'                                               "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                               "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'                                          "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                                     " GROUP BY assessedemployeeunkid " & _
'                            ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "assessedemployeeunkid " & _
'                                          ",ISNULL(SUM(RevScore),0) AS RevScore " & _
'                                 "FROM hrassess_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                               " hrassess_analysis_tran.analysisunkid " & _
'                                               ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname *  " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS RevScore " & _
'                                      "FROM hrassess_analysis_tran " & _
'                                               "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'                                               "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                               "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'                                          "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                                     "GROUP BY assessedemployeeunkid " & _
'                            ") AS D ON D.assessedemployeeunkid = hremployee_master.employeeunkid WHERE 1 = 1 " & _
'                            " AND CAST((ISNULL(SELF,0)+ISNULL(AssessScore,0)+ISNULL(RevScore,0))/3 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "

'                ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'                    StrQ &= ",ISNULL(SELF,0) SELF " & _
'                            ",ISNULL(AssessScore,0) AS ASSESSOR " & _
'                            ",CAST((ISNULL(SELF,0)+ISNULL(AssessScore,0))/2 AS DECIMAL(10,2)) AS TotalScore " & _
'                            "FROM hremployee_master " & _
'                                "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                                "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "selfemployeeunkid " & _
'                                          ",ISNULL(SUM(SELF),0) AS SELF " & _
'                                 "FROM hrassess_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                               " hrassess_analysis_tran.analysisunkid " & _
'                                               ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname *  " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS SELF " & _
'                                      "FROM hrassess_analysis_tran " & _
'                                               "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'                                               "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                               "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'                                          "GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid  AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                                     " GROUP BY selfemployeeunkid " & _
'                            ") AS B ON B.selfemployeeunkid = hremployee_master.employeeunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "assessedemployeeunkid " & _
'                                          ",CAST(SUM(AssessScore)/COUNT(DISTINCT assessormasterunkid) AS DECIMAL(10,2)) AS AssessScore " & _
'                                 "FROM hrassess_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                               " hrassess_analysis_tran.analysisunkid " & _
'                                               ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname *  " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS AssessScore " & _
'                                      "FROM hrassess_analysis_tran " & _
'                                               "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'                                               "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                               "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'                                          "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                                     "GROUP BY assessedemployeeunkid " & _
'                            ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid WHERE 1 = 1 " & _
'                            " AND CAST((ISNULL(SELF,0)+ISNULL(AssessScore,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "

'                ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'                    StrQ &= ",ISNULL(SELF,0) SELF " & _
'                            ",ISNULL(RevScore,0) AS REVIEWER " & _
'                            ",CAST((ISNULL(SELF,0)+ISNULL(RevScore,0))/2 AS DECIMAL(10,2)) AS TotalScore " & _
'                            "FROM hremployee_master " & _
'                                "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                                "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "selfemployeeunkid " & _
'                                      ",SELF " & _
'                                 "FROM hrassess_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                               " hrassess_analysis_tran.analysisunkid " & _
'                                               ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname *  " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS SELF " & _
'                                      "FROM hrassess_analysis_tran " & _
'                                               "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'                                               "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                               "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'                                          "GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid  AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "'  AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                                     " GROUP BY selfemployeeunkid " & _
'                            ") AS B ON B.selfemployeeunkid = hremployee_master.employeeunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "assessedemployeeunkid " & _
'                                          ",ISNULL(SUM(RevScore),0) AS RevScore " & _
'                                 "FROM hrassess_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                               " hrassess_analysis_tran.analysisunkid " & _
'                                               ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname *  " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS RevScore " & _
'                                      "FROM hrassess_analysis_tran " & _
'                                               "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'                                               "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                               "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'                                          "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                                     " GROUP BY assessedemployeeunkid " & _
'                            ") AS D ON D.assessedemployeeunkid = hremployee_master.employeeunkid WHERE 1 = 1 " & _
'                            " AND CAST((ISNULL(SELF,0)+ISNULL(RevScore,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "

'                ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'                    StrQ &= ",ISNULL(AssessScore,0) AS ASSESSOR " & _
'                            ",ISNULL(RevScore,0) AS REVIEWER " & _
'                            ",CAST((ISNULL(AssessScore,0)+ISNULL(RevScore,0))/3 AS DECIMAL(10,2)) AS TotalScore " & _
'                            "FROM hremployee_master " & _
'                                "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                                "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "assessedemployeeunkid " & _
'                                          ",CAST(SUM(AssessScore)/COUNT(DISTINCT assessormasterunkid) AS DECIMAL(10,2)) AS AssessScore " & _
'                                 "FROM hrassess_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                               " hrassess_analysis_tran.analysisunkid " & _
'                                               ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname *  " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS AssessScore " & _
'                                      "FROM hrassess_analysis_tran " & _
'                                               "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'                                               "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                               "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'                                          "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                                     "GROUP BY assessedemployeeunkid " & _
'                            ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "assessedemployeeunkid " & _
'                                          ",ISNULL(SUM(RevScore),0) AS RevScore " & _
'                                 "FROM hrassess_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                               " hrassess_analysis_tran.analysisunkid " & _
'                                               ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname *  " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS RevScore " & _
'                                      "FROM hrassess_analysis_tran " & _
'                                               "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'                                               "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                               "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'                                          "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY  hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                                     "GROUP BY assessedemployeeunkid " & _
'                            ") AS D ON D.assessedemployeeunkid = hremployee_master.employeeunkid WHERE 1 = 1 " & _
'                            " AND CAST((ISNULL(AssessScore,0)+ISNULL(RevScore,0))/2 AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "

'                ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'                    StrQ &= ",ISNULL(SELF,0) SELF " & _
'                            ",ISNULL(SELF,0) AS TotalScore " & _
'                            "FROM hremployee_master " & _
'                                "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                                "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "selfemployeeunkid " & _
'                                          ",ISNULL(SUM(SELF),0) AS SELF " & _
'                                 "FROM hrassess_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                               " hrassess_analysis_tran.analysisunkid " & _
'                                               ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname *  " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS SELF " & _
'                                      "FROM hrassess_analysis_tran " & _
'                                               "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'                                               "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                               "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'                                      "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'                                          "GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid  AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                                     "GROUP BY selfemployeeunkid " & _
'                            ") AS B ON B.selfemployeeunkid = hremployee_master.employeeunkid WHERE 1 = 1 " & _
'                            "  AND ISNULL(SELF,0) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "

'                ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'                    StrQ &= ",ISNULL(AssessScore,0) AS ASSESSOR " & _
'                            ",ISNULL(AssessScore,0) AS TotalScore " & _
'                            "FROM hremployee_master " & _
'                                "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                                "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "assessedemployeeunkid " & _
'                                          ",CAST(SUM(AssessScore)/COUNT(DISTINCT assessormasterunkid) AS DECIMAL(10,2)) AS AssessScore " & _
'                                 "FROM hrassess_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                               " hrassess_analysis_tran.analysisunkid " & _
'                                               ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname *  " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS AssessScore " & _
'                                      "FROM hrassess_analysis_tran " & _
'                                               "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'                                               "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                               "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'                                          "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                                     "GROUP BY assessedemployeeunkid " & _
'                            ") AS C ON C.assessedemployeeunkid = hremployee_master.employeeunkid " & _
'                                " WHERE 1 = 1 " & _
'                            " AND ISNULL(AssessScore,0) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "

'                ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'                    StrQ &= ",ISNULL(RevScore,0) AS REVIEWER " & _
'                            ",ISNULL(RevScore,0) AS TotalScore " & _
'                            "FROM hremployee_master " & _
'                                "	JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                                "	JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                            "JOIN " & _
'                            "( " & _
'                                 "SELECT " & _
'                                       "assessedemployeeunkid " & _
'                                          ",ISNULL(SUM(RevScore),0) AS RevScore " & _
'                                 "FROM hrassess_analysis_master " & _
'                                 "JOIN " & _
'                                 "( " & _
'                                      "SELECT " & _
'                                               " hrassess_analysis_tran.analysisunkid " & _
'                                               ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname *  " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS RevScore " & _
'                                      "FROM hrassess_analysis_tran " & _
'                                               "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'                                               "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'                                           "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                               "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'                                          "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'                                 ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                                     "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & intPeriodUnkid & "' " & _
'                                     "GROUP BY assessedemployeeunkid " & _
'                            ") AS D ON D.assessedemployeeunkid = hremployee_master.employeeunkid WHERE 1 = 1 " & _
'                            " AND ISNULL(RevScore,0) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'                End If
'        End Select


'            'S.SANDEEP [ 14 AUG 2013 ] -- START -- ADDED
'            'ENHANCEMENT : TRA CHANGES
'            '   " JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid "
'            '   " JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid "
'            '   " JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid "
'            '   " JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid "
'            '   " JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid "
'            '   * CAST((weight/100) AS DECIMAL(10,2))
'            '   * (hrassess_group_master.weight/100)
'            'S.SANDEEP [ 14 AUG 2013 ] -- END


'            'S.SANDEEP [ 22 OCT 2013 ] -- START
'            'ENHANCEMENT : ENHANCEMENT
'            ' CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2)) -- Removed
'            ' IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))")  -- ADDED
'            'S.SANDEEP [ 22 OCT 2013 ] -- END


'            'S.SANDEEP [ 22 OCT 2013 ] -- START
'            Select Case mintAppointmentTypeId
'                Case enAD_Report_Parameter.APP_DATE_FROM
'                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_BEFORE
'                    If mdtDate1 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_AFTER
'                    If mdtDate1 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
'                    End If
'            End Select
'            'S.SANDEEP [ 22 OCT 2013 ] -- END

'        dsList = objDataOpetation.ExecQuery(StrQ, "List")

'        If objDataOpetation.ErrorMessage <> "" Then
'            exForce = New Exception(objDataOpetation.ErrorNumber & " : " & objDataOpetation.ErrorMessage)
'            Throw exForce
'        End If


'        Return dsList

'    Catch ex As Exception
'        Throw New Exception(ex.Message & " Procedure : GetSummaryFilter; Modulename : " & mstrModuleName)
'    Finally
'    End Try
'End Function

'    'S.SANDEEP [ 14 AUG 2013 ] -- START
'    'ENHANCEMENT : TRA CHANGES
'    Private Function Get_Overall_Filter(ByVal dRow As DataRow, ByVal iPeriodId As Integer) As DataSet
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Dim objDataOpetation As New clsDataOperation
'        Dim dsList As New DataSet
'        Try
'            'S.SANDEEP [ 04 MAR 2014 ] -- START
'            Dim iStringValue As String = String.Empty
'            'S.SANDEEP [ 30 MAY 2014 ] -- START
'            'Dim objWSetting As New clsWeight_Setting(True)
'            Dim objWSetting As New clsWeight_Setting(iPeriodId)
'            'S.SANDEEP [ 30 MAY 2014 ] -- END
'            If objWSetting._Weight_Typeid <= 0 Then
'                iStringValue = " AND kpiunkid > 0 AND targetunkid > 0 AND initiativeunkid  > 0  "
'            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD1 Then
'                iStringValue = " AND kpiunkid <= 0 AND targetunkid <= 0 AND initiativeunkid <= 0 "
'            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD2 Then
'                iStringValue = " AND kpiunkid > 0 "
'            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD3 Then
'                iStringValue = " AND targetunkid > 0 "
'            ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD5 Then
'                iStringValue = " AND initiativeunkid > 0 "
'            End If
'            'S.SANDEEP [ 04 MAR 2014 ] -- END

'            'S.SANDEEP [ 10 SEPT 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            StrQ = " SELECT employeeunkid,ECODE,ENAME,appointeddate,EMAIL,F_RefId,EGUID,JOB_TITLE,DEPT,Overall_Score FROM ("
'            'S.SANDEEP [ 10 SEPT 2013 ] -- END
'            StrQ &= "SELECT " & _
'       " employeeunkid " & _
'       ",employeecode AS ECODE " & _
'       ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS ENAME " & _
'       ",CONVERT(CHAR(8),appointeddate,112) AS appointeddate " & _
'       ",ISNULL(email,'') AS EMAIL " & _
'       ",'" & dRow.Item("filter_refid") & "' AS F_RefId " & _
'                   ",'" & dRow.Item("GUID") & "' AS EGUID " & _
'                   ",ISNULL(hrjob_master.job_name,'') AS JOB_TITLE " & _
'                   ",ISNULL(hrdepartment_master.name,'') AS DEPT "

'            If CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'                StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/3 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/3 *(bsc_weight/100) AS DECIMAL(10,2)) AS Overall_Score "
'            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'                StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Overall_Score "
'            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'                StrQ &= ",CAST((ISNULL(S_GA.S_GA,0) + ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Overall_Score "
'            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'                StrQ &= ",CAST((ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100) AS DECIMAL(10,2)) AS Overall_Score "
'            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'                StrQ &= ",CAST((ISNULL(S_GA.S_GA,0)) * (ge_weight/100) + (ISNULL(S_BSC.S_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Overall_Score "
'            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'                StrQ &= ",CAST((ISNULL(A_GA.A_GA,0)) * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0)) * (bsc_weight/100) AS DECIMAL(10,2)) AS Overall_Score "
'            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'                StrQ &= ",CAST((ISNULL(R_GA.R_GA,0)) * (ge_weight/100)+(ISNULL(R_BSC.R_BSC,0))*(bsc_weight/100) AS DECIMAL(10,2)) AS Overall_Score "
'            End If

'            StrQ &= "FROM hremployee_master " & _
'                    "JOIN hrassessment_ratio ON hremployee_master.jobgroupunkid = hrassessment_ratio.jobgroupunkid AND periodunkid = '" & iPeriodId & "' AND hrassessment_ratio.isactive = 1 " & _
'                    "JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
'                    "JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
'                    "LEFT JOIN " & _
'                    "( " & _
'                         "SELECT " & _
'                              " selfemployeeunkid AS EmpId " & _
'                              ",ISNULL(SUM(SELF),0) AS S_GA " & _
'                         "FROM hrassess_analysis_master " & _
'                              "JOIN " & _
'                              "( " & _
'                                   "SELECT " & _
'                                        " hrassess_analysis_tran.analysisunkid " & _
'                                        ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname *  " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS SELF " & _
'                                   "FROM hrassess_analysis_tran " & _
'                                        "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'                                        "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'                                        "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                        "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'                                   "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 " & _
'                                   "GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'                              ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid  AND iscommitted = 1 " & _
'                         "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid = '" & iPeriodId & "' " & _
'                         "GROUP BY selfemployeeunkid " & _
'                    ") AS S_GA ON S_GA.EmpId = employeeunkid " & _
'                    "LEFT JOIN " & _
'                    "( " & _
'                    "SELECT " & _
'                               "selfemployeeunkid " & _
'                              ",SELF AS S_BSC " & _
'                         "FROM hrbsc_analysis_master " & _
'                         "JOIN " & _
'                         "( " & _
'                              "SELECT " & _
'                                   "analysisunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS SELF " & _
'                              "FROM hrbsc_analysis_tran " & _
'                              "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                              iStringValue & " " & _
'                              "GROUP BY  analysisunkid " & _
'                         ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' AND hrbsc_analysis_master.periodunkid = '" & iPeriodId & "' " & _
'                    ") AS S_BSC ON S_BSC.selfemployeeunkid = employeeunkid " & _
'                    "LEFT JOIN " & _
'                    "( " & _
'                         "SELECT " & _
'                               "assessedemployeeunkid " & _
'                              ",CAST(SUM(AssessScore)/COUNT(DISTINCT assessormasterunkid) AS DECIMAL(10,2)) AS A_GA " & _
'                         "FROM hrassess_analysis_master " & _
'                         "JOIN " & _
'                         "( " & _
'                              "SELECT " & _
'                                   " hrassess_analysis_tran.analysisunkid " & _
'                                   ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname *  " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS AssessScore " & _
'                              "FROM hrassess_analysis_tran " & _
'                                   "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'                                   "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'                                   "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                   "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'                              "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'                         ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & iPeriodId & "' " & _
'                         "GROUP BY assessedemployeeunkid " & _
'                    ") AS A_GA ON A_GA.assessedemployeeunkid = employeeunkid " & _
'                    "LEFT JOIN " & _
'                    "( " & _
'                         "SELECT " & _
'                               "assessedemployeeunkid " & _
'                              ",CAST(SUM(AssessScore)/COUNT(assessedemployeeunkid) AS DECIMAL(10,2)) AS A_BSC " & _
'                         "FROM hrbsc_analysis_master " & _
'                         "JOIN " & _
'                         "( " & _
'                              "SELECT " & _
'                                    "analysisunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS AssessScore " & _
'                              "FROM hrbsc_analysis_tran " & _
'                                   "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                   iStringValue & " " & _
'                              "GROUP BY  analysisunkid " & _
'                         ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND hrbsc_analysis_master.periodunkid = '" & iPeriodId & "' " & _
'                         "GROUP BY assessedemployeeunkid " & _
'                    ") AS A_BSC ON A_BSC.assessedemployeeunkid = employeeunkid " & _
'                    "LEFT JOIN " & _
'                    "( " & _
'                         "SELECT " & _
'                               "assessedemployeeunkid " & _
'                              ",ISNULL(SUM(RevScore),0) AS R_GA " & _
'                         "FROM hrassess_analysis_master " & _
'                         "JOIN " & _
'                         "( " & _
'                              "SELECT " & _
'                                   " hrassess_analysis_tran.analysisunkid " & _
'                                   ",CAST(ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname *  " & IIf(ConfigParameter._Object._ConsiderItemWeightAsNumber = True, "CAST((hrassess_item_master.weight) AS DECIMAL(10,2))", "CAST((hrassess_item_master.weight/100) AS DECIMAL(10,2))") & "  ELSE CAST(0 AS DECIMAL(10,2)) END) AS DECIMAL(10, 2)),0) * (hrassess_group_master.weight/100) AS DECIMAL(10,2)) AS RevScore " & _
'                              "FROM hrassess_analysis_tran " & _
'                                   "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
'                                   "JOIN hrassess_group_master ON hrassess_analysis_master.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
'                                   "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                                   "JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_analysis_tran.assessitemunkid " & _
'                              "WHERE ISNULL(hrassess_analysis_tran.isvoid,0) = 0 GROUP BY hrassess_analysis_tran.analysisunkid,hrassess_group_master.weight " & _
'                         ") AS Slf ON hrassess_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND hrassess_analysis_master.periodunkid ='" & iPeriodId & "' " & _
'                         "GROUP BY assessedemployeeunkid " & _
'                    ") AS R_GA ON R_GA.assessedemployeeunkid = employeeunkid " & _
'                    "LEFT JOIN " & _
'                    "( " & _
'                         "SELECT " & _
'                               "assessedemployeeunkid " & _
'                              ",RevScore AS R_BSC " & _
'                         "FROM hrbsc_analysis_master " & _
'                         "JOIN " & _
'                         "( " & _
'                              "SELECT " & _
'                                    "analysisunkid " & _
'                                   ",ISNULL(CAST(SUM(CASE WHEN ISNUMERIC(resultname) <> 0 THEN resultname ELSE CAST(0 AS DECIMAL(10,2)) END)AS DECIMAL(10,2)),0) AS RevScore " & _
'                              "FROM hrbsc_analysis_tran " & _
'                                   "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
'                              "WHERE ISNULL(hrbsc_analysis_tran.isvoid,0) = 0 " & _
'                                   iStringValue & " " & _
'                              "GROUP BY  analysisunkid " & _
'                         ") AS Slf ON hrbsc_analysis_master.analysisunkid = Slf.analysisunkid AND iscommitted = 1 " & _
'                         "WHERE isvoid = 0 AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND hrbsc_analysis_master.periodunkid = '" & iPeriodId & "' " & _
'                    ") AS R_BSC ON R_BSC.assessedemployeeunkid = employeeunkid "

'            StrQ &= " WHERE 1 = 1 "

'            'S.SANDEEP [ 22 OCT 2013 ] -- START
'            Select Case mintAppointmentTypeId
'                Case enAD_Report_Parameter.APP_DATE_FROM
'                    If mdtDate1 <> Nothing AndAlso mdtDate2 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') BETWEEN '" & eZeeDate.convertDate(mdtDate1) & "' AND '" & eZeeDate.convertDate(mdtDate2) & "' "
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_BEFORE
'                    If mdtDate1 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') < '" & eZeeDate.convertDate(mdtDate1) & "' "
'                    End If
'                Case enAD_Report_Parameter.APP_DATE_AFTER
'                    If mdtDate1 <> Nothing Then
'                        StrQ &= " AND ISNULL(CONVERT(CHAR(8), hremployee_master.appointeddate,112),'') > '" & eZeeDate.convertDate(mdtDate1) & "' "
'                    End If
'            End Select
'            'S.SANDEEP [ 22 OCT 2013 ] -- END


'            If CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'                StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/3 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/3 *(bsc_weight/100)) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'                StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0) + ISNULL(A_GA.A_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0)+ ISNULL(A_BSC.A_BSC,0))/2 *(bsc_weight/100)) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'                StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0)+ ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(S_BSC.S_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100)) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'                StrQ &= " AND CAST(((ISNULL(A_GA.A_GA,0) + ISNULL(R_GA.R_GA,0))/2 * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0) + ISNULL(R_BSC.R_BSC,0))/2 *(bsc_weight/100)) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'            ElseIf CBool(dRow.Item("isself_score")) = True AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'                StrQ &= " AND CAST(((ISNULL(S_GA.S_GA,0)) * (ge_weight/100) + (ISNULL(S_BSC.S_BSC,0)) * (bsc_weight/100)) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = True AndAlso CBool(dRow.Item("isreviewer_score")) = False Then
'                StrQ &= " AND CAST(((ISNULL(A_GA.A_GA,0)) * (ge_weight/100)+(ISNULL(A_BSC.A_BSC,0)) * (bsc_weight/100)) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'            ElseIf CBool(dRow.Item("isself_score")) = False AndAlso CBool(dRow.Item("isassessor_score")) = False AndAlso CBool(dRow.Item("isreviewer_score")) = True Then
'                StrQ &= " AND CAST(((ISNULL(R_GA.R_GA,0)) * (ge_weight/100)+(ISNULL(R_BSC.R_BSC,0))*(bsc_weight/100)) AS DECIMAL(10,2)) " & dRow.Item("contition").ToString & " '" & dRow.Item("filter_value").ToString & "' "
'            End If

'            'S.SANDEEP [ 10 SEPT 2013 ] -- START
'            'ENHANCEMENT : TRA CHANGES
'            StrQ &= ") AS A WHERE 1 = 1 AND Overall_Score > 0 "
'            'S.SANDEEP [ 10 SEPT 2013 ] -- END


'            dsList = objDataOpetation.ExecQuery(StrQ, "List")

'            If objDataOpetation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOpetation.ErrorNumber & " : " & objDataOpetation.ErrorMessage)
'                Throw exForce
'            End If


'            Return dsList

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Get_Overall_Filter", mstrModuleName)
'            Return Nothing
'        Finally
'        End Try
'    End Function
'    'S.SANDEEP [ 14 AUG 2013 ] -- END

'#End Region

'	'<Language> This Auto Generated Text Please Do Not Modify it.
'#Region " Language & UI Settings "
'	Public Shared Sub SetMessages()
'		Try
'			Language.setMessage(mstrModuleName, 1, "Filter Criteria")
'			Language.setMessage(mstrModuleName, 2, "Value")
'			Language.setMessage(mstrModuleName, 3, "Condition")
'			Language.setMessage(mstrModuleName, 4, "Operation")
'			Language.setMessage(mstrModuleName, 6, "Code")
'			Language.setMessage(mstrModuleName, 7, "Employee")
'			Language.setMessage(mstrModuleName, 8, "Email")
'			Language.setMessage(mstrModuleName, 9, "Appointed Date")
'			Language.setMessage(mstrModuleName, 10, "Job Title")
'			Language.setMessage(mstrModuleName, 11, "Department")
'			Language.setMessage(mstrModuleName, 12, "Overall Score")

'		Catch Ex As Exception
'			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
'		End Try
'	End Sub
'#End Region 'Language & UI Settings
'	'</Language>
'End Class