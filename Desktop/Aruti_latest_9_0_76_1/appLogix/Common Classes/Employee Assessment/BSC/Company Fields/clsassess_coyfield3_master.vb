﻿'************************************************************************************************************************************
'Class Name : clsassess_coyfield3_master.vb
'Purpose    :
'Date       :05-May-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_coyfield3_master
    Private Shared ReadOnly mstrModuleName As String = "clsassess_coyfield3_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintCoyfield3unkid As Integer = 0
    Private mintCoyfield2unkid As Integer = 0
    Private mintFieldunkid As Integer = 0
    Private mstrField_Data As String = String.Empty
    Private mintUserunkid As Integer = 0
    Private mdblWeight As Double = 0
    Private mintPeriodunkid As Integer = 0
    Private mdtStartdate As Date = Nothing
    Private mdtEnddate As Date = Nothing
    Private mintStatusunkid As Integer = 0
    Private mintOwnerrefid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mstrVoidreason As String = String.Empty
    Private mdtVoiddatetime As Date = Nothing
    Private mintCoyFieldTypeId As Integer = 0

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set coyfield3unkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Coyfield3unkid() As Integer
        Get
            Return mintCoyfield3unkid
        End Get
        Set(ByVal value As Integer)
            mintCoyfield3unkid = value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set coyfield2unkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Coyfield2unkid() As Integer
        Get
            Return mintCoyfield2unkid
        End Get
        Set(ByVal value As Integer)
            mintCoyfield2unkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fieldunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Fieldunkid() As Integer
        Get
            Return mintFieldunkid
        End Get
        Set(ByVal value As Integer)
            mintFieldunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set field_data
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Field_Data() As String
        Get
            Return mstrField_Data
        End Get
        Set(ByVal value As String)
            mstrField_Data = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set weight
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Weight() As Double
        Get
            Return mdblWeight
        End Get
        Set(ByVal value As Double)
            mdblWeight = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Startdate() As Date
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As Date)
            mdtStartdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enddate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Enddate() As Date
        Get
            Return mdtEnddate
        End Get
        Set(ByVal value As Date)
            mdtEnddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ownerrefid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ownerrefid() As Integer
        Get
            Return mintOwnerrefid
        End Get
        Set(ByVal value As Integer)
            mintOwnerrefid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set coyfieldtypeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _CoyFieldTypeId() As Integer
        Set(ByVal value As Integer)
            mintCoyFieldTypeId = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  coyfield3unkid " & _
              ", coyfield2unkid " & _
              ", fieldunkid " & _
              ", field_data " & _
              ", userunkid " & _
              ", weight " & _
              ", periodunkid " & _
              ", startdate " & _
              ", enddate " & _
              ", statusunkid " & _
              ", ownerrefid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
             "FROM hrassess_coyfield3_master " & _
             "WHERE coyfield3unkid = @coyfield3unkid "

            objDataOperation.AddParameter("@coyfield3unkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintCoyfield3UnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintcoyfield3unkid = CInt(dtRow.Item("coyfield3unkid"))
                mintcoyfield2unkid = CInt(dtRow.Item("coyfield2unkid"))
                mintfieldunkid = CInt(dtRow.Item("fieldunkid"))
                mstrfield_data = dtRow.Item("field_data").ToString
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mdblweight = CDbl(dtRow.Item("weight"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                If IsDBNull(dtRow.Item("startdate")) = False Then
                    mdtStartdate = dtRow.Item("startdate")
                Else
                    mdtStartdate = Nothing
                End If
                If IsDBNull(dtRow.Item("enddate")) = False Then
                    mdtEnddate = dtRow.Item("enddate")
                Else
                    mdtEnddate = Nothing
                End If
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintOwnerrefid = CInt(dtRow.Item("ownerrefid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  coyfield3unkid " & _
              ", coyfield2unkid " & _
              ", fieldunkid " & _
              ", field_data " & _
              ", userunkid " & _
              ", weight " & _
              ", periodunkid " & _
              ", startdate " & _
              ", enddate " & _
              ", statusunkid " & _
              ", ownerrefid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
             "FROM hrassess_coyfield3_master "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_coyfield3_master) </purpose>
    Public Function Insert(Optional ByVal mdtOwner As DataTable = Nothing, Optional ByVal mdicInfoFieldData As Dictionary(Of Integer, String) = Nothing) As Boolean
        If isExist(mstrField_Data, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@coyfield2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoyfield2unkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, mstrField_Data.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblWeight.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            If mdtStartdate <> Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@ownerrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwnerrefid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            
            strQ = "INSERT INTO hrassess_coyfield3_master ( " & _
              "  coyfield2unkid " & _
              ", fieldunkid " & _
              ", field_data " & _
              ", userunkid " & _
              ", weight " & _
              ", periodunkid " & _
              ", startdate " & _
              ", enddate " & _
              ", statusunkid " & _
              ", ownerrefid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime" & _
            ") VALUES (" & _
              "  @coyfield2unkid " & _
              ", @fieldunkid " & _
              ", @field_data " & _
              ", @userunkid " & _
              ", @weight " & _
              ", @periodunkid " & _
              ", @startdate " & _
              ", @enddate " & _
              ", @statusunkid " & _
              ", @ownerrefid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voidreason " & _
              ", @voiddatetime" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCoyfield3unkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_coyfield3_master", "coyfield3unkid", mintCoyfield3unkid, , mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtOwner IsNot Nothing Then
                Dim objOwner As New clsassess_coyowner_tran
                objOwner._DatTable = mdtOwner
                If objOwner.InsertDelete_Allocation(objDataOperation, mintUserunkid, mintCoyfield3unkid, mintCoyFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objOwner = Nothing
            End If

            If mdicInfoFieldData IsNot Nothing Then
                Dim objInfoField As New clsassess_coyinfofield_tran
                objInfoField._dicInfoField = mdicInfoFieldData
                If objInfoField.InsertDelete_InfoField(objDataOperation, mintUserunkid, mintCoyfield3unkid, mintCoyFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objInfoField = Nothing
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_coyfield1_master) </purpose>
    Public Function Update(Optional ByVal mdtOwner As DataTable = Nothing, Optional ByVal mdicInfoFieldData As Dictionary(Of Integer, String) = Nothing) As Boolean
        If isExist(mstrField_Data, mintPeriodunkid, mintCoyfield3unkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@coyfield3unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoyfield3unkid.ToString)
            objDataOperation.AddParameter("@coyfield2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCoyfield2unkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, mstrField_Data)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblWeight.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            If mdtStartdate <> Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate.ToString)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@ownerrefid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwnerrefid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            strQ = "UPDATE hrassess_coyfield3_master SET " & _
                   "  coyfield2unkid = @coyfield2unkid" & _
                   ", fieldunkid = @fieldunkid" & _
                   ", field_data = @field_data" & _
                   ", userunkid = @userunkid" & _
                   ", weight = @weight" & _
                   ", periodunkid = @periodunkid" & _
                   ", startdate = @startdate" & _
                   ", enddate = @enddate" & _
                   ", statusunkid = @statusunkid" & _
                   ", ownerrefid = @ownerrefid" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voidreason = @voidreason" & _
                   ", voiddatetime = @voiddatetime " & _
                   "WHERE coyfield3unkid = @coyfield3unkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_coyfield3_master", mintCoyfield3unkid, "coyfield3unkid", 2, objDataOperation) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_coyfield3_master", "coyfield3unkid", mintCoyfield3unkid, , mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If mdtOwner IsNot Nothing Then
                Dim objOwner As New clsassess_coyowner_tran
                objOwner._DatTable = mdtOwner
                If objOwner.InsertDelete_Allocation(objDataOperation, mintUserunkid, mintCoyfield3unkid, mintCoyFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objOwner = Nothing
            End If

            If mdicInfoFieldData IsNot Nothing Then
                Dim objInfoField As New clsassess_coyinfofield_tran
                objInfoField._dicInfoField = mdicInfoFieldData
                If objInfoField.InsertDelete_InfoField(objDataOperation, mintUserunkid, mintCoyfield3unkid, mintCoyFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objInfoField = Nothing
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_coyfield3_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal iDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objCoyField4 As New clsassess_coyfield4_master

        If iDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = iDataOpr
        End If

        Try
            strQ = "UPDATE hrassess_coyfield3_master SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voidreason = @voidreason" & _
                   ", voiddatetime = @voiddatetime " & _
                   "WHERE coyfield3unkid = @coyfield3unkid AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@coyfield3unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_coyfield3_master", "coyfield3unkid", intUnkid, , mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsChild As New DataSet : Dim dtmp As DataRow() = Nothing
            '==================|START VOIDING CHILD DATA FIELDS IF MAPPED|==============='
            dsChild = clsCommonATLog.GetChildList(objDataOperation, "hrassess_coyfield4_master", "coyfield3unkid", intUnkid)
            dtmp = dsChild.Tables(0).Select("isvoid = 0")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    objCoyField4._Isvoid = mblnIsvoid
                    objCoyField4._Voiddatetime = mdtVoiddatetime
                    objCoyField4._Voidreason = mstrVoidreason
                    objCoyField4._Voiduserunkid = mintVoiduserunkid
                    If objCoyField4.Delete(dr.Item("coyfield4unkid"), objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If
            '==================|ENDING VOIDING CHILD DATA FIELDS IF MAPPED|==============='

            '==================|START VOIDING INFORMATIONAL FIELDS IF MAPPED|==============='
            dsChild = clsCommonATLog.GetChildList(objDataOperation, "hrassess_coyinfofield_tran", "coyfieldunkid", intUnkid)
            dtmp = dsChild.Tables(0).Select("coyfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD3 & "'")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_coyinfofield_tran", "coyinfofieldunkid", dr.Item("coyinfofieldunkid"), , mintVoiduserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
                strQ = "DELETE FROM hrassess_coyinfofield_tran WHERE coyfieldunkid = '" & intUnkid & "' AND coyfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD3 & "' "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            '==================|ENDING VOIDING INFORMATIONAL FIELDS IF MAPPED|==============='

            '==================|START VOIDING GOAL OWNSER DATA IF MAPPED|==============='
            dsChild = clsCommonATLog.GetChildList(objDataOperation, "hrassess_coyowner_tran", "coyfieldunkid", intUnkid)
            dtmp = dsChild.Tables(0).Select("coyfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD3 & "'")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_coyowner_tran", "ownertranunkid", dr.Item("ownertranunkid"), , mintVoiduserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
                strQ = "DELETE FROM hrassess_coyowner_tran WHERE coyfieldunkid = '" & intUnkid & "' AND coyfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD3 & "' "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            '==================|ENDING VOIDING GOAL OWNSER DATA IF MAPPED|==============='

            If iDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If iDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objCoyField4 = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@coyfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, ByVal iPeriodId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  coyfield3unkid " & _
                   ", coyfield2unkid " & _
                   ", fieldunkid " & _
                   ", field_data " & _
                   ", userunkid " & _
                   ", weight " & _
                   ", periodunkid " & _
                   ", startdate " & _
                   ", enddate " & _
                   ", statusunkid " & _
                   ", ownerrefid " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voidreason " & _
                   ", voiddatetime " & _
                   "FROM hrassess_coyfield3_master " & _
                   "WHERE field_data = @field_data " & _
                   "AND isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND coyfield3unkid <> @coyfield3unkid"
            End If

            If iPeriodId > 0 Then
                strQ &= " AND periodunkid = @PeriodId "
            End If

            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, strName.Length, strName)
            objDataOperation.AddParameter("@coyfield3unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Get Data Set for Combo </purpose>
    Public Function getComboList(Optional ByVal iList As String = "List", Optional ByVal iAddSelect As Boolean = False, Optional ByVal iParentId As Integer = 0) As DataSet
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            If iAddSelect = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            StrQ &= "SELECT coyfield3unkid AS Id, field_data AS Name FROM hrassess_coyfield3_master WHERE isvoid = 0 "
            If iParentId > 0 Then
                StrQ &= " AND coyfield2unkid = '" & iParentId & "' "
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        Finally
            objDataOperation = Nothing
            dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Get OwnerRefId for Default </purpose>
    Public Function GetOwnerRefId() As Integer
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT TOP 1 ISNULL(ownerrefid,0) AS Id FROM hrassess_coyfield3_master WHERE isvoid = 0 "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item("Id"))
            Else
                Return -1
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetOwnerRefId", mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP [ 10 JAN 2015 ] -- START
    Public Function GetListForTansfer(ByVal xPeriodId As Integer) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT " & _
                       "     coyfield3unkid " & _
                       "    ,coyfield2unkid " & _
                       "    ,fieldunkid " & _
                       "    ,field_data " & _
                       "    ,userunkid " & _
                       "    ,weight " & _
                       "    ,periodunkid " & _
                       "    ,startdate " & _
                       "    ,enddate " & _
                       "    ,statusunkid " & _
                       "    ,ownerrefid " & _
                       "    ,isvoid " & _
                       "    ,voiduserunkid " & _
                       "    ,voidreason " & _
                       "    ,voiddatetime " & _
                       "FROM hrassess_coyfield3_master " & _
                       "WHERE isvoid = 0 AND periodunkid = '" & xPeriodId & "' ORDER BY coyfield3unkid "

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetListForTansfer", mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function GetCoyFieldUnkid(ByVal xFieldData As String, ByVal xPeriodId As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim xUnkid As Integer = -1
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT @xUnkid = coyfield3unkid FROM hrassess_coyfield3_master WHERE isvoid = 0 AND periodunkid = @xPeriodId AND field_data = @xFieldData "

                objDo.AddParameter("@xUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUnkid, ParameterDirection.InputOutput)
                objDo.AddParameter("@xPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
                objDo.AddParameter("@xFieldData", SqlDbType.NVarChar, xFieldData.Length, xFieldData)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                xUnkid = objDo.GetParameterValue("@xUnkid")

            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCoyFieldUnkid", mstrModuleName)
        Finally
        End Try
        Return xUnkid
    End Function
    'S.SANDEEP [ 10 JAN 2015 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data.")
			Language.setMessage(mstrModuleName, 2, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
