﻿'************************************************************************************************************************************
'Class Name :clsassess_coyinfofield_tran.vb
'Purpose    :
'Date       :24-Apr-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_coyinfofield_tran

#Region " Private Variables "

    Private Const mstrModuleName = "clsassess_coyinfofield_tran"
    Private mstrMessage As String = ""
    Private mintCoyInfoFieldtranunkid As Integer
    Private mdicInfoField As Dictionary(Of Integer, String)

#End Region

#Region " Properties "

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _dicInfoField() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicInfoField = value
        End Set
    End Property

#End Region

#Region " Private Methods "

    Public Function Get_Data(ByVal iCoyFieldUnkid As Integer, ByVal iCoyFieldTypeId As Integer) As Dictionary(Of Integer, String)
        Dim StrQ As String = String.Empty
        Dim objDataOpr As New clsDataOperation
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim mdicInfoField As New Dictionary(Of Integer, String)
        Try
            StrQ = "SELECT " & _
                   "  coyinfofieldunkid " & _
                   ", coyfieldunkid " & _
                   ", fieldunkid " & _
                   ", field_data " & _
                   ", coyfieldtypeid " & _
                   "FROM hrassess_coyinfofield_tran " & _
                   "WHERE coyfieldunkid = @coyfieldunkid AND coyfieldtypeid = @coyfieldtypeid "

            objDataOpr.AddParameter("@coyfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCoyFieldUnkid.ToString)
            objDataOpr.AddParameter("@coyfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCoyFieldTypeId.ToString)

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    If mdicInfoField.ContainsKey(dRow.Item("fieldunkid")) = False Then
                        mdicInfoField.Add(dRow.Item("fieldunkid"), dRow.Item("field_data"))
                    End If
                Next
            End If

            Return mdicInfoField
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Data", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Public Function InsertDelete_InfoField(ByVal objDataOpr As clsDataOperation, ByVal iUserId As Integer, ByVal iCoyFieldUnkid As Integer, ByVal iCoyFieldTypeId As Integer) As Boolean
        Dim StrQI, StrQU, StrQ As String
        Dim exForce As Exception
        Dim iCoyInfoFieldId As Integer = 0
        Try
            StrQI = "INSERT INTO hrassess_coyinfofield_tran ( " & _
                        "  coyfieldunkid " & _
                        ", fieldunkid " & _
                        ", field_data " & _
                        ", coyfieldtypeid" & _
                    ") VALUES (" & _
                        "  @coyfieldunkid " & _
                        ", @fieldunkid " & _
                        ", @field_data " & _
                        ", @coyfieldtypeid" & _
                    "); SELECT @@identity"

            StrQU = "UPDATE hrassess_coyinfofield_tran SET " & _
                        "  coyfieldunkid = @coyfieldunkid" & _
                        ", fieldunkid = @fieldunkid" & _
                        ", field_data = @field_data" & _
                        ", coyfieldtypeid = @coyfieldtypeid " & _
                    "WHERE coyinfofieldunkid = @coyinfofieldunkid "

            For Each iKey As Integer In mdicInfoField.Keys
                objDataOpr.ClearParameters()
                StrQ = "SELECT coyinfofieldunkid FROM hrassess_coyinfofield_tran WHERE coyfieldunkid = '" & iCoyFieldUnkid & "' AND coyfieldtypeid = '" & iCoyFieldTypeId & "' AND fieldunkid = '" & iKey & "' "

                Dim dsList As New DataSet
                dsList = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    iCoyInfoFieldId = dsList.Tables(0).Rows(0).Item("coyinfofieldunkid")
                End If

                If iCoyInfoFieldId > 0 Then
                    objDataOpr.AddParameter("@coyinfofieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCoyInfoFieldId.ToString)
                    objDataOpr.AddParameter("@coyfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCoyFieldUnkid.ToString)
                    objDataOpr.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iKey.ToString)
                    objDataOpr.AddParameter("@field_data", SqlDbType.NVarChar, mdicInfoField(iKey).Length, mdicInfoField(iKey).ToString)
                    objDataOpr.AddParameter("@coyfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCoyFieldTypeId.ToString)

                    Call objDataOpr.ExecNonQuery(StrQU)

                    If objDataOpr.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If

                    If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_coyinfofield_tran", iCoyInfoFieldId, "coyinfofieldunkid", 2, objDataOpr) Then
                        If clsCommonATLog.Insert_AtLog(objDataOpr, 2, "hrassess_coyinfofield_tran", "coyinfofieldunkid", iCoyInfoFieldId, , iUserId) = False Then
                            exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                Else
                    objDataOpr.AddParameter("@coyfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCoyFieldUnkid.ToString)
                    objDataOpr.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iKey.ToString)
                    objDataOpr.AddParameter("@field_data", SqlDbType.NVarChar, mdicInfoField(iKey).Length, mdicInfoField(iKey).ToString)
                    objDataOpr.AddParameter("@coyfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCoyFieldTypeId.ToString)

                    dsList = objDataOpr.ExecQuery(StrQI, "List")

                    If objDataOpr.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If

                    mintCoyInfoFieldtranunkid = dsList.Tables(0).Rows(0).Item(0)

                    If clsCommonATLog.Insert_AtLog(objDataOpr, 1, "hrassess_coyinfofield_tran", "coyinfofieldunkid", mintCoyInfoFieldtranunkid, , iUserId) = False Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If

                End If
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertDelete_InfoField", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

End Class
