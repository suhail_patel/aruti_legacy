﻿'************************************************************************************************************************************
'Class Name : clsassess_empstatus_tran.vb
'Purpose    :
'Date       :29-May-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clsassess_empstatus_tran
    Private Const mstrModuleName = "clsassess_empstatus_tran"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintStatustranunkid As Integer = 0
    Private mintEmployeeunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mintStatustypeid As Integer = 0
    Private mintAssessormasterunkid As Integer = 0
    Private mintAssessoremployeeunkid As Integer = 0
    Private mdtStatus_Date As Date = Nothing
    Private mstrCommtents As String = String.Empty
    Private mblnIsunlock As Boolean = False
    Private mintUserunkid As Integer = 0
    Private mintLoginemployeeunkid As Integer = 0

    'S.SANDEEP [05 DEC 2015] -- START
    Private mblnSkipApprovalFlow As Boolean = False
    'S.SANDEEP [05 DEC 2015] -- END

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statustranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statustranunkid() As Integer
        Get
            Return mintStatustranunkid
        End Get
        Set(ByVal value As Integer)
            mintStatustranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statustypeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statustypeid() As Integer
        Get
            Return mintStatustypeid
        End Get
        Set(ByVal value As Integer)
            mintStatustypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessormasterunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Assessormasterunkid() As Integer
        Get
            Return mintAssessormasterunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessormasterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessoremployeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Assessoremployeeunkid() As Integer
        Get
            Return mintAssessoremployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessoremployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set status_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Status_Date() As Date
        Get
            Return mdtStatus_Date
        End Get
        Set(ByVal value As Date)
            mdtStatus_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set commtents
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Commtents() As String
        Get
            Return mstrCommtents
        End Get
        Set(ByVal value As String)
            mstrCommtents = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isunlock
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isunlock() As Boolean
        Get
            Return mblnIsunlock
        End Get
        Set(ByVal value As Boolean)
            mblnIsunlock = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property


    'S.SANDEEP [05 DEC 2015] -- START
    Public WriteOnly Property _SkipApprovalFlow() As Boolean
        Set(ByVal value As Boolean)
            mblnSkipApprovalFlow = value
        End Set
    End Property
    'S.SANDEEP [05 DEC 2015] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  statustranunkid " & _
              ", employeeunkid " & _
              ", periodunkid " & _
              ", statustypeid " & _
              ", assessormasterunkid " & _
              ", assessoremployeeunkid " & _
              ", status_date " & _
              ", commtents " & _
              ", isunlock " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", ISNULL(isapprovalskipped,0) AS isapprovalskipped " & _
             "FROM hrassess_empstatus_tran " & _
             "WHERE statustranunkid = @statustranunkid "
            'S.SANDEEP [05 DEC 2015] -- START {isapprovalskipped} -- END

            objDataOperation.AddParameter("@statustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatustranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintStatustranunkid = CInt(dtRow.Item("statustranunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintStatustypeid = CInt(dtRow.Item("statustypeid"))
                mintAssessormasterunkid = CInt(dtRow.Item("assessormasterunkid"))
                mintAssessoremployeeunkid = CInt(dtRow.Item("assessoremployeeunkid"))
                mdtStatus_Date = dtRow.Item("status_date")
                mstrCommtents = dtRow.Item("commtents").ToString
                mblnIsunlock = CBool(dtRow.Item("isunlock"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))

                'S.SANDEEP [05 DEC 2015] -- START
                mblnSkipApprovalFlow = CBool(dtRow.Item("isapprovalskipped"))
                'S.SANDEEP [05 DEC 2015] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isStatusExists(ByVal iEmpUnkid As Integer, _
                                   ByVal iPeriodUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim blnFlag As Boolean = False
        Try
            strQ = "SELECT TOP 1 " & _
                   "  statustranunkid " & _
                   ", employeeunkid " & _
                   ", yearunkid " & _
                   ", periodunkid " & _
                   ", statustypeid " & _
                   ", assessormasterunkid " & _
                   ", assessoremployeeunkid " & _
                   ", status_date " & _
                   ", commtents " & _
                   "FROM hrassess_empstatus_tran WITH (NOLOCK) " & _
                   "WHERE employeeunkid = @employeeunkid " & _
                   " AND periodunkid = @periodunkid " & _
                   "ORDER BY statustranunkid DESC "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpUnkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Select Case CInt(dsList.Tables(0).Rows(0).Item("statustypeid"))
                    Case enObjective_Status.SUBMIT_APPROVAL, enObjective_Status.FINAL_SAVE
                        blnFlag = True
                End Select
            End If

            Return blnFlag

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isStatusExists", mstrModuleName)
            Return True
        End Try
    End Function

    Public Function isExist(ByVal iEmpUnkid As Integer, _
                            ByVal iPeriodUnkid As Integer, _
                            ByVal iStatusId As Integer, _
                            Optional ByVal iDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Dim objDataOperation As clsDataOperation
        If iDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = iDataOpr
        End If
        Try
            strQ = "SELECT TOP 1 " & _
                   "  statustranunkid " & _
                   ", employeeunkid " & _
                   ", periodunkid " & _
                   ", statustypeid " & _
                   ", assessormasterunkid " & _
                   ", assessoremployeeunkid " & _
                   ", status_date " & _
                   ", commtents " & _
                   "FROM hrassess_empstatus_tran WITH (NOLOCK) " & _
                   "WHERE employeeunkid = @employeeunkid " & _
                   " AND periodunkid = @periodunkid " & _
                   " ORDER BY statustranunkid DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpUnkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                If dsList.Tables(0).Rows(0).Item("statustypeid") = iStatusId Then
                    blnFlag = True
                Else
                    blnFlag = False
                End If
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_empstatus_tran) </purpose>
    Public Function Insert(Optional ByVal iDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal iSelfAssignment As Boolean = False, _
                           Optional ByVal iVoidProgress As Boolean = False) As Boolean 'S.SANDEEP [29 JAN 2015] -- START {iSelfAssignment} -- END
        'S.SANDEEP |09-JUL-2019| -- START {iVoidProgress} -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If iDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = iDataOpr
            objDataOperation.ClearParameters()
        End If
        If isExist(mintEmployeeunkid, mintPeriodunkid, mintStatustypeid, iDataOpr) Then
            Return True
        End If
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@statustypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatustypeid.ToString)
            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessormasterunkid.ToString)
            objDataOperation.AddParameter("@assessoremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessoremployeeunkid.ToString)
            objDataOperation.AddParameter("@status_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStatus_Date.ToString)

            'Shani(06-Feb-2016) -- Start
            'PA Changes Given By Glory for CCBRT
            'objDataOperation.AddParameter("@commtents", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrCommtents.ToString)
            objDataOperation.AddParameter("@commtents", SqlDbType.NText, mstrCommtents.Length, mstrCommtents.ToString)
            'Shani(06-Feb-2016) -- End

            objDataOperation.AddParameter("@isunlock", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsunlock.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            'S.SANDEEP [05 DEC 2015] -- START
            objDataOperation.AddParameter("@isapprovalskipped", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnSkipApprovalFlow)
            'S.SANDEEP [05 DEC 2015] -- END


            strQ = "INSERT INTO hrassess_empstatus_tran ( " & _
                       "  employeeunkid " & _
                       ", periodunkid " & _
                       ", statustypeid " & _
                       ", assessormasterunkid " & _
                       ", assessoremployeeunkid " & _
                       ", status_date " & _
                       ", commtents " & _
                       ", isunlock " & _
                       ", userunkid " & _
                       ", loginemployeeunkid" & _
                       ", isapprovalskipped " & _
                   ") VALUES (" & _
                       "  @employeeunkid " & _
                       ", @periodunkid " & _
                       ", @statustypeid " & _
                       ", @assessormasterunkid " & _
                       ", @assessoremployeeunkid " & _
                       ", @status_date " & _
                       ", @commtents " & _
                       ", @isunlock " & _
                       ", @userunkid " & _
                       ", @loginemployeeunkid" & _
                       ", @isapprovalskipped " & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintStatustranunkid = dsList.Tables(0).Rows(0).Item(0)

            If mintStatustypeid = enObjective_Status.FINAL_SAVE Then
                strQ = "UPDATE hrassess_empfield1_master SET isfinal = 1 WHERE employeeunkid = '" & mintEmployeeunkid & "' AND periodunkid = '" & mintPeriodunkid & "'; "
                'S.SANDEEP [29 JAN 2015] -- START
                If iSelfAssignment = True Then
                    strQ &= "UPDATE hrassess_competence_assign_master SET isfinal = 1 WHERE employeeunkid = '" & mintEmployeeunkid & "' AND periodunkid = '" & mintPeriodunkid & "'; "
                End If
                'S.SANDEEP [29 JAN 2015] -- END

                'S.SANDEEP [23 DEC 2015] -- START

                'S.SANDEEP [28 DEC 2015] -- START
                'strQ = "UPDATE hrassess_plan_customitem_tran SET isfinal = 1 WHERE employeeunkid = '" & mintEmployeeunkid & "' AND periodunkid = '" & mintPeriodunkid & "' "
                strQ &= "UPDATE hrassess_plan_customitem_tran SET isfinal = 1 WHERE employeeunkid = '" & mintEmployeeunkid & "' AND periodunkid = '" & mintPeriodunkid & "' "
                'S.SANDEEP [28 DEC 2015] -- END

                'S.SANDEEP [23 DEC 2015] -- END

                objDataOperation.ExecNonQuery(strQ)
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            ElseIf (mintStatustypeid = enObjective_Status.OPEN_CHANGES Or mintStatustypeid = enObjective_Status.PERIODIC_REVIEW) Then
                strQ = "UPDATE hrassess_empfield1_master SET isfinal = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' AND periodunkid = '" & mintPeriodunkid & "'; "
                'S.SANDEEP [29 JAN 2015] -- START
                If iSelfAssignment = True Then
                    strQ &= "UPDATE hrassess_competence_assign_master SET isfinal = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' AND periodunkid = '" & mintPeriodunkid & "'; "
                End If
                'S.SANDEEP [29 JAN 2015] -- END

                'S.SANDEEP [23 DEC 2015] -- START

                'S.SANDEEP [28 DEC 2015] -- START
                'strQ = "UPDATE hrassess_plan_customitem_tran SET isfinal = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' AND periodunkid = '" & mintPeriodunkid & "' "
                strQ &= "UPDATE hrassess_plan_customitem_tran SET isfinal = 0 WHERE employeeunkid = '" & mintEmployeeunkid & "' AND periodunkid = '" & mintPeriodunkid & "' "
                'S.SANDEEP [28 DEC 2015] -- END

                'S.SANDEEP [23 DEC 2015] -- END

                'S.SANDEEP |09-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : PA CHANGES
                If iVoidProgress Then
                    strQ &= "UPDATE hrassess_empupdate_tran SET isvoid = 1,voiduserunkid = '" & mintUserunkid & "',voiddatetime = GETDATE(),voidreason='" & mstrCommtents.Replace("'", "''") & "' WHERE employeeunkid = '" & mintEmployeeunkid & "' AND periodunkid = '" & mintPeriodunkid & "' "
                End If
                'S.SANDEEP |09-JUL-2019| -- END

                objDataOperation.ExecNonQuery(strQ)
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_empstatus_tran", "statustranunkid", mintStatustranunkid, , mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function Get_Last_StatusId(ByVal xEmployeeId As Integer, ByVal xPeriodId As Integer, ByRef xStatusTranId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer
        Dim StrQ As String = String.Empty
        Dim xLastStatusId As Integer = 0
        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        End If
        Try
            StrQ = "SELECT TOP 1 @LstId = statustypeid, @StatTranId = statustranunkid FROM hrassess_empstatus_tran WITH (NOLOCK) " & _
                   "WHERE employeeunkid = '" & xEmployeeId & "' AND periodunkid = '" & xPeriodId & "' ORDER BY statustranunkid DESC "

            objDataOperation.AddParameter("@LstId", SqlDbType.Int, eZeeDataType.INT_SIZE, xLastStatusId, ParameterDirection.InputOutput)
            objDataOperation.AddParameter("@StatTranId", SqlDbType.Int, eZeeDataType.INT_SIZE, xStatusTranId, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            xLastStatusId = objDataOperation.GetParameterValue("@LstId")
            xStatusTranId = objDataOperation.GetParameterValue("@StatTranId")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Last_StatusId", mstrModuleName)
        Finally
        End Try
        Return xLastStatusId
    End Function

End Class
