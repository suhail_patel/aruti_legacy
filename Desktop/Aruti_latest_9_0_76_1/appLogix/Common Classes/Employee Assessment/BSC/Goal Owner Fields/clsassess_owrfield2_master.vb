﻿'************************************************************************************************************************************
'Class Name :clsassess_owrfield2_master.vb
'Purpose    :
'Date       :09-May-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_owrfield2_master
    Private Shared ReadOnly mstrModuleName As String = "clsassess_owrfield2_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintOwrfield2unkid As Integer = 0
    Private mintOwrfield1unkid As Integer = 0
    Private mintFieldunkid As Integer = 0
    Private mstrField_Data As String = String.Empty
    Private mdblWeight As Double = 0
    Private mdtStartdate As Date = Nothing
    Private mdtEnddate As Date = Nothing
    Private mintStatusunkid As Integer = 0
    Private mintUserunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mstrVoidreason As String = String.Empty
    Private mdtVoiddatetime As Date = Nothing
    Private mintOwrFieldTypeId As Integer = 0
    Private mDecPct_Completed As Decimal = 0
    Private mintPeriodunkid As Integer = 0
    Private mintOwnerunkid As Integer = 0

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set owrfield2unkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Owrfield2unkid() As Integer
        Get
            Return mintOwrfield2unkid
        End Get
        Set(ByVal value As Integer)
            mintOwrfield2unkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set owrfield1unkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Owrfield1unkid() As Integer
        Get
            Return mintOwrfield1unkid
        End Get
        Set(ByVal value As Integer)
            mintOwrfield1unkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fieldunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Fieldunkid() As Integer
        Get
            Return mintFieldunkid
        End Get
        Set(ByVal value As Integer)
            mintFieldunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set field_data
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Field_Data() As String
        Get
            Return mstrField_Data
        End Get
        Set(ByVal value As String)
            mstrField_Data = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set weight
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Weight() As Double
        Get
            Return mdblWeight
        End Get
        Set(ByVal value As Double)
            mdblWeight = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Startdate() As Date
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As Date)
            mdtStartdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enddate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Enddate() As Date
        Get
            Return mdtEnddate
        End Get
        Set(ByVal value As Date)
            mdtEnddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set Owrfieldtypeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _OwrFieldTypeId() As Integer
        Set(ByVal value As Integer)
            mintOwrFieldTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pct_completed
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Pct_Completed() As Decimal
        Get
            Return mDecPct_Completed
        End Get
        Set(ByVal value As Decimal)
            mDecPct_Completed = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set Periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set Ownerunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ownerunkid() As Integer
        Get
            Return mintOwnerunkid
        End Get
        Set(ByVal value As Integer)
            mintOwnerunkid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  owrfield2unkid " & _
              ", owrfield1unkid " & _
              ", fieldunkid " & _
              ", field_data " & _
              ", weight " & _
              ", startdate " & _
              ", enddate " & _
              ", statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
              ", pct_completed " & _
              ", ownerunkid " & _
              ", periodunkid " & _
             "FROM hrassess_owrfield2_master " & _
             "WHERE owrfield2unkid = @owrfield2unkid "

            objDataOperation.AddParameter("@owrfield2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwrfield2unkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintOwrfield2unkid = CInt(dtRow.Item("owrfield2unkid"))
                mintOwrfield1unkid = CInt(dtRow.Item("owrfield1unkid"))
                mintFieldunkid = CInt(dtRow.Item("fieldunkid"))
                mstrField_Data = dtRow.Item("field_data").ToString
                mdblWeight = CDbl(dtRow.Item("weight"))
                If IsDBNull(dtRow.Item("startdate")) = False Then
                    mdtStartdate = dtRow.Item("startdate")
                Else
                    mdtStartdate = Nothing
                End If
                If IsDBNull(dtRow.Item("enddate")) = False Then
                    mdtEnddate = dtRow.Item("enddate")
                Else
                    mdtEnddate = Nothing
                End If
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mDecPct_Completed = dtRow.Item("pct_completed")
                mintOwnerunkid = CInt(dtRow.Item("ownerunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                Exit For
            Next

            'S.SANDEEP [16 JUN 2015] -- START
            Dim dsUpdate As DataSet
            Dim objProgress As New clsassess_owrupdate_tran
            objProgress._DataOperation = objDataOperation
            dsUpdate = objProgress.GetLatestProgress(ConfigParameter._Object._CurrentDateAndTime, mintOwnerunkid, mintPeriodunkid)
            If dsUpdate IsNot Nothing Then
                Dim pRow() As DataRow = dsUpdate.Tables(0).Select("owrfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD2 & "' AND owrfieldunkid = '" & mintOwrfield2unkid & "'")
                If pRow.Length > 0 Then
                    mDecPct_Completed = pRow(0).Item("pct_completed")
                    mintStatusunkid = pRow(0).Item("statusunkid")
                End If
            End If
            objProgress = Nothing
            'S.SANDEEP [16 JUN 2015] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  owrfield2unkid " & _
              ", owrfield1unkid " & _
              ", ownerunkid " & _
              ", periodunkid " & _
              ", fieldunkid " & _
              ", field_data " & _
              ", weight " & _
              ", startdate " & _
              ", enddate " & _
              ", statusunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
              ", pct_completed " & _
             "FROM hrassess_owrfield2_master "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_owrfield2_master) </purpose>
    Public Function Insert(Optional ByVal mdicInfoFieldData As Dictionary(Of Integer, String) = Nothing, Optional ByVal mdtOwner As DataTable = Nothing) As Boolean
        If isExist(mstrField_Data, , mintOwrfield1unkid, mintPeriodunkid, mintOwnerunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@owrfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwrfield1unkid.ToString)
            objDataOperation.AddParameter("@ownerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwnerunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)

            'Shani(06-Feb-2016) -- Start
            'PA Changes Given By Glory for CCBRT
            'objDataOperation.AddParameter("@field_data", SqlDbType.NText, mstrField_Data.Length, mstrField_Data.ToString)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, mstrField_Data.ToString)
            'Shani(06-Feb-2016) -- End

            objDataOperation.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblWeight.ToString)
            If mdtStartdate <> Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@pct_completed", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecPct_Completed.ToString)

            strQ = "INSERT INTO hrassess_owrfield2_master ( " & _
                       "  owrfield1unkid " & _
                       ", ownerunkid " & _
                       ", periodunkid " & _
                       ", fieldunkid " & _
                       ", field_data " & _
                       ", weight " & _
                       ", startdate " & _
                       ", enddate " & _
                       ", statusunkid " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidreason " & _
                       ", voiddatetime " & _
                       ", pct_completed" & _
                   ") VALUES (" & _
                       "  @owrfield1unkid " & _
                       ", @ownerunkid " & _
                       ", @periodunkid " & _
                       ", @fieldunkid " & _
                       ", @field_data " & _
                       ", @weight " & _
                       ", @startdate " & _
                       ", @enddate " & _
                       ", @statusunkid " & _
                       ", @userunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voidreason " & _
                       ", @voiddatetime" & _
                       ", @pct_completed" & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintOwrfield2unkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_owrfield2_master", "owrfield2unkid", mintOwrfield2unkid, , mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtOwner IsNot Nothing Then
                Dim objOwner As New clsassess_owrowner_tran
                objOwner._DatTable = mdtOwner.Copy
                If objOwner.InsertDelete_Owners(objDataOperation, mintUserunkid, mintOwrfield2unkid, mintOwrFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objOwner = Nothing
            End If

            If mdicInfoFieldData IsNot Nothing Then
                Dim objInfoField As New clsassess_owrinfofield_tran
                objInfoField._dicInfoField = mdicInfoFieldData
                If objInfoField.InsertDelete_InfoField(objDataOperation, mintUserunkid, mintOwrfield2unkid, mintOwrFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objInfoField = Nothing
            End If

            'S.SANDEEP [16 JUN 2015] -- START
            Dim objUpdateProgress As New clsassess_owrupdate_tran
            objUpdateProgress._DataOperation = objDataOperation
            objUpdateProgress._Fieldunkid = mintFieldunkid
            objUpdateProgress._Isvoid = False
            objUpdateProgress._Ownerunkid = mintOwnerunkid
            objUpdateProgress._Owrfieldunkid = mintOwrfield2unkid
            objUpdateProgress._Owrfieldtypeid = mintOwrFieldTypeId
            objUpdateProgress._Pct_Completed = mDecPct_Completed
            objUpdateProgress._Periodunkid = mintPeriodunkid
            objUpdateProgress._Remark = ""
            objUpdateProgress._Statusunkid = mintStatusunkid
            objUpdateProgress._Updatedate = ConfigParameter._Object._CurrentDateAndTime
            objUpdateProgress._Userunkid = mintUserunkid
            objUpdateProgress._Voiddatetime = Nothing
            objUpdateProgress._Voidreason = ""
            objUpdateProgress._Voiduserunkid = -1

            If objUpdateProgress.Insert() = False Then
                If objUpdateProgress._Message <> "" Then
                    mstrMessage = objUpdateProgress._Message & " " & Language.getMessage(mstrModuleName, 28, "You can track the changes by clicking Update Progress link from the list.")
                    Exit Try
                Else
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objUpdateProgress = Nothing
            'S.SANDEEP [16 JUN 2015] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_owrfield2_master) </purpose>
    Public Function Update(Optional ByVal mdicInfoFieldData As Dictionary(Of Integer, String) = Nothing, Optional ByVal mdtOwner As DataTable = Nothing) As Boolean
        If isExist(mstrField_Data, mintOwrfield2unkid, mintOwrfield1unkid, mintPeriodunkid, mintOwnerunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@owrfield2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwrfield2unkid.ToString)
            objDataOperation.AddParameter("@owrfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwrfield1unkid.ToString)
            objDataOperation.AddParameter("@ownerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwnerunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)

            'Shani(06-Feb-2016) -- Start
            'PA Changes Given By Glory for CCBRT
            'objDataOperation.AddParameter("@field_data", SqlDbType.NText, mstrField_Data.Length, mstrField_Data.ToString)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, mstrField_Data.ToString)
            'Shani(06-Feb-2016) -- End

            objDataOperation.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblWeight.ToString)
            If mdtStartdate <> Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@pct_completed", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecPct_Completed.ToString)

            strQ = "UPDATE hrassess_owrfield2_master SET " & _
                   "  owrfield1unkid = @owrfield1unkid" & _
                   ", ownerunkid = @ownerunkid" & _
                   ", periodunkid = @periodunkid" & _
                   ", fieldunkid = @fieldunkid" & _
                   ", field_data = @field_data" & _
                   ", weight = @weight" & _
                   ", startdate = @startdate" & _
                   ", enddate = @enddate" & _
                   ", statusunkid = @statusunkid" & _
                   ", userunkid = @userunkid" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voidreason = @voidreason" & _
                   ", voiddatetime = @voiddatetime " & _
                   ", pct_completed = @pct_completed " & _
                   "WHERE owrfield2unkid = @owrfield2unkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_owrfield2_master", mintOwrfield2unkid, "owrfield2unkid", 2, objDataOperation) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_owrfield2_master", "owrfield2unkid", mintOwrfield2unkid, , mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If mdtOwner IsNot Nothing Then
                Dim objOwner As New clsassess_owrowner_tran
                objOwner._DatTable = mdtOwner.Copy
                If objOwner.InsertDelete_Owners(objDataOperation, mintUserunkid, mintOwrfield2unkid, mintOwrFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objOwner = Nothing
            End If

            If mdicInfoFieldData IsNot Nothing Then
                Dim objInfoField As New clsassess_owrinfofield_tran
                objInfoField._dicInfoField = mdicInfoFieldData
                If objInfoField.InsertDelete_InfoField(objDataOperation, mintUserunkid, mintOwrfield2unkid, mintOwrFieldTypeId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objInfoField = Nothing
            End If

            'S.SANDEEP [16 JUN 2015] -- START

            'S.SANDEEP [27 Jan 2016] -- START
            'Dim objUpdateProgress As New clsassess_owrupdate_tran
            'objUpdateProgress._DataOperation = objDataOperation
            'objUpdateProgress._Fieldunkid = mintFieldunkid
            'objUpdateProgress._Isvoid = False
            'objUpdateProgress._Ownerunkid = mintOwnerunkid
            'objUpdateProgress._Owrfieldunkid = mintOwrfield2unkid
            'objUpdateProgress._Owrfieldtypeid = mintOwrFieldTypeId
            'objUpdateProgress._Pct_Completed = mDecPct_Completed
            'objUpdateProgress._Periodunkid = mintPeriodunkid
            'objUpdateProgress._Remark = ""
            'objUpdateProgress._Statusunkid = mintStatusunkid
            'objUpdateProgress._Updatedate = ConfigParameter._Object._CurrentDateAndTime
            'objUpdateProgress._Userunkid = mintUserunkid
            'objUpdateProgress._Voiddatetime = Nothing
            'objUpdateProgress._Voidreason = ""
            'objUpdateProgress._Voiduserunkid = -1

            'If objUpdateProgress.Insert() = False Then
            '    If objUpdateProgress._Message <> "" Then
            '        mstrMessage = objUpdateProgress._Message & " " & Language.getMessage(mstrModuleName, 28, "You can track the changes by clicking Update Progress link from the list.")
            '        Exit Try
            '    Else
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If

            'objUpdateProgress = Nothing
            'S.SANDEEP [27 Jan 2016] -- END

            'S.SANDEEP [16 JUN 2015] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_coyfield1_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal iDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objOwrField3 As New clsassess_owrfield3_master

        If iDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = iDataOpr
        End If

        Try
            strQ = "UPDATE hrassess_owrfield2_master SET " & _
                   "  isvoid = @isvoid " & _
                   ", voiduserunkid = @voiduserunkid " & _
                   ", voidreason = @voidreason " & _
                   ", voiddatetime = @voiddatetime " & _
                   "WHERE owrfield2unkid = @owrfield2unkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@owrfield2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Length, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_owrfield2_master", "owrfield2unkid", intUnkid, , mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsChild As New DataSet : Dim dtmp As DataRow() = Nothing
            '==================|START  VOIDING CHILD DATA FIELDS IF MAPPED|==============='
            dsChild = clsCommonATLog.GetChildList(objDataOperation, "hrassess_owrfield3_master", "owrfield2unkid", intUnkid)
            dtmp = dsChild.Tables(0).Select("isvoid = 0")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    objOwrField3._Isvoid = mblnIsvoid
                    objOwrField3._Voiddatetime = mdtVoiddatetime
                    objOwrField3._Voidreason = mstrVoidreason
                    objOwrField3._Voiduserunkid = mintVoiduserunkid
                    If objOwrField3.Delete(dr.Item("owrfield3unkid"), objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If
            '==================|ENDING VOIDING CHILD DATA FIELDS IF MAPPED|==============='

            '==================|START VOIDING INFORMATIONAL FIELDS IF MAPPED|==============='
            dsChild = clsCommonATLog.GetChildList(objDataOperation, "hrassess_owrinfofield_tran", "owrfieldunkid", intUnkid)
            dtmp = dsChild.Tables(0).Select("owrfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD2 & "'")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_owrinfofield_tran", "owrinfofieldunkid", dr.Item("owrinfofieldunkid"), , mintVoiduserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
                strQ = "DELETE FROM hrassess_owrinfofield_tran WHERE owrfieldunkid = '" & intUnkid & "' AND owrfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD2 & "' "

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            '==================|ENDING VOIDING INFORMATIONAL FIELDS IF MAPPED|==============='

            'S.SANDEEP [16 JUN 2015] -- START
            '==================|START  VOIDING PROGRESS FIELDS IF MAPPED|==============='
            dsChild = clsCommonATLog.GetChildList(objDataOperation, "hrassess_owrupdate_tran", "owrfieldunkid", intUnkid)
            dtmp = dsChild.Tables(0).Select("owrfieldtypeid = '" & enWeight_Types.WEIGHT_FIELD2 & "'")
            If dtmp.Length > 0 Then
                For Each dr As DataRow In dtmp
                    Dim objProgress As New clsassess_owrupdate_tran

                    objProgress._DataOperation = objDataOperation
                    objProgress._Isvoid = mblnIsvoid
                    objProgress._Voiddatetime = mdtVoiddatetime
                    objProgress._Voidreason = mstrVoidreason
                    objProgress._Voiduserunkid = mintVoiduserunkid

                    If objProgress.Delete(dr.Item("owrupdatetranunkid")) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    objProgress = Nothing
                Next
            End If
            '==================|ENDING  VOIDING PROGRESS FIELDS IF MAPPED|==============='
            'S.SANDEEP [16 JUN 2015] -- END

            If iDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If iDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@coyfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iFieldData As String, Optional ByVal intUnkid As Integer = -1, Optional ByVal iOwnerField1Id As Integer = 0, Optional ByVal iPeriod As Integer = 0, Optional ByVal iOwnerId As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "  hrassess_owrfield2_master.owrfield2unkid " & _
                    ", hrassess_owrfield2_master.owrfield1unkid " & _
                    ", hrassess_owrfield2_master.fieldunkid " & _
                    ", hrassess_owrfield2_master.field_data " & _
                    ", hrassess_owrfield2_master.weight " & _
                    ", hrassess_owrfield2_master.userunkid " & _
                    ", hrassess_owrfield2_master.isvoid " & _
                    ", hrassess_owrfield2_master.voiduserunkid " & _
                    ", hrassess_owrfield2_master.voidreason " & _
                    ", hrassess_owrfield2_master.voiddatetime " & _
                    "FROM hrassess_owrfield2_master " & _
                    " LEFT JOIN hrassess_owrfield1_master ON hrassess_owrfield1_master.owrfield1unkid  = hrassess_owrfield2_master.owrfield1unkid "

            strQ &= "WHERE hrassess_owrfield2_master.field_data = @field_data AND hrassess_owrfield2_master.isvoid = 0 "

            If iOwnerField1Id > 0 Then
                strQ &= "   AND hrassess_owrfield2_master.owrfield1unkid = '" & iOwnerField1Id & "' "
            End If

            If iOwnerId > 0 Then
                strQ &= "   AND hrassess_owrfield2_master.ownerunkid = '" & iOwnerId & "' "
            End If

            If iPeriod > 0 Then
                strQ &= "   AND hrassess_owrfield2_master.periodunkid = '" & iPeriod & "' "
            End If

            If intUnkid > 0 Then
                strQ &= " AND hrassess_owrfield2_master.owrfield2unkid <> @owrfield2unkid"
            End If


            'Shani(06-Feb-2016) -- Start
            'PA Changes Given By Glory for CCBRT
            'objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, iFieldData)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, iFieldData.Length, iFieldData)
            'Shani(06-Feb-2016) -- End

            objDataOperation.AddParameter("@owrfield2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Get Data Set for Combo </purpose>
    Public Function getComboList(ByVal iOwnerId As Integer, ByVal iPeriodId As Integer, Optional ByVal iParentId As Integer = 0, Optional ByVal iList As String = "List", Optional ByVal iAddSelect As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            If iAddSelect = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            StrQ &= "SELECT owrfield2unkid AS Id, field_data AS Name FROM hrassess_owrfield2_master WHERE isvoid = 0 " & _
                    " AND hrassess_owrfield2_master.ownerunkid = '" & iOwnerId & "' AND hrassess_owrfield2_master.periodunkid = '" & iPeriodId & "' "

            If iParentId > 0 Then
                StrQ &= " AND hrassess_owrfield2_master.owrfield1unkid = '" & iParentId & "' "
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        Finally
            objDataOperation = Nothing
            dsList.Dispose()
        End Try
    End Function

    Public Function Get_OwnerField2Unkid(ByVal iFieldData As String, ByVal iOwnerField1Id As Integer, ByVal iPeriodId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                    "  owrfield2unkid " & _
                    "FROM hrassess_owrfield2_master " & _
                    " JOIN hrassess_owrfield1_master ON hrassess_owrfield1_master.owrfield1unkid  = hrassess_owrfield2_master.owrfield1unkid " & _
                    "WHERE hrassess_owrfield2_master.field_data = @field_data AND hrassess_owrfield2_master.isvoid = 0 " & _
                    "   AND hrassess_owrfield1_master.owrfield1unkid = @owrfield1unkid AND hrassess_owrfield1_master.periodunkid = @periodunkid "

            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, iFieldData)
            objDataOperation.AddParameter("@owrfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iOwnerField1Id)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0).Item("owrfield2unkid")
            Else
                Return 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_OwnerField2Unkid", mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP [ 10 JAN 2015 ] -- START
    Public Function GetListForTansfer(ByVal xPeriodId As Integer) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT " & _
                       "     owrfield2unkid " & _
                       "    ,owrfield1unkid " & _
                       "    ,ownerunkid " & _
                       "    ,periodunkid " & _
                       "    ,fieldunkid " & _
                       "    ,field_data " & _
                       "    ,weight " & _
                       "    ,userunkid " & _
                       "    ,isvoid " & _
                       "    ,voiduserunkid " & _
                       "    ,voidreason " & _
                       "    ,voiddatetime " & _
                       "    ,startdate " & _
                       "    ,enddate " & _
                       "    ,statusunkid " & _
                       "    ,pct_completed " & _
                       "FROM hrassess_owrfield2_master " & _
                       "WHERE isvoid = 0 AND periodunkid = '" & xPeriodId & "' ORDER BY owrfield2unkid "

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetListForTansfer", mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function GetOwrFieldUnkid(ByVal xFieldData As String, ByVal xPeriodId As Integer, ByVal xOwnerUnkid As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim xUnkid As Integer = -1
        Try
            Using objDo As New clsDataOperation
                StrQ = "SELECT @xUnkid = owrfield2unkid FROM hrassess_owrfield2_master WHERE isvoid = 0 " & _
                       " AND periodunkid = @xPeriodId AND field_data = @xFieldData AND ownerunkid = @xOwnerUnkid "

                objDo.AddParameter("@xUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUnkid, ParameterDirection.InputOutput)
                objDo.AddParameter("@xPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
                objDo.AddParameter("@xFieldData", SqlDbType.NVarChar, xFieldData.Length, xFieldData)
                objDo.AddParameter("@xOwnerUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xOwnerUnkid)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                xUnkid = objDo.GetParameterValue("@xUnkid")

            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetOwrFieldUnkid", mstrModuleName)
        Finally
        End Try
        Return xUnkid
    End Function
    'S.SANDEEP [ 10 JAN 2015 ] -- END


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, this data is already defined. Please define new data.")
            Language.setMessage(mstrModuleName, 2, "Select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
