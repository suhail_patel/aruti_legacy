﻿'************************************************************************************************************************************
'Class Name : clsAssess_Field_Mapping.vb
'Purpose    :
'Date       :15-May-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsAssess_Field_Mapping
    Private Shared ReadOnly mstrModuleName As String = "clsAssess_Field_Mapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintMappingunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mDecWeight As Decimal = 0
    Private mintFieldunkid As Integer = 0
    Private mintUserunkid As Integer = 0
    Private mblnIsactive As Boolean = True

#End Region

#Region " Enums "

    Public Enum enWeightCheckType
        CKT_COMPANY_LEVEL = 1
        CKT_ALLOCATION_LEVEL = 2
        CKT_EMPLOYEE_LEVEL = 3
    End Enum

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set weight
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Weight() As Decimal
        Get
            Return mDecWeight
        End Get
        Set(ByVal value As Decimal)
            mDecWeight = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fieldunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Fieldunkid() As Integer
        Get
            Return mintFieldunkid
        End Get
        Set(ByVal value As Integer)
            mintFieldunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  hrassess_field_mapping.mappingunkid " & _
                   ", hrassess_field_mapping.periodunkid " & _
                   ", hrassess_field_mapping.weight " & _
                   ", hrassess_field_mapping.fieldunkid " & _
                   ", hrassess_field_mapping.userunkid " & _
                   ", hrassess_field_mapping.isactive " & _
                   "FROM hrassess_field_mapping " & _
                   " JOIN cfcommon_period_tran ON hrassess_field_mapping.periodunkid = cfcommon_period_tran.periodunkid " & _
                   "WHERE mappingunkid = @mappingunkid "

            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintMappingunkid = CInt(dtRow.Item("mappingunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mDecWeight = CDec(dtRow.Item("weight"))
                mintFieldunkid = CInt(dtRow.Item("fieldunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  period_name AS fperiod " & _
                   " ,fieldcaption AS fcaption " & _
                   " ,(SELECT STUFF((SELECT ',' + CAST(hrassess_field_master.fieldcaption AS NVARCHAR(MAX)) FROM hrassess_field_master WHERE isused = 1 AND isinformational = 1 ORDER BY fieldcaption FOR XML PATH('')),1,1,'')) AS finfofield " & _
                   " ,hrassess_field_mapping.mappingunkid " & _
                   " ,hrassess_field_mapping.periodunkid " & _
                   " ,hrassess_field_mapping.weight " & _
                   " ,hrassess_field_mapping.fieldunkid " & _
                   " ,hrassess_field_mapping.userunkid " & _
                   " ,hrassess_field_mapping.isactive " & _
                   " ,cfcommon_period_tran.yearunkid " & _
                   " ,cfcommon_period_tran.statusid " & _
                   "FROM hrassess_field_mapping WITH (NOLOCK) " & _
                   " JOIN hrassess_field_master WITH (NOLOCK) ON hrassess_field_mapping.fieldunkid = hrassess_field_master.fieldunkid " & _
                   " JOIN cfcommon_period_tran WITH (NOLOCK) ON hrassess_field_mapping.periodunkid = cfcommon_period_tran.periodunkid " & _
                   "WHERE 1 = 1 "

            If blnOnlyActive Then
                strQ &= " AND hrassess_field_mapping.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_field_mapping) </purpose>
    Public Function Insert() As Boolean
        If isExist(mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Setting already defined for the selected period. Please define new setting.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecWeight.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "INSERT INTO hrassess_field_mapping ( " & _
              "  periodunkid " & _
              ", weight " & _
              ", fieldunkid " & _
              ", userunkid " & _
              ", isactive" & _
            ") VALUES (" & _
              "  @periodunkid " & _
              ", @weight " & _
              ", @fieldunkid " & _
              ", @userunkid " & _
              ", @isactive" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintMappingunkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_field_mapping", "mappingunkid", mintMappingunkid, , mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_field_mapping) </purpose>
    Public Function Update() As Boolean
        If isExist(mintPeriodunkid, mintMappingunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Setting already defined for the selected period. Please define new setting.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecWeight.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "UPDATE hrassess_field_mapping SET " & _
                   "  periodunkid = @periodunkid" & _
                   ", weight = @weight" & _
                   ", fieldunkid = @fieldunkid" & _
                   ", userunkid = @userunkid" & _
                   ", isactive = @isactive " & _
                   "WHERE mappingunkid = @mappingunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_field_mapping", mintMappingunkid, "mappingunkid", 2, objDataOperation) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_field_mapping", "mappingunkid", mintMappingunkid, , mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_field_mapping) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal iUserId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrassess_field_mapping SET isactive = 0 " & _
                   "WHERE mappingunkid = @mappingunkid "

            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_field_mapping", "mappingunkid", intUnkid, , iUserId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer, Optional ByVal blnIsEdit As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If blnIsEdit = False Then
                strQ = "SELECT 1 FROM hrassess_scalemapping_tran WHERE mappingunkid = @mappingunkid "

                objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Else
                strQ = "SELECT " & _
                       " @fieldunkid = fieldunkid " & _
                       ",@periodunkid = periodunkid " & _
                       "FROM hrassess_field_mapping WHERE mappingunkid = '" & intUnkid & "' "

                Dim iFieldId, iPeriodId As Integer : iFieldId = 0 : iPeriodId = 0

                objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iFieldId, ParameterDirection.InputOutput)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId, ParameterDirection.InputOutput)

                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                iFieldId = CInt(objDataOperation.GetParameterValue("@fieldunkid"))
                iPeriodId = CInt(objDataOperation.GetParameterValue("@periodunkid"))

                Dim xQRY As String = String.Empty

                strQ = "SELECT @QRY = @QRY + 'SELECT 1 FROM ' + TABLE_NAME + ' WHERE ' + COLUMN_NAME + ' = ''" & iFieldId & "'' AND periodunkid = ''" & iPeriodId & "'' AND isvoid = 0 UNION ' + CHAR(13) " & _
                       "FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'fieldunkid' AND TABLE_NAME NOT IN('hrassess_field_master','hrassess_field_mapping','hrassess_periodic_review') " & _
                       "AND TABLE_NAME NOT LIKE '%info%' ORDER BY TABLE_NAME "
                objDataOperation.AddParameter("@QRY", SqlDbType.NVarChar, 2000, xQRY, ParameterDirection.InputOutput)

                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                xQRY = CStr(objDataOperation.GetParameterValue("@QRY"))

                xQRY = xQRY.Substring(0, Len(xQRY) - 7) '-- REMOVE LAST UNION FROM QUERY

                dsList = objDataOperation.ExecQuery(xQRY, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iperiodunkid As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "  mappingunkid " & _
                    ", periodunkid " & _
                    ", weight " & _
                    ", fieldunkid " & _
                    ", userunkid " & _
                    ", isactive " & _
                    "FROM hrassess_field_mapping " & _
                    "WHERE periodunkid = @periodunkid " & _
                    "AND isactive = 1 "

            If intUnkid > 0 Then
                strQ &= " AND mappingunkid <> @mappingunkid"
            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iperiodunkid)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Get_Map_FieldId(ByVal iPeriodId As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim iLinkedFieldId As Integer = -1
        Dim dsList As New DataSet
        objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT fieldunkid FROM hrassess_field_mapping WITH (NOLOCK) where periodunkid = @periodunkid AND isactive = 1 "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                iLinkedFieldId = dsList.Tables(0).Rows(0).Item("fieldunkid")
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Map_FieldId", mstrModuleName)
        Finally
        End Try
        Return iLinkedFieldId
    End Function

    Public Function Get_MappingUnkId(ByVal iPeriodId As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim iLinkedFieldId As Integer = -1
        Dim dsList As New DataSet
        objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT mappingunkid FROM hrassess_field_mapping WITH (NOLOCK) where periodunkid = @periodunkid AND isactive = 1 "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                iLinkedFieldId = dsList.Tables(0).Rows(0).Item("mappingunkid")
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Map_FieldId", mstrModuleName)
        Finally
        End Try
        Return iLinkedFieldId
    End Function

    Public Function Get_Map_FieldName(ByVal iPeriodId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim iLinkedFieldName As String = String.Empty
        Dim dsList As New DataSet
        objDataOperation = New clsDataOperation
        Try
            StrQ = "SELECT fieldcaption FROM hrassess_field_mapping WITH (NOLOCK) JOIN hrassess_field_master WITH (NOLOCK) ON hrassess_field_mapping.fieldunkid = hrassess_field_master.fieldunkid WHERE periodunkid = @periodunkid AND isactive = 1"

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                iLinkedFieldName = dsList.Tables(0).Rows(0).Item("fieldcaption")
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Map_FieldId", mstrModuleName)
        Finally
        End Try
        Return iLinkedFieldName
    End Function

    Public Function IsValid_Date(ByVal iLinkedFieldId As Integer, ByVal xDate1 As Date, ByVal xDate2 As Date, ByVal iChkType As enWeightCheckType, ByVal iParentId As Integer) As String
        Dim iMsg As String = String.Empty
        Try
            If iParentId > 0 Then
                Dim objFMst As New clsAssess_Field_Master
                Dim iExOrdr As Integer = -1
                iExOrdr = objFMst.Get_Field_ExOrder(iLinkedFieldId)
                Select Case iChkType
                    Case enWeightCheckType.CKT_COMPANY_LEVEL
                        Select Case iExOrdr
                            Case enWeight_Types.WEIGHT_FIELD1
                                Dim objCoy1 As New clsassess_coyfield1_master
                                objCoy1._Coyfield1unkid = iParentId
                                If objCoy1._Startdate <> Nothing AndAlso objCoy1._Enddate <> Nothing Then
                                    If xDate1.Date > objCoy1._Enddate.Date Or xDate1.Date < objCoy1._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 10, "Sorry, Start Date between") & " " & _
                                        objCoy1._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objCoy1._Enddate.Date
                                    End If
                                    If xDate2.Date > objCoy1._Enddate.Date Or xDate2.Date < objCoy1._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 12, "Sorry, End Date between") & " " & _
                                        objCoy1._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objCoy1._Enddate.Date
                                    End If
                                End If
                                objCoy1 = Nothing
                            Case enWeight_Types.WEIGHT_FIELD2
                                Dim objCoy2 As New clsassess_coyfield2_master
                                objCoy2._Coyfield2unkid = iParentId
                                If objCoy2._Startdate <> Nothing AndAlso objCoy2._Enddate <> Nothing Then
                                    If xDate1.Date > objCoy2._Enddate.Date Or xDate1.Date < objCoy2._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 10, "Sorry, Start Date between") & " " & _
                                        objCoy2._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objCoy2._Enddate.Date
                                    End If
                                    If xDate2.Date > objCoy2._Enddate.Date Or xDate2.Date < objCoy2._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 12, "Sorry, End Date between") & " " & _
                                        objCoy2._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objCoy2._Enddate.Date
                                    End If
                                End If
                                objCoy2 = Nothing
                            Case enWeight_Types.WEIGHT_FIELD3
                                Dim objCoy3 As New clsassess_coyfield3_master
                                objCoy3._Coyfield3unkid = iParentId
                                If objCoy3._Startdate <> Nothing AndAlso objCoy3._Enddate <> Nothing Then
                                    If xDate1.Date > objCoy3._Enddate.Date Or xDate1.Date < objCoy3._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 10, "Sorry, Start Date between") & " " & _
                                        objCoy3._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objCoy3._Enddate.Date
                                    End If
                                    If xDate2.Date > objCoy3._Enddate.Date Or xDate2.Date < objCoy3._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 12, "Sorry, End Date between") & " " & _
                                        objCoy3._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objCoy3._Enddate.Date
                                    End If
                                End If
                                objCoy3 = Nothing
                            Case enWeight_Types.WEIGHT_FIELD4
                                Dim objCoy4 As New clsassess_coyfield4_master
                                objCoy4._Coyfield4unkid = iParentId
                                If objCoy4._Startdate <> Nothing AndAlso objCoy4._Enddate <> Nothing Then
                                    If xDate1.Date > objCoy4._Enddate.Date Or xDate1.Date < objCoy4._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 10, "Sorry, Start Date between") & " " & _
                                        objCoy4._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objCoy4._Enddate.Date
                                    End If
                                    If xDate2.Date > objCoy4._Enddate.Date Or xDate2.Date < objCoy4._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 12, "Sorry, End Date between") & " " & _
                                        objCoy4._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objCoy4._Enddate.Date
                                    End If
                                End If
                                objCoy4 = Nothing
                            Case enWeight_Types.WEIGHT_FIELD5
                                Dim objCoy5 As New clsassess_coyfield5_master
                                objCoy5._Coyfield5unkid = iParentId
                                If objCoy5._Startdate <> Nothing AndAlso objCoy5._Enddate <> Nothing Then
                                    If xDate1.Date > objCoy5._Enddate.Date Or xDate1.Date < objCoy5._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 10, "Sorry, Start Date between") & " " & _
                                        objCoy5._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objCoy5._Enddate.Date
                                    End If
                                    If xDate2.Date > objCoy5._Enddate.Date Or xDate2.Date < objCoy5._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 12, "Sorry, End Date between") & " " & _
                                        objCoy5._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objCoy5._Enddate.Date
                                    End If
                                End If
                                objCoy5 = Nothing
                        End Select
                    Case enWeightCheckType.CKT_ALLOCATION_LEVEL
                        Select Case iExOrdr
                            Case enWeight_Types.WEIGHT_FIELD1
                                Dim objOwr1 As New clsassess_owrfield1_master
                                objOwr1._Owrfield1unkid = iParentId
                                If objOwr1._Startdate <> Nothing AndAlso objOwr1._Enddate <> Nothing Then
                                    If xDate1.Date > objOwr1._Enddate.Date Or xDate1.Date < objOwr1._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 10, "Sorry, Start Date between") & " " & _
                                        objOwr1._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objOwr1._Enddate.Date
                                    End If
                                    If xDate2.Date > objOwr1._Enddate.Date Or xDate2.Date < objOwr1._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 12, "Sorry, End Date between") & " " & _
                                        objOwr1._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objOwr1._Enddate.Date
                                    End If
                                End If
                                objOwr1 = Nothing
                            Case enWeight_Types.WEIGHT_FIELD2
                                Dim objOwr2 As New clsassess_owrfield2_master
                                objOwr2._Owrfield2unkid = iParentId
                                If objOwr2._Startdate <> Nothing AndAlso objOwr2._Enddate <> Nothing Then
                                    If xDate1.Date > objOwr2._Enddate.Date Or xDate1.Date < objOwr2._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 10, "Sorry, Start Date between") & " " & _
                                        objOwr2._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objOwr2._Enddate.Date
                                    End If
                                    If xDate2.Date > objOwr2._Enddate.Date Or xDate2.Date < objOwr2._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 12, "Sorry, End Date between") & " " & _
                                        objOwr2._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objOwr2._Enddate.Date
                                    End If
                                End If
                                objOwr2 = Nothing
                            Case enWeight_Types.WEIGHT_FIELD3
                                Dim objOwr3 As New clsassess_owrfield3_master
                                objOwr3._Owrfield3unkid = iParentId
                                If objOwr3._Startdate <> Nothing AndAlso objOwr3._Enddate <> Nothing Then
                                    If xDate1.Date > objOwr3._Enddate.Date Or xDate1.Date < objOwr3._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 10, "Sorry, Start Date between") & " " & _
                                        objOwr3._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objOwr3._Enddate.Date
                                    End If
                                    If xDate2.Date > objOwr3._Enddate.Date Or xDate2.Date < objOwr3._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 12, "Sorry, End Date between") & " " & _
                                        objOwr3._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objOwr3._Enddate.Date
                                    End If
                                End If
                                objOwr3 = Nothing
                            Case enWeight_Types.WEIGHT_FIELD4
                                Dim objOwr4 As New clsassess_owrfield4_master
                                objOwr4._Owrfield4unkid = iParentId
                                If objOwr4._Startdate <> Nothing AndAlso objOwr4._Enddate <> Nothing Then
                                    If xDate1.Date > objOwr4._Enddate.Date Or xDate1.Date < objOwr4._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 10, "Sorry, Start Date between") & " " & _
                                        objOwr4._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objOwr4._Enddate.Date
                                    End If
                                    If xDate2.Date > objOwr4._Enddate.Date Or xDate2.Date < objOwr4._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 12, "Sorry, End Date between") & " " & _
                                        objOwr4._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objOwr4._Enddate.Date
                                    End If
                                End If
                                objOwr4 = Nothing
                            Case enWeight_Types.WEIGHT_FIELD5
                                Dim objOwr5 As New clsassess_owrfield5_master
                                objOwr5._Owrfield5unkid = iParentId
                                If objOwr5._Startdate <> Nothing AndAlso objOwr5._Enddate <> Nothing Then
                                    If xDate1.Date > objOwr5._Enddate.Date Or xDate1.Date < objOwr5._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 10, "Sorry, Start Date between") & " " & _
                                        objOwr5._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objOwr5._Enddate.Date
                                    End If
                                    If xDate2.Date > objOwr5._Enddate.Date Or xDate2.Date < objOwr5._Startdate.Date Then
                                        iMsg = Language.getMessage(mstrModuleName, 12, "Sorry, End Date between") & " " & _
                                        objOwr5._Startdate.Date & Language.getMessage(mstrModuleName, 11, "And") & " " & objOwr5._Enddate.Date
                                    End If
                                End If
                                objOwr5 = Nothing
                        End Select
                End Select
                objFMst = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid_Date", mstrModuleName)
        Finally
        End Try
        Return iMsg
    End Function


    'S.SANDEEP [10 DEC 2015] -- START
    Public Function Is_Update_Progress_Started(ByVal iCheckType As enWeightCheckType, _
                                               ByVal iPeriodId As Integer, _
                                               ByVal iOwnerId As Integer, _
                                               ByVal iEmployeeId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim iMessage As String = String.Empty
        Dim iTableName As String = String.Empty
        Dim iFieldName As String = String.Empty
        Dim iMappedFieldId As Integer = -1
        Dim iExOdr As Integer = -1
        Dim iMappedFieldName As String = String.Empty
        iMappedFieldId = Get_Map_FieldId(iPeriodId)
        iMappedFieldName = Get_Map_FieldName(iPeriodId)

        objDataOperation = New clsDataOperation
        Try
            If iMappedFieldId > 0 Then
                Dim objFMaster As New clsAssess_Field_Master
                iExOdr = objFMaster.Get_Field_ExOrder(iMappedFieldId, True)
                objFMaster = Nothing

                Dim iProgressUpdate As Decimal = 0

                Select Case iCheckType
                    Case enWeightCheckType.CKT_ALLOCATION_LEVEL
                        iTableName = "hrassess_owrupdate_tran"
                    Case enWeightCheckType.CKT_EMPLOYEE_LEVEL
                        iTableName = "hrassess_empupdate_tran"
                End Select

                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'StrQ = "SELECT @progress = ISNULL(pct_completed,0) FROM " & iTableName & " WHERE isvoid = 0 AND periodunkid = '" & iPeriodId & "' "
                StrQ = "SELECT @progress = CAST(ISNULL(pct_completed,0) AS DECIMAL(36,2)) FROM " & iTableName & " WITH (NOLOCK) WHERE isvoid = 0 AND periodunkid = '" & iPeriodId & "' "
                'S.SANDEEP |21-AUG-2019| -- END

                If iOwnerId > 0 AndAlso iCheckType = enWeightCheckType.CKT_ALLOCATION_LEVEL Then
                    StrQ &= " AND ownerunkid  = '" & iOwnerId & "' "
                End If
                If iEmployeeId > 0 AndAlso iCheckType = enWeightCheckType.CKT_EMPLOYEE_LEVEL Then
                    StrQ &= " AND employeeunkid = '" & iEmployeeId & "' "
                End If

                objDataOperation.AddParameter("@progress", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iProgressUpdate, ParameterDirection.Output)


                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                'S.SANDEEP [28 DEC 2015] -- START
                'iProgressUpdate = CDec(objDataOperation.GetParameterValue("@progress"))
                If IsDBNull(objDataOperation.GetParameterValue("@progress")) = True Then
                    iProgressUpdate = 0
                Else
                    iProgressUpdate = CDec(objDataOperation.GetParameterValue("@progress"))
                End If
                'S.SANDEEP [28 DEC 2015] -- END


                If iProgressUpdate > 0 Then
                    iMessage = Language.getMessage(mstrModuleName, 14, "Failed to Open Goals because Employee has updated progress of some of these Goals.")
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Is_Update_Progress_Started; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return iMessage
    End Function
    'S.SANDEEP [10 DEC 2015] -- END

    Public Function Is_Weight_Set_For_Commiting(ByVal iCheckType As enWeightCheckType, _
                                                ByVal iPeriodId As Integer, _
                                                ByVal iOwnerId As Integer, _
                                                ByVal iEmployeeId As Integer) As String
        Dim StrQ As String = String.Empty
        Dim iMessage As String = String.Empty
        Dim iTableName As String = String.Empty
        Dim iFieldName As String = String.Empty
        Dim iTotalWeight, iAssignedWeight As Decimal
        Dim iMappedFieldId As Integer = -1
        Dim iExOdr As Integer = -1
        Dim iMappedFieldName As String = String.Empty
        iMappedFieldId = Get_Map_FieldId(iPeriodId)
        iMappedFieldName = Get_Map_FieldName(iPeriodId)

        objDataOperation = New clsDataOperation
        Try
            If iMappedFieldId > 0 Then
                iTotalWeight = 0 : iAssignedWeight = 0

                StrQ = "SELECT @weight = weight FROM hrassess_field_mapping WITH (NOLOCK) WHERE periodunkid = @periodunkid AND fieldunkid = @fieldunkid"

                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId, ParameterDirection.Input)
                objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iMappedFieldId, ParameterDirection.Input)
                objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iTotalWeight, ParameterDirection.Output)

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                iTotalWeight = CDec(objDataOperation.GetParameterValue("@weight"))

                If iTotalWeight > 0 Then
                    Dim objFMaster As New clsAssess_Field_Master
                    iExOdr = objFMaster.Get_Field_ExOrder(iMappedFieldId, True)
                    objFMaster = Nothing

                    Select Case iExOdr
                        Case enWeight_Types.WEIGHT_FIELD1
                            Select Case iCheckType
                                Case enWeightCheckType.CKT_COMPANY_LEVEL
                                    iTableName = "hrassess_coyfield1_master"
                                    iFieldName = "coyfield1unkid"
                                Case enWeightCheckType.CKT_ALLOCATION_LEVEL
                                    iTableName = "hrassess_owrfield1_master"
                                    iFieldName = "owrfield1unkid"
                                Case enWeightCheckType.CKT_EMPLOYEE_LEVEL
                                    iTableName = "hrassess_empfield1_master"
                                    iFieldName = "empfield1unkid"
                            End Select
                        Case enWeight_Types.WEIGHT_FIELD2
                            Select Case iCheckType
                                Case enWeightCheckType.CKT_COMPANY_LEVEL
                                    iTableName = "hrassess_coyfield2_master"
                                    iFieldName = "coyfield2unkid"
                                Case enWeightCheckType.CKT_ALLOCATION_LEVEL
                                    iTableName = "hrassess_owrfield2_master"
                                    iFieldName = "owrfield2unkid"
                                Case enWeightCheckType.CKT_EMPLOYEE_LEVEL
                                    iTableName = "hrassess_empfield2_master"
                                    iFieldName = "empfield2unkid"
                            End Select
                        Case enWeight_Types.WEIGHT_FIELD3
                            Select Case iCheckType
                                Case enWeightCheckType.CKT_COMPANY_LEVEL
                                    iTableName = "hrassess_coyfield3_master"
                                    iFieldName = "coyfield3unkid"
                                Case enWeightCheckType.CKT_ALLOCATION_LEVEL
                                    iTableName = "hrassess_owrfield3_master"
                                    iFieldName = "owrfield3unkid"
                                Case enWeightCheckType.CKT_EMPLOYEE_LEVEL
                                    iTableName = "hrassess_empfield3_master"
                                    iFieldName = "empfield3unkid"
                            End Select
                        Case enWeight_Types.WEIGHT_FIELD4
                            Select Case iCheckType
                                Case enWeightCheckType.CKT_COMPANY_LEVEL
                                    iTableName = "hrassess_coyfield4_master"
                                    iFieldName = "coyfield4unkid"
                                Case enWeightCheckType.CKT_ALLOCATION_LEVEL
                                    iTableName = "hrassess_owrfield4_master"
                                    iFieldName = "owrfield4unkid"
                                Case enWeightCheckType.CKT_EMPLOYEE_LEVEL
                                    iTableName = "hrassess_empfield4_master"
                                    iFieldName = "empfield4unkid"
                            End Select
                        Case enWeight_Types.WEIGHT_FIELD5
                            Select Case iCheckType
                                Case enWeightCheckType.CKT_COMPANY_LEVEL
                                    iTableName = "hrassess_coyfield5_master"
                                    iFieldName = "coyfield5unkid"
                                Case enWeightCheckType.CKT_ALLOCATION_LEVEL
                                    iTableName = "hrassess_owrfield5_master"
                                    iFieldName = "owrfield4unkid"
                                Case enWeightCheckType.CKT_EMPLOYEE_LEVEL
                                    iTableName = "hrassess_empfield5_master"
                                    iFieldName = "empfield5unkid"
                            End Select
                    End Select
                    StrQ = "SELECT @Assigned_Weight = ISNULL(SUM(weight),0) FROM " & iTableName & " WITH (NOLOCK) WHERE isvoid = 0 "
                    If iOwnerId > 0 AndAlso iCheckType = enWeightCheckType.CKT_ALLOCATION_LEVEL Then
                        StrQ &= " AND ownerunkid  = '" & iOwnerId & "' "
                    End If
                    If iEmployeeId > 0 AndAlso iCheckType = enWeightCheckType.CKT_EMPLOYEE_LEVEL Then
                        StrQ &= " AND employeeunkid = '" & iEmployeeId & "' "
                    End If
                    objDataOperation.AddParameter("@Assigned_Weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iAssignedWeight, ParameterDirection.Output)


                    'S.SANDEEP [10 FEB 2016] -- START
                    StrQ &= " AND " & iTableName & ".periodunkid = '" & iPeriodId & "' "
                    'S.SANDEEP [10 FEB 2016] -- END

                    objDataOperation.ExecNonQuery(StrQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    End If

                    iAssignedWeight = CDec(objDataOperation.GetParameterValue("@Assigned_Weight"))

                    If iAssignedWeight < iTotalWeight Then
                        iMessage = Language.getMessage(mstrModuleName, 6, "Sorry, you cannot submit") & " " & iMappedFieldName & ". " & _
                                   Language.getMessage(mstrModuleName, 7, "Reason") & " " & iMappedFieldName & ", " & _
                                   Language.getMessage(mstrModuleName, 8, "are not adding up to") & " " & iTotalWeight & ". "
                    ElseIf iAssignedWeight > iTotalWeight Then
                        iMessage = Language.getMessage(mstrModuleName, 6, "Sorry, you cannot submit") & " " & iMappedFieldName & ". " & _
                                   Language.getMessage(mstrModuleName, 7, "Reason") & " " & iMappedFieldName & ", " & _
                                   Language.getMessage(mstrModuleName, 9, "are adding more than") & " " & iTotalWeight & ". "
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Weight_Set_For_Commiting", mstrModuleName)
        Finally
        End Try
        Return iMessage
    End Function

    Public Function Is_Valid_Weight(ByVal iCheckType As enWeightCheckType, _
                                    ByVal iWeightType As enWeight_Types, _
                                    ByVal iWeight As Decimal, _
                                    ByVal iPeriodId As Integer, _
                                    ByVal iMappedFieldId As Integer, _
                                    ByVal iOwnerId As Integer, _
                                    ByVal iEmployeeId As Integer, _
                                    ByVal iMode As enAction, _
                                    ByVal iFieldUnkid As Integer) As String

        Dim StrQ As String = String.Empty
        Dim iMessage As String = String.Empty
        Dim iTableName As String = String.Empty
        Dim iFieldName As String = String.Empty
        Dim iTotalWeight, iAssignedWeight As Decimal
        Dim iCheckValue As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            iTotalWeight = 0 : iAssignedWeight = 0

            StrQ = "SELECT @weight = weight FROM hrassess_field_mapping WITH (NOLOCK) WHERE periodunkid = @periodunkid AND fieldunkid = @fieldunkid"

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId, ParameterDirection.Input)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iMappedFieldId, ParameterDirection.Input)
            objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iTotalWeight, ParameterDirection.Output)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            iTotalWeight = CDec(objDataOperation.GetParameterValue("@weight"))

            If iTotalWeight > 0 Then
                Select Case iWeightType
                    Case enWeight_Types.WEIGHT_FIELD1
                        Select Case iCheckType
                            Case enWeightCheckType.CKT_COMPANY_LEVEL
                                iTableName = "hrassess_coyfield1_master"
                                iFieldName = "coyfield1unkid"
                            Case enWeightCheckType.CKT_ALLOCATION_LEVEL
                                iTableName = "hrassess_owrfield1_master"
                                iFieldName = "owrfield1unkid"
                            Case enWeightCheckType.CKT_EMPLOYEE_LEVEL
                                iTableName = "hrassess_empfield1_master"
                                iFieldName = "empfield1unkid"
                        End Select
                    Case enWeight_Types.WEIGHT_FIELD2
                        Select Case iCheckType
                            Case enWeightCheckType.CKT_COMPANY_LEVEL
                                iTableName = "hrassess_coyfield2_master"
                                iFieldName = "coyfield2unkid"
                            Case enWeightCheckType.CKT_ALLOCATION_LEVEL
                                iTableName = "hrassess_owrfield2_master"
                                iFieldName = "owrfield2unkid"
                            Case enWeightCheckType.CKT_EMPLOYEE_LEVEL
                                iTableName = "hrassess_empfield2_master"
                                iFieldName = "empfield2unkid"
                        End Select
                    Case enWeight_Types.WEIGHT_FIELD3
                        Select Case iCheckType
                            Case enWeightCheckType.CKT_COMPANY_LEVEL
                                iTableName = "hrassess_coyfield3_master"
                                iFieldName = "coyfield3unkid"
                            Case enWeightCheckType.CKT_ALLOCATION_LEVEL
                                iTableName = "hrassess_owrfield3_master"
                                iFieldName = "owrfield3unkid"
                            Case enWeightCheckType.CKT_EMPLOYEE_LEVEL
                                iTableName = "hrassess_empfield3_master"
                                iFieldName = "empfield3unkid"
                        End Select
                    Case enWeight_Types.WEIGHT_FIELD4
                        Select Case iCheckType
                            Case enWeightCheckType.CKT_COMPANY_LEVEL
                                iTableName = "hrassess_coyfield4_master"
                                iFieldName = "coyfield4unkid"
                            Case enWeightCheckType.CKT_ALLOCATION_LEVEL
                                iTableName = "hrassess_owrfield4_master"
                                iFieldName = "owrfield4unkid"
                            Case enWeightCheckType.CKT_EMPLOYEE_LEVEL
                                iTableName = "hrassess_empfield4_master"
                                iFieldName = "empfield4unkid"
                        End Select
                    Case enWeight_Types.WEIGHT_FIELD5
                        Select Case iCheckType
                            Case enWeightCheckType.CKT_COMPANY_LEVEL
                                iTableName = "hrassess_coyfield5_master"
                                iFieldName = "coyfield5unkid"
                            Case enWeightCheckType.CKT_ALLOCATION_LEVEL
                                iTableName = "hrassess_owrfield5_master"
                                iFieldName = "owrfield4unkid"
                            Case enWeightCheckType.CKT_EMPLOYEE_LEVEL
                                iTableName = "hrassess_empfield5_master"
                                iFieldName = "empfield5unkid"
                        End Select
                End Select
                'S.SANDEEP [20 MAR 2015] -- START
                'StrQ = "SELECT @Assigned_Weight = ISNULL(SUM(weight),0) FROM " & iTableName & " WHERE isvoid = 0 "
                StrQ = "SELECT ISNULL(SUM(CAST(weight AS DECIMAL(36,6))),0) AS TW FROM " & iTableName & " WITH (NOLOCK) WHERE isvoid = 0 "
                'S.SANDEEP [20 MAR 2015] -- END

                If iMode = enAction.EDIT_ONE Then
                    StrQ &= " AND " & iTableName & "." & iFieldName & " <> '" & iFieldUnkid & "' "
                End If

                If iCheckType = enWeightCheckType.CKT_COMPANY_LEVEL Then
                    StrQ &= " AND periodunkid = '" & iPeriodId & "' "
                End If

                If iOwnerId > 0 AndAlso iCheckType = enWeightCheckType.CKT_ALLOCATION_LEVEL Then
                    StrQ &= " AND ownerunkid  = '" & iOwnerId & "' AND periodunkid = '" & iPeriodId & "' "
                End If
                If iEmployeeId > 0 AndAlso iCheckType = enWeightCheckType.CKT_EMPLOYEE_LEVEL Then
                    StrQ &= " AND employeeunkid = '" & iEmployeeId & "' AND periodunkid = '" & iPeriodId & "' "
                End If

                'S.SANDEEP [20 MAR 2015] -- START
                'objDataOperation.AddParameter("@Assigned_Weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iAssignedWeight, ParameterDirection.Output)
                'objDataOperation.ExecNonQuery(StrQ)

                Dim dsList As New DataSet
                dsList = objDataOperation.ExecQuery(StrQ, "List")
                'S.SANDEEP [20 MAR 2015] -- END

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                'S.SANDEEP [20 MAR 2015] -- START
                'iAssignedWeight = CDec(objDataOperation.GetParameterValue("@Assigned_Weight"))
                If dsList.Tables(0).Rows.Count > 0 Then
                    iAssignedWeight = dsList.Tables(0).Rows(0).Item("TW")
                End If
                'S.SANDEEP [20 MAR 2015] -- END

                If (iAssignedWeight + iWeight) > iTotalWeight Then

                    'Pinkal (15-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'iMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Total weight allocated for the selected period in field mapping is") & " " & iTotalWeight.ToString & ". " & _
                    '           Language.getMessage(mstrModuleName, 3, "Total weight assigned so far is ") & " " & iAssignedWeight.ToString() & ". " & _
                    '           Language.getMessage(mstrModuleName, 4, "The weight you are trying to assign is ") & " " & iWeight & ". " & _
                    '           Language.getMessage(mstrModuleName, 5, "Please set the weight that does not exceeds total weight setting.")



                    'Pinkal (22-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                    'iMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Total weight allocated for the selected period in field mapping is") & " " & iTotalWeight.ToString() & "%. " & _
                    '          Language.getMessage(mstrModuleName, 3, "Total weight assigned so far is ") & " " & iAssignedWeight.ToString("#0.00") & "%. " & _
                    '          Language.getMessage(mstrModuleName, 4, "The weight you are trying to assign is ") & " " & iWeight & "%. " & _
                    '           Language.getMessage(mstrModuleName, 5, "Please set the weight that does not exceeds total weight setting.")

                    iMessage = Language.getMessage(mstrModuleName, 15, "Sorry, Total weight allocated is") & " " & iTotalWeight.ToString() & "%. " & _
                              Language.getMessage(mstrModuleName, 3, "Total weight assigned so far is ") & " " & iAssignedWeight.ToString("#0.00") & "%. " & _
                              Language.getMessage(mstrModuleName, 16, "Please set the weight that does not exceed") & " " & iTotalWeight.ToString() & "%. "

                    'Pinkal (22-Mar-2019) -- End

                    'Pinkal (15-Mar-2019) -- End
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Valid_Weight", mstrModuleName)
        Finally
        End Try
        Return iMessage
    End Function

    Public Function Get_MappedField_ComboList(ByVal iEmployeeId As Integer, ByVal iPeriodId As Integer, Optional ByVal iAddSelect As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        objDataOperation = New clsDataOperation
        Try
            Dim iLinkedField As Integer = -1
            iLinkedField = Get_Map_FieldId(iPeriodId)
            If iLinkedField > 0 Then
                If iAddSelect = True Then
                    StrQ = "SELECT 0 AS Id, @Select As Name UNION "
                End If
                Dim objFMaster As New clsAssess_Field_Master
                Dim iExOdr As Integer = 0
                iExOdr = objFMaster.Get_Field_ExOrder(iLinkedField, True)
                Select Case iExOdr
                    Case enWeight_Types.WEIGHT_FIELD1
                        StrQ &= "SELECT empfield1unkid AS Id, field_data AS Name FROM hrassess_empfield1_master WITH (NOLOCK) WHERE hrassess_empfield1_master.isvoid = 0 " & _
                               "AND hrassess_empfield1_master.employeeunkid = '" & iEmployeeId & "' AND hrassess_empfield1_master.periodunkid = '" & iPeriodId & "' AND hrassess_empfield1_master.isfinal = 1 "
                    Case enWeight_Types.WEIGHT_FIELD2
                        StrQ &= "SELECT empfield2unkid AS Id, hrassess_empfield2_master.field_data AS Name " & _
                               "FROM hrassess_empfield2_master WITH (NOLOCK) " & _
                               "    JOIN hrassess_empfield1_master WITH (NOLOCK) ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid AND hrassess_empfield1_master.isvoid = 0 " & _
                               "WHERE hrassess_empfield2_master.isvoid = 0 AND hrassess_empfield2_master.employeeunkid = '" & iEmployeeId & "' AND hrassess_empfield2_master.periodunkid = '" & iPeriodId & "' " & _
                               "    AND hrassess_empfield1_master.isfinal = 1 "
                    Case enWeight_Types.WEIGHT_FIELD3
                        StrQ &= "SELECT empfield3unkid AS Id, hrassess_empfield3_master.field_data AS Name " & _
                               "FROM hrassess_empfield3_master WITH (NOLOCK) " & _
                               "    JOIN hrassess_empfield2_master WITH (NOLOCK) ON hrassess_empfield2_master.empfield2unkid = hrassess_empfield3_master.empfield2unkid AND hrassess_empfield2_master.isvoid = 0 " & _
                               "    JOIN hrassess_empfield1_master WITH (NOLOCK) ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid AND hrassess_empfield1_master.isvoid = 0 " & _
                               "WHERE hrassess_empfield3_master.isvoid = 0 AND hrassess_empfield3_master.employeeunkid = '" & iEmployeeId & "' AND hrassess_empfield3_master.periodunkid = '" & iPeriodId & "' " & _
                               "    AND hrassess_empfield1_master.isfinal = 1 "
                    Case enWeight_Types.WEIGHT_FIELD4
                        StrQ &= "SELECT empfield4unkid AS Id, hrassess_empfield4_master.field_data AS Name " & _
                               "FROM hrassess_empfield4_master WITH (NOLOCK) " & _
                               "    JOIN hrassess_empfield3_master WITH (NOLOCK) ON hrassess_empfield3_master.empfield3unkid = hrassess_empfield4_master.empfield3unkid AND hrassess_empfield3_master.isvoid = 0 " & _
                               "    JOIN hrassess_empfield2_master WITH (NOLOCK) ON hrassess_empfield2_master.empfield2unkid = hrassess_empfield3_master.empfield2unkid AND hrassess_empfield2_master.isvoid = 0 " & _
                               "    JOIN hrassess_empfield1_master WITH (NOLOCK) ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid AND hrassess_empfield1_master.isvoid = 0 " & _
                               "WHERE hrassess_empfield4_master.isvoid = 0 AND hrassess_empfield4_master.employeeunkid = '" & iEmployeeId & "' AND hrassess_empfield4_master.periodunkid = '" & iPeriodId & "' " & _
                               "    AND hrassess_empfield1_master.isfinal = 1 "
                    Case enWeight_Types.WEIGHT_FIELD5
                        StrQ &= "SELECT empfield5unkid AS Id, hrassess_empfield5_master.field_data AS Name " & _
                               "FROM hrassess_empfield5_master WITH (NOLOCK) " & _
                               "    JOIN hrassess_empfield4_master WITH (NOLOCK) ON hrassess_empfield4_master.empfield4unkid = hrassess_empfield5_master.empfield4unkid AND hrassess_empfield4_master.isvoid = 0 " & _
                               "    JOIN hrassess_empfield3_master WITH (NOLOCK) ON hrassess_empfield3_master.empfield3unkid = hrassess_empfield4_master.empfield3unkid AND hrassess_empfield3_master.isvoid = 0 " & _
                               "    JOIN hrassess_empfield2_master WITH (NOLOCK) ON hrassess_empfield2_master.empfield2unkid = hrassess_empfield3_master.empfield2unkid AND hrassess_empfield2_master.isvoid = 0 " & _
                               "    JOIN hrassess_empfield1_master WITH (NOLOCK) ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid AND hrassess_empfield1_master.isvoid = 0 " & _
                               "WHERE hrassess_empfield5_master.isvoid = 0 AND hrassess_empfield5_master.employeeunkid = '" & iEmployeeId & "' AND hrassess_empfield5_master.periodunkid = '" & iPeriodId & "' " & _
                               "    AND hrassess_empfield1_master.isfinal = 1 "
                End Select
                objFMaster = Nothing
            Else
                StrQ = "SELECT 0 AS Id, @Select As Name "
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_MappedField_ComboList", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Public Function GetTotalWeight(ByVal iCheckType As enWeightCheckType, _
                                   ByVal xPeriodId As Integer, _
                                   ByVal xOwnerId As Integer, _
                                   ByVal xEmpId As Integer) As Double
        Dim iTableName As String = String.Empty
        Dim StrQ As String = String.Empty
        Dim iTotalWeight As Double = 0
        Dim xLinkedFieldId As Integer = -1
        Dim xExOrdr As Integer = 0
        Try
            xLinkedFieldId = Get_Map_FieldId(xPeriodId)
            If xLinkedFieldId > 0 Then
                Dim objFMaster As New clsAssess_Field_Master
                xExOrdr = objFMaster.Get_Field_ExOrder(xLinkedFieldId)
                objFMaster = Nothing

                'S.SANDEEP [09 FEB 2015] -- START
                If xExOrdr <= 0 Then Return 0
                'S.SANDEEP [09 FEB 2015] -- END

                Select Case xExOrdr
                    Case enWeight_Types.WEIGHT_FIELD1
                        Select Case iCheckType
                            Case enWeightCheckType.CKT_COMPANY_LEVEL
                                iTableName = "hrassess_coyfield1_master"
                            Case enWeightCheckType.CKT_ALLOCATION_LEVEL
                                iTableName = "hrassess_owrfield1_master"
                            Case enWeightCheckType.CKT_EMPLOYEE_LEVEL
                                iTableName = "hrassess_empfield1_master"
                        End Select
                    Case enWeight_Types.WEIGHT_FIELD2
                        Select Case iCheckType
                            Case enWeightCheckType.CKT_COMPANY_LEVEL
                                iTableName = "hrassess_coyfield2_master"
                            Case enWeightCheckType.CKT_ALLOCATION_LEVEL
                                iTableName = "hrassess_owrfield2_master"
                            Case enWeightCheckType.CKT_EMPLOYEE_LEVEL
                                iTableName = "hrassess_empfield2_master"
                        End Select
                    Case enWeight_Types.WEIGHT_FIELD3
                        Select Case iCheckType
                            Case enWeightCheckType.CKT_COMPANY_LEVEL
                                iTableName = "hrassess_coyfield3_master"
                            Case enWeightCheckType.CKT_ALLOCATION_LEVEL
                                iTableName = "hrassess_owrfield3_master"
                            Case enWeightCheckType.CKT_EMPLOYEE_LEVEL
                                iTableName = "hrassess_empfield3_master"
                        End Select
                    Case enWeight_Types.WEIGHT_FIELD4
                        Select Case iCheckType
                            Case enWeightCheckType.CKT_COMPANY_LEVEL
                                iTableName = "hrassess_coyfield4_master"
                            Case enWeightCheckType.CKT_ALLOCATION_LEVEL
                                iTableName = "hrassess_owrfield4_master"
                            Case enWeightCheckType.CKT_EMPLOYEE_LEVEL
                                iTableName = "hrassess_empfield4_master"
                        End Select
                    Case enWeight_Types.WEIGHT_FIELD5
                        Select Case iCheckType
                            Case enWeightCheckType.CKT_COMPANY_LEVEL
                                iTableName = "hrassess_coyfield5_master"
                            Case enWeightCheckType.CKT_ALLOCATION_LEVEL
                                iTableName = "hrassess_owrfield5_master"
                            Case enWeightCheckType.CKT_EMPLOYEE_LEVEL
                                iTableName = "hrassess_empfield5_master"
                        End Select
                End Select

                'S.SANDEEP [20 MAR 2015] -- START
                'StrQ = "SELECT @TotWeight = ISNULL(SUM(weight),0) FROM " & iTableName & " WHERE isvoid = 0 "

                'Shani (01-Dec-2016) -- Start
                'Enhancement - Changes for Header caption for Tanapa Company and some other changes for ACB company
                'StrQ = "SELECT ISNULL(SUM(CAST(weight AS DECIMAL(36,6))),0) AS TW FROM " & iTableName & " WHERE isvoid = 0 "
                StrQ = "SELECT ISNULL(SUM(CAST(" & iTableName & ".weight AS DECIMAL(36,6))),0) AS TW FROM " & iTableName & " WITH (NOLOCK) "

                If iCheckType = enWeightCheckType.CKT_EMPLOYEE_LEVEL Then
                    If xExOrdr > 4 Then
                        StrQ &= " JOIN hrassess_empfield4_master WITH (NOLOCK) ON hrassess_empfield4_master.empfield4unkid = hrassess_empfield5_master.empfield4unkid "
                    End If

                    If xExOrdr > 3 Then
                        StrQ &= " JOIN hrassess_empfield3_master WITH (NOLOCK) ON hrassess_empfield3_master.empfield3unkid = hrassess_empfield4_master.empfield3unkid "
                    End If

                    If xExOrdr > 2 Then
                        StrQ &= " JOIN hrassess_empfield2_master WITH (NOLOCK) ON hrassess_empfield2_master.empfield2unkid = hrassess_empfield3_master.empfield2unkid "
                    End If

                    If xExOrdr > 1 Then
                        StrQ &= " JOIN hrassess_empfield1_master WITH (NOLOCK) ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid "
                    End If

                    'Shani (06-Dec-2017) -- Start
                    'Issue - 
                    'StrQ &= "   JOIN hrassess_perspective_master ON hrassess_perspective_master.perspectiveunkid = hrassess_empfield1_master.perspectiveunkid " & _
                    '        "WHERE " & iTableName & ".isvoid = 0 AND hrassess_perspective_master.isactive = 1 "
                    'End If

                    'S.SANDEEP |05-APR-2019| -- START
                    'StrQ &= "   JOIN hrassess_perspective_master ON hrassess_perspective_master.perspectiveunkid = hrassess_empfield1_master.perspectiveunkid " & _
                    '        "WHERE " & iTableName & ".isvoid = 0 AND hrassess_perspective_master.isactive = 1 "

                    StrQ &= "   LEFT JOIN hrassess_perspective_master WITH (NOLOCK) ON hrassess_perspective_master.perspectiveunkid = hrassess_empfield1_master.perspectiveunkid AND hrassess_perspective_master.isactive = 1  " & _
                            " WHERE " & iTableName & ".isvoid = 0 "
                    'S.SANDEEP |05-APR-2019| -- END
                    
                Else
                    StrQ &= "WHERE " & iTableName & ".isvoid = 0 "
                End If

                'Shani (06-Dec-2017) -- End

                'Shani (01-Dec-2016) -- End

                'S.SANDEEP [20 MAR 2015] -- END

                If xOwnerId > 0 AndAlso iCheckType = enWeightCheckType.CKT_ALLOCATION_LEVEL Then
                    StrQ &= " AND ownerunkid  = '" & xOwnerId & "' "
                End If
                If xEmpId > 0 AndAlso iCheckType = enWeightCheckType.CKT_EMPLOYEE_LEVEL Then
                    StrQ &= " AND " & iTableName & ".employeeunkid = '" & xEmpId & "' "
                End If

                'S.SANDEEP [03 FEB 2016] -- START
                StrQ &= " AND " & iTableName & ".periodunkid = '" & xPeriodId & "' "
                'S.SANDEEP [03 FEB 2016] -- END


                'S.SANDEEP [20 MAR 2015] -- START
                'objDataOperation.AddParameter("@TotWeight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, iTotalWeight, ParameterDirection.Output)
                'objDataOperation.ExecNonQuery(StrQ)
                'S.SANDEEP [20 MAR 2015] -- END

                Dim dsList As New DataSet
                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                'S.SANDEEP [20 MAR 2015] -- START
                'iTotalWeight = CDbl(objDataOperation.GetParameterValue("@TotWeight"))
                If dsList.Tables(0).Rows.Count > 0 Then
                    iTotalWeight = dsList.Tables(0).Rows(0).Item("TW")
                Else
                    iTotalWeight = 0
                End If
                'S.SANDEEP [20 MAR 2015] -- END

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetTotalWeight", mstrModuleName)
        Finally
        End Try
        Return iTotalWeight
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Setting already defined for the selected period. Please define new setting.")
            Language.setMessage(mstrModuleName, 3, "Total weight assigned so far is")
            Language.setMessage(mstrModuleName, 6, "Sorry, you cannot submit")
            Language.setMessage(mstrModuleName, 7, "Reason")
            Language.setMessage(mstrModuleName, 8, "are not adding up to")
            Language.setMessage(mstrModuleName, 9, "are adding more than")
            Language.setMessage(mstrModuleName, 10, "Sorry, Start Date between")
            Language.setMessage(mstrModuleName, 11, "And")
            Language.setMessage(mstrModuleName, 12, "Sorry, End Date between")
            Language.setMessage(mstrModuleName, 13, "Select")
            Language.setMessage(mstrModuleName, 14, "Failed to Open Goals because Employee has updated progress of some of these Goals.")
			Language.setMessage(mstrModuleName, 15, "Sorry, Total weight allocated is")
			Language.setMessage(mstrModuleName, 16, "Please set the weight that does not exceed")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class