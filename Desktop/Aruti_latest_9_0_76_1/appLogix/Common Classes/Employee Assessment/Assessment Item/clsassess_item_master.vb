﻿'************************************************************************************************************************************
'Class Name : clsassess_item_master.vb
'Purpose    : All Assess Item Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :28/06/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsassess_item_master
    Private Shared ReadOnly mstrModuleName As String = "clsassess_item_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAssessitemunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mintAssessgroupunkid As Integer
    Private mintResultgroupunkid As Integer
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty

    'S.SANDEEP [ 29 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES 
    'TYPE : EMPLOYEMENT CONTRACT PROCESS
    Private mdecWeight As Decimal
    'S.SANDEEP [ 29 DEC 2011 ] -- END

    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintPeriodunkid As Integer
    Private mintYearunkid As Integer
    'S.SANDEEP [ 28 DEC 2012 ] -- END

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessitemunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Assessitemunkid() As Integer
        Get
            Return mintAssessitemunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessitemunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Assessgroupunkid() As Integer
        Get
            Return mintAssessgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessgroupunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ResultGroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Resultgroupunkid() As Integer
        Get
            Return mintResultgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintResultgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = Value
        End Set
    End Property

    'S.SANDEEP [ 29 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES 
    'TYPE : EMPLOYEMENT CONTRACT PROCESS
    ''' <summary>
    ''' Purpose: Get or Set weight
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Weight() As Decimal
        Get
            Return mdecWeight
        End Get
        Set(ByVal value As Decimal)
            mdecWeight = value
        End Set
    End Property
    'S.SANDEEP [ 29 DEC 2011 ] -- END


    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = Value
        End Set
    End Property
    'S.SANDEEP [ 28 DEC 2012 ] -- END


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  assessitemunkid " & _
              ", code " & _
              ", name " & _
              ", assessgroupunkid " & _
              ", isnull(resultgroupunkid,0) as resultgroupunkid " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2 " & _
              ", ISNULL(weight,0) AS weight " & _
              ", periodunkid " & _
              ", yearunkid " & _
             "FROM hrassess_item_master " & _
             "WHERE assessitemunkid = @assessitemunkid " 'S.SANDEEP [ 29 DEC 2011 --> ISNULL(weight,0) AS weight ]
            'S.SANDEEP [ 28 DEC 2012 ] -- START {periodunkid,yearunkid} -- END



            objDataOperation.AddParameter("@assessitemunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintAssessitemUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintassessitemunkid = CInt(dtRow.Item("assessitemunkid"))
                mstrcode = dtRow.Item("code").ToString
                mstrname = dtRow.Item("name").ToString
                mintAssessgroupunkid = CInt(dtRow.Item("assessgroupunkid"))
                mintResultgroupunkid = CInt(dtRow.Item("resultgroupunkid"))
                mstrdescription = dtRow.Item("description").ToString
                mblnisactive = CBool(dtRow.Item("isactive"))
                mstrname1 = dtRow.Item("name1").ToString
                mstrName2 = dtRow.Item("name2").ToString
                'S.SANDEEP [ 29 DEC 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES 
                'TYPE : EMPLOYEMENT CONTRACT PROCESS
                mdecWeight = dtRow.Item("weight")
                'S.SANDEEP [ 29 DEC 2011 ] -- END

                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintYearunkid = CInt(dtRow.Item("yearunkid"))
                'S.SANDEEP [ 28 DEC 2012 ] -- END

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  hrassess_item_master.assessitemunkid " & _
              ", hrassess_item_master.code " & _
              ", hrassess_item_master.name " & _
              ", hrassess_item_master.assessgroupunkid " & _
              ", hrassess_group_master.assessgroup_name as groupname" & _
              ", hrassess_item_master.resultgroupunkid " & _
              ", cfcommon_master.name as Resultgroup " & _
              ", hrassess_item_master.description " & _
              ", hrassess_item_master.isactive " & _
              ", hrassess_item_master.name1 " & _
              ", hrassess_item_master.name2 " & _
              ", ISNULL(hrassess_item_master.weight,0) AS weight " & _
                    ", hrassess_item_master.periodunkid " & _
                    ", hrassess_item_master.yearunkid " & _
                    ", cfcommon_period_tran.period_name AS Period " & _
                    ", cfcommon_period_tran.statusid " & _
             "FROM hrassess_item_master  " & _
             " LEFT JOIN hrassess_group_master on hrassess_group_master.assessgroupunkid = hrassess_item_master.assessgroupunkid " & _
                    " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = hrassess_item_master.periodunkid " & _
             " LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = hrassess_item_master.resultgroupunkid " 'S.SANDEEP [ 29 DEC 2011 --> ISNULL(weight,0) AS weight ]
            'S.SANDEEP [ 28 DEC 2012 ] -- START {periodunkid,yearunkid} -- END
            'S.SANDEEP [ 09 AUG 2013 hrassess_item_master. ] -- START -- END



            If blnOnlyActive Then
                strQ &= " WHERE hrassess_item_master.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_item_master) </purpose>
    Public Function Insert() As Boolean

        'S.SANDEEP [ 28 DEC 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(mstrCode, , , mintAssessgroupunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Assess Item Code is already defined. Please define new Assess Item Code.")
        '    Return False
        'ElseIf isExist("", mstrName, , mintAssessgroupunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Assess Item Name is already defined. Please define new Assess Item Name.")
        '    Return False
        'End If

        If isExist(mstrCode, , , mintAssessgroupunkid, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Assess Item Code is already defined. Please define new Assess Item Code.")
            Return False
        ElseIf isExist("", mstrName, , mintAssessgroupunkid, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Assess Item Name is already defined. Please define new Assess Item Name.")
            Return False
        End If
        'S.SANDEEP [ 28 DEC 2012 ] -- END

        

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessgroupunkid.ToString)
            objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultgroupunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)

            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecWeight.ToString)
            'S.SANDEEP [ 29 DEC 2011 ] -- END

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            strQ = "INSERT INTO hrassess_item_master ( " & _
              "  code " & _
              ", name " & _
              ", assessgroupunkid " & _
              ",resultgroupunkid " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2" & _
              ", weight" & _
                                  ", periodunkid " & _
                                  ", yearunkid" & _
            ") VALUES (" & _
              "  @code " & _
              ", @name " & _
              ", @assessgroupunkid " & _
              ", @resultgroupunkid " & _
              ", @description " & _
              ", @isactive " & _
              ", @name1 " & _
              ", @name2" & _
              ", @weight" & _
                                  ", @periodunkid " & _
                                  ", @yearunkid" & _
            "); SELECT @@identity" 'S.SANDEEP [ 29 DEC 2011 --> weight ]
            'S.SANDEEP [ 28 DEC 2012 ] -- START {periodunkid,yearunkid} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAssessitemunkid = dsList.Tables(0).Rows(0).Item(0)

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_item_master", "assessitemunkid", mintAssessitemunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_item_master) </purpose>
    Public Function Update() As Boolean
        'S.SANDEEP [ 28 DEC 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(mstrCode, "", mintAssessitemunkid, mintAssessgroupunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Assess Item Code is already defined. Please define new Assess Item Code.")
        '    Return False
        'ElseIf isExist("", mstrName, mintAssessitemunkid, mintAssessgroupunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 2, "This Assess Item Name is already defined. Please define new Assess Item Name.")
        '    Return False
        'End If

        If isExist(mstrCode, "", mintAssessitemunkid, mintAssessgroupunkid, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Assess Item Code is already defined. Please define new Assess Item Code.")
            Return False
        ElseIf isExist("", mstrName, mintAssessitemunkid, mintAssessgroupunkid, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Assess Item Name is already defined. Please define new Assess Item Name.")
            Return False
        End If
        'S.SANDEEP [ 28 DEC 2012 ] -- END
        

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@assessitemunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintassessitemunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessgroupunkid.ToString)
            objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultgroupunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)
            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecWeight.ToString)
            'S.SANDEEP [ 29 DEC 2011 ] -- END

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            strQ = "UPDATE hrassess_item_master SET " & _
              "  code = @code" & _
              ", name = @name" & _
              ", assessgroupunkid = @assessgroupunkid" & _
              ", resultgroupunkid = @resultgroupunkid " & _
              ", description = @description" & _
              ", isactive = @isactive" & _
              ", name1 = @name1" & _
              ", name2 = @name2 " & _
              ", weight = @weight " & _
              ", periodunkid = @periodunkid " & _
              ", yearunkid = @yearunkid " & _
            "WHERE assessitemunkid = @assessitemunkid " 'S.SANDEEP [ 29 DEC 2011 --> weight ]
            'S.SANDEEP [ 28 DEC 2012 ] -- START {periodunkid,yearunkid} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_item_master", mintAssessitemunkid, "assessitemunkid", 2) Then

                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_item_master", "assessitemunkid", mintAssessitemunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_item_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Assess Item. Reason: This Assess Item is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            strQ = "Update hrassess_item_master set isactive = 0 " & _
            "WHERE assessitemunkid = @assessitemunkid "

            objDataOperation.AddParameter("@assessitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_item_master", "assessitemunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        objDataOperation = New clsDataOperation
        Try


            'S.SANDEEP [ 18 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "select isnull(assessitemunkid,0) from hrassess_analysis_tran where assessitemunkid = @assessitemunkid"
            strQ = "SELECT " & _
                   " 'SELECT * FROM ' + TABLE_NAME + ' WHERE '+ COLUMN_NAME +' = ' AS Qry " & _
                   "FROM INFORMATION_SCHEMA.COLUMNS " & _
                   "WHERE COLUMN_NAME='assessitemunkid' AND TABLE_NAME <> 'hrassess_item_master' "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dRow As DataRow In dsList.Tables(0).Rows

                strQ = dRow.Item("Qry").ToString & "'" & intUnkid & "' "

            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Next
            'S.SANDEEP [ 18 FEB 2012 ] -- END


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", _
                                          Optional ByVal strName As String = "", _
                                          Optional ByVal intUnkid As Integer = -1, _
                                          Optional ByVal intAssessGrpId As Integer = -1, _
                                          Optional ByVal intPeriodId As Integer = -1) As Boolean 'S.SANDEEP [ 28 DEC 2012 ] -- START {intPeriodId} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  assessitemunkid " & _
              ", code " & _
              ", name " & _
              ", assessgroupunkid " & _
              ", resultgroupunkid " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2 " & _
             "FROM hrassess_item_master " & _
             "WHERE  1=1"

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strCode.Length > 0 Then
                strQ &= " AND code = @code "
            End If

            If strName.Length > 0 Then
                strQ &= " AND name = @name"
            End If

            If intAssessGrpId > 0 Then
                strQ &= " AND assessgroupunkid = @assessgroupunkid"
            End If

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If intPeriodId > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            If intUnkid > 0 Then
                strQ &= " AND assessitemunkid <> @assessitemunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@assessitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessGrpId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal intAssessGroupunkid As Integer = -1) As DataSet
    Public Function getListForCombo(Optional ByVal strListName As String = "List", _
                                                          Optional ByVal mblFlag As Boolean = False, _
                                                          Optional ByVal intAssessGroupunkid As Integer = -1, _
                                                          Optional ByVal blnSortById As Boolean = False, _
                                                          Optional ByVal intPeriodId As Integer = 0) As DataSet 'S.SANDEEP [ 28 DEC 2012 ] -- START {intPeriodId} -- END
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as assessitemunkid, ' ' +  @name  as name UNION "
            End If

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If blnSortById Then
            '    strQ &= "SELECT assessitemunkid, name  FROM hrassess_item_master where (assessgroupunkid = @assessgroupunkid or @assessgroupunkid = -1) AND isactive = 1  ORDER BY assessitemunkid "
            'Else
            '    strQ &= "SELECT assessitemunkid, name  FROM hrassess_item_master where (assessgroupunkid = @assessgroupunkid or @assessgroupunkid = -1) AND isactive = 1  ORDER BY name "
            'End If

            strQ &= "SELECT assessitemunkid, name  FROM hrassess_item_master where (assessgroupunkid = @assessgroupunkid or @assessgroupunkid = -1) AND isactive = 1  "

            If intPeriodId >= 0 Then
                strQ &= " AND periodunkid = " & intPeriodId
            End If

            If blnSortById Then
                strQ &= " ORDER BY assessitemunkid "
            Else
                strQ &= " ORDER BY name "
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessGroupunkid)
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    'S.SANDEEP [ 29 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES 
    'TYPE : EMPLOYEMENT CONTRACT PROCESS
    Public Sub RemainingWeight(ByVal intAssessGrpId As Integer, ByRef mdecRemainingWeight As Decimal, ByRef mdecAssigned As Decimal, Optional ByVal intItemMasterId As Integer = -1)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT " & _
                   " (100 - ISNULL(SUM(weight),0)) AS Remaining " & _
                   ",ISNULL(SUM(weight),0) AS Assigned " & _
                   "FROM hrassess_item_master " & _
                   "WHERE assessgroupunkid = '" & intAssessGrpId & "' " & _
                   "   AND isactive = 1 "

            If intItemMasterId > 0 Then
                strQ &= " AND hrassess_item_master.assessitemunkid <> '" & intItemMasterId & "' "
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdecRemainingWeight = CDec(dsList.Tables(0).Rows(0)("Remaining"))
            mdecAssigned = CDec(dsList.Tables(0).Rows(0)("Assigned"))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: RemainingWeight; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 29 DEC 2011 ] -- END

    'Pinkal (06-Feb-2012) -- Start
    'Enhancement : TRA Changes
    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetResultGroupFromAssessmentGroup() As Integer
        Dim dsList As DataSet = Nothing
        Dim mintResultGroupUnkid As Integer = 0
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()
            strQ = "SELECT top 1 ISNULL(resultgroupunkid,0) AS resultgroupunkid FROM hrassess_item_master Where isactive = 1"
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mintResultGroupUnkid = CInt(dsList.Tables(0).Rows(0)("resultgroupunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetResultGroupFromAssessmentGroup; Module Name: " & mstrModuleName)
        End Try
        Return mintResultGroupUnkid
    End Function
    'Pinkal (06-Feb-2012) -- End

    'S.SANDEEP [ 14 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Get_Item_Id(ByVal iCode As String, ByVal iName As String, ByVal iGroupId As Integer, ByRef iWeight As Decimal) As Integer
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = "" : Dim exForce As Exception
        Dim iValue As Integer
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT assessitemunkid " & _
                   " ,ISNULL(weight,0) AS weight " & _
                   "FROM hrassess_item_master WHERE isactive = 1 "

            If iCode.Trim.Length > 0 Then
                StrQ &= " AND code = '" & iCode & "' "
            End If

            If iName.Trim.Length > 0 Then
                StrQ &= " AND name = '" & iName & "' "
            End If

            If iGroupId > 0 Then
                StrQ &= " AND assessgroupunkid = '" & iGroupId & "' "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                iValue = dsList.Tables("List").Rows(0).Item("assessitemunkid")
                iWeight = CDec(dsList.Tables("List").Rows(0).Item("weight"))
            End If

            Return iValue

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Item_Id; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 14 AUG 2013 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Assess Item Code is already defined. Please define new Assess Item Code.")
			Language.setMessage(mstrModuleName, 2, "This Assess Item Name is already defined. Please define new Assess Item Name.")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class