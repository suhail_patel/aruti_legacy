﻿'************************************************************************************************************************************
'Class Name : clsassess_ratio_tran.vb
'Purpose    :
'Date       :14-Mar-2016
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clsassess_ratio_tran
    Private Shared ReadOnly mstrModuleName As String = "clsassess_ratio_tran"
    Dim mstrMessage As String = ""

    Public Property _Message() As String
        Get
            Return mstrMessage
        End Get
        Set(ByVal value As String)
            mstrMessage = value
        End Set
    End Property

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (clsassess_ratio_tran) </purpose>
    Public Function Insert(ByVal objDataOperation As clsDataOperation, _
                           ByVal intRatioMstUnkid As Integer, _
                           ByVal strAllocationIds As String, _
                           ByVal intAllocRefUnkid As Integer, _
                           ByVal intUserUnkid As Integer, _
                           ByVal dtDateTime As DateTime, _
                           ByVal blnIsInSert As Boolean) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mintRatioTranUnkId As Integer = -1
        Dim dsAllocated As New DataSet
        Dim blnFlagIsInserted As Boolean = False
        Dim blnIsUpdate As Boolean = False
        Try
            If blnIsInSert = False Then
            dsAllocated = GetRatioAllocations(objDataOperation, intRatioMstUnkid)
            If dsAllocated.Tables(0).Rows.Count > 0 Then
                blnIsUpdate = True
                Dim dtTemp() As DataRow = dsAllocated.Tables(0).Select("allocationid NOT IN(" & strAllocationIds & ")")
                If dtTemp.Length > 0 Then
                    For i As Integer = 0 To dtTemp.Length - 1
                        Call Delete(objDataOperation, dtTemp(i)("ratiotranunkid"), intUserUnkid, dtDateTime)
                        If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_ratio_master", "ratiounkid", intRatioMstUnkid, "hrassess_ratio_tran", "ratiotranunkid", dtTemp(i)("ratiotranunkid"), 2, 3) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next
                End If
            End If
            End If
            For Each StrId As String In strAllocationIds.Split(",")
                If isExist(objDataOperation, intRatioMstUnkid, StrId) = True Then Continue For
                objDataOperation.ClearParameters() : mintRatioTranUnkId = -1

                objDataOperation.AddParameter("@ratiounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRatioMstUnkid.ToString)
                objDataOperation.AddParameter("@allocationid", SqlDbType.Int, eZeeDataType.INT_SIZE, StrId)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")

                strQ = "INSERT INTO hrassess_ratio_tran ( " & _
                           "  ratiounkid " & _
                           ", allocationid " & _
                           ", isvoid " & _
                           ", voiduserunkid " & _
                           ", voiddatetime " & _
                           ", voidreason" & _
                       ") VALUES (" & _
                           "  @ratiounkid " & _
                           ", @allocationid " & _
                           ", @isvoid " & _
                           ", @voiduserunkid " & _
                           ", @voiddatetime " & _
                           ", @voidreason" & _
                       "); SELECT @@identity"

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintRatioTranUnkId = dsList.Tables(0).Rows(0).Item(0)

                If blnFlagIsInserted = False Then blnFlagIsInserted = True

                If blnIsUpdate = False Then
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_ratio_master", "ratiounkid", intRatioMstUnkid, "hrassess_ratio_tran", "ratiotranunkid", mintRatioTranUnkId, 1, 1) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Else
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_ratio_master", "ratiounkid", intRatioMstUnkid, "hrassess_ratio_tran", "ratiotranunkid", mintRatioTranUnkId, 2, 1) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            Next

            If blnFlagIsInserted = False Then
                If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hrassess_ratio_master", intRatioMstUnkid, "ratiounkid", 2) Then
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_ratio_master", "ratiounkid", intRatioMstUnkid, "hrassess_ratio_tran", "ratiotranunkid", -1, 2, 0) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:Insert ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function GetRatioAllocations(ByVal objDataOperation As clsDataOperation, ByVal intRatioGrpId As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ &= "SELECT " & _
                    " ratiotranunkid " & _
                    ",ratiounkid " & _
                    ",allocationid " & _
                    ",isvoid " & _
                    ",voiduserunkid " & _
                    ",voiddatetime " & _
                    ",voidreason " & _
                    "FROM hrassess_ratio_tran " & _
                    "WHERE ratiounkid = '" & intRatioGrpId & "' AND isvoid = 0 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:GetRatioAllocations ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function isExist(ByVal objDataOperation As clsDataOperation, ByVal intRatioMstId As Integer, ByVal intAllocationId As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Try
            strQ = "SELECT 1 FROM hrassess_ratio_tran WHERE ratiounkid = '" & intRatioMstId & "' AND allocationid = '" & intAllocationId & "' AND isvoid = 0 "


            If objDataOperation.RecordCount(strQ) <= 0 Then
                blnFlag = False
            Else
                blnFlag = True
            End If


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return blnFlag
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:isExist ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Delete(ByVal objDataOperation As clsDataOperation, ByVal intRatioTranId As Integer, ByVal intUserId As Integer, ByVal dtDate As DateTime) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "UPDATE hrassess_ratio_tran " & _
                   " SET isvoid = 1, voiduserunkid = @voiduserunkid, voiddatetime = @voiddatetime, voidreason = @voidreason " & _
                   "WHERE ratiotranunkid = @ratiotranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@ratiotranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRatioTranId)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtDate)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 1, "Un-assigned"))

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:Delete ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

End Class
