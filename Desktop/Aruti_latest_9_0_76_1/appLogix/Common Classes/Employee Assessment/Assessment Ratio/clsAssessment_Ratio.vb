﻿'************************************************************************************************************************************
'Class Name : clsAssessment_Ratio.vb
'Purpose    :
'Date       :09-Aug-13
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsAssessment_Ratio
    Private Shared ReadOnly mstrModuleName As String = "clsAssessment_Ratio"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintRatiounkid As Integer
    Private mintJobgroupunkid As Integer
    Private mintPeriodunkid As Integer
    Private mdecGe_Weight As Decimal
    Private mdecBsc_Weight As Decimal
    Private mblnIsactive As Boolean = True

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ratiounkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ratiounkid() As Integer
        Get
            Return mintRatiounkid
        End Get
        Set(ByVal value As Integer)
            mintRatiounkid = value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobgroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Jobgroupunkid() As Integer
        Get
            Return mintJobgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintJobgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ge_weight
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ge_Weight() As Decimal
        Get
            Return mdecGe_Weight
        End Get
        Set(ByVal value As Decimal)
            mdecGe_Weight = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set bsc_weight
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Bsc_Weight() As Decimal
        Get
            Return mdecBsc_Weight
        End Get
        Set(ByVal value As Decimal)
            mdecBsc_Weight = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  ratiounkid " & _
              ", jobgroupunkid " & _
              ", periodunkid " & _
              ", ge_weight " & _
              ", bsc_weight " & _
              ", isactive " & _
             "FROM hrassessment_ratio " & _
             "WHERE ratiounkid = @ratiounkid "

            objDataOperation.AddParameter("@ratiounkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintRatioUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintratiounkid = CInt(dtRow.Item("ratiounkid"))
                mintjobgroupunkid = CInt(dtRow.Item("jobgroupunkid"))
                mintperiodunkid = CInt(dtRow.Item("periodunkid"))
                mdecGe_Weight = dtRow.Item("ge_weight")
                mdecBsc_Weight = dtRow.Item("bsc_weight")
                mblnisactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  hrassessment_ratio.ratiounkid " & _
                   ", hrassessment_ratio.jobgroupunkid " & _
                   ", hrassessment_ratio.periodunkid " & _
                   ", hrassessment_ratio.ge_weight " & _
                   ", hrassessment_ratio.bsc_weight " & _
                   ", hrassessment_ratio.isactive " & _
                   ", ISNULL(hrjobgroup_master.name,'') AS Job_Group " & _
                   ", ISNULL(cfcommon_period_tran.period_name,'') AS Period " & _
                   ", cfcommon_period_tran.statusid " & _
                   "FROM hrassessment_ratio " & _
                   "  JOIN cfcommon_period_tran ON hrassessment_ratio.periodunkid = cfcommon_period_tran.periodunkid " & _
                   "  JOIN hrjobgroup_master ON hrassessment_ratio.jobgroupunkid = hrjobgroup_master.jobgroupunkid "

            If blnOnlyActive Then
                strQ &= " WHERE hrassessment_ratio.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassessment_ratio) </purpose>
    Public Function Insert() As Boolean
        If isExist(mintJobgroupunkid, mintPeriodunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Ratio for BSC and General Assessment is already present for the selected Job Group(s) and Period(s)." & _
                                              "As a result, The following Job Group and Period will be skipped.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintjobgroupunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintperiodunkid.ToString)
            objDataOperation.AddParameter("@ge_weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecGe_Weight)
            objDataOperation.AddParameter("@bsc_weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBsc_Weight)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

            StrQ = "INSERT INTO hrassessment_ratio ( " & _
              "  jobgroupunkid " & _
              ", periodunkid " & _
              ", ge_weight " & _
              ", bsc_weight " & _
              ", isactive" & _
            ") VALUES (" & _
              "  @jobgroupunkid " & _
              ", @periodunkid " & _
              ", @ge_weight " & _
              ", @bsc_weight " & _
              ", @isactive" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintRatiounkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassessment_ratio", "ratiounkid", mintRatiounkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassessment_ratio) </purpose>
    Public Function Update() As Boolean
        If isExist(mintJobgroupunkid, mintPeriodunkid, mintRatiounkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Ratio for BSC and General Assessment is already present for the selected Job Group(s) and Period(s)." & _
                                              "As a result, The following Job Group and Period will be skipped.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@ratiounkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintratiounkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintjobgroupunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintperiodunkid.ToString)
            objDataOperation.AddParameter("@ge_weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecGe_Weight)
            objDataOperation.AddParameter("@bsc_weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBsc_Weight)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)

            strQ = "UPDATE hrassessment_ratio SET " & _
                     "  jobgroupunkid = @jobgroupunkid" & _
                     ", periodunkid = @periodunkid" & _
                     ", ge_weight = @ge_weight" & _
                     ", bsc_weight = @bsc_weight" & _
                     ", isactive = @isactive " & _
                     "WHERE ratiounkid = @ratiounkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassessment_ratio", "ratiounkid", mintRatiounkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassessment_ratio) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "UPDATE hrassessment_ratio SET " & _
                     " isactive = 0 " & _
                     "WHERE ratiounkid = @ratiounkid "

            objDataOperation.AddParameter("@ratiounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iJobGroupId As Integer, ByVal iPeriodId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  ratiounkid " & _
                   ", jobgroupunkid " & _
                   ", periodunkid " & _
                   ", ge_weight " & _
                   ", bsc_weight " & _
                   ", isactive " & _
                   "FROM hrassessment_ratio " & _
                   "WHERE jobgroupunkid = @jobgroupunkid " & _
                   "AND periodunkid = @periodunkid AND isactive = 1 "

            If intUnkid > 0 Then
                strQ &= " AND ratiounkid <> @ratiounkid"
            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iJobGroupId)
            objDataOperation.AddParameter("@ratiounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Sub Get_Ratio(ByRef iGE_Ratio As Decimal, ByRef iBSC_Ratio As Decimal, ByVal iJobGrpId As Integer, ByVal iPeriodId As Integer)
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                   " ge_weight ,bsc_weight " & _
                   "FROM hrassessment_ratio " & _
                   "WHERE periodunkid = '" & iPeriodId & "' AND jobgroupunkid = '" & iJobGrpId & "' " & _
                   "AND isactive = 1 "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                iGE_Ratio = dsList.Tables("List").Rows(0).Item("ge_weight")
                iBSC_Ratio = dsList.Tables("List").Rows(0).Item("bsc_weight")
            Else
                iGE_Ratio = 0 : iBSC_Ratio = 0
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Ratio", mstrModuleName)
        Finally
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Ratio for BSC and General Assessment is already present for the selected Job Group(s")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
