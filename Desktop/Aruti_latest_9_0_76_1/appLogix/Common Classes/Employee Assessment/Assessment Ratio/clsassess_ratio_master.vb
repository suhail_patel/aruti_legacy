﻿'************************************************************************************************************************************
'Class Name : clsassess_ratio_master.vb
'Purpose    :
'Date       :14-Mar-2016
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clsassess_ratio_master
    Private Shared ReadOnly mstrModuleName As String = "clsassess_ratio_master"
    Dim mstrMessage As String = ""
    Dim objRatioTran As New clsassess_ratio_tran


#Region " Private variables "

    Private mintRatiounkid As Integer = 0
    Private mintAllocrefunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mDecGe_Weight As Decimal = 0
    Private mDecBsc_Weight As Decimal = 0
    Private mintEmployeeunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = -1
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    Private mintUserunkid As Integer = 0
    Private mstrRatioAllocationIds As String = String.Empty

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ratiounkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ratiounkid() As Integer
        Get
            Return mintRatiounkid
        End Get
        Set(ByVal value As Integer)
            mintRatiounkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocrefunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Allocrefunkid() As Integer
        Get
            Return mintAllocrefunkid
        End Get
        Set(ByVal value As Integer)
            mintAllocrefunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ge_weight
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ge_Weight() As Decimal
        Get
            Return mDecGe_Weight
        End Get
        Set(ByVal value As Decimal)
            mDecGe_Weight = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set bsc_weight
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Bsc_Weight() As Decimal
        Get
            Return mDecBsc_Weight
        End Get
        Set(ByVal value As Decimal)
            mDecBsc_Weight = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get ratioallocationids
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _RatioAllocationIds() As String
        Get
            Return mstrRatioAllocationIds
        End Get
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  ratiounkid " & _
              ", allocrefunkid " & _
              ", periodunkid " & _
              ", ge_weight " & _
              ", bsc_weight " & _
              ", employeeunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", userunkid " & _
             "FROM hrassess_ratio_master " & _
             "WHERE ratiounkid = @ratiounkid "

            objDataOperation.AddParameter("@ratiounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRatiounkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintRatiounkid = CInt(dtRow.Item("ratiounkid"))
                mintAllocrefunkid = CInt(dtRow.Item("allocrefunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mDecGe_Weight = CDec(dtRow.Item("ge_weight"))
                mDecBsc_Weight = CDec(dtRow.Item("bsc_weight"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                Exit For
            Next

            GetAllocationIds(mintRatiounkid, objDataOperation)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal strFilterString As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "     cfcommon_period_tran.period_name " & _
                   "    ,CASE WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.BRANCH & "  THEN @BRANCH " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.DEPARTMENT_GROUP & "  THEN @DEPARTMENT_GROUP " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.DEPARTMENT & "  THEN @DEPARTMENT " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.SECTION_GROUP & "  THEN @SECTION_GROUP " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.SECTION & "  THEN @SECTION " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.UNIT_GROUP & "  THEN @UNIT_GROUP " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.UNIT & "  THEN @UNIT " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.TEAM & "  THEN @TEAM " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.JOB_GROUP & "  THEN @JOB_GROUP " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.JOBS & " THEN @JOBS " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.CLASS_GROUP & " THEN @CLASS_GROUP " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.CLASSES & " THEN @CLASSES " & _
                   "     END AS allocation_type " & _
                   "    ,CASE WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.BRANCH & "  THEN ISNULL(ST.name,'') " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.DEPARTMENT_GROUP & "  THEN ISNULL(DG.name,'') " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.DEPARTMENT & "  THEN ISNULL(DM.name,'') " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.SECTION_GROUP & "  THEN ISNULL(SG.name,'') " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.SECTION & "  THEN ISNULL(SM.name,'') " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.UNIT_GROUP & "  THEN ISNULL(UG.name,'') " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.UNIT & "  THEN ISNULL(UM.name,'') " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.TEAM & "  THEN ISNULL(TM.name,'') " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.JOB_GROUP & "  THEN ISNULL(JG.name,'') " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.JOBS & " THEN ISNULL(JM.job_name,'') " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.CLASS_GROUP & " THEN ISNULL(CG.name,'') " & _
                   "          WHEN hrassess_ratio_master.allocrefunkid = " & enAllocation.CLASSES & " THEN ISNULL(CM.name,'') " & _
                   "     END AS allocation_name " & _
                   "    ,hrassess_ratio_master.ge_weight " & _
                   "    ,hrassess_ratio_master.bsc_weight " & _
                   "    ,cfcommon_period_tran.statusid " & _
                   "    ,hrassess_ratio_master.ratiounkid " & _
                   "    ,hrassess_ratio_tran.ratiotranunkid " & _
                   "    ,hrassess_ratio_tran.allocationid " & _
                   "    ,hrassess_ratio_master.allocrefunkid " & _
                   "    ,hrassess_ratio_master.periodunkid " & _
                   "    ,hrassess_ratio_master.employeeunkid " & _
                   "    ,hrassess_ratio_master.isvoid " & _
                   "    ,hrassess_ratio_master.voiduserunkid " & _
                   "    ,hrassess_ratio_master.voiddatetime " & _
                   "    ,hrassess_ratio_master.voidreason " & _
                   "FROM hrassess_ratio_master " & _
                   "    JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = hrassess_ratio_master.periodunkid " & _
                   "    JOIN hrassess_ratio_tran ON hrassess_ratio_master.ratiounkid = hrassess_ratio_tran.ratiounkid " & _
                   "    LEFT JOIN hrstation_master AS ST ON ST.stationunkid = hrassess_ratio_tran.allocationid " & _
                   "    LEFT JOIN hrdepartment_group_master AS DG ON DG.deptgroupunkid = hrassess_ratio_tran.allocationid " & _
                   "    LEFT JOIN hrdepartment_master AS DM ON DM.departmentunkid = hrassess_ratio_tran.allocationid " & _
                   "    LEFT JOIN hrsectiongroup_master AS SG ON SG.sectiongroupunkid = hrassess_ratio_tran.allocationid " & _
                   "    LEFT JOIN hrsection_master AS SM ON SM.sectionunkid = hrassess_ratio_tran.allocationid " & _
                   "    LEFT JOIN hrunitgroup_master AS UG ON UG.unitgroupunkid = hrassess_ratio_tran.allocationid " & _
                   "    LEFT JOIN hrunit_master AS UM ON UM.unitunkid = hrassess_ratio_tran.allocationid " & _
                   "    LEFT JOIN hrteam_master AS TM ON TM.teamunkid = hrassess_ratio_tran.allocationid " & _
                   "    LEFT JOIN hrjobgroup_master AS JG ON JG.jobgroupunkid = hrassess_ratio_tran.allocationid " & _
                   "    LEFT JOIN hrjob_master AS JM ON JM.jobunkid = hrassess_ratio_tran.allocationid " & _
                   "    LEFT JOIN hrclassgroup_master AS CG ON CG.classgroupunkid = hrassess_ratio_tran.allocationid " & _
                   "    LEFT JOIN hrclasses_master AS CM ON CM.classesunkid = hrassess_ratio_tran.allocationid " & _
                   "WHERE hrassess_ratio_master.isvoid = 0 AND hrassess_ratio_tran.isvoid = 0 "

            If strFilterString.Trim.Length > 0 Then
                strQ &= " AND " & strFilterString
            End If

            objDataOperation.AddParameter("@BRANCH", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 430, "Branch"))
            objDataOperation.AddParameter("@DEPARTMENT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 429, "Department Group"))
            objDataOperation.AddParameter("@DEPARTMENT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 428, "Department"))
            objDataOperation.AddParameter("@SECTION_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 427, "Section Group"))
            objDataOperation.AddParameter("@SECTION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 426, "Section"))
            objDataOperation.AddParameter("@UNIT_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 425, "Unit Group"))
            objDataOperation.AddParameter("@UNIT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 424, "Unit"))
            objDataOperation.AddParameter("@TEAM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 423, "Team"))
            objDataOperation.AddParameter("@JOB_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 422, "Job Group"))
            objDataOperation.AddParameter("@JOBS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 421, "Jobs"))
            objDataOperation.AddParameter("@CLASS_GROUP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 420, "Class Group"))
            objDataOperation.AddParameter("@CLASSES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 419, "Classes"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_ratio_master) </purpose>
    Public Function Insert(ByVal strAllocationIds As String, ByVal dtCurrentDateTime As DateTime) As Boolean
        Dim intMstRatioId As Integer = 0
        If isExist(mintAllocrefunkid, mintPeriodunkid, mDecGe_Weight, mDecBsc_Weight, -1, intMstRatioId) Then
            If intMstRatioId <= 0 Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Assessment Ratio is already present for allocation for the selected period.")
            Return False
            End If    
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            If intMstRatioId <= 0 Then
            objDataOperation.AddParameter("@allocrefunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocrefunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@ge_weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecGe_Weight)
            objDataOperation.AddParameter("@bsc_weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecBsc_Weight)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)

            strQ = "INSERT INTO hrassess_ratio_master ( " & _
              "  allocrefunkid " & _
              ", periodunkid " & _
              ", ge_weight " & _
              ", bsc_weight " & _
              ", employeeunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", userunkid " & _
            ") VALUES (" & _
              "  @allocrefunkid " & _
              ", @periodunkid " & _
              ", @ge_weight " & _
              ", @bsc_weight " & _
              ", @employeeunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @userunkid " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintRatiounkid = dsList.Tables(0).Rows(0).Item(0)
            Else
                mintRatiounkid = intMstRatioId
            End If
            
            If objRatioTran.Insert(objDataOperation, mintRatiounkid, strAllocationIds, mintAllocrefunkid, mintUserunkid, dtCurrentDateTime, True) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_ratio_master) </purpose>
    Public Function Update(ByVal strAllocationIds As String, ByVal dtCurrentDateTime As DateTime) As Boolean
        If isExist(mintAllocrefunkid, mintPeriodunkid, mDecGe_Weight, mDecBsc_Weight, mintRatiounkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Assessment Ratio is already present for allocation for the selected period.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@ratiounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRatiounkid.ToString)
            objDataOperation.AddParameter("@allocrefunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocrefunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@ge_weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecGe_Weight)
            objDataOperation.AddParameter("@bsc_weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mDecBsc_Weight)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)

            strQ = "UPDATE hrassess_ratio_master SET " & _
                   "  allocrefunkid = @allocrefunkid" & _
                   ", periodunkid = @periodunkid" & _
                   ", ge_weight = @ge_weight" & _
                   ", bsc_weight = @bsc_weight" & _
                   ", employeeunkid = @employeeunkid" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   ", userunkid = @userunkid " & _
                   "WHERE ratiounkid = @ratiounkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objRatioTran.Insert(objDataOperation, mintRatiounkid, strAllocationIds, mintAllocrefunkid, mintUserunkid, dtCurrentDateTime, False) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_ratio_master) </purpose>
    Public Function Delete(ByVal mDicDeleteIds As Dictionary(Of Integer, String)) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            If mDicDeleteIds.Keys.Count > 0 Then
                Dim blnVoidMaster As Boolean = False
                Dim iRowCount As Integer = 0 : Dim intAuditValue As Integer = 2
                For Each iKey As Integer In mDicDeleteIds.Keys

                    iRowCount = objDataOperation.RecordCount("SELECT 1 FROM hrassess_ratio_tran WHERE hrassess_ratio_tran.ratiounkid = '" & iKey & "'")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If iRowCount = mDicDeleteIds(iKey).Split(",").Count Then
                        blnVoidMaster = True
                    End If

                    If blnVoidMaster = True Then

                        strQ = "UPDATE hrassess_ratio_master SET " & _
                               "  isvoid = @isvoid" & _
                               ", voiduserunkid = @voiduserunkid" & _
                               ", voiddatetime = @voiddatetime" & _
                               ", voidreason = @voidreason " & _
                               "WHERE ratiounkid = @ratiounkid "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@ratiounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iKey)
                        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


                        Call objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        intAuditValue = 3

                    End If
                    Dim strTranIds As String() = mDicDeleteIds(iKey).Split(",")
                    For index As Integer = 0 To strTranIds.Length - 1

                        strQ = "UPDATE hrassess_ratio_tran SET " & _
                               "  isvoid = @isvoid" & _
                               ", voiduserunkid = @voiduserunkid" & _
                               ", voiddatetime = @voiddatetime" & _
                               ", voidreason = @voidreason " & _
                               "WHERE ratiotranunkid = @ratiotranunkid "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@ratiotranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strTranIds(index))
                        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


                        Call objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_ratio_master", "ratiounkid", iKey, "hrassess_ratio_tran", "ratiotranunkid", strTranIds(index), intAuditValue, enAuditType.DELETE, False, mintVoiduserunkid) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Next

                Next
            End If


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intAllocrefunkid As Integer, _
                            ByVal iPeriodId As Integer, _
                            ByVal decGEWgt As Decimal, _
                            ByVal decBSCWgt As Decimal, _
                            Optional ByVal intUnkid As Integer = -1, _
                            Optional ByRef intMastRatioId As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  ratiounkid " & _
                   ", allocrefunkid " & _
                   ", periodunkid " & _
                   ", ge_weight " & _
                   ", bsc_weight " & _
                   ", employeeunkid " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", voidreason " & _
                   "FROM hrassess_ratio_master " & _
                   "WHERE ge_weight = @ge_weight AND bsc_weight = @bsc_weight " & _
                   "AND periodunkid = @periodunkid AND allocrefunkid = @allocrefunkid AND isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND ratiounkid <> @ratiounkid"
                objDataOperation.AddParameter("@ratiounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)
            objDataOperation.AddParameter("@ge_weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decGEWgt)
            objDataOperation.AddParameter("@bsc_weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, decBSCWgt)
            objDataOperation.AddParameter("@allocrefunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocrefunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intMastRatioId = dsList.Tables(0).Rows(0)("ratiounkid")
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Private Sub GetAllocationIds(ByVal intRatioMstId As Integer, ByVal objDataOperation As clsDataOperation)
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Try
            strQ = "SELECT ISNULL(STUFF " & _
                   "((SELECT " & _
                         "',' + CAST(hrassess_ratio_tran.allocationid AS NVARCHAR(50)) " & _
                      "FROM hrassess_ratio_tran " & _
                      "WHERE hrassess_ratio_tran.ratiounkid = '" & intRatioMstId & "' AND hrassess_ratio_tran.isvoid = 0 " & _
                      "ORDER BY hrassess_ratio_tran.allocationid FOR XML PATH('')), 1, 1, '' " & _
                    "),'') AS Allocation "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                mstrRatioAllocationIds = dsList.Tables("List").Rows(0)("Allocation")
            Else
                mstrRatioAllocationIds = ""
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:GetAllocationIds ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function GetAllocationIdsByPeriod(ByVal intPeriodId As Integer) As String
        Dim strRatioAllocationIds As String = String.Empty
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Try
            strQ = "SELECT ISNULL(STUFF " & _
                    "				((SELECT " & _
                    "					',' + CAST(hrassess_ratio_tran.allocationid AS NVARCHAR(50)) " & _
                    "				  FROM hrassess_ratio_tran " & _
                    "					JOIN hrassess_ratio_master ON hrassess_ratio_master.ratiounkid = hrassess_ratio_tran.ratiounkid " & _
                    "				  WHERE hrassess_ratio_master.isvoid = 0 AND hrassess_ratio_tran.isvoid = 0 " & _
                    "					AND hrassess_ratio_master.periodunkid = '" & intPeriodId & "' " & _
                    "				  ORDER BY hrassess_ratio_tran.allocationid FOR XML PATH('')), 1, 1, '' " & _
                    "             ),'') AS Allocation "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                strRatioAllocationIds = dsList.Tables("List").Rows(0)("Allocation")
            Else
                strRatioAllocationIds = ""
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return strRatioAllocationIds
    End Function

    Public Function GetAllocReferenceId(ByVal intPeriodId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intAllocationRefId As Integer = 0
        Dim objDataOperation As New clsDataOperation
        Try
            strQ = "SELECT " & _
                   "  allocrefunkid " & _
                   "FROM hrassess_ratio_master " & _
                   "WHERE periodunkid = @periodunkid AND isvoid = 0 "

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intAllocationRefId = dsList.Tables(0).Rows(0).Item("allocrefunkid")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:GetAllocReferenceId ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return intAllocationRefId
    End Function

    Public Sub Get_Ratio(ByRef dblGeneralPercent As Decimal, ByRef dblBSCPercent As Decimal, ByVal intPeriodId As Integer, ByVal intEmployeeId As Integer, ByVal dtEffDate As Date)
        Try
            Dim dsList As New DataSet
            Dim StrQ As String = String.Empty
            Dim intAllocRefId As Integer = 0
            Dim objRatio As New clsassess_ratio_master

            intAllocRefId = objRatio.GetAllocReferenceId(intPeriodId)

            Using objDo As New clsDataOperation

                Dim StrQJoin As String = ""
                Dim StrQInnr As String = ""
                Dim StrQColName As String = ""

                Select Case intAllocRefId
                    Case enAllocation.JOBS, enAllocation.JOB_GROUP
                        StrQInnr = "    SELECT " & _
                                   "         jobgroupunkid " & _
                                   "        ,jobunkid " & _
                                   "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                   "        ,employeeunkid " & _
                                   "    FROM hremployee_categorization_tran " & _
                                   "    WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEffDate) & "' " & _
                                   "        AND hremployee_categorization_tran.employeeunkid = '" & intEmployeeId & "' "

                        Select Case intAllocRefId
                            Case enAllocation.JOB_GROUP
                                StrQColName = " A.jobgroupunkid "
                            Case enAllocation.JOBS
                                StrQColName = " A.jobunkid "
                        End Select

                    Case enAllocation.COST_CENTER
                    Case enAllocation.EMPLOYEE
                    Case Else
                        StrQInnr = "SELECT " & _
                                   "     employeeunkid " & _
                                   "    ,stationunkid " & _
                                   "    ,deptgroupunkid " & _
                                   "    ,departmentunkid " & _
                                   "    ,sectiongroupunkid " & _
                                   "    ,sectionunkid " & _
                                   "    ,unitgroupunkid " & _
                                   "    ,unitunkid " & _
                                   "    ,teamunkid " & _
                                   "    ,classgroupunkid " & _
                                   "    ,classunkid " & _
                                   "    ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                   "FROM hremployee_transfer_tran " & _
                                   "WHERE isvoid = 0 AND CONVERT(NVARCHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(dtEffDate) & "' " & _
                                   " AND hremployee_transfer_tran.employeeunkid = '" & intEmployeeId & "' "

                        Select Case intAllocRefId
                            Case enAllocation.BRANCH
                                StrQColName = " A.stationunkid "
                            Case enAllocation.DEPARTMENT_GROUP
                                StrQColName = " A.deptgroupunkid "
                            Case enAllocation.DEPARTMENT
                                StrQColName = " A.departmentunkid "
                            Case enAllocation.SECTION_GROUP
                                StrQColName = " A.sectiongroupunkid "
                            Case enAllocation.SECTION
                                StrQColName = " A.sectionunkid "
                            Case enAllocation.UNIT_GROUP
                                StrQColName = " A.unitgroupunkid "
                            Case enAllocation.UNIT
                                StrQColName = " A.unitunkid "
                            Case enAllocation.TEAM
                                StrQColName = " A.teamunkid "
                            Case enAllocation.CLASS_GROUP
                                StrQColName = " A.classgroupunkid "
                            Case enAllocation.CLASSES
                                StrQColName = " A.classunkid "
                        End Select

                End Select

                StrQJoin = " JOIN hrassess_ratio_tran ON " & StrQColName & " = hrassess_ratio_tran.allocationid " & _
                           " JOIN hrassess_ratio_master ON hrassess_ratio_master.ratiounkid = hrassess_ratio_tran.ratiounkid AND hrassess_ratio_master.periodunkid = '" & intPeriodId & "' "

                StrQ = "SELECT " & _
                       "     ISNULL(bsc_weight,0) AS G_Ratio " & _
                       "    ,ISNULL(ge_weight,0) AS C_Ratio " & _
                       "FROM " & _
                       "( " & _
                       " " & StrQInnr & " " & _
                       ") AS A " & _
                       " " & StrQJoin & " " & _
                       "WHERE A.rno = 1 AND hrassess_ratio_master.allocrefunkid = '" & intAllocRefId & "' AND hrassess_ratio_master.isvoid = 0 AND hrassess_ratio_tran.isvoid = 0 "

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                If dsList.Tables("List").Rows.Count > 0 Then
                    If IsDBNull(dsList.Tables("List").Rows(0)("G_Ratio")) = False Then
                        dblGeneralPercent = CDbl(dsList.Tables("List").Rows(0)("G_Ratio"))
                    End If
                    If IsDBNull(dsList.Tables("List").Rows(0)("C_Ratio")) = False Then
                        dblBSCPercent = CDbl(dsList.Tables("List").Rows(0)("C_Ratio"))
                    End If
                Else
                    dblGeneralPercent = 0 : dblBSCPercent = 0
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:Get_Ratio ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Assessment Ratio is already present for allocation for the selected period.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'------------SANDEEP SHARMA [14 MAR 2016] ----------------- ORIGINAL CLASS BEFORE CCBRT CHANGES BASED ON ALLOCATION -----------------------------|
'Public Class clsAssessment_Ratio
'    Private Shared ReadOnly mstrModuleName As String = "clsAssessment_Ratio"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""

'#Region " Private variables "

'    Private mintRatiounkid As Integer
'    Private mintJobgroupunkid As Integer
'    Private mintPeriodunkid As Integer
'    Private mdecGe_Weight As Decimal
'    Private mdecBsc_Weight As Decimal
'    Private mblnIsactive As Boolean = True

'#End Region

'#Region " Properties "

'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set ratiounkid
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Ratiounkid() As Integer
'        Get
'            Return mintRatiounkid
'        End Get
'        Set(ByVal value As Integer)
'            mintRatiounkid = value
'            Call GetData()
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set jobgroupunkid
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Jobgroupunkid() As Integer
'        Get
'            Return mintJobgroupunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintJobgroupunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set periodunkid
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Periodunkid() As Integer
'        Get
'            Return mintPeriodunkid
'        End Get
'        Set(ByVal value As Integer)
'            mintPeriodunkid = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set ge_weight
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Ge_Weight() As Decimal
'        Get
'            Return mdecGe_Weight
'        End Get
'        Set(ByVal value As Decimal)
'            mdecGe_Weight = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set bsc_weight
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Bsc_Weight() As Decimal
'        Get
'            Return mdecBsc_Weight
'        End Get
'        Set(ByVal value As Decimal)
'            mdecBsc_Weight = value
'        End Set
'    End Property

'    ''' <summary>
'    ''' Purpose: Get or Set isactive
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    Public Property _Isactive() As Boolean
'        Get
'            Return mblnIsactive
'        End Get
'        Set(ByVal value As Boolean)
'            mblnIsactive = value
'        End Set
'    End Property

'#End Region

'    ''' <summary>
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Sub GetData()
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'              "  ratiounkid " & _
'              ", jobgroupunkid " & _
'              ", periodunkid " & _
'              ", ge_weight " & _
'              ", bsc_weight " & _
'              ", isactive " & _
'             "FROM hrassessment_ratio " & _
'             "WHERE ratiounkid = @ratiounkid "

'            objDataOperation.AddParameter("@ratiounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRatiounkid.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            For Each dtRow As DataRow In dsList.Tables(0).Rows
'                mintRatiounkid = CInt(dtRow.Item("ratiounkid"))
'                mintJobgroupunkid = CInt(dtRow.Item("jobgroupunkid"))
'                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
'                mdecGe_Weight = dtRow.Item("ge_weight")
'                mdecBsc_Weight = dtRow.Item("bsc_weight")
'                mblnIsactive = CBool(dtRow.Item("isactive"))
'                Exit For
'            Next
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Sub

'    ''' <summary>
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                   "  hrassessment_ratio.ratiounkid " & _
'                   ", hrassessment_ratio.jobgroupunkid " & _
'                   ", hrassessment_ratio.periodunkid " & _
'                   ", hrassessment_ratio.ge_weight " & _
'                   ", hrassessment_ratio.bsc_weight " & _
'                   ", hrassessment_ratio.isactive " & _
'                   ", ISNULL(hrjobgroup_master.name,'') AS Job_Group " & _
'                   ", ISNULL(cfcommon_period_tran.period_name,'') AS Period " & _
'                   ", cfcommon_period_tran.statusid " & _
'                   "FROM hrassessment_ratio " & _
'                   "  JOIN cfcommon_period_tran ON hrassessment_ratio.periodunkid = cfcommon_period_tran.periodunkid " & _
'                   "  JOIN hrjobgroup_master ON hrassessment_ratio.jobgroupunkid = hrjobgroup_master.jobgroupunkid "

'            If blnOnlyActive Then
'                strQ &= " WHERE hrassessment_ratio.isactive = 1 "
'            End If

'            dsList = objDataOperation.ExecQuery(strQ, strTableName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'        Return dsList
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> INSERT INTO Database Table (hrassessment_ratio) </purpose>
'    Public Function Insert() As Boolean
'        If isExist(mintJobgroupunkid, mintPeriodunkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "Ratio for BSC and General Assessment is already present for the selected Job Group(s) and Period(s)." & _
'                                              "As a result, The following Job Group and Period will be skipped.")
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobgroupunkid.ToString)
'            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
'            objDataOperation.AddParameter("@ge_weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecGe_Weight)
'            objDataOperation.AddParameter("@bsc_weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBsc_Weight)
'            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

'            strQ = "INSERT INTO hrassessment_ratio ( " & _
'              "  jobgroupunkid " & _
'              ", periodunkid " & _
'              ", ge_weight " & _
'              ", bsc_weight " & _
'              ", isactive" & _
'            ") VALUES (" & _
'              "  @jobgroupunkid " & _
'              ", @periodunkid " & _
'              ", @ge_weight " & _
'              ", @bsc_weight " & _
'              ", @isactive" & _
'            "); SELECT @@identity"

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mintRatiounkid = dsList.Tables(0).Rows(0).Item(0)

'            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassessment_ratio", "ratiounkid", mintRatiounkid) = False Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Update Database Table (hrassessment_ratio) </purpose>
'    Public Function Update() As Boolean
'        If isExist(mintJobgroupunkid, mintPeriodunkid, mintRatiounkid) Then
'            mstrMessage = Language.getMessage(mstrModuleName, 1, "Ratio for BSC and General Assessment is already present for the selected Job Group(s) and Period(s)." & _
'                                              "As a result, The following Job Group and Period will be skipped.")
'            Return False
'        End If

'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            objDataOperation.AddParameter("@ratiounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRatiounkid.ToString)
'            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobgroupunkid.ToString)
'            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
'            objDataOperation.AddParameter("@ge_weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecGe_Weight)
'            objDataOperation.AddParameter("@bsc_weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecBsc_Weight)
'            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

'            strQ = "UPDATE hrassessment_ratio SET " & _
'                     "  jobgroupunkid = @jobgroupunkid" & _
'                     ", periodunkid = @periodunkid" & _
'                     ", ge_weight = @ge_weight" & _
'                     ", bsc_weight = @bsc_weight" & _
'                     ", isactive = @isactive " & _
'                     "WHERE ratiounkid = @ratiounkid "

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassessment_ratio", "ratiounkid", mintRatiounkid) = False Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    ''' <returns>Boolean</returns>
'    ''' <purpose> Delete Database Table (hrassessment_ratio) </purpose>
'    Public Function Delete(ByVal intUnkid As Integer) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "UPDATE hrassessment_ratio SET " & _
'                     " isactive = 0 " & _
'                     "WHERE ratiounkid = @ratiounkid "

'            objDataOperation.AddParameter("@ratiounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
'            Return False
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    ''' <summary>
'    ''' Modify By: Sandeep Sharma
'    ''' </summary>
'    ''' <purpose> Assign all Property variable </purpose>
'    Public Function isExist(ByVal iJobGroupId As Integer, ByVal iPeriodId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                   "  ratiounkid " & _
'                   ", jobgroupunkid " & _
'                   ", periodunkid " & _
'                   ", ge_weight " & _
'                   ", bsc_weight " & _
'                   ", isactive " & _
'                   "FROM hrassessment_ratio " & _
'                   "WHERE jobgroupunkid = @jobgroupunkid " & _
'                   "AND periodunkid = @periodunkid AND isactive = 1 "

'            If intUnkid > 0 Then
'                strQ &= " AND ratiounkid <> @ratiounkid"
'            End If

'            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPeriodId)
'            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iJobGroupId)
'            objDataOperation.AddParameter("@ratiounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList.Tables(0).Rows.Count > 0
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try
'    End Function

'    Public Sub Get_Ratio(ByRef iGE_Ratio As Decimal, ByRef iBSC_Ratio As Decimal, ByVal iJobGrpId As Integer, ByVal iPeriodId As Integer)
'        Dim StrQ As String = String.Empty
'        Dim exForce As Exception
'        Dim dsList As New DataSet
'        Try
'            objDataOperation = New clsDataOperation

'            StrQ = "SELECT " & _
'                   " ge_weight ,bsc_weight " & _
'                   "FROM hrassessment_ratio " & _
'                   "WHERE periodunkid = '" & iPeriodId & "' AND jobgroupunkid = '" & iJobGrpId & "' " & _
'                   "AND isactive = 1 "

'            dsList = objDataOperation.ExecQuery(StrQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            If dsList.Tables("List").Rows.Count > 0 Then
'                iGE_Ratio = dsList.Tables("List").Rows(0).Item("ge_weight")
'                iBSC_Ratio = dsList.Tables("List").Rows(0).Item("bsc_weight")
'            Else
'                iGE_Ratio = 0 : iBSC_Ratio = 0
'            End If

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Get_Ratio", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'End Class
